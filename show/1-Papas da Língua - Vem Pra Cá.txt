Papas da Língua - Vem Pra Cá

[Intro] C  Am7  Bm7  Em

      C      Am7       Bm7       Em
E|---------------------------------|
B|---------------------------------|
G|---------------------------------| (2x)
D|----0--2---0--5---7--4---0-------|
A|----------------------------2----|
E|---------------------------------|

     C    D/C     Bm7         Em
Não ver você não tem explicação
   C   D/C    Bm7      Em
É caminhar pela escuridão
 C      D/C    Bm7           Em
Ficar afim e não poder falar
    C    D/C    Bm7           Em
Querer o sim e não se acostumar

      C    Am7         Bm7       Em
E|---------------------------------|
B|---------------------------------|
G|---------------------------------| (2x)
D|--0--2-5----0-7--5-4-5-4--4/5---2|
A|---------------------------------|
E|---------------------------------|
          C              D/C
Com a solidão, o medo de amar

           Bm7           Em
Estranho vazio no seu olhar
           C              D/C
Eu tento achar em algum lugar
             Bm7             Em
O amor que você deixou pra trás

[Riff 2]
E|-7-10--7-12---7-10-8-7-8-10------|
B|---------------------------------|
G|---------------------------------| (2x)
D|---------------------------------|
A|---------------------------------|
E|---------------------------------|

        C  Am7         Bm7  Em         C  Am7        Bm7
Vem pra cá,   vem pra cá,    vem pra cá,  vem pra cá

(Repete tudo)

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
D/C = X 3 X 2 3 2
Em = 0 2 2 0 0 0
