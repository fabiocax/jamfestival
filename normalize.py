import os
import glob


for arquivo in glob.glob('jams_new/*.mp3'):
    arquivo_origem=arquivo
    arquivo_destino=arquivo.replace('jams_new/','jams/')

    os.system('ffmpeg-normalize "'+arquivo_origem+'" -c:a mp3 -f --keep-loudness-range-target -o "'+arquivo_destino+'"')
