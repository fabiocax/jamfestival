#! /usr/bin/python
# -*- coding: utf-8 -*-

import RPi.GPIO as gpio
import time
from requests import get
import threading
import os
os.chdir('/home/fabio/jam/jamfestival/')

gpio.setmode(gpio.BCM)

gpio.setup(18, gpio.IN, pull_up_down = gpio.PUD_UP)
gpio.setup(1, gpio.OUT)
gpio.setup(23, gpio.IN, pull_up_down = gpio.PUD_UP)
gpio.setup(7, gpio.OUT)
gpio.setup(8, gpio.OUT)
for i in [1,3]:
        gpio.output(8, gpio.HIGH)
        time.sleep(0.2)
        gpio.output(8, gpio.LOW)

        gpio.output(1, gpio.HIGH)
        time.sleep(0.2)
        gpio.output(1, gpio.LOW)
        gpio.output(7, gpio.HIGH)
        time.sleep(0.2)
        gpio.output(7, gpio.LOW)
        gpio.output(8, gpio.HIGH)

        gpio.output(7, gpio.HIGH)




def pisca_led():
        gpio.output(8, gpio.LOW)
        time.sleep(0.5)
        gpio.output(8, gpio.HIGH)

def pisca_led2():
        gpio.output(8, gpio.LOW)
        time.sleep(0.2)
        gpio.output(8, gpio.HIGH)
        time.sleep(0.2)
        gpio.output(8, gpio.LOW)
        time.sleep(0.2)
        gpio.output(8, gpio.HIGH)  

def pisca_led_play():
        status=get("http://127.0.0.1:8000/pi_busy").status_code()
        if status==200:
                gpio.output(1, gpio.LOW)
                time.sleep(0.2)
                gpio.output(1, gpio.HIGH)
        elif status==300:
                 gpio.output(1, gpio.HIGH)
        else:
                gpio.output(8, gpio.LOW)
                time.sleep(0.1)
                gpio.output(8, gpio.HIGH)  

def button_esquerdo_envents(tempo):
        if tempo >0:
                if tempo <=10:
                        print("execute previous")
                        get("http://127.0.0.1:8000/pi_previous")   
                        threading.Thread(target=pisca_led2).start()
                if tempo >10 :
                        
                        print("execute play")
                        get("http://127.0.0.1:8000/pi_play")
                        threading.Thread(target=pisca_led2).start()
                if tempo > 50:
                        os.system("su - fabio -c 'cd /home/fabio/jam/jamfestival/; git stash; git pull' ")
                        gpio.output(8, gpio.LOW)
        return 0


def button_direito_envents(tempo):
        if tempo >0:
                if tempo <=10:
                        print("execute next")
                        get("http://127.0.0.1:8000/pi_next")
                        threading.Thread(target=pisca_led2).start()
                if tempo >10 and tempo < 50:

                        print("execute stop")
                        get("http://127.0.0.1:8000/pi_stop")
                        threading.Thread(target=pisca_led2).start()
                if tempo > 50:
                        os.system("reboot")
                        gpio.output(8, gpio.LOW)

        return 0

button_esquerdo_count=0
button_direito_count=0
while True:

        if(gpio.input(18) == 1):
                gpio.output(1, gpio.LOW)
                button_esquerdo_envents(button_esquerdo_count)
                button_esquerdo_count=0
                
        else:
                
                button_esquerdo_count=button_esquerdo_count+1
                gpio.output(1, gpio.HIGH)
                if button_esquerdo_count >9:
                        threading.Thread(target=pisca_led).start()
                

        if(gpio.input(23) == 1):

                gpio.output(7, gpio.LOW)
                button_direito_envents(button_direito_count)
                button_direito_count=0
        else:
                button_direito_count=button_direito_count+1
                gpio.output(7, gpio.HIGH)
                if button_direito_count > 9:
                        threading.Thread(target=pisca_led).start()

        #threading.Thread(target=pisca_led_play).start()

        time.sleep(0.1)

gpio.cleanup()
exit()
