Dazaranha - Só Pra Se Dar Com As Estrelas

(intro)  Am G Dm F Em7

Am    G             Dm             
Inventou de ir pro céu             
           F     Em7         Am    
Só pra se dar com as estrelas        (2x)
Am   G              Dm             
Inventou de ir pro céu             
             F     Em7         Am  
Pra se encontar com as estrelas    

  Am                   G
O alvo era boca da noite
                    D/F#
Atirava-se na brincadeira
           F       Em7
Só pra se dar com as estrelas

Am                   G
Sentado debaixo do céu
                    D/F#
Sentado de frenta pro mar
       F
Sem estar contando estrelas
      Em7                 Am
Sem estar esperando a paz

D|-------|
A|-0-2-3-| (*)
E|-------|

(refrão)
*C                            G    
Viva e lambuza que a vida é sua    
     Dm        F        G            (2X)
E a lua nos reboca pro céu         

(solo 2x) Am G Dm F Em7 Am

(repete tudo)
(refrão 2x)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D/F# = 2 X 0 2 3 2
Dm = X X 0 2 3 1
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
