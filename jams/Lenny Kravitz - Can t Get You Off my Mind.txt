Lenny Kravitz - Can't Get You Off my Mind

G
Life is just a lonely highway

I'm out here on the open road
Am
I'm old enough to see behind me

But young enough to feel my soul
G
I don't wanna lose you baby

And I don't wanna be alone
Am
Don't wanna live my days without you

But for now I've got to be without you

[Refrão]

C                         D
I've got a pocket full of money
C                     D
And a pocket full of keys that have no bounds
C                     G
But when it comes to lovin'
   D                         C          G
I just can't get you off my mind, yeah

G                    Am
Babe can't you see........that this is killing me
G
I don't wanna push you baby

And I don't want you to be told
Am
It's just that I can't breathe without you

Feel like I'm gonna lose control

[Refrão]

C                         D
I've got a pocket full of money (oh yes I do)
C          D
And a pocket full of keys that have no bounds
C                       G
But when it comes to lovin'
  D                                   C  G
I just can't get you off my mind, yeah

         Eb7+
Am I a fool to think that there's a little hope?
G
Yeah

Tell me baby
      Eb7+
What are the rules, the reasons, and the do's and don'ts?
G
Yeah

( G  D/F#  F )

Tell me baby, tell me baby
             C
What do you feel inside?

( G Am  G Am )

[Refrão]

C                          D
I've got a pocket full of money
C                     D
And a pocket full of keys that have no bounds, oh yeah
C                       G
But when it comes to lovin'
  D                                   C  G
I just can't get you off my mind, yeah
  D                                   C  G
I just can't get you off my mind, yeah

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Eb7+ = X X 1 3 3 3
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
