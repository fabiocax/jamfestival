ABBA - Dancing Queen

[Intro] G  C/G  G  C/G
        G  C/G  G
        D/F#  Em7  D

[Refrão]

 D              B7
You can dance, you can jive
 Em7                    A/C#
Having the time of your life
     C              Am7
Oh, see that girl, watch that scene
             G
Diggin' the dancing queen

( C/G  G  C/G  G  C/G )

[Primeira Parte]

  G                              C/G
Friday night and the lights are low
 G                          Em7  D4  D
Looking out for a place to go
  D                        D4   D
Where they play the right music

Getting in the swing
                  D         Em  D  Em
You've  come to look for a king

G                      C/G
Anybody could be that guy
 G                              Em7  D4  D
Night is young and the music's high
 D                  D4   D
With a bit of rock music
               D4
everything is fine
               D          Em  D  Em
You're in the mood for a dance
              Am7           D
And when you get the chance

[Refrão]

             G
You are the dancing queen
 C/G                   G        C/G
Young and sweet, only seventeen
 G              C/G
Dancing queen, feel the beat
          G
From the tambourine
D/F#  Em  D
Oh   yeah

 D              B7
You can dance, you can jive
 Em7                    A/C#
Having the time of your life
     C              Am7
Oh, see that girl, watch that scene
             G            C/G
Diggin' the dancing queen

( G  C/G  G  C/G  G  C/G )

[Segunda Parte]

 G                           C/G
You're a tease, you turn 'em on
 G                                 Em7  D4  D
Leave 'em burning and then you're gone
                  D4    D
Looking out for another

anyone will do
               D          Em  D  Em
You're in the mood for a dance
              Am7           D
And when you get the chance

[Refrão]

             G
You are the dancing queen
 C/G                   G       C/G
Young and sweet, only seventeen
 G              C/G
Dancing queen, feel the beat
          G
From the tambourine
D/F#  Em  D
Oh   yeah

 D              B7
You can dance, you can jive
 Em7                    A/C#
Having the time of your life
     C              Am7
Oh, see that girl, watch that scene
             G
Diggin' the dancing queen

( C/G  G  C/G )

             G        C/G
Diggin' the dancing queen

[Final] G  C/G  G
        C/G  G  C/G
        G  C/G  G
        C/G  G  C/G


----------------- Acordes -----------------
G = 3 2 0 0 0 3
A/C# = X 4 X 2 5 5
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
C/G = 3 3 2 X 1 X
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D4 = X X 0 2 3 3
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
