Garotos da Rua - Tô de saco cheio

[Tab - Intro]

  A                D                    A  (4x)          D
E|---------------------------------------|----|--------------------------|
B|---------------------------------------|--10|--------------------------|
G|------9----------7---------------------| 11--|11-9-9--7-----------------|
D|-7--9^-9^-9^-7-9-7---7---9^-7---7^-----|----|--------7--7--9^---7--7-9^|
A|-7-----------------9---9------9---9-7--|----|------9---9-9-------9-----|
E|---------------------------------------|----|--------------------------|

[Tab - Solo]

E|-------------12-------12--------------------------------------12-|
B|--10--10--12^--12^-12^--12^--10-12-10-----------------10--12^----|
G|----11--11-------11--------11---------11-9--------9-11-----------|
D|------------------------------------------11-9^-11---------------|
A|-----------------------------------------------------------------|
E|-----------------------------------------------------------------|

E|-/12-12-12-/12-12-12-10-----------------------|
B|-/12-12-12-/1| 2-12-12-10---10-12-10----10-12-|
G|-------------------------------------11-------|
D|----------------------------------------------|
A|----------------------------------------------|
E|----------------------------------------------|

E|---14p12----12h14p12----12h14~-----14----17^-17^-17~--|
B|-12--------14----------14--------------14-------------|
G|------------------------------------------------------|
D|------------------------------------------------------|
A|------------------------------------------------------|
E|------------------------------------------------------|

(A D)
Lá em casa continuam
Os mesmos problemas
Lá em casa continuam
Me perturbando
Lá em casa continuam
Enchendo o saco

(A D)
Se metem na minha vida não dão folga o dia inteiro
Tão sempre reclamando de dinheiro
Não posso banho demorado
Nem ficar cantando no chuveiro

(A D)
Lá em casa continuam
Os mesmos problemas
Lá em casa continuam
Me perturbando
Lá em casa continuam
Enchendo o saco

(A D)
Não deixam escutar o som mais alto
Os coroas não querem mudar
Ficam me cobrando profissão bem-sucedida
Pra me fazer seguro de vida

(A D)
Lá em casa continuam
Os mesmos problemas
Lá em casa continuam
Me perturbando
Lá em casa continuam
Enchendo o saco

[Solo]

Lá em casa continuam
Os mesmos problemas
Lá em casa continuam
Me perturbando
Lá em casa continuam
Enchendo o saco

(A D)
A todo momento tem um bico no meu quarto
É roupa cabelo, as más companhias
Querem que eu guarde economia na poupança
Porque que eu tenho que ser um rapaz de família

(A D)
Lá em casa continuam
Os mesmos problemas
Lá em casa continuam
Me perturbando
Lá em casa continuam
Enchendo o saco

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
