Engenheiros do Hawaii - O Papa É Pop

[Intro] Dm  G  Bb9  C  Dm
        Dm  G  Bb9  C  Dm

E|------------------------------------------|
B|-13b15r13-10------------------------------|
G|-------------12---10----10-12-12~---------|
D|---------------------12-------------------|
A|------------------------------------------|
E|------------------------------------------|

E|------------------------------------------|
B|--------------10---------------10---------|
G|-----10-10-12----12-10------------12~-----|
D|-/12-------------------12---12------------|
A|------------------------------------------|
E|------------------------------------------|

E|------------------------------------------|
B|-13b15r13-10------------------------------|
G|-------------12---10----10-12-12~---------|
D|---------------------12-------------------|
A|------------------------------------------|
E|------------------------------------------|

E|------------------------------------------|
B|--------------10--------------------------|
G|-----10-10-12----12-10--------------------|
D|-/12-------------------12-----------------|
A|------------------------------------------|
E|------------------------------------------|

[Primeira Parte]

 F               C
Todo mundo tá relendo
                 Bb9 ( Bb9  C )
O que nunca foi lido
 F                 C
Todo mundo tá comprando
            Bb9   ( Bb9  C )
Os mais vendidos

[Pré-Refrão]

  F                  C
Qualquer nota, qualquer notícia
 Bb9                C
Páginas em branco, fotos coloridas
  F                  C
Qualquer nova, qualquer notícia
  Bb9            C              Bb9
Qualquer coisa que se mova é um alvo
              C
E ninguém tá salvo

[Segunda Parte]

 F               C
Todo mundo tá revendo
                 Bb9 ( Bb9  C )
O que nunca foi visto
 F         C
Tá na cara
                 Bb9  ( Bb9  C )
Tá na capa da revista

[Pré-Refrão]

  F                 C
Qualquer nota, uma nota preta
 Bb9                C
Páginas em branco, fotos coloridas
  F             C
Qualquer rota, rotatividade
  Bb9            C              Bb9
Qualquer coisa que se mova é um alvo
              C
E ninguém tá salvo
       Bb9        C
Um disparo, um estouro

[Refrão]

   Dm            G
O papa é pop, o papa é pop
   Bb9        C      Dm
O pop não poupa ninguém
   Dm                    G
O papa levou um tiro à queima roupa
   Bb9        C      Dm
O pop não poupa ninguém

   Dm            G
O papa é pop, o papa é pop
   Bb9        C      Dm
O pop não poupa ninguém
   Dm                    G
O papa levou um tiro à queima roupa
   Bb9        C      Dm
O pop não poupa ninguém

[Terceira Parte]

F               C
Uma palavra na tua camiseta
Bb9
    O planeta na tua cama
F           C
Uma palavra  escrita a lápis
Bb9
    Eternidades da semana

[Pré-Refrão]

  F                  C
Qualquer nota, qualquer notícia
 Bb9                C
Páginas em branco, fotos coloridas
  F                  C
Qualquer nova, qualquer notícia
  Bb9            C              Bb9
Qualquer coisa que se mova é um alvo
              C
E ninguém tá salvo
       Bb         C
Um disparo, um estouro

[Refrão]

   Dm            G
O papa é pop, o papa é pop
   Bb9        C      Dm
O pop não poupa ninguém
   Dm                    G
O papa levou um tiro à queima roupa
   Bb9        C      Dm
O pop não poupa ninguém

   Dm            G
O papa é pop, o papa é pop
   Bb9        C      Dm
O pop não poupa ninguém
   Dm                    G
O papa levou um tiro à queima roupa
   Bb9        C      Dm
O pop não poupa ninguém

[Ponte]

 Bb9
Toda catedral é populista

É pop, é macumba prá turista

E afinal o que é rock n' roll?

Os óculos do John, ou o olhar do Paul

[Solo] Dm  G  Bb9  C  Dm
       Dm  G  Bb9  C  Dm

[Refrão]

   Dm            G
O papa é pop, o papa é pop
   Bb9        C      Dm
O pop não poupa ninguém
   Dm                    G
O papa levou um tiro à queima roupa
   Bb9        C      Dm
O pop não poupa ninguém

   Dm            G
O papa é pop, o papa é pop
   Bb9        C      Dm
O pop não poupa ninguém
   Dm                    G
O papa levou um tiro à queima roupa
   Bb9              
O pop não poupa ninguém
   Bb9              
O pop não poupa ninguém
   Bb9              Dm
O pop não poupa ninguém

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bb9 = X 1 3 3 1 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
