Ramones - Poison Heart

[Intro]

   C#m     B            C#m        B
E|-0-0---0---0-------------0-----0--------|
B|-5-5---5---5---4h5p4-----5-----5--------|
G|-6---4---4---4-------4-6---4-6---4------|
D|-6--------------------------------------|
A|----------------------------------------|
E|----------------------------------------|

               C#m      B               A
E|-0-------------0---0----0---------------|
B|-5---4h5p4-----5---5----5----4h5p4------|
G|---4-------4-6---4---4----4--------4-2~-|
D|----------------------------------------|
A|----------------------------------------|
E|----------------------------------------|

C#5          E5     C#5          E5
No one ever thought this one would survive
C#5             E5            A5        B5
Helpless child, gonna walk a drum beat behind
C#5            E5    C#5          E5
Lock you in a dream, never let you go
C#5          E5        A5        B5
Never let you laugh or smile, not you

       A5            B5          C#5     B5    A5
Well, I just want to walk right out of this world
       A5       B5           C#5
'Cause everybody has a poison heart
   A5            B5          C#5     B5    A5
I just want to walk right out of this world
        A5    B5           C#5      B5   C#5  B5
'Cause everybody has a poison heart

C#5                     E5       C#5          E5
Making friends with a homeless torn up man
C#5      E5                    A5         B5
He just kind of smiles, it really shakes me up
C#5                       E5         C#5     E5
There's danger on every corner but I'm okay
C#5              E5                A5         B5
Walking down the street trying to forget yesterday

   A5            B5           C#5     B5     A5
I just want to walk right out of this world
        A5     B5           C#5     (2x)
'Cause everybody has a poison heart

B5           C#5 B5            C#5   B5           A5
  A  poison heart, a poison heart, a poison heart

Yeah

[Solo]
          E                    A
E|------------------------------------------|
B|------------------------------------------|
G|----------------------7-7-7-8--8s10-8-7---| (2x)
D|-7-7-7-8--8s10-8-7----------------------7-|
A|-------------------8----------------------|
E|------------------------------------------|

               C#5    E5     C#5        E5
You know that life really takes its toll
         C#        E             A5         B5
And a poet's gut reaction is to search his very soul
 C#              E5       C#5       E5
So much damn confusion before my eyes
        C#5             E5          A5                B5
But nothing seems to phase me and this one still survives

   A5           B5          C#5     B5    A5
I just want to walk right out of this world
        A5     B5           C#5                (2x)
'Cause everybody has a poison heart

   A5            B5          C#5     B5    A5
I just want to walk right out of this world

        A5     B5
'Cause everybody has

          C#5    B5
A poison heart     (7x)
         A5
A poison heart

( A5  B5  C#5 )

----------------- Acordes -----------------
A = X 0 2 2 2 0
A5 = X 0 2 2 X X
B = X 2 4 4 4 2
B5 = X 2 4 4 X X
C# = X 4 6 6 6 4
C#5 = X 4 6 6 X X
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
E5 = 0 2 2 X X X
