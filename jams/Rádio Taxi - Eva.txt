Rádio Taxi - Eva

[Intro] D  Bm  G  Em  A4  A

E|15-----14~--------------14-15----------------------------------|
B|18b20--17b19~-----15-17-------17b19-15-17b19-17b19r17-15~------|
G|--------------s14----------------------------------------------|
D|---------------------------------------------------------------|
A|---------------------------------------------------------------|
E|---------------------------------------------------------------|

E|--------14-15-------19-19--------------------------------------|
B|---15-17-------17b19-----19r17-17b19r17-15~-17b19r17~----------|
G|s14--------------------------------------------------14s16s14~-|
D|---------------------------------------------------------------|
A|---------------------------------------------------------------|
E|---------------------------------------------------------------|

     G                              A
Meu amor olha só hoje o sol não apareceu
     G                         D
É o fim da aventura humana na Terra
      G                                   A
Meu planeta adeus fugiremos nós dois na arca de Noé
          G                           D
Olha meu amor o final da odisseia terrestre
            A
Sou Adão e você será

               D                   D   A/C#    Bm
Minha pequena Eva, o nosso amor na última astronave
            Bm/A   D/F#  G   D/F#  Em
Além do infinito eu vou voar
  Em/D       A
Sozinho com você
               D                   D     A/C#    Bm
E voando bem alto, me abraça pelo espaço de um instante
                 Bm/A  D/F#  G  D/F#  Em
Me cobre com teu corpo e me dá
        A
A força pra viver

                       Bm                        G
E pelo espaço de um instante afinal, não há nada mais
      Em                 G
Que o céu azul pra gente voar
                             A
Sobre o Rio, Beirute ou Madagascar
        G                      D
Toda a terra reduzida a nada a nada mais
                    G                               A
E minha vida é um flash de controles, botões anti-atômicos
               G                           D
Olha bem meu amor é o fim da odisseia terrestre
            A
Sou Adão e você será

               D                   D   A/C#    Bm
Minha pequena Eva, o nosso amor na última astronave
            Bm/A   D/F#  G   D/F#  Em
Além do infinito eu vou voar
  Em/D       A
Sozinho com você
               D                   D     A/C#    Bm
E voando bem alto, me abraça pelo espaço de um instante
                 Bm/A  D/F#  G  D/F#  Em
Me cobre com teu corpo e me dá
        A
A força pra viver

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A4 = X 0 2 2 3 0
Bm = X 2 4 4 3 2
Bm/A = X 0 4 4 3 X
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
Em/D = X X 0 4 5 3
G = 3 2 0 0 0 3
