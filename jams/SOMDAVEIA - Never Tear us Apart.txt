INXS - Never Tear us Apart

[Intro] Am  F  Dm  F
        Am  F  Dm  F
        Am  F  Dm  F
        Am  Dm  F

      Am                F
Don't ask me what you know is true
               Dm          F
Don't have to tell you, I love your precious heart

C  F         C      F           C       F
I,   I was standing   you were there
               C        F                 G
Two worlds collided       and they could never, tear us apart

E|-------------------------------|
B|-----0--3-5-0----0----3-5--0---|
G|-------------------------------|

          Am              F
We could live     For a thousand years
          Dm                   F
But if I hurt you    I'd make wine from your tears

   Am               F
I told you     That we could fly
         Dm                           F
'Cause we all have wings     But some of us don't know why

C  F         C      F           C       F
I,   I was standing   you were there
               C        F                 G
Two worlds collided       and they could never, ever tear us apart

E|-------------------------------|
B|-----0--3-5-0----0----3-5--0---|
G|-------------------------------|

[Solo] Am  F  Dm  F

E|-11/12b~~~~~~12-11-10-7/8-----------12/15-12p10-12-12b--10-8--8/10-10/12-12/13-13/15-|
B|---------------------------10p8-10---------------------------------------------------|
G|-------------------------------------------------------------------------------------|

C       F
I (Don't ask me)
         C             F
I was standing (You know it's true)
               C              F
Mm, you were there (Worlds collided)
              C                     F
Two worlds collided (We're shining through)
               G
And they could never tear us apart

C       F
You (Don't ask me)
           C                      F
You were standing (You know it's true)
       C               F
I was there (Worlds collided)
              C                     F
Two worlds collided (We're shining through)
               G
And they could never tear us apart

C      F
I-i-i
       C         F
I was standing
         C      F
You were there
C  F         C      F           C       F
I (Don't ask me),   i was standing   you were there
               C      
Two worlds collided       
F                 G
  and they could never, ever tear us apart

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
