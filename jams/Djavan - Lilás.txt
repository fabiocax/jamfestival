Djavan - Lilás

[Intro] Am7(11)  F7M  Am7(11)  F7M
        Am7(11)  F7M  Am7(11)  F7M

[Dedilhado -Intro 4x]

   Am7(11)      F7M
E|-----3---3------0-------------------------|
B|-----3---3------1-----0h1-----------------|
G|-----5---5------2-----0h2-----------------|
D|-----5---5------2-------------------------|
A|-0-0---0---0------------------------------|
E|--------------1---1-1---------------------|
       ↓   ↓      ↓

[Primeira Parte]

[Dedilhado - Primeira Parte]

Parte 1 de 4

   Am7(11)
E|---3-----x-3--3---------------------------|
B|---3-----x-3--3---------------------------|
G|---5-----x-5--5---------------------------|
D|---5-----x----5---------------------------|
A|-0---0-0-x-0----0-0-x-0-------------------|
E|--------------------x---------------------|
     ↓          ↓

Parte 2 de 4

   G6(9)
E|---0-----x----0---------------------------|
B|---0-----x-0--0---------------------------|
G|---2-----x-2--2---------------------------|
D|---2-----x-2--2---------------------------|
A|---------x----------x---------------------|
E|-3---3-3-x-3----3-3-x-0-------------------|
     ↓          ↓

Parte 3 de 4

   F7M       F7M(11+)
E|---0-----x----0---------------------------|
B|---1-----x-0--0---------------------------|
G|---2-----x-2--2---------------------------|
D|---2-----x-2--2---------------------------|
A|---------x----------x---------------------|
E|-1---1-1-x-1----1-1-x-1-------------------|
     ↓          ↓

Parte 4 de 4

   Dm7          E7(9+)
E|---1-----x------3-----x-------------------|
B|---1-----x------3-----x-------------------|
G|---2-----x------1-----x-------------------|
D|-0---0-0-x-0----2-----x-------------------|
A|----------------------x-------------------|
E|--------------0---0-0-x-0-----------------|
     ↓            ↓

Parte 4 de 4 (com variação)

   Dm7          F/G
E|---1-----x------------x-------------------|
B|---1-----x------1-----x-------------------|
G|---2-----x------2-----x-------------------|
D|-0---0-0-x-0----3-----x-------------------|
A|----------------------x-------------------|
E|--------------3---3-3-x-------------------|
     ↓            ↓

Am7(11)
        Amanhã, outro dia
G6(9)
      Lua sai, ventania
   F7M            F7M(11+)
Abraça uma nuvem que passa no ar
 Dm7             E7(9+)
Beija, brinca e deixa passar

Am7(11)
        E no ar de outro dia
G6(9)
      Meu olhar surgia
     F7M                F7M(11+)
Nas pontas de estrelas perdidas no mar
  Dm7                   F/G
Pra chover de emoção, trovejar

[Pré-Refrão]

[Dedilhado - Pré-Refrão]

Parte 1 de 2 - 2x

   Am   G6(9)    F7M
E|----0-----0------0------x-----------------|
B|-1--1-----0------1------x-----------------|
G|-2--2-----2------2------x-----------------|
D|-2--2-----2------2------x-----------------|
A|-0----------------------x-----------------|
E|------3-3---3--1----0-0-x-----------------|
      ↓     ↓      ↓      

Parte 2 de 2

   Am          B/A
E|----0-----0-------------------------------|
B|-1--1-----1---2---------------------------|
G|-2--2-----2---2---------------------------|
D|-2--2-----2---2---------------------------|
A|-0----0-0---1-2---------------------------|
E|------------------------------------------|
      ↓     ↓    

         Am         G6(9)  F7M
Raio se libertou, clareou muito mais
 Am          G6(9)    F7M
Se encantou pela cor lilás
          Am
Prata na luz do amor
      A/B
Céu azul

[Refrão]

                C7M       Em7
Eu quero ver o pôr do sol
           C7M    Em7
Lindo como ele só
                    F#7(13-)
E gente pra ver, e viajar
 Am7(9)        Em
No seu mar de raio

[Tab - Refrão]

   Em
E|-0----------------------------------------|
B|-0----------------------------------------|
G|-0----------------------------------------|
D|-2-0--------------------------------------|
A|-----2-0----------------------------------|
E|---------3-0------------------------------|
   ↓

                C7M       Em7
Eu quero ver o pôr do sol
           C7M    Em7
Lindo como ele só
                    F#7(13-)
E gente pra ver, e viajar
 Am7(9)        Em
No seu mar de raio

( C7M  F7M(11+)  Em )
( C7M  F7M(11+)  Em )
( C7M  F7M(11+)  Em )
( C7M  F7M(11+)  Em )

[Tab - Interlúdio]

Parte 1 de 2

   C7M            F7M(11+)
E|-0-0--------0-x--0-0--------0-x-----------|
B|-0---3p0--3---x--0---3p0--3---x-----------|
G|-0---0----0---x--2---2----2---x-----------|
D|-----0--------x------2--------x-----------|
A|-3--------3---x---------------x-----------|
E|-----------------1--------1---x-----------|
       ↓               ↓

Parte 2 de 2

   Em
E|-0-0--------0-x--0-0--------0-x-----------|
B|-0---3p0--3---x--0---3p0--3---x-----------|
G|-0---0----0---x--0---0----2---x-----------|
D|-----2--------x------2--------x-----------|
A|--------------x---------------x-----------|
E|-0--------0---x--0--------1---x-----------|
       ↓               ↓
         
[Primeira Parte]

Am7(11)
        Amanhã, outro dia
G6(9)
      Lua sai, ventania
   F7M            F7M(11+)
Abraça uma nuvem que passa no ar
 Dm7             E7(9+)
Beija, brinca e deixa passar

Am7(11)
        E no ar de outro dia
G6(9)
      Meu olhar surgia
     F7M                F7M(11+)
Nas pontas de estrelas perdidas no mar
  Dm7                   F/G
Pra chover de emoção, trovejar

[Pré-Refrão]

         Am         G6(9)  F7M
Raio se libertou, clareou muito mais
 Am          G6(9)    F7M
Se encantou pela cor lilás
          Am
Prata na luz do amor
      A/B
Céu azul

[Refrão]

                C7M       Em7
Eu quero ver o pôr do sol
           C7M    Em7
Lindo como ele só
                    F#7(13-)
E gente pra ver, e viajar
 Am7(9)        Em
No seu mar de raio

                C7M       Em7
Eu quero ver o pôr do sol
           C7M    Em7
Lindo como ele só
                    F#7(13-)
E gente pra ver, e viajar
 Am7(9)        Em
No seu mar de raio

( C7M  F7M(11+)  Em )
( C7M  F7M(11+)  Em )
( C7M  F7M(11+)  Em )
( C7M  F7M(11+)  Em )

[Refrão]

                C7M       Em7
Eu quero ver o pôr do sol
           C7M    Em7
Lindo como ele só
                    F#7(13-)
E gente pra ver, e viajar
 Am7(9)        Em
No seu mar de raio

                C7M       Em7
Eu quero ver o pôr do sol
           C7M    Em7
Lindo como ele só
                    F#7(13-)
E gente pra ver, e viajar
 Am7(9)        Em
No seu mar de raio

[Final] C7M  Em7  C7M  Em7
        F#7(13-)  Am7(9)  Em

----------------- Acordes -----------------
A/B = X P2 2 2 2 X
Am = X 0 2 2 1 0
Am7(11) = X 0 5 5 P3 3
Am7(9) = X 0 5 5 0 0
B/A = X 0 4 4 4 X
C7M = X 3 2 0 0 X
Dm7 = X X 0 2 P1 1
E7(9+) = X X 2 1 3 3
Em = X X 2 0 0 0
Em7 = 0 2 2 0 3 0
F#7(13-) = 2 X 2 3 3 0
F/G = 3 X 3 2 1 X
F7M = 1 X 2 2 1 0
F7M(11+) = 1 X 2 2 0 0
G6(9) = 3 X 2 2 0 0
