U2 - Stuck in a Moment You Can't Get Out Of

Intro: D  D9/F#  G  D9/F#  A5(9) Bm  G  D

D          D9/F#       G            D9/F#
I'm not afraid of anything in this world
         A5(9)             Bm               G
There's nothing you can throw at me that I haven't
           D
  already heard
                    D9/F#    G        D9/F#
I'm just trying to find a decent melody
   A5(9)           Bm        G         D
A song that I can sing in my own company
Bm                            G
  I never thought you were a fool
E                       G
  But darling, look at you
Bm                       G                      Bm
   You gotta stand up straight, carry your own weight
                       A5(9)
These tears are going nowhere, baby

D         D9/F#  G            D9/F#
  You've got to get yourself together
            A5(9)      Bm                 G        D
You've got stuck in a moment and now you can't get out of it
D        D9/F#    G             D9/F#
  Don't say that later will be better now you're
  A#º       Bm             G                    D
stuck in a moment and you can't get out of it

               D9/F#       G               D9/F#
I will not forsake, the colours that you bring
         A5(9)                  Bm
But the nights you filled with fireworks
      G                D
They left you with nothing
               D9/F#         G                    D9/F#
I am still enchanted by the light you brought to me
         A5(9)              Bm           G
I still listen through your ears, and through your eyes
D
I can see
Bm                   G
 And you are such a fool
E               G
 To worry like you do
Bm             G                             Bm
  I know it's tough, and you can never get enough
                          A5(9)
Of what you don't really need now... my oh my

D         D9/F#  G            D9/F#
  You've got to get yourself together
             A5(9)     Bm                 G        D
You've got stuck in a moment and now you can't get out of it

     D9/F# G           D9/F#
 Oh love  look at you now
                      A#º       Bm
You've got yourself stuck in a moment and now you
 G        D
can't get out of it

Em
I was unconscious, half asleep
     G                                   D
The water is warm till you discover how deep...
Em
I wasn't jumping... for me it was a fall
        G                          C9     A5(9)
It's a long way down to nothing at all

D         D9/F#  G            D9/F#
  You've got to get yourself together
            A5(9)      Bm                 G        D
You've got stuck in a moment and now you can't get out of it
D        D9/F#    G             D9/F#
  Don't say that later will be better now you're
  A#º       Bm             G                    D
stuck in a moment and you can't get out of it

    D9/F#   G         D
And if the night runs over
A5(9)   Bm      G         D
    And if the day won't last
    D9/F#   G         D9/F#
And if our way should falter
A#º   Bm       G     D
    Along the stony pass (2x)

It's just a moment
                D
This time will pass

----------------- Acordes -----------------
Capotraste na 2ª casa
Cº*  = X 1 2 0 2 0 - (*Dº na forma de Cº)
B5(9)*  = X 0 2 2 0 0 - (*C#5(9) na forma de B5(9))
C#m*  = X 2 4 4 3 2 - (*D#m na forma de C#m)
D9*  = X 3 2 0 3 3 - (*E9 na forma de D9)
E*  = X X 0 2 3 2 - (*F# na forma de E)
E9/G#*  = 2 X 0 2 3 0 - (*F#9/A# na forma de E9/G#)
F#*  = 0 2 2 1 0 0 - (*G# na forma de F#)
F#m*  = 0 2 2 0 0 0 - (*G#m na forma de F#m)
A*  = 3 2 0 0 0 3 - (*B na forma de A)
