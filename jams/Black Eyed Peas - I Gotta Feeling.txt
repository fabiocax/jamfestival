Black Eyed Peas - I Gotta Feeling

(Riff 1)

E|-----------------------------------------------------
B|--3-3-3-3-1-1-0-0-1-1-1-1-1-1-1-1-0-0-0-0-0-0-0-0-1-1
G|--0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0
D|-----------------------------------------------------
A|-----------------------------------------------------
E|-----------------------------------------------------

E|------------| 
B|-1-1-1-1-1-1| 
G|-0-0-0-0-0-0| 
D|------------| 
A|------------| 
E|------------| 

[Refrão]

          G
I gotta  feeling
        C
That tonight's

Be a good night
        Em
That tonight's

Gonna be a good night
        C
That tonight's

Gonna be a good good night

          G
I gotta  feeling
        C
That tonight's

Be a good night
        Em
That tonight's

Gonna be a good night
        C
That tonight's

Gonna be a good good night

          G
I gotta  feeling
        C
That tonight's

Be a good night
        Em
That tonight's

Gonna be a good night
        C
That tonight's

Gonna be a good good night

          G
I gotta  feeling
        C
That tonight's

Be a good night
        Em
That tonight's

Gonna be a good night
        C
That tonight's

Gonna be a good good night

( G  C  Em  C )

[Primeira Parte]

G
 Tonight's the night
Let's live it up
C
 I got my money
Let's spend it up
Em
  Go out and smash it
Like Oh My God
C
 Jump off that sofa
Let's get get off
G
 I know that we'll have a ball
C
 If we get down and go out
And just loose it all
Em
  I feel stressed out
I wanna let it go
C
 Let's go way out spaced out
And loosing all control
G
 Fill up my cup
Mazal tov
C
 Look at her dancing
Just take it off
Em
  Let's paint the town
We'll shut it down
C
 Let's burn the roof
And then we'll do it again
G
 Let's do it
Let's do it
Let's do it
       C
Let's do it
And do it
And do it
Let's live it up
     Em
And do it
And do it
And do it, do it, do it
       C
Let's do it
Let's do it
Let's do it 'cuz

[Refrão]

          G
I gotta  feeling
        C
That tonight's

Be a good night
        Em
That tonight's gonna

Be a good night
        C
That tonight's gonna

Be a good good night

[Segunda Parte]

G
 Tonight's the night
Let's live it up
C
 I got my money
Let's spend it up
Em
  Go out and smash it
Like Oh My God
C
 Jump off that sofa
Let's get get off

G
 Fill up my cup (Drink)
Mazal tov (Le chaim)
C
 Look at her dancing
(Move it Move it)
Just take it off
Em
  Let's paint the town
We'll shut it down
C
 Let's burn the roof
And then we'll do it again

G
 Let's do it
Let's do it
Let's do it
       C
Let's do it
And do it
And do it
Let's live it up
     Em
And do it
And do it
And do it, do it, do it
       C
Let's do it
Let's do it
Let's do it, do it, do it

 G
Here we come
Here we go
We gotta rock

C
Easy come
Easy go
Now we on top

 Em
Feel the shot
Body rock
Rock it don't stop
 C
Round and round
Up and down
Around the clock
 G
Monday, Tuesday
Wednesday and Thursday
  C
Friday, Saturday
Saturday and Sunday
 Em
Get get get get with us
You know what we say
 C
Party every day
Pa pa pa Party every day

[Refrão]

          G
I gotta  feeling
        C
That tonight's

Be a good night
        Em
That tonight's

Gonna be a good night
That tonight's

Gonna be a good good night
          G
I gotta  feeling
        C
That tonight's

Be a good night
        Em
That tonight's

Gonna be a good night
        C
That tonight's

Gonna be a good good night


----------------- Acordes -----------------
C = X 3 2 0 1 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
