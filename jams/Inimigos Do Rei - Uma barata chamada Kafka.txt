Inimigos Do Rei - Uma barata chamada Kafka

(intro)

    C                  F    G        C                    F F# G Bb C
|-----------------------------------------------------------------------
|-----------------------------------------------------------------------
|-5-5------------------------------5-5--------------------------3-5----- (2x)
|-5-5-----------------3-3--5-5-----5-5--------------------3-4-5-3-5-----
|-3-3-----1-----1-----3-3--5-5-----3-3------1------1------3-4-5-1-3-----
|------3-----3----0---1-1--3-3-----------3------3-----0---1-2-3---------

   C           F          Bb    G7
Encontrei uma barata na cozinha
C              F   Bb              G7
Eu olhei pra ela   ela olhou pra mim
    C      F       Bb          G7
Ofereci a ela um pedaço de pudim
    C      F           Bb G7
O curioso foi que ela

           C  G7     F
Ela disse sim   vem cá ficar comigo
  C    F                    G7
Sim! Gosta de tudo que eu gosto
C   G7      F          G7
Sim!   Vem cá ficar comigo
C   G7        F   G7
Sim!   Vem, kakfa

   C     F            Bb          G7
Ofereci a ela um disco do Sex pistols
     C    F          Bb      G7
Ofereci a ela uma batida de limão
    C         F     Bb         G7
Perguntei se ela gostava dos beatles
    C         F       Bb       G7
Perguntei se ela era de escorpião
           C  G7     F
Ela disse sim   vem cá ficar comigo
  C    F                    G7
Sim! Gosta de tudo que eu gosto
C   G7      F          G7
Sim!   Vem cá ficar comigo
C   G7        F   G7
Sim!   Vem, kakfa

F            G7                    F
Você mora na barata ribeiro num edifício
                     G7
Que tem um buraco perto do chuveiro
   F                           G7
Já se drogou com detefon, insetizan, fumou baygon
F                        G7
Tudo quanto é tipo de veneno você acha bom

C   G7      F          G7
Sim!   Vem cá ficar comigo
 C    F                    G7
Sim! Gosta de tudo que eu gosto
C   G7      F
Sim!   Vem kafungá!

Como posso evitar essa coincidencia
Encontrar uma barata com a minha aparencia!
Como posso evitaaaarrr!

(2x)

Lacucaratia, lacucaratia
Tome cuidado com a sandalia de borracha

C   G7      F          G7
Sim!   Vem cá ficar comigo
 C    F                    G7
Sim! Gosta de tudo que eu gosto
C   G7        F   G7
Sim!   Vem, kakfa

(2x)

Lacucaratia, lacucaratia
Tome cuidado com a sandalia de borracha!!

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
