Lulu Santos - Tudo Bem

[Intro] C7M  F7M

[Primeira Parte]

              C7M       F7M
Já não tenho dedos pra contar
            C7M          F7M
De quantos barrancos despenquei
           C7M          Am7
E quantas pedras me atiraram
             E7(5+)  E7
Ou quantas atirei
       Am7            E7(5+)
Tanta farpa tanta mentira
       Am7          D7(4)  D7
Tanta falta do que dizer
     Dm    Em  F7M
Nem sempre é "so easy"
     F/G  F7M  Bb7M  Eb7M
 Se viver

                C7M              F7M
Hoje eu não consigo mais me lembrar
            C7M         F7M
De quantas janelas me atirei
          C7M             Am7
E quanto rastro de incompreensão
       E7(5+)  E7
Eu já deixei
       Am7                E7(5+)
Tanto bons quanto maus motivos
        Am7         D7(4)  D7
Tantas vezes desilusão ... e quase
  Dm   Em      F7M   F/G  Bb/C  C7
Quase nunca a vida é um  balão

[Refrão]

       F7M         F/G
Mas o teu amor me cura
Abº          Am7       D7(4)  D7
  De uma loucura qualquer
        F7M
É encostar no seu peito
   Abº           G7
E se isso for algum defeito
           C7M  F7M
Por mim   tudo bem

[Solo] C7M  F7M  C7M  F7M

[Segunda Parte]

                C7M              F7M
Hoje eu não consigo mais me lembrar
            C7M         F7M
De quantas janelas me atirei
          C7M             Am7
E quanto rastro de incompreensão
       E7(5+)  E7
Eu já deixei
       Am7                E7(5+)
Tanto bons quanto maus motivos
        Am7         D7(4)  D7
Tantas vezes desilusão ... e quase
  Dm   Em      F7M   F/G  Bb/C  C7
Quase nunca a vida é um  balão

[Refrão]

       F7M         F/G
Mas o teu amor me cura
Abº        Am7      D7(4)   D7
   De uma loucura qualquer
     F7M
É encostar no seu peito
   Abº           G7
E se isso for algum defeito
           C7M  F7M
Por mim   tudo bem

       F7M         F/G
Mas o teu amor me cura
Abº        Am7      D7(4)   D7
   De uma loucura qualquer
     F7M
É encostar no seu peito
   Abº           G7
E se isso for algum defeito
           C7M  F7M
Por mim   tudo bem

[Final] C7M  F7M

----------------- Acordes -----------------
Capotraste na 2ª casa
Bbº*  = 4 X 3 4 3 X - (*Cº na forma de Bbº)
Bm7*  = X 0 2 0 1 0 - (*C#m7 na forma de Bm7)
C/D*  = X 3 X 3 3 1 - (*D/E na forma de C/D)
C7M*  = X 1 3 2 3 1 - (*D7M na forma de C7M)
D7*  = X 3 2 3 1 X - (*E7 na forma de D7)
D7M*  = X 3 2 0 0 X - (*E7M na forma de D7M)
E7*  = X X 0 2 1 2 - (*F#7 na forma de E7)
E7(4)*  = X X 0 2 1 3 - (*F#7(4) na forma de E7(4))
Em*  = X X 0 2 3 1 - (*F#m na forma de Em)
F#7*  = 0 X 0 0 1 0 - (*G#7 na forma de F#7)
F#7(5+)*  = 0 X 0 1 1 0 - (*G#7(5+) na forma de F#7(5+))
F7M*  = X X 1 3 3 3 - (*G7M na forma de F7M)
F#m*  = 0 2 2 0 0 0 - (*G#m na forma de F#m)
G/A*  = 3 X 3 2 1 X - (*A/B na forma de G/A)
G7M*  = 1 X 2 2 1 X - (*A7M na forma de G7M)
A7*  = 3 5 3 4 3 3 - (*B7 na forma de A7)
