Chris Isaak - Wicked Game

[Intro] Bm  A  E  Bm  A  E
        Bm  A  E  Bm  A  E
        
[Primeira Parte] 

     Bm                    
The world was on fire and 
 A                        E
No one could save me but you
        Bm                     
It's strange what desire will
 A                   E
Make foolish people do
Bm                   A                     
I never dreamed that I'd meet 
               E
Somebody like you
Bm                   A                       
I never dreamed that I'd lose
          E
Somebody like you 

[Refrão] 

    Bm  A                   E
No, I  don't wanna fall in love

(This world is only gonna 

Break your heart)

    Bm  A                    E
No, I  don't wanna  fall in love

(This world is only gonna 

Break your heart)
      Bm  A  E
With you
      Bm  A  E
With you

(This world is only gonna 

Break your heart)

[Segunda Parte] 

Bm                A
   What a wicked game to play
E
  To make me feel this way
Bm                 A 
   What a wicked thing to do
E
  To let me dream of you
Bm                 A 
   What a wicked thing to say
E
  You never felt this way
Bm                 A 
   What a wicked thing to do
E
  To make me dream of you and

[Refrão] 

Bm  A                   E
I  don't wanna fall in love

(This world is only gonna 

Break your heart)

    Bm  A                   E
No, I  don't wanna fall in love

(This world is only gonna 

Break your heart)
      Bm  A  E
With you

[Solo] Bm  A  E  Bm  A  E
       Bm  A  E 

[Primeira Parte] 

     Bm                    
The world was on fire and 
 A                        E
No one could save me but you
        Bm                     
It's strange what desire will
 A                   E
Make foolish people do
Bm                   A                     
I never dreamed that I'd love 
          E
Somebody like you
Bm                   A                       
I never dreamed that I'd lose
          E
Somebody like you 

[Refrão Final] 

    Bm  A                   E
No, I  don't wanna fall in love

(This world is only gonna 

Break your heart)
    Bm  A                   E
No, I  don't wanna fall in love

(This world is only gonna 

Break your heart)

      Bm  A  E
With you

(This world is only gonna 

Break your heart)

      Bm  A  E
With you

(This world is only gonna 

Break your heart)

    Bm  A  E
No, I  

(This world is only gonna 

Break your heart)

( Bm  A  E )

(This world is only gonna 

Break your heart)

Bm        A          E
   Nobody   loves no one

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
E = 0 2 2 1 0 0
