Engenheiros do Hawaii - Dom Quixote

[Intro] A  A5(9)  A
        D5(7M/9)  D5(9)  D5(7M/9)
        A  A5(9)  A
        D5(7M/9)  D5(9)  D5(7M/9)

[Primeira Parte]

 A             A5(9)       A
Muito prazer, meu nome é otário
 D5(7M/9)              
Vindo de outros tempos 
 D5(9)           D5(7M/9)
Mas sempre no horário
 A               A5(9)   
Peixe fora d'água
      A          D5(7M/9)  D5(9)
Borboletas no aquário

 A             A5(9)       A
Muito prazer, meu nome é otário
    D5(7M/9)        
Na ponta dos cascos 
D5(9)      D5(7M/9)
E fora do páreo
 A           A5(9)
Puro sangue    
   A        D5(7M/9)  D5(9)
Puxando carroça

( A A5(9) A )
( D5(7M/9) D5(9) D5(7M/9) )
( A A5(9) A )
( D5(7M/9) D5(9) D5(7M/9) )

A               A5(9)    A
Um prazer cada vez mais raro
 D5(7M/9)         D5(9)      D5(7M/9)
Aerodinâmica num tanque de guerra
A                    
Vaidades que a terra 
A5(9) A         D5(7M/9) D5(9)
Um   dia há de comer

A                A5(9)   A
As de espadas fora do baralho
  D5(7M/9)         D5(9)        D5(7M/9)
Grandes negócios, pequeno empresário
 A            A5(9)         D5(7M/9) D5(9)
Muito prazer me chamam de otário
 Bm                    D5
Por amor às causas perdidas

[Refrão]

 A               C#7
Tudo bem... até pode ser
           F#m       
Que os dragões 
        F#m/E     D5
Sejam moinhos de vento

 A               C#7
Tudo bem... seja o que for
      F#m       
Seja por amor
    F#m/E     D5
Às causas perdidas
 Bm                    E
Por amor às causas perdidas

( F#m  D5(9)  F#m  F#m/E  D5(9) )
( Bm  D5(9)  Bm  E )

[Refrão]

 A              C#7
Tudo bem...até pode ser
           F#m       
Que os dragões 
        F#m/E     D5
Sejam moinhos de vento

 A                   C#7
Muito prazer... ao seu dispor
        F#m       
Se for por amor 
    F#m/E     D5
Às causas perdidas
 Bm                    E
Por amor às causas perdidas

[Final] A  A5(9)  A
        D5(7M/9)  D5(9)  D5(7M/9)
        A  A5(9)  A
        D5(7M/9)  D5(9)  D5(7M/9)  A

----------------- Acordes -----------------
A = X 0 2 2 2 0
A5(9) = X 0 2 2 0 0
Bm = X 2 4 4 3 2
C#7 = X 4 3 4 2 X
D5 = X 5 7 7 X X
D5(7M/9) = X X 0 2 2 0
D5(9) = X 5 7 9 X X
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
F#m/E = X X 2 2 2 2
