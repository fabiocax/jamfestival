Al Green - Let´s Stay Together

[Intro] Gm7  Am7  Gm7  Am7
        Gm7  Am7  Gm7  C7

[Primeira Parte]

F7M           Am/D
I, I'm so in love with you
             Bb
Whatever you want to do
                  Bbm9/Db
Is all right with me
      Am7 Gm7       F7M   Em       Dm9
Cause you   make me feel  so brand new
   Am7 Gm7  F7M           Am7        Dm9
And I     want to spend my life with you

[Segunda Parte]

            F7M                       Am/D
Let me say since, since we've been together
              Bb
Loving you forever
          Bbm9/Db
Is what I need
Am7  Gm7      F7M           Em    Dm9
Let me be the one you come running to
Am7  Gm7    F7M    Em Dm9
I'll  never be    untrue

[Refrão]

 Gm9                Am7
Let's, let's stay together
       Gm9
Lovin' you whether, whether
     Bb7M     Am7   Dm7     C
Times are good or bad, happy or sad

[Solo] Gm9  Ab7M  Gm9   Ab9   Ab    

           Bb7M    Am7   Dm7     C
Whether times are good or bad, happy or sad

[Terceira Parte]

 F7M                 Am/D
Why, why some people break up
                     Bb
Then turn around and make up
          Bbm9/Db
I just can't see
Am7   Gm7   F7M     Em    Dm9
You'd never do   that to me (would you, baby)
Am7    Gm7   F7M      Em      Dm9
Staying around you is all I see (Here's what I want us to do)

Gm9                      Am7
Let's, we oughta stay together
           Gm9
Loving you whether, whether
     [Gm9  ] Am7   Gm7     C
Times are good or bad, happy or sad

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Ab7M = 4 X 5 5 4 X
Ab9 = 4 6 8 5 4 4
Am/D = X X 0 2 1 0
Am7 = X 0 2 0 1 0
Bb = X 1 3 3 3 1
Bb7M = X 1 3 2 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm7 = X 5 7 5 6 5
Dm9 = X 5 7 9 6 5
Em = 0 2 2 0 0 0
F7M = 1 X 2 2 1 X
Gm7 = 3 X 3 3 3 X
Gm9 = 3 5 7 3 3 3
