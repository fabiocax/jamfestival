Lulu Santos - Tempos Modernos

[Primeira Parte] 

Dm            F       C        G
   Eu vejo a vida melhor no futuro
Dm         F         C          G
   Eu vejo isso por cima de um muro
      F    Am   Dm            F      G
De hipocrisia que insiste em nos rodear

Dm            F         C        G
   Eu vejo a vida mais farta e clara
Dm          F       C      G
   Repleta de toda satisfação
        F     Am    Dm      F         G
Que se tem direito do firmamento ao chão

[Segunda Parte] 

Dm            F       C        G
   Eu quero crer no amor numa boa
Dm           F          C          G
   Que isso valha pra qualquer pessoa
      F   Am    Dm           F       G
Que realizar a força que tem uma paixão

Dm             F      C      G
   Eu vejo um novo começo de era
Dm           F        C          G
   De gente fina, elegante e sincera
       F   Am    Dm 
Com habilidade pra dizer
      F          G
Mais sim do que não

[Refrão] 

              Bb
Hoje o tempo voa, amor
               Dm
Escorre pelas mãos
                 C
Mesmo sem se sentir
                  Bb
Não há tempo que volte, amor
                      Dm
Vamos viver tudo que há pra viver
                C
Vamos nos permitir

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
