Vitor Kley - A Cura (part. Lulu Santos)

[Intro] D  G  D  G

   D                G
E|-------0h2p0------------------------------|
B|-----3-------3----------3-----------------|
G|---2-----------2------0-------------------|
D|-0------------------0---------------------|
A|------------------------------------------|
E|------------------3-----------------------|

   D                G
E|-------0h2p0------------------------------|
B|-----3-------3----------3-----------------|
G|---2-----------2------0-------------------|
D|-0------------------0---------------------|
A|------------------------------------------|
E|------------------3-----------------------|

[Primeira Parte] 

D        Bbº  Bm              Em7
  Existirá,   em todo porto tremulará
D             Bm       F#m  A7
  A velha bandeira da vida
D         Bbº  Bm        Em7
  Acenderá    todo farol iluminará
D            A7     G  
  Uma ponta de esperança

D         Bbº  Bm          Em7
  E se virá,  será quando menos se esperar
D              Bm     F#m  A7
  Da onde ninguém imagina
D        Bbº  Bm           Em7          
  Demolirá,  toda certeza vã, não sobrará
D        A7       D   
  Pedra sobre pedra

[Segunda Parte] 

Em7         Bm7(11)       D       A7
   Enquanto isso não nos custa insistir
        Em7      Bm7(11)      D            A
Na questão do desejo, não deixar se extinguir
C       G        A     Bm7(11)
  Desafiando de vez a noção
E7                         A7  A4(6)         A7  A6  
   Na qual se crê que o infe________rno é aqui

[Riff] 

   A7 A4(6) A7 A6
E|------------------------------------------|
B|-2--3-----5--7----------------------------|
G|------------------------------------------|
D|-2--4-----5--7----------------------------|
A|-0----------------------------------------|
E|------------------------------------------|

[Refrão] 

D        Bbº
  Existirá
Bm             Em7
E toda raça então experimentará
 D           A      G
Para todo o mal, a cura

D   G   A         G
   Iê, iê, na-na-ná
D   G   A         G
   Iê, iê, na-na-ná

[Primeira Parte] 

D        Bbº  Bm              Em7
  Existirá,   em todo porto tremulará
D             Bm       F#m  A7
  A velha bandeira da vida
D        Bbº  Bm        Em7
  Acenderá   todo farol iluminará
D            A7     G  
  Uma ponta de esperança

D         Bbº  Bm          Em7
  E se virá,  será quando menos se esperar
D              Bm     F#m  A7
  Da onde ninguém imagina
D        Bbº  Bm           Em7          
  Demolirá,  toda certeza vã, não sobrará
D        A7       D   
  Pedra sobre pedra

[Segunda Parte] 

Em7         Bm7(11)       D       A7
   Enquanto isso não nos custa insistir
        Em7      Bm7(11)      D            A
Na questão do desejo, não deixar se extinguir
C       G        A     Bm7(11)
  Desafiando de vez a noção
E7                         A7  A4(6)         A7  A6  
   Na qual se crê que o infe________rno é aqui

[Refrão] 

D        Bbº
  Existirá
Bm             Em7
E toda raça então experimentará
D              A      G
  Para todo o mal, a cura, a cura

D   G   A         G
   Iê, iê, na-na-ná
D   G   A         G
   Iê, iê, na-na-ná
D   G   A         G
   Iê, iê, na-na-ná
D   G   A         G
   Iê, iê, na-na-ná

[Segunda Parte] 

Em7          Bm7(11)       D       A7
    Enquanto isso não nos custa insistir
        Em7      Bm7(11)      D            A
Na questão do desejo, não deixar se extinguir
C       G        A     Bm7(11)
  Desafiando de vez a noção
E7                         A7  A4(6)         A7  A6  
   Na qual se crê que o infe________rno é aqui

[Refrão] 

D        Bbº
  Existirá
Bm             Em7
E toda raça então experimentará
 D           A      G   Gm  D
Para todo o mal, a cura

----------------- Acordes -----------------
Capotraste na 2ª casa
B*  = X 0 2 2 2 0 - (*C# na forma de B)
B4(6)*  = 5 4 2 2 3 2 - (*C#4(6) na forma de B4(6))
B6*  = 5 X 4 6 5 X - (*C#6 na forma de B6)
B7*  = X 0 2 0 2 0 - (*C#7 na forma de B7)
Cº*  = X 1 2 0 2 0 - (*Dº na forma de Cº)
C#m*  = X 2 4 4 3 2 - (*D#m na forma de C#m)
C#m7(11)*  = 7 X 7 7 5 X - (*D#m7(11) na forma de C#m7(11))
D*  = X 3 2 0 1 0 - (*E na forma de D)
E*  = X X 0 2 3 2 - (*F# na forma de E)
F#7*  = 0 2 2 1 3 0 - (*G#7 na forma de F#7)
F#m7*  = 0 2 2 0 3 0 - (*G#m7 na forma de F#m7)
G#m*  = 2 4 4 2 2 2 - (*A#m na forma de G#m)
A*  = 3 2 0 0 0 3 - (*B na forma de A)
Am*  = 3 5 5 3 3 3 - (*Bm na forma de Am)
