Papas da Língua - No Calor da Hora

D       G              D        G
Olha só aquela estrela lá no céu

D                 G            D    G
Será que existe alguém morando nela

D         G          D        G
Olha só o infinito é tão legal
D              G     D
O que será que vem depois, depois

Em    Gm          A
Esse papo já não vai
Em   G               D
É melhor deixar pra lá

A        G                   D        G
Feche os olhos não finja pra mim, voce está afim
A       G                  D                 G   Gm6
Abre os braços me diga que sim: não sei não a resposta!

D    G      A     D
No calor da hora
                G           A  D
Meu bem voce não disse que não
                 G     A     D
Meu bem voce não foi embora

    G      A      D
No calor da hora
                G          A          D
Meu bem voce não disse que não, não, não
                 G     A
Meu bem voce não foi embora

Bm          Em
E O Dia amanheceu em paz
G          D
E O Dia amanheceu em paz

Em         G        D
E O Dia amanheceu em paz

E|--3/5--10-10-9-7-9-------------7-----------------------------------|
B|-------------------10-7--10-10------8-7-5----8-7-5-7\5--7\5--------|
G|----------------------------------7--------7-----------------------|
D|-------------------------------------------------------------------|
A|-------------------------------------------------------------------|
E|-------------------------------------------------------------------|

A        G                   D         G
Feche os olhos não finja pra mim, voce está afim
A       G                  D                   G
Abre os braços me diga que sim: não sei não a resposta!

D    G
No calor da hora
D                 G
Meu bem voce não disse que não
D            G
Meu bem voce não foi embora

D    G      A
No calor da hora
D                G          A
Meu bem voce não disse que não, não, não
D                 G     A
Meu bem voce não foi embora

Bm             Em
E O Dia amanheceu em paz
G              D
E O Dia amanheceu em paz

Em         G      D
O Dia amanheceu em paz

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
Gm6 = 3 X 2 3 3 X
