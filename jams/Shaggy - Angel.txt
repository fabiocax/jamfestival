Shaggy - Angel


E|--------------| 


Tuning - Tune down whole step - D G C F A D

[Intro]

F#                B                        
  Now this one's dedicated to all the 
       C#                                 
youths who want to say nice things to all 
 B
them girls  hahaha
F#                   B                 
   Treat y'all like diamonds and pearls, 
          C#                                 
(Dedicated to all the girls around the world)
   B

F#                                  B   
     Now this is Rayvon and Shaggy with a 
                        C#        B
combination you can't miss  hahaha
                        F#                 B 
Flip this one upon the musical disc,  (Wah!  
  C#       B
   Uh- uh!    Wah- wah- wah!)

[Chorus]

F#                B                C#    
   Girl you’re my angel, you’re my darlin' 
   B
angel
F#                B                C#   B
   Closer than my peeps you are to me,  baby
F#                  B                 C#   
   Shorty you’re my angel, you’re my 
   B
darlin' angel
F#                  B                   C# 
   Girl, you are my friend when I’m in 
  B
need, lady

[Verse 1]

F#               B
  Life a one big party when you’re still 
young
C#                          B
  But who’s gonna have your back when it all 
done?
F#                                B
(yeah) It's all good when you're little, you 
have pure fun
C#                    B
Can’t be a fool, son, what about the long 
run?
F#                         B
(yah)  Lookin' back shorty always a mention
C#                   B
   Say me not givin' her much attention
F#                              B
(yeah) She was there through my incarceration
  C#                    B
I wanna show the nation my appreciation

[Chorus]

F#                B                C#    
   Girl you’re my angel, you’re my darlin' 
   B
angel (uh!)
F#                B                C#   B
   Closer than my peeps you are to me,  baby 
 (selah!)
F#                  B                 C#   
   Shorty you’re my angel, you’re my 
   B
darlin' angel
F#                  B                   C# 
   Girl, you are my friend when I’m in 
  B
need, lady

[Verse 2]

F#                          B
  You’re a queen and that’s how you should be
 treated  (Ah!)
C#                          B
   Though you never get the lovin' that you 
needed
F#                          B
(yeah)   Coulda left but I called and you 
heeded
C#                    B
Begged and I pleaded, mission completed
F#                         B
(yah) Mama said that I and I dissed the 
program
C#                        B
   Not the type to mess around with your 
emotions
F#                              B
(yeah)   But the feelin' that I have for you 
are so strong
C#                     B
Been together so long, this could never be 
wrong.

[Chorus]

F#                B                C#    
   Girl you’re my angel, you’re my darlin' 
   B
angel (uh!)
F#                B                C#   B
   Closer than my peeps you are to me,  baby
F#                  B                 C#   
   Shorty you’re my angel, you’re my 
   B
darlin' angel
F#                  B                   C# 
   Girl, you are my friend when I’m in 
  B
need, lady

[Bridge]

F#                         B
   Girl, in-spite of my behavior ,
                C#                   B
 see you’re my savior, (you must be sent from
 up above)
F#                        B
   Ah you appear to me so tender
       C#                 B
 I surrender (Thanks for givin' me your 
love.)
F#                         B
   Girl, in-spite of my behavior,
                C#                   B
 see you’re my savior, (you must be sent from
 up above)
F#                        B
   Ah you appear to me so tender,
                   D     N.C.
 well, girl I surrender (Said thanks for 
givin' me your love.)

[Verse 3]

F#               B
  Life a one big party when you’re still 
young
C#                          B
  And who’s gonna have your back when it all 
done?
F#                                B
(yeah) It's all good when you're little, you 
have pure fun
C#                    B
Can’t be a fool, son, what about the long 
run?
F#                         B
(yah)  Lookin' back shorty always a mention
C#                   B
   Say me not givin' her much attention
F#                              B
(yeah) She was there through my incarceration
  C#                    B
I wanna show the nation my appreciation

[Chorus]

F#                B                C#    
   Girl you’re my angel, you’re my darlin' 
   B
angel (wah!)
F#                B                C#   B
   Closer than my peeps you are to me,  baby
F#                  B                 C#   
   Shorty you’re my angel, you’re my 
   B
darlin' angel  (ah!)
F#                  B                   C# 
   Girl, you are my friend when I’m in 
  B
need, lady

[Chorus]

F#                B                C#    
   Girl you’re my angel, you’re my darlin' 
   B
angel (uh-wah)
F#                B                C#   B
   Closer than my peeps you are to me,  baby
F#                  B                 C#   
   Shorty you’re my angel, you’re my 
   B
darlin' angel
F#                  B                   C# 
   Girl, you are my friend when I’m in 
  B
need, lady

[Outro]

F# B   C#  B
F#


----------------- Acordes -----------------
B = X 2 4 4 4 2
C# = X 4 6 6 6 4
F# = 2 4 4 3 2 2
