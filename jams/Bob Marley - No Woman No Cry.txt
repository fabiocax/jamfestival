Bob Marley - No Woman No Cry

[Intro] C  G4/B  Am  F7M
        C  F7M  C/E  C

[Tab - Intro]

  C        G4/B       Am       F
E|-------------------------------0---------|
B|----1----0--1--0-------1--x--1---x-------|
G|----0----0-------0-----2--x--2---x-------|
D|--2---2--0-------------2--x--3---x--0----|
A|3--------2----------0-----x------------0-|
E|-----------------------------------------|

  C         C  C/E  C
E|-----------------------------------------|
B|----1-----1--1----------1----------------|
G|----0-----2--0----------0----------------|
D|--2---2---3--2--0-----2---2--------------|
A|3------------------3-----------0-2-------|
E|-----------------------------3-----3-----|

[Refrão]

[Tab - Refrão]

  C        G4/B       Am       F
E|-------------------------------0---------|
B|----1----0--1--0-------1--x--1---x-------|
G|----0----0-------0-----2--x--2---x-------|
D|--2---2--0-------------2--x--3---x--0----|
A|3--------2----------0-----x------------0-|
E|-----------------------------------------|

  C         C  C/E  C
E|-----------------------------------------|
B|----1-----1--1----------1----------------|
G|----0-----2--0----------0----------------|
D|--2---2---3--2--0-----2---2--------------|
A|3------------------3-----------0-2-------|
E|-----------------------------3-----3-----|

C     G4/B      Am  F7M
  No woman no cry
 C  F7M  C/E      C
No wo___man  no cry

C     G4/B      Am  F7M
  No woman no cry
 C  F7M  C/E      C
No wo___man  no cry

[Primeira Parte]

[Tab - Primeira Parte]

C         G4/B      Am      F7M
E|-------------------------------0---------|
B|-1--1------1--1------1---1---1-----------|
G|-0--0------0--0------2---2---2-----------|
D|-2--2------0--0------2---2---3----0------|
A|-3--3--3---2--2--2---0---0----------0----|
E|-----------------------------------------|

C          G4/B    Am             
  Say I remember when
            F7M
We used to sit
C             G4/B         
  In a government yard
     Am       F7M
In Trenchtown
C         G4/B             
  Oba, observing
     Am        F7M
The hypocrites
         C            G4/B          
As they would mingle with
                    Am  F7M
The good people we meet

C                  G4/B
  Good friends we have
     Am                   F7M
Oh, good friends we have lost
C   G4/B     Am  F7M
  Along the way
C                G4/B
  In this great future
     Am                F7M
You can't forget your past
C              G4/B     Am  F7M
  So dry your tears, I seh

[Refrão 2]

C     G4/B      Am  F7M
  No woman no cry
 C  F7M  C/E      C
No wo___man  no cry

G4/B                Am            
     Little darling
            F7M
Don't shed no tears
 C  F7M  C/E      C
No wo___man  no cry

[Segunda Parte]

C           G4/B    
  Said I remember
  Am             F7M
When we used to sit
C               G4/B       
  In the government
          Am       F7M
Yard in Trenchtown
C           G4/B         
  And then Georgie
       Am            F7M
Would make the fire lights
          C          G4/B                 
As it was,  logwood burning'
             Am    F7M
Through the nights

C                G4/B               
  Then we would cook
     Am           F7M
Cornmeal porridge
C                 G4/B      Am  F7M
  Of which I'll share with you
C     G4/B      Am          F7M
  My feet is my only carriage
C              G4/B         Am
  So I've got to push on through
           F7M
But, while I'm gone, I mean

C                     G4/B
  Everything’s gonna be alright
Am               F7M
   Everything’s gonna be alright
C                     G4/B
  Everything’s gonna be alright
Am               F7M
   Everything’s gonna be alright

[Refrão]

C     G4/B      Am  F7M
  No woman no cry
 C  F7M  C/E      C
No wo___man  no cry

C     G4/B      Am  F7M
  No woman no cry
 C  F7M  C/E      C
No wo___man  no cry

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C/E = X X 2 0 1 0
F = 1 3 3 2 1 1
F7M = X X 3 2 1 0
G4/B = X 2 0 0 1 X
