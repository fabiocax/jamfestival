Elton John - Rocket Man

Capotraste na 3ª casa

[Primeira Parte]

Em                                  A7       
   She packed my bags last night, pre-flight  
Em                   A7  G/B
   Zero hour, nine a.m.
C                   G/B  Am   Am/G                
  And I'm gonna be hi______gh    
               D/F#
As a kite by then

Em                              A7    
   I miss the earth so much, I miss my wife
Em                      A7  G/B
   It's lonely out in space
C            G/B  Am         Am/G  D/F#  D7
  On such a ti______meless flight

[Refrão]

G                                         C
  And I think it's gonna be a long, long time
                                         G
Till touchdown brings me round again to find
                                    C
I'm not the man they think I am at home
            G/B       A7
Oh no, no, no, I'm a Rocket Man
 C                                 G        C
Rocket Man, burning out a fuse up here alone

G                                         C
  And I think it's gonna be a long, long time
                                         G
Till touchdown brings me round again to find
                                    C
I'm not the man they think I am at home
            G/B       A7
Oh no, no, no, I'm a Rocket Man
 C                                 G        C
Rocket Man, burning out a fuse up here alone

[Segunda Parte]

Em                                  A7           
   Mars ain't the kind of place to raise your kids
Em                      A7  G/B
   In fact its cold as hell
C                      G/B     Am        Am/G      
  And there's no one there to raise them
        D/F#
If you did

Em                                  A7      
   And all this science, I don't understand
Em                            A7
   It's just my job 5 days a week
   G/B    C  G/B  Am    Am/G        D/F#
A Rocket ma__________an,    Rocket man

[Refrão]

G                                         C
  And I think it's gonna be a long, long time
                                         G
Till touchdown brings me round again to find
                                    C
I'm not the man they think I am at home
            G/B       A7
Oh no, no, no, I'm a Rocket Man
 C                                 G        C
Rocket Man, burning out a fuse up here alone

G                                         C
  And I think it's gonna be a long, long time
                                         G
Till touchdown brings me round again to find
                                    C
I'm not the man they think I am at home
            G/B       A7
Oh no, no, no, I'm a Rocket Man
 C                                 G        C
Rocket Man, burning out a fuse up here alone

                             G              C
And I think it's gonna be a long, long time
                             G              C
And I think it's gonna be a long, long time
                             G              C
And I think it's gonna be a long, long time
                             G              C
And I think it's gonna be a long, long time
                             G              C
And I think it's gonna be a long, long time
                             G              C
And I think it's gonna be a long, long time

----------------- Acordes -----------------

C7*  = X 0 2 0 2 0 - (*D#7 na forma de C7)
Cm*  = X 0 2 2 1 0 - (*D#m na forma de Cm)
Cm/A#*  = 3 X 2 2 1 X - (*D#m/C# na forma de Cm/A#)
D#*  = X 3 2 0 1 0 - (*F# na forma de D#)
F/A*  = 2 X 0 2 3 2 - (*G#/C na forma de F/A)
F7*  = X X 0 2 1 2 - (*G#7 na forma de F7)
Gm*  = 0 2 2 0 0 0 - (*A#m na forma de Gm)
A#*  = 3 2 0 0 0 3 - (*C# na forma de A#)
A#/D*  = X 2 0 0 3 3 - (*C#/F na forma de A#/D)
