Nenhum de Nós - Eu não Entendo (Acústico)

(intro) A  A5+ Bm E

A
Por que você não disse que viria?
A5+
Logo agora que eu tinha
Bm
Me curado das feridas
E
Que você abriu quando se foi

A
Por que chegou sem avisar?
A5+
Eu queria tempo prá me preparar
Bm
Com roupa limpa, a casa em ordem
E
E um sorriso falso prá enganar

D
Eu não entendo a sua volta
E
Eu não entendo a sua indecisão
D
Num dia sou seu grande amor
E
No outro dia não
F#m
Eu não entendo a sua volta
A
Eu não entendo a sua indecisão
Bm
Num dia sou seu grande amor
E
No outro dia não Não Não

(intro)

A
Por que a surpresa da sua volta?
A5+
Justo quando eu tento vida nova
Bm
Você vem prá perguntar
E
Se tudo que eu sentia acabou
A
Você até parece um vício
A5+
Que largar é quase impossível
Bm
Exige muito sacrifício
F
E quando eu me considerava limpo
Bm                       E
Vem você prá me oferecer mais Mais! Mais! ( 2X )

(refrão)

(solo)
E|---------------------------------------------------------------------------------------------
B|---------------------------------------------------------------------------------------------
G|----------------4h6-6---6~7-6-4-2~4-2-2--6-6~7-6-4-2~4---4-4h6-4--4-6------------------------
D|----2~4-2-------------------------------------------------------------6-4--2-----------------
A|-------------4-------------------------------------------------------------------------------
E|---------------------------------------------------------------------------------------------

E|---------------------------------------------------------------12----10-----7-------------------
B|------------------------9------9---9--------10h12-12--12--10h12----10----7h9----7h9-10-12-10----
G|--------9----9~11-9~11--9~11-9---9~11-9---------------------------------------------------------
D|---9~11---11------------------------------------------------------------------------------------
A|------------------------------------------------------------------------------------------------
E|------------------------------------------------------------------------------------------------

(parte repete varias vezes no refrão pós-solo)

E|----------------12-------12------------
B|------------10-------10----10----------
G|----11~13------------------------------
D|---------------------------------------
A|---------------------------------------
E|---------------------------------------

(refrão)
(intro)

----------------- Acordes -----------------
A = X 0 2 2 2 0
A5+ = X 0 3 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
