Green Day - Boulevard Of Broken Dreams

[Primeira Parte]

Em           G
   I walk a lonely road
    D             A             Em
The only one that I have ever known
            G
Don't know where it goes
 D               A
But its home to me and I walk alone

[Riff 1] Em7  G  D  A7(4)

   Em7         G           D
E|-------2--3--------7--5--------2--3-------|
B|-------3--3--------0--0--------3--3-------|
G|-0--0--0--0--------0--0--2--2--2--2-------|
D|-2--2--------0--0--------0--0-------------|
A|-2--2--------x--x--------0--0-------------|
E|-0--0--------3--3-------------------------|

   A7(4)
E|-------3----------------------------------|
B|-3--3--3----------------------------------|
G|-2--2--2----------------------------------|
D|-2--2-------------------------------------|
A|-0--0-------------------------------------|
E|------------------------------------------|

[Variação]

   A7(4)       G6/B
E|------------------------------------------|
B|-3--3--3--3--3----------------------------|
G|-0--0--0--0--0----------------------------|
D|-2--2--2--0--0----------------------------|
A|-0--0--0--2--2----------------------------|
E|------------------------------------------|

Em7             G6
    I walk this empty street
D            A4                   Em7
On the boulevard of broken dreams
           G6
Where the city sleeps
    D            A7(4)                Em7  G
And I'm the only one and I walk alone

D          A7(4)            Em7  G
  I walk alone I walk alone
D          A7(4)      G6/B
  I walk alone I walk a

[Refrão]

C9      G5    D5(9)          Em7(9)
   My shadows only one that walks beside me
C9      G5      D5(9)           Em7(9)
   My shallow hearts the only thing that's beating
C9      G5      D5(9)            Em7(9)
   Sometimes I wish someone up there will find me
C9        G5       B7
   Till then I'll walk alone

( Em  G  D  A )
( Em  G  D  A )

[Primeira Parte - Variação]

Em7              G6
    I'm walking down the line
  D5(9)          A4              Em7
That divides me somewhere in my mind
        G          D5(9)
On the border line of the edge
     A7(4)
And were I walk alone

( Em7  G  D  A7(4) )

Em7          G6                 D5(9)
    Read between the lines of what's
              A4                Em7
Fucked up and every things all right
          G6              D5(9)          A7(4)
Check my vital signs to know I'm still alive
                 Em7  G
And I walk alone

D          A7(4)            Em7  G
  I walk alone I walk alone
D          A7(4)      G6/B
  I walk alone I walk a

[Refrão]

C9      G5    D5(9)          Em7(9)
   My shadows only one that walks beside me
C9      G5      D5(9)           Em7(9)
   My shallow hearts the only thing that's beating
C9      G5      D5(9)            Em7(9)
   Sometimes I wish someone up there will find me
C9        G5       B7
   Till then I'll walk alone

( Em  G  D  A )
( Em  G  D  A )

Em7  G  D          A7(4)      G6/B
          I walk alone I walk a

( C9  G  D  Em7(9) ) (3X)
( C9  G  B11 )

   C9          G           D
E|-0--0--2--3--------7--5--------2--3-------|
B|-3--3--3--3--------0--0--------3--3-------|
G|-0--0--0--0--------0--0--2--2--2--2-------|
D|-x--x--------0--0--------0--0-------------|
A|-3--3--------x--x--------0--0-------------|
E|-------------3--3-------------------------|

   Em7(9)
E|-------3----------------------------------|
B|-------3----------------------------------|
G|-0--0--0----------------------------------|
D|------------------------------------------|
A|-2--2--2----------------------------------|
E|-0--0--0----------------------------------|

   C9          G           B11
E|-0--0--2--3--------7--5--0--0--0--0--0----|
B|-3--3--3--3--------0--0--4--4--4--4--4----|
G|-0--0--0--0--------0--0--4--4--4--4--4----|
D|-2--2--------0--0--------x--x--x--x--x----|
A|-3--3--------0--0--------2--2--2--2--2----|
E|------------3--3--------------------------|

E|-0--0--0--0--0--0--0--0-------------------|
B|-4--4--4--4--4--4--4--4-------------------|
G|-4--4--4--4--4--4--4--4-------------------|
D|-x--x--x--x--x--x--x--x-------------------|
A|-2--2--2--2--2--2--2--2-------------------|
E|------------------------------------------|

Em7             G6
    I walk this empty street
D            A4                   Em7
On the boulevard of broken dreams
           G6
Where the city sleeps
    D            A7(4)
And I'm the only one and I walk a

[Refrão]

C9      G5    D5(9)          Em7(9)
   My shadows only one that walks beside me
C9      G5      D5(9)           Em7(9)
   My shallow hearts the only thing that's beating
C9      G5      D5(9)            Em7(9)
   Sometimes I wish someone up there will find me
C9        G5       B7
   Till then I'll walk alone

[Final] E5  C7M  D6(9)  A9/C#  G6  B11/D#
        E5  C7M  D6(9)  A9/C#  G6  B11/D#
        E5  C7M  D6(9)  A9/C#  G6  B11/D#  Em7(9)

----------------- Acordes -----------------
Capotraste na 1ª casa
A#*  = X 0 2 2 2 0 - (*B na forma de A#)
A#4*  = X 0 2 2 3 0 - (*B4 na forma de A#4)
A#7(4)*  = X 0 2 0 3 0 - (*B7(4) na forma de A#7(4))
A#9/D*  = X 4 2 2 0 X - (*B9/D# na forma de A#9/D)
C11*  = X 2 4 4 5 2 - (*C#11 na forma de C11)
C11/E*  = X 6 4 4 5 7 - (*C#11/F na forma de C11/E)
C7*  = X 2 1 2 0 2 - (*C#7 na forma de C7)
C#7M*  = X 3 2 0 0 X - (*D7M na forma de C#7M)
C#9*  = X 3 5 5 3 3 - (*D9 na forma de C#9)
D#*  = X X 0 2 3 2 - (*E na forma de D#)
D#5(9)*  = X 5 7 9 X X - (*E5(9) na forma de D#5(9))
D#6(9)*  = X 5 4 2 0 0 - (*E6(9) na forma de D#6(9))
F5*  = 0 2 2 X X X - (*F#5 na forma de F5)
Fm*  = 0 2 2 0 0 0 - (*F#m na forma de Fm)
Fm7*  = 0 2 2 0 3 0 - (*F#m7 na forma de Fm7)
Fm7(9)*  = X 7 5 7 7 X - (*F#m7(9) na forma de Fm7(9))
G#*  = 3 2 0 0 0 3 - (*A na forma de G#)
G#5*  = 3 5 5 X X X - (*A5 na forma de G#5)
G#6*  = 3 X 2 4 3 X - (*A6 na forma de G#6)
G#6/C*  = 7 5 5 7 5 X - (*A6/C# na forma de G#6/C)
