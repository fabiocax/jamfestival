Bruno Mars - The Lazy Song

[Refrão]

[Tab - Refrão]

    B          F#             E
E|---------------------------------------------------|
B|----7--x--7----11--x--11------9--x--9---9--x--9----|
G|----8--x--8----11--x--11------9--x--9---9--x--9----|
D|----9--x--9----11--x--11------9--x--9---9--x--9----|
A|-------------9--------------7----------------------|
E|--7------------------------------------------------|

    B           D#             E
E|----------------------------------------------------|
B|----7--x--7-----8--x--8--------9--x--9---9--x--9----|
G|----8--x--8-----8--x--8--------9--x--9---9--x--9----|
D|----9--x--9-----8--x--8--------9--x--9---9--x--9----|
A|--------------6--------------7----------------------|
E|--7-------------------------------------------------|

   C#m          D#m         E            E
E|----------------------------------------------------|
B|----5--x--5-----7--x--7-----9--x--9----9------------|
G|----6--x--6-----8--x--8-----9--x--9----9------------|
D|----6--x--6-----8--x--8-----9--x--9----9------------|
A|--4-----------6-----------7------------7------------|
E|----------------------------------------------------|

   B                F#          E
Today I don't feel like doing anything
B             F#        E
I just wanna lay in my bed
       B                F#
Don't feel like picking up my phone
    E
So leave a message at the tone
         B               D#       E
Cause today I swear I'm not doing anything

[Primeira Parte]

               B                    F#
Uh, I'm gonna kick my feet up and stare at the fan
 E
Turn the TV on, Throw my hand in my pants
 B              F#        E
Nobody's gonna tell me I can't, nah

         B                        F#
I'll be lying on the couch just chillin in my snuggie
  E
Click to MTV so they can teach me how to dougie
      B            F#           E
Cause in my castle I'm the freakin man

[Pré-Refrão]

        C#m
Oh Oh, yes I said it
D#m
    I said it
   E               F#
I said it cause I can

[Refrão]

   B                F#        E
Today I don't feel like doing anything
B             F#        E
I just wanna lay in my bed
       B                F#
Don't feel like picking up my phone
    E
So leave a message at the tone
         B               D#       E
Cause today I swear I'm not doing anything

Nothing at all
B         F#
  Ooh hoo    ooh hoo
 E
Hoo ooh ooh, Nothing at all
B         F#
  Ooh hoo    ooh hoo
 E
Hoo ooh ooh

[Segunda Parte]

         B                  F#
Tomorrow I wake up, do some P90X
        E
Meet a really nice girl have some really nice sexy
      B                F#              E
And she's gonna scream out, "this is great"
   B                            F#
I might mess around and get my college degree
   E
I bet my old man will be so proud of me
     B              F#           E
I'm sorry pops you just have to wait

[Pré-Refrão]

        C#m
Oh Oh, yes I said it
D#m
     I said it
   E               F#
I said it cause I can

[Refrão]

   B                F#          E
Today I don't feel like doing anything
B             F#        E
I just wanna lay in my bed
       B                F#
Don't feel like picking up my phone
    E
So leave a message at the tone
         B               D#       E
Cause today I swear I'm not doing anything

[Ponte]

[Tab - Ponte]

   C#m          F#           G#m7     
E|----------------------------------------------------|
B|----5--x--5-----2--x--2------4--x--4----------------|
G|----6--x--6-----3--x--3------4--x--4----------------|
D|----6--x--6-----4--x--4------4--x--4----------------|
A|--4-------------------------------------------------|
E|--------------2------------4------------------------|

     C#m                  F#
No I ain't gonna comb my hair
        G#m7
Cause I ain't going anywhere
 C#m            F#       G#m7
No no no no no no no no no
             C#m                F#
I'll just strut in my birthday suit
        G#m7
And let everything hang loose
 C#m                 F#                  G#m7
Yeah yeah yeah yeah yeah yeah yeah yeah yeah

[Refrão]

   B                F#        E
Today I don't feel like doing anything
B             F#        E
I just wanna lay in my bed
       B                F#
Don't feel like picking up my phone
    E
So leave a message at the tone
         B               D#       E
Cause today I swear I'm not doing anything

Nothing at all
B         F#
  Ooh hoo    ooh hoo
 E
Hoo ooh ooh, Nothing at all
B         F#
  Ooh hoo    ooh hoo
 E
Hoo ooh ooh

----------------- Acordes -----------------
B = 7 9 9 8 7 7
C#m = X 4 6 6 5 4
D# = X 6 8 8 8 6
D#m = X 6 8 8 7 6
E = X 7 9 9 9 7
F# = X 9 11 11 11 9
G#m7 = 4 6 4 4 4 4
