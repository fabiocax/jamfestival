import pygame
pygame.init()
class Jam():    
    def __init__(self, music):
        self.music=music
        pygame.init()
        pygame.mixer.init()
    def play(self):
        pygame.quit()
        pygame.init()
        pygame.mixer.init()
        pygame.mixer.music.load(self.music)
        #pygame.event.wait()
        pygame.mixer_music.play()
    def stop(self):    
        pygame.mixer_music.stop()
        
    def pause(self):
        if pygame.mixer_music.get_busy() == True:
            pygame.mixer_music.pause()
        else:
            pygame.mixer_music.unpause()
    def volume(self, volume):
        pygame.mixer_music.set_volume(volume)
    def get_info(self):
        pos=pygame.mixer_music.get_pos()
        return str(pos/1000)
    def is_busy(self):
        return pygame.mixer_music.get_busy()
