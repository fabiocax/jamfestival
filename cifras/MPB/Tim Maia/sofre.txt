Tim Maia - Sofre

(C F G F C G)

É engraçado
A gente ama, troca juras de amor, promete nunca se separar.
A gente deposita uma confiança tão grande na pessoa
amada, que jamais você poderia imaginar que a pessoa que
você quer tanto esteja lhe traindo de uma maneira tão fria e suja.
A dor que você sente é tanta, que às vezes você deseja até morrer.
E desta dor nasce um ódio, um rancor, uma espécie de
vingança, que eu agora vou cantar.

C
Não vou mais chorar, mas se eu chorar
           F
vai ser baixinho pra ninguém me ver
O quanto eu sofro, pois amei você.
G
E vou seguir meu caminho triste
     F
Como antes de te conhecer
         C
Porém marcado, humilhado, magoado.

     G
Pode cer.
C
Você foi mulher
Se isso é ser mulher
         F
Está enganada, pois não é não.
Isto foi pura podridão
G
Se valeu do sentimento puro
              F
e belo que eu tinha por você
          C
para fazer as suas crueldades e
                 G
 maldades sem perdão.
      C
Agora Sofre
Sofre
     F
Todo mal que cê me fez
Você bem cedo irá pagar!

G                 F
Disse a todo mundo que eu que era o mal
        C
E, no entanto foi você quem riu
          G
E quem me fez penar!
         C
Agora Sofre
Sofre
     F
Todo mal que cê me fez
Você bem cedo irá pagar!
G
Disse a todo mundo ...

----------------- Acordes -----------------
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
