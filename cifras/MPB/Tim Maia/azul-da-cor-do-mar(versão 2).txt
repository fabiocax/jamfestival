Tim Maia - Azul da Cor do Mar

A  Bm
Ah,   se o mundo inteiro
      C#m
Me pudesse ouvir
              Bm
Tenho muito pra contar
E7                 A7  E7
   Dizer que aprendi

A  Bm
E     na vida a gente
        C#m
Tem que entender
               Bm
Que um nasce pra sofrer
E7                   A7
   Enquanto o outro ri

( Bm  C#m  D7  E7 )

 A7  Bm
Mas     quem sofre

                 C#m
Sempre tem que procurar
            Bm
Pelo menos vir achar
E7               A7  E7
   Razão para viver

 A7   Bm
Ver       na vida algum motivo
  C#m
Pra sonhar
              Bm
Ter um sonho todo azul
E7                 A7
   Azul da cor do mar

( Bm  C#m  D7  E7 )

 A7  Bm
Mas      quem sofre
                 C#m
Sempre tem que procurar
            Bm
Pelo menos vir achar
E7               A7  E7
   Razão para viver

 A7  Bm
Ver     na vida algum motivo
  C#m
Pra sonhar
              Bm
Ter um sonho todo azul
E7                 A7
   Azul da cor do ma

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
