Tim Maia - Acenda o Farol

Dm    Bm
Pneu furou
       G         Bm                         Em
Acenda o farol, acenda o farol
Am       G       Am
Se alguém ligou
G             Bm      Dm                G
Acenda o farol, acenda o farol
Dm     G      Em
Se alguém ligou
Am     Bm      Em
 Minha senhora
Am     G                   Bm                     Em
Se alguém lhe amou, e foi-se embora

Am                Bm
Você pode se encontrar
Dm   G                    Em
Você deve se ajudar
Am       G
E viver tranquilamente

           Bm            Em
Sentindo    disposto
Dm    Bm
Pneu furou
        G         Bm                          Em
Acenda o farol, acenda o farol
Am       G       Am
Se alguém ligou
G             Bm      Dm                G
Acenda o farol, acenda o farol
Dm     G      Em
Se alguém ligou
Am     Bm      Em
 Minha senhora
Am     G                     Bm          Em
Se alguém lhe amou, e foi-se embora

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
