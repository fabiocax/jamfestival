Tim Maia - Flor de Liz

[Intro] C7M(9)  Fm6
        C7M(9)  Fm6
        C7M(9)  Fm6
        C7M(9)  Fm6

   C7M(9)                   Bm7(5-)     E7(13-)
Valei-me, Deus! É o fim do nosso amor
         Am7(9)      D7(13)
Perdoa, por favor
             Gm7         C7(9)
Eu sei que o erro aconteceu
             F#m7(5-)        B7(9-)
Mas não sei o        que fez
        Bb7M      A7(13-)
Tudo mudar de vez
              F#m7(5-)     B7(9-)
Onde foi que eu errei?
                Em7(9)     A7(13-)
Eu só sei que amei,  que amei
      D7(9)         G4(7/9) G7(9-)
Que amei,     que amei


   C7M(9)              Bm7(5-)    E7(13-)
Será talvez, que minha ilusão
             Am7(9)    D7(13)
Foi dar meu coração
          Gm7
Com toda força
          C7(9)     F#m7(5-)    B7(9-)
Pra essa moça me fazer feliz

          Bb7M         A7(13-)
E o destino não quis
          F#m7(5-)     B7(9-)
Me ver como raiz
         Em7(9)      A7(13-)
De uma flor de lis
                    Am6
E foi assim que eu vi
        Fm6      C7M(9)     E7(9+)
Nosso amor  na poeira,    poeira
 Am7(9)           E/G#      Gm7
Morto na beleza fria  de Maria

[Refrão]
     C7(9)         F7M
E o meu jardim da vida
      Bb7(9)     Em7(9)
Ressecou,    morreu
    A7(13-)         D7(9)
Do pé que brotou Maria
     G4(7/9)        G7(9-)    Gm7
Nem mar________garida     nasceu (3X)

     C7(9)         F7M
E o meu jardim da vida
      Bb7(9)     Em7(9)
Ressecou,    morreu
     A7(13-)         D7(9)
Do pé que brotou Maria
     G4(7/9)        G7(9-)      C6(9)
Nem mar________garida     nasceu

( C7M(9)  Fm6 )
( C7M(9)  Fm6 )

----------------- Acordes -----------------
A7(13-) = X 0 X 0 2 1
Am6 = 5 X 4 5 5 X
Am7(9) = X X 7 5 8 7
B7(9-) = X 2 1 2 1 X
Bb7(9) = X 1 0 1 1 X
Bb7M = X 1 3 2 3 1
Bm7(5-) = X 2 3 2 3 X
C6(9) = X 3 2 2 3 3
C7(9) = X 3 2 3 3 X
C7M(9) = X 3 2 4 3 X
D7(13) = X 5 X 5 7 7
D7(9) = X 5 4 5 5 X
E/G# = 4 X 2 4 5 X
E7(13-) = 0 X 0 1 1 0
E7(9+) = X 7 6 7 8 X
Em7(9) = X 7 5 7 7 X
F#m7(5-) = 2 X 2 2 1 X
F7M = 1 X 2 2 1 X
Fm6 = 1 X 0 1 1 X
G4(7/9) = 3 X 3 2 1 X
G7(9-) = 3 X 3 1 0 X
Gm7 = 3 X 3 3 3 X
