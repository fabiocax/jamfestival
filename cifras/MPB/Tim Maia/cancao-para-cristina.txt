Tim Maia - Canção Para Cristina

Intro: A7+ D7+/A A7+ D7+/A A7+ D7+/A C#7

    A7+
Cristina,
                      D7+
Às vezes eu te chamo Cris
                      Bm
Um sonho que eu mesmo fiz
Bm/A                   G#° C#7
Um tempo que vai começar
    A7+
Cristina,
                   D7+
Menina que me aconteceu
                   Bm7
Um anjo que me devolveu
      E        A7+  F#m7 Bm7
A vontade de amar
      E        A7+   A7+/G
A vontade de amar


D7+            D#°               A7+    A7+/G
Eu sei que é difícil se dar por amor novamente
D7+            D#°              F#°       Esus4   E    E7
Depois que se sente a dor pelo amor que se foi (uoh uoh yeah yeah)

    A7+
Cristina,
                      D7+
Escuta o coração que diz
                     Bm
A gente pode ser feliz
Bm/A                G#° C#7
A gente deve se ajudar
    A7+
Cristina,
                   D7+
Levarei por onde for
                      Bm7
A marca desse nosso amor
        E              A7+  F#m7 Bm7
Nas canções que eu cantar
        E              A7+   A7+/G
Nas canções que eu cantar

D7+            D#°               A7+    A7+/G
Eu sei que é difícil se dar por amor novamente
D7+            D#°              F#°       Esus4   E    E7
Depois que se sente a dor pelo amor que se foi (uoh uoh yeah yeah)

    A7+
Cristina,
                      D7+
Escuta o coração que diz
                     Bm
A gente pode ser feliz
Bm/A                G#° C#7
A gente deve se ajudar
    A7+
Cristina,
                   D7+
Levarei por onde for
                      Bm7
A marca desse nosso amor
        E              A7+  F#m7 Bm7
Nas canções que eu cantar
        E              A7+  F#m7 Bm7
Nas canções que eu cantar
        E              A7+  F#m7 Bm7 E A7+
Nas canções que eu cantar

----------------- Acordes -----------------
A7+ = X 0 2 1 2 0
Bm = X 2 4 4 3 2
Bm/A = X 0 4 4 3 X
Bm7 = X 2 4 2 3 2
C#7 = X 4 3 4 2 X
D#° = X X 1 2 1 2
D7+ = X X 0 2 2 2
D7+/A = 5 5 4 2 2 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
Esus4 = 0 2 2 2 0 0
F#m7 = 2 X 2 2 2 X
F#° = 2 X 1 2 1 X
G#° = 4 X 3 4 3 X
