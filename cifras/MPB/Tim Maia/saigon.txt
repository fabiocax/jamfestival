Tim Maia - Saigon

          D     D5+
Tantas palavras
         D6    D5+
Meias palavras
            F#m5-/7
Nosso apartamento
     B7         Em7
Um pedaço de Saigon
    G/A
Me disse adeus no espelho
         D7+ G/A
   com batom
             D    D5+
Vai minha estrela
     D6    D5+
Iluminando
           F#m5-/7
Toda essa cidade
        B7           Em7
Como um céu de luz neon
    G/A
Seu brilho silencia todo

  D7+ D7
  som
   Gm7        C7        F7+
Às vezes você anda por aí
                   G/A
Brinca de se entregar
                 D7+ D7
Sonha pra não dormir
  Gm7             C7
E quase sempre eu penso em
          F7+
   te deixar
                G/A
E é só você chegar
                   D7+ D5+
Pra eu esquecer de mim
D7    G7+
Anoiteceu
G#m            C#7
Olho    pro céu e vejo como
     C7
   é bom
B7                        Em7
Ver as estrelas na escuridão
  G7+           Em7 G/A
Espero você voltar
       D7+
Pra Saigon

----------------- Acordes -----------------
B7 = X 2 1 2 0 2
C#7 = X 4 3 4 2 X
C7 = X 3 2 3 1 X
D = X X 0 2 3 2
D5+ = X 5 4 3 3 X
D6 = X 5 4 2 0 X
D7 = X X 0 2 1 2
D7+ = X X 0 2 2 2
Em7 = 0 2 2 0 3 0
F#m5-/7 = 2 X 2 2 1 X
F7+ = 1 X 2 2 1 X
G#m = 4 6 6 4 4 4
G/A = 5 X 5 4 3 X
G7+ = 3 X 4 4 3 X
Gm7 = 3 X 3 3 3 X
