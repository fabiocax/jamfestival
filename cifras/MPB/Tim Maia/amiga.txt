Tim Maia - Amiga

[Intro] E  Bm7  E7  A7M  Am7
          D7/9  G7M  C7/9  A/B

E             Bm             E7                   A7M
Amiga, eu precisava de você nesta hora
  Am                           C/D               G7M
E foi tão bom eu te encontrar agora
        C7M                    F#m          A/B
Tenho tanta coisa para te dizer
E            Bm               E7                      A7M
Amiga, tua palavra é o meu caminho certo
   Am                    C/D                    G7M
Teu pensamento, o meu livro aberto
       C7M                           F#m        A/B
Não vá agora, eu preciso de você

  A7M                     A/B                G#m
Amiga, o meu passado você conhece
       C#9-/7                           F#m
E muitas vezes você se esquece
        A/B                                                          G#m  C#9-/7
Que agora no presente tenho tanta coisa pra contar

  F#m                         A/B                        G#m
Amiga, estou tão só que às vezes me perco
         C#9-/7                        F#m
Tua palavra será o meu acerto
   A/B                                               E   A/B
Preciso agora de você um pouco mais

  E              Bm                   E7                      A7M
Amiga, você se lembra como eu era antigamente
   Am                             C/D                         G7M
Um cara puro, sem maldades, mas esta gente
          C7M                                        F#m      A/B
Me fez assim tão só sem um direito sequer
 E                   Bm                  E7                   A7M
Amiga, virei escravo dos sucessos de hoje em dia
   Am                            C/D                   G7M
Mas ninguém sabe muitas vezes eu fingia
               C7M                                             F#m      A/B
Pois quem eu gosto há muito tempo me deixou

 A7M                          A/B                G#m
Amiga, ela se foi e me deixou marcado
        C#9-/7                                        F#m
Nunca pensei que amar assim fosse errado
       A/B                                            G#m   C#9-/7
Agora me perdoe, que vontade de chorar
 F#m                       A/B                      G#m
Amiga, dê tuas mãos e me mostre a hora
        C#9-/7                            F#m
De ver o mundo melhor que agora
       A/B                                         E    A/B
Para sentir que ainda posso ser feliz

----------------- Acordes -----------------
A/B = X 2 2 2 2 X
A7M = X 0 2 1 2 0
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
C/D = X X 0 0 1 0
C7/9 = X 3 2 3 3 X
C7M = X 3 2 0 0 X
D7/9 = X 5 4 5 5 X
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
G7M = 3 X 4 4 3 X
