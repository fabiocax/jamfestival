Tim Maia - Ela Partiu

[Intro] Dm  Am

       Dm                    Dm
E|-x9/10-x-10-10x10-10-10-x-x9/10-x-10-10x10-10-10---|
B|-x9/10-x-10-10x10-10-10-x-x9/10-x-10-10x10-10-10---|
G|-x9/10-x-10-10x10-10-10-x-x9/10-x-10-10x10-10-10---|
D|---------------------------------------------------|
A|---------------------------------------------------|
E|---------------------------------------------------|

       Am             Am
E|-x4/5-x-5-5-x-5-5-5-x-x4/5-x-5-5-x-5-5-5-----------|
B|-x4/5-x-5-5-x-5-5-5-x-x4/5-x-5-5-x-5-5-5-----------|
G|-x4/5-x-5-5-x-5-5-5-x-x4/5-x-5-5-x-5-5-5-----------|
D|---------------------------------------------------|
A|---------------------------------------------------|
E|---------------------------------------------------|

Ela partiu
                       Am
Partiu e nunca mais voltou

       Dm                        Am
Ela sumiu, sumiu e nunca mais voltou

                      C
Se souberem onde ela está
   E           F           F#°
Digam-me e vou lá buscá-la
  C            Am
Pelo menos telefone em seu nome
         F          G
Me dê uma dica, uma pista, insista

Ei yeah, e nunca mais voltou
(Não voltou não)

        Dm
Ela partiu
                       Am
Partiu e nunca mais voltou
       Dm                        Am
Ela sumiu, sumiu e nunca mais voltou

       Dm                        Am
Ela sumiu, sumiu e nunca mais voltou
        Dm
Ela partiu
                       Am
Partiu e nunca mais voltou

                      C
Se eu soubesse onde ela foi
   E           F           F#°
Iria atrás, mas não sei mais nem direção
C                       Am
Várias noites que eu não durmo um segundo
         F                   G
Estou cansado, magoado, exausto

                      C
Se souberem onde ela está
   E           F           F#°
Digam-me e vou lá buscá-la
  C            Am
Pelo menos telefone em seu nome
         F          G
Me dê uma dica, uma pista, insista

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
F#° = 2 X 1 2 1 X
G = 3 2 0 0 0 3
