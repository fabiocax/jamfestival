Tim Maia - Dance Enquanto É Tempo

(intro) D G

D                                           G
Todos vão dançando com animação, todos dando a mão
Bm           A               G
Vê se deixa esta tristeza, bicho
E7                                 F#7
Pegue a dama e vem dançar
Bm     A                G
Até eu que estava nessa, bicho
E7                          A7
Decidi, vou me soltar
D
Todos vão dançando com animação
                                                           G
Todos num só ritmo, todos dando a mão
Bm          A              G
Sai da fossa, não tolero encrenca
E7                              F#7
O que eu quero é sambar
Bm         A               G
Já mandei parar com isso, cara

E7                            A7
Entra no samba, vem sambar

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
F#7 = 2 4 2 3 2 2
G = 3 2 0 0 0 3
