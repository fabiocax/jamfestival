Tim Maia - Razão de Sambar

Em
É terça-feira, é quarta-feira, é quinta-feira
                      D7M
todo mundo quer sambar
Em
Esquecem tudo, esquecem a vida, esquecem a morte
                         D7M
esquecem o que devem fazer
Em
Parecem loucos, mas aos poucos você vai chegando à mesma
         D7M
conclusão
Em
Que dessa vida não se leva, só se deixa
                       D7M
e por isso eu dou razão
Em
Para os que pulam, os que dançam, os que sambam
                        D7M
a noite inteira sem parar
Em
Por isso mesmo vou pegar meu violão

                           D7M
e vou pro samba, vou sambar!
Em              D7M
Vou sambar (várias vezes)

( Em  D7M )

----------------- Acordes -----------------
D7M = X X 0 2 2 2
Em = 0 2 2 0 0 0
