Tim Maia - Chocolate

A7+        A#º      Bm7
Chocolate, chocolate, chocolate
G#m7/5- C#7  F#m7    B7              F#m7    B7
Eu só quero chocolate....Só quero chocolate
       Bm7   (Cm7)     C#m7     F#7
Não adianta vir com guaraná pra mim
      Bm7            D/E       A7+
É chocolate o que eu quero beber

A7+        A#º      Bm7
Chocolate, chocolate, chocolate
G#m7/5- C#7  F#m7    B7              F#m7    B7
Eu só quero chocolate....Só quero chocolate
       Bm7   (Cm7)     C#m7     F#7
Não adianta vir com guaraná pra mim
      Bm7            D/E       A7+
É chocolate o que eu quero beber

          A#º             Bm7
Não quero chá, não quero café
               G#m7/5-   C#7         F#m7   B7
Não quero coca-cola, me liguei no chocolate

              F#m7   B7
Só quero chocolate
       Bm7   (Cm7)     C#m7     F#7
Não adianta vir com guaraná pra mim
      Bm7             D/E      A7+
É chocolate o que eu quero beber

A7+        A#º      Bm7
Chocolate, chocolate

----------------- Acordes -----------------
A#º = X 1 2 0 2 0
A7+ = X 0 2 1 2 0
B7 = X 2 1 2 0 2
Bm7 = X 2 4 2 3 2
C#7 = X 4 3 4 2 X
C#m7 = X 4 6 4 5 4
Cm7 = X 3 5 3 4 3
D/E = X X 2 2 3 2
F#7 = 2 4 2 3 2 2
F#m7 = 2 X 2 2 2 X
G#m7/5- = 4 X 4 4 3 X
