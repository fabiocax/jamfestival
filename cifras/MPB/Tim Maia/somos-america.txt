Tim Maia - Somos América

Am7 G7 F7M
Vim avisar
      Am7          G7         F7M
Desse jeito não dá mais pra ficar
                Am7 G7   F7M
Ninguém me convidou pra votar
    Am7         G7       F7M
Decidir ou pelo menos opinar
      C7M  Dm7       C7M   F/G G#°
Somos América, somos América

Am7 G7 F7M
De..va..gar
      Am7      G7            F7M
Não comece remexer sem perguntar
                 Am7 G7   F7M
Ninguém lhe convidou pra votar
     Am7        G7       F7M
Decidir ou pelo menos opinar
       C7M  Dm7      C7M   Dm
Somos América, somos América


                    Em7
Armas e mísseis complicam my friend
Dm7            C7M
Não vão solucionar
Dm7             Em7
Somos amigos, vizinhos my friend
   Dm7                 F/G G#°
So listen my friend so what

Am7             G7        F7M
Vamos ver quem chega no final
     Am7   G7    F7M
Cara limpa ou doidão
                Am7   G7    F7M
Ninguém se convidou   pra mudar
     Am7         G7       F7M
Resolver ou pelo menos ajudar

       C7M  Dm7       C7M   Dm
Somos América, somos América

                 Em7
Seja no norte do centro ou do sul
  Dm7                C7M
Esteja em qualquer lugar
Dm7             Em7
Somos amigos, vizinhos my friend
       Dm7                F/G G#°
Está ouvindo my friend? Vê là!

Am7            G7         F7M
Vamos ver quem chega no final
     Am7   G7    F7M
Cara limpa ou doidão
                Am7   G7    F7M
Ninguém se convidou   pra mudar
     Am7         G7       F7M
Resolver ou pelo menos ajudar

       C7M  Dm7       C7M   Dm
Somos América, somos América

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
C7M = X 3 2 0 0 X
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Em7 = 0 2 2 0 3 0
F/G = 3 X 3 2 1 X
F7M = 1 X 2 2 1 X
G#° = 4 X 3 4 3 X
G7 = 3 5 3 4 3 3
