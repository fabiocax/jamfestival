Tim Maia - Não Quero Dinheiro

(intro 2x) A

(parte 1)

A                  F#m
  Vou pedir pra você voltar
A                  F#m
  Vou pedir pra você ficar
      E4                A  E4
Eu te amo, eu te quero bem
A                  F#m
  Vou pedir pra você gostar
A                  F#m
  Vou pedir pra você me amar
      Dm                       E4  A7(4)  A7
Eu te amo, eu te adoro, meu amor

(refrão)
            D                 E/D
A semana inteira, fiquei esperando
               C#m                  F#m
Pra te ver sorrindo, pra te ver cantando

               Bm                     E4
Quando a gente ama, não pensa em dinheiro
             A                         A7    A7(4)
Só se quer amar, se quer amar, se quer amar
            D                   E/D
De jeito maneira, não quero dinheiro
               C#m                  F#m
Quero amor sincero, isto é o que espero
                  Bm                  E4                 A   E4
Grito ao mundo inteiro, não quero dinheiro eu só quero amar

(parte 2)

A                 F#m
  Te espero para ver se você vem
A                     F#m                          E4
  Não te troco nesta vida por ninguém porque eu te amo
             A   E4
Eu te quero bem
A                  F#m
  Acontece que na vida a gente tem
A                     F#m
  Ser feliz por ser amado por alguem
             Dm                      E4  A7(4)  A7
Porque eu te amo, eu te adoro, meu amor

(refrão)

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
A7(4) = X 0 2 0 3 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E/D = X 5 X 4 5 4
E4 = 0 2 2 2 0 0
F#m = 2 4 4 2 2 2
