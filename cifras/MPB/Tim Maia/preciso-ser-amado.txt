Tim Maia - Preciso Ser Amado

[Riff]

          Bm
E|------------|
B|------------|
G|------------|
D|------------|
A|----0--2----|
E|0-2---2-----|

Bm    E         A
Eu preciso de carinho
Bm    E        A
Eu preciso de calor
Bm    E          A
Venha abrir o meu caminho

( C#m  Cm  Bm )

Bm    E          A
Venha ser o meu amor

D     D#º       A
Eu preciso ser amado
D     D#º      A
Ser amado pra valer
F#m7
Ando até preocupado
      B7           E7
Sem vontade de viver
Bm    E            A
Venha ser a minha amada

( C#m  Cm  Bm )

Bm     E         A
Me acompanhe por favor
Bm     E         A
Minha linda namorada

( C#m  Cm  Bm )

Bm     E         A
Pois é seu o meu amor
D      D#º        A
Eu preciso ser amado
D      D#º        A
Ser amado pra valer
F#m7
Ando até preocupado
       B7         E7
Sem vontade de viver
Bm    E            A
Venha ser a minha amada

( C#m  Cm  Bm )

Bm     E         A
Me acompanhe por favor
Bm     E         A
Minha linda namorada

( C#m  Cm  Bm )

Bm     E         A
Pois é seu o meu amor
C#m Cm Bm
Bm     E         A
Pois é seu o meu amor
C#m Cm Bm
Bm     E       A
Todo seu o meu amor

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D#º = X X 1 2 1 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m7 = 2 X 2 2 2 X
