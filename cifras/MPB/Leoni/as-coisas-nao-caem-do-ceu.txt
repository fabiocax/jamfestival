Leoni - As Coisas Não Caem do Céu.

Intro: C

   C                    Dm
Será que a gente se esquece
    Fm               C
Ou nunca chegou a saber
Que esse mundo é nosso
                     Dm
Quando a gente toma posse
Arregaça as mangas
   Fm                  C
E faz o que tem que fazer?

                       Dm
Por que todo mundo reclama
        Fm                C
Do que lê de manhã no jornal?
Mesmo sem mexer um dedo
              Dm
Se acha no direito de se achar acima
        Fm                 C
Muito acima de tudo que é mal


 Am              C/G
Não imagina ter nada com isso
    F                         C
Alguém, mas não nós, tem que resolver
   Am                 C/G
A gente já fez nossa parte
               F
Xingando essa corja
            C
Em frente à TV

      C                         Dm
Por que é que eu me encho de orgulho
 Fm                     C
Só porque um dia eu postei
                                           Dm
Um link pra uma causa nobre pra ajudar os pobres, qualquer coisa assim
            Fm                  C
Que eu não li, mas eu compartilhei
                            Dm
Por que é que a gente se espanta
          Fm                 C
Com qualquer preconceito dos outros
                            Dm
Mas no nosso caso é sempre diferente a gente só quer defender
      Fm                    C
A cultura, a moral e o bom gosto

   Am               C/G
Será que o mundo seria melhor
      F                  C
Se algum de nós pudesse decidir
    Am                C/G
O que todos devem sonhar todo dia
    F                 C
E qual o caminho pra ser feliz?

   C                    Dm
Será que eu um dia acredito
    Fm                 C
De tanto que escuto dizer
Que ser gentil e generoso
               Dm
Importa muito pouco
Que eu não sou ninguém
        Fm                 C
Sem dinheiro, beleza ou poder?

   Am                    C/G
Será que ao invés do prazer de viver
       F                      C
De sentir, de provar, sento pra assistir
   Am                  C/G
A vida emprestada das celebridades
   F                   C
Sozinho na sala antes de dormir?

      F                        C
Por que é que a gente ainda espera?
    F                  C
As coisas não caem do céu
    E                 C         G
Esquece a esperança e entra na dança
        F          G       C
Que as coisas não caem do céu
    E                 C         G
Esquece a esperança e entra na dança
        F          G       C
Que as coisas não caem do céu
    F          G       C
As coisas não caem do céu
Eu sei e você sabe
    F          G       C    C
As coisas não caem do céu

      C                Dm
Por que todo mundo reclama
        Fm                C
Do que lê de manhã no jornal?

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C/G = 3 3 2 X 1 X
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
