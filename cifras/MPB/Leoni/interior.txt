Leoni - Interior

F
Quase ninguém no bar
C
E é melhor assim...
F
Tanta cidade igual
C
Que passou por mim
Am
Sento pra te ligar
Dm
E eu me sinto em paz
F              G
Se faz frio aqui
C
Tanto Faz

(refrão)
C
O que é que eu vou contar
Dm
Ando sem pensar pra onde eu vou

F                           G
Tanto céu e chão pra misturar
C
Pouco pra lembrar

O que é que eu vou contar
Dm
Se todo lugar é só mais um
F                         G
Tanto interior pra misturar
C
Pouco pra lembrar

F
Faz uma hora ou mais
C
Que eu cheguei aqui
F
É tarde e ainda não sei
C
Onde eu vou dormir
Am
Quase ninguém no bar
Dm
Só silêncio e paz
F                G
Onde isso vai dar?
C
Tanto faz

(refrão)
C
O que é que eu vou contar
Dm
Ando sem pensar pra onde eu vou
F                         G
Tanto céu e chão pra misturar
C
Pouco pra lembrar

O que é que eu vou contar
Dm
Se todo lugar é só mais um
F                        G
Tanto interior pra misturar
C
Pouco pra lembrar

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
