Leoni - Do Teu Lado

Intro:  E C#m7 A7M/9 Am7
        E C#m7 A7M/9 Am7 B(add9)

      E               E(add9)
Te escrevo esta canção
C#4/7              C#m7
Pra te fazer companhia
A(add9)
Pra segurar tua mão
Am9
Não te deixar sozinha

    E              E(add9)
Canção feita de pele
      C#4/7           C#m7
Pra usar por baixo da roupa
A(add9)
Canção pra te deixar
         Am9       E
Um gosto doce na boca


E(add9) C#4/7 C#m7 A(add9) Am9

      E               E(add9)
Te escrevo esta canção
C#4/7                 C#m7
Porque nem sempre ando perto
A(add9)
E essa canção me ajuda
  Am9
A atravessar o deserto

   E                  E(add9)
Canção de fim de tarde
       C#4/7              C#m7
Pra se infiltrar nos seus poros
 A(add9)
Pra acordar com você
       Am9
E te olhar no fundo dos olhos

   F#m4
Canção pra andar do teu lado
   G#m
Em toda e qualquer cidade
    A7M/9                B7
Pra te cobrir de sorrisos
                      E  E(add9)  C#4/7
Quando eu chorar de saudade

C#m7 A(add9) Am9

      E               E(add9)
Te escrevo esta canção
C#4/7              C#m7
Pra te fazer companhia
A(add9)
Pra segurar tua mão
Am9
Não te deixar sozinha

    E              E(add9)
Canção feita de pele
      C#4/7           C#m7
Pra usar por baixo da roupa
A(add9)
Canção pra te deixar
         Am9       E
Um gosto doce na boca

   F#m4
Canção pra andar do teu lado
   G#m
Em toda e qualquer cidade
    A7M/9                B7
Pra te cobrir de sorrisos
                      E  E(add9)  C#4/7
Quando eu chorar de saudade

C#m7 A(add9) Am9
E E(add9) C#4/7 C#m7 A(add9) Am9

----------------- Acordes -----------------
A(add9) = X 0 2 2 0 0
A7M/9 = X 0 2 1 0 0
Am7 = X 0 2 0 1 0
Am9 = X 0 2 4 1 0
B(add9) = X 2 4 4 2 2
B7 = X 2 1 2 0 2
C#4/7 = X 4 6 4 7 4
C#m7 = X 4 6 4 5 4
E = 0 2 2 1 0 0
E(add9) = 0 2 4 1 0 0
F#m4 = 2 2 4 2 2 2
G#m = 4 6 6 4 4 4
