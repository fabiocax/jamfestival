Ney Matogrosso - Por Que A Gente É Assim ?

Cazuza-Frejat

Tom :A

A                 C  D
( Mais uma dose?
A                            C  D
 É claro que eu estou a fim
   A                    C
 A noite nunca tem fim
 D   A                         C  D
 Por que que a gente é assim? )......... Refrão

 A                 C  D
 Agora fica comigo
   A                           C  D
 E vê se não desgruda de mim
 A                        C  D
 Vê se ao menos me engole
         A                 C  D
 Mas não me mastiga assim


                G
 Canibais de nós mesmos
D                  A/C#   Bm     A
 Antes que a terra nos    coma
G                D
 Cem gramas, sem dramas
               A
 Por que que a gente é assim?

( Refrão )

 A   G   A   F#m   A   F#m   G   A   Ab   F#m

               C#m
 Você tem exatamente
          Bm                       D7
 Três mil horas pra parar de me beijar
 A                      C#m
 Hum, meu bem, você tem tudo
        B7
 Pra me conquistar
F#m            C#m
 Você tem exatamente
      Bm                        D7
 Um segundo pra aprender a me amar
A                  C#m
 Você tem a vida inteira
        D
 Pra me devorar
             A
 Pra me devorar!

( Refrão )

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
Ab = 4 3 1 1 1 4
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
D7 = X X 0 2 1 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
