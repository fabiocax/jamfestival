Ney Matogrosso - Seu Valdir

Introdução: C7+ Cm7 G F7 E7 Am D7 G
 G              C
Seu Valdir o senhor
   D7          G  E7
Magoou meu coração
             Am7         D7
Fazer isto comigo seu Valdir
      G           B7
Isso não se faz não
                    Em7
Eu trago dentro do peito
                 A7
Um coração apaixonado
               D7
Batendo pelo senhor

O senhor tem que dar um jeito
                 D7+
Senão eu vou cometer um suicídio
      D7                       G
Nos dentes de um ofídio vou morrer

              C7+
Eu falo tudo isso
                  Cm7
Pois sei que o senhor
                G7+ B7
Está gamado em mim
                       Em7
Eu quero ser o seu brinquedo favorito
      A7                    D7
Seu apito, sua camisa de cetim
                      C7+
Mas o senhor precisa ser mais decidido
         D7                         G7+
E demonstrar que corresponde o meu amor

Podem crer
   E7                Am7
Senão eu vou chorar muito seu Valdir
   D7                   G7+
Pensando que vou lhe perder

Seu Valdir meu amor

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
C7+ = X 3 2 0 0 X
Cm7 = X 3 5 3 4 3
D7 = X X 0 2 1 2
D7+ = X X 0 2 2 2
E7 = 0 2 2 1 3 0
Em7 = 0 2 2 0 3 0
F7 = 1 3 1 2 1 1
G = 3 2 0 0 0 3
G7+ = 3 X 4 4 3 X
