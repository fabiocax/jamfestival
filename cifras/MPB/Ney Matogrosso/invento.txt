Ney Matogrosso - Invento

Am7
Vento
               Am7/G
Quem vem das esquinas
Am7/F#
E ruas vazias
Am7/F
De um céu interior
Am7
Alma
                     Am7/G
De flores quebradas
            Am7/F#
Cortinas rasgadas
     Am7/F
Papéis sem valor

Dm7
Vento
                Dm7/C
Que varre os segundos

                      G/B
Prum canto do mundo
                     G/Bb
Que fundo nao tem

Dm7
Leva
                 Dm7/C
Um beijo perdido
                   G/B
Um verso bandido

Um sonho refém
Am7                           Em7
Que eu não possa ler, nem desejar
Am7                            Em7
Que eu não possa imaginar
F7+
Oh, vento que vem
F#º
Pode passar
G            E7
Inventa fora de mim
Am7
Outro lugar

Am7
Vento
                Am7/G
Que dança nas praças
                Am7/F#
Que quebra as vidraças
Am7/F
Do interior
Am7
Alma
                   Am7/G
Que arrasta correntes
                 Am7/F#
Que força os batentes
                 Am7/F
Que zomba da dor
Dm7
Vento
           Dm7/C
Que joga na mala
             G/B
Os móveis da sala
             G/Bb
E a sala também

Dm7
Leva...

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Am7/G = 3 0 2 0 1 0
Dm7 = X 5 7 5 6 5
Dm7/C = X 3 0 2 1 1
E7 = 0 2 2 1 3 0
Em7 = 0 2 2 0 3 0
F7+ = 1 X 2 2 1 X
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G/Bb = X 1 0 0 3 3
