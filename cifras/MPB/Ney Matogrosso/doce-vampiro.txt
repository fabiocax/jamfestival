Ney Matogrosso - Doce Vampiro

Introdução: A7/C# G7+/B A7/C# G7+/B C5- D5- A7/C#

            D/F#
Venha me beijar
           A7     Em7/9
Meu doce vampiro, ououuu
           A7
Na luz do luar
 D7            G
Ahaaa, venha sugar o calor
     G7           C         G
De dentro do meu sangue vermelho
 G7           C          A7
Tão vivo tão eterno veneno
              D
Que mata sua sede
             A7
Que me bebe quente
G         D
Como um licor
             A7  A6
Brindando a morte

          Em7/9
E fazendo amor
             A7   Em7/9
Meu doce vampiro, ououuu
            A7
Na luz do luar
 D7            G
Ahaaa, me acostumei com você
 G7          C          G
Sempre reclamando da vida
      G7         C            A7
Me ferindo me curando a ferida
                  D  E/D
Mas nada disso importa
             A/C#       G     D
Vou abrir a porta pra você entrar
              E7   A7     (Em7/9 A7)
Beijar minha boca até me matar

----------------- Acordes -----------------
A/C# = X 4 X 2 5 5
A6 = 5 X 4 6 5 X
A7 = X 0 2 0 2 0
A7/C# = X 4 5 2 5 X
C = X 3 2 0 1 0
C5- = X 3 4 5 5 3
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D5- = X 5 6 7 7 5
D7 = X X 0 2 1 2
E/D = X 5 X 4 5 4
E7 = 0 2 2 1 3 0
Em7/9 = X 7 5 7 7 X
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
G7+/B = 7 X 5 7 7 X
