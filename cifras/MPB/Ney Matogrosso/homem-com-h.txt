Ney Matogrosso - Homem com H

Intro: Em A7 D G C#m5-/7 F#7
Bm Em A7 D C#7 F#7 Bm

                    Bm
Nunca vi rastro de cobra
                  Em
Nem couro de lobisomem
Se correr o bicho pega
                  Bm
Se ficar o bicho come
                 Bm/A
Porque eu sou é home
                 Abº
Porque eu sou é home
                 F#7
Menino eu sou é home
                 Bm
Menino eu sou é home
       A7                D F#7
Quando eu estava pra nascer
                      Bm  Em
De vez em quando eu ouvia

                Bm Em
Eu ouvia mãe dizer
                       Bm  Em
Ai meu Deus como eu queria
                       Bm  Em
Que essa cabra fosse home
                  Bm
Cabra macho pra danar
Em     A7         D
Ah! Mamãe aqui estou eu
   G            C#m5-/7
Mamãe aqui estou eu
      F#7     Bm
Sou homem com H
        F#7
E como sou
Estribilho
       A7        D F#7
Eu sou homem com H
                  Bm  Em
E com H sou muito home
                 Bm  Em
Se você quer duvidar
                    Bm  Em
Olhe bem pelo meu nome
                Bm    Em
Já tô quase namorando
                Bm
Namorando pra casar
Em     A7             D
Ah! Maria diz que eu sou
  G               C#m5-/7
Maria diz que eu sou
      F#7     B
Sou homem com H
E como sou

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Abº = 4 X 3 4 3 X
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
Bm/A = X 0 4 4 3 X
C#7 = X 4 3 4 2 X
C#m5-/7 = X 4 5 4 5 X
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F#7 = 2 4 2 3 2 2
G = 3 2 0 0 0 3
