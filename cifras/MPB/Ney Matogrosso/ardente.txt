Ney Matogrosso - Ardente

- F#
Introdução: G#m5-/7 C#7/9- F#m7+ F#m7

G#m5-/7 C#7/9- F#m7+ F#m7 Bm7
Sou ardente não vou negar
      E7     A7+
Gosto de me secar ao sol
G/A A7     D7+    G#5-/7
Aprecio qualquer paixão
G#m5-/7  C#7/9- F#m7+ F#m7 G#m5-/7
Qualquer coisa como brincar
        C#7/9-      F#m7+ F#m7 Bm7
E o meu corpo é que nem farol
    E7         A7+
Indicando que pode entrar
G/A  A7    D7+    G#5-/7
Apontando uma direção
G#m5-/7  C#7/9-      F#m7+
Pra quem quer se queimar
G#m7       C#7/9-    F#7+         F#/E
Procuro alguém tão singelo como eu

              F#7        B7+        Fm7
Que não se esconda das coisas naturais
           Bb7     Ebm7
Não tenha medo do fogo
                   G#7
Nem do vento que carrega
      G#m7      C#7/9                F#7+
Pois afinal os elementos são todos iguais
         Gº     G#m7       C#7/9 E/F#
Não é preciso fugir você vai ver
       F#7       B7+            Fm7
A tempestade não vai lhe machucar
         Bb7         Ebm7
Se o coração é quem manda
                  G#m7      C#7/9        F#7+
A natureza quer apenas lhe fazer aproveitar
      Gº         G#m5-/7 C#7/9  F#m7+ F#m7
E mostrando que eu sou ardente como você
Bm7    E7     A7
Sou demente, mas quem não é?
G/A    A7   D7+      G#5-/7
Eu aprendo qualquer lição
G#m5-/7 C#7/9- F#m7+ F#m7 G#m5-/7
Na versão que você quiser
    C#7/9-       F#m7+  F#m7
Sonhador desses sonhos meus
Bm7  E7      A7+
Amoroso e fatal demais
G/A      A7    D7+      G#m5-/7 G#m5-/7
Louco e solto graças a Deus
       C#7/9-      F#7+
Quero arder sempre mais

Solo da 2ª parte

G#m5-/7 C#7/9-      F#m7
Quero arder sempre mais (5x)

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
B7+ = X 2 4 3 4 2
Bb7 = X 1 3 1 3 1
Bm7 = X 2 4 2 3 2
C#7/9 = X 4 3 4 4 X
C#7/9- = X 4 3 4 3 X
D7+ = X X 0 2 2 2
E/F# = X X 4 4 5 4
E7 = 0 2 2 1 3 0
Ebm7 = X X 1 3 2 2
F# = 2 4 4 3 2 2
F#/E = X X 2 3 2 2
F#7 = 2 4 2 3 2 2
F#7+ = 2 X 3 3 2 X
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
F#m7+ = X X 4 6 6 5
Fm7 = 1 X 1 1 1 X
G#5-/7 = 4 X 4 5 3 X
G#7 = 4 6 4 5 4 4
G#m5-/7 = 4 X 4 4 3 X
G#m7 = 4 X 4 4 4 X
G/A = 5 X 5 4 3 X
Gº = 3 X 2 3 2 X
