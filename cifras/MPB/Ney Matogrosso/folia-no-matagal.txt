Ney Matogrosso - Folia no Matagal

Introdução: D7+ Dm7 C#m7 Cº Bm7 E7 A7+

   A7+              Bm7
O mar passa saborosamente
    E7       A7+
A língua na areia
                    E          C#7
Que bem debochada, cínica que é
  F#m7       B7           Bm7     E7
Permite deleitada esses abusos do mar
    Bm7          E7           A7+
Por trás de uma folha de palmeira
   Bm7     E7
A lua poderosa
   A7+         A7
Mulher muito fogosa
    D7+ Dm7  C#m7 Cº  Bm7         E7      A7+
Vem nua, vem nua sacudindo e brilhando inteira
    D7+ Dm7  C#m7 Cº  Bm7         E7      A7+
Vem nua, vem nua sacudindo e brilhando inteira
    Bm7         E7        A7+
Palmeiras se abraçam fortemente

                A7            D7+
Suspiram, dão gemidos, soltam ais
        Dm7      D#º       A7+
Um coqueirinho pergunta docemente
F#7         B7                  E7
A outro coqueiro que o olha sonhador
Bm7        E7        A7+
Você me amará eternamente?
   Bm7      E7          A7
Ou amanhã tudo já se acabou?
D7+      Dm7 C#m7      F#7
Nada acabará grita o matagal
 Bm7   E7      A7
Nada ainda começou
D7+      Dm7 C#m7      F#7
Nada acabará grita o matagal
Bm7   E7      A7
Nada ainda começou
    Bm7         E7
São dois coqueirinhos
 A7+
Ainda em botão
 C#7                   F#m7
Nem conhecem ainda o que é uma paixão
  D7+          Dm7 A7+          F#7
E lá em cima a lua já virada em mel
 Bm7       E7         A7      F#7
Olha a natureza se amando ao léu
   D7+       Dm7
E louca de desejo
    A7+        F#7
Fulgura num lampejo
   Bm7         E7      A7+
E rubra se entrega ao céu
   D7+       Dm7
E louca de desejo
   A7+        F#7
Fulgura num lampejo
   Bm7         E7      A7+ D7+ Dm7 C#m7 Cº Bm7 E7 A7+
E rubra se entrega ao céu

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
B7 = X 2 1 2 0 2
Bm7 = X 2 4 2 3 2
C#7 = X 4 3 4 2 X
C#m7 = X 4 6 4 5 4
Cº = X 3 4 2 4 2
D#º = X X 1 2 1 2
D7+ = X X 0 2 2 2
Dm7 = X 5 7 5 6 5
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#7 = 2 4 2 3 2 2
F#m7 = 2 X 2 2 2 X
