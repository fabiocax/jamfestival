Ivan Lins - Dona Palmeira

[Intro] Bm7/9

 Bm7/9
Dona palmeira, deixa eu descansar

Dona palmeira, deixa eu descansar
        Em7/9  A7/13        Dm7/9
Dona palmeira, dê-me seus cabelos
 G7/13        C#m7/9    F#7/5+      Bm7/9
Pr'eu fazer esteira, pr'eu poder sonhar
 Bm7/9
Dona palmeira de santa valia

Dona palmeira de santa valia
        Em7/9   A7/13           Dm7/9
Dona palmeira, deixa eu me esconder
    G7/13       C#m7/9    F#7/5+   Bm7/9
Dos olhos e das balas  da cavalaria
Em7/9   A7/13       D7+/9   G7/13
Ah!           Cavalaria
C#m7/9   F#7/5+       Bm7/9
Oh!             Cavalaria
Em7/9   A7/13   Dm7/9   G7/13   C#m7/9   F#7/5+   B7(9/4)   B7/9-
 Ah!

----------------- Acordes -----------------
A7/13 = X 0 X 0 2 2
B7(9/4) = X 2 2 2 2 2
B7/9- = X 2 1 2 1 X
Bm7/9 = X 2 0 2 2 X
C#m7/9 = X 4 2 4 4 X
D7+/9 = X 5 4 6 5 X
Dm7/9 = X 5 3 5 5 X
Em7/9 = X 7 5 7 7 X
F#7/5+ = 2 X 2 3 3 2
G7/13 = 3 X 3 4 5 X
