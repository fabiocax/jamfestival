Ivan Lins - Dona Benta

B7M          Em6/B
Que pessoa é essa
B7M            Em6/B
Que fazendo as contas
B7M             Em6/B 
Que contando os pontos
       B7M      D#m7(9) D7(9/#11)
Faz um lindo bordado

E7M          C#m6/E
Que pessoa é essa
E7M          C#m6/E
Que tem a liberdade
E7M          C#m6/E D#m7(9)  G#7(b5)
De abrir a janela esbarrando no verde

C#m7(9)     F#7(13)
Que magia é essa?
C#m7(9)       F#7(13)     G#m7(9)
Que milagre é esse?   Hunhummmm
C#m7(9)         F#7(13)
Mas que magia é essa?
C#m7(9)        F#7(13)     G#m7(9)
Que milagre é esse?   Hunhummmm

B7M          Em6/B
Que pessoa é essa
B7M            Em6/B
Que não se desaponta
B7M             Em6/B
Que nunca se espanta
    B7M              D#m7(9) D7(9/#11)
Que vive e que       sonha
E7M          C#m6/E
Que pessoa é essa
E7M          C#m6/E
Que sem nenhum esforço
E7M          C#m6/E
No jantar ou almoço
D#m7(9)  G#7(b5)
Tem a mesa farta

C#m7(9)         F#7(13)
Mas que magia é essa?
C#m7(9)        F#7(13)     G#m7(9)
Que milagre é esse?   Hunhummmm
C#m7(9)         F#7(13)
Mas que magia é essa?
C#m7(9)        F#7(13)     G#m7(9)
Que milagre é esse?   Hunhummmm

----------------- Acordes -----------------
B7M = X 2 4 3 4 2
C#m6/E = X X 2 3 2 4
C#m7(9) = X 4 2 4 4 X
D#m7(9) = X 6 4 6 6 X
D7(9/#11) = X X 0 1 1 0
E7M = X X 2 4 4 4
Em6/B = X 2 2 0 2 0
F#7(13) = 2 X 2 3 4 X
G#7(b5) = 4 X 4 5 3 X
G#m7(9) = X X 6 4 7 6
