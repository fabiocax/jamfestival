Ivan Lins - Um Natal Brasileiro

Intro: E7+ A7+

E7+  A7+/E      E7+
Voa, voa, passarinho
 A7+  E/G#     F#m7/9 F#m/E
Pousa de mansinho
 A7+/9     B7/9 B7/4 B7
No cartão-postal
E7+  A7+/E      E7+
Canta, conta, Padrezinho
 A7+  E/G#  F#m7/9 F#m/E
Como que seria
 A7+/9     B7/9 B7/4 B7
Um feliz Natal

A      A/B          G#m7/9 C#m7/9
Que o Betinho imaginou
A      A/B          E7+
E que o povo tem de cor
E/F# E/G# A7+    G#m7/9      F#m7/9
   Esquecer que dura só um dia
 G#m7/9           A7+
Lembrar velhas canções
        G#m7      F#m
Que um ano se anuncia
       G#m7/9      A
Mais paz nos corações
   G#m7/9        F#M7/9
Beijar quem lhe faria
  B7/9          E7+
Feliz o seu Natal

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/B = X 2 2 2 2 X
A7+ = X 0 2 1 2 0
A7+/9 = X 0 2 1 0 0
A7+/E = X X 2 2 2 4
B7 = X 2 1 2 0 2
B7/4 = X 2 4 2 5 2
B7/9 = X 2 1 2 2 X
C#m7/9 = X 4 2 4 4 X
E/F# = X X 4 4 5 4
E/G# = 4 X 2 4 5 X
E7+ = X X 2 4 4 4
F#m = 2 4 4 2 2 2
F#m/E = X X 2 2 2 2
F#m7/9 = X X 4 2 5 4
G#m7 = 4 X 4 4 4 X
G#m7/9 = X X 6 4 7 6
