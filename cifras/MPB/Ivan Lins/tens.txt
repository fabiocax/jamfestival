Ivan Lins - Tens


E/D  C#m7               F#m7              B7/4
        Tens o meu sorriso, tens tua agonia  
     B7            E7+/4         
Tens a festa, tens a dança, tens  
   E7+     A7+
a cantoria 
               Bbm5-/7  
Tens no meu amor         
D#7/9-/5-       D7+,G#7,C#7+,G7
 tua      teimosia  
C7+          C6        F7+   F6  
   Tens no meu silencio tua garantia  
Bb7+      Bb6           Am7  
  Tens na tua graça, e o beijo,  
      Dm7      G7/4  
tens a fantasia  
        G7             C7/4  
Tens na mão a faca e o queijo  
       C7          F7+/4 F7+ E4/7  E/D   
Tens a noite, o   dia  
C#m7               F#m7                              
    Tens na minha ausência tua  
         B7/4           B7           E7+/4
companhia       Tens a fama, tens a lama, tens 
  E7+     A7+
a ironia  
             Bbm5-/7   
Tens na minha dor  
D#7/9-/5-     D7+,G#7,C#7+,G7  
 tua      melodia  
C7+          C6        F7+   F6  
  Tens na minha sombra a tua moradia  
Bb7+      Bb6          Am7           Dm7      G7/4 
  Tens no quarto o cão-vigia, tens a valentia  
          G7           C7/4      
Mas só na minha morte então terás  
 C7      F7+/4,F7+,E7/4,E/D  
tua calmaria

----------------- Acordes -----------------
A7+ = X 0 2 1 2 0
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
B7/4 = X 2 4 2 5 2
Bb6 = X 1 3 0 3 X
Bb7+ = X 1 3 2 3 1
Bbm5-/7 = X 1 2 1 2 X
C#7+ = X 4 6 5 6 4
C#m7 = X 4 6 4 5 4
C6 = 8 X 7 9 8 X
C7 = X 3 2 3 1 X
C7+ = X 3 2 0 0 X
C7/4 = X 3 3 3 1 X
D#7/9-/5- = X X 1 2 2 0
D7+ = X X 0 2 2 2
Dm7 = X 5 7 5 6 5
E/D = X 5 X 4 5 4
E4/7 = 0 2 0 2 0 X
E7+ = X X 2 4 4 4
E7+/4 = X X 2 2 4 4
E7/4 = 0 2 0 2 0 X
F#m7 = 2 X 2 2 2 X
F6 = 1 X 0 2 1 X
F7+ = 1 X 2 2 1 X
F7+/4 = X X 3 3 5 5
G#7 = 4 6 4 5 4 4
G7 = 3 5 3 4 3 3
G7/4 = 3 5 3 5 3 X
