Ivan Lins - Boa Nova

[Intro:] C7M(9) C6(9) G7(4)(9) G7(9)

C7M(9)
Vi a chuva da Amazônia
               G7(4)(9)
Vi todo o Jequitinhonha verdejar
C7M(9)
São João com o boi na ilha
                   G7(4)(9)      F#m7
Vi quadrilha com o forró no Ceará
       B7         Em7         A7 F#m7
Vi pinhão no pinheiro do Paraná
           B7               Em7                    A7 F#m7
           Côco verde no coqueiro em Paquetá
      B7           Em7          A7
Manga rosa nas mangueiras do Pará
      Dm7            G7(4)(9)
Quando vi você chegar

Vi o galo em Olinda
Vi capimba caboclinhos siriá
Vi vovô dançar ciranda
A varanda encher de melro e sabiá
Vi arara azul no galho do Araçá
Jacutinga no toco do Jatobá
Uirapuru cantando no Jequitibá
Quando vi você chegar

F#m7(9)  B7(4)   B7    E7M
Rio das    flores florescer
                  D7M                      E7M G7 F#m7(9)
Rio bonito toda se enfeitar pra esperar você
     B7(4)   B7  E7M
Porto Alegre festejar
                D7M                   E7M G7(4)(9)
Porto Velho rejuvenescer por você chegar

Vi trevo de quatro folhas
Vi a rolha do champanhe saltitar
Vi na carta dos ciganos
Nos arcanos o tarô me confirmar, confirmar
Vi brotar fruta madura no pomar
Vi jardim nascer jasmim e manacá
No Shashim crescer pé de tambatajá
Quando vi você chegar

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
B7(4) = X 2 4 2 5 2
C6(9) = X 3 2 2 3 3
C7M(9) = X 3 2 4 3 X
D7M = X X 0 2 2 2
Dm7 = X 5 7 5 6 5
E7M = X X 2 4 4 4
Em7 = 0 2 2 0 3 0
F#m7 = 2 X 2 2 2 X
F#m7(9) = X X 4 2 5 4
G7 = 3 5 3 4 3 3
G7(4)(9) = 3 X 3 2 1 X
G7(9) = 3 X 3 2 0 X
