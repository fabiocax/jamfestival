Ivan Lins - Quem Me Dera

[Intro:] A9

      A7M(9)   A6(9)         A9/E D7M
Quem me dera te dar, quem me dera,
  A/C#     Bm7   E7(4)(9) A9
Bons ventos e brisas
      A7M(9)     A6(9)        A9/E D7M
Quem me dera, pois é, quem me dera,
 A/C#      Bm7  Bm7/A E/G#
Se tanto precisas

                     D/F#   Dm/F   A/E
Te dar jeito, te encher de motivos,
 C#7(4)  C#7  F#m7(11) B7(4)(9)
Te dar    serven...tia
      B7(9)       B7(4)(9)  B7(9) D7M
Te dar corda, te encher de amigos,
  A/C#      Bm7  E7(4)(9) A9
Te dar mais valia

        E/G#  F#m7(11)         A9/E D7M
Quem me dera   te dar, quem me dera,
  A/C#       Bm7  E7(4)(9) A9
O que mais querias
        E/G#  F#m7(11)        A9/E D7M
Quem me dera, pois é, quem me dera,
  A/C#     Bm7 Bm7/A E/G#
Te dar garantia

        E       D/F#         Dm/F  A/E
Te dar força, mexer com teus brios,
G#m7(11)     C#7  F#m7(11)  B7(4)(9)
Te dar        valen......tia
     B7(9)     B7(4)(9)     B7(9)  D7M
Te dar filhos, serenos, tranquilos,
  A/C#      Bm7 E7 Fº F#m7
Te dar companhia

Bm7  E7      Bm7          E7   Bm7
Fora tudo que tenho de aprender
         E7        Bm7        E7 F#m7(9)
Fora os versos que tenho de criar
     C#7(#9)  F#m7(9)   C#7(#9) F#m7(9)
Fora as rezas que tenho de fazer
    C#7(#9)   F#m7(9)       C6
Fora o verde que tenho de inventar

Fora as luzes que tenho de acender
Fora os rios que tenho de inundar
Fora o dia que tenho de tecer
Fora o medo que tenho de espantar

Solo: Bm7 E7(4)(9) A9

Te dar calma, te encher de mistérios,
Te dar desafios
Te dar sonhos, mexer no teu tédio,
Te dar meus navios

Te dar sorte, te encher de imprevistos,
Te dar mais ainda
Te dar festas, mexer no teu riso,
Te dar boas vindas

----------------- Acordes -----------------
A/C# = X 4 X 2 5 5
A/E = 0 X 2 2 2 0
A6(9) = 5 4 4 4 X X
A7M(9) = X 0 2 1 0 0
A9 = X 0 2 2 0 0
A9/E = 0 X 2 2 0 0
B7(4)(9) = X 2 2 2 2 2
B7(9) = X 2 1 2 2 X
Bm7 = X 2 4 2 3 2
Bm7/A = 5 2 4 2 3 2
C#7 = X 4 3 4 2 X
C#7(#9) = X 4 3 4 5 X
C#7(4) = X 4 6 4 7 4
C6 = 8 X 7 9 8 X
D/F# = 2 X 0 2 3 2
D7M = X X 0 2 2 2
Dm/F = X X 3 2 3 1
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E7 = 0 2 2 1 3 0
E7(4)(9) = X X 2 2 3 2
F#m7 = 2 X 2 2 2 X
F#m7(11) = 2 X 2 2 0 X
F#m7(9) = X X 4 2 5 4
Fº = X X 3 4 3 4
G#m7(11) = 4 X 4 4 2 X
