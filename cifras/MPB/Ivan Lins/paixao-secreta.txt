Ivan Lins - Paixão Secreta

( Ivan Lins / Vítor Martins )


[Intro:] F9 Em7 Dm7 G7(4) G7(13)

C7M(9)
Quando abriu a porta e entrou
     Am7(9)
Mudou tanto a atmosfera
            Dm7(9)           F/G
Que nem precisei dizer quem era
  G7(9)        C7M(9)
Bastou se aproximar de mim
   C/E               F7(9)(11) G7(9)(11)
Pra eu jogar fora o meu gim

 Am7(9)
E sem usar droga nenhuma
         F9
Estranhamente eu entrei numa
      C/E Dm7(9)               F/G
De contar piadas, de dar gargalhadas
G7(9)   C7M(9)                      E7/G#
E quando dei por mim já estava assim

Igual aos seres mais felizes
Aos que nunca estiveram em crise
Aos que já esqueceram suas cicatrizes
Emoçando outra vez meu coração

   F9                   Bb7(9)(11) Bb7(9)
Estava revelando a todos
   Em7         A7(4) D7(9)(11)
Minha paixão secreta
    F9          F/G G7(9)
Minha paixão secreta
  C7(4)   C7 C7(9)(11) C/Bb
Minha paixão
   F9                   Bb7(9)(11) Bb7(9)
Estava revelando a todos
   Em7         A7(4) D7(9)(11)
Minha paixão secreta
    F9          F/G G7(9) C7M(9)
Minha paixão secreta

Quando abriu a porta e saiu
Mudou tanto a atmosfera
Que nem precisei dizer quem era
Bastou se afastar de mim
E o garçom me trouxe o gim

----------------- Acordes -----------------
A7(4) = X 0 2 0 3 0
Am7(9) = X X 7 5 8 7
Bb7(9) = X 1 0 1 1 X
Bb7(9)(11) = X 1 1 1 1 1
C/Bb = X 1 2 0 1 X
C/E = 0 3 2 0 1 0
C7 = X 3 2 3 1 X
C7(4) = X 3 3 3 1 X
C7(9)(11) = X 3 3 3 3 3
C7M(9) = X 3 2 4 3 X
D7(9)(11) = X 5 5 5 5 5
Dm7 = X 5 7 5 6 5
Dm7(9) = X 5 3 5 5 X
E7/G# = 4 X 2 4 3 X
Em7 = 0 2 2 0 3 0
F/G = 3 X 3 2 1 X
F7(9)(11) = X X 3 3 4 3
F9 = 1 3 5 2 1 1
G7(13) = 3 X 3 4 5 X
G7(4) = 3 5 3 5 3 X
G7(9) = 3 X 3 2 0 X
G7(9)(11) = 3 X 3 2 1 X
