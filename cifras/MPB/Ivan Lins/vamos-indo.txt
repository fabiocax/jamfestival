Ivan Lins - Vamos Indo

Ivan Lins / Aldir Blanc

[Intro:] Bm7(9) Bm7(9)/A G6 F#7(b13)
         Bm7(9) Bm7(9)/A G6 F#7(b13)(11) F#7/A# Bm7(9)

                             Em7(9)
Ah, como nos leva esse vinho!
                         G#º G7(4)
Corpos levitando no ninho
       G7   F#7(4)    F#7 Bm7(9)
Desgarramos: Shangrilá

                            Em7(9)
Me deságua em mar de carinho
                        G#º    G7(4)
Me naufraga em lençóis de linho
 G7    F#7(4) F#7 Bm7(9)  Em7(9)
Leguas, levas de   delírio

       Gm/Bb   D7M/A D6/A Em7(9)
Luar de seda azulando
           Gm/Bb    D7M/A    Bm7(9) C7(9)(13)
Do Farol da Barra ao Redentor
        C7(b9)    F7M/C Dm7(9) Fm7(9)
E uma canção de Caetano
         Bb7(13) Em7(11) F#7(b13)(11) F#7/A# Bm7(9)
Encantando nosso amor

Ah, como nos leva esse vinho!
Infinito é meio caminho
Fecha os olhos, vamos indo

----------------- Acordes -----------------
Bb7(13) = 6 X 6 7 8 X
Bm7(9) = X 2 0 2 2 X
Bm7(9)/A = 5 4 4 P7 7 7
C7(9)(13) = X 3 2 P3 3 5
C7(b9) = X 3 2 3 2 X
D6/A = X 0 4 4 3 X
D7M/A = 5 5 4 2 2 2
Dm7(9) = X 5 3 5 5 X
Em7(11) = X 7 X 7 8 5
Em7(9) = X 7 5 7 7 X
F#7 = 2 4 2 3 2 2
F#7(4) = 2 4 2 4 2 X
F#7(b13) = 2 X 2 3 3 2
F#7(b13)(11) = 2 X 2 4 3 X
F#7/A# = 6 X 4 6 5 X
F7M/C = X 3 3 2 5 X
Fm7(9) = X X 3 1 4 3
G#º = 4 X 3 4 3 X
G6 = 3 X 2 4 3 X
G7 = 3 5 3 4 3 3
G7(4) = 3 5 3 5 3 X
Gm/Bb = 6 5 5 3 X X
