Ivan Lins - Madalena

[Intro] D7M(9)  D6(9)  A4(7/9) A7(9-)

          D7M(9) D6(9)
Oh, Madalena
        A4(7/9) A7(9-)  D7M(9) D6(9)
O meu peito percebeu
      A4(7/9) A7(9-)   D7M(9)
Que o mar é uma gota
      A4(7/9)  A7(9-)  Am7  D7(9)
Comparado ao pranto meu
        G7M  G6
Fique certa
           Am7  D7(9-)    G7M  G6
Quando o nosso amor desperta
        Am7    D7(9-)  G7M G6
Logo o sol se desespera
        G7M  F#      G/A
E se esconde lá na serra
 B7/9-   Em
Eh Madalena
        Em7/D        C#7/9+
O que é meu não se divide
         F#5+/7     Bm7
Nem tão pouco se admite
          Bm7/A       G#m7  C#7/9
Quem do nosso amor duvide.
       F#7+      G#m7      A#m7
Até a lua se arrisca num palpite
                   A7
Que o nosso amor existe
           D/E              A4(7/9)
Forte ou fraco, alegre ou triste

 (D7M(9) A4(7/9))
Oh, Madalena, Madalena, Madalena, Madalena
    D7M(9)    A4(7/9)       D7M(9)
Oh Ma, oh Mada, oh Madale
       A4(7/9)              D7M(9)
Oh Madale, le, le, le oh Ma, oh Mada

( G7M(9)  G6  C/D  D7(b9) )

       G7M(9) G6
Oh, Madalena
      C/D  D7(b9)  G7M(9) G6
O meu peito per...cebeu
      C/D D7(b9) G7M(9) G6
Que o mar é uma gota
     C/D     D7(b9) Dm7(9) F/G G7(13)
Comparado ao pranto meu
      C7M  Am7
Fique certa
        Dm7(9)   G7    C7M  Am7
Quando o nosso amor desperta
      Dm7(9)   G7  C7M Am7
Logo o sol se desespera
      Dm7(9) Dm7(9)/C Bm7(11)
E se esconde lá na serra

E7(b13) Am7(9)
Eh Madalena
      Gm7(11)    C7 F#m7(9)
O que é meu não se divide
      B7(b13)    Em7M(9) Em7(9)
Nem tão pouco se admite
       Dm7(9) G7(13) C#m7(11) F#7(b9)
Quem do nosso amor duvide
     B7M(9)    E/B          C#/B
Até a lua se arrisca num palpite
     G7M/B  A7(13) D7M Bm7
Que o nosso amor existe
          Em7   A7(13)    Am7(9) C/D
Forte ou fraco, alegre ou triste

----------------- Acordes -----------------
A#m7 = X 1 3 1 2 1
A4(7/9) = 5 X 5 4 3 X
A7 = X 0 2 0 2 0
A7(13) = X 0 X 0 2 2
A7(9-) = 5 X 5 3 2 X
Am7 = X 0 2 0 1 0
Am7(9) = X X 7 5 8 7
B7(b13) = X 2 X 2 4 3
B7/9- = X 2 1 2 1 X
B7M(9) = X 2 1 3 2 X
Bm7 = X 2 4 2 3 2
Bm7(11) = 7 X 7 7 5 X
Bm7/A = 5 2 4 2 3 2
C#/B = X 2 3 1 2 X
C#7/9 = X 4 3 4 4 X
C#7/9+ = X 4 3 4 5 X
C#m7(11) = X 4 4 4 5 4
C/D = X X 0 0 1 0
C7 = X 3 2 3 1 X
C7M = X 3 2 0 0 X
D/E = X X 2 2 3 2
D6(9) = X 5 4 2 0 0
D7(9) = X 5 4 5 5 X
D7(9-) = X 5 4 5 4 X
D7(b9) = X 5 4 5 4 X
D7M = X X 0 2 2 2
D7M(9) = X 5 4 6 5 X
Dm7(9) = X 5 3 5 5 X
E/B = X 2 2 1 0 0
E7(b13) = 0 X 0 1 1 0
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
Em7(9) = X 7 5 7 7 X
Em7/D = X X 0 0 3 0
Em7M(9) = 0 X X 0 4 2
F# = 2 4 4 3 2 2
F#5+/7 = 2 X 2 3 3 2
F#7(b9) = X X 4 3 5 3
F#7+ = 2 X 3 3 2 X
F#m7(9) = X X 4 2 5 4
F/G = 3 X 3 2 1 X
G#m7 = 4 X 4 4 4 X
G/A = 5 X 5 4 3 X
G6 = 3 X 2 4 3 X
G7 = 3 5 3 4 3 3
G7(13) = 3 X 3 4 5 X
G7M = 3 X 4 4 3 X
G7M(9) = 3 2 4 2 X X
G7M/B = 7 X 5 7 7 X
Gm7(11) = 3 X 3 3 1 X
