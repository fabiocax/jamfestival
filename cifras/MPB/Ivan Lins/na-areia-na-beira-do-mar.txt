Ivan Lins - Na Areia, na Beira do Mar

Ivan Lins / Cláudio Jorge

G                 C9
Meu amor, vem me ver
                   G
Vem saber, vem contar
                           C9
Quantas vezes escrevo teu nome
                       G
Na areia, na beira do mar

                    F#m7
A rosa que trago no peito
   B7             Em7
Você me mandou tatuar
     A7                Dm7
Teu nome gravado de um jeito
                G7       C C7M
Que faz eu pra sempre ficar
   C6                   A7
Ficar escrevendo o teu nome
              D7  D/F# G
Na areia, na beira do mar

Meu barco, meu porto, meu leme,
Meu cais, minha luz do luar
Você guia meu pensamento
Fazendo eu depressa voltar
Voltar escrevendo o teu nome
Na areia, na beira do mar

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
C6 = 8 X 7 9 8 X
C7M = X 3 2 0 0 X
C9 = X 3 5 5 3 3
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
Dm7 = X 5 7 5 6 5
Em7 = 0 2 2 0 3 0
F#m7 = 2 X 2 2 2 X
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
