Ivan Lins - Dindi

( Aloysio de Oliveira / Tom Jobim )


C7M                 Bb7M
Céu! Tão grande é o céu
            C7M                 Bb7M
E bandos de nuvens que passam ligeiras
   A7M        F#m7(9) F#m7M(9)         Bm7 E7(9)(11) E7(9) A7M
Pra onde elas vão? Ah! Eu não sei, não sei...
    C7M                Bb7M
E o vento que fala nas folhas
               C7M                   Bb7M
Contando as histórias que são de ninguém...
A7M       F#m7(9) F#m7M(9)   Bm7 E7(9)(11) E7(9)
Mas que são mi...nhas e de você também...

C7M    Bb7M C7M
Ah! Dindi...
                           Gm7
Se soubesses o bem que te quero
F#7(b5)   F7M       Bb7(9)      C7M           Bb7M C7M
O mundo seria Dindi, tudo, Dindi, lindo, Dindi...
       Bb7M
Ah! Dindi...
C7M                  Gm7   F#7(b5)   F7M        Bb7(9)
Se um dia você for embora me leva contigo, Dindi
          C7M           F#m7(b5)(9) B7(b13) Em7M(9) Em7(9)
Fica Dindi...Olha, Dindi...

         B7(b13)  B7 Em7(9) B7(b13) Em7(9) A7(13) Dm7(9)
E as águas desse rio onde vão eu não sei
             A7(b13) A7 Dm7(9) A7(b13)
A minha vida inteira esperei,
   Dm7(9) G7(11) G7(13) C7M    Bb7M C7M
Esperei     por       vo....cê, Dindi
                              Gm7
Que é a coisa mais linda que existe
 F#7(b5) F7M(9)    F7M
Você não existe Dindi
F#7M(9) F#7M G7M(9)   G7M           G#7M(9)   G#6 D#7(9)(11)
Olha Dindi...Deixa Dindi...Que eu te adore Dindi...

C#7M   B7M
Ah! Dindi...
      C#7M                 G#m7
Se soubesses o bem que te quero
G7(b5)  Gm7(b5)    F#m6         Fm7           E7(9)(11) E7(9)
O mundo seria Dindi, tudo, Dindi, lindo, Dindi...
Bbm7        D#7(b9)
Nosso, Dindi...

Solo: C#7M G#m7 E7M Bm7 G7M F7M

G7(4) G7  C7M    Bb7M
Ah!  vo...cê, Dindi...
 C7M                          Gm7
Que é a coisa mais linda que existe
 F#7(b5) F#m7(b5)    Fm6
Você não existe Dindi
          Em7         D#7(9)(11) D#7(9)
Olha Dindi...Adivinha Dindi...
Am7                  G#7M(b13)   E7M
Deixa Dindi...Que eu te adore Dindi...

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7(13) = X 0 X 0 2 2
A7(b13) = X 0 X 0 2 1
A7M = X 0 2 1 2 0
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
B7(b13) = X 2 X 2 4 3
B7M = X 2 4 3 4 2
Bb7(9) = X 1 0 1 1 X
Bb7M = X 1 3 2 3 1
Bbm7 = X 1 3 1 2 1
Bm7 = X 2 4 2 3 2
C#7M = X 4 6 5 6 4
C7M = X 3 2 0 0 X
D#7(9) = X 6 5 6 6 X
D#7(9)(11) = X X 1 1 2 1
D#7(b9) = X 6 5 6 5 X
Dm7(9) = X 5 3 5 5 X
E7(9) = X X 2 1 3 2
E7(9)(11) = X X 2 2 3 2
E7M = X X 2 4 4 4
Em7 = 0 2 2 0 3 0
Em7(9) = X 7 5 7 7 X
Em7M(9) = 0 X X 0 4 2
F#7(b5) = 2 X 2 3 1 X
F#7M = 2 X 3 3 2 X
F#7M(9) = 2 1 3 1 X X
F#m6 = 2 X 1 2 2 X
F#m7(9) = X X 4 2 5 4
F#m7(b5) = 2 X 2 2 1 X
F#m7(b5)(9) = P2 3 6 2 X X
F#m7M(9) = 2 0 3 1 X X
F7M = 1 X 2 2 1 X
F7M(9) = X 8 7 9 8 X
Fm6 = 1 X 0 1 1 X
Fm7 = 1 X 1 1 1 X
G#6 = 4 X 3 5 4 X
G#7M(9) = 4 3 5 3 X X
G#7M(b13) = 4 X 5 5 5 X
G#m7 = 4 X 4 4 4 X
G7 = 3 5 3 4 3 3
G7(11) = 3 5 3 5 3 X
G7(13) = 3 X 3 4 5 X
G7(4) = 3 5 3 5 3 X
G7(b5) = 3 X 3 4 2 X
G7M = 3 X 4 4 3 X
G7M(9) = 3 2 4 2 X X
Gm7 = 3 X 3 3 3 X
Gm7(b5) = 3 X 3 3 2 X
