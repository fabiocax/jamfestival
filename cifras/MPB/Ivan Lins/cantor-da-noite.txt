Ivan Lins - Cantor da Noite

 Em7(9)(11)          A7(4)(9) A7(13)         D9 B7(4)(9)
Era apenas uma noite a mais                sem sono
   Em7(9)(11)         A7(4)(9)   A7(13)       Am7(11) D7(b9)
Dessas noites que a gente sai                 sem dono
 G#m7(11)         C#7(b9) C7(4)(9)   C7(9)(#11)
Procurando um samba-canção      no vento
 Em7(9)(11)             A7(4)(9)   A7(13)     C#m7(9)(11)   F#7(b9)
Pois o rádio não me deixa ouvir                    faz tempo

Era apenas um ponto de luz discreto
E aquela voz cada vez mais, mais perto
Foi entrando em meu coração contente
Como se eu fosse ser feliz pra sempre

Bm7         E7(9)(13)
Não, não pare mais,
Bm7       E7(9)(13)
Canta, canta mais
     G7M(9)       C7(4)(9)(13)
Pois no fundo você
     C7(9)        D/F# Bm7(9) Bm7M(9)
Sabe o bem que me faz

 Fm7(9)(11)          Bb7(4)(9)  Bb7(13)       Eb9   Gm7 C7(#9)
Só assim eu uso a alma e não                   o corpo
 Fm7(9)(11)          Bb7(4)(9)   Bb7(13)       Bbm7(11)   Eb7(b9)
Só assim eu sinto mais prazer                      e gosto
  Am7(11)            D7(13)  D/C    B7(4)(9) B7(9)(13)
Só assim estou comigo mais            um pouco
 Em7(9)(11)        A7(4) A7(13)
Só assim sou todo coração
 Fm7(9)(11)           Bb7(4) Bb7(13)
Só assim estou em minhas mãos
   Em7(9)(11)           A7(4) A7      D9 Bm7(11) Em7(9)
E não quero pôr os pés no chão        em vão

Solo: A7(4)(9) A/G F#(#5) B7(13) B7(b13) E7(13) E7(b13)
      A7(4)(9) A/G F#(#5) B(#5)

   Em7(9)(11)           A7(4) A7
E não quero pôr os pés no chão
  F6(9)
Em vão

----------------- Acordes -----------------
A/G = 3 X 2 2 2 X
A7 = X 0 2 0 2 0
A7(13) = X 0 X 0 2 2
A7(4) = X 0 2 0 3 0
A7(4)(9) = 5 X 5 4 3 X
Am7(11) = 5 X 5 5 3 X
B(#5) = X 2 1 0 0 X
B7(13) = X 2 X 2 4 4
B7(4)(9) = X 2 2 2 2 2
B7(9)(13) = 7 X 7 6 4 4
B7(b13) = X 2 X 2 4 3
Bb7(13) = 6 X 6 7 8 X
Bb7(4) = X 1 3 1 4 1
Bb7(4)(9) = X 1 1 1 1 1
Bbm7(11) = 6 X 6 6 4 X
Bm7 = X 2 4 2 3 2
Bm7(11) = 7 X 7 7 5 X
Bm7(9) = X 2 0 2 2 X
Bm7M(9) = X 2 0 3 2 X
C#7(b9) = X 4 3 4 3 X
C#m7(9)(11) = X 4 2 4 4 2
C7(#9) = X 3 2 3 4 X
C7(4)(9) = X 3 3 3 3 3
C7(4)(9)(13) = X 3 3 3 3 5
C7(9) = X 3 2 3 3 X
C7(9)(#11) = X 3 2 3 3 2
D/C = X 3 X 2 3 2
D/F# = 2 X 0 2 3 2
D7(13) = X 5 X 5 7 7
D7(b9) = X 5 4 5 4 X
D9 = X X 0 2 3 0
E7(13) = 0 X 0 1 2 0
E7(9)(13) = 0 X 0 1 2 2
E7(b13) = 0 X 0 1 1 0
Eb7(b9) = X 6 5 6 5 X
Eb9 = X 6 8 8 6 6
Em7(9) = X 7 5 7 7 X
Em7(9)(11) = X 7 5 7 7 5
F#(#5) = 2 5 4 3 2 X
F#7(b9) = X X 4 3 5 3
F6(9) = 1 0 0 0 X X
Fm7(9)(11) = X 8 6 8 8 6
G#m7(11) = 4 X 4 4 2 X
G7M(9) = 3 2 4 2 X X
Gm7 = 3 X 3 3 3 X
