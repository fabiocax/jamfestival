Ivan Lins - Camaleão

D
Sou um camaleão
Bm7
To aqui mas não tô
G7+                   A7
Do arco-íris furtei a cor
D
Moro em constelação
Bm7
Sumo a cada manhã
G7+                   A7
Da festança que deu tupã

G              A
Dança cauda terçã
D7+            Bm
Braço de cortesã
G7+                   A7
Esmeralda e oi que azulou
G              A
Tive um amor assim
D7+            Bm
Juras que só a mim
G7+                   A7
Mas viu outro e mudou de cor

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
D = X X 0 2 3 2
D7+ = X X 0 2 2 2
G = 3 2 0 0 0 3
G7+ = 3 X 4 4 3 X
