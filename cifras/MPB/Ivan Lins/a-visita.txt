Ivan Lins - A Visita

[Intro] E7M  D/E  C9/E  F#/E  
        A/B  F/B  E7M

E7M           D#m7(9)   G#7(5+)
Minha amiga, me visite 
C#m7  C#m/B        Bbm7   D#6(7)
Tenho andado muito triste
    G#m7M(9)      G#m/F#  Fm7  Bb6(7)
Sem notícias dos amigos
        C#/D#        F#m7  B6(7)
Sem ter sonhos nesta cama 
E7M                D#m7(9)  G#7(5+)
E nos meus fins de semana
       C#m7   C#m/B  Bbm7  D#6(7)
Faço a barba, me penteio
  F#/G#   C#/G#  F#m7  F/B
E nada, ninguém veio
E7M                D#m7(9)  G#7(5+)
Quero ver o seu sorriso
    C#m7   C#m/B   Bbm7  D#6(7)
Sua cara tão bonita
    G#m7M(9)    G#m/F#  Fm7  Bb6(7)
Os seus dentes me mordendo
      C#/D#        F#m7  B6(7)
Me marcando, me sangrando
E7M              D#m7(9)  G#7(5+)
Minha amiga, me visite
         C#m7   C#m/B  Bbm7  D#6(7)
Pra eu ficar me enganando
    F#/G#  C#/G#    F#m7  F/B 
Pensando que estou vivo

----------------- Acordes -----------------
A/B = X 2 2 2 2 X
Bbm7 = X 1 3 1 2 1
C#/D# = 11 X 11 10 9 X
C#/G# = 4 X 3 1 2 X
C#m/B = 7 X 6 6 5 X
C#m7 = X 4 6 4 5 4
C9/E = X X 2 5 3 0
D#m7(9) = X 6 4 6 6 X
D/E = X X 2 2 3 2
E7M = X X 2 4 4 4
F#/E = X X 2 3 2 2
F#/G# = 4 X 4 3 2 X
F#m7 = 2 X 2 2 2 X
F/B = X 2 3 2 1 1
Fm7 = 1 X 1 1 1 X
G#7(5+) = 4 X 4 5 5 4
G#m/F# = X X 4 4 4 4
G#m7M(9) = 4 2 5 3 X X
