Ivan Lins - Lembrança

[Intro:] F#7M(9) G#m7(9) F#6(9/11+) B7M(6) E7(4)(9) E7(9)(#11)
         Eb7(4)(9) Eb7(9)(#11) D7(4)(9) D7(9)(#11) C#7(4)(9)
         Fm7(9) Gm7(9) G#m7(9) C#7(b9) F#7M(9)

 G#m7(9)    F#6(9/11+)  B7M(6)  E7(4)(9)      E7(9) F#/Bb     Ebm7(9)(11) Ebm7 G#7(4)(9)
Sei que        o papel que    me o............ferece,           por hora
G#7(9)(#11)     C#7(4)(9)
Me entristece, me apavora
C#/B     Bb7(9)(13)  Bb7(9)     Eb7(#9)       Eb7(9)      Eb7(b9)    Eb7(b13)     G#7(4)(9)   G#7(9)(13)
Não me acrescenta,                                           não           me         interessa
C#7(4)(9) C#7(4) G#/F# Bm/F#
Me violenta

Sinceramente, agradeço, não posso
Conheço o preço de um remorso
De uma maldade
De uma tortura
F#7M(9) F#7M(#11)
De uma saudade

Fm7(9)  Bb7(#9) Ebm7(9)(11)  Ebm/C#  Cm7(9)(11) F7(b9) Bb7M(9) Gm7(11)
Fica na carne, nos ossos e no coração
Am7(9) D7(13) Gm7M(9) Gm/F
Doendo pra sempre
 C#7(4)(9) C#7(b5)
Lembrando a gente

Acreditar nos seus projetos, seus planos
Será por certo ou outro engano
Mais um tropeço
Mais um fracasso
Que eu não mereço

Fim: F#7M(9) E7M(9) D7M(9) C#7(4)(9) F#m7(9)

----------------- Acordes -----------------
Am7(9) = X X 7 5 8 7
B7M(6) = X 2 X 3 4 4
Bb7(#9) = X X 8 7 9 9
Bb7(9) = X 1 0 1 1 X
Bb7(9)(13) = X 1 0 1 1 3
Bb7M(9) = X 1 0 2 1 X
Bm/F# = X X 4 4 3 2
C#/B = X 2 3 1 2 X
C#7(4) = X 4 6 4 7 4
C#7(4)(9) = X 4 4 4 4 4
C#7(b5) = X 4 5 4 6 X
C#7(b9) = X 4 3 4 3 X
Cm7(9)(11) = X 3 1 3 3 1
D7(13) = X 5 X 5 7 7
D7(4)(9) = X 5 5 5 5 5
D7(9)(#11) = X X 0 1 1 0
E7(4)(9) = X X 2 2 3 2
E7(9) = X X 2 1 3 2
E7(9)(#11) = X X 2 3 3 2
Eb7(#9) = X 6 5 6 7 X
Eb7(4)(9) = X X 1 1 2 1
Eb7(9) = X 6 5 6 6 X
Eb7(9)(#11) = X X 1 2 2 1
Eb7(b13) = X 6 X 6 8 7
Eb7(b9) = X 6 5 6 5 X
Ebm/C# = 9 X 8 8 7 X
Ebm7 = X X 1 3 2 2
Ebm7(9)(11) = X 6 4 6 6 4
F#/Bb = 6 X 4 6 7 X
F#6(9/11+) = X 3 2 2 3 2
F#7M(#11) = 2 X 3 3 1 1
F#7M(9) = 2 1 3 1 X X
F7(b9) = X X 3 2 4 2
Fm7(9) = X X 3 1 4 3
G#/F# = 2 X 1 1 1 X
G#7(4)(9) = 4 X 4 3 2 X
G#7(9)(#11) = 4 3 4 3 3 X
G#7(9)(13) = 4 X 4 3 1 1
G#m7(9) = X X 6 4 7 6
Gm/F = X X 3 3 3 3
Gm7(11) = 3 X 3 3 1 X
Gm7(9) = X X 5 3 6 5
Gm7M(9) = 3 1 4 2 X X
