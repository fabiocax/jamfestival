Luiz Gonzaga - Acauã

F                                  F7
Acauã, acauã vive cantando
                 Bb       C7       F   F7
Durante o tempo do verão
         Bb            C7       F
No silêncio das tardes agourando
                     G7/B   C/Bb   F/A
Chamando a seca pro sertão
                     G7/B   C/Bb   F/A
Chamando a seca pro sertão

F7
Acauã,
Acauã,
Teu canto é penoso e faz medo
Te cala acauã,
               G7/B   C/Bb   F/A
Que é pra chuva voltar cedo
                 G7/B   C/Bb   F/A
Que é pra chuva voltar cedo


F7
Toda noite no sertão
Canta o João Corta-Pau
A coruja, mãe da lua
       G7/B    C/Bb      F/A
A peitica e o bacurau

F7
Na alegria do inverno
Canta sapo, gia e rã
Mas na tristeza da seca
         G7/B    C/Bb      F/A
Só se ouve acauã
          G7/B    C/Bb      F/A
Só se ouve acauã
F7
Acauã, Acauã...

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C/Bb = X 1 2 0 1 X
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
F7 = 1 3 1 2 1 1
G7/B = X 2 3 0 3 X
