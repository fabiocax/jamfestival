Luiz Gonzaga - Vem Morena

                     Fm
Vem morena pros meus braços
      Bb7          Fm
Vem morena, vem dançar
      Bb7         Fm
Quero ver tu requebrando
       Bb7        Fm
Quero ver tu requebrar

       Bb7       Fm
Quero ver tu remexendo
                   Bb7
No resfolêgo da sanfona
             Fm
Até o sol raiar

       Bb7       Fm
Quero ver tu remexendo
                   Bb7
No resfolêgo da sanfona
             Fm
Até o sol raiar


                 Fm
Esse teu fungado quente
       Bb7          Fm
Bem no pé do meu pescoço
     Bb7           Fm
Arrepia o corpo da gente
       Bb7          Fm
Faz o velho ficar moço
    Bb7        C7
E o coração da gente
       Bb7            Fm
Bota o sangue em alvoroço

                 Fm
Esse teu suor salgado
       Bb7        Fm
É gostoso e tem sabor
       Bb7         Fm
Pois o teu corpo suado
          Bb7        Fm
Com esse cheiro de fulô
       Bb7       C7
Tem o gosto temperado
    Bb7         Fm
Dos temperos do amor

----------------- Acordes -----------------
Bb7 = X 1 3 1 3 1
C7 = X 3 2 3 1 X
Fm = 1 3 3 1 1 1
