Luiz Gonzaga - Xodó

[Intro] E  C#m  F#m

  E                       C#m
Que falta eu sinto de um bem
  F#m                 B    E
Que falta me faz um xodo
 E                         C#m
Mas como eu não tenho ninguem
F#m                B       E
Eu levo a vida assim tão só
Bm               E
Eu só quero um amor
  Bm                F#m
Que acabe o meu sofrer
C#m        F#m
Um xodó pra mim
 C#m             F#m
Do meu jeito assim
  B                 E
Que alegre o meu viver
  E                       C#m
Que falta eu sinto de um bem

  F#m             B     E
Que falta me faz um xodo
 E                         C#m
Mas como eu não tenho ninguem
F#m                B       E
Eu levo a vida assim tão só
Bm               E
Eu só quero um amor
  Bm                F#m
Que acabe o meu sofrer
C#m        F#m
Um xodó pra mim
 C#m             F#m
Do meu jeito assim
         B            E
Que alegre o meu sofrer

----------------- Acordes -----------------
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
