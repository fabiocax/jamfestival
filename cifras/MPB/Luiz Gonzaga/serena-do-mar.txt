Luiz Gonzaga - Serena do Mar

Intro: G  D  A  G  D

D
Serena no mar, serena no mar
                 Am
Um, dois, três, quatro
                        Em
Cinco, seis, é a minha vez

Serena no mar, serena no mar
                     F#m
Eu vou caçar lá uma rês
        A           D
E no mato vou galopar

Serena no mar, serena no mar
         Am
Cabra danado
                   Em
Nunca diga que me deu


Serena no mar, serena no mar
                        F#m
Oi diga que brigou mais eu
         A             D  D7
E quase morre de apanhar

          G
Mas, serenou

Caminho lá na praia
           F#m
Eu espero já
              B7
Espero boquioso
               Em
Um côco de ganzá
            A7
Se deu pro cão, seu moço
                  Am  D7
Mas tem Deus prá dar
          G
Mas, serenou

Menina olha e sai
                 F#m
Deixe a gente olhar
                    B7
Quem diz que dá no couro
               Em               G
No couro não dá   no couro não dá
             D
Serenou no mar

Serena no mar, serena no mar
          F#m
Prá inbornar
                  Em
Espingarda é baioneta

Serena no mar, serena no mar
                 A7
Laçando sou carrapeta
                  D
Só paro quando parar

Serena no mar, serena no mar
           Am
Deixa de fágio
                   Em
E não me faça desafio

Serena no mar, serena no mar
                   F#m
Comigo só tem no trilho
     A           D  D7
Sereia canta no mar

          G
Mas, serenou

Caminho lá na praia
           F#m
Eu espero já
              B7
Espero boquioso
               Em
Um côco de ganzá
            A7
Se deu pro cão, seu moço
                  Am  D7
Mas tem Deus prá dar
          G
Mas, serenou

Menina olha e sai
                 F#m
Deixe a gente olhar
                    B7
Quem diz que dá no couro
               Em               G
No couro não dá   no couro não dá
             D
Serenou no mar

Intro: G  D  A  G  D

Serena no mar.....

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
