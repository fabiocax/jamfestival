Luiz Gonzaga - Amei À Toa

Intro: G7 C G Em A7 D7 G (2x)

G      C       G
Amar é coisa á boa
   G7    C        G
Só nunca foi pra mim

Em       A7    Em
Ai como amei à toa
G7       C     G
Ai  como fui ruim

Em       A7    Em
Ai como amei à toa
     C   D     G
Ai  como fui ruim

Solo G7 C G Em A7 D7 G (2x)
G          C      G
Se hoje eu vivo a esmo
    G7    C      G
Sem ter a quem amar


Em      A7      Em
Culpado sou eu mesmo
    G7    C     G
Por não saber amar

Em      A7      Em
Culpado sou eu mesmo
    C      D    G
Por não saber amar

Solo G7 C G C G Em A7 Em C D G/ Em A7 Em A7 D7 G

 Repete tudo outra vez e finaliza com o solo

Solo G7 C G Em A7 D7 G (2x)

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
