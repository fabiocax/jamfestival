Luiz Gonzaga - Forró No Escuro

                 Bm
o candieiro se apagou
        C     D    G
O sanfoneiro cochilou
      Bm/C  Bm   Am
A sanfona não parou
     D7         G
E o forró continuou

                    D
Meu amor não vá simbora
Não vá simbora
                 G
Fique mais um bocadinho
Um bocadinho
                       D
Se você for seu nego chora
Seu nego chora
                      G
Vamos dançar mais um tiquinho
Mais um tiquinho

                      D
Quando eu entro numa farra
                     G
Num quero sair mais não
                    D
Vou inté quebrar a barra
                     G
E pegar o sol com a mão

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
