Novos Baianos - Acabou Chorare

A7M    E/G#        G6      D/F#
   Aca_bou chorare,  ficou tudo lindo
A7M      E/G#   G6   D/F#
   De ma_nhã ce_dinho
A7M     E/G#        G6       D/F#
   Tudo cá cá cá na fé fé fé
A7M      E/G#     G6           D/F#
   No bu bu li li, no bu bu li lindo
A7M  E/G#     G6    D/F#    E7
     No bu bu bulin_do

Bm7    E7(9)        C#m7(5-)
Talvez    pelo bura_quinho
     F#m7      Bm7  E7            A7M  Bbº
Inva_diu_me a casa, me acordou na ca___ma
Bm7   E7(9)       A7M       F#m7  Bm7
Tomou    meu cora_ção e sen_tou
E7          A7M  Bbº
   Na minha mão
Bm7    E7(9)        C#m7(5-)
Talvez    pelo bura_quinho
     F#m7      Bm7  E7            A7M  Bbº
Inva_diu_me a casa, me acordou na ca___ma
Bm7   E7(9)       A7M       F#m7  Bm7
Tomou    meu cora_ção e sen_tou
E7          A7M  E/G#
   Na minha mão
   G6         D/F#
A|_b| elha, abe_lhinha

A7M    E/G#        G6        D/F#
   Aca_bou chorare,  faz zum_zum p'reu ver
A7M    E/G#        G6      D/F#
   Faz zumzum p'ra mim
A7M       E/G#      G6         D/F#
   Abelho abelhinho escondido faz bonito
A7M      E/G#    G6   D/F#    E7
   Faz zumzum e mel

Bm7  E7(9)      C#m7(5-)     F#m7
Inda de lambuja tem o carnei_rinho
Bm7    E7       A7M  Bbº
   Pre_sente na bo___ca
Bm7  E7(9)      A7M          F#m7  Bm7
Acor_dando toda gente tão su_ave   mé
E7        A7M  Bbº
Que suave_men__te
Bm7  E7(9)      C#m7(5-)     F#m7
Inda de lambuja tem o carnei_rinho
Bm7    E7       A7M  Bbº
   Pre_sente na bo___ca
Bm7  E7(9)      A7M          F#m7  Bm7
Acor_dando toda gente tão su_ave   mé
E7        A7M  E/G#
Que suave_men__te
   G6           D/F#
Ab_elha carnei_rinho

A7M    E/G#        G6      D/F#
   Aca_bou chorare   no meio do mundo
A7M      E/G#  G6  D/F#
   Respi_rei  eu fundo
A7M       E/G#    G6      D/F#
   Foi_se tudo p'ra escan_teio
A7M    E/G#       G6         D/F#
   E o sapo na lagoa entre nessa que é boa
A7M  E/G#    G6    D/F#   
     Fiz zumzum e pronto

----------------- Acordes -----------------
A7M = X 0 2 1 2 0
Bbº = X 1 2 0 2 0
Bm7 = X 2 4 2 3 2
C#m7(5-) = X 4 5 4 5 X
D/F# = 2 X 0 2 3 2
E/G# = 4 X 2 4 5 X
E7 = 0 X 0 0 1 0
E7(9) = X X 2 1 3 2
F#m7 = 2 X 2 2 2 X
G6 = 3 X 2 4 3 X
