Novos Baianos - 29 Beijos

 Bm                                               Em7
Eu não quero, não quero mais "preocupations" comigo

 F#7                                                                    Bm
 E nem de leve águas passadas, canto e recanto de lágrimas no meu coração

Eu não quero não
  C5        G7
 Espero, espero
  Am
 Espero lhe ver, lhe encontrar
 F           G           Am
Tenho 29 beijos pra lhe dar
 F           G           Am
Tenho 29 beijos pra lhe dar
 F            G          A
 Tenho 29 beijos pra lhe dar

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C5 = X 3 5 5 X X
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
F#7 = 2 4 2 3 2 2
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
