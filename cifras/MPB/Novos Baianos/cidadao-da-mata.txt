Novos Baianos - Cidadão da Mata

E   D         E         D
Meu rancho de palha lala
E   D         E        D
Meu campo de malha lala
E           A
Um rosto na talha lala
E   D         E       D
E a chuva na calha lala

E  D           E      D   E
  Cidadão da mata... eu sou
D           E         D
Cidadão da mata... eu sou

E  D       E        D
A água na lata tata
E  D        E         D
A mesa tão farta tata
E          A
Perdido na data tata
E  D         E        D
Achei-me na mata tata

E  D           E      D   E
  Cidadão da mata... eu sou
D           E          D
Cidadão da mata... eu sou

E  D        E        D
Um touro na raia iaia
E  D        E        D
Um potro na baia iaia
E           A
A gente não vaia iaia
E    D          E        D
Qualquer um que caia iaia

E  D           E      D   E
  Cidadão da mata... eu sou
D           E          D
Cidadão da mata... eu sou

E   D           E          D
Meu cão, minha gralha lhalha
E  D       E            D
A minha pirralha lhalha
E               A
Meu pão, minha tralha lhalha
E    D         E           D
E um velho que ralha lhalha

E  D           E      D   E
  Cidadão da mata... eu sou
D           E          D
Cidadão da mata... eu sou

E   D       E        D
Meu rabo de saia iaia
E  D       E         D
Comigo na praia iaia
E             A
Mais se o dia raia iaia
E  D        E        D
A gente trabaia iaia

E  D          E       D   E
  Cidadão da mata... eu sou
D          E      D    E  D
Matador da vila... não sou
     E
Não sou

Amo, amo a mata
Porque nela não há preços
Amo o verde que me envolve
O verde sincero que me diz
Que a esperança, não é a ultima que morre
Quem morre por último é o herói
E o herói, é o cabra que não teve tempo de correr

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
