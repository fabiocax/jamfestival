Novos Baianos - Casei No Natal, Larguei No Reveillon

Intro: G F Bb F D7

G F Bb F D7
Que bom que é casar               |
No dia de natal, meu bem          | 2X
que bom poder largar              | 
que bom poder largar no reveillon |

Dm7                      Em7
Thank you very much, seu Nelson
Dm7                  Em7
Tô eu no carnaval soltinha da silva
Dm       Am       Dm
Fugi que nem passarinho
Dm      Am        G
Livre até quarta feira

(Ai, seu Daniel, o negocio agora só depois de fevereiro!)

Que bom que é casar               
No dia de natal, meu bem          
que bom poder largar              
que bom poder largar no reveillon 


Thank you very much, seu nelson
To eu no carnaval soltinha da silva
Fugi que nem passarinho
Livre até quarta feira

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
