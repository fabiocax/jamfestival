Novos Baianos - Sensibilidade da Bossa

D6/F#
À primeira vista,

      F°                    Em7
amor sempre como na(se)nsibilidade da bossa,

           A7
hoje um desbaratinador.

      D6/F#
Ainda sou o amor

       F°
e o sorriso e a flor,

            Em7
mas é que agora
                     A7
apareceu mais um espinho.

Am7                       D7(9)
Sendo o corcovado e o bondinho,
      Cm9/D              G7+
eu e você aqui neste cantinho,
       Gm7                     F#m7
protegidos por incrível que pareça
                 B7(9-)
até dos raios solares.

       Em7  A7         D6
A rua não, eu nem lhe conto.
     Em7   A7           D6      F°
Não tá, tá tá tá tá, tá tá, não tá.
    Em7        A7      D6
Manera de maneiras, manera!

  Em7    A7     D6
Não é arrego não!
    Fº   Em7   A7      D6      (A/9-)
Eu apenas tenho educação!

----------------- Acordes -----------------
A/9- = X 0 2 3 2 0
A7 = X 0 2 0 2 0
Am7 = X 0 2 0 1 0
B7(9-) = X 2 1 2 1 X
D6 = X 5 4 2 0 X
D6/F# = 2 X 0 2 0 X
D7(9) = X 5 4 5 5 X
Em7 = 0 2 2 0 3 0
F#m7 = 2 X 2 2 2 X
F° = X X 3 4 3 4
Fº = X X 3 4 3 4
G7+ = 3 X 4 4 3 X
Gm7 = 3 X 3 3 3 X
