Novos Baianos - Isso Aqui o Que É

Intro: E  C#m  F#m B7 (2x)
E  B7

 E               F°        F#m
Isso aqui, ô, ô é um pouquinho de brasil iaiá
F#m                          F#m       B7       E        Bm
Desse brasil que  canta e é feliz,   feliz,   feliz,   é
          E7              A
Também um pouco  de uma raça
          Am           E         C#m7
Que não tem medo de fumaça ai, ai
 F#m        B7     E
E não se entrega não
Olha o jeito
          E         C#m7     F#m
Nas cadeiras que ela sabe dar                (2x)
             B7                  E
Olha só o remelecho que ela sabe dar

E  C#m7   F#m           B7   E            C#m7
Morena  boa que me   faz  penar  bota a sandália
      F#m             B7       E
De  prata e  vem pro samba sambar

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
F° = X X 3 4 3 4
