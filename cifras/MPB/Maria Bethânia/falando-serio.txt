Maria Bethânia - Falando Sério


 F#7+
 Falando sério
                     G#m7          C#7/9
 É bem melhor você parar com essas  coisas
     G#m7             C#7/9      C#7/9-
 De olhar pra mim com olhos de promessas
 F#7+ G°      G#m7             C#7/9-
 De...pois  sorrir como quem nada quer
 F#7+
 Você não sabe
                   G#m7
 Mas é que tenho cicatrizes
 C#7/9   G#m7     C#7/9
 Que a  vida fez
     G#m7  C#7/9   C#7/9-
   tenho medo de fazer  planos
   F#7+        F#7
 De tentar e  sofrer outra vez
        B7+
 Falando  sério

   Bm7     E7/9                      A#m7
 Eu não queria ter você por um programa
    D#7/9-                     G#m7
 E apenas ser mais um na sua cama
 C#7/9  C#7/9-  F#7+   F#7
 Por uma noite apenas e nada mais
        B7+
 Falando  sério
   Bm7   E7/9                         A#m7
 Entre nós dois tinha de haver mais sentimento
    D#7/9-                  G#m7
 Não quero seu amor por um momento

C#7/9   C#7/9-    F#7+           F#7+
 E ter a vida inteira pra me arrepender.

----------------- Acordes -----------------
A#m7 = X 1 3 1 2 1
B7+ = X 2 4 3 4 2
Bm7 = X 2 4 2 3 2
C#7/9 = X 4 3 4 4 X
C#7/9- = X 4 3 4 3 X
D#7/9- = X 6 5 6 5 X
E7/9 = X X 2 1 3 2
F#7 = 2 4 2 3 2 2
F#7+ = 2 X 3 3 2 X
G#m7 = 4 X 4 4 4 X
G° = 3 X 2 3 2 X
