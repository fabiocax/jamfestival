Maria Bethânia - Mel

Intro: G7M

   A#°     Am7    D7                  G7M
Ó abelha rainha        faz de mim um instrumento
               E      Bm7(b5)  E                  Am7
De teu prazer, sim ,           e de tua glória
D7                   G7M                        E
Pois se é noite de completa escuridão
Am7                           Cm7
Provo do favo de teu mel
Bm                       F    E       A7         D7      G7M  G7
Cavo a direita claridade do céu  e agarro o sol com a mão
C                 Bm          E   Am            D7
É meio dia, é meia noite, é toda hora
G7M               F#m         B7
Lambe olhos, torce cabelos
Em             Dm            G7
Feticeira vamo-nos embora
C                 Bm          E
É meio dia, é meia noite
Am7                     D7
Faz zum zum na testa

              Am                    D7
Na janela, na fresca da telha
Bm                E                       Bm                 E
Pela escada, pela porta, pela estrada toda à fora
Am7            D7                       Am
Anima de vida o seio da floresta
                     D7 Bm  Bm7          E
Amor empresta         a praia deserta
                    Bm  E
Zumbe na orelha, concha do mar
Am7            D7                     Am                       D7
Ó abelha boca de mel, carmim , carnuda, vermelha
                    A#°   Am7     D7                  G7M
Ó abelha rainha        faz de mim um instrumento
               E      Bm7(b5)  E             Am7  D7
De teu prazer, sim ,           de tua glória
                  G7M
E de tua glória

----------------- Acordes -----------------
A#° = X 1 2 0 2 0
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
Bm7(b5) = X 2 3 2 3 X
C = X 3 2 0 1 0
Cm7 = X 3 5 3 4 3
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
G7 = 3 5 3 4 3 3
G7M = 3 X 4 4 3 X
