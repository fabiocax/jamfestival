Maria Bethânia - Motriz

F7M     Bb/C    F7M     Bb/C    F7M     Bb/C F7M  Cm7 F7
Embaixo a Terra e em cima o macho, o céu
Bb7M    Eb6/Bb Bb7M    Eb6/Bb  Bb7M    Eb6/Bb Bb7M    Eb6/Bb
E, entre os dois, a ideia de um sinal
C/E        Bb/D   C/E    Bb/D   C        C/E
Traçado em luz e em tudo a voz de minha mãe
Dm      C
E a minha voz na dela
Bb7M               F7M
A tarde dói de tão igual

F7M     Bb/C    F7M     Bb/C    F7M     Bb/C F7M  Cm7 F7
A tarde que atravessa o corredor
Bb7M    Eb6/Bb Bb7M    Eb6/Bb  Bb7M    Eb6/Bb Bb7M    Eb6/Bb
Que paz! Que luz que faz! Que voz! Que dor!
C/E        Bb/D   C/E    Bb/D   C        C/E
Que doce amargo cada vez que o vento traz
Dm      C            Bb7M         F7M     Bb7M
A nossa voz que chama o verde do canavial, canavial
F7M
E nós, mãe

Bb7M
Candeias, motriz

F7M     Bb/C    F7M     Bb/C    F7M     Bb/C F7M  Cm7 F7
Aquilo que eu não fiz e tanto quis
Bb7M    Eb6/Bb Bb7M    Eb6/Bb  Bb7M    Eb6/Bb Bb7M    Eb6/Bb
É tudo o que eu não sei, mas a voz diz
C/E        Bb/D   C/E    Bb/D   C        C/E
E que me faz e traz capaz de ser feliz
Dm      C          Bb7M
Pelo Céu, pela terra a tarde igual
    F7M           Bb7M
Pelo sinal, pelo sinal
F7M
E nós, mãe
Bb7M
A penha
F7M
Matriz
Bb7M
Motriz
F7M
Matriz

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bb7M = X 1 3 2 3 1
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
Dm = X X 0 2 3 1
F7M = 1 X 2 2 1 X
Gm = 3 5 5 3 3 3
