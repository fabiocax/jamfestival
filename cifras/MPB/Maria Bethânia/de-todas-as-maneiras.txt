Maria Bethânia - De Todas as Maneiras


Intro: D7+ D6 Dm6 A#7+/D

 D7+     D6  Dm6         A#7+/D  Dm7
De todas as maneiras que há de amar
 C/E          B/D#
Nós já nos amamos
      F#4/7     F#7    F#4/7       F#7
Com todas as palavras feitas pra sangrar
           Bm7+ Bm7 Bm6 Bm5+ B
Já nos cortamos
  C7+      B7      C7+       B7      Em7
Agora já passa da hora, tá lindo lá fora
              E/G#           D/A            F#7/A#
Larga a minha mão, solta as unhas do meu coração
                  F#m5-/7 B7
Que ele está apressado
    Em7               A7/9
E desanda a bater desvairado
                 D7+
Quando entra o verão

 D7+     D6  Dm6         A#7+/D  Dm7
De todas as maneiras que há de amar
 C/E        B/D#
Já nos machucamos
      F#4/7     F#7    F#4/7       F#7
Com todas as palavras feitas pra humilhar
       Bm7+ Bm7 Bm6 Bm5+ B
Nos afagamos
  C7+      B7      C7+       B7      Em7
Agora já passa da hora, tá lindo lá fora
              E/G#           D/A            F#7/A#
Larga a minha mão, solta as unhas do meu coração
                  F#m5-/7 B7
Que ele está apressado
    Em7               A7/9
E desanda a bater desvairado
                 D7+
Quando entra o verão

----------------- Acordes -----------------
A7/9 = 5 X 5 4 2 X
B = X 2 4 4 4 2
B/D# = X 6 X 4 7 7
B7 = X 2 1 2 0 2
Bm5+ = 7 10 9 7 8 7
Bm6 = 7 X 6 7 7 X
Bm7 = X 2 4 2 3 2
Bm7+ = X 2 4 3 3 2
C/E = 0 3 2 0 1 0
C7+ = X 3 2 0 0 X
D/A = X 0 X 2 3 2
D6 = X 5 4 2 0 X
D7+ = X X 0 2 2 2
Dm6 = X 5 X 4 6 5
Dm7 = X 5 7 5 6 5
E/G# = 4 X 2 4 5 X
Em7 = 0 2 2 0 3 0
F#4/7 = 2 4 2 4 2 X
F#7 = 2 4 2 3 2 2
F#7/A# = 6 X 4 6 5 X
F#m5-/7 = 2 X 2 2 1 X
