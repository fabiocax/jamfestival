Maria Bethânia - Oração da Mãe Menininha

C
Ai minha mãe
              Dm
Minha Mãe Menininha
G7
Ai minha mãe
                  C
Menininha do Gantois
                                       Dm
A estrela mais linda, hein? Tá no Gantois
                G7                       C
E o sol mais brilhante, hein? Tá no Gantois
                                    Dm
A beleza do mundo, hein? Tá no Gantois
             G7                     C
E a mão da doçura, hein? Tá no Gantois
                                     Dm
O consolo da gente, hein? Tá no Gantois
                G7                     C
E a Oxum mais bonita, hein? Tá no Gantois
               Dm
Olorum quem mandou

               G7
Essa filha de Oxum
               Dm
conta da gente
              G7
E de tudo que há
               Dm       G7
Olorum quem mandou ô ô
            C
Ora iê iê ô...
            Dm  G7
Ora iê iê ô...
            C
Ora iê iê ô...


----------------- Acordes -----------------
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
G7 = 3 5 3 4 3 3
