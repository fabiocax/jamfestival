Maria Bethânia - Sorte

             Dm       Gm            C/E
Se aquela estrela    Velar por nós
Dm             Bb7M  Gm         C/E
   Será que escuta      minha voz?
Gm       Bb7M   Dm      C
  E me ajuda atravessar
Gm           Bb7M     Dm     C
  a noite escura que vai passar...
Bb7M             Dm  Bb7M           Am
Pode estar escrito em algum lugar
Bb7M             Dm         C       Bb7M
Ou a gente escreve ao caminhar
                 Dm                    Bb7M
Onde estiver olhe para o céu
Dm               Bb7M
Essa estrela vai guiar
F    Bb7M
onde    você for
Gm         Bb7M  Dm         C
Seja o que for    tiver que ser
Gm             Bb7M      Dm       C
Olhe para o céu para agradecer

Bb7M             Dm Bb7M           Am
Pode estar escrito em algum lugar
Bb7M             Dm         C       Bb7M
Ou a gente escreve ao caminhar
Bb7M             Dm Bb7M           Am
Pode estar escrito em algum lugar
Bb7M             Dm         C       Bb7M
Ou a gente escreve ao caminhar

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb7M = X 1 3 2 3 1
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
