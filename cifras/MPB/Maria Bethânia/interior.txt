Maria Bethânia - Interior


Intro: D7+ G7+


  D7+
Maninha me mande um pouco
                  G7+
Do verde que te cerca
            A
Um pote de mel
A/G           D7+          G/A     A/G
Meu coleiro cantor, meu cachorro veludo
             D7+    D5+/7 D6/7+ D5+/7
E umas jaboticabas
 D7+                                   G7+
Maninha me traga meu pé de laranja-da-terra
Me traga também um pouco de chuva
                       F7+
Com cheiro de terra molhada
 Em            F#m
Um gosto de comida caseira

 G7+             F#m
Um retrato das crianças
 G7+                    F7+
E não se esqueça de me dizer
 A7                    D6/7+
Como vai indo minha madrinha
            Bb7+               C7+
E não se esqueça de uma reza forte
             D6/7+/9
Contra mau-olhado

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/G = 3 X 2 2 2 X
A7 = X 0 2 0 2 0
Bb7+ = X 1 3 2 3 1
C7+ = X 3 2 0 0 X
D5+/7 = X 5 X 5 7 6
D6/7+ = 10 9 9 11 10 9
D6/7+/9 = 10 9 9 9 10 9
D7+ = X X 0 2 2 2
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
F7+ = 1 X 2 2 1 X
G/A = 5 X 5 4 3 X
G7+ = 3 X 4 4 3 X
