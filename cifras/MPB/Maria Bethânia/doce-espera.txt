Maria Bethânia - Doce Espera

E      Am6/E   E    Am6/E
A vida é ......dura
E          Am6/E         E     Am6/E
Quando se espera ....alguém
A       A7+    A7
A vida  é     fria
A             A7+      A7
Quando esse alguém não vem
             E7
Oh! Dura vida
Am6/E    E      Am6/E    E
Espera fria
         Am6/E    E    Am6/E   E
Só te queria um tempo
          Am6/E   E
Um tempo até gastar
A       A7+    A7
Toda tesão da vida
A       A7+    A7
Depois podia até parar
          E7
Tesão da vida

Am6/E   E
Doce espera

F7      Bbm6/F  F7   Bbm6/F
A vida   é     dura
   F7      Bbm6/F  F7   Bbm6/F
Quando se espera alguém
Bb     Bb7+   Bb7
A vida  é    fria
Bb        Bb7+         Bb7
Quando esse alguém não vem
            F7      Bbm6/F
Oh! Dura vida
           F7   Bbm6/F
Espera fria
F7     Bbm6/F   F7   Bbm6/F
Só te queria um tempo
F7         Bbm6/F
Um tempo até gastar
Bb      Bb7+   Bb7
Toda tesão da vida
Bb      Bb7+       Bb7
Depois podia até parar

Tesão da vida

Doce espera

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
Am6/E = X X 2 2 1 2
Bb = X 1 3 3 3 1
Bb7 = X 1 3 1 3 1
Bb7+ = X 1 3 2 3 1
Bbm6/F = X X 3 3 2 3
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F7 = 1 3 1 2 1 1
