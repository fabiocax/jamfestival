Maria Bethânia - Alteza

E7+/9                  A7+
Quando meu homem foi embora
E7+/9                      A7+
Soltou aos quatro ventos um recado
E7+/9               A7+
Que meu trono era manchado
E7+/9            A7+
E meu reino esfiapado
F#m7                 B7
Sou uma rainha que voluntariamente
C#m
Abdiquei cetro e coroa
F#m7
E que me entrego e me dou
B7               C#m7
Inteiramente ao que sou
F#m7                      Am      G#m7  C#7/9
A vida nômade que no meu sangue ecoa
F#7              A7
Abro a porta do carro fissurada
F#7               A7
Toma-me ao mundo cigano e sou puxada

F#m7    B7
Por um torvelinho

(E7+/9             A7+)
Abraça a todos os lugares
Chamam por mim os bares poeirentos
E eu espreito da calçada
Se meu amor bebe por lá
Como me atraem os colares de luzes
À beira do caminho
Errante, pego o volante
E faço nele o meu ninho
Pistas de meu homem aqui e ali rastreio
Parto pra súbitas, inéditas, paisagens
Acendo alto o meu farol de milha
Em cada uma das cidades por que passo
Seu nome escuto na trilha

Aldeia da Ajuda, Viçosa
Porto Seguro, Guarapari, Prado
Itagi, Belmonte, Prado
Jequié, Trancoso, Prado
Meu homem no meu coração
Eu carrego com todo cuidado
Partiu sem me deixar nem caixa-postal, direção
Chego a um lugar e ele já levantou a tenda
Meu Deus! Será que eu caí num laço
Caí numa armadilha, uma cilada
E que este amor que toda me espraiou
Não passou de uma lenda

Pois quando chego num lugar
Dali ele já levantou a tenda
A tenda, a tenda, a tenda, a tenda

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
C#7/9 = X 4 3 4 4 X
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
E7+/9 = X 7 6 8 7 X
F#7 = 2 4 2 3 2 2
F#m7 = 2 X 2 2 2 X
G#m7 = 4 X 4 4 4 X
