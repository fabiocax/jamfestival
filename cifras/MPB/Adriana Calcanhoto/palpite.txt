Adriana Calcanhotto - Palpite

[Intro]

E|--------------------------------|
B|--------------------------------|
G|--------------------------------|
D|--------------------------------|
A|-2h4-2--------------------------| 
E|------4-2-4-2-0-----------------|

      E
E|--------------------------------|
B|--------------------------------|
G|--------------------------------|
D|--------------------------------|
A|-2h4-2--------------------------|
E|------4-2-4-2-2-----------------|

      F#/E
E|--------------------------------|
B|--------------------------------|
G|--------------------------------|
D|--------------------------------|
A|--------------------------------|
E|-6-4-6--------------------------|


      A9
E|--------------------------------|
B|--------------------------------|
G|--------------------------------|
D|-2h4-2--------------------------|
A|-------4-2-4-2-2----------------|
E|--------------------------------|

[Solo]

E|-7---------------------5-4------------|
B|---8b-5-------5/7----------5/7-7------|
G|-------4/1-3------5-7------------4----|
D|--------------------------------------|
A|--------------------------------------|
E|--------------------------------------|

E                                       F#/E
 Tô com saudade de você Debaixo do meu cobertor
A9                 Am            E
 E te arrancar suspiros, fazer amor

 E                           F#/E
Tô com saudade de você Na varanda em noite quente
A9             Am            C#m          A
 E o arrepio frio que dá na gente Truque do desejo
           C#m             A
Guardo na boca o gosto do beijo
     C       D          C            D
 Eu sinto a falta de você  me sinto só
   E                 F#/E                A9   Am      E
E aí, será que você volta? Tudo à minha volta     é triste
   E                F#/E        A9         Am     E
E aí, o Amor pode acontecer de novo pra você, Palpite

E                                 F#/E
 Tô com saudade de você do nosso banho de chuva
 A9                Am              E
Do calor na minha pele, da língua tua

 E                           F#/E
Tô com saudade de você Censurando o meu vestido
A9             Am           C#m                A
 As juras de Amor  ao pé do ouvido, truque do desejo
           C#m             A
Guardo na boca o gosto do beijo
C
 Eu sinto a falta de você
             D
   Me sinto só

C
 Eu sinto a falta de você
             D
   Me sinto só

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#/E = X X 2 3 2 2
