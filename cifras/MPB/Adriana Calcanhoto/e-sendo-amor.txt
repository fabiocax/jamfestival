Adriana Calcanhotto - E Sendo Amor

[Intro] C#m  Fm  A7(b13)
        C#m  Fm  A7(b13)

C#m         Fm   C#m         Fm
Você não quis,     não deu valor
C#m         Fm     A7(b13)
E sendo amor de verdade
C#m         Fm   C#m        Fm
Desperdiçou sexo do bom
C#m         Fm
Meu próprio som
A7(b13)      C#m         Fm
E silêncio e outras raridades

C#m         Fm   C#m         Fm
Que faço eu?        Aonde vou?
C#m         Fm        A7(b13)
Com a dor que me reparte
C#m      Fm   C#m         Fm
Aonde por?     Pra quem eu dou?
C#m         Fm         A7(b13)
A flor que em flor se debate


C#m         Fm  C#m         Gm
Você não diz,    mas balançou
C#m         Fm      A7(b13)
Qual a razão e a vontade
C#m         Fm C#m         Fm
Desperdiçou sexo do bom
C#m         Fm
Meu próprio som
C#m     Fm C#m         Fm
E silêncio e outras raridades

C#m       Fm  C#m         Fm
Que faço eu?  Aonde vou?
C#m         Fm      A7(b13)
Com a dor que me reparte
C#m       Fm  C#m         Fm
Aonde por? Pra quem eu dou?
C#m         Fm      A7(b13)
A flor que em flor se debate
C#m       Fm  C#m         Fm
O que doeu levo onde vou
C#m       Fm      A7(b13)
Sendo que sou a metade
C#m       Fm  C#m         Fm
O que doeu levo onde for
C#m       Fm      A7(b13)
Feito feroz tatuagem

( C#m  Fm  A7(b13) )
( C#m  Fm  A7(b13) )

----------------- Acordes -----------------
A7(b13) = X 0 X 0 2 1
C#m = X 4 6 6 5 4
Fm = 1 3 3 1 1 1
Gm = 3 5 5 3 3 3
