Adriana Calcanhotto - Maresia

Intro:] E6/9   Bbm7(11) Eb7   Ab7+ C#7/9   F#7+ B7(13)

                 E6/9
O meu amor me deixou
                 Bbm7(11)
Levou minha identidade
Eb7                     Ab7+  C#7/9
Não sei mais bem onde estou
                F#7+
Nem onde a realidade


Bb7/9               Eb7+
Ah, se eu fosse marinheiro
                     Ab7+
Era eu quem tinha partido
                  F7/9
Mas meu coração ligeiro
                Bb7(13)   B7(13)
Não se teria partido



                E6/9
Ou se partisse colava
              Bbm7(11)
Com cola de maresia
Eb7           Ab7+    C#7/9
Eu amava e desamava
                 F#7+
Sem peso e com poesia


Bb7/9                Eb7+
Ah, se eu fosse marinheiro
               Ab7+
Seria doce meu lar
                  F7/9
Não só o Rio de Janeiro
               Bb7(13)   B7(13)
A imensidão e o mar


           E6/9
Leste oeste norte sul
                  Bbm7(11)
Onde um homem se situa
Eb7                   Ab7+    C#7/9
Quando o sol sobre o azul
                   F#7+    Bb7/9
Ou quando no mar a lua

                Eb7+                 Ab7+
Não buscaria conforto nem juntaria dinheiro
Abm7               Eb7+
Um amor em cada porto
Bb7/9                Eb7+
Ah, se eu fosse marinheiro
                  Ab7+
Nem pensaria em dinheiro
Abm7              Eb7+
Um amor em cada porto
Bb7/9                Eb7+    Ab7+ Abm7   Eb7+ Bb7/9
Bb7(13) B7(13)
Ah, se eu fosse marinheiro


Repete tudo desde o início

----------------- Acordes -----------------
Ab7+ = 4 X 5 5 4 X
Abm7 = 4 X 4 4 4 X
B7(13) = X 2 X 2 4 4
Bb7(13) = 6 X 6 7 8 X
Bb7/9 = X 1 0 1 1 X
Bbm7(11) = 6 X 6 6 4 X
C#7/9 = X 4 3 4 4 X
E6/9 = X 7 6 6 7 7
Eb7 = X 6 5 6 4 X
Eb7+ = X X 1 3 3 3
F#7+ = 2 X 3 3 2 X
F7/9 = X X 3 2 4 3
