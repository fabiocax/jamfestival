Adriana Calcanhotto - Lig-lig-lig-lé

  Abm6           F#7  E7 Eb7
 Lig  -  lig  -  lig  -  lé
  Abm6           F#7  E7 Eb7
 Lig  -  lig  -  lig  -  lé

 C#m7 Abm Eb7 Abm6

  Abm6           F#7  E7 Eb7
 Lig  -  lig  -  lig  -  lé
  Abm6           F#7  E7 Eb7
 Lig  -  lig  -  lig  -  lé

 C#m7 Abm Eb7 Abm6

 Lá vem o seu china na ponta do pé!

 Lig-lig-lig-lig-lig-lig-lé! 

 Dez tões, vinte pratos, banana e café!
 Eb7                     Abm6  Eb7/9
 Lig-lig-lig-lig-lig-lig-lé


  Abm6
 Lá vem o seu china na ponta do pé!
  Abm6
 Lig-lig-lig-lig-lig-lig-lé!

 Dez tões, vinte pratos, banana e café!
 Eb7                     Abm6  Eb7
 Lig-lig-lig-lig-lig-lig-lé

    Ab                         F/A Bbm7
 Chinês, come somente uma vez por mês
       Bbm7          Eb7              Ab      Ab7
 Não vai mais a Shangai buscar a "baterflay"!
  Db6                     D°   Ab/Db
 Aqui, com a morena fez a sua fé!
  Bbm7   Eb4   Abm6
 Lig-lig-lig-lé!

  Abm6
 Lá vem o seu china na ponta do pé!
  Abm6
 Lig-lig-lig-lig-lig-lig-lé!

 Dez tões, vinte pratos, banana e café!
 Eb7                     Abm6  Eb7
 Lig-lig-lig-lig-lig-lig-lé

    Ab                         F/A Bbm7
 Chinês, come somente uma vez por mês
       Bbm7          Eb7              Ab      Ab7
 Não vai mais a Shangai buscar a "baterflay"!
  Db6                     D°  Ab/Db
 Aqui, com a morena fez a sua fé!
 Bbm7    Dbm Eb7 Ab6
 Lig-lig-lig -   lé
  Ab6   A6        Ab6
 Lé-lé  Lé-lé  Lé-lé-lé

----------------- Acordes -----------------
A6 = 5 X 4 6 5 X
Ab = 4 3 1 1 1 4
Ab/Db = X 4 6 5 4 4
Ab6 = 4 X 3 5 4 X
Ab7 = 4 6 4 5 4 4
Abm = 4 6 6 4 4 4
Abm6 = 4 X 3 4 4 X
Bbm7 = X 1 3 1 2 1
C#m7 = X 4 6 4 5 4
Db6 = 9 X 8 10 9 X
Dbm = X 4 6 6 5 4
D° = X X 0 1 0 1
E7 = 0 2 2 1 3 0
Eb4 = X 6 8 8 9 6
Eb7 = X 6 5 6 4 X
Eb7/9 = X 6 5 6 6 X
F#7 = 2 4 2 3 2 2
F/A = 5 X 3 5 6 X
