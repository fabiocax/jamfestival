Adriana Calcanhotto - Medo de Amar N° 3

D
Você diz que eu te assusto
F#m
Você diz que eu te desvio
C
Também diz que eu sou um bruto
G
E me chama de vadio

D
Você diz que eu te desprezo
F#m
Que eu me comporto muito mal
C
Também diz que eu nunca rezo
G
Ainda me chama de animal

          D
Você não tem medo de mim
          E
Você não tem medo de mim

          C
Você tem medo, é do amor
          G
Que você guarda para mim
          D
Você não tem medo de mim
          E
Você não tem medo de mim
          C
Você tem medo, é de você
          G
Você tem medo, é de querer

D
Você diz que eu sou demente
F#m
Que eu não tenho salvação
C
Você diz que simplesmente
G
Sou carente de razão

D
Você diz que eu te envergonho
F#m
Também diz que eu sou cruel
C
Que no teatro do teu sonho
G
Para mim não tem papel

          D
Você não tem medo de mim
          E
Você não tem medo de mim
          C
Você tem medo, é do amor
          G
Que você guarda para mim
          D
Você não tem medo de mim
          E
Você não tem medo de mim
          C
Você tem medo, é de você
          G
Você tem medo, é de querer
       D
Me amar

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
