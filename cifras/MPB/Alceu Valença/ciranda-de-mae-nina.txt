Alceu Valença - Ciranda de Mãe Nina

 B    Bb     Ebm
Foi numa ciranda
G#m    F#m        B
Na estrada de Paulista
C#m                 B
Lá pras bandas de Olinda
C#m              F#m  G#
Laço branco no cabelo
              C#m  G#
Da menina Severina
              C#m
Da menina Severina

C#m
Navegando no seu corpo
                   C#7
Emoldurado pela chita
         F#m B     E    A
Com seu corpo navegando
      F#m   G#      C#  C#7
No terreiro de Mãe Nina

         F#m B     E    A
Com seu corpo navegando
      F#m   G#      C#  C#7
No terreiro de Mãe Nina

B         Bm  A         B  E
Ô cirandeiro, ô cirandeiro

      F#            B
Onde anda aquela estrada
A                G#m
De Olinda ou Paulista?
      C#        F#m
Onde laço onde fita
       B          E
Laço branco da menina?
        A         G#
Onde a voz do cirandeiro?
                C#
Veste chita Severina?
      C#7       F#m
Onde anda meu irmão?
                E
A ciranda se acabando
                      A
O sol mordendo a madrugada
                    F#m
No compasso da lembrança
                G#
Eu aqui sem esperança
                    C#  C#7 (F#m B E A F#m G# C# C#7)
Sem ciranda ou cirandá

          F#m
Vim do Recife
      B            E
Um rapaz me perguntou
        A            F#m
Se na ciranda que eu vou
      G#          C#    C#7
Se tinha muitas morenas
          F#m
Eu disse tem
        B          E
Muitas morenas, mulata
        A          F#m
Dessas que a morte mata
    G#             C#
E depois chora com pena

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
Bb = X 1 3 3 3 1
Bm = X 2 4 4 3 2
C# = X 4 6 6 6 4
C#7 = X 4 3 4 2 X
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
Ebm = X X 1 3 4 2
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
G# = 4 3 1 1 1 4
G#m = 4 6 6 4 4 4
