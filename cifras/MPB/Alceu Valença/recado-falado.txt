Alceu Valença - Recado Falado

Intro: A C A G
 G        D                 Em7       D        A
Haverá sempre, sempre entre nós esse digo não digo
 G        D          Em7     D       A
Esse T de tensão, esse A de amor ressentido
           C      A           C (Cº) A
Qualquer coisa no ar, esse desassossego (2x)
G          D         Em7       D            A
Um recado falado, um bilhete guardado, um segredo
G             D          Em7      D         A
Um desejo no lábio, um carinho travado, um azedo
           C      A           C (Cº) A
Qualquer coisa no ar, esse desassossego (2x)
G             D      Em7     D        A
No metrô da saudade seremos fiéis passageiros
G            D         Em7       D           A
Um agosto molhado, um dezembro passado, um janeiro
           C      A           C (Cº) A
Qualquer coisa no ar, esse desassossego (2x)
G           D            Em7       D         A
Cessará finalmente entre nós esse mito, não minto

G           D          Em7   D      A
Esse I de ilusão, esse T de tesão infinito
           C      A           C (Cº) A
Qualquer coisa no ar, esse desassossego (2x)

----------------- Acordes -----------------
A = X 0 2 2 2 0
C = X 3 2 0 1 0
Cº = X 3 4 2 4 2
D = X X 0 2 3 2
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
