Alceu Valença - Baião

E
EU VOU MOSTRAR PRÁ VOCES
COMO SE DANÇA O BAIÃO
QUEM QUISER APRENDER
                  A7
FAVOR PRESTAR ATENÇÃO
MORENA CHEGUE PRÁ CÁ
BEM JUNTO AO MEU CORAÇÃO
                D
AGORA É SÓ ME SEGUIR
           B7          E
QUE EU VOU DANÇAR UM BAIÃO
                  A
EU JÁ DANCEI BALANCÊ
XAMEGO, SAMBA E XERÉM
MAS O BAIÃO TEM UM QUE
                         Em
QUE AS OUTRAS DANÇAS NÃO TEM
                   A7
QUEM QUISER É SÓ DIZER
                    D
POIS EU COM SATISFAÇÃO

       B7             E
VOU DANÇAR CANTANDO BAIÃO
                   A
EU JÁ DANCEI NO PARÁ
TOQUEI SANFONA EM BELÉM
CANTEI LÁ NO CEARÁ
                  Em
E SEI O QUE ME CONVÉM
                     A7
E QUEM QUISER É SÓ DIZER
                   D
POIS EU COM SATISFAÇÃO
       B7             E
VOU DANÇAR CANTANDO BAIÃO

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
