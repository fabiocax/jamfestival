Alceu Valença - Como Dois Animais

[Intro]  G   Dm  G   Dm   G  Dm  G   Dm

G                                       F          G
    Uma moça bonita de olhar agateado
                                  F                G        F
Deixou em pedaços      meu coração
G                                          F       G
   Uma onça pintada e seu tiro certeiro
                                              F          G     F
Deixou os meus nervos de aço no chão

G                                      F          G
   Uma moça bonita de olhar agateado
                                  F                G        F
Deixou em pedaços      meu coração
G                                          F       G
   Uma onça pintada e seu tiro certeiro
                                              F          G    F
Deixou os meus nervos de aço no chão

   Em                                                    Bm
          Foi mistério e segredo e muito mais

                       Em                   Bm
Foi divino brinquedo e muito mais
                           C          G        F
Se amar como doIs   animais

  Em                                                    Bm
          Foi mistério e segredo e muito mais
                       Em                   Bm
Foi divino brinquedo e muito mais
                           C          G        F
Se amar como doIs   animais

G                                                 F         G
    Meu olhar vagabundo de cachorro vadio
                                          F           G    F
Olhava a pintada e ela estava no cio
G                                                      F        G
    Era um cão vagabundo e uma onça pintada
                                         F            G     F
Se amando na praça como os animais

G                                                 F         G
    Meu olhar vagabundo de cachorro vadio
                                          F           G    F
Olhava a pintada e ela estava no cio
G                                                      F        G
    Era um cão vagabundo e uma onça pintada
                                         F            G
Se amando na praça como os animais
                                         F            G
Se amando na praça como os animais
                                         F            G
Se amando na praça como os animais
                F  G                F  G                 F   G
carnaval          carnaval          carnaval

----------------- Acordes -----------------
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
