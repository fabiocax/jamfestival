Alceu Valença - Mister Mistério

(intro)  F Bb

 Bb            F
Mister Mistério
 Bb             F
Mister Mistério sou
 Bb             C
Mister Mistério sou
                   F
É sou, eu já estou

 Bb            F
Mister Mistério
 Bb             F
Mister Mistérioso
 Bb             C
Mister Mistério sou
                   F
É sou, eu já estou

E
Esconder o crime


E disfarçar a dor
D           G
Ex, ex, 'excândalo’
D           G
Ex, ex, 'excândalo’
E   F                   E
Escapada de carreira maneira
     F    G  F
Escapada

 Bb            F
Mister Mistério
 Bb             F
Mister Mistérioso
 Bb             C
Mister Mistério sou
                   F
É sou, eu já estou

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
