Alceu Valença - Dança Das Borboletas

(intro) G

(violão) G
G|---0-0---0-0---0-0---0-0-|
D|-2-----4-----5-----4-----|

E|-12-12-14-14-13-13-12-12-10-10-7-5-7-|

G
As borboletas estão voando

(riff 1)
B|-8-8-7-7-5-5-3-3-------|
G|-----------------9-7-9-|

G                            (riff 1)
A dança louca das borboletas
G                           (riff 1)
As borboletas estão girando
G                           (riff 1)
Estão virando a sua cabeça

G
Olhe as borboletas estão invadindo

Os apartamentos, cinemas e bares

Esgotos e rios e lagos e mares

Em um rodopio de arrepiar

(riff 2)
E|-----------------------10-9-------|
B|---8---12---10-8---8--------12----|
G|-9---9----9------9---9---------12-|

G
E derrubam janelas e portas de vidro
Escadas rolantes e das chaminés
Mergulham e giram num véu de fumaça
E é como um arco-íris no centro do céu

(riff 3)
E|-----------------------10-9-------|-----------------12-12-14-14-13-13-12-12-10-10-7-5---|
B|---8---12---10-8---8--------12----|---8---12---10-8-----------------------------------12|
G|-9---9----9------9---9---------12-|-9---9----9------------------------------------------|

B|-12-10-8-12-10-8-15-14-12----|
G|--------------------------12-|

----------------- Acordes -----------------
G = 3 2 0 0 0 3
