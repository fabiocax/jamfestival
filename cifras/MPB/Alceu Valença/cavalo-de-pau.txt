Alceu Valença - Cavalo-de-Pau

        Gm         C7     Gm
De puro éter assoprava o vento
                     C7   Gm
Formando ondas pelo milharal
                    C7    Gm
Teu pelo claro boneca dourada
                      C7    Gm
Meu pelo escuro, cavalo-de-pau
        Gm         C7     Gm
De puro éter assoprava o vento
                     C7   Gm
Formando ondas pelo milharal
                    C7    Gm
Teu pelo claro boneca dourada
                      C7    Gm
Meu pelo escuro, cavalo-de-pau

        Bb          Eb    Gm
Cavalo doido por onde trafegas
               Bb           Eb  Gm
Depois que eu vim parar na capital

         Cm          D/F#   Gm
Me derrubaste como quem me nega
        Eb         Cm    Gm
Cavalo doido, cavalo-de-pau
        Eb         Cm    Gm
Cavalo doido, cavalo-de-pau
        Eb         Cm    Gm
Cavalo doido, cavalo-de-pau

        Bb          Eb    Gm
Cavalo doido em sonho me levas
            Bb            Eb   Gm
Teu nome é tempo, vento, vendaval
         Cm          D/F#   Gm
Me derrubaste como quem me nega
        Eb         Cm    Gm
Cavalo doido, cavalo-de-pau
        Eb         Cm    Gm
Cavalo doido, cavalo-de-pau
        Eb         Cm    Gm
Cavalo doido, cavalo-de-pau

        Gm         C7     Gm
De puro éter assoprava o vento
                     C7   Gm
Formando ondas pelo milharal
                    C7    Gm
Teu pelo claro boneca dourada
                      C7    Gm
Meu pelo escuro, cavalo-de-pau
        Gm         C7     Gm
De puro éter assoprava o vento
                     C7   Gm
Formando ondas pelo milharal
                    C7    Gm
Teu pelo claro boneca dourada
                      C7    Gm
Meu pelo escuro, cavalo-de-pau

        Bb          Eb    Gm
Cavalo doido por onde trafegas
               Bb           Eb  Gm
Depois que eu vim parar na capital
         Cm          D/F#   Gm
Me derrubaste como quem me nega
        Eb         Cm    Gm
Cavalo doido, cavalo-de-pau
        Eb         Cm    Gm
Cavalo doido, cavalo-de-pau
        Eb         Cm    Gm
Cavalo doido, cavalo-de-pau

        Bb          Eb    Gm
Cavalo doido em sonho me levas
            Bb            Eb   Gm
Teu nome é tempo, vento, vendaval
         Cm          D/F#   Gm
Me derrubaste como quem me nega
        Eb         Cm    Gm
Cavalo doido, cavalo-de-pau
        Eb         Cm    Gm
Cavalo doido, cavalo-de-pau
        Eb         Cm    Gm
Cavalo doido, cavalo-de-pau

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
D/F# = 2 X 0 2 3 2
Eb = X 6 5 3 4 3
Gm = 3 5 5 3 3 3
