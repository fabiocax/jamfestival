Alceu Valença - Petrolina Juazeiro

[Intro] Gm  Dm  A7  Dm
        Gm  Dm  A7  Dm
        C  A#  A7

Dm                      A7                Dm
  Na margem do São Francisco, nasceu a beleza
         A7             Dm
E a natureza ela conservou

   D7       Gm          C7    F
Jesus abençoou com sua mão divina
         Bb           Gm           A7
Pra não morrer de saudade, vou voltar
          Dm
Pra Petrolina
   D7       Gm          C7    F
Jesus abençoou com sua mão divina
         Bb           Gm           A7
Pra não morrer de saudade, vou voltar
          Dm
Pra Petrolina


                  A7            Dm
Do outro lado do rio tem uma cidade
                  A7                    Dm
Que em minha mocidade eu visitava todo dia

   D7          Gm          C7   F
Atravessava a ponte ai que alegria
    Bb         Gm        A7         Dm
Chegava em Juazeiro, Juazeiro da Bahia
   D7          Gm          C7   F
Atravessava a ponte ai que alegria
    Bb         Gm        A7         Dm
Chegava em Juazeiro, Juazeiro da Bahia

            Gm
Hoje eu me lembro que nos tempos de
   Dm
Criança
                    A7                  Dm
Esquisito era a carranca e o apito do trem
 D7         Gm             C7         F
Mas achava lindo quando a ponte levantava
  Bb          Gm          A7         Dm  D7
E o vapor passava num gostoso vai e vem
      Gm  C7   F   Bb    Gm   A7     Dm   D7
Petrolina, Juazeiro, Juazeiro, Petrolina
       Gm     C7              F
Todas duas eu acho uma coisa linda
    Bb          Gm       A7        Dm
Eu gosto de Juazeiro e adoro Petrolina

                Gm       A7        Dm
Eu gosto de Juazeiro e adoro Petrolina
                Gm       A7        Dm
Eu gosto de Juazeiro e adoro Petrolina
                Gm       A7        Dm
Eu gosto de Juazeiro e adoro Petrolina
                Gm       A7        Dm
Eu gosto de Juazeiro e adoro Petrolina

( Gm  Dm  A7  Dm )
( Gm  Dm  A7  Dm )
( C  A#  A7 )

Dm                      A7                Dm
  Na margem do São Francisco, nasceu a beleza
         A7             Dm
E a natureza ela conservou

   D7       Gm          C7    F
Jesus abençoou com sua mão divina
         Bb           Gm           A7
Pra não morrer de saudade, vou voltar
          Dm
Pra Petrolina
   D7       Gm          C7    F
Jesus abençoou com sua mão divina
         Bb           Gm           A7
Pra não morrer de saudade, vou voltar
          Dm
Pra Petrolina

                  A7            Dm
Do outro lado do rio tem uma cidade
                  A7                    Dm
Que em minha mocidade eu visitava todo dia

   D7          Gm          C7   F
Atravessava a ponte ai que alegria
    Bb         Gm        A7         Dm
Chegava em Juazeiro, Juazeiro da Bahia
   D7          Gm          C7   F
Atravessava a ponte ai que alegria
    Bb         Gm        A7         Dm
Chegava em Juazeiro, Juazeiro da Bahia

            Gm
Hoje eu me lembro que nos tempos de
   Dm
Criança
                    A7                  Dm
Esquisito era a carranca e o apito do trem
 D7         Gm             C7         F
Mas achava lindo quando a ponte levantava
  Bb          Gm          A7         Dm  D7
E o vapor passava num gostoso vai e vem

      Gm  C7   F   Bb    Gm   A7     Dm   D7
Petrolina, Juazeiro, Juazeiro, Petrolina
       Gm     C7              F
Todas duas eu acho uma coisa linda
    Bb          Gm       A7        Dm
Eu gosto de Juazeiro e adoro Petrolina

                Gm       A7        Dm
Eu gosto de Juazeiro e adoro Petrolina
                Gm       A7        Dm
Eu gosto de Juazeiro e adoro Petrolina
                Gm       A7        Dm
Eu gosto de Juazeiro e adoro Petrolina
                Gm       A7        Dm
Eu gosto de Juazeiro e adoro Petrolina

----------------- Acordes -----------------
A# = X 1 3 3 3 1
A7 = X 0 2 0 2 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
