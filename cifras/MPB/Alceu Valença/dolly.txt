Alceu Valença - Dolly

Intro: ( A9  A9/F A9/F# A9/F)
                 ( A9  A9/F A9/F# A9/F)
                 Corra ponha os pés nessa estrada
                 Que não vai dar em nada
                 Que não adianta fingir
                 Seja boba. bem debochada
                 Uma pessoa clonada ovelha Dolly, dolly
                 ( A9  A9/F A9/F# A9/F)
                 Corra ponha os pés nessa estrada
                 Que não vai dar em nada
                 Que não adianta fingir
                 Seja tola, mal educada
                 Uma pessoa gelada ovelha Dolly, dolly
                     F#m                 C#m
                 Andar, andar, fugir assim
                     F#m                 C#m
                 Andar, andar, volta pra mim

                 D7                 C#m
                 Seja boba, bem debochada
                 Bm                   F#m
                 Diga que chegamos ao fim

                 D7                      C#m
                 Fale que vai bem, muito amada
                                   Bm
                 E vive um conto de fadas
                         E               A
                 Nem se lembra mais de mim
                 (voltar)

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
A9/F = 1 0 2 2 0 0
A9/F# = 2 X 2 2 0 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
