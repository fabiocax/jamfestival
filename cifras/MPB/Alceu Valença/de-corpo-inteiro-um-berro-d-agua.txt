Alceu Valença - De Corpo Inteiro (Um Berro D'Água)

G
Sonhamos água
                    2X
Sonhos de peixe
          C
Pra me deixar assim (Pra nos deixar assim)
  F        Bb
Depois com sede
           C
De corpo inteiro
         F              G      C Am G(2X)
Um berro d'água, tantas vezes

G
Ruas desertas
               2X
Velhas paredes

           C
Desenho um verso
         F      Bb
E um coração vermelho

           C
De corpo inteiro
         F              G       C Am G(2X)
Um berro d água, tantas vezes

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
