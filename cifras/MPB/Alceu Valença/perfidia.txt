Alceu Valença - Perfídia

     C  Am  Dm
Te amei
   G              C       Am    Dm
Como ninguém te amou, querida
    G          C        Am
De ti o menor gesto adorei
      Dm              E   G
Esquecido da própria vida
    C   Am  Dm
perfídia
    G                 C        Am  Dm
Mandaste embora e eu não esqueci
     G             C       Am     Dm
Das rosas, das orquídeas, das violetas
               E
Que eu dava à ti
      F
Distraída no ambiente luxuoso
                E
Em tu sempre vivias
       F
Tu deixaste que murchastes minhas flores

                   E   G
Meu buquê de fantasias
    C   Am Dm
E agora
      G        C        Am  Dm
Que adoras à quem te magoas
    G         C          Am
Perdoas pelo bem que te fiz
    Dm       G     C
Perdoas e serás feliz

      F
Distraída no ambiente luxuoso
                E
Em tu sempre vivias
       F
Tu deixaste que murchastes minhas flores
                   E   G
Meu buquê de fantasias
    C   Am Dm
E agora
      G        C        Am  Dm
Que adoras à quem te magoas
    G         C          Am
Perdoas pelo bem que te fiz
    Dm       G     C
Perdoas e serás feliz

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
