Alceu Valença - Sete Desejos

Intro: D9 A/D (5x)
         C/D Em/D Dm5+9 D9 G/D A/D
   D9             A/D
Recomeçando das cinzas
    C/D              Em/D
Eu faço versos tão claros
   Dm5+9        D9
Projeto sete desejos
      G/D  A/D    D9
Na fumaça do cigarro
              A/D
Eu penso na blusa branca
    C/D                 Em/D
De renda, que dei pra ela
    Dm5+9         D9
Na curva de suas ancas
  G/D         A/D     D9  A/D D9 A/D
Quando escanchada na sela
            D9           A/D
Lembro um flamboyant vermelho
     C/D           Em/D
No desmantelo da tarde

   Dm5+9           D9
A mala azul, arrumada
 G/D      A/D     D9
Que projetava a viagem
       A/D
Recomeçando das cinzas
C/D                 Em/D
Vou recompondo a paisagem
  Dm5+9                 D9
Lembro um flamboyant vermelho
 G/D       A/D     D9  A/D D9 A/D
No desmantelo da tarde
    D9             A/D
E agora penso na réstia
    C/D         Em/D
Daquela luz amarela
         Dm5+9      D9
Que escorria do telhado
        G/D      A/D   D9
Para dourar os olhos dela
       A/D
Recomeçando das cinzas
C/D                 Em/D
Vou renascendo pra ela
    Dm5+9          D9
E agora penso na réstia
  G/D    A/D     D9
Daquela luz amarela
    D9               A/D
E agora penso na estrada
                    Em/D
Da vida, tem ida e volta
         Dm5+9      D9
Ninguém foge ao destino
       G/D      A/D       D9  A/D D9 A/D
Esse trem que nos transporta

----------------- Acordes -----------------
A/D = X X 0 6 5 5
C/D = X X 0 0 1 0
D9 = X X 0 2 3 0
Em/D = X X 0 4 5 3
G/D = X 5 5 4 3 X
