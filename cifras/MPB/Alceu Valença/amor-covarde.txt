Alceu Valença - Amor Covarde

Intro 3x: G# Gm Dm7 G# Gm Dm7

        Fm7       Gm7        Dm7
Amor covarde que morde que arde
      Fm7       Gm7     Dm7
Capela magra metade de mim
        D#7/9                   Bb7M
A madrugada se arrasta tão lenta assim
G# Gm Dm G# Gm Dm G# Gm Dm7
Dor dor dor dor dor dor
        Fm7   Gm7      Dm7
Moça bonita novilha tão rara
              Fm7 Gm7      Dm7
Não há quem valha metade de mim
        D#7/9            Bb7M
A dor do amor navalhada que arde assim
G# Gm Dm G# Gm Dm G# Gm Dm7
Dor dor dor dor dor dor (bis)
    Gm                    Dm
Estrela d'alva pedaço de lua
     Gm                  Dm
A pele nua cheirando a jasmim

      D#7/9             Bb7M
Rubra cereja bandeja de prata dói hum!
G# Gm Dm G# Gm Dm G# Gm Dm
Dor dor dor dor dor dor
       Fm7 Gm7    Dm7
Moça bonita novilha tão rara
             Fm7 Gm7      Dm7
Não ha quem valha metade de mim
        D#7/9                      Bb7M
Nascemos sós só seremos serenos no fim

----------------- Acordes -----------------
Bb7M = X 1 3 2 3 1
D#7/9 = X 6 5 6 6 X
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Fm7 = 1 X 1 1 1 X
G# = 4 3 1 1 1 4
Gm = 3 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
