Alceu Valença - Espelho Cristalino

[Intro] G  D  A

E|-7-10-------9-7----------7-10--------|-7-10------9-7----------7-10--------|
B|------10--7-----10-8-7-5------10-7-5-|------10-7-----10-8-7-5------10-7-5-|
G|-------------------------------------|------------------------------------|
D|-------------------------------------|------------------------------------|
A|-------------------------------------|------------------------------------|
E|-------------------------------------|------------------------------------|

      G       D            A
Essa rua sem céu, sem horizontes
        G     D            A
Foi um rio de águas cristalinas
       G        D          A
Serra verde molhada de neblina
       G         D         A
Olho d'água sangrava numa fonte
      G        D           A
Meu anel cravejado de brilhantes
       G         D         A
São os olhos do capitão Corisco

     G            D         A
É a luz que incendeia meu ofício
       G       D           A
Nessa selva de aço e de antenas
        G            D          A
Beija-flor estou chorando suas penas
      G          D             A    B  A
Derretidas na insensatez do asfalto

        G           D           A
Mas eu tenho meu espelho cristalino
           G          D          A
Que uma baiana me mandou de Maceió
     G       D             A
Ele tem uma luz que me alumia
         G      D             A
Ao meio-dia clareia a luz do sol
    G           D           A
Eu tenho meu espelho cristalino
           G          D          A
Que uma morena me mandou de Maceió
     G       D             A
Ele tem uma luz que me alumia
         G      D             A
Ao meio-dia clareia a luz do sol

        G      D         A
Que me dá o veneno da coragem
     G            D           A
Pra girar nesse imenso carrossel
     G         D         A
Flutuar e ser gás paralisante
     G           D           A
E saber que a cidade é de papel
       G         D            A
Ter a luz do passado e do presente
    G         D          A
Viajar pelas veredas do céu
        G           D          A
Pra colher três estrelas cintilantes
      G        D             A
E pregar nas abas do meu chapéu
         G        D          A
Vou clarear o negror do horizonte
           G          D          A
É tão brilhante a pedra do meu anel

( G  D  A )
( G  D  A )

        G          D           A
Mas eu tenho um espelho cristalino
           G          D          A
Que uma baiana me mandou de Maceió
     G       D             A
Ele tem uma luz que me alumia
         G      D             A
Ao meio-dia clareia a luz do sol
    G           D           A
Eu tenho meu espelho cristalino
           G          D          A
Que uma baiana me mandou de Maceió
     G       D             A
Ele tem uma luz que me alumia
         G      D             A
Ao meio-dia clareia a luz do sol
    G           D           A
Eu tenho meu espelho cristalino
    G           D           A
Eu tenho meu espelho cristalino
    G           D           A
Eu tenho meu espelho cristalino
    G           D           A
Eu tenho meu espelho cristalino

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
D = X X 0 2 3 2
G = 3 2 0 0 0 3
