Alceu Valença - No Balanço da Canoa

[Intro] Bb  A  Gm  C7  Gm

       Bb  A
Dona Maria cuidado com a saia
              C7                    Gm
No balanço da canoa o vento pode alevantar
     C7                               Gm
O canoeiro só rema com a proa
            C7                    Gm
Cuidado canoeiro pra canoa não virar

       Bb  A
Dona Maria cuidado com a saia
              C7                    Gm
No balanço da canoa o vento pode alevantar
     C7                               Gm
O canoeiro só rema com a proa
            C7                    Gm
Cuidado canoeiro pra canoa não virar

( Bb  A  Gm  C7  Gm )


         C7                 Gm
No alto mar tem muita coisinha boa
                   C7                   Gm
Tem jangada e tem canoa pra quem quiser passear

    Bb A   Gm  C7  Gm
O canoeiro só rema na proa
           C7                   Gm
Cuidado canoeiro pra canoa não virar

    Bb A Bb Gm  C7  Gm
O meu canoeiro só rema na proa
           C7                   Gm
Cuidado canoeiro pra canoa não virar

                 C7            Gm
Meu navio de um bambo e ele cambiou
                 C7              Gm
Bambeou mais não virou lá no auto mar

----------------- Acordes -----------------
A = X 0 2 2 2 0
A# = X 1 3 3 3 1
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
