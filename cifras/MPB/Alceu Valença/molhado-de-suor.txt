Alceu Valença - Molhado de Suor

(intro) E

E |---------------------------|
B |-4/5-4-4/5-4-4/5-4-5-7-5-4-|
G |---------------------------|
D |---------------------------|
A |---------------------------|
E |---------------------------|

E
Eu gosto
É de ter ver bonita
          A
Com aquele vestido
Que eu acho que era branco
         E
E que no fim do ano
E     A         E
Você tingiu de azul
E      A         E
Você tingiu de azul


E
Eu gosto
É de olhar teus olhos
           A
Se espalhando na tarde

Em busca de miragens
           E
De bolas coloridas
     E   A      E
Que desciam do céu
     E   A      E
Que desciam do céu

B |-5-3-2--------------|
G |-------4-2-1--------|

 (E A)
B |------3-2-----------3-2-|
G |--1-4-----4-1---1-4-----|
D |2-------------2---------|

(E A)
Eu gosto
É de morrer de sede
E é de beber teu beijo
É de tocar teu corpo
Molhado de suor
Molhado de Suor...

B |-5-5-4-4-2-2-------------------------------------------------|
G |-------------4-4-2-2-1-1-------4-4-2-2-1-1-------------------|
D |-------------------------4-4-2-------------4-4-2-2-0-0-------|
A |-------------------------------------------------------4-4-2-|

----------------- Acordes -----------------
A = X 0 2 2 2 0
E = 0 2 2 1 0 0
