Alceu Valença - O Tempo e o Vento

Intro: Em Bm C D G A7 Em A7 G

( Dm  G )
Meu passarinho foi embora
Bateu asas e voou
      Em
Já estou contando as horas
      A7   C      G
De rever o meu amor

          Dm
Faz tato tempo
                     G
E hoje lembrei de você
             Em
Sem dor no peito
                    Bm
Relembrei o que passou
          C
Na grama fina
                   G
Nos molhamos no sereno

                 A7
Como cobra no veneno
                   G  B
Misturamos nosso amor

B7                      Em
   Eu viajei por muito tempo, morena

B7                Em
   Como o rio viajou
A7                      D
   Carregando essa tristeza morena
A7                  D
   Navegando minha dor

B7                     Em
   Mas o tempo foi passando
C#7                    F#m
   Você nunca mais voltou
D#7                        G#m
   E hoje a volta das lembranças
E7        D#7     G#m
   Me feriu e magoou  (3x)

( D7  G )

           Dm
Faz tanto tempo
                         G
E eu só escuto em meu caminho
            Em
Na voz do vento
                       Bm
Teu cantar no meu quintal
             C                      G
E sei que o tempo foge do ressentimento
                     A7
Pois o amor no pensamento
                  G
É quase sobrenatural

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C#7 = X 4 3 4 2 X
D = X X 0 2 3 2
D#7 = X 6 5 6 4 X
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G#m = 4 6 6 4 4 4
