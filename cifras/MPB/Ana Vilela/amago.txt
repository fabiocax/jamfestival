Ana Vilela - Âmago

[Intro]  Am  E
         Am  E

 Am
Sempre existiu num combate constante
                                E
Com seu reflexo no espelho brilhante
 Am
Já fixado no quarto vermelho
    Dm        Am       Dm
Da cor que é dor de viver
       E       Am  E
Sem poder se amar

 Am                          E
Nunca foi apresentada ao sossego
  Am                        E
E amanheceu a vida em desespero
  Am
Quando a falta de vida
              Dm
Já se fez presente

    Am         Dm         E        Am  E
Na hora em primeiro se espera o viver
 Dm            Am
Mas guarda o amor
   Dm                    E        Am  G  F  E
No âmago de um peito que arde em dor

 Am
Já viu o mundo com os olhos do medo
                              E
Quando guardou sua dor em segredo
 Am
Numa redoma de vidro que corta-lhe
  Dm        Am       Dm       E     Am  E
A alma e o peito cansados de se isolar
 Am7
Se acostumou a aceitar a metade

De todo amor que merece em verdade

Sem se dar conta
                       Dm
Que só cabe a ela aceitar
    Am          Dm       E       Am  E
O amor que ela mesma julgar merecer

 Dm             Am
Mas mesmo a sofrer
   Dm                     E       Am  E
Entende que a dor também faz crescer

(  Am  E  Am  E  )
(  E  Am  )

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
