Ana Vilela - Bullet Train

                      A
It's not about having all the people
                       D
in the world for you
                  A
It's about knowing that somewhere
                                    E
Someone watches over you
                  A
It's about singing and being able to listen
                                D
More than the voice itself
                  A
It's about dancing in the rain of life
                  E
That falls on us

            D
It is to know if you feel infinite
      E
In such a vast and beautiful universe

                          A
To know how to dream
                D                             E
And then make it worth every verse
                                      A
From that poem about believing

                       A
It's not about getting on top of the world
                               D
Know that you have won
                   A
It's about climbing and feeling
                                         E
That the way strengthened you
                          A
It's about being shelter
                                        D
And also to live in other hearts
                        A
And so having friends with you
               E
In all situations

        D
We can not have it all
            E                                                      A
What would be the grace of the world if it were so
           D
That's why I prefer smiles
              E
And the gifts that life brought
             A
Close to me

It's not about all your money
       D
Can buy
                       A
And yes about every moment
               E
Smile to share
                    A
It's also not about running
                                                 D
Against the time to always have more
                               A
Because when you least expect it
                     E
Life is behind us

 D
Hold your son on his lap
   E
Smile and hug your parents
          A
While here
         D
That life is bullet train partner
                                  A
And we're just passing on

  D    E          A
Laia laia laia laia
  D    E          A
Laia laia laia laia


 D
Hold your son on his lap
  E
Smile and hug your parents
          A
While here
        D
That life is bullet train partner
          E
And we're just passing on bullet train

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
