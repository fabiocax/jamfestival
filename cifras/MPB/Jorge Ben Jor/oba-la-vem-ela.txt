Jorge Ben Jor - Oba Lá Vem Ela

           Bm7
Oba, La vem ela
Em7             Bm7
Estou de olho nela.
 Em7          Bm7
Oba, La vem ela
Em7             Bm7  Em7
Estou de olho nela.

     Bm7                    Em7
Não me importo, que ela não me olhe
     Bm7                             Em7
Nao diga nada e não saiba que eu existo, quem eu sou
        Bm7                                Em7
Pois eu sei muito bem quem ela é e fico contente
                      Bm7      Em7
só de ver ela passar,  wowba

 Em7          Bm7
Oba, La vem ela
Em7             Bm7
Estou de olho nela.

 Em7          Bm7
Oba, La vem ela
Em7             Bm7  Em7
Estou de olho nela.

  A7
A noite é linda
Em7          A7
E ela mais ainda
Em7          A7 A#º
Todinha de rosa
      Bm7         A7             Em7  A7
Mais linda, mais meiga, que uma rosa.

             Bm7
Oba, La vem ela
Em7             Bm7
Estou de olho nela.
Em7           Bm7
Oba, La vem ela
Em7             Bm7  Em7
Estou de olho nela.

     Bm7                    Em7
Não me importo, que falem o que pensem
     Bm7                             Em7                       Bm7
Pois sem saber ela é minha alegria, ela tem um perfume
                                           Em7                                    Bm7
De uma flor que eu não sei o nome , Mas ela deve ter um nome tão bonito
           Em7                 Bm7   Em7
 igual a Ela, oba oba oba oba    ooba

{Refrão}

----------------- Acordes -----------------
A#º = X 1 2 0 2 0
A7 = X 0 2 0 2 0
Bm7 = X 2 4 2 3 2
Em7 = 0 2 2 0 3 0
