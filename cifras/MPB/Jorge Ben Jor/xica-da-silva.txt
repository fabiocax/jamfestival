Jorge Ben Jor - Xica da Silva

(intro) Am7  Bm7

( Am7  Bm7 ) (repetir essa seqüência durante toda a música)

Xica da, Xica da, Xica da, Xica da Silva...a negra (2x)

Xica da Silva, a negra, a negra,
De escrava a amante, mulher, mulher
Do fidalgo tratador João Fernandes, ai ai ai

(refrão)

A imperatriz do Tijuco,
a dona de Diamantina,
morava com a sua côrte
cercada de belas mucamas
Num castelo na Chácara da Palha
de arquitetura sólida e requintada,
onde tinha até um lago artificial
e uma luxuosa galera
que seu amor João Fernandes, o tratador,

mandou fazer só para ela, ai ai ai

(refrão)

Muito rica e invejada,
temida e odiada,
pois com as suas perucas
cada uma de uma cor,
jóias, roupas exóticas
das Índias, Lisboa e Paris
a negra era obrigada
a ser recebida como uma grande senhora
da côrte
do Rei Luís
da côrte
do Rei Luís
ai ai ai ai ai ai ai...

(refrão)

ai ai ai ai ai ai ai...

(solo)


----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Bm7 = X 2 4 2 3 2
