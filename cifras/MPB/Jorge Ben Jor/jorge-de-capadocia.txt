Jorge Ben Jor - Jorge de Capadócia

Intro: Em  F#m7  G  F#m7       4x

Em  F#m7  G  F#m7                   Em  F#m7  G  F#m7
Jorge,                             da Capadócia,
Em  F#m7  G  F#m7                   Em  F#m7  G  F#m7
Jorge,                             da Capadócia,

Solo: Em/D C#m7/5- C7+ Em               4x

Em/D                C#m7/5- C7+              Em
       Jorge sentou praça,       na cavalaria
Em/D                    C#m7/5-
       Eu estou   feliz porque
     C7+                                Em
eu também sou da sua companhia
Em/D            C#m7/5-          C7+       Em
       Eu estou vestido com as roupas       e as armas de Jorge
Em/D                                    C#m7/5-  C7+
       Para que meus inimigos tenham pés
               Em
E não me alcancem

Em/D                           C#m7/5-  C7+
       Para que meus inimigos tenham mãos
                           Em
E não me peguem e não me toquem
Em/D                            C#m7/5-    C7+
       Para que meus inimigos tenham olhos
           Em
E não me vejam
           Em/D                       C#m7/5-  C7+
E nem mesmo pensamentos eles possam ter
                   Em
Para me fazerem mal
Em/D             C#m7/5- C7+               Em
         Armas de fogo,        meu corpo não alcançará
Em/D                       C#m7/5- C7+
      Facas lanças se quebrem
                     Em
Sem o meu corpo tocar
Em/D                               C#m7/5-  C7+
    Cordas correntes se arrebentem

                                  Em
Sem o meu corpo amarrar
Em/D                    C#m7/5-                    C7+       Em
      Pois eu estou vestido com as roupas e as armas de Jorge
Em/D                     C#m7/5-  C7+       Em
      Jorge é da Capadócia,          viva Jorge!
Em/D                     C#m7/5-  C7+           Em
      Jorge é da Capadócia,          salve Jorge!
Em/D    C#m7/5-  C7+                   Em
      Perseverança        ganhou do sórdido fingimento
Em/D     C#m7/5-   C7+                Em
      E disso tudo,         nasceu o amor

Em/D C#m7/5- C7+ Em      2x

Em/D          C#m7/5-  C7+       Em
Ogã,                     toca pra Ogum
Em/D    C#m7/5- C7+                     Em
           Ogã,       Ogã, toca pra Ogum
Em/D                     C#m7/5- C7+             Em
       Jorge é da Capadócia,     Jorge é da Capadócia
   Em/D                    C#m7/5-         C7+        Em
Ogã,            toca pra Ogum,      Ogã, toca pra Ogum
Em/D                C#m7/5-  C7+                  Em
      Jorge sentou praça,           na cavalaria
Em/D                 C#m7/5-    C7+
        E eu estou feliz porque
                                Em
eu também sou da sua companhia
  Em/D          C#m7/5-  C7+           Em
Ogã, toca pra Ogum, Ogã, toca pra Ogum
Em/D           C#m7/5- C7+              Em
Jorge da Capadócia,    Jorge da Capadócia

Solo: Em/D C#m7/5- C7+ Em               6x

Em/D         C#m7/5-                          C7+          Em
          Ogã,           toca pra Ogum, Ogã, toca pra Ogum
Em/D           C#m7/5- C7+            Em
Ogã, toca pra Ogum, Ogã, toca pra Ogum
Diz Jorge,
Em/D           C#m7/5- C7+                 Em
Jorge da Capadócia,    Jorge da Capadócia

----------------- Acordes -----------------
C#m7/5- = X 4 5 4 5 X
C7+ = X 3 2 0 0 X
Em = 0 2 2 0 0 0
Em/D = X X 0 4 5 3
F#m7 = 2 X 2 2 2 X
G = 3 2 0 0 0 3
