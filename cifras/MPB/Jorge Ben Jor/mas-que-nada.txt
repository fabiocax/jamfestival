Jorge Ben Jor - Mas que nada

[Intro] Gm7  Cm7  F7  Cm7  Gm7  Cm7  Gm7  D7(#9)

Gm7  Cm7  F7  Gm7  Cm7  Gm7  D7(#9)
Oh.............O Ariá raiou oba oba oba

         Gm7              D7(#9)
Mas que nada sai da minha frente
              Gm7
Que eu quero passar
       D7(#9)     Gm7
Pois o samba está animado
         D7(#9)    Gm7  G7
O que eu quero é sambar

 Cm7        F7          Gm7
Este samba que é misto de maracatu
                   Cm7
É samba de preto velho
  F7          Gm7
Samba de preto tú


D7(#9)             Gm7      D7(#9)                Gm7
        Mas que nada    um samba como este tão legal
D7(#9)          Gm7       D7(#9)   Gm7  G7
Você não vai querer que eu chegue no final

 Cm7        F7          Gm7
Este samba que é misto de maracatu
                   Cm7
É samba de preto velho
  F7          Gm7
Samba de preto tú

----------------- Acordes -----------------
Cm7 = X 3 5 3 4 3
D7(#9) = X 5 4 5 6 X
F7 = 1 3 1 2 1 1
G7 = 3 5 3 4 3 3
Gm7 = 3 X 3 3 3 X
