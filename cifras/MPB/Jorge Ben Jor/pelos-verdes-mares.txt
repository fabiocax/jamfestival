Jorge Ben Jor - Pelos Verdes Mares

Intro: G#m7

Obs: o acorde D#m7(b13) é usado: Dedo 1 corda Ré na 6ª casa; Dedo 2 corda Sol na 6ª casa;
Dedo 3 corda Si na 7ª casa; Dedo 4 corda Mi (mizinha) na 7ª casa. Agora é só desfrutar dessa música
maravilhosa do Rei do Swingue.

G#m7         C#m7       D#m7   G#m7
Pelos verdes mares naveguei, amor

G#m7         C#m7       D#m7   G#m7
Pelos verdes mares atrás do seu calor

(Nessa parte, segura no G#m7)

G#m7
Delirante, deliciante
Inebriante, contagiante

D#m7(b13) D#7(9)     G#m7
Mulher dos meus pensamentos


D#m7(b13) D#7(9)               G#m7
Cheio de amor, venho procurando vencer

D#m7(b13) D#7(9)     G#m7
Mulher dos meus pensamentos

D#m7(b13) D#7(9)               G#m7
Cheio de amor, venho procurando te ver

D#m7(b13) D#7(9)     G#m7
Mulher dos meus pensamentos

D#m7(b13) D#7(9)               G#m7            G#7
Venho suspirando por um sorriso seu a qualquer momento

C#m7            F#7(9)        G#m7          G#7
Pois com você a paz dentro de mim é mais profunda

C#m7     F#7(9)
E angelical, amor

C#m7         F#7(9)          G#m7        G#7
Os meus pensamentos são mais férteis e serenos

C#m7     F#7(9)
E adocicados, amor


G#m7
Delirante, deliciante
Inebriante, contagiante


G#m7         C#m7       D#m7   G#m7
Pelos verdes mares naveguei, amor

G#m7         C#m7       D#m7   G#m7
Pelos verdes mares atrás do seu calor

----------------- Acordes -----------------
C#m7 = X 4 6 4 5 4
D#7(9) = X 6 5 6 6 X
D#m7 = X X 1 3 2 2
D#m7(b13) = X X 1 4 2 2
F#7(9) = X X 4 3 5 4
G#7 = 4 6 4 5 4 4
G#m7 = 4 X 4 4 4 X
