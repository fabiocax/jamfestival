Jorge Ben Jor - Zumbi

E|----------------------------------------
B|----------------------------------------
G|--7-10-10-7-10-10-7------10-7-10-10-10--
D|-------------------10-8-8---------------
A|----------------------------------------
E|----------------------------------------

Bb
Angola congo benguela
                    D#
Monjolo cabinda mina
           Dm7   F   E D#
Quiloa rebolo
                   Bb
Aqui onde estão os homens
D#                Bb
Há um grande leilão
D#
Dizem que nele há
           Bb
Um princesa à venda

D#                      Bb
Que veio junto com seus súditos
D#                      Bb
Acorrentados em carros de boi

D#
Eu quero ver
Bb
Eu quero ver
D#
Eu quero ver
Bb          D# F
Eu quero ver

Bb
Angola congo benguela
                    D#
Monjôlo cabinda mina
            Dm7  F   E D#
Quiloa rebolo
                   Bb
Aqui onde estão os homens
D#                 Bb
Dum lado cana de açúcar
D#                 Bb
Do outro lado o cafezal
D#                    Bb
Ao centro senhores sentados
D#                      Bb
Vendo a colheita do algodão branco
D#                      Bb
Sendo colhidos por mãos negras

D#
Eu quero ver
Bb
Eu quero ver
D#
Eu quero ver
Bb
Eu quero ver

D#     F          Bb
Quando Zumbi chegar
D#     F          Bb
O que vai acontecer
                   D#
Zumbi é senhor das guerras
     F           Bb
È senhor das demandas
              D#     F
Quando Zumbi chega e Zumbi
Bb            D#     F
É quem manda

Bb
Eu quero ver
D# F
Eu quero ver
Bb
Eu quero ver

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
D# = X 6 5 3 4 3
Dm7 = X 5 7 5 6 5
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
