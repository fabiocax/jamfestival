Jorge Ben Jor - Hermes Trismegisto

Intro:
D           G
Ih ih ih ih ih ih ih

D      G
Hermes
D              G
Trismegisto escreveu
        D           G             Bm7             G
com uma ponta de diamante em uma lâmina de esmeralda
D            G        D                  G
O que está embaixo é como o que está no alto,
D               G       D                G
e o que está no alto é como o que está embaixo.
D                                G      D             G
E por essas coisas fazem-se os milagres  de uma coisa só.
D                  G      D               G
E como todas essas coisas são e provêm de um
D                G
pela mediação do um,
               D                               G         D        G
assim todas as coisas são nascidas desta única coisa por adaptação.


D           Bm7  D         G
O sol é seu pai, a lua é sua mãe.
D                       Bm7
O vento o trouxe em seu ventre.
D                G          D      G
A terra é sua nutriz e receptáculo.

D                    G                D           G
O Pai de tudo, o Thelemeu do mundo universal está aqui.
D                    G                D           G
O Pai de tudo, o Thelemeu do mundo universal está aqui.

D           G
Ih ih ih ih ih ih ih

    D     Bm7   G             D     G
Sua força ou potência está inteira,
       D   G               D     G
se ela é é é convertida em terra.

D              G             D             G
Tu separarás a terra do fogo e o sutil do espesso,
    D                    G
docemente, com grande desvelo.
     D              G       D           G
Pois Ele ascende da terra e descende do céu
    D               G             D    G
e recebe a força das coisas superiores
       D    G       D    G
e das coisas  inferiores.
     D            G               D        G
Tu terás por esse meio a glória do mundo, do mundo
  D            G              D      G
e toda obscuridade fugirá de ti.
  D            G              D      G
e toda obscuridade fugirá de ti.

    D    G            D   G
É a força de toda força,
       D                   G
pois ela vencerá qualquer coisa sutil
D              G
e penetrará qualquer coisa sólida.
D                    G
Assim, o mundo foi criado.
D                 G        D       G
Disso sairão admiráveis adaptações,
     D            G
das quais aqui o meio é dado.
     D         G       D           G
Por isso fui chamado Hermes Trismegistro,
     D         G       D           G
Por isso fui chamado Hermes Trismegistro,

D                        G          D           G
tendo as três partes da filosofia universal.
D                        G          D           G
tendo as três partes da filosofia universal.

D                      G G7        D      G
O que disse da Obra Solar está completo.
D                      G Gm7        D     G
O que disse da Obra Solar está completo.

D      G
Hermes
D              G
Trismegisto escreveu
        D           G             Bm7             G
com uma ponta de diamante em uma lâmina de esmeralda

D           G
Ih ih ih ih ih ih ih

----------------- Acordes -----------------
Bm7 = X 2 4 2 3 2
D = X X 0 2 3 2
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
Gm7 = 3 X 3 3 3 X
