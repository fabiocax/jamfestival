Jorge Ben Jor - Boa Jornada

D         (D  C#  C)
Viva o sol
        G                         A
Que faz amor com a madrugada até de manhã
D         (D  C#  C)
O homem que
        G                         A
Vive pensando como deve seguir o seu destino
Bm   F#m        D          E          Em7   A
Boa jornada pra quem nunca vai se entregar

D         (D  C#  C)
Aquele que
        G                         A
Nunca foi santo nem anjo mas parece ser
D         (D  C#  C)
O homem que
        G                         A
É descente, não tem muito mas é puro e contente
Bm   F#m        D          E          Em7  Am
Boa jornada porque um novo dia nascerá


G                       C
Você não ficará sozinho, seja como for
G                       C
Põe energia em seu caminho, boa jornada!
G                       C
Fé em Deus e pé na tábua, Segue a sua estrada
G                       C
Pegue o seu presente, Boa jornada pra você!
G                       C
Você não estará sozinho, seja como for
G                       C
Alegria em seu caminho, Boa jornada!
G                       C
Você não esta sozinho tem alguém esperando por você
Em   A     D
Para amar!

G       C
Uuu uuu uuu ei ei!
G       C
Uuu uuu uuu ei ei!

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C# = X 4 6 6 6 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
