Jorge Ben Jor - Minha menina

Intro: G C G C
      Bb A G Bb A G
(G C)
Ela é minha menina
E eu sou o menino dela
Ela é o meu amor
E eu sou o amor todinho dela
G                      A
A lua prateada se escondeu
B          C        G
E o sol dourado apareceu
                    A
Amanheceu um lindo dia
              B
Cheirando alegria
          C
Pois eu sonhei e acordei
         D
Pensando nela
(G C)
Pois ela é minha menina

E eu sou o menino dela
Ela é o meu amor
E eu sou o amor todinho dela
G                 A
A roseira já deu rosa
B         C                 G
E a rosa que eu ganhei foi ela
                         A
Por ela eu ponho meu coração
           B
Na frente da razão
           C
Pois vou dizer pra todo mundo
           D
Como gosto dela
(G C)
Porque ela é minha menina
E eu sou o menino dela
Ela é o meu amor
E eu sou o amor todinho dela
Minha menina
Minha menina

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
