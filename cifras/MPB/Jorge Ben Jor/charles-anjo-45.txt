Jorge Ben Jor - Charles anjo 45

Intro: Am  F/A  D/A  G/A

Am   D        G    C
Oba, oba, oba charles
Am    D          G     C
Como é  que é my friend Charles
Am    D           G      C
Como vão as coisas Charles?
Am     D Am           D   Am            D               G
Charles.........Anjo 45...protetor dos fracos e dos oprimidos
                C                 Am       D              Am   D
Robin Hood dos morros..rei da malandragem..um homem de verdade
            D              G                     C                Am   D
Com muita coragem......Só porque um dia Charles marcou bobeira
     Am          G    C            Am         D
E foi tirar sem querer férias numa colônia penal
Am                  D      Am
Então os malandros otários
             D
Deitaram na sopa
Am                D                     G
E uma tremenda bagunça o nosso morro virou

        C               Am  D
Pois o morro que era o céu
Am           D                   G   C  Am
Sem o nosso Charles o inferno virou
Am          D   Am    D   Am                        D                  G             C  Am
Mas Deus é justo e verdadeiro e antes de acabar as férias nosso Charles  vai voltar
Am             D    Am                 D   Am             D  Am                 D
Para alegria geral, todo o morro vai sambar

antecipando o carnaval, Vai ter feijoada  uma missa em ação de graças

Am              D              G          C Am
Whisky com cerveja e outras milongas mais
Vai ter queima de fogos
e Saraivada de balas pro ar
pra quando o nosso Charles voltar

D      Am    D        G      C
Oba!....oba, oba, oba Charles
Am    D           G   C
Como é my friend Charles
Am         D          G     C
Como vão as coisas Charles?
Am  F/A        D/A   G/A
(      Charles, 45        )

Am     D      Am     D    Am            D                G
Charles.........Anjo 45...protetor dos fracos e dos oprimidos

Am         D      Am      D              Am         D           G                   C Am
Mas Deus é justo e verdadeiro e antes de acabar as férias nosso Charles  vai voltar
Am             D        Am             D   Am           D    Am                  D
Para alegria geral, antecipando o carnaval, vai ter feijoada, missa em ação de graças
Am              D               G          C  Am
Whisky com cerfveja e outras milongas mais
D       Am   D         G    C  Am
Oba!....oba, oba, oba Charles
Am     D          G     C  Am
Como é my friend Charles
Am     D            G      C  Am
Como vão as coisas Charles?

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/A = X 0 X 2 3 2
F/A = 5 X 3 5 6 X
G = 3 2 0 0 0 3
G/A = 5 X 5 4 3 X
