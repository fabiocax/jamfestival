Jorge Ben Jor - Olha a Pipa

Am7                     Dm7     Em7
 Naquele campo verde que ainda existe
Am7              Dm7  Em7
 Longe dos fios elétricos
Am7                         Dm7 Em7
 Eu vou soltar a minha pipa, eu vou
Am7                         Dm7 Em7
 Eu vou soltar a minha pipa, eu vou

       Am7 Bm7 Dm7     Em7
Olha a pipa, voando no céu
       Am7 Bm7 Dm7     Em7
Olha a pipa, voando no céu
        Am7         Bm7          Dm7            Em7
É de bambu, é de papel, ela é de linha de carretel
       Am7 Bm7 Dm7 Em7
Olha a pipa

Am7
Pipa,
   Bm7
Papagaio,

     Dm7
Barrilete,
   Em7
Pandorga

Am7
Pipa,
   Bm7
Papagaio,
     Dm7
Barrilete,
   Em7
Pandorga

Am7                     Dm7           Em7
 Pipa feita de papel de seda amarelo e branco
Am7                         Dm7             Em7
 Papagaio feito de papel de seda vermelho e ouro
Am7                          Dm7         Em7
 Barrilete feito de papel de seda azul e rosa
Am7                         Dm7          Em7
 Pandorga feita de papel de seda verde e roxo

Am7                  Dm7 Em7 Am7                     Dm7         Em7
 A minha linha é Rococó,      e tem cerol de vidro moído e goma-arábica
Am7                  Dm7 Em7 Am7                     Dm7         Em7
 A minha linha é Rococó,      e tem cerol de vidro moído e goma-arábica
      Am7                                 Dm7       Em7
 Quem quer cruzar, quem quer cruzar, quem quer cruzar?
Am7                                            Dm7        Em7
 Tem gilete no rabo, tem gilete no rabo, tem gilete no rabo

(Repete desde o início)

       Am7 Bm7 Dm7     Em7
Olha a pipa, voando no céu

Am7         Bm7         Dm7         Em7
pipa, pipa, pipa, pipa, pipa, pipa, pipa
Am7         Bm7          Dm7        Em7
céu, céu, céu, céu, céu, céu, céu, céu.

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Bm7 = X 2 4 2 3 2
Dm7 = X 5 7 5 6 5
Em7 = 0 2 2 0 3 0
