Jorge Ben Jor - Frases

(Dm C Am Am7)
Eu só quero que Deus me ajude
E o menino também
Pois a rosa é uma flor
A flor é uma rosa
E o menino não é ninguem

(Dm Am Em Am7)
Olha o menino ui
Olha o menino ui, ui, ui, ui

(Dm C Am Am7)
Há seis mil anos que o homem vive feliz fazendo guerra e asneiras
Há seis mil anos Deus perde tempo fazendo flores e estrelas

(Dm Am Em Am7)
Olha o menino ui
Olha o menino ui, ui, ui, ui

(Dm C Am Am7)


Eu sou um homem sincero porque nasci cresci e vivo livre
Eu sou um homem sincero quero morrer nascer e viver livre

(Dm Am Em Am7)
Olha o menino ui
Olha o menino ui, ui, ui, ui

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
