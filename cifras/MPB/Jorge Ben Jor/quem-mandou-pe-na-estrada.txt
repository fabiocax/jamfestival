Jorge Ben Jor - Quem mandou (Pé na estrada)

Intro: G°  F#°  G

     A#°              Am7               G
Quem mandou você brincar de amor comigo, amor
             A#°
(Quem mandou, quem mandou?)
        Am7                 G
Você brincar de amor comigo,amor
                              A#°
(Quem mandou, quem mandou?) (2x)

Am7        G           A#°
Eu   te    amo
Am7        G           A#°
Eu te quero
Am7      G                                   A#°
Eu te gosto, meu amor
Am7           G
Eu te adoro

A#°   Am7       G
Que gamação danada


              A#°          Am7         G
Não sei se fico ou se meto o pé na estrada   (2x)

A#°  Am7   G
Quem mandou?
   A#°        Am7           G
Você brincar de amor comigo amor.(2x)
A#°        Am7                   G
Eu te amo, eu te amo, eu te amo...

----------------- Acordes -----------------
A#° = X 1 2 0 2 0
Am7 = X 0 2 0 1 0
F#° = 2 X 1 2 1 X
G = 3 2 0 0 0 3
G° = 3 X 2 3 2 X
