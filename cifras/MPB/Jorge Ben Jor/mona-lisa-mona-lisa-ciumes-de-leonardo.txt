Jorge Ben Jor - Mona Lisa Mona Lisa (Cíúmes De Leonardo)

Intro: A7+ G7+ (x2)

A           G7+
Ai, ai, ai,ai, aiaiai

 A               G7+
Mona Lisa, Mona Lisa

 A
Você é tão Gioconda
   G7+
Você é tão maliciosa
   A
Você é tão diferente e bela
   G7+
Você é tão misteriosa
     A
Que nem uma chuva de ouro
   G7+
Seria tão atraente
        A
Como você

       G7+        A       G7+
Mon amour como você Mona Lisa, ai ai

   A
Atrás desse sorriso
     G7+
Tem coisas enigmáticas
     A
Que só Leonardo da Vinci
       G7+
Conseguiu descobrir
    A                  G7+
Por isso na calada da noite
                  A
Quando todos estiverem dormindo
           G7+
Com seus amores
A               G7+
Eu escalarei o Louvre
A                 G7+
E conquistarei você
A               G7+
Eu escalarei o Louvre
A                 G7+
E dormirei com você
 A               G7+
Mon amour, Mona Lisa
 A               G7+
Mon amour, Mon cheri.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7+ = X 0 2 1 2 0
G7+ = 3 X 4 4 3 X
