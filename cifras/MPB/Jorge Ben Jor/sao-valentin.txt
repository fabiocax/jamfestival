Jorge Ben Jor - São Valentin

[Intro]  B  A

 Am          D7/9        Dm7      G7
Valentine's day, dia de São Valentin
   Am7  D7/9 Dm7 Em7
Menina

  Am7                 D7/9
Quero te encontrar, quero te abraçar
  Dm7               G7
Quero te abraçar, quero te beijar
  Am7              D7/9           Dm7  Em7
Quero te beijar, quero te amar menina

 Am7             D7/9  Dm7    G7        Am7
Se voce me der chance te conquisto, te levo
       D7/9       Dm7        G7           Am7
Entre beijos e abraços para ver o por do sol
D7/9      Dm7      G7    Am7      D7/9   Dm7  Em7
E um arco-iris na mágica Ilha de Gungaporanga
 Am7       D7/9          Dm7   Em7     Am7 D7/9          Dm7 G7
Terra do Poeta Jorge de Lima que escreveu  e declamou assim:


   Am7       D7/9         Dm7         G7
Você é meu amor porque você é o meu amor
 Am7                D7/9
Mesmo que você não seja o meu amor
   Dm7        G7
Você vai ser sempre o meu amor
   Am7       D7/9      Dm7        G7
Você está aqui quando não está aqui
    Am7          D7/9       Dm7       G7
E quando está aqui, está aqui duas vezes

     Am7            D7/9    Dm7           G7
Uma vez no meu coração e na outra vez também
     Am7            D7/9    Dm7           Em7
Uma vez no meu coração e na outra vez também

           Am7    D7/9   Dm7 Em7           Am7   D7/9       Dm7 G7
Meu São Valentin diz pra ela     que eu estou esperando por ela

           Am7   D7/9      Dm7 Em7          Am7 D7/9    Dm7 G7
Meu São Valentin avisa pra ela    que eu estou de olho nela

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
B = X 2 4 4 4 2
D7/9 = X 5 4 5 5 X
Dm7 = X 5 7 5 6 5
Em7 = 0 2 2 0 3 0
G7 = 3 5 3 4 3 3
