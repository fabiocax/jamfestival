Jorge Ben Jor - Zé Canjica

Intro: A4  G6 (5x)

         A4  G6                        A4  G6
Está chovendo e a chuva vai molhar alguém
                      A4
Que outrora caia toda molhada
                 G6
Nos braços meus
                  A4
Não é verdade não, não pode ser
G6                 A4
Não é verdade não, não pode ser
  G6         A4            G6      A4
Silêncio     vai embora me deixa perdão
  G6         A4            G6      A4
Silêncio vai embora me     deixa perdão
G6                          A4
Me desculpem meus amigos gente
G6
Se eu estou confuso
  A4          G6            A4
E triste      e até mal humorado (2x)


G6                    A4
Mas é que eu já não sou
    G6           A4
Namorado do meu amor

(Atenção para a mudança da velocidade da
batida a partir da próxima estrofe)

Bm7             E7(9)             D7(9)
Sei que a minha maré não está prá peixe
GM7                 C7(9)             A4
Mas eu não vou desistir de pescar porque
G6                   A4          G6         A4
Pois ainda resta em mim um       fio de esperança
                 G6
E a vontade de viver
         A4               G6         A4  G6
Prá conseguir conquistar novamente ela
         A4               G6         A4  G6
Prá conseguir conquistar novamente ela
  A4         G6      A4        G6   A4       G6     A4 G6
Silêncio      vai embora       me deixa          perdão
  A4         G6      A4        G6   A4       G6     A4 G6
Silêncio      vai embora       me deixa          perdão

(pausa no violão para solo de percursão)

(G6  A4)

G6              A4        G6
   Mas é que eu já não sou
        A4        G6  A4          G6
O Zé Canjica          do meu amor

(A4  G6)

G6              A4        G6
   Mas é que eu já não sou
        A4        G6  A4  G6
O Zé Canjica          do meu amor

  A4         G6      A4        G6   A4       G6     A4 G6
Silêncio      vai embora       me deixa          perdão

----------------- Acordes -----------------
A4 = X 0 2 2 3 0
Bm7 = X 2 4 2 3 2
C7(9) = X 3 2 3 3 X
D7(9) = X 5 4 5 5 X
E7(9) = X X 2 1 3 2
G6 = 3 X 2 4 3 X
