Jorge Ben Jor - Ela Mora Na Pavuna

Intro
A7 Eb7(9) D7(9)


A7 Eb7(9) D7(9)
Ela mora na Pavuna
A7              Eb7(9) D7(9)
E dança no simpático Pavunense


A7                  Eb7(9) D7(9)
Conheci naquela domingueira legal
A7               Eb7(9) D7(9)
Naquela domingueira coloquial
A7                 Eb7(9) D7(9)
Que nega é essa, Que nega é essa
A7             Eb7(9) D7(9)
Toda cor de jambo, lisinha
A7              Eb7(9) D7(9)
Muita cor de jambo, que gracinha
A7                    Eb7(9) D7(9)
Sem solda elétrica, Sem solda elétrica

A7              Eb7(9) D7(9)
Bom de passar a mão E namorar
A7         Eb7(9) D7(9)
E convidar Para um piquenique
A7            Eb7(9) D7(9)
Na romântica ilha de Paquetá
Porque

A7 Eb7(9) D7(9)
Ela mora na Pavuna
A7              Eb7(9) D7(9)
E dança no simpático Pavunense

A7                     Eb7(9) D7(9)
A fim de levar um lero Parti pra dentro da área
A7                     Eb7(9) D7(9)
Mas ela não gostou das minhas intimidades
A7                           Eb7(9) D7(9)
E me botou pra fora de campo Com uma certeira joelhada
A7            Eb7(9) D7(9)
Dizendo, nem vem que não tem
A7               Eb7(9) D7(9)
Aqui só que dá sapeca É meu bem
A7            Eb7(9) D7(9)
Dizendo, nem vem que não tem
A7               Eb7(9) D7(9)
Aqui só quem brinca É meu bem
A7            Eb7(9) D7(9)
E disse mais Sou mangueirense
A7            Eb7(9) D7(9)
Eu corro atrás Sou fluminense
Ai, ai, ai

A7        Eb7(9) D7(9)
Esta nega está maluca
A7          Eb7(9) D7(9)
Ou quer me deixar maluco
{bis}

A7       Eb7(9) D7(9)
Nega bonita, gostosa e fresca Você vai ver
A7         Eb7(9) D7(9)
Eu vou fazer sua cabeça
{bis}

Ela mora na Pavuna...

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
D7(9) = X 5 4 5 5 X
Eb7(9) = X 6 5 6 6 X
