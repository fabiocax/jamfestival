Jorge Ben Jor - Emílio

(Participação especial da Timbalada)


Intro: F#m7 Bm7 C#m7
       F#m7      Bm7 C#m7
Não vai dar, vai dar
       F#m7      Bm7 C#m7
Não vai dar, vai dar
         F#m7
O guerenguê, o guerenguê
        Bm7  C#m7
O guerenguê
         F#m7
O guerenguê, o guerenguê
         Bm7 C#m7
O guerenguê
    F#m7               Bm7 C#m7
O Emílio quer comer acarajé
     F#m7                 Bm7   C#m7
Mas sara baiana não quer dá

   F#m7                      Bm7  C#m7
O Emílio está indócil está de pé
                 F#m7                    Bm7  C#m7
Está com água na boca, atrás dessa coisa louca
     F#m7               Bm7 C#m7
Mas Sara Baiana não quer dá
F#m7        Bm7      C#m7
Seu acarajé, seu munguzá
    F#m7                            Bm7
Ela diz que pra comer tem que conquistar
            C#m7
Ou tem que pagar
    F#m7                           Bm7
Ela diz que pra comer tem que conquistar
           C#m7
Ou tem que pagar
F#m7             Bm7  C#m7
Ondina avisa Amaralina
      F#m7             Bm7  C#m7
Que o Emílio está na Pituba
            F#m7
Chorando de fome
              Bm7  C#m7
Chorando de paixão
               F#m7                   Bm7    C#m7
Comendo com os olhos este acarajé tentação
F#m7   Bm7  C#m7
Ai, ai, ai
F#m7    Bm7  C#m7
Ai, ai, ai
F#m7   Bm7  C#m7
Ui, ui, ui,
F#m7
Eh, eh, eh
Bm7     C#m7
Oh, oh, oh

----------------- Acordes -----------------
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
F#m7 = 2 X 2 2 2 X
