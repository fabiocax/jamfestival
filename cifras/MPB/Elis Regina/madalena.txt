Elis Regina - Madalena

Intro: D7M(9)  D6(9)  Em7(9)  A7(13)   D7M(9)  D6(9)  Em7(9)  A7(13) D7M(9)  D6(9)  Em7(9)  A7(13)  D7M(9)  D6(9)  Em7(9)  A7(13)

              D7M(9) D6(9)          Em7(9)         A7(13)  D7M(9) D6(9)
Oh, Madalena         o meu pei.....to   perce|----beu
          Em7(9)   A7(13)      D7M(9)  D6(9)      Em7(9)          A7(13)    D7(4/9)  D7(9)
Que o mar       é u......ma  go.....ta       comparado   ao pran.....to  meu
       G7M                 D7(4/9)              G7M
Fique certa    quando o nos-------so amor desper----ta
             D7(4/9)     D7/9  G7M          A/D           F#m7(9)  B7(9)
Logo o sol se deses----pe|---ra e se esconde lá na ser----ra  Eh

Madalena o que é meu      não se divi------de
        F#7(b9)         Bm9          Bm9/A        G#m7(9)  C#(9b/b13 )
Nem tão pouco    se admi----te quem do nosso amor duvi------de

      F#7M     G#m7              A#m7                        A7M
Até a lua se arrisca num    palpi----te que o nosso amor exis---te
          Am7    E7(9)         A7(4/9)    A7(9-/13)
Forte ou fraco,  alegre ou tris------te ...

----------------- Acordes -----------------
A#m7 = X 1 3 1 2 1
A/D = X X 0 6 5 5
A7(13) = X 0 X 0 2 2
A7(4/9) = 5 X 5 4 3 X
A7(9-/13) = X 0 5 6 7 6
A7M = X 0 2 1 2 0
Am7 = X 0 2 0 1 0
B7(9) = X 2 1 2 2 X
Bm9 = X 2 4 6 3 2
Bm9/A = X 0 4 6 3 2
C#m7(9) = X 4 2 4 4 X
D6(9) = X 5 4 2 0 0
D7(4/9) = X 5 5 5 5 5
D7(9) = X 5 4 5 5 X
D7/9 = X 5 4 5 5 X
D7M(9) = X 5 4 6 5 X
E7(9) = X X 2 1 3 2
Em7(9) = X 7 5 7 7 X
Em7(9)/D = 10 9 9 P12 12 12
F#7(b9) = X X 4 3 5 3
F#7M = 2 X 3 3 2 X
F#m7(9) = X X 4 2 5 4
G#m7 = 4 X 4 4 4 X
G#m7(9) = X X 6 4 7 6
G7M = 3 X 4 4 3 X
