Elis Regina - Exaltação a Tiradentes


D            Gm7  C7/9   Em7           A7  D    G7+   D
   Joaquim José               da Silva Xavier
         B7      Em7
Morreu a vinte e um de abril
E7                 Em7        A7
Pela independência do Brasil
       D   D7+          Bb7/13      Bb7   Em7
Foi traído     e não traiu jamais
         A7        D             G7
A Inconfidência de Minas Gerais
       D   D7+          Bb7/13      Bb7   Em7
Foi traído     e não traiu jamais
         A7        D
A Inconfidência de Minas Gerais
Em7       B7/9- Em7       A7
Joaquim José da Silva Xavier
      D       G7  D      A7/13
Era o nome de Tiradentes
D   Bm7       Em7  A7 Em7  A6/9       F#m7   Em7
Foi    sacrifica...do pela nossa liberda.....de

D7+  F#m7     Em7
Este grande herói
    G7           Em7 A7 D7+
Pra sempre há de ser lembrado

----------------- Acordes -----------------
A6/9 = 5 4 4 4 X X
A7 = X 0 2 0 2 0
A7/13 = X 0 X 0 2 2
B7 = X 2 1 2 0 2
B7/9- = X 2 1 2 1 X
Bb7 = X 1 3 1 3 1
Bb7/13 = 6 X 6 7 8 X
Bm7 = X 2 4 2 3 2
C7/9 = X 3 2 3 3 X
D = X X 0 2 3 2
D7+ = X X 0 2 2 2
E7 = 0 2 2 1 3 0
Em7 = 0 2 2 0 3 0
F#m7 = 2 X 2 2 2 X
G7 = 3 5 3 4 3 3
G7+ = 3 X 4 4 3 X
Gm7 = 3 X 3 3 3 X
