Elis Regina - Altos e Baixos

D/E    E7  A7+
Foi, quem sabe, esse disco
       F#m       D6+            C#m5-/7 F#5+/7
Esse risco de sombra em teus cílios
 Bm7+/9                  E7
Foi ou não meu poema no chão
            A7+      D/E E7
Ou talvez nossos filhos
   A7+           G#7
As sandálias de saltos tão altos
  G7+        D7           G7+
O relógio batendo, o sol posto, o relógio
     F#7              B
As sandálias, e eu bantendo em teu rosto
      D7+ G7
E a queda dos saltos tão altos
 A7+              F#m
Sobre os nossos filhos
        D/E
Com um raio de sangue no chão
 E7                Em  A5+/7
Do risco em teus cílios

  D7+                 D#m5-/7       G#7
Foram discos demais, desculpas demais
        G7                          F#7
Já vão tarde essas tardes e mais tuas aulas
            B7      E7       C#7
Meu táxi, whisky, Dietil, Diempax
F#7  Bm            C#m    Dm7            G7
Ah, mas há que se louvar entre altos e baixos
 C#m                       Dm7 C#m
O amor quando traz tanta vida
G7  F#7  B7           E7          Am7 D7/9/11+
Que até pra morrer leva tempo demais

----------------- Acordes -----------------
A5+/7 = X 0 X 0 2 1
A7+ = X 0 2 1 2 0
Am7 = X 0 2 0 1 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
Bm7+/9 = X 2 0 3 2 X
C#7 = X 4 3 4 2 X
C#m = X 4 6 6 5 4
C#m5-/7 = X 4 5 4 5 X
D#m5-/7 = X X 1 2 2 2
D/E = X X 2 2 3 2
D6+ = 10 10 10 11 10 12
D7 = X X 0 2 1 2
D7+ = X X 0 2 2 2
D7/9/11+ = X X 0 1 1 0
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F#5+/7 = 2 X 2 3 3 2
F#7 = 2 4 2 3 2 2
F#m = 2 4 4 2 2 2
G#7 = 4 6 4 5 4 4
G7 = 3 5 3 4 3 3
G7+ = 3 X 4 4 3 X
