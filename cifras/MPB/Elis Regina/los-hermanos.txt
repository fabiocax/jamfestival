Elis Regina - Los Hermanos

 Gm                D7
Yo tengo tantos Hermanos
                   Gm
Que no los puedo contar
                     D7
En el valle en la montaña
                     Gm
En la pampa y en el mar
                      D7
Cada cual con sus trabajos
                     Gm
Con sus sueños cada cual
                   D7
Con la esperanza delante
                     Gm  G7
Con los recuerdos detrás
                   Cm
Yo tengo tantos Hermanos
       A#        D7  Gm
Que no los puedo contar
                  D7
Gente de mano caliente

                  Gm
Por eso de la amistad
                      D7
Con um lloro para llorarlo
                   Gm
Con un rezo para rezar
                   D7
Con un horizonte aberto
                      Gm
Que siempre esta más allá
                    D7
Y esa fuerza pa buscarlo
                Gm
Con tezón y voluntad
                   D7
Cuando parece más cerca
                   Gm  G7
Es cuando se aleja más
                    Cm
Yo tengo tantos Hermanos
       A#        D7  Gm
Que no los puedo contar
                  D7
Y asi seguimos andando
               Gm
Curtidos de soledad
                     D7
Nos perdemos por el mundo
                   Gm
Nos volvemos a encontrar
               D7
Y asi nos reconocemos
               Gm
Por el lejano mirar
                      D7
Por las coplas que mordemos
                 Gm
Semillas de imensidade
                  D7
Y así seguimos andando
               Gm
Curtidos de soledad
                         D7
Y en nosotros nuestros muertos
                    Gm
Pa que nadie quede atrás
                   D7
Yo tengo tantos Hermanos
                    G4/7  G7
Que no los puedo contar
                      Cm
Y una hermana muy hermosa
        A#    D7   Gm
Que se llama libertad

----------------- Acordes -----------------
A# = X 1 3 3 3 1
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
G4/7 = 3 5 3 5 3 X
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
