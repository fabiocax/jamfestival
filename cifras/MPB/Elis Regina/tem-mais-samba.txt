Elis Regina - Tem Mais Samba

Intro: F#7

Bm7                   E7(9)              Am7
   Tem mais samba no encontro que na espe___ra
                    D7(9)          Bm7
Tem mais samba a maldade que a feri___da
                  E7(9)          Am7
Tem mais samba no porto que na ve___la
                   D7(9)             Bm7
Tem mais samba o perdão que a despedi___da
                   D7(9)            E7(9)
Tem mais samba nas mãos do que nos o_____lhos
                      F#7(b9/b13)            Bm7
Tem mais samba no chão           do que na lu___a
                  D7(9)          E7(9)
Tem mais samba no homem que traba_____lha
                  F#7(b9/b13)            Bm7
Tem mais samba no som        que vem a ru___a
                  A7(13)           Am6/C
Tem mais samba no peito de quem cho_____ra
                      D7(9)
Tem mais samba no pran_____to de quem vê


G7M                    C#m7(b5)     F#7(b13)      Bm7
   Que o bom samba não tem     lugar        nem ho___ra
               C#7                F#7/4 F#7(b13)
O coração de fo___ra samba sem querer
Bm7 A7(9)       D7M  G7M C#m7(b5) F#7(b13)      B7M B6
Vem      que pas___sa    teu              sofrer
Bm7                 E7(9)             C#7        C#m7 F#7(b13)
Se todo mundo sambas_____se seria tão fácil viver
Bm7 A7(9)       D7M  G7M C#m7(b5) F#7(b13)      B7M B6
Vem      que pas___sa    teu              sofrer
Bm7                 E7(9)             C#7        C#m7 F#7(b13)
Se todo mundo sambas_____se seria tão fácil viver

----------------- Acordes -----------------
A7(13) = X 0 X 0 2 2
A7(9) = 5 X 5 4 2 X
Am6/C = X 3 4 2 5 2
Am7 = X 0 2 0 1 0
B6 = X 2 4 1 4 X
B7M = X 2 4 3 4 2
Bm7 = X 2 4 2 3 2
C#7 = X 4 3 4 2 X
C#m7 = X 4 6 4 5 4
C#m7(b5) = X 4 5 4 5 X
D7(9) = X 5 4 5 5 X
D7M = X X 0 2 2 2
E7(9) = X X 2 1 3 2
F#7(b13) = 2 X 2 3 3 2
F#7(b9/b13) = 2 X 2 3 3 3
F#7/4 = 2 4 2 4 2 X
G7M = 3 X 4 4 3 X
