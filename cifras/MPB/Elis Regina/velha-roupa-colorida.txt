Elis Regina - Velha Roupa Colorida

[Intro] D  F#  G  G#  A7 (D  A)

                    G
Você não sente nem vê
                                        D
Mas eu não posso deixar de dizer, meu amigo
                A              G         D
Que uma nova mudança em breve vai acontecer
                                 A
E o que há algum tempo era novo, jovem
 G         D            A            G       B  E  B
Hoje é antigo, e precisamos todos rejuvenescer

        D                                 A
Nunca mais teu pai falou: "she's leaving home"
                 G                D
E meteu o pé na estrada, "like a rolling stone..."
                              A
Nunca mais você buscou sua  menina
                    G                            D
Para correr no seu carro...(loucura, chiclete e som)

                                A
Nunca mais você saiu a rua em grupo reunido
              G                        D
O dedo em v, cabelo ao vento, amor e flor, que era o cartaz
                                    A
No presente a mente, o corpo é diferente
                   G                       D
E o passado é uma roupa que não nos serve mais
                                     A
No presente a mente, o corpo é diferente
                   G                           B  E  B
E o passado é uma roupa que não nos serve mais

(refrão)

      D                     A                         G
Como poe, poeta louco americano, eu pergunto ao passarinho:
                        D
 black bird, o que se faz?"
                            A
E raven never raven never raven
                   G                        D
Black bird me responde: "tudo já ficou pra atrás"
                            A
E raven never raven never raven
                  G
Assum preto me responde:
                       B  E  B
"o passado nunca mais"

      A             G        D
E precisamos todos rejuvenescer (3x)
        A   G           D
E precisamos rejuvenescer

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
B = X 2 4 4 4 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
