Elis Regina - Meninas da Cidade

F#m7/E
São 12 pancadas, 12 badaladas
Bb7+
Sol a pino, a telha vã
Em7(9)
Esquenta o pó da minha casa

Esquenta a bilha d'água
Am7
De tanto que ferve na minha mão
                          Em7(9)
Agulha e pano, armas de todo dia
Am7
Na minha mão
Tesoura e fé
E pé
Em7(9)
Na mesma tábua em falso
Destino e pé descalço
Am7
Desde manhã sentada e presa aqui

            Em7
Rasgando as sedas das rainhas
Os brancos das donzelas
Bb7+
Que no escuro da cidade alguém há de despir
Em7(9)
Ninguém verá tão belas
Filhas da falsidade
Am7
A vila é tão pequena e infeliz sem elas que
F#m7/E
Que são doze pancadas
São doze ruelas
    Bb7+
Que desgraçadamente sempre vão dar
      Em7(9)
Numa mesma praça seca, de noite suspirada
    Am7                                     Em7(9)
De noite tão imensamente farta das paixões do dia
    Am7                                          Em7(9)
De noite suficientemente larga pras bandalharias
Bb7+
Meninas que se vêem chegando aqui
Em7(9)
Cinturas ainda finas
Medir felicidade
Bb7+
No rosto a marca dos batons
                       Em7(9)
Das senhoras de bem, as damas da cidade
F7+
No peito arfante
O roxo das mordidas mais ferozes
Em7(9)
Filhos da mesma terra
Andantes e viajores
Am7                          F#m7/E
Rapazes e senhores de mais realidade

São doze pancadas, já são doze dadas
Bb7+
A lua a pino
                    Em7(9)
E eu já sei que vou entrar na madrugada
Rematando bainhas
Am7                                            F#m7/E
Pregando rendas que amanhã vai ser o baile das rainhas
Bb7+                                   Em7(9)
Amanhã já se sabe que elas vão fazer a história da cidade
São muito cinderelas

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Bb7+ = X 1 3 2 3 1
Em7 = 0 2 2 0 3 0
Em7(9) = X 7 5 7 7 X
F#m7/E = X X 2 2 5 2
F7+ = 1 X 2 2 1 X
