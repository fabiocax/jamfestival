Elis Regina - Cabaré

A    C#    D              A
Na porta lentas luzes de neon
    Am                      E
Na mesa flores murchas de crepon
      F                         E
E a luz grená filtrada entre conversas
  F                              E
Inventa um novo amor, loucas promessas


[Refrão]

A7
De tomara-que-caia surge a crooner do norte
                   F      E              Am
Nem aplausos, nem vaias: um silêncio de morte
A     A7+        A7    F#           B
Ah, quem sabe de si nesses bares escuros
                 G                     E   D
Quem sabe dos outros, das grades, dos muros



      A   C#   D             A

No drama sufocado em cada rosto
   F                        E
A lama de não ser o que se quis
   F                           E
A chama quase morta de um sol posto
   F                        E
A dama de um passado mais feliz


[Refrão]

A7
De tomara-que-caia surge a crooner do norte
                   F      E              Am
Nem aplausos, nem vaias: um silêncio de morte
A     A7+        A7    F#           B
Ah, quem sabe de si nesses bares escuros
                 G                     E   D
Quem sabe dos outros, das grades, dos muros


Am              F
Um cuba-libre treme na mão fria
     Am                   F
Ao triste strip-tease da agonia
                         Dm
De cada um que deixa o cabaré
    F                 Dm
Lá fora a luz do dia fere os olhos

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
C# = X 4 6 6 6 4
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
