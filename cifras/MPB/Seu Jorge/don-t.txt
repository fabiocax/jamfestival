Seu Jorge - Don't

D       A7     D      D7       G
Don't, don't, that's what you say
       D          F#m           Em   A7
Each time that I hold you this way,
                  D                  G
When I feel like this and I want to kiss you,
 A7                D    G  A7
baby, don't say don't.
D        A7    D     D7     G
Don't, don't leave my embrace
D               F#m          Em    A7
For here in my heart is your place
                       D                 G
When the night grows cold and I want to hold you,
A7                D     D7
baby, don't say don't.
G                  Gm             D
If you think that this is just a game
       D7
I'm playing
E7
If you think that I don't mean

A7     G        A7
ev'ry word I'm saying
D        A7
Don't, don't,
D      D7        G
Don't feel that way
     D             F#m          Em    A7
I'm your love and yours I will stay
                D
This you can believe
                G
I will never leave you
Em                D   G
Heave knows I won't
A7                D
Baby, don't say don't

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
