Seu Jorge - Convite Para a Vida


Gm7
Sou morador da favela
Cm7
Também sou filho de Deus
Am5-/7              D7
Não sou de chorar mazelas
Gm7                 D7
mas meu amor se perdeu
Gm7
Sou operário da vida
Cm7
Da vida que Deus me deu
Am5-/7              D7
Mas se eu chego atrasado
Gm7                G7
O meu alguém já comeu

Refrão:
Cm7   Gm7   Am5-/7 D7   Gm7 G7
João, José, Jesus,    Mané

Cm7   Gm7   Am5-/7 D7   Gm7  D7
Tião, Lelé, Xangô,    Bené

Gm7
É a Cidade de Deus
                Cm7
Só que Deus esqueceu de olhar
 Am5-/7                D7          Gm7
A essa gente que não cansa de apanhar
D7          Gm7                        Cm7
Não vem dizer que a situação é uma questão de trabalhar
Am5-/7               D7           Gm7 G7
Que vai ter nego querendo te advogar

Refrão

(solo de cavaquinho)

Refrão(3x)

----------------- Acordes -----------------
Am5-/7 = 5 X 5 5 4 X
Cm7 = X 3 5 3 4 3
D7 = X X 0 2 1 2
G7 = 3 5 3 4 3 3
Gm7 = 3 X 3 3 3 X
