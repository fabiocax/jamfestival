Seu Jorge - Everybody Let's Go

Intro: B7M E4(7/9)  A7M Gb4/7  (6x)

B7M                      Gbm7/11          B7M                      Gbm7/11 E7M
Acabei de me vestir pra ver meu amor, oh oh oh
B7M                          Gbm7/11 B7/F  E7M
Mais bonito eu nunca vi e ela aportou
          E4/7
Descolada, assanhada na calçada
B7M                      Gbm7/11          B7M                 Gbm7/11 E7M
Entre trinques e siris, e o pôr do sol
B7M                          Gbm7/11B7/F E7M
Foi aí que eu descobri o meu futebol
        E4(7/9)           E4/7               Gb4/7
Bobeou entrei com bola e tudo é gol, oh uo

B7M                   E4(7/9) A7M                  Gb4/7
Everybody let's go, go, gol, gol, gol, gol, gol
B7M                   E4(7/9) A7M                  Gb4/7
Everybody let's go, go, gol, gol, gol, gol, gol
B7M                   E4(7/9) A7M                  Gb4/7
Everybody let's go, go, gol, gol, gol, gol, gol

B7M                   E4(7/9) A7M                  Gb4/7
Everybody let's go, go, gol, gol, gol, gol, gol

----------------- Acordes -----------------
A7M = X 0 2 1 2 0
B7M = X 2 4 3 4 2
E4(7/9) = X X 2 2 3 2
E4/7 = 0 2 0 2 0 X
E7M = X X 2 4 4 4
Gb4/7 = 2 4 2 4 2 X
Gbm7/11 = 2 X 2 2 0 X
