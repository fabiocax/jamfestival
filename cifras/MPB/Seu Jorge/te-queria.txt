Seu Jorge - Te Queria

Intro: Am7  D7  G7

Am7                                   D7
   Eu te queria eu te queria para mim
         G7
Nega não vá
Am7                                   D7
   Eu te queria eu te queria para mim
         G7
Nega não vá
F7                  Am7  D7/9
   Barba e cabelo black
F7                     Am7  G#m7  Gm7  C7/9
   Sempre fui o seu moleque
F7              E7              Am7
   Que fica sambando numa perna só
     C         F7
Só, só, só, só
             E7               Am7
Eu sou um moleque no black no pé
     C         F7
Pé, pé, pé, pé

             E7                 Am7  D7  G7
Que fica sambando numa perna só

Am7  D7  G7

Am7                                   D7
   Eu te queria eu te queria para mim
         G7
Nega não vá
Am7                                   D7
   Eu te queria eu te queria para mim
         G7
Nega não vá

F7                     Am7   D7/9
  Já dancei samba de breck
F7                  Am7  G#m7  Gm7  C7/9
   No tempo da sua avó
F7              E7              Am7
   Que fica sambando numa perna só
     C         F7
Só, só, só, só
             E7               Am7
Eu sou um moleque no black no pé
     C         F7
Pé, pé, pé, pé
             E7              Am7   C   F7   E7
Que fica sambando numa perna só

Am7  C   F7   E7...

    Am7          C        C7/9        D7        D7/9

   ||||||     ||||||     ||||||     ||||||     ||||||
   ||||||     ||||||     ||1|||     ||||||     ||||||
   ||||||     |1||||     |2|34|     ||||1|     ||||||
   ||||||     ||||||     ||||||     ||2|||     ||1|||
   1|234|     ||234|     ||||||     |3|4||     |2|34|
   Oxooox     xOooox     xOooox     xOooox     xOooox

     E7         F7         G7         Gm7       G#m7

   |||1||     <-----     ||||||     ||||||     ||||||
   |2||||     |||2||     ||||||     ||||||     ||||||
   ||||||     |3||||     <-----     1|234|     ||||||
   ||||||     ||||||     |||2||     ||||||     1|234|
   ||||||     ||||||     |3||||     ||||||     ||||||
   Oooooo     Oooooo     Oooooo     Oxooox     Oxooox

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
C7/9 = X 3 2 3 3 X
D7 = X X 0 2 1 2
D7/9 = X 5 4 5 5 X
E7 = 0 2 2 1 3 0
F7 = 1 3 1 2 1 1
G#m7 = 4 X 4 4 4 X
G7 = 3 5 3 4 3 3
Gm7 = 3 X 3 3 3 X
