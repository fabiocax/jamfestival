Seu Jorge - Rebel Rebel

D7M/9         E7M/9
Você não sabe se vai ou vem
Pouco me importa se o dinheiro é seu
Ei baby se o cabelo é legal
Moda na gringa é feliz natal

Se equivocou mas ficou tudo bem
Agora diz que está na onda zen
Ei baby você venceu
Passe amanhã e pegue o que é seu

A7M          D7M/9
A maquiagem vai desmanchar
Bm7                  E7/9
Para o seu medo aparecer

D7M/9         E7M/9
Zero a zero, agora eu vou
Você deu mole então eu marco um gol

Zero a zero, você venceu

Passe amanhã e pegue o que é seu

2x

----------------- Acordes -----------------
A7M = X 0 2 1 2 0
Bm7 = X 2 4 2 3 2
D7M/9 = X 5 4 6 5 X
E7/9 = X X 2 1 3 2
E7M/9 = X 7 6 8 7 X
