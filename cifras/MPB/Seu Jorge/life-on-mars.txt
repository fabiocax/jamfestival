Seu Jorge - Life On Mars

  F      F/E         Eb5-
 Muitas vezes o coração
         D             Gm
 Não consegue compreender
          F                 C7
 O que a mente não faz questão
                             F
 E nem tem forças para obedecer
          F/E            Eb5-
 Quantos sonhos já destruí
       D               Gm
 E deixei escapar das mãos
         F                 C
 E se o futuro assim permitir
                        Eb
 Não pretendo viver em vão
       C/E             Fm
 Meu amor não estamos sós
         G#/F#               C#9
 Tem um mundo a esperar por nós
       A5-            Bbm
 Do infinito do céu azul

  B
 Pode ter vida em marte
        Bb
 Entao vem cá
     Eb
 Me dá a sua língua
 Gm
 Então vem
      F#º          F
 Eu quero abraçar vocë
  Fm               Cm
 Seu poder vem do sol
  Eb
 Minha medida
  Bb
 Meu bem
    Eb
 Vamos viver a vida
 Gm
 Então vem
  F#º                   F
 Se não eu vou perder quem sou
  Fm              Cm
 Vou querer me mudar
  Eb               Gm
 Para uma Life on Mars

( Bm/F#  F  Em  F  F#º Gm  G#º Am  Bb  Bbm  Eb  F )

 Gm
 Entao vem
   F#º                   F
 Que eu quero abraçar você
  Fm               Cm
 Seu poder vem do sol
  Eb
 Minha medida
  Bb
 Meu bem
    Eb
 Vamos viver a vida
 Gm
 Então vem
  F#º                      F
 Senão eu vou perder quem sou
       Fm
 Ou ou ou
                  Cm
 Vou querer me mudar
  Eb               Gm
 Para uma Life on Mars

Bm/F#  F Em  F  F#º  Gm  G#º  Am  Bb  Bbm  Eb  F
Aaaaaaaahhhhhhh

----------------- Acordes -----------------
A5- = X 0 7 6 4 X
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
Bb = X 1 3 3 3 1
Bbm = X 1 3 3 2 1
Bm/F# = X X 4 4 3 2
C = X 3 2 0 1 0
C#9 = X 4 6 6 4 4
C/E = 0 3 2 0 1 0
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
Eb = X 6 5 3 4 3
Eb5- = X 6 7 8 8 6
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F#º = 2 X 1 2 1 X
F/E = 0 X X 2 1 1
Fm = 1 3 3 1 1 1
G#/F# = 2 X 1 1 1 X
G#º = 4 X 3 4 3 X
Gm = 3 5 5 3 3 3
