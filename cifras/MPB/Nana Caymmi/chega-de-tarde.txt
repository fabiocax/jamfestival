Nana Caymmi - Chega de Tarde

Intro: Gm6 Ebm6(9) D7(b9) (2x)

         Gm6      Ebm6(9)
Chega de tarde
       D7(b9)         Gm6        Ebm6(9)
Ele se vai sem muito alarde
          D7(b9)    Am7(b5)       D7(b9)
Vai encontrar a sua solidão
     D7          Gm6   Ebm6(9) D7(b9)
Ele sai pra passear

         Gm6      Ebm6(9)
Chega de tarde
      D7(b9)          Gm6      Ebm6(9)
A luz do sol ainda arde
         D7(b9)        Am7(b5)       D7(b9)
Leva um segredo no seu coração
        D7           G6
Leva a tarde a procurar
    Cm7        G7(b13)
A representar


         C7M    F#m7(b5)
Chega de tarde
          B7(b9)          Em7      C#m7(b5)
Quero que Deus me livre e guarde
        F#7(b13)   Bm7    C#m7 Dm7
Do o fantasma do ciúme
      G7(b9)       C7M     D7(b9)
Dos delírios da paixão


         Gm6      Ebm6(9)
Chega de tarde
         D7(b9)    Gm6      Ebm6(9)
Deixa no ar uma saudade
      D7(b9)       Am7(b5)       D7(b9)
Desaparece entre a multidão
    D7           G6
Ele sai pra passear

----------------- Acordes -----------------
Am7(b5) = 5 X 5 5 4 X
B7(b9) = X 2 1 2 1 X
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
C#m7(b5) = X 4 5 4 5 X
C7M = X 3 2 0 0 X
Cm7 = X 3 5 3 4 3
D7 = X X 0 2 1 2
D7(b9) = X 5 4 5 4 X
Dm7 = X 5 7 5 6 5
Ebm6(9) = X 6 4 5 6 X
Em7 = 0 2 2 0 3 0
F#7(b13) = 2 X 2 3 3 2
F#m7(b5) = 2 X 2 2 1 X
G6 = 3 X 2 4 3 X
G7(b13) = 3 X 3 4 4 3
G7(b9) = 3 X 3 1 0 X
Gm6 = 3 X 2 3 3 X
