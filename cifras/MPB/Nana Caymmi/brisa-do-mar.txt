Nana Caymmi - Brisa do Mar

Intro: G7M  C7/9  Am7/11  D7/9-

G7M              G6              E4/7/9  E7/9-
Brisa do mar, confidente do meu coração
     Am5-/7   D4/7/9    G#7M    G7M Am7 D7/9-
Me sinto capaz, de uma nova ilusão
      G7M
Que também passará
      G6              E4/7/9  E7/9-
Como ondas na beira do cais
Am5-/7              D4/7/9
Juras, promessas, canções
        G#7M     G7M
Mas por onde andarás
    G4/7/9         G7/9-
Pra ser feliz, não há uma lei
    C7/9+
Não há porém sempre é bom
A4/7/9        A7/9-
Viver a vida, atenta ao que diz
    D4/7/9          D7/9-      G#7/9+
No fundo do peito o seu coração

   G7M                G6         E4/7/9        E7/9-
E saber entender os segredos que ele ensinar
     Am5-/7     D4/7/9    G#7M  G7M
Mensagens sutis, como a brisa do mar

----------------- Acordes -----------------
A4/7/9 = 5 X 5 4 3 X
Am5-/7 = 5 X 5 5 4 X
Am7 = X 0 2 0 1 0
Am7/11 = 5 X 5 5 3 X
C7/9 = X 3 2 3 3 X
C7/9+ = X 3 2 3 4 X
D4/7/9 = X 5 5 5 5 5
E4/7/9 = X X 2 2 3 2
G#7/9+ = X X 6 5 7 7
G#7M = 4 X 5 5 4 X
G4/7/9 = 3 X 3 2 1 X
G6 = 3 X 2 4 3 X
G7M = 3 X 4 4 3 X
