Zé Ramalho - Entre a Serpente e a Estrela

(intro)  Bb F D# F Bb F D# F

E|-----------3---------|--------------------|---------------------|---------------|
B|--6-------------6----|--3-----------------|---------------------|---------------|
G|---------------------|---------------5----|--3-------------3----|---------------|
D|---------------------|--------------------|-----------5---------|--5---3--------|
A|---------------------|--------------------|---------------------|---------------|
E|---------------------|--------------------|---------------------|---------------|

E|-----------3---------|--------------------|---------------------|---------------|
B|--6-------------6----|--3-----------------|---------------------|---------------|
G|---------------------|---------------5----|--3-------------3----|---------------|
D|---------------------|--------------------|-----------5---------|--5---3--------|
A|---------------------|--------------------|---------------------|---------------|
E|---------------------|--------------------|---------------------|---------------|

Bb                 F         D#       Bb
   Há um brilho de faca onde o amor vier
Bb                 Dm       D#        F
   E ninguém tem o mapa da alma da mulher
           D#            F                     Bb  F  Gm
   Ninguém sai com o coração sem sangrar ao tentar reve-la

Bb              F                 D#
   Um ser maravilhoso, entre a serpente e a estrela

( Bb F D# Bb F )

Bb                     F             D#           Bb
   Um grande amor no passado se transforma em aversão
Bb                  D       D#         F
   E os dois lado à lado corroem o coração
        D#            F                   Bb            F        Gm
   Não existe saudade mais cortante que o de um grande amor ausente
   Bb            F                 D#
   Dura feito diamante corta a ilusão da gente

( Bb F D# D B )

C                  G         F           C
   Toco a vida pra frente fingindo não sofrer
C                 Em      F              G
   Mas o peito dormente espera um bem querer
     F             G                C      G      Am
   E sei que não será surpresa se o futuro me trouxer
C               G            F               C
   O passado de volta num semblante de mulher

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D = X X 0 2 3 2
D# = X 6 5 3 4 3
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
