Zé Ramalho - Um Pequeno Xote

[Intro] Bm  Bm/A  G7+  Em  Bm  Bm/A  G7+  Em  Bm

       Em                Bm
Toda pessoa merece felicidade
       Em                   Bm
Uma vontade danada de bem viver
          Em                     Bm
O mundo é bom quando se for entender
          A           Bb°      Bm
Que tudo vinga quando se pode querer
          Em                     Bm
O mundo é bom quando se for entender
          A           Bb°      Bm
Que tudo vinga quando se pode querer

 Em        F#      Bm  Bm/A
Deixe o fogo na metade
 Em         A7       D
Deixe a cidade derreter
G          F#        Bm  Bm/A
Que todo povo tem saudade

        Em          F#        Bm
Minha vontade fica dentro de você
 Em        F#      Bm  Bm/A
Deixe o fogo na metade
 Em         A7       D
Deixe a cidade derreter
G          F#        Bm  Bm/A
Que todo povo tem saudade
        Em          F#        Bm
Minha vontade fica dentro de você

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bb° = X 1 2 0 2 0
Bm = X 2 4 4 3 2
Bm/A = X 0 4 4 3 X
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
G7+ = 3 X 4 4 3 X
