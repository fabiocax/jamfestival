Zé Ramalho - Vila do Sossego

Intro: G5  G7+  C7+
       Bm  Am  E

E|7-------------------7----7-------------------7/10-7---------------10-8\7--------|
B|--10-10\8-------8-----10---10\8-------8/10-8--------10-8/10---8-7--------10\8---|
G|----------9-7/9---9-------------9-7/9-----------------------9-----------------9-|
D|--------------------------------------------------------------------------------|
A|--------------------------------------------------------------------------------|
E|--------------------------------------------------------------------------------|

E|7-------------------7----7-------------------7/10-7---------------10-8\7--------|
B|--10-10\8-------8-----10---10\8-------8/10-8--------10-8/10---8-7--------10\8---|
G|----------9-7/9---9-------------9-7/9-----------------------9-----------------9-|
D|--------------------------------------------------------------------------------|
A|--------------------------------------------------------------------------------|
E|--------------------------------------------------------------------------------|

            G5                 G7+       C7+
Oh, eu não sei se eram os antigos que diziam
           G5         G7+        C7+
Em seus papiros Papillon já me dizia

     Am           Am/G           D/F#
Que nas torturas toda carne se trai
     Am                  Am/G             D/F#
Que normalmente, comumente, fatalmente, felizmente,
       F                  C                   G            Am  E
Displicentemente o nervo se contrai, oh,oh,oh,oh com precisão

E|7-------------------7----7-------------------7/10-7---------------10-8\7--------|
B|--10-10\8-------8-----10---10\8-------8/10-8--------10-8/10---8-7--------10\8---|
G|----------9-7/9---9-------------9-7/9-----------------------9-----------------9-|
D|--------------------------------------------------------------------------------|
A|--------------------------------------------------------------------------------|
E|--------------------------------------------------------------------------------|

       G5         G7+       C7+
Nos aviões que vomitavam pára-quedas
       G5         G7+        C7+
Nas casamatas, casas vivas, caso morras
Am          Am/G             D/F#
E nos delírios, meus grilos, temer
     Am              Am/G                    D/F#
O casamento, o rompimento, o sacramento, o documento
     F                  C             G                  Am  E
Como um passatempo quero mais te ver, oh,oh,oh,oh,oh com aflição

E|7-------------------7----7-------------------7/10-7---------------10-8\7--------|
B|--10-10\8-------8-----10---10\8-------8/10-8--------10-8/10---8-7--------10\8---|
G|----------9-7/9---9-------------9-7/9-----------------------9-----------------9-|
D|--------------------------------------------------------------------------------|
A|--------------------------------------------------------------------------------|
E|--------------------------------------------------------------------------------|

        G5          G7+        C7+
Meu treponema não é pálido nem viscoso
       G5          G7+        C7+
E os meus gametas se agrupam no meu som
Am           Am/G         D/F#
E as querubinas, meninas, rever
    Am          Am/G                 D/F#
Um compromisso submisso, rebuliço no cortiço
      F               C              G       Am E
Chame o padre "Ciço" para me benzer, oh,oh,oh,oh,oh,oh com devoção

E|7-------------------7----7-------------------7/10-7---------------10-8\7--------|
B|--10-10\8-------8-----10---10\8-------8/10-8--------10-8/10---8-7--------10\8---|
G|----------9-7/9---9-------------9-7/9-----------------------9-----------------9-|
D|--------------------------------------------------------------------------------|
A|--------------------------------------------------------------------------------|
E|--------------------------------------------------------------------------------|

E|7-------------------7----7-------------------7/10-7---------------10-8\7--------|
B|--10-10\8-------8-----10---10\8-------8/10-8--------10-8/10---8-7--------10\8---|
G|----------9-7/9---9-------------9-7/9-----------------------9-----------------9-|
D|--------------------------------------------------------------------------------|
A|--------------------------------------------------------------------------------|
E|--------------------------------------------------------------------------------|

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am/G = 3 X 2 2 1 X
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C7+ = X 3 2 0 0 X
D/F# = 2 X 0 2 3 2
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G5 = 3 5 5 X X X
G7+ = 3 X 4 4 3 X
