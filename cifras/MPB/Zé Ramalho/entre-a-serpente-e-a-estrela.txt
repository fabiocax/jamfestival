Zé Ramalho - Entre a Serpente e a Estrela

[Intro] C  Em  F  G
        C  Em  F  G

C                    Em
  Há um brilho de faca
F               C
  Onde o amor vier
C                  Em
  E ninguém tem o mapa
   F           G
Da alma da mulher
         F             G
Ninguém sai com o coração
                   C   Em  F
Sem sangrar ao tentar re--vê-la
C                   Em
  Um ser maravilhoso
            F
Entre a serpente e a estrela

( C  Em  F  G )


C                         Em
  Um grande amor no passado
     F                C
Se transforma em aversão
C                   Em
  E os dois lado à lado
    F          G
Corroem o coração
      F             G
Não existe saudade mais cortante
       C             Em    F
Que a de um grande amor ausente
 C             G
Dura feito diamante
     F      G
Corta a ilusão da gente

( C  Em  F  G  A )

D                   F#m
  Toco a vida pra frente
 G               D
Fingindo não sofrer
D                 F#m
  Mas o peito dormente
   G              A
Espera um bem querer
   G             A
E sei que não será surpresa
      D      F#m    G
Se o futuro me trouxer
D               F#m
  O passado de volta
         G     A     D
Num semblante de mulher

D               F#m
  O passado de volta
         G     A     D
Num semblante de mulher

( D  F#m  G  A )
( D  F#m  G  A )

   G             A
E sei que não será surpresa
      D      F#m    G
Se o futuro me trouxer
D               F#m
  O passado de volta
         G     A     D
Num semblante de mulher

D               F#m
  O passado de volta
         G     A     D
Num semblante de mulher

[Final] D  F#m  G  A
        D  F#m  G  A
        D  F#m  G  A  D

----------------- Acordes -----------------
A = X 0 2 2 2 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
