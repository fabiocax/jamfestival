Zé Ramalho - Lamento Sertanejo

Intro: D7  Am  Am/G

       D/F#
Por ser de lá
      F7+           Am
Do sertão, lá do serrado
         B° Am/C C#° Dm
Lá do interior do mato
    G7            C7+    Gm7
Da caatinga, do roçado
 C7             F7+
Eu quase não saio
      E              Am
Eu quase não tenho amigo
 Am/G               D/F#
Eu quase que não consigo
           F7+        E          Am
Viver na cidade sem ficar contrariado

(Intro)


Am/G    D/F#
Por ser de lá
     F7+          Am
Na certa por isso mesmo
           B°  Am/C C#° Dm
Não gosto de cama mole
         G7           C7+
Não sei comer sem torresmo
Gm7 C7        F7+
 Eu quase não falo
 Em                  Am
Eu quase não sei de nada
 Am/G                D/F#
Sou como rês desgarrada
            F7+        Em        Am
Nessa multidão boiada caminhando à esmo

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am/C = X 3 2 2 5 X
Am/G = 3 X 2 2 1 X
B° = X 2 3 1 3 1
C#° = X 4 5 3 5 3
C7 = X 3 2 3 1 X
C7+ = X 3 2 0 0 X
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F7+ = 1 X 2 2 1 X
G7 = 3 5 3 4 3 3
Gm7 = 3 X 3 3 3 X
