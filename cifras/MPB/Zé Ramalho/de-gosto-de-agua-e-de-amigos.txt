Zé Ramalho - De Gosto, de Água e de Amigos

D                             Bm
Nada tem o gosto do que nunca acaba
G         B7                  Em
É como beber água na casa de amigos
G          Gm          F#7          Bm
É como se abrigar dos ventos e dos perigos
E           A7                    D     G A7 D
É como se sentir no chão e bem guardado
D                                 Bm
Porque mesmo o tempo não desfaz a trama
G           B7                     Em
E as formas com que o mundo feio nos difama
G           Gm           F#7          Bm
E as armas que vêm contra nós, durante a vida
E       A7                 D
Nada valerão na casa dos amigos

Bm                C#m              F#7
E nas intempéries, pedras dos caminhos
Bm                C#m            F#7
Nos rodamoinhos de nossas caminhadas

Am             D7           G        Gm
No sol dos desertos, de outros desencantos
F#7          Bm   E           A7                D
Achar um recanto que nos dê a noite, dias e pousadas
Bm                C#m               F#7
E toda segurança, que descansa no corpo
Bm              C#m           F#7
O agradecimento, que brota na alma
Am           D7       G         Gm
Fazem do ardor da luta, um rude oposto
F#7          Bm    E          A7              D     F#
Extrair do rosto, o estranho gosto do doce da água
G           A7                  ( G A7 D )
O estranho gosto do doce da água.



----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
Bbº = X 1 2 0 2 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
C#m7b5 = X 4 5 4 5 X
C/G = 3 3 2 X 1 X
D = X X 0 2 3 2
D/A = X 0 X 2 3 2
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F# = 2 4 4 3 2 2
F#7 = 2 4 2 3 2 2
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
F/A = 5 X 3 5 6 X
G = 3 2 0 0 0 3
G/A = 5 X 5 4 3 X
Gm = 3 5 5 3 3 3
Gm6 = 3 X 2 3 3 X
