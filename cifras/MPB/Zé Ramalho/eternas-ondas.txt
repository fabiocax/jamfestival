Zé Ramalho - Eternas Ondas

Intro: Bm  D F#m C#m5-/7 F# Bm

 Em                             F#              Bm  Em
Quanto tempo temos antes de voltarem aquelas ondas
                            A     F#          Bm  D
Que vieram como gotas em silêncio   tão furioso
                            F#        C#m5-/7
Derrubando homens entre outros animais
              F#              Bm    Em
Devastando a sede desses matagais
                         F#
Devorando árvores, pensamentos seguindo
   Bm    Em                               A    F#           Bm  D
A linha     do que foi escrito pelo mesmo lábio    tão furioso
                              F#      C#m5-/7
E se teu amigo vento não te procurar
             F#               Bm      D
É porque multidões ele foi arrastar
                              F#      C#m5-/7 F# G G# F# F#7 Bm
E se teu amigo vento não te procurar

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C#m5-/7 = X 4 5 4 5 X
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
F#7 = 2 4 2 3 2 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
