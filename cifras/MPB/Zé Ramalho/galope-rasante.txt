Zé Ramalho - Galope Rasante

(intro)  D  ( Gm  F  F#º )

  Gm
A sombra que me move
    F        F#º
Também me ilumina
    Gm
Me leve nos cabelos
    F         F#º
Me lave na piscina

   Dm
Em cada ponto claro
  D#
Cometa que cai no mar
   Dm
Em cada cor diferente
    D#
Que tente me clarear

   Gm              Gm/F
É noite que vai chegar

   Gm/E            Gm/D#
É claro que é de manhã
   Gm       Gm/F   Gm/E   Gm/D#
É moça e anciã

  Gm
O pêlo do cavalo
   F          F#º
O vento pela crina
  Gm
O hábito no olho
   F         F#º
Veneno, lamparina

   Dm
Debaixo de sete quedas
    D#
Querendo me levantar
   Dm
Debaixo do teu cabelo
   D#
A fonte de se banhar

   Gm             Gm/F
É ouro que vai pingar
    Gm/E        Gm/D#
Na prata do camelô
   Gm           Gm/F   Gm/E   Gm/D#
É noite do meu amor

   Gm              Gm/F
É noite que vai chegar
   Gm/E            Gm/D#
É claro que é de manhã
   Gm       Gm/F   Gm/E   Gm/D#
É moça e anciã

(solo) ( Gm  F  F#º ) ( Dm  D# ) (Gm  Gm/F  Gm/E  Gm/D#)

----------------- Acordes -----------------
D = X X 0 2 3 2
D# = X 6 5 3 4 3
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
F#º = 2 X 1 2 1 X
Gm = 3 5 5 3 3 3
Gm/D# = 3 6 5 3 3 3
Gm/E = 0 5 5 3 3 3
Gm/F = X X 3 3 3 3
