Zé Ramalho - S.O.S.

Intro: G A C G D

E|----------------------------------------------------------|
B|--8-8-10/12-10-8------10/12-10-8------10/12-10-8-8--------|
G|-----------------9-9-------------9-9----------------7/9-7-|
D|----------------------------------------------------------|
A|----------------------------------------------------------|
E|----------------------------------------------------------|

 Hoje é domingo, missa e praia, céu de anil...
      C
 Tem sangue no jornal
     D
 Bandeiras na avenida Brasil
G                          A
 Lá por detrás da triste e linda Zona Sul
     C
 Vai tudo muito bem
    D                          G
 Formigas que trafegam sem porquê


E|--------------------------|
B|------3-------------------|
G|--2/4---2p0---------------|
D|------------2~--2-2p0-----|
A|----------------------2---|
E|------------------------3-|

 E das janelas destes quartos de pensão
 C                    D                       G
 Eu, como vetor, tranqüilo tento uma transmutação.

           C       D        Bm
 Ô ô seu moço, do disco voador
     Em        C       D          G
 Me leve com você, pra onde você for
          C         D             Bm
 Ô ô seu moço, mas não me deixe aqui
    Em                C           D         G
 Enquanto eu sei que tem tanta estrela por aí...

 (Intro:)

G                    A
 Andei rezando para Totens e Jesus
    C
 Jamais olhei pro céu
      D
 Meu disco voador além
G                   A                 C
 Já fui macaco em domingos glaciais, Atlantas colossais...
     D                       G
 Que eu não soube como utilizar

E|--------------------------|
B|------3-------------------|
G|--2/4---2p0---------------|
D|------------2~--2-2p0-----|
A|----------------------2---|
E|------------------------3-|

                          A
 E nas mensagens que nos chegam sem parar
      C
 Ninguém pode notar
    D                         G
 Estão muito ocupados pra pensar

           C       D        Bm
 Ô ô seu moço, do disco voador
     Em        C       D          G
 Me leve com você, pra onde você for
          C         D             Bm
 Ô ô seu moço, mas não me deixe aqui
    Em                C           D         G
 Enquanto eu sei que tem tanta estrela por aí...


    Em                C    |
 Enquanto eu sei que tem   |
           D        G      | 2x
 Tanta estrela por aí...   |


 Solo: G A C G

E|---------------------------------------------------------------------------|
B|--8-8-10/12-10-8------10/12-10-8------10/12-10-8-8------3------------------|
G|-----------------9-9-------------9-9----------------2/4---2p0--------------|
D|--------------------------------------------------------------2~-2-2p0-----|
A|-----------------------------------------------------------------------2---|
E|-------------------------------------------------------------------------3-|

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
