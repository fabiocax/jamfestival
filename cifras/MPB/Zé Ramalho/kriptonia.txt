Zé Ramalho - Kriptonia

[Intro]  D  F#m

 D                            F#m7
    Não admito que me fale assim
D                            F#m7
  Eu sou o seu décimo-sexto pai
Bm                                           F#m7
  Sou primogênito do teu avô, primeiro curandeiro
Bm                                      F#m7
  Alcoviteiro das mulheres que corriam sob o teu nariz

  A                                   C#7
 Me deves respeito, pelo menos dinheiro
 D                                     A
 Esse é o cometa fulgurante que espatifou

Refrão:
 G                 D                        B7
  Um asteróide pequeno que todos chamam de Terra
 G                 D                        B7
  Um asteróide pequeno que todos chamam de Terra


( Bm  A  G  E )
( Bm  A  G  E  A  Bm )
( A )

D                          F#m7
    De Kryptônia desce teu olhar
D                            F#m7
  E quatro elos prendem tua mão
Bm                                               F#m7
  Cala-te boca, companheiro, vá embora, que má criação!
Bm                                      F#m7
  De outro jeito não se dissimularia a suma criação

F#m7 G#m7 A                                     C#7
        E foi o silêncio que habitou-se no meio
D                                      A
  Ele é o cometa fulgurante que espatifou

 G                 D                        B7
  Um asteróide pequeno que todos chamam de Terra
 G                 D                        B7
  Um asteróide pequeno que todos chamam de Terra

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C#7 = X 4 3 4 2 X
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
G = 3 2 0 0 0 3
G#m7 = 4 X 4 4 4 X
