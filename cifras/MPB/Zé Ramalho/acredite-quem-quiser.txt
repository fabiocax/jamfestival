Zé Ramalho - Acredite Quem Quiser

A                     D            A
Filhos de Jacó, testemunhas de Jeová
             F#m        B7         E
Bichos que habitam esta arca de Noé
A
Salvem o que puder salvar
        D                  A
Pois o homem veio pra acabar
    Bm        E            A   D  E
E o fim ta difícil de evitar
   A
   Pai, Pai, Senhor               /
           D            A         /
   Jogue chuva sobre mim          /
                 F#m        B7    /
   Salve a minha terra e renove   /
            E                     /
   Este jardim                    /
   A                              /   REFRÃO
   Pai, Pai, Senhor               /
          D            Dm         /
   Jogue chuva sobre mim          /

   A              B7         E    /
   Salve a minha terra e renove   /
            A                     /
   Este jardim                    /
A
Muitos falam de amor
 D                 A
E não sei fazer o mal
             F#m         B7       E
Mas o que se vê é uma mentira social
A
Uns apelam pra razão
D                  A
Outros para o coração
     Bm         E
Mas beijam como Judas
        A   D  E
Na traição


(REFRÃO 2X)


SOLO:  A D A F# B7 E

A
Acredite quem quiser
D                 A
Nesta falta de moral
                 F#m
O povo é quem se dana
       B7        E
Nesta luta desigual
A
Não se pode confiar
D                       D
Os homens querem se eleger
        Bm            E          A   D  E
E essa gente está cansada de sofrer

(REFRÃO)


Musica cifrada por
RICK MORA


----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
