Zé Ramalho - O Que Vale Para Sempre

Am
Int. G Am F C    2x
     C G F E E7

Am
Diga-me como é que foi
                    C
Aquilo que voçê me deu
                 G
Onde é que encontrou
               Am
Como é que conheceu
            G
Minha fascinação
           Am
Parece não morreu
F       E     Am
E vai ser para sempre...
Am
Não aqui,quem sabe ali
                   C
Deitar com ela,lá vou eu

                G
E vou com o coração
              Am
Com tudo que tiver
            G
Como se furacão
            Am
Como voçê quiser
F     E     Am         G7
E voçê para sempre

          Am      F      C
E se o brilho nos olhos tiver
          Am      F      C
É a razão pro que der e vier
           G            F
E o que quiser,e o que fiser
    E
E o que puser
        E7              Am      E
Tudo que é,nesse mundo será...


Am
Nunca fique na visão
                     C
Naquela que não pode ver
             G
Que tudo é e são
            Am
As horas de viver
                 G
Que valem para sempre
            Am
O que voçe fizer
F      E           Am
E seja algo que foi e é.



----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
