Zé Ramalho - Zyliana

Intro: C Am C Am G Am G F E7

 Am                  G             F
No tempo em que eu andava pela poeira
  F7                          E7
Daquele velho brejo de onde rumei
       Am           G            F
Não tinha tanta mágoa rente no olho
   F7                         E7
De alegrias é só do que eu chorei

Am
Não andava pela rua
Am/G
Credo cruz ave maria
F#m7/5-                     E7 D/F# E/G# Am7
Nada sei do que eu queria saber
Bm7/5-          E7           Am G F E Am
Nada sei do que eu queria saber

              G               F
O homem já procura agora um caminho

    F7                       E7
Que o leve de volta para um lar
  Am               G           F
A foz de um grande rio me arrastou
  F7                     E7
E num toco boiando fui lutar

Am
Netuno com seu tridente
Am/G
Proteja-me consciente
F#m7/5-                      E7 D/F# E/G# Am
Na queda que o rio corre pro mar
    Bm7/5-       E7          Am
Na queda que o rio corre pro mar
   G      F
Oh oh, Oh oh,
   E      Am
Oh oh, Oh oh

               G           F
O peso desses anos me acordou
   F7                     E7
De um sono profundo de condor
   Am           G          F
Se você não tem nada pra fazer
 F7                     E7
Amigo nada tens a se perder

Am
Feche o quarto com cimento
  Am/G
E veja que mundo cinzento
  F#m7/5-             E7 D/F# E/G# Am
E como ficou o verbo amar
  Bm7/5-     E7       Am
E como ficou o verbo amar

   G      F
Oh oh, Oh oh,
   E      Am
Oh oh, Oh oh
   G      F
Oh oh, Oh oh,
   E  Am
Oh oh

               G           F
O peso desses anos me acordou...

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am/G = 3 X 2 2 1 X
Am7 = X 0 2 0 1 0
Bm7/5- = X 2 3 2 3 X
C = X 3 2 0 1 0
D/F# = 2 X 0 2 3 2
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
F#m7/5- = 2 X 2 2 1 X
F7 = 1 3 1 2 1 1
G = 3 2 0 0 0 3
