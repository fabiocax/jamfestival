Zé Ramalho - Beira Mar

Intro: G  Em9

G                               Em
  Eu entendo a noite como um oceano
                                 G
Que banha de sombras o mundo de sol
C                        Am
A aurora que luta por um arrebol
                             Bm
De cores vibrantes e ar soberano
                            Am
Um olho que mira nunca o engano
                                   Em9
Durante o instante que vou contemplar
D                             Bm
Além, muito além onde quero chegar
                             G
Caindo a noite, me lanço no mundo
A                            F#7
  Além do limite do vale profundo
                               Bm
Que sempre começa na beira do mar

G                 Em    (G Em9)
 É na beira do mar

G                                        Em9
  Ói, por dentro das águas há quadros e sonhos
                                 G
E coisas que sonham o mundo dos vivos
C                                  Am
  Há peixes milagrosos, insetos nocivos
                               Bm
Paisagens abertas, desertos medonhos
                                 Am
Léguas cansativas, caminhos tristonhos
                             Em9
Qua fazem o homem se desenganar
D                                 Bm
  Há peixes que lutam para se salvar
                                G
Daqueles que caçam em mar revoltoso
A                                       F#7
  E outros que devoram com gênio assombroso
                               Bm
As vidas que caem na beira do mar
G                  Em9    (G Em9)
  É na beira do mar

G                                Em9
  E até que a morte eu sinta chegando
                                 G
Prossigo cantando, beijando o espaço
C                          Am
Além do cabelo que desembaraço
                           Bm
Invoco as águas a vir inundando
                               Am
Pessoas e coisas que vão se arrastando
                              Em9
Do meu pensamento já podem lavar
D                                 Bm
  Ah, no peixe de asas eu quero voar
                         G
Sair do oceano de tez poluída
A                                F#7
  Cantar um galope fechando a ferida
                              Bm
Que só cicatriza na beira do mar
G                  Em    (G Em9)
  É na beira do mar

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
Em9 = 0 2 4 0 0 0
F#7 = 2 4 2 3 2 2
G = 3 2 0 0 0 3
