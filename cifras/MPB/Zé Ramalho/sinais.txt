Zé Ramalho - Sinais

Intro: Bm

Bm
Sinais de que os tempos passaram

Passaram e mudaram demais

Sinais de que tudo mudaram

Viraram para frente e pra trás

 Em                        A
Sinais de que não temos culpa
               D7+
 de ficarmos assim
               Bm
Como simples mortais
G#°
Sinais de que não conhecemos
               G      F#
O valor dessa vida voraz


Bm
Sinais no teu rosto cansado

Calado, mesmo quando falais

Sinais de que vamos agora

Na hora desse sonho audaz

Em                       A
Sinais de que ainda dá tempo
                D7+
De chamar tua irmã
                  Bm
Que perdeu-se no tempo
                 G#°
De falar com a outra

Com teu homem querido
              G
Teu ator preferido
      F#
Quem mais?

Bm
Sinais de algum alienígena

Que faz sobre os campos sinais

Sinais que parimos um clone

Capaz de sermos imortais

Em                        A
Sinais que marcaram teu corpo
               D7+
Que tiraram as mãos
               Bm
Do destino que vais
G#°
Sinais de que ainda nascemos
                G
E não temos ao menos
   F#
A paz!
              (G  Em  Bm)
Oh, oh, oh, sinais

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D7+ = X X 0 2 2 2
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
G#° = 4 X 3 4 3 X
