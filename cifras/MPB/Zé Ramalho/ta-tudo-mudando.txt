Zé Ramalho - Ta Tudo Mudando

(capo 2ª casa - tom F#m)

(intro) Em    Am   B7   Em   B7

Em                                       Am
Um Preocupado, Andando Preocupado ninguém a minha frente
                        Em                               B7
Nada ao meu lado Uma mulher no meu colo E ela bebe champanhe
 Em                             Am
Pele alva olhos assassinos Olho pro céu com velho olhar de menino
           Em              B7          Em   B7
Estou bem vestido  Esperando o último trem
C                                         Em
Em pé num cada falso Com minha cabeça no laço
C                                         Em     B7
Tenho mais ou menos um segundo e meio Pra saber o que faço
Em                                   Am
Pessoas loucas, tempo estranho Estou trancado, eu não alcanço
            Em  B7                    Em     B7
Eu me importava        Mas tá tudo mudando
 Em                               Am
Esse lugar não me faz bem a saúde      E deveria estar em Hollywood

           Em                           B7
Por um segundo, eu Pensei ver algo se mover
   Em                                                   Am
Tome liçoes de dança Vá no candomblé Leve a vida como pode, Vá até onde der
Em                    B7               Em
So um tolo ia achar Que não há o que dizer
C                                             Em
Muita água embaixo da ponte Muitas outras coisas também
  Am                                              B7
não se levante cavalheiro Eu estou apenas de passagem
Em                                   Am
Pessoas loucas, tempo estranho Estou trancado, eu não alcanço
          Em     B7                 Em   B7
Eu me importava ......Mas tá tudo mudando

(solo)

 Em                                  Am
Andando milhas de estrada ruim Se a biblia esta correta O mundo vai explodir
  Em                       B7
Fico longe de mim até me perder
Em                              Am
A mão afaga A arma que atira A verdade desse mundo  Vem de uma grande mentira
Em              B7            Em
De mãos atadas Não se pode vencer
C                               Em
Seu Jose e Dona Lucia Eles pularam no lago
C                                     B7
Pra cometer esse erro  Eu não estou preparado
Em                                   Am
Pessoas loucas, tempo estranho Estou trancado, eu não alcanço
             Em B7               Em B7             Em B7            Em B7
Eu me importava Mas tá tudo mudando,  tá tudo mudando, tá tudo mudando, tá tudo mudando...

----------------- Acordes -----------------
Capotraste na 2ª casa
Am*  = X 0 2 2 1 0 - (*Bm na forma de Am)
B7*  = X 2 1 2 0 2 - (*C#7 na forma de B7)
C*  = X 3 2 0 1 0 - (*D na forma de C)
Em*  = 0 2 2 0 0 0 - (*F#m na forma de Em)
