Zé Ramalho - Visões de Zé Limeira Sobre o Final do Século XX

   A            C#m
Vejo discos de metal
    D                     E7
Pairando pelas noites do país
    A            C#m
Minhas loucas conclusões nada dizem
   D                      E7
Residem nos cabelos de Sansão
C#m  F#m                        B
Ah!  Deuses,  Astronautas me ajudam
          E7    D                 A    E7 A
a conseguir    O meu velo de mercúrio

  A          C#m
A imagem milenar
  D                           E7
Dos grandes dinossauros que domei
  A              C#m
Uma espaçonave é a tua residência
  D                         E7
Paciência,  mas o éter me chamou

C#m  F#m                     B
Ah!    Sou um panteísta sufocado
            E7    D               A
Pelas canções    Do acetato de mercúrio
 D                 A                     D                       A
E os terráqueos conseguiram finalmente conquistar sua terra maisgarrida
 D               A                 D                  A
Borboletas de acrílico puseram suas asas num cabide esquisito
          G           D             A
Gerou conflito entre as gerações febris
D                  A                   D                      A
Era um porco chovinista procurado pelo karma de lançar outro vapor
D              A                D                    A
Cogumelos nucleares iluminam as campinas do planeta abissal
       G              D          A
No carnaval dos seres brancos e azuis (bis)

                C#m
Foi eleito um faraó
   D                    E7
E longas catacumbas perfurei
 A                 C#m
Pelas plainas do sertão quase quente
    D                   E7
Correntes de platina separou
C#m    F#m                  B
        As águas do oceano encantado
            E7     D                 A
Que Deus criou    Pelas algas de mercúrio

  D             A
O meu velo de mercúrio
  D             A
O acetato de mercúrio
  D             A
Pelas algas de mercúrio

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
