Jorge Vercillo - Himalaia

(Versão ao Vivo)

(Intro - Solo)
E|--3-5-7-8-7-----------------------------------------------------------
B|--------------10---8/10/8--10---8--7 ------7/8/7--7-------7/8/75------
G|---------------------------------------11------------11---------------
D|----------------------------------------------------------------------
A|----------------------------------------------------------------------
E|----------------------------------------------------------------------

E|------5/7---7--7--------3/5---3---2---
B|--------------------------------------
G|--------------------------------------
D|--------------------------------------
A|--------------------------------------
E|--------------------------------------

E|--3-5-7-8-7-----------------------------------------------------------
B|--------------10---8/10/8--10---8--7 ------7/8/7--7-------7/8/75------
G|---------------------------------------11------------11---------------
D|----------------------------------------------------------------------
A|----------------------------------------------------------------------
E|----------------------------------------------------------------------


E|------5/7---5--3/5--3---2/3--2-----------
B|-----------------------------------------
G|-----------------------------------------
D|-----------------------------------------
A|-----------------------------------------
E|-----------------------------------------

[Intro:] Em D C D D#º

           Em  D                  C D D#º
O mundo inteiro está guardado em mim
       Em  D                   C D D#º
De Saravejo aos templos de Pequim
           Em D                 C D D#º
Não tenha medo de amar e ser feliz,
        Em D                C D G
É o segredo divino do aprendiz

                         Bm7      C
Quero andar nas ruas que há em mim,
                   D         G
Conhecer esquinas do coração,
                     Bm7      C
Desabar nos próprios botequins
                   D
Nos subúrbios da iluminação

      Em              D
Na leveza do ser a beleza é voar
       C
O Himalaia, o Himalaia, o Himalaia, o Himalaia,
       D          D#º
O Himalaia inteiro
    Em                  D
Alquimia é reter as montanhas no olhar,
       C
O Himalaia, o Himalaia, o Himalaia, o Himalaia,
       D          D#º
O Himalaia inteiro

           Em  D                C D D#º
O mundo inteiro está mudado em mim
         Em D                 C D G
Desde Soweto aos muros de Berlim

Sou um rio das mágoas que há em mim,
Cachoeira que desaba no mar,
Oceano das vidas que vivi
E de outras que virei navegar

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D#º = X X 1 2 1 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
