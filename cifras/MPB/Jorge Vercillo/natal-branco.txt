Jorge Vercillo - Natal Branco

[Intro] D7+/9  Dm7/9  D7+/9  Dm7/9

D7+/9
Lá
B7/9-       Em7/9
Onde a neve cai
Fº  F#m7
Si..nos
G7+
  Festejam
  A/G        F#m7  Em7  A7/9
A noite de Natal

     D7+/9
Os pinheiros
Am7   D7/9   G7+  Gm6
Bran..cos de neve
         F#m
São como torres
  B7/9-     Em6  Em5+  A7/9  A7/9-
De uma catedral


D7+/9
Lá
B7/9-       Em7/9
Onde a neve cai
Fº  F#m7
Sempre
G7+   F#m7
 Por sobre
  Em7  A7/13  D7+/9  G/A  A7/9
A lua tropi..cal

    D7+/9
O Natal
  Am7    D7/9  G7+  Gm6
É sempre ora..ção
   F#m7  B7/9-   Em7/9
Oração       de paz
A7/9  Bb7+
Universal

( G7/5+  Cm7/9  C#º  Dm7 )
( D#7+  F/D#  D7+/9  G/A  A7/9 )

     D7+/9
Os pinheiros
Am7   D7/9   G7+  Gm6
Bran..cos de neve
         F#m
São como torres
  B7/9-     Em6  Em5+  A7/9  A7/9-
De uma catedral

D7+/9
Lá
B7/9-       Em7/9
Onde a neve cai
Fº  F#m7
Sempre
G7+   F#m7
 Por sobre
  Em7  A7/13  D7+/9  G/A  A7/9
A lua tropi..cal

    D7+/9
O Natal
  Am7    D7/9  G7+  Gm6
É sempre ora..ção
   F#m7  B7/9-    Em7/9
Oração        de paz
A7/9
Universal

[Final] D7+/9  Dm7/9  D7+/9  Dm7/9

----------------- Acordes -----------------
A/G = 3 X 2 2 2 X
A7/13 = X 0 X 0 2 2
A7/9 = 5 X 5 4 2 X
A7/9- = 5 X 5 3 2 X
Am7 = X 0 2 0 1 0
B7/9- = X 2 1 2 1 X
Bb7+ = X 1 3 2 3 1
C#º = X 4 5 3 5 3
Cm7/9 = X 3 1 3 3 X
D#7+ = X X 1 3 3 3
D7+/9 = X 5 4 6 5 X
D7/9 = X 5 4 5 5 X
Dm7 = X 5 7 5 6 5
Dm7/9 = X 5 3 5 5 X
Em5+ = X X 2 0 1 0
Em6 = X 7 X 6 8 7
Em7 = 0 2 2 0 3 0
Em7/9 = X 7 5 7 7 X
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
F/D# = X X 1 2 1 1
Fº = X X 3 4 3 4
G/A = 5 X 5 4 3 X
G7+ = 3 X 4 4 3 X
G7/5+ = 3 X 3 4 4 3
Gm6 = 3 X 2 3 3 X
