Jorge Vercillo - Rima

Intro.: D7M  D7M(9)  G7M  A7(4/9) A7(9)
         D7M(9)  G7M  A7(4/9) A7(9)


D7M(9)           D7(9)
Filha  da poesia
              G7M    A7(4/9) A7(9)
Mãe da minha emoção
D7M(9)             D7(9)
Sombra dessa magia
             G7M    F#7
Luz da minha razão

Bm7
Jóia rara em Sevilha
Bm(7M)
Obra-prima em Milão
Em7
Isca pra essa armadilha
Em7(b5)       A7(4/9) A7(9)
Alvo do meu tesão


D7M(9)              D7(9)
Pouso de um delírio
             G7M   A7(4/9) A7(9)
Pena da inspiração
D7M(9)          D7(9)
Fruto ímpar do idílio
          G7M   F#7
Asa, imaginação

Bm7
Viga da amizade
Bm(7M)
Elo    do meu viver
Em7
Doce de uma saudade
Em7(b5)           A7(4/9) A7(9)
Chama   do meu querer
Em7
Casa da harmonia
        F#m7
Rua que eu   vou passar
G7M
Causa dessa alegria
E7(9)         A7(4/9) A7(9)
Cama do meu sonhar

D7M(9)               D7(9)
Tombo que se levanta
             G7M   A7(4/9) A7(9)
e suplanta a dor
D7M(9)        D7(9)
H2O    na planta
             G7M   F#7
Pilha do meu robô

Bm7
Monalisa sem tela
Bm(7M)
Novela sem tevê
Em7
Tudo que rima com ela
Em7(b5)                  A7(4/9) A7(9)
eu      falo com mais prazer
Em7
Filha da poesia
      F#m7
porta-voz do esplendor
    G7M
que eu descobri um dia
E7(9)         A7(4/9) A7(9)
dona do meu amor

D7M(9)             G7M
Yaô... Yaô, Yaô... Yaô...
A7(4/9)          A7(9)
Dona    do meu amor
D7M(9)             G7M
Yaô... Yaô Yaô...  Yaô...
A7(4/9)          A7(9)
Dona    do meu amor

  ( Introdução )

D7M(9)              D7(9)
Pouso de um delírio
             G7M   A7(4/9) A7(9)
Pena da inspiração
D7M(9)          D7(9)
Fruto ímpar do idílio
          G7M   F#7
Asa, imaginação

Bm7
Viga da amizade
Bm(7M)
Elo    do meu viver
Em7
Doce de uma saudade
Em7(b5)           A7(4/9) A7(9)
Chama   do meu querer
Em7
Casa da harmonia
        F#m7
Rua que eu   vou passar
G7M
Causa dessa alegria
E7(9)         A7(4/9) A7(9)
Cama do meu sonhar

D7M(9)               D7(9)
Tombo que se levanta
             G7M   A7(4/9) A7(9)
e suplanta a dor
D7M(9)        D7(9)
H2O    na planta
             G7M   F#7
Pilha do meu robô

Bm7
Monalisa sem tela
Bm(7M)
Novela sem tevê
Em7
Tudo que rima com ela
Em7(b5)                  A7(4/9) A7(9)
eu      falo com mais prazer
Em7
Filha da poesia
      F#m7
porta-voz do esplendor
    G7M
que eu descobri um dia
E7(9)         A7(4/9) A7(9)
dona do meu amor

D7M(9)             G7M
Yaô... Yaô, Yaô... Yaô...
A7(4/9)          A7(9)
Dona    do meu amor
D7M(9)             G7M
Yaô... Yaô Yaô...  Yaô...
A7(4/9)          A7(9)
Dona    do meu amor


     D7M      D7M(9)       G7M      A7(4/9)     A7(9)      D7(9)      F#7

5ª |

----------------- Acordes -----------------
A7(4/9) = 5 X 5 4 3 X
A7(9) = 5 X 5 4 2 X
Bm(7M) = X 2 4 3 3 2
Bm7 = X 2 4 2 3 2
D7(9) = X 5 4 5 5 X
D7M = X X 0 2 2 2
D7M(9) = X 5 4 6 5 X
E7(9) = X X 2 1 3 2
Em7 = 0 2 2 0 3 0
Em7(b5) = X X 2 3 3 3
F#7 = 2 4 2 3 2 2
F#m7 = 2 X 2 2 2 X
G7M = 3 X 4 4 3 X
