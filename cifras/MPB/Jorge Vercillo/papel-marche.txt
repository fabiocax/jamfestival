Jorge Vercillo - Papel Marchê

Intro:

Primeira Parte Intro:

Am7M(9)
F7M(6)  Fm7M(6)  Eb7M(6)  Ebm7M(6)
(passagem 1: C7(9)  C7  C7(4) C7(4/9) )

Passagem 1:

   C7(9)  C7  C7(4)  C7(4/9)
e|-------------------3-------------------|
B|-3------5---6------3-------------------|
G|-3------3---3------3-------------------|
D|---------------------------------------|
A|-3----------3--------------------------|
E|---------------------------------------|

Segunda Parte Intro 2x:

F7M  ( Dm7(9)  Dm7(11) )  C7M(5+)  A7(9)  A7(b13)

D7(9)  D7(4/9)  D7(9/#11)  G7(4/9)  C7M(9)
(passagem 1: C7(9)  C7  C7(4) C7(4/9) )

Primeira Estrofe:

F7M              Dm7(11)  Dm7(9)
    cores do mar,        festa do sol
C7M(5+)                    A7(9)
        Vida é fazer todo sonho brilhar
       Fm7
Ser feliz
                G7(13)  C7M      G/B
No seu colo dormir e depois acordar
       A7(9)   A7(b13)   D7(9/#11)  G7(4/9)  C7M(9)
Sendo seu colorido brinquedo de    papel machê

(passagem 1: C7(9)  C7  C7(4) C7(4/9) )

F7M              Dm7(11)  Dm7(9)
    cores do mar,        festa do sol
C7M(5+)                    A7(9)
        Vida é fazer todo sonho brilhar
       Fm7
Ser feliz
                G7(13)  C7M      G/B
No seu colo dormir e depois acordar
       A7(9)   A7(b13)   D7(9/#11)  G7(4/9)  C7M(9)
Sendo seu colorido brinquedo de    papel machê

(passagem 2: E7(b9)  E7  E7 )

Passagem 2:

   E7(b9)  E7  E7
e|-------------7-------------------------|
B|-6-------5-----9-----------------------|
G|-7-------------------------------------|
D|---------------------------------------|
A|-7-----------7-------------------------|
E|---------------------------------------|

Segunda Estrofe:

    Am7M(9)              F7M(6)
Dormir no seu colo é tornar a nascer
    Dm7(11)           C7M(5+)
Violeta e azul outro ser
           A7(b9)  A7(b9/13)
Luz do querer
     F7M
Não vá desbotar
   Dm7
Lilás, cor do mar
       C7M(5+)
Seda, cor de batom
     A7(b13)
Arco-íris, crepom
      D7(9)    D7(9/13)
Nada vai desbotar
      D7(9/#11)  G7(4/9)  C7M(9)
Brinquedo de    papel machê

(passagem 2)

(Repete Segunda Estrofe) (Passagem 1)

Interlúdio:

F7M  ( Dm7(9)  Dm7(11) )  C7M(5+)  A7(9)  A7(b13)
D7(9)  D7(4/9)  D7(9/#11)  G7(4/9)  C7M(9)
(passagem 1)

F7M  ( Dm7(9)  Dm7(11) )  C7M(5+)  A7(9)  A7(b13)
D7(9)  D7(4/9)  D7(9/#11)  G7(4/9)  C7M(9)
(passagem 2)

Segunda Estrofe:

    Am7M(9)              F7M(6)
Dormir no seu colo é tornar a nascer
    Dm7(11)           C7M(5+)
Violeta e azul outro ser
           A7(b9)  A7(b9/13)
Luz do querer
     F7M
Não vá desbotar
   Dm7
Lilás, cor do mar
       C7M(5+)
Seda, cor de batom
     A7(b13)
Arco-íris, crepom
      D7(9)    D7(9/13)
Nada vai desbotar
      D7(9/#11)  G7(4/9)  C7M(9)
Brinquedo de    papel machê

(passagem 1)

Final:

F7M  ( Dm7(9)  Dm7(11) )  C7M(5+)  A7(9)  A7(b13)
D7(9)  D7(4/9)  D7(9/#11)  G7(4/9)  C7M(9)
(passagem 1)

F7M  ( Dm7(9)  Dm7(11) )  C7M(5+)  A7(9)  A7(b13)
D7(9)  D7(4/9)  D7(9/#11)  G7(4/9)
( Bb7(9)  A7(9)  D7(9/#11)  G7(13)  C7M(9) )

----------------- Acordes -----------------
A7(9) = 5 X 5 4 2 X
A7(b13) = X 0 X 0 2 1
A7(b9) = 5 X 5 3 2 X
A7(b9/13) = 5 X X P3 7 3
Am7M(9) = 5 3 6 4 X X
Bb7(9) = X 1 0 1 1 X
C7 = X 3 2 3 1 X
C7(4) = X 3 3 3 1 X
C7(4/9) = X 3 3 3 3 3
C7(9) = X 3 2 3 3 X
C7M = X 3 2 0 0 X
C7M(5+) = X 3 2 1 0 0
C7M(9) = X 3 2 4 3 X
D7(4/9) = X 5 5 5 5 5
D7(9) = X 5 4 5 5 X
D7(9/#11) = X X 0 1 1 0
D7(9/13) = X 5 4 5 0 0
Dm7 = X 5 7 5 6 5
Dm7(11) = X 5 5 5 6 5
Dm7(9) = X 5 3 5 5 X
E7 = 0 2 2 1 3 0
E7(b9) = X X 2 1 3 1
Eb7M(6) = X 6 5 5 3 3
Ebm7M(6) = 11 13 12 11 13 11
F7M = 1 X 2 2 1 X
F7M(6) = 1 X 2 2 3 X
Fm7 = 1 X 1 1 1 X
Fm7M(6) = 1 3 2 1 3 1
G/B = X 2 0 0 3 3
G7(13) = 3 X 3 4 5 X
G7(4/9) = 3 X 3 2 1 X
