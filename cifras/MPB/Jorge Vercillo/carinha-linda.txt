Jorge Vercillo - Carinha Linda

(Jorge Vercilo)
Intro.: Eb7M  F9  G4/7  Eb7M  F9  C9

  Gm              Gm7+
Cabelo molhado ao sol
                    Cm7      F7/9-  Bb7M  D7/9
caído nos ombros dourados de contra-luz
  Gm              Gm7+
O mar de milhas azuis
                          Cm7       Bb7M   Am7
escorre entre os seios na pele de alguém
              D7
que eu quero bem


  Gm                Gm7+
O sal que tempera a cor

                       Cm7       F7/9-  Bb7M  D7/9
e deixa o seu gosto na língua do meu   amor
    Gm              Gm7+
que sabe me enlouquecer

                          Cm7      F7/9-  Bb7M   Am7/5-
e eu "armo a barraca" na areia pra não   dizer:

       D7/5+  Gm     Gm7+
Que carinha   linda!
            Gm7              Gm6
que olhar safado, gesto alucinado
           Cm7                  Fsus      F7/9-    Bb7M
me deixa passado, inseguro de ciúme sem saber   porquê.

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Am7/5- = 5 X 5 5 4 X
Bb7M = X 1 3 2 3 1
C9 = X 3 5 5 3 3
Cm7 = X 3 5 3 4 3
D7 = X X 0 2 1 2
D7/5+ = X 5 X 5 7 6
D7/9 = X 5 4 5 5 X
Eb7M = X X 1 3 3 3
F7/9- = X X 3 2 4 2
F9 = 1 3 5 2 1 1
Fsus = 1 3 3 2 1 1
G4/7 = 3 5 3 5 3 X
Gm = 3 5 5 3 3 3
Gm6 = 3 X 2 3 3 X
Gm7 = 3 X 3 3 3 X
Gm7+ = 3 5 4 3 3 3
