Jorge Vercillo - Canavial

Introdução:   Ôriçaruê... saô... lagé...
Ôriçaruê... odé oh mi mãe... Ôriçaruê...
saô lagé...babá...Ôriçaruê... odé oh mi mãe...

(A G D/F# D9 D/E A G D/F# D/E D/A A) 2x.

A9 A/G        D/F#
Cresci no canavial
Dm/F              A/E
subindo no pé de manga
Ebm7(5-)                D7+
Um dia me vi perdido em Sampa
D/E             A
longe do meu quintal

A/G               D/F#
Vejo os latinos que vão
Dm/F             A/E
Lavar o chão de Miami
Ebm7(5-)          D7+
Prefiro ser a rainha do enxame

D/E           D/A A
que rabo de leão

A            G
Vosmecê me levou
      D/F#      D9    D/E
a fecundar    a ter--ra
A               G
Como amor fecundou
  D/F#       D/E  D/A A
me enamorar de "ocê”
A             G
Vosmecê me ensinou
  D/F#    D9    D/E
fertilizar a terra
A               G
Como amor me levou
        D/F#      D/E  A9
a engravidar   vo -- cê

A/G                     D/F#
Depois do frio passar
Dm/F                    A/E
Meses de longa espera
Ebm7(5-)                 D7+
Todo o plantio com a primavera
D/E            A
vem nos glorificar

A/G                   D/F#
Olho o seu ventre crescer
Dm/F              A/E
vejo um planeta Terra
Ebm7(5-)                D7+
Pelo feitio, disse a parideira:
D/E          D/A A
-uma menina virá

A            G
Vosmecê me levou
      D/F#      D9    D/E
a fecundar    a ter--ra
A               G
Como amor fecundou
  D/F#       D/E D/A A
me enamorar de "ocê”
A             G
Vosmecê me ensinou
  D/F#    D9    D/E
fertilizar a terra
A               G
Como amor me levou
        D/F#      D/E  A9
a engravidar   vo -- cê

Solo: Modula para E (1ª vez B9 A E/G# E9 E/F# / na 2ª vez E/B B9) 2x.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/E = 0 X 2 2 2 0
A/G = 3 X 2 2 2 X
A9 = X 0 2 2 0 0
B9 = X 2 4 4 2 2
D/A = X 0 X 2 3 2
D/E = X X 2 2 3 2
D/F# = 2 X 0 2 3 2
D7+ = X X 0 2 2 2
D9 = X X 0 2 3 0
Dm/F = X X 3 2 3 1
E = 0 2 2 1 0 0
E/B = X 2 2 1 0 0
E/F# = X X 4 4 5 4
E/G# = 4 X 2 4 5 X
E9 = 0 2 4 1 0 0
Ebm7(5-) = X X 1 2 2 2
G = 3 2 0 0 0 3
