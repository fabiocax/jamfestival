Jorge Vercillo - Signo de Ar

[Intro] G7M  G9  C7M  C  C9
        G7M  G9  C7M  C  C9

G7M  G7M6
An--da
 G7M        G6        G7M/5+  G6
Manda e desmanda num bei----jo
 G7M      G7    C7M   C6
Por onde passa, encanta
   C7M         C6    F7/5b
O seu sobrenome é dese|--jo
 F7(13)             G7M  G7M6
Por você o sol se levanta
 G7M        G6        G7M/5+  G6
Me tira do sono e do sério
 G7M        G7        C7M  C7M6
Sopra no ouvido esse mantra
 C7M          C7M6   F7(5b)
Seu andar me deixa aé---reo
 F7(13)          G7M  G7M6 G7M C#7
Seu sorriso faz verão


 Am7(11)
Signo de ar
  C/D        B/D     G7M/5+  G7M6
Que mistério envolve o seu  caminhar?
F7(9/5b)       A#º             C/A
Abre as varandas do meu coração
  G#7M        G7M    G7M(13)  G7M(9)  C#7
Que visão! Você e o mar
 Am7(11)
Signo de ar
 C/D     B/D    E7(4)  E7(9)
Faz o paraíso nos visitar
 Fº         A#º         C/A
Com seu sorriso de constelação
 G#7M     G7M  G7M(13)  G7M(9)  G7M(13)  G7M(b13)  G7M(13)  F7(13)
No varal do verão
                   G7M  G7M(13)  G7M(9)  G7M(13)  G7M(b13)  G7M(13)  F7(13)
Seu sorriso faz verão

 ( A#7+(11)  F7(11)  B7+/F  F7(11) )

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
C6 = 8 X 7 9 8 X
C7M = X 3 2 0 0 X
C9 = X 3 5 5 3 3
Cm6 = X 3 X 2 4 3
Cm7M = X 3 5 4 4 3
D/E = X X 2 2 3 2
D6(b9) = X 5 7 4 4 X
E7 = 0 2 2 1 3 0
E7(4) = 0 2 0 2 0 X
F7(13) = 1 X 1 2 3 X
Fº = X X 3 4 3 4
G = 3 2 0 0 0 3
G#7M = 4 X 5 5 4 X
G#º = 4 X 3 4 3 X
G6 = 3 X 2 4 3 X
G7(b9) = 3 X 3 1 0 X
G7M = 3 X 4 4 3 X
G7M(13) = 3 X 4 4 5 X
G7M(9) = 3 2 4 2 X X
G7M(b13) = 3 X 4 4 4 X
G9 = 3 X 0 2 0 X
