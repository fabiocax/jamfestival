Jorge Vercillo - Himalaia

Intro: Em  D  C  D  D#º

           Em  D                  C D D#º
O mundo inteiro está guardado em mim
       Em  D                   C D D#º
De Sarajevo aos templos de Pequim
           Em D                 C D D#º
Não tenha medo de amar e ser feliz,
        Em D                C D
É o segredo divino do aprendiz

G/D                      Bm7
Quero andar nas ruas que há em mim,
C                  D
Conhecer esquinas do coração,
G/D                  Bm7
Desabar nos próprios botequins
C                  D
Nos subúrbios da iluminação

O mundo inteiro está mudado em mim

Desde Soweto aos muros de Berlim

Sou um rio das mágoas que há em mim,
Cachoeira que desaba no mar,
Oceano das vidas que vivi
E de outras que virei navegar

      Em              D
Na leveza do ser a beleza é voar
       C
o Himalaia, o Himalaia, o Himalaia, o Himalaia,
       D          D#º
o Himalaia inteiro

Alquimia é reter as montanhas no olhar,
o Himalaia, o Himalaia, o Himalaia, o Himalaia,
o Himalaia inteiro

----------------- Acordes -----------------
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D#º = X X 1 2 1 2
Em = 0 2 2 0 0 0
G/D = X 5 5 4 3 X
