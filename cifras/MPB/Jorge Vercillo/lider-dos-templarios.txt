Jorge Vercillo - Líder Dos Templários

         Am    E7/G#      C/G
Tem fé que Jorge é de ajudar
D/F#     D7/F#  F7M       Bm7(b5)    E7  Am7  E7
A todo Bra-----sileiro, Brasileiro gue-rreiro
        Am        E7/G#  C/G
São Jorge cavaleiro da flor
 D/F#       D7/F#  F7M    Bm7(b5)   E7  Am7  D/F#  D7/F#  F7M
São Jorge pro-----tetor,protetor, protetor

Oxossi da mata, Ogun do Ferro
          D/F#  D7/F#  F7M   E7(#9)
Salamâleico, âleicon, salamalan

   Am7          E7        Am7
Estórias de um Santo lutador
 Dm7       G7           C7M
Lider soberano dos templários
 Gm7(9)     C7(b9)           F7M  C/E
No povo a sua força se perpetuou
   Dm7               E7      Am7(9)
E hoje vive em nosso imaginário

   Dm7               E7      Am7(9)  E7(#9)
E hoje vive em nosso imaginário

Mas todo imaginário tem valor
E pode transformar esse cenário
A mente criadora é um dom maior
Naqueles que são revolucionários
Naqueles que são revolucionários

         Am      E7/G#    C/G
Tem fé que Jorge é de ajudar
D/F#     D7/F#  F7M       Bm7(b5)    E7  Am7  E7
A todo Bra-----sileiro, Brasileiro gue-rreiro
    Am    E7/G#       C/G
Eu sou cavaleiro da flor
 D/F#       D7/F#  F7M    Bm7(b5)   E7  Am7
São Jorge pro-----tetor,protetor, protetor

 D/F#       D7/F#  F7M    Bm7(b5)   E7  Am7
São Jorge pro-----tetor,protetor, protetor

[Solo]  D/F#  D7/F#  F7M  Bm7(b5)  E7  Am7
        D/F#  D7/F#  F7M  E7(#9)

O terreno ambiguo
Por que ele é um herói
Ele tem pulsações humanas
E é por isso que ele á tão querido
Em todos os lugares
Pelas, crianças, pela população

De Camões a Fernando Pessoa, nós somos resultado
Dessa peregrinação longíngua
De combates e de glórias
De afirmação do bem contra o mal
E mesmo na era cibernética
No mundo digital, no holograma
Ali está São Jorge, triunfante lá na frente de todos nós
É a pipoca da pororoca da imaginação

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Am7(9) = X X 7 5 8 7
Bm7(b5) = X 2 3 2 3 X
C/E = 0 3 2 0 1 0
C/G = 3 3 2 X 1 X
C7(b9) = X 3 2 3 2 X
C7M = X 3 2 0 0 X
D/F# = 2 X 0 2 3 2
D7/F# = X X 4 5 3 5
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
E7(#9) = X 7 6 7 8 X
E7/G# = 4 X 2 4 3 X
F7M = 1 X 2 2 1 X
G7 = 3 5 3 4 3 3
Gm7(9) = X X 5 3 6 5
