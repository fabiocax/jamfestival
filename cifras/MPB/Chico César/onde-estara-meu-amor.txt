Chico César - Onde Estará Meu Amor

 

G                   Em 
 Como esta noite findará 
              C 
 E o sol então rebrilhará 
              G 
 Estou pensando em você... 
 D             G9 
 Onde estará o meu amor ? 
            Em9 
 Será que vela como eu ? 
              C9 
 Será que chama como eu ? 
               G9 
 Será que pergunta por mim ? 
 D             G9 
 Onde estará o meu amor ? 
             Em9 
 Se a voz da noite responder 
                 C9 
 Onde estou eu, onde está você 
              G9 
 Estamos cá dentro de nós 
 D 
 Sós... 
         D/F#             G9 
 Onde estará o meu amor ? 
                   Em9 
 Se a voz da noite silenciar 
             C9 
 Raio de sol vai me levar 
                     G9       D  D/F# 
 Raio de sol vai lhe trazer

----------------- Acordes -----------------
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
Em9 = 0 2 4 0 0 0
G = 3 2 0 0 0 3
G9 = 3 X 0 2 0 X
