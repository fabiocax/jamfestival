Chico César - Floriô

E                               B
Arroz deu cacho e o feijão floriô
          A                        E
Milho na palha, coração cheio de amor

E                                  A
Povo sem terra fez a guerra por justiça
A                                       E
Visto que não tem preguiça este povo de pegar
E                                A
Cabo de foice, também cabo de enxada
A                  B                        E
Pra poder fazer roçado e o Brasil se alimentar

E                              B
Arroz deu cacho e o feijão floriô
          A                        E
Milho na palha, coração cheio de amor

E                                A
Com sacrifício debaixo da lona preta
A                                    E
Inimigo fez careta mas o povo atravessou
E                                   A
Rompendo cercas que cercam a filosofia
A                   B                       E
De ter paz e harmonia para quem planta o amor

E                               B
Arroz deu cacho e o feijão floriô
          A                        E
Milho na palha, coração cheio de amor

E                                   A
Erguendo a fala gritando Reforma Agrária
A                                               E
Porque a luta não para quando se conquista o chão
E                                   A
Fazendo estudo, juntando a companheirada
A               B                       E
Criando cooperativa pra avançar a produção

E                               B
Arroz deu cacho e o feijão floriô
          A                        E
Milho na palha, coração cheio de amor

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
E = 0 2 2 1 0 0
