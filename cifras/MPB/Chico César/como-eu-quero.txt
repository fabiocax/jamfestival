Chico César - Como Eu Quero

A                         E
Diz pra eu ficar muda faz cara de mistério
 F#m                      D
Tira essa bermuda que eu quero você sério
 A                        E
Dramas do sucesso, mundo particular
 F#m                    D
Solos de guitarra não vão me conquistar

F#m7     C#m7                D9
Uh, eu quero você como eu quero
F#m7     C#m7                D9
Uh, eu quero você como eu quero

A                         E
O que você precisa é de um retoque total
 F#m                      D
Vou transformar o seu rascunho em arte final
A                         E
Agora não tem jeito cê tá numa cilada
 F#m                      D
Cada um por si você por mim mais nada

Bm        D          A9                E
Longe do meu domínio cê vai de mal a pior
Bm               D             A9
Vem que eu te ensino como ser bem melhor

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
Bm = X 2 4 4 3 2
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
