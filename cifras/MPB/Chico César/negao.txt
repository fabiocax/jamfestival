Chico César - Negão

G                    D
Negam que aqui tem preto, negão
C                         G            D
Negam que aqui tem preconceito de cor
G             D
Negam a negritude, essa negação
C                      G         D
 Nega a atitude de um negro amor
Bm            Em           Am
Mas pra todo canto aonde com você, eu vou
Bm            Em             Am        D
Com o canto do olho lançam setas de indagação
C                       G              Em
Ainda não sabem, mas sabemos que opressão
C                         G               Em
É a falta de pressa do opressor pedir perdão
      Am           D               C         G   D
A quem não perdeu tempo e a muito tempo perdoou oou
C                    G
Mas nunca esqueceu, não

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
