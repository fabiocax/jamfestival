Chico César - Paraíba Meu Amor

              Cm7
Paraíba meu amor

Eu estava de saída, mas eu vou ficar
 Cm   C7     Fm7   Bb7             Eb
Não quero chorar o choro da despedida
      Ab           Dm7(b5)
O acaso da minha vida
         G7      Cm7  C7
Um dado não abolirá

          Fm7                Cm
Pois seguirás bem dentro de mim
                      G7
Como um São João sem fim
                C7
Queimando o sertão
          Fm                 Cm
E a fogueirinha lanterna de laser
       Eb/Bb       Ab       G   Cm
Que ilumina o festejo do meu coração

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Bb7 = X 1 3 1 3 1
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
Cm7 = X 3 5 3 4 3
Dm7(b5) = X X 0 1 1 1
Eb = X 6 5 3 4 3
Eb/Bb = X X 8 8 8 6
Fm = 1 3 3 1 1 1
Fm7 = 1 X 1 1 1 X
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
