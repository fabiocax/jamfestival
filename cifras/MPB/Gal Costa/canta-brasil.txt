Gal Costa - Canta, Brasil

C7(4/9/13)    F6(9)    Bbm6/F     F7M         Bbm6   F6/A    Ab° Gm7
           As selvas te deram nas noites seus ritmos bárbaros
     C7(b9)                       Ab°               Gm7(11) Am7(b5)
           Os negros trouxeram de longe reservas de pranto
     D7(b9)    Gm7      Gm/F      Cm6/Eb   D7(b9)  Gm(7M) Gm7 Gm6
           Os brancos falaram de amores em suas canções
            G7(4/9)           G7(9)              Gm7   C7(#5) F6(9) C7b9/b13) F6(9) C7(b9/b13) F6(9) C7(b9/b13)
           E dessa mistura de vozes nasceu o teu canto

F6(9) Gm7 C7(b9/13)        F6        G#°        Am  Am(7M) Am7
                    Brasil     Minha voz enternecida
       Bb6                           A7
           Já dourou os teus brasões
    D7  G7          C7           F6
           Na expressão mais comovida
                       Dm7(9)        Gm7    Am7(b5)
           Das mais ardentes canções
    D7(b9)     Gm Gm(7M) Gm7  Am7(b5)    D7(b9)          Gm7
           Também,                    A beleza deste céu
                  D7(b9)          Gm7
           Onde o azul é mais azul

                 Bbm6            F7M/A       G#°               Am7 D7(b9) Gm7 C7(b9) F7M
           Na aquarela do Brasil       Eu cantei de Norte a Sul

                Bb7M              A7 D7 G7 C7 Am7(b5)
           Mas agora o teu cantar,                    Meu Brasil quero escutar
                                        D7(b9)       Gm  Gm(7M) Gm7 Gm6
           Nas preces da sertaneja, Nas ondas do rio-mar
           Bb6  Bbm6                      Am7         G#°             Am7
           Oh!       Esse rio - turbilhão,     Entre selvas de rojão,
                Bb6               Am7(b5)         D7(b9)            G7(13)  G7(b13) C7(4/9)
           Continente a caminhar!         No céu!        No mar! Na  terra!
           C7(b9)        F6  C7(b9/13)       F6        G#°        Am  Am(7M) Am7
           Canta, Brasil!              Brasil    Minha voz enternecida
       Bb6                           A7
           Já dourou os teus brasões
    D7  G7          C7           F6
           Na expressão mais comovida
                       Dm7(9)        Gm7    Am7(b5)
           Das mais ardentes canções
    D7(b9)     Gm Gm(7M) Gm7  Am7(b5)    D7(b9)          Gm7
           Também,                    A beleza deste céu
                  D7(b9)          Gm7
           Onde o azul é mais azul
                 Bbm6            F7M/A       G#°               Am7 D7(b9) Gm7 C7(b9) F7M
           Na aquarela do Brasil       Eu cantei de Norte a Sul

                Bb7M              A7 D7 G7 C7 Am7(b5)
           Mas agora o teu cantar,                    Meu Brasil quero escutar
                                        D7(b9)       Gm  Gm(7M) Gm7 Gm6
           Nas preces da sertaneja, Nas ondas do rio-mar
           Bb6  Bbm6                      Am7         G#°             Am7
           Oh!       Esse rio - turbilhão,     Entre selvas de rojão,
                Bb6               Am7(b5)         D7(b9)            G7(13)  G7(b13) C7(4/9)
           Continente a caminhar!         No céu!        No mar! Na  terra!
           C7(b9)        F6  Eb7/Bb  D7/A  Eb7  D7       G7(13) G7(b13) C7(4/9)
           Canta, Brasil!      No    céu   No   mar Na  terra!
           C7(b9)        F6  Eb7/Bb  D7/A  Eb7  D7       G7(13) G7(b13) C7(4/9)
           Canta, Brasil!      No    céu   No   mar Na  terra!
           C7(b9)        F6  Eb7/Bb  D7/A  Eb7  D7       G7(13) G7(b13) C7(4/9)
           Canta, Brasil!      No    céu   No   mar Na  terra!
           C7(b9)        F6  Eb7/Bb  D7/A  Eb7  D7       G7(13) G7(b13) C7(4/9)
           Canta, Brasil!      No    céu   No   mar Na  terra!
           C7(b9)     F7M      Gb7M(#11)   F7M
           Canta, Brasil!!!!

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Ab° = 4 X 3 4 3 X
Am = X 0 2 2 1 0
Am(7M) = X 0 2 1 1 0
Am7 = X 0 2 0 1 0
Am7(b5) = 5 X 5 5 4 X
Bb6 = X 1 3 0 3 X
Bb7M = X 1 3 2 3 1
Bbm6 = 6 X 5 6 6 X
Bbm6/F = X X 3 3 2 3
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
C7(#5) = X 3 X 3 5 4
C7(4/9) = X 3 3 3 3 3
C7(4/9/13) = X 3 3 3 3 5
C7(b9) = X 3 2 3 2 X
C7(b9/13) = 8 X X P6 10 6
C7(b9/b13) = X 3 2 3 2 4
Cm6/Eb = X X 1 2 1 3
D7 = X X 0 2 1 2
D7(b9) = X 5 4 5 4 X
D7/A = 5 X 4 5 3 X
Dm7(9) = X 5 3 5 5 X
Eb7 = X 6 5 6 4 X
Eb7/Bb = 6 X 5 6 4 X
F6 = 1 X 0 2 1 X
F6(9) = 1 0 0 0 X X
F6/A = 5 3 3 5 3 X
F7M = 1 X 2 2 1 X
F7M/A = 5 X 3 5 5 X
G#° = 4 X 3 4 3 X
G7 = 3 5 3 4 3 3
G7(13) = 3 X 3 4 5 X
G7(4/9) = 3 X 3 2 1 X
G7(9) = 3 X 3 2 0 X
G7(b13) = 3 X 3 4 4 3
Gb7M(#11) = 2 X 3 3 1 1
Gm = 3 5 5 3 3 3
Gm(7M) = 3 5 4 3 3 3
Gm/F = X X 3 3 3 3
Gm6 = 3 X 2 3 3 X
Gm7 = 3 X 3 3 3 X
Gm7(11) = 3 X 3 3 1 X
