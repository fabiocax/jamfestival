Gal Costa - Quase Um Segundo

Intro 4x: Bb  Ab

Gm         Bb/F             C/E
Eu queria ver no escuro do mundo
               Eb7M(9)    Gm
Aonde está o que você quer
                Bb/F           C/E
Pra me transformar no que te agrada
           D/F#    Gm7
No que me faça ver

              C7(9)      Eb7M(9)           D7
Quais são as cores e as coisas pra te prender
Bb          Bb7/D            Eb7M(9)
Eu tive um sonho ruim e acordei chorando
             D/F#
Por isso eu te liguei

Eb7M(9)     D7       Gm7        Bb/F
Será que você ainda pensa em mim
Eb7M(9)     D7       Em7 D/E Em7 D/E
Será que você ainda pensa

Fm7(9)    Bb7(13)  C/E Eb6 Bb/D
lá lá lá lá lá lá lá

              Bb/F              C/E
Ás vezes te odeio por quase um segundo
          Eb7M(9)    Bb/D
Depois te amo mais
                 Bb/F              C/E
Teus pêlos, teu rosto, teu gosto, tudo
                 D/F#        Gm7
Tudo que não me deixa em paz

              C7(9)      Eb7M(9)           D7
Quais são as cores e as coisas pra te prender
Bb          Bb7/D            Eb7M(9)
Eu tive um sonho ruim e acordei chorando
             D/F#
Por isso eu te liguei

Eb7M(9)     D7       Gm7        Bb/F
Será que você ainda pensa em mim
Eb7M(9)     D7       Em7 D/E Em7 D/E
Será que você ainda pensa
Fm7(9)    Bb7(13)  C/E Eb6 Bb/D Bb/F C/E D/F# Gm
lá lá lá lá lá lá lá

              Bb/F              C/E
Ás vezes te odeio por quase um segundo
          Eb7M(9)    Gm
Depois te amo mais
                 Bb/F              C/E
Teus pêlos, teu rosto, teu gosto, tudo
                 D/F#        Gm7
Tudo que não me deixa em paz

              C7(9)      Eb7M(9)           D7
Quais são as cores e as coisas pra te prender
Bb          Bb7/D            Eb7M(9)
Eu tive um sonho ruim e acordei chorando
             D/F#
Por isso eu te liguei

Eb7M(9)     D7       Gm7        Bb/F
Será que você ainda pensa em mim
Eb7M(9)     D7       Em7 D/E Em7 D/E
Será que você ainda pensa
Fm7(9)    Bb7(13)  C/E
lá lá lá lá lá lá lá

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Bb = X 1 3 3 3 1
Bb/D = X 5 X 3 6 6
Bb/F = X 8 8 7 6 X
Bb7(13) = 6 X 6 7 8 X
Bb7/D = X 5 6 3 6 X
C/E = 0 3 2 0 1 0
C7(9) = X 3 2 3 3 X
D/E = X X 2 2 3 2
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
Eb6 = X X 1 3 1 3
Eb7M(9) = X 6 5 7 6 X
Em7 = 0 2 2 0 3 0
Fm7(9) = X X 3 1 4 3
Gm = 3 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
