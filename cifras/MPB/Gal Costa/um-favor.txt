Gal Costa - Um Favor


   F#7+            F#6
Eu hoje acordei pensando
   F#7+               F#6
Porque que eu vivo chorando
  Aº             G#m7   D#7   G#m7
Podendo lhe procurar
Se a lágrima é tão maldita
                  C#7/9
E a pessoa mais bonita
                     F#7+   C#7/9+  F#7+
Cobre o rosto pra chorar
       F#6        F#7+   F#6   F#7+
E refletindo um segundo
     F#6         C#m7    F#7
Resolvi pedir ao mundo
                    B7+   Bm6
Que me fizesse um favor
                        A#m7
Para que eu não mais chorasse
      D#7/9      G#m7
Que alguém me ajudasse

C#7/9            F#7+   F#6
A encontrar meu amor
  F#7+    F#6         F#7+    F#6
Maestros, músicos, cantores
                  F#7+
Gente de todas as cores
Aº                  G#m7   D#7/9-   G#m7
Faça esse favor pra mim
               D#7      G#m7
Quem souber cantar, que cante
              D#7      G#m7
Quem souber tocar, que toque
C#7/9                  F#7+    F#7
Flauta, trombone ou clarim
                             C#m7
Quem puder gritar que grite
                   F#7
Quem tiver apito, apite
          C#m7 F#7  B7+   Bm6
Faça esse mundo acordar
                    A#m7
Para que onde ela esteja
      D#7/9         G#m7
Saiba que alguém rasteja
  C#7/9            F#7+
Pedindo pra ela voltar

----------------- Acordes -----------------
A#m7 = X 1 3 1 2 1
Aº = 5 X 4 5 4 X
B7+ = X 2 4 3 4 2
Bm6 = 7 X 6 7 7 X
C#7/9 = X 4 3 4 4 X
C#7/9+ = X 4 3 4 5 X
C#m7 = X 4 6 4 5 4
D#7 = X 6 5 6 4 X
D#7/9 = X 6 5 6 6 X
D#7/9- = X 6 5 6 5 X
F#6 = 2 X 1 3 2 X
F#7 = 2 4 2 3 2 2
F#7+ = 2 X 3 3 2 X
G#m7 = 4 X 4 4 4 X
