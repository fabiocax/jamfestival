Gal Costa - Baby

      D  G                       D
Você precisa saber da piscina, da
              G                   D  G  D
Margarina, da Carolina, da gasolina
        G                D  G
Você precisa saber de mim
   D        Bm   Em           A7
Baby, baby, eu sei que é assim
   D        Bm   Em           A7
Baby, baby, eu sei que é assim
       D  G                  D
Você precisa tomar um sorvete
              G               D
Na lanchonete, andar com gente
               G  D
Me ver de perto.
           G                  D  G
Ouvir aquela canção do Roberto
   D        Bm  Em         A7
Baby, baby, há quanto tempo
   D        Bm  Em         A7  D
Baby, baby, há quanto tempo


         G                  D  G
Você precisa aprender inglês
                             D
Precisa aprender o que eu sei
                       G
E o que eu não sei mais
                       D  G  D
E o que eu não sei mais
          G                  D  G
Não sei, comigo vai tudo azul
                       D  G
Contigo vai tudo em paz
                        D
Vivemos na melhor cidade
           G
Da América do Sul
            D
Da América do Sul
          G               D  G
Você precisa, você precisa
           D   G             D  G
Não sei, leia na minha camisa
  D         Bm  Em    A7
Baby, baby, I love you
  D         Bm  Em    A7
Baby, baby, I love you

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
