Gal Costa - Até Quem Sabe

A7+              F#5+/7     Bm7   C#m7/9 F#5+/7
Até um dia, até talvez, até quem sabe
     Bm7   F7   E4/7      E5+/7   A7+  A4/7
Até você sem fantasia, sem mais saudade
         D7+          E/G#                 C#m7/9
Agora a gente tão de repente nem mais se entende
    F#4/7  F#5+/7  B4/7    G6/9             E4/7
Nem mais pretende seguir fingindo, seguir seguindo
    A7+            F#5+/7
Agora vou pra onde for
          Bm7 F#5+/9-
Sem mais você
         Bm7        F6/7  E4/7
Sem me querer, sem mesmo ser
E5+/7   A7+  A4/7 D#5+/7
Sem entender
        D7+    D#m7   G#5+/7
Vou me beber, vou me perder
C#m7/9 F#4/7 F#5+/9-
Pela cidade
      F#m7   B7   E4/7
Até um dia, até talvez

 E7       A7+     A7+ F#5+/7
Até quem sabe

----------------- Acordes -----------------
A4/7 = X 0 2 0 3 0
A7+ = X 0 2 1 2 0
B4/7 = X 2 4 2 5 2
B7 = X 2 1 2 0 2
Bm7 = X 2 4 2 3 2
C#m7/9 = X 4 2 4 4 X
D#5+/7 = X 6 X 6 8 7
D#m7 = X X 1 3 2 2
D7+ = X X 0 2 2 2
E/G# = 4 X 2 4 5 X
E4/7 = 0 2 0 2 0 X
E5+/7 = 0 X 0 1 1 0
E7 = 0 2 2 1 3 0
F#4/7 = 2 4 2 4 2 X
F#5+/7 = 2 X 2 3 3 2
F#5+/9- = X 9 8 9 8 10
F#m7 = 2 X 2 2 2 X
F6/7 = 1 X 1 2 3 X
F7 = 1 3 1 2 1 1
G#5+/7 = 4 7 6 5 4 X
G6/9 = X X 5 4 5 5
