Gal Costa - Caras e Bocas

 A7M
Quando canto um segredo
                     D7M         B7
Quando mostro algum medo, ou mais
E7
Quando falo de amor ou desejo
                              A7M
Minha boca se mostra macia, vermelha
        A7       D7M
Mas se dessa garganta
Dm7            G
Das cordas escondidas
               C
Desse peito sufocado
F#m7               B7/9-
Desse coração atrapalhado
E7/9
Surge uma nota brilhante
                  A7M
De cristal transparente
A7                   D7
Minha cara invade a cena, rasga a vida

  G             F#m      A7M
Mostra o brilho agudo musical

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7M = X 0 2 1 2 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
D7 = X X 0 2 1 2
D7M = X X 0 2 2 2
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
E7/9 = X X 2 1 3 2
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
G = 3 2 0 0 0 3
