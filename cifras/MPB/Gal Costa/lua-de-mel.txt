Gal Costa - Lua de Mel

G
Lua-de-mel mam mam mamãe
Eu to em lua-de-mel
              D7           Eb7
Eu to morando num pedaço do céu
        D7     G      Eb7   D7
Como o diabo gosta
G Dm7 G7 (2a. vez)
   C7+          B7                Em
Todo delito doce deleite todo desfrute
A7          Eb                      F7
Tem permissão tudo que dá prazer
      Bb7+   Am7   D7
Tentação
C7+                  B7              Em
O dia inteiro nadar no mar banco de areia
A7             Eb
imensidão tardes desmaios
  F7          Bb7+  Am7  D7
Nossa canção que diz
 G
Lua de mel mam mam mamãe eu to em lua-de-mel

          D7              Eb7
Eu to morando num pedaço do céu
     D7          G    Dm7   G7
Como o diabo gosta
  C7+
Da vida eu já conheço
Cm7            G          A7
A dor de não poder viver como eu queria
        Eb7
Mas uma coisa eu posso
    D7
E quando eu quero canto, canto . . .
REFRÃO

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
Bb7+ = X 1 3 2 3 1
C7+ = X 3 2 0 0 X
Cm7 = X 3 5 3 4 3
D7 = X X 0 2 1 2
Dm7 = X 5 7 5 6 5
Eb = X 6 5 3 4 3
Eb7 = X 6 5 6 4 X
Em = 0 2 2 0 0 0
F7 = 1 3 1 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
