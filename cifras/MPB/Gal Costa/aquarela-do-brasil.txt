Gal Costa - Aquarela do Brasil

   G6(9)
Brasil!
                Gº
Meu Brasil brasileiro
               G6(9)
Meu mulato inzoneiro
                       F7    E7
Vou cantar-te nos meus ver...sos
Am7          D7(9)     Am7
   O Brasil, samba que dá
           D7(9)      Am7
Bamboleio, que faz gingar
          D7(9)   Am7
Ó Brasil, do meu amor
         D7(9)   G    Em7
Terra de Nosso Senhor
   Am7  D7(9) G    Em7
Brasil!    Brasil!
    Am7  D7(9)  G    G5+   G6   G5+
Pra mim,    pra mim
G  G5+  G6           G5+        Am   Am5+  Am6
Ó          abre a cortina do passado

           Am5+        Am   Am5+  Am6
Tira a mãe preta do serrado
           Am5+        G    G5+  G6
Bota o rei congo no congado
    Am  Am5+  Am6
Brasil!
    G   G7  F#7  F7
Brasil!
 E    F  E           F           E   F  E
Deixa      cantar de novo o trovador
       F            E   F  E
A merencória luz da lua
        E7          Am   Am5+  Am6  Am5+
Toda canção do meu amor
Am    Am5+  Am6          Am7(b5)  Bm7
Quero           ver essa dona caminhando
G          Em7       A7
   Pelos salões arrastando
Am7         D7(9)   G    Em7
   O seu vestido rendado
   Am7  D7(9) G    Em7
Brasil!    Brasil!
    Am7  D7(9)  G    G5+   G6   G5+
Pra mim,    pra mim
   G6(9)
Brasil!
               Gº
Terra boa e gostosa
             G6(9)
Da morena sestrosa
               F7    E7
De olhar indiscre ... to
Am7          D7(9)     Am7
   O Brasil, samba que dá
           D7(9)      Am7
Bamboleio, que faz gingar
          D7(9)   Am7
Ó Brasil, do meu amor
         D7(9)   G    Em7
Terra de Nosso Senhor
   Am7  D7(9) G    Em7
Brasil!    Brasil!
    Am7  D7(9)  G    G5+   G6   G5+
Pra mim,    pra mim
G  G5+  G6       G5+           Am   Am5+  Am6
O         esse coqueiro que dá côco
          Am5+          Am   Am5+  Am6
Oi, onde amarro a minha rede
           Am5+        G   G5+  G6
Nas noites claras de luar
    Am  Am5+  Am6
Brasil!
    G   G7  F#7  F7
Brasil!
E  F  E       F           E      F  E
Oi      estas fontes murmurantes
           F            E    F  E
Oi onde eu mato a minha sede
         E7          Am   Am5+  Am6  Am5+
E onde a lua vem brincar
Am  Am5+  Am6         Am7(b5)        Bm7
O,            esse Brasil lindo e trigueiro
G             Em7      A7
   É o meu Brasil brasileiro
Am7         D7(9)      G    Em7
   Terra de samba e pandeiro
   Am7  D7(9) G    Em7
Brasil!    Brasil!
    Am7  D7(9)  G    G5+   G6   G5+
Pra mim,    pra mim

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Am5+ = 5 8 7 5 6 5
Am6 = 5 X 4 5 5 X
Am7 = X 0 2 0 1 0
Am7(b5) = 5 X 5 5 4 X
Bm7 = X 2 4 2 3 2
D7(9) = X 5 4 5 5 X
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
F#7 = 2 4 2 3 2 2
F7 = 1 3 1 2 1 1
G = 3 2 0 0 0 3
G5+ = 3 X 1 0 0 X
G6 = 3 X 2 4 3 X
G6(9) = X X 5 4 5 5
G7 = 3 5 3 4 3 3
Gº = 3 X 2 3 2 X
