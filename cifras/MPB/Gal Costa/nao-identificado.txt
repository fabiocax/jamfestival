Gal Costa - Não Identificado

Intro: G D G D G D

D           G               Bm
Eu vou fazer uma canção pra ela
Em             A7          D
 Uma canção singela, brasileira
G                       D          G
Para lançar depois do carnaval
D             G               Bm
Eu vou fazer um iê-iê-iê romântico
    Em       A7            D
Um anticomputador sentimental

G                    C           D7
Eu vou fazer uma canção de amor
G                      C           D7
Para gravar num disco voador
G                    C           D7
Eu vou fazer uma canção de amor
G                       C       A
Para gravar num disco voador


D           G               Bm
Uma canção dizendo tudo a ela
Em                A7       D
Que ainda estou sozinho, apaixonado
G                       D          G
Para lançar no espaço sideral
D             G               Bm
Minha paixão há de brilhar na noite
    Em          A7            D
No céu de uma cidade do interior

G                       C           D7
Como um objeto não identificado
G                       C           D7
Como um objeto não identificado
G                          C        D7
Que ainda estou sozinho, apaixonado
G                       C           D7
Como um objeto não identificado
G                      C            D7
Para gravar num disco voador
G                    C              D7
Eu vou fazer uma canção de amor
G                        C          D7
Como um objeto não identificado

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
