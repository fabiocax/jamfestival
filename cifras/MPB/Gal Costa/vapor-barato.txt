Gal Costa - Vapor Barato

[Intro] Am  G  F  E
        Am  G  F  E

    Am
Oh, sim
                 G                     F
Eu estou tão cansado, mas pra não pra dizer
                 E             E7
Que eu não acredito mais em você
Am                    G
Com minhas calças vermelhas
                     F           E  E7
meu casaco de general cheio de anéis
          A                   G
Eu vou descendo por todas as ruas
          F                  E  E7
Eu vou tomar aquele velho navio

          Am
Eu não preciso de muito dinheiro
G
  Graças a Deus

          F           E E7
E não importa, honey


            Am     G           F   E E7
Minha honey baby, baby, honey baby
               Am      G
Oh minha honey baby, baby
       F   E  E7
Honey baby

    Am              G     F
Oh! Sim eu estou cansado mas não pra dizer
                   E     E7
Que eu estou indo embora
Am                           G
Talvez eu volte, um dia eu volto
 F                              E  E7
mas quero esquecê-la eu preciso
Am                         G
Oh minha grande, oh minha pequena
F                      E  E7
Oh minha grande obsessão

               Am     G         F   E  E7
Oh minha honey baby baby honey baby
               Am           G
Oh minha honey baby, honey baby
       F  E  E7
Honey baby

Am
Ando tão à flor da pele
                G
Que qualquer beijo de novela me faz chorar
F
Ando tão à flor da pele
          E                       E7
Que teu olhar flor na janela me faz morrer
Am
Ando tão à flor da pele
           G
Que meu desejo se confunde com a vontade de não ser
F
Ando tão à flor da pele
              E                  E7
Que a minha pele tem o fogo do juízo final

    Dm7
Um barco sem porto

Sem rumo, sem vela
   Am7
Cavalo sem sela
    D#º
Um bicho solto

Um cão sem dono

Um menino, um bandido
    E7
Às vezes me preservo, noutras, suicido

Am          G         F              E  E7
Baby honey baby baby baby.... honey baby
               Am           G
Oh minha honey baby, honey baby
       F                            E  E7
Honey baby, baby, baby, baby, baby, baby...

Am
Ando tão à flor da pele
                G
Que qualquer beijo de novela me faz chorar
F
Ando tão à flor da pele
          E                       E7
Que teu olhar flor na janela me faz morrer
Am
Ando tão à flor da pele
           G
Que meu desejo se confunde com a vontade de nem ser
F
Ando tão à flor da pele
              E                  E7
Que a minha pele tem o fogo do juízo final

Am          G           F                  E  E7
Baby honey baby, honey baby, baby, baby, baby
               Am           G
Oh minha honey baby, honey baby
       F                        E  E7
Honey baby, baby, baby, baby, baby

( Am G F E E7 )

Am
Ando tão à flor da pele
                G
Que qualquer beijo de novela me faz chorar
F
Ando tão à flor da pele
          E                       E7
Que teu olhar flor na janela me faz morrer
Am
Ando tão à flor da pele
           G
Que meu desejo se confunde com a vontade de nem ser
F
Ando tão à flor da pele
              E                  E7
Que a minha pele tem o fogo do juízo final

Am
Baby

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
D#º = X X 1 2 1 2
Dm7 = X 5 7 5 6 5
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
