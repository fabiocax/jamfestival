Gal Costa - A História de Lily Braun

(De: Chico Buarque & Edu Lobo)

Intr.: C7(#9) / F7(9) / D7(#9) / G7(b13) / C7(#9) / F7(9) / D7(#9) / G7(b13) /
       C7(#9) / F7(9) / D7(#9) / G7(b13) / C7(#9) / F7(9) / D7(#9) / G7(b13) /

C7(#9)      F7(9)   D7(#9)         G7(b13)
Como  num romance O homem dos meus sonhos
    C7(#9)     F7(9)   D7(#9)       G7(b13)
Me apareceu no dancing Era   mais um
C7(#9)          F7(9) D7(#9)           G7(b13)
Só    que num relance Os    seus olhos me     chuparam
C7(#9)       F7(9)  D7(#9)  G7(b13)
Feito um zoom
C7(#9)     F7(9)  D7(#9)        G7(b13)
Ele   me comi___a Com   aqueles olhos
     C7(#9)       F7(9)  D7(#9)            G7(b13)
De comer   fotografi___a Eu    disse cheese
C7(#9)            F7(9)  D7(#9)           G7(b13)
E     de close em close  Fui   perdendo a pose
        C7(#9)      F7(9)  D7(#9)  G7(b13)
E até sorri,  feliz


C7/4(9/13)           C7(9)      C7(#9)        F7(13)
E         voltou Me ofereceu um drinque Me chamou   de anjo azul
        A7/4(9)            A7(b9)             D7(#9) Ab7(9/#11) G7/4(9/13) Db7(9/#11)
Minha visão    Foi desde então   ficando flou

C7(#9)     F7(9) D7(#9)           G7(b13)
Como  no cinema  Me    mandava às vezes
    C7(#9)     F7(9)  D7(#9)       G7(b13)
Uma rosa  e um poema  Foco  de luz
C7(#9)          F7(9) D7(#9)        G7(b13)
Eu,   feito uma gema  Me    desmilingüindo toda
   C7(#9)         F7(9)  D7(#9)  G7(b13)
Ao som   do blues
C7(#9)       F7(9)  D7(#9)        G7(b13)
Abu___sou do scoth  Disse que meu corpo
       C7(#9)       F7(9)  D7(#9)             G7(b13)
Era só dele  aquela noite  Eu    disse please
C7(#9)     F7(9)  D7(#9)          G7(b13)
Xale  no decote   Disparei com as faces
       C7(#9)       F7(9)  D7(#9)  G7(b13)
Rubras e     febris

C7/4(9/13)          C7(9)      C7(#9)           F7(13)
E         voltou No derradeiro show  Com dez poe______mas e um buquê
          A7/4(9)              A7(b9)           D7(#9) Ab7(9/#11) G7/4(9/13) Db7(9/#11)
Eu disse adeus   Já vou com os meus  Numa turnê

E7(#9) / Bb7(9/#11) / A7/4(9/13) / Eb7(9/#11) /

D7(#9)       G7(9)   E7(#9)         A7(b13)
Como  amar espo___sa Disse ele que agora
       D7(#9)       G7(9)   E7(#9)         A7(b13)
Só me amava  como espo___sa Não   como star
    D7(#9)    G7(9) E7(#9)           A7(b13)
Me amassou as rosas Me    queimou as fotos
      D7(#9)        G7(9)  E7(#9)  A7(b13)
Me beijou   no altar
D7(#9)       G7(9) E7(#9)       A7(b13)
Nunca mais romance Nunca mais cinema
      D7(#9)           G7(9)   E7(#9)           A7(b13)
Nunca mais  drinque no dancing Nunca mais cheese
D7(#9)        G7(9)  E7(#9)     A7(b13)
Nunca uma espelunca  Uma   rosa nunca
      D7(#9)      G7(9)  E7(#9)  A7(b13)
Nunca mais  feliz

D7/4(9/13) / / / D7(9) / D7(#9) / G7(13) / / / / / / / B7/4(9) / / / B7(b9) / / /
E7(#9) / Bb7(9/#11) / A7/4(9/13) / Eb7(9/#11) /

D7(#9)       G7(9) E7(#9)       A7(b13)
Nunca mais romance Nunca mais cinema
      D7(#9)           G7(9)   E7(#9)           A7(b13)
Nunca mais  drinque no dancing Nunca mais cheese
D7(#9)        G7(9)  E7(#9)     A7(b13)
Nunca uma espelunca  Uma   rosa nunca
      D7(#9)      G7(9)  E7(#9)  A7(b13)
Nunca mais  feliz

D7(#9) / G7(9) / E7(#9) / A7(b13) / D7(#9) / G7(9) / E7(#9) / A7(b13) /
D7(#9) / G7(9) / E7(#9) / A7(b13) / D7(#9) / G7(9) / E7(#9) / A7(b13) / D7(#9)

ACORDES:

C7(#9)     - X3234X
F7(9)      - XX3243     E7(#9)     - X7678X
D7(#9)     - X5456X     Bb7(9/#11) - 6X655X
G7(b13)    - 3X344X     A7/4(9/13) - X05432
C7/4(9/13) - X33335     Eb7(9/#11) - X65665
C7(9)      - X3233X     G7(9)      - XX5465
F7(13)     - 1X123X     A7(b13)    - 5X566X
A7/4(9)    - X0543X     D7/4(9/13) - X55500
A7(b9)     - X0532X     D7(9)      - X5455X
Ab7(9/#11) - 4X433X     G7(13)     - 3X345X
G7/4(9/13) - 3X3210     B7/4(9)    - 7X765X
Db7(9/#11) - X43443     B7(b9)     - 7X754X

----------------- Acordes -----------------
A7(b13) = X 0 X 0 2 1
A7(b9) = 5 X 5 3 2 X
A7/4(9) = 5 X 5 4 3 X
A7/4(9/13) = X 0 0 0 0 2
Ab7(9/#11) = 4 3 4 3 3 X
B7(b9) = X 2 1 2 1 X
B7/4(9) = X 2 2 2 2 2
Bb7(9/#11) = X 1 0 1 1 0
C7(#9) = X 3 2 3 4 X
C7(9) = X 3 2 3 3 X
C7/4(9/13) = X 3 3 3 3 5
D7(#9) = X 5 4 5 6 X
D7(9) = X 5 4 5 5 X
D7/4(9/13) = X 5 5 5 5 7
Db7(9/#11) = X 4 3 4 4 3
E7(#9) = X 7 6 7 8 X
Eb7(9/#11) = X X 1 2 2 1
F7(13) = 1 X 1 2 3 X
F7(9) = X X 3 2 4 3
G7(13) = 3 X 3 4 5 X
G7(9) = 3 X 3 2 0 X
G7(b13) = 3 X 3 4 4 3
G7/4(9/13) = 3 X 3 2 1 0
