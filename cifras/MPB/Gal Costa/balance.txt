Gal Costa - Balancê

 A       E7      A
Ô balancê balancê
 D                 C#7
Quero dançar com você
 F#m      C#m    F#m
Entra na roda morena pra ver
Bm      E7     A E7
Ô balancê balancê

 A                    C#7
Quando por mim você passa
 D                   A
Fingindo que não me vê
 D      Dm     C#m7     F#7
Meu coração quase se despedaça
 Bm      E7     A
No balancê balancê

(refrão)
 A                  C#7
Você foi minha cartilha

  D            A
Você foi meu abc
D            Dm       C#m7     F#7
E por isso eu sou a maior maravilha
B7       E7      A E7
No balancê balancê

(refrão)
A                  C#7
Eu levo a vida pensando
 D               A
Pensando só em você
 D          Dm       C#m7       F#7
E o tempo passa e eu vou me acabando
B7       E7      A E7
No balancê balancê

(refrão)

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C#7 = X 4 3 4 2 X
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
F#7 = 2 4 2 3 2 2
F#m = 2 4 4 2 2 2
