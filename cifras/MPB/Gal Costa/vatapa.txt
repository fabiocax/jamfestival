Gal Costa - Vatapá

D6/9             G/A              D6/9
  QUEM QUIZER VATAPÁ QUE PROCURE FAZER PRIMEIRO
     G/A            D6/9
  O FUBÁ DEPOIS O DENDÊ
             G/A                     D6/9
  PROCURE UMA NEGA BAIANA QUE SAIBA MEXER
              G/A             D6/9
  QUE SAIBA MEXER,QUE SAIBA MEXER
              G/A                    D6/9
  PROCURE UMA NEGA BAIANA QUE SAIBA MEXER
              G/A            D6/9
  QUE SAIBA MEXER,QUESAIBA MEXER

                  G/A              D6/9
  QUEM QUIZER VATAPÁ QUE PROCURE FAZER PRIMEIRO
      Go             D7
  O FUBA DEPOIS O DENDÊ
             Em7/9        A7           D
  PROCURE UMA NEGA BAIANA QUE SAIBA MEXER
   B7         Em7/9  A7         D7/9
  QUE SAIBA MEXER,  QUE SAIBA MEXER

  G#7/5-     G7+
  UMA NEGA BAIANA QUE SAIBA
      Gm7    F#7/5+  B7/9        Em7/9  A7
  QUE SAIBA MEXER    QUE SAIBA MEXER QUE SAIBA

D6/9               G/A                D6/9
BOTA CASTANHA DE CAJU  UM BOCADINHO A MAIS
             G/A                       D6/9
PIMENTA MALAGUETA      UM BOCADINHO A MAIS
 G7+  G#o     D
AMENDOIM,CAMARÃO
        B7  Em7/9   A7      Am7
RALA O COCO NA HORA DE MACHUCAR
G#m7/5-    Gm7       F#7/5+       B7/9
SAL COM GENGIBRE E CEBOLA AI, AI, AI
Em7/9    A7      D
NA HORA DE TEMPERAR
              G/A                   D6/9
NÃO PARA DE MEXER QUE É PRA NÃO EMBOLAR
          G/A                D6/9
PANELA NO FOGO NÃO DEIXA QUEIMAR
                      G/A                       D6/9
COM QUALQUER DEZ MIL RÉIS E UMA NEGA SE FAZ VATAPÁ
G/A            D6/9
  QUE BOM VATAPÁ

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
B7/9 = X 2 1 2 2 X
D = X X 0 2 3 2
D6/9 = X 5 4 2 0 0
D7 = X X 0 2 1 2
D7/9 = X 5 4 5 5 X
Em7/9 = X 7 5 7 7 X
F#7/5+ = 2 X 2 3 3 2
G#7/5- = 4 X 4 5 3 X
G#m7/5- = 4 X 4 4 3 X
G/A = 5 X 5 4 3 X
G7+ = 3 X 4 4 3 X
Gm7 = 3 X 3 3 3 X
