Gal Costa - Você Não Gosta de Mim

[Intro] Eb7M(9)  Bb7(13)

Eb7M              Gm7
Você não gosta de mim
Cm                 Fm7
Não sinto o ar se aquecer
             Bb7
Ao redor de você
                  Eb7M     Bb7(13)
Quando eu volto da estrada

Eb7M                Gm7
Por que será que é assim?
Cm7                   Fm7
Dou aos seus lábios a mão
                  Bb7
E eles nem dizem não
                  E7(9)
E eles não dizem nada
Ab7M              A°
Como é que vamos viver

Gm7              E°
Gerando luz sem calor?
F7
Que imagem do amor
                Fm7     Bb7(13)
Podemos nos oferecer?

Eb7M              Gm7
Você não gosta de mim
Cm7               Fm7
Que novidade infeliz
                Bb7
O seu corpo me diz
                 Eb7M  Bb7(13)
Pelos gestos da alma!

Eb7M              Gm7
A gente vê que é assim
Cm7                  Fm7
Seja de longe ou de perto
                Bb7
No errado e no certo
                Eb7(9)
Na fúria e na calma

Ab7M               A°
Você me impede de amar
Gm7                  E°
E eu que só gosto do amor
F7
Por que é que não nos
Fm7         Bb7       Eb7M   G7
Dizemos que tudo acabou?

Cm7M    Cm7       Dm7(b5)
Talvez assim descubramos
       G7        Cm7     G7
O que é que nos une
Cm7M  Cm7        Dm7(b5)
Medo, destino, capricho
       G7       Gm7   C7(9)
Ou um mistério maior
Fm7                  F#°
Eu jamais cri que o ciúme nos
          Gm7        C7(9)
Tornasse imunes ao desamor
F7
Então, por favor
            Fm7      Bb7(13)
Evite esse costume ruim

Eb7M              Gm7
Você não gosta de mim
Cm7            Fm7
É só ciúme vazio
               Bb7
Essa chama de frio
               Eb7M
Esse rio sem água

Eb7M               Gm7
Por que será que é assim?
Cm7                Fm7
Somente encontra motivo
               Bb7
Pra manter-se vivo
                Eb7(9)
Este amor pela mágoa

Ab7M           A°
Então digamos adeus
Gm7              E°
E nos deixemos viver
Fm7                   Bb7
Já não faz nenhum sentido
             Eb7M
Eu gostar de você

----------------- Acordes -----------------
Ab7M = 4 X 5 5 4 X
A° = 5 X 4 5 4 X
Bb7 = X 1 3 1 3 1
Bb7(13) = 6 X 6 7 8 X
C7(9) = X 3 2 3 3 X
Cm = X 3 5 5 4 3
Cm7 = X 3 5 3 4 3
Cm7M = X 3 5 4 4 3
Dm7(b5) = X X 0 1 1 1
E7(9) = X X 2 1 3 2
Eb7(9) = X 6 5 6 6 X
Eb7M = X X 1 3 3 3
Eb7M(9) = X 6 5 7 6 X
E° = X X 2 3 2 3
F#° = 2 X 1 2 1 X
F7 = 1 3 1 2 1 1
Fm7 = 1 X 1 1 1 X
G7 = 3 5 3 4 3 3
Gm7 = 3 X 3 3 3 X
