Gal Costa - Flor de Maracujá

Intro:  C6/9

C6/9
Lá no avarandado na luz do meio dia
Dm7                  Em   Dm7   G7       C6/9
O segredo dos teus olhos tanta coisa me dizia
O cabelo solto ao vento, o teu jeito de olhar
Dm7              Em    Dm7    G7     C6/9
E no seu corpo moreno, a flor de maracujá
G4
Dia de sol, cheiro de flor
              C6/9
Gosto de mar, amor
G4
Na tua cor, luz do luar
                 C6/9
Vento que vem do mar
C6/9
Roda, gira vira o vento, meu amor vai te levar
Dm7                  Em    Dm7   G7         C6/9
Bem pra lá do fim do mundo onde eu vou te chamar

----------------- Acordes -----------------
C6/9 = X 3 2 2 3 3
Dm7 = X 5 7 5 6 5
Em = 0 2 2 0 0 0
G4 = 3 5 5 5 3 3
G7 = 3 5 3 4 3 3
