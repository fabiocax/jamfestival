João Gilberto - Trevo de 4 Folhas

A7+
Vivo esperando e procurando
   B7/13           B7/5+
Um trevo no meu jardim
E7/9                A7+      F#m7
Quatro folhinhas nascidas ao léu
B7/13  B7/5+   E7/9     E7/9-
Me levariam pertinho do céu
A7+
Feliz eu seria e o trevo faria
    B7/13            B7/5+
Que ela voltasse pra mim
E7/9             A7+  F#m7
Vivo esperando e procurando
   B7/13 B7/5+ E7/9 E7/9- A7+
Um trevo    no meu  jar...dim

----------------- Acordes -----------------
A7+ = X 0 2 1 2 0
B7/13 = X 2 X 2 4 4
B7/5+ = X 2 X 2 4 3
E7/9 = X X 2 1 3 2
E7/9- = X X 2 1 3 1
F#m7 = 2 X 2 2 2 X
