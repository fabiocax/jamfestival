João Gilberto - Wave

[Intro] D6/9/11+

D7+/9            Bbº                 Am7
Vou te contar, os olhos já não podem ver
D7/9-       G7+    Gm6            F#7/13 F#5+/7
Coisas que só o coração pode entender
F#m7  B7/9-     E7/9
Fundamental é mesmo o amor
    Bb7/9        A7/9-     Dm7/9
É impossível ser feliz sozinho
          D7+/9 Bbº                   Am7
O resto é mar, é tudo que eu nem sei contar
D7/9-       G7+           Gm6          F#7/13 F#5+/7
São coisas lindas que eu tenho pra te dar
F#m7   B7/9-    E7/9
Fundamental é mesmo o amor
   Bb7/9          A7/9-  Dm7/9
É impossível ser feliz sozinho
Gm7        C/Bb       Am7
Da primeira vez era a cidade
G#/Bb        Bb/G#        Gm7
Da segunda, o cais e a eternidade

G/A        D7+/9 Bbº                     Am7
Agora eu já sei da onda que se ergueu no mar
D7/9-     G7+         Gm6           F#7/13 F#5+/7
E das estrelas que esquecemos de contar
F#m7   B7/9-       E7/9
O amor se deixa surpreender
            Bb7/9          A7/9- Dm7/9 D6/9/11+
Enquanto a noite vem nos envolver

----------------- Acordes -----------------
A7/9- = 5 X 5 3 2 X
Am7 = X 0 2 0 1 0
B7/9- = X 2 1 2 1 X
Bb/G# = 4 X 3 3 3 X
Bb7/9 = X 1 0 1 1 X
Bbº = X 1 2 0 2 0
C/Bb = X 1 2 0 1 X
D7+/9 = X 5 4 6 5 X
D7/9- = X 5 4 5 4 X
Dm7/9 = X 5 3 5 5 X
E7/9 = X X 2 1 3 2
F#5+/7 = 2 X 2 3 3 2
F#7/13 = 2 X 2 3 4 X
F#m7 = 2 X 2 2 2 X
G#/Bb = X 1 1 1 1 X
G/A = 5 X 5 4 3 X
G7+ = 3 X 4 4 3 X
Gm6 = 3 X 2 3 3 X
Gm7 = 3 X 3 3 3 X
