João Gilberto - Amor Certinho

Intro: D7+

Em7       A7-/9  D7+  D6
Amor a primeira vista
Em7       A7-/9   C#º C#m7
Amor de primeira mão
C#m4/7     C7/5-   Bm7      Bm6
É ele que chega cantando, sorrindo
 Bm7      Bm6     Em7  A7-/9
Pedindo entrada no coração
 Em7    A7-/9   D7+    D6
O nosso amor já tem patente
      Em7    A7-/9      F#m               B7
Tem marca registrada, é amor que a gente sente
    G7+          G#º        F#m7     B7
Eu gravo até em disco todo esse meu carinho
      Em7      Eº          D7+       B7
Todo mundo vai saber o que é amar certinho
 G7+        Gm6             F#m7
Esse tal de amor não foi inventado
      B7          E7       A7              F#m7            B7
Foi negócio bem bolado direitinho pra nós dois, foi ou não foi?

 G7+        Gm6             F#m7
Esse tal de amor não foi inventado
      B7          Em7     A7-/9            D7+
Foi negócio bem bolado direitinho pra nós dois

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
Bm6 = 7 X 6 7 7 X
Bm7 = X 2 4 2 3 2
C#m4/7 = X 4 4 4 5 4
C#m7 = X 4 6 4 5 4
C#º = X 4 5 3 5 3
C7/5- = X 3 4 3 5 X
D6 = X 5 4 2 0 X
D7+ = X X 0 2 2 2
E7 = 0 2 2 1 3 0
Em7 = 0 2 2 0 3 0
Eº = X X 2 3 2 3
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
G#º = 4 X 3 4 3 X
G7+ = 3 X 4 4 3 X
Gm6 = 3 X 2 3 3 X
