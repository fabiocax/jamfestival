João Gilberto - Ela é carioca

Introdução: F#m7/5- Fm6 C7M/9 Am7 D7/13 D7/13b Dm7 Fm7M/5- C7M/5+
            F#m7/5- Fm6 Bb7/9 C7M/9 Am7 D7/13 D7/13b Dm7 Fm7M/5- C7M/5+

      C7M            Am7
Ela é carioca, ela é carioca
D7/9                        Dm7/9
Basta o jeitinho dela andar
                  Eb7/9   D7/9     Abº
Nem ninguém tem carinho assim para dar
   Gm7                  C7/9
Eu vejo na cor dos seus olhos
             D/F#     Fm6
As noites do Rio ao luar
         C7M/9 B7/9#
Vejo a a mesma luz,
       Bb7M/9  A7/9#       A7M       Db7/13
vejo o mesmo   céu, vejo o mesmo mar
      C7M            Am7
Ela é meu amor, só viveu pra mim
D7/9                          Dm7/9
A mim que vivi para encontrar

               Eb7/9   D7/9      Abº
Na luz do seu olhar, a paz que sonhei
   Gm7                      C7/9
Só sei que eu sou louco por ela
                D/F#     Fm6
E pra mim ela é linda demais
   C7M/9  B7/9#
E além do mais,
  Bb7M/9                 C7M/5+
Ela é carioca, ela é carioca

Fm7M/5- : XX3454
C7M/5+  : X3210X

----------------- Acordes -----------------
A7/9# = X X 7 6 8 8
A7M = X 0 2 1 2 0
Abº = 4 X 3 4 3 X
Am7 = X 0 2 0 1 0
B7/9# = X 2 1 2 3 X
Bb7/9 = X 1 0 1 1 X
Bb7M/9 = X 1 0 2 1 X
C7/9 = X 3 2 3 3 X
C7M = X 3 2 0 0 X
C7M/5+ = X 3 2 1 0 0
C7M/9 = X 3 2 4 3 X
D/F# = 2 X 0 2 3 2
D7/13 = X 5 X 5 7 7
D7/13b = X 5 X 5 7 6
D7/9 = X 5 4 5 5 X
Db7/13 = X 4 X 4 6 6
Dm7 = X 5 7 5 6 5
Dm7/9 = X 5 3 5 5 X
Eb7/9 = X 6 5 6 6 X
F#m7/5- = 2 X 2 2 1 X
Fm6 = 1 X 0 1 1 X
Gm7 = 3 X 3 3 3 X
