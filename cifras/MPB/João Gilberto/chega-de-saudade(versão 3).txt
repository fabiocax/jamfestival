João Gilberto - Chega de Saudade

 (intro)
Gm7/9 Dm7 B° A#m6 Dm7 D#7/9/5-


  Dm7           B°
  Vai minha tristeza
          A#m6
  E diz a ela
          Dm7
  Que sem ela não pode ser
          Bm7/5- Am7                A#7+
  Diz-lhe numa prece que ela regresse
                 A7          A7/5+
  Porque eu não posso mais sofrer

  Dm7         B°        A#m6              Am6
  Chega de saudade, a realidade é que sem ela
          A°          Gm7      A7     Dm7
  Não há paz, não há beleza, é só tristeza
                          B°             A#m6            Dm7
  E a melancolia que não sai de mim, não sai de mim, não sai


    (passagem)
    Em7 A7/13


  D7+/F# F#°         E7/9
  Mas se ela voltar, se ela voltar
           Em7/9   A7/9-     F#° Bm7/F#
  Que coisa linda, que coisa lou-ca
                     F°        Em7/9
  Pois há menos peixinhos a nadar no mar
                E7/9                      Gm6  A7
  Do que os beijinhos que eu darei na sua boca

  D7+/F#          E7/9      F#7            Bm7   A#m7
  Dentro dos meus braços os abraços hão de ser milhões de abraços
  A° G7+           Gm7           F#m7
  Apertado assim, colado assim, calado assim
  F#m7    B7   E7/9     A7/4/13          F#7
  Abraços e beijinhos e carinhos sem ter fim

             B7              E7/9            A7/9/4    D7+
  Que é pra acabar com esse negócio de viver longe de mim
  D6         F#°        E7/9       A7/9/4       D7+
  Não quero mais esse negócio de você viver assim
  D6        F#°         E7/9      A7/9/4        D7+
  Vamos deixar desse negócio de você viver sem mim

----------------- Acordes -----------------
A#7+ = X 1 3 2 3 1
A#m6 = 6 X 5 6 6 X
A#m7 = X 1 3 1 2 1
A7 = X 0 2 0 2 0
A7/13 = X 0 X 0 2 2
A7/4/13 = X 0 2 0 3 2
A7/5+ = X 0 X 0 2 1
A7/9- = 5 X 5 3 2 X
A7/9/4 = 5 X 5 4 3 X
Am6 = 5 X 4 5 5 X
Am7 = X 0 2 0 1 0
A° = 5 X 4 5 4 X
B7 = X 2 1 2 0 2
Bm7 = X 2 4 2 3 2
Bm7/5- = X 2 3 2 3 X
Bm7/F# = X X 4 4 3 5
B° = X 2 3 1 3 1
D#7/9/5- = X X 1 2 2 1
D6 = X 5 4 2 0 X
D7+ = X X 0 2 2 2
D7+/F# = 2 X 0 2 2 2
Dm7 = X 5 7 5 6 5
E7/9 = X X 2 1 3 2
Em7 = 0 2 2 0 3 0
Em7/9 = X 7 5 7 7 X
F#7 = 2 4 2 3 2 2
F#m7 = 2 X 2 2 2 X
F#° = 2 X 1 2 1 X
F° = X X 3 4 3 4
G7+ = 3 X 4 4 3 X
Gm6 = 3 X 2 3 3 X
Gm7 = 3 X 3 3 3 X
Gm7/9 = X X 5 3 6 5
