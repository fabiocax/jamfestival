João Gilberto - Felicidade

                                (Happiness)
                                -----------
                written by Tom Jobim and Vinicius de Moraes

                        transcribed and translated by
                Jason Brazile (*) from:

                            Live in Montreux
                        Elektra Musician 9 60760-2


Notes:
        (1) under the grid, "x" means don't play this string
        (2) under the grid, "O" means play the open string
        (3) "minor 7 b5" is the same as "half-diminished 7"


 C maj 6 9           E min 7            F# min 6           E min 7
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | o o | | 2      | | | | o o 3      | | o | | |        | | o | | | 2
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
o | | | o |        | | | o | |        o | | o | o        | | | | o o
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | o | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
  x       x        O x x                x     x          x x

Tristeza           -lici-             -dade              sim
nao tem fin
fe-

    A7               D min 7              G7               C maj 7
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
o | o | o | 5      | o | o | | 5      o | o | o | 3      | o | | | o 3
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | o | |        | | | | o |        | | | o | |        | | | o | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | o | | |        | | | | | |        | | | | o |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
  x       x        x         x          x       x        x   x

                                                                   A

  C maj 9           B min 7 b5           E7 +5             C maj 6
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| o | | o | 3      | o | o | | 2      | | | o o |        | | | | o |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | o | |        | | o | o |        | | | | | |        | | o | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | o | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
x         x        x         x        O x O     x        x O   O   x

felici-            -dade e            a pluma            que o vento
                   como                                  vai le-


   G7 +5             G min 7            G dim 7            F maj 7
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
o | o | | | 3      o | o o o | 3      | | o | o | 2      o | | | o |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | o o |        | | | | | |        o | | o | |        | | o o | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
  x       x          x       x          x       x          x       x

-vando pelo        ar                                    Voa tao


   E7 +5             A min 7            D maj 6            A min 7
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | o o |        | | | | o |        | | | o | o 2      | | | | o |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | o | | |        | | | | | |        | | o | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | o        | | | | | |        | | | | | o
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
O x O     x        x O   x            x x O   O          x O   x

leve mas           tem a vida         breve Pre-         -cisa que
                                                         haja

  E7 +5              A min 7           G maj 6 9           C maj 9
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | o o |        | | | | o |        | | o o | | 2      | | o | | | 2
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | o | | |        o | | | | |        | o | | o |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | o        | | | | | |        | | | o | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
O x O     x        x O   x              x     O          x         x

vento sem          -rar               A felicidade       pobre pa-
pa-                                   do


  C maj 7            G min 7            G min 6            F maj 6
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| o | | | | 3      o | o o o | 3      | | o | | | 2      o | | | o |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | o | |        | | | | | |        o | | o o |        | | | o | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | o | o |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
x         x          x       x          x       x          x O     x

-rece              a grade ilu-       -sao do            Carnival


  F maj 7              D11               G7 +5             C maj 7
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
o | | | o |        | | | | o |        o | o | | | 3      | o | | | o 3
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | o o | |        | | | o | |        | | | o o |        | | | o | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | o        | | | | | |        | | | | o |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
  x       x        x x O                x       x        x   x

          A        gente traba-       -lha o             ano in-


  C maj 9           F# min 11             B7               E min 7
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | o | | | 2      o | | o | | 2      | o | o | | 2      | | | | o o 3
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| o | | o |        | | | | | |        | | | | | |        | | | o | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | o | |        | | | | | |        | | o | o |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
x         x          x x   O O        x         x        O x x

-teiro Por         um momen-          -to de sonho       fazer a
                                      pra

    A7               D min 7             E7 +5             A min 7
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | o | 2      | | | | o o        | | | o o |        | | | | o |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | o | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | o
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
x O x O   O        x x O              O x O     x        x O x O

fanta-             -sia                        de        rei ou de
                                                         pirata ou
                                                         jardi-

  D maj 6            D min 6            A min 7             E7 +5
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | o | o 2      | | | | | o        | | | | o |        | | | o o |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | o | |        | | o | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | o        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
x x O   O          x x O   O          x O   x            O x O     x

-neira                       e        tudo se aca-       -bar na
                                                         quarta


  A min 7           C maj 6 9           E min 7            F# min 6
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | o |        | | o o | | 2      | | | | o o 3      | | o | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | o | | |        o | | | o |        | | | o | |        o | | o | o
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | o        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
x O   x              x       x        O x x                x     x

feira Tri-         -steza nao         -lici-             -dade
                   tem fin fe-

  E min 9           C maj 6 9           E min 7            F# min 6
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | o | | o 2      | | o o | | 2      | | | | o o 3      | | o | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | o |        o | | | o |        | | | o | |        o | | o | o
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
x x   O              x       x        O x x                x     x

sim    Tri-        -steza nao         -lici-             -dade
                   tem fin fe-

  E min 9           A min 7 b5          D min 9               E7
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | o 2      | | o | o | 2      | | | | o |        | | | o | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | o |        | | | | | |        | | | o | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
O x x O            x O   O   x        x x O     O        O x O   O x


  C maj 6            C maj 7           B min 7 b5             E7
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | o |        | | o | | | 2      | o | o | | 2      | | | o | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | o | | |        | o | | | |        | | o | o |        | | o | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | o |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
x O   O   x        x     O O x        x         x        O x       x

                   A felici-          -dade e como       a gota de


  A min 7             G7 +5             G min 7            G dim 7
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | o |        o | o | | | 3      o | o o o | 3      | | o | o | 2
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | o | | |        | | | o o |        | | | | | |        o | | o | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | o        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
x O   x              x       x          x       x          x       x

orvalho numa       petala de          flor


  F maj 7             E7 +5             A min 7            D maj 6
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
o | | | o |        | | | o o |        | | | | o |        | | | o | o 2
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | o o | |        | | | | | |        | | o | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | o        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
  x       x        O x o     x        x O   x            x x O   O

Brilha tran-       -quilha de-        -pois de le-       -ve oscila e


  A min 7             E7 +5             A min 7           G maj 6 9
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | o |        | | | o o |        | | | | o |        | | o o | | 2
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | o | | |        | | | | | |        | | o | | |        o | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | o        | | | | | |        | | | | | o        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
x O   x            O X O     x        x O   x              x     O x

cai como uma       lagrima de         amor               A minha
                                                         felicidade
                                                         esta sonhando

  G min 7            G min 6            F maj 6            F maj 7
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
o | o o o | 3      | | o | | | 2      o | | | o |        o | | | o |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        o | | o o |        | | | o | |        | | o o | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
  x       x          x       x          x O     x          x       x

nos olhos          da minha           namora-            -da


    D11               G7 +5             C maj 7            C maj 9
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | o |        o | o | | | 3      | o | | | o 3      | | o | | | 2
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | o | |        | | | o o |        | | | o | |        | o | | o |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | o        | | | | | |        | | | | o |        | | | o | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
x x O                x       x        x   x              x         x

E como esta        noite pa-          -sando pa-         -sando

 F# min 11             B7               E min 7              A7
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
o | | o | | 2      | o | o | | 2      | | | | o o 3      | | | | o | 2
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | o | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | o | o |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
  x x   O O        x         x        O x x              x O x O   O

em busca da        madrugada          baixo por          fa-
                   Falem

  D min 7             E7 +5             A min 7            D maj 6
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | o o        | | | o o |        | | | | o |        | | | o | o 2
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | o | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | o        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
x x O              O x O     x        x O x O            x x O   O

-vor               pra que            ela acorde         dia
                                      alegre como o

  D min 6            A min 7             E7 +5             A min 7
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | o        | | | | o |        | | | o o |        | | | | o |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | o | |        | | | | | |        | | | | | |        | | o | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | o        | | | | | |        | | | | | o
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
x x O   O          x O x O            O x O     x        x O   x

         o-        -frecendo          beijos de a-       -mor   Tri-


 C maj 6 9           E min 7           F# min 6            E min 9
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | o o | | 2      | | | | o o 3      | | o | | |        | | o | | o 2
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
o | | | o |        | | | o | |        o | | o | o        | | | | o |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
  x       x        O x x                x     x          x x   O

-steza nao         -lici-             -dade              sim
tem fin fe-

    A7               D min 9              G7               C maj 7
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | o | o | 2      | | | | o |        o | o | o | 3      | o | | | o 3
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | o | |        | | | o | |        | | | o | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | o |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
x O   O   x        x x O     O          x       x        x   x

                                                                   A

  C maj 9           B min 7 b5           E7 +5             C maj 6
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| o | | o | 3      | o | o | | 2      | | | o o |        | | | | o |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | o | |        | | o | o |        | | | | | |        | | o | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | o | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
x         x        x         x        O x O     x        x O   O   x

felici-            -dade e            como a pluma       vento vai
                                      que o              le-

   G7 +5             G min 7            G dim 7            F maj 7
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
o | o | | | 3      o | o o o | 3      | | o | o | 2      o | | | o |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | o o |        | | | | | |        o | | o | |        | | o o | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
  x       x          x       x          x       x          x       x

-vando pelo        ar                                    Voa tao


  E7 +5              A min 7            D maj 6            A min 7
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | o o |        | | | | o |        | | | o | o 2      | | | | o |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | o | | |        | | | | | |        | | o | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | o        | | | | | |        | | | | | o
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
O x O     x        x O   x            x x O   O          x O   x

leve mas           tem a vida         breve pre-         -cisa que
                                                         haja


   E7 +5             A min 7            C maj 7            G min 7
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | o o |        | | | | o |        | | o | | | 2      o | o o o | 3
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | o | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | o        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
O x O     x        x O x O            x     O O x          x       x

vento sem          -rar               A felicidade       tao deli-
pa-                                   e uma coisa
                                      louca e

  G min 6            F maj 6            F maj 7              D11
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | o | | | 2      o | | | o |        o | | | o |        | | | | o |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
o | | o o |        | | | o | |        | | o o | |        | | | o | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | o
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
  x       x          x O     x          x       x        x x O

cada tam-          -bem                       Tem        flores e a-


    G7                G7 +5             C maj 7            C maj 9
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
o | o | o | 3      o | o | | | 3      | o | | | | 3      | | o | | | 2
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | o | |        | | | o o |        | | | o | |        | o | | o |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | o | o |        | | | o | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |        | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
  x       x          x       x        x         x        x         x

-mores                      de        todas as           cores Tem


 F# min 11             B 7               E min 7              A7
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
o | | o | | 2       | o | o | | 2      | | | | o o 3      | | o | o | 2
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |         | | | | | |        | | | o | |        | | | | | |
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |         | | o | o |        | | | | | |        | | | | | |
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |         | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
  x x   O O         x         x        O x x              x O   O   x

ninhos de           passarinhos               Tudo        bom     ela


  D min 9              E7 +5             A min 7            D maj 6
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | o |         | | | o o |        | | | | o |        | | | o | o 2
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | o | |         | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |         | | | | | |        | | | | | o        | | | | | |
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |         | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
x x O     O         O x O     x        x O x O            x x O   O

tem                        Pois        e por ela          delicada
                                       ser assim
                                       tao

  D min 6             A maj 7             E7 +5             A min 7
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | o         | | | | o |        | | | o o |        | | | | o |
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | o | |         | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |         | | | | | o        | | | | | |        | | | | | o
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |         | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
x x O   O           x O x O            O x O     x        x O x O

Que eu              trato dela         sempre muito       bem    Tri-


 C maj 6 9            E min 7            F# min 6           E min 7
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | o o | | 2       | | | | o o 3      | | o | | |        | | | | o o 3
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
o | | | o |         | | | o | |        o | | o | o        | | | o | |
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |         | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |         | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
  x       x         O x x                x     x          O x x

-steza nao          -lici-             -dade              sim    Tri-
tem fin fe-


 C maj 6 9            E min 7              B7               E min 9
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | o o | | 2       | | | | o o 3      | | o | | |        | | | | | o 2
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
o | | | o |         | | | o | |        o | | o | |        | | | | o |
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |         | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |         | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
  x       x         O x x                x     O x        O x x O

-steza nao          -lici-             -dade              sim    Tri-
tem fin fe-


 C maj 6 9            E min 7              B7               E min 9
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | o o | | 2       | | | | o o 3      | | o | | |        | | | | | o 2
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
o | | | o |         | | | o | |        o | | o | |        | | | | o |
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |         | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |         | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
  x       x         O x x                x     O x        O x x O

-steza nao          -lici-             -dade              sim    Tri-
tem fin fe-


 C maj 6 9            E min 7              B7               E min 9
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | o o | | 2       | | | | o o 3      | | o | | |        | | | | | o 2
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
o | | | o |         | | | o | |        o | | o | |        | | | | o |
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |         | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |         | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
  x       x         O x x                x     O x        O x x O

-steza nao          -lici-             -dade              sim     Tri-
tem fin fe-


 C maj 6 9            E min 7              B7               E min 9
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | o o | | 2       | | | | o o 3      | | o | | |        | | | | | o 2
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
o | | | o |         | | | o | |        o | | o | |        | | | | o |
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |         | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |         | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
  x       x         O x x                x     O x        O x x O

-steza nao          -lici-             -dade              sim    Tri-
tem fin fe-


 C maj 6 9            E min 7            F# min 6           E min 9
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | o o | | 2       | | | | o o 3      | | o | | |        | | | | o o 3
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
o | | | o |         | | | o | |        o | | o | o        | | | o | |
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |         | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |         | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
  x       x         O x x                x     x          O x x

-steza nao          -lici-             -dade              sim
tem fin fe-


 E min 6 9            D min 7               G7              C maj 7
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | o o 2       | | | | o o        o | o | o | 3      | o | | | o 3
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |         | | | o | |        | | | o | |        | | | o | |
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |         | | | | | |        | | | | | |        | | | | o |
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
| | | | | |         | | | | | |        | | | | | |        | | | | | |
+-+-+-+-+-+         +-+-+-+-+-+        +-+-+-+-+-+        +-+-+-+-+-+
O x x O             x x O                x       x        x   x
