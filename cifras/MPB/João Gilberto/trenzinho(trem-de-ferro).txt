João Gilberto - Trenzinho (Trem de Ferro)


  G7M       F7M       G7M
O trem blim blom blim blom
      F7M         G7M   F7M   G7M   F7M
Vai saindo da estação
  C7M   C#º   Dm7
E eu
      G7      C6/G
Deixo meu coração
          Am6
Com pouco mais
Com pouco mais
Com pouco mais
                   G7M   G7   F#7   F7   E7
Lá bem longe o meu bem
             Am7
Acenando com lenço
               A7
Bandeira da saudade
Am6    G7M
Muito além

          Am7
Acelera a marcha
  Am6          G6
O trem pelo sertão e eu
           B7/F#
Só levo saudade
           Em7
No meu coração
                    C7M
Lá na curva o trem apita
Desce a serra
       C#º    Bm7
E a saudade aumenta
    E7/9-        Am7
Uma coisa me atormenta
      Am6         G6
Vem falar do meu amor

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am6 = 5 X 4 5 5 X
Am7 = X 0 2 0 1 0
B7/F# = X X 4 4 4 5
Bm7 = X 2 4 2 3 2
C#º = X 4 5 3 5 3
C6/G = 3 X 2 2 1 X
C7M = X 3 2 0 0 X
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
E7/9- = X X 2 1 3 1
Em7 = 0 2 2 0 3 0
F#7 = 2 4 2 3 2 2
F7 = 1 3 1 2 1 1
F7M = 1 X 2 2 1 X
G6 = 3 X 2 4 3 X
G7 = 3 5 3 4 3 3
G7M = 3 X 4 4 3 X
