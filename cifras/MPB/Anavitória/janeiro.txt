Anavitória - Janeiro

[Intro] Dm Am7

          Dm           Am7
É que tu tem asas nos pés
        Bb9    Bbm  F     C
Que me faz querer voar também
      Dm                  Am7
Tu é paz, tu é luz, tu é mar
     Bb9 Bbm F C
É encaixe

          Dm               Am7
É que te sinto no meu paladar
        Bb9  Bbm      F     C
Que me faz querer cantar também
       Dm                    Am7
Te querer, te conter em meu lar
      Bb9  Bbm  F  A7         Dm
Se encaixe              em mim

             Am7                Dm
Que entre janeiros busco encontrar

            Am7                 Dm
A cor do cabelo que vai te agradar
               Am7              Bb9
O meu melhor cheiro pra sintonizar
     Bbm          F        A7      Dm
Teu sorriso mais belo pra desconcertar

               Am7             Dm
Rever teus segredos no particular
                 Am7               Dm
Revirar os teus medos sem me assustar
                 Am7              Bb9
Debruçar nos chamegos pra me acalmar
    Bbm          F   A7                Dm
É que tu foi feita      pra eu me encaixar

( Dm  Am7  Dm  Am7)

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am7 = X 0 2 0 1 0
Bb9 = X 1 3 3 1 1
Bbm = X 1 3 3 2 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
