Anavitória - Calendário

[Primeira Parte]

          F
Não se arrisque em tentar
         Dm7
Me escrever nas suas melhores linhas
           Am7
Eu não preciso de altar
          Bb9
Só vem repousa sua paz na minha
          F
Eu já te disse alguma vez
              Dm7
Tu tem pra mim   o nome mais bonito
        Am7
A boca canta ao te chamar
              Bb4      Bbm
Teu canto chama o meu sorriso

[Segunda Parte]


          Dm            Dm/C
E tá tão fácil de encaixar
           Dm/B
Os teus cenários na minha rotina
           Dm      Dm/C
No calendário passear
          Dm/B
O fim de tarde em qualquer esquina
         Bb
Tão natural que me parece sina
     Bbm
Vem cá

[Refrão]

                         Gm
Te sinto seu ao se entregar
    Bb9            Dm7
Me tenho inteira pra você
     Am                  Gm
Te guardo solto pra se aventurar
Bb9                    Dm
   É tão bonito te espiar viver
      Am                   Gm
Se encontra, se perde, se vê
     Bb9              Dm7
Mas volta pra se dividir
  A            Bb
Amor, é que você fica tão bem aqui
         Bbm                        F
É que você fica tão bem aqui comigo

[Primeira Parte]

          F
Não se arrisque em tentar
         Dm7
Me escrever nas suas melhores linhas
           Am7
Eu não preciso de altar
          Bb9
Só vem repousa sua paz na minha
         F
E eu te digo outra vez
              Dm7
Tu tem pra mim   o nome mais bonito
        Am7
A boca canta ao te chamar
              Bb4      Bbm
Teu canto chama o meu sorriso

[Segunda Parte]

          Dm            Dm/C
E tá tão fácil de encaixar
           Dm/B
Os teus cenários na minha rotina
           Dm      Dm/C
No calendário passear
          Dm/B
O fim de tarde em qualquer esquina
         Bb
Tão natural que me parece sina
     Bbm
Vem cá

[Refrão]

                         Gm
Te sinto seu ao se entregar
    Bb9            Dm7
Me tenho inteira pra você
     Am                  Gm
Te guardo solto pra se aventurar
Bb9                    Dm7
   É tão bonito te espiar viver
      Am                   Gm
Se encontra, se perde, se vê
     Bb9              Dm7
Mas volta pra se dividir
  A            Bb
Amor, é que você fica tão bem aqui
         Bbm                        F
É que você fica tão bem aqui comigo

[Solo] Dm  A
       F  A  F  A
       Dm  Dm/C  Dm/B

[Refrão]

                         Gm
Te sinto seu ao se entregar
    Bb9            Dm7
Me tenho inteira pra você
     Am                  Gm
Te guardo solto pra se aventurar
Bb9                    Dm7
   É tão bonito te espiar viver
      Am                   Gm
Se encontra, se perde, se vê
     Bb9              Dm7
Mas volta pra se dividir
  A            Bb
Amor, é que você fica tão bem aqui
         Bbm                        F
É que você fica tão bem aqui comigo

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Bb = X 1 3 3 3 1
Bb4 = X 1 3 3 4 1
Bb9 = X 1 3 3 1 1
Bbm = X 1 3 3 2 1
Dm = X X 0 2 3 1
Dm/B = X 2 X 2 3 1
Dm/C = X 3 X 2 3 1
Dm7 = X 5 7 5 6 5
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
