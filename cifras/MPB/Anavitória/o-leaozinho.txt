Anavitória - O Leãozinho

[Intro] G  C  Am  D  Ebº  Em
        C  Am  D7

[Primeira Parte]

G                    D7
  Gosto muito de te ver,leãozinho
Em                   Bm
   Caminhando sob o sol
C                    F            Em
  Gosto  muito de você, leãozinho
 G               D7
Para desentristecer, leãozinho
Em              Bm
O  meu coração tão só
 C                    F          G
Basta eu encontrar você no caminho

[Refrão]

Em               Em/D#           Em/D  C#m7(5-)
Um  filhote de leão, raio da manhã

C7M                 Em/B         Am  D7
Arrastando o meu olhar como um imã
Em                 Em/D#           Em/D  C#m7(5-)
O meu coração é o sol pai de toda cor
  C7M                   Em/B    Am  D7
Quando ele lhe doura a pele ao léu

[Segunda Parte]

 G                  D7
Gosto de te ver ao sol, leãozinho
Em                      Bm
   De te ver entrar no mar
 C             F          Em
Tua pele, tua luz, tua juba

 G                 D7
Gosto de ficar ao sol, leãozinho
 Em                Bm
De molhar minha juba
 C                   F             G
De estar perto de você e entrar numa

[Refrão]

Em               Em/D#           Em/D  C#m7(5-)
Um  filhote de leão, raio da manhã
C7M                 Em/B         Am  D7
Arrastando o meu olhar como um imã
Em                 Em/D#           Em/D  C#m7(5-)
O meu coração é o sol pai de toda cor
  C7M                   Em/B    Am  D7
Quando ele lhe doura a pele ao léu

( G  C  Am  D  Ebº  Em  C  Am  D7 )

[Refrão]

Em               Em/D#           Em/D  C#m7(5-)
Um  filhote de leão, raio da manhã
C7M                 Em/B         Am  D7
Arrastando o meu olhar como um imã
Em                 Em/D#           Em/D  C#m7(5-)
O meu coração é o sol pai de toda cor
  C7M                   Em/B    Am  D7
Quando ele lhe doura a pele ao léu

[Segunda Parte]

 G                 D7
Gosto de ficar ao sol, leãozinho
 Em                Bm
De molhar minha juba
 C                   F             Em  Am
De estar perto de você e entrar numa

C                    F            Em
  Gosto  muito de você, leãozinho

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C#m7(5-) = X 4 5 4 5 X
C7M = X 3 2 0 0 X
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Ebº = X X 1 2 1 2
Em = 0 2 2 0 0 0
Em/B = X 2 2 0 0 0
Em/D = X X 0 4 5 3
Em/D# = X X 1 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
