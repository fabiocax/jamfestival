﻿Anavitória - Pirlimpimpim

Capo Casa 2

   D       A      G    D
O olho grande da menina
D        A      G    D
É um pequenino olho grande
D             A     G       D
Que acende a luz daquela estrela
D           A        G       D
E aquela estrela é a mais distante
D       A      G    D
O sol entrando pela porta
D    A      G    D
E o luar pela janela
D          A    G    D
Se não me derem a resposta
D        A      G      D
Não saio não daqui sem ela

                G
Não tenha medo vovó
                 A
Não tenha medo mamãe

            Gbm                  Bm
Faz muito tempo que eu não via assim
         E                     A  G  D
Tá lá no livro tin tin por tin tin

          A  G  D
Pirlimpimpim
          A  G  D
Pirlimpimpim

----------------- Acordes -----------------
Capotraste na 2ª casa
A*  = X 0 2 2 2 0 - (*B na forma de A)
Bm*  = X 2 4 4 3 2 - (*C#m na forma de Bm)
D*  = X X 0 2 3 2 - (*E na forma de D)
E*  = 0 2 2 1 0 0 - (*F# na forma de E)
G*  = 3 2 0 0 0 3 - (*A na forma de G)
Gbm*  = 2 4 4 2 2 2 - (*Abm na forma de Gbm)
