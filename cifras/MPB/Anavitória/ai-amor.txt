Anavitória - Ai, Amor

[Introdução] E/B  F#11/C#  A9/E  A6/E

Parte 1 de 2

   E/B         F#11/C#           A9/E
E|------------------------------------------|
B|-------0-------0--------0-------0---------|
G|-------1-------3--------3-------3---------|
D|---2-------2--------4-------4-------------|
A|-2---2---2---2----4---4---4---4-----------|
E|------------------------------------------|

Parte 2 de 2

   A9/E         A6/E
E|------------------------------------------|
B|-------0-------7--------7-------7---------|
G|-------6-------6--------6-------6---------|
D|---7-------7--------7-------7-------------|
A|-7---7---7---7----7---7---7---7-----------|
E|------------------------------------------|


[Primeira parte]

E/B          F#11/C#
Ei, fiz questão da promessa lembrar
      A9/E
Tu jurou minha mão não soltar
   A6/E
E se foi junto dela

E/B         F#11/C#
Ei, cê não sabe a falta que faz
      A9/E
Será que teus dias tão iguais
    A6/E
Eu me pego pensando

[Primeira Parte]

Parte 1 de 2

   E/B             F#11/C#
E|------------------------------------------|
B|-------0-------0--------0-------0---------|
G|-------1-------1--------3-------3---------|
D|---2-------2--------4-------4-------------|
A|-2---2---2---2----4---4---4---4-----------|
E|------------------------------------------|

Parte 2 de 2

   A9/E         A6/E
E|------------------------------------------|
B|-------0-------7--------7-------7---------|
G|-------6-------6--------6-------6---------|
D|---7-------7--------7-------7-------------|
A|-7---7---7---7----7---7---7---7-----------|
E|------------------------------------------|

[Refrão]

    A
Ai, amor
   F#m         C#m
Será que tu divide a dor
    G#m          A
Do teu peito cansado
                A9           E/B  E7/B
Com alguém que não vai te sarar

      A
Meu amor
    F#m      C#m
Eu vivo no aguardo
    G#m         A
De ver você voltando

Cruzando a porta
       E/B  F#11/C#  A9/E  A6/E
Pararará

[Refrão]

Parte 1 de 4

   A                F#m
E|------------------------------------------|
B|-------2-------2--------2-------2---------|
G|-------2-------2--------2-------2---------|
D|---2-------2--------4-------4-------------|
A|-0---0---0---0----------------------------|
E|------------------2---2---2---2-----------|

Parte 2 de 4

   C#m              G#m
E|------------------------------------------|
B|-------5-------5--------4-------4---------|
G|-------6-------6--------4-------4---------|
D|---6-------6--------6-------6-------------|
A|-4---4---4---4----------------------------|
E|------------------4---4---4---4-----------|

Parte 3 de 4

   A                A9
E|------------------------------------------|
B|-------2-------2--------0-------0---------|
G|-------2-------2--------2-------2---------|
D|---2-------2--------2-------2-------------|
A|-0---0---0---0----2---2---2---2-----------|
E|------------------------------------------|

Parte 4 de 4

   E/B              E7/B
E|------------------------------------------|
B|-------0-------0--------3-------3---------|
G|-------1-------1--------1-------1---------|
D|---2-------2--------2-------2-------------|
A|-2---2---2---2----2---2---2---2-----------|
E|------------------------------------------|

Repete Parte 1 de 4

   A                F#m
E|------------------------------------------|
B|-------2-------2--------2-------2---------|
G|-------2-------2--------2-------2---------|
D|---2-------2--------4-------4-------------|
A|-0---0---0---0----------------------------|
E|------------------2---2---2---2-----------|

Repete Parte 2 de 4

   C#m              G#m
E|------------------------------------------|
B|-------5-------5--------4-------4---------|
G|-------6-------6--------4-------4---------|
D|---6-------6--------6-------6-------------|
A|-4---4---4---4----------------------------|
E|------------------4---4---4---4-----------|

Parte 3 de 4 (Variação)

   A
E|------------------------------------------|
B|-------2-------2--------------------------|
G|-------2-------2--------------------------|
D|---2-------2------------------------------|
A|-0---0---0---0----2-----------------------|
E|------------------------------------------|

[Segunda Parte]

E/B          F#11/C#
Ei, diz pra mim o que eu quero escutar
      A9/E
Só você sabe adivinhar
                   A6/E
Meus desejos secretos

E/B         F#11/C#
Ei, faz de conta que não percebi
       A9/E
Que você não esteve aqui
                    A6/E
Com teu jeito singelo

[Refrão]

    A
Ai, amor
   F#m         C#m
Será que tu divide a dor
    G#m          A
Do teu peito cansado
                             E/B  E7/B
Com alguém que não vai te sarar

      A
Meu amor
    F#m      C#m
Eu vivo no aguardo
    G#m         A
De ver você voltando, cruzando a porta

[Terceira Parte]

                 F#m
Sem hora pra voltar
A                   G#m
  Sem rota pra tua fuga
       C#m
Iá iá iá

                  A
Com tempo pra perder
               A9
Teu olho degradê pra colorir
         E/B          E7/B
Pra colorir, pra colorir

                 F#m
Sem hora pra voltar
A                   G#m
  Sem rota pra tua fuga
       C#m
Iá iá iá
               A
Com tempo pra perder
               A9         E/B  E7/B
Teu olho degradê pra colorir

[Terceira Parte]

Parte 1 de 4

   F#m              A
E|------------------------------------------|
B|-------4-------4--------2-------2---------|
G|-------4-------4--------2-------2---------|
D|---4-------4--------2-------2-------------|
A|------------------0---0---0---0-----------|
E|-2---2---2---2----------------------------|

Parte 2 de 4

   G#m              C#m
E|------------------------------------------|
B|-------4-------4--------5-------5---------|
G|-------4-------4--------6-------6---------|
D|---6-------6--------6-------6-------------|
A|------------------4---4---4---4-----------|
E|-4---4---4---4----------------------------|

Parte 3 de 4

   A                A9
E|------------------------------------------|
B|-------2-------2--------0-------0---------|
G|-------2-------2--------2-------2---------|
D|---2-------2--------2-------2-------------|
A|-0---0---0---0----2---2---2---2-----------|
E|------------------------------------------|

Parte 4 de 4

   E/B              E7/B
E|------------------------------------------|
B|-------0-------0--------3-------3---------|
G|-------1-------1--------1-------1---------|
D|---2-------2--------2-------2-------------|
A|-2---2---2---2----2---2---2---2-----------|
E|------------------------------------------|

[Refrão]

    A
Ai, amor
   F#m         C#m
Será que tu divide a dor
    G#m          A
Do teu peito cansado
                A9           E/B  E7/B
Com alguém que não vai te sarar

      A
Meu amor
    F#m      C#m
Eu vivo no aguardo
    G#m         A
De ver você voltando

Cruzando a porta
         E/B  F#11/C#  A9/E  A6/E
Parararará

E/B  F#11/C#  A9/E  A6/E
(Vocalize)

----------------- Acordes -----------------
A = X 0 2 2 2 0
A6/E = X 7 7 6 7 X
A9 = X 0 2 2 0 0
A9/E = X 7 7 6 0 0
C#m = X 4 6 6 5 4
E/B = X 2 2 1 0 0
E7/B = X 2 2 1 3 X
F#11/C# = X 4 4 3 0 X
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
