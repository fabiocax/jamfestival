Guilherme Arantes - Prelúdio - O Amor Nascer

Intro: C  G  Fm6/G#  C/G  G#7+  Fm6/G#  C/G  G#  Fm6

C   G            Am         F   G           C9/E C/E Dm7
Ah!   Se eu conseguisse te dizer    o quanto amei
G7     F9/C C           F   G            C9/E Dm7
E nunca soube   me expressar    eu me assustei
G7        F9/C C      F   G         C/E   Dm7
Pois tinha pressa de viver   pressa demais
G7/4  G7            C9/E C/E Dm7  G7        F9/C C      F
De aprender como é que faz        pra chegar perto de você
G C9/E C/E  G#7+  Fm6/G# Dm7/5-   G7/4  G  G#7+ Fm6/G#  Dm7/5- C7+/G
E revelar        aquele brilho no olhar       aquele jeito de sorrir
G#7+ Fm6/G#   F9/G C/G   G#7+ Fm6/G# C/G G# Fm6

   Ao ver o amor nascer
C G           Am        F  G      C9/E C/E Dm7
Ah! Se fosse fácil mergulhar numa ilusão
G7   F9/C C        F G              C9/E Dm7
E não conter à explosão de um grande amor
G7/4 G7            C9/E C/E Dm7  G7 F9/C C        F
De uma semente que esperou chegar seu tempo pra brotar
G C9/E C/E   G#7+   Fm6/G# Dm7/5- G7/4 G G#7+ Fm6/G# Dm7/5- C7+/G
E revelar      aquele brilho no olhar,     Aquele jeito de sorrir
G#7+ Fm6/G# G7/4 G7 C    G#7+    Fm6/G#    C
Ao ver o amor nascer.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
C/G = 3 3 2 X 1 X
C7+/G = 3 X 2 4 1 X
C9/E = X X 2 5 3 0
Dm7 = X 5 7 5 6 5
Dm7/5- = X X 0 1 1 1
F = 1 3 3 2 1 1
F9/C = X 3 X 5 6 3
F9/G = X X 5 5 6 3
Fm6 = 1 X 0 1 1 X
Fm6/G# = 4 5 3 5 3 X
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
G#7+ = 4 X 5 5 4 X
G7 = 3 5 3 4 3 3
G7/4 = 3 5 3 5 3 X
