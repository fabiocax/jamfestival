Guilherme Arantes - Todo Mês de Maio Na Maior

D                     Em
Não é caro o que eu queria 
                  Bm
Uma pausa pra pensar 
                 Em               G
Colocar o corpo e a cabeça em dia 
       A7         D
Pra melhor recomeçar  
 
D             Em
O outono na fazenda 
               Bm
Toda tarde cochilar 
                 Em                    G
Com o cheiro luxuoso de um fogão de lenha 
      A7          D
Perfumando todo o ar 
 
D              Em
Meu cavalo pelo vale 
             Bm
Vento no canavial 
                  Em                    G
Sol na pele avermelhando a nossa cara-pálida 
     A7   D
Da cida...de 

G  Em             Bm
Uhhhh...eu, você e só 
E           Em    A7
Todo mês de maio  
     D
Na maior

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
