Guilherme Arantes - Nossa Imensidão a Dois

[Intro] C  G/B  Am  C/G  
        F  Am/E  Dm  G

C        G
É só conforto
C            G                     C
Tudo que há nas memórias que eu buscar
            G                     Am           G#º           G
Lá no fundo as histórias nem são tão perfeitas como hoje parecem
       C/D
Se cristalizar
F            G          C
Num caleidoscópio de ilusão
             G
Nunca foi simples
C             G            C
No retrato precário que ficou
       G                  Am 
Tão borrado de lágrima e suor
        G#º            G
Nenhum atalho nem elevador
         C/D
Pra se pegar
F       Am/E          G
Além da maciez da sua mão

[Refrão]

       C   G/B
Eu te amo
          Am            C/G        F
Eu te abraço em nossa imensidão a dois
     Am/E       Dm           G
Transpiração do aroma de uma paz

F           F4
Vida tão íngreme
F            F4                  F
Cordas, grampos e pedras pra espetar
               F4              Dm
Cruz de pregos nos pés a nos prender
          Dbº            F/C
Destroços de naufrágios boiam
           G/B
Pra nos apegar
Bb                  Dm          C
Que eu nem sei como eu cheguei aqui
           G
O lema de hoje
C          G                  C
Quero mais do que nunca dar valor
            G
Mais do que todo sempre
        Am              G#º            G            C/D
Eu vejo nosso amor como uma construção de engenho e arte
F                  G
Pra além da inspiração

[Refrão]

       C   G/B
Eu te amo
          Am            C/G        F
Eu te abraço em nossa imensidão a dois
     Am/E       Dm           G
Transpiração do aroma de uma paz

       C   G/B
Eu te amo
          Am            C/G        F
Eu te abraço em nossa imensidão a dois
     Am/E       Dm           G
Transpiração do aroma de uma paz

       C   G/B
Eu te amo
          Am            C/G        F
Eu te abraço em nossa imensidão a dois
     Am/E       Dm           G
Transpiração do aroma de uma paz

       C  G/B  Am  C/G  F  Am/E  Dm  G
Eu te amo

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am/E = 0 X 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C/D = X X 0 0 1 0
C/G = 3 3 2 X 1 X
Dbº = X 4 5 3 5 3
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
F/C = X 3 3 2 1 1
F4 = 1 3 3 3 1 1
G = 3 2 0 0 0 3
G#º = 4 X 3 4 3 X
G/B = X 2 0 0 3 3
