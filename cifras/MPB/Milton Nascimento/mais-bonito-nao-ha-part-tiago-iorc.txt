Milton Nascimento - Mais Bonito Não Há (part. Tiago Iorc)

[Intro] C/G  Am7  Em7(9)/B
        C/G  Am7  Em7(9)/B

 C/G
Nada mais belo
    Am7
Que olhar de criança
    Em7(9)
No sol da manhã
  C/G
Chuva de carinho
         Am7
É o que posso pedir
 Em7(9)
Nessa imagem tão sã

 Bb
Lindo no horizonte
    F
O amanhã
             C/G
Que eu nunca esqueci


 Bb
Doce lembrança
    F
Do sonho
                G
Que eu vejo daqui

 G
Ser amor pra quem anseia
 F
Solidão de casa cheia
 G
Dar a voz que incendeia
 Am7               Am7/G      F
Ter um bom motivo para acreditar

                 Em
Mais bonito não há
          F7M
Pode acreditar
                 C/G  Am7  Em7(9)/B
Mais bonito não há

( C/G  Am7  Em7(9) )

 C/G
Nada mais belo
       Am7
Que abraço sereno
     Em7(9)
E sabor de perdão

 C/G
Ver a beleza
      Am7
E em gesto pequeno
    Em7(9)
Ter a imensidão

 Bb                F
Como espalhar por aí
                    C/G
Qualquer coisa que faça sorrir
Bb            F
Aquietar o silêncio
           G
Das dores daqui

 G
Ser amor pra quem anseia
 F
Solidão de casa cheia
 G
Dar a voz que incendeia
 Am7               Am7/G      F
Ter um bom motivo para acreditar

                 Em
Mais bonito não há
            F7M
Pode acreditar
                 Em
Mais bonito não há
            F7M
Pode acreditar

( Em7(9)/B  F  Em7(9)/B  F )
( Em7(9)/B  F  G )

Ser amor pra quem anseia
 F
Solidão de casa cheia
 Em
Dar a voz que incendeia
 Am7               Am7/G      F
Ter um bom motivo para acreditar

                 C/G
Mais bonito não há
          F
Pode acreditar
                 Em
Mais bonito não há
          F
Pode acreditar
                 Em
Mais bonito não há
          F
Pode acreditar
                 C/G
Mais bonito não há

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Am7/G = 3 0 2 0 1 0
Bb = X 1 3 3 3 1
C/G = 3 3 2 X 1 X
Em = 0 2 2 0 0 0
Em7(9) = X 7 5 7 7 X
Em7(9)/B = X 2 4 0 3 0
F = 1 3 3 2 1 1
F7M = 1 X 2 2 1 X
G = 3 2 0 0 0 3
