Milton Nascimento - Dona Olímpia

Intro: G6  F#7 C/E  Am7(9) B7

  Em7(9)                         Am7(9) Bm7/A Am7(9)
Vai e não esquece de chorar
Em7(9)                             Am7(9) Bm7/A Am7(9)
Vê se não esquece de mentir
                            G#7(5-)
Dizer até manhã
                 G6
E não regressar mais
                     F#7                C/E   Am7(9) B7
Vê se não esquece de sumir

 Em7(9)                       Am7(9) Bm7/A Am7(9)
É ficou assim, caiu no ar
Em7(9)                     Am7(9) Bm7/A Am7(9)
É passou assim, não quer passar
                  G#7(5-)
Não pára de doer
              G6
E não vai parar mais

               F#7          C/E
Nem de vez em quando vai sarar
       Am7(9)         B7
Me xinga me deixa me cega

     Em7(9)                   Am7(9) Bm7/A Am7(9)
Mas vê se não esquece de voltar

( Em7(9)   Am7(9) )

                      G#7(5-)
Tentar compreender
              G6
Quase não falar mais
             F#7            C/E
E nem ser preciso perdoar
        Am7(9)       B7
Me xinga me deixa me cega
    Em7(9)
Mas vê

----------------- Acordes -----------------
Am7(9) = X X 7 5 8 7
B7 = X 2 1 2 0 2
Bm7/A = 5 2 4 2 3 2
C/E = 0 3 2 0 1 0
Em7(9) = X 7 5 7 7 X
F#7 = 2 4 2 3 2 2
G#7(5-) = 4 X 4 5 3 X
G6 = 3 X 2 4 3 X
