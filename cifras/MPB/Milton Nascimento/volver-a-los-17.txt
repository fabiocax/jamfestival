Milton Nascimento - Volver a Los 17

Intro: Am Am7 F7+ E7

Am                        C
Volver a los diecisiete después de vivir un siglo
   Am                   C
Es como decifrar signos sin ser sabio competente
  Dm                          G
Volver a ser de repente tan frágil como un segundo
 Dm                       G
Volver a sentir profundo como un niño frente a Dios
 Dm                  G      E7              Am   40-53-52-50
Eso es lo que siento yo en este instante fecundo
         G           C
Se va enredando, enredando
            G         C
Como en el muro, la hiedra
        G         C
Y va brotando, brotando
           G             C
Como el musguito en la piedra
          G              G#º            Am
Como el musguito en la piedra, ay si si si...

                           C
Mi paso es retrocedido cuando el de ustedes avanza
   Am                         C
El arco de las alianzas ha penetrado en mi nido
    Dm                       Em
Con todo su colorido se ha paseado por mis venas
   Dm                      Em
Y hasta la dura cadena con que nos ata el destino
    F                G          G#º           Am   40-53-52-50
Es como un diamante fino que alumbra mi alma serena
..
 Am                            C
Lo que puede el sentimiento no lo ha podido el saber
 Am                      C
Ni el más claro proceder ni el más ancho pensamiento
 Dm                             Em
Todo lo cambia el momento cual mago condescendiente
     Dm                   Em
Nos aleja dulcemente de rancores y violencias
 F                     G          G#º         Am     40-53-52-50
Sólo el amor con su ciencia nos vuelve tan inocentes
.. Am7 F7+ E7
 Am           Am7+        Am7     Am6
El amor es torbellino de pureza original
  F7+            Am6     Am7     Am7+   Am
Hasta el feroz animal susurra su dulce trino
  Dm                       G
Detiene a los peregrinos libera a los prisioneros
  Dm                        G
El amor con sus esmeros al viejo lo vuelve niño
      Dm            G        E7             Am   40-53-52-50
Y al malo sólo el cariño lo vuelve puro y sincero
..
 Am                           C
De par en par la ventana se abrió como por encanto
 Am                         C
Entró el amor con su manto como una tibia mañana
    Dm                    Em
Al son de su bella diana hizo brotar el jazmín
 Dm                      Em
Volando cual serafín al cielo le puso aretes.
  F               G              G#º        Am    40-53-52-50
Y mis años en diecisiete los convertió el querubín

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am6 = 5 X 4 5 5 X
Am7 = X 0 2 0 1 0
Am7+ = X 0 2 1 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F7+ = 1 X 2 2 1 X
G = 3 2 0 0 0 3
G#º = 4 X 3 4 3 X
