Milton Nascimento - Um Sonho Na Correnteza

Intro: B  C7M

Bm7            E
A primavera que espero
Bm7               E
Por ti irmã não se alcança
Bm7
É paraíso, passageiro
E
Um quintal, um chão mineiro
C7M   B      E
Folha na correnteza
C7M   B          E
Castelo de fina areia
Bm7                  E
Onde a esperança me deixa, deixa
Bm7               E
Eu vejo com meu desleixo
Bm7                      E
Dou  de graça e  bem doado


O meu riso, minha veia
C7M  B      E
Tudo vira areia
C7M  B           E
Num sonho na correnteza
Fm7    G7    E
Jogarei no ar
Fm7    G7      E
Por qualquer sinal
Fm7    G7     C#m7(b5)
Não me espere não
        B7
Por que já fui,
C7M         B7
Não sei pra que...
Bm7                  E
Mando um abraço pra ti
Bm7                  E
Pequenina tão semente
Bm7                  E
E minha lei e minha norma

Minha cara e meu destino
C7M  B      E
Tudo vira areia

C7M  B           E
Um sonho na correnteza
Fm7    G7    E
Jogarei no ar
Fm7    G7      E
Por qualquer sinal
Fm7    G7     C#m7(b5)
Não me espere não
       B7
Por que já fui,
C7M         B7
Não sei pra que...

----------------- Acordes -----------------
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
Bm7 = X 2 4 2 3 2
C#m7(b5) = X 4 5 4 5 X
C7M = X 3 2 0 0 X
E = 0 2 2 1 0 0
Fm7 = 1 X 1 1 1 X
G7 = 3 5 3 4 3 3
