Milton Nascimento - Fazenda

Intro: A7+

Bm7       Gm7  Bm7            Gm7  Bm7
Água de beber,   bica no quintal
           Gm7 F#m7 Bm7
Sede de viver  tudo
          Gm7    Bm7     Gm7  Bm7
E o esquecer era tão normal
          Gm7  F#m7
Que o tempo   parava
A7+    D/A A7+            D/A
E a meninada respirava o vento
A7+      D/A
Até vir a noite
    C#m7    F#7/4   C#m7            F#7/4
E os velhos falavam      coisas dessa vida
Bm7      D7+  Bm7        C#m7  Bm7
Eu era criança,  hoje é você
         Gm7 F#m7
E no amanhã  nós
Bm7      D7+  Bm7        C#m7  Bm7
Eu era criança,  hoje é você

         Gm7 F#m7
E no amanhã  nós
Bm7       Gm7  Bm7            Gm7  Bm7
Água de beber,   bica no quintal
           Gm7 F#m7  Bm7
Sede de viver  tudo
          Gm7   Bm7      Gm7  Bm7
E o esquecer era tão normal
          Gm7  F#m7
Que o tempo   parava
A7+      D/A  A7+            D/A
Tinha sabiá,      tinha laranjeira
A7+         D/A           C#m7      F#7/4  C#m7
Tinha manga rosa, tinha o sol da manhã
         F#7/4    Bm7      D7+
E na despedida, tios na varanda
Bm7       C#m7  Bm7        Gm7   F#m7
Jipe na estrada     e o coração lá
Bm7      Em7/9  A7
Tios na varanda
D7+  G#m7/5-  C#7/9- F#7/5+  Bm7
Jipe   na        estrada

        Gm7   F#m7
e o coração lá
Bm7      D7+
Tios na varanda
Bm7       C#m7  Bm7        Gm7   F#m7
Jipe na estrada     e o coração lá

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
Bm7 = X 2 4 2 3 2
C#7/9- = X 4 3 4 3 X
C#m7 = X 4 6 4 5 4
D/A = X 0 X 2 3 2
D7+ = X X 0 2 2 2
Em7/9 = X 7 5 7 7 X
F#7/4 = 2 4 2 4 2 X
F#7/5+ = 2 X 2 3 3 2
F#m7 = 2 X 2 2 2 X
G#m7 = 4 X 4 4 4 X
G#m7/5- = 4 X 4 4 3 X
Gm7 = 3 X 3 3 3 X
