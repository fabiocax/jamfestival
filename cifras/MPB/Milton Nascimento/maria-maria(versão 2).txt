Milton Nascimento - Maria, Maria

G
Maria, Maria
    D                C
É o dom,uma certa magia
                 G    F#º
É uma força que nos alerta
Em               C             F
Uma mulher que merece viver e amar
             C           G
Como outra qualquer no planeta

   G
Maria, Maria
    D                 C
É o som,é a cor,é o suor
               G       F#º
É a dose mais forte e lenta
Em               C                 F
De uma gente que ri quando deve chorar
             C      G
E não vive apenas aguenta


G                   D                  C
Mas é preciso ter força,é preciso ter raça
              G    F#º
É preciso ter gana,sempre
Em                     C             F
Quem traz no corpo essa marca Maria, Maria
           C         G
Mistura a dor e a alegria

G                   D
Mas é preciso ter manha
              C                   G     F#º
É preciso ter graça,é preciso ter sonho,sempre
Em                     C                F
Quem traz na pele essa marca possui a estranha
             C      G
Mania de ter fé na vida

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F#º = 2 X 1 2 1 X
G = 3 2 0 0 0 3
