Milton Nascimento - Mar do Nosso Amor

Dm7
Haja coração
E haja tal coragem
Pra querer te alcançar
Haja resistência
Luminosidade
Pra saber te agitar

E4(7/9)
Quando fantasia e realidade
Gm7
Dançam na mesma estação

Dm7
Haja novidade
Haja madrugada
Pra poder te entender

Em7(9)
Mutante
Gm7
Que nomes você tem?


F#7(b13)
Poço de carinho
C7(9)           B7(#9)
E estouro de boiada
Em7
Corre no meu peito
Gm7                 Bm7
Vem cor de pecado e lua

Gm7            F#7(b13)
É, pássaro que chega
C7(9)        B7(#9)
Quer luz e calor
Em7                 Dm7
Como o mar do nosso amor

( Dm7  Em7(b5)  Bb7M )

Em7
Namorar no parque
Viajar no quarto
E muito que se viver
Gm7
Busca da loucura
Doce criatura
É tanto pra se querer

Em7(9)
Viagem
Gm7
Que ninguém descreveu
F#7(b13)
Voa contra o tempo
C7(9)                 B7(#9)
Vive a noite e o dia
Em7
Muda a geografia
Gm7
Faz qualquer rincão tão perto

Gm7             F#7(b13)
E o pássaro que volta
C7(9)        B7(#9)
Traz luz e calor
Em7                  Dm7
Como o mar do nosso amor

( Dm7  Em7(b5)  Bb7M )

Em7
Namorar no parque
Viajar no quarto
E muito que se viver
Gm7
Busca da loucura
Doce criatura
É tanto pra se querer

Em7
Viagem
Gm7
Que ninguém descreveu
F#7(b13)
Voa contra o tempo
C7(9)                 B7(#9)
Vive a noite e o dia
Em7
Muda a geografia
Gm7
Faz qualquer rincão tão perto

Gm7                    F#7(b13)
E o pássaro que volta
C7(9)        B7(#9)
Traz luz e calor
Em7                     Dm7
Como o mar do nosso amor

----------------- Acordes -----------------
B7(#9) = X 2 1 2 3 X
Bb7M = X 1 3 2 3 1
Bm7 = X 2 4 2 3 2
C7(9) = X 3 2 3 3 X
Dm7 = X 5 7 5 6 5
E4(7/9) = X X 2 2 3 2
Em7 = 0 2 2 0 3 0
Em7(9) = X 7 5 7 7 X
Em7(b5) = X X 2 3 3 3
F#7(b13) = 2 X 2 3 3 2
Gm7 = 3 X 3 3 3 X
