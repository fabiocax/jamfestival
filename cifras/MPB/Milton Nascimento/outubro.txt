Milton Nascimento - Outubro

Intro: Dm4/7

Dm4/7
Tanta gente no meu rumo
Mas eu sempre vou só
Nessa terra desse jeito
         F#m7   B7 Gm7
Já não sei viver
                  Bb/C
Deixo tudo deixo nada
            F#m7                B5-/7 Em7
Só do tempo eu não posso me livrar
                  A5+/7   Bm7
E ele corre para ter meu dia de morrer
G/A                 A7
Mas se eu tiro do lamento um novo canto
Bb/C              C7  D7
Outra vida vai nascer
                     C/D
Vou achar um novo amor
                     G7+ D/F#
Vou morrer só quando for

 Em7            F/G       C7+
E jogar no meu braço no mundo
             F6/7
Fazer meu outubro de homem
           Am7       D7
Matar com amor essa dor
 G7+ D/F# Em7
Vou
             F/G        C7+
Fazer desse chão minha vida
                 F6/7
Meu peito é que era deserto
           D7/9+  E7+ Dm4/7
O mundo já era assim
Tanta gente no meu rumo
Já não sei viver só
Foi um dia e é sem jeito
              F#m7 B5-/7 Gm7
Que eu vou contar
                 C7          F#m7
Certa moça me falando alegria
                  B5-/7 Em7
De repente ressurgiu
                       A5+/7
Minha história está contada
            Bm7
Vou me despedir

----------------- Acordes -----------------
A5+/7 = X 0 X 0 2 1
A7 = X 0 2 0 2 0
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
Bb/C = X 3 X 3 3 1
Bm7 = X 2 4 2 3 2
C/D = X X 0 0 1 0
C7 = X 3 2 3 1 X
C7+ = X 3 2 0 0 X
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
D7/9+ = X 5 4 5 6 X
Dm4/7 = X 5 5 5 6 5
E7+ = X X 2 4 4 4
Em7 = 0 2 2 0 3 0
F#m7 = 2 X 2 2 2 X
F/G = 3 X 3 2 1 X
F6/7 = 1 X 1 2 3 X
G/A = 5 X 5 4 3 X
G7+ = 3 X 4 4 3 X
Gm7 = 3 X 3 3 3 X
