Milton Nascimento - Mistérios

Cm9/A#       F7(9)/A       A#sus13/G#   A#13/G#
Um fogo queimo...u den...tro de mim
                                D#7M/A   A4(7) D7/A   A#sus13/G#
Que não tem mais jeito... de se apagar
              A#13/G#  Gm7  C7(9)/G   Fm7  A#7(9)/F
Nem mesmo com toda a....á...gua do mar
             Amb6  Amb6(11)        G#6  G#6/#11          Gsus4(9)  G6/b9
Preciso aprender os mistérios do fo...go   pra te incendiar
Cm9/A#    F7(9)/A       A#sus13/G#   A#13/G#
Um rio passo..u de...ntro de mim
                                  D#7M/A   A4(7) D7/A   A#sus13/G#
Que eu não tive jeito de a...travessar
             A#13/G#    Gm7     C7(9)/G   Fm7  A#7(9)/F
Preciso um navio... pra... me levar
             Amb6   Amb6(11)       G#6   G#6/#11       G4/7(9)   G6/b9
Preciso aprender os mistérios do ri...o     pra te navegar
Csus4(9) C7(9)        Csus4(9)      C7/b9   A#sus4(9)
   Vi.....da.....bre...ve,      nature....za
          G#6#11     Gsus4(9)   G6b9
Quem  man...dou, coração?


Cm7(9)     F7(9)         A#sus13     A#7#11
Um vento bate...u de..ntro... de mim
                            A#6        A4(7) D7  A#sus13
Que eu não tive jeito... de... segurar
         A#7(13)               Gm7   C7(9)   Fm7   A#7(9)
A vida passou pra.. me... carregar
             Amb6   Amb6(11)         G#6  G#6#11       Gsus4(9)   Fm7  Fm/D#
Preciso aprender os mistérios da  vi...da   pra te ensinar
             Amb6   Amb6(11)         G#6  G#6#11       Gsus4(9)   Fm7  Fm/D#
Preciso aprender os mistérios da  vi...da   pra te ensinar

----------------- Acordes -----------------
A#6 = X 1 3 0 3 X
A#7(13) = 6 X 6 7 8 X
A#7(9) = X 1 0 1 1 X
A4(7) = X 0 2 0 3 0
C7(9) = X 3 2 3 3 X
C7/b9 = X 3 2 3 2 X
Cm7(9) = X 3 1 3 3 X
Cm9/A# = X 1 1 0 3 X
D7 = X X 0 2 1 2
D7/A = 5 X 4 5 3 X
F7(9) = X X 3 2 4 3
Fm/D# = X 6 X 5 6 4
Fm7 = 1 X 1 1 1 X
G#6 = 4 X 3 5 4 X
G4/7(9) = 3 X 3 2 1 X
Gm7 = 3 X 3 3 3 X
