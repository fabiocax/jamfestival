Milton Nascimento - Beco do Mota


Dm7/C                 Em7/D
Clareira na noite, na noite
 C
Procissão deserta, deserta
                        G/B   Dm7/A      Bb/C
Nas portas da arquidiocese desse meu país
Dm7/C                Em7/D
Procissão deserta, deserta
 C
Homens e mulheres na noite
                        G/B  Dm7/A       Bb/C
Homens e mulheres na noite desse meu país
F7+
Nessa praça não me esqueço
             Em7/9
E onde era o novo fez-se o velho
Colonial vazio
F7+
Nessas tardes não me esqueço
             Em7/9
E onde era o vivo fez-se o morto

Aviso pedra fria
C         C/B
Acabaram com o beco
        C/Bb          F7+
Mas ninguém lá vai morar
              Dm7
Cheio de lembranças vem o povo
  Em7/9
Do fundo escuro do beco
Dm7                  A7/4
Nessa clara praça se dissolver
F7+
Pedra, padre, ponte, muro
            Em7/9
E um som cortando a noite escura
Colonial vazia
F7+
Pelas sombras da cidade
          Em7/9
Hino de estranha romaria
Lamento água viva
C
Acabaram com o beco...
Dm7/C                Em7/D
Procissão deserta, deserta
C
Homens e mulheres na noite
                      G/B   Dm7/A       Bb/C
Homens e mulheres na noite desse meu país
Dm7/C             Em7/D
Na porta do beco estamos
 C
Procissão deserta, deserta
                        G/B Dm7/A       Bb/C
Nas portas da arquidiocese desse meu país
Dm7/C                  Em7/D
Diamantina é o Beco do Mota
C
Minas é o Beco do Mota
                    G/B
Brasil é o Beco do Mota
Dm7/A      Bb/C
Viva o meu país!

----------------- Acordes -----------------
A7/4 = X 0 2 0 3 0
Bb/C = X 3 X 3 3 1
C = X 3 2 0 1 0
C/B = X 2 2 0 1 0
C/Bb = X 1 2 0 1 X
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Dm7/A = X 0 0 2 1 1
Dm7/C = X 3 0 2 1 1
Em7/9 = X 7 5 7 7 X
Em7/D = X X 0 0 3 0
F7+ = 1 X 2 2 1 X
G/B = X 2 0 0 3 3
