Milton Nascimento - Casamiento de Negros

Intro 4x: E A

          A            E
Se ha formado un casamiento
      G              D
Todo cubierto de negros.
        B7
Negros novios y padrinos,
E7/B      B7            E7/B
negros cuñados y suegros.
        D         G  D
Y el cura que los casó
    A             E  A
era de los mismos negros.

E A - 4x
          A          E
Cuando empezaron la fiesta
      G              D
pusieron un mantel negro.
        B7
Luego llegaron al postre,

E7/B      B7            E7/B
se servieron higos secos,
     D            G  D
y se fueron a acostar
       A           E  A
debajo de un cielo negro.

( E A )(4x)

        A                E
Y ya están las dos cabezas
      G                  D
de la negra con el negro.
    B7                  E7/B  B7
Amanecieron con frío,
                    E7/B
tuvieron que prender fuego.
        D         G  D
Carbón trajo la negrita,
       A              E  A
carbón que también es negro.

E A - 4x
      D                A
Algo le duele a la negra.
      C              G
Vino el médico del pueblo,
        E7              A7
recetó emplasto de barro
      E7                A7
pero del barro más negro,
       G           D  G
que le dieron a la negra
     D            A  D
zumo de maqui del cerro.

A D - 4x
     A          E
Y ya murió la negrita,
      G                  D
¡que pena para el pobre negro!
     B7                E7/B
Y la fueron a enterrar
      B7               E7/B
en cajón pintado de negro.
         D           G  D
No prendieron ni una vela,
         A             E  A
¡Ay! que velorio más negro.

( E A )(4x)

     A           E
Y ya partió la negrita
     G                D
levitando para el cielo.
    B7                 E7/B
Era un día muy nublado,
     B7            E7/B
todo se veía negro.
          D            G  D
Le abrió la puerta San Pedro
        A              E  A
que era de los mismos negros.

( E A )(4x)
( A E G D B7 E7/B B7 E7/B D G D A E A )
( E A )(4x)

      D                A
Y ya partió la negrita
     C              G
levitando para el cielo.
        E7             A7
Era un día muy nublado,
     E7        A7
todo se veía negro.
         G             D  G
Le abrió la puerta San Pedro
        D             A     D
que era de los mismos negros.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
E7/B = X 2 2 1 3 X
G = 3 2 0 0 0 3
