Milton Nascimento - Canções e Momentos

G9                                    C/G
Há canções e há momentos,
              G5+/9                   Em7/9
que eu não sei como explicar
      Am7/E                     Bm7/11
Em que a voz é um instrumentos
C9                       D7/9           G9  C/G
Que eu não posso controlar
 G9                   Cm7M/G
Ela vai ao infinito
G5+/9                   Em7/9
Ela amarra todos nós
C9       C7M    Bm7/11  Am7/11     D7    G9     C/G
E é um só sentimento, na platéia e na voz
G9                                    C/G
Há canções e há momentos,
    G5+/9             Bm7/11
Em que a voz vem da raiz
C9                                        G9
Eu não sei se quando triste
               Am7         D7     G9    C/G
Ou se quando estou feliz

                                                Am7
Eu só sei que há momentos
Bm7/11                        C9
Que se casa com canção
G9                       Cm7M/G
De fazer tal casamento
                    D7/9           G9    C/G   G9
Vive a minha profissão.


Vocalizes:
G  Cm7M/G  G5+/9  Em7/9 C9  Bm7/11  Am7/9  D/9  G9

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Am7/11 = 5 X 5 5 3 X
Am7/9 = X X 7 5 8 7
Am7/E = X X 2 2 1 3
Bm7/11 = 7 X 7 7 5 X
C/G = 3 3 2 X 1 X
C7M = X 3 2 0 0 X
C9 = X 3 5 5 3 3
Cm7M/G = 3 3 5 4 4 3
D/9 = X X 0 2 3 0
D7 = X X 0 2 1 2
D7/9 = X 5 4 5 5 X
Em7/9 = X 7 5 7 7 X
G = 3 2 0 0 0 3
G5+/9 = 3 X 1 2 0 X
G9 = 3 X 0 2 0 X
