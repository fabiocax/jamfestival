Milton Nascimento - Roupa Nova

Intro: (E E7/9)

(E E7/9 E)
Todos os dias, toda manhã
Sorriso aberto e roupa nova
(A A7 A)
Passarinho preto de terno branco
(E E7/9 E)
Pinduca vai esperar o trem
Todos os dias, toda manhã
Ele sozinho na plataforma
(A A7 A)
Ouve o apito, sente a fumaça
(E E7/9 E)
E vê chegar o amigo trem
 B4/7           B/C#
Que acontece que nunca parou
A7+              B7+
Nesta cidade de fim de mundo
A7+              Ab7+
E quem viaja pra capital

G7+                   B7
Não tem olhar para o braço que acenou
(E E7/9 E)
O gesto humano fica no ar
O abandono fica maior
(A A7 A)                  (E E7/9 E)
E lá na curva desaparece a sua fé
 B4/7
Homem que é homem não perde a esperança, não
B/C#
Ele vai parar
B4/7
Quem é teimoso não sonha outro sonho, não
F#m7           Bm7
Qualquer dia ele para
(E E7/9 E)
Assim Pinduca toda manhã
Sorriso aberto e roupa nova
(A A7 A)
Passarinho preto de terno branco
(E E7/9 E)      B4/7
Vem a renovar a sua fé

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
A7+ = X 0 2 1 2 0
Ab7+ = 4 X 5 5 4 X
Ab7+ = 4 X 5 5 4 X
B/C# = X 6 X 4 7 7
B4/7 = X 2 4 2 5 2
B4/7 = X 2 4 2 5 2
B7 = X 2 1 2 0 2
B7+ = X 2 4 3 4 2
B7+ = X 2 4 3 4 2
Bm7 = X 2 4 2 3 2
E = 0 2 2 1 0 0
E7/9 = X X 2 1 3 2
F#m7 = 2 X 2 2 2 X
G7+ = 3 X 4 4 3 X
G7+ = 3 X 4 4 3 X
