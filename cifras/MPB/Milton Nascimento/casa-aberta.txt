Milton Nascimento - Casa Aberta

Badd9
Lua luou, vento ventou
Aadd9
Rio correu pro mar
G
Foi beijar
Aadd9         Badd9
As areias de lá

Badd9
Mato queimou, fogo apagou
Aadd9
O céu escureceu
G
Vem de lá
Aadd9        Badd9
Lambuzada no breu

Badd9
Na casa aberta
Aadd9
É noite de festa

G        F#            Badd9
Dançam Geralda, Helena, Flor
G
Na beira do rio
A
Escuto Ramiro
G       F#          Badd9
Dona Mercês toca tambor

Badd9
Na casa aberta
Dº
É noite de festa
C#m    F#           Badd9
Dançam Geralda, Helena, Flor
C7+
Na beira do rio
C#m
Escuto Ramiro
G       F#/Bb       Badd9
Dona Mercês toca tambor

( Badd9 )

Badd9
Lua luou, Vento ventou
Aadd9
Rio correu pro mar
G
Foi beijar
Aadd9         Badd9
As areias de lá

Badd9
Mato queimou, fogo apagou
Aadd9
O céu escureceu
G
Vem de lá,
Aadd9        Badd9
Lambuzada no breu

Badd9
Na casa aberta
Dº
É noite de festa
C#m    F#           Badd9
Dançam Geralda, Helena, Flor
C7+
Na beira do rio
C#m
Escuto Ramiro
G       F#/Bb       Badd9
Dona Mercês toca tambor

( Badd9 Aadd9 G Aadd9 )

Badd9
Lua azul, lua azul turquesa
Já que a casa está vazia
Vem me fazer companhia
                   (Aadd9)
Na janela da cozinha

(Badd9)
Lua azul, lua azul turquesa
Já que a casa está vazia
Vem me fazer companhia
                    (Aadd9)
Na janela da cozinha

Badd9
Na casa aberta
Aadd9
É noite de festa
G        F#            Badd9
Dançam Geralda, Helena, Flor
G
Na beira do rio
A
Escuto Ramiro
G        F#         Badd9
Dona Mercês toca tambor

Vou descendo o rio a nado
Parauna de mergulho
Pra salvar aquela morena
Ô meu Deus
Beiço de cajú maduro

Vou descendo o rio a nado
Parauna de mergulho
Pra salvar aquela morena
Ô meu Deus
Beiço de cajú maduro

----------------- Acordes -----------------
A = X 0 2 2 2 0
Aadd9 = X 0 2 2 0 0
Badd9 = X 2 4 4 2 2
C#m = X 4 6 6 5 4
C7+ = X 3 2 0 0 X
Dº = X X 0 1 0 1
F# = 2 4 4 3 2 2
F#/Bb = 6 X 4 6 7 X
G = 3 2 0 0 0 3
