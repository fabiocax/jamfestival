Milton Nascimento - Tristesse

   ( Milton Nascimento - Telo Borges)

Tom : Gbm
Introd :  Gbm7, Gbm7M, Gbm7,Gbm6

Gbm7  Bm7/F#  Gbm7 Bm7/F#  Gbm7   E/G#
Como  você /  pode pedir / pra eu falar
   Gbm7  Bm7/F#
do nosso amor
Gbm7        E/G#      Bb6(b5) A7   Ab7      Db7
Que foi tão forte e   ainda    é / mas cada um
     Gbm7  Bm7/F#  Gbm7  Bm7/F#
se   foi
Gbm7    Bm7/F#   Gbm7     Bm7/F#  Gbm7      E/G#
Quanta saudade   brilha em mim  /  se cada  sonho
    Gbm7  Bm7/F#
é   seu
Gbm7    E/G#     Bb6(b5)   A7     Ab7
Virou história em sua    vida   / mas  pra mim
 Db7    Gbm7  Ab° Gb/Bb
não   morreu

 Bm7             Bm7M         Bm7          Em6/B
Lembra, lembra, lembra /cada instante que passou
Bm7      Em6/B        Bm7         Em6/B
De cada perigo , da audácia,  do temor
 Bm7      A/C#           Cm6        G/B     Db7
Que sobrevivemos , que cobrimos de emoção / volta a
  Gb7    Bm7  Em6/B , Bm7, Em6/B
pensar então
 Bm7            Bm7M            Bm7      Em6/B
Sinto , penso, espero , / fico tenso toda vez
Bm7          Em6/B        Bm7        Em6
Que nos encontramos, nos olhamos sem viver
Bm7      A/C#               Cm6          G/B
Pára de fingir que não sou parte do seu mundo
 Db7      Gb7   [Bm7 ,Em6/B(2 vezes)] Ab° , G7
Volta a pensar então
                           Gbm7 Bm7/F#…….....
(repetir tudo)(no final)...Como você pode pedir

Acordes :
A7       x02020
Ab7      46454x
Ab°      4x343x
A/C#     x4x255
Bm7/F#   2x020x
Bb6(b5)  x1x030
Bm7      x24232
Bm7M     x24332
Cm6      x3x243
Db7      x46464
E/G#     4x245x
Em6/B    x2x020
Gbm7     2x222x
Gbm7M    245222
Gbm6     2x122x
Gb/Bb    6x467x
G/B      x2x033
Gb7      242322
G7       353433

----------------- Acordes -----------------
A/C# = X 4 X 2 5 5
A7 = X 0 2 0 2 0
Ab7 = 4 6 4 5 4 4
Ab° = 4 X 3 4 3 X
Bb6(b5) = X X 8 9 8 6
Bm7 = X 2 4 2 3 2
Bm7/F# = X X 4 4 3 5
Bm7M = X 2 4 3 3 2
Cm6 = X 3 X 2 4 3
Db7 = X 4 3 4 2 X
E/G# = 4 X 2 4 5 X
Em6 = X 7 X 6 8 7
Em6/B = X 2 2 0 2 0
G/B = X 2 0 0 3 3
G7 = 3 5 3 4 3 3
Gb/Bb = 6 X 4 6 7 X
Gb7 = 2 4 2 3 2 2
Gbm6 = 2 X 1 2 2 X
Gbm7 = 2 X 2 2 2 X
Gbm7M = X X 4 6 6 5
