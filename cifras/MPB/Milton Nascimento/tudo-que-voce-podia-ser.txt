Milton Nascimento - Tudo Que Você Podia Ser

Afinação: D A D G B E

Intro:Dm Gm7/9

Dm       Am7/9/11 Dm Am7/9/11 G#m7/9/11 G7/9/11
Com sol e chuva você sonhava
   Gm7/9/11    Am7/9
Que ia ser melhor depois
       Gm7/9                Am7/9
Você queria ser o grande herói das estradas
Gm7/9/11  Am7/9/11   Em7/9/11   Dm
Tudo que você queria ser
Dm     Am7/9/11 Dm     Am7/9/11 G#m7/9/11
Sei um segredo você tem medo
  Gm7/9/11       Am7/9/11
Só pensa agora em voltar
         Gm7/9/11               Am7/9/11
Não fala mais na bota e do anel de Zapata
Gm7/9/11  Am7/9/11  Em7/9/11 Gm7/9/11
Tudo que você devia ser   sem medo
(Dm7/9/11 Gm7/9/11 Em7/9/11 Am7/9/11)

Gm7/9/11           Am7/9/11
E não se lembra mais de mim
         Gm7/9/11                 Am7/9/11
Você não quis deixar que eu falasse de tudo
Gm7/9/11  Am7/9/11 Em7/9/11 Gm7/9/11
Tudo que você podia ser    na estrada
(Dm7/9/11 Gm7/9/11 Em7/9/11 Am7/9/11)
Dm7     Am7/9/11       Dm7   Am7/9/11
Ah! Sol e chuva na sua estrada
   Gm7/9/11        Am7/9/11
Mas não importa não faz mal
     Gm7/9/11              Am7/9/11
Você ainda pensa e é melhor do que nada
Gm7/9    Am7/9         Em7/9  Gm7/9
Tudo que você consegue ser    ou nada
(Dm7/9 Gm7/9 Em7/9 Am7/9)
Gm7/9       Am7/9
Não importa não faz mal...

----------------- Acordes -----------------
Am7/9 = X X 7 5 8 7
Am7/9/11 = 5 3 5 4 3 X
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Dm7/9 = X 5 3 5 5 X
Dm7/9/11 = X 5 3 5 5 3
Em7/9 = X 7 5 7 7 X
Em7/9/11 = X 7 5 7 7 5
G#m7/9/11 = 4 2 4 3 2 X
G7/9/11 = 3 X 3 2 1 X
Gm7/9 = X X 5 3 6 5
Gm7/9/11 = 3 1 3 2 1 X
