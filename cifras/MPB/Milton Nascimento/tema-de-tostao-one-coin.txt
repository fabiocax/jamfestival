Milton Nascimento - Tema De Tostão (One Coin)

Intro: F F# G C
     ( C9/D Bb/D ) (3x)
     ( Dm7 C9/D Dm7 Em7 Bb/D Dm7 C9/D )

Bb/D C9/D Em7    Dm7  C9/D
Estou falando da vida moreno
Em7              Dm7  C9/D
Falando da dor e do medo
Em7     Bb/D Dm7   C9/D
Da ferida aberta veneno
Em7
Que nos mata mais cedo

(Intro)

Bb/D C9/D Em7    Dm7  C9/D
Estou falando da vida alegria
Em7              Dm7  C9/D
Da casa, o filho, e a filha
Bb/D   C9/D  Dm7 Em7
A faca, o pão e o sorriso

Bb/D   C9/D  Dm7 Em7
E a luz em toda mulher
Bb/D   C9/D  Dm7 Em7
E a luz em toda mulher
Bb/D   C9/D  Dm7 Em7
Tudo é razão de viver
Bb/D   C9/D  Dm7 Em7
A casa, o filho e o pão
Bb/D   C9/D  Dm7 Em7
A filha, a faca e a luz

( Bb/D   C9/D  Dm7 Em7 )

----------------- Acordes -----------------
Bb/D = X 5 X 3 6 6
C = X 3 2 0 1 0
C9/D = X 5 5 5 3 3
Dm7 = X 5 7 5 6 5
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
