Erasmo Carlos - Quando Será o Dia?

A
Eu namoro com um broto
 Bm        E
Sensacional
A
Que se o amor começa
             G#º     Bm9/C#
A incrementar fala não

F#m                 G#º     Bm9/C#
Eu então apelo e ela fala não
A                       G#º   Bm9/C# F#º F#m
Eu me desespero e ela fala não
Bm                         E
Aí então eu fico encabulado

Isso me agita o coração

A     C#m
Baby, baby
D               E
Quando será o dia
A           C#m
Baby, baby, baby, baby
D                  E
Em que o não termina?
A          C#m
Baby, baby, baby, baby
D           Bm9/C# F#º
Sha-la-la, la la

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
F#º = 2 X 1 2 1 X
G#º = 4 X 3 4 3 X
