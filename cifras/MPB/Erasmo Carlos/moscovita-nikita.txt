Erasmo Carlos - Moscovita (Nikita)

INTRODUÇÃO:G G/B Am7 Bm7 C7+

G    G/B         C                          G
MOSCOVITA, TUDO BEM, SEU PEQUENO MUNDO CONGELOU.
                  D7                             G
VOCÊ NÃO VAI ENCONTRAR, LUGAR MAIS QUENTE QUE ESSE ONDE ESTOU...
         G/B        C                               G
ATRÁS DO FRIO TEM O SOL, OLHE AO SEU REDOR SE A SOLIDÃO.
                        D7                          G
TE FAZ SOFRER, TE FAZ CHORAR, EU ESTOU PERTO DO SEU CORAÇÃO...

                C                            G
MOSCOVITA NUNCA DIGA NÃO, DIGA SIM SE OUVIR O SOM.
                           D7                  G
DE MINHA VOZ DIZENDO QUE TE AMA, MINHA AMIGA SONHAR É BOM...
     G/B        C                             G
OUTRO DIA NA TELEVISÃO, DAQUI DE LONGE EU VI VOCÊ.
                        D7   C/E  D/F#            G
POVO E SOLDADOS DANDO AS MÃOS,QUEM SÃO, QUERIDA,SÃO SEUS 
        G/B C C#º D7
IRMÃOS...

G    G/B             C                                G
VOCÊ PENSA SEMPRE EM MIM, QUANDO LÊ AS CARTAS QUE ESCREVI.
                   D7                         C
E NAS NOITES TÃO IGUAIS, MENINA VOCÊ TEM ALGUÉM AÍ...
                 G                          G
SE A SAUDADE APERTAR, ARMAS JÁ NÃO PODEM IMPEDIR.
                 D7                                G
A LIBERDADE SOMOS NÓS, VIVEMOS JUNTOS O QUE ESTÁ POR VIR.
(A D A DA D D#º E7)
SOLO....
A              D                             A
MOSCOVITA NUNCA DIGA NÃO, DIGA SIM SE OUVIR O SOM.
                           E7                 A
DE MINHA VOZ DIZENDO QUE TE AMA, MINHA AMIGA SONHAR É BOM...
               D                              A
OUTRO DIA NA TELEVISÃO, DAQUI DE LONGE EU VI VOCÊ.
                       E7   D/F# E/G#              A
POVO E SOLDADO DANDO AS MÃOS, QUEM SÃO, QUERIDA, SÃO SEUS 

IRMÃOS...

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am7 = X 0 2 0 1 0
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
C#º = X 4 5 3 5 3
C/E = 0 3 2 0 1 0
C7+ = X 3 2 0 0 X
D = X X 0 2 3 2
D#º = X X 1 2 1 2
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
E/G# = 4 X 2 4 5 X
E7 = 0 2 2 1 3 0
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
