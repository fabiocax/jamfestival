Erasmo Carlos - Convite Para Nascer de Novo?

[Intro] Em  Em 
        Em  Em  
        Em  Em/D 
        C  B7 
        Em  Em/D
        C  B7 

Em      Em/D
Houve um tempo em que eu chorava quase todo dia
C
 Dando linha a uma vida extremamente chata
Am                                   B7
 Com a vontade disponível de não existir
Em      Em/D
Houve um tempo em que eu morava com minha tristeza
C
 Era amigo e confidente das manhãs sem sol
Am                                   B7
 Prisioneiro de mim mesmo, sem poder fugir
Am
 De repente o infinito de uma coisa boa
B7                               C
 Começou devagarinho a orbitar em mim   
                    D#°
Como num conto de fadas dos irmãos Grimm
Em      Em/D
 Era um universo puro de uma pessoa
C       G/B
Que me viu um mundo morto portador de vida
Am                     B7
Como um beija-flor perdido no próprio jardim

Entra a banda e bateria

( Em  Em/D  C  B7 )
( Em  Em/D  C  B7 )
( Em  Em/D  C  B7 )

Em           Em/D
Era um momento claro de fazer saudade
C
 Um encontro do destino com a felicidade
Am                                     B7
Formidáveis primaveras de estações sem dor
Em           Em/D
Parecia uma chance pra nascer de novo
C
 Uma plenitude mansa que acendeu a chama
Am                                 B7
 De incontáveis alegrias vindas do amor

Am                                         B7
Foi assim que eu mergulhei no mar daquele afeto         
                                           C
Esquecendo a fé sem rosto do meu peito inquieto         
                       D#°
Vendo os seios sobre a mesa que jorravam mel
Em                Em/D
E ouvindo interjeições de sentimentos puros
C
Investi na sensação de emoções sem juros
Am              B7
E ganhei um universo pra chamar de céu

( Em  Em/D  C  B7 )
( Em  Em/D  C  B7 )
( Em  Em/D  C  B7 )

Em           Em/D
Parecia uma chance pra nascer de novo
C
  Uma plenitude mansa que acendeu a chama
Am                                 B7
 De incontáveis alegrias vindas do amor         

Em staccato
Am                                          B7
 Foi assim que eu mergulhei no mar daquele afeto        
        C
Esquecendo a fé sem rosto do meu peito inquieto         
                         D#°
Vendo os seios sobre a mesa que jorravam mel
Em                  Em/D
 E ouvindo interjeições de sentimentos puros
C
 Investi na sensações de emoções sem juros
Am                B7
 E ganhei um universo pra chamar de céu

( Em  Em/D  C  B7 )       
( Em  Em/D  C  B7 )       
( Em  Em/D  C  B7 )                     

Em      Em/D    C       B7
 Ô Oh            Ô Oh    Ô Oh   
Em      Em/D    C       B7
 Ô Oh            Ô Oh    Ô Oh   
Em             Em/D            C        B7
 Ganhei um universo pra chamar de céu           
Em             Em/D            C        B7
 Ganhei um univ erso pra chamar de céu          
Em             Em/D            C        B7
          Pra chamar de c       éu      
Em             Em/D            C        B7
          Pra chamar de c       éu        
Em             Em/D            C
B7            Em
Que jorrava mel

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
D#° = X X 1 2 1 2
Em = 0 2 2 0 0 0
Em/D = X X 0 4 5 3
G/B = X 2 0 0 3 3
