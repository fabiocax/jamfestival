Erasmo Carlos - O pica-pau


G
com meu bem fui ao cinema
assistir ao festival
                      D7
quando apareceu o pica-pau
    G                 G7
na hora de beijar meu bem
   C               Cm
na tela apareceu alguém
G      E7  Am   D7     G   D7
ou ou ou ou ou o pica-pau

G
depois disso meu amor
não me deu mais atenção
                         D7
só dizia que bichinho legal!
      G               G7
bem zangado então fiquei
     C                Cm
por causa deste meu rival
G      E7  Am   D7     G   D7
ou ou ou ou ou o pica-pau

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
