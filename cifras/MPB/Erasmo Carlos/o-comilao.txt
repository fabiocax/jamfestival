Erasmo Carlos - O Comilão

Intro:

E  Eb  D  C#
E  Eb  D  C#
A  B   B

       E
O comilão
          A7                       E
ganhou de prêmio num programa de televisão
      D7                       C#7
uma passagem pra comer pelo brasil
        F#m7    B7
tudo de graça

             E
chorou de apetite
   A7               E7
fingindo que era emoção
      D7                 C#7             F#m7    B7
seus amigos sempre lhe falaram em pratos típicos

      E            A7              E
desmaiou , e foi levado para o camarim
D7                        C#7
uns juravam que era apendicite
                      F#m7    B7
outros diziam que era rim



                 E
 ninguém desconfiava
           A7                  E
 que a sua boca estava cheia d'água
                 E
 ninguém desconfiava
           A7                  E
 que a sua boca estava cheia d'água



      E
o comilão
        A7             E7
foi acordado pela produção
D7                       C#7
com a cabeça cheia de tempero
                   F#m7     B7
e com a barriga na mão
             E
comeu esfomeado
A7                   E7
desfalcando a exportação


Refrão

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
C# = X 4 6 6 6 4
C#7 = X 4 3 4 2 X
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
Eb = X 6 5 3 4 3
F#m7 = 2 X 2 2 2 X
