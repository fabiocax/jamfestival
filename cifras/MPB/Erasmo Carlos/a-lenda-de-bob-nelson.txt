Erasmo Carlos - A Lenda de Bob Nelson

[Intro] D9  A  E9  A

E9
Sem seu coldre bordado a ouro
D9
Roupa de brim em vez de couro
E9
Sem uma estrela pra por no peito
D9                                A
Que é sua por lei e por direito

E9
Seguindo rastros no asfalto
D9
Sem ter mocinhas pra salvar
E9
Sem o relincho amigo
D9
Do seu cavalo Raio de Luar 
            A
Assim não dá

C
Lutando contra os índios daqui mesmo 
                                 A
Sem nenhum dólar furado pra furar 
C
Sem desfazer dos demais heróis 
                                   Bm  E
Fica sendo o mais teimoso dos cowboys 

A                 D9                      A
Que eu vi Bob Nelson, o único cowboy daqui

( D9  A  E9  A )
( D9  A  E9  D9 )

[Solo] A  D9  A  E9  A 

C
Lutando contra os índios daqui mesmo 
                                 A
Sem nenhum dólar furado pra furar 
C
Sem desfazer dos demais heróis 
                                   Bm  E
Fica sendo o mais teimoso dos cowboys 
A                 D9                      A
Que eu vi Bob Nelson, o único cowboy daqui

( D9  A  E9  A )

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
E9 = 0 2 4 1 0 0
