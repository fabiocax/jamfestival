Erasmo Carlos - Teletema

Intro: Bb7M  Eb/F

Bb7M            Bb6              Cm
Rumo, estrada turva, sou despedida
           Cº                  Gm7
Por entre lenços brancos de partida
       C7(9)             F7          Ab7
Em cada curva sem ter você vou mais só

Db7M           Db6              Ebm
Corro rompendo laços, abraços, beijos
         Ebº               Bbm7
Em cada passo é você quem vejo
         Eb7(9)             Ab7        F7
No tele-espaço pousado em cores no além

Brando, corpo celeste, meta metade
Meu santuário, minha eternidade
Iluminando o meu caminho e fim

Dando a incerteza tão passageira
Nós viveremos uma vida inteira
Eternamente, somente os dois mais ninguém

Bb  Bb7M    Eb7M    Eb
Eu vou de sol a sol
             Cm              C7
Desfeito em cor, refeito em som
             F      B
Perfeito em tanto amor

----------------- Acordes -----------------
Ab7 = 4 6 4 5 4 4
B = X 2 4 4 4 2
Bb = X 1 3 3 3 1
Bb6 = X 1 3 0 3 X
Bb7M = X 1 3 2 3 1
Bbm7 = X 1 3 1 2 1
C7 = X 3 2 3 1 X
C7(9) = X 3 2 3 3 X
Cm = X 3 5 5 4 3
Cº = X 3 4 2 4 2
Db6 = 9 X 8 10 9 X
Db7M = X 4 6 5 6 4
Eb = X 6 5 3 4 3
Eb/F = X 8 8 8 8 X
Eb7(9) = X 6 5 6 6 X
Eb7M = X X 1 3 3 3
Ebm = X X 1 3 4 2
Ebº = X X 1 2 1 2
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
Gm7 = 3 X 3 3 3 X
