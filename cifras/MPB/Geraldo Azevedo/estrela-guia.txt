Geraldo Azevedo - Estrela Guia

G9                   D/F#
Ela apareceu hoje no céu
                       E/F    G7
Só pra me ver tenho certeza
C                  G/B
Minha estrela guia brilha
                                   Am      Am/G
E o seu brilho vaza as folhas da palmeira,
                D    D/C
Caindo toda em mim...

G9                   D/F#
Minha estrela guia os meus passos
           D/F    G7
No azul planeta
C                  G/B
Minha namorada vem de longe
                  Am  Am/G
Pra fazer minha cabeça,
                D  D/C
Caindo toda em mim...


G9                  G7
Estrela, estrela,estrela
    C                C7M Am9/C
És deusa da noite do Rio
D/C
No Rio de Janeiro tão quente,
    G/B               G9 G7
Pra ti que vieste do frio

C7M                  C#º
Estrela, estrela, estrela
             D  D/C     G9    G7
De barro eu sou         arlequim
C9                  C#º
De prata tu és colombina,
           D  D/C     G9     G7
No cheiro deste    jardim

C9                  C#º D
Estrela, estrela, estrela
G9
Guia...

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am/G = 3 X 2 2 1 X
C = X 3 2 0 1 0
C#º = X 4 5 3 5 3
C7M = X 3 2 0 0 X
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
D/C = X 3 X 2 3 2
D/F = X X 3 2 3 2
D/F# = 2 X 0 2 3 2
E/F = 1 2 2 1 0 0
G/B = X 2 0 0 3 3
G7 = 3 5 3 4 3 3
G9 = 3 X 0 2 0 X
