Geraldo Azevedo - Cravo Vermelho

Intro: D  G  D  A7  D

D        A7            D
Quintais varandas de Olinda
D        A7          D
Farol ladeiras de luar
D             A7        D
Descansa teus olhos mirante
D            A7         D
Não tem marinheiro no mar
G                 D
Eu sou daqui mas vim de longe
            A7                      D
Contando estrelas naveguei na barca
grande
        A7           D
Cravo vermelho na lapela
        A7                     D
A madrugada vou varar junto com ela
D      A7          D
Tem rosa de cor morena

D       A7            D
Uma verbena pra me enfeitar
      A7               D
São flores de carne e osso
         A7               D
Pro meu pescoço pro meu colar

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
