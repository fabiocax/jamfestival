Geraldo Azevedo - Suíte Correnteza


 (Em  Am7   Em  E4  Em)
A luz do sol que encandeia  /  sereia de além mar
clara como o clarão do dia  /  marejou meu olhar
 (G/B  G/E Em  Em7 Em )
olho d'água beira de rio  /  vento vela bailar
(Em  Am7   Em  E4  Em)        (Am7  Em)
Barcarola do São Francisco   /   me leva para amar
 (Em  Am7   Em  E4  Em)
Era um domingo de lua  /  quando deixei Jatobá
era quem sabe esperança  /  indo a outro lugar
 (G/B  G/E Em       Em7 Em)
Barcarola do São Francisco  /  veleja agora no mar
 (Em  Am7   Em  E4  Em)         (Am7     Em)
sem leme mapa ou tesouro   /   de prata ou luar.

Talismã
Tom :Em
  Em                 Am
Diana me dê, um talismã...viajar
            C
você já pensou ir mais eu viajar

          Am                     Em
quando o sol desmaiar...ah, vou viajar...
 Am             A9               Am/F#
Olha essa sombra, esse rastro de mim
 Am             A9              Am/F#
olha essa sombra, essa réstia de sol
  C               Am               Em
você já pensou, ir mais eu, Diana...viajar...
 Em                             Am
Diana me dê um talismã, um talismã...

Caravana
  Am       Bm        Am       Bm
Corra não pare, não pense demais
  C/G         E/G#      Am
Repare essas velas no cais
       E/B    Am/C E/D D/E E7
Que a vida cigana
E  E/D E/B Am   Bm      Am     Bm
É caravana é pedra de gelo ao sol
   C/G        E/G#      Am
Degelou teus olhos tão sós
     E/B         Am/C E/D D/E E7 Am
Num mar de água clara

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
Am = X 0 2 2 1 0
Am/C = X 3 2 2 5 X
Am/F# = 2 X 2 2 1 0
Am7 = X 0 2 0 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C/G = 3 3 2 X 1 X
D/E = X X 2 2 3 2
E = 0 2 2 1 0 0
E/B = X 2 2 1 0 0
E/D = X 5 X 4 5 4
E/G# = 4 X 2 4 5 X
E4 = 0 2 2 2 0 0
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
G/B = X 2 0 0 3 3
G/E = 0 2 0 0 3 3
