Geraldo Azevedo - Semente e Fruto

[Intro]  G  D  F#7  Bm  Em  D  F#7/C#  Bm

G  D                F#7    Bm
Tomara, seja sempre assim, tomara
Em         D  F#7/C# Bm
Tempos viverão em    nós
G        D    F#7      Bm
Pura energia, paz e alegria
Em          D  F#7/C# Bm
Harmonia em nossa     voz

A vida há de ser assim
A onda vem beijar o cais
Em todo continente o sol vai nascer
Pra toda nossa gente a esperança
Aonde for esteja a fim
Da paz fruto do amor, em si

Tomara, seja sempre assim, tomara
Tempos viverão em nós
Pura energia, paz e alegria

Harmonia em nossa alma

Amor é que me faz sentir
A chama que me ilumina
Sublime sentimento do coração
Semente das mais lindas lembranças
Aquele que quer ser feliz
Se plantando amor, dá mais

Tomara, seja sempre assim, tomara
Tempos viverão em nós
Pura energia, paz e alegria
Harmonia em nossa alma

----------------- Acordes -----------------
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F#7 = 2 4 2 3 2 2
F#7/C# = X 4 4 3 5 X
G = 3 2 0 0 0 3
