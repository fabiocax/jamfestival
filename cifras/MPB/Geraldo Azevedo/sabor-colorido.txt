Geraldo Azevedo - Sabor Colorido

[Intro] Dm  C/E  F  Bb  F

 Dm  Bb  C          F  A7/E
Mel...  eu quero mel
                    Dm
Quero mel de toda flor
    Bb        C         F
Da rosa, rosa, rosa amarela encarnada
  A7/E               Dm            Bb
Branca como cravo, lírio e jasmim
       C          F  A7/E
Eu quero mel pra mim
 Dm  Bb  C           F   A7/E
Mel...  você quer mel?
                    Dm
Quero mel de toda flor
Bb         C           F         A7/E
  Da margarida sempre viva, viva
             Dm
Gira, gira, girassol
Bb           C           F         A7/E
  Se te dou mel pode pintar perigo

                 Dm
E logo aqui, no meu quintal
    Bb      C            F         A7/E
Cuidado, pode pintar formiga, viu?
 Dm  Bb  C          F  A7/E
Mel...  eu quero mel
                    Dm
Quero mel de toda flor
Bb     C      F  A7/E              Dm
  Colorido sabor... do mel de toda flor
Bb            C    F                A7/E
Antes que um passarinho aventureiro
                     Dm
Que beija um beijo, doce sabor
Bb   C       F   A7/E
  Sabor colorido
 Dm  Bb  C         F  A7/E
Mel... eu quero mel
                    Dm
Quero mel de toda flor
         Bb       C             F
Da açucena, violeta, flor de lís
                        A7/E
Flor de lótus, flor de cactos
                   Dm
Flor do pé de buriti
 Bb       C            F
Dália, papoula, crisântemo
                  A7/E                  Dm
Sonho maneiro, sereno, fulô do mandacaru
              Bb       C           F
Fulô do marmeleiro, fulô de catingueira
              A7/E                Dm
Fulô de laranjeira, fulô de jatobá
         Bb        C            F
Das imburanas, baraúnas, pé de cana
                     A7/E                Dm
Xique-xique, mel da cana, cana do canavial
               Bb            C          F  A7/E
Vem me dar um mel que eu quero me lambuzar
 Dm  Bb  C          F  A7/E
Mel...  eu quero mel
                    Dm
Quero mel de toda flor
Bb            C    F                A7/E
Antes que um passarinho aventureiro
                     Dm
Que beija um beijo, doce sabor
Bb   C       F   A7/E
  Sabor colorido
 Dm
Mel

----------------- Acordes -----------------
A7/E = 0 X 2 2 2 3
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
