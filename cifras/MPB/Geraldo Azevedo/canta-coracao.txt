Geraldo Azevedo - Canta Coração

Intr.: D D4 D

        D/C        G             B7       Em      :
Canta, canta passarinho, canta, canta miudinho    :
    A7             D D4 D                         :
Na palma da minha mão                             :
       D/C      G               B7          Em    : Refrão
Quero ver você voando, quero ouvir você cantando  :
       A7         D D4 D                          :
Quero paz no coração                              :
       D/C       G              B7          Em    :
Quero ver você voando, quero ouvir você cantando  :
       A7          D D4 D
Na palma da minha mão

    D/C            G          D/F#         C
Na palma da minha mão tem os dedos tem as linhas
       E7/B         Am         C7/G       F  C
Que olhar cigano caminha procurando alcançar
           D                 D/C          G/Bb
A nau perdida, o trem que chega, a nova dança

      A7        D               A7     D      D/C
Mata verde esperança, em suas tranças vou voar

      G    D/F#   Em A7         D D4 D
Passarin...in...nho    eu vou voar


(repete refrão)

       A7         D D4 D
Quero paz no coração

      D/C       G      D/F#            C
Meu alegre coração é triste como um camelo
    E7/B               Am      C7/G           F  C
É frágil que nem brinquedo, é forte como um leão
        D             D/C          G/Bb
É todo zelo, é todo amor, é desmantelo
        A7            D              A7           D      D/C
É querubim, é cão de fogo, é Jesus Cristo, é Lampião

      G    D/F#   Em A7         D D4 D D/C
Passarin...in...nho    eu vou voar
      G    B7     Em A7         D D4 D D/C
Passarin...in...nho    eu vou voar
      G    D/F#   Em A7         D D4 D D/C
Passarin...in...nho    eu vou voar

G B7 Em A7 D (...)

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
C7/G = 3 X 2 3 1 X
D = X X 0 2 3 2
D/C = X 3 X 2 3 2
D/F# = 2 X 0 2 3 2
D4 = X X 0 2 3 3
E7/B = X 2 2 1 3 X
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/Bb = X 1 0 0 3 3
