Geraldo Azevedo - O amanhã é distante

Intr.: (C F) F C

      C        F             C   F
E se hoje não fosse essa estrada
      C           F            C    F C
Se a noite não tivesse tanto atalho
       F      G            C    F C
O amanhã não fosse tão distante
     F         G        C   F C
Solidão seria nada pra você

       F       G            C           |
Se ao menos o meu amor estivesse aqui   |
        F       G             C         |
E eu pudesse ouvir o seu coração        |refrão

Se ao menos mentisse ao meu lado        |
   F              G             Intr.   |
Estaria em minha cama... outra vez      |

        C           F          C     F
Meu reflexo não consigo ver na água

       C       F               C  F C
Nem fazer canções sem nenhuma dor
       F    G             C
Nem ouvir o eco dos meus passos
         F             G               C
Nem lembrar meu nome quando alguém chamou

(repete refrão)

     C       F          C    F
Há beleza no rio do meu canto
     C       F                C   F C
Há beleza em tudo o que há no céu
       F           G             C
Porém nada com certeza é mais bonito
        F         G             C
Quando lembro dos olhos do meu bem

(repete refrão)

----------------- Acordes -----------------
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
