Geraldo Azevedo - Dia Branco

[Intro] D  D5+  D6  D7  G  G#º  A7(4)  A7
        D  D5+  D6  D7  G  Gm6/Bb

D           F#m/C
  Se você vier
         D/C
Pro que der e vier
   G   Gm6/Bb A7
Comigo
                 D  F#m/C#
Eu te prometo o sol
           D/C
Se hoje o sol sair
       G   Gm6/Bb  A7
Ou a chuva
             D  D5+
Se a chuva cair
          D6
Se você vier
    D7               G  G/F#
Até onde a gente chegar

       Em      G/D      A/C#  G/B
Numa praça na beira do mar
       A            A/G   D   D5+
Num pedaço de qualquer lugar
            D6   D7
Nesse dia branco
               G
Se branco ele for
      F#          F#/E      Bm  D7
Esse tanto, esse canto de amor
      G            G#º
Se você quiser e vier
         A7(4)  A7    D   D5+  D6  D7
Pro que der e vier comigo

( G  G#º  A7(4)  A7 )
( D  D5+  D6  D7  G  G#º  A7(4)  A7 )

               G
Se branco ele for
      A#º       ( Gº  Eº  C#º  A#º  F#)
Esse tanto, esse canto
     Bm  D7
De amor
      G            G#º
Se você quiser e vier
         A7(4)  A7    D   D5+  D6  D7
Pro que der e vier comigo

               G
Se branco ele for
      F#          F#/E      Bm  D7
Esse tanto, esse canto de amor
      G            G#º
Se você quiser e vier
         A7(4)  A7    D   D5+  D6  D7
Pro que der e vier comigo
   G   Gm6/Bb
Comigo
   D
Comigo

----------------- Acordes -----------------
A = X 0 2 2 2 0
A#º = X 1 2 0 2 0
A/C# = X 4 X 2 5 5
A/G = 3 X 2 2 2 X
A7 = X 0 2 0 2 0
A7(4) = X 0 2 0 3 0
Bm = X 2 4 4 3 2
C#º = X 4 5 3 5 3
D = X X 0 2 3 2
D/C = X 3 X 2 3 2
D5+ = X 5 4 3 3 X
D6 = X 5 4 2 0 X
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
Eº = X X 2 3 2 3
F# = 2 4 4 3 2 2
F#/E = X X 2 3 2 2
F#m/C# = X 4 4 2 2 2
G = 3 2 0 0 0 3
G#º = 4 X 3 4 3 X
G/B = X 2 0 0 3 3
G/D = X 5 5 4 3 X
G/F# = X X 4 4 3 3
Gm6/Bb = X 1 2 0 3 0
Gº = 3 X 2 3 2 X
