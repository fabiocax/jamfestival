Maria Gadú - Lanterna Dos Afogados

[Intro] D4/G  A/G  F#m7  Bm
        D4/G  A/G  F#m7  Bm  C

[Primeira Parte]

             D4/G             A/G
Quando tá escuro e ninguém te ouve
                F#m7         Bm
Quando chega a noite e você pode chorar
               D4/G             A/G
Há uma luz no túnel dos desesperados
               F#m7              Bm
Há um cais do porto pra quem precisa chegar

[Refrão]

          D4/G         A/G
Tô na lanterna dos afogados
           F#m7             Bm        C
Tô te esperando, vê se não vai demorar


[Segunda Parte]

           D4/G               A/G
Uma noite longa por uma vida curta
                 F#m7          Bm
Mas já não me importa basta poder te ajudar
              D4/G                A/G
E são tantas marcas que já fazem parte
             F#m7           Bm
Do que sou agora mas ainda sei me virar

[Refrão]

          D4/G         A/G
Tô na lanterna dos afogados
           F#m7             Bm        C
Tô te esperando, vê se não vai demorar

( D4/G  A/G )
( D4/G  A/G  F#m7  Bm  C )

[Segunda Parte]

           D4/G               A/G
Uma noite longa por uma vida curta
                 F#m7          Bm
Mas já não me importa basta poder te ajudar
              D4/G                A/G
E são tantas marcas que já fazem parte
             F#m7           Bm
Do que sou agora mas ainda sei me virar

[Refrão]

          D4/G         A/G
Tô na lanterna dos afogados
           F#m7             Bm
Tô te esperando, vê se não vai demorar

( D4/G  A/G  F#m7  Bm )
( D4/G  A/G  F#m7  Bm )
( D4/G  A/G  F#m7  Bm  C )

[Refrão]

          D4/G         A/G
Tô na lanterna dos afogados
           F#m7             Bm        C
Tô te esperando, vê se não vai demorar

----------------- Acordes -----------------
A/G = 3 X 2 2 2 X
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D4/G = 3 X X 2 3 3
F#m7 = 2 X 2 2 2 X
