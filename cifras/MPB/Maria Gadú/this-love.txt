Maria Gadú - This Love

E7                         Am7                      Dm7
I was so high I did not recognize, the fire burning in her eyes
                  E7
The chaos that controlled my mind
                          Am7                    Dm7
Whispered goodbye she got on a plane never to return again
                 E7
But always in my heart, Oh

(refrão)
Am7   Dm7     G7          C         Am7          Dm7
This love has taken its toll on me, she said goodbye
     G7           C
Too many times before
     Am7  Dm7       G7                C       Am7        Dm7
And her heart is breakin' in front of me, 'n I have no choice
      G7                        C
'Cause I won't say goodbye anymore
E7      Am7         Dm7      E7
Whoa-oh-oh, whoa-oh-oh, whoa-oh-oh-oh-oh


I tried me best to feed her appetite, keep her coming every night
So hard to keep her satisfied, oh
Kept playin' love like it was just a game, pretending to be the same
To turn around and leave again, but oh-oh

Dm7                             C
I'll fix these broken things, repair your broken wings
E7                         Am7
And make sure everything's alright (it's alright)
Dm7                         C
My pressure on your hips, sinkin' in' my fingertips
E7                          Am7
Every inch of you because I know that's what you want me to do

This love has taken its toll on me, she said bye
Too many times before
And my heart is breakin' in front of me, she said goobye
Too many times before

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
G7 = 3 5 3 4 3 3
