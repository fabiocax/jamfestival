Maria Gadú - Ne Me Quitte Pas

[Intro] Em7  Em7/D  Am7  B7(b13)  B7
        Em7  Em7/D  Am7  B7(b13)  B7

[Primeira Parte]

              Em7              Em/D
Ne me quitte pas, Il faut oublier
            Am7
Tout peut s'oublier qui s'enfuit déjà
                        B7
Oublier le temps des malentendus
                Em7            Em/D
Et le temps perdu à savoir comment
         Am7                    B7                      Em7
Oublier ces heures qui tuaient parfois a coups de pourquoi
                Em/D
Le coeur du bonheur
              Am7
Ne me quitte pas
              B7
Ne me quitte pas

              Em7  B7
Ne me quitte pas

[Segunda Parte]

               Em7                C               B7
Moi je t'offrirai, des perles de pluie venues de pays
                Em7
Où il ne pleut pas
                  Em7                  C
Je  creuserai la terre jusqu'après ma mort
                  D                   B7
Pour couvrir ton corps d'or et de lumière
             Em7                  Dm
Je ferai un domaine ou l'amour sera roi
              C    B7        Em7
Ou l'amour sera loi ou tu sera reine
              G
Ne me quitte pas
              Am7
Ne me quitte pas
 B7
Ne me quitte pas

( Em7  Em7/D  Am7  B7 )

[Terceira Parte]

              Em7             Em/D
Ne me quitte pas Je t'inventerai
         Am7
Des mots insensés Que tu comprendras
            D7                D7/C
Je te parlerai De ces amants-là
                  G/B                Em7
Qui ont vue deux fois Leurs coeurs s'embraser
            Am7              B7
Je te raconterai L'histoire de ce roi
          Em7              Em/D
Mort de n'avoir pas Pu te rencontrer
              Am7
Ne me quitte pas
              B7
Ne me quitte pas
              Em7  B7
Ne me quitte pas

[Quarta Parte]

            Em7               C
On a vu souvent Rejaillir le feu
                B7
De l'ancien volcan
                     Em7
Qu'on croyait trop vieux Il est paraît-il
               C                     D                   B7
Des terres brûlées Donnant plus de blé Qu'un meilleur avril
                    Em7                      Dm
Et quand vient le soir Pour qu'un ciel flamboie
                 C   B7           Em7
Le rouge et le noir Ne s'épousent-ils pas
        G
Ne me quitte pas
        Am7
Ne me quitte pas
 B7
Ne me quitte pas

[Quinta Parte]

              Em7                     Em/D
Ne me quitte pas Je ne vais plus pleurer
                    Am7
Je ne vais plus parler Je me cacherai là

           B7
A te regarder Danser et sourire
            Em7                 Em/D
Et à t'écouter Chanter et puis rire
                Am7               B7
Laisse-moi devenir L'ombre de ton ombre
         Em7                Em/D
L'ombre de ta main L'ombre de ton chien
              Am7
Ne me quitte pas
              B7
Ne me quitte pas
              Em7  B7
Ne me quitte pas

( Em7  Em7/D  Am7  B7 )
( Em7  Em7/D  Am7  B7 )
( Em7  Em7/D  Am7  B7 )
( Em7  Em7/D  Am7  B7 )

              Em7
Ne me quitte pas

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
B7(13-) = X 2 X 2 4 3
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
D7/C = X 3 X 2 3 2
Dm = X X 0 2 3 1
Em/D = X X 0 4 5 3
Em7 = 0 2 2 0 3 0
Em7/D = X X 0 0 3 0
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
