Maria Gadú - Caleidoscópio

Intro 4x: B7  D#7/9  E7/9

B7
Não é preciso apagar a luz
D#7/9  E7/9
Eu fecho os olhos tudo bem
B7           D#7/9 ^ E7/9
Num caleidoscópio sem lógica

B7
Eu quase posso ouvir a sua voz
D#7/9  E7/9
Eu sinto a sua mão a me guiar
B7           D#7/9  E7/9
Pela noite a caminho de casa

Refrão:
C#m7
Quem é que vai pagar as contas desse amor pagão?
D#m7
Te dar a mão ou me trazer a tona pra respirar

C#m7
Vai chamar meu nome ou te escutar

B7
Me pedindo pra apagar a luz
D#7/9  E7/9
Amanheceu é hora de domir
B7          D#7/9  E7/9
Nesse nosso relogio sem orbita

B7
Se tudo tem que terminar assim
D#7/9  E7/9
Que pelo menos seja até o fim
B7                D#7/9  E7/9
Pra gente não ter nunca mais que terminar

( B7  D#7/9  E7/9 ) (4x)

(Refrão)

( B7  D#7/9  E7/9 ) (4x)

Dedilhado: B7 6 (432)
           D#7/9  E7/9 5 4 (432) (432) 5 3 4 2 5 3 4 2

----------------- Acordes -----------------
B7 = X 2 1 2 0 2
C#m7 = X 4 6 4 5 4
D#7/9 = X 6 5 6 6 X
D#m7 = X X 1 3 2 2
E7/9 = X X 2 1 3 2
