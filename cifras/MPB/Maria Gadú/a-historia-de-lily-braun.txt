Maria Gadú - A História de Lily Braun

[Intro] C7/9   Ab7(13)   Eb7(#9)   D7(#9)   G7(b13)
        C7/9   Ab7(13)   Eb7(#9)  D7(#9)  C#7/9

 C7/9  Ab7(13)  Eb7(#9)  D7(#9)            G7(b13)
Como  num romance        o homem dos meus sonhos
   C7/9      Ab7(13)   Dm7(5b)    G7(b13)
Me apareceu no dancing era   mais um
 C7/9      Ab7(13)    Dm7(5b)       G7(b13)
Só    que num relance os    seus olhos me     chuparam
 C7/9     Ab7(13)  Dm7(5b)  G7(b13)
Feito um zoom
C7(9)     Ab7(13)  Eb7(#9)  D7(#9)       G7(b13)
Ele   me comi___a          com   aqueles olhos
    C7(9)     Ab7(13)    Dm7(5b)        G7(b13)
De comer   fotografi___a eu    disse cheese
C7(9)      Ab7(13)  Dm7(5b)        G7(b13)
E     de close em close  fui   perdendo a pose
       C7(9)     Ab7(13)  Dm7(5b)  G7(b13)
E até sorri,  feliz

Gm7               C7(9)                  F7+(9)
E   voltou me ofereceu um drinque me chamou   de anjo azul

       Em7(5b)           A7(b13)           Dm7(5b)  Ab7(13)  G7(b13)
Minha visão    foi desde então   ficando flou
 C7(9)    Ab7(13)  Eb7(#9)  D7(#9)           G7(b13)
Como  no cinema            me    mandava às vezes
C7(9)       Ab7(13)    Dm7(5b)     G7(b13)
Uma rosa  e um poema  foco  de luz
C7(9)     Ab7(13)     Dm7(5b)        G7(b13)
Eu,   feito uma gema me    desmilingüindo toda
    C7(9)     Ab7(13)  Dm7(5b)  G7(b13)
Ao som   do blues
C7(9)      Ab7(13)   Dm7(5b)       G7(b13)
Abu___sou do scoth  disse que meu corpo
        C7(9)     Ab7(13)  Dm7(5b)       G7(b13)
Era só dele  aquela noite  eu    disse please
 C7(9)      Ab7(13)   Dm7(5b)        G7(b13)
Xale  no decote   disparei com as faces
       C7(9)     Ab7(13)  Eb7(#9)  D7(#9)  G7(b13)
Rubras e     febris

Gm7                       C7(9)                F7+(9)
E  voltou No derradeiro show  com dez poemas e um buquê
         Em7(5b)            A7(b13)        Dm7(5b)  Ab7(13)  G7(13-)
Eu disse adeus   já vou com os meus  numa turnê

 C7(9)     Ab7(13)  Db7(5-)          G7(b13)
Como  amar esposa  disse ele que agora
      C7(9)   Ab7(13)  Dm7(5b)        G7(b13)
Só me amava  como esposa não   como star
   C7(9)   Ab7(13)  Eb7(#9)  D7(#9)         G7(b13)
Me amassou as    rosas              me queimou as fotos
    C7(9)       Ab7(13)  Eb7(#9)  D7(#9)  G7(b13)
Me beijou no altar
 C7(9)      Ab7(13)  Dm7(5b)      G7(b13)
Nunca mais romance  nunca mais cinema
       C7(9)         Ab7(13)    Dm7(5b)        G7(b13)
Nunca mais  drinque no dancing nunca mais cheese
 C7(9)       Ab7(13)  Dm7(5b)     G7(b13)
Nunca uma espelunca   uma   rosa nunca
       C7(9)   Ab7(13)  Eb7(#9)  D7(#9)  G7(b13)
Nunca mais  feliz

( C7/9  Ab7(13)  Dm7(5b)  G7(b13) )
( C7/9  Ab7(13)  Eb7(#9)  D7(#9)  G7(b13) )

Gm7               C7(9)               F7+(9)
E voltou no derradeiro show  com dez poemas e um buquê
         Em7(5b)               A7(b13)       Dm7(5b)  Ab7(13)  G7(13-)
Eu disse adeus  já vou com os meus  numa turnê

 C7(9)        Ab7(13)  Db7(5-)          G7(b13)
Como  amar espo___sa  disse ele que agora
      C7(9)   Ab7(13)     Dm7(5b)     G7(b13)
Só me amava  como esposa não   como star
   C7(9)   Ab7(13)  Eb7(#9)  D7(#9)        G7(b13)
Me amassou as rosas         me queimou as fotos
       C7(9)      Ab7(13)  Eb7(#9)  D7(#9)  G7(b13)
Me beijou   no altar
 C7(9)      Ab7(13)  Dm7(5b)      G7(b13)
Nunca mais romance  nunca mais cinema
       C7(9)            Ab7(13)  Dm7(5b)     G7(b13)
Nunca mais  drinque no dancing  nunca mais cheese
 C7(9)       Ab7(13)  Dm7(5b)     G7(b13)
Nunca uma espelunca   uma   rosa nunca
       C7(9)   Ab7(13)  Eb7(#9)  D7(#9)  G7(b13)
Nunca mais  feliz

( C7/9  Ab7(13)  Dm7(5b)  G7(b13) )
( C7/9  Ab7(13)  Eb7(#9)  D7(#9)  G7(b13) )

                                                      Ab7(13)
E|---------------------------------------------------x-----------------|
B|---------------------------------------------------6-----------------|
G|--------------------------------1-0-------0--------5-----------------|
D|--------------0-1----------0-1----------1----------4-----------------|
A|----1-2-3-------------2-3-------------3------------x-----------------|
E|-3-------------------------------------------------4-----------------|

----------------- Acordes -----------------
A7(b13) = X 0 X 0 2 1
Ab7(13) = 4 X 4 5 6 X
C#7/9 = X 4 3 4 4 X
C7(9) = X 3 2 3 3 X
C7/9 = X 3 2 3 3 X
D7(#9) = X 5 4 5 6 X
Db7(5-) = X 4 5 4 6 X
Dm7(5b) = X X 0 1 1 1
Eb7(#9) = X 6 5 6 7 X
Em7(5b) = X X 2 3 3 3
F7+(9) = X 8 7 9 8 X
G7(13-) = 3 X 3 4 4 3
G7(b13) = 3 X 3 4 4 3
Gm7 = 3 X 3 3 3 X
