Marisa Monte - Xote Das Meninas

        C         F         C
Mandacaru quando fulorá na seca
        C7              F
E um siná que a chuva chega no sertão
        C
Toda menina que enjoa da boneca
     A7            D7        G7         C
É sinal de que o amor já chegou no coração
          F             C
Meia comprida não quer mais sapato baixo
    A7          D7            G7            C
Vestido bem cintado não quer mais vestir gibão

         Bm7/5- E7           Am
Ela só quer só pensa em namorar
         Bm7/5- E7           Am
Ela só quer só pensa em namorar
         Bm7/5- E7           Am
Ela só quer só pensa em namorar
         Bm7/5- E7           Am
Ela só quer só pensa em namorar


          Dm7     G7    C
De manhã cedo já tá pintada
              Bm7/5-   E7        Am
Só vive suspirando sonhando acordada
                  Bm7/5- E7         Am
O pai leva ao doutor a  filha adoentada
                Bm7/5-    E7             Am
Não come nem estuda, não dorme nem quer nada

         Bm7/5- E7           Am
Ela só quer só pensa em namorar
         Bm7/5- E7           Am
Ela só quer só pensa em namorar
         Bm7/5- E7           Am
Ela só quer só pensa em namorar
         Bm7/5- E7           Am
Ela só quer só pensa em namorar

          Dm7    G7  C
Mas o doutor nem examina
                   Bm7/5-   E7             Am
Chamando o pai do lado lhe diz logo em surdina
                 Bm7/5-     E7        Am
Que o mal é da idade e que pra tal menina
                Bm7/5-   E7       Am
Não há um só remédio em toda medicina

         Bm7/5- E7           Am
Ela só quer só pensa em namorar
         Bm7/5- E7           Am
Ela só quer só pensa em namorar
         Bm7/5- E7           Am
Ela só quer só pensa em namorar
         Bm7/5- E7           Am
Ela só quer só pensa em namorar

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bm7/5- = X 2 3 2 3 X
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
D7 = X X 0 2 1 2
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
G7 = 3 5 3 4 3 3
