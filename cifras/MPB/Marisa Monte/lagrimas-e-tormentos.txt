Marisa Monte - Lágrimas e Tormentos

(intro) Bb7M  F7(b13)

 Bb7M       Bb6
Lagrimas,tormentos
          Dm7(b5) G7 Cm7 G#m6 Cm7
Quantas desilusoes
                              F7  Bb7M  Bb6
Foram tantos sofrimentos e decepcoes
             Fm7     Fm6      Eb7M  G7(b13)  Cm7
Mas um dia o destino a tudo modificou
       F7        Bb7M
Minhas lagrimas secaram
        G7          Gm6
Meus tormentos terminaram
        F7          Fm7  Fm6  Eb7M(9)
Foi uma nuvem que passou
       F/Eb      Bb7M
Minhas lagrimas secaram
       G7        Gm6
Meus tormentos terminaram
        F7         Bb7M  Bb6
Foi uma nuvem que passou

  Cm7           F7           Bb7M
E hoje a minha vida e um carrossel de alegrias
Am7(b5)          D7           Dm7(b5)      G7
E como se nao bastasse, estou amando de verdade
Cm7       Dbdim                Bb7M     G7
Me perdoa se eu me excedo em minha euforia
      Gm6        F7          Dm7(b5)  G7
Mas e que agora sei o que e felicidade
   Cm7    Dbdim               Bb7M    G7
Me perdoa se eu me excedo em minha euforia
    Gm6         F7            Bb7M   F7(b13)
Mas e que agora sei o que e felicidade

----------------- Acordes -----------------
Am7(b5) = 5 X 5 5 4 X
Bb6 = X 1 3 0 3 X
Bb7M = X 1 3 2 3 1
Cm7 = X 3 5 3 4 3
D7 = X X 0 2 1 2
Dbdim = X 4 5 3 5 3
Dm7(b5) = X X 0 1 1 1
Eb7M = X X 1 3 3 3
Eb7M(9) = X 6 5 7 6 X
F/Eb = X X 1 2 1 1
F7 = 1 3 1 2 1 1
F7(b13) = 1 X 1 2 2 1
Fm6 = 1 X 0 1 1 X
Fm7 = 1 X 1 1 1 X
G#m6 = 4 X 3 4 4 X
G7 = 3 5 3 4 3 3
G7(b13) = 3 X 3 4 4 3
Gm6 = 3 X 2 3 3 X
