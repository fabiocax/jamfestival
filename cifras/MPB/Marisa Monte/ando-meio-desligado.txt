Marisa Monte - Ando Meio Desligado

Intro: Cm7 F

Cm7   F           Cm7  F        Cm7    F          Cm7  F
Ando meio desligado eu nem sinto meus pés no chão

Cm7   F            Cm7  F        Cm7  F             Cm7  F
Olho e não vejo nada eu só penso se você me quer

Bb                          Cm7                          Bb
Eu não vejo a hora de lhe dizer aquilo tudo que eu decorei

Bb                            Cm7
E depois o beijo que eu já sonhei você vai sentir mas

      Ab Bb           Cm7          Ab Bb             Cm7
Por favor não leve a mal eu só quero que você me queira

            Ab Bb           Cm7
Não leve a mal, não leve a mal.

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Bb = X 1 3 3 3 1
Cm7 = X 3 5 3 4 3
F = 1 3 3 2 1 1
