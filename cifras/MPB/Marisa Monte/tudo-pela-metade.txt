Marisa Monte - Tudo Pela Metade

(intro) A7 D7

   A7
Eu admiro o que não presta
         D7
Eu escravizo quem eu gosto
          A7
Eu não entendo
                      D7
Eu trago o lixo para dentro. (bis)

   G             F#      F  E
Eu abro a porta para estranhos
A7          D7
  Eu cumprimento
     G            F#     F  E
Eu quero aquilo que não te...nho
A7                   D7
  Eu tenho tanto a fazer
G     F#    F   E       A7 D7
  Eu faço tudo pela metade

    A7       D7
Eu não percebo

C                      G
 Eu falo muito palavrão Eu falo muito mal
A                     C            G                 D
 Eu falo muito mesmo sem saber o que estou falando
C                       G        A     C   G     D
 Eu falo muito bem, eu minto... ô...

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
