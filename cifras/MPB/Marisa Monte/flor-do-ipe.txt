Marisa Monte - Flor do Ipê

[Intro]  A9

A9             G#7  C#m    F#7
Onde quer que eu vá levo o meu amor
   B7       E7        A9  E7/A
De Botafogo até o Arpoador
A9       G#7  C#m      F#7
Levo meu amor para onde eu for
   B7/D#          E/D       A/C#
Do Capricórnio até o Equador

F7+            G7            A9
Quando a noite cai me dá saudade
Dm9           E7              A9
Quando cai no chão a flor do Ipê
G#m5-/7      C#7      F#m7  F#m9/E
Tenho essa felicidade
B7               Dm6  E7
Posso recordar você

A9        G#7  C#m      F#7
Levo meu amor com a solidão

  B7          E7            A9
É bom assim, a paz não é pra mim
A9          G#7  C#m         F#7
Para onde eu for levo o meu amor
   B7       E7           A9
Na rua, no hotel, no elevador
A7+        G#7  C#m             F#7
Levo o meu amor onde quer que eu vá
   B7/D#       E/D        A/C#
Na Lapa, Niterói, Paquetá

F7+         G7        A9
Na calçada, flores amarelas
Dm9         E7             A9
Formam um tapete ao pé do ipê
G#m(b5)/7  C#7        F#m7  F#m9/E
Você não é mais aquela
B7             Dm6  E7
Mas ainda faz doer

A9       G#7   C#m      F#7
Levo meu amor com a solidão
   B7            E7            A9     F#7
Tá certo assim, a paz não é pra mim
   B7             E7           C#m   F#7
Tá certo assim, a paz não é pra mim
   B7            E7            A9
Tá certo assim, a paz não é pra mim

----------------- Acordes -----------------
A/C# = X 4 X 2 5 5
A7+ = X 0 2 1 2 0
A9 = X 0 2 2 0 0
B7 = X 2 1 2 0 2
B7/D# = X X 1 2 0 2
C#7 = X 4 3 4 2 X
C#m = X 4 6 6 5 4
Dm6 = X 5 X 4 6 5
Dm9 = X 5 7 9 6 5
E/D = X 5 X 4 5 4
E7 = 0 2 2 1 3 0
E7/A = 5 7 6 7 5 X
F#7 = 2 4 2 3 2 2
F#m7 = 2 X 2 2 2 X
F#m9/E = 0 4 6 2 2 2
F7+ = 1 X 2 2 1 X
G#7 = 4 6 4 5 4 4
G#m5-/7 = 4 X 4 4 3 X
G7 = 3 5 3 4 3 3
