Marisa Monte - Chuva No Mar

A
Coisas transformam-se em mim
       D7/9
É como chuva no mar
E7
Se desmancha assim em
A
Ondas a me atravessar
         D7/9          E7
Um corpo sopro no ar
                  D7
Com um nome pra chamar
      E7
É só alguém batizar

Nome pra chamar de
A
Nuvem, vidraça, varal
       D7/9
Asa, desejo, quintal
  E7
O horizonte lá longe

A
Tudo o que o olho alcançar
         D7/9
E o que ninguém escutar
E7              D7
Te invade sem parar
E7                   F#m7
Te transforma sem ninguém notar
E7
Frases, vozes, cores
A
Ondas, frequências, sinais
          D7/9
O mundo é grande demais
E7                        D7
Coisas transformam-se em mim
            E7
Por todo o mundo é assim

A
Coisas transformam-se em mim
       D7/9
É como chuva no mar
E7
Se desmancha assim em
A
Ondas a me atravessar
         D7/9          E7
Um corpo sopro no ar
                  D7
Com um nome pra chamar
      E7
É só alguém batizar

Nome pra chamar de
A
Nuvem, vidraça, varal
       D7/9
Asa, desejo, quintal
  E7
O horizonte lá longe
A
Tudo o que o olho alcançar
         D7/9
E o que ninguém escutar
E7              D7
Te invade sem parar
E7                   F#m7
Te transforma sem ninguém notar
E7
Frases, vozes, cores
A
Ondas, frequências, sinais
          D7/9
O mundo é grande demais
E7                        D7
Coisas transformam-se em mim
            E7
Por todo o mundo é assim
                   A
Isso nunca vai ter fim

----------------- Acordes -----------------
A = X 0 2 2 2 0
D7 = X X 0 2 1 2
D7/9 = X 5 4 5 5 X
E7 = 0 2 2 1 3 0
F#m7 = 2 X 2 2 2 X
