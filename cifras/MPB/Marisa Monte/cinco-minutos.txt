Marisa Monte - Cinco Minutos

Intro: F : E : Eb : D C#
G : F# : Dm : %

C
Pedi você
                F#
Prá esperar 5 minutos só
Você foi embora
         Dm
sem me atender
     G
Não sabe o que perdeu

               F#
Pois você não viu,
você não viu...
     Dm
Como eu fiquei
 G
Pedi Você
                      F#
Prá esperar 5 minutos só

você foi embora,
embora, embora
     Dm
sem me atender...

      G
pois você não viu,
não viu, não viu
     Dm
como eu fiquei
F                  E
dizem que foi chorando,
   Eb        D     C#
sorrindo, cantando
F
Os meus amigos,
          E
meus amigos, até disseram

          E       D
Que foi amando, amando
G
Pois você não sabe,
você não sabe
F#
E nunca, e nunca,
E nunca, e nunca,
E nunca, e nunca,
      Dm
Vai saber porque
      C
Pois você não sabe

                F#
quanto vale 5 minutos,
              Dm
5 minutos na vida

----------------- Acordes -----------------
C = X 3 2 0 1 0
C# = X 4 6 6 6 4
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
