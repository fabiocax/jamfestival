Marisa Monte - Três Letrinhas

Intro:
F#     G#7/F# / Bm/F# / F#7M /
F#7M / G#7/F# / Bm/F# / F#7M / F#7M

F#              G#7
Sim, são três letrinhas
          G#m7
Todas bonitinhas
C#7(9)      F#7M
Fáceis de dizer

       G#m7  A#m7
Ditas por você
                 D#m7
Nesse seu sim, assim
               G#7
Outras três também

Representam não
G#m7                    C#7
Que não fica bem no seu coração


F#              G#7
Sim, são três letrinhas
           G#m7
Todas bonitinhas
C#7(9)      F#7M
Fáceis de dizer
      G#m7  A#m7
Ditas por você
                 D#m7
Nesse seu sim, assim
               G#7
Outras três também
Representam não
G#m7                    C#7    D#m7
Que não fica bem no seu coração

            G#7             G#m7
É minha canção resto de oração
      C#7     F#
Que fugiu da igreja
         E7      D#7
Não quis mais do vinho
             G#m7
Foi tomar cerveja
             A#7
Voltou ao jardim

         D#m7  B
E tá esperando gente
        C#    F#
Que só disse sim
  D#m7         B
E tá esperando gente
       C#    F#
Que só disse sim
  D#m7         B
E tá esperando gente
       C#    F#
Que só disse sim
  D#m7         B
E tá esperando gente
       C#    F#
Que só disse sim

----------------- Acordes -----------------
A#7 = X 1 3 1 3 1
A#m7 = X 1 3 1 2 1
B = X 2 4 4 4 2
Bm/F# = X X 4 4 3 2
C# = X 4 6 6 6 4
C#7 = X 4 3 4 2 X
C#7(9) = X 4 3 4 4 X
D#7 = X 6 5 6 4 X
D#m7 = X X 1 3 2 2
E7 = 0 2 2 1 3 0
F# = 2 4 4 3 2 2
F#7M = 2 X 3 3 2 X
G#7 = 4 6 4 5 4 4
G#7/F# = 2 X 1 1 1 X
G#m7 = 4 X 4 4 4 X
