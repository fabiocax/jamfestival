Marisa Monte - De Mais Ninguém

[Intro] Dm  Bm7(b5)  Am  B7
        E7  Am  E7

[Primeira Parte]

Am        Am/G  F6
  Se ela me deixou, a dor
Bm7(b5)        E7     Am             E7
      É minha só, não é de mais ninguém
Am           A7/C# Dm
  Aos outros eu devolvo a dor
G7            G/F   C/E  C/Bb
  Eu tenho a minha dor

[Segunda Parte]

A7         A7/C#           Dm
  Se ela preferiu ficar sozinha
G7                  C  E7/B
Ou já tem um outro bem
Am        Am/G  F
  Se ela me deixou a dor é minha

         Bm7(b5)
A dor é de quem tem

[Primeira Parte]

Am          Am/G      F
  É meu troféu, é o que restou
Bm7(b5)             E7   Am           E7
      É o que me aquece sem me dar calor
Am           A7/C#   Dm
  Se eu não tenho o meu amor
G7            G/F   C/E  C/Bb
  Eu tenho a minha dor

[Segunda Parte]

A7                     Dm
  A sala, o quarto, a casa está vazia
G7                 C  E7/B
A cozinha, o corredor
Am              Am/G F
  Se nos meus braços ela não se aninha
         Bm7(b5)  E7
A dor é minha, a dor

[Primeira Parte]

Am        Am/G  F6
  Se ela me deixou, a dor
Bm7(b5)        E7     Am             E7
      É minha só, não é de mais ninguém
Am           A7/C# Dm
  Aos outros eu devolvo a dor
G7            G/F   C/E  C/Bb
  Eu tenho a minha dor

[Segunda Parte]

A7         A7/C#           Dm
  Se ela preferiu ficar sozinha
G7                  C  E7/B
Ou já tem um outro bem
Am        Am/G  F
  Se ela me deixou a dor é minha
         Bm7(b5)
A dor é de quem tem

( Am  Am/G  F  Bm7(b5) )
( E7  Am  E7 )
( Am  A7/C#  Dm )
( G7  G/F  C/E  C/Bb )
( A7  A7/C#  Dm  G7  C )
( E7  Am  Am/G  F  Bm7(b5) )

[Primeira Parte]

Am            Am/G     F
  É o meu lençol, é o cobertor
Bm7(b5)             E7   Am           E7
      É o que me aquece sem me dar calor
Am           A7/C#   Dm
  Se eu não tenho o meu amor
G7            G/F   C/E  C/Bb
  Eu tenho a minha dor

[Segunda Parte]

A7                     Dm
  A sala, o quarto, a casa está vazia
G7                 C  E7/B
A cozinha, o corredor
 Am           Am/G F
Se nos meus braços ela não se aninha
         Bm7(b5)  E7
A dor é minha, a dor

[Final] Am  Am/G  F  Bm7(b5)
        E7  Am  E7
        Am  A7/C#  Dm

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7/C# = X 4 5 2 5 X
Am = X 0 2 2 1 0
Am/G = 3 X 2 2 1 X
B7 = X 2 1 2 0 2
Bm7(b5) = X 2 3 2 3 X
C = X 3 2 0 1 0
C/Bb = X 1 2 0 1 X
C/E = 0 3 2 0 1 0
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
E7/B = X 2 2 1 3 X
F = 1 3 3 2 1 1
F6 = 1 X 0 2 1 X
G/F = 1 X X 0 0 3
G7 = 3 5 3 4 3 3
