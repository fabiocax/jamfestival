Marisa Monte - Beija Eu

[Intro] D  D7M  D6  D7M
        D  D7M  D6  D7M
        D  D7M  D6  D7M
        D  D7M  D6  D7M
        D  D7M  D6  D7M
        D  D7M  D6  D7M

[Primeira Parte]

 D
Seja eu
 D7M
Seja eu
 D7           Em
Deixa que eu seja eu
    E7
E aceita
       A       A/G
O que seja seu
       A/F#     A/E
Então deita e aceita eu


 D
Molha eu
 D7M
Seca eu
 D7           Em
Deixa que eu seja o céu
     E7
E receba
    A       A/G
O que seja seu
       A/F#     A/E    D
Anoiteça e amanheça eu

[Refrão]

D
  Beija eu
D7M
    Beija eu
D7               Em
   Beija eu, me beija
 E7
Deixa
       A    A/G
O que seja ser
       A/F#     A/E
Então beba e receba
     D        D7M
Meu corpo no seu corpo
D7          Em
Eu, no meu corpo
 E7
Deixa
       A  A/G
Eu me deixo
     A/F#       A/E  D
Anoiteça e amanheça

[Primeira Parte]

 D
Seja eu
 D7M
Seja eu
 D7           Em
Deixa que eu seja eu
    E7
E aceita
       A       A/G
O que seja seu
       A/F#     A/E
Então deita e aceita eu

 D
Molha eu
 D7M
Seca eu
 D7           Em
Deixa que eu seja o céu
     E7
E receba
    A       A/G
O que seja seu
       A/F#     A/E    D
Anoiteça e amanheça eu

[Solo] D  D7M  D7  Em  E7
       A  A/G  A/F#  A/E
       D  D7M  D7  Em  E7
       A  A/G  A/F#  A/E

[Refrão]

D
  Beija eu
D7M
    Beija eu
D7               Em
   Beija eu, me beija
 E7
Deixa
       A    A/G
O que seja ser
       A/F#     A/E
Então beba e receba
     D        D7M
Meu corpo no seu corpo
D7          Em
Eu, no meu corpo
 E7
Deixa
       A  A/G
Eu me deixo
     A/F#       A/E
Anoiteça e amanheça

[Final] D  D7M  D7  Em  E7
        A  A/G  A/F#  A/E
        D  D7M  D7  Em  E7
        A  A/G  A/F#  A/E  D

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/E = 0 X 2 2 2 0
A/F# = 2 0 2 2 2 0
A/G = 3 X 2 2 2 X
D = X X 0 2 3 2
D6 = X 5 4 2 0 X
D7 = X X 0 2 1 2
D7M = X X 0 2 2 2
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
