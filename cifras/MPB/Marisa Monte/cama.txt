Marisa Monte - Cama

Intro: C#m C#m C#m6

C#m  C#m6       C#m    C#m6
Cama, garganta e água
       C#m
Gargalo e lábio
 C#m6    C#m    C#m6  F#m
Batom e língua, flama lama
F#m6      F#m F#m6   C#m
Como quem ama uma palavra
 C#m6      C#m
Beijo e gengiva
C#m6    C#m
Ar e saliva
C#m6     C#m  C#m6   F#m
Riso e comida chama, clama
F#m6      F#m F#m6  C#m   C#m6   C#m
Como quem ama uma palavra

Interlúdio: ( C#m F#m C#m F#m G# )

----------------- Acordes -----------------
C#m = X 4 6 6 5 4
C#m6 = X 4 X 3 5 4
F#m = 2 4 4 2 2 2
F#m6 = 2 X 1 2 2 X
G# = 4 3 1 1 1 4
