Djavan - Se Não Vira Jazz

Intro 2x:

Parte 1

E|-------------------------------------------|
B|-------------------------------------------|
G|-------2--------2-----------2--------2-----|
D|---2-3------2-3---3-2---2-3------2-3-------|
A|-3--------3-----------3--------3-----------|
E|-------------------------------------------|

Parte 2

E|-------------------------------------------|
B|-------------------------------------------|
G|-------------------------------------------|
D|-------3--------3----------3-----3---------|
A|---3-4------3-4---4-2----2-----2-----------|
E|-4--------4-----------3------3-------------|

Refrão:


           C7M     Dm7     Em7
Pra conversar, brigar, beijar
        A7(b13)   Dm7
Eu tô pronto pra tudo
Bbm6  A7(b13)   Dm7  G7(4/9)
     Não quero nada
 G7(9)   C7M  Db6(9)  F#7(4/9)  G7(4/9)
Sem solução

         C7M      Dm7   Em7
Viver é mais que bom demais
           A7(b13)   Dm7  Bbm6
Quando o amor tá incluso
A7(b13)   Dm7  G7(4/9)
É um    abuso
 G7(9)    C7M  Fm6
De perfeição

Primeira Parte:

Cm7          Ab7(13)
Eu te quero sã
           Fm7               Dm7(b5)   G7(b9)
Como uma flor no ardor do calor da manhã
  Cm7         Ab7(13)
Pra que lamentar
               Fm7        Dm7(b5)
O que a gente não fez passou

  G7(b9)       Cm7          Ab7(13)
Agora é outra vez pra se ligar
          Fm7              Dm7(b5)   G7(b9)
Pra discutir como reconstruir nossa paz
    Cm7          Ab7M           Fm7
E o ideal de um lar brilhar de novo
    G7(4/9)  G7(9)
Se não vira jazz

Refrão:

           C7M     Dm7     Em7
Pra conversar, brigar, beijar
        A7(b13)   Dm7
Eu tô pronto pra tudo
Bbm6  A7(b13)   Dm7  G7(4/9)
     Não quero nada
 G7(9)   C7M  Db6(9)  F#7(4/9)  G7(4/9)
Sem solução

         C7M      Dm7   Em7
Viver é mais que bom demais
           A7(b13)   Dm7  Bbm6
Quando o amor tá incluso
A7(b13)   Dm7  G7(4/9)
É um    abuso
 G7(9)   (Intro 1x)
De perfeição

Segunda Parte:

Cm7
A noite
Fm7                  Cm7
    A passear pela bruma
Fm7                 Dm7(b5)
    Verá em cada colina
    G7(b9)
O quanto eu te amo
 Cm7   Fm7                Cm7
Depois     a madrugada invade
Fm7                    Dm7(b5)
Um sonho em que tu me tinhas
  G7(4/9)   G7(9)
Amor de verdade

Refrão:

           C7M     Dm7     Em7
Pra conversar, brigar, beijar
        A7(b13)   Dm7
Eu tô pronto pra tudo
Bbm6  A7(b13)   Dm7  G7(4/9)
     Não quero nada
 G7(9)   C7M  Db6(9)  F#7(4/9)  G7(4/9)
Sem solução

         C7M      Dm7   Em7
Viver é mais que bom demais
           A7(b13)   Dm7   Bbm6
Quando o amor tá incluso
A7(b13)   Dm7  G7(4/9)
É um    abuso
 G7(9)    C7M
De perfeição

Segunda Parte:

Cm7
A noite
Fm7                  Cm7
    A passear pela bruma
Fm7                 Dm7(b5)
    Verá em cada colina
    G7(b9)
O quanto eu te amo
 Cm7   Fm7                Cm7
Depois     a madrugada invade
Fm7                    Dm7(b5)
Um sonho em que tu me tinhas
  G7(4/9)   G7(9)
Amor de verdade

Refrão:

           C7M     Dm7     Em7
Pra conversar, brigar, beijar
        A7(b13)   Dm7
Eu tô pronto pra tudo
Bbm6  A7(b13)   Dm7  G7(4/9)
     Não quero nada
 G7(9)   C7M  Db6(9)  F#7(4/9)  G7(4/9)
Sem solução

         C7M      Dm7   Em7
Viver é mais que bom demais
           A7(b13)   Dm7   Bbm6
Quando o amor tá incluso
A7(b13)   Dm7  G7(4/9)
É um    abuso
 G7(9)   (Parte 1 da Intro 2x)
De perfeição

----------------- Acordes -----------------
A7(b13) = X 0 X 0 2 1
Ab7(13) = 4 X 4 5 6 X
Ab7M = 4 X 5 5 4 X
Bbm6 = 6 X 5 6 6 X
C7M = X 3 2 0 0 X
Cm7 = X 3 5 3 4 3
Db6(9) = X 4 3 3 4 4
Dm7 = X 5 7 5 6 5
Dm7(b5) = X X 0 1 1 1
Em7 = 0 2 2 0 3 0
F#7(4/9) = 2 X 2 1 0 X
Fm6 = 1 X 0 1 1 X
Fm7 = 1 X 1 1 1 X
G7(4/9) = 3 X 3 2 1 X
G7(9) = 3 X 3 2 0 X
G7(b9) = 3 X 3 1 0 X
