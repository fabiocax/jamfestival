Djavan - Mal de Mim

Intro 4x: Am7  C7(9)  F7M  G7(4/9/13)

Bm7(b5)       E7(b13)      Am7    Am/G         F7(9/#11)
Eu     pensei          que fosse coisa para um dia       só
Bm7(b5)  E7(b13)             Bm7(b5) E7(b13)    Am7   Am/G
Ficar    de      mal de mim  Reagi          sou seu amigo
        B7/F#       F7M       E7(b13)   Bm7(b5)       E7(b13)
E digo: Como   vai? Você fica seria   e nem     sinal
   Am7    Am/G                F7(9/#11)    Bm7(b5)    E7(b13)
Brigou comigo e a solidão servira   de lugar  pra nós     dois

Bm7(b5)         E7(b13)      Am7   Am/G         B7/F#
Se      é  amor          que tal agir       não radicalizar?
F7M          E7(b13) Am7    C7(9)          F7M
Sejamos mais lisos  pega   esse meu ombro rega
       Bb7(9/#11)                      Am7
Se adormecer eu ei que o sono passo a perna
C7(9)            F7M            Bb7(9/#11)
Nessa distância férrea, que marcou...

Bm7(b5)      E7(b13)    Am7     Am/G         F7(9/#11)
Meu     amor         dormir contigo é escutar Gal       e Tom

Bm7(b5)           E7(b13)      Bm7(b5)  E7(b13)    Am7  Am/G
O      que rolar  é       bom,  passear         rever amigos
         B7/F#         F7M       E7(b13)     Am7         C7(9) / / /
Conduzir boas novas,   visitar a Grécia no futuro

----------------- Acordes -----------------
Am/G = 3 X 2 2 1 X
Am7 = X 0 2 0 1 0
B7/F# = X X 4 4 4 5
Bb7(9/#11) = X 1 0 1 1 0
Bm7(b5) = X 2 3 2 3 X
C7(9) = X 3 2 3 3 X
E7(b13) = 0 X 0 1 1 0
F7(9/#11) = 1 0 1 0 0 X
F7M = 1 X 2 2 1 X
G7(4/9/13) = 3 X 3 2 1 0
