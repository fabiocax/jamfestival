Djavan - Dona do Horizonte

D7M
Eu já nasci
Minha mãe quem diz
A7M
Predestinado ao canto

D7M
Ela falou
Que eu tinha o dom
A7M
Quando eu estava
Na soleira
Dos meus poucos anos!

D7M
Foi indo assim, quando dei por mim
A7M
Já não fazia outra coisa
D7M
Cantava ali, só pra ela ouvir
A7M
E me dizer coisas tão boas


Por exemplo:

D7M                Dm7
Quero vê-lo o mais querido
A7M
Como nosso orlando
D7M                 Dm7
Hei de ler seu nome escrito
A7M
Em placa de avenida

F#m7                Bm7
Não vai mudar, toda mãe é assim
D7M               Esus4
Mãe é o nome do amor!

D7M
Logo cresci
Minha mãe ali
A7M
Dona do horizonte

DM7
Me fez ouvir
Dalva de Oliveira
AM7
E Ângela Maria todo dia
Deusas que adorava

D7M
Tinha prazer
Em me levar pra ver
A7M
Luiz Gonzaga cantar

D7M
Não sem deixar
De advertir
AM7
Pra que eu estudasse sempre mais
E sem descanso

D7M               Dm7
Quero vê-lo o mais querido
A7M
Como nosso orlando

D7M                Dm7
Hei de ler seu nome escrito
A7M
Em placa de avenida

F#m7                 Bm7
Não vai mudar, toda mãe é assim
D7M              Esus4
Mãe é o nome do amor!

----------------- Acordes -----------------
A7M = X 0 2 1 2 0
Bm7 = X 2 4 2 3 2
D7M = X X 0 2 2 2
F#m7 = 2 X 2 2 2 X
