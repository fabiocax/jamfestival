Djavan - Nobreza

Am                       C           Em
  Nossa velha amizade nasceu
               F                   C7M        Am         E7
De uma luz   que acendeu  aos olhos de abril
Am                           C                 Em
  Com cuidado e espanto, eu te olhei
          C7M               E7
No entanto, você sorriu.
Am                            C           Em
  Concedendo-me a graça de ver
     F                 C7M    Am         E7
Talhada em você a nobreza de frente
Em                       A7      F#m                        F
O amor se desnudando,   no meio de tanta gente
Am                        C            Em
  Um doce descascado pra mim
      F                 C7M         Am         E7
Eu guardo pro fim, pra comer demorado
Am                        C            Em
  Uma grande amizade é assim
         C7M                 E7
Dois homens apaixonados

Am                   C         Em
   E sentir a alegria de ver
     F              C7M      Am          E7
A mão do prazer acenando pra gente
Em                               A7                    F
  O amor crescendo enfim como um capim
                   C
Pros meus dentes

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C7M = X 3 2 0 0 X
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
