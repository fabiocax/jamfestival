Djavan - Solitude

[Introdução] G6

[Primeira Parte]

          Am7
Amor em queda
            Cm6
Mesmo tal moeda
           G6
Perde cotação
          Am7
Um mundo louco
       Cm6
Evolui aos poucos
            G6
Pela contramão
          Am7
O erro invade
           Cm6
Tudo o que é cidade
              G6
Cai na imensidão


[Segunda Parte]

  G6
Guerra vende armas

Mantém cargos

Destrói sonhos
             C7M
Tudo de uma vez
      Am7
Sensatez
         G6
Não tem vez

Vidas fardos

Meros dados

Incontáveis casos
        C7M
De desamor
        Am7
Quanta dor
       G6
Muita dor

[Terceira Parte]

        Am7
Parece tarde
           Cm6
Falar de amizade
               G6
Ver com o coração
         Am7
E desse jeito
       Cm6
Reparar    defeito
              G6
Estendendo a mão

[Segunda Parte]

  G6
Guerra vende armas

Mantém cargos

Destrói sonhos
             C7M
Tudo de uma vez
      Am7
Sensatez
         G6
Não tem vez

Vidas fardos

Meros dados

Incontáveis casos
        C7M
De desamor
        Am7
Quanta dor
       G6
Muita dor

[Quarta Parte]

            Am7
Quem é que sabe
           Cm6
O quanto lhe cabe
           G6
Dessa solitude?
            Am7
Por isso a hora
           Cm6
De fazer é agora
             G6
Tome uma atitude

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
C7M = X 3 2 0 0 X
Cm6 = X 3 X 2 4 3
G6 = 3 X 2 4 3 X
