Djavan - Amar É Tudo

Intro 2x: C7M(9)  G#7(11+)  C#7M(9)  Dm7(5-)  G7(5+)

 C7M(9)
Meu amor,eu nem sei te dizer quanta dor
         G#7(11+)         C#7M(9)
mesmo a noite      não sabia
                   Dm7(5-)   G7(5+)
o que o amor escondi_________a
 C7M(9)
Minha  vida, que fazer com minh'alma perdida
        G#7(11+)   C#7M(9)
foi um raio       de ilusão
                Dm7(5-) G7(5+)
bem no meu coração

Segunda Parte:
   C7M(9)         C7(9)       F7M       Bb7(9)
E veio   com tudo       dissabor e tudo
   C7M(9)         C7(9)       F7M        Bb7(9)
E veio   com tudo       dissabor e tudo
Cm7(9)        Bbm6                    G#7M  C#7(9)
       Eu sei       que eu não sei viver          sem ela

Cm7(9)       Bbm6                 G#7M
       Assim       um simples talvez
    Dm7(5-)    G7(5+)
me deses______pera

Terceira Parte:
Cm7(9)         Bbm6              G#7M
       Ninguém      pode querer bem
C#7(9)
       sem ralar
Cm7(9)        Bbm6               G#7M
       Não há      nada o que fazer
  Dm7(5-)   G7(5+)
amar    é  tudo

(Repete tudo)

   C7M(9)         C7(9)       F7M       Bb7(9)
E veio   com tudo       dissabor e tudo
   C7M(9)         C7(9)       F7M        Bb7(9)
E veio   com tudo       dissabor e tudo
Cm7(9)        Bbm6                    G#7M  C#7(9)
       Eu sei       que eu não sei viver          sem ela
Cm7(9)       Bbm6                 G#7M
       Assim       um simples talvez
    Dm7(5-)    G7(5+)
me deses______pera

Cm7(9)         Bbm6              G#7M G#7M(13)
       Ninguém      pode querer bem
C#7(9)
       sem ralar
Cm7(9)        Bbm6               G#7M G#7M(13)
       Não há      nada o que fazer
  Dm7(5-)   G7(5+)
amar    é  tudo

( C7M(9)  G#7(11+)  C#7M(9)  Dm7(5-)  G7(5+) )

----------------- Acordes -----------------
Bb7(9) = X 1 0 1 1 X
Bbm6 = 6 X 5 6 6 X
C#7(9) = X 4 3 4 4 X
C#7M(9) = X 4 3 5 4 X
C7(9) = X 3 2 3 3 X
C7M(9) = X 3 2 4 3 X
Cm7(9) = X 3 1 3 3 X
Dm7(5-) = X 5 6 5 6 X
F7M = 1 X 2 2 1 X
G#7(11+) = 3 X 3 4 2 X
G#7M = 4 X 5 5 4 X
G#7M(13) = 4 X 5 5 6 X
G7(5+) = 3 X 3 4 4 3
