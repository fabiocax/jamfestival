Djavan - Orquídea

[Intro] E7M  F7M

Dm6
Lembra aquela Phalaenopsis
       Bbm6
Que você me deu
Dm6
Me deixou com Sophronitis
           Bbm6
Por um beijo seu
Cm7   F7(9)     Bb7M Eb7(9)
Pleurothallis, Paphiopedilum
F7M/A  Abdim(b13)
Cores demais
Gm7    C7(9)
Nada comum
Dm6
Cyrtopodium, Sarcoglottis
          Bbm6
De um bem-querer
Dm6
Fui à Brassia na Malaxis

            Bbm6
Mas não vi você
Cm7 F7(9) Bb7M   Eb7(9)
Amabilis, Violácea-me aqui
F7M/A  Abdim(b13) Gm7 C7(9)
Javanica,                 Guttata-me
Gm7 C7(9) Am7  Abdim(b13)
Cattleya ou Purpurata
Gm7 C7(9)   F/A
Me remete a você
                          Abm6
Uma Brassavola nata
Gm7 C7(9) Am7 Abdim(b13)
Seu labelo bicolor
Gm7
Desacata
   C7(9)                F7M F6
Pela fragrância da cor
Gm7          C7(9)
Sépala, labelo, coluna
   F7M            Fdim
E pétala tem a flor
Gm7
Flor que nasce, cresce
C7(9)        F7M
E flora por amor

----------------- Acordes -----------------
Abdim(b13) = 4 X 3 4 5 X
Abm6 = 4 X 3 4 4 X
Am7 = X 0 2 0 1 0
Bb7M = X 1 3 2 3 1
Bbm6 = 6 X 5 6 6 X
C7(9) = X 3 2 3 3 X
Cm7 = X 3 5 3 4 3
Dm6 = X 5 X 4 6 5
E7M = X X 2 4 4 4
Eb7(9) = X 6 5 6 6 X
F/A = 5 X 3 5 6 X
F6 = 1 X 0 2 1 X
F7(9) = X X 3 2 4 3
F7M = 1 X 2 2 1 X
F7M/A = 5 X 3 5 5 X
Fdim = X X 3 4 3 4
Gm7 = 3 X 3 3 3 X
