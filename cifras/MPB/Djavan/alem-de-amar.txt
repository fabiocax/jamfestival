Djavan - Além de Amar

de: Djavan

Intro.: C7M / / / /


C7M*
     Quero esquecer é como nódoa e não sai
B7(#5)
       Nem pra fugir, sair já não me distrai
Bb7M                          A7(b13)
     Como extinguir as sobras de     uma paixão
Dm7            Em7          Fm6 (omit5)
    Retidas no mais secreto vão

C7M*
     Tanto a dizer, mas tais palavras não saem
B7(#5)
       Donas de si pra não me verem ruir me traem
Bb7M                       A7(b13)
     Eu me perdi e não sei como   explicar
Dm7                  Em7          Fm6 (omit5)
    O que foi que eu fiz além de amar?


Eb7M(9)          Fm7        Gm7   Ab7
        Ou é desamor ou é intriga
Db7M(9)          G7(#5) C7M(9)           G7(#5) G7(13)
Seja   o que for,       já    nos desune

Eb7M(9)          Fm7             Gm7   Ab7
       Se é por amar que a gente briga
    Db7M(9)   G7(#5)   C7M(9) C7(9)
Bem pode   ser       ciúme

                 F#m7(b5)         Fm6
Mas tudo é pouco pra      tanto sofrer
Em7(9)  Eb7(9) D7(9)          Fm6         C7M*  G7(4/9)
Sem   razão         pra convencer meu coração

C7M* / / / / B7(#5) / / / / Bb7M / / A7(b13) / / Dm7 Em7 Fm6 (omit5) / /
C7M* / / / / B7(#5) / / / / Bb7M / / A7(b13) / / Dm7 Em7 Fm6 (omit5) / /

Eb7M(9)          Fm7        Gm7   Ab7
        Ou é desamor ou é intriga
Db7M(9)          G7(#5) C7M(9)           G7(#5) G7(13)
Seja   o que for,       já    nos desune

Eb7M(9)          Fm7             Gm7   Ab7
       Se é por amar que a gente briga
    Db7M(9)   G7(#5)   C7M(9) C7(9)
Bem pode   ser       ciúme

                 F#m7(b5)          Fm6
Mas tudo é pouco pra     tanto sofrer
Em7(9)  Eb7(9) D7(9)          Fm6         C7M*
Sem   razão         pra convencer meu coração


    C7M       C7M*     B7(#5)     Bb7M    A7(b13)     Dm7       Em7    Fm6 (omit5)

   ||||||    ||||||    |||||| 6ª 1|||2| 5ª 1|2||| 5ª |----- 7ª |----- 7ª |||1||
   ||1|||    ||||||    |-----    ||34||    |||34|    ||||2|    ||||2|    |2||||
   |2||||    |-----    |||||2    ||||||    ||||||    ||3|||    ||3|||    ||||3|
   ||||||    |||2||    ||||3|    ||||||    ||||||    ||||||    ||||||    |||||4
   ||||||    ||3|4|    ||||||    ||||||    ||||||    ||||||    ||||||    ||||||
    O ooo     O ooo     O ooo    O ooo     O ooo      Oooo      Oooo      O ooo


    Eb7M(9)    Fm7       Gm7       Ab7      Db7M(9)   G7(#5)    C7M(9)    G7(13)

 5ª ||1|||    1|234|    ||||||    ||||||    ||||||    ||||||    ||||||    ||||||
    |2||3|    ||||||    ||||||    ||||||    ||||||    ||||||    ||1|||    ||||||
    |||4||    ||||||    1|234|    ||||||    ||1|||    1|2|||    |2||3|    1|2|||
    ||||||    ||||||    ||||||    1|2|3|    |2||3|    |||34|    |||4||    |||3||
    ||||||    ||||||    ||||||    |||4||    |||4||    ||||||    ||||||    ||||4|
     Oooo     O ooo     O ooo     O ooo      Oooo     O ooo      Oooo     O ooo


          C7(9)   F#m7(b5)    Fm6      Em7(9)    Eb7(9)     D7(9)    G7(4/9)

         ||||||    ||||1|    2||34| 5ª ||1||| 5ª ||1|||    ||||||    ||||1|
         ||1|||    2|34||    ||||||    ||||||    |2|34|    ||||||    |||2||
         |2|34|    ||||||    ||||||    |2|34|    ||||||    ||||||    3|4|||
         ||||||    ||||||    ||||||    ||||||    ||||||    ||1|||    ||||||
         ||||||    ||||||    ||||||    ||||||    ||||||    |2|34|    ||||||


----------------- Acordes -----------------
A7(b13) = X 0 X 0 2 1
Ab7 = 4 6 4 5 4 4
B7(#5) = X 2 X 2 4 3
Bb7M = X 1 3 2 3 1
C7(9) = X 3 2 3 3 X
C7M = X 3 2 0 0 X
C7M(9) = X 3 2 4 3 X
D7(9) = X 5 4 5 5 X
Db7M(9) = X 4 3 5 4 X
Dm7 = X 5 7 5 6 5
Eb7(9) = X 6 5 6 6 X
Eb7M(9) = X 6 5 7 6 X
Em7 = 0 2 2 0 3 0
Em7(9) = X 7 5 7 7 X
F#m7(b5) = 2 X 2 2 1 X
Fm6 = 1 X 0 1 1 X
Fm7 = 1 X 1 1 1 X
G7(#5) = 3 X 3 4 4 3
G7(13) = 3 X 3 4 5 X
G7(4/9) = 3 X 3 2 1 X
Gm7 = 3 X 3 3 3 X
