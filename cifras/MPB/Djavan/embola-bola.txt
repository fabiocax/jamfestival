Djavan - Embola Bola

(intro) B7/13 Em7/9 A7/5+ D6/9 B7/13 Em7/9 A7/5+ D6/9

B7/13               Em7/9          A7/5+                     D6/9
Embola a bola embora sem rebolar, Embola a bola embora sem rebolar,
B7/13               Em7/9          A7/5+                     D6/9
Embola a bola embora sem rebolar, Embola a bola embora sem rebolar.

B7/13      Em7/9 A7/5+          D6/9        B7/13           Em7/9
Passo nessa vida como passo na avenida entre nobres e caras-de-pau
    A7/5+          Am7   D7/9   G#7/5-                 G7+
Sufocados no mesmo ideal     mas, à mercê de um falso alento
      C7/9    F7+      Am7 D7/9      G7+
Eu me deito esperando sorrindo ou xingando o que jamais
A7/4/9 F#m7/5-  B7/9-            E7/9    A7/5+ D6/9
Teceu-me consideração dança que marcou   eu     não

----------------- Acordes -----------------
A7/4/9 = 5 X 5 4 3 X
A7/5+ = X 0 X 0 2 1
Am7 = X 0 2 0 1 0
B7/13 = X 2 X 2 4 4
B7/9- = X 2 1 2 1 X
C7/9 = X 3 2 3 3 X
D6/9 = X 5 4 2 0 0
D7/9 = X 5 4 5 5 X
E7/9 = X X 2 1 3 2
Em7/9 = X 7 5 7 7 X
F#m7/5- = 2 X 2 2 1 X
F7+ = 1 X 2 2 1 X
G#7/5- = 4 X 4 5 3 X
G7+ = 3 X 4 4 3 X
