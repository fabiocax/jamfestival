Djavan - Linha do Equador

Intro: E7M F#m7 E7M F#m7 E7M F#m7 E7M F#m7

Primeira Parte:
 E7M                          F#m7
Luz das estrelas laço do infinito
             G#m7      F#m7
Gosto tanto dela assim
 E7M                       F#m7
Rosa amarela voz de todo grito
             G#m7      Gm7(13)
Gosto tanto dela assim

Segunda Parte:
F#m7                    E7M
     Esse imenso, desmedido amor
      G°
Vai além de seja o que for
F#m7                           E7M
     Vai além de onde eu vou, do que sou,
            G°
Minha dor, minha linha do equador

F#m7                    E7M
     Esse imenso, desmedido amor
      G°
Vai além de seja o que for
F#m7
     Passa mais além do

Primeira Parte com variação na letra:
 E7M
Céu de brasília
               F#m7
Traço do arquiteto
             G#m7      F#m7
Gosto tanto dela assim
 E7M                       F#m7
Gosto de filha música de preto
             G#m7      Gm7(13)
Gosto tanto dela assim

Segunda Parte com variação na letra:
F#m7                 E7M
     Essa desmesura de paixão
      G°
É loucura do coração
F#m7                      E7M
     Minha Foz do Iguaçu Pólo Sul, meu azul
 G°
Luz do sentimento nu
F#m7                    E7M
     Esse imenso, desmedido amor
      G°
Vai além de seja o que for
F#m7                           E7M
     Vai além de onde eu vou, do que sou,
            G°                 G#m7
Minha dor, minha linha do equador

Terceira Parte:
       C#m7              A7M
Mas é doce morrer nesse mar
        Am6   Am7M(9)       Am6  G#m7
De lembrar e nunca esquecer
       C#m7                  A7M
Se eu tivesse mais alma pra dar
      Am6  Am7M(9)   Am6
Eu daria,  isso pra mim é viver

E7M  F#m7  G#m7  F#m7 (2x)

Primeira Parte com variação na letra:
 E7M
Céu de brasília
               F#m7
Traço do arquiteto
             G#m7      F#m7
Gosto tanto dela assim
 E7M                       F#m7
Gosto de filha música de preto
             G#m7      Gm7(13)
Gosto tanto dela assim

Segunda Parte com variação na letra:
F#m7                 E7M
     Essa desmesura de paixão
      G°
É loucura do coração
F#m7                      E7M
     Minha Foz do Iguaçu Pólo Sul, meu azul
 G°
Luz do sentimento nu
F#m7                    E7M
     Esse imenso, desmedido amor
      G°
Vai além de seja o que for
F#m7                           E7M
     Vai além de onde eu vou, do que sou,
            G°                 G#m7
Minha dor, minha linha do equador

Terceira Parte:
       C#m7              A7M
Mas é doce morrer nesse mar
        Am6   Am7M(9)       Am6  G#m7
De lembrar e nunca esquecer
       C#m7                  A7M
Se eu tivesse mais alma pra dar
      Am6  Am7M(9)   Am6
Eu daria,  isso pra mim é viver

E7M  F#m7  G#m7  F#m7...

----------------- Acordes -----------------
A7M = X 0 2 1 2 0
Am6 = 5 X 4 5 5 X
Am7M(9) = 5 3 6 4 X X
C#m7 = X 4 6 4 5 4
E7M = X X 2 4 4 4
F#m7 = 2 X 2 2 2 X
G#m7 = 4 X 4 4 4 X
Gm7(13) = X 10 X 10 11 12
G° = 3 X 2 3 2 X
