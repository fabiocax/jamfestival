Djavan - Fly Me To The Moon

(intro) G6/9  Fm6  G6/9  Fm6  G#m6 G#°

Am7           Dm7
Fly Me to the Moon,
           G74(9) G7(9)    C7M  C7(#9)
And let me play among the stars.
F7M               Dm7             Bm7(b5) E7(b9) Am7  A7(#5)
 Let me see what spring is like on Jupiter and Mars.
     Dm7      G74(9) G7(9)     Em7  A7(#5) A7
In other words,       hold my hand!
     Dm7(b5)  Ab7(#11) G7(9)   C7M  F7(#11) Bm7(b5) E7(#9)
In other words,       darling, kiss me.

Am7                Dm7
Fill my heart with song,
           G74(9) G7(9)   C7M  C7(#9)
and let me sing forever more.
F7M            Dm7             Bm7(b5) E7(b9) Am7  A7(#5)
You are all I long for, all I worship and adore.
     Dm7      G74(9) G7(9)   Em7(b5)  A7(#5)
In other words,     please be true!

     Dm7(b5)  G74(9) G7(9) G6/9  Fm6
In other words,    I love you!

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7(#5) = X 0 X 0 2 1
Ab7(#11) = 4 X 4 5 3 X
Am7 = X 0 2 0 1 0
Bm7(b5) = X 2 3 2 3 X
C7(#9) = X 3 2 3 4 X
C7M = X 3 2 0 0 X
Dm7 = X 5 7 5 6 5
Dm7(b5) = X X 0 1 1 1
E7(#9) = X 7 6 7 8 X
E7(b9) = X X 2 1 3 1
Em7 = 0 2 2 0 3 0
Em7(b5) = X X 2 3 3 3
F7(#11) = 1 X 1 2 0 X
F7M = 1 X 2 2 1 X
Fm6 = 1 X 0 1 1 X
G#m6 = 4 X 3 4 4 X
G#° = 4 X 3 4 3 X
G6/9 = X X 5 4 5 5
G7(9) = 3 X 3 2 0 X
G74(9) = 3 X 3 2 1 X
