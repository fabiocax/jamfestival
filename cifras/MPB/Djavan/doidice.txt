Djavan - Doidice

Intro 3x:  Gm7  C7(9)  Gm7  Am7  D7(b9)

Gm7       C7(9)                         Gm7
É natural,      vendaval que passa aqui
                  Am7        D7(9)   Gm7      A7(b13)
Mais doidice ali      Ou uma seca que arrasou
                 Dm7(9)          Am7     D7(b9)
Pior é não te ver       agora,  aflora   vícios

Gm7           C7(9)                               Gm7
Claras manhãs,      ou tanto mais que eu possa ter
                Am7          D7(9)     Gm7   A7(b13)
Nada quer dizer     se o teu beijo não é meu
D7(9)                                           G7(13)
Cio chegando, calor explodindo, temores rondando o ar
 Gm7  C7(9)     Dm7
E eu pensando em ti

Bb7M               A7(b13)
      Me apaixonei ?
F7(9)       B7(b5)
Talvez,      pode ser

Bb7M             A7(b13)
      Enlouqueci ?
F7(9)   Cm7         F7(9)
Não sei,     nunca vi
Bb7M            A7(b13)
      Preciso sair
Am7(b5)                D7(b9)        Gm7
        Depois que eu descobri  que  há   você
C7(9)                       Gm7    Am7  D7(b9)
      Nunca mais existi...

( Gm7   C7(9)   Gm7  Am7  D7(b9) )  2 vezes

Gm7           C7(9)               Gm7    Am7  D7(b9)
   Cuanto más       me olvidas te amo más
Gm7      C7(9)
Estrella,     sin tu amor
   Gm7              Am7               D7(b9)
no sé, no sé, no sé, no sé , no sé, no sé

Gm7           C7(9)               Gm7    Am7  D7(b9)
   Cuanto más       me olvidas te amo más
Gm7      C7(9)
Estrella,     sin tu amor
   Gm7              Am7               D7(b9)
no sé, no sé, no sé, no sé , no sé, no sé...

----------------- Acordes -----------------
A7(b13) = X 0 X 0 2 1
Am7 = X 0 2 0 1 0
Am7(b5) = 5 X 5 5 4 X
B7(b5) = X 2 3 2 4 X
Bb7M = X 1 3 2 3 1
C7(9) = X 3 2 3 3 X
Cm7 = X 3 5 3 4 3
D7(9) = X 5 4 5 5 X
D7(b9) = X 5 4 5 4 X
Dm7 = X 5 7 5 6 5
Dm7(9) = X 5 3 5 5 X
F7(9) = X X 3 2 4 3
G7(13) = 3 X 3 4 5 X
Gm7 = 3 X 3 3 3 X
