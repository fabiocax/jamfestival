Djavan - Irmã de Neon

(intro) E7M  F7M(13)/C

C#m7           C#m(7M)
Tudo pra subir tudo pra vencer
C#m7                A7M
Tudo pra que eu possa  ser seu na vida
F#m7       B7(4/9)            E7M    F7M(13)/C
Encontrar uma      saída me envolver
C#m7           C#m(7M)
Quando decidir faça-me um sinal
 C#m7                       G#m7
E eu estarei apto pra ser teu   e pronto
F#m7        B7(4/9)             E7M   G#7(b13)
Faça de mim seu     quintal seu lazer

C#m7             C#m(7M)              C#m7          C#m6
Entre beijos divinais,   uma mordida aqui outra ali
F#m7         B7(4/9)          E7M
Cresce nos matagais  o meu querer por ti
C#m7              C#m(7M)                C#m7         C#m6
Perfumada flor-de-lã      sua aragem me apraz e seduz

F#m7         B7(4/9)               E7M        G#7(b13)
     Cabelos quantos pelos  tantos ui, uis...

C#m7           C#m(7M)                 C#m7           C#m6
Monumento colossal     deusa com véu a tal a que diz
F#m7          B7(4/9)        E7M        G#7(b13)
Como e porquê merece   ser a mais feliz
C#m7          C#m(7M)               C#m7         C#m6
Eva irmã de néon      filha de Barcelona e Gaudi
                   F#m7     G#m7       A7M    D7(9)
Tu tens asas pra voar   mas eu  te esperarei aqui

Em7       C7     F#m7(b5)       B7
Pra te mostrar o que      é bom
Em7       C7       F#m7(b5)     B7
E   te contar como eu       sou
Em7     C7     F#m7(b5)
Te  ensinar coisas      da vida
B7      Em7     C     F#m7(b5)  B7
Ser seu guia ou o que for

     Em7    A7    Dm7
Sair pra dançar contigo
G7        C7M  C7   F#m7(b5)   B7(b9)
Ser teu  amigo teu amor

Em7    C7     F#m7(b5)       B7
Te  mostrar o que      é bom
Em7       C7       F#m7(b5)     B7
E   te contar como eu       sou
Em7     C7     F#m7(b5)
Te  ensinar coisas      da vida
B7      Em7     C     F#m7(b5)  B7
Ser seu guia ou o que for

     Em7    A7    Dm7
Sair pra dançar contigo
G7        C7M  C7   F#m7(b5)   B7(b9)
Ser teu  amigo teu amor
     Em7    A7    Dm7
Sair pra dançar contigo
G7        C7M  C7   F#m7(b5)   B7(b9)
Ser teu  amigo teu amor
     Em7    A7    Dm7
Sair pra dançar contigo
G7        C7M  C7   F#m7(b5)   B7(b9)
Ser teu  amigo teu amor

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7M = X 0 2 1 2 0
B7 = X 2 1 2 0 2
B7(4/9) = X 2 2 2 2 2
B7(b9) = X 2 1 2 1 X
C = X 3 2 0 1 0
C#m(7M) = X 4 6 5 5 4
C#m6 = X 4 X 3 5 4
C#m7 = X 4 6 4 5 4
C7 = X 3 2 3 1 X
C7M = X 3 2 0 0 X
D7(9) = X 5 4 5 5 X
Dm7 = X 5 7 5 6 5
E7M = X X 2 4 4 4
Em7 = 0 2 2 0 3 0
F#m7 = 2 X 2 2 2 X
F#m7(b5) = 2 X 2 2 1 X
F7M(13)/C = 8 8 X 9 P10 10
G#7(b13) = 4 X 4 5 5 4
G#m7 = 4 X 4 4 4 X
G7 = 3 5 3 4 3 3
