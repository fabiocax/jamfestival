Djavan - Farinha

de: Djavan

Intro.: ( Dm7 C7 Bb7M A7  Dm7 Gm7 C7(9) F7M  A7 ) 2X


e|---------5-10-8------6-----5---|
B|-6-6---6-----------8-----6-----|
G|-----7-----------9-----7-------|

e|---------5-10-8-----6------5---|
B|-6-6---6-----------8-----6---6-|
G|-----7-----------9-----7-------|


            Dm7                      Am7
A farinha é feita de uma planta da família
           D7(9)         G7(9)
Das euforbiáceas, euforbiáceas
   Gm/F            Em7(b5)
De nome manihot utilíssima
           Gm/F                Em7(b5)
Que um tio meu apelidou de macaxeira

             C7(9)              F7M      A7 A7(4) 10 A7
E foi aí que todo mundo achou melhor!...

Dm7                                 Am7
    A farinha tá no sangue do nordestino
                  D7(9)               G7(9)
Eu já sei desde menino o que ela pode dar
  C7(9)                       Gm7
E tem  da grossa, tem da fina se  não tem da quebradinha
C7(9)             F7M
Vou  na vizinha pegar


            A7        Dm7
Pra fazer pirão ou mingau
               A7       Dm7
Farinha com feijão é animal!
                F7/C         Bb7M
O cabra que não tem eira nem beira
                   D7                   Gm7
Lá no fundo do quintal tem um pé de macaxeira

  C7(9)           F7M                 A7                Dm7
A macaxeira é popular é macaxeira pr`ali, macaxeira pra cá
                    Bb7M              A7
E em tudo que é farinhada a macaxeira tá


F7M                           Gm7
Você não sabe o que é farinha boa
                  C7(9)                   F7M
Farinha é a que a mãe   me manda lá de Alagoas!

                              Gm7
Você não sabe o que é farinha boa
                  C7(9)                   F7M
Farinha é a que a mãe   me manda lá de Alagoas!

                              Gm7
Você não sabe o que é farinha boa
                  C7(9)                   F7M
Farinha é a que a mãe   me manda lá de Alagoas!

                              Gm7
Você não sabe o que é farinha boa
                  C7(9)                   F7M   A7
Farinha é a que a mãe   me manda lá de Alagoas!

              INTRODUÇÃO:


     Dm7        C7       Bb7M       A7        Gm7      C7(9)     F7M       Am7

 5ª |<----    ||||1|    |<----    ||||||    ||||||    ||||||    1|||2|    ||||||
    ||||2|    ||2|||    |||2||    ||2|3|    ||||||    ||1|||    ||34||    ||||||
    ||3|||    |3|4||    ||3|4|    ||||||    1|234|    |2|34|    ||||||    ||||||
    ||||||    ||||||    ||||||    ||||||    ||||||    ||||||    ||||||    ||||||
    ||||||    ||||||    ||||||    ||||||    ||||||    ||||||    ||||||    1|234|
     O ooo     Oooo      O ooo     O ooo    O ooo      Oooo     O ooo     O ooo


         D7(9)     G7(9)    Em7(b5)    Gm/F      A7(4)     F7/C       D7

        ||||||    ||||||    ||||||    ||||||    |||||| 6ª ||||1|    ||||1|
        ||||||    |||1||    ||1|||    ||||||    ||2|||    ||2|||    |||2|3
        ||||||    2|3|4|    |||234    ||<---    ||||3|    3||4||    ||||||
        ||1|||    ||||||    ||||||    ||||||    ||||||    ||||||    ||||||
        |2|34|    ||||||    ||||||    ||||||    ||||||    ||||||    ||||||


----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7(4) = X 0 2 0 3 0
Am7 = X 0 2 0 1 0
Bb7M = X 1 3 2 3 1
C7 = X 3 2 3 1 X
C7(9) = X 3 2 3 3 X
D7 = X X 0 2 1 2
D7(9) = X 5 4 5 5 X
Dm7 = X 5 7 5 6 5
Em7(b5) = X X 2 3 3 3
F7/C = X 3 3 2 4 X
F7M = 1 X 2 2 1 X
G7(9) = 3 X 3 2 0 X
Gm/F = X X 3 3 3 3
Gm7 = 3 X 3 3 3 X
