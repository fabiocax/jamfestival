Djavan - Estátua de Sal

D7(9)                        E7(9)
que você não me faça jus
A7M(9)
 a gente até releva
D7(9)                               E7(9)
 mas você bem que tem um plus
A7M(9)
 isso ninguém pode negar
D7(9)                                E7(9)
 e são tantos momentos bons
Bm7(11)              Esus(#4)
 apesar dos senões
Bm7(11)              E7(9)
 você sabe a hora certa
A7M(9)                    D7M(6add)
 de trocar a pele da vilã
Bm7(11)                         E7(9)
 por alguém de mente aberta
A7M(9)                    D7M(6add)
 de quem quase sempre sou fã
Am7                          D7(9)
 você tem uma coisa boa

G7M                        Gm6
que às vezes soa estranho
F7M                         Bbsus     A9     A7/4
 e um esplendor  encontrado em cada nota do soul, soul
Em7/A                                 A6(9)
 ou no mar quando o sol ascender
A9                    A7/4 6add
 deusa do amor ou
Em7/A                        A6(9)
 estátua de sal qual será você
A9                            A7/4 6add
 que vai dizer quem sou
Em7/A                           A6(9)
 pra que eu não vá errar e me perder?
  I

----------------- Acordes -----------------
A6(9) = 5 4 4 4 X X
A7/4 = X 0 2 0 3 0
A7M(9) = X 0 2 1 0 0
A9 = X 0 2 2 0 0
Am7 = X 0 2 0 1 0
Bbsus = X 1 3 3 3 1
Bm7(11) = 7 X 7 7 5 X
D7(9) = X 5 4 5 5 X
E7(9) = X X 2 1 3 2
Em7/A = X 0 0 0 0 0
F7M = 1 X 2 2 1 X
G7M = 3 X 4 4 3 X
Gm6 = 3 X 2 3 3 X
