Djavan - Brilho da Noite

de: Djavan

Intro.: D7M(9)  F°  Emadd9    A(b9)


      D7M(9)      F°    Emadd9
Que será     que existe
            A(b9)        D7M(9)      F°     Emadd9   A(b9)
Além desse olhar, quanto há     de segredo?

        D7M(9)    F°    Emadd9                A(b9)
E entre Mozart  e Liszt          por quem optará
  D7M(9)      F°            Emadd9   A(b9)
Encara   as alturas ou a placidez?

     D7M(9)    F°   Emadd9          A(b9)
E em caso   de amor          virá tateando
    D7M(9)          F°      Emadd9   A(b9)
Ou irá     por onde eu for?

    D7M(9)    F°     Emadd9            C7(9)
Matinal    ou vampira,       afeita ao pecado?


Gm7                 D7M(9)*
   Seja agulhada ou zen
Gm7              D7M(9)*
   Sou mais você e     mais ninguém
G#m7(b5)         C#7(b9)      F#7M      G°
        Pra me tirar    desse caos onde estou
Bm7        F7(9) Em7       A(b9)
Doido pra amar,  louco de amor!


       D7M(9)     F°
Quando quero  ficar feliz
Emadd9              A(b9)
        Começo a pensar
     D7M(9)    F°      Emadd9   A(b9)
Em você     me lambendo

         D7M(9)    F°
Me vejo atuar   em seu filme
Emadd9        A(b9)
        E bem enquadrado
Am7(9)           D7(9) Gm7        C7(9)
      Posso conferir       sua perfeição:
F7M         A#7(b9)    Em7(9)  A7(13)
Que mal pro meu    coração...


          D7M(9) F° Emadd9   A(b9)
Brilho da noite
         D7M(9) F° Emadd9   A(b9)
Causa perdida
        D7M(9) F° Emadd9   A(b9)
Meu horizonte
        D7M(9) F° Emadd9   A(b9)
Tudo na vida


  D7M(9)       F°      Emadd9      A(b9)      C7(9)       Gm7       D7M(9)*

  ||||||     1||2||     ||||||     ||||||     ||||||     ||||||  4ª ||1|||
  |||23|     ||||||     ||||||     ||1|2|     ||1|||     ||||||     |2||3|
  ||||||     ||||||     ||||||     |||3||     |2|34|     1|234|     |||4||
  ||||||     ||||||     ||3|||     ||||||     ||||||     ||||||     ||||||
  ||||||     ||||||     ||||||     ||||||     ||||||     ||||||     ||||||
    Oooo     O ooo      O ooo       Oooo       Oooo      O ooo       Oooo


 G#m7(b5)    C#7(b9)     F#7M        G°        Bm7        F7(9)      Em7

  ||||||     ||||||     ||||||     ||||||     ||||||     ||||||     ||||||
  ||||||     ||||||     1|||2|     ||1|2|     |1|2|3     |||1||     |12|||
  ||||1|     ||1|2|     ||34||     3||4||     ||||4|     ||2||3     ||||4|
  2|34||     |3|4||     ||||||     ||||||     ||||||     ||||4|     ||||||
  ||||||     ||||||     ||||||     ||||||     ||||||     ||||||     ||||||
  O ooo       Oooo      O ooo      O ooo       O ooo       Oooo     O ooo


      Am7(9)      D7(9)      F7M       A#7(b9)    Em7(9)     A7(13)

      ||||||     ||||||     1|||2|     |2|3||     ||||||     ||||||
      ||||||     ||||||     ||34||     ||||||     |||||2     ||2|34
      ||||||     ||||||     ||||||     ||||||     ||||3|     ||||||
      ||||||     ||1|||     ||||||     ||||||     ||||||     ||||||
      ||23||     |2|34|     ||||||     ||||||     ||||||     ||||||


----------------- Acordes -----------------
A#7(b9) = 6 X 6 4 3 X
A(b9) = X 0 2 3 2 0
A7(13) = X 0 X 0 2 2
Am7(9) = X X 7 5 8 7
Bm7 = X 2 4 2 3 2
C#7(b9) = X 4 3 4 3 X
C7(9) = X 3 2 3 3 X
D7(9) = X 5 4 5 5 X
D7M(9) = X 5 4 6 5 X
Em7 = 0 2 2 0 3 0
Em7(9) = X 7 5 7 7 X
Emadd9 = 0 2 4 0 0 0
F#7M = 2 X 3 3 2 X
F7(9) = X X 3 2 4 3
F7M = 1 X 2 2 1 X
F° = X X 3 4 3 4
G#m7(b5) = 4 X 4 4 3 X
Gm7 = 3 X 3 3 3 X
G° = 3 X 2 3 2 X
