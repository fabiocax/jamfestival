Djavan - Lambada de Serpente

[Intro] Em  D  Em  D

   Em               F#m
Cuidar dum pé de milho
          Em                  D
Que demora na semente
        Em                   F#m
Meu pai disse meu filho
         Em                   D     D7
Noite fria, tempo quente

        G             F#m            Em      A7  D
Lambada de serpente a traição me enfei....tiçou
           G              F#m        Em       A7    D
Quem tem amor ausente já viveu a minha dor
        G             F#m            Em      A7  D
Lambada de serpente a traição me enfei....tiçou
           G              F#m        Em       A7    D
Quem tem amor ausente já viveu a minha dor

       Em                F#m
No chão da minha terra

         Em               D
Um lamento de corrente
       Em                F#m
Um grão de pé de guerra
        Em                     D    D7
Pra colher dente por dente

        G             F#m            Em      A7  D
Lambada de serpente a traição me enfei....tiçou
           G              F#m        Em       A7    D
Quem tem amor ausente já viveu a minha dor
        G             F#m            Em      A7  D
Lambada de serpente a traição me enfei....tiçou
           G              F#m        Em       A7    D
Quem tem amor ausente já viveu a minha dor

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
