Fafá de Belém - Que Me Venha Esse Homem


Bm7                  Em7/9
   Que me venha esse homem
A7/13               D7+
   Depois de alguma chuva
G7+                 C#m7/b5
   Que me prenda de tarde
F#7/5+         Bm7
Em sua teia de veludo
                   Em7
Que me fira com os olhos
F#7             Bm7
E me penetre em tudo
                  Em7/9
Que me venha esse homem
A7/13           D7+
   De músculos exatos
G7+               C#m7/b5
   Com um desejo agreste
F#7/5+          Bm7
Com um cheiro de mato

                 Em7
Que me prenda de noite
F#7            Bm7
Em sua rede de braços
                 Em7/9
Que me venha com força
A7/13              D7+
Com gosto de desbravar
               C#7
Que me faça de mata
                  F#m
Pra percorrer devagar
               Cº
Que me faça de rio
C#7                 F#m
Pra se deixar naufragar
                  Em7/9
Que me salve esse homem
A7/13               D7+
   Com sua febre de fogo
G7+                   C#m7/b5
   Que me prenda no espaço
F#7/5+           Bm7
De seu passo mais louco
                  Em7
Que me venha esse homem
F#7                Bm7
Que me arranque do sono
                  Em7
Que me venha esse homem
F#7                Bm7
Que me machuque um pouco

----------------- Acordes -----------------
A7/13 = X 0 X 0 2 2
Bm7 = X 2 4 2 3 2
C#7 = X 4 3 4 2 X
C#m7/b5 = X 4 5 4 5 X
Cº = X 3 4 2 4 2
D7+ = X X 0 2 2 2
Em7 = 0 2 2 0 3 0
Em7/9 = X 7 5 7 7 X
F#7 = 2 4 2 3 2 2
F#7/5+ = 2 X 2 3 3 2
F#m = 2 4 4 2 2 2
G7+ = 3 X 4 4 3 X
