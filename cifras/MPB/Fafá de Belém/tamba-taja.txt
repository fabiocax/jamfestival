Fafá de Belém - Tamba Tajá

E7+   F°
Tamba Tajá
F#m       F5-/7
Me faz feliz
E7+     F°
Que meu amor
        F#m7 B7
Seja só meu
G#m7    G°           F#m7
Que meu amor seja só meu
            F5-/7
De mais ninguém
           G#m7
Que seja meu
        G°
Todinho meu
            F#m7 F5-/7
De mais ninguém
E      C#7/9-
Tamba Tajá
F#m       B7
Me faz feliz

   G#m7   G°       F#m7
Verso em mim te carregou
                 B7
Se o meu amor rugiu
G#m7    G°
Pelo rosado
      F#m7         B7   G#m7
Para guerra, para morte
         G°             F#m7
Assim carregue o nosso amor
       B4/7  B7
À boa sorte
G#m7    G° F#m7 F5-/7
Tamba Tajá

ESTRIBILHO

G#m7        G°          F#m7
Que ninguém mais possa beijar
         B7
O que beijei
G#m7        G°           F#m7
Que ninguém mais possa afagar
         B7
O que afaguei
          G°
Nem posso olhar
           F#m7       B7/9-
Dentro dos anos que olhei
G#m7   G°  F#m7 F5-/7
Tamba Tajá

----------------- Acordes -----------------
B4/7 = X 2 4 2 5 2
B7 = X 2 1 2 0 2
B7/9- = X 2 1 2 1 X
C#7/9- = X 4 3 4 3 X
E = 0 2 2 1 0 0
E7+ = X X 2 4 4 4
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
F° = X X 3 4 3 4
G#m7 = 4 X 4 4 4 X
G° = 3 X 2 3 2 X
