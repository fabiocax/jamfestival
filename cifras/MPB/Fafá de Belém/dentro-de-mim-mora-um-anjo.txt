Fafá de Belém - Dentro de Mim Mora um Anjo

Intro: Em G6/7 C7+ D7/9 G7+ Em  B7/Eb Em/D C#m5-/7 Am

Em       G6/7       C7+   D7/9
Quem me vê assim cantando
          D7+     G7+
Não sabe nada de mim
                       Em7
Dentro de mim mora um anjo
Que tem a boca pintada
                    Bm5-/7 E7
Que tem as unhas pintadas
                 Am7    Am/G
Que tem as asas pintadas
                  C#m7/9
Que passa horas à fio
     F#5-/7       F#m7  B7
No espelho do toucador
  Em7      G6/7        C7+ D7/9
Dentro de mim mora um anjo
                  G7+
Que me sufoca de amor

                       Em
Dentro de mim mora um anjo
Montado sobre um cavalo
    A/C#            C7+
Que ele sangra de espora
                   C#m7/9
Ele é meu lado de dentro
    F#5-/7          F#m7  B7
Eu sou seu lado de fora
 Em7   G6/7         C7+   D7/9
Quem me vê assim cantando
                 G7+
Não sabe nada de mim
                       Em7
Dentro de mim mora um anjo
Que arrasta suas medalhas
               Bm5-/7 E7
E que batuca pandeiro
                        Am    Am/G
Que me prendeu em seus laços
                    C#m7/9
Mas que é meu prisioneiro
F#5-/7        F7+
Acho que é colombina
E7/9-           Eb7+
Acho que é bailarina
 F#m     B7    Em7
Acho que é brasileiro
 Em    B7/Eb  Em/D   C#m5-/7 Am7
Quem me vê assim cantando
          D7/9    G7+
Não sabe nada de mim

----------------- Acordes -----------------
A/C# = X 4 X 2 5 5
Am = X 0 2 2 1 0
Am/G = 3 X 2 2 1 X
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
B7/Eb = X X 1 2 0 2
Bm5-/7 = X 2 3 2 3 X
C#m5-/7 = X 4 5 4 5 X
C#m7/9 = X 4 2 4 4 X
C7+ = X 3 2 0 0 X
D7+ = X X 0 2 2 2
D7/9 = X 5 4 5 5 X
E7 = 0 2 2 1 3 0
E7/9- = X X 2 1 3 1
Eb7+ = X X 1 3 3 3
Em = 0 2 2 0 0 0
Em/D = X X 0 4 5 3
Em7 = 0 2 2 0 3 0
F#5-/7 = 2 X 2 3 1 X
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
F7+ = 1 X 2 2 1 X
G7+ = 3 X 4 4 3 X
