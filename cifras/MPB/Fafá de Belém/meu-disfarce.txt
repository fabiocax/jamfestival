Fafá de Belém - Meu Disfarce

(intro) Em A D7M Bm7 Em F#7 G F#7

   Bm7                      F#7
Você vê esse meu jeito de pessoa liberada
        B7                     Em
Mas não sabe que por dentro não é isso, não sou nada
                F#7            Bm     Bm7
Tenho ares de serpente mas em casos de amor
      C#7                        F#7
Sou pequeno sou carente sou mais frágil que uma flor
       Bm7                      F#7
Eu me pinto e me disfarço companheiro do perigo
        B7                    Em
Eu me solto em sua festa mas sozinho eu não consigo
       Em/C#          F#7         Bm             Bm7
Digo coisas que eu não faço, faço coisas que eu não digo
        C#7                     F#7
Eu te quero meu amado, não somente seu amigo
     Em          A7                       D7M
Cada vez que eu sinto um beijo seu na minha face
     Bm7                    Em       F#7                       Bm7
Eu luto pra manter o meu disfarce e não deixar tão claro que te quero

B7     Em     A7                       D7M     Bm7                         Em
Cada vez se torna mais difícil o meu teatro, não dá mais pra fugir do seu contato
  F#7                  G   F#7
Estou apaixonado por você   (primeira vez)
                       Bm
Estou apaixonado por você   (segunda vez)

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
C#7 = X 4 3 4 2 X
D7M = X X 0 2 2 2
Em = 0 2 2 0 0 0
Em/C# = X 4 2 0 0 X
F#7 = 2 4 2 3 2 2
G = 3 2 0 0 0 3
