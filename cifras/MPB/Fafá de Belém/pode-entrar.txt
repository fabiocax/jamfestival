Fafá de Belém - Pode Entrar

   Dm7        G7           C
A casa escancarada a lua ali
       Am           Bm5-
Meu cachorro nunca morde
      E7           Am
Meu quintal tem sapotí
           C/G            Dm7
tem um roseiral crescendo lindo
          F       F#º   C/G
Quem for louco ou for poeta
      Dm7    G7       C
Pode entrar seja bem vindo


 Dm7          G7        C
Aqui passa o bonde da lapinha
        Am         Bm5-
Passa a filha da rainha
          E7      Am
Passa um disco voador
             C/G            Dm7
Às vezes ele gira, pára e pisca

     F       F#º    C/G
Como quem quase se arrisca
    Dm7     G7     C
A parar pra conversar


    Dm7          G7             C
Mas não me sinto só, tenho um vizinho
          Am      Bm5-
Que é um bêbado velhinho
        E7         Am
Que acredita no destino
            C/G        Dm7
Ele mora em cima do arvoredo
     F    F#º     C/G
Ele tem muitos brinquedos
    Dm7    G7    C
Ele sempre foi menino


 Dm7       G7           C
Agora se vocês me dão licença
       Am          Bm5-
Eu vou ver um passarinho
       E7         Am
Que me chama no quintal
                C/G         Dm7
Depois eu vou deitar para sonhar
     F    F#º   C/G
E dançar com a cigana
          Dm7   G7    C
Que eu perdi no carnaval

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C/G = 3 3 2 X 1 X
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
F#º = 2 X 1 2 1 X
G7 = 3 5 3 4 3 3
