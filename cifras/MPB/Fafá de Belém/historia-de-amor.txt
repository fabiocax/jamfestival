Fafá de Belém - História de Amor

1ª Parte A:
       G                                            Bm
Por querer amar demais
      Am                           G
Passei do meu prazer
       G                                           Bm
Eu queria muito mais
        Am          D     G         Em
Do que podia ter

                              Am
Pra viver um sonho
 D                   Bm
Pra fica a sós
 Em                        Am
Pra correr por todo lado
      D                          Bm
E só pensar em nós

 Em                    Am
Pra dizer te amo

             D                Bm
Sem ter que falar
Em                    Am
E viver um ano
                      A                               D
E um ano nem sequer passar

2ª Parte :
               C                   D
Quase tudo maravilha
                Bm               Em
Quase tudo coração
      Am                           D
Fiquei como uma ilha
            G
No meio da paixão

               C                      D
Você fez o que sabia
   Bm                     Em
Até chegar o fim
           Am               D
Não era um amor
                                G
Tão grande assim

1ª Parte B :
      G                               Bm
Por saber de tanto amar
        Am                         G
Voltei então pra mim
                                            Bm
Por não ter que magoar
     Am                          G        Em
Achei melhor assim

                               Am
Pra soltar nos olhos
D                               Bm
Pra mostrar na voz
Em                          Am
Pra correr por todo lado
       D                           Bm
E não voltar pra nós

Em                      Am
Pra ouvir te amo
             D               Bm
Sem ter que falar
Em                         Am                  A
Ter amor pra mais de um ano
                                       D
E um ano mais amar

(+2ª Parte 2X)

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
