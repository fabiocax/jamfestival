Tom Jobim - Samba de Maria Luiza

              Ab7M(9)
É do cabelo amarelo
Db7M(6)          Ab7M(9)
Dos óio cor de chuchu
Db7M(6)              Ab7M(9)
Quando eu virar gente grande
Bbm7    Eb7      Dm7(b5)   G7(b9)
Me caso logo com tu
  C6        Fm6/C  Fm6/C*
O samba de Maria  Luiza
  C7M       Fm6/C  Fm6/C*
O samba de Maria Luiza
  Em7        Eb7
O samba de Maria Luiza
   Abm7(9) Db7(b9)  C7M(9)  A7(b13)  D7(9)  G7/13(b9)
é bonito   pra   chuchu
  C6        Fm6/C  C7M(9)  A7  D7   G7     C7M(9)
O samba de Maria Luiza     é bonito pra chuchu
A7  D7      G7  C7M      Am7
Ela canta e ela dança, menina
  D7(9)    G7  C6               F7/4(9)
O samba da Marilu, Marilu, Marilu, Marilu

  C6        Fm6/C  C7M(9) A7  D7   G7     C7M(9)
O samba de Maria Luiza    é bonito como o quê
  A7    D7       G7  C7M
E é por isso que o papai
      Am7   D7(9) G7      C6
Já tá apaixonado  por você
Am7      D7(9) G7(b13)  C6
Tá apaixonado por    você
             C9/6
Maria Luiza, Ah

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7(b13) = X 0 X 0 2 1
Ab7M(9) = 4 3 5 3 X X
Abm7(9) = X X 6 4 7 6
Am7 = X 0 2 0 1 0
Bbm7 = X 1 3 1 2 1
C6 = 8 X 7 9 8 X
C7M = X 3 2 0 0 X
C7M(9) = X 3 2 4 3 X
C9/6 = X 3 2 2 3 3
D7 = X X 0 2 1 2
D7(9) = X 5 4 5 5 X
Db7(b9) = X 4 3 4 3 X
Db7M(6) = X 4 3 3 1 1
Dm7(b5) = X X 0 1 1 1
Eb7 = X 6 5 6 4 X
Em7 = 0 2 2 0 3 0
F7/4(9) = X X 3 3 4 3
Fm6/C = X 3 3 1 3 X
G7 = 3 5 3 4 3 3
G7(b13) = 3 X 3 4 4 3
G7(b9) = 3 X 3 1 0 X
G7/13(b9) = 3 5 3 P4 5 4
