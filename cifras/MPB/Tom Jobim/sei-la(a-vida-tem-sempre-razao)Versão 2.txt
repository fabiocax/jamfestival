Tom Jobim - Sei Lá (A Vida Tem Sempre Razão)

(intro) Em7/5-  Ebm7  Dm7
       G7  C7  F7  Bb

                                  C
Tem dias que eu fico pensando na vida
           F7             Bb
E sinceramente não vejo saída
                       Csus      F7+
Como é por exemplo que dá pra entender
  Dm7        Gm7      C7         F
A gente mal nasce e começa a morrer
  F7         Bb          Gm7      C
Depois da chegada vem sempre a partida
              F7            Fm7
Porque não há nada sem separação

Bb7  Em7/5- Ebm7
Sei lá,
     Dm7 G7
Sei lá
C7            F7     Fm7   Bb7
A Vida é uma grande ilusão


Bb7  Em7/5- Ebm7
Sei lá,
     Dm7 G7
Sei lá
       C7           F7          Bb
Eu só sei que ela está com a razão

(solo) Em7/5-  Ebm7  Dm7
       G7  C7  F7  Bb

Bb7  Em7/5- Ebm7
Sei lá,
     Dm7 G7
Sei lá
C7            F7     Fm7   Bb7
A Vida é uma grande ilusão

Bb7  Em7/5- Ebm7
Sei lá,
     Dm7 G7
Sei lá
       C7           F7          Bb
Eu só sei que ela está com a razão

                                  C
A gente nem sabe que males se apronta
            F7                  Bb
Fazendo de conta, fingindo esquecer
                 Csus         F7+
Que nada renasce antes que se acabe
    Dm7        Gm7           C7      F
E o sol que desponta tem que adormecer
   F7      Bb     Gm7         C
De nada adianta ficar-se de fora
          F7                   Fm7
A hora do sim é o descuido do não

Bb7  Em7/5- Ebm7
Sei lá,
     Dm7 G7
Sei lá
C7            F7     Fm7   Bb7
A Vida é uma grande ilusão

Bb7  Em7/5- Ebm7
Sei lá,
     Dm7 G7
Sei lá
       C7           F7          Bb
Eu só sei que ela está com a razão

(solo) Em7/5-  Ebm7  Dm7
       G7  C7  F7  Bb

Bb7  Em7/5- Ebm7
Sei lá,
     Dm7 G7
Sei lá
C7                 F7     Fm7   Bb7
Eu só sei que é preciso a paixão

Bb7  Em7/5- Ebm7
Sei lá,
     Dm7 G7
Sei lá
       C7           F7          Bb
Eu só sei que ela está com a razão

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bb7 = X 1 3 1 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Csus = X 3 2 0 1 0
Dm7 = X 5 7 5 6 5
Ebm7 = X X 1 3 2 2
Em7/5- = X X 2 3 3 3
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
F7+ = 1 X 2 2 1 X
Fm7 = 1 X 1 1 1 X
G7 = 3 5 3 4 3 3
Gm7 = 3 X 3 3 3 X
