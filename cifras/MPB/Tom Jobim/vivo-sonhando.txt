Tom Jobim - Vivo Sonhando

 Gmaj7            G6                   Cm9/G Cm7/G
Vivo sonhando, Sonhando mil horas sem fim
Gmaj7                  G6                  Bm7 E7/9-
Tempo em que vou perguntando, Se gostas de mim
Am7                Cm6                  Bm7             E7/9-
Tempo de falar em estrelas, Falar de um mar, De um céu assim
Am7                 Aº               Em9      Gm6
Falar do bem que se tem mas você não vem, Não vem
Gmaj7           G6               Cm9/G Cm7/G
Você não vindo, Não vindo a vida tem fim
Gmaj7             G6                 Bm7 E7/9-
Gente que passa sorrindo zombando de mim
Am7              Cm6      Bm7            E7/9-
E eu a falar em estrelas,     mar, amor, luar
A7/6     A7/5+ Am6            Gmaj7 G6
Pobre de mim   que só sei te amar

----------------- Acordes -----------------
A7/5+ = X 0 X 0 2 1
A7/6 = X 0 X 0 2 2
Am6 = 5 X 4 5 5 X
Am7 = X 0 2 0 1 0
Aº = 5 X 4 5 4 X
Bm7 = X 2 4 2 3 2
Cm6 = X 3 X 2 4 3
Cm7/G = 3 1 1 3 1 X
Cm9/G = 3 3 5 7 4 3
E7/9- = X X 2 1 3 1
Em9 = 0 2 4 0 0 0
G6 = 3 X 2 4 3 X
Gm6 = 3 X 2 3 3 X
Gmaj7 = 3 X 4 4 3 X
