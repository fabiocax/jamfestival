Tom Jobim - One Note Samba

        Dbm7          C7
This is just a little samba,
       B7/4         Bb7/-5
Built upon a single note
      Dbm7               C7
Other notes are bound to follow,
        B7/4               Bb7/-5
But the root is still that note
         Em7/9          A7/+5
Now this new one is the consequence,
       Dmaj7               Dm7     G7
Of the one we've just been through
       Dbm7     C7     B7/4        Bb7/-5         A6/9
As I'm bound to be the unavoidable consequence of you
Dm7                            G7
There's so many people who can talk
                               Cmaj7
And talk and talk and just say nothing,

Or nearly nothing
Cm7                            F7
I have used up all the scale I know,

                            Bbmaj7
And at the end I've come to nothing,
          Bdim   Bb7/-5
Or nearly no.....thing
     Dbm7            C7
So I came back to my first note,
     B7/4              Bb7/-5
As I must come back to you
       Dbm7           C7
I will pour into that one note,
        B7/4            Bb7/-5
All the love I feel for you
   Em7/9             A7/+5
Anyone who wants the whole show,
      Dmaj7        Dm7   G7
Re mi fa sol la si do
        C6                B7
He will find himself with no show,
       Bbmaj7           A7
Better play the note you know

----------------- Acordes -----------------
A6/9 = 5 4 4 4 X X
A7 = X 0 2 0 2 0
A7/+5 = X 0 X 0 2 1
B7 = X 2 1 2 0 2
B7/4 = X 2 4 2 5 2
Bb7/-5 = X 1 2 1 3 X
Bbmaj7 = X 1 3 2 3 1
Bdim = X 2 3 1 3 1
C6 = 8 X 7 9 8 X
C7 = X 3 2 3 1 X
Cm7 = X 3 5 3 4 3
Cmaj7 = X 3 2 0 0 X
Dbm7 = X 4 6 4 5 4
Dm7 = X 5 7 5 6 5
Dmaj7 = X X 0 2 2 2
Em7/9 = X 7 5 7 7 X
F7 = 1 3 1 2 1 1
G7 = 3 5 3 4 3 3
