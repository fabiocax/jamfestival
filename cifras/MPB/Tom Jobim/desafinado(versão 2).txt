Tom Jobim - Desafinado

F7M                           G7(11+)
   Se você disser que eu desafino amor,
Gm7                   C7           Am7(5-)   D7(13-)
   Saiba que isso em mim provoca imensa dor
Gm7(9)         A7(9-)      D7M              D7(9-)
   Só privilegiados tem ouvido igual ao seu
G7(9)          G7(9-)      Gm7(5-)     Gb7M(11+)
   Eu possuo apenas o que Deus me deu

 F7M                    G7(11+)
   Se você insiste em classificar,
Gm7             C7            Am7(5-)  D7(13-)
   Meu comportamento de anti-musical
Gm7(9)       A7(9-)           Dm7(11)  E7(9+)
   Eu, mesmo mentindo devo argumentar,
A7M   Ab7(13-)            G7(13)      Gb7(11+)
   Que isto é bossa nova, Que isto é muito natural

     A7M           A#°          Bm7(11)         E7(9-)
   O que você não sabe, nem sequer pressente,
A7M                Am7(9)       Bm7(5-)         Bb7(9)
   É que os desafinados também tem um coração

C7M              C#°         Dm7(11)    G7(13-)
   Fotografei você na minha Rolleiflex,
        Gm7           D7(9-)           G7(13) G7(13-) C4(7/9) C7(9-/11+)
   Revelou-se a sua enorme ingratidão

F7M                       G7(11+)          Gm7
   Só não poderá falar assim do meu amor,
               C7         Am7(5-)           D(13-)
   Este é o maior que você pode encontrar, viu
Bb7M(9)             Bbm6       Am7         Ab°7M
   Você com a sua música esqueceu o principal,
           G7
   Que no peito dos desafinados
               Gb7M(11+)
   No fundo do peito bate calado,
                           Gm7   C7(9-)  Gm7(11)  Gb7(11+)  F7M
   Que no peito dos desafinados também   bate um co______ração

  ( F7M(6/9/11+) )

----------------- Acordes -----------------
A#° = X 1 X 0 2 0
A7(9-) = X 0 X 3 2 3
A7M = X 0 X 1 2 0
Ab7(13-) = 4 X 4 5 5 X
Am7 = X 0 2 0 1 0
Am7(5-) = 5 X 5 5 4 X
Am7(9) = X X 7 5 8 7
Bb7(9) = X 1 0 1 1 X
Bb7M(9) = X 1 0 2 1 X
Bbm6 = 6 X 5 6 6 X
Bm7(11) = 7 X 7 7 5 X
Bm7(5-) = X 2 3 2 3 X
C#° = X 4 5 3 5 3
C4(7/9) = X 3 3 3 3 3
C7 = X 3 2 3 1 X
C7(9-) = X 3 2 3 2 X
C7(9-/11+) = X 3 2 3 2 2
C7M = X 3 2 0 0 X
D7(13-) = X 5 X 5 7 6
D7(9-) = X 5 4 5 4 X
D7M = X X 0 2 2 2
Dm7(11) = X 5 5 5 6 5
E7(9+) = X 6 5 6 7 X
E7(9-) = X X 2 1 3 1
F7M = 1 X 2 2 1 X
F7M(6/9/11+) = 1 0 0 0 0 0
G7 = 3 5 3 4 3 3
G7(11+) = 3 X 3 4 2 X
G7(13) = 3 X 3 4 5 X
G7(13-) = 3 X 3 4 4 3
G7(9) = 3 X 3 2 0 X
G7(9-) = 3 X 3 1 0 X
Gb7(11+) = 2 X 2 3 1 X
Gb7M(11+) = 2 X 3 3 1 1
Gm7 = 3 X 3 3 3 X
Gm7(11) = 3 X 3 3 1 X
Gm7(5-) = 3 X 3 3 2 X
Gm7(9) = X X 5 3 6 5
