Tom Jobim - Águas de Março

[Intro] B/A

[Primeira Parte]

   B/A                        G#m6
É pau, é pedra, é o fim do caminho
               Em6/G                B7M/F#
É um resto de toco,   é um pouco sozinho
              F7(9)                  E7M(9)
É um caco de vidro,   é a vida, é o sol
                A7(13)                 B6(9)
É a noite, é a morte, é o laço, é o anzol
             B7(9-)             C#/F
É peroba do campo, é o nó da madeira
            Em6                B6(9)
Caingá, candeia, é o Matita Pereira
              B7(9-)               Fm7(5b)
É madeira de vento, tombo da ribanceira
                 Em6                       B6(9)
É o mistério profundo, é o queira ou não queira


              B/A                  G#m6
É o vento ventando,  é o fim da ladeira
               Em6/G             B7M/F#
É a viga, é o vão,  festa da cumeeira
              F7(9)               E7M(9)
É a chuva chovendo, é conversa ribeira
              A7(13)              B6(9)
Das águas de março,é o fim da canseira

[Segunda Parte]

              B/A                 G#m6
É o pé, é o chão é a marcha estradeira
               Em6/G              B7M/F#
Passarinho na mão, pedra de atiradeira
            B7(9-)          C#/F
Uma ave no céu,uma ave no chão
                    Em6                  B6(9)
É um regato, é uma fonte,é um pedaço de pão

[Primeira Parte]

              B/A                 G#m6
É o fundo do poço, é o fim  do caminho
               Em6/G               B7M/F#
No rosto o desgosto, é um pouco sozinho
                     F7(9)                    E7M(9)
É um estrepe, é um prego   é uma ponta, é um ponto
               A7(13)                   B6(9)
É um pingo pingando, é uma conta, é um conto
                  B7(9-)                 C#/F
É um peixe, é um gesto, é uma prata brilhando
               Em6               B6(9)
É a luz da manhã, é o tijolo chegando

[Segunda Parte]

                B/A               G#m6
É a lenha, é o dia  é o fim da picada
                Em6/G                   B7M/F#
É a garrafa de cana, o estilhaço na estrada
                B7(9-)             C#/F
É o projeto da casa, é o corpo na cama
                Em6                  B6(9)
É o carro enguiçado,  é a lama, é a lama

[Primeira Parte]

                   B/A                     G#m6
É um passo, é uma ponte, é um sapo, é uma rã
               Em6/G               B7M/F#
É um resto de mato   na luz da manhã
                 F7(9)              E7M(9)
São as águas de março   fechando o verão
                 A7(13)          B6(9)
É a promessa de vida no teu coração

( F7(9)  D  B7M/F#  F7(9)  E7M(9) )
( A7(13)  B6(9)  B/A  G#m6  Em6/G  B7M/F# )

[Terceira Parte]

                   B7(9)            E7M(9)
É uma cobra, é um pau,  é João, é José
                 Em6                B6(9)
É um espinho na mão, é um corte no pé
                 Bm7                C#/B
São as águas de março fechando o verão
                 C/B             B
É a promessa de vida no teu coração

             B/A                G#m6
É pau, é pedra,   é o fim do caminho
               Em6/G              B7M/F#
É um resto de toco, é um pouco sozinho
                   Bm7                     C#/B
É um passo, é uma ponte, é um sapo, é uma rã
               C/B                   B
É um belo horizonte, é uma febre terçã
                 Bm7                C#/B
São as águas de março fechando o verão
                 C/B             B
É a promessa de vida no teu coração

    B7(9-)    E7M(9)     Em6      B6(9)
Au, edra, im, inho resto oco ouco inho
     B7(9)   E7M(9)  Em6      B6(9)
aco idro ida ol oite orte aço ol

                 B7(9-)             E7M(9)
São as águas de março fechando o verão
                 Em6             B6(9)
É a promessa de vida no teu coração

( B7(9-)  E7M(9)  Em6  B6(9) )
( Bm7  C#/B  B/A#  B )

----------------- Acordes -----------------
A7(13) = X 0 X 0 2 2
B = X 2 4 4 4 2
B/A = X 0 4 4 4 X
B/A# = 6 X 4 4 4 X
B6(9) = X 2 1 1 2 2
B7(9) = X 2 1 2 2 X
B7(9-) = X 2 1 2 1 X
B7M/F# = X X 4 4 4 6
Bm7 = X 2 4 2 3 2
C#/B = X 2 3 1 2 X
C#/F = X 8 X 6 9 9
C/B = X 2 2 0 1 0
D = X X 0 2 3 2
E7M(9) = X 7 6 8 7 X
Em6 = X 7 X 6 8 7
Em6/G = 3 4 2 4 2 X
F7(9) = X X 3 2 4 3
Fm7(5b) = X X 3 4 4 4
G#m6 = 4 X 3 4 4 X
