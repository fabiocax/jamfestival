Tom Jobim - Eu e Meu Amor / Lamento No Morro (Pot-Pourri)

[Intro] Am7  Am6
        Am7  Am6
        Am7  Am6
        Am7  Am6

Bm7(b5)  Bb7(b5)  Am7
Eu e o meu amor
Am7/G       F7+
E o meu amor
E7     Am7
Que foi-se embora
          D9                     Am7
Me deixando tanta dor
Am6        Am7
Tanta tristeza
    D9                    Am7
No meu pobre coração
Dm7 G13      C7+
Que até jurou
G7(b13)      C7+
Não me deixar

               F7+
E foi-se embora
                        Bm7(b5)      E7
Para nunca mais voltar

Bm7(b5)  Bb7(b5)  Am7
Eu e o meu amor
Am7/G       F7+
E o meu amor
E7     Am7
Que foi-se embora
          D9                     Am7
Me deixando tanta dor
Am6        Am7
Tanta tristeza
    D9                    Am7
No meu pobre coração

( Am7  Am6 )

Am7   Dm7  G13  C7+
Não posso esquecer
C7       F7+
O teu olhar
E7         Am7       Am6
Longe dos olhos meus
Am7   Dm7  G13  C7+
Ai, o meu viver
C7       F7+
É de esperar
E7         Am7       Am6
Pra te dizer adeus

Dm9    E7    Am7   Am6
Mulher amada
Dm9  G13 C7M
Destino meu
B7   E7(b9) Am7
É madrugada
F7+                                       E7 E7(b9)
Sereno dos meus olhos já correu

Am7 Dm7 G13 G7(b13)
Não posso esquecer
C7+   C7   F7+
O teu olhar
E7         Am7
Longe dos olhos meus

----------------- Acordes -----------------
Am6 = 5 X 4 5 5 X
Am7 = X 0 2 0 1 0
Am7/G = 3 0 2 0 1 0
B7 = X 2 1 2 0 2
Bb7(b5) = X 1 2 1 3 X
Bm7(b5) = X 2 3 2 3 X
C7 = X 3 2 3 1 X
C7+ = X 3 2 0 0 X
C7M = X 3 2 0 0 X
D9 = X X 0 2 3 0
Dm7 = X 5 7 5 6 5
Dm9 = X 5 7 9 6 5
E7 = 0 2 2 1 3 0
E7(b9) = X X 2 1 3 1
F7+ = 1 X 2 2 1 X
G13 = 3 X 2 4 3 X
G7(b13) = 3 X 3 4 4 3
