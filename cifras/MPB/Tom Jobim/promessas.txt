Tom Jobim - Promessas


G7+  G5+/7 Am7          Bb° Bm7                G5+/7
Sim, promessas fiz, fiz projetos pensei tantas coisas
   C7+      F7/9
E vem o coração e diz
    Bm7                 Em             C/D     E7/9-
Que só em seus braços, amor, eu posso ser feliz
   Am7                E7/9-               A9
Eu tenho um amor para dar, o que é que eu vou fazer.
D7/9              G7+ G5+/7 Am7 Bb° Bm7
Eu tentei te esquecer e prometi apagar da minha vida
      G5+/7   C7+      F7/9
Esse sonho e vem o coração e diz
    Bm7                 Em           Cm7/9 F7/13 Bm7 E7/9-
Que só em teus braços, amor, eu posso ser feliz
    A7                  C/D           G
Que só em teus braços, amor, eu posso ser feliz

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A9 = X 0 2 2 0 0
Am7 = X 0 2 0 1 0
Bb° = X 1 2 0 2 0
Bm7 = X 2 4 2 3 2
C/D = X X 0 0 1 0
C7+ = X 3 2 0 0 X
Cm7/9 = X 3 1 3 3 X
D7/9 = X 5 4 5 5 X
E7/9- = X X 2 1 3 1
Em = 0 2 2 0 0 0
F7/13 = 1 X 1 2 3 X
F7/9 = X X 3 2 4 3
G = 3 2 0 0 0 3
G5+/7 = 3 X 3 4 4 3
G7+ = 3 X 4 4 3 X
