Tom Jobim - Corcovado

[Intro] Am6  G#°(b13)  Gm7
        C7(9)  F7M  Fm6  Em7
        Am7  D7(9)  G#°(b13)

Am6                 G#º(b13)
Um cantinho, um violão
                   Gm7
Este amor, uma canção
           C7(9)           F7M  F6
Pra fazer feliz a quem se a___ma
 Fm6          Bb7(9)
Muita calma pra pensar
       Em7(9)  A7(13)  A7(13-)   Am6
E ter tempo        pra    sonhar
   D7                    Dm7
Da janela vê-se o corcovado
    G7       G#°
O redentor que lindo
  Am6               G#º(b13)
Quero a vida sempre assim
Com você perto de mim

Gm7   C7(9)       F7M    F6
Até o apagar da velha chama
Fm6      Bb7(9)    Em7
E eu que era triste
           Am7      Dm7
Descrente desse mundo
              G4/7(9)  G7(9)-  Em7/5-  A7/13-
Ao encontrar você       eu  conheci
Dm7             G7         Am6
O que é a felicidade meu amor

( Am6  G#º(b13)  Gm7  C7(9) )
( F7M  F6  Fm6  Bb7(9)  Em7(9) )
( A7/13  A7/13-  Am6  Fm6  Bb7(9) )
( Am6  G#º(b13)  Gm7  C7(9) )
( F7M  F6  Fm6  Bb7(9)  Em7(9) )
( A7/13  A7/13-  Am6  Fm6  Bb7(9) )

----------------- Acordes -----------------
A7(13) = X 0 X 0 2 2
A7(13-) = X 0 X 0 2 1
A7/13 = X 0 X 0 2 2
A7/13- = X 0 X 0 2 1
Am6 = 5 X 4 5 5 X
Am7 = X 0 2 0 1 0
Bb7(9) = X 1 0 1 1 X
C7(9) = X 3 2 3 3 X
D7 = X X 0 2 1 2
D7(9) = X 5 4 5 5 X
Dm7 = X 5 7 5 6 5
Em7 = 0 2 2 0 3 0
Em7(9) = X 7 5 7 7 X
Em7/5- = X X 2 3 3 3
F6 = 1 X 0 2 1 X
F7M = 1 X 2 2 1 X
Fm6 = 1 X 0 1 1 X
G#° = 4 X 3 4 3 X
G#°(b13) = 4 X 3 4 5 X
G#º(b13) = 4 X 3 4 5 X
G4/7(9) = 3 X 3 2 1 X
G7 = 3 5 3 4 3 3
Gm7 = 3 X 3 3 3 X
