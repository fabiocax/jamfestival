Melim - Meu Abrigo

[Intro]

G   B7          Em  C
Uh, uh, uh, uh, uh, uh
G   B7          Em  C
Uh, uh, uh, uh, uh, uh

[Primeira parte]

            G
Desejo a você,
                D
O que há de melhor
               Em
A minha companhia
                   C
Pra não se sentir só

                  G
O Sol, a Lua e o mar
                 D
Passagem pra viajar

                 Em
Pra gente se perder
            C
E se encontrar

[Pré-refrão]

 Em         D
Vida boa, brisa e paz
         C                     G
Nossas brincadeiras ao entardecer
Em           D
Rir à toa é bom demais
   C                           D
O meu melhor lugar sempre é você

[Refrão]

     C      G         Em       D
Você é a razão da minha felicidade
          C              G
Não vá dizer que eu não sou
  Em        D
Sua cara-metade
      C          G    Em           D
Meu amor, por favor, vem viver comigo
       C        D
O seu colo é o meu abrigo
G   B7          Em  E
Uh, uh, uh, uh, uh, uh

[Segunda parte]

             A
Quero presentear
                 E
Com flores Iemanjá
             F#m
Pedir um paraíso
                   D
Pra gente se encostar

               A
Uma viola a tocar
                       E
Melodias pra gente dançar
                 F#m
A bênção das estrelas
            D
A nos iluminar

[Pré-refrão]

 F#m        E
Vida boa, brisa e paz
    D                      A
Trocando olhares ao anoitecer
 F#m         E
Rir à toa é bom demais
   D                           E
Olhar pro céu, sorrir e agradecer

[Refrão final]

     D      A         F#m      E
Você é a razão da minha felicidade
          D              A
Não vá dizer que eu não sou
  F#m       E
Sua cara-metade
      D          A    F#m          E
Meu amor, por favor, vem viver comigo
       D        E
O seu colo é o meu abrigo
A   C#7        F#m
Uh, uh, meu abrigo
       D              A
O seu colo é o meu abrigo
A   C#7        F#m
Uh, uh, meu abrigo
       D        E     A
O seu colo é o meu abrigo

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
C#7 = X 4 6 4 6 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 3 3
