﻿Melim - Dois Corações

Capo Casa 1

[Intro] F  C  F  C  G

[Primeira Parte]

         Am
Eu era apenas eu
   G
Nada demais
            F
Chorava enquanto meu coração escondido
                C          G
Ia seguindo os teus sinais

       Am
Você era você
       G
Tão especial
             F
Me olhava igual sorriso de criança
                      C       G
Esperando o presente de natal


[Pré-Refrão]

 F           G
Vi que era amor

Quando te achei em mim
Am       F       G
E me perdi em você

[Refrão]

       F
Somos verso e poesia

Outono e ventania
  C
Praia e carioca
       F
Somos pão e padaria

Piano e melodia
 C
Filme e pipoca

         F                 C
De dois corações um só se fez
        F             G        C
Um que vale mais que dois ou três

( F  C  F  G  C )

[Pré-Refrão]

 F           G
Vi que era amor

Quando te achei em mim
Am       F       G
E me perdi em você

[Refrão]

       F
Somos verso e poesia

Outono e ventania
  C
Praia e carioca
       F
Somos pão e padaria

Piano e melodia
 C
Filme e pipoca

         F                 C
De dois corações um só se fez
        F             G        C
Um que vale mais que dois ou três

[Final] F  C  F  C
        F  C  F  G  C

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
