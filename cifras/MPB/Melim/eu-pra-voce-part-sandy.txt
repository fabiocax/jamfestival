﻿Melim - Eu Pra Você (part. Sandy)

Capo Casa 1

[Intro] C  Dm

   C                 Dm
E|---0---0---0---0------1----1----1----1--------------|
B|---1---1---1---1------3----2----2----2--------------|
G|---0---0---0---0------2----3----3----3--------------|
D|-----2---3---2-----0-------0----0----0--------------|
A|-3--------------------------------------------------|
E|----------------------------------------------------|

   C                 Dm
E|---0---0---0---0------1----1----1----1--------------|
B|---1---1---1---1------3----2----2----2--------------|
G|---0---0---0---0------2----3----3----3--------------|
D|-----2---3---2-----0-------0----0----0--------------|
A|-3--------------------------------------------------|
E|----------------------------------------------------|

[Primeira Parte]

    C                  Dm
Cansei de morrer por amor

          C                 Dm
Então me deixa viver por você?
    Am                         D7
Cansei de deixar pistas pra você encontrar
     F7M                          G6
Mas, então, vem, se eu te chamar?

[Segunda Parte]

       C                    Dm
Tô cansada de ser outro alguém
       C                            Dm
De fingir que eu tô bem, se eu não tô
          Am                    D7
Então me deixa te dar o que eu sou
    F7M
Me deixa ser eu pra você

[Refrão]

    C                       Dm
Esquece essa história inventada de achar
     Am                       F7M
Que dá pra controlar seu coração
    C                 Dm
Me deixa parar de tentar enganar o meu
  F7M  Am7  Dm  G   C
Que   só  quer ser teu
  F7M  Am7  Dm  G   C
Que   só  quer ser teu

( C  Dm )
( C  Dm )

   C                 Dm
E|---0---0---0---0------1----1----1----1--------------|
B|---1---1---1---1------3----2----2----2--------------|
G|---0---0---0---0------2----3----3----3--------------|
D|-----2---3---2-----0-------0----0----0--------------|
A|-3--------------------------------------------------|
E|----------------------------------------------------|

   C                 Dm
E|---0---0---0---0------1----1----1----1--------------|
B|---1---1---1---1------3----2----2----2--------------|
G|---0---0---0---0------2----3----3----3--------------|
D|-----2---3---2-----0-------0----0----0--------------|
A|-3--------------------------------------------------|
E|----------------------------------------------------|

[Segunda Parte]

       C                    Dm
Tô cansada de ser outro alguém
       C                            Dm
De fingir que eu tô bem, se eu não tô
          Am                    D7
Então me deixa te dar o que eu sou
    F7M
Me deixa ser eu pra você

[Refrão]

    C                       Dm
Esquece essa história inventada de achar
     Am                       F7M
Que dá pra controlar seu coração
    C                 Dm
Me deixa parar de tentar enganar o meu
  F7M  Am7  Dm  G   C
Que   só  quer ser teu
  F7M  Am7  Dm  G   C
Que   só  quer ser teu

[Ponte]

Dm
 É vontade que fala, né?
G11
 É uma voz que não cala, é
Am7                             F7M
 Que me diz que eu só posso morrer por amor
 Dm              G/B
Se for pra vivermos por nós dois

[Refrão]

    C                       Dm
Esquece essa história inventada de achar
     Am                       F7M
Que dá pra controlar seu coração
    C                 Dm
Me deixa parar de tentar enganar o meu
  F7M  Am7  Dm  G   C
Que   só  quer ser teu
  F7M  Am7  Dm  G   C
Que   só  quer ser teu

----------------- Acordes -----------------
Capotraste na 1ª casa
Am*  = X 0 2 2 1 0 - (*A#m na forma de Am)
Am7*  = X 0 2 0 1 0 - (*A#m7 na forma de Am7)
C*  = X 3 2 0 1 0 - (*C# na forma de C)
D7*  = X X 0 2 1 2 - (*D#7 na forma de D7)
Dm*  = X X 0 2 3 1 - (*D#m na forma de Dm)
F7M*  = 1 X 2 2 1 X - (*F#7M na forma de F7M)
G*  = 3 2 0 0 0 3 - (*G# na forma de G)
G/B*  = X 2 0 0 3 3 - (*G#/C na forma de G/B)
G11*  = 3 5 5 5 3 3 - (*G#11 na forma de G11)
G6*  = 3 X 2 4 3 X - (*G#6 na forma de G6)
