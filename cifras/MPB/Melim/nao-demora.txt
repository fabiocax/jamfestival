﻿Melim - Não Demora

Capo Casa 3

Am   Em      F
  Ah Ah Uh Ah
     G     Am
Ah Ah Uh Ah
    Em      F G
Ah Ah Uh Ah
              Am
Me sinto tão bem
        Em         F                       G
É tao bom parar o mundo pra sonhar com você
           Am                Em         F
De olhos fechados faço questão de te ver
         Am             Em
O nosso amor não é normal
         F                          G
Mas uma pitada de loucura não faz mal
              Am          Em                 F
Melhor que ser meio sei lá, sem graça e sem sal
Dm                                Am
Juntos no caminho a gente continua
                               Em
Um pé na estrada ouvindo Cássia, All Star Azul

Dm                                     Am
Diz que tá na minha que eu já tô na sua
                    Em                E
Te beijando viajando assim de norte a sul
Am    Em  F
Não demora
          G                      Am
Não deixa pra amanhã o nosso agora
  Em   F   G
Ah   ah  ah
Am    Em  F
Não demora
          G                       Am
Não deixa pra amanhã o nosso agora
  Em   F
Ah   ah
             G                       Am
Te quero pra ontem, já passou da hora
    Em     F  G
Ah ah uh ah (Vamos viver)

    Em     F  G
Ah ah uh ah (Vamos viver)

----------------- Acordes -----------------
Capotraste na 3ª casa
Am*  = X 0 2 2 1 0 - (*Cm na forma de Am)
Dm*  = X X 0 2 3 1 - (*Fm na forma de Dm)
E*  = 0 2 2 1 0 0 - (*G na forma de E)
Em*  = 0 2 2 0 0 0 - (*Gm na forma de Em)
F*  = 1 3 3 2 1 1 - (*G# na forma de F)
G*  = 3 2 0 0 0 3 - (*A# na forma de G)
