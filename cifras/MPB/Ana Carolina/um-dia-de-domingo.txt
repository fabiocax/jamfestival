Ana Carolina - Um Dia de Domingo

(part.: show de Celso Fonseca)

G#7+           Cm7
Eu preciso te falar
A#m7    D#7/9          G#7+
Te encontrar de qualquer jeito
                  Cm7
Pra sentar e conversar
A#m7    D#7/9                 G#7+  G#7/4(9)
Depois andar de encontro ao vento
G#7(9)        C#7+                      D#7
Eu preciso respirar      O mesmo ar que te rodeia
                  Cm7                          Fm7
E na pele eu quero ter   O mesmo sol que te bronzeia
               A#m7                         D#7/9
Eu preciso te tocar  E outra vez te ver sorrindo
                    G#7+   G#7/4(9)
Te encontrar num sonho lindo
 G#7(9)           A#m7                        D#7/9
Já não dá mais pra viver   Um sentimento sem sentido
               Cm7          E°              Fm7
Eu preciso descobrir     A emoção de estar contigo

               A#m7                     D#7/9 D#7(b9)
Ver o sol amanhecer    E ver a vida acontecer
                  G#7+
Como um dia de domingo

         C#7+              Cm7    Fm7
Faz de conta que ainda é cedo
     A#m7           D#7/9      G#7+   G#7/4(9)  G#7(9)
Tudo vai ficar por conta da emoção
        C#7+               Cm7   Fm7
Faz de conta que ainda é cedo
    A#m7            D#7/9  D#7(b9)     G#7+
E deixar falar a     voz    do     coração

(repete tudo)
(repete refrçao 2x)

----------------- Acordes -----------------
A#m7 = X 1 3 1 2 1
C#7+ = X 4 6 5 6 4
Cm7 = X 3 5 3 4 3
D#7 = X 6 5 6 4 X
D#7(b9) = X 6 5 6 5 X
D#7/9 = X 6 5 6 6 X
E° = X X 2 3 2 3
Fm7 = 1 X 1 1 1 X
G#7(9) = 4 X 4 3 1 X
G#7+ = 4 X 5 5 4 X
G#7/4(9) = 4 X 4 3 2 X
