Ana Carolina - Tô Saindo


Am7
Um buraco é um lugar onde alguém afunda
Um buraco é um lugar onde eu não quero estar
Deixa estar. deixa estar

Minha boca linda
Quero te ver, quero te ver passar
Am7                                        D7/9
Eu tô saindo, eu tô saindo, eu tô saindo deste
        Am7                         C7
Eu tô saindo, eu tô saindo deste buraco
D7/9
Help! Eu preciso sambar Help!
Não há quem me pare Help!
               C7     { Am7  D7/9 }
Eu preciso sambar

Um buraco no chão é uma armadilha
Um buraco no teto é complicação
Se chover quero estar do teu lado, ó minha

Se o sol aparecer já não garanto não

Eu tô saindo, eu to saindo deste, eu tô saindo deste
Eu tô saindo, eu to saindo deste, eu tô saindo já
Eu tô saindo, eu to saindo deste, eu tô saindo deste
Eu tô saindo, eu to saindo deste, eu tô saindo deste buraco

Help! Eu preciso sambar
Help! Não há quem me pare
Help! Eu preciso sambar
Uma fenda, uma fossa, uma rachadura
Uma vala, um cabresto eu não quero não
Eu te vi no buraco da fechadura Com um buraco feito a bala no teu coração
Uma faca, um vão, um beco sem saída um buraco no dente é uma dor de cão
eu não quero passar pela avenida numa escola sem enredo e uma de evolução
eu to saindo...

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
C7 = X 3 2 3 1 X
D7/9 = X 5 4 5 5 X
