Ana Carolina - Zé do Caroço

Dm79                   Gm7
No serviço de auto-falante
                       Dm79
No morro do Pau da Bandeira
                        Bb7+
Quem avisa é o Zé do Caroço
                         Gm7
Amanhã vai fazer alvoroço
                      A7
Alertando a favela inteira
                                Bb7+
Ai como eu queria que fosse em mangueira
                            Dm79
Que existisse outro Zé do Caroço
                               Gm7
Pra falar de uma vez pra esse moço
                       Bb7+
Carnaval não é esse colosso
                          A7
Nossa escola é raiz, é madeira
                           Bb7+
Mas é o Morro do Pau da Bandeira

                         Dm79
  De uma Vila Isabel verdadeira
                   Gm7
O Zé do Caroço trabalha
                 Bb7+
O Zé do Caroço batalha
                        A7
E que malha o preço da feira
                                Bb7+ Bb7+ Am7 Gm7
E na hora que a televisão brasileira
                      A7         Dm79
Destrói toda gente com a sua novela
                          Gm7
É que o Zé bota a boca no mundo
                        Bb7+
Ele faz um discurso profundo
                        A7
Ele quer ver o bem da favela
                           Bb7+ Bb7+ Am7 Gm7
    Está nascendo um novo líder
                          Dm79
    No morro do Pau da Bandeira
                           Bb7+ Bb7+ Am Gm7
    Está nascendo um novo líder
                         Dm79
    No morro do Pau da Bandeira
                           Bb7+  Gm7
    No morro do Pau da Bandeira
                           A7
    No morro do Pau da Bandeira
  Bb7+                 Bb7+ Am7 Gm7              Dm79
  Lê     Lê       Lê                Lê LêLê LêLê Lê

 e e e ê......

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Bb7+ = X 1 3 2 3 1
Dm79 = X 5 3 5 5 X
Gm7 = 3 X 3 3 3 X
