Ana Carolina - O Violão

Intro 2x:  C6/G    Am/C

C6/G    Am/C          C6/G
Um dia eu vi numa estrada
Am/C         C6/G
Um arvoredo caído
C/G        G6/B        Dm    Dm7 A5+
Não era um tronco qualquer
            G7        Dm   Dm7/C
Era madeira de pinho
Dm6/B            Dm
E um artesão esculpia
G9/7      G        C6/G
O corpo de uma mulher
Bm7/F#    E7       Bm7/F#
Depois eu vi pela noite
E7           Bm7/F#
O artesão nos caminhos
E7               Am Am7M Am Am7M
Colhendo raios de lua
F#m7            Dm
Fazia cordas de prata

F#m7                 Dm    Dm7/C
Que se esticadas, vibravam
Dm6/B           G9/7 G7
O corpo da mulher nua
C6/G   Am/C       C6/G
E o artesão finalmente
Am/C              C6/G
Nesta mulher de madeira
C7(9)           F7M
Botou o coração
Fm6                    C G/B
E lhe apertou contra o peito
Am                    Am/D
E deu-lhe um nome bonito
      G6/B G7     F7+   Fm6  C7M
Assim nasceu o violão

----------------- Acordes -----------------
A5+ = X 0 3 2 2 0
Am = X 0 2 2 1 0
Am/C = X 3 2 2 5 X
Am/D = X X 0 2 1 0
Am7M = X 0 2 1 1 0
Bm7/F# = X X 4 4 3 5
C = X 3 2 0 1 0
C/G = 3 3 2 X 1 X
C6/G = 3 X 2 2 1 X
C7(9) = X 3 2 3 3 X
C7M = X 3 2 0 0 X
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Dm7/C = X 3 0 2 1 1
E7 = 0 2 2 1 3 0
F#m7 = 2 X 2 2 2 X
F7+ = 1 X 2 2 1 X
F7M = 1 X 2 2 1 X
Fm6 = 1 X 0 1 1 X
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G6/B = 7 5 5 7 5 X
G7 = 3 5 3 4 3 3
G9/7 = 3 X 3 2 0 X
