Ana Carolina - Heroína e Vilã

Intro: Am7

  Am7                              Dm7
Estenda no chão o tapete que eu quero passar
Gsus4         G7(b9)           C7M/9      F#7(#11)
Esqueça a razão deixe tudo pra me adorar
F7M              G#dim           Am7
Ponha em minha mão uma pedra bonita
B7                        E7            E7(#9) E7(b9)
Eu lhe segredo mentiras, você acredita

Am7                              Dm7
Não ouça, não, o que andam falando de mim
Gsus4         G7(b9)                 C7M/9      F#7(#11)
Por puro ciúme, despeito, inveja ou coisas assim
F7M              G#dim           Am7
Perca a noção, do perigo que espreita
B7                                E7
Eu faço a cama na lama e você se deita

 Dm7             Gsus4   C7M/9            F#7(#11)
 Você vai me seguir, sou sua heroína e vilã

 F7M               E7                   Am7   A7
 Viva comigo essa noite e esqueça o amanhã

Dm7             Gsus4   C7M/9            F#7(#11)
 Você vai me seguir, sou sua heroína e vilã
 F7M               E7                   Am7   E7(#9)
 Viva comigo essa noite e esqueça o amanhã

Am7                              Dm7
Não diga não, aos caprichos de uma mulher
Gsus4         G7(b9)          C7M/9      F#7(#11)
Preste atenção se você realmente me quer
F7M           G#dim           Am7
Em compensação, vou mudar sua vida
B7                              E7
Mas se você não quiser já estou de saida

 Dm7             Gsus4   C7M/9            F#7(#11)
 Você vai me seguir, sou sua heroína e vilã
 F7M               E7                   Am7   A7
 Viva comigo essa noite e esqueça o amanhã

Dm7             Gsus4   C7M/9            F#7(#11)
 Você vai me seguir, sou sua heroína e vilã
 F7M               E7                   Am7   E7(#9)
 Viva comigo essa noite e esqueça o amanhã

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
C7M/9 = X 3 2 4 3 X
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
E7(#9) = X 7 6 7 8 X
E7(b9) = X X 2 1 3 1
F#7(#11) = 2 X 2 3 1 X
F7M = 1 X 2 2 1 X
G#dim = 4 X 3 4 3 X
G7(b9) = 3 X 3 1 0 X
Gsus4 = 3 5 5 5 3 3
