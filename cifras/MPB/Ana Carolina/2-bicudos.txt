Ana Carolina - 2 Bicudos

(intro 2x)  Dm7 A/C#

Dm7
Quando eu te vi
        A/C# ('lá' em pestana)
Andava tão desprevenido
Dm7                   A/C#
Que nem ouvi tocar o alarme de perigo
Gm        C7/9        F      Bb
E você foi me conquistando devagar
                          A7/4      A7
Quando notei já não tinha como recuar
Dm7                     A/C#
E foi assim que nos juntamos distraídos
Dm7                   A/C#
Que no começo tudo é muito divertido
Gm          C7/9   F         Bb
Mas sempre tinha um amigo pra falar
                           A7/4         A7
Que o nosso amor nunca foi feito pra durar


(refrão)
Dm7                            Gm
Por mais que eu durma eu não descanso
                              C7/9
Por mais que eu corro eu não te alcanço
                      A7/4             A7
Mas não tem jeito eu não sei como esperar
       Gm              C7/9
Desesperar também não vou
     F              Bb
Não vou deixar você passar
     Bm7/11              Bb7/5-
Como água escorrendo nos dedos
   A7/4            A7
Fluindo pra outro lugar

Dm7                       A/C#
Ninguém pode negar que o nosso amor é tudo
Dm7                  A/C#
Tudo que pode acontecer com dois bicudos
Gm           C7/9        F           Bb
Não são tão poucas as arestas pra aparar
                            A7/4        A7
Mas é que o meu desejo não deseja se calar
Dm7                A/C#
Até os erros já parecem ter sentindo
Dm7                    A/C#
Não sei se eu trair primeiro ou fui traído
Gm       C7/9      F       Bb
Não te pedi uma conduta exemplar
                                   A7/4         A7
Mas é que sua ausência é o que me dói no calcanhar

(refrão)
Dm7                             Gm
Por mais que eu durma eu não descanso
                             C7/9
Por mais que eu corro eu não te alcanço
                       A7/4            A7
Mas não tem jeito eu não sei como esperar
Gm                     C7/9
Desesperar também não vou
    F               Bb
Não vou deixar você passar
      Bm7/11              Bb7/5-
Como água escorrendo nos dedos
    A7/4             A7
Fluindo pra outro lugar
Gm            C7/9
Será sempre será
         F            Bb
O nosso amor não morrerá
   Bm7/11              Bb7/5-
Depois que eu perdi o meu medo
     A7/4        A7
Não vou mais te deixar

----------------- Acordes -----------------
A/C# = X 4 X 2 5 5
A7 = X 0 2 0 2 0
A7/4 = X 0 2 0 3 0
Bb = X 1 3 3 3 1
Bb7/5- = X 1 2 1 3 X
Bm7/11 = 7 X 7 7 5 X
C7/9 = X 3 2 3 3 X
Dm7 = X 5 7 5 6 5
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
