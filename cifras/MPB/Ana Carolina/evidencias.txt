Ana Carolina - Evidências

Acompanhe que ela abafa as cordas, logo não toca as notas C9 e G/B por completo

Tom : C

           C9                  G/B
Quando eu digo que deixei de te    amar
       Am      Am7+
É porque eu te amo
           Dm7          Dm7+
Quando eu digo que não quero
 Mais você
        Dm                      E F F# G
É porque eu te quero
 G7
eu tenho medo de te dar meu
     G#º
Coração
         Am7
E confessar  que eu estou
Em tuas mãos
        Dm7        Dm7+
Mas não posso imaginar

O que vai ser de mim
           F/G      G7(b9)
Se eu te perder um dia

   C9                  G/B
Eu me afasto e me defendo
De você
       Am      Am7+
Mas depois me entrego
Dm7          Dm7+
Faço tipo, falo coisas que
Eu não sou
        Dm                      E F F# G
Mas depois eu nego
      G7
Mas a verdade é que sou
     G#º
Louco por você
                  Am7
E tenho medo de pensar
Em te perder
          Dm7                    F/G
Eu preciso aceitar que não dá
                G7(b9)                    C9
Mais pra  Separar as nossas vidas

C9
E nessa loucura de dizer
Que não te quero
       G
Vou negando as aparências,
Disfarçando as evidências
          F
Mas pra que viver fingindo
Se eu não posso enganar
       F/G
Meu coração
     G7/9
Eu sei     que te amo,

C9
Chega de mentiras
De negar o meu desejo
        G
Eu te quero mais que tudo
eu preciso do seu beijo
       F
Eu entrego a minha vida
Pra você   fazer o que quiser
     F/G           G7(b9)
De mim
Só  quero  ouvir você dizer
Que sim

  C9        C#m7+
Diz que é verdade que tem
 Dm7
Saudade
          F/G            G7/9
Que ainda você pensa muito
Em mim
  C9        C#m7+
Diz que é verdade que tem
 Dm7
Saudade
          F/G            G7(b9)
Que ainda você  quer
F
Viver pra mim


C9
E nessa loucura de dizer
Que não te quero
       G
Vou negando as aparências,
Disfarçando as evidências
          F
Mas pra que viver fingindo
Se eu não posso enganar
      F/G
Meu coração
     G7(b9)
Eu sei     que te amo,

C9
Chega de mentiras
De negar o meu desejo
        G
Eu te quero mais que tudo
eu preciso do seu beijo

 F
Eu entrego a minha vida
Pra você    fazer o que quiser
     F/G           G7(b9)
De mim
Só  quero  ouvir você dizer
Que sim

  C9        C#m7+
Diz que é verdade que tem
 Dm7
Saudade
         F/G             G7/9
Que ainda você pensa muito
Em mim
  C9        C#m7+
Diz que é verdade que tem
 Dm7
Saudade
          F/G            G7/9
Que ainda você  quer
F
Viver pra mim

C9

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Am7+ = X 0 2 1 1 0
C = X 3 2 0 1 0
C#m7+ = X 4 6 5 5 4
C9 = X 3 5 5 3 3
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Dm7+ = X X 0 2 2 1
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
F/G = 3 X 3 2 1 X
G = 3 2 0 0 0 3
G#º = 4 X 3 4 3 X
G/B = X 2 0 0 3 3
G7 = 3 5 3 4 3 3
G7(b9) = 3 X 3 1 0 X
G7/9 = 3 X 3 2 0 X
