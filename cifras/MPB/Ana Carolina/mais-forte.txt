Ana Carolina - Mais Forte

Em
Quando eu não falo
É aí que me entende
Am
Se tenta me soltar
É aí que me prende
D
E sigo sabendo de mim
                 Em      B7
pelas coisas que perco
Em
Te traço e encontro
Outra face que é minha
Am
Sem ter as respostas
Pra curvas ou linhas
D
E sigo sabendo de mim
                  Em      B7
Nas perguntas que calo


(Refrão)

Em
Sei que mudei
Sonhei, sorri, caí
          Am
Depois eu me levantei
Tudo que sofri
            C
Me fez mais forte, eu sei
B7                   Em      B7
Pronta pra sofrer de novo

Em
Eu rasgo os tecidos
Que lavam meus sonhos
Am
Eu ando enxergando
Por trás dos meus olhos
D
E sigo sabendo de mim
                  Em     B7
Pelas sombras que piso
Em
Entendo, me perco
Respondo e te traço
Am
Encontro, me calo
Te prendo e me rasgo
D
E sigo sabendo de mim
                    Em     B7
Pelas coisas que eu faço

(Refrão)

Repete a música toda.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
