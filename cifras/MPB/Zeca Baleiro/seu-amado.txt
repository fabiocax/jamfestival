Zeca Baleiro - Seu Amado

Am                          Dm
Baby, por você vou até a Croácia
                             E
O amor não se encontra em farmácia
                        Am
É remédio pra nossa loucura

Dm                              Am
Baby, o amor quando não mata engorda
                             F
Faz do homem gentil um calhorda
                       E
Torna a meretriz uma pura


Am                          Dm
Baby, o amor não tem preço nem prazo
                             E
Se você me quiser eu me caso
                        Am
No civil no católico até



Dm                              Am
Baby, vou te dar sacanagem e ternura
                        F
Te ofertar a minha rapadura
     E                  Am
Caviar, chocolate e rapé


Am                              E
Baby, por você eu encaro o Maguila
                         Bm
Bebo vodka, chopp e tequila
      C#m               F#m
Por você rasgo meu coração

Bm                               F#m
Baby, com você fica perto o infinito
                            D
Por você ganho o jogo no grito
                      C#
Venha clarear meu apagão


Am                                Dm
Baby, por você vou até o cú do Judas
                          E
Cruzo o triângulo das bermudas
                      Am
Corro até o Rally Dakar

Dm                               Am
Baby, minha vida é ópera sem graça
                            F
Uma flor murcha, morta na praça
                        E
Venha já me reger, me regar


Am                             E
Baby, por você eu enfrento o diabo
                       Bm
Bato no delegado e no cabo
         C#m               F#m
Eu não quero parecer o Paixão

Bm                            F#m
Baby, por você toda dor eu aguento
                        D
Vou até Bagdá num jumento
                          C#
Volto pra São Luiz num fuscão

Am                                 Dm
Baby, por você minha alma gela e treme
                  E
Sou o jabá da sua FM
                        Am
Sou a CUT e você o grevista

Dm                             Am
Baby, o cupido é que foi o culpado
                          F
Pode crer quero ser seu amado
       E                Am
Quero ser seu Amado Batista
       E                Am
(Deixa eu ser seu amado Batista)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C# = X 4 6 6 6 4
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
