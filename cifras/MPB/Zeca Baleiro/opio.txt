Zeca Baleiro - Ópio

  C                                               Am7
Eu não quero ver você cuspindo ódio
  C                                               Am7
Eu não quero ver você fumando ópio para sarar a dor
 F
Eu não quero ver você chorar veneno
 C9/F
Não quero beber o teu café pequeno
 Bb/D
Eu não quero isso seja lá o que isso for

  G#7+
Eu não quero aquele

Eu não quero aquilo
Am7+
Peixe na bocada um crocodilo
  D#9                              G4   G
Braço da Vênus de Milo acenando tchau

  C                           Am7
Não quero medir a altura do tombo

   C                            Am7
Nem passar agosto esperando setembro, se bem me lembro
   F
O melhor futuro este hoje escuro
  C9/F
O maior desejo da boca é o beijo
 Bb/D
Eu não quero ter o tejo me escorrendo das mãos

            G#7+
Quero a Guanabara, quero o Rio Nilo
  Am7+
Quero tudo ter, estrela flor estilo
D#9                               G4  G
Tua língua em meu mamilo água e sal

        Am7+      Em7
       Nada tenho vez em quando tudo
        F9                           Fm
       Tudo quero mais ou menos quanto
        Am7+     Em7
       Vida vida noves fora zero                     (2x)
           F9.                         Fm
       Quero viver, quero ouvir, quero ver

(repete tudo)

 F9                    Fm                C
Se é assim quero sim, acho que vim prá te ver

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Am7+ = X 0 2 1 1 0
Bb/D = X 5 X 3 6 6
C = X 3 2 0 1 0
C9/F = 1 3 2 0 3 0
D#9 = X 6 8 8 6 6
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
F9 = 1 3 5 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
G#7+ = 4 X 5 5 4 X
G4 = 3 5 5 5 3 3
