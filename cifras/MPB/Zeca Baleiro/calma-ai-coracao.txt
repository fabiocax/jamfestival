Zeca Baleiro - Calma Aí, Coração

   Dm              A#
Eu já falei tantas vezes
  C            Am
E você nada de me ouvir
A#               Gm
Vão-se os dias, anos e meses
  A
E tudo que você sabe fazer é sentir
Dm                    A#
Quantas canções falam de você
C                    Am
Tantas paixões sem você não são
A#                    Gm
Não pare nunca pra eu não morrer
A
Nem voe tão mais além do chão

( Dm A# C Am A# Gm A )

Deixa, me deixa em paz, ó meu coração
Chega, o que liberta é também prisão

Deixa, deixa assim, só e salvo e são
Quem tanto bate um dia apanha
Chega de manha, não me assanha
Doido, louco, maluco coração
Dm                A#
Coração surdo não tem juízo
    C                Am
Não ouve nunca a voz da razão
  A#               Gm
E razão você sabe, é preciso
    A
Pra curar a sua loucura, coração
   Dm           A#
Bandido cansado de enganos
   C                 Am
Heróis de capa e espada na mão
  A#             Gm
Esquece metas, retas e planos
  A
Veleja no mar escuro da ilusão

( Dm A# C Am A# Gm A )

Deixa, me deixa em paz, ó meu coração
Chega, o que liberta é também prisão
Deixa, deixa assim, só e salvo e são
Quem tanto bate um dia apanha
Chega de manha, não me assanha
Doido, louco, maluco coração

----------------- Acordes -----------------
A = X 0 2 2 2 0
A# = X 1 3 3 3 1
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Gm = 3 5 5 3 3 3
