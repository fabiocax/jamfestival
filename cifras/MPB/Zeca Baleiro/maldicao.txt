Zeca Baleiro - Maldição

Tom :A

A            D/A     A      E/G#
Baudelaire, Macalé, Luiz Melodia
F#m        C#m7
Quanta maldição
F#m         C#m7   D7M   D#º
O meu coração não quer dinheiro
        E7/4  E7
Quer poesia
A            D/A     A      E/G#
Baudelaire, Macalé, Luiz Melodia
(O resto da música corresponde aos mesmos acordes anteriores)
Rimbaud a missão
Poeta e ladrão
Escravo da paixão sem guia
Edgard Allan põe tua mão na pia
Lava com sabão
Tua solidão
Tão infinita quanto o dia
Vicentinho, Van Gogh, Luiza Erundina

Voltem pro sertão
Pra plantar feijão
Tulipas para burguesia
Baudelaire Macalé Luiz Melodia
Waly Salomão, Itamar Assumpção
O resto é perfumaria

----------------- Acordes -----------------
A = X 0 2 2 2 0
C#m7 = X 4 6 4 5 4
D#º = X X 1 2 1 2
D/A = X 0 X 2 3 2
D7M = X X 0 2 2 2
E/G# = 4 X 2 4 5 X
E7 = 0 2 2 1 3 0
E7/4 = 0 2 0 2 0 X
F#m = 2 4 4 2 2 2
