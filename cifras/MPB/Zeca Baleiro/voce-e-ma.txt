Zeca Baleiro - Você É Má

C#m                               G#/C
Vá se danar! Você dá nada a ninguém,
                                    A
Nenhum olhar, nunca falou: tudo bem!

Tem, mas não dá, sorrir jamais lhe convém,
     F#m  G#m A                       B7
Você é má, mas há de ter um bem!

C#m                             G#/C
Você dá nada a ninguém, vá se danar!
                                    A
Danada não perde o trem, sabe nadar,

Mas nada sabe de alguém que sabe amar,
     F#m  G#m                   A  B7
Eu quero ser seu bem, você é má!

E                             B/D#
Você é maluca, você é malina,
      C#m                    A   Am
Você é malandra, só não é massa!

    E                          B/D#
E você magoa, e você massacra,
     C            F#m   G#m   A    B7
E você machuca, e você mata!...

C#m                              G#/C
Vá se danar, você dá nada a ninguém,
                                      A
Nunca dará, nem mesmo um simples amém

A Deus dirá, diz que não vai à Belém,
  F#m  G#m           A            B7
Você é má, mas há de ter um bem!

C#m                               G#/C
Você dá nada a ninguém, vá se danar
                                   A
Danada finge tão bem, sabe negar

Jamais dará a quem tem demais pra dar,
F#m  G#m                   A    B7
Mas eu serei seu bem, você é má!

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B/D# = X 6 X 4 7 7
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#/C = X 3 X 1 4 4
G#m = 4 6 6 4 4 4
