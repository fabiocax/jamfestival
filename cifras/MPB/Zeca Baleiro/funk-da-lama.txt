Zeca Baleiro - Funk da Lama

Intro: Bm

Bm
Tanto faz se é Ivete ou Shakira,
Tanto faz se é Sá Rodrigues ou Guarabirá
        Em
Você vai ter que responder pelo que faz,
        C                                 Bm
Você vai ter que responder pelo que diz.

Bm
Tanto faz se é patrão ou se é pelego
Tanto faz se é Pelé ou se é Diego
         Em
Você vai ter que responder pelo que faz,
         C                                Bm
Você vai ter que responder pelo que diz.

Bm
Bota as mãos nas cadeiras
      C
Vai ate o chão com graça

    Bm
Sua moral do chão não passa.
Bm
Bota a mão nas cadeiras
      C
Dança com malemolência,
Bm
Bota a mão na consciência.

Bm            C             Bm
Vem cachorra, nem precisa de cama,
       Em                      C                Bm
O mundo tá atoladinho, o mundo tá atoladinho na lama
       Em                      C                Bm
O mundo tá atoladinho, o mundo tá atoladinho na lama
Bm            C             Bm
Vem cachorra, nem precisa de cama,
       Em                      C                Bm
O mundo tá atoladinho, o mundo tá atoladinho na lama
       Em                      C                Bm
O mundo tá atoladinho, o mundo tá atoladinho na lama

Bm
Tanto faz se é Demóstenes ou Palocci
Se é Fábio Melo ou Marcelo Rossi
        Em
Você vai ter que responder pelo que faz,
        C                                 Bm
Você vai ter que responder pelo que diz.

Bm
Tanto faz se é o Homem do Ano ou Mulher Pêra
Tanto faz se é Bolsonaro ou se é Gabeira,
        Em
Você vai ter que responder pelo que faz,
        C                                 Bm
Você vai ter que responder pelo que diz.

Bm
Bota as mãos nas cadeiras
      C
Vai ate o chão com graça
    Bm
Sua moral do chão não passa.
Bm
Bota a mão nas cadeiras
      C
Dança com malemolência,
Bm
Bota a mão na consciência.

Bm            C             Bm
Vem cachorra, nem precisa de cama,
       Em                      C                Bm
O mundo tá atoladinho, o mundo tá atoladinho na lama
       Em                      C                Bm
O mundo tá atoladinho, o mundo tá atoladinho na lama
Bm            C             Bm
Vem cachorra, nem precisa de cama,
       Em                      C                Bm
O mundo tá atoladinho, o mundo tá atoladinho na lama
       Em                      C                Bm
O mundo tá atoladinho, o mundo tá atoladinho na lama

----------------- Acordes -----------------
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
Em = 0 2 2 0 0 0
