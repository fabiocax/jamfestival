Zeca Baleiro - Guru da Galera

Introdução: (Bm Em F#)

Bm
Deus
      Em                             Bm
me deixa ser guru dessa galera
     Em             F#                       Bm
vê que tá todo mundo à minha espera
G               Em             F#
para anunciar um novo fim
Bm
Deus
      Em                           Bm
quinze minutos de eternidade
        Em              F#           Bm
mais que a glória da posteridade
     G                                Em
às legiões e tribos desse mundo
                     F#
é o que virei anunciar


REFRÃO:
    Bm
Escuta
     Em
é o som do meu rebanho
             D                                        F#
que atravessa esse mar morto sem tamanho
   Em                                   F#
atrás de um rio jordão para lavar
      G           Em
As mágoas
                                        F#
que um banho vai tornar a alma pura
                                   Em
quem sabe um dia essa água cura
   F#                                         Bm
a sede de quem não quer se afogar

Introdução: (Bm Em F#)

Bm
Deus
    Em                                Bm
neguinho alucina com meu grito
        Em           F#                  Bm
meu coração hebreu fugiu do egito
   G               Em                 F#
e no meio do mar vermelho vai passar
Bm
Deus
  Em                                           Bm
nego quer um milagre em cada esquina
     Em                 F#             Bm
na praça a massa reza e desafina
   G
tua palavra santa minha boca canta
                   F#
para o fim louvar

REPETE REFRÃO:

      G           Em
as mágoas
                                        F#
que um banho vai tornar a alma pura
                                   Em
quem sabe um dia essa água cura
   F#                                         Bm
a sede de quem não quer se afogar

(Bm Em F#) Deus.....Deus.....Deus...

----------------- Acordes -----------------
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
