Zeca Baleiro - Boi do Dono

Em             Am Am/G
Morena toma o meu braço
            B7
minha dança meu passo
       B7/F              Em B7
é tudo que eu posso te dar
 Em                 D
morena leva o meu beijo
                   Am
meu carinho meu desejo
             B7  E7
meu amor meu maracá
  Am   Am/G                      Em
se eu pudesse eu te dava toda riqueza
                 B7
luxo glória e beleza
B7/F                  Em E7
remédio pra toda dor
          Am
ah eu te dava
                     Em
os leões do meu palácio

                     B7
tudo quanto é rima fácil
B7/F                    Em
meu jardim crivado de flor
                    B7
te dava a minha língua
             Em
e o meu coração
                    B7
se eu fosse dono do mar
        B7/F              Em
se eu fosse dono do maranhão
Em                        Am Am/G
morena toma este poema
                  B7
meu canto de siriema
B7/F              Em B7
meu doce de buriti
Em                  D
morena minha vida é tua
                     Am
prometo te dar a lua
              B7  E7
se a lua tu me pedir
Am   Am/G                         Em
se eu pudesse eu te dava meu sotaque
               B7
rolls-royce cadilac
B7/F         Em E7
camafeu e bibelô
           Am
ah eu te dava
                     Em
meu penacho de brincante
                   B7
brisa da maré vazante
                   Em
água fresca sombra e calor
                    B7
te dava a minha língua
             Em
e o meu coração
                     B7
se eu fosse dono do mar
        B7/F              Em
se eu fosse dono do maranhão

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am/G = 3 X 2 2 1 X
B7 = X 2 1 2 0 2
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
