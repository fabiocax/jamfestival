Zeca Baleiro - Babylon

(intro) Dm Eº

               Dm                                          Eº

E|------------0--0p1-------------              e|-----------0--0p3-------
B|---------3----------3----------              B|--------2----------2----
G|------2----------------2-------              G|------3--------------3--       (2x)
D|---0---------------------------              D|----2-------------------
A|-------------------------------              A|------------------------
E|-------------------------------              E|------------------------

(solo gaita - repete algumas vezes na musica - para violão)

E|---10------------------------------------------------------
B|------10------------------------10h11--11------------------
G|---------10----------------10h12----------12--11h10--------
D|------------12-------11h12---------------------------------
A|---------------10h12---------------------------------------
E|-----------------------------------------------------------

E|-----------------------------------------------------------
B|-----------------------------------------------------------
G|---10h11-11-10--------------------9h10-10-9----------------
D|----------------12----------11h12-----------12-11----------
A|-------------------12-10h12--------------------------------
E|-----------------------------------------------------------


Dm               Eº
baby i'm so alone
                        Dm     Eº
vamos pra babylon
Dm                          Eº
viver a pão-de-ló e moet chandon
                        Dm             Eº
vamos pra babylon    vamos pra babylon
Dm                                     Eº
gozar sem se preocupar com amanhã
                        Dm             Eº
vamos pra babylon   baby baby babylon

E|----------3-------------3------------3-------3--------3--
B|--------3----3--------3---3--------3--------3-------3----
G|------3--------3----3-------3----3--------3-------3------
D|----5-------------4------------3--------2-------1--------
A|---------------------------------------------------------
E|---------------------------------------------------------

      Gm/D                        Ebmaj7
comprar o que houver au revoir ralé
         Gm             Gm/F                  Eº
finesse s'il vous plait mon dieu je t'aime glamour
     Gm/D
manhattan by night
     Ebmaj7             Gm             Gm/F    Eº     C#dim
passear de iate nos mares do pacífico sul
Dm                          Eº
baby i'm alive like a rolling stone
                        Dm   Eº
vamos pra babylon
Dm                                 Eº
vida é um souvenir made in hong kong
                        Dm
vamos pra babylon babulon
 Eº
vamos pra babylon
Dm                                    Eº
vem ser feliz ao lado desse bon vivant
                        Dm         Eº
vamos pra babylon    baby baby babylon
     Gm/D                  Ebmaj7
de tudo provar champanhe caviar
  Gm            Gm/F                     Eº
scotch escargot rayban bye bye miserê
        Gm/D           Ebmaj7
kaya now to me o céu seja aqui
     Gm        Gm/F        Eº   C#dim
minha religião é o prazer
Dm
não tenho dinheiro pra pagar a minha ioga
Am
não tenho dinheiro pra bancar a minha droga
Gm
eu não tenho renda pra descolar a merenda
Eº                                 C#dim
cansei de ser duro vou botar minh'alma à venda
Dm
eu não tenho grana pra sair com o meu broto
Am
eu não compro roupa por isso que eu ando roto
Gm
nada vem de graça nem o pão nem a cachaça
E                                  Eb         Dm
quero ser o caçador ando cansado de ser caça

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C#dim = X 4 5 3 5 3
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
Eb = X 6 5 3 4 3
Ebmaj7 = X X 1 3 3 3
Eº = X X 2 3 2 3
Gm = 3 5 5 3 3 3
