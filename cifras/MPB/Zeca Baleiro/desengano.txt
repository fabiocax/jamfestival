Zeca Baleiro - Desengano

Intro: C9

C                      G/B
Toda vez que olho o desengano
       Am                         Am/G
Nas frases do canto fosco dessa juventude
Dm                E7
Vejo meu sorriso magro,
           F                C
Meu corpo suado se encarquilhar
E7
E quando franzo a testa,
     Fº             Am                Am/G
E sério suo o rosto cor de madrugada
    Dm                E7                      F
E quando me deprimo e curvo os ombros pra pensar
C
Penso nos martírios,
G/B               Am               Am/G
Todos os delírios loucos que vivenciamos
Dm                E7               F                C
E vejo por quanto anos nos aventuramos querendo voar

E7               Fº
Voar pra sair de perto,
          Am                Am/G
De todo deserto desses abandonos,
   Dm               E7              F
E constatando o desengano se despedaçar.
C
Desfeito em pedaços,
G/B              Am       Am/G
Sigo no encalço desse sonho
Dm                 E7
Vejo meu sorriso magro,
         F               C
Coração amargo se atrapalhar
   E7
Quando franzo a testa,
  Fº                 Am               Am/G
E sério suo o rosto cor de madrugada
   Dm                  E7                  F
Quando abro os olhos, olhos claros para o mar.

Solo 2x: Am Am/G F F

C
Penso nos martírios,
G/B               Am               Am/G
Todos os delírios loucos que vivenciamos
Dm                E7               F                C
E vejo por quanto anos nos aventuramos querendo voar
E7               Fº
Voar pra sair de perto,
          Am                Am/G
De todo deserto desses abandonos,
   Dm               E7              F
E constatando o desengano se despedaçar.
C
Desfeito em pedaços,
G/B              Am       Am/G
Sigo no encalço desse sonho
Dm                 E7
Vejo meu sorriso magro,
         F               C
Coração amargo se atrapalhar
   E7
Quando franzo a testa,
  Fº                 Am               Am/G
E sério suo o rosto cor de madrugada
   Dm                  E7                  F
Quando abro os olhos, olhos claros para o mar.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am/G = 3 X 2 2 1 X
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
Fº = X X 3 4 3 4
G/B = X 2 0 0 3 3
