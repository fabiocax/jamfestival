Zeca Baleiro - Você me faz parecer

Pois toda essa beleza que te veste
Vende meu coração que é teu espelho
Meu bem, é bem melhor que tudo posto

(solo)
(Am - Em - F - G)

(Am - Em - F - G)
Quando você pinta tinta nessa tela cinza
Quando você passa doce nessa fruta passa
Quando você entra mãe benta mora os pedaços
Quando você chega nega flor do mercado de pixe, flor de azevixe

(Refrão)

F                     G                     Em                    Am
Voce me faz parecer menos só, menos sozinho
F                     G                     Em                     E
Você me faz parecer menos pó, menos pozinho

(Am - Em - F - G)
Quando você fala bala no meu velho-0este

Quando você dança lança flecha estigmia
Quando você pousa mariposa flor na mesa
de sangue enxarca a camisa

(Refrão)

(Am - Em - F - G)
Quando você diz o que ninguém diz
Quando você quer o que ninguém quiz
Quando você usa blusa pra que eu possa ser de liz
Quando você arde ao arder a sua teia cheia de ardiz
Quando você faz a minha carne triste quase feliz

(Refrão)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
