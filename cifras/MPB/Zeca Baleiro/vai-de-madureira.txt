Zeca Baleiro - Vai de Madureira

Intro:

Em D (2x)
Segura!

Em D (2x)
Vai, vai, vai, vai, vai

Em D (4x)

           Em
Se não tem água Perrier, eu não vou me aperrear
      C
Se tiver o que comer, não precisa caviar
                  Am
Se faltar molho rosé, no dendê vou me acabar
        B
Se não tem Moet Chandon, cachaça vai apanhar

Em D (2x)


   Em
Esquece Ilhas Caiman, deposita em Paquetá
        C
Se não posso um Cordon Bleu, cabidela e vatapá
          Am
Quem não tem Las Vegas vai no bingo de Irajá
          B
Quem não tem Beverly Hills mora no BNH

Em D (2x)

         Em
Quem não pode, quem não pode
                      C
Nova York vai de Madureira

Quem não pode Nova York
                 Am
Vai de Madureira

Quem não pode Nova York
            B
Vai de Madureira

        C
Se não tem Empório Armani
       Am                       B
Não importa vou na Creuza costureira do oitavo andar
        C                      Am
Se não rola aquele almoço no Fasano (na 2ª, ANTIQUÁRIO)
                   B
Vou na vila, vou comer a feijoada da Zilá

         Em
Quem não pode, quem não pode
                      C
Nova York vai de Madureira

Quem não pode Nova York
                 Am
Vai de Madureira

Quem não pode Nova York
            B
Vai de Madureira

Em D (4x)

Repete toda a música
(atenção para as duas primeiras estrofes)

Em                      D
Só ponho Reebok no meu samba
                      C
Quando a sola do meu Bamba
               B
A sola do meu Bamba
          Em
Chegar ao fim

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
