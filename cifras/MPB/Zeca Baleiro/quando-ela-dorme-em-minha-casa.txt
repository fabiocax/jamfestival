Zeca Baleiro - Quando Ela Dorme Em Minha Casa

Intro: E F#m A E

E                   F#m
Quando ela dorme em minha casa
A                 E
O mundo acorda cantando
E                   F#m
Quando ela dorme em minha casa
A                 E
O mundo acorda cantando

G                   A
Sonhos de lata e de rosas
C                            E
Gritam no silêncio branco do muro
G                    A
Com o futuro em suas asas
C
Ela se vai
       B
Ela se foi


A                     B
E esses versos são lamentos
G                   A
Que eu deixo nas calçadas
C
Canções que inventam pedras sobre a fome
E
Aí escreverei teu nome
     F#m
No azul do firmamento
A              E
Onde a dalva apareceu
E
Aí escreverei teu nome
     F#m
No azul do firmamento
A              E
Onde a dalva apareceu

E F#m A E

E                   F#m
Quando ela dorme em minha casa
A                 E
O mundo acorda cantando
E                   F#m
Quando ela dorme em minha casa
A                 E
O mundo acorda cantando

G                   A
Sonhos de lata e de rosas
C                            E
Gritam no silêncio branco do muro
G                    A
Com o futuro em suas asas
C
Ela se vai
       B
Ela se foi

A                     B
E esses versos são lamentos
G                   A
Que eu deixo nas calçadas
C
Canções que atiram pedras sobre a fome
E
Aí escreverei teu nome
     F#m
No azul do firmamento
A              E
Onde a dalva apareceu
E
Aí escreverei teu nome
     F#m
No azul do firmamento
A              E
Onde a dalva apareceu
E
Aí escreverei teu nome
     F#m
No azul do firmamento
A              E
Onde a dalva apareceu

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C = X 3 2 0 1 0
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
