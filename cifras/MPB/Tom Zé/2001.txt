Tom Zé - 2001

Intro: D F G

 D                G
"Astronarta" libertado
      C             C#
Minha vida me ultrapassa
         D    A          D  A7  D
Em qualquer rota que eu faça
D                  G
Dei um grito no escuro
        C          C#
Sou parceiro do futuro
      D   A     D
Na reluzente galáxia

C#7         F#m
  Eu quase posso "palpar"
C#7         F#m
  A minha vida que grita
C            F
  Emprenha, se reproduz

E7          A
 Na velocidade da luz
C#          F#m
  A cor do céu me compõe
C#       F#m
  O mar azul me dissolve
C      F
 A equação me propõe
E7       A
Computador me resolve

C#7    F#m
Amei a velocidade
C#7       F#m
Casei com sete planetas
C           F
Por filho, cor e espaço
E7           A
Não me tenho nem me faço
C#7    F#m
A rota do ano-luz
C#7      F#m
Calculo dentro do passo
C         F
Minha dor é cicatriz
E7           A
Minha morte não me quis

C#7        F#m
Nos braços de dois mil anos
C#7       F#m
Eu nasci sem ter idade
C            F
Sou casado, sou solteiro
E7         A
Sou baiano e estrangeiro
C#7          F#m
Meu sangue é de gasolina
C#7      F#m
Correndo não tenho mágoa
C            F
Meu peito é de "sar" de fruta
E7        A
Fervendo no copo d'água

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
C = X 3 2 0 1 0
C# = X 4 6 6 6 4
C#7 = X 4 3 4 2 X
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
