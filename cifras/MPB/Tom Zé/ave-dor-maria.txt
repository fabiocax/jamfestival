Tom Zé - Ave Dor Maria

Coro das rezadeiras:

Ave Maria, cheia de graça, o Senhor é convosco, bendita sois vós entre as mulheres, bendito é o fruto do vosso ventre, Jesus.
Santa Maria, Mãe de Deus, rogai por nós, pecadores, agora e na hora de nossa morte.
Amém.

Em               Cm
Coro dos acusadores:    Mulher é o mal
                                                  B                      Em
Que Lúcifer bota fé.
                Cm
Quando achou
                                           B                    Em
Primeiro ovo do Cão
               G/F#
Ela chocou.
 Em               Cm
Cru, Belzebu
                                           B                    Em
Do rabo fez um pirão
                                       Cm
Foi o pão

                                           B                    Em
Que o diabo amassou
              G/F#
E ela assou.

   E         A
Mônica Sol-Musa:        Ave Maria!
           Gº                 Eº
Aqui por nós, Maria,
   B             E
Vem levantar a voz.
   A            Am                E
Tem misericórdia da mulher,
             B7
Nas aflições
                                 E
Que o homem cria contra nós.

            Em
Coro das mulheres:      De giz

                  Cm
Me cobris
                                 B                      Em
De tanta lama e ferida.
         Cm
Argüis
                                  B                     Em
O nada contra o nariz,
        G
Ó suicida,


Coro dos acusadores:    Mulher é o mal... etc

                                      Em
Coro das mulheres:      E vês
                Cm
Toda vez
                                          B                   Em
A tua morte plural
         Cm
Viuvez
                                          B                     Em
Procuras doce no sal,
                G
E nem me vês.


Coro dos acusadores:    Cru, Belzebu... etc

                                         Em
Coro das mulheres:      Essa mão,
                Cm
Tua senha
                                          B                     Em
Pela navalha da luz.
           Cm
E no credo
                                          B                         Em
A outra mão não te nego,
              G
Desce da cruz!

Desce da cruz!

        Desce da cruz!

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
Cm = X 3 5 5 4 3
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
Eº = X X 2 3 2 3
G = 3 2 0 0 0 3
G/F# = X X 4 4 3 3
Gº = 3 X 2 3 2 X
