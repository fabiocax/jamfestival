Tom Zé - Todos Os Olhos

No caso desta música, o acorde C7 é revezado regularmente com C7/G.

Intro: (tocar 4x; na 4ª, tocar apenas a primeira tablatura).

E|--7--8-----7--8----7--8--9- 
B|--------------------------- 
G|--------------------------- 
D|--------------------------- 
A|--------------------------- 
E|--------------------------- 

E|--8--------------| 
B|-----------9--8--| 
G|----10--9--------| 
D|-----------------| 
A|-----------------| 
E|-----------------|

De vez em quando

C7
De vez em quando todos os olhos se voltam prá mim

De lá de dentro da escuridão
Esperando e querendo que eu seja um herói

Que eu seja um herói

             F       (G7 G#7) A7
Mas eu sou inocente, eu sou inocente
           D7            G7
Eu sou inocente, eu sou inocente
          C7
Eu sou inocên (2x)

(Repetir introdução)

De vez em quando

C7
De vez em quando todos os olhos se voltam prá mim
De lá do fundo da escuridão
Esperando e querendo que eu saiba

Querendo que eu saiba

                    F       (G7 G#7) A7
Mas eu não sei de nada, eu não sei de nada
               D7                 G7
Eu não sei de nada, eu não sei de nada
               C7
Eu não sei de ná (2x)

(Repetir introdução)

De vez em quando

C7
De vez em quando todos os olhos se voltam prá mim
De lá do fundo da escuridão
Esperando que eu seja um Deus, querendo apanhar,
Querendo que eu bata, querendo que eu seja um Deus

Querendo que eu seja um Deus

                      F          (G7 G#7) A7
Mas eu não tenho chicote, eu não tenho chicote
                  D7                 G7
Eu não tenho chicote, eu não tenho chicote
                C7
Eu não tenho chicó

                 F       (G7 G#7) A7
Mas eu sou até fraco, eu sou até fraco
             D7                G7
Eu sou até fraco, eu sou até fraco
            C7
Eu sou até frá

(Repetir introdução)
Eu sou inocente!

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
C7 = X 3 2 3 1 X
C7/G = 3 X 2 3 1 X
D7 = X X 0 2 1 2
F = 1 3 3 2 1 1
G#7 = 4 6 4 5 4 4
G7 = 3 5 3 4 3 3
