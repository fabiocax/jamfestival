Tom Zé - O Anfitrião

      D               E    Cm
minha dor, você tem razão,

                   B
então não faça cerimônia
                A
sou a tua nova casa
                E      Cm
sou o teu anfitrião.

                  B
se recoste no meu ombro
                    A
se debruce nos meus olhos
                  E
se quiser me dê a mão.

Refrão:
G           Cm
eu chamei a dor
                   G
pra fazer um samba triste

                   Cm
e pedir que me arrumasse
     D        A  E
um amor e uma mágoa.
G            Cm
ela bateu na porta,
                G
combinou tudo comigo
                 Cm
depois me disse adeus,
     D        A  E
um amor e uma mágoa.


minha dor, desta vez é pior,
depois que você foi embora
reparei dentro do peito
um vazio anormal.

nem aquele amor que nunca tive,
nem a mágoa que criamos,
somente a morte ou coisa igual.

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
