Tom Zé - Irene


G        D7
Eu quero ir minha gente
            G
Eu não sou daqui
 C            D7
Eu não tenho nada, nada
 C         D7
Quero ver Irene rir
 C         D7             G    D# C G
Quero ver Irene dar sua risada
 G          D7   C        D7
Irene ri, Irene ri, Irene ri
 C     D7  C    D7   C    D7
Irene ri, Irene ri, Irene ri
 C         D7              G  D# C G
Quero ver Irene dar sua risada
...
G
Quero ver Irene
 D#
Quero ver Irene

 C              D7        G
Quero ver Irene dar sua risada

----------------- Acordes -----------------
C = X 3 2 0 1 0
D# = X 6 5 3 4 3
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
