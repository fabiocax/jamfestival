Tom Zé - Canção de Nora (Casa de Bonecas)

        Dm
Homem do Gênesis:       Sobre o abismo pairava
   E
Deus:
      Gm                                 A7
O homem era um dos aliados
   Dm
Seus.
                A
Era de se ver,
                 Dm
Era de se ver.

                                        Dm
Mas Nora ignora os poderes
     E
Reais,
        Gm                      A7
O chicote, a espada e suas leis
         Dm
Morais.

                A
Era de se ver,
                Dm
Era de se ver.

                                                Gm
E quando decide escrever
                       Gm
O seu próprio roteiro,
                     A7
Quebrar as correntes
                     Dm
Do secular cativeiro,

                 E
Então ela pede
                 A
Às forças do sangue
   Dm
Valia
                       E
E logo a sala se torna,
                 A
Da sua pessoa,
       D
Vazia.

                         A
Coro de Ibsen:  Na hora em que Nora
                                G
Sai, bate a porta
                A
Abre-se um vão
                                        D                  G
O céu quase aborta
                   A
A lei que era morta
              D
Cai no porão.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
