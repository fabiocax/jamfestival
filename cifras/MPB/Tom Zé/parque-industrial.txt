Tom Zé - Parque Industrial

Introdução: Dm Bb Dm Bb

Dm         Bb       Dm             Am      G
Retocai o céu de anil, bandeirolas no cordão
         G               A
Grande festa em toda a nação
Dm           Bb     Dm           Am     G
Despertai com orações o avanço industrial
       G               A
Vem trazer nossa redenção.

                 F   C
Tem garotas-propaganda

Aeromoças e ternura no cartaz,
F        C        F
Basta olhar na parede,
 C         F
Minha alegria
        Bb         C
Num instante se refaz

F                F      Bb   F
Pois temos o sorriso engarrafado
         E          A
Já vem pronto e tabelado
                 G      A
É somente requentar e usar,
                 G      A
É somente requentar e usar,
          A    G      A   (A G)
Porque é made, made, made,
A  (A G A)  D
Made in Brazil
          A    G      A   (A G)
Porque é made, made, made,
A  (A G A)  D (Dm A)
Made in Brazil

A G A G

Dm         Bb       Dm             Am      G
Retocai o céu de anil, bandeirolas no cordão
         G               A
Grande festa em toda a nação
Dm           Bb     Dm           Am     G
Despertai com orações o avanço industrial
       G               A
Vem trazer nossa redenção.
                 F   C
A revista moralista
 F          Bb         C   Dm    A
Traz uma lista dos pecados da vedete
F        C        F
E tem jornal popular que
 C         F            Bb       C
Nunca se espreme porque pode derramar.
F        C       F      Bb     F
É um banco de sangue encadernado
         E          A
Já vem pronto e tabelado
                 G      A
É somente folhear e usar,
                 G      A
É somente folhear e usar,
          A    G      A   (A G)
Porque é made, made, made,
A  (A G A)  D
Made in Brazil
          A    G      A   (A G)
Porque é made, made, made,
A  (A G A)  D
Made in Brazil

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
