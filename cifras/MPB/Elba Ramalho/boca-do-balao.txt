Elba Ramalho - Boca do Balão

Cm
Na cidade grande
Por mais que eu ande
Ainda me espanto
                      F
Quando ouço uma explosão
           Bb                   D#
Lá no interior sempre era São João
           Bb                   D#
Lá no interior sempre era São João
           Gm
Viva São João
          Cm
Meu carneirinho
            C7
Como te esperei
         Fm
Ano inteirinho
   Fm        Bb
Ao pé da fogueira
   D#       G#
Madeira, velame

      Fm             G              C# C
Que o nosso amor inflame a noite inteira
    Fm          Bb
Rasteiro ou no chão
   D#         G#
A gente se esquenta
  Fm           G         Cm
E arrebenta a boca do balão

Viva a São João

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C# = X 4 6 6 6 4
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
D# = X 6 5 3 4 3
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
Gm = 3 5 5 3 3 3
