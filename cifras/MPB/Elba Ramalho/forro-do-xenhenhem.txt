Elba Ramalho - Forró do Xenhenhém

Introd: G Cm G C7 Fm Cm G Cm Fm Cm G Cm

Cm                             G7
Morena forrozeira do cangote suado
                                Cm
Tô ficando arriado com você meu bem
                                    G7
Com esse rebolado teu corpinho fica mole
                               Cm
E nesse bole-bole, nesse vai e vem
             Cm            Fm
O coração da gente chega lateja
               G            Cm
E a gente só deseja passar bem

              Fm          Eb
Com você meu bem no xénhenhém    2X
          G             Cm
 No xenhenhém, no xenhenhém



Cm                                          G7
Quem foi esse inteligente que inventou o forró
                        Cm
E fez a morena levantar pó
            Bb              Cm
Ele é um artista, trabalhou bem
       G                   Cm
E a morena forrozeira é de quem
            G                        Cm
Estiver disposto pra ganhar no xenhenhém


(Cm G)

Xenhenhém, xenhenhém, xenhenhém(3x)

Vou fazer de tudo pra ganhar no xenhenhém

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
Eb = X 6 5 3 4 3
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
