Elba Ramalho - Riso Cristalino

E                              B
O Meu olhar não leva jeito de chorar
                            E
quando vê o teu sorriso derramar
                              B
esse sorriso cristalino de alegria
                               E
quando vejo que a praia deu no mar
                                    B
É melhor ser triste assim como eu estou
                                E
do que ser feliz na vida como estais
                              B
pois felicidade em mim é teu amor
                                   E
que é mais claro que uma noite de luar
                      G#          C#m
Quando a brisa desta noite te abraçar
                    G#        C#m
vai sentir o frio forte da paixão
                       F#            B
o meu braço abraça o corpo de outro amor

                       F#          B A G# F# E
quando vejo que essa praia deu no mar

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
G# = 4 3 1 1 1 4
