Elba Ramalho - Facilita

(Luiz Ramalho)


Intro.: F7 - C - F7 - C - F7 - C - F G - C

         F7                C
Comadre Joana sempre reparou
         F7               C
A minissaia que a filha tem
        F7                 C
O namorado se invocou também
          F7         C
E certo dia pra ela falou:
               E7                Am
Tua saia, Bastiana, termina muito cedo
                E7               Am
Tua blusa, Bastiana, termina muito tarde

              G          C
Mas ela respondeu: Oi, facilita
                    G         C
Pra dançar o xenhenhém, oi, facilita

                G            C
Pra peneirar o xerém, oi, facilita
                   G          C
Pra dançar na gafieira, oi, facilita
                  G             C
Pra mandar pra lavadeira, oi, facilita
                   G          C
Pra correr na capoeira, oi, facilita
                  G          C
Pra subir no caminhão, oi, facilita
                    G          C
Pra passar no ribeirão, oi, facilita

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
G = 3 2 0 0 0 3
