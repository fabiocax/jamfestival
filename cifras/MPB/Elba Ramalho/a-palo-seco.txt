Elba Ramalho - A Palo Seco

(Belchior)

Intro.: Am7 - Am7 - D7 - D7 - Am7 - Am7 - D7 - G7(sus4)

C7M                   Em7
Se você vier me perguntar por onde andei

      Dm7                   G7(sus4)  G7(b9)
No tempo em que você sonhava
C7M                   Em7
De olhos abertos lhe direi
   Dm7                   G7(sus4)  G#º7
Amigo, eu me desesperava
Am7                   D7
Sei, que assim falando pensas
                   G7(sus4)  G7           C  G/B
Que esse desespero é      moda em 76

Am7                   D7
Mas ando mesmo descontente


                   G7(sus4)   G7                                 C          E7(b9)
Desesperadamente eu   grito em português

Am7                   A/C#     D7(sus4)
Mas ando mesmo descontente

      Ab7(#11)      G7(sus4)   G7             C          G7(sus4)
Desesperadamente eu   grito em português


C                  Em7                           F            D/F#
Tenho 25 anos de sonhos e de sangue

            G7(sus4)
E de América do Sul

C                              Em7
Por força desse destino

                   F      D/F#
Um tango argentino

             G7(sus4)               G#º7
Me cai bem melhor que um blues
Am7                   D7
Sei, que assim falando pensas


                   G7(sus4)    G7                   C  G/B
Que esse desespero é        moda em 76


       Am7      C/G                   D/F#
Eu quero é que esse canto torto

                   G7(sus4)    G7                               C7M     E7(b9)
Feito faca, corte a        carne de vocês

       Am7     A/C#                 D7(sus4)
Eu quero é que esse canto torto

Ab7(#11)                   G7(sus4)    G7                          C7M     E7(b9)
Feito faca, corte a        carne de vocês

----------------- Acordes -----------------
A/C# = X 4 X 2 5 5
Ab7(#11) = 4 X 4 5 3 X
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
C/G = 3 3 2 X 1 X
C7M = X 3 2 0 0 X
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
D7(sus4) = X X 0 2 1 3
Dm7 = X 5 7 5 6 5
E7(b9) = X X 2 1 3 1
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
G#º7 = 4 X 3 4 3 X
G/B = X 2 0 0 3 3
G7 = 3 5 3 4 3 3
G7(b9) = 3 X 3 1 0 X
G7(sus4) = 3 5 3 5 3 X
