Elba Ramalho - Caravana

(Alceu Valença/Geraldo Azevedo)

Dm         Gm        Dm      C
Corra, não pare, não pense demais
  Dm         Am       Dm
Repare essas velas no cais
      Gm        Bb°     A7   Bb°
Que a vida é ciga.......na
A7   Bbº      A7    Dm
É    ca       ra    vana
  Gm       Dm      C
É pedra de gelo ao sol
  Dm         Am        Dm
Degelou teus olhos tão sós
    Gm          Bb°   A7 Bb° A7 Bb° A7
Num mar de água cla...ra
Dm    C       Gm     Em     Dm
La    lalala  lala   la      uê
      C       Gm     Em      Dm
La    lalala  lala   la      uê
   C      Gm   Em  Dm     C      Gm     Em   Dm
La lalala lala la  uê lalauê lalauê lalauê   la


C  Gm Em Dm ( 2x )

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bbº = X 1 2 0 2 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
Gm = 3 5 5 3 3 3
