Elba Ramalho - Chameguinho

(Cecéu)


Intro: A - DG - DG6 - F# - Bm - Em - F# - Bm

            Bm
Meu bem castiga
                F#m
Nesse passo miudinho
Faz aquele chameguinho
                     Bm
E não dê bola pra ninguém
                     F#m
Você é meu café com leite
                         Bm
É o queijo dentro do meu pão
     B                   Em
É o chão pra quem eu me deite
A                 D
Que ascende seu coração
    Bm             Em
Meu bem você me sacuda

     A
Me acuda
           D
E me dê um nó

F#               Bm
Mostre pra esse povo
             Em
Que você é quente
                 F#
Mostre pra essa gente
                Bm
Que você é meu xodó

              A               D
Quero ver o o povo todo se babando
       G          D
Quero ver você dançando
    Em            F#
Comigo num canto só

               Bm
Com aquele chamego
          Em
De fazer roer
                A
Pra todo mundo ver
                   D
E o chão levantar pó

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
