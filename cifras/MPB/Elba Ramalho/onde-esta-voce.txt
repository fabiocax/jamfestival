Elba Ramalho - Onde Está Você

(intro 2x) Gm C7 Gm Bb7+ C7 Gm C7

Gm     C7   Gm
Onde está você
     F   Eb   F      Bb7+
Apareça aqui pra me ver
         C7     Gm   C7
Eu vou gostar demais

Gm     C7     Gm
Sabes onde estou
   F     Eb      F     Bb7+
e nada mudou Venha me dizer
       C7    Gm   C7
onde você andou

Gm     C7   Gm
Onde está você
     F   Eb   F      Bb7+
Apareça aqui pra me ver
          C7     Gm   C7
Eu vou gostar demais


Gm     C7     Gm
Sabes onde estou
    F    Eb       F     Bb7+
e nada mudou Venha me dizer
       C7    Gm   C7
onde você andou

Gm           F        Eb
Eu andei sem te encontrar
          F     Dm7          C7      Gm
Em quase todo lugar eu perguntava por ti
            C7       Gm
Teus passos sempre segui
         F         Eb          D7       Gm
Querendo te encontrar só pra falar de amor

           C7      Gm
Frases que nunca falei
         C7        Eb
Carinhos que nunca fiz
  F7                Bb7+
Beijos que nunca te dei
Am7/-5   D7/+5    Gm
O amor que te neguei
      F            Eb
Mas agora quero te dar
       D7    Gm
E te fazer feliz

(intro 2x) Em A7 Em G7+ A7 Em A7

Em     A7   Em
Onde está você
     D   C   D      G7+
Apareça aqui pra me ver
          A7     Em   A7
Eu vou gostar demais

Em     A7     Em
Sabes onde estou
    D    C       D      G7+
e nada mudou Venha me dizer
       A7    Em   A7
onde você andou

Em     A7   Em
Onde está você
     D   C   D      G7+
Apareça aqui pra me ver
          A7     Em   A7
Eu vou gostar demais

Em     A7     Em
Sabes onde estou
    D    C       D      G7+
e nada mudou Venha me dizer
       A7    Em   A7
onde você andou

Em           D        C
Eu andei sem te encontrar
          D     Bm7          A7       Em
Em quase todo lugar eu perguntava por ti
            A7       Em
Teus passos sempre segui
         D         C          B7       Em
Querendo te encontrar só pra falar de amor

           A7      Em
Frases que nunca falei
         A7        C
Carinhos que nunca fiz
  D7                 G7+
Beijos que nunca te dei
F#m7/-5   B7/+5   Em
O amor que te neguei
  D            C
agora quero te dar
       B7    Em
E te fazer feliz

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am7/-5 = 5 X 5 5 4 X
B7 = X 2 1 2 0 2
B7/+5 = X 2 X 2 4 3
Bb7+ = X 1 3 2 3 1
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
D = X X 0 2 3 2
D7 = X X 0 2 1 2
D7/+5 = X 5 X 5 7 6
Dm7 = X 5 7 5 6 5
Eb = X 6 5 3 4 3
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F#m7/-5 = 2 X 2 2 1 X
F7 = 1 3 1 2 1 1
G7+ = 3 X 4 4 3 X
Gm = 3 5 5 3 3 3
