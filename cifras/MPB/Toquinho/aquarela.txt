Toquinho - Aquarela

[Intro] G  G/B  C  C/D
        G  G/B  C  C/D

Parte 1

   G         G/B
E|------------------------------------------|
B|-----------0------------0-----------------|
G|-0---0-2-0--------0-2-0-------------------|
D|---0------------0-------------------------|
A|--------------2---------------------------|
E|-3----------------------------------------|

Parte 2

   C         C/D
E|------------------------------------------|
B|------------0---1-0-----------------------|
G|-----0-2-0----0-----2---------------------|
D|---0--------0-----------------------------|
A|-3----------------------------------------|
E|------------------------------------------|


[Primeira Parte]

Parte 1

   G             G/B
E|------------------------------------------|
B|-------0--------------0-------------------|
G|-----0-0------------0-0-------------------|
D|---0-----0---0----0-----0---0-------------|
A|----------------2---------2---------------|
E|-3---------3------------------------------|

Parte 2

   C             C/D
E|----------------------0-------------------|
B|-------1------------1-1-------------------|
G|-----0-0----------0-----0---0-------------|
D|---2-----2---2--0---------0---------------|
A|-3---------3------------------------------|
E|------------------------------------------|

G                 G/B
  Numa folha qualquer
               C        C/D
Eu desenho um sol amarelo
G                      G/B
  E com cinco ou seis retas
           C           C/D
É fácil fazer um castelo
G                   G/B
  Corro o lápis em torno da mão
      C         C/D
E me dou uma luva
G               G/B
  E se faço chover com dois riscos
      C             C/D
Tenho um guarda-chuva

[Pré-Refrão]

Parte 1

   Em            Em/D
E|-------0--------------0-------------------|
B|-----0-0------------0-0-------------------|
G|---0-----0---0----0-----0---0-------------|
D|-2---------2----0---------0---------------|
A|------------------------------------------|
E|------------------------------------------|

Parte 2

   C             F7M
E|-------0--------------0-------------------|
B|-----1-1------------1-1-------------------|
G|---0-----0---0----2-----2---2-------------|
D|----------------3---------3---------------|
A|-3---------0------------------------------|
E|------------------------------------------|

Parte 3

   G             G/B
E|------------------------------------------|
B|-------0--------------0-------------------|
G|-----0-0------------0-0-------------------|
D|---0-----0---0----0-----0---0-------------|
A|----------------2---------2---------------|
E|-3---------3------------------------------|

Parte 4

   C             C/D
E|---------------------0-------------0------|
B|-------1-----------1-1-----------1-1------|
G|-----0-0---------0-----0---0---0-----0---0|
D|---2-----2---2-0---------0---0---------0--|
A|-3---------3------------------------------|
E|------------------------------------------|

Em                     Em/D
   Se um pinguinho de tinta
             C               F7M
Cai num pedacinho azul do papel
G                  G/B
  Num instante imagino
              C             C/D
Uma linda gaivota a voar no     céu

[Refrão]

Parte 1

   G             D/F#
E|------------------------------------------|
B|-------0--------------3-------------------|
G|-----0-0------------2-2-------------------|
D|---0-----0---0----0-----0---0-------------|
A|------------------------------------------|
E|-3---------3----2---------0---------------|

Parte 2

   C/E            C/D
E|----------------------0-------------------|
B|-------1------------1-1-------------------|
G|-----0-0----------0-----0---0-------------|
D|---2-----2---2--0---------0---------------|
A|------------------------------------------|
E|-0---------0------------------------------|

(Repete Parte 1 e Parte 2)

Parte 3

   G             B7
E|------------------------------------------|
B|-------0--------------4-------------------|
G|-----0-0------------2-2-------------------|
D|---0-----0---0----4-----4---4-------------|
A|----------------2---------2---------------|
E|-3---------3------------------------------|

Parte 4

   Em             A7
E|------------------------------------------|
B|-------0--------------2-------------------|
G|-----0-0------------1-1-------------------|
D|---2-----2---2----2-----2---2-------------|
A|----------------0---------0---------------|
E|-0---------0------------------------------|

Parte 5

   C/D
E|-------0--------0-------------------------|
B|-----1-1--------1-------------------------|
G|---0-----0---0--0-------------------------|
D|-0---------0----0-------------------------|
A|------------------------------------------|
E|------------------------------------------|

 G          D/F#
Vai voando,      contornando
    C/E        C/D
A imensa curva     norte-sul
 G          D/F#
Vou com ela      viajando
 C/E          C/D
Havaí, Pequim     ou Istambul
 G                B7              Em
Pinto um barco a vela branco navegando
               A7             C/D
É tanto céu e mar num beijo azul

G               D/F#
Entre as nuvens      vem surgindo
    C/E        C/D
Um lindo avião     rosa e grená
 G            D/F#
Tudo em volta      colorindo
 C/E           C/D
Com suas luzes     a piscar
 G          B7                Em
Basta imaginar e ele está partindo
          A7
Sereno e lindo
                 C/D
E se a gente quiser ele vai pousar

[Repete Intro] G  G/B  C  C/D

[Segunda Parte]

G                 G/B
  Numa folha qualquer
                 C           C/D
Eu desenho um navio de partida
G                   G/B
  Com alguns bons amigos
            C           C/D
Bebendo de bem com a vida
G                  G/B
  De uma América a outra
            C            C/D
Consigo passar num segundo
G                     G/B
  Giro um simples compasso
                  C         C/D
E num círculo eu faço o mundo

[Pré-Refrão]

Em              Em/D
   Um menino caminha
               C         F7M
E caminhando chega no muro
G                 G/B
  E ali logo em frente
                C
A esperar pela gente
            C/D
O futuro está

[Refrão]

  G        D/F#
E o futuro      é uma astronave
 C/E         C/D
Que tentamos     pilotar
 G            D/F#
Não tem tempo      nem piedade
 C/E         C/D
Nem tem hora     de chegar
 G           B7               Em
Sem pedir licença muda nossa vida
           A7               C/D
Depois convida a rir ou chorar

 G            D/F#
Nessa estrada      não nos cabe
 C/E            C/D
Conhecer ou ver     o que virá
G          D/F#
O fim dela      ninguém sabe
 C/E         C/D
Bem ao certo     onde vai dar
 G           B7              Em
Vamos todos numa linda passarela
            A7                C/D
De uma aquarela que um dia enfim

Descolorirá

[Final]

G                 G/B
  Numa folha qualquer
               C        C/D
Eu desenho um sol amarelo

Que descolorirá

G                      G/B
  E com cinco ou seis retas
           C           C/D
É fácil fazer um castelo

Que descolorirá
G                     G/B
  Giro um simples compasso
                  C         C/D
E num círculo eu faço o mundo

Que descolorirá

( G  G/B  C  C/D  G )

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
B7 = X 2 4 2 4 2
C = X 3 2 0 1 0
C/D = X X 0 0 1 0
C/E = 0 3 2 0 1 0
D/F# = 2 X 0 2 3 X
Em = 0 2 2 0 0 0
Em/D = X X 0 0 0 0
F7M = X X 3 2 1 0
G = 3 X 0 0 0 X
G/B = X 2 0 0 0 X
