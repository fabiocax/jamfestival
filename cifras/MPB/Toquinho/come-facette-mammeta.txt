Toquinho - Come Facette Mammeta

Dm
Quanno mammeta t'há fatta
                    A7
Quanno mammeta t'há fatta
Vuò sapé come facette?
                Dm
Vuò sapé come facette?
Pe'mpastà sti ccarne belle
                     A7
Pe'mpastà sti ccarne belle
Tutto chello che mettette?
                    D
Tutto chello che mettette?
Ciento rose 'ncappucciate
                   A7
Dint'a martola mmiscate
Latte e rrose rose e latte
                     D
Te facette 'ncopp 'o fatto
Nun c'è bisogno 'a zingara
              A7
P'addivinà cuncè

Come t'há fatto mammeta
                    Dm   A7
'O saccio meglio 'e te
     Dm
E pe'fasta vocca bella
                 A7
E pe'fasta vocca bella
Nun servette 'a stessa 'addosa
                          Dm
Nun servette 'a stessa 'addosa
Vuò sapé che 'nce mettette?
                     A7
Vuò sapé che 'nce mettette?
Mo te dico tuttocosa
                D
Mo te dico tuttocosa:
'Nu panaro chino chino
                       A7
Tutt' e fravule 'e ciardino
Mele zucchero e cannella
                       D
Te 'mpastaie sta vocca belle
Nun c'è bisogno 'a zingara
              A7
P'addivinà cuncè
Come t'há fatto mammeta
                    Dm    A7   Dm   A7   Bb7
'O saccio meglio 'e te
     Ebm
E pe'fa sti trezze d'oro
                     Bb7
E pe'fa sti trezze d'oro
Mamma toia s'appezzentette
                     Ebm
Mamma toia s'appezzentette
Bella mia tu qua'muneta?
                   Bb7
Bella mia tu qua'muneta?
Vuò sapé che 'nce servette?
                     Eb
Vuò sapé che 'nce servette?
'Na miniera sana sana
                  Bb7
Tutta fatta a filagrana
'Nce vulette pe'sti ttrezze
                     Eb
Ch'a vasà nun ce sta prezze
Nun c'è bisogno 'a zingara
              Bb7
P'addivinà cuncè
Come t'há fatto mammeta
                    Ebm   Bb7   Ebm
'O saccio meglio 'e te

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bb7 = X 1 3 1 3 1
D = X X 0 2 3 2
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
Ebm = X X 1 3 4 2
