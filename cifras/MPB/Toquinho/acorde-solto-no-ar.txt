Toquinho - Acorde solto no ar


D7+                    F#7                     G7+
   Quando do meu violão solto um acorde no ar
                              F#7           G7+
É como quem bebe um chopp num canto de bar
                 G#º D7+              B7/5+   B7
Ou quem sai do cinema com o amor de domingo
     E7/9              Em7/9       A7/13  D7+
Quem vai pra Bahia brincar no carnaval
                    F#7                     G7+
Quando do meu violão solto um acorde no ar
                         F#7           G7+
É como quem vê a vida no tempo passar
                   G#º  D7+            B7/5+   B7
Ou quem busca na esquina o amor do seu dia
     E7/9      Bb7 A7 D7+
Quem faz da alegria o seu quintal.

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7/13 = X 0 X 0 2 2
B7 = X 2 1 2 0 2
B7/5+ = X 2 X 2 4 3
Bb7 = X 1 3 1 3 1
D7+ = X X 0 2 2 2
E7/9 = X X 2 1 3 2
Em7/9 = X 7 5 7 7 X
F#7 = 2 4 2 3 2 2
G#º = 4 X 3 4 3 X
G7+ = 3 X 4 4 3 X
