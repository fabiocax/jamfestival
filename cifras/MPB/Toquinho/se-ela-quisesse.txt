Toquinho - Se Ela Quisesse

C7          F7+                   Gm7
   Se ela tivesse a coragem de morrer de amor
          C7/9                           F7+
Se não soubesse que a paixão traz sempre muita dor
          E7/4         E7         A7
Se ela me desse toda a devoção da vida
          D7/9                    G7     C7/9
Num só instante sem momento de partida

        F7+                      Gm7
Pudesse ela me dizer o que eu preciso ouvir
              C7/9                              Cm7        F7
Que o tempo insiste, porque existe um tempo que há de vir
          Bb7+        Bº            F/C
Se ela quisesse, se tivesse essa certeza
     A/C#         Gm/D
De repente, que beleza,
      C7            Cm7          F7
Ter a vida assim ao seu dispor
      Bb7+     Bº        F/C
Ela veria, saberia que doçura,

      A/C#          Gm/D
Que delícia, que loucura,
       C7          F7+
Como é lindo se morrer de amor

----------------- Acordes -----------------
A/C# = X 4 X 2 5 5
A7 = X 0 2 0 2 0
Bb7+ = X 1 3 2 3 1
Bº = X 2 3 1 3 1
C7 = X 3 2 3 1 X
C7/9 = X 3 2 3 3 X
Cm7 = X 3 5 3 4 3
D7/9 = X 5 4 5 5 X
E7 = 0 2 2 1 3 0
E7/4 = 0 2 0 2 0 X
F/C = X 3 3 2 1 1
F7 = 1 3 1 2 1 1
F7+ = 1 X 2 2 1 X
G7 = 3 5 3 4 3 3
Gm/D = X 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
