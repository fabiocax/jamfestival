Toquinho - A Tonga da Mironga do Kabuletê

(introdução)
e|-10--10----8--7--------8---7-------------10---8--7--|
B|-----------------10----------------7-8--------------|
G|-------------------------------7--------------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

G6/9     Am7     D7(13)     G6/9     Am7     D7/4     D7

G6/9     Am7     D7(13)     Bb7(13)     Eb7+     Am7

D7(9)

G7+                 Am7    D7(9)   G7+
   Eu  caio  de  bos -  sa
                   Am7   D7(9)   G7+
eu  sou  quem  eu  sou
                  Am7    D7(9)   G7+
eu  saio  da  fos -  sa
      G6              Am7   D7(9)
xin - gando  em  na - gô

G7+                      Am7    D7(9)
Você  que  ouve  e  não  fala
G7+                      F#m7(b5)   B7
você  que  olha  e  não  vê
Em7                     A7(13)   A7
eu  vou  lhe  dar  uma  pala
Em7        A7(13)            Am    D7(13)
você  vai  ter  que  apren - der
   G7+              Am7               D7(9)   G6/9   Am7   D7(13)
a  tonga  da  miron -  ga  do  ka  -  buletê
   G7+              Am7    G/B            Am7   D7(9)
a  tonga  da  miron -  ga  do  kabuletê
   G7+              Am7             D7(9)   Ab7+
a  tonga  da  miron -  ga  do  ka - buletê

G7+                  Am7    D7(9)   G7+
   Eu  caio  de  bos -  sa
                   Am7   D7(9)   G7+
eu  sou  quem  eu  sou
                  Am7    D7(9)   G7+
eu  saio  da  fos -  sa
      G6              Am7   D7(9)
xin - gando  em  na - gô
G7+                    Am7    D7(9)
Você  que  lê  e  não  sabe
G7+                      F#m7(b5)   B7
você  que  reza  e  não  crê
Em7                       A7(13)    A7
você  que  entra  e  não  cabe
Em7        A7(13)         Am    D7(13)
você  vai  ter  que  vi - ver
    G7+              Am7             D7(9)   G7+   Am7   D7(9)
na  tonga  da  miron -  ga  do  ka - buletê
    G7+              Am7    G/B           Am7   D7(9)
na  tonga  da  miron -  ga  do  kabuletê
    G7+              Am7            D7(9)    Ab7+
na  tonga  da  miron - ga  do  ka - buletê

G6/9    Am7     D7(13)     G6/9     Am7     D7     G6/9     Am7     D7(13)     Bb7(13)     Eb7+     Ab7(#11)

G7+                      Am7    D7(9)
Você  que  fuma  e  não  traga
G7+                        F#m7(b5)     B7
e  que  não  paga  pra  ver
Em7                     A7(13)    A7
vou  lhe  rogar  uma    praga
Em7         A7(13)       Am    D7(13)
eu  vou  é  mandar  você
     G7+              Am7          D7(9)     G7+     Am7       D7(9)
pra  tonga  da  miron -  ga  do  kabuletê
     G7+              Am7    G/B           Am7    D7(9)
pra  tonga  da  miron -  ga  do  kabuletê
     G7+              Am7             D7(9)    Ab7+
pra  tonga  da  miron -  ga  do  ka - buletê

(final)

G6/9     Am7     D7(13)     G6/9     Am7     D7     G6/9     Am7     D7(13)

Bb7(13)     Eb7+     Ab7(#11)     Gadd9

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7(13) = X 0 X 0 2 2
Ab7(#11) = 4 X 4 5 3 X
Ab7+ = 4 X 5 5 4 X
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
Bb7(13) = 6 X 6 7 8 X
D7 = X X 0 2 1 2
D7(13) = X 5 X 5 7 7
D7(9) = X 5 4 5 5 X
D7/4 = X X 0 2 1 3
Eb7+ = X X 1 3 3 3
Em7 = 0 2 2 0 3 0
F#m7(b5) = 2 X 2 2 1 X
G/B = X 2 0 0 3 3
G6 = 3 X 2 4 3 X
G6/9 = X X 5 4 5 5
G7+ = 3 X 4 4 3 X
Gadd9 = 3 X 0 2 0 X
