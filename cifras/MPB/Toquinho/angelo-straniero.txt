Toquinho - Angelo Straniero

Intro: D   Gm/D   D   Gm/D   D   Gm/D   D   Gm/D   A7/4   A7
D6/9
Stella dove sei?
         G6          G    D/F#
In quale notte brillerai
  Em7       A7       D6/9          A7(9/13)
E quale melodia ballerai con me?
D6/9                      G6         G     D/F#
Come una bugia, che cerca la sua verita'
   Em7         A7            D7/4       D7
Io trovero' la strada per raggiungerti
          G             A/G              F#m7   B7/9-
Nel mio viaggio affrontero' il mare e il cielo,
          Em7     A7         Am       D7/A
Giorni difficilli,   mille pericoli,
           G            A/G            F#m7
Saro' un pirata, un vagabondo, un forastiero,
       Cm6    E7(9)        A7(9)      D6/9
Un angelo straniero, prigioniero di felicita'.
Gm/D   D   Gm/D   A7/4   A7
D6/9
Dimmi cosa vuoi,

       G6           G    D/F#
Devo volare o stare giu'?
   Em7       A7         D6/9      A7(9/13)
In quale labirinto devo perdermi?
D6/9                    G6            G     D/F#
Parlano di te, come una terra che non c'e',
   Em7        A7           D7/4      D7
Ma io testardamente voglio crederci.
         G         A/G              F#m7     B7/9-
E la mia nave trovera' il tuo nascondiglio,
             Em7  A7        Am(add9)   C6/9
Sara' in un' isola   in una lacrima,
C7(#11)     G          A/G           F#m7
Sara' un puntino fermo li' sull' orizzonte,
         Cm6   E7(9)              A7(9)      D6/9
Un bacio sulla fronte, sara' un istante di felicita'.
Gm/D   D   Gm/D   D   Gm/D   D   Gm/D   D   Gm6/D   D4   D

----------------- Acordes -----------------
A/G = 3 X 2 2 2 X
A7 = X 0 2 0 2 0
A7(9) = 5 X 5 4 2 X
A7(9/13) = 5 X 5 6 7 7
A7/4 = X 0 2 0 3 0
Am = X 0 2 2 1 0
Am(add9) = X 0 2 4 1 0
B7/9- = X 2 1 2 1 X
C6/9 = X 3 2 2 3 3
C7(#11) = X 3 4 3 5 X
Cm6 = X 3 X 2 4 3
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D4 = X X 0 2 3 3
D6/9 = X 5 4 2 0 0
D7 = X X 0 2 1 2
D7/4 = X X 0 2 1 3
D7/A = 5 X 4 5 3 X
E7(9) = X X 2 1 3 2
Em7 = 0 2 2 0 3 0
F#m7 = 2 X 2 2 2 X
G = 3 2 0 0 0 3
G6 = 3 X 2 4 3 X
Gm/D = X 5 5 3 3 3
Gm6/D = X 5 5 3 5 X
