Toquinho - A Espingarda de Rolha

G                  C
Tenho uma família que só faz maldade,
Am                  G
É um perigo ao povo de qualquer cidade.
                  C
Meu avô é bravo e se chama canhão,
Bm              C              D     G
Quando vai pra guerra não tem compaixão.
C              Am              D     G
Quando vai pra guerra não tem compaixão.

Eu tenho uma tia, mulher do meu tio
Que é muito maldoso e se chama fuzil.
Ela é mais que feia, é assustadora:
È a tagarela da metralhadora.

D
De nossa família
A
Temos tanta mágoa,
Bm
Eu e meu maninho,

   C           G
O revolvinho d'água.
 B           A     G
Não matar e nem ferir.
              F    C
Só brincar e divertir:
C           D       G
Essa foi a nossa escolha.

 C               D         Bm
Meu maninho é o revolvinho d'água,
    Am         D         G
Eu sou a espingarda de rolha.

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
