Toquinho - Deixa acontecer

Toquinho / Vinicius de Moraes

[Intro:] Am Am(b13) Am(13) Am7 Am(13) Am(b13) Am

Am Am(b13)        Am(13)
Ah, não tente explicar
Am7          Dm7
Nem se desculpar
 G7           Em7(b5) A7
Nem tente esconder
Dm Dm(b13)   Dm(13) Dm(b13)
Se vem do coração,
Dm           Bm7(b5) E7
Não tem jeito, não,
            Am E7(b13) E7
Deixa acontecer

Am                    F7M Am
O amor é essa força incontida,
                  F7M C
Desarruma a cama e a vida,

              G/B     Gm/Bb A7 Dm
Nos fere, maltrata e seduz
                    Bb Dm
É feito uma estrela cadente
                    Bb F7M
Que risca o caminho da gente,
             F#º     Cº Bm7(b5) E7(4) E7
Nos enche de força e de luz

Vai debochar da dor
Sem nenhum pudor
Nem medo qualquer
Ah, sendo por amor,
Seja como for
E o que Deus quiser

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Am(b13) = 5 8 7 5 6 5
Am7 = X 0 2 0 1 0
Bb = X 1 3 3 3 1
Bm7(b5) = X 2 3 2 3 X
C = X 3 2 0 1 0
Cº = X 3 4 2 4 2
Dm = X X 0 2 3 1
Dm(b13) = X X 0 3 3 1
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
E7(4) = 0 2 0 2 0 X
E7(b13) = 0 X 0 1 1 0
Em7(b5) = X X 2 3 3 3
F#º = 2 X 1 2 1 X
F7M = 1 X 2 2 1 X
G/B = X 2 0 0 3 3
G7 = 3 5 3 4 3 3
Gm/Bb = 6 5 5 3 X X
