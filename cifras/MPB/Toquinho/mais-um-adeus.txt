Toquinho - Mais Um Adeus

Em    D#dim  G7/D  Dbdim  C7M  B7/4  B7   Dm6/F
Mais  um    adeus    uma se...pa  ....... ração
E7     A7(13) A7(b13) D7/4  D7     G7(13)  G7(b13)
Outra vez                 so....li.......dão
 C7M   C#dim  C7     B7
Outra vez sofrimen...to
Em    D#dim  G7/D  Dbdim      C7M  B7/4 B7 Dm6/F
Mais  um    adeus    que não pode  esperar


E7(b9)  Am7   B7    Em  Em/D
O   amor é uma agonia
Dbdim   B7(#5) Dm6/F
Vem de noite   vai de dia
  E7(b13)  Am7 B7       Em  Em/D
É uma   alegria e de repente
      C7(9)  B7   Em  B7
Uma vontade  de chorar
Em              D#dim              G7/D
Olha benzinho cuidado com o seu resfriado
            Dbdim           C7M
Não pegue sereno, não tome gelado



B7/4   B7      Dm6/F              E7
O gim é um veneno cuidado benzinho  não beba demais
A7(13) A7(b13) D7/4  D7  G7(13) G7(b13)
Se guar..... .      de  para mim
C7M  C#dim         C7      B7
A ausência é um sofrimen...to
Em              D#dim                G7/D
E se tiver um momento me escreva um carinho
            Dbdim          C7M
E mande o dinheiro pro apartamento
            B7/4        B7     Dm6/F
Porque o vencimento não é como eu
           E7
Não pode esperar


Am7  B7    Em  Em/D
O amor é uma agonia
Dbdim  B7(#5)  Dm6/F
Vem de noite   vai de dia
  E7(b13) Am7  B7       Em
É uma    alegria e de repente
 Em/D   C7(9)   B7  Em
Uma   vontade  de chorar

----------------- Acordes -----------------
A7(13) = X 0 X 0 2 2
A7(b13) = X 0 X 0 2 1
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
B7(#5) = X 2 X 2 4 3
B7/4 = X 2 4 2 5 2
C#dim = X 4 5 3 5 3
C7 = X 3 2 3 1 X
C7(9) = X 3 2 3 3 X
C7M = X 3 2 0 0 X
D#dim = X X 1 2 1 2
D7 = X X 0 2 1 2
D7/4 = X X 0 2 1 3
Dbdim = X 4 5 3 5 3
Dm6/F = 1 X 0 2 0 X
E7 = 0 2 2 1 3 0
E7(b13) = 0 X 0 1 1 0
E7(b9) = X X 2 1 3 1
Em = 0 2 2 0 0 0
Em/D = X X 0 4 5 3
G7(13) = 3 X 3 4 5 X
G7(b13) = 3 X 3 4 4 3
G7/D = X X 0 0 0 1
