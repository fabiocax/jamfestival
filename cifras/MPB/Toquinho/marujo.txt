Toquinho - Marujo

Intr.: F C G C  (4x)
                  Dm          G                C     Am
Prepara o barco, ô Senhor, que é tempo.
                  Dm        G        C
Recolhe a corda que vai zarpar.
                 Dm         G                C     Am
Levanta a vela, ô Senhor, que o vento
                Dm          G          C
Sabe o caminho que vai pro mar.

                                       G
Lustra o leme, encera o mastro,

Lava esse convés.
                                    C
Cobre o bote, guarda a bóia

Pra não ter revés.
                                  G
Olha o mapa, segue o rumo,


Foge das marés,
      C
Marujo.
       Dm                                    G
Atenção que eu também fui do mar,
         C    Am
E eu fujo.
           Dm                        G                          C
E que nosso Senhor te proteja pro barco voltar.

                           G
Vara o dia, vira a noite,

Vara a viração.
                              C
Leva a vida, joga a sorte

Nessa embarcação.
                                G
Deixa a morte, foge dela,

Dessa assombração,
      C
Marujo.
        Dm                                   G
Atenção que eu também fui do mar
         C    Am
E eu fujo.
          Dm                         G                         C
E que nosso Senhor te proteja pro barco voltar.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
