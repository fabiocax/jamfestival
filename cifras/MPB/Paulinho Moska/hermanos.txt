Paulinho Moska - Hermanos

Intro: G  D/F#  Em  Em/D  C7M  G/B  Am7  D7

G            D/F#      Em                  Bm            C7M
Sempre que a vida estiver     Dentro de um corredor qualquer
           G/B       A4 A7                 D4(7) D7
Quando seu dia escurecer     Logo pela manhã
G        D/F#      Em              Bm        C7M
Você não deve desistir     A rua é feita pra sair
   G/B           A4 A7                     D4(7) D7
E renovar o seu amor     Sempre que precisar

Em       B9       C9       B4/C#          G
É agora… É pra já… Não demora, vamos lá… Cantar
Ebº      F#º      Em          A4 A7
Essa canção que está no ar
Am7          Bm              C7M     B4/C#
Descobrir um novo sol    Nascendo devagar
Am7        D4     D        G
Vida pura, Pronta pra Brilhar…

             D/F#        Em                  Em/D        C7M
Sempre que o céu for desabar    Sobre seus ombros sem perdão
          G/B      Am7                  D7
Quando a noite esfriar     Fundo do coração
G            D/F#   Em              Em/D          C7M
Saiba que estarei aqui   Quando não tiver mais ninguém
          G/B           Am7                    D7               Em       Bm7
Segure a minha mão e me dá    A sua mão também…                 É agora… É pra já… 

A   E/G#  F#m  F#m/E D7M  A/C# Bm7 E7
Lá lá lá lá

A             E/G#        F#m                 F#m/E        D7M
E quando a montanha despencar    Quando uma estrela se apagar
     A/C#        Bm7                  E7
Se o pior acontecer    Lembre-se de crescer
A           E/G#     F#m           F#m/E        D7M
Quem conduz Água e Calor    Tudo floresce ganha cor
             A/C#         Bm7                  E7
Sempre que alguém puder regar    Sementes de amor

A   E/G#  F#m  F#m/E D7M  A/C# Bm7 E7
Lá lá lá lá

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A4 = X 0 2 2 3 0
A7 = X 0 2 0 2 0
Am7 = X 0 2 0 1 0
B4/C# = X 6 X 4 7 7
B9 = X 2 4 4 2 2
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
C7M = X 3 2 0 0 X
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D4 = X X 0 2 3 3
D4(7) = X X 0 2 1 3
D7 = X X 0 2 1 2
D7M = X X 0 2 2 2
E/G# = 4 X 2 4 5 X
E7 = 0 2 2 1 3 0
Ebº = X X 1 2 1 2
Em = 0 2 2 0 0 0
Em/D = X X 0 4 5 3
F#m = 2 4 4 2 2 2
F#m/E = X X 2 2 2 2
F#º = 2 X 1 2 1 X
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
