Paulinho Moska - Meu Nome É Saudade de Você

[Intro] D  Bm  G  Gm

D             D/C#               Bm
Ei, o tempo passa e eu não te vejo
                       G
Desde o nosso último encontro
       Gm        D     A7
Que só faço me perder

D               D/C#        Bm 
Fui soprando o vento do desejo
                  G
E cheguei mesmo a ponto
       Gm        D    
De tentar te esquecer

D7      G
Vem me matar
        Gm                 D
Que meu nome é saudade de você
D7     G
Vem me amar
      Gm                  D
Nunca some a vontade de te ver
      Bm         Em
Tento tanto entender
       Gm       D
E continuo sem saber
                  
         D                  D/C#         Bm
Porque o sol ainda ilumina nossas flores
                  G
Com a grama do jardim
     Gm           D    A7
Emoldurando o amanhecer

         D              D/C#       Bm  
Só que a luz brilhante dessas cores
                      G
Sem a chama que há em ti
        Gm       D    
Pode o amor anoitecer

D7      G
Vem me matar
        Gm                 D
Que meu nome é saudade de você
D7     G
Vem me amar
      Gm                  D
Nunca some a vontade de te ver
      Bm         Em
Tento tanto entender
       Gm       D
E continuo sem saber
         Bm         Em
Eu tento tanto entender
       Gm       D
E continuo sem saber

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D/C# = X 4 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
