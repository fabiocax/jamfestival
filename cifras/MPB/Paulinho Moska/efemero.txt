Paulinho Moska - Efêmero

Intr.: ( Bm7(9) / Em/G ) 6x


Bm7(9)                Em(9)/G
Se tu achas que não te dou valor
   Bm7(9)             G9(11+)
Que não demonstro o meu amor
 Em7(9)                  A7      D7M  C#m7(b5)  F#7(b13)
E que só penso no que é bom para mim
Bm7(9)           Em(9)/G
Saiba que tbm percebo em tua dor
 Bm7(9)          G9(#11)
A mesma vontade, o mesmo calor
 Em7(9)             A7          D7M  D7(9)  Ab7(11#)
Querendo o mundo inteiro só pra ti


    G7M             C7(13)
Mas não viemos do mesmo lugar
F7M             Bb7(13)
Não sabemos onde vamos chegar
Eb7M      D7/4(9)  G7(9/13)    C9
Bela é a diferen......ça do amar


Bm7(9)          Em(9)/G
Cada gota que me destes
Bm7(9)                    G9(#11)
E as homenagens que eu te preste
   Em7(9)                A7        D7M  C#m7(b5)  F#7(b13)
São todas juntas tão pequenas para nós
Bm7(9)              Em(9)/G
Pois somos como um onceano
Bm7(9)           G9(#11)
Executando nosso plano
Em7(9)         A7         D7M  D7(9)  Ab7(11#)
De ser banhado por outros Sóis


    G7M             C7(13)
São tantos interesses em comum
F7M                 Bb7(13)
Vamos tentar não esquecer nenhum
Eb7M          D7/4(9)            G7(9/13)
Porque por um momento conseguimos ser...
C/G
Um


Ab / Cm7(9) / Ab7M /

Cm7(9) / Ab7M     / Cm7(9)  / Ab7M     / Fm7(9)  / Bb7  / Eb7M / Dm7(b5) G7(b13) /
Cm7    / Ab7M(9)  / Cm7     / Ab7M(13) / Fm7(13) / Bb7  / Eb7M / Eb7(9) A7(11#)  /

Ab7M   / C#7(9) / F#7M   / B7(9) / E7M    / C#m7 F#7 /
Bm7(9) / Em/G   / Bm7(9) / Em/G  / Bm7(9) / Em/G     / Bm7(9) /


ACORDES:

Bm7(9) __________ 020222
Em/G ____________ 3x245x
Em(9)/G _________ 3x2454
G9(#11) _________ 3x0220
A7 ______________ x02020
D7M _____________ x54222
C#m7(b5) ________ x4545x
F#7(b13) ________ 2x233x
D7(9) ___________ x5455x
Ab7(11#) ________ 4x453x
G7M _____________ 3x4432
C7(13) __________ x3x355
F7M _____________ 1x2210
Bb7(13) _________ x1x133
Eb7M ____________ x65333
D7/4(9) _________ x55555
G7(9/13) ________ 3x3455
C9 ______________ 8 10 12 988
C/G _____________ 332010
Ab ______________ 4x111x
Cm7(9) __________ x3133x
Ab7M ____________ 4x5543
Fm7(9) __________ 131143
Bb7 _____________ x13131
Dm7(b5) _________ x5656x
G7(b13) _________ 3x344x
Cm7 _____________ x35343
Ab7M(9) _________ 4x5343
Ab7M(13) ________ 4x556x
Fm7(13) _________ 131131
Eb7(9) __________ x6566x
A7(11#) _________ 5x564x
Db7 _____________ x4344x
Gb7M ____________ 2x3321
B7(9) ___________ x2122x
E7M _____________ x76444
C#m7 ____________ x46454
F#7 _____________ 242322

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Ab = 4 3 1 1 1 4
Ab7M = 4 X 5 5 4 X
Ab7M(13) = 4 X 5 5 6 X
Ab7M(9) = 4 3 5 3 X X
B7(9) = X 2 1 2 2 X
Bb7 = X 1 3 1 3 1
Bb7(13) = 6 X 6 7 8 X
Bm7(9) = X 2 0 2 2 X
C#7(9) = X 4 3 4 4 X
C#m7 = X 4 6 4 5 4
C#m7(b5) = X 4 5 4 5 X
C/G = 3 3 2 X 1 X
C7(13) = X 3 X 3 5 5
C9 = X 3 5 5 3 3
Cm7 = X 3 5 3 4 3
Cm7(9) = X 3 1 3 3 X
D7(9) = X 5 4 5 5 X
D7/4(9) = X 5 5 5 5 5
D7M = X X 0 2 2 2
Dm7(b5) = X X 0 1 1 1
E7M = X X 2 4 4 4
Eb7(9) = X 6 5 6 6 X
Eb7M = X X 1 3 3 3
Em/G = 3 X 2 4 5 X
Em7(9) = X 7 5 7 7 X
F#7 = 2 4 2 3 2 2
F#7(b13) = 2 X 2 3 3 2
F#7M = 2 X 3 3 2 X
F7M = 1 X 2 2 1 X
Fm7(13) = 1 3 1 1 3 1
Fm7(9) = X X 3 1 4 3
G7(9/13) = 3 X 3 2 0 0
G7(b13) = 3 X 3 4 4 3
G7M = 3 X 4 4 3 X
G9(#11) = 3 0 0 0 2 3
G9(11+) = 3 0 0 0 2 3
