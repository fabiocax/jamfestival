Paulinho Moska - Sem dizer adeus

    A9                 F#    D9      D     E11  E
E|---7------5----4------2-----0------2--------------
B|---5------5----5------2-----3------3--------------
G|----------------------3-----2------2------3---2---
D|----------------------4-----0---0---------3---3---
A|---0---0----------------------------------3---3---
E|---------------5--------------------------0---0---



A9    F#m11
Eu
               D9                  E11         A9   F#m11
Chorei até ficar debaixo d'água, submerso por você
                D9              E11                   F#m11
Gritei até perder o ar que eu já nem tinha pra sobreviver
B7   E7sus4  E7
(Eu andei...)
A9  F#m11
Eu
                 D9          E11              A9   F#m11
Andei até chegar no último lugar pisado por alguém
                      D9          E11                  F#m11
Só pra poder provar o que era estar depois do final do além
B7    E7sus4  E7
(Eu andei...)
        F#m11          B7                  Abm7/b5
E cheguei exatamente onde algum dia
C#7/b9               F#m11  A9/E             B/Eb       Dm6
Você disse que partia         pra nunca mais voltar
               A/C#   Cdim7         Bm7          E11   A9
E eu já estava lá    a te esperar sem dizer adeus
(intro)
A9  F#m11
Eu
                D9           E11                         A9
Fiquei sozinho até pensar que estar sozinho é achar que tem
F#m11
alguém
              D9              E11              F#m11
Já me esqueci do que não fiz e o que farei pra te esquecer também?
B7  E7sus4  E7   A9  F#m11                    D9
Se   eu                não sei         o nome do que sinto
             E11                 A9    F#m11
Não tem nome que domine o meu querer
                 D9                     E11          F#m11
Não vou voltar atrás, o chão sumiu a cada passo que eu dei
B7  E7sus4 E7
(Andei...)
F#m11     B7                      Abm7/b5
E cheguei exatamente onde algum dia
C#7/b9            F#m11  A9/E             B/Eb        Dm6
Você disse que partia       pra nunca mais voltar
        A/C#      Cdim7   Bm7       E11    G6(9) A9 G6(9) A9
E eu já estava lá    a te esperar sem dizer adeus

 B7                           F#m11
                            
E|---2-------              E|---x-------
B|---0-------              B|---0-------           
G|---2-------              G|---3-------
D|---1-------              D|---4-------            
A|---2-------              A|---4-------
E|-----------              E|---2-------    


 E7sus4                      E7
                            
E|---x-------              E|----x------
B|---0-------              B|----0------            
G|---2-------              G|----1------
D|---0-------              D|----0------           
A|---2-------              A|----2------ 
E|---0-------              E|----0------    

 Abm7/b5                     C#7/b9
                         
E|---x-------              E|-----------
B|---3-------              B|----3------            
G|---4-------              G|----4------
D|---4-------              D|----3------            
A|---x-------              A|----4------
E|---4-------              E|-----------    

  A9/E                        B/Eb 

E|-----------              E|----7------
B|---0-------              B|----7------            
G|---2-------              G|----4------
D|---2-------              D|-----------            
A|---x-------              A|----6------
E|---0-------              E|-----------    

  Dm6                        A/C# 

E|---5-------              E|----5------
B|---6-------              B|----5------            
G|---4-------              G|----2------
D|-----------              D|-----------            
A|---5-------              A|----4------
E|-----------              E|-----------    

  Cm7                         Bm7

E|---3-------              E|----2------
B|---4-------              B|----3------            
G|---3-------              G|----2------
D|---5-------              D|----4------            
A|---3-------              A|----2------
E|-----------              E|-----------
  G6(9)

E|---5-------
B|---5-------
G|---4-------
D|---5-------
A|-----------
E|-----------

----------------- Acordes -----------------
A/C# = X 4 X 2 5 5
A9 = X 0 2 2 0 0
A9/E = 0 X 2 2 0 0
Abm7/b5 = 4 X 4 4 3 X
B/Eb = X 6 X 4 7 7
B7 = X 2 1 2 0 2
Bm7 = X 2 4 2 3 2
C#7/b9 = X 4 3 4 3 X
Cdim7 = X 3 4 2 4 2
Cm7 = X 3 5 3 4 3
D = X X 0 2 3 2
D9 = X X 0 2 3 0
Dm6 = X 5 X 4 6 5
E = 0 2 2 1 0 0
E11 = 0 2 2 2 0 0
E7 = 0 2 2 1 3 0
E7sus4 = 0 2 0 2 0 X
F# = 2 4 4 3 2 2
F#m11 = 2 2 4 2 2 2
G6(9) = X X 5 4 5 5
