Roberto Carlos - Tentativa

  Em                 B/D#      G/D
Na tentativa de esquecer você
                        A/C#  C
Já fiz de tudo o que podia
                      G     A7
E não achando uma solução
                                  D
Me vi perdendo as rédeas do meu coração

 Em                              B/D#  
E me encontrei te amando simplesmente
      G                         A
Com aquele mesmo amor de antigamente
     C                       G
Do qual não consegui me afastar
     A7                 D
Te amei ainda mais, querendo não te amar

   Bb                         Am7(b5)   D7
Andei por uma estrada muito longa e pensei
  Gm                  Fm7   Bb7    Eb
Que um outro alguém podia aparecer
  D7                        Gm   Gm/F
E de repente nesse meu caminho
     C7                   F7
Um novo amor pudesse acontecer

     Bb                       Am7(b5)        D7
Mas não, o meu amor é muito grande agora eu sei
  Gm             Fm7   Bb7     Eb
E não me deixa mesmo te esquecer
 D                             Gm  Gm/F
Não sei o que fazer da minha vida
    C                     F7  Bb  Am7(b5) D7 G C F#m(b5) B7 
Se pra viver preciso de você

 Em                     B/D#  G
E cada vez que eu te encontro
                     A  C
Você parece mais bonita
                      G
Modificando os meus planos
 A7                          D7
De não querer você na minha vida

 Em                   B/D#     G/D
Na tentativa de esquecer você
                       A/C#  C  G  A7  D  Em
Já fiz de tudo que podia.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A7 = X 0 2 0 2 0
Am7(b5) = 5 X 5 5 4 X
B/D# = X 6 X 4 7 7
B7 = X 2 1 2 0 2
Bb = X 1 3 3 3 1
Bb7 = X 1 3 1 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Eb = X 6 5 3 4 3
Em = 0 2 2 0 0 0
F#m(b5) = 2 X 4 2 1 X
F7 = 1 3 1 2 1 1
Fm7 = 1 X 1 1 1 X
G = 3 2 0 0 0 3
G/D = X 5 5 4 3 X
Gm = 3 5 5 3 3 3
Gm/F = X X 3 3 3 3
