Roberto Carlos - Você Como Vai

Introdução: F G/F C/E Am7 Dm7 G7

          C
Tenho andado sem destino
      G/B
Sem carinho e sem amor
     Am7
E provado o gosto amargo
        Am7/G
O prato feito pela dor
            F
Você, como vai?
G/F         C/E
Você, como vai?
Am7         Dm7  G7
Você, como vai?
        C
Tenho tido a companhia
           G/B
De alguns amigos, e o meu cão
       Am7
Que inocente late forte
        Am7/G
Pra espantar a solidão
            F
Você, como vai?
G/F         C/E
Você, como vai?
Am7         Dm7  G7
Você, como vai?
            Eb  F
Você, como vive?
      Eb/G    C7  F
Onde você está?
 Bb        Eb      Gm
Quem te levou de mim?
          Cm  Bb Gm  G#
Quem tem você agora?
Eb  F        Eb/G  C7  F
E quem te telefona?
 Bb       Eb      Gm
Pra te falar de amor?
           Cm  Bb  Gm  G#
Quem te levou embora?
            Eb
Você, como vai?
F/Eb        Bb/D
Você, como vai?
Gm7          Cm7  F7
Você, como vai?
C
Hoje eu encontrei
          G/B
Seu nome no meu coração
                  Am7
Por mais que eu tente esquecer

             Am/G
Vive em meu peito essa paixão
            F
Você, como vai?
G/F          C/E
Você, como vai?
           Dm7  G7
Você, como vai?
           Eb     F
Você, como vive?
             Eb/G  C7   F
Quem vive em meu lugar?
 Bb        Eb        Gm
Quem faz você sorrir?
           Cm  Bb Gm   G#
E por quem você chora?
Eb  F               Eb/G   C7  F
E quem te enxuga as lágrimas?
 Bb           Eb   Gm
Quem não te deixa só?
            Cm  Eb  Gm G#
Quem te pergunta agora?
            Eb
Você, como vai?
F/Eb         Bb/D
Você, como vai?
Gm          Cm7  F7
Você, como vai?
            Eb
Você, como vai?
          F/Eb          Bb/D
O tempo não passou pra nós
        Gm        Cm7             F
Nada mudou, estou aqui, tudo é igual
            Eb
Você, como vai?
          F/Eb         Bb/D
Eu vou vivendo a vida só
         Gm             Cm7             F
O tempo passa, o que fazer, não te esqueci
            Eb
Você, como vai?
         F/Eb           Bb/D
O tempo não passou pra nós
       Gm7          Cm7          F         
Nada mudou, estou aqui tudo é igual
             Eb
Você, como vai?

----------------- Acordes -----------------
Am/G = 3 X 2 2 1 X
Am7 = X 0 2 0 1 0
Am7/G = 3 0 2 0 1 0
Bb = X 1 3 3 3 1
Bb/D = X 5 X 3 6 6
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
Cm7 = X 3 5 3 4 3
Dm7 = X 5 7 5 6 5
Eb = X 6 5 3 4 3
Eb/G = 3 X 1 3 4 X
F = 1 3 3 2 1 1
F/Eb = X X 1 2 1 1
F7 = 1 3 1 2 1 1
G# = 4 3 1 1 1 4
G/B = X 2 0 0 3 3
G/F = 1 X X 0 0 3
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
