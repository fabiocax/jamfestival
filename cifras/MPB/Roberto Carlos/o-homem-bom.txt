Roberto Carlos - O Homem Bom


G                 B7            Em
Vai como um vento solto numa campina
   C
Desliza na relva verde
        Am           D7
E vai subindo pela colina
G               B7
Todas as folhas secas
        Em              C
Viram tapete aqui neste chão
    G               D7
Nos pés desse homem bom
            C              G
Que só tem amor no seu coração
              B7              Em
Vê outra madrugada que vem chegando
C
Fala com os passarinhos
              Am             D7
Brinca com as flores vai meditando
G             B7
Ele é um mensageiro
      Em               C
Da alegria e jamais da dor
G            D7
Quer a felicidade
         C              G
Da humanidade seja onde for
             Em
Ele é uma pessoa
             Bm7
Que ama e perdoa e não vê a quem
C            G
Anda pelos caminhos
          A              D7
Levando a paz ajudando alguém
G              Em
Por todos os lugares
            Bm7
Cruzando os mares fazendo o bem
C              G
Ele é um homem bom
           D7               G
Distribui amor e tudo o que tem
                B7               Em
Cheio de amor é fé ele é nosso irmão
C
Aquele grande amigo
         Am             D7
Que no perigo estende a mão
G              B7
Tem no olhar a calma
           Em            C
Tem luz na alma e na sua voz
G                D7
Tem sempre uma palavra
          C                 G
De amor e paz pra dizer pra nós
              B7            Em
Sabe tudo que diz o livro sagrado
C
E tudo o que ele ensina
           Am              D7
Em seu coração ele tem guardado
G                B7
Quem sabe o nome dele
              Em              C
Se é Pedro ou Paulo ou se é João
   G                  D7
Só sei que é um homem bom
              C              G
Por que tem Jesus no seu coração

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
