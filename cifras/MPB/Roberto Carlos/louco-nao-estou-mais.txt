Roberto Carlos - Louco Não Estou Mais

Intro: C Am F G7 C Am F G7 C

G7  C
   Louco não estou mais
Malena eu esqueci
Am
Susi aquela ingrata nunca mais eu vi
F             G7                     C
  Vivo chorando e ninguém chora por mim
G7      C
  Meu beijo já não faz, não faz splish splash
  Am
Nunca mais chamei ninguém de baby meu bem
F              G7
  Vivo chorando
                     C   C7
E ninguém chora por mim
  F                C
Até Mr. Sandman me aconselhou
     F                   C
- Roberto tome jeito, arranje um amor
    F                C
Brotinho sem juízo deixe de ser
   D7                  G7
Senão em sua vida você vai sofrer.
 C
Linda foi namoro que pouco durou
Am
Triste e abandonado atualmente eu estou
F
  Vivo chorando
G7                     C  G7
  E ninguém chora por mim.
     C
Pertinho de onde eu moro
Tem um broto encantador
Am
   Linda,
     F
Oh, Linda
G7     C      G7
  Mr. Sandman
C         Am
Baby, meu bem
F     G7
Oh, Malena
C
  Splish, Splash
  F                C
Até Mr. Sandman me aconselhou...

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
D7 = X X 0 2 1 2
F = 1 3 3 2 1 1
G7 = 3 5 3 4 3 3
