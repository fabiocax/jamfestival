Roberto Carlos - Não Adianta Nada

Intro: D7/9+  G7  D7/9+

    D7/9+
Não adianta nada
Você querer fugir
E nem ficar grilada
Eu não sou de desistir

       G7
Não adianta andar fugindo
Continuar dizendo não

D7/9+
Pare de andar correndo
Por aí sem direção

              A7
Por que mais cedo ou mais tarde
     G7
Você vai cair 
              D7/9+
Aqui na minha mão
Conheço bem a vida
Você tem muito que aprender
Pode aprender comigo
Experimente só pra ver

        G7
É com você que estou falando
Acho bom você saber

   D7/9+
Eu vou ganhar você na raça
É a minha decisão

             A7
Porque mais cedo ou mais tarde
     G7                     D7/9+  G7 G#7 A7
Você vai cair aqui na minha mão

(Introdução)

  D7/9+
Você já me conhece 
E sabe como eu sou
Sabe onde ando
E os lugares onde vou

          G7
Por isso fique sempre perto
E cada vez estou mais certo

     D7/9+
Que você já sabe disso
E não vai dizer que não
             A7
Porque mais cedo ou mais tarde
     G7                     D7/9+  G7 G#7 A7
Você vai cair aqui na minha mão
    D7/9+
Não adianta nada
Não adianta nada

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
D7/9+ = X 5 4 5 6 X
G#7 = 4 6 4 5 4 4
G7 = 3 5 3 4 3 3
