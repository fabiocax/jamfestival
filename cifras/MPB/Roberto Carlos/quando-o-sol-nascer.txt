Roberto Carlos - Quando o Sol Nascer

Introdução: G7/#5 Cm7 F7 Bb Gm7 F7 Bb Gm7

G7/#5               Cm7
Você pensa que a verdade 
 F7                  Bb   Gm7
É o que vai na sua mente 
                      Cm7
E se esquece que no amor 
F7                Bb    Gm7
Tudo é muito diferente 
                     Cm7
Quer amar e não se deixa 
 F7                    Bb  Gm7                  
Se envolver e se entregar 
                      Cm7
Não demonstra o que sente 
                   F7
Não se lança livremente 
                         Bb
Não se arrisca e não se dá 

      D7
Experimente abrir a porta quando o dia amanhecer 
     Gm7
E abrir os braços quando eu chegar 
    C7
Ouvir o que eu não tive ainda a chance de dizer 
   F7/4               F7
E tudo pra você vai mudar 

         D7
Vou te levar pra ver as cores desse dia feliz 
    Gm7
Aproveitar o sol e depois 
   C7
À noite em meus abraços te envolver e sonhar 
    F7/4                       F7
Estrelas vão sorrir pra nós dois 

          G7/#5       Cm7
E quando o dia amanhecer 
  F7               Bb  Gm7
Vou ouvir você dizer: 
    Cm7                 F7
O amor é a coisa mais linda 
                  Bb  Gm7
Que existe entre nós 

SOLO: Cm7 F7 Bb Gm7 Cm7 F7 Bb

       D7
Experimente abrir a porta quando o dia amanhecer 
      Gm7
E abrir os braços quando eu chegar 
    C7
Ouvir o que eu não tive ainda a chance de dizer 
   F7/4                F7
E tudo pra você vai mudar 
 
          D7
Vou te levar pra ver as cores desse dia feliz 
    Gm7
Aproveitar o sol e depois 
   C7
À noite nos meus braços te envolver e sonhar 
    F7/4                      F7
Estrelas vão sorrir pra nós dois 

           G7/#5      Cm7
E quando o dia amanhecer 
(Quando o sol nascer...) 
 F7              Bb  Gm7
Vou ouvir você dizer 
(...vou amar você) 
   Cm7                  F7
O amor é a coisa mais linda 
                  Bb  Gm7 
Que existe entre nós 
           Cm7    F7
(Quando o sol nascer...) 
              Bb  Gm7
Ouvir você dizer 
(...vou amar você) 
    C7                 F7
O amor é a coisa mais linda 
                  Bb  Gm7
Que existe entre nós 
           Cm7    F7
(Quando o sol nascer...) 
             Bb  Gm7
Ouvir você dizer 
(...vou amar você) 
    C7                 F7
O amor é a coisa mais linda 
                  Bb  Gm
Que existe entre nós

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C7 = X 3 2 3 1 X
Cm7 = X 3 5 3 4 3
D7 = X X 0 2 1 2
F7 = 1 3 1 2 1 1
F7/4 = 1 3 1 3 1 X
G7/#5 = 3 X 3 4 4 3
Gm = 3 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
