Roberto Carlos - Maria Carnaval e Cinzas

        Am    E         Am
Nasceu Maria quando a folia
         Am7              E
Perdia a noite, ganhava o dia
         E            E
Foi fantasia seu enxoval
         E           Am
Nasceu Maria no Carnaval
             Dm       E
E não lhe chamaram assim
      Am               Dm
Como tantas Marias de santas
  E        Am           Dm
Marias de flor, seria Maria
  E      Am             Dm
Maria somente, Maria semente
    E           Am
De samba e de amor
         Dm       E   Am
Não era noite não era dia
        Dm        E    Am
Só madrugada, só fantasia
            Dm
Só morro e samba
 E     Am
Viva Maria
             Am7
Quem sabe a sorte
         E              E
Lhe sorriria e um dia viria
               Am
De porta-estandarte
             Dm
Sambando com arte
   E        Am
Puxando cordões e em plena
   Dm     E        Am
Folia de certo estaria
             Dm        E
Nos olhos e sonhos de mil
    Am
Foliões

         Am    E         Am
Morreu Maria quando a folia
           Am7           E
Na quarta feira também morria
          E              E
E foi de cinzas seu enxoval
        E             Am
Viveu apenas um Carnaval
              Dm
Que fosse chamada
   E        Am
Então como tantas
           Dm
Marias de santas
  E        Am               Dm
Marias de flor, em vez de Maria
  E      Am             Dm
Maria somente, Maria semente
    E          Am
De samba e de dor
         Dm        E   Am
Não era noite, não era dia
         Dm        E    Am
Somente restos de fantasia
         Dm      E      Am
Somente cinzas, pobre Maria
          Am           E
Jamais a vida lhe sorriria
          E
E nunca viria de
            Am
Porta-estandarte
             Dm
Sambando com arte
   E        Am
Puxando cordões
          Dm       E      Am
E não estaria em plena folia
             Dm
Nos olhos e sonhos
   E       Am
De mil foliões

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
