Roberto Carlos - Um Milhão de Amigos

[Intro] E  A  E  A

A           E                A               E                 A  
Eu quero apenas olhar os campos, eu quero apenas cantar meu canto
            E             A                    E             A
 Eu só não quero cantar sozinho, eu quero um coro de passarinhos

         D                  A               E            A  
Quero levar o meu canto amigo   a qualquer amigo que precisar
A7       D                  A                E          A7
Eu quero ter um milhão de amigos e bem mais forte poder cantar
           D                  A              E          A
Eu quero ter um milhão de amigos e bem mais forte poder cantar

           E                 A            E              A
Eu quero apenas um vento forte levar meu barco no rumo norte

A        E             A               E           A        
E no caminho o que eu pescar quero dividir quando lá 
chegar

         D                  A             E            A      A7
Quero levar o meu canto amigo a qualquer amigo que precisar
           D                  A              E             A     A7
Eu quero ter um milhão de amigos e bem mais forte poder cantar
           D                  A             E               A
Eu quero ter um milhão de amigos e bem mais forte poder cantar

           E                 A           E                  A
Eu quero crer na paz do futuro eu quero ter um quintal sem muro
             E              A            E             A
Quero meu filho pisando firme, cantando alto, sorrindo livre

         D               A                  E            A  A7
Quero levar o meu canto amigo a qualquer amigo que precisar
           D                A                E            A     A7
Eu quero ter um milhão de amigos e bem mais forte poder cantar
           D               A                E             A
Eu quero ter um milhão de amigos e bem mais forte poder cantar

           E              A               E             A
Eu quero amor decidindo a vida sentir a força da mão amiga

A         E               A             E                       A    A7
O meu irmão com sorriso aberto, se ele chorar, quero estar por perto

         D               A                    E            A    A7
Quero levar o meu canto amigo a qualquer amigo que precisar
           D                  A             E            A     A7
Eu quero ter um milhão de amigos e bem mais forte poder cantar
           D                  A               E          A
Eu quero ter um milhão de amigos e bem mais forte poder cantar

          E              A           E               A
Venha comigo olhar os campos cante comigo também meu canto
            E             A                  E             A
 Eu só não quero cantar sozinho, eu quero um coro de passarinhos

         D                  A               E               A  A7
Quero levar o meu canto amigo a qualquer amigo que precisar
           D                  A             E            A    A7
Eu quero ter um milhão de amigos e bem mais forte poder cantar
           D                  A             E           A
Eu quero ter um milhão de amigos e bem mais forte poder cantar

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
