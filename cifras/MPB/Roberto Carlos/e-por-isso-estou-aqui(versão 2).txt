Roberto Carlos - E Por Isso Estou Aqui

Tom: F#

 Gb                      Db
Olha dentro dos meus olhos
            B       Db             Gb      Db
Vê quanta tristeza de chorar por ti, por ti
 Gb                Db               B
Olha eu já não podia mais viver sozinho
 Db          Gb   Db     Gb
E por isso eu estou aqui
 B       Db        Gb            B   Db          Gb
De saudade eu chorei e até pensei que ia morrer
 B     E         A
Juro que eu não sabia
      D        G
Que viver sem ti
           Db
Eu não poderia
 Gb              Db                  B
Olha quero te dizer todo aquele pranto
 Db          Gb       Db
Que chorei por ti, por ti
 Gb                   Db                   B
Tinha uma saudade imensa de alguém que pensa
 Db          Gb
E morre por ti
B       Db        Gb            B   Db          Gb
De saudade eu chorei e até pensei que ia morrer
 B     E          A
Juro que eu não sabia
D              G
Que viver sem ti
           Db
Eu não poderia
 Gb             Db                  B
Olha quero te dizer todo aquele pranto
 Db           Gb       Db
Que chorei por ti, por ti
 Gb                   Db                   B
Tinha uma saudade imensa de alguém que pensa
 Db           Gb Db Gb
E morre por ti

----------------- Acordes -----------------
A*  = X 0 2 2 2 0 - (*G# na forma de A)
B*  = X 2 4 4 4 2 - (*A# na forma de B)
D*  = X X 0 2 3 2 - (*C# na forma de D)
Db*  = X 4 6 6 6 4 - (*C na forma de Db)
E*  = 0 2 2 1 0 0 - (*D# na forma de E)
G*  = 3 2 0 0 0 3 - (*F# na forma de G)
Gb*  = 2 4 4 3 2 2 - (*F na forma de Gb)
