Roberto Carlos - E Não Vou Mais Deixar Você Tão Só

Introd:  A E D E A E D E

E|-----0-2-0------------------------------------0-2-0---------------------------------|
G|---2-------4----4-2----4-2-----2---4-2-4---2--------4-----4-2-----4-2-----4-4-2-1-2-|
D|-2---------------------------------------2------------------------------------------|
A|------------------------------------------------------------------------------------|
E|------------------------------------------------------------------------------------|

 A           E7             D
Se a vida inteira, você esperou,
            E7   A           E7
Um grande amor e de triste até chorou,
          D            E7            A
Sem esperança de encontrar alguém
          E7         D
Fique sabendo, que eu também,
         A              E7
Andei sozinho e sem ninguém pra mim
    D                        E7
Fiquei sem entregar o meu carinho
  A            E7               D
Se na tua estrada, não houve flor,
            E7
Foi só tristeza enfim
  A        E7            D
E em cada dia sem ter amor
      E7            A
Foi tudo tão ruim
          E7               D
Vou confessar então meu coração
                A                   E7
Não quer mais existir, meus olhos vermelhos
     D                        E   A E D E A E D E
Cansados de chorar querem sorrir
 Em7        A
Ah!   Por isso foi que eu decidi,
      D
Não fico nem mais um minuto aqui
   B7             E7                   A
Eu vou, buscar o meu amor, o meu amor
          E7             D              A
Eu nunca tive alguém, agora vou olhar você meu bem
                 E7
Guarde o meu coração,
    D                       A
E nunca mais eu vou deixar você tão só
    D                       A
E nunca mais eu vou deixar você tão só
    D            E          A
E nunca mais eu vou ficar também tão só

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
Em7 = 0 2 2 0 3 0
