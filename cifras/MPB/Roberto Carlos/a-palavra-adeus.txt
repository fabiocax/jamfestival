Roberto Carlos - A Palavra Adeus

[Intro] A  A7M  Bm7  E7
        A  A7M  Bm7  E7
 
A  A7M            A6     A7M              Bm7    E7    Bm7  E7
Eu,   não posso compreender  porque tudo mudou pra mim  
Bm7  E7          Bm7  E7             A      A7M  A6  A7M
Foi,  uma palavra só    que tudo destruiu em mim
  A      A7M            A7              D      D7M  D6  D7M       
Porque o mar tão calmo  não se transformou em fúria
   Bm7                                     E         E7
E não,calou a voz que sem tremer  me disse aquele adeus
A  A7M            A6    A7M           Bm7           E7  Bm7  E7
Vi,   a luz do entardecer aos poucos se apagar pra mim 
Bm7  E7           Bm7    E7          A            A7M  A6  A7M
Não,   não sabia se você   ainda estava junto a mim
   A         A7M         A7                      D   D7M  D6  D7M
Chorei e o pranto a deslizar    nem mesmo me deixava ver
    Bm7                E7                   A      A7M  Bm7  E7
O rosto, que disse sem tremer        aquele triste adeus

( A  A7M  A6  A7M )
( Bm7  E7  Bm7  E7 )
( Bm7  E7  Bm7  E7 )
( A  A7M  A6  A7M )

  A       A7M          A7             D       D7M  D6  D7M     
Porque o mar tão calmo não se transformou em fúria
   Bm7                                      E         E7
E não, calou a voz que sem tremer  me disse aquele adeus
A  A7M             A6   A7M    
Vi,    a luz do entardecer     
           Bm7           E7  Bm7  E7  Bm7  E7          Bm7    E7
Aos poucos se apagar pra mim       não,  não sabia se você
         A            A7M  A6  A7M    
Ainda estava junto a mim     
   A         A7M        A7    
Chorei e o pranto a deslizar     
                D     D7M  D6  D7M   
Nem mesmo me deixava ver     
    Bm7                   E7           A      A7M  Bm7  E7
O rosto, que disse sem tremer  aquele triste adeus 

         A       A7M  Bm7  E7           A      A7M  Bm7  E7  A
Aquele triste adeus          aquele triste adeus

----------------- Acordes -----------------
A = X 0 2 2 2 0
A6 = 5 X 4 6 5 X
A7 = X 0 2 0 2 0
A7M = X 0 2 1 2 0
Bm7 = X 2 4 2 3 2
D = X X 0 2 3 2
D6 = X 5 4 2 0 X
D7M = X X 0 2 2 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
