Roberto Carlos - N�o Se Esque�a de Mim (Cifra)
(Roberto Carlos-Erasmo Carlos)


Intro: C Em F Bb7 C  

              Em7  Dm7               G7   Dm7 G7  
Onde voc� estiver, n�o se esque�a de mim  
Dm7               G7   C                        
Com quem voc� estiver, n�o se esque�a de mim  
                  Em7  Dm7         G7    Dm7 G7  
Eu quero apenas estar, no seu pensamento  
Dm7               G7   C                 Am Dm7 G7  
Por um momento pensar, que voc� pensa em mim  
C             Em7  Dm7               G7 Dm7 G7  
Onde voc� estiver, n�o se esque�a de mim  
Dm7                     G7   Gm                A7  
Mesmo que exista outro amor, que te fa�a feliz  
Dm7                 G7                 
Se resta em sua lembran�a  
   C         Am    
Um pouco, do muito que eu te quis  
Dm7           G7            C  
Onde voc� estiver, n�o se esque�a de mim  

Instrumental  

Em7 Dm7 G7 Dm7 G7 C  

                 Em7   Dm7         G7    Dm7 G7  
Eu quero apenas estar, no seu pensamento  
Dm7               G7   C                 Am  G#7  
Por um momento pensar, que voc� pensa em mim    
C#            Fm7  D#m7              G#7  D#m7 G#7  
Onde voc� estiver, n�o se esque�a de mim  
D#m7              G#7   G#m                   Bb7  
Quando voc� se lembrar, n�o se esque�a que eu  
D#m7           G#7          C#          Bbm    
Que eu, n�o consigo a***, voc� da minha vida  
D#m7          G#7                   C#  Fm7 D#m7  
Onde voc� estiver n�o se esque�a de mim  
G#7               C#  Fm7 D#m7  
N�o se esque�a de mim  
G#7               C#  Fm7 D#m7 G#7 C#  
N�o se esque�a de mim... 


  C            Em           F            Bb7          Em7          Dm7        
  ||||o|       ||||||       o|||oo       |o|o|o       ||||||     5 |o|o|o     
  ||o|||       |oo|||       |||o||       ||||||       |oo|||       ||||o|     
  |o||||       ||||||       |oo|||       ||o|o|       ||||o|       ||o|||     
  ||||||       ||||||       ||||||       ||||||       ||||||       ||||||     
  ||||||       ||||||       ||||||       ||||||       ||||||       ||||||     


  G7           Am           Gm           A7           G#7          C#         
  ||||||       ||||o|       ||||||       ||||||     4 o|o|oo     4 |o|||o     
  ||||||       ||oo||       ||||||       ||o|o|       |||o||       ||||||     
  o|o|oo       ||||||       o||ooo       ||||||       |o||||       ||ooo|     
  |||o||       ||||||       ||||||       ||||||       ||||||       ||||||     
  |o||||       ||||||       |oo|||       ||||||       ||||||       ||||||     


  Fm7          D#m7         G#m          Bbm                     
  o|ooo|       ||o|||     4 o||ooo       |o|||o     
  ||||||       ||||oo       ||||||       ||||o|     
  ||||||       |||o||       |oo|||       ||oo||     
  ||||||       ||||||       ||||||       ||||||     
  ||||||       ||||||       ||||||       ||||||     


