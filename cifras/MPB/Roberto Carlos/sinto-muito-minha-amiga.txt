Roberto Carlos - Sinto Muito Minha Amiga

Intro: Bm

Bm
    Sinto muito minha amiga
       F#/A#
Mas eu tenho que dizer
        A°
Nosso amor está em crise
      G6
Temos que reconhecer
     Em         F#7
É melhor falar agora
       Bm           Bm/A
Do que há entre nós dois
         G7+
Se um de nós se vai embora
     F#7
Pode ser pior depois
    Bm
Se ainda existe amor
       F#/A#
Ainda existe condição
      Aº
De tentarmos novamente
       G6
Contornar a situação
     Em            F#7
Mas existem tantas mágoas
       Bm         Bm/A
Que tentamos sufocar
       G7+
Tantas marcas que ficaram
     F#7
Precisamos apagar

B7+                          D#m7
Ah...eu não queria que esse amor
   B7                         E7+
Chegasse ao ponto de uma despedida
  Em7
Saber quem é culpado
      D#m7        G#m7
Não resolve nossa vida
C#m7           F#7         Bm
Ah...eu sinto muito minha amiga!

----------------- Acordes -----------------
A° = 5 X 4 5 4 X
Aº = 5 X 4 5 4 X
B7 = X 2 1 2 0 2
B7+ = X 2 4 3 4 2
Bm = X 2 4 4 3 2
Bm/A = X 0 4 4 3 X
C#m7 = X 4 6 4 5 4
D#m7 = X X 1 3 2 2
E7+ = X X 2 4 4 4
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F#/A# = 6 X 4 6 7 X
F#7 = 2 4 2 3 2 2
G#m7 = 4 X 4 4 4 X
G6 = 3 X 2 4 3 X
G7+ = 3 X 4 4 3 X
