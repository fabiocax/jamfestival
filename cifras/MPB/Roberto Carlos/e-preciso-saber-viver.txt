Roberto Carlos - É Preciso Saber Viver

[Intro] D  D7+  D7  G  Gm  D  Bm  E  G  A
 
        D
Quem espera que a vida
      D7+
Seja feita de ilusão
       D7
Pode até ficar maluco
       G
Ou morrer na solidão
      Gm
É preciso ter cuidado
          D            Bm
Pra mais tarde não sofrer
E          G       A
É preciso saber viver

      D
Toda pedra no caminho
      D7+
Você deve retirar
       D7
Numa flor que tem espinhos
      G
Você pode se arranhar
      Gm
Se o bem e o mal existem
   D         Bm
Você pode escolher
E          G       A
É preciso saber viver

G          D       Bm
É preciso saber viver
G          D       Bm
É preciso saber viver
G          D       Bm
É preciso saber viver
 E       G A
Saber viver

[Solo]  D  D7+  D7  G  Gm  D  Bm  E  G  A

E|------10-8-10------------------------------------|
B|-8h10---------8/10-------------------------------|
G|-------------------10-9--------------------------|
D|-------------------------------------------------|
A|-------------------------------------------------|
E|-------------------------------------------------|

E|------10-8-10-13-15-13-15/17--15-13-15-------14--|
B|-8h10-------------------------------15-15-14-14--|
G|-------------------------------------------------|
D|-------------------------------------------------|
A|-------------------------------------------------|
E|-------------------------------------------------|

E|-8b(10)-5------------------3---------------------|
B|----------8b(10)-6-----6-----3-------------------|
G|-------------------5h7---------4-----------------|
D|-------------------------------------------------|
A|-------------------------------------------------|
E|-------------------------------------------------|

      D
Toda pedra no caminho
      D7+
Você deve retirar
       D7
Numa flor que tem espinhos
      G
Você pode se arranhar
      Gm
Se o bem e o mal existem
   D         Bm
Você pode escolher
E          G       A
É preciso saber viver

G          D       Bm
É preciso saber viver
G          D       Bm
É preciso saber viver
G          D       Bm
É preciso saber viver
 E       G A
Saber viver
G          D       Bm
É preciso saber viver
G          D       Bm
É preciso saber viver
G          D       Bm
É preciso saber viver
 E       G A
Saber viver

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
D7+ = X X 0 2 2 2
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
