Roberto Carlos - Ninguém Vai Tirar Você de Mim

Introdução: ( Cm )
 
  Cm
 Não me canso de falar que te amo 
                      C7          Fm
 E que ninguém vai tirar você de mim 
            Bb                       Cm
 Nada me importa se eu tenho você comigo 
                   G7
 Eu por você faço tudo 
                      Cm   G7
 Pode crer no que eu digo 
        Cm
 Sou feliz e nada mais me interessa 
                              C7
 Não vou ser triste e nem chorar 
              Fm
 Por mais ninguém 
          Bb
 Esqueço tudo, até de mim 
               Cm
 Quando estou perto de você 
           Fm             G7
 Eu fico triste só de pensar 
           C  G7
 Em te perder 
    C            Am
 O nosso amor é puro 
    Dm7           G7
 Espero nunca acabar 
      C                Am
 Por isso meu bem até juro 
     Dm7             G7
 De nunca em nada mudar 
     C           Am
 Mas se ficar um só momento 
    Dm7          G7
 Sozinho sem te ver 
           Cm             G7
 Eu fico triste só de pensar 
           Cm   G7
 Em te perder 
 
  Solo: Cm  Fm  Bb  Gm  Cm  Fm  G#7  G7
 
    C            Am
 O nosso amor é puro 
    Dm7            G7
 Espero nunca acabar 
      C                Am
 Por isso meu bem até juro 
     Dm7              G7
 De nunca em nada mudar 
     C           Am
 Mas se ficar um só momento 
   Dm7           G7
 Sozinho sem te ver 
           Cm             G7
 Eu fico triste só de pensar 
           Cm  G7
 Em te perder 
           Cm  G7
 Em te perder 
           Cm
 Em te perder.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
Dm7 = X 5 7 5 6 5
Fm = 1 3 3 1 1 1
G#7 = 4 6 4 5 4 4
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
