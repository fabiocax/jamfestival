Roberto Carlos - Nossa Canção

      A              C#7
Olha aqui, preste atenção!
D       E7       A
Essa é a nossa canção
Bm7              E7
Vou cantá-la seja onde for
Bm7            E7           Bm7        E7
Para nunca esquecer o nosso amor, nosso amor
 A      C#7
Veja bem,    foi você
D   E7         A
A razão e o porquê
Bm7              E7
De nascer essa canção assim
Bm7            E7                A
Pois você é o amor que existe em mim

       G#m7/4       C#7        F#m      G#m7/4    C#7   F#m
Você partiu          e    me deixou nunca mais    você voltou
 Bm7  E7       A   Bm7     C#7
Pra me tirar     da solidão  e  até você voltar
Bm7       C#7
Meu bem eu vou cantar
Bm7  E7       A
Essa  nossa canção

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm7 = X 2 4 2 3 2
C#7 = X 4 3 4 2 X
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
G#m7/4 = 4 X 4 4 2 X
