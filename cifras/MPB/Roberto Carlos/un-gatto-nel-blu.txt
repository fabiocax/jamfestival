Roberto Carlos - Un Gatto Nel Blu

[Intro] D#  A#7  D#

D#                         A#7
Quand'ero bambino, che allegria
             Fm            D#
Giocare alla guerra per la via
              D#7        G#
Saltare un cancello io e te
  G#m     D#           A#7
E poi una mela, l'emozione
           D#
Gli occhi tuoi
                       A#7
Le rose, l'amore, casa mia
               Fm          D#
E un gatto per farci compagnia
                 D#7
Ma da quanda é finita
       G#    G#m
Io non so perché
     D#          A#7          D#
La finesta é piú grande senza te
             Gm
Un gatto nel blu
           A#m
Guarda le stelle
     C7                        Fm
Non vuol tornare en casa senza te
            Fm7
Sapessi quaggiu
          G#
Che notte bella
    A#7                        D#
Chi sá se un gran dolore si cancella ?
             Gm
Un gatto nel blu
         Cm
Ecco che tu
           G#
Sfondi dal cuore
A#7       D#
Mio caro amore
           Gm
Fra poco sarai
              Cm
Negli occhi miei
         G#   A#7
Anche stasera
            D#  A#7  D#  B7
Una lacrima sei

E                       B7
Bambina, bambina, vita mia
             F#m          E
Profumo di giglio, che fa via
                 E7          A
Se amare é un sbaglio, colpa mia
     Am    E
Pero io in fondo
      B7           E
Della vitta que ne so
             G#m
Um gatto nel blu
           Bm
Guarda la stelle
      C#7                      F#m
Non vuol tornare in casa senza te
            Fm7
Sapessi quaggiu
          A
Che notte bella
    B7                         E
Chi sa se un gran dolore si cancela ?
             G#m
Un gatto nel blu
         C#m
Ecco che tu
             A
Sfondi dal cuore
B7        E
Mio caro amore
           G#m
Fra poco sarai
              C#m
Negli occhi miei
            A
La prima chiara
B7      E
Di primavera
             G#m
Un gatto nel blu
         C#m
Ecco che tu
          A    B7
Anche strasera
            E
Una lacrima sei
             G#m
Un gatto nel blu
         C#m
Ecco che tu
             A
Sfondi dal cuore
B7        E
Mio caro amore
           G#m
Fra poco sarai
              C#m
Negli occhi miei
            A
La prima chiara
B7      E
Di primavera
             G#m
Un gatto nel blu
         C#m
Ecco che tu
             A
Sfondi dal cuore
B7        E
Mio caro amore
           G#m
Fra poco sarai
              C#m
Negli occhi miei
            A
La prima chiara
B7      E
Di primavera
            G#m
Lá la la la ra

----------------- Acordes -----------------
A = X 0 2 2 2 0
A#7 = X 1 3 1 3 1
A#m = X 1 3 3 2 1
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C#7 = X 4 3 4 2 X
C#m = X 4 6 6 5 4
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
D# = X 6 5 3 4 3
D#7 = X 6 5 6 4 X
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
Fm = 1 3 3 1 1 1
Fm7 = 1 X 1 1 1 X
G# = 4 3 1 1 1 4
G#m = 4 6 6 4 4 4
Gm = 3 5 5 3 3 3
