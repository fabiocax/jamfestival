Roberto Carlos - Meu Menino Jesus (Cifra)


 D        G/D   D/F#   D7   G           A       D   D/F# 
Oh! Meu Menino Jesus /        Na noite desse Natal /  
 G       A/G          D    D/C#      Bm     Bm/A           Esus4   E  A 
S�o as estrelas que brilham no     c�u /     Do seu amor um sinal 
 D         G/D       D   D7           G      A7      D     D7 
Que em toda casa a alegria /        Seja pra todos igual /  
G         A/G    D     A/C#         Bm       Bm/A    G        A7          D     G/D   D 
Brisa de flor perfumando o          jardim /         Chuva de amor no quintal 
F#7          Bm7     F#7                Bm7   Esus4  E        A         Em            A A/G A/D A/E 
E nessa noite feliz/Noite de paz e de amor / Todos veremos no c�u / A estrela do Salvador 
   D     G/D      D      D7         G        A       D         D/F# 
Te pe�o Menino Jesus /            Ponha na mesa de algu�m /  
  G          A            D        A/C#       Bm        Bm/A         Em    A7         D 
O que esse algu�m sempre quis      e n�o        tem /                  Felicidade tamb�m /  
 D A D D7/9/F# G A D D7/9/F# G A D A/C# Bm Bm7 Esus4 A7 
L�, l�, l�, l�    

 D         G/D       D   D7           G      A7      D     D7 
Que em toda casa a alegria /        Seja pra todos igual /  
G         A/G    D     A/C#         Bm       Bm/A    G        A7          D     G/D   D 
Brisa de flor perfumando o          jardim /         Chuva de amor no quintal 
F#7          Bm7     F#7                Bm7   Esus4  E        A         Em            A A/G A/D A/E 
E nessa noite feliz/Noite de paz e de amor / Todos veremos no c�u / A estrela do Salvador 
   D     G/D      D      D7         G        A       D         D/F# 
Te pe�o Menino Jesus /            Ponha na mesa de algu�m /  
  G          A            D        A/C#       Bm        Bm/A         Em    A7         D 
O que esse algu�m sempre quis      e n�o        tem /                  Felicidade tamb�m /  
 D A D D7/9/F# G A D D7/9/F# G A D A/C# Bm Bm7 Esus4 A7 
L�, l�, l�, l�    




  D            G/D          D/F#         D7           G            A          
  ||||||       ||||||       ||||||       ||||o|       ||||||       ||||||     
  |||o|o       ||||||       o||o|o       |||o|o       |o||||       ||ooo|     
  ||||o|       ||||o|       ||||o|       ||||||       o|||oo       ||||||     
  ||||||       |||o||       ||||||       ||||||       ||||||       ||||||     
  ||||||       |oo|||       ||||||       ||||||       ||||||       ||||||     


  A/G          D/C#         Bm           Bm/A         Esus4        E          
  ||||||       ||||||       ||||||       ||||||       ||||||       |||o||     
  ||ooo|       |||o|o       |o|||o       |||||o       |ooo||       |oo|||     
  o|||||       ||||o|       ||||o|       ||||o|       ||||||       ||||||     
  ||||||       |o||||       ||oo||       ||oo||       ||||||       ||||||     
  ||||||       ||||||       ||||||       ||||||       ||||||       ||||||     


  A7           A/C#         F#7          Bm7          Em           A/D        
  ||||||       ||||||       ||||||       ||||||       ||||||     5 ||||oo     
  ||o|o|       |||o||       o|o|oo       |o|o|o       |oo|||       |||o||     
  ||||||       ||||||       |||o||       ||||o|       ||||||       ||||||     
  ||||||       |o||||       |o||||       ||o|||       ||||||       ||||||     
  ||||||       ||||oo       ||||||       ||||||       ||||||       ||||||     


  A/E          D7/9/F#                 
  ||||||     5 o|o|o|     
  ||ooo|       |||||o     
  ||||||       |o||||     
  ||||||       ||||||     
  ||||||       |||o||     
