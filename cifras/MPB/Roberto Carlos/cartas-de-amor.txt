Roberto Carlos - Cartas de Amor

Intro: (G7+ Am/G) Cm6/G

G7+                 Em
   Ontem amor eu reli
Am7          D7/9-       G7+   
   As cartas que te escrevi
C#m7/5-          F#7
      Frases repletas de
  Bm7 Bm7/5- E7     
Amor
Am7           E7/9-   Am D7/9-
   Que por você eu senti, sofri
G7+                 G6
   Ontem amor, revivi
Am/G            A°/G     G7+ Db9/7
   Lembranças do nosso amor
C7+           Am7/5-              G/D 
   Nas velhas frases que meu coração
   Bb°   
Ditou
Am7          D7/9-      G7+ Am D7   
   Naquelas cartas de amor
G7+                G6  
   Ontem amor, revivi...
            G7+
Cartas de amor

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am/G = 3 X 2 2 1 X
Am7 = X 0 2 0 1 0
Am7/5- = 5 X 5 5 4 X
A°/G = X X 5 8 7 8
Bb° = X 1 2 0 2 0
Bm7 = X 2 4 2 3 2
Bm7/5- = X 2 3 2 3 X
C#m7/5- = X 4 5 4 5 X
C7+ = X 3 2 0 0 X
Cm6/G = X X 5 5 4 5
D7 = X X 0 2 1 2
D7/9- = X 5 4 5 4 X
Db9/7 = X 4 3 4 4 X
E7 = 0 2 2 1 3 0
E7/9- = X X 2 1 3 1
Em = 0 2 2 0 0 0
F#7 = 2 4 2 3 2 2
G/D = X 5 5 4 3 X
G6 = 3 X 2 4 3 X
G7+ = 3 X 4 4 3 X
