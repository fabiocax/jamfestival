Roberto Carlos - Super-her�i (Cifra)

Intro: G Em C D7 G Em C D7 D#o

Em              A      Em              A
Quando voc� precisar, alguma coisa de mim
Em            D/E          E7      Am
� s�  me telefonar, que eu irei correndo
D                F#m5-/7 B7 Em 
Meu telefone eu deixei,    na cabeceira da cama
F#m5-/7                       B7/4  B7
N�o importa a hora que for me chama
Em                     A   Em                     A
J� n�o sou mais, seu amor, mas me preocupo contigo
Em                    D/E   E7   Am
Saiba que sou, seu melhor      amigo
D              F#m5-/7 B7       Em               
Eu sou o super her�i,       esque�o a dor que me d�i
F#m5-/7                B7    Em7   Bm7  E7
Pra me tirar de qualquer perigo
Am       D7   G7+    Em  F#m5-/7    B7               Em       Bm7  E7    
N�o se culpe eu aprendi que o sentimento ningu�m, domina
   C    D7   Bm7       C7+  F#m5-/7   B7               Em7   A  Em7  A7
Te agrade�o, eu fui feliz, mas o meu sonho de amor termina
Fm7               Bb   Fm7              Bb
Se acaso voc� chorar e precisar do meu ombro
Fm7                 Eb/F       F7          Bbm
Estou nos restos do amor, que ficou nos escombros
Db             Gm5-/7 C7  Fm7 
Mesmo ferido demais       eu te direi sempre assim
Gm5-/7              C7   Fm7  Bb  Intro
Chame quando precisar  de mim
Bbm     Eb   Ab7     Fm7    Gm5-/7  C7               Fm7    Cm7  F7
N�o se culpe, eu aprendi que o sentimento ningu�m domina
  Db    Eb     Cm7    Db7+  Gm5-/7   C7                Fm7   Bb
Te agrade�o, eu fui feliz, mas o meu sonho de amor termina
Gm5-/7            C7      Fm7   Bb
Chame quando precisar de mim 
Gm5-/7            C7    Fm7
Chame quando precisar de mim.....Chame quando precisar de mim....



  G            Em           C            D7           D#o          A          
  ||||||       ||||||       ||||o|       ||||o|       ||o|o|       ||||||     
  |o||||       |oo|||       ||o|||       |||o|o       |||o|o       ||ooo|     
  o|||oo       ||||||       |o||||       ||||||       ||||||       ||||||     
  ||||||       ||||||       ||||||       ||||||       ||||||       ||||||     
  ||||||       ||||||       ||||||       ||||||       ||||||       ||||||     


  D/E          E7           Am           D            F#m5-/7      B7         
  ||||||       |||o||       ||||o|       ||||||       ||||o|       ||o|||     
  ||oo|o       |oo|||       ||oo||       |||o|o       o|oo||       |o|o|o     
  ||||o|       ||||o|       ||||||       ||||o|       ||||||       ||||||     
  ||||||       ||||||       ||||||       ||||||       ||||||       ||||||     
  ||||||       ||||||       ||||||       ||||||       ||||||       ||||||     


  B7/4         Em7          Bm7          G7+          C7+          A7         
  ||||||       ||||||       ||||||       ||||||       ||||||       ||||||     
  |o|o|o       |oo|||       |o|o|o       |o|||o       ||o|||       ||o|o|     
  ||||||       ||||o|       ||||o|       o|||o|       |o||||       ||||||     
  ||o|||       ||||||       ||o|||       ||||||       ||||||       ||||||     
  ||||o|       ||||||       ||||||       ||||||       ||||||       ||||||     


  Fm7          Bb           Eb/F         F7           Bbm          Db         
  o|ooo|       |o|||o     8 |oooo|       o|o|oo       |o|||o     4 |o|||o     
  ||||||       ||||||       ||||||       |||o||       ||||o|       ||||||     
  ||||||       ||ooo|       ||||||       |o||||       ||oo||       ||ooo|     
  ||||||       ||||||       ||||||       ||||||       ||||||       ||||||     
  ||||||       ||||||       ||||||       ||||||       ||||||       ||||||     


  Gm5-/7       C7           Eb           Ab7          Cm7          Db7+       
  ||||||       ||||o|     3 |||o|o     4 o|o|oo       ||||||       |||oo|     
  ||||o|       ||o|||       ||||o|       |||o||       ||||||       ||||||     
  o|oo||       |o|o||       ||o|||       |o||||       |o|o|o       ||o|||     
  ||||||       ||||||       |o||||       ||||||       ||||o|       |o|||o     
  ||||||       ||||||       ||||||       ||||||       ||o|||       ||||||     



