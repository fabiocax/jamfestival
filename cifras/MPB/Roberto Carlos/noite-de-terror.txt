Roberto Carlos - Noite de Terror

Intro: A7
A7
Fazia noite fria
Eu logo fui dormir
Soprava um vento forte
E eu não pude mais sair
D7
Pensei com meus botões
Um bom livro eu vou ler
Am
E um trago de uísque
Que é para me aquecer
E7
Mas uma coisa vejam
D7
Me aconteceu
E7
Uma mão gelada
D7
Em meu ombro bateu
A7
Gritar eu quis
Porém a voz não me saiu
E o livro que eu lía
Até de minhas mãos sumiu
D7
Tremi de cima a baixo
Sem sair do lugar
A7
Quando de repente
Eu ouvi alguém falar
E7
Bem junto de mim
D7 A7
Esse alguém me falou bem assim
"Eu sou o Frankstein!"
A7
Tomou conta de mim
Tamanha tremedeira
Mais nada quis ouvir
Pois corri pela ladeira
D7
Mas de repente então
Voltou-se o panorama
A7
Quando dei por mim
Eu estava em minha cama
E7
Alguém bate a porta
D7
Vou logo ver quem é
E7
Deve ser meu broto
D7
Pois fantasma não dá pé
A7
Mas quando a porta abriu
Fiquei logo a tremer
Senti por todo o corpo
Um frio percorrer
D7
Fiquei no chão calado
Com o cabelo arrepiado
A7
Maior foi meu pavor
Pois não era o meu amor
E7
E esse alguém que eu vi
D7 A7
Me falou novamente assim

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
