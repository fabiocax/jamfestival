Roberto Carlos - A Menina e o Poeta

(intro) E  E7+  E7  A  B7  E  Am7 B7

E|-4--7-4---------7-4--------7-5-4-5-----------4-5-4---4-----------7-5-4-|
B|---------7-5-4-------7-5-3---------5-------5-------7---7-5-------------|
G|-------------------------------------6-4-4-----------------4-5-4-------|

E         E7+         E7                 A
Virgem, menina morena, nos cabelos uma trança,
                     B7                       E
no rosto um jeito criança, na voz um canto, mulher.
E         E7+         E7                 A
Virgem, menina morena, nos olhos toda primavera
                     B7                       E   Am7  B7
no corpo uma longa espera, coração banhado em fé.
E                   E7+     E7             A
A tarde corre pra noite, a lua desperta sorrindo
              B7                       E
a menina à janela, botão em flor se abrindo
                    E7+   E7                   A
nasceu o primeiro desejo, conhecer o primeiro amor.
                    B7                  E
Na história de um poeta, a menina acreditou
                    Am7      B7         E   D/E  E  D/E
na história de um poeta, a menina acreditou
E       D/E         E  D/E              E
Mas o poeta foi um dia e até hoje não voltou
D/E               E        D/E       E
ninguém sabe o caminho que o poeta levou
   Am13      B7      E       Am13   B7   E
o vento que foi com ele, um dia por lá voltou
   Am13      B7      E       Am13 B7   E     Am13 B7
mas só que voltou sozinho e a menina chorou.
E      Am13 B7   E      Am13  B7    E
Na história do poeta a menina acreditou
       D/E  B7   E         Am13  B7  E
e dos olhos da menina uma lágrima rolou.
       Am13  B7   E        Am13  B7  E
E dos olhos da menina uma lágrima rolou

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am13 = 5 X 4 5 5 X
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
D/E = X X 2 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
E7+ = X X 2 4 4 4
