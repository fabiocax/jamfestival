Roberto Carlos - Os Velhinhos

Intro: G Em Am D7

G                   Bm
Quando a velhice chegar
                Am  D7
Eu não sei se terei
                  G   Em Am D7
Tanto amor pra te dar
G                   Bm
Quando a velhice chegar
                Am  D7
Eu não sei se terei
                  G   G7
Tanto amor pra te dar
C    D7                 G
Hoje,   vem amor, vem amar
Em                 Am    D7
  Os meus lábios esperam
                G  G7
Te querendo beijar
   Cm               G
Amanhã estaremos velhinhos
Em              Am     D7
  Contaremos juntinhos
Os segredos do amor
Os segredos do amor
                  G     Em Am D7
Para os nossos netinhos

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
