Roberto Carlos - À Janela

Intro 2x: Em Bm7/F# 

 Em             Em7+
  Da janela o horizonte
        Em7           Em7+           Am Am7+ Am7 Am7+
 A liberdade de uma estrada eu posso ver
    B7
 O meu pensamento voa livre e em sonhos
              Em
 Pra longe de onde estou
 Em             Em7+       Em7       Em7+
 Eu às vezes penso até onde essa estrada
         Am Am7+ Am7 Am7+
 Pode levar alguém

 Bm7                           B7
 Tanta gente já se arrependeu, e eu
     E              F#m       B7
 Eu vou pensar, eu vou pensar

   E               E7+
 Quantas vezes eu pensei
   E7(13)       E7+     F#m F#m7+ F#m7 F#m7+
 Sair de casa, mas eu desisti
  B7
 Pois eu sei, lá fora eu não teria
                    E   F#m B7
 O que eu tenho agora aqui

   E               E7+
 Meu pai me dá conselhos
   E7(13)       E7+     F#m F#m7+ F#m7 F#m7+
 Minha mãe vive falando sem saber
         B7
 Que eu tenho os meu problemas e que às vezes
                  E E7+ E7(13)
 Só eu posso resolver
 E7+    F#m F#m7+ F#m7 F#m7+ E G#m7 C#m7 C#m7/Bb
 Coisas da vida,   choque de opiniões
            F#m  B7           E  F#m  B7
 Coisas da vida,   coisas da vida

   E               E7+
 Novamente eu penso ir embora
   E7(13)       E7+     F#m F#m7+ F#m7 F#m7+
 Viver a vida que eu quiser
  B7
 Caminhar no mundo enfrentando
                      E  F#m  B7
 Qualquer coisa que vier
   E               E7+
 Penso andar sem rumo
   E7(13)       E7+     F#m F#m7+ F#m7 F#m7+
 Pelas ruas, pela noite, sem pensar
 B7
 No que vou dizer em casa
                    E E7+ E7(13)
 Nem satisfações a dar

 E7+    F#m F#m7+ F#m7 F#m7+ E G#m7 C#m7 C#m7/Bb
 Coisas da vida,   choque de opiniões
            F#m  B7           E  F#m  B7
 Coisas da vida,   coisas da vida

   E               E7+
 Penso duas vezes, me convenço
   E7(13)       E7+     F#m F#m7+ F#m7 F#m7+
 Que aqui é o meu lugar
     B7
 Lá fora, às vezes chove
                                  E  F#m  B7
 E é quase certo que eu vou querer voltar
   E               E7+
 A noite é sempre fria
   E7(13)       E7+     F#m F#m7+ F#m7 F#m7+
 Quando não se tem um teto com amor
   B7
 E esse amor eu tenho, mas me esqueço
                        E E7+ E7(13)
 Às vezes de lhe dar valor
 E7+    F#m F#m7+ F#m7 F#m7+ E G#m7 C#m7 C#m7/Bb
 Coisas da vida,   choque de opiniões
            F#m  B7           E    Gm  C7(9)
 Coisas da vida,   coisas da vida

  F            F7+        F7(13)
 Tudo tem seu tempo e uma vida inteira
  F7+          Gm Gm7+ Gm7 Gm7+
 Eu tenho pra viver
   C7
 E nessa vida é necessário a gente
                   F F7+ F7(13) F7+
 Procurar compreender

  F            F7+        F7(13)
 Coisas que aborrecem, muitas vezes
  F7+          Gm Gm7+ Gm7 Gm7+
 Acontecem por amor
   C7
 E esse amor eu tenho esquecido às vezes
               F F7+ F7(13) F7+
 De lhe dar valor
  F7+     Gm Gm7+ Gm7 Gm7+ F Am7 Dm7 Dm7/C
 Coisas da vida,   choque de opiniões
            Gm   C7           F F7+ F7(13) F7+
 Coisas da vida,   coisas da vida
  F7+     Gm Gm7+ Gm7 Gm7+ F Am7 Dm7 Dm7/C
 Coisas da vida,   choque de opiniões.
            Gm   C7           F F7+ F7(13) F7+
 Coisas da vida,   coisas da vida
           Gm   C7           F F7+ F7(13) F7+ ...

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Am7+ = X 0 2 1 1 0
B7 = X 2 1 2 0 2
Bm7 = X 2 4 2 3 2
Bm7/F# = X X 4 4 3 5
C#m7 = X 4 6 4 5 4
C7 = X 3 2 3 1 X
C7(9) = X 3 2 3 3 X
Dm7 = X 5 7 5 6 5
Dm7/C = X 3 0 2 1 1
E = 0 2 2 1 0 0
E7(13) = 0 X 0 1 2 0
E7+ = X X 2 4 4 4
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
Em7+ = X X 2 4 4 3
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
F#m7+ = X X 4 6 6 5
F7(13) = 1 X 1 2 3 X
F7+ = 1 X 2 2 1 X
G#m7 = 4 X 4 4 4 X
Gm = 3 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
Gm7+ = 3 5 4 3 3 3
