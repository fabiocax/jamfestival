Roberto Carlos - Esqueça

Tom : D
Intro: D                     F#m   G                 A7
        40 42 44 40 40 42 44 32 44 30 32 34 30 30 32 34 12 11 10

   D                      F#m
    Esqueça se ele não te ama 
G                          A7 
    Esqueça se ele não te quer 
    F#                         Bm
    Não chore mais não sofra assim 
    E7                        A7
Porque posso te dar amor sem fim 
   D                     F#m 
    Ele não pensa em querer-te  
   G                       F# 
    Te faz sofrer e até chorar
    Bm          Bb5+
    Não chore mais, vem 
     D
Pra mim, vem 
      Bº         Bm
 Não sofra, não pense 
     Em    A7       D  Bm Em A7
Não chore mais meu bem 
 

Solo: 
D                     F#m
40 42 44 40 40 42 44 32 44
G                    A7
30 32 34 30 30 32 34 22 34 32 34 22
F#                   Bm
33 34 22 33 33 34 22 12 10 23
E7                             A7
23 23 22 20 23 23 22 20 30 31 32 

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bb5+ = X 1 4 3 3 X
Bm = X 2 4 4 3 2
Bº = X 2 3 1 3 1
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
