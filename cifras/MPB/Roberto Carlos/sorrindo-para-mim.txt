Roberto Carlos - Sorrindo Para Mim


intro:
e|-----3-----7-5-3-----|
B|-5-5---3-5-------3-5-|
G|---------------------|
D|---------------------|

E
 Meu bem eu vou partir
                  B7
 Mas sem me despedir
 Você vai chorar
                 E  E7
 E eu não quero ver
             A
 Quero recordar
                  E7  D#7  D7  C#7
 Os seus olhos assim
                   F#m
 Brilhando de contente
      B7           F#m
 Brilhando alegremente
    B7           E  A  E
 Sorrindo para mim
             A 
 Na minha viagem levarei
              E
 Como recordação um beijo seu
             A
 Você vai ficar longe de mim
                F#m            B7
 Mas não quero vê-la triste assim
                   E 
 Mas quando eu voltar
                B7
 Serei feliz então
 Posso lhe entregar
            E  E7
 O meu coração
                  A
 Mas quero encontrar
                 E7   D#7  D7  C#7
 Os seus olhos assim
                 F#m 
 Chorando na verdade
      B7        F#m
 Mas de felicidade
     B7         E
 Sorrindo para mim

 

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
C#7 = X 4 3 4 2 X
D#7 = X 6 5 6 4 X
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
