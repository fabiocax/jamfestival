Roberto Carlos - O Ano Passado (Cifra)
(Roberto Carlos-Erasmo Carlos)

Intro: E Amaj7 B7 Amaj7 B7 E Amaj7 B7 Amaj7 B7  

  E             Amaj7             E   Amaj7 
O ouro no ano passado subiu sem parar 
   E                 Amaj7             E 
Os gritos na bolsa falaram de outros valores 
A                   E     G#             C#m   
Corpos estranhos no ar, silenciosos voadores 
     A                              B7 
Quem sabe olhando o futuro do ano passado 
  E                  Amaj7         E     Amaj7 
O mar quase morre de sede no ano passado 
   E              Amaj7             E 
Os rios ficaram doentes com tanto veneno 
  A            E         G#            C#m 
Diante da economia, quem pensa em ecologia 
     A                                            B7 
Se o d�lar � verde � mais forte que o verde que havia 
A              B7               E   C#m 
O que ser� o futuro que hoje se faz 
A                 B7            C#m  B7 
A natureza, as crian�as e os animais 
E                  Amaj7           E     Amaj7 
Quantas baleias queriam nadar como antes 
E                 Amaj7             E 
Quem inventou o fuzil para matar elefantes 
A                 E      G#                C#m 
Quem padeceu de ins�nia, com a sorte da Amaz�nia 
   A                                    B7 
Na lei do machado o mais forte do ano passado 
E             Amaj7            E  Amaj7 
N�o adianta soprar a fuma�a do ar 
E                 Amaj7              E 
As chamin�s do progresso n�o podem parar 
     A                  E             G#             C#m   
Quem sabe um museu no futuro, vai guardar em lugar seguro 
   A                                   B7 
Um pouco de ar puro, reliquia do ano passado 
A              B7               E   C#m 
O que ser� o futuro que hoje se faz 
A                 B7            C#m  B7 E Amaj7 E 
A natureza, as crian�as e os animais 
                      Amaj7            E      Amaj7   
Os campos risonhos um dia tiveram mais flores 
     E                    Amaj7            E 
E os bosques tiveram mais vida e at� mais amores 
     A               E     G#                 C#m   
Quem briga com a natureza, envenena a pr�pria mesa 
A                                    B7 
Contra a for�a de Deus, n�o existe defesa 
A              B7               E   C#m 
O que ser� o futuro que hoje se faz 
A                 B7            C#m  
A natureza, as crian�as e os animais 
A              B7               E   C#m 
O que ser� o futuro que hoje se faz 
A                 B7            C#m  
A natureza, as crian�as e os animais... 



  E            Amaj7        B7           A            G#           C#m        
  |||o||       |||o||       ||o|||       ||||||     4 o|||oo     4 |o|||o     
  |oo|||       ||o|o|       |o|o|o       ||ooo|       |||o||       ||||o|     
  ||||||       ||||||       ||||||       ||||||       |oo|||       ||oo||     
  ||||||       |||||o       ||||||       ||||||       ||||||       ||||||     
  ||||||       ||||||       ||||||       ||||||       ||||||       ||||||     



