Roberto Carlos - Se Você Pretende

Introdução: G7+  B7  Em7  B7

             G7+  B7
Se você pretende
               G7+  B7
Se envolver comigo
             G7+
Venha com cuidado
              Em7
Pra não machucar
                 Am7
Outra vez minha vida
E7              Am7  Am7+ 
   Não promete nada
                Am7  D7/9
Eu te aceito assim
             Am7
Fica do meu lado
              D7/9
Passado é passado
               G7+  D7/9
'Tá certo pra mim
             G7+  B7
Se você pretende
               G7+   B7
Mexer nas lembranças
            Dm7
Matar a saudade
                G7
Do tempo mais lindo
              C7+
Que a gente viveu

Mas se você pretende
Cm7               G/B  Bb°
   Me amar e ir embora
               Am7  D7/9
Me faz feliz agora
                 G7+   G7
Depois me diz adeus
              C7+
O que você pretende
Cm7                 G/B  Bb°
   Não sei e não importa
                   Am7
Primeiro a gente sonha
                 D7/9
Depois a gente acorda
                G7+  G7
E então a gente vê

C7+     D/C
   Volta
Nosso amor
      B7           E7
Não está terminado
             Am7             D7/9
Seu lugar é aqui do meu lado
               Dm7  G7
Fica perto de mim

C7+      D/C
   Volta
              B7
Esse caso de amor
             E7
Não tem jeito
                   Am7
E eu guardei seu lugar
             D7/9
No meu peito
               G7+   D7/9
Fica perto de mim

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Am7+ = X 0 2 1 1 0
B7 = X 2 1 2 0 2
Bb° = X 1 2 0 2 0
C7+ = X 3 2 0 0 X
Cm7 = X 3 5 3 4 3
D/C = X 3 X 2 3 2
D7/9 = X 5 4 5 5 X
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
Em7 = 0 2 2 0 3 0
G/B = X 2 0 0 3 3
G7 = 3 5 3 4 3 3
G7+ = 3 X 4 4 3 X
