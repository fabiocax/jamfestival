Roberto Carlos - Detalles

Bm           E          A
No ganas al intentar el olvidarme
Bm                   E
Durante mucho tiempo en tu vida
    A
Yo voy a vivir
Bm              E
Detalles tan pequeños de los dos
A                          Em
Son cosas muy grandes para olvidar
Bm                  G
Y a todo hora van estar presentes
E
Ya lo verás

Bm               E         A
Si otro hombre apareciera por tu ruta
Bm           E
Y esto te trajese recuerdos mios
A
La culpa es tuya,
Bm             E
El ruido enloquecedor de su auto
A                        Em
Será la causa obligada o algo así
Bm        G                  E
Inmediatamente tú vas a acordarte de mí

Bm
Yo sé que otro
E                     A
Debe estar hablando a tu oído
Bm               E
Palabras de amor como yo te hablé
 A
Más yo dudo
Bm             E
Yo dudo que él tenga tanto amor
A                   Em
Y hasta la forma de mi decir
Bm               G         E
Y en esa hora tú vas a acordarte de mí

Bm
En la noche
E                        A
Envuelta en el silencio de tu cuarto
Bm           E             A
Antes de dormir tú buscas mi retrato
Bm              E
Pero aún cuando no quisieras verme sonreír
A                     Em
Tú ves mi sonrisa lo mismo así
Bm                G
Y todo eso va a hacer
            E
Que tú te acuerdes de mí

Bm
Si alguien tocase
E                      A
Tu cuerpo como yo, no digas nada
Bm                   E
No vayas a decir mi nombre sin querer
A
A la persona errada
Bm              E
Pensando en el amor de ese momento
A                      Em
Desesperada vas a intentar llegar al fin
Bm                G
Y hasta en ese momento
               E
Tú irás a acordarte de mí

Bm                  E
Yo sé que mientras existamos
   A
Recordaremos
Bm                    E
Y que el tiempo transforma
              A
Todo amor en casi nada
                E
Más casi yo me olvido de un gran detalle
A                           Em
Un gran amor no va a morir así
Bm                   G
Por eso, de vez en cuando tú vas
E
Vas a acordarte de mí

Bm                E           A
No ganas nada con intentar el olvidarme
Bm                   E
Durante mucho tiempo en tu vida
    A
Yo voy a vivir

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
