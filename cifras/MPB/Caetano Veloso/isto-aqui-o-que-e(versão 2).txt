Caetano Veloso - Isto Aqui o Que É?

(intro) E7M(9)

E7M(9)                         Eb5/B          F#7m     F#m7M
      Isso aqui, ô, ô é um pouquinho de brasil iaiá
F#7m     F#m7M               A9/B    B7(13)   E7M(9)   Eb5/B
Desse brasil que  canta e é feliz,   feliz,   feliz,   é
                        F#m7    F#m7M    F#m7
Também um pouco  de uma raça
                     E7M(9)      Eb5/B
Que não tem medo de fumaça ai, ai
 B7        B7(9)     E7M(9)
E não se entrega não
E7M(9)                         Eb5/B          F#7m     F#m7M
      Isso aqui, ô, ô é um pouquinho de brasil iaiá
F#7m     F#m7M               A9/B    B7(13)   E7M(9)   Eb5/B
Desse brasil que  canta e é feliz,   feliz,   feliz,   é
                        F#m7    F#m7M    F#m7
Também um pouco  de uma raça
                     E7M(9)      Eb5/B
Que não tem medo de fumaça ai, ai
 B7        B7(9)     E7M(9)
E não se entrega não

Olha o jeito
       G#m11        Gm11     E7M(9)
Nas cadeiras que ela sabe dar
            B7(9)                 E7M(9)
Olha só o remelecho que ela sabe dar
Olha o jeito
       G#m11        Gm11     E7M(9)
Nas cadeiras que ela sabe dar
            B7(9)                 E7M(9)
Olha só o remelecho que ela sabe dar

  G#m11  F#m7              B7(9)             G#m11
Morena  boa que me   faz  penar  bota a sandália
     F#m7             B7(9)     E7M(9)
De  prata e  vem pro samba sambar
  G#m11  F#m7              B7(9)             G#m11
Morena  boa que me   faz  penar  bota a sandália
     F#m7             B7(9)     E7M(9)
De  prata e  vem pro samba sambar

----------------- Acordes -----------------
A9/B = X 2 2 2 0 0
B7 = X 2 1 2 0 2
B7(13) = X 2 X 2 4 4
B7(9) = X 2 1 2 2 X
E7M(9) = X 7 6 8 7 X
F#m7 = 2 X 2 2 2 X
F#m7M = X X 4 6 6 5
G#m11 = 4 X 1 4 2 X
Gm11 = 3 X 0 3 1 X
