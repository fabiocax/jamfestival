Caetano Veloso - Tempestades Solares

  A7       A7/G
Você provocou
               F#7/5-  F7+     E7
Tempestades solares no meu coração
 A7                                            D7/9
Com as mucosas venenosas de sua alma de mulher
 G13
Você faz o que quer
F#7
Você me exasperou
F7+                 A#7+     B7/5-    E7/5+
Você não sabe viver onde eu sou
A-
Então adeus
          C7/9
ou seja outra:
F7+          F#º      G7    C#7/9
Alguém que aguente o sol

----------------- Acordes -----------------
A#7+ = X 1 3 2 3 1
A7 = X 0 2 0 2 0
A7/G = 3 X 2 2 2 X
B7/5- = X 2 3 2 4 X
C#7/9 = X 4 3 4 4 X
C7/9 = X 3 2 3 3 X
D7/9 = X 5 4 5 5 X
E7 = 0 2 2 1 3 0
E7/5+ = 0 X 0 1 1 0
F#7 = 2 4 2 3 2 2
F#7/5- = 2 X 2 3 1 X
F#º = 2 X 1 2 1 X
F7+ = 1 X 2 2 1 X
G13 = 3 X 2 4 3 X
G7 = 3 5 3 4 3 3
