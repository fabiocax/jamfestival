Caetano Veloso - Solito

D                    A/C#               Bm  Bm/A
A veces en el silencio de la noche
Em                   Em/D                A7/C#  A7
Me quedo imaginando a nosotros dos
D                        A/C#         Bm   Bm/A         Em
Me quedo ahí, soñando despierto, juntando
Em                Em/D          A7  A#°
El antes, el ahora y el después
Bm              F#m7          G7+
Por qué me dejas tan suelto?
Bm                 F#m7     D/E
Por qué no te pegas a mí?
Bm                 F#m7     G7+
Me estoy sintiendo muy solo…

D                      A/C#         Bm  Bm/A
No quiero ni quiero ser tu dueño
Em                     Em/D          A7/C# A7
Es que un cariño a veces cae bien
D                       A/C#        Bm   Bm/A        Em
Yo tengo mis secretos y planes ocultos

Em                Em/D          A7  A#°
Que sólo abro para ti y nadie más
Bm               F#m7      G7+
Por qué me olvidas y hundes?
Bm                   F#m7           D/E
Y si yo me interesase en alguien?
Bm                  F#m7       G7+
Y si ella de repente me gana?
D                                             A                       Bm     Bm/A
Si es que nos gustamos, es claro que nos cuidamos
Em                               G                           C
Dices que me amas, sólo de boca hacia fuera
D                       A           Bm          Bm/A    G
O es que me engañas o no estás madura
A             D/F#    D
Dónde estás ahora
D
Aqui!!

----------------- Acordes -----------------
A = X 0 2 2 2 0
A#° = X 1 2 0 2 0
A/C# = X 4 X 2 5 5
A7 = X 0 2 0 2 0
A7/C# = X 4 5 2 5 X
Bm = X 2 4 4 3 2
Bm/A = X 0 4 4 3 X
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/E = X X 2 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
Em/D = X X 0 4 5 3
F#m7 = 2 X 2 2 2 X
G = 3 2 0 0 0 3
G7+ = 3 X 4 4 3 X
