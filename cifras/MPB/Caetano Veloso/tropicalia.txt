Caetano Veloso - Tropicália

(F7 Cm7)
Sobre a cabeça os aviões
(F7 Cm7)
Sob os meus pés, os caminhões
(F7 Cm7)
Aponta contra os chapadões, meu nariz
(Bbm Eb)
Eu organizo o movimento
(Bbm Eb)
Eu oriento o carnaval
(Bbm Eb)
Eu inauguro o monumento
(Bbm Eb)
No planalto central do país

(Eb Db)
Viva a bossa, sa, sa
 (Eb Db)                     2X      (Ab Db Eb Cm7)
Viva a palhoça, ça, ça, ça, ça

(F7 Cm7)
O monumento é de papel crepom e prata

(F7 Cm7)
Os olhos verdes da mulata
(F7 Cm7)
A cabeleira esconde atrás da verde mata
            (F7 Cm7)
O luar do sertão
(Bbm Eb)
O monumento não tem porta
(Bbm Eb)
A entrada é uma rua antiga,
(Bbm Eb)
Estreita e torta
(Bbm Eb)
E no joelho uma criança sorridente,
(Bbm Eb)
Feia e morta,Estende a mão

 (Eb Db)
Viva a mata, ta, ta
 (Eb Db)               2x         (Ab Db Eb Cm7)
Viva a mulata, ta, ta, ta, ta

(F7 Cm7)  (segue a mesma metrica das priemeiras partes até o fim)
No pátio interno há uma piscina
Com água azul de Amaralina
Coqueiro, brisa e fala nordestina
E faróis

(Bbm Eb)
Na mão direita tem uma roseira
Autenticando eterna primavera
E no jardim os urubus passeiam
A tarde inteira entre os girassóis

(Eb Db)
Viva Maria, ia, ia
(Eb Db)              2x         (Ab Db Eb Cm7)
Viva a Bahia, ia, ia, ia, ia

(F7 Cm7)
No pulso esquerdo o bang-bang
Em suas veias corre muito pouco sangue
Mas seu coração
Balança a um samba de tamborim

(Bbm Eb)
Emite acordes dissonantes
Pelos cinco mil alto-falantes
Senhoras e senhores
Ele pões os olhos grandes sobre mim

(Eb Db)
Viva Iracema, ma, ma
(Eb  Db)          2x         (Ab Db Eb Cm7)
Viva Ipanema, ma, ma, ma, ma

(F7 Cm7)
Domingo é o fino-da-bossa
Segunda-feira está na fossa
Terça-feira vai à roça
Porém,
(Bbm Eb)
o monumento
É bem moderno
Não disse nada do modelo
Do meu terno
Que tudo mais vá pro inferno, meu bem
Que tudo mais vá pro inferno, meu bem

(Eb Db)
Viva a banda, da, da
(Eb  Db)          2x         (Ab Db Eb Cm7)
Carmem Miranda, da, da, da, da

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Bbm = X 1 3 3 2 1
C = X 3 2 0 1 0
Cm7 = X 3 5 3 4 3
Db = X 4 6 6 6 4
Eb = X 6 5 3 4 3
F7 = 1 3 1 2 1 1
