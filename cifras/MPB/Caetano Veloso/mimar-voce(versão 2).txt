Caetano Veloso - Mimar Você

(intro) D7+     G7+    Em7     A7

 D7+             F#m7
Te quero só pra mim
G7+                  A7
Você mora em meu coração
D7+          Bm7  F#m7
Não me deixe só aqui
G7+                 A7
Esperando mais um verão
     Bm7                                Em7    A7
Te espero meu bem pra a gente se amar de novo
D7+   A/C#   Bm7
     Mimar você
G7+            A7
Nas quatro estações
     Bm7
Relembrar
G7+                 Em7     A7
Aquilo que passamos juntos
D7+    A/C#   Bm7
   Bem bom viver

G7+                             Em7
Andar de mãos dadas na beira da praia
           A7                    D7+
Por este momento eu sempre esperei
G7+                D7+
    Eu sempre esperei
G7+                D7+
    Eu sempre esperei
G7+                D7+
    Eu sempre esperei

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bm7 = X 2 4 2 3 2
Em7 = 0 2 2 0 3 0
F#m7 = 2 X 2 2 2 X
