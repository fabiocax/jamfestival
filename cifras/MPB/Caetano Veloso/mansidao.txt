Caetano Veloso - Mansidão

G7+            Gº       F#m7/5-                 B7/9-
Vasto céu, diminuta luz estelar brilha entre as nuvens
Em7/9             A7/13            G#7+         D7/9
Esta voz que o cantar me deu é uma festa paz em mim
G7+             Gº         F#m7/5-         B7/9-
Violão deita em minha mão, acordar algumas notas
Em7/9        A7/13      Am7            D7/9    G7+
Colocar com exatidão na sombra o clarão    sem fim
C7+               Bm7              Em7/9               F#m7       B7
Um amor que já me fez chorar agora não fará, não sofro mais assim
C7+                 Bm7                Am7  D7/9
Pois está tudo onde deve estar, nada será ruim
G7+           Gº        F#m7/5-              B7/9-
Mansidão, luminosa paz, minha voz e aquela estrela
Em7/9            A7/13                   Am7    D7/9  G7+
Vasto chão, sensação feliz, seda, linho, lã,        cetim.

----------------- Acordes -----------------
A7/13 = X 0 X 0 2 2
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
B7/9- = X 2 1 2 1 X
Bm7 = X 2 4 2 3 2
C7+ = X 3 2 0 0 X
D7/9 = X 5 4 5 5 X
Em7/9 = X 7 5 7 7 X
F#m7 = 2 X 2 2 2 X
F#m7/5- = 2 X 2 2 1 X
G#7+ = 4 X 5 5 4 X
G7+ = 3 X 4 4 3 X
Gº = 3 X 2 3 2 X
