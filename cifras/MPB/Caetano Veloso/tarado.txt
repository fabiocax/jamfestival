Caetano Veloso - Tarado

Am7                 D7/9
Gosto de ficar na praia deitado
Am7                 D7/9         G7+ Ab7+
Com a cabeça no travesseiro de areia
Am7                    D7/9
Olhando coxas gostosas por todo lado
Am7                     D7/9                G7+
Das mais lindas garotas e também das mais feias
 Ab7+                        G7+
Porque são todas gostosas sereias
Am7                       D7/9    Ab7+ C6/9
Do meu olhar de supremo tarado, tarado

----------------- Acordes -----------------
Ab7+ = 4 X 5 5 4 X
Am7 = X 0 2 0 1 0
C6/9 = X 3 2 2 3 3
D7/9 = X 5 4 5 5 X
G7+ = 3 X 4 4 3 X
