Caetano Veloso - Empty Boat

G#        C#          G#
From the stern to the bow
C#             G#
Oh; my boat is empty
C#               G#
Yes, my heart is empty
C#                G#          C# G# C# G# 2X
From the hole to the how

G#        C#        G#
From the rudder to the sail
C#            G#
Oh my boat is empty
C#              G#
Yes, my hand is empty
C#                 G#
From the wrist to the nail    C# G# C# G#

G#        C#           G#
From the ocean to the bay
C#               G#
Oh, the sand is clean

C#               G#
Oh, my mind is clean
C#                 G#
From the night to the day     C# G# C# G# 2X

G#        C#          G#
From the stern to the bow
C#              G#
Oh, my boat is empty
C#              G#
Oh, my head is empty
C#                G#
From the nape to the brow     C# G# C# G#

G#        C#          G#
From the east to the west
C#                 G#
Oh, the stream is long
C#                 G#
Yes, my dream is wrong
C#                 G#
From the birth to the death  C# G# C# G#

----------------- Acordes -----------------
C# = X 4 6 6 6 4
G# = 4 3 1 1 1 4
