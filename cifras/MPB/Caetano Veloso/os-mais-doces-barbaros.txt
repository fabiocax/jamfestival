Caetano Veloso - Os Mais Doces Bárbaros

Intro: F#m - A - F#m - A - E

F#m   A          F#m  A
Com amor no coração
F#m   A           E   F#m
Preparamos a invasão
Bm     D         A
Cheios de felicidade
   F#m        E      A   E
Entramos na cidade amada
F#m     A            F#m  A
Peixe Espada, peixe luz
F#m    A        E    F#m
Doce bárbaro Jesus
Bm     D         A
Sabe bem quem né otário
F#m         E     A    E
Peixe do aquário nada


F#m                          A
Alto astral, altas transas, lindas canções

F#m                          A
Afoxés, astronaves, aves, cordões
F#m                              A
Avançando através dos grossos portões
F#m                      A
Nossos planos são muito bons.


F#m      A        F#m   A
Com a espada de Ogum
F#m   A           E     F#m
E a benção de Olorum
Bm       D         A
Como um raio de Iansã
   F#m        E      A    E
Rasgamos a manhã vermelha
F#m   A             F#m   A
Tudo ainda é tal e qual
F#m    A           E     F#m
E no entanto nada igual
Bm      D          A
Nós cantamos de verdade
F#m                E     A     E
E é sempre outra cidade velha

F#m                          A
Alto astral, altas transas, lindas canções
F#m                          A
Afoxés, astronaves, aves, cordões
F#m                              A
Avançando através dos grossos portões
F#m                      A
Nossos planos são muito bons.

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
