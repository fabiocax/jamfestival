Caetano Veloso - La Barca

G7M             A#º            Am7
Dicen que la distancia es el olvido
Am6                        G7M
 pero yo no concibo esa razón
                   A#º         Am7
porque yo seguiré siendo el cautivo
Am6                            F#m7 B7/9-
de los caprichos de tu corazón.

  Em7         B7/9-          Em7/9
Supiste esclarecer mis pensamientos,
                             D7/9
 me diste la verdad que yo soñé,
                            C7/9
ahuyentaste de mí los sufrimientos
                             B7/9
en la primera noche que te amé.

C7M/9                       Cm6
Hoy mi playa se viste de amargura
Bm7                          Bm6 E7/9-
porque tu barca tiene que partir

Am7                        Am6
 a cruzar otros mares de locura
Dm7                          E7/9-
 cuida que no naufrague tu vivir.


Am7                              Cm6
Cuando la luz del sol se esté apagando
     Bm7                  Bm6  E7/9-
y te sientas cansada de vagar,
Am7                             D7/9
piensa que yo por ti estaré esperando
                          G7M
hasta que tú decidas regresar.

----------------- Acordes -----------------
