Caetano Veloso - Cobra Coral

 C7M                     D7(9)
Pára de ondular,agora, cobra coral:
                 Dm7
A fim de que eu copie as cores
G7            C7M
Com que te adornas,
                      Em7  Eb7      Dm7
A fim  de que eu faça um colar para dar
A7(b13)   Dm7  D7(9)

À  minha amada,
                  Fm6
A fim de que tua beleza
      C7M
Teu langor
     D7(13) D7(b13)
Tua elegân...cia

  D7(9)           G7           C7M
Reinem sobre as cobras não corais.

----------------- Acordes -----------------
A7(b13) = X 0 X 0 2 1
C7M = X 3 2 0 0 X
D7(13) = X 5 X 5 7 7
D7(9) = X 5 4 5 5 X
D7(b13) = X 5 X 5 7 6
Dm7 = X 5 7 5 6 5
Eb7 = X 6 5 6 4 X
Em7 = 0 2 2 0 3 0
Fm6 = 1 X 0 1 1 X
G7 = 3 5 3 4 3 3
