Caetano Veloso - Luz do Sol

D6(9)       Am7
Luz  do sol
Aº          G7M      Gm6
Que a folha traga e traduz
F#m7         B7(9)
    em verde novo
    A#7M               Bb7      D#7+       D7M(9)
Em folha, em graça, em vida em força, em  luz

D6(9)    Am7
Céu azul
Aº      G7M         Gm6
Que vem até onde os pés
F#m7         B7(9)       A#7M                        D7M(9)  D7/4(9) D7(9)
   Tocam na terra    e a terra inspira e exala seus azuis

G7M
Reza,  reza o  rio
Gm6               D7M(9)      D7/4/9  D7/9
Córrego pro rio, o rio pro mar
G7M               Gm6                  D7M(9)
Reza correnteza, roça a beira, doura a areia

C#m7/11                   C7/13
Marcha  um  homem sobre o chão
        Bm7                 Bm6
Leva no coração uma ferida acesa
Em7/9             A7(13)
Dono do sim e do não
            D7M(9)             D7M(9)
Diante da visão da infinita beleza
G#m7(b5)                G7(#11)       F#m7             B7(9)
Finda por ferir com a mão essa delicadeza a coisa mais querida
     E7/13               Em7    A7(9)
A    gló.....ria    da   vi.....da

D6(9)       Am7
Luz  do sol
Aº            G7M      Gm6
Que  a  folha traga e traduz
F#m7         B7(9)
    Em verde novo
    A#7M                 Bb7     D#7+       D7M(9)
Em folha,  em graça , em vida em força, em  luz

G7M
Reza,  reza o  rio
Gm6               D7M(9)      D7/4/9  D7/9
Córrego pro rio, o rio pro mar
G7M               Gm6                  D7M(9)
Reza correnteza, roça a beira, doura a areia
C#m7/11                   C7/13
Marcha  um  homem sobre o chão
        Bm7                 Bm6
Leva no coração uma ferida acesa
Em7/9             A7(13)
Dono do sim e do não
            D7M(9)             D7M(9)
Diante da visão da infinita beleza
G#m7(b5)                G7(#11)       F#m7             B7(9)
Finda por ferir com a mão essa delicadeza a coisa mais querida
     E7/13               Em7    A7(9)
A    gló.....ria    da   vi.....da

D6(9)       Am7
Luz  do sol
Aº            G7M      Gm6
Que  a  folha traga e traduz
F#m7         B7(9)
    Em verde novo
    A#7M                 Bb7     D#7+       D7M(9)
Em folha,  em graça , em vida em força, em  luz

----------------- Acordes -----------------
A#7M = X 1 3 2 3 1
A7(13) = X 0 X 0 2 2
A7(9) = 5 X 5 4 2 X
Am7 = X 0 2 0 1 0
Aº = 5 X 4 5 4 X
B7(9) = X 2 1 2 2 X
Bb7 = X 1 3 1 3 1
Bm6 = 7 X 6 7 7 X
Bm7 = X 2 4 2 3 2
C#m7/11 = X 4 4 4 5 4
C7/13 = X 3 X 3 5 5
D#7+ = X X 1 3 3 3
D6(9) = X 5 4 2 0 0
D7(9) = X 5 4 5 5 X
D7/4(9) = X 5 5 5 5 5
D7/4/9 = X 5 5 5 5 5
D7/9 = X 5 4 5 5 X
D7M(9) = X 5 4 6 5 X
E7/13 = 0 X 0 1 2 0
Em7 = 0 2 2 0 3 0
Em7/9 = X 7 5 7 7 X
F#m7 = 2 X 2 2 2 X
G#m7(b5) = 4 X 4 4 3 X
G7(#11) = 3 X 3 4 2 X
G7M = 3 X 4 4 3 X
Gm6 = 3 X 2 3 3 X
