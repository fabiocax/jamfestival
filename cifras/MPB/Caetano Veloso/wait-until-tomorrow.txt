Caetano Veloso - Wait Until Tomorrow

Intro: A D/E

           A                        D/G
Well, I'm standing here freezing, inside your golden garden
A                    D/E
Got my ladder leaned up against your wall
  A                    D/E
Tonight's the night we planned to run away together
     A
Come on dolly mae
           D/E
There's no time to stall
But now you're telling me that ah...
E7                              Am7/4  G
 I think we better wait till
E7                              Am7/4  G
 I think we better wait till
E7                              Am7/4  G
 I think we better wait till
Got to make sure it's right
So until goodnight

A
Oh, dolly mae
            D/E
How can you hang me up this way
       A                                D/E
Oh the phone you said you wanted to run off with me today
         A                                  D/E
Now I'm standing here like some turned down serenading fool
         A                             D/E
Hearing strange words stutter from the mixed up mind of you
And you keep telling me that ah...
E7                              Am7/4 G
 I think we better wait till
E7                              Am7/4 G
 I think we better wait till
E7                              Am7/4 G
 I think we better wait till
Got to make sure it's right
So until goodnight
A
Oh, dolly mae
           E7
Girl, you must be insane
A                                          E7
So unsure of yourself learning from your unsure window pane
     A                                     E7
Do I see a silhouette of somebody pointing something from a tree?
A
Click, bang
Oh, what a hang
     E7                   D/E
Your daddy just shot poor me
And I hear you say
As I fade away
E7                            Am7/4 G
We don't have to wait till
E7                            Am7/4 G
We don't have to wait till
E7                            Am7/4 G
We don't have to wait till
It must not have been right, so forever, goodnight
E7                            Am7/4 G
We don't have to wait till
It must not have been right, so forever, goodnight

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am7/4 = 5 X 5 5 3 X
D/E = X X 2 2 3 2
D/G = 3 X X 2 3 2
E7 = 0 2 2 1 3 0
G = 3 2 0 0 0 3
