Caetano Veloso - Os Argonautas

Intro: Am Bm5-/7 E7 Am Bm5-/7 E7 F G7 C Bm5-/7 E7 Am

Am     Bm5-/7    E   Am      C Bm5-/7
O barco, meu coração não aguenta
E7                 F
Tanta tormenta, alegria
 G                  C
Meu coração não contenta
 C#m5-/7   E7 Am Bm5-/7 E7 Am   F G    A
O dia, o marco, meu coração, o porto, não
            C#m     B7           E7  A
Navegar é preciso, viver não é preciso (2x)
Am  Bm5-/7    E7  Am         C  Bm5-/7
O barco, noite no céu tão bonito
 E7              F
Sorriso solto perdido
  G7           C
Horizonte, madrugada
   B7   E7  Am Bm5-/7 E7 Am
O riso, o arco, da madrugada
  F  G    A
O porto, nada

            C#m     B7           E7  A
Navegar é preciso, viver não é preciso (2x)
  Am  Bm5-/7 E7 Am    C  Bm5-/7 E7
O barco, o automóvel brilhante
                    F  G7
O trilho solto, o barulho
                     C
Do meu dente em tua veia
   E7         Am  Bm5-/7 E7  Am
O sangue, o charco, barulho lento
  F  G     A
O porto silêncio
            C#m     B7           E7  A
Navegar é preciso, viver não é preciso (2x)

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
Bm5-/7 = X 2 3 2 3 X
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
C#m5-/7 = X 4 5 4 5 X
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
