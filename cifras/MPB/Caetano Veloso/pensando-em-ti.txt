Caetano Veloso - Pensando Em Ti

F                Am
Eu amanheço pensando em ti
 Ab°     Gm
  eu anoiteço pensando em ti
             C7
Eu não te esqueço
                          F  Cm7/5-
É dia e noite pensando em ti
          D7                      Gm   Bbm
Eu vejo a vida pela luz dos olhos teus
            F     Dm
Me deixe ao menos
       Gm    C7    F
Por favor pensar e deus
      D7            Gm
Nos cigarros que eu fumo
   C7         F
Te vejo nas espirais
    Am            Ab°
Nos livros que eu tento ler
   C7   C5+        F
Em cada frase tu estás

        Eb          D7
Nas orações que eu faço
                      Gm
Eu encontro os olhos teus
Bbm            F     Dm
  me deixe ao menos
      Gm     C7     F
Por favor pensar em deus

----------------- Acordes -----------------
Ab° = 4 X 3 4 3 X
Am = X 0 2 2 1 0
Bbm = X 1 3 3 2 1
C5+ = X 3 2 1 1 X
C7 = X 3 2 3 1 X
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
