Caetano Veloso - Linha do Equador

Intro: Eb7M Fm7 Eb7M Fm7 Eb7M Fm7 Eb7M Fm7

Primeira Parte:
 Eb7M                          Fm7
Luz das estrelas laço do infinito
             Gm7      Fm7
Gosto tanto dela assim
 Eb7M                       Fm7
Rosa amarela voz de todo grito
             Gm7      F#m7(13)
Gosto tanto dela assim

Segunda Parte:
Fm7                    Eb7M
     Esse imenso, desmedido amor
      F#°
Vai além de seja o que for
Fm7                           Eb7M
     Vai além de onde eu vou, do que sou,
            F#°
Minha dor, minha linha do equador

Fm7                    Eb7M
     Esse imenso, desmedido amor
      F#°
Vai além de seja o que for
Fm7
     Passa mais além do

Primeira Parte com variação na letra:
 Eb7M
Céu de brasília
               Fm7
Traço do arquiteto
             Gm7      Fm7
Gosto tanto dela assim
 Eb7M                       Fm7
Gosto de filha música de preto
             Gm7      F#m7(13)
Gosto tanto dela assim

Segunda Parte com variação na letra:
Fm7                 Eb7M
     Essa desmesura de paixão
      F#°
É loucura do coração
Fm7                      Eb7M
     Minha Foz do Iguaçu Pólo Sul, meu azul
 F#°
Luz do sentimento nu
Fm7                    Eb7M
     Esse imenso, desmedido amor
      F#°
Vai além de seja o que for
Fm7                           Eb7M
     Vai além de onde eu vou, do que sou,
            F#°                 Gm7
Minha dor, minha linha do equador

Terceira Parte:
       Cm7              Ab7M
Mas é doce morrer nesse mar
        Abm6   Abm7M(9)       Abm6  Gm7
De lembrar e nunca esquecer
       Cm7                  Ab7M
Se eu tivesse mais alma pra dar
      Abm6  Abm7M(9)   Abm6
Eu daria,  isso pra mim é viver

Eb7M  Fm7  Gm7  Fm7 (2x)

Primeira Parte com variação na letra:
 Eb7M
Céu de brasília
               Fm7
Traço do arquiteto
             Gm7      Fm7
Gosto tanto dela assim
 Eb7M                       Fm7
Gosto de filha música de preto
             Gm7      F#m7(13)
Gosto tanto dela assim

Segunda Parte com variação na letra:
Fm7                 Eb7M
     Essa desmesura de paixão
      F#°
É loucura do coração
Fm7                      Eb7M
     Minha Foz do Iguaçu Pólo Sul, meu azul
 F#°
Luz do sentimento nu
Fm7                    Eb7M
     Esse imenso, desmedido amor
      F#°
Vai além de seja o que for
Fm7                           Eb7M
     Vai além de onde eu vou, do que sou,
            F#°                 Gm7
Minha dor, minha linha do equador

Terceira Parte:
       Cm7              Ab7M
Mas é doce morrer nesse mar
        Abm6   Abm7M(9)       Abm6  Gm7
De lembrar e nunca esquecer
       Cm7                  Ab7M
Se eu tivesse mais alma pra dar
      Abm6  Abm7M(9)   Abm6
Eu daria,  isso pra mim é viver

Eb7M  Fm7  Gm7  F#m7...

----------------- Acordes -----------------
Ab7M = 4 X 5 5 4 X
Abm6 = 4 X 3 4 4 X
Abm7M(9) = 4 2 5 3 X X
Cm7 = X 3 5 3 4 3
Eb7M = X X 1 3 3 3
F#m7(13) = X 10 X 10 11 12
F#° = 2 X 1 2 1 X
Fm7 = 1 X 1 1 1 X
Gm7 = 3 X 3 3 3 X
