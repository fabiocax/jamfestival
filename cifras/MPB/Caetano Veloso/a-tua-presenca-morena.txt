Caetano Veloso - A Tua Presença Morena

[Intro]  A7  G7  F7  E7

         A7     G7                F7               E7
A tua presença entra pelos sete buracos da minha cabeça
         A7
A tua presença
G7             F7               E7
  Pelos olhos, boca, narinas e orelhas
         A7
A tua presença
G7               F7                  E7
  Paralisa meu momento em que tudo começa
         A7
A tua presença
G7                 F7              E7
  Desintegra e atualiza a minha presença
          A7
A tua presença
G7              F7                           E7
  Envolve o meu tronco, meus braços e minhas pernas
         A7
A tua presença

G7                     D               F#
   É branca, verde, vermelha, azul e amarela
         B7
A tua presença
A7                G7                     F7                 E7
É negra, negra, negra, negra, negra, negra, negra, negra, negra
         A7
A tua presença
G7                  F7             E7
   Transborda pelas portas e pelas janelas
         A7
A tua presença
G7                 F7                E7
   Silencia os automóveis e as motocicletas
         A7
A tua presença
G7               F7                   E7
   Se espalha no campo derrubando as cercas
         A7
A tua presença
G7                F7                   E7
  É tudo o que se come, tudo o que se reza
         A7
A tua presença
G7            F7               E7
  Coagula o jorro da noite sangrenta
         A7
A tua presença
G7                  F7                 E7
   É a coisa mais bonita em toda a natureza
         A7
A tua presença
G7              D                 F#
  Mantém sempre teso o arco da promessa
          B7
A tua presença
A7           G7              F7              E7
   Morena, morena, morena, morena, morena, morena
   A
Morena

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
F# = 2 4 4 3 2 2
F7 = 1 3 1 2 1 1
G7 = 3 5 3 4 3 3
