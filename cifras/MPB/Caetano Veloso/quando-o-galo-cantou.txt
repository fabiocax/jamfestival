Caetano Veloso - Quando o Galo Cantou

                 Bb7M
Quando o galo cantou
                    A7                     G#6
Eu ainda estava agarrado ao seu pé e à sua mão
            G7
Uma unha na nuca, você já maluca
De tanta alegria do corpo, da alma
C7M(9)
E do espírito são
               F7(9)                      Bm7
Eu pensava que nós não nos desgrudaríamos mais
      E7(9-)      A7
O que fiz pra merecer essa paz
Dm7
Que o sexo traz?

          Bb7M
O relógio parou,
              A7                     G#6
Mas o sol penetrou entre os pelos brasis
                G7
Que definem sua perna e a nossa vida eterna,

Você se consterna e diz
             C7M(9)
"não, não se pode, ninguém, pode ser tão feliz"
            F7(9)
Eu queria parar
                          Bm7
Nesse instante de nunca parar
       E7(9-)   A7
Nós instituímos esse lugar
D7(9)
Nada virá

Gm                  C7(9)     C7(9-)  F7M
Deixa esse ponto brilhar no atlântico sul,
     D7(9-)
Todo azul
Gm                   C7(9)   C7(9-)
Deixa essa cântico entrar no sol,
       F7M
No céu nu
Em7               A7       Dm7
Deixa o pagode romântico soar
                 Bb7M
Deixa o tempo seguir,
               A7                  Dm7
Mas quedemos aqui, deixa o galo cantar

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bb7M = X 1 3 2 3 1
Bm7 = X 2 4 2 3 2
C7(9) = X 3 2 3 3 X
C7(9-) = X 3 2 3 2 X
C7M(9) = X 3 2 4 3 X
D7(9) = X 5 4 5 5 X
D7(9-) = X 5 4 5 4 X
Dm7 = X 5 7 5 6 5
E7(9-) = X X 2 1 3 1
Em7 = 0 2 2 0 3 0
F7(9) = X X 3 2 4 3
F7M = 1 X 2 2 1 X
G#6 = 4 X 3 5 4 X
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
