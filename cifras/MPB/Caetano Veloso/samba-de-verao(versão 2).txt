Caetano Veloso - Samba de Verão

DM7/9
Você viu só que amor nunca vi coisa assim
Am7/4                      Db7
E passou, nem parou mas olhou só pra mim
GM7
Se voltar vou atrás vou pedir, vou falar
Gm7                           C7/9
Vou dizer que o amor foi feitinho pra dar

F#m7 F#°             Em7/9    Dbm7/4
Olha... é como o verão
F#7/9+           Bm7    E7/9
Quente o coração
                 Em7/9
Salta de repente
                     A13
Para ver a menina que vem

DM7/9
Ela vem sempre tem esse mar no olhar
Am7/4                        Db7
E vai ver, tem que ser nunca tem quem amar

GM7
Hoje sim diz que sim já cansei de esperar
Gm7                           C7/9
Nem parei nem dormi só pensando em me dar

F#m7 F#°                 Em7/9 A13
Peço,  mas você não vem           bem
DM7/9  DbM7/9 CM7/9
Deixo então...
CM7/9  DbM7/9  DM7/9
Falo só...
DM7/9  DbM7/9  CM7/9
Digo ao céu...
CM7/9  DbM7/9  DM7/9
Mas você...
DM/F#
Vem.

----------------- Acordes -----------------
A13 = 5 X 4 6 5 X
Bm7 = X 2 4 2 3 2
Db7 = X 4 3 4 2 X
F#° = 2 X 1 2 1 X
Gm7 = 3 X 3 3 3 X
