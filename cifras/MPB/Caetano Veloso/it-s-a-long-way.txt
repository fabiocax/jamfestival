Caetano Veloso - It’s a Long Way

(Caetano Veloso)

Introdução ( D, A, G, C )   Dedilhado

 D, A, G, C
Woke up this morning
Singing an old, old Beatles song
We're not that strong, my lord
You know we ain't that strong
(Bm, F#m )
I hear my voice among others
In the break of day
Hey, brothers
Say, brothers
It's a long long long long way

Os olhos da cobra verde
Hoje foi que arreparei
Se arreparasse a mais tempo
Não amava quem amei


Arrenego de quem diz
Que o nosso amor se acabou
Ele agora estámais firme
Do que quando começou

A água com areia brinca na beira do mar
A água passa e a areia fica no lugar

E se não tivesse o amor
E se não tivesse essa dor
E se não tivesse sofrer
E se não tivesse chorar
E se não tivesse o amor

No Abaeté tem uma lagoa escura
Arrodeada de areia branca

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
