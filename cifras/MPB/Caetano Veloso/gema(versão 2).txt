Caetano Veloso - Gema

  C6/9                    Dm7/9
Brilhante e,de noite dentro da mata
           G7                          Am7
Na escuridao luz exata vejo voce
  C6/9                      Dm7/9
Divina e,diamantina presenca
        G7                                      Am7
Na solidao de quem pensa so em voce
   Fm7      Bb7  Eb7M  Ab7M     C#7M    Dm7 G7 C6/9
Esquecer nao   revelacao           deixa eu ver
          Dm7/9
Pedra clarao da floresta
               G7
Gema do olho da festa
                 Am7         C6/9
Deixa eu saber,meu amor
              Dm7/9
Dona da minha cabeca
                   G7
Nao nunca desapareca
            Am7
Do seu amor

      Fm7  Bb7 Eb7M      Ab7M C#7M Dm7 G7
Esquecer nao,me   perder nao
  C6/9                    Dm7/9
Estrela e na praca negra da selva
             G7
Gota de luz sobre a relva
                 Am7
Meu bem querer
      C6/9                Dm7/9
Lua sol e,centro do meu pensamento
                  G7
Meu canto dentro do vento
            Am7
Busca voce
        Fm7 Bb7 Eb7M   Ab7M  C#7M Dm7 G7
Esquecer nao esconder nao
   C6/9
Brilhante, ê

----------------- Acordes -----------------
Ab7M = 4 X 5 5 4 X
Am7 = X 0 2 0 1 0
Bb7 = X 1 3 1 3 1
C#7M = X 4 6 5 6 4
C6/9 = X 3 2 2 3 3
Dm7 = X 5 7 5 6 5
Dm7/9 = X 5 3 5 5 X
Eb7M = X X 1 3 3 3
Fm7 = 1 X 1 1 1 X
G7 = 3 5 3 4 3 3
