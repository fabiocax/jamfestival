Caetano Veloso - Iansã

Intro: Bm E7 A7 D

Bm                      E7
Senhora das nuvens de chumbo
 A7                        D
Senhora do mundo dentro de mim
 Bm                 E7
Rainha dos raios, rainha dos raios
 A7                      D
Rainha dos raios, tempo bom, tempo ruim
 Bm           E7
Senhora das chuvas de junho
 A7                       D
Senhora de tudo dentro de mim
 Bm                E7
Rainha dos raios, rainha dos raios
 A7                     D
Rainha dos raios, tempo bom, tempo ruim
                                E7
Eu sou o céu para as tuas tempestades
A7                B7
Um céu partido ao meio no meio da tarde

  E7           D                 E7    B7
Eu sou um céu para as tuas tempestades
 E7
Deusa pagã dos relâmpagos
 A7
Das chuvas de todo ano
          D
Dentro de mim, dentro de mim

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
