Caetano Veloso - Musa Híbrida

Bm7
Musa híbrida
     Bm6
Musa híbrida
Bm7                       Bm6
de olho verde e carapinha cúprica
         Bm7
cúprica, cúprica, cúprica
Bm6
onça, onça

Bbm7
A minha voz tão fosca
Bm7                    E7
Brilha por teus lábios bundos
Bbm7
A malha do teu pêlo
Bm7                 E7
Dongo, congo, gê, tupi, batavo, luso,
  A7M
Hebreu e mouro



Bbm7
Se espalha pelo mundo
Bm7             E7
Vamos refazer o mundo

Dbm7
Teu buço louro
Dm7
Meu canto mestiçoso

A7M
Tu, onça tu
F7M
Eu, jacaré, eu

----------------- Acordes -----------------
A7M = X 0 2 1 2 0
Bbm7 = X 1 3 1 2 1
Bm6 = 7 X 6 7 7 X
Bm7 = X 2 4 2 3 2
Dbm7 = X 4 6 4 5 4
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
F7M = 1 X 2 2 1 X
