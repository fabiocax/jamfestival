Caetano Veloso - O Calhambeque

Intro: F#  E  F#  E

( F#  E )
Essa e uma das muitas historias que acontecem comigo,
Primeiro foi suzy quando tinha labreta,depois comprei
Um carro parei na contra-mão tudo isso sem contar o
Tremendo tapa que tomei com a historia do splich-splach
Mas essa historia tambem é interessante.

   F#           E           F#           E
Mandei meu cadillac pro mecânico outro dia
F#             E           F#           E
Pois há muito tempo um conserto ele pedia
  F#                       B
Como vou viver sem meu carango pra correr
        F#          E    F#           E         F#      E F# C#7
Meu cadillac, bip, bip, quero consertar o cadillac
    F#         E         F#           E
Com muita paciência o rapaz me ofereceu
   F#          E             F#       E
Um carro todo velho que por lá apareceu

  F#                       B
Enquanto o cadillac consertava eu usava
        F#           E     F#         E           F#      E F# C#7
O calhambeque, bip, bip,  quero buzinar o calhambeque

  F#       E          F#          E
Saí da oficina um pouquinho desolado
   F#           E           F#          E
Confesso que estava até um pouco envergonhado
 F#                        B
Olhando para o lado com a cara de malvado
        F#           E    F#        E            F#      E F# C#7
O calhambeque, bip, bip, buzinei assim o calhambeque
F#     E          F#             E
Uma garota fez sinal para eu parar
  F#            E             F#           E
E no meu calhambeque fez questão de passear
    F#                        B
Não sei o que pensei, mas eu não acreditei
            F#           E      F#            E            F#      E F# C#7
Que o calhambeque, bip, bip, o broto quis andar no calhambeque
  F#             E               F#           E
E muitos outros brotos que encontrei pelo caminho
  F#            E            F#         E
Falavam "que estouro, que beleza de carrinho"
  F#                         B
E fui me acostumando e do carango fui gostando
        F#           E    F#           E           F#      E F# C#7
O calhambeque, bip, bip, quero conservar o calhambeque

F#     E         F#           E
 Mas o cadillac finalmente ficou pronto
  F#           E            F#          E
Lavado, consertado, bem pintado, um encanto
    F#                      B
Mas o meu coração na hora exata de trocar
        F#           E        F#         E               F#      E F# C#7
O calhambeque, bip, bip, meu coração ficou com o calhambeque

Segue o mesmo padrão enquento ele fala:

Bem...Vocês me desculpem mas agora eu...Vou me embora
existem mil garotas querendo passear comigo mas
e por causa do calhambeque sabe!!!
Bye...Bye...Byyyyye.

----------------- Acordes -----------------
B = X 2 4 4 4 2
C#7 = X 4 3 4 2 X
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
