Caetano Veloso - Luna Rossa

De: A.Vian / V. de Crescenzo

Introdução : C Ab7 D7 Db7+ G7

Cm            Db7+ G7
 Li ari il irà
             Cm
 Li ari il irà
             Db7+ G7
 Li ari il irà
             Cm7
 Li ari il irà
Cm7            Dm7/5- G7         Cm7
 Vaca distrattamente     abbandunato
                         Dm7/5-  G7      Cm7
 L'uocchie sotto 'o cappiello   annascunnute
Fm7                Gm7 C7  Fm7  Fm6
 Mane in'à sacca e bavero ajzto
Fm7                  Gm7 C7         Fm7  Fm6
 Vaca fiscann'a 'e ste___lle caso'asciute


Cm7 Dm7/5- G7 C7+

C7+                                       C/E         Ebº    Dm7
 E 'a luna rossa me parla 'e te lo le dommando si aspiette a me
                            Gº
E me risponne: si ó vvuò sapé ?
G7                   C7+
 Ccá 'num ce sta nisciuna!
E io chiamo 'o nomme pe'te vede'
            C/E      Ebº      Dm7
 Ma tutt'a gente ca parla 'e te responne:
                      Gº
 "É tardi: che vuò sapé?!
G7                  C7+
 Ccá 'num ce stá nisciuna!"
       Cm7
 Luna rossa
                  Fm7
 Chi me sarrà sincera?
       Dm7/5-
 Luna rossa
         Ab7          D7/A
 Se n'é ghiuta l'alta sera
            G7
 Senza me vedè!
             C7+
 E io dico ancora ca "aspietta a me
           C/E      Ebº      Dm7
 Fore'o balcone stanotte 'e ttre
                           Gº
 E prega 'e sante pe'me vedê
 G7                C7+
 Ma nun ce stà nisciuna!"
 Cm7                    Dm7/5- G7           Cm7
 Mille 'e cchiù a appuntame______nte aggiu tenuto
                    Dm7/5- G7              Cm7
 Tante e cchiù sigarey_____te aggio appicciato
 Fm7              Gm7/5- C7          Fm7  Fm6
 Mille tazze `e cafè        me sò `bevutto
Fm7               Gm7/5- C7        Fm7  Fm6
 Mille vucchelle amare      aggiuvasato

Cm7 Dm7/5- G7 C7+

             C7+
 E io dico ancora ca "aspietta 'e me
            C/E      Ebº      Dm7
 Fore 'o balcone stanotte 'e ttre
                           Gº
 E prega 'e sante pe'me vedè
G7                 C7+
 Ma nun ce stà nisciuna!
 Dm7/5-      G7    Cm7
 Ma nun ce stà nisciuna!
 Dm7/5-      G7    Cm7+
 Ma nun ce stà nisciuna!
 Cm9/7+
 Luna, luna, luna ...

----------------- Acordes -----------------
Ab7 = 4 6 4 5 4 4
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
C7 = X 3 2 3 1 X
C7+ = X 3 2 0 0 X
Cm = X 3 5 5 4 3
Cm7 = X 3 5 3 4 3
Cm7+ = X 3 5 4 4 3
Cm9/7+ = X 3 1 4 3 X
D7 = X X 0 2 1 2
D7/A = 5 X 4 5 3 X
Db7+ = X 4 6 5 6 4
Dm7 = X 5 7 5 6 5
Dm7/5- = X X 0 1 1 1
Ebº = X X 1 2 1 2
Fm6 = 1 X 0 1 1 X
Fm7 = 1 X 1 1 1 X
G7 = 3 5 3 4 3 3
Gm7 = 3 X 3 3 3 X
Gm7/5- = 3 X 3 3 2 X
Gº = 3 X 2 3 2 X
