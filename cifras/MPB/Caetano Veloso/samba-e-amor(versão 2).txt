Caetano Veloso - Samba e Amor

 Dm            Bm7/5-       Gm/Bb    A7
Eu faço samba e amor até mais tar......de
  Dm          Bm7/5-    D/C
E tenho muito sono de manhã
  G/B         Bbº      F/A      Gm/Bb
Escuto a correria da cidade que arde
   Bm7/5-          E7 A7  A7/5+
E apressa o dia de amanhã
   Dm          G/B           Gm/Bb     A7
De madrugada a gente inda se a.........ma
    Dm        Bm7/5-     D/C
E a fábrica começa a buzinar
  G/B         Bbº           F/A      Gm/Bb
O trânsito contorna a nossa cama - reclama
   Bm7/5-         E7    A7
Do nosso eterno espreguiçar
   Dm         D7         Gm7     C7/9
No colo da benvinda companheira
   F7+         Eb6/9   D7/9
No corpo do bendito violão
   Gm7/9         A/C#          D/C    G/B
Eu faço samba e amor a noite inteira

    Gm/Bb            A7         Dm   Bbm6  A7
Não tenho a quem prestar satisfação
   Dm            Bm7/5-       Gm/Bb    A7
Eu faço samba e amor até mais tar......de
  Dm          Bm7/5-       D/C
E tenho muito mais o que fazer
  G/B         Bbº      F/A         Gm/Bb
Escuto a correria da cidade - que alarde
  Bm7/5-         E7          A7   A7/5+
Será que é tão difícil amanhecer?
    Dm           D7/9-        Gm7    C7/9
Não sei se preguiçoso ou se covarde
  F7+          Eb6/9       D7/9
Debaixo do meu cobertor de lã
   G7/9          C#m6/5+      D/C    G/B
Eu faço samba e amor até mais tarde
  Gm/Bb       A7        Dm
E tenho muito sono de manhã

----------------- Acordes -----------------
A/C# = X 4 X 2 5 5
A7 = X 0 2 0 2 0
A7/5+ = X 0 X 0 2 1
Bbm6 = 6 X 5 6 6 X
Bbº = X 1 2 0 2 0
Bm7/5- = X 2 3 2 3 X
C#m6/5+ = X 4 X 3 5 5
C7/9 = X 3 2 3 3 X
D/C = X 3 X 2 3 2
D7 = X X 0 2 1 2
D7/9 = X 5 4 5 5 X
D7/9- = X 5 4 5 4 X
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
Eb6/9 = X X 1 0 1 1
F/A = 5 X 3 5 6 X
F7+ = 1 X 2 2 1 X
G/B = X 2 0 0 3 3
G7/9 = 3 X 3 2 0 X
Gm/Bb = 6 5 5 3 X X
Gm7 = 3 X 3 3 3 X
Gm7/9 = X X 5 3 6 5
