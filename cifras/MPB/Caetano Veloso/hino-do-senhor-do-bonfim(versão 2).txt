Caetano Veloso - Hino do Senhor do Bonfim

 C
Glória a Ti, neste dia de glória,
             F           G                       C      G
Glória a Ti, redentor, que há cem anos
              C
Nossos pais conduziste à vitória
             B7                          Em
Pelos mares e campos baianos.
F             G            C
Desta sagrada colina,
F                   G       C
Mansão da misericórdia,
                              F
Dá-nos a graça divina
 G                             C
Da justiça e da concórdia.
 C
Glória a Ti nessa altura sagrada,
            F           G           C       G
És o eterno farol, és o guia.
           C
És Senhor, sentinela avançada,

          B7                          Em
És a guarda imortal da bahia.
 F            G           C
Desta sagrada colina,
F                   G       C
Mansão da misericórdia,
                            F
Dá-nos a graça divina
  G                          C
Da justiça e da concórdia.
 C
Aos teus pés que nos deste o direito,
              F                      G               C       G
Aos teus pés que nos deste a verdade,
               C
Trata e exulta num férvido preito
                   B7                   Em
A alma em festa da tua cidade.
  F           G           C
Desta sagrada colina,
 F                 G        C
Mansão da misericórdia,
                              F
Dá-nos a graça divina
  G                         C   C7
Da justiça e da concórdia.
 F                          C
Dá-nos a graça divina
 G                           C
Da justiça e da concórdia.

----------------- Acordes -----------------
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
