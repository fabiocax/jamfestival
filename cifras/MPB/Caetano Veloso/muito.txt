Caetano Veloso - Muito

G              C/F
Eu sempre quis muito
Em            Eb7       C
Mesmo que parecesse ser modesto
Eb7      F      Bb      Am7(b5)
Juro que eu não presto
     D7(b9)  G    C/F
Eu sou muito louco, muito
Em            Eb7         C
Mas na sua presença O meu desejo
Eb  F  Bb       Am7(b5)
Parece pequeno
      D7(b9)  G    C/F
Muito é muito pouco, muito

Refrão:
Bm7(b5)  E7(b9)   C#m7/5-  C7+
    Broto você é muito, muito
Bm7(b5)  E7(b9)   A7(13) A7/5+ Am7 Adim
    Broto você é muito, muito


G              C/F
Eu nunca quis pouco
Em            Eb7         C
Falo de quantidade e intensidade
Eb7    F      Bb      Am7(b5)
Bomba de hidrogênio
     D7(b9)  G    C/F
Luxo para todos, todos
Em              Eb7              C
Mas eu nunca pensei que houvesse tanto
Eb   F  Bb       Am7(b5)
Coração brilhando
      D7(b9)  G    C/F
No peito do mundo louco

Refrão:
Bm7(b5)  E7(b9)   C#m7/5-  C7+
Gata você é muito
Bm7(b5)  E7(b9)   A7(13) A7/5+ Am7 Adim
Broto você é massa, massa

----------------- Acordes -----------------
A7(13) = X 0 X 0 2 2
A7/5+ = X 0 X 0 2 1
Adim = 5 X 4 5 4 X
Am7 = X 0 2 0 1 0
Am7(b5) = 5 X 5 5 4 X
Bb = X 1 3 3 3 1
Bm7(b5) = X 2 3 2 3 X
C = X 3 2 0 1 0
C/F = X 8 10 9 8 8
C7+ = X 3 2 0 0 X
D7(b9) = X 5 4 5 4 X
E7(b9) = X X 2 1 3 1
Eb = X 6 5 3 4 3
Eb7 = X 6 5 6 4 X
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
