Caetano Veloso - Tigresa

Introdução:

  Bm                  Em                    Bm              G
Uma tigresa de unhas negras e íris cor de mel
  Bm               Em               D                  Em
Uma mulher, uma beleza que me aconteceu
      Bm                            G                               Bm                 F#m
Esfregando sua pele de ouro marrom do seu corpo contra o meu
          G                         A7            Bm
Me falou que o mal é bom e o bem cruel

  Bm                          Em                    Bm                 G
Enquanto os pelos dessa deusa tremem ao vento ateu
  Bm               Em               D                  Em
Ela me conta, sem certeza, tudo que viveu
           Bm              G                          Bm                          F#m
Que gostava de política em mil novecentos e setenta e seis
            G                         A7            Bm
E hoje dança no Frenetic Dancing Days

  Bm                  Em                    Bm              G
Ela me conta que era atriz e trabalhou no "Hair"

  Bm               Em               D                  Em
Com alguns homens foi feliz, com outros foi mulher
      Bm                            G                               Bm                 F#m
Que tem muito ódio no coração, que tem dado muito amor
          G                         A7            Bm
E espalhado muito prazer e muita dor

  Bm                  Em                    Bm              G
Mas ela ao mesmo tempo diz que tudo vai mudar
  Bm               Em               D                  Em
Porque ela vai ser o que quis, inventando um lugar
      Bm                            G                               Bm                 F#m
Onde a gente e a natureza feliz vivam sempre em comunhão
          G                         A7            Bm
E a tigresa possa mais do que um leão

  Bm                  Em                    Bm              G
As garras da felina me marcaram o coração
  Bm               Em               D                  Em
Mas as besteiras de menina que ela disse não
      Bm                            G                               Bm                 F#m
E eu corri para o violão, num lamento, e a manhã nasceu azul
          G                         A7            Bm
Como é bom poder tocar um instrumento

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
