Caetano Veloso - Un Vestido Y Un Amor

E9
Te vi
B/Eb                        C#m7
Juntabas margaritas del mantel
    A9      A/B             G#m7
Ya sé que te traté bastante mal
      C#7/b9                    F#m7
No sé si eras un ángel o un rubí
          A/B     A9         A/B
O simplemente te vi

Te vi
Saliste entre la gente a saludar
Los astros se rieron otra vez
La llave de mandala se quebró
O simplemente te vi

G#m7         C#m7 C#m7/B  A9
Todo lo que diga esta de más
       A/B
Las luces siempre encienden

         G#m7
En el alma
G#7/#5           C#m7 C#m7/B  A9
Y cuando me pierdo en la ciudad
   A/B               A9
Vos ya sabes comprender
  A/B                A9
Es solo un rato no más
F#7          B7      Bm6     Ab/C
Tendría que llorar o salir a matar
  C#m7      C#m7/B       A9
Te vi,     te vi,        te vi
    A/B                 E9
Yo no buscaba a nadie y te vi

Te vi
Fumabas unos chinos en Madrid
Hay cosas que te ayudan a vivir
No hacías otra cosa que escribir
Y yo simplemente te vi

Me fui
Me voy de vez en cuando a algún lugar
Ya sé, no te hace gracia este país
Tenias un vestido y un amor
Yo simplemente te vi

----------------- Acordes -----------------
A/B = X 2 2 2 2 X
A9 = X 0 2 2 0 0
Ab/C = X 3 X 1 4 4
B/Eb = X 6 X 4 7 7
B7 = X 2 1 2 0 2
Bm6 = 7 X 6 7 7 X
C#7/b9 = X 4 3 4 3 X
C#m7 = X 4 6 4 5 4
C#m7/B = 7 4 6 4 5 4
E9 = 0 2 4 1 0 0
F#7 = 2 4 2 3 2 2
F#m7 = 2 X 2 2 2 X
G#7/#5 = 4 X 4 5 5 4
G#m7 = 4 X 4 4 4 X
