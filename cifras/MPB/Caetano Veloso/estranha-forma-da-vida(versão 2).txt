Caetano Veloso - Estranha Forma da Vida

Em          B      Em
Foi por vontade de Deus
                        B
Que eu vivo nesta ansiedade
    B7/F#             B7
Que todos os ais são meus
C                    B
Que é toda minha a saudade
B7                 Em
Foi por vontade de Deus


        B             Em
Que estranha forma de vida
E7                Am
Tem este meu coração
F#m7(5-) B7       B
Vive de  vida perdida
                     B7/F#
Quem lhe daria o condão
B7                     Em
Que estranha forma de vida


     B          Em
Coração independente
                   B
Coração que nao comando
B7/F#                  B7
Vives perdido entre a gente
C                 B
Teimosamente sangrando
B7              Em   Em9 Em
Coração independente

    B                Em   E7
Eu não te acompanho mais
                  Am
Pára, deixa de bater
F#m7(5-)            B7
Se não sabes aonde vais
                     B7/F#
Porque teimas em correr
    B7              Em   B7 Em
Eu não te acompanho mais

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
B7/F# = X X 4 4 4 5
C = X 3 2 0 1 0
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
Em9 = 0 2 4 0 0 0
F#m7(5-) = 2 X 2 2 1 X
