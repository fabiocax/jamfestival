Caetano Veloso - Baião da Penha

( Guio de Morais e David Nasser )

D7/9
Demonstrando a minha fé
Vou subir a Penha a pé
      G           D7/9
Pra fazer uma oração
                G
Vou pedir à padroeira
                D7/9
Numa prece verdadeira
       A            D7/9
Que proteja o meu baião

Nossa Senhora da Penha

Minha voz talvez não tenha
   G               D7/9
O poder de te exaltar
                     G
Mas dê benção , padroeira ,

                    D7/9
Pra essa gente brasileira
          A             D7/9
Que quer paz pra trabalhar
        G
Penha, Penha
D7/9    A             D7/9
Eu vim aqui me ajoelhar
         G
Venha, venha
D7/9     A              D7/9
Trazer paz para o meu lar.

----------------- Acordes -----------------
A = X 0 2 2 2 0
D7/9 = X 5 4 5 5 X
G = 3 2 0 0 0 3
