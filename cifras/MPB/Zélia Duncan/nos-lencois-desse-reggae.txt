Zélia Duncan - Nos Lencóis Desse Reggae

  
      A
Flash de viagem
            E7
Vontade de cantar um reggae
        D
Dono do impulso
                 A
Que empurra o coração
       E7          A
E o coração pra vida.
             A
E a vida é de morte
           E7
Minha única sorte
         D
Seria de ter esse reggae
        A
Vontade de fazê-lo
E7         A       A7
No meio da fumaça verde
       D
Não me negue
       A
Só me reggae
      E7                  A     A7
Só me toque quando eu pedir
       D
Senão pode
         
A
Ferir o dia
        D
Todo cinza
A      E7                 A
Que eu trouxe pra nós dois.
                  A
Nos lençóis desse reggae
           E7
Passagem pra Marrakesh
         D
Dono do impulso que empurra
      A          E7         A     A7
O coração e o coração pra vida.
      D
Não me negue
       A
Só me reggae
      E7                    A
Só me esfregue quando eu pedir
          A7
E eu peço sim!
      D
Senão pode
        A
Ferir o dia
      D
Todo cinza
A      E7    G      D
Que eu trouxe pra nós dois.
                       A   G   D
Nos lençóis desse reggae...
       A    G    D
Nos lençóis desse reggae
       A    G    D
Oh oh yeah... yeah... yeah...

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
G = 3 2 0 0 0 3
