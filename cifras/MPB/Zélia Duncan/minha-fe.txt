Zélia Duncan - Minha Fé

Intro: A   G   F   E

       A
Me pergunto
                    G
Onde é que foi parar
                F      E
A minha fé, a fé, a fé
   A
Voltou pra casa a pé 
   G
E ainda não chegou
   F
Espero na janela
E
Tento não me preocupar
A
Com ela
       G    F
Mas a fé
E
Sabe como é que é?
     A
Acredita em qualquer um
G
Tudo pra ela é comum
F
Tudo com ela é viável
   E
E eu aqui um tanto instável
A
Meio no claro,
G
Meio no escuro
    F
Tropeço
               E
Enquanto procuro acreditar
A
Na leveza,

Na cidade
G
Na beleza que me invade
F                    E
Na bondade dos automóveis


       F
Enquanto imóveis
E
Em suas garagens
       A
Me pergunto
                    G
Onde é que foi parar
                F      E
A minha fé, a fé, a fé
A
Nos tratados 

Nas palavras
G
Nos portões da tua casa
F
Nos transportes colectivos 
E
Na pureza das torcidas
A                    G   F   E
Gritando seus adjetivos
   A
Espero

Me quebro
    G
Tropeço no escuro
   F            E
E ainda procuro
         A
A minha fé

----------------- Acordes -----------------
A = X 0 2 2 2 0
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
