Zélia Duncan - Partir, Andar

De: Herbert Vianna 

Intro: (E  B9  C#m  A) 

E                  B9 
Partir andar, eis que chega 
C#m                  A 
Essa velha hora tão sonhada 
C#m                  B9 
Nas noites de velas acesas 
G#m               A 
No clarear da madrugada 

E                     B9 
Só uma estrela anunciando o fim 
C#m                      A 
Sobre o mar sobre a calçada 
C#m                B9 
E nada mais te prende aqui 
D9 
Dinheiros, grades ou palavras 

E                   B9 
Partir Andar, Eis que chega 
C#m                          A 
Não há como deter a alvorada 
C#m                            B9 
Pra dizer, um bilhete sobre a mesa 
G#m                 A 
Para se mandar, o pé na estrada 

E                       B9 
Tantas mentiras e no fim 
C#m                   A 
Faltava sempre uma palavra 
C#m               B9 
Faltava quase sempre um sim 
D9 
Agora já não falta nada 

Bb7+  A7/5+  G#6        G7 
Eu    não    quis, te fazer infeliz 
C7+      Am      Bm7/5-            B7        E 
Não quis.... Por tanto não querer,    talvez fiz... 

E  B9  C#m  A 
C#m  B9  G#m  A 
  

E                  B9 
Partir andar, eis que chega 
C#m                  A 
Essa velha hora tão sonhada 
C#m                  B9 
Nas noites de velas acesas 
G#m               A 
No clarear da madrugada 

E                     B9 
Só uma estrela anunciando o fim 
C#m                      A 
Sobre o mar sobre a calçada 
C#m                B9 
E nada mais te prende aqui 
D9
Agora já não falta nada... 
Não falta nada... 


Acordes:
    E   C#m  B9   A    D9   Bb7+ A7/5+ A6   G7   B7   Bm7/5-
E |-0---X----X----X----X----X----X-----4----3----X----X
A |-2---4----2----0----X----2----0-----0----5----2----2
D |-2---6----4----2----0----4----2-----3----3----4----3
G |-1---6----4----2----2----3----0-----5----4----2----2
B |-0---5----2----2----3----4----2-----4----3----4----3
e |-0---4----2----0----0----2----1-----0----3----2----X

----------------- Acordes -----------------
A = X 0 2 2 2 0
A6 = 5 X 4 6 5 X
A7/5+ = X 0 X 0 2 1
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
B9 = X 2 4 4 2 2
Bb7+ = X 1 3 2 3 1
Bm7/5- = X 2 3 2 3 X
C#m = X 4 6 6 5 4
C7+ = X 3 2 0 0 X
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
G#6 = 4 X 3 5 4 X
G#m = 4 6 6 4 4 4
G7 = 3 5 3 4 3 3
