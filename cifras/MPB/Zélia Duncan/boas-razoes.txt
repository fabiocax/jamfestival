Zélia Duncan - Boas Razões

Capo Casa 3

Intro: Am  Em  F7+  E7

Am          Em
Teu fogo inflama a razão
F7+        E7
Perguntas quimão então
Am          Em
Meu coração quer pensar
F7+        E7
Respostas caem ao chão

Dm        E7
Se eu vou te amar
Am        C
São boas minhas razões
D7      F7
Pra que te amar?
D7       Am        C
Por que razão te confessar?
D7          F7
Boas razões pra te ama
D7      Am      C
Não vou mais confessar

Am                Em
Talvez teu charme me atraia
F7+      E7
Talvez a tal solidão
Am           Em
Má sorte ou belas paavras
F7+         E7
Talvez um vício ou paixão

Dm
Não guardo mais
E7
Melhor falar
Am             C
Qualquer razão pra te amar
D7         F7
Não guardo mais
D7
Por que razão
Am      C
Te confessar
D7          F7
Boas razões pra te amar
D7      Am        C
Não vou mais confessar

Bb     F7
Parece um anjo
Bb    F7
Esquece as asas por aqui
Am C D7 F7
Glo-ri-a

Bb        F7
Santos perfumes
Bb        F7
Vozes do céu vem pra e ouvir
Am C  D7 F7
Ale-lu-ia

Am          Em
Talvez teu cheiro de flor
F7+          E7
Um jeito de adormcer
Am         Em
No frio faz um calor
F7+         E7
Motivos pra me aquecer

Dm
Não guardo mais
E7
Melhor falar
Am             C
Qualquer razão pra te amar
D7         F7
Não guardo mais
D7
Por que razão
Am      C
Te confessar
D7          F7
Minhas razões pra te amar
D7      Am        C
Não vou mais confessar
D7      F7
Boas razões pra te amar
D7      Am      C
Nunca mais confessar
D7        F7
Minhas razões pra te amar

( D7  Am  C  D7  F7 )

----------------- Acordes -----------------
Capotraste na 3ª casa
Am*  = X 0 2 2 1 0 - (*Cm na forma de Am)
Bb*  = X 1 3 3 3 1 - (*Db na forma de Bb)
C*  = X 3 2 0 1 0 - (*D# na forma de C)
D7*  = X X 0 2 1 2 - (*F7 na forma de D7)
Dm*  = X X 0 2 3 1 - (*Fm na forma de Dm)
E7*  = 0 2 2 1 3 0 - (*G7 na forma de E7)
Em*  = 0 2 2 0 0 0 - (*Gm na forma de Em)
F7*  = 1 3 1 2 1 1 - (*G#7 na forma de F7)
F7+*  = 1 X 2 2 1 X - (*G#7+ na forma de F7+)
