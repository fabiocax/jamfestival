Zélia Duncan - Aberto

(intro) F Eb Dm Bb F

Bb     F       Gm         Dm       Eb       F    Gm
Vou tentar manter o coração aberto pra você,
Cm          Gm
Apesar dos outros,
Cm          Gm
Apesar dos medos,
C            Gm            C          F
Apesar dos monstros nos meus pesadelos
Bb      F      Gm         Dm       Eb       F    Gm
Vou tentar manter o coração aberto pra você,
Cm         Gm
Apesar dos trincos,
Cm         Gm
Apesar dos trancos,
C          Gm               C         Eb
Apesar dos dias repetidos que são tantos.
C               Gm                C          Eb
Eu vou tentar manter o coração aberto pra você.
C
Apesar da chuva,
Gm
Apesar da rua,
C              Eb
Apesar da hora,
F
Apesar dos pesares, das canções, dos lugares,
Apesar dos meus pensamentos, dos perigos, dos próximos momentos.

F Bb F (repete 1º parte)

F
Eu de coração aberto pra você,
                          Bb
de coração aberto pra você.

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
