Zélia Duncan - Imorais

G7M D/C A

G7M     D/C       A  
Os imorais falam de nós
           G7M            D/C           A 
Do nosso gosto, nosso encontro, da nossa voz
G7M      D/C            A 
Os imorais se chocam por nós
   G7M              D/C          A        D/B
Por nosso brilho, nosso estilo, nossos lençóis

       Bm     F#m7           G7M
Mas um dia, eu sei, a casa cai
    Bm                F#m7
E então a moral da história
       G7M              D/C
Vai estar sempre na glória
    D/B               A        G7M   D/C   A
De fazermos o que nos satisfaz

G7M      D/C             A
Os imorais falam de nós
           G7M             D/C                A
Do nosso gosto, nosso encontro, da nossa voz
G7M      D/C                 A
Os imorais sorriram pra nós
          G7M              D/C               A
Fingiram trégua, fizeram média, venderam paz



----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D/B = X 2 0 2 3 2
D/C = X 3 X 2 3 2
F#m7 = 2 X 2 2 2 X
G7M = 3 X 4 4 3 X
