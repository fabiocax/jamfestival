Gilberto Gil - Chiclete Com Banana

Intro:   C7M  C#º  G  E7/9  A7  D7  Dm7  G7  C7M  C#º  G  E7/9  A7  D7  G7M  D7/9+

      G7M       E7/b9      A7
Eu só ponho bib-bop no meu samba
             D7/b9            G7M
Quando o Tio Sam tocar o tamborim
             E7/9                   A7
Quando ele pegar no pandeiro e no zabumba
                D7/9            G7M
Quando ele aprender que o samba não é rumba
 E7/b9         A7     D7/b9         G7M
Aí eu vou misturar, Miami com Copacabana
   E7/9                    A7
Chicletes eu misturo com banana
         D7/9             G7M
Eu o meu samba vai ficar assim

( G7M   C7/9   G7M )

E7/9   A7          D7          G7M
    Eu quero ver a grande confusão

( G7M   C7/9   G7M )
E7/9   A7        D7         G7M
       É o samba-rock meu irmão

G#º               Am7
É, mas em compensação
                   D7/9                    G7M
Quero ver o Boogie-woogie de pandeiro e violão
C7M         C#º G           E7/9   A7       D7/9      G7M
Quero ver o Tio Sam de frigideira, numa batucada brasileira
C7M         C#º G           E7/9   A7       D7/9      G7M
Quero ver o Tio Sam de frigideira, numa batucada brasileira

( G7M   C7/9   G7M )

E7/9   A7          D7          G7M
    Eu quero ver a grande confusão
( G7M   C7/9   G7M )
E7/9   A7        D7         G7M
       É o samba-rock meu irmão

( C7M  C#º  G  E7/9  A7  D7  Dm7  G7  C7M  C#º  G  E7/9  A7  D7  G7M  C7/9 )

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am7 = X 0 2 0 1 0
C#º = X 4 5 3 5 3
C7/9 = X 3 2 3 3 X
C7M = X 3 2 0 0 X
D7 = X X 0 2 1 2
D7/9 = X 5 4 5 5 X
D7/b9 = X 5 4 5 4 X
Dm7 = X 5 7 5 6 5
E7/9 = X X 2 1 3 2
E7/b9 = X X 2 1 3 1
G = 3 2 0 0 0 3
G#º = 4 X 3 4 3 X
G7 = 3 5 3 4 3 3
G7M = 3 X 4 4 3 X
