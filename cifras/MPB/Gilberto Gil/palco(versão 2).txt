Gilberto Gil - Palco

Intr.: E Gbm7 Abm7 Gbm7 E

 E          Gbm7         Abm7            Gbm7
Subo nesse palco, minha alma cheira a talco
      E          Gbm7      Abm7     Gbm7
Como bumbum de bebê,       de      bebê
 E          Gbm7         Abm7         A7M      B7
Minha aura clara, só quem é clarividente pode ver
A7M    B7
Pode ver
  E          Gbm7         Abm7            Gbm7
Trago a minha banda, só quem sabe onde é Luanda
    E           Gbm7       Abm7     Gbm7
Saberá lhe dar valor,      dar valor
 E          Gbm7           Abm7            A7M      B7
Vale quanto pesa prá quem preza o louco bumbum do tambor
A7M    B7
Do tambor

 Dbm7        Gbm7
 Fogo eterno prá afugentar

 A7M            B7
 O inferno prá outro lugar
 Dbm7        Gbm7
 Fogo eterno prá consumir
 A7M          B7
 O inferno, fora daqui
(introdução)
 La laia, laia, laia laia laia

 E           Gbm7           Abm7            Gbm7
Venho para a festa, sei que muitos têm na testa
        E           Gbm7    Abm7    Gbm7
O deus-sol como um sinal,   um sinal
E          Gbm7         Abm7         A7M      B7
Eu como devoto trago um cesto de alegrias de quintal
A7M     B7
De quintal
 E           Gbm7           Abm7            Gbm7
Há também um cântaro, quem manda é Deus a música
   E         Gbm7       Abm7     Gbm7
Pedindo prá deixar,     prá deixar
E          Gbm7        Abm7          A7M       B7
Derramar o bálsamo, fazer o canto, cantar o cantar
A7M     B7
Lá, lá, iá
Dbm7
Fogo eterno...

----------------- Acordes -----------------
A7M = X 0 2 1 2 0
Abm7 = 4 X 4 4 4 X
B7 = X 2 1 2 0 2
Dbm7 = X 4 6 4 5 4
E = 0 2 2 1 0 0
Gbm7 = 2 X 2 2 2 X
