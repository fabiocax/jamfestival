Gilberto Gil - Back In Bahia

Intro: A7  D7

A7                           D7                 A7
Lá em Londres, vez em quando me sentia longe daqui
D7                                              A7
Vez em quando, quando me sentia longe, dava por mim
                    E7                   D7                       A7
Puxando o cabelo nervoso, querendo ouvir Celly Campelo pra não cair
              E7                    D7                   A7
Naquela fossa em que vi um camarada meu de Portobello cair
               E7                       D7                     A7
Naquela falta de juízo que eu não tinha nem uma razão pra curtir
                 E7                    D7                            A7
Naquela ausência de calor, de cor, de sal, de sol, de coração pra sentir
               E7                   D7                       A7  E7 Etc...
Tanta saudade preservada num velho baú de prata dentro de mim
 A7                      D7                      A7
Digo num baú de prata porque prata é a luz do luar
D7                                            A7
Do luar que tanta falta me fazia junto com o mar
                   E7                 D7                  A7
Mar da Bahia cujo verde vez em quando me fazia bem relembrar

               E7                        D7                      A7
Tão diferente do verde também tão lindo dos gramados campos de lá
                E7                    D7                         A7
Ilha do Norte onde não sei se por sorte ou por castigo dei de parar
                 E7                        D7                    A7
Por algum tempo que afinal passou depressa, como tudo tem de passar
                  E7                 D7                    A7
Hoje eu me sinto como se ter ido fosse necessário para voltar
                E7                       D7                 A7
Tanto mais vivo de vida mais vivida, dividida pra lá e pra cá
                       E      A
Mamãe eu sei como é gostoso mamar!

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
