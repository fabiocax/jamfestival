Gilberto Gil - Esperando Na Janela

Intro 2x: E  B7  E  B7  E

Parte 1
E                           B7
Ainda me lembro do seu caminhar
                                E
Seu jeito de andar eu me lembro bem
                           B7
Fico tentando sentir o seu cheiro
                  E
E o jeito que ela tem

E                           B7
O tempo todo eu fico feito tonto
                               E
Sempre procurando, mas ela não vem
                          B7
E esse aperto no fundo do peito
                                   E
Desses que o sujeito não pode aguentar ai
                              B7
E esse aperto aumenta o meu desejo

                                     E  A E
E eu não vejo a hora de poder lhe falar

(Refrão)
                         B7
Por isso eu vou na casa dela ai ai
                      E
Falar do meu amor por ela vai
                      B7
Ta me esperando na janela, ai ai
                      E    A E
Não sei se vou me segurar

(repete refrão depois repete parte 1)

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
