Gilberto Gil - Coragem Pra Suportar

Dm                 G/D
Lá no sertão quem tem
G/D               Dm
Coragem pra suportar
Dm                 G/D
Tem que viver pra ter
G/D                Dm
Coragem pra suportar
Dm             G/D
E somente plantar
G/D                Dm
Coragem pra suportar
Dm             G/D
E somente colher
G/D      C        Dm
Coragem pra suportar
Dm                 C
E mesmo quem não tem
C                  Dm
Coragem pra suportar
Dm                 C
Tem que arranjar também

C                  Dm
Coragem pra suportar

Dm
Ou então
         Gm
Vai embora
            C7
Vai pra longe
     F
E deixa tudo
       Bb7
Tudo que é nada
           Eb
Nada pra viver
          G
Nada pra dar
        A7(#9)         Dm
Coragem     pra suportar

----------------- Acordes -----------------
A7(#9) = X X 7 6 8 8
Bb7 = X 1 3 1 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/D = X 5 5 4 3 X
Gm = 3 5 5 3 3 3
