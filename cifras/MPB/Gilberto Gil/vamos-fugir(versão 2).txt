Gilberto Gil - Vamos Fugir

[Intro] D  E  F#m

E|------------------|--------------------|--------------9------------14p12-|
B|--------14--------|-15-14-12-15-14-----|--------10-12---12-10------------|
G|--11-14----11-14--|----------------14--|---9-11---------------11---------|
D|------------------|--------------------|---------------------------------|
A|------------------|--------------------|---------------------------------|
E|------------------|--------------------|---------------------------------|

        A
Vamos fugir
        E
Deste lugar, baby
        F#m        D
Vamos fugir, Tô cansado de esperar
E                   F#m
Que você me carregue
         A
Vamos fugir
        E
Pra outro lugar, baby

        F#m
Vamos fugir
      D
Pra onde quer que você vá
E                   F#m
Que você me carregue
                A     E     D
Pois diga que irá,Irajá, Irajá
                                           A
Prá onde eu só veja você, você veja a mim só
    E      D
Marajó, Marajó
                                            A
Qualquer outro lugar comum, outro lugar qualquer
    E       D
Guaporé, Guaporé
                                            A
Qualquer outro lugar ao sol, Outro lugar ao sul
     E7        D
Céu azul, céu azul, onde haja só o meu corpo nu
                      E D E D
Junto ao seu corpo nu, uh, uh,uh,uh,uh

E|----------~10-9---------|----------10-9-7---------|
B|-------10--------9--7---|-------10--------9-9/10--|
G|--9-11------------------|--9-11-------------------|
D|------------------------|-------------------------|
A|------------------------|-------------------------|
E|------------------------|-------------------------|

           A
Vamos fugir
        E
Pra outro lugar, baby
        F#m
Vamos fugir
         D               E                F#m
Pra onde haja um tobogã, onde a gente escorregue

( D  E  F# )

E|-------------------12-9-7----------------|-------------------------|
B|-------10-10--------------10----10---10--|-------------------------|
G|--9-11-------11-9-------------11----11---|-----9---------------11--|
D|-----------------------------------------|--11---11-9--11p9--------|
A|-----------------------------------------|------------------12-----|
E|-----------------------------------------|-------------------------|

E|-----------------12-9-7------------------|------------14p12---12-14p12---12-14p12----|
B|-----10-10--------------10-10---10--10---|------7-10-------14---------14---------14--|
G|--11-------11-9---------------11--11-----|--6-9--------------------------------------|
D|-----------------------------------------|-------------------------------------------|
A|-----------------------------------------|-------------------------------------------|
E|-----------------------------------------|-------------------------------------------|

E|------------------------------14p12-|------------------14p12-14p12-14p12-14p12-14p12-|
B|--14p12-----------------12-14-----------|-------10-12--------------------------------|
G|--------14-11-14-11-14------------------|--9-11--------------------------------------|
D|----------------------------------------|--------------------------------------------|
A|----------------------------------------|--------------------------------------------|
E|----------------------------------------|--------------------------------------------|

        A
Vamos fugir
        E
Deste lugar, baby
        F#m        D
Vamos fugir, Tô cansado de esperar
E                   F#m
Que você me carregue
         A
Vamos fugir
        E
Pra outro lugar, baby
        F#m
Vamos fugir
      D
Pra onde quer que você vá
E                   F#m
Que você me carregue
                A     E     D
Pois diga que irá,Irajá, Irajá
                                           A
Prá onde eu só veja você, você veja a mim só
    E      D
Marajó, Marajó
                                            A
Qualquer outro lugar comum, outro lugar qualquer
    E       D
Guaporé, Guaporé
                                            A
Qualquer outro lugar ao sol, Outro lugar ao sul
     E7        D
Céu azul, céu azul, onde haja só o meu corpo nu
                      E D E D
Junto ao seu corpo nu, uh, uh,uh,uh,uh

E|----------~10-9---------|----------10-9-7---------|
B|-------10--------9--7---|-------10--------9-9/10--|
G|--9-11------------------|--9-11-------------------|
D|------------------------|-------------------------|
A|------------------------|-------------------------|
E|------------------------|-------------------------|

           A
Vamos fugir
        E
Pra outro lugar, baby
        F#m
Vamos fugir
         D               E                F#m
Pra onde haja um tobogã, onde a gente escorregue
     D
Todo dia de manhã
E                       F#m
Flores que a gente regue
    D
Uma banda de maçã
E                    F#m
Outra banda de reggae
      D
Tô cansado de esperar
E                   F#m
Que você me carregue
      D
Pra onde quer que você vá
E                   F#m
Que você me carregue
         D
Pra onde haja um tobogã
E                      F#m
Onde a gente escorregue
     D
Todo dia de manhã
E                       F#m
Flores que a gente regue
    D
Uma banda de maçã
E                    F#m
Outra banda de reggae

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
