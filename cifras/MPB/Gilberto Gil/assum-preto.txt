Gilberto Gil - Assum Preto

Introdução
E|-------12--0------12--0------12--0---
B|--10--13-----10--13-----10-13-----10-
G|-------------------------------------
D|-------------------------------------
A|-------------------------------------
E|-------------------------------------

Dedilhado entre as estrofes
E|----17--12-15-13-10--13-12-----12-10------------12--0------12--0------12--0---
B|-------------------12-----13-10-----12-13-10--13-----10--13-----10--13-----10-
G|------------------------------------------------------------------------------
D|------------------------------------------------------------------------------
A|------------------------------------------------------------------------------
E|------------------------------------------------------------------------------



        Dm
Tudo em vorta é só beleza
         D             Gm
Sol de Abril e a mata em frô

              Gm            Dm
  | Mas Assum Preto, cego dos óio
2x|           A7                 D
  | Num vendo a luz, ai, canta de dor


      Dm
Tarvez por ignorança
       D       Gm
Ou mardade das pió
              Gm          Dm
  | Furaro os óio do Assum Preto
2x|           A7                  D
  | Pra ele assim, ai, cantá de mió   (bis)


     Dm
Assum Preto veve sorto
    D           Gm
Mas num pode avuá
               Gm             Dm
  | Mil vezes a sina de uma gaiola
2x|            A7                Dm
  | Desde que o céu, ai, pudesse oiá     (bis)


    Dm
Assum Preto, o meu cantar
   D                 Gm
É tão triste como o teu
             Gm        Dm
  | Também roubaro o meu amor
2x|          A7                 D
  | Que era a luz, ai, dos óios meu  (bis)


----------------- Acordes -----------------
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
Gm = 3 5 5 3 3 3
