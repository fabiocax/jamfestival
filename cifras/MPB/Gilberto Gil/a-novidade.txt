Gilberto Gil - A Novidade

[Intro]  A  Bm  A  Bm  A  Bm  A  Bm

A       Bm        A     Bm
Uh-uuuuuuhh   ah-ahhhh
 A       Bm        A    Bm
Uh-uuuuuuhh   ah-ahhhh


   A                    Bm        A                  Bm
A novidade veio dar à praia, na qualidade rara de sereia
   A                   Bm    A                      Bm
Metade o busto de uma deusa Maia, metade um grande rabo de baleia
   A              Bm               A              Bm
A novidade era o máximo,   do paradoxo estendido na areia
                       A         Bm
Alguns a desejar seus beijos de deusa
              A             Bm
Alguns a desejar seu rabo pra ceia

C#m7            D7+       C#m7         D7+    C#m7    Bm7  E
   Ó mundo tão desigual, tudo é tão desigual, ô ô   ô ô ô

C#m7                D7+         C#m7     D7+    C#m7  Bm7   E
   De um lado este carnaval, de outro a fome total, ô ô   ô ô ô

     A            Bm       A                  Bm
E a novidade que seria um sonho, o milagre risonho da sereia
 A                      Bm      A                       Bm
Virava um pesadelo tão medonho, ali naquela praia, ali na areia
     A             Bm                     A          Bm
A novidade era a guerra   entre o feliz poeta e o esfomeado
         A         Bm                    A              Bm
Estraçalhando uma sereia bonita, despedaçando o sonho pra cada lado

C#m7            D7+       C#m7         D7+    C#m7    Bm7  E
   Ó mundo tão desigual, tudo é tão desigual, ô ô   ô ô ô
C#m7                D7+         C#m7     D7+    C#m7  Bm7   E
   De um lado este carnaval, de outro a fome total, ô ô   ô ô ô

A       Bm        A     Bm
Uh-uuuuuuhh   ah-ahhhh
 A       Bm        A    Bm
Uh-uuuuuuhh   ah-ahhhh

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
D7+ = X X 0 2 2 2
E = 0 2 2 1 0 0
