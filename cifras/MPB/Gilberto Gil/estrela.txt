Gilberto Gil - Estrela

[Intro]  B/E  Badd4/E  E9

 F#m7  B7(9)       F#m7   B7(9)
Há          de surgir
        F#m7
Uma estrela no céu
      B7(9)       Eadd9/G#   C#7(b9)
Cada vez que ocê sorrir
 F#m7   B7(9)      F#m7   B7(9)
Há            de apagar
    F#m7
Uma estrela no céu
   B7(9)           F#m7  F#7(#5)
Cada vez que ocê chorar

   Bmaj7
O contrário também
         C#m7       D#m7  C#m7
Bem que pode acontecer
           Bmaj7
De uma estrela brilhar

          C#m7    D#m7
Quando a lágrima cair
Em7   Dmaj7
Ou então
           Em7             F#m7
De uma estrela cadente se jogar
 Gmaj7      F#m7(9)
Só     pra ver
           Em7              Dmaj7  G7(#5)
A flor do seu sorriso se abrir
 Cmaj7
Hum!
 Bbmaj7    Amaj7  Gmaj7
Deus    fará
   F#m7
Absurdos
                Fmaj7
Contanto que a vida
        Emaj7  G7(#5)
Seja assim
 Cmaj7
Sim
Bbmaj7    Amaj7  Gmaj7
Um     altar
        F#m7
Onde a gente celebre
           Fmaj7      Emaj9
Tudo o que Ele consentir

----------------- Acordes -----------------
Amaj7 = X 0 2 1 2 0
B/E = X 7 9 8 7 7
B7(9) = X 2 1 2 2 X
Badd4/E = 0 2 4 4 5 2
Bbmaj7 = X 1 3 2 3 1
Bmaj7 = X 2 4 3 4 2
C#7(b9) = X 4 3 4 3 X
C#m7 = X 4 6 4 5 4
Cmaj7 = X 3 2 0 0 X
D#m7 = X X 1 3 2 2
Dmaj7 = X X 0 2 2 2
E9 = 0 2 4 1 0 0
Eadd9/G# = 4 X 4 4 5 X
Em7 = 0 2 2 0 3 0
Emaj7 = X X 2 4 4 4
Emaj9 = X 7 6 8 7 X
F#7(#5) = 2 X 2 3 3 2
F#m7 = 2 X 2 2 2 X
F#m7(9) = X X 4 2 5 4
Fmaj7 = 1 X 2 2 1 X
G7(#5) = 3 X 3 4 4 3
Gmaj7 = 3 X 4 4 3 X
