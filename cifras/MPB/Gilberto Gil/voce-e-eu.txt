Gilberto Gil - Você e Eu

D6(9)
Podem me chamar
       C#7(#9)
E me pedir e me rogar
        D6(9)
E podem mesmo falar mal
         F#m7(b5)         B7(b9)
Ficar de mal, que não faz mal

Em7
Podem preparar
           Gm6
Milhões de festas ao luar
           D6/F#
Que eu não vou ir
             F°
Melhor nem pedir
           Em7         A7(13)
Que eu não vou ir, não quero ir

         D6(9)
E também podem me intrigar

        C#7(#9)
E até sorrir e até chorar
        D6(9)
E podem mesmo imaginar
        F#m7(b5)      B7(b9)
O que melhor lhes parecer

Em7
Podem espalhar
                Gm6
Que eu estou cansado de viver
           D6/F#
E que é uma pena
    B7(b9)       E7(9)    F°
Pra quem me conheceu

F#m7(b5)  B7(b9)  E7(9)  A7(13)  D6(9)
Eu  sou   mais  você     e       eu

----------------- Acordes -----------------
A7(13) = X 0 X 0 2 2
B7(b9) = X 2 1 2 1 X
C#7(#9) = X 4 3 4 5 X
D6(9) = X 5 4 2 0 0
D6/F# = 2 X 0 2 0 X
E7(9) = X X 2 1 3 2
Em7 = 0 2 2 0 3 0
F#m7(b5) = 2 X 2 2 1 X
F° = X X 3 4 3 4
Gm6 = 3 X 2 3 3 X
