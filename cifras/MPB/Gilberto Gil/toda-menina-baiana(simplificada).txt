Gilberto Gil - Toda Menina Baiana

[Intro] Em  Dm  Em  Dm  Em  Dm  Em  Dm

 Em           Dm            Em        Dm
Toda menina baiana tem um santo que Deus dá
 Em           Dm           Em       Dm
Toda menina baiana tem encanto que Deus dá
 Em           Dm            Em        Dm
Toda menina baiana tem um jeito que Deus dá
  Em          Dm          Em      Dm            C
Toda menina baiana tem defeito também que Deus dá

          D7            F
Que Deus deu, que Deus dá
 C7                           Am
Que Deus entendeu de dar a primazia
 C7                                Am
Pro bem, pro mal, primeira mão na Bahia
 C7                              Am
Primeira missa, primeiro índio abatido
                  F
Também que Deus deu

  C7                            Am
Que Deus entendeu de dar toda magia
  C7                                   Am
Pro  bem, pro mal, primeiro chão na Bahia
  C7                             Am
Primeiro carnaval, primeiro pelourinho
                 F
Também que Deus deu

         Dm            Em        Dm           Em
A, a, a, a,  que Deus deu  ô, ô, ô,  que Deus dá
         Dm            Em        Dm           Em
A, a, a, a,  que Deus deu  ô, ô, ô,  que Deus dá


 Em           Dm            Em        Dm
Toda menina baiana tem um santo que Deus dá
 Em           Dm           Em       Dm
Toda menina baiana tem encanto que Deus dá
 Em           Dm            Em        Dm
Toda menina baiana tem um jeito que Deus dá
  Em          Dm          Em      Dm            C
Toda menina baiana tem defeito também que Deus dá

          D7            F
Que Deus deu, que Deus dá
 C7                           Am
Que Deus entendeu de dar a primazia
 C7                                Am
Pro bem, pro mal, primeira mão na Bahia
 C7                              Am
Primeira missa, primeiro índio abatido
                  F
Também que Deus deu
  C7                            Am
Que Deus entendeu de dar toda magia
  C7                                   Am
Pro  bem, pro mal, primeiro chão na Bahia
  C7                             Am
Primeiro carnaval, primeiro pelourinho
                 F
Também que Deus deu

         Dm            Em        Dm           Em
A, a, a, a,  que Deus deu  ô, ô, ô,  que Deus dá
         Dm            Em        Dm           Em
A, a, a, a,  que Deus deu  ô, ô, ô,  que Deus dá

         Dm            Em        Dm           Em
A, a, a, a,  que Deus deu  ô, ô, ô,  que Deus dá
         Dm            Em        Dm           Em
A, a, a, a,  que Deus deu  ô, ô, ô,  que Deus dá

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
