Gilberto Gil - Parabolicamará

Intro: D C F C D

    (  D  C  )
Antes mundo era pequeno porque Terra era grande
Hoje mundo é muito grande porque Terra é pequena
                                 F
Do tamanho da antena parabolicamará
          ( Bb C )                          F  ( C D )
Ê volta do mundo camará,ê mundo da volta camará
     ( C D )
Antes longe era  distante perto só quando dava
Quando muito ali defronte e o horizonte acabava
                                          F
Hoje lá trás dos montes dendê em casa camará
           ( Bb C )                              ( C D )
Ê volta do mundo camará, ê ê mundo da volta camará
F         C             Bb   C   F        C         Bb      C
De jangada leva uma eternidade, de saveiro leva uma encarnação
F         C            Bb   C   F         C        Bb       C
De jangada leva uma eternidade, de saveiro leva uma encarnação
( C D )
Pela onda luminosa, leva o tempo de um raio

Tempo que levava rosa pra aprumar o balaio
                                      F
Quando sentia que o balaio ia escorregar
          ( Bb C )                                                    ( C D )
Ê volta do mundo, camará, ê ê mundo da volta camará
              ( C D )
Esse tempo nunca passa não é de ontem nem de hoje
Mora no som da cabeça, nem tá preso nem foge
                                          F
No instante que tange o berimbau, meu camará
         ( Bb C )                                                     ( C D )
Ê volta do mundo, camará, ê ê mundo da volta camará


uma eternidade, de saveiro leva uma encarnação
F           C       Bb     C    F         C         Bb    C
De jangada leva uma eternidade, de saveiro leva uma encarnação
          Bb        C      D    ( Bb C D )
De avião o tempo de uma saudade
( C  D  )
Esse tempo não tem rédea vem nas asas do vento
O momento da tragédia, Chico Ferreira e Bento
Só souberam na hora do destino apresentar
Ê volta do mundo camará, ê ê mundo da volta camará ....
F            C     Bb C  F C Bb C
De jangada leva

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D = X X 0 2 3 2
F = 1 3 3 2 1 1
