Gilberto Gil - Morena

Intro:  Bb7/13 Am7  E7/9+/13-

    Am7 D7/9- Gm7 C7/9- Bm7
Uh, uh...
E7/9        Am7
Morena
    Am7 D7/9- Gm7 C7/9- Bm7
Uh, uh...
E7/9        Am7
Morena

Mundo, menino pequenino
Não tem cabeça pra pensar
                          B7/9+ E7
Leva qualquer proposta à perder
       C7/9                        F7+
Ah, morena, esse mundo cresce tudo
  Dm7/9              Cm7/9     Bm5-/7 E7
Cresce a esperança, sobe a maré
       Am7 D7/9- Gm7 C7/9- Bm7
Uh, uh...

E7/9-      Am7
Moreno
       Am7 D7/9- Gm7 C7/9- Bm7
Uh, uh...
E7/9-      Am7
Moreno

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
B7/9+ = X 2 1 2 3 X
Bb7/13 = 6 X 6 7 8 X
Bm5-/7 = X 2 3 2 3 X
Bm7 = X 2 4 2 3 2
C7/9 = X 3 2 3 3 X
Cm7/9 = X 3 1 3 3 X
Dm7/9 = X 5 3 5 5 X
E7 = 0 2 2 1 3 0
E7/9 = X X 2 1 3 2
F7+ = 1 X 2 2 1 X
Gm7 = 3 X 3 3 3 X
