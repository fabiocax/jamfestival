Gilberto Gil - Estrela

[Intro] E7M  Am6/E

F#m7  B7     F#m7   B7
Há          de surgir
        F#m7
Uma estrela no céu
      B7            G#m7   C#7(b9)
Cada vez que ocê sorrir
 F#m7   B7       F#m7    B7
Há            de apagar
     F#m7
Uma estrela no céu
    B7          G#m7    F#sus/13
Cada vez que ocê chorar

     B7M
O contrário também
         C#m7       D#m7  C#m7
Bem que pode acontecer
           B7M
De uma estrela brilhar

                C#m7   D#m7
Quando a lágrima cair
Em7 A7  D7M
Ou então
                 Em7      F#m7 F#7(#5)
De uma estrela cadente se jogar
G7M         F#m7
Só     pra ver
             Em7   G/A    D7M    G7/9
A flor do seu sorriso se abrir

C7M Bb7M           A9  G9
Hum!      Deus    fará
   F#m7                   F7M(b5)
Absurdos  contanto que a vida
      E9   G7/9
Seja assim
C7M Bb7M        A9  G9
Sim     no     altar
        F#m7
Onde a gente celebre
      F7M(b5)      E7M
Tudo o que Ele consentir

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A9 = X 0 2 2 0 0
Am6/E = X X 2 2 1 2
B7 = X 2 1 2 0 2
B7M = X 2 4 3 4 2
Bb7M = X 1 3 2 3 1
C#7(b9) = X 4 3 4 3 X
C#m7 = X 4 6 4 5 4
C7M = X 3 2 0 0 X
D#m7 = X X 1 3 2 2
D7M = X X 0 2 2 2
E7M = X X 2 4 4 4
E9 = 0 2 4 1 0 0
Em7 = 0 2 2 0 3 0
F#4/7 = 2 4 2 4 2 X
F#7 = 2 4 2 3 2 2
F#7(#5) = 2 X 2 3 3 2
F#m7 = 2 X 2 2 2 X
F7M(b5) = 1 X X 2 0 0
G#m7 = 4 X 4 4 4 X
G/A = 5 X 5 4 3 X
G7/9 = 3 X 3 2 0 X
G7M = 3 X 4 4 3 X
G9 = 3 X 0 2 0 X
