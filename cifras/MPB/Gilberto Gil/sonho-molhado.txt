Gilberto Gil - Sonho Molhado

(Intro) 4x
                                                            C7/9
E|-----0-|-----0-|-----0-|-----0-|-------0-|-----0-|-----0-|--
B|---1---|--3----|---6---|--8----|---11----|---1---|--3----|-3
G|3------|5------|7------|9------|12-------|3------|5------|-3
D|-------|-------|-------|-------|---------|-------|-------|-2
A|-------|-------|-------|-------|---------|-------|-------|-3
E|-------|-------|-------|-------|---------|-------|-------|--

          C7/9
Faz muito tempo que eu não tomo chuva
          F7/9                            G7/9        C7/9
Faz muito tempo que eu não sei o que é me deixar molhar
          C7/9
Bem molhadinho, molhadinho de chuva
          F7/9                            G7/9        C7/9
Faz muito tempo que eu não sei o que é pegar um toró
          Bm7           E7/9     Am7  Am/C
De tá na chuva quando a chuva cair
          Bm7           E7/9     Am7
De não correr pra me abrigar, me cobrir

          D7/9             G7+  G#°  Am7
De ser assim uma limpeza total
          D7/9        G7+
De tá na rua e ser um banho
   Bm7  E7/9
Na rua
    A7 A#7 B7 C7
Um banho

De ser igual quando a gente vai dormir
Que a gente sente alguém acariciar
Depois que passa o furacão de prazer
Ficar molhado e ser um sonho
Molhado
Um sonho

(Intro)

       C7/9
Eu vou dormir (enxutinho)
E acordo molhadinho de chuva

----------------- Acordes -----------------
A#7 = X 1 3 1 3 1
A7 = X 0 2 0 2 0
Am/C = X 3 2 2 5 X
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
Bm7 = X 2 4 2 3 2
C7 = X 3 2 3 1 X
C7/9 = X 3 2 3 3 X
D7/9 = X 5 4 5 5 X
E7/9 = X X 2 1 3 2
F7/9 = X X 3 2 4 3
G#° = 4 X 3 4 3 X
G7+ = 3 X 4 4 3 X
G7/9 = 3 X 3 2 0 X
