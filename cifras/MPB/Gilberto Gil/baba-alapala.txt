Gilberto Gil - Babá Alapalá

[Intro]  F  F#  G
         F  F#  G
         F  F#  G
         F  F#  G
         F  F#

    G      Dm
Aganju, Xangô
     G        Dm
Alapalá, Alapalá
     G          Dm    G
Alapalá, Xangô, Aganju
    G      Dm
Aganju, Xangô
     G        Dm
Alapalá, Alapalá
     G          Dm    G
Alapalá, Xangô, Aganju
    G      Dm
Aganju, Xangô
     G        Dm
Alapalá, Alapalá

     G          Dm    G
Alapalá, Xangô, Aganju
    G      Dm
Aganju, Xangô
     G        Dm
Alapalá, Alapalá
     G          Dm    G
Alapalá, Xangô, Aganju

        C             Gm7
O filho perguntou pro pai
            C         Gm7
Onde é que tá o meu avô?
       C              Gm7
O meu avô, onde é que tá?
         C            Gm7
O pai perguntou pro avô
            C          Gm7
Onde é que tá meu bisavô?
        C              Gm7
Meu bisavô, onde é que tá?
       F            C7/E
Avô perguntou: Bisavô
           Dm       C
onde é que tá tataravô?
      Dm       F     C
Tataravô, onde é que tá?
                  Gm7
Tataravô, bisavô, avô
       C         Gm7
Pai Xangô, Aganju
     C               G
Viva Egum, babá Alapalá!

( Dm  G  Dm  G  Dm  G )

    G      Dm
Aganju, Xangô
     G        Dm
Alapalá, Alapalá
     G          Dm    G
Alapalá, Xangô, Aganju
    G      Dm
Aganju, Xangô
     G        Dm
Alapalá, Alapalá
     G          Dm    G
Alapalá, Xangô, Aganju
    G      Dm
Aganju, Xangô
     G        Dm
Alapalá, Alapalá
     G          Dm    G
Alapalá, Xangô, Aganju
    G      Dm
Aganju, Xangô
     G        Dm
Alapalá, Alapalá
     G          Dm    G
Alapalá, Xangô, Aganju

     C            Gm7        C      Gm7
Alapalá, Egum, espírito elevado ao céu
         C          Gm7         C   Gm7
Machado alado, asas do anjo Aganju
     C            Gm7        C      Gm7
Alapalá, Egum, espírito elevado ao céu
          C               F
Machado astral, ancestral do metal
   C            F
Do ferro natural
         C
Do corpo preservado
  F               C      F
Embalsamado em bálsamo sagrado
       C             F           G
Corpo eterno e nobre de um rei nagô

----------------- Acordes -----------------
C = X 3 2 0 1 0
C7/E = 0 3 2 3 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
Gm7 = 3 X 3 3 3 X
