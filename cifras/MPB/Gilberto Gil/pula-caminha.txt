Gilberto Gil - Pula, Caminha

Intro: F7(11/13)  F7  E7(11/13)  E7  A7+  D7+
       F7(11/13)  F7  E7(11/13)  E7  A7+

D7+
Pula, caminha não pode parar
A7+     Aº           A7+         Cº
Pula, caminha que eu quero passar
Bm7    E7/9      Bm7      E7/9
Pula, morena, que eu quero ver
      B7                     E7/9 A7+
Ficar parado assim é que não pode ser
D7+
Eu também brincar não queria
E7/9        A7+
Sem querer entrei na folia
C#5+/7      D7+
Vou me esbaldar pra valer
B7                           E7/9 A7+
Ficar parado assim é que não pode ser

----------------- Acordes -----------------
A7+ = X 0 2 1 2 0
Aº = 5 X 4 5 4 X
B7 = X 2 1 2 0 2
Bm7 = X 2 4 2 3 2
C#5+/7 = X 4 X 4 6 5
Cº = X 3 4 2 4 2
D7+ = X X 0 2 2 2
E7 = 0 2 2 1 3 0
E7(11/13) = 0 2 0 2 2 X
E7/9 = X X 2 1 3 2
F7 = 1 3 1 2 1 1
F7(11/13) = 1 3 1 3 3 X
