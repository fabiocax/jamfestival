Gilberto Gil - Chiquinho Azevedo

Intro: | G | % | G | % | G | % | G | % |

          G
Chiquinho Azevedo
      Gm         Am
Garoto de Ipanema
                       Cm
Já salvou um menino
                       Am
Na Praia, no Recife
             G            F         Em
Nesse dia Momó também estava com a gente

Intermezzo: | G | % | G | % | G | % | G | % |

             G
Levou-se o menino
              Gm           Am
Pra uma clínica em frente
                         Cm
E o médico não quis

                         Am
Vir atender a gente
              G         F          Em
Nessa hora nosso sangue ficou bem quente

Intermezzo: | G | % | G | % | G | % | G | % |

         G
Menino morrendo
       Gm           Am
Era aquela agonia
                      Cm
E o doutor só queria
                      Am
Mediante dinheiro
                      G      F     Em
Nessa hora vi quanto o mundo está doente

               G
Discutiu-se muito
    Gm           Am
Ameaçou-se briga
                       Cm
Doze litros de água
                       Am
Tiraram da barriga
              G       F      Em
Do menino que sobreviveu finalmente

Intermezzo: | G | % | G | % | G | % | G | % |

                   A7       D7
Muita gente me pergunta
                   G        Em
Se essa estória aconteceu
                  A7        D7
Aconteceu minha gente
                       G  Em
Quem está contando sou eu
              A7      D7
Aconteceu e acontece
               G      Em
Todo dia por aí
              A7      D7
Aconteceu e acontece
                          G
Que esse mundo é mesmo assim

          G
Chiquinho Azevedo
      Gm           Am
Teve muita coragem
                       Cm
Lá na Boa Viagem
                       Am
Na praia, no Recife
          G           F          C  G Ab7 G7
Nesse dia Momó também estava com a gente

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Ab7 = 4 6 4 5 4 4
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
