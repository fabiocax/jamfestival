Gilberto Gil - As Pegadas do Amor

Introo: C7   F7+   Fm6   C

       G7          F#7
Nem um bode prá sangrar
       Bb
Nem um bé
    G7             F#7
Uma cabra a espernear
     F7/5-   G#9/11+
Um baé
      F7/5-           G#9/11+
Um porquinho, um bezerrinho
    D#m              D#m7   D#m6
Uma pomba, uma preá
   C#m                   C#m7   C#m6
Animal de sangue quente
   Bm                 Bm7   Bm6
Atacado a sangue frio
   Bbm               Bbm7   Bbm6   Bbm7   Bbm6   Bbm7   Bbm6   Bbm7   Bbm6
Só ver sangue jorrar
       G7         F#7
Nem um pouco de pesar

     Bb
De pavor
       G7           F#7
Nem um cabra a implorar
      F7/5-   G#9/11+
Por favor
       F7/5-          G#9/11+
Nem um corpo a estrebuchar
      D#m                 D#m7   D#m6
Ao tremor das minhas mãos
        C#m              C#m7   C#m6
Nem uma marca de horror
        Bm               Bm7   Bm6
No chão do meu coração
        Bbm             Bbm7   Bbm6   Bbm7   Bbm6   Bbm7   Bbm6   Bbm7   Bbm6
Só as pegadas do amor.

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bbm = X 1 3 3 2 1
Bbm6 = 6 X 5 6 6 X
Bbm7 = X 1 3 1 2 1
Bm = X 2 4 4 3 2
Bm6 = 7 X 6 7 7 X
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
C#m6 = X 4 X 3 5 4
C#m7 = X 4 6 4 5 4
C7 = X 3 2 3 1 X
D#m = X X 1 3 4 2
D#m6 = X X 1 3 1 2
D#m7 = X X 1 3 2 2
F#7 = 2 4 2 3 2 2
F7+ = 1 X 2 2 1 X
F7/5- = 1 X 1 2 0 X
Fm6 = 1 X 0 1 1 X
G#9/11+ = 4 1 1 1 3 4
G7 = 3 5 3 4 3 3
