Gilberto Gil - Viramundo

Intro.: (E B7/F#) F#7 - B7   E

           B7/F#      E
Sou viramundo virado
            B7/F#          E
Nas rondas das maravilhas
    A                E7/9
Cortando a faca e facão
                  E7/9
Os desatinos da vida
   B7/F#            E
Gritando para assustar
    G#7        C#m
A coragem da inimiga
 B7/F#                   E
Pulando pra não ser preso
      E7 (7ª casa)      A
Pelas cadeias da intriga
    E7/9                A
Prefiro ter toda a vida
 B7/F#              E
A vida como inimiga

    G#7        C#m
A ter na morte da vida
 B7/F#             E
Minha sorte decidida

Intro

         B7/F#        E
Sou viramundo virado
            B7/F#    E
Pelo mundo do sertão
    A                 E7/9
Mas ainda viro este mundo
     A                E7/9
Em festa, trabalho e pão
 B7/F#             E
Virado será o mundo
    G#7        C#m
E viramundo verão
 B7/F#               E
O virador deste mundo
      E7 (7ª casa)   A
Astuto, mau e ladrão
     E7/9           A
Ser virado pelo mundo
 B7/F#                E
Que virou com certidão
    G#7        C#m
Ainda viro este mundo
   B7/F#                E
Em festa, trabalho e pão

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
B7/F# = X X 4 4 4 5
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
E7/9 = X X 2 1 3 2
F#7 = 2 4 2 3 2 2
G#7 = 4 6 4 5 4 4
