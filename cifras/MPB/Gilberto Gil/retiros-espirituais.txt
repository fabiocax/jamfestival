Gilberto Gil - Retiros Espirituais

Intro: C#m F#m Bm Bm6/b C#7/9- F#m A E11

A                   A7          D7+                       E7b  Fº
Nos meus retiros espirituais descubro certas coisas tão normais
F#m7+/9       B7/9                  Bm5-/7 A7
Como estar defronte de uma coisa e ficar
D               E7b     C#m7         F#m
Horas à fio com ela, bárbara, bela, tela de TV
B7/9                                                 E4/7
Você há de achar gozado Barbarela dita assim dessa maneira
                 E5+  A  A7/9   D7+             E7
Brincadeira sem nexo que gente maluca gosta de fazer
A                       A7           D7+               C#7
Eu diria mais tudo não passa dos espirituais sinais iniciais desta canção
 F#m
Retirar tudo o que eu disse, reticenciar que eu juro
 B7/9
Censurar ninguém se atreve
  E7              E5+/5/3b               Am9    E7/9
É tão bom sonhar contigo, oh, luar tão cândido
A                   A7          D7+                    E7b  Fº
Nos meus retiros espirituais descubro certas coisas anormais

F#m7+/9       B7/9                Bm5-/7 A7
Como alguns instantes vacilantes e só
D               E7b       C#m7         F#m
Só com você e comigo, pouco faltando, devendo chegar
B7/9                                       E4/7
Um momento novo, vento devastando como um sonho
                        E5+  A  A7/9   D7+             E7
Sobre a destruição de tudo, que gente maluca gosta de sonhar
A                        A7           D7+               C#7
Eu diria sonhar com você jaz nos espirituais sinais iniciais desta canção
 F#m
Retirar tudo o que eu disse, reticenciar que eu juro
 B7/9
Censurar ninguém se atreve
  E7              E5+/5/3b               Am9    E7/9
É tão bom sonhar contigo, oh, luar tão cândido
A                   A7          D7+                        E7b  Fº
Nos meus retiros espirituais, descubro certas coisas tão banais
F#m7+/9       B7/9                Bm5-/7 A7
Como ter problemas, ser o mesmo que não
D                 E7b     C#m7              F#m
Resolver tê-los é ter, resolver ignorá-los é ter
B7/9                                                E4/7
Você há de achar gozado ter que resolver de ambos os lados
             E5+  A  A7/9   D7+             E7
De minha equação, que gente maluca tem que resolver
A                        A7           D7+               C#7
Eu diria o problema se reduz aos espirituais sinais iniciais desta canção
 F#m
Retirar tudo o que eu disse, reticenciar que eu juro
 B7/9
Censurar ninguém se atreve
  E7              E5+/5/3b               Am9    E7/9
É tão bom sonhar contigo, oh, luar tão cândido

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
A7/9 = 5 X 5 4 2 X
Am9 = X 0 2 4 1 0
B7/9 = X 2 1 2 2 X
Bm = X 2 4 4 3 2
Bm5-/7 = X 2 3 2 3 X
C#7 = X 4 3 4 2 X
C#7/9- = X 4 3 4 3 X
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
D7+ = X X 0 2 2 2
E11 = 0 2 2 2 0 0
E4/7 = 0 2 0 2 0 X
E5+ = 0 X 2 1 1 0
E7 = 0 2 2 1 3 0
E7/9 = X X 2 1 3 2
F#m = 2 4 4 2 2 2
F#m7+/9 = 2 0 3 1 X X
Fº = X X 3 4 3 4
