Vanessa da Mata - Ilegais

(Bm7   C#m7)
Desse jeito vão saber de nós dois
Dessa nossa vida
E será uma maldade veloz
Malignas línguas
Nossos corpos não conseguem ter paz
Em uma distância
Nossos olhos são dengosos demais
Que não se consolam, clamam fugazes
Olhos que se entregam, ilegais

(Bm7   C#m7)
Eu só sei que eu quero você
Pertinho de mim
Eu, quero você dentro de mim
Eu, quero você em cima de mim
Eu quero você

(Bm7   C#m7)
Desse jeito vão saber de nós dois
Dessa nossa farra

E será uma maldade voraz
Pura hipocrisia
Nossos corpos não conseguem ter paz
Em uma distância
Nossos olhos são dengosos demais, demais
Que não se consolam, clamam fugazes
Olhos que se entregam, olhos ilegais

(Bm7   C#m7)
Eu só sei que eu quero você
Pertinho de mim
Eu, quero você dentro de mim
Eu, quero você em cima de mim
Eu quero você

----------------- Acordes -----------------
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
