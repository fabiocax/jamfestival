Chico Buarque - O Jumento

Luiz Enriquez & Sérgio Bardotti (versão: Chico Buarque)

  A
Jumento não é

Jumento não é
                     E7
O grande malandro da praça
   A         E7       A
Trabalha, trabalha de graça.

Não agrada a ninguém

Nem nome não tem
                     E7
É manso e não faz pirraça.
                A       D     E7
Mas quando a carcaça ameaça rachar
    D           A           E7         A
Que coices, que coices, que coices que dá.


  E7       A           E7         A
O pão, a farinha, o feijão, carne-seca,
  E7       A       E7        A
limão, mexerica, mamão, melancia,
     E7       A     E7 A
quem é que carrega? Hi-ho.

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
