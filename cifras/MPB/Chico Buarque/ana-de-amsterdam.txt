Chico Buarque - Ana de Amsterdam

Bm7(9)
          Sou Ana do dique e das docas
          Da compra, da venda, da troca das pernas
          Dos braços, das bocas, do lixo, dos bichos, das fichas
    A/G
          Sou Ana das loucas
               B
          Até amanhã
            A     B
          Sou Ana, da cama
            A      Am6      G7M
          Da cana, fulana, sacana
            Gm6    F#7(4) B A B
          Sou Ana de Amsterdam

                   B7M
          Eu cruzei um oceano
              Am6     Em/G
          Na esperança de casar
               G°       G#/F#
          Fiz mil bocas pra Solano

               C#7/F#     F#7         B  A B A B
          Fui beijada        por    Gaspar

                Bm7(9)
          Sou Ana de cabo a tenente
          Sou Ana de toda patente, das Índias
          Sou Ana do Oriente, Ocidente, acidente, gelada
    A/G
          Sou Ana, obrigada
              B        A
          Até amanhã,     sou Ana
            B        A       Am6   G7M
          Do cabo, do raso, do rabo, dos ratos
           Gm6     F#7(4) B A B
          Sou Ana de Amsterdam

                     B7M
          Arrisquei muita braçada
               Am6       Em/G
          Na esperança de outro mar
               G°       G#/F#
          Hoje sou carta marcada
              C#7/F#        F#7        B   A B A B
          Hoje sou         jogo   de   azar

                    Bm7(9)
          Sou Ana de vinte minutos
          Sou Ana da brasa dos brutos na coxa
          Que apaga charutos
          Sou Ana dos dentes rangendo
    A/G
          E dos olhos enxutos
               B       A
          Até amanhã,     sou Ana
              B     A      Am6        G7M
          Das marcas, das macas, das vacas, das pratas
            Gm6    F#7(4) B A B A B Bm7(9)
          Sou Ana de Amsterdam

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/G = 3 X 2 2 2 X
Am6 = 5 X 4 5 5 X
B = X 2 4 4 4 2
B7M = X 2 4 3 4 2
Bm7(9) = X 2 0 2 2 X
C#7/F# = 2 4 3 4 2 X
Em/G = 3 X 2 4 5 X
F#7 = 2 4 2 3 2 2
F#7(4) = 2 4 2 4 2 X
G#/F# = 2 X 1 1 1 X
G7M = 3 X 4 4 3 X
Gm6 = 3 X 2 3 3 X
G° = 3 X 2 3 2 X
