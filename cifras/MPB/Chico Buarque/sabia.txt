Chico Buarque - Sabiá

[Intro:] Ebm7/Bb Bb7M(9) Bb7(9-/11+) Gb/Bb F/A E/G#
         Bb7(4)(9) Bb7(b9) Bb7(13)

Eb6(9) Ebº   Fm7 Bb7(b9)             Eb6(9) Ebº   Fm7 G7
Vou    vol...tar        sei que ainda vou   vol...tar
             Cm7 G#m7M/B Gm/Bb A7(b5)           Ab7M Gm7 Fm7
Para o meu lugar   foi    lá         e  é ainda lá
        Bº      Cm7 Eb7/Bb   Aº Abº       Cm7 Cº Fm7(9)/C G7
Que eu hei de ouvir   can...tar    uma sabiá
Eb6(9) Ebº   Fm7 Bb7(b9)              F#6 F#º   G#m7 C#7(b9)
Vou    vol...tar        sei que ainda vou vol...tar
             F#m       F#m/E  D#m7     G#7(b9)  C#m7 Am6
Vou deitar à sombra de uma palmeira que  já não há
          C#m7 F#7(b9)           Bm7 Gm6
Colher a flor         que já não dá
         Bm7  E7(4)(9)  C#7/F  F#m
E algum amor talvez possa espantar
F(#5) Am7/E  Cm/Eb        D7  Bbm6/Db C7(4)
As     noites     que eu não queria
    C7  Abm6/Cb Bb7(4) Bb7(4)(b9) Bb7(9)(13)
E anunci...ar         o    di.........a


Eb6(9) Ebº   Fm7 Bb7(b9)             Ebm7M Ebm7 Ebº   G#m7 C#7(b9)
Vou    vol...tar        sei que ainda vou       vol...tar
               F#7M    F#7         B7M      G#m7   D#m7
Não vai ser em vão que fiz tantos planos de me enganar
   D#m7/C# B7M      G#m7     F#6
Como fiz enganos de me encontrar
    B7M/F#  E7M      B/D# D#m7/C#
Como fiz estradas de me  perder
       B7M    E7M           B/D# D#m6 Dm6 C#m6 Bb7(b13) E6(9) Eb6(9)
Fiz de tudo e nada de te esquecer

----------------- Acordes -----------------
A7(b5) = X 0 1 0 2 X
Ab7M = 4 X 5 5 4 X
Abm6/Cb = X 2 3 1 4 1
Abº = 4 X 3 4 3 X
Am6 = 5 X 4 5 5 X
Am7/E = X X 2 2 1 3
Aº = 5 X 4 5 4 X
B/D# = X 6 X 4 7 7
B7M = X 2 4 3 4 2
B7M/F# = X X 4 4 4 6
Bb7(13) = 6 X 6 7 8 X
Bb7(4) = X 1 3 1 4 1
Bb7(4)(9) = X 1 1 1 1 1
Bb7(4)(b9) = 6 X 6 4 4 X
Bb7(9)(13) = X 1 0 1 1 3
Bb7(9-/11+) = X 1 0 1 0 0
Bb7(b13) = X 1 X 1 3 2
Bb7(b9) = 6 X 6 4 3 X
Bb7M(9) = X 1 0 2 1 X
Bbm6/Db = X 4 5 3 6 3
Bm7 = X 2 4 2 3 2
Bº = X 2 3 1 3 1
C#7(b9) = X 4 3 4 3 X
C#7/F = X X 3 4 2 4
C#m6 = X 4 X 3 5 4
C#m7 = X 4 6 4 5 4
C7 = X 3 2 3 1 X
C7(4) = X 3 3 3 1 X
Cm/Eb = X X 1 0 1 3
Cm7 = X 3 5 3 4 3
Cº = X 3 4 2 4 2
D#m6 = X X 1 3 1 2
D#m7 = X X 1 3 2 2
D#m7/C# = 9 6 8 6 7 6
D7 = X X 0 2 1 2
Dm6 = X 5 X 4 6 5
E/G# = 4 X 2 4 5 X
E6(9) = X 7 6 6 7 7
E7(4)(9) = X X 2 2 3 2
E7M = X X 2 4 4 4
Eb6(9) = X X 1 0 1 1
Eb7/Bb = 6 X 5 6 4 X
Ebm7 = X X 1 3 2 2
Ebm7/Bb = 6 4 4 6 4 X
Ebm7M = X 6 8 7 7 6
Ebº = X X 1 2 1 2
F#6 = 2 X 1 3 2 X
F#7 = 2 4 2 3 2 2
F#7(b9) = X X 4 3 5 3
F#7M = 2 X 3 3 2 X
F#m = 2 4 4 2 2 2
F#m/E = X X 2 2 2 2
F#º = 2 X 1 2 1 X
F(#5) = X X 3 2 2 1
F/A = 5 X 3 5 6 X
Fm7 = 1 X 1 1 1 X
G#7(b9) = 4 X 4 2 1 X
G#m7 = 4 X 4 4 4 X
G#m7M/B = X 2 1 1 4 3
G7 = 3 5 3 4 3 3
Gb/Bb = 6 X 4 6 7 X
Gm/Bb = 6 5 5 3 X X
Gm6 = 3 X 2 3 3 X
Gm7 = 3 X 3 3 3 X
