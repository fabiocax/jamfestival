Chico Buarque - Não Existe Pecado Ao Sul do Equador

(De: Chico Buarque & Ruy Guerra)

Intr.: F#° / / / C6/E / Cm6/Eb / Dm7 / G7(13) / C7 / / /
       F#° / / / C6/E / Cm6/Eb / Dm7 / G7 / C6/9 / / /

C6/9                                      Bbm6    Dm7
    Não existe pecado do lado de baixo do e____quador
Bbm6                   Dm7               G7(13)      G#°    C6/9
    Vamos fazer um peca___do rasgado, sua______do, a todo vapor
                      Gm7                    C7/4(9)           C7/9           F7M
Me deixa ser teu escra___cho, capacho, teu ca_______cho, um ria____cho de amor
Fm6                      En7            G#/F#           Dm7
   Quando é lição de esculacho, olha aí,      sai de bai___xo
       G7(13)          C6/9
Que eu sou   professor

                        Dm7           G7(13)           C6/E
Deixa a tristeza pra lá,    vem comer,       me jantar
      Cm6/Eb      Dm7      G7(13)      C7/4(9)
Sarapatel,  caruru,  tucupi,     tacacá
     C7/9    E°          A7(b13)         Dm7
Vê se    me u__sa, me abu_______sa, lambu___za

              Em7         Dm7     G7(13) C6/9
Que a tua Cafu___za não po___de espe_____rar

                        Dm7           G7(13)           C6/E
Deixa a tristeza pra lá,    vem comer,       me jantar
      Cm6/Eb      Dm7      G7(13)      C7/4(9)
Sarapatel,  caruru,  tucupi,     tacacá
     C7/9       E°         A7(b13)        Dm7
Vê se    me esgo__ta, me bo_______ta na me___sa
                 Em7         Dm7     G7(13) C6/9
Que a tua holande___sa não po___de espe_____rar

               C6/9                       Bbm6    Dm7
Não existe peca____do do lado de baixo do e____quador
Bbm6                   Dm7               G7(13)      G#°    C6/9
    Vamos fazer um peca___do rasgado, sua______do, a todo vapor
                      Gm7                    C7/4(9)           C7/9           F7M
Me deixa ser teu escra___cho, capacho, teu ca_______cho, um ria____cho de amor
Fm6                       En7            G#/F#           Dm7
   Quando é missão de esculacho, olha aí,      sai de bai___xo
         G7(13)    C6/9
Eu sou embaixa__dor

F#° / / / C6/E / Cm6/Eb / Dm7 / G7(13) / C7 / / /
F#° / / / C6/E / Cm6/Eb / Dm7 / G7(13) / C6/9

----------------- Acordes -----------------
A7(b13) = X 0 X 0 2 1
Bbm6 = 6 X 5 6 6 X
C6/9 = X 3 2 2 3 3
C6/E = X 7 5 5 8 5
C7 = X 3 2 3 1 X
C7/4(9) = X 3 3 3 3 3
C7/9 = X 3 2 3 3 X
Cm6/Eb = X X 1 2 1 3
Dm7 = X 5 7 5 6 5
Em7 = 0 2 2 0 3 0
E° = X X 2 3 2 3
F#° = 2 X 1 2 1 X
F7M = 1 X 2 2 1 X
Fm6 = 1 X 0 1 1 X
G#/F# = 2 X 1 1 1 X
G#° = 4 X 3 4 3 X
G7 = 3 5 3 4 3 3
G7(13) = 3 X 3 4 5 X
Gm7 = 3 X 3 3 3 X
