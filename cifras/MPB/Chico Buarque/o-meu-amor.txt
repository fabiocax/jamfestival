Chico Buarque - O Meu Amor

[Intro]  Cm7/9  Eb7/9  Dm7/9  Eb7/9


Gm  D/F#  Fm7  Eb6/9  Ab7+                 F/A
O  meu  amor  tem um jeito manso que é só seu
                Ab/Bb  Bb7              Bº
E que me deixa louca quando me beija a boca
                             Ab7+/C
A minha pele toda fica arrepiada
Cm6             Eb/F
E me beija com calma e fundo
         F7/9            Bb7/9   D7
Até minh'alma se sentir beijada, ai
Gm  D/F#  Fm7  Eb6/9  Ab7+                 F/A
O  meu  amor  tem um jeito manso que é só seu
                   Ab/Bb     Bb7               Bº
Que rouba os meus sentidos, viola os meus ouvidos
                                Ab7+/C
Com tantos segredos lindos e indecentes
 Cm6             Eb/F              F7/9
Depois brinca comigo, ri do meu umbigo

               Bb7/9  D7
E me crava os dentes, ai
G           D/F#                   Dm7   G7
Eu sou sua menina, viu? E ele é o meu rapaz
 Cm7    F7        Bb7+            Bbm6  Abm6  Gm
Meu corpo é testemunha do bem que ele  me    faz
   D/F#  Fm7  Eb6/9  Ab7+                 F/A
O meu  amor  tem um jeito manso que é só seu
              Ab/Bb   Bb7             Bº
De me deixar maluca quando me roça a nuca
                                Ab7+/C
E quase me machuca com a barba malfeita
Cm6             Eb/F                  F7/9
E de pousar as coxas entre as minhas coxas
               Bb7/9  D7
Quando ele se deita,  ai
Gm  D/F#  Fm7  Eb6/9  Ab7+                 F/A
O  meu  amor  tem um jeito manso que é só seu
             Ab/Bb    Bb7             Bº
De me fazer rodeios, de me beijar os seios
                               Ab7+/C
Me beijar o ventre e me deixar em brasa
 Cm6             Eb/F                F7/9
Desfruta do meu corpo como se o meu corpo
             Bb7/9  D7
Fosse a sua casa,   ai
G           D/F#                   Dm7   G7
Eu sou sua menina, viu? E ele é o meu rapaz
 Cm7  F7         Bb7+              Bbm6  Abm6  Gm
Meu  corpo é testemunha do bem que ele  me    faz

----------------- Acordes -----------------
Ab/Bb = X 1 1 1 1 X
Ab7+ = 4 X 5 5 4 X
Ab7+/C = X 3 X 1 4 3
Abm6 = 4 X 3 4 4 X
Bb7 = X 1 3 1 3 1
Bb7+ = X 1 3 2 3 1
Bb7/9 = X 1 0 1 1 X
Bbm6 = 6 X 5 6 6 X
Bº = X 2 3 1 3 1
Cm6 = X 3 X 2 4 3
Cm7 = X 3 5 3 4 3
Cm7/9 = X 3 1 3 3 X
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
Dm7 = X 5 7 5 6 5
Dm7/9 = X 5 3 5 5 X
Eb/F = X 8 8 8 8 X
Eb6/9 = X X 1 0 1 1
Eb7/9 = X 6 5 6 6 X
F/A = 5 X 3 5 6 X
F7 = 1 3 1 2 1 1
F7/9 = X X 3 2 4 3
Fm7 = 1 X 1 1 1 X
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
