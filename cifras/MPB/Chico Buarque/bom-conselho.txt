Chico Buarque - Bom Conselho

(De: Chico Buarque)

G7M                    Em7 G7/D                       A7/C#
   Ouça um bom conselho        Que eu lhe dou de graça
Eb             D7(b9)                 Gm7   D7(b9)
  Inútil dormir      que a dor não pas___sa
G7M              Em7 G7/D                A7/C#
   Espere sentado        Ou você se cansa
Eb                     D7(b9)           Gm7   G7(b9/b13)
  Está provado, quem espera  nunca alcan___ça

Cm7(9)                                                                        G/F
      Venha, meu amigo Deixe esse regaço Brinque com meu fogo Venha se queimar
Cm7(9)
      Faça como eu digo Faça como eu faço
Em7(b5)              A7(b13)      Am7(b5)      D7(b9)
       Aja duas vezes       antes de     pensar

G7M                    Em7 G7/D                   A7/C#
   Corro atrás do tempo        Vim de não sei onde
Eb          D7(b9)              Gm7  D7(#9)
  Devagar é que   não se vai lon___ge

G7M                 Em7 G7/D               A7/C#
   Eu semeio o vento        Na minha cidade
Eb                D7(b9)             Gm7 Bb7
  Vou pra rua e be______bo a tempestade
Eb                D7(b9)             Gm7 Bb7
  Vou pra rua e be______bo a tempestade
Eb                  D7(b9)          Gm7(9)   C7(13) Gm7(9)
  Vou pra rua e bebo      a tempesta______de

ACORDES:

G7M        - 3X443X
Em7        - 022030     G/F     - XX3433
G7/D       - X5546X     Em7(b5) - 0X2333
A7/C#      - X4525X     A7(b13) - X0X021
Eb         - X6534X     Am7(b5) - X0554X
D7(b9)     - X5454X     D7(#9)  - X5456X
Gm7        - 3X333X     Bb7     - 6X676X
G7(b9/b13) - 3X3444     Gm7(9)  - 35X365
Cm7(9)     - X3133X     C7(13)  - X3X355

----------------- Acordes -----------------
A7(b13) = X 0 X 0 2 1
A7/C# = X 4 5 2 5 X
Am7(b5) = 5 X 5 5 4 X
Bb7 = X 1 3 1 3 1
C7(13) = X 3 X 3 5 5
Cm7(9) = X 3 1 3 3 X
D7(#9) = X 5 4 5 6 X
D7(b9) = X 5 4 5 4 X
Eb = X 6 5 3 4 3
Em7 = 0 2 2 0 3 0
Em7(b5) = X X 2 3 3 3
G/F = 1 X X 0 0 3
G7(b9/b13) = 3 X 3 4 4 4
G7/D = X X 0 0 0 1
G7M = 3 X 4 4 3 X
Gm7 = 3 X 3 3 3 X
Gm7(9) = X X 5 3 6 5
