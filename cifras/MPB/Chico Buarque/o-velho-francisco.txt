Chico Buarque - O Velho Francisco

(De: Chico Buarque)

Em         B7/F#    Em   D7/F#  G(add9)       D7/F#     G(add9) G7(9)
Já gozei de     boa vida        Tinha  até meu     bangalô
C           Am7  D7/F#        G(add9)      D7/F#     G(add9) B7/F#
Cobertor, comida Roupa lavada Vida   veio e     me levou
Em             B7/F#     Em  D7/F#  G(add9)         D7/F#    G(add9) G7(9)
Fui eu mesmo al_____forriado        Pela   mão do Im_____perador
C            Am7  D7/F#          G(add9)      D7/F#     G(add9)  G6/9  G7(9)
Tive terra, arado Cavalo e brida Vida   veio e     me levou
G(add9)           Bm7/F#  Dm/F      E7        Am7
Hoje   é dia de visita    Vem aí meu  grande amor
Am/G            D7/F#             G(add9)         D7/F#      G(add9) B7/F#
Ela vem toda de brinco Vem todo domingo   Tem chei_____ro de flor

Em            B7/F#      Em   D7/F#  G(add9)            D7/F#         G(add9) G7(9)
Quem me vê, vê     nem bagaço        Do     que viu quem     me enfrentou
C         Am7         D7/F#          G(add9)      D7/F#     G(add9) B7/F#
Campeão do   mundo Em queda-de-braço Vida   veio e     me levou

Li jornal, bu_____la e prefácio        Que    aprendi sem     professor

C            Am7   D7/F#           G(add9)      D7/F#     G(add9)  G6/9  G7(9)
Freqüentei palácio Sem  fazer feio Vida   veio e     me levou
G(add9)           Bm7/F#  Dm/F      E7        Am7
Hoje   é dia de visita    Vem aí meu  grande amor
Am/G            D7/F#             G(add9)         D7/F#      G(add9) B7/F#
Ela vem toda de brinco Vem todo domingo   Tem chei_____ro de flor

Em         B7/F#  Em     D7/F#  G(add9)         D7/F#    G(add9) G7(9)
Eu gerei dezoito  filhas        Me     tornei na_____vegador
C           Am7      D7/F#        G(add9)      D7/F#     G(add9) B7/F#
Vice-rei das   ilhas Da   Caraíba Vida   veio e     me levou
Em         B7/F#       Em    D7/F#  G(add9)       D7/F#    G(add9) G7(9)
Fechei negó_____cio da China        Desbravei o in_____terior
C       Am7    D7/F#         G(add9)      D7/F#     G(add9)  G6/9  G7(9)
Possuí mina De prata, jazida Vida   veio e     me levou
G(add9)           Bm7/F#  Dm/F      E7        Am7
Hoje   é dia de visita    Vem aí meu  grande amor
Am/G            D7/F#                 G(add9)       D7/F#     G(add9) B7/F#
Hoje não deram almoço, né? Acho que o moço   até Nem     me lavou

Em          B7/F#    Em   D7/F#  G(add9)      D7/F#      G(add9) G7(9)
Acho que fui     deputado        Acho   que tu_____do acabou
C           Am7       D7/F#          G(add9)      D7/F#     G(add9)
Quase que Já   não me lembro de nada Vida   veio e     me levou

ACORDES:

Em      - 02200X     Am7    - X0201X
B7/F#   - 2X120X     G6/9   - 3X220X
D7/F#   - 2X021X     Bm7/F# - 2X423X
G(add9) - 3X020X     Dm/F   - 1X023X
G7(9)   - 3X320X     E7     - 02213X
C       - X3201X     Am/G   - 3X221X

----------------- Acordes -----------------
Am/G = 3 X 2 2 1 X
Am7 = X 0 2 0 1 0
B7/F# = X X 4 4 4 5
Bm7/F# = X X 4 4 3 5
C = X 3 2 0 1 0
D7/F# = X X 4 5 3 5
Dm/F = X X 3 2 3 1
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
G(add9) = 3 X 0 2 0 X
G6/9 = X X 5 4 5 5
G7(9) = 3 X 3 2 0 X
