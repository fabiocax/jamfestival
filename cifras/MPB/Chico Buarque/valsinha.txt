Chico Buarque - Valsinha

    Aº                 G#7              C#m              C#m/E
Um dia ele chegou tão diferente do seu jeito de sempre chegar
   Aº               G#7                  C#m                C#m/E
Olhou-a dum jeito muito mais quente do que sempre costumava olhar
   C#7                 C#7(9-)              F#m             F#m/A
E não maldisse a vida tanto quanto era seu jeito de sempre falar
   D#7                 D#7/A                   G#7               F#m  G#7
E nem deixou-a só num canto, pra seu grande espanto convidou-a pra rodar
  Aº                G#7                 C#m                C#m/E
E então ela se fez bonita com há muito tempo não queria ousar
     Aº            G#7                 C#m             C#m/E
Com seu vestido decotado cheirando a guardado de tanto esperar
   C#7                       C#7(9-)            F#m                F#m/A
Depois os dois deram-se os braços com há muito tempo não se usava dar
    D#7                 D#7/A              G#7                F#m    G#7
E cheios de ternura e graça foram para a praça e começaram a se abraçar
  Aº                  G#7              C#m              C#m/E
E ali dançaram tanta dança que a vizinhança toda despertou
   Aº            G#7             C#m          C#m/E
E foi tanta felicidade que toda cidade se iluminou
   C#7                 C#7(9-)               F#m                F#m
E foram tantos beijos loucos, tantos gritos roucos, como não se ouvia mais

       C#m                    G#7              C#m
Que o mundo compreendeu, e o dia amanheceu em paz

----------------- Acordes -----------------
Aº = 5 X 4 5 4 X
C#7 = X 4 3 4 2 X
C#7(9-) = X 4 3 4 3 X
C#m = X 4 6 6 5 4
C#m/E = X 7 6 6 9 X
D#7 = X 6 5 6 4 X
F#m = 2 4 4 2 2 2
F#m/A = X 0 4 2 2 2
G#7 = 4 6 4 5 4 4
