Chico Buarque - Choro Bandido

(Intérpretes: Tom Jobim e Edu Lobo)
(De: Chico Buarque e Edu Lobo)

Intr.: Dm7  Fm(7M)/Ab Fm7/Ab Fm6/Ab
       Fm(7M) Fm7 Bb7/4(9) A7(b9)

Dm(7M 9)         Dm7/9       G7/4(9) G7(b5) G7(9 13)
Mesmo  que os cantores sejam falsos  como   eu
  G7(#9 b13)  C7M/9        E7(b9)
Serão       bonitas, não importa
      Am7/11      D7(9 #11)
São bonitas as canções
Dm(7M 9)    Dm7/9       G7/4(9)
Mesmo   miseráveis os poetas
        G7/9         C7M(#5)  C7M
Os seus versos serão bons

F#m7/9                      B7(b9 13)
Mesmo  porque as notas eram surdas
                         E°(add9)  E7M/9
Quando um deus sonso e ladrão

A#m7(b5)                      D#7
Fez     das tripas a primeira lira
       A7/9           G#7M G7(#5) F#7(#11) F7(#11)
Que animou  todos os sons

E7M/9                  C#m(7M 9)
E    daí nasceram as baladas
       C#m7/9       A7M                     G#7/4(9)
E os arroubos de bandidos como eu, cantando assim:
G#7/9 C#m7(b5)                F#7/4(9)
              Você nasceu pra mim
F#7 Bm7(b5)                E7/4(9)  Eb7/4(9)
           Você nasceu pra mim

Dm(7M 9)      Dm7/9           G7/4(9)
Mesmo   que você   feche os ouvidos
G7(b5)     G7(9 13) G7(#9 b13)   C7M/9
E     as janelas    do        vestido
      E7(b9)      Am7/11        D7(9 #11)
Minha musa  vai cair    em tentação
Dm(7M 9)         Dm7/9        G7/4(9)
Mesmo   porque estou  falando grego
        G7/9   C7M(#5)  C7M
Com sua imaginação

F#m7/9                  B7(b9 13)
Mesmo  que você fuja de mim
                     E°(add9)  E7M/9
Por labirintos e alçapões
A#m7(b5)                   D#7
Saiba   que os poetas como cegos
      A7/9         G#7M G7(#5) F#7(#11) F7(#11)
Podem ver na escuridão

  E7M/9                         C#m(7M 9)
E eis  que, menos sábios do que antes
        C#m7/9    A7M                       G#7/4(9)
Os seus lábios ofegantes Hão de se entregar assim:
G#7/9 C#m7(b5)              F#7/4(9)
              Me leve até o fim
F#7 Bm7(b5)              E7/4(9)  Eb7/4(9)
           Me leve até o fim

Dm(7M 9)        Dm7/9        G7/4(9) G7(b5) G7(9 13)
Mesmo  que os romances sejam falsos  como o nosso
G7(#9 b13)  C7M/9        E7(b9)
São       bonitas, não importa
      Am7/11       D7(9 #11) Eb°(b13)
São bonitas  as canções
G7/4(9)
Mesmo  sendo errados os amantes
                  F7M  C/E Dm7/9 Db7M C6/G
Seus amores serão bons

....................................
Qualquer dúvida, me mande um e-mail:

....................................

----------------- Acordes -----------------
A#m7(b5) = X 1 2 1 2 X
A7(b9) = 5 X 5 3 2 X
A7/9 = 5 X 5 4 2 X
A7M = X 0 2 1 2 0
Am7/11 = 5 X 5 5 3 X
Bb7/4(9) = X 1 1 1 1 1
Bm7(b5) = X 2 3 2 3 X
C#m7(b5) = X 4 5 4 5 X
C#m7/9 = X 4 2 4 4 X
C/E = 0 3 2 0 1 0
C6/G = 3 X 2 2 1 X
C7M = X 3 2 0 0 X
C7M(#5) = X 3 2 1 0 0
C7M/9 = X 3 2 4 3 X
D#7 = X 6 5 6 4 X
Db7M = X 4 6 5 6 4
Dm7 = X 5 7 5 6 5
Dm7/9 = X 5 3 5 5 X
E7(b9) = X X 2 1 3 1
E7/4(9) = X X 2 2 3 2
E7M/9 = X 7 6 8 7 X
Eb7/4(9) = X X 1 1 2 1
F#7 = 2 4 2 3 2 2
F#7(#11) = 2 X 2 3 1 X
F#7/4(9) = 2 X 2 1 0 X
F#m7/9 = X X 4 2 5 4
F7(#11) = 1 X 1 2 0 X
F7M = 1 X 2 2 1 X
Fm(7M) = X X 3 5 5 4
Fm6/Ab = 4 5 3 5 3 X
Fm7 = 1 X 1 1 1 X
Fm7/Ab = 4 X 3 5 4 X
G#7/4(9) = 4 X 4 3 2 X
G#7/9 = 4 X 4 3 1 X
G#7M = 4 X 5 5 4 X
G7(#5) = 3 X 3 4 4 3
G7(b5) = 3 X 3 4 2 X
G7/4(9) = 3 X 3 2 1 X
G7/9 = 3 X 3 2 0 X
