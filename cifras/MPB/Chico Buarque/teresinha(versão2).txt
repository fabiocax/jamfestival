Chico Buarque - Teresinha

[Intro:] Bm Bm/A G6 F#7 Bm

                 B/A        Aº            G6
O primeiro me chegou como quem vem do florista
         C7M/G     C#m7(b5)          F#7(b13)       Bm
Trouxe um bicho de pelúcia, trouxe um broche de ametista
      G7M      E7/G#           Bm/A          F#7/Bb
Me contou suas viagens e as vantagens que ele tinha
        D         B7/D#        C#7/F      A6  D/C
Me mostrou o seu relógio, me chamava de rainha
                      G/B       Gm/Bb        B7/A
Me encontrou tão desarmada que tocou meu coração
         B7     B/A E7/G#          G6    F#7(b13) Bm
Mas não me nega...va nada e, assustada, eu disse não

O segundo me chegou como quem chega do bar
Trouxe um litro de aguardente tão amarga de tragar
Indagou o meu passado e cheirou minha comida
Vasculhou minha gaveta, me chamava de perdida
Me encontrou tão desarmada que arranhou meu coração
Mas não me entregava nada e, assustada, eu disse não


O terceiro me chegou como quem chega do nada
Ele não me trouxe nada também nada perguntou
Mal sei como ele se chama, mas entendo o que ele quer
Se deitou na minha cama e me chama de mulher
Foi chegando sorrateiro e antes que eu dissesse não
Se instalou feito um posseiro dentro do meu coracão

----------------- Acordes -----------------
A6 = 5 X 4 6 5 X
Aº = 5 X 4 5 4 X
B/A = X 0 4 4 4 X
B7 = X 2 1 2 0 2
B7/A = X 0 4 4 4 X
B7/D# = X X 1 2 0 2
Bm = X 2 4 4 3 2
Bm/A = X 0 4 4 3 X
C#7/F = X X 3 4 2 4
C#m7(b5) = X 4 5 4 5 X
C7M/G = 3 X 2 4 1 X
D = X X 0 2 3 2
D/C = X 3 X 2 3 2
E7/G# = 4 X 2 4 3 X
F#7 = 2 4 2 3 2 2
F#7(b13) = 2 X 2 3 3 2
F#7/Bb = 6 X 4 6 5 X
G/B = X 2 0 0 3 3
G6 = 3 X 2 4 3 X
G7M = 3 X 4 4 3 X
Gm/Bb = 6 5 5 3 X X
