Chico Buarque - Sinhá

Intro: Cm  Fm6  G7/B  Cm  Fm6  G7/B  Ab5  G(#5)  Fm6  G7/B
       Cm  Am7(b5)  Db7M  G7
       Cm  Eb7M/Bb  D7/A  Fm6/Ab  G7

Cm                      Fm6        Cm
 Se a dona se banhou eu não estava lá
    D7/A           Gm     D7/A         G7/B
Por Deus, Nosso Senhor, eu não olhei Sinhá
             Cm      Fm6              Cm
Estava lá na roça    sou de olhar ninguém
    D7/A      Eb7M/Bb   D7/A        G7/B
Não tenho mais cobiça    nem enxergo bem

                  Fm6/Ab           G7     Cm
Para quê me pôr no tronco para que me aleijar
            Gm7(b5)         C7    Fm6
Eu juro a vosmecê que nunca vi Sinhá
    Fm7                          Bb7  Eb6
Por que me faz tão mal com olhos tão azuis
   Cm         Db6(9)    G7/B   Cm
Me benzo com o sinal da santa cruz


( Eb7M/Bb  D7/A  Fm6/Ab  G7  Cm  Eb7M/Bb  D7/A  Fm6/Ab  G7 )

Cm                       Fm6        Cm
 Eu só cheguei no açude atrás da sabiá
 D7/A        Gm      D7/A         G7/B
Olhava o arvoredo, eu não olhei Sinhá
                Cm     Fm6          Cm
Se a dona se despiu, eu já andava além
 D7/A    Eb7M/Bb   D7/A         G7/B
Estava na moenda, estava para Xerém

                   Fm6/Ab          G7     Cm
Por que talhar meu corpo, eu não olhei Sinhá
               Gm7(b5)            C7     Fm6
Para que que vosmincê, meus olhos vai furar
    Fm7                     Bb7  Eb6
Eu choro em Iorubá, mas oro por Jesus
     Cm         Db6(9)   G7/B   C9
Para que que vassuncê me tira a luz

                   Em/B   Am7         Dm(11)
E assim vai se encerrar o conto de um cantor
               G7/B      F#º      D7/A
Com voz do pelourinho e ares de senhor
              C9     B7(13)    E7(9)
Cantor atormentado herdeiro sarará
   A7          Dm7          Ebº           Em7(b5)
Do nome e do renome de um feroz senhor de engenho
         Fm6            Cm            Fm6       Fm6/Ab
E das mandingas de um escravo que no engenho enfeitiçou
   G7   Cm
   Si...nhá

( Eb7M/Bb  D7/A  Fm6/Ab  G7  Cm  Eb7M/Bb  D7/A  Fm6/Ab  G7 )

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Ab5 = 4 6 6 X X X
Am7 = X 0 2 0 1 0
Am7(b5) = 5 X 5 5 4 X
B7(13) = X 2 X 2 4 4
Bb7 = X 1 3 1 3 1
C7 = X 3 2 3 1 X
C9 = X 3 5 5 3 3
Cm = X 3 5 5 4 3
D7/A = 5 X 4 5 3 X
Db6(9) = X 4 3 3 4 4
Db7M = X 4 6 5 6 4
Dm(11) = X 5 5 7 6 5
Dm7 = X 5 7 5 6 5
E7(9) = X X 2 1 3 2
Eb6 = X X 1 3 1 3
Eb7M/Bb = 6 6 5 3 3 3
Ebº = X X 1 2 1 2
Em/B = X 2 2 0 0 0
Em7(b5) = X X 2 3 3 3
F#º = 2 X 1 2 1 X
Fm6 = 1 X 0 1 1 X
Fm6/Ab = 4 5 3 5 3 X
Fm7 = 1 X 1 1 1 X
G(#5) = 3 X 1 0 0 X
G7 = 3 5 3 4 3 3
G7/B = X 2 3 0 3 X
Gm = 3 5 5 3 3 3
Gm7(b5) = 3 X 3 3 2 X
