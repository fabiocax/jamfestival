Chico Buarque - Angélica

[ Miltinho / Chico Buarque ]


[Intro:] G7(b9) C Em7/B C4/Bb F7M/A G7(b9) C

              Em7/B C4/Bb
Quem é essa mulher
         F7M/A   Fm6/Ab    C4/G  C9/G
Que canta sempre esse estribilho?
           D7(9)/F#    Bb(4+)/F F7M(6) Fm6(7M)
Só queria embalar meu filho
                 E7 E/D Am7(9) G7(9)(11) C
Que mora na escuridão do mar

Quem é essa mulher
Que canta sempre esse lamento?
Só queria lembrar o tormento
Que fez meu filho suspirar

Quem é essa mulher
Que canta sempre o mesmo arranjo?

Só queria agasalhar meu anjo
E deixar seu corpo descansar

Quem é essa mulher
Que canta como dobra um sino?
Queria cantar por meu menino
                E7  E/D   C  Bb7(9) Eb
Que ele já não pode mais cantar

              Gm7/D Eb4/Db
Quem é essa mulher
       Ab7M(9)/C    B°    Eb4/Bb
Que canta sempre esse estribilho?
Eb9/Bb  A7(b5)     Db(4+)/Ab Ab7M(6) Abm6(7M)
Só queria embalar meu filho
               G7 G/F Cm7   Cm7/Bb Ab7M Eb7M/G Fm7(9) Cm7M(9)(11)
Que mora na escuridão do mar


Db(4+)/Ab = 4x668x
Bb(4+)/F = 1x335x

----------------- Acordes -----------------
A7(b5) = X 0 1 0 2 X
Ab7M = 4 X 5 5 4 X
Ab7M(6) = 4 X 5 5 6 X
Abm6(7M) = 4 6 5 4 6 4
Am7(9) = X X 7 5 8 7
Bb(4+)/F = 1 X 3 3 5 X
Bb7(9) = X 1 0 1 1 X
B° = X 2 3 1 3 1
C = X 3 2 0 1 0
C4/Bb = X 1 2 0 1 1
C4/G = 3 3 5 5 6 3
C9/G = 3 X 5 5 3 3
Cm7 = X 3 5 3 4 3
Cm7/Bb = 6 3 5 3 4 3
Cm7M(9)(11) = X 3 1 4 3 1
D7(9)/F# = 5 7 5 9 5 6
Db(4+)/Ab = 4 X 6 6 8 X
E/D = X 5 X 4 5 4
E7 = 0 2 2 1 3 0
Eb = X 6 5 3 4 3
Eb4/Bb = X 1 1 3 4 4
Eb4/Db = X 4 1 1 2 X
Eb7M/G = 3 X 1 3 3 X
Eb9/Bb = X 1 X 3 4 1
Em7/B = X 2 2 0 3 0
F7M(6) = 1 X 2 2 3 X
F7M/A = 5 X 3 5 5 X
Fm6(7M) = 1 3 2 1 3 1
Fm6/Ab = 4 5 3 5 3 X
Fm7(9) = X X 3 1 4 3
G/F = 1 X X 0 0 3
G7 = 3 5 3 4 3 3
G7(9)(11) = 3 X 3 2 1 X
G7(b9) = 3 X 3 1 0 X
Gm7/D = X 5 3 3 3 3
