Chico Buarque - Teresinha

 Bm            B/A          E7/Ab         E/G#
O primeiro me chegou como quem vem do florista
        Em/G         F#4/7            F#7(b9)       Bm
Trouxe um bicho de pelúcia, trouxe um broche de ametista
    G7+          G#º          D/A             F#7/Bb
Me contou suas viagens e as vantagens que ele tinha
      D            Ebº         C#7        F#
Me mostrou o seu relógio, me chamava de rainha
      D/C           G/B          Gm         B7
Me encontrou tão desarmada que tocou meu coração
      B/A         E/G#         G6       F#7   Bm
Mas não me negava nada, e assustada, eu disse não

Bm            B/A          E7/Ab       E/G#
O segundo me chegou como quem chega do bar
         Em/G         F#4/7         F#7(b9)    Bm
Trouxe um litro de aguardente tão amarga de tragar
    G7+         G#º         D/A        F#7/Bb
Indagou o meu passado e cheirou minha comida
      D           Ebº        C#7       F#
Vasculhou minha gaveta me chamava de perdida

      D/C           G/B          Gm         B7
Me encontrou tão desarmada que arranhou meu coração
      B/A         E/G#            G6       F#7   Bm
Mas não me entregava nada, e assustada, eu disse não

 Bm            B/A        E7/Ab         E/G#
O terceiro me chegou como quem chega do nada
    Em/G          F#4/7       F#7(b9)     Bm
Ele não me trouxe nada também nada perguntou
    G7+             G#º         D/A             F#7/Bb
Mal sei como ele se chama mas entendo o que ele quer
      D            Ebº         C#7       F#
Se deitou na minha cama e me chama de mulher
      D/C           G/B            Gm          B7
Foi chegando sorrateiro e antes que eu dissesse não
      B/A             E/G#          G6     F#7   Bm
Se instalou feito um posseiro dentro do meu coração

----------------- Acordes -----------------
B/A = X 0 4 4 4 X
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C#7 = X 4 3 4 2 X
D = X X 0 2 3 2
D/A = X 0 X 2 3 2
D/C = X 3 X 2 3 2
E/G# = 4 X 2 4 5 X
E7/Ab = 4 X 2 4 3 X
Ebº = X X 1 2 1 2
Em/G = 3 X 2 4 5 X
F#4/7 = 2 4 2 4 2 X
F#7 = 2 4 2 3 2 2
F#7(b9) = X X 4 3 5 3
F#7/Bb = 6 X 4 6 5 X
F#m = 2 4 4 2 2 2
G#º = 4 X 3 4 3 X
G/B = X 2 0 0 3 3
G6 = 3 X 2 4 3 X
G7+ = 3 X 4 4 3 X
Gm = 3 5 5 3 3 3
