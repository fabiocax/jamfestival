Chico Buarque - Sem Fantasia

Em7              F#m5-/7 B7    Bm5-/7                E9-
Vem, meu menino vadio,       vem, sem mentir prá você
Am7  D7(9)                G7M  G7                 C7M
Vem,    mas vem sem fantasia,   que da noite pro dia
C/Bb            F#m5-/7    B7
Você não vai crescer
Em7               F#m5-/7  B7                   Bm5-/7   E7
Vem, por favor não evites    meu amor, meus convites
                  Am7    A#°                 Em/B     C7M
Minha dor, meus apelos, vou te envolver nos cabelos,
                     Bm5-/7   E7             Am7
Vem perde-te em meus braços pelo amor de Deus
                     A#°                       Em/B     C7M
Vem que eu te quero fraco, vem que eu te quero tolo
                     F#  B7  Em  B7
Vem que eu te quero to...do meu
Em             F#m5-/7  B7                      Bm5-/7               E9-
Ah, eu quero te dizer      que o instante de te ver custou tanto penar
                  Am7     D7(9)         G7M         G7           C7M
Não vou me arrepender, só vim te convencer que eu vim prá não morrer
   Am7          F#5-/7    B7          Em                    F#m5-/7
De tanto te esperar, eu quero te contar das chuvas que apanhei

     B7          Bm5-/7    E9-          Am7
Das noites que varei no escuro a te buscar
                  A#°                  Em/B     C7M            E9-
Eu quero te mostrar as marcas que ganhei nas lutas contra o rei
                     Am7                    A#°                  Em/B
Nas discussões com Deus, e agora que cheguei eu quero a recompensa
C7M                 F#7           B7     Em7
Eu quero a prenda imensa dos carinhos teus

                   C#°                  Em/B
E agora que cheguei eu quero a recompensa
C7M                 F#7           B7     Em7
Eu quero a prenda imensa dos carinhos teus

----------------- Acordes -----------------
A#° = X 1 2 0 2 0
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
Bm5-/7 = X 2 3 2 3 X
C#° = X 4 5 3 5 3
C/Bb = X 1 2 0 1 X
C7M = X 3 2 0 0 X
D7(9) = X 5 4 5 5 X
E7 = 0 2 2 1 3 0
E9- = 0 2 3 1 0 0
Em = 0 2 2 0 0 0
Em/B = X 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F# = 2 4 4 3 2 2
F#5-/7 = 2 X 2 3 1 X
F#7 = 2 4 2 3 2 2
F#m5-/7 = 2 X 2 2 1 X
G7 = 3 5 3 4 3 3
G7M = 3 X 4 4 3 X
