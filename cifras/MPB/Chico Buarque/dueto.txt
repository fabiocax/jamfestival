Chico Buarque - Dueto

[Intro] Ab6/C  G7/B  Ab6/C  G7/B  Ab7M
       G7(b13)  C7/4(9)  C7/4(9) C7(b9)

      F6/A       E7/G#      F6/A       E7/G#
(ELA) Consta nos astros Nos signos Nos búzios
   F6      F#º       Gm6     G#º
Eu li num anúncio Eu vi no espelho
   Am7(b5)       Ebm6/Gb      Em7(b5)      A7(13) A7(b13)
Tá lá     no evange_____lho Garantem os orixás
  Dm7       G7     C7/4(9)        C7/4(9) C7(b9)
Serás o meu amor Serás    a minha paz

      F6/A       E7/G#     F6/A      E7/G#
(ELE) Consta nos autos Nas bulas Nos dogmas
   F6      F#º     Gm6       G#º
Eu fiz uma tese Eu li num tratado
  Am7(b5)     Ebm6/Gb       Em7(b5)         A7(13) A7(b13)
Está     computa_____do Nos da_____dos oficiais
  Dm7        G7    Cm7         F7
Serás o meu amor Serás a minha paz


      Bb7M       Bb6       Bm7(b5)     E7(b9)
(ELA) Mas se a ciência provar    o contrário (ELE)
   Am       Am7M      Am7    Am6
E se o calendário nos contrariar
          C7M/G                    G7(b13)  G7           C7/4(9)
(OS DOIS) Mas   se o destino insistir        Em nos separar
F6/A              E7/G#                          F6/A
Danem-se (ELA) Os astros (ELE) Os autos (ELA) Os signos (ELE) Os dogmas
         E7/G#                        F6                             F#º
(ELA) Os búzios (ELE) As bulas (ELA) Anúncios (ELE) Tratados (ELA) Ciganas
                        Gm6                          G#º
(ELE) Projetos (ELA) Profetas (ELE) Sinopses (ELA) Espelhos (ELE) Conselhos
             Am7(b5)         Ebm6/Gb      Em7(b5)          A7(13) A7(b13)
(OS DOIS) Se da_____ne o evange_____lho E to_____dos os orixás
  Dm7        G7    C7/4(9)        C7/4(9)  C7(b9)  Ab6/C
Serás o meu amor Serás,   amor, a mi_______nha     paz

( G7/B  Ab6/C  G7/B  Ab7M  G7(b13)  C7/4(9) )

      F6/A      E7/G#          F6/A           E7/G#         F6        F#º
(ELE) Consta na pauta (ELA) No Karma (ELE) Na carne (ELA) Passou na novela
        Gm6     G#º          Am7(b5)       Ebm6/Gb
(ELE) Está no seguro (ELA) Pixa_____ram no mu_____ro
         Em7(b5)            A7(13) A7(b13)
(ELE) Mandei    fazer um cartaz
            Dm7        G7    Cm7         F7
(OS DOIS) Serás o meu amor Serás a minha paz

      Bb7M                Bm7(b5)     E7(b9)         Am        Am(7M)    Am7     Am6
(ELE) Mas se a ciência provar    o contrário (ELA) E se o calendário nos contrariar
          G7(13)                  G7(b13)  G7           C7/4(9)
(OS DOIS) Mas   se o destino insistir        Em nos separar
F6/A              E7/G#                          F6/A
Danem-se (ELE) Os astros (ELA) Os autos (ELE) Os signos (ELA) Os dogmas
         E7/G#                        F6                             F#º
(ELE) Os búzios (ELA) As bulas (ELE) Anúncios (ELA) Tratados (ELE) Ciganas
                        Gm6                          G#º
(ELA) Projetos (ELE) Profetas (ELA) Sinopses (ELE) Espelhos (ELA) Conselhos
             Am7(b5)         Ebm6/Gb      Em7(b5)          A7(13) A7(b13)
(OS DOIS) Se da_____ne o evange_____lho E to_____dos os orixás
  Dm7        G7    C7/4(9)        C7/4(9)  C7(b9)  Ab6/C
Serás o meu amor Serás,   amor, a mi_______nha     paz

( G7/B  Ab6/C  G7/B  Ab7M  G7(b13)  C7/4(9) )

      Ab6/C      G7/B            Ab6/C            G7/B   Ab7M  G7(b13)  C7/4(9)
(ELE) Consta nos mapas (ELA) Nos lábios (ELE) Nos lápis
      Ab6/C      G7/B           Ab6/C           G7/B   Ab7M  G7(b13)  C7/4(9)
(ELA) Consta nos Ovnis (ELE) No Pravda (ELA) Na vodca

[Final] Ab6/C  G7/B  Ab6/C  G7/B  Ab7M  G7(b13)  C7/4(9)

----------------- Acordes -----------------
A7(13) = X 0 X 0 2 2
A7(b13) = X 0 X 0 2 1
Ab6/C = X 3 1 1 4 1
Ab7M = 4 X 5 5 4 X
Am = X 0 2 2 1 0
Am(7M) = X 0 2 1 1 0
Am6 = 5 X 4 5 5 X
Am7 = X 0 2 0 1 0
Am7(b5) = 5 X 5 5 4 X
Am7M = X 0 2 1 1 0
Bb6 = X 1 3 0 3 X
Bb7M = X 1 3 2 3 1
Bm7(b5) = X 2 3 2 3 X
C7(b9) = X 3 2 3 2 X
C7/4(9) = X 3 3 3 3 3
C7M/G = 3 X 2 4 1 X
Cm7 = X 3 5 3 4 3
Dm7 = X 5 7 5 6 5
E7(b9) = X X 2 1 3 1
E7/G# = 4 X 2 4 3 X
Ebm6/Gb = 2 3 1 3 1 X
Em7(b5) = X X 2 3 3 3
F#º = 2 X 1 2 1 X
F6 = 1 X 0 2 1 X
F6/A = 5 3 3 5 3 X
F7 = 1 3 1 2 1 1
G#º = 4 X 3 4 3 X
G7 = 3 5 3 4 3 3
G7(13) = 3 X 3 4 5 X
G7(b13) = 3 X 3 4 4 3
G7/B = X 2 3 0 3 X
Gm6 = 3 X 2 3 3 X
