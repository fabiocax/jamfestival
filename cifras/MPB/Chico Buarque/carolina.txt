Chico Buarque - Carolina

(De: Chico Buarque)

B7(#5)      E7M(9)                     D#m7(b5)
      Caroli______na Nos seus olhos fun________dos
G#7                C#m7      G#m7     C#7(b9)        F#m7  B7(9)
   Guarda tanta dor     A dor    de to_______do esse mundo
     F#m7                  B7(9)            E7M(9)
Eu já    lhe expliquei que não  vai dar Seu pranto não vai nada ajudar
   C#m7(9)           F#7         F#m7              B7(b9)
Eu já     convidei para dançar É hora, já sei, de aproveitar

     E7M(9)                     D#m7(b5) G#7(b13)
Lá fo______ra, amor Uma rosa nasceu
              C#m7               Bm7  C#7(b9)  F#m7
Todo mundo sambou  Uma estrela caiu
      Am6               E7M(9)      C#m7                   F#7(13)   F#7(b13)
Eu bem   que mostrei sor______rindo Pela janela, ói que lin_______do
F#7                 F#m7  B7(#5)
Mas Carolina não viu

      E7M(9)                      D#m7(b5)
Caroli______na Nos seus olhos tris________tes

G#7                 C#m7       G#m7      C#7(b9)     F#m7  B7(9)
   Guarda tanto amor     O amor    que já       não existe
      F#m7               B7(9)     E7M(9)
Eu bem    que avisei vai acabar De tudo  lhe dei para aceitar
    C#m7(9)           F#7          F#m7           B7(b9)
Mil versos cantei pra lhe agradar Agora não sei como    explicar

     E7M(9)                    D#m7(b5) G#7(b13)
Lá fo______ra, amor Uma rosa morreu
             C#m7               G#m7(b5)  C#7(b9)  F#m7
Uma festa acabou  Nosso barco partiu
      Am6               E7M(9)     C#m7                F#m7
Eu bem   que mostrei a e______la O tempo passou na jane____la
         B7(b9)          G#7(13) G#7 C#7(b9)  F#m7
Só Caroli______na não viu
      Am6               E7M(9)     C#m7                F#m7
Eu bem   que mostrei a e______la O tempo passou na jane____la
         B7(b9)          Em6
Só Caroli______na não viu

ACORDES:

B7(#5)   - X2524X
E7M(9)   - 024140     F#7      - 2X232X
D#m7(b5) - X6767X     B7(b9)   - X2121X
G#7      - 4X454X     G#7(b13) - 4X455X
C#m7     - X4645X     Bm7      - X2423X
G#m7     - 4X444X     Am6      - 5X455X
C#7(b9)  - X4343X     F#7(13)  - 2X234X
F#m7     - 2X222X     F#7(b13) - 2X233X
B7(9)    - X2122X     G#m7(b5) - 4X443X
C#m7(9)  - X4244X     Em6      - 0X542X

----------------- Acordes -----------------
Am6 = 5 X 4 5 5 X
B7(#5) = X 2 X 2 4 3
B7(9) = X 2 1 2 2 X
B7(b9) = X 2 1 2 1 X
Bm7 = X 2 4 2 3 2
C#7(b9) = X 4 3 4 3 X
C#m7 = X 4 6 4 5 4
C#m7(9) = X 4 2 4 4 X
D#m7(b5) = X X 1 2 2 2
E7M(9) = X 7 6 8 7 X
Em6 = X 7 X 6 8 7
F#7 = 2 4 2 3 2 2
F#7(13) = 2 X 2 3 4 X
F#7(b13) = 2 X 2 3 3 2
F#m7 = 2 X 2 2 2 X
G#7 = 4 6 4 5 4 4
G#7(13) = 4 X 4 5 6 X
G#7(b13) = 4 X 4 5 5 4
G#m7 = 4 X 4 4 4 X
G#m7(b5) = 4 X 4 4 3 X
