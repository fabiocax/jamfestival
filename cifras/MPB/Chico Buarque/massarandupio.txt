Chico Buarque - Massarandupió

    E7M                G#m/B
No mundaréu de areia à beira-mar
         F#7(9)  Am6
De Massarandupió
    E7M              G#m/B
Em volta da massaranduba-mor
          A6
De Massarandupió
   A7M
Aquele pia
  Am6
Aquele neguinho
Aquele psiu
    E7M      Fdim
Um bacuri ali sozinho
   F#7(9)
Caminha
C#m/F#              B7(9)
Ali onde ninguém espia
B7(9b)               F#7(9)
Ali onde a perna bambeia

C#m/F#              B7(9)  F#m/B
Ali onde não há caminho

     E7M              G#m/B
Lembrar a meninice é como ir
           F#7(9)      Am6
Cavucando de sol a sol
    E7M                 G#m/B
Atrás do anel de pedra cor de areia
         A6
Em Massarandupió
   A7M
Cavuca daqui
   Am6
Cavuca de lá
Cavuca com fé

     E7M
Oh, São Longuinho
     Fdim
Oh, São Longuinho
     F#7(9)
Quem sabe
    C#m/F#                  B7(9)
De noite o vento varre a praia
   B7(9b)             F#7(9)
Arrasta a saia pela areia
C#m/F#            B7(9) F#m/B
E sobe num redemoinho

       E7M(9) D7M(9)
É o xuá
     E7M(9)          F#7(13b)
Das ondas a se repetir
      A7M                  C#7(9b)
Como é que eu vou saber dormir
          F#7M G#m7/F#
Longe do mar
   C#7M             B7M
Ó mãe, pergunte ao pai
            C#7M              Adim
Quando ele vai soltar a minha mão
     Bbm7
Onde é que o chão acaba
    Db/Ab             Bbm6      F#m6
E principia toda a arrebentação
   E7M               E7(9)
Devia o tempo de criança ir se
              A7M      Am6
Arrastando até escoar, pó a pó

    E7M                 E7(9)
Num relógio de areia o areal de
       A7M   Am6    E7M(9)
Massarandupió

----------------- Acordes -----------------
A6 = 5 X 4 6 5 X
A7M = X 0 2 1 2 0
Adim = 5 X 4 5 4 X
Am6 = 5 X 4 5 5 X
B7(9) = X 2 1 2 2 X
B7(9b) = X 2 1 2 1 X
B7M = X 2 4 3 4 2
Bbm6 = 6 X 5 6 6 X
Bbm7 = X 1 3 1 2 1
C#7(9b) = X 4 3 4 3 X
C#7M = X 4 6 5 6 4
C#m/F# = X X 4 6 5 4
D7M(9) = X 5 4 6 5 X
Db/Ab = 4 X 3 1 2 X
E7(9) = X X 2 1 3 2
E7M = X X 2 4 4 4
E7M(9) = X 7 6 8 7 X
F#7(13b) = 2 X 2 3 3 2
F#7(9) = X X 4 3 5 4
F#7M = 2 X 3 3 2 X
F#m/B = X 2 4 2 2 2
F#m6 = 2 X 1 2 2 X
Fdim = X X 3 4 3 4
G#m/B = X 2 X 1 4 4
G#m7/F# = X X 4 4 7 4
