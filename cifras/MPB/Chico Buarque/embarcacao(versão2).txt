Chico Buarque - Embarcação

Francis Hime / Chico Buarque

[Intro:] Cm7 F7(9) Bb7M Eb7M Am7(b5) D7(b9) Dm7(b5) G7(b9)
            Cm7 C#º Gm/D Eb7M Am7(b5) D7(b9) Gm7 Ab7(9)

Gm7                          A/G                            Cm6      D7/F#
Sim, foi que nem um temporal, foi um vaso de cristal
                 Gm7                 G7/B
Que partiu dentro de mim
                         Cm7(11)            F7(9)                Bb7M       Eb7M
Ou quem sabe os ventos pondo fogo numa embarcação
                   Em7(b5)              A7(b9)        Cm6/Eb F7(9)
Os quatro elementos num momento de paixão

Bb7M                         F/A                                        Fm/Ab
Deus, eu pensei que fosse Deus e que os mares fossem meus
               Abm6      C7(9) F#7M                       Bb/F    Gm7
Como pensam os ingleses, mel, eu pensei que fosse mel
          C7(4)       C7(13)            F7(4)(9)      D7(4) D7
E bebi da vida como bebe um marinheiro de partida


G6(9)                       D/F#                          Dm9/F
Meu, eu pensei que fosse meu o calor do corpo teu
                  E7                          A7(9)
Que indendeia meu corpo há meses
Eb7M                    G/D           Em7(9)
Ah, como eu precisava amar
                          A7(9)            D7 G7(4)(9)            G7(9)
E antes mesmo do galo cantar, eu te neguei três vezes
Cm7                       G/D         Em7(9)
Cais, ficou tão pequeno cais
                 A7(9)        D7(13)  G6(9) D#7(b9)
Te perdi de vista para nunca mais

G#m7                               Bb/Ab                                  C#m6      Eb7/G
Mais, mais que a vida em minhas mãos, mais que jura de cristão
                                G#m7          G#7/C
Mais que as pedras desse cais
              C#m7(11)     F#7(9)              B7M     E7M
Eu te dei certeza da certeza do meu coração
            Fm7(b5)       Bb7(b9)  Bbm7(b5) F#7(9)
Mas a natureza vira a mesa da razão

B7M                         F#/A#                                     F#m/A
Deus, eu pensei que fosse Deus e que os mares fossem meus
                 Am6    C#7(9)/G# G7M                       B/F#   G#m7
Como pensam os ingleses, mel, eu pensei que fosse mel
          C#7(4)    C#7(13)         F#7(4)(9)    D#7(4) D#7
E bebi da vida como bebe um marinheiro de partida

----------------- Acordes -----------------
A/G = 3 X 2 2 2 X
A7(9) = 5 X 5 4 2 X
A7(b9) = 5 X 5 3 2 X
Ab7(9) = 4 X 4 3 1 X
Abm6 = 4 X 3 4 4 X
Am6 = 5 X 4 5 5 X
Am7(b5) = 5 X 5 5 4 X
B/F# = X X 4 4 4 2
B7M = X 2 4 3 4 2
Bb/Ab = 4 X 3 3 3 X
Bb/F = X 8 8 7 6 X
Bb7(b9) = 6 X 6 4 3 X
Bb7M = X 1 3 2 3 1
Bbm7(b5) = X 1 2 1 2 X
C#7(13) = X 4 X 4 6 6
C#7(4) = X 4 6 4 7 4
C#m6 = X 4 X 3 5 4
C#m7(11) = X 4 4 4 5 4
C#º = X 4 5 3 5 3
C7(13) = X 3 X 3 5 5
C7(4) = X 3 3 3 1 X
C7(9) = X 3 2 3 3 X
Cm6 = X 3 X 2 4 3
Cm6/Eb = X X 1 2 1 3
Cm7 = X 3 5 3 4 3
Cm7(11) = X 3 3 3 4 3
D#7 = X 6 5 6 4 X
D#7(4) = X X 1 3 2 4
D#7(b9) = X 6 5 6 5 X
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
D7(13) = X 5 X 5 7 7
D7(4) = X X 0 2 1 3
D7(b9) = X 5 4 5 4 X
D7/F# = X X 4 5 3 5
Dm7(b5) = X X 0 1 1 1
Dm9/F = 1 0 0 2 1 0
E7 = 0 2 2 1 3 0
E7M = X X 2 4 4 4
Eb7/G = 3 X 1 3 2 X
Eb7M = X X 1 3 3 3
Em7(9) = X 7 5 7 7 X
Em7(b5) = X X 2 3 3 3
F#/A# = 6 X 4 6 7 X
F#7(4)(9) = 2 X 2 1 0 X
F#7(9) = X X 4 3 5 4
F#7M = 2 X 3 3 2 X
F#m/A = X 0 4 2 2 2
F/A = 5 X 3 5 6 X
F7(4)(9) = X X 3 3 4 3
F7(9) = X X 3 2 4 3
Fm/Ab = 4 3 3 1 X X
Fm7(b5) = X X 3 4 4 4
G#7/C = X 3 4 1 4 X
G#m7 = 4 X 4 4 4 X
G/D = X 5 5 4 3 X
G6(9) = X X 5 4 5 5
G7(4)(9) = 3 X 3 2 1 X
G7(9) = 3 X 3 2 0 X
G7(b9) = 3 X 3 1 0 X
G7/B = X 2 3 0 3 X
G7M = 3 X 4 4 3 X
Gm/D = X 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
