Chico Buarque - Roda Viva

[Intro]  Em7(9)  A7(13)  D7M  D6  G#m7(5b)  C#7  F#7  Bm7

 Bm7                     G7
Tem dias que a gente se sente
                     F#7
Como quem partiu ou morreu
           Em7    A7   D7M
A gente estancou de repente
    D6            G#m7(5b)  C#7    F#7
Ou foi o mundo então      que cresceu
F#7       B7      Em7
A gente quer ter voz ativa
          A7      D6
No nosso destino mandar
              C#7    F#7(13)  Bm7
Mas eis que chega a roda     viva
             G7          F#7
E carrega o destino prá lá

      Bm7          Bm/A
Roda mundo roda gigante

     Em             A7
Rodamoinho, roda   pião
         Am7   D7(9b)  G7M
O tempo rodou num ins_____tante
               F#7     Bm7
Nas voltas do meu coração


Bm7                   G7
A gente vai contra a corrente
                 F#7
Até não poder resistir
             Em7      A7     D7M
Na volta do barco é que     sente
    D6       G#m7(5b)   C#7    F#7
O quanto deixou        de cumprir
                 B7    Em7
Faz tempo que a gente cultiva
              A7          D6
A mais linda roseira que há
              C#7    F#7(13)  Bm7
Mas eis que chega a roda     viva
             G7          F#7
E carrega a roseira prá lá

      Bm7          Bm/A
Roda mundo roda gigante
      Em             A7
Rodamoinho, roda   pião
         Am7      D7(9b)   G7M
O tempo rodou num ins_____tante
               F#7     Bm7
Nas voltas do meu coração


Bm7             G7
A roda da saia mulata
                         F#7
Não quer mais rodar não senhor
           Em7   A7    D7M
Não posso fazer sere__nata
 D6        G#m7(5b)   C#7  F#7
A roda de samba     aca___bou
         B7        Em7
A gente toma a iniciativa
          A7       D6
Viola na rua a cantar
              C#7    F#7(13)  Bm7
Mas eis que chega a roda     viva
              G7       F#7
E carrega a viola prá lá

      Bm7          Bm/A
Roda mundo roda gigante
      Em            A7
Rodamoinho, roda   pião
         Am7      D7(9b)   G7M
O tempo rodou num ins_____tante
               F#7     Bm7
Nas voltas do meu coração

Bm7                  G7
O samba, a viola, a roseira
                    F#7
Um dia a fogueira queimou
           Em7   A7     D7M
Foi tudo ilusão passa__geira
D6            G#m7(5b)    C#7  F#7
Que a brisa primeira     le___vou
            B7      Em7
No peito a saudade cativa
               A7      D6
Faz força pro tempo parar
              C#7    F#7(13)  Bm7
Mas eis que chega a roda     viva
             G7
E carrega a saudade prá lá

      Bm7        Bm/A
Roda mundo roda gigante
     Em             A7
Rodamoinho, roda   pião
         Am7   D7(9b)      G7M
O tempo rodou num ins_____tante
               F#7     Bm7
Nas voltas do meu coração

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7(13) = X 0 X 0 2 2
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
Bm/A = X 0 4 4 3 X
Bm7 = X 2 4 2 3 2
C#7 = X 4 3 4 2 X
D6 = X 5 4 2 0 X
D7(9b) = X 5 4 5 4 X
D7M = X X 0 2 2 2
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
Em7(9) = X 7 5 7 7 X
F#7 = 2 4 2 3 2 2
F#7(13) = 2 X 2 3 4 X
G#m7(5b) = 4 X 4 4 3 X
G7 = 3 5 3 4 3 3
G7M = 3 X 4 4 3 X
