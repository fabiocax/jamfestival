Chico Buarque - Recife, Cidade Lendária

D/A             D(#5)/A         D6/A D7/A
Eu ando pelo Recife, noites sem fim
F#m7     F#m(#5)    F#m   Db7/Ab    F#m7
percorro bairros distantes sempre a escutar
B7(b9) Em7                    Dbm7(b5)  F#7
      Luanda, Luanda onde estás
Bm7         E7(9)  Em7(9)  A7(b13)
É a alma de prêto a penar
D         D(#5)    D6
Recife, cidade lendária
D(#5)    D                         Em7 B7/F#
de prêtas de engenho cheirando a banguê
Em7        B7/Eb      Em7
Recife de velhos sobrados
Em7/D        A7/C#
compridos, escuros
      A/G    D6/F#
faz gosto se vê
D                      Db7
Recife teus lindos jardins
          C7M        B7         Em7 B7/F#
recebem a brisa que vem do alto mar

Em7                    F7
Recife, teu céu tão bonito
               Bb7             Em7(9)  A7(b13)
tem noites de lua pra gente cantar
D       D(#5)    D6
Recife de cantandores
D(#5)    D                  Em7 B7/F#
vivendo da glória em pleno terreiro
Em7   B7/Eb      Em7
Recife dos maracatús
Em7/D        A7/C# A/G         D6/F
dos tempos distante de Pedro Primeiro
Am6/C                      B7(b9)
Responde ao que vou perguntar
       Em7               Abm7(b5) Db7(b9)
que é feito de teus lampiões
        F#m7   B7(b9)       Em7
Onde outrora os boêmios cantavam
      A7         D
suas lindas canções.

(solo)
D F#7/Db Bm7 D7/A
G B7/F# Em7 G7/D
C E7/B Am7 C7/G
F A/E Dm7 F7/C
Bb A7 Ab7M G7
Gb7M C7 F Em7(9) A7(b13)

----------------- Acordes -----------------
A/E = 0 X 2 2 2 0
A/G = 3 X 2 2 2 X
A7 = X 0 2 0 2 0
A7(b13) = X 0 X 0 2 1
A7/C# = X 4 5 2 5 X
Ab7M = 4 X 5 5 4 X
Abm7(b5) = 4 X 4 4 3 X
Am6/C = X 3 4 2 5 2
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
B7(b9) = X 2 1 2 1 X
B7/Eb = X X 1 2 0 2
B7/F# = X X 4 4 4 5
Bb = X 1 3 3 3 1
Bb7 = X 1 3 1 3 1
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
C7/G = 3 X 2 3 1 X
C7M = X 3 2 0 0 X
D = X X 0 2 3 2
D(#5) = X 5 4 3 3 X
D/A = X 0 X 2 3 2
D6 = X 5 4 2 0 X
D6/A = X 0 4 4 3 X
D6/F# = 2 X 0 2 0 X
D7/A = 5 X 4 5 3 X
Db7 = X 4 3 4 2 X
Db7(b9) = X 4 3 4 3 X
Db7/Ab = 4 X 3 4 2 X
Dbm7(b5) = X 4 5 4 5 X
Dm7 = X 5 7 5 6 5
E7(9) = X X 2 1 3 2
E7/B = X 2 2 1 3 X
Em7 = 0 2 2 0 3 0
Em7(9) = X 7 5 7 7 X
Em7/D = X X 0 0 3 0
F = 1 3 3 2 1 1
F#7 = 2 4 2 3 2 2
F#7/Db = X 4 4 3 5 X
F#m = 2 4 4 2 2 2
F#m(#5) = 2 5 4 2 3 2
F#m7 = 2 X 2 2 2 X
F7 = 1 3 1 2 1 1
F7/C = X 3 3 2 4 X
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
G7/D = X X 0 0 0 1
Gb7M = 2 X 3 3 2 X
