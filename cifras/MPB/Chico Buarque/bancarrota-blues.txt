Chico Buarque - Bancarrota Blues

(De: Chico Buarque & Edu Lobo)

Intr.: E7M(9) / C7(9) / F7M / F#m7 B7(9) E/G# / Gº F#7 F#m7 / B7/4(9) B7(b9)

E7M(9)      A#m7(b5)    E6/B       A#m7(b5)  E7M(9)      A#m7(b5)   E6/B A#m7(b5)
      Uma fazen______da Com casarão         Imensa  varan________da
Bm7       E7(b9)    A7(13)     C#7(9)         G7(#11)   F#7(13) C7(9) B7/4(9)
Dá gerimum       Dá muito mamão       Pé de jaca_____randá
            E7M(9) A#m7(b5) E6/B  A#m7(b5)     E7M(9) A#m7(b5) E6/B A#m7(b5)
Eu posso vender                   Quanto  você dá?
E7M(9)         A#m7(b5)    E6/B         A#m7(b5)    E7M(9)         A#m7(b5)   E6/B A#m7(b5)
      Algum mosqui______to Chapéu de sol         Bastante água fres________ca
Bm7        E7(b9)     A7(13)         C#7(9)             G7(#11)   F#7(13) C7(9) B7/4(9)
Tem surubim       Tem isca  pra anzol       Mas nem tem que    pescar
            E7M(9) A#m7(b5) E6/B  A#m7(b5)       E6/B A#m7(b5) E7M(9) G#7(b13) G#7
Eu posso vender                   Quanto  quer pagar?

C#m         C#m(7M)       C#m7       C#m6     A7(9)                 G#7(13) D7(9)
   O que eu te_____nho Eu devo a Deus     Meu chão, meu céu, meu mar
   C#7/4(9)          C#7(b9)   F#7(13)           Gº
Os olhos   do meu bem        E os     filhos meus

    E/G#            C7(9)     B7/4(9) C7(9) B7/4(9)
Se alguém pensa que vai  levar
            E7M(9) A#m7(b5) E6/B
Eu posso vender
A#m7(b5)      G#7(13) D7(9) C#7/4(9) C#7(b9) F#7(13) C7(9) B7/4(9) B7(b9)
Quanto  vai pagar?

E7M(9)      A#m7(b5) E6/B         A#m7(b5)  E7M(9)      A#m7(b5)   E6/B A#m7(b5)
      Os diamantes   rolam no chão        O ouro  é poei________ra
Bm7         E7(b9)    A7(13)       C#7(9)        G7(#11)    F#7(13) C7(9) B7/4(9)
Muita mulher      pra passar sabão       Papoula pra    cheirar
            E7M(9) A#m7(b5) E6/B  A#m7(b5)      E7M(9) A#m7(b5) E6/B A#m7(b5)
Eu posso vender                   Quanto  vai pagar?
E7M(9)           A#m7(b5)     E6/B      A#m7(b5)  E7M(9)     A#m7(b5)    E6/B A#m7(b5)
      Negros quimbun______dos Pra variar        Diversos açoi________tes
Bm7         E7(b9)    A7(13)       C#7(9)         G7(#11)  F#7(13) C7(9) B7/4(9)
Doces lundus      Pra nhonhô sonhar      À sombra dos    oitis
            E7M(9) A#m7(b5) E6/B  A#m7(b5)           E6/B A#m7(b5) E7M(9) G#7(b13) G#7
Eu posso vender                   Que     é que você diz?

C#m      C#m(7M)   C#m7       C#m6     A7(9)            G#7(13) D7(9)
   Sou feliz     E devo a Deus     Meu é____den tropical
  C#7/4(9)             C#7(b9)   F#7(13)           Gº
Orgulho   dos meus pais        E dos    filhos meus
   E/G#         C7(9)       B7/4(9) C7(9) B7/4(9)
Ninguém me tira nem  por mal
             E7M(9) A#m7(b5) E6/B
Mas posso vender
A#m7(b5)        G#7(13) D7(9) C#7/4(9) C#7(b9) F#7(13) C7(9) B7/4(9)
Deixe   algum sinal

E7M(9) / A#m7(b5) / E6/B /

A#m7(b5)        G#7(13) D7(9) C#7/4(9) C#7(b9) F#7(13) C7(9) B7/4(9)
Deixe   algum sinal

E7M(9) / C7(9) / F7M / F#m7 B7(9) E/G# / Gº F#7 F#m7 / B7/4(9) B7(b9) E7(#9) / / / /

----------------- Acordes -----------------
A#m7(b5) = X 1 2 1 2 X
A7(13) = X 0 X 0 2 2
A7(9) = 5 X 5 4 2 X
B7(9) = X 2 1 2 2 X
B7(b9) = X 2 1 2 1 X
B7/4(9) = X 2 2 2 2 2
Bm7 = X 2 4 2 3 2
C#7(9) = X 4 3 4 4 X
C#7(b9) = X 4 3 4 3 X
C#7/4(9) = X 4 4 4 4 4
C#m = X 4 6 6 5 4
C#m(7M) = X 4 6 5 5 4
C#m6 = X 4 X 3 5 4
C#m7 = X 4 6 4 5 4
C7(9) = X 3 2 3 3 X
D7(9) = X 5 4 5 5 X
E/G# = 4 X 2 4 5 X
E6/B = X 2 2 1 2 X
E7(#9) = X 7 6 7 8 X
E7(b9) = X X 2 1 3 1
E7M(9) = X 7 6 8 7 X
F#7 = 2 4 2 3 2 2
F#7(13) = 2 X 2 3 4 X
F#m7 = 2 X 2 2 2 X
F7M = 1 X 2 2 1 X
G#7 = 4 6 4 5 4 4
G#7(13) = 4 X 4 5 6 X
G#7(b13) = 4 X 4 5 5 4
G7(#11) = 3 X 3 4 2 X
Gº = 3 X 2 3 2 X
