Chico Buarque - Essa Moça Tá Diferente

(De: Chico Buarque)

          Bm7      G7      F#7    Bm7
Essa moça tá diferente, já não me conhece mais
B7(b9)         Em7/9   A7/13    Am6
      Está pra lá   de pra  frente
     G7M  G7             F#7
Está me passando pra trás

          Bm7    G7         F#7  Bm7
Essa moça tá decidida, a se supermodernizar
B7(b9)       Em7/9   A7/13  Am6
      Ela só samba escondi.....da
      G7M    G7          F#7
Que é pra ninguém reparar

           Bm7      G7     F#7    Bm7
Eu cultivo rosas e rimas, achando que é muito bom
B7(b9)       Em7/9 A7/13  Am6
      Ela me olha  de    cima
      G7M  G7           F#7
E vai desinventar o som


               Bm7         G7
Faço-lhe um concerto de flauta
      F#7    Bm7         B7(b9)
E não lhe desperto emoção
         Em7/9    A7/13 Am6    G7M    G7       F#7
Ela quer ver  o astronauta  descer na televisão

                Bm7                 Bm/A
Mas o tempo vai,    mas o tempo vem
              G7M                     Bm7/F#
Ela me desfaz,    mas o que é que tem
              C#°         Bm7                              F#7
Que ela só me guarda despeito, que ela só me guarda desdém
                Bm7                 Bm/A
Mas o tempo vai,    mas o tempo vem
              G7M                     Bm7/F#
Ela me desfaz,    mas o que é que tem
             C#°         Bm7
Se do lado esquerdo do peito
              G7      F#7
No fundo, ela ainda me   quer bem

Bm7                   G7      F#7    Bm7
   Essa moça tá diferente, já não me conhece mais
B7(b9)         Em7/9   A7/13   Am6
      Está pra lá   de pra  frente
     G7M  G7             F#7
Está me passando pra trás

              Bm7      G7           F#7    Bm7
Essa moça é a tal da janela, que eu me  cansei de cantar
B7(b9)          Em7/9   A7/13 Am6     G7M   G7             F#7
      E agora está   só na    dela, botando só pra quebrar

                Bm7                 Bm/A
Mas o tempo vai,    mas o tempo vem
              G7M                     Bm7/F#
Ela me desfaz,    mas o que é que tem
              C#°         Bm7                              F#7
Que ela só me guarda despeito, que ela só me guarda desdém
                Bm7                 Bm/A
Mas o tempo vai,    mas o tempo vem
              G7M                     Bm7/F#
Ela me desfaz,    mas o que é que tem
             C#°         Bm7
Se do lado esquerdo do peito
           G7         F#7         Bm7
No fundo, ela ainda me   quer bem

....................................
Qualquer dúvida, me mande um e-mail:

....................................

----------------- Acordes -----------------
A7/13 = X 0 X 0 2 2
Am6 = 5 X 4 5 5 X
B7(b9) = X 2 1 2 1 X
Bm/A = X 0 4 4 3 X
Bm7 = X 2 4 2 3 2
Bm7/F# = X X 4 4 3 5
C#° = X 4 5 3 5 3
Em7/9 = X 7 5 7 7 X
F#7 = 2 4 2 3 2 2
G7 = 3 5 3 4 3 3
G7M = 3 X 4 4 3 X
