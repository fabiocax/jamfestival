Chico Buarque - Piano Na Mangueira

(De: Chico Buarque & Tom Jobim)

       A7(b9)                       Dm7             G7/4 G7      Cm7
Manguei______ra, estou aqui na plataforma da Estação       Primei___ra
F7                      Dm7(b5) G7(b13)
  O morro veio me chamar
  Cm7                  Ebm6     Bb7M/D
De   terno branco e chapéu de pa______lha
              Em7     A7(b9)      D7M         A7(b9)
Vou me apresentar à mi______nha no___va parcei______ra
      D7M              Cm6            Eº
Já man___dei subir o piano pra Manguei__ra

A7(b9)                    Dm7           G7/4 G7    Cm7
      A minha música não é   de levantar       poei___ra
F7                           D7/13 D7(b13) G7/4(9) G7(b9)
  Mas pode entrar no barracão
  Cm7                 Ebm6      Bb7M/D
On___de a cabrocha pendura a sai______a
         C7(9/#11)             Cm7/9
No amanhecer      da quarta-fei_____ra


Manguei____ra, Es__tação      Primei___ra de     Manguei______ra

                   Dm7             G7/4 G7      Cm7
Estou aqui na plataforma da Estação       Primei___ra
F7                      Dm7(b5) G7(b13)
  O morro veio me chamar
  Cm7                  Ebm6     Bb7M/D
De   terno branco e chapéu de pa______lha
              Em7     A7(b9)      D7M         A7(b9)
Vou me apresentar à mi______nha no___va parcei______ra
      D7M              Cm6            Eº
Já man___dei subir o piano pra Manguei__ra

A7(b9)                    Dm7           G7/4 G7    Cm7
      A minha música não é   de levantar       poei___ra
F7                           D7/13 D7(b13) G7/4(9) G7(b9)
  Mas pode entrar no barracão
  Cm7                 Ebm6      Bb7M/D
On___de a cabrocha pendura a sai______a
         C7(9/#11)             Cm7/9
No amanhecer      da quarta-fei_____ra

Manguei____ra, Es__tação      Primeira de       Manguei__ra
       Bb6 Bb(b6)       Bb  Bb(#5)       Bb6  Bb(b6)       Bb
Mangueira,       Manguei__ra,     Manguei___ra,     Manguei__ra...

----------------- Acordes -----------------
A7(b9) = 5 X 5 3 2 X
Bb = X 1 3 3 3 1
Bb(#5) = X 1 4 3 3 X
Bb6 = X 1 3 0 3 X
Bb7M/D = X 5 X 3 6 5
C7(9/#11) = X 3 2 3 3 2
Cm6 = X 3 X 2 4 3
Cm7 = X 3 5 3 4 3
Cm7/9 = X 3 1 3 3 X
D7(b13) = X 5 X 5 7 6
D7/13 = X 5 X 5 7 7
D7M = X X 0 2 2 2
Dm7 = X 5 7 5 6 5
Dm7(b5) = X X 0 1 1 1
Ebm6 = X X 1 3 1 2
Em7 = 0 2 2 0 3 0
Eº = X X 2 3 2 3
F7 = 1 3 1 2 1 1
F7/9 = X X 3 2 4 3
G7 = 3 5 3 4 3 3
G7(b13) = 3 X 3 4 4 3
G7(b9) = 3 X 3 1 0 X
G7/4 = 3 5 3 5 3 X
G7/4(9) = 3 X 3 2 1 X
