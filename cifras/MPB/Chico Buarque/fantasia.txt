Chico Buarque - Fantasia

(De: Chico Buarque)

Cm7(9)         Db/Cb   Cm7(9)       G7/B    Ab6             Gm6     Fm7
E     se, de repente A gente não sentisse A dor que a gente finge E sente
         C7/E    F/Eb        F7/C   G7         G7/4      Ab6           Gm6
Se, de repente A gente distraísse O ferro do suplício Ao som de uma canção
  Fm7           Bb7(9)     Eb7M  Ab7             Db7M  Dm7(b5)          G7/4  G7 G7(b13) G7
Então, eu te convi____daria         Pra uma fantasia          Do meu violão

Eb7M  G7/D              Cm7(9)  Bº Bb7                 Eb7M  Am7  D7  G
Canta,    canta uma esperança      Canta, canta uma alegria  Can__ta  mais
Eb7(9)            Ab7M  C7            Fm  G7(b13) G7   G7(b13) G7
      Revirando a noite   Revelando o dia Noi_____te e di______a
G7(b13) G7   G7(b13) G7
Noi_____te e di______a

Eb7M  G7/D            Cm7(9)  Bº Bb7               Eb7M  Am7  D7  G
Canta     a canção do homem      Canta a canção da vida  Can__ta  mais
Eb7(9)              Ab7M  C7             Fm    G7(b13) G7  G7(b13) G7
      Trabalhando a terra   Entornando o vinho Can_____ta, can_____ta
G7(b13) G7  G7(b13) G7
Can_____ta, can_____ta


Eb7M  G7/D             Cm7(9)  Bº Bb7               Eb7M  Am7  D7  G
Canta      a canção do gozo       Canta a canção da graça Can__ta  mais
Eb7(9)             Ab7M  C7             Fm    G7(b13) G7  G7(b13) G7
      Preparando a tinta   Enfeitando a praça Can_____ta, can_____ta
G7(b13) G7  G7(b13) G7
Can_____ta, can_____ta

Eb7M  G7/D            Cm7(9)  Bº Bb7               Eb7M  Am7  D7  G
Canta     a canção de glória     Canta a santa melodia   Can__ta  mais
Eb7(9)            Ab7M  C7            Fm  G7(b13) G7   G7(b13) G7
      Revirando a noite   Revelando o dia Noi_____te e di______a
G7(b13) G7   G7(b13) G7  G7(b13) G7   G7(b13) G7  G7(b13) G7   G7(b13) G7
Noi_____te e di______a   Noi_____te e di_______a, noi_____te e di______a

Cm7(9)         Db/Cb   Cm7(9)       G7/B    Ab6             Gm6     Fm7
E     se, de repente A gente não sentisse A dor que a gente finge E sente
         C7/E    F/Eb        F7/C   G7         G7/4      Ab6           Gm6
Se, de repente A gente distraísse O ferro do suplício Ao som de uma canção
  Fm7           Bb7(9)     Eb7M  Ab7             Db7M  G7          Cm7(9)
Então, eu te convi____daria         Pra uma fantasia     Do meu violão

Eb7M  G7/D              Cm7(9)  Bº
Canta,    canta uma esperança...

ACORDES:

Cm7(9) - X3133X
Db/Cb  - X2312X     Db7M    - X46564
G7/B   - X2303X     Dm7(b5) - X5656X
Ab6    - 4X354X     G7(b13) - 3X344X
Gm6    - 3X233X     G7/D    - X5546X
Fm7    - 1X111X     Bº      - X2313X
C7/E   - XX2313     Bb7     - X13131
F/Eb   - XX1211     Am7     - X02213
F7/C   - X3324X     D7      - XX0212
G7     - 3X343X     G       - 320033
G7/4   - 3X353X     Eb7(9)  - X6566X
Bb7(9) - X1011X     Ab7M    - 4X554X
Eb7M   - X65333     C7      - X35353
Ab7    - 4X454X     Fm      - 133111

----------------- Acordes -----------------
Ab6 = 4 X 3 5 4 X
Ab7 = 4 6 4 5 4 4
Ab7M = 4 X 5 5 4 X
Am7 = X 0 2 0 1 0
Bb7 = X 1 3 1 3 1
Bb7(9) = X 1 0 1 1 X
Bº = X 2 3 1 3 1
C7 = X 3 2 3 1 X
C7/E = 0 3 2 3 1 0
Cm7(9) = X 3 1 3 3 X
D7 = X X 0 2 1 2
Db7M = X 4 6 5 6 4
Dm7(b5) = X X 0 1 1 1
Eb7(9) = X 6 5 6 6 X
Eb7M = X X 1 3 3 3
F/Eb = X X 1 2 1 1
F7/C = X 3 3 2 4 X
Fm = 1 3 3 1 1 1
Fm7 = 1 X 1 1 1 X
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
G7(b13) = 3 X 3 4 4 3
G7/4 = 3 5 3 5 3 X
G7/B = X 2 3 0 3 X
G7/D = X X 0 0 0 1
Gm6 = 3 X 2 3 3 X
