Marina Lima - Eu Acho Que Não Sou Só Eu


     D           Am           A4   D6/9
Eu sei que você não quer mais amor
Am          A4          C7+       D6/9
Será que é? Só pra me provocar ira
           C7+          D6/9  D4  D6/9
Só pra me instigar mais se for meu bem
             Am  A4           D6/9
Você já conseguiu quase me piorou
     Am   A4      C7+                    D6
E até pariu essa enorme vontade de vida
       C7+                           D6/9   D4
De ter você sempre perto e aos meus pés
          D4   D            D6/9    F7+/5-
Ah! o amor Ah! o amor e acho que não sou
                 D6/9  F7+/5-
Só eu que sinto, não.........

----------------- Acordes -----------------
A4 = X 0 2 2 3 0
Am = X 0 2 2 1 0
C7+ = X 3 2 0 0 X
D = X X 0 2 3 2
D4 = X X 0 2 3 3
D6 = X 5 4 2 0 X
D6/9 = X 5 4 2 0 0
F7+/5- = 1 X X 2 0 0
