Marina Lima - À Francesa/Uma Noite e Meia

À FRANCESA

Bm               Bm7M      Bm7
Meu amor se você for embora
                 Bm6
Sabe lá o que será de mim
Em
Passeando pelo mundo a fora
Bm                 Bm7M        Bm7
Na cidade que não tem mais fim
                Bm6
Hora dando fora hora bola
Em7
Uma irresponsável pobre de mim

G
Se eu te peço para ficar ou não
D
Meu amor eu lhe juro
G
Que não quero deixá-lo na mão

   D
E nem sozinho no escuro
          E
Mas os momentos felizes
Não estão escondidos
  A               A7
Nem no passado e nem no futuro

Meu amor não vai haver tristeza
Nada além de fim de tarde a mais
Mas depois as luzes todas acesas
Paraísos artificiais
E se você saísse à francesa
Eu viajaria muito mais


UMA NOITE E MEIA


Bm
Vem chegando o verão
A
Um calor no coração
G
Essa magia colorida
F#
Coisas da vida

Bm
Não demora muito agora
A
Toda de bundinha de fora
G
Top less na areia
F#7/4      F#7
Virando sereia

G               A       Bm
Essa noite eu quero te ter
G          A            Bm
Toda se ardendo só pra mim
G               A       Bm
Essa noite eu quero te ter
G        Em          F#7/4 F#
Te envolver, te seduzir

O dia inteiro de prazer
Tudo que quiser, eu vou te dar
O mundo inteiro aos seus pés
Só pra poder te amar

Roubo as estrelas lá no céu
Numa noite e meia desse sabor
Pego a lua, aposto no mar
Como eu vou te ganhar

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
Bm6 = 7 X 6 7 7 X
Bm7 = X 2 4 2 3 2
Bm7M = X 2 4 3 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F# = 2 4 4 3 2 2
F#7 = 2 4 2 3 2 2
F#7/4 = 2 4 2 4 2 X
G = 3 2 0 0 0 3
