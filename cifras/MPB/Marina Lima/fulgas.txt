Marina Lima - Fulgás

[Intro] Bm  F#7(b13) Bm7  F#7(b13)

Bm          Bm7+      Bm7 Bm7+
Meu mundo você é quem faz
Bm     Bm7+    Em Em7M Em7
Música letra e dança
                   F#7
Tudo em você é fullgás
                 Bm
Tudo você é quem lança
       F#7
Lança mais e mais
Bm          Bm7+      Bm7 Bm7+
Só vou te contar um segredo
Bm     Bm7+    Em Em7M Em7
Nada de mau nos alcança
                   F#7
Pois tendo você meu brinquedo
                  G7+   A6 B
Nada machuca nem cansa


       B                     B7/4 B7
Então venha me dizer o que será
        F#m7 B7/13 E7M
Da minha vida sem você
Em7             D#m7     G#7 C#7/9
Noites de frio, dia não há
                   G           F#
E um mundo estranho pra me segurar

           B                B7/4 B7
Então onde quer que você vá, é lá
             E7M
Que eu vou estar amor esperto
A9              Bm Bm7+ Bm7 Bm7+
Tão bom te amar
Bm          Bm7+      Bm7 Bm7+
E tudo de lindo que eu faço
Bm     Bm7+    Em Em7M Em7
Vem com você vem feliz
                   F#7
Você me abre seus braços
                   G Em
E a gente faz um país
                   F#7
Você me abre seus braços
                     G7+  A6  B7/4 B7
E a gente faz um país

----------------- Acordes -----------------
A6 = 5 X 4 6 5 X
A9 = X 0 2 2 0 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
B7/13 = X 2 X 2 4 4
B7/4 = X 2 4 2 5 2
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
Bm7+ = X 2 4 3 3 2
C#7/9 = X 4 3 4 4 X
D#m7 = X X 1 3 2 2
E7M = X X 2 4 4 4
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
Em7M = X X 2 4 4 3
F# = 2 4 4 3 2 2
F#7 = 2 4 2 3 2 2
F#m7 = 2 X 2 2 2 X
G = 3 2 0 0 0 3
G#7 = 4 6 4 5 4 4
G7+ = 3 X 4 4 3 X
