Marina Lima - Ensaios de Amor

Intro: A#7+ Am7 D7 Gm7 C7/9 B7/9 A#7+ Am7 D7 G7/13 A#7/9

F7+
Te pressenti
                  D7/9
No início de uma nova canção
F7+                     D7/9  D7/9-
Onde costumava morar a emoção
 G7/13                 D7/9
E dei pra imaginar o timbre da sua vez
 G7/13
Seu jogo de cena
                     Gm7    C7/9
Seu texto pra me conquistar
      F7+
Eu já sei até de cor
    C7/9
De bom só pra melhor
          D7/9
Fizemos tantos ensaios
                   Bm7
Em todos os tons do amor

E5+/7              Am7  A#º Bm7
Que na estréia da gente, a sós
E4/7       E7         Am7 D7/9 G7+ C7/9
No nosso corpo é que acende a luz

----------------- Acordes -----------------
A#7+ = X 1 3 2 3 1
A#7/9 = X 1 0 1 1 X
A#º = X 1 2 0 2 0
Am7 = X 0 2 0 1 0
B7/9 = X 2 1 2 2 X
Bm7 = X 2 4 2 3 2
C7/9 = X 3 2 3 3 X
D7 = X X 0 2 1 2
D7/9 = X 5 4 5 5 X
D7/9- = X 5 4 5 4 X
E4/7 = 0 2 0 2 0 X
E5+/7 = 0 X 0 1 1 0
E7 = 0 2 2 1 3 0
F7+ = 1 X 2 2 1 X
G7+ = 3 X 4 4 3 X
G7/13 = 3 X 3 4 5 X
Gm7 = 3 X 3 3 3 X
