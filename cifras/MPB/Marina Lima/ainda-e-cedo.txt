Marina Lima - Ainda é cedo

[Intro:] Em D Bm

                   Em  D
Uma menina me ensinou
                   Bm
Quase tudo que eu sei
                  Em
Era quase escravidão
    D                      Bm
Mas ela me tratava como um rei
                  Em     D
Ela fazia muitos planos
                      Bm
Eu só queria estar ali
                Em
Sempre ao lado dela
        D           Bm
Eu não tinha onde ir

Mas, egoísta que eu sou,
Me esqueci de ajudar

A ela como ela me ajudou
E não quis me separar
Ela também estava perdida
E por isso se agarrava a mim também
E eu me agarrava a ela
Porque eu não tinha mais ninguém

                     Em          D           Bm
E eu dizia: Ainda é cedo, cedo, cedo, cedo, cedo

Sei que ela terminou
O que eu nem comecei
E o que ela descobriu
Eu aprendi também, eu sei
Ela me falou: - Você tem medo
Aí eu disse: - Quem tem medo é você
Falamos o que não devia
Nunca ser dito por ninguém
ela me disse: - Eu não sei
Mais o que eu sinto por você
Vamos dar um tempo, um dia a gente se vê

----------------- Acordes -----------------
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
