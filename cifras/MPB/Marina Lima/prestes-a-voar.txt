Marina Lima - Prestes a voar

Intro:
E|----------------------|-------------------------------------------------
B|----------------------|-------1--------------------------1-2--1/2-4---1-
G|-----------------1----|-----1---3--3-1-----------------1------------3---
D|-1h3-3-1h3-1h3-3---3--|-1h3------------3-1-1-3-3---1h3------------------
A|----------------------|-------------------------------------------------
E|----------------------|-------------------------------------------------


Marina Lima /Antonio Cícero

[Intro:] Fm

G#
Na tarde violeta
C#
E violenta
G#                 F#7/4 F#7
Você gelou o meu olhar
G#
Era num cruzamento

C#
Fazia tempo,
G#                     F#7/4 F#7
Te perguntei "o que que há?"

Você me disse apenas:
"Fui ao inferno,
Tudo prá não te procurar"
Eu me senti num beco
E disse a seco
"Como é que não te vi por lá?"

Fm
Eu já quis morrer
Eu já quis matar
Tudo por alguém
Que não sabe amar

Eu analiso tudo
E me pergunto
"Quero a verdade
Ou quero amar?"
À beira do abismo,
Queira ou não queira,
Estamos prestes a voar

----------------- Acordes -----------------
C# = X 4 6 6 6 4
F#7 = 2 4 2 3 2 2
F#7/4 = 2 4 2 4 2 X
Fm = 1 3 3 1 1 1
G# = 4 3 1 1 1 4
