Marina Lima - Cara

A            D     A
 Jamais foi tão escuro
          D    G
 No país do futuro
        Bm    E7
 E da televisão
A         D   A
 E nessa labirinto
           D         G
 O que eu sinto, eu sinto
          Bm    E7
 E chamam de paixão

C                          Bb
 E me apaixonam questões ardentes
                             F  Fm   C
 Que nem consigo assim de repente expor
                           Bb
 Mas entre elas há coisas raras
                             F  Fm     C
 Que são belezas, loucuras, taras de amor


A          D    A
 Há sonhos e insônias
           D   G
 Ozônios e Amazônias
            Bm       E7
 E um novo amor no ar
A          D         A
 Entre bilhões de humanos
        D      G
 E siderais enganos
              Bm    E7
 Eu quero é te abraçar

C                           Bb
 Mil novecentos e não sei quanto
                         F  Fm   C
 É fim de século e no entanto é meu
                          Bb
 Meu cada gesto e cada segundo
                              F  Fm     C
 Em que te amar é um claro assunto no breu

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bb = X 1 3 3 3 1
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
