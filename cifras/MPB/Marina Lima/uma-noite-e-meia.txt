Marina Lima - Uma Noite e Meia

(intro) Bm A G F#

Bm
  Vem chegando o verão
A
  Um calor no coração
G
  Essa magia colorida
F#
  Coisas da vida

Bm
  Não demora muito agora
A
  Toda de bundinha de fora
G
  Top less na areia
F#4      F#
  Virando sereia

G              A         Bm
  Essa noite eu quero te ter

G              A         Bm
  Toda se ardendo só pra mim
G              A         Bm
  Essa noite eu quero te ter
G            G#°       F#4   F#
  Te envolver, te seduzir

Bm
  Vem chegando o verão...

Bm
  O dia inteiro de prazer
A
  Tudo que quiser, eu vou te dar
G
  O mundo inteiro aos seus pés
F#4                F#
  Só pra poder te amar

Bm
  Roubo as estrelas lá no céu
A                         G
  Numa noite e meia desse sabor
                        F#4
  Pego a lua, aposto no mar
F#
  Como eu vou te ganhar

G
  Essa noite eu quero...

(solo)  Bm A G F#

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
F# = 2 4 4 3 2 2
F#4 = 2 4 4 4 2 2
G = 3 2 0 0 0 3
G#° = 4 X 3 4 3 X
