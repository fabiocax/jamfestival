Marina Lima - À francesa

Am           Am7M           Am7
Meu amor se você for embora
                 Am6
Sabe lá o que será de mim
 Dm7            F/G
Passeando pelo mundo a fora
Am                 Am7M        Am7
Na cidade que não tem mais fim
                  Am6
Hora dando fora hora bola
Dm7                F/G
Uma irresponsável pobre de mim

F
Se eu te peço para ficar ou não
C                   C7
Meu amor eu lhe juro
F
Que não quero deixá-lo na mão
   C
E nem sozinho no escuro

         D7
Mas os momentos felizes
         D7
Não estão escondidos
  G               G7
Nem no passado e nem no futuro

 Am                 Am7M        Am7
Meu amor não vai haver tristeza
                      Am6
Nada além de fim de tarde a mais
Dm7                  F/G
Mas depois as luzes todas acesas
 Am            Am7M  Am7
Paraísos artificiais
                 Am6
E se você saísse à francesa
Dm7                  F/G     G7
Eu viajaria muito mais muito mais
F
Se eu te peço para ficar ou não
C                   C7
Meu amor eu lhe juro
F
Que não quero deixá-lo na mão
   C                    C7
E nem sozinho no escuro
         D7
Mas os momentos felizes
         D7
Não estão escondidos

 G               G7
Nem no passado e nem no futuro

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am6 = 5 X 4 5 5 X
Am7 = X 0 2 0 1 0
Am7M = X 0 2 1 1 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
D7 = X X 0 2 1 2
Dm7 = X 5 7 5 6 5
F = 1 3 3 2 1 1
F/G = 3 X 3 2 1 X
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
