Lenine - Aquilo Que Dá No Coração

[Intro] F5  F5/G  Bb  F/A  
        Dm9  G7(5+)  G4  A#m9  
        F5  F5/G  Bb  F/A  
        Dm9  G7(5+)  G4  A#m9                        

 F5              F5/G
Aquilo que dá no coração
      Bb         F/A                                  
E nos joga nessa sinuca
    Dm9          G7(5+)                            
Que faz perder o ar e a razão
      G4            A#m9             
E arrepia o pêlo da nuca
F5               F5/G          
Aquilo reage em cadeia
     Bb           F/A                   
Incendeia o corpo inteiro
  Dm9          G7(5+)                       
Faísca, risca, trisca, arrodeia
   G4             A#m9               
Dispara o rito certeiro

F6      C/E       
Avassalador
Bb         C6  
Chega sem avisar
Bb                  F/A                    
Toma de assalto, atropela
C             Bb                   
Vela de incendiar
F6      C/E       
Arrebatador
Bb               C6           
Vem de qualquer lugar
Bb                F/A   
Chega, nem pede licença
 G7         F#7M
Avança sem ponderar

( F5  F5/G  Bb  F/A )
( Dm9  G7(5+)  G4  A#m9 ) 

F5             F5/G                          
Aquilo bate, ilumina
           Bb              F/A                        
Invade a retina , retém no olhar
   Dm9              G7(5+)                        
O lance que laça na hora
        G4               A#m9            
Aqui e agora, futuro não há
F5                F5/G           
Aquilo se pega de jeito
           Bb              F/A                  
Te dá um sacode, pra lá de além
  Dm9              G7(5+)                       
O mundo muda, estremece
           G4                  A#m9
O caos acontece , não poupa ninguém

F6      C/E       
Avassalador
Bb          C6  
Chega sem avisar
Bb                   F/A                    
Toma de assalto, atropela
C             Bb                   
Vela de incendiar
F6      C/E       
Arrebatador
Bb               C6           
Vem de qualquer lugar
Bb                F/A   
Chega, nem pede licença
 F7          E7M
Avança sem ponderar

----------------- Acordes -----------------
A#m9*  = X 1 3 5 2 1 - (*G#m9 na forma de A#m9)
Bb*  = X 1 3 2 1 1 - (*Ab na forma de Bb)
Bb*  = X 5 X 3 6 6 - (*Ab na forma de Bb)
Bb*  = X 1 2 0 1 X - (*Ab na forma de Bb)
Dm9*  = X 5 7 9 6 5 - (*Cm9 na forma de Dm9)
E7M*  = X X 2 4 4 4 - (*D7M na forma de E7M)
F#7M*  = 2 X 3 3 2 X - (*E7M na forma de F#7M)
G4*  = 3 5 5 5 3 3 - (*F4 na forma de G4)
G7*  = 3 5 3 4 3 3 - (*F7 na forma de G7)
G7(5+)*  = 3 X 3 4 4 3 - (*F7(5+) na forma de G7(5+))
