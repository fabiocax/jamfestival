Lenine - Rua da Passagem (Trânsito)

Em                 D     Em
Os curiosos atrapalham o trânsito
              D     Em
Gentileza é fundamental
                     D     Em
Não adianta esquentar a cabeça
                    D     Em
Não precisa avançar no sinal
                    D     Em
Dando seta pra mudar de pista
                    D     Em
Ou pra entrar na transversal
                  D         Em
Pisca alerta pra encostar na guia
              D         Em
Pára brisa para o temporal
                 D         Em
Já buzinou, espere, não insista
                  D         Em
Desencoste o seu do meu metal
                   D         Em
Devagar pra contemplar a vista
             D         Em
Menos peso do pé no pedal
                    D         Em
Não se deve atropelar um cachorro
             D         Em
Nem qualquer outro animal
                 D         Em

Todo mundo tem direito à vida
                D         Em
Todo mundo tem direito igual

                D         Em
Motoqueiro caminhão pedestre
                  D         Em
Carro importado carro nacional
               D         Em
Mas tem que dirigir direito
                 D         Em
Para não congestionar o local
                  D         Em
Tanto faz você chegar primeiro
                D         Em
O primeiro foi seu ancestral
                D         Em
É melhor você chegar inteiro
                D         Em
Com seu venoso e seu arterial
               D         Em
A cidade é tanto do mendigo
      D         Em
Quanto do policial

                 D         Em
Todo mundo tem direito à vida
                D         Em
Todo mundo tem direito igual

                D         Em
Travesti trabalhador turista
           D         Em
Solitário família casal
                 D         Em
Todo mundo tem direito à vida
                D         Em
Todo mundo tem direito igual
                D         Em
Sem ter medo de andar na rua
                D         Em
Porque a rua é o seu quintal
                 D         Em
Todo mundo tem direito à vida
                D         Em
Todo mundo tem direito igual

                D         Em
Boa noite, tudo bem, bom dia
           D         Em
Gentileza é fundamental
                     D         Em
Pisca alerta pra encostar na guia
                        D         Em
Com licença, obrigado, até logo, tchau

----------------- Acordes -----------------
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
