Lenine - Ninguem Faz Ideia

afinação: Drop D (6D 5A 4D 3G 2B 1E)

intro: A9  G9  D7/9 (arpejo)
       A9  G9  D7/9 (D* D#* D* Db* C* C*)
       A9  G9  D7/9 (arpejo)
       A9  G9  D7/9 (harmonico casa 7 cordas 4 e 5)
 
----------|-------------|----------|
-4s5------|-5-6-5-4-3-3-|---(7)----|
----------|-5-6-5-4-3-3-|---(7)----|
--------0-|-------------|----------|
-----3----|-------------|----------|
----------|-------------|----------|

    A9            G9
Malucos e donas de casa
     D7/9
Vocês aí na porta do bar
    A9             G9
os cães sem dono, os boiadeiros
    D7/9
as putas Babalorixás
    A9           G9
Os genios, os caminhoneiros
       D7/9                              A9      G9
Os sem terra e sem teto, atores, maestros, djs
                 D7/9                                A9     G9
os Undergrounds, os megastars, os rolling stones e o rei

                       D7/9                                   A9     G9
ninguém faz idéia de quem vem lá, de quem vem lá, de quem vem lá,
                     D7/9        (harmonico)
ninguém faz idéia de quem vem lá,

   A9         G9            D7/9
Ciganas e neo - nazistas, o bruxo, o mago pajé
        A9               G9
os escritores de science fiction
        D7/9
quem diz e quem nega o que é
           A9          G9
Os que fazem greve de fome
         D7/9
Bandidos, cientistas do espaço
                    A9
os prêmios nobel da paz
     G9                      D7/9
o Dalai Lama, o Mister Bean, burros, Intelectuais

   A9    G9                     D7/9                                      A9  G9
Eu pensei: ninguém faz idéia de quem vem lá, de quem vem lá, de quem vem lá,
                   D7/9           D*Db*D*Db*C*C*
ninguém faz idéia de quem vem lá,
   A9   G9                      D7/9                                    Bb  C
Eu pensei: ninguém faz idéia de quem vem lá, de quem vem lá, de quem vem lá,
                   D7/9           (harmonico)
ninguém faz idéia de quem vem lá,

Introdução...
repete até o fim...

    A9        G9       D7/9       Bb         C
----------|----5-----|-----0----|----------|----------|
-----0----|----0-----|-----5----|-----1----|----3-----|
-----2----|----0-----|-----4----|-----3----|----5-----|
-----2----|----2-----|-----5----|-----3----|----5-----|
-----0----|----0-----|-----5----|----------|----------|
-----0----|----------|----------|----------|----------|

----------------- Acordes -----------------
A9 = X 0 2 2 0 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D = X X 0 2 3 2
D# = X 6 5 3 4 3
D7/9 = X 5 4 5 5 X
Db = X 4 6 6 6 4
G9 = 3 X 0 2 0 X
