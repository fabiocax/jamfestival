Roberta Campos - E Eu Fico

Intro: E7 A7 E7

                     A7                 E7   A7
um pouco de café de novo pra aquecer meu dia
                    A7            E7  A7
um pouco de tv e o sonho já é fantasia
      E7 A7           E7    A7
e eu grito e você não escuta
      E7 A7              E7  A7           E7  A7   E7 A7
e eu fico, ah, eu fico maluca,  eu fico maluca

E7                      A7                E7   A7
escrevo num papel as frases que eu falei no dia
E7                         A7             E7   A7
recordo do silencio, do compasso de tua melodia

      E7 A7           E7    A7
e eu grito e você não escuta
      E7 A7             E7  A7            E7  A7  E7  A7
e eu fico, ah, eu fico maluca,  eu fico maluca


C#m                              G#
  aqui o silencio já não existe mais
C#m                              G#
  os sonhos pintados nas paredes ficaram pra trás
C#m                              G#
  as unhas que pinto já não são as minhas
              F#                      A   A9       E  A
que eu fazia poesia, pra rimar com a pi...a, vazia

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
A9 = X 0 2 2 0 0
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F# = 2 4 4 3 2 2
G# = 4 3 1 1 1 4
