Roberta Campos - Libélula

Intro: (Em  Db) (2x)
G

C
Com as manhãs que vão
                 G
O céu se abre em chuva
C
E o sol que vem depois
               G
Um vento leve pro mar
Am
Me leva com você
   C                G
Um doce suspirar na curva
             Am
Não dá pra esquecer
    C                  G
Pois tudo que eu olho tem você

C
Num meio tanto faz
             G
A minha vida é tua
C
E quando olho pra trás
                G
Segura em minhas curvas
        Am
Me leva com você
    C              G
Um doce suspirar na curva
            Am
Não dá pra esquecer
      C              G
Pois tudo que eu olho tem você

Am    C       G
Não vá, não vá embora
Am           C        G
Não vá, não, a chuva não demora

Am    C       G
Não vá, não vá embora
Am           C        G
Não vá, não, a chuva não demora

        Am
Me leva com você
    C              G
Um doce suspirar na curva
            Am
Não dá pra esquecer
      C              G
Pois tudo que eu olho tem você

(Em C) (2x)

----------------- Acordes -----------------
G*  = 3 2 0 0 0 3 - (*F# na forma de G)
Am*  = X 0 2 2 1 0 - (*G#m na forma de Am)
C*  = X 3 2 0 1 0 - (*B na forma de C)
Em*  = 0 2 2 0 0 0 - (*D#m na forma de Em)
G*  = 3 2 0 0 0 3 - (*F# na forma de G)
