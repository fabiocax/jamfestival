Roberta Campos - Fuga

Intro: ( F# B ) Bm
F#                           B
aqui não há mais flores, o céu se abriu de amores
F#                        Bm
você partiu na noite, levando meu amor
F#                        B
a alma não é pequena, é infinita a vida
F#                   Bm
é tudo sem o nada, é para minha sorte

F#  B        Bm           F#  B
ih!! quero fugir daqui, quero fugir
            Bm
ih!! quero fugir daqui

F#              B         F#            B
coloco minhas botas, sou sim pra toda vida
   F#                B          Bm
eu luto um luto de glória, e a paz é concebida
F#                B          F#      B
estampo a minha força, que trouxe alegria

   F#         B         Bm
a toda essa fonte, de música e nostalgia


F#  B        Bm           F#  B
ih!! quero fugir daqui, quero fugir
            Bm
ih!! quero fugir daqui
       F#  B        Bm
quero fugir ih ih...quero fugir daqui
       F#       B         Bm
quero fugir, ihh ê quero fugir daqui

( F#  B )

----------------- Acordes -----------------
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
F# = 2 4 4 3 2 2
