﻿Roberta Campos - Pedaço do Ceu

Capo Casa 3

Em7                              C
Céu azul, uma nuvem pairou
                   G
Formando traços teus
       D/F#
E o meu amor crescendo mais

Em7                                  C
Se nós dois não formos tão iguais
                      G
Posso te traduzir no meu amor
     D/F#
Crescendo mais

C
No peito até dói
G
Pensar em você traz
D/F#
As melhores lembranças
C
As melhores lembranças


C
Assim que eu desenhar
G
Meus passos no papel
D/F#
Eu vou te encontrar
C
Te levo um pedaço do céu

----------------- Acordes -----------------
Capotraste na 3ª casa
C*  = X 3 2 0 1 0 - (*D# na forma de C)
D/F#*  = 2 X 0 2 3 2 - (*F/A na forma de D/F#)
Em7*  = 0 2 2 0 3 0 - (*Gm7 na forma de Em7)
G*  = 3 2 0 0 0 3 - (*A# na forma de G)
