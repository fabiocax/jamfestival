João Bosco - Casa de Marimbondo

    E7(#9)    A7(#11/13)       E7(#9)
Meu samba é casa        de marimbondo
       A7(13)         D7(9)          G7(13) Gb7 F7(#11)
Tem sempre   enxame pra    quem mexer
    E7(#9)        A7(13)  G7(13) C7
Não sabe com quem está falando
   A7(13)                B7(4/9)              Em7 A/E Em7 A/E
Nem quer saber, nem quer saber, nem quer saber

    E7           A7               D7          G7
Tem gente aí que acha que samba é contravenção
  C7           B7           F#7    B7
Eu saco bem o tipo e sou de opinião
      E7        A7              D7           G7
Que é nego acredita que sempre tá com a razão
                     C7M(9)            A7          B7    B7(#5)
Meu samba sempre diz:      Essa não! Essa não! Essa  não!

        E7(#9)    A7(#11/13)       E7(#9)  A7(13)       D7(9)    G7(13) Gb7 F7(#11)
Se o morro    fica          fazendo média e aceitando a situação
      E7(#9)          A7(13)      G7(13) C7   A7(13)        B7(4/9)          Em7 A/E Em7 A/E
Meu samba   chega e de      cara fei____a, dá decisão, dá decisão, dá decisão
  E7           A7               D7          G7
Tem gente aí que acha que samba é contravenção
  C7           B7           F#7    B7
Eu saco bem o tipo e sou de opinião
      E7        A7              D7           G7
Que é nego acredita que sempre tá com a razão
                     C7M(9)            A7          B7    B7(#5)
Meu samba sempre diz:      Essa não! Essa não! Essa  não!

        E7(#9)    A7(#11/13)       E7(#9)   A7(13)     D7(9)    G7(13) Gb7 F7(#11)
Se o morro    fica          fazendo média e aceitando a situação
      E7(#9)          A7(13)      G7(13) C7   A7(13)        B7(4/9)          Em7 A/E Em7 A/E
Meu samba   chega e de      cara fei____a, dá decisão, dá decisão, dá decisão

   A7(13)         B7(4/9)         Em7 A/E Em7 A/E
Dá decisão, dá decisão, dá decisão

----------------- Acordes -----------------
A/E = 0 X 2 2 2 0
A7 = X 0 2 0 2 0
A7(13) = X 0 X 0 2 2
B7 = X 2 1 2 0 2
B7(#5) = X 2 X 2 4 3
B7(4/9) = X 2 2 2 2 2
C7 = X 3 2 3 1 X
C7M(9) = X 3 2 4 3 X
D7 = X X 0 2 1 2
D7(9) = X 5 4 5 5 X
E7 = 0 2 2 1 3 0
E7(#9) = X 7 6 7 8 X
Em7 = 0 2 2 0 3 0
F#7 = 2 4 2 3 2 2
F7(#11) = 1 X 1 2 0 X
G7 = 3 5 3 4 3 3
G7(13) = 3 X 3 4 5 X
Gb7 = 2 4 2 3 2 2
