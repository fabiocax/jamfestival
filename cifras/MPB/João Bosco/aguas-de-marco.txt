João Bosco - Águas de Março

            C/Bb                              Am6
É pau, é pedra, é o fim do caminho
                   A/G                       C6/G
É um resto de toco,         é um pouco sozinho
                   Gb7(#11)                   Fmaj7
É um caco de vidro,         é a vida, é o sol
                     Fm6                         C6/9
É a noite, é a morte,       é o laço, é o anzol
                   Gm7(9)/C                   Gbm7(b5)
É peroba do campo,           é o nó da madeira
           Fm6                       C6/9
Caingá, candeia, é o matita pereira
                    Gm7(9)/C       C7           Gbm7(b5)
É madeira de vento,          tombo da ribanceira
                       Fm6                           C6/9
É o mistério profundo,      é o queira ou não queira
                   Gm7       C7           Gbm7(b5)
É o vento ventando,     é o fim da ladeira
                  Fm6                    C6/9
É a viga, é o vão,     festa da cumeeira
                   Gm7       C7           Gbm7(b5)
É a chuva chovendo,    é conversa ribeira
                   Fm6                     C6/9
Das águas de março,    é o fim da canseira
                 C/Bb                       Am6
É o pé, é o chão      é a marcha estradeira
                  A/G                     C6/9
Passarinho na mão,       pedra de atiradeira
               Gm7(9)/C                 Gbm7(b5)
Uma ave no céu,         uma ave no chão
                        Fm6                     C6/9
É um regato, é uma fonte,    É um pedaço de pão
               C7/4       C7          Gbm7(b5)
É o fundo do poço,  é o fim  do caminho
                    Fm6                    C6/9
No rosto o desgosto,    é um pouco sozinho
                        C/Bb                          Am6
É um estrepe, é um prego      É uma ponta, é um ponto
                    Fm6/Ab                         Cmaj7/G
É um pingo pingando,       É uma conta, é um conto
                       Gm7(9)/C                        Gbm7(b5)
É um peixe, é um gesto,          É uma prata brilhando
                  Fm6                     Cmaj7/G
É a luz da manhã,     é o tijolo chegando
        Cmaj7         C/Bb                   Am6
É a lenha,    é o dia       é o fim da picada
                    Fm6/Ab                       C6/9
É a garrafa de cana,       o estilhaço na estrada
                    Gm7      C7(9)       D/C
É o projeto da casa,     é o corpo na cama
                    Fm/C                    C6/G
É o carro enguiçado,     é a lama, é a lama
                  C7/4         C7/G          Am6/C
É um passo, é uma ponte, é um sapo, é uma rã
                   Fm6(9)/C
É um resto de mato          na luz da manhã
Cmaj7           C7/G                   Gbm7(b5)
São as águas de março fechando o verão
                  Fm(maj7)                 Fm6/C
É a promessa de vida        no teu coração

C6/9     C/Bb                      Am6
É pau, é pedra, é o fim do caminho
                     A/G                      Cmaj7/G
É um resto de toco,         é um pouco sozinho
                       Cm7                      D/C
É um passo, é uma ponte,    é um sapo, é uma rã
                   Fm6(9)/C                    C
É um belo horizonte,        é uma febre terçã

                Cm7                D/C
São as águas de março fechando o verão
                  Db/C
É a promessa de vida   no teu coração

( Gb/E  Eb/Db  C/Bb  Am6  A/G  Cmaj7/G  Gb7(#11)  Fmaj7  Fm6  C6  C/Bb  Am6  A/G C6/9 )

                    Gm7(9)/C                  Gbm7(b5)
É uma cobra, é um pau,         é João, é José
                    Fm6                   C6/9
É um espinho na mão,     é um corte no pé

                C/Bb                   Am6
São as águas de março fechando o verão
                A/G              C/G
É a promessa de vida no teu coração

         C/Bb                        Am6
É pau, é pedra,   é o fim do caminho
                   A/G                    C6/9
É um resto de toco,       é um pouco sozinho
                   Gm7     C7           Gbm7(b5)
É um caco de vidro,    é a vida, é o sol
                     Fm6                       Cmaj7/G
É a noite, é a morte,    é um laço, é um anzol

                Gm7     C7            Gbm7(b5)
São as águas de março fechando o verão
                  Fm6               C(add9)
É a promessa de vida no teu coração

( Gm7/C  D/C  Fm6/C  Cmaj7  Gm7(9)  D/C  Fm6/C  Cmaj7  Cm7  D/C  Db/C  C )

----------------- Acordes -----------------
A/G = 3 X 2 2 2 X
Am6 = 5 X 4 5 5 X
Am6/C = X 3 4 2 5 2
C = X 3 2 0 1 0
C(add9) = X 3 5 5 3 3
C/Bb = X 1 2 0 1 X
C/G = 3 3 2 X 1 X
C6 = 8 X 7 9 8 X
C6/9 = X 3 2 2 3 3
C6/G = 3 X 2 2 1 X
C7 = X 3 2 3 1 X
C7(9) = X 3 2 3 3 X
C7/4 = X 3 3 3 1 X
C7/G = 3 X 2 3 1 X
Cm7 = X 3 5 3 4 3
Cmaj7 = X 3 2 0 0 X
Cmaj7/G = 3 X 2 4 1 X
D/C = X 3 X 2 3 2
Db/C = X 3 3 1 2 1
Eb/Db = X 4 5 3 4 X
Fm(maj7) = X X 3 5 5 4
Fm/C = X 3 3 1 1 1
Fm6 = 1 X 0 1 1 X
Fm6(9)/C = X 3 3 1 P3 3
Fm6/Ab = 4 5 3 5 3 X
Fm6/C = X 3 3 1 3 X
Fmaj7 = 1 X 2 2 1 X
Gb/E = X X 2 3 2 2
Gb7(#11) = 2 X 2 3 1 X
Gbm7(b5) = 2 X 2 2 1 X
Gm7 = 3 X 3 3 3 X
Gm7(9) = X X 5 3 6 5
Gm7/C = X 3 3 3 3 3
