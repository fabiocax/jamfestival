João Bosco - Preta-porter de tafetá

João Bosco / Aldir Blanc

[Intro:] D7M(6)(9) A7(b13)(9)

D7M(6)(9)    G7(13) D7M(6)(9) C7(9)(13) C7(9)
Pagode em Cocotá vi a nega rebolá
B7(9) B7(9)(13) B7(b9) Em7(9) Em7M(9) Em7(9) Em7M(9)
Num preta-porter de tafetá
    G6        Ab°    D7M(6)(9) C7(9) B7(#9) B7(b9)
Beijei meu patuá, oi sambá, oi ulalá
       E7(9)       A7(9) A7(b9) D7M(6)(9) A7(b13)(9)
Mé carrefour o randevú vai começá

Além de me empurrá kes que sé tamanduá
Purquá jé suí du zanzibar
Aí eu me criei pas de bafo meu bombom
          E7(9)       A7(9) A7(b9) D7(4) D7 D/C
Pra que zangá sou primo do Villegagnón

   G7M  G7M(#5) G7M(6)
Voalá e çava patati patata
    A7(9) D7M(6)(9)   A7(b13)(9) D7M(6)(9)
Boulevar sarará sou da praça Mauá
 F#m7(b5)       B7(b9)
Dendê matinê padedê
                   E7(9)         A7(9) A7(b9)
Meu peticomitê bambolê encaçapo você

D7M(6)(9)      G6                  F#/E       Eb°
Taí seu Mitterrand marcamos pra manhã em Paquetá
                 Abm7(b5) Gm6
Num flamboyant en fleur
F#7(b13) B7(#9) E7(9) A7(9) C/D D7(9)
Onde eu vou ter colher
Abm7(b5) Gm6 F#7(b13) B7(#9) E7(9) A7(9) D7M(6)(9) A7(b13)(9)
Pompadú zulú manjei toá bocú

----------------- Acordes -----------------
A7(9) = 5 X 5 4 2 X
A7(b13)(9) = 5 X 5 4 6 X
A7(b9) = 5 X 5 3 2 X
Abm7(b5) = 4 X 4 4 3 X
Ab° = 4 X 3 4 3 X
B7(#9) = X 2 1 2 3 X
B7(9) = X 2 1 2 2 X
B7(9)(13) = 7 X 7 6 4 4
B7(b9) = X 2 1 2 1 X
C/D = X X 0 0 1 0
C7(9) = X 3 2 3 3 X
C7(9)(13) = X 3 2 P3 3 5
D/C = X 3 X 2 3 2
D7 = X X 0 2 1 2
D7(4) = X X 0 2 1 3
D7(9) = X 5 4 5 5 X
D7M(6)(9) = 2 2 2 6 3 5
E7(9) = X X 2 1 3 2
Eb° = X X 1 2 1 2
Em7(9) = X 7 5 7 7 X
Em7M(9) = 0 X X 0 4 2
F#/E = X X 2 3 2 2
F#7(b13) = 2 X 2 3 3 2
F#m7(b5) = 2 X 2 2 1 X
G6 = 3 X 2 4 3 X
G7(13) = 3 X 3 4 5 X
G7M = 3 X 4 4 3 X
G7M(#5) = 3 X 4 4 4 X
G7M(6) = 3 X 4 4 5 X
Gm6 = 3 X 2 3 3 X
