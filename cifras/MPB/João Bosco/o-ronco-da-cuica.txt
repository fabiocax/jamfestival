João Bosco - O Ronco da Cuíca

Dm7     G7(9) Dm7
Roncou, roncou
                   G7(9)            Dm7   
Roncou de raiva a cuíca, roncou de fome...   (BIS)
        G7(9) Dm7 
Alguém man__dou
                  G7(9)
Mandou parar a cuíca
             Dm7  
É coisa dos home
                 G7(9)          Dm7
A raiva dá pra parar, pra interromper
           G7(9)        Dm7
A fome não dá pra interromper
             G7(9)            Dm7  
A raiva e a fome é coisa dos home
                    G7(9)         Dm7
A fome tem que ter raiva pra interromper
             G7(9)       Dm7
A raiva é a fome de interromper
            G7(9)             Dm7  
A fome e a raiva é coisa dos home
            
É coisa dos home
            
É coisa dos home
            
A raiva e a fome
           
Mexendo a cuíca
           
Vai ter que roncar

----------------- Acordes -----------------
Dm7 = X 5 7 5 6 5
G7(9) = 3 X 3 2 0 X
