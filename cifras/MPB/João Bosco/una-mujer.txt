João Bosco - Una Mujer

Paúl Misraki / Pondal Ríos / Olivar

     D/F#                  F6(13)
La mujer que al amor no se asoma
   A/E               F/A
No merece llamarse mujer
         Bm7                     Cº
Es cual flor que no esparce su aroma
        C#m7             F#7(b13)
Como un leño que no sabe arder

      D/F#                 F6(13)
La pasión tiene un mágico idioma
        A/E                F#7
Que con besos se debe aprender
          Bm7    Dm7        C#m7    F#7
Puesto que una mujer que no sabe querer
     B7               E7 E7(b9)
No merece llamarse mujer

    A7M F#m7  Bm7 Cº   C#m7              F#7(b13)
Una mujer debe ser soñadora, coqueta y ardiente
     Bm7      Dm7         C#m7     F#7
Debe darse al amor con frenético ardor
     Bm7 E7(b9) A7M
Para ser una mujer

----------------- Acordes -----------------
A/E = 0 X 2 2 2 0
A7M = X 0 2 1 2 0
B7 = X 2 1 2 0 2
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
Cº = X 3 4 2 4 2
D/F# = 2 X 0 2 3 2
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
E7(b9) = X X 2 1 3 1
F#7 = 2 4 2 3 2 2
F#7(b13) = 2 X 2 3 3 2
F#m7 = 2 X 2 2 2 X
F/A = 5 X 3 5 6 X
