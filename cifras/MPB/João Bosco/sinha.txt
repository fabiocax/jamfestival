João Bosco - Sinhá

[Intro] Cm  Fm6  G/B  Cm  Fm6  
        G/B  Fm7/C  Cm  Fm6  G/B  
        Cm  Cm/A  C#  G7(13-)  Cm  
        Gm/Bb  D7/A  G#6(5-)  G7  Cm

                 Cm    Fm6       Cm
Se a dona se banhou Eu não estava lá
    D7/A           Gm7         D7/A    G/B
Por Deus, nosso Senhor Eu não olhei, Sinhá
               Cm  Fm6              Cm
Estava lá na roça Sou de olhar ninguém
    D7/A        Gm/Bb  D7/A        G/B
Não tenho mais cobiça Nem enxergo bem
                  G#6(5-)          G7    Cm
Pra quê me por no tronco Pra quê me aleijar
              Gm7(5-)        C    Fm6
Eu juro a vosmecê que nunca vi Sinhá
                   Fm7           Bb7   Cm/Eb
Por que me faz tão mal Com olhos tão azuis
   Cm            C#6       G/B  Cm
Me benzo com o sinal da Santa Cruz

Gm/Bb  D7/A  G#6(5-)  G7  Cm

                   Cm    Fm6       Cm
Eu só cheguei no açude Atrás da sabiá
 D7/A         Gm7    D7/A        G/B
Olhava o arvoredo Eu não olhei Sinhá
                 Cm   Fm6          Cm
Se a dona se despiu Eu já andava além
  D7/A     Gm/Bb   D7/A        G/B
Estava na moenda Estava para Xerém
                   G#6(5-)         G7    Cm
Por que talhar meu corpo Eu não olhei Sinhá
               Gm7(5-)          C    Fm6
Pra quê que vosmecê meus olhos vai furar
                Fm7    Bb7      Cm/Eb
Eu choro em iorubá,Mas oro por Jesus
    Cm           C#6   G7(13-)  C
Pra quê que vosmecê me tira a luz
                  Em/B   Am7            Dm4
E assim vai se encerrar O canto de um cantor
               G/B
Com voz no pelourinho
F#dim        Cdim
E ares de senhor
             C7M     B7(13)       E7
Cantor atormentado Herdeiro sarará
   A7        Dm7
Do nome do renome
      B(b9)/Eb          Em7
De um feroz senhor de engenho
        Fm6            Cm
E das mandingas de um escravo
       Fm7(13)/C     C#7M(9)
Que no engenho enfeitiçou
G7  Cm
Sinhá

Gm/Bb  D7/A  G#6(5-)  G7  Cm
Iê iê
Iê iê re

Intro

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am7 = X 0 2 0 1 0
B7(13) = X 2 X 2 4 4
Bb7 = X 1 3 1 3 1
C = X 3 2 0 1 0
C# = X 4 6 6 6 4
C#6 = 9 X 8 10 9 X
C#7M(9) = X 4 3 5 4 X
C7M = X 3 2 0 0 X
Cdim = X 3 4 2 4 2
Cm = X 3 5 5 4 3
Cm/A = X 0 5 5 4 3
Cm/Eb = X X 1 0 1 3
D7/A = 5 X 4 5 3 X
Dm4 = X 5 5 7 6 5
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
Em/B = X 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F#dim = 2 X 1 2 1 X
Fm6 = 1 X 0 1 1 X
Fm7 = 1 X 1 1 1 X
Fm7/C = X 3 1 1 1 1
G/B = X 2 0 0 3 3
G7 = 3 5 3 4 3 3
G7(13-) = 3 X 3 4 4 3
Gm/Bb = 6 5 5 3 X X
Gm7 = 3 X 3 3 3 X
Gm7(5-) = 3 X 3 3 2 X
