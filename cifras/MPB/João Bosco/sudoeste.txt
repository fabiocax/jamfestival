João Bosco - Sudoeste


Intro: (A7/13 G#7/13 G7/13) 
(D7/9 C#7/9 C7/9) B7/9 E7/9+

Am7
O vento deu no matagal e deu na cara dos cajás
No coqueiral, nas borboletas, azulão, nos curiós
Jaca manteiga, jaca mãe, no bambuzal, nas teias
             D7/9                                G7
Tristes do porão, caramanchão, capim navalha coração
                          C7/9
Canavial, acorde um acordeão lá do matão
                         F7                    Bm5-/7
Menina moça gavião, aluvião, anunciando anunciação
                                    E7/9+
E o garnisé cortando os pulsos da manhã nesse quintal
E o vento deu na manga
           A7            D7/9                      G7
E o vento deu no mangueiral, na roupa branca do varal
       C7/9                    Bm5-/7         E7
No lodaçal, e deu na cara da ilusão, no temporal
                        A7/13 G#7/13 G7/13 G#7/13
Deu nas pegadas desse chão
         A7         D7/9                     G7
Eo vento deu no natural, deu no início de natal
          C7/9                 Bm5-/7
Deu no olhar e deu na cara do luar, deu na paixão
                        E7/9+
De um galo doido no quintal

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7/13 = X 0 X 0 2 2
Am7 = X 0 2 0 1 0
B7/9 = X 2 1 2 2 X
Bm5-/7 = X 2 3 2 3 X
C#7/9 = X 4 3 4 4 X
C7/9 = X 3 2 3 3 X
D7/9 = X 5 4 5 5 X
E7 = 0 2 2 1 3 0
E7/9+ = X 6 5 6 7 X
F7 = 1 3 1 2 1 1
G#7/13 = 4 X 4 5 6 X
G7 = 3 5 3 4 3 3
G7/13 = 3 X 3 4 5 X
