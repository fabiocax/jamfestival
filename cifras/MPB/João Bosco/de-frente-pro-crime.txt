João Bosco - De Frente pro Crime

Intro: 

           Am11         G7(13-)         F#7         E4(7/9)         Eb7         Eb11+
E|----------------------------------------------------7---7-7-7------7-9------10-----|
B|--------7-7-7-5-7-8-7-8/10-10-10-10-7-10---8---------10-------10----------|
G|-----7---------------------------------------------------------------------------------|
D|----------------------------------------------------------------------------------------|
A|----------------------------------------------------------------------------------------|
E|----------------------------------------------------------------------------------------|

   D9            Gm6      D9
Tá lá o corpo estendido no chão
   D/F#                Gm         Bm7
Em vez de um rosto uma foto de um gol
   Bm5+                        Bm7   Bm7/A
Em vez de reza uma praga de alguém
  G6            F#m7       A9/E
E um silêncio servindo de amém


    D9            Gm6      D9
O bar mais perto depressa lotou
   D/F#            Gm         Bm7
Malandro junto com trabalhador
   Bm5+                   Bm7   Bm7/A
Um homem subiu na mesa do bar
  G6            A7(9)    D9
E fez discurso pra vereador


               G#7/5-     G9   A7(9)             D9
Veio camelô vender      anel, cordão, perfume barato
                D/C      Bm5+     A/G             D6/F#
E a baiana prá fazer pastel e um bom churrasco de gato
                    D/C  Bm5+  A/G             F#7(13)
Quatro horas da manhã baixou o santo na porta-bandeira
      F#7(13-)  B7(9) B7(9-)  Bm5+  A7(9)
E a moçada resolveu        parar, e então...


     D9            Gm6      D9
Tá lá o corpo estendido no chão
   D6/F#                Gm         Bm7
Em vez de um rosto uma foto de um gol
   Bm5+                        Bm7   Bm7/A
Em vez de reza uma praga de alguém
  G6            F#m7       A9/E
E um silêncio servindo de amém


    D9            Gm6       D9
Sem pressa foi cada um pro seu lado
   D/F#                Gm        Bm7
Pensando numa mulher ou num time
   Bm5+                        Bm7   Bm7/A
Olhei o corpo no chão e fechei
  G6            A7(9)    D9
Minha janela de frente pro crime

----------------- Acordes -----------------
A/G = 3 X 2 2 2 X
A7(9) = 5 X 5 4 2 X
A9/E = 0 X 2 2 0 0
Am11 = X 0 0 2 1 0
B7(9) = X 2 1 2 2 X
B7(9-) = X 2 1 2 1 X
Bm5+ = 7 10 9 7 8 7
Bm7 = X 2 4 2 3 2
Bm7/A = 5 2 4 2 3 2
D/C = X 3 X 2 3 2
D/F# = 2 X 0 2 3 2
D6/F# = 2 X 0 2 0 X
D9 = X X 0 2 3 0
E4(7/9) = X X 2 2 3 2
Eb11+ = X 6 7 8 8 6
Eb7 = X 6 5 6 4 X
F#7 = 2 4 2 3 2 2
F#7(13) = 2 X 2 3 4 X
F#7(13-) = 2 X 2 3 3 2
F#m7 = 2 X 2 2 2 X
G#7/5- = 4 X 4 5 3 X
G6 = 3 X 2 4 3 X
G7(13-) = 3 X 3 4 4 3
G9 = 3 X 0 2 0 X
Gm = 3 5 5 3 3 3
Gm6 = 3 X 2 3 3 X
