João Bosco - Fantasia

 

 Am7           F7+                Am7 D7/9
Olhando na quarta-feira as ruas vazias
        Dm7/9           F/G            C7+ F7+
Com os garis dando um jeito em nossa moral
 Bm7          E7/9-          Am  Am/G
Custei a compreender que fantasia
       F#º             F7        E7    E7/9-
É um troço que o cara tira no carnaval
   Am7            F7+             Am7  D7/9
E usa nos outros dias por toda a vida
Dm7/9               F/G              C7+ F7+
Dizendo: "Olá! Como vai?" e coisas assim
 Bm7               E7/9-       Am   Am/G
O nó da gravata apertando o pescoço
 F#º                F7              E7
Olhando o fundo do poço e rindo de mim
Am      Am/G     F#m5-/7  F7+
Ria, rasguei a fantasia, ria
             Am/E
Queimei a garantia, ria
  C7         F7+
Tô solto por aí
E7/9-  Am  Am/G     F#m5-/7   F7+
Doido, eu danço de Pierrot, triste
                Am/E
Morrendo em meu amor, ria
 C7         B6/7     Bb6/7 A
Vendo você morrer de rir

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Am/E = 0 X 2 2 1 0
Am/G = 3 X 2 2 1 X
Am7 = X 0 2 0 1 0
B6/7 = X 2 X 2 4 4
Bb6/7 = 6 X 6 7 8 X
Bm7 = X 2 4 2 3 2
C7 = X 3 2 3 1 X
C7+ = X 3 2 0 0 X
D7/9 = X 5 4 5 5 X
Dm7/9 = X 5 3 5 5 X
E7 = 0 2 2 1 3 0
E7/9- = X X 2 1 3 1
F#m5-/7 = 2 X 2 2 1 X
F#º = 2 X 1 2 1 X
F/G = 3 X 3 2 1 X
F7 = 1 3 1 2 1 1
F7+ = 1 X 2 2 1 X
