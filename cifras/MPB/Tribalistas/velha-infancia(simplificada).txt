﻿Tribalistas - Velha Infância

Capo Casa 2

Em
  Você é assim
Am
  Um sonho pra mim
D                      Em
  E quando eu não te vejo

Eu penso em você
Am
  Desde o amanhecer
D                    Em
  Até quando eu me deito

          Am
Eu gosto de você
D           Em
  E gosto de ficar com você
            Am
Meu riso é tão feliz contigo
D                 Em
  O meu melhor amigo é o meu amor


E a gente canta
Am
  E a gente dança
D                    Em
  E a gente não se cansa

De ser criança
Am
  Da gente  brincar
D                    Em
  Da nossa velha infância

            Am
Seus olhos meu clarão
D            Em
  Me guiam dentro da escuridão
            Am
Seus pés me abrem o caminho
D             Em
  Eu sigo e nunca me sinto só

Você é assim
Am
  Um sonho pra mim
D                      Em
  Quero te encher de beijos

Eu penso em você
Am
  Desde o amanhecer
D                   Em
  Até quando eu me deito

          Am
Eu gosto de você
D           Em
  E gosto de ficar com você
            Am
Meu riso é tão feliz contigo
D                 Em
  O meu melhor amigo é o meu amor

A gente canta
Am
  A gente dança
D                  Em
  A gente não se cansa

De ser criança
Am
  Da gente  brincar
D                    Em
  Da nossa velha infância

            Am
Seus olhos meu clarão
D            Em
  Me guiam dentro da escuridão
            Am
Seus pés me abrem o caminho
D             Em
  Eu sigo e nunca me sinto só

Você é assim
Am                 D           Em
  Um sonho pra mim, você é assim

----------------- Acordes -----------------
Capotraste na 2ª casa
Am*  = X 0 2 2 1 0 - (*Bm na forma de Am)
D*  = X X 0 2 3 2 - (*E na forma de D)
Em*  = 0 2 2 0 0 0 - (*F#m na forma de Em)
