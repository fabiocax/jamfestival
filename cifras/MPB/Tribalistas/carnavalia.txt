Tribalistas - Carnavália

[Intro]  Bb  Fm

 Bb
Vem pra minha ala
                    Fm
Que hoje a nossa escola vai desfilar
 Bb
Vem fazer história
                    Fm
Que hoje é dia de glória nesse lugar

 Cm
Vem comemorar
                  G#
Escandalizar ninguém
Vem me namorar
                   Bb
Vou te namorar também
Vamos pra avenida
            Fm
Desfilar a vida

Carnavalizar

Cm
A Portela tem Mocidade
        G#
Imperatriz
No Império tem
                Cm
Uma Vila tão feliz
Beija-Flor vem ver
A porta-bandeira
 G#                            Bb
Na Mangueira tem morena da Tradição

                           Fm
Sinto a batucada se aproximar
Bb                        Fm
Estou ensaiado para te tocar

 Cm
Repique tocou
             G#
O surdo escutou
E o meu corasamborim
 Cm
Samborim
Cuíca gemeu
             G#
Será que era eu
Quando ela passou por mim

 Bb                Fm
Lá lá lá lá lá lá lá lá lá lá

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
Fm = 1 3 3 1 1 1
G# = 4 3 1 1 1 4
