Tribalistas - Feliz e Saudável

[Intro]  G  C  G  C

G           C
Eu vou sumir,vou desaparecer
G               C
Vou me encontrar,você vai me perder
G          C
Eu vou me desapaixonar de vez
G          C                 B7
Mas se você me procurar talvez
          E7
Eu posso me arrepender
A7       D7               G
E até topar namorar com você
          C            G  C
Pois sou feliz e saudável
 G       C        G    F#7
Muito feliz e saudável

Bm       Bm7+               Bm7
Às vezes acho que chegou ao fim

          Bm6               Em
Que você nunca vai gostar de mim
         A7  Em        A7
Às vezes sim,às vezes não
Bm       Bm7+               Bm7
Já perguntei para o meu coração
          Bm6               Em
Mas ele só aumenta a confusão
          A7  Em        A7
Às vezes não,às vezes sim

G           C
Pode ligar que eu não vou atender
G             C
Vou me vingar,vou te fazer sofrer
G           C
Você vai ver,eu vou te enlouquecer
G             C            B7
Te maltratar pra você aprender
          E7
Mas posso me arrepender
A7       D7              G
E até topar namorar com você
           C             G    C
Pois sou feliz e saudável
G       C            G
Super feliz e saudável

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
Bm6 = 7 X 6 7 7 X
Bm7 = X 2 4 2 3 2
Bm7+ = X 2 4 3 3 2
C = X 3 2 0 1 0
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F#7 = 2 4 2 3 2 2
G = 3 2 0 0 0 3
