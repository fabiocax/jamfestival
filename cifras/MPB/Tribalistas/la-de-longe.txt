Tribalistas - Lá de Longe

[Intro] A

A                                                        D6
Longe, lá de longe, de onde toda a beleza do mundo se esconde
                       Dm6                                   A
Mande, para ontem, uma voz que se expanda e suspenda esse instante
                                                 D6
Lá de longe, de onde toda a beleza do mundo se esconde
                       Dm6                                   A
Mande, para ontem, uma voz que se expanda e suspenda esse instante
                                                 D6
Lá de longe, de onde toda a beleza do mundo se esconde
                   Dm6
Cante, para hoje

[Solo] A

A                                                        D6
Longe, lá de longe, de onde toda a beleza do mundo se esconde
                       Dm6                                   A
Mande, para ontem, uma voz que se expanda e suspenda esse instante

                                                 D6
Lá de longe, de onde toda a beleza do mundo se esconde
                       Dm6                                   A
Mande, para ontem, uma voz que se expanda e suspenda esse instante

Lá de longe, lá de longe, lá de longe, lá de longe

----------------- Acordes -----------------
A = X 0 2 2 2 0
D6 = X 5 4 2 0 X
Dm6 = X 5 X 4 6 5
