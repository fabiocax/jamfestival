Tribalistas - Trabalivre

    Dm               G  Dm
Um dia minha mãe me disse
            G    Dm            G  Dm
Você já é grande,tem que trabalhar
    Dm              G   Dm
Naquele instante aproveitei a chance
     G   Dm         G   Dm
Vi que eu era livre para me virar
           A     G              Dm
Fiz minha mala, comprei a passagem
         A      G                  Dm
O tempo passou depressa e eu aqui cheguei
            A       G            Dm
Passei por tudo que é dificuldade
               G                     A
Me perdi pela cidade mas já me encontrei

   Dm    G   Dm
Domingo boto meu pijama
       G      Dm        G  Dm
Deito lá na cama para não cansar

         Dm       G    Dm
Segunda-feira eu já tô de novo
    G    Dm          G   Dm
Atolado de trabalho para entregar
    A         G         Dm
Na terça não tem brincadeira
        A     G           Dm
Quarta-feira tem serviço para terminar
     A        G           Dm
Na quinta já tem hora extra
      G                         A
E na sexta o expediente termina no bar
             Dm                 G  Dm
Mas tenho o sábado inteiro pra mim mesmo
 G   Dm
Fora do emprego
  G   Dm
Pra me aprimorar

    Dm           G  Dm
Sou easy, eu não entro em crise
       G    Dm
Tenho tempo livre
     G  Dm
Pra me trabalhar

----------------- Acordes -----------------
