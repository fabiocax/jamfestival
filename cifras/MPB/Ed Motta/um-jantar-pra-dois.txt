Ed Motta - Um Jantar Pra Dois

Intro: F#m7   D7M   G#m7(b5)   C#7sus4

F#m7                         D7M        G#m7(b5)    C#7sus4
Eu que sou, eu que fui, eu que jurei não mais sofrer
F#m7                         D7M        G#m7(b5)    C#7sus4
Eu que sou, eu que fui, eu que jurei não mais sofrer
D7M         C#7sus4      D7M         G#m7(b5)    C#7sus4          F#m7
Mas será que somente eu errei, não vou mais procurar    você       Oh, não

( D7M   G#m7(b5)   C#7sus4 )

F#m7                          D7M       G#m7(b5)    C#7sus4
Tolo eu fui em ser bom, sempre bom, esperando    não sei bem o que  não sei
F#m7                          D7M        G#m7(b5)   C#7sus4
Tolo eu fui em ser bom, sempre bom, esperando    não sei bem o que, não sei
D7M           C#7sus4     D7M                 G#m7(b5)   C#7sus4   F#m7
Mas se eu não aguentar,   o motivo estar, eu vou levar um jantar pra dois

( D7M   G#m7(b5)   C#7sus4 )

F#m7                 D7M     G#m7(b5)       C#7sus4
Até um dia, se você me procurar   talvez eu posso estar
F#m7                 D7M     G#m7(b5)       C#7sus4
Até um dia, se você me procurar   talvez eu posso estar

----------------- Acordes -----------------
C#7sus4 = X 4 6 4 7 4
D7M = X X 0 2 2 2
F#m7 = 2 X 2 2 2 X
G#m7(b5) = 4 X 4 4 3 X
