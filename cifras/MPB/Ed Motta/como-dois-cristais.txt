Ed Motta - Como Dois Cristais

Am7      G7             C7+
Eu já sonhei alguém pra mim
  Bm7/5-       E7/9-
E vi de       cara
Am7         G7           C7+    E7
Que esse alguém parece demais você
Am7            Gm7   C7/9   F7+
O som do seu nome lembra o mar
           Bm7/5- E7
Batendo no cais
Am7         Gm7    C7/9  F7+
Exposta na imagem film-noir
      E7/9+ E7/9-    Am7
Que se mostra e me atrai
      G7                C7+ Bm7/5- E7/9+
Me atraiu o dom  de sorrir ficando séria
Am7   G7             C7+   E7
De atrair a luz e de ser feliz
Am7          Gm7   C7/9   F7+
Os olhos nos olhos eu pensei
                E7
Conheço esse olhar
Am7         Gm7    C7/9 F7+
Enquanto me olha fico high
      E7/9+        Am7
E os sonhos são reais
F/G                  Am7 (A9 na última vez - são 4 vezes)
Olhos como dois cristais
Am7     G7            C7+
Não esperei a cena acabar
    E7 E7/9+ E7/9-
Pra ir embora
Am7     G7         C7+   E7
Fora de lá podia tocar você
Am7        Gm7 C7/9  F7+
É que o cinema era ali
          E7
Na vida real
Am7         Gm7 C7/9  F7+
Exposta na imagem film-noir
       E7/9+ E7/9-    Am7
Que se mostra e me atrai
F/G                   Am7 (A9)
Olhos como dois cristais

----------------- Acordes -----------------
A9 = X 0 2 2 0 0
Am7 = X 0 2 0 1 0
Bm7/5- = X 2 3 2 3 X
C7+ = X 3 2 0 0 X
C7/9 = X 3 2 3 3 X
E7 = 0 2 2 1 3 0
E7/9+ = X 6 5 6 7 X
E7/9- = X X 2 1 3 1
F/G = 3 X 3 2 1 X
F7+ = 1 X 2 2 1 X
G7 = 3 5 3 4 3 3
Gm7 = 3 X 3 3 3 X
