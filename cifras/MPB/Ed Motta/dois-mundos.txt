Ed Motta - Dois Mundos

Bb                C       F
Tenha fé em tudo que acredita
Bb              C      F
Dois mundos distintos são
Bb              F
Deixe o seu destino agir
   Dm      F  Bb       C9
Guiar seu coração

Fm                C#
Um paraíso sem igual
Fm                    Ab
Num mundo cheio de amor
         C#       Bb    F
Viver assim traz tanta paz

Bb                   C    F
As pegadas sobre a areia fina
Bb              C      F
Dois mundos distintos são
Bb              F
Deixe o seu destino agir
      Dm             Bb
Guiar seu coração

Fm                                C#     Eb
Por sobre as árvores viver
Fm                                Ab
A família ver crescer
               C#        Bb
Viver assim traz tanta paz

Eb                 Ab      Bb
Ter coragem. recomeçar
Eb                Ab      Bb
Com toda sua força
Eb                   Ab   Bb
Reconstruir um novo lar
    Eb              Ab                F
Na vida que segue perigos vai encontrar

Fm                              C#
Palavras não curam a dor
Fm                                   Ab
De um coração que se partiu
             C#     F     Bb
Mas não é o fim só basta crer

F                C         Bb
Alguém agora está chamando
F                      Bb      C
Dois mundos distintos são

Bb                   F
Deixe o seu destino agir
   Dm             Bb
Guiar seu coração

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C# = X 4 6 6 6 4
C9 = X 3 5 5 3 3
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
