Ed Motta - Garota de Ipanema

[Intro] Am7/4   G#7(13)   C#7M(9)  F#7(13)      
        Am7/4   G#7(13)   C#7M(9)  F#7(13)   
        Am7/4   G#7(13)   C#7M(9)  F#7(13)   
        Am7/4   G#7(13)   C#7M(9)  F#7(13)                       

F7M(9)        D7/4                                   
Olha que coisa mais linda
         G7(13)                                        
Mais cheia de graça
G#°(b13)              Gm7(11)                    
É ela menina que vem e que passa
       C7(9-)                                        
Num doce balanço,
 C/A#     Am7/4   G#7(13)  C#7M(9)  F#7(13)  F#7                   
A caminho do mar

F7M(9)        D7/4                                  
Moça do corpo dourado
         G7(13)                                       
Do sol de Ipanema
   G#°(b13)             Gm7(11)              
O seu balançado é mais que um poema
         C7(9-)                                         
É a coisa mais linda
F#7(11+)  F7M(9)                                         
Que eu já vi passar

F#7M                     B7(9)                       
Ah, por que estou tão sozinho?
F#m7                     D7(9)                        
Ah, por que tudo é tão triste?
Gm7                Eb7(9)                                
Ah, a beleza que existe
     Am7               D7(9-)                         
A beleza que não é só minha
 Gm7                C7(9-)                           
Que também passa sozinha

F7M(9)         D7/4                                     
Ah, se ela soubesse
          G7(13)                                     
Que quando ela passa
G#°(b13)                Gm7(9)                   
O mundo inteirinho se enche de graça
        C7(9-)  F#7(11+)                                     
E fica mais lindo
      F7M(9)(13)                                      
Por causa do amor

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Am7/4 = 5 X 5 5 3 X
B7(9) = X 2 1 2 2 X
C#7M(9) = X 4 3 5 4 X
C/A# = X 1 2 0 1 X
C7(9-) = X 3 2 3 2 X
D7(9) = X 5 4 5 5 X
D7(9-) = X 5 4 5 4 X
D7/4 = X X 0 2 1 3
Eb7(9) = X 6 5 6 6 X
F#7 = 2 4 2 3 2 2
F#7(11+) = 2 X 2 3 1 X
F#7(13) = 2 X 2 3 4 X
F#7M = 2 X 3 3 2 X
F#m7 = 2 X 2 2 2 X
F7M(9) = X 8 7 9 8 X
F7M(9)(13) = 5 5 5 9 6 8
G#7(13) = 4 X 4 5 6 X
G#°(b13) = 4 X 3 4 5 X
G7(13) = 3 X 3 4 5 X
Gm7 = 3 X 3 3 3 X
Gm7(11) = 3 X 3 3 1 X
Gm7(9) = X X 5 3 6 5
