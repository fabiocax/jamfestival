Ed Motta - Manuel

Intro: Cm7 F7 G7

   Cm7  F7    G7         Cm7 F7 G7             Cm7 F7 G7        Cm7 F7 G7
Manuel ......    Foi pro céu ..............Manuel.......Foi pro céu


Cm7
Ia pro trabalho cansado, às 6 da manhã, Ouvia no seu rádio, calcinhas e sutiãs
    Cm7                  F7         G7       Cm7                    F7        G7
No rádio era um funk, o trem tava lotado, pensou no seu salário, ficou desanimado
      Cm        Cm7         F/A        G#    F7   G7
Se eu fosse americano minha vida não seria assim

REFRÃO

Cm7
Dia após dia, ouvia a sua vó lhe falar, o mundo é fabuloso, o ser humano é que não é legal
   Cm7                   F7         G7      Cm7                    F7         G7
No rádio era um funk, o trem tava lotado, pensou no seu salário, ficou desanimado
       Cm        Cm7           F/A        G#    F7        G7
Se eu fosse um político, minha vida não seria assim , não,não, não, não

REPETE PARTE 2

----------------- Acordes -----------------
Cm = X 3 5 5 4 3
Cm7 = X 3 5 3 4 3
F/A = 5 X 3 5 6 X
F7 = 1 3 1 2 1 1
G# = 4 3 1 1 1 4
G7 = 3 5 3 4 3 3
