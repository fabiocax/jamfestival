Ed Motta - Mentiras Fáceis

Intro 8x: Gm9  Dm9  Fm9  Cm9 

Gm9 Dm9               Fm9 Cm9
O discurso, até que é bom
Gm9    Dm9           Fm9 Cm9
Causa impacto e sensação
Gm9      Dm9        Fm9 Cm9
Morde e sopra o coração
Gm9 Ab13          G13   Db9(#11)
Me enlouquece no final, mal

Dm11            G7(#5)
Nem bem, vira a esquina
                     Cm7M(9)
E só decide o que te convém, não mais
Fm11        E13
Nem bem se despede e já esquece o que ficou
Eb7M(9) A7(b5) Ab7M(9) D7(#9) (Gm9 Dm9 Fm9 Cm9) (4x)
Men-----tiras fáceis, fa------tais, demais

Dm11            G7(#5)
Nem bem, vira a esquina
                     Cm7M(9)
E só decide o que te convém, não mais
Fm11        E13
Nem bem se despede e já esquece o que ficou
Eb7M(9) A7(b5) Ab7M(9) D7(#9)
Men-----tiras são fa---tais
Eb7M(9) A7(b5) Ab7M(9) D7(#9)
Des-----cidas trági----cas
Eb7M(9) A7(b5) Ab7M(9) D7(#9) G7M(9)
Vi------vendo em doses mor----tais uhhhh...

----------------- Acordes -----------------
A7(b5) = X 0 1 0 2 X
Ab13 = 4 X 3 5 4 X
Ab7M(9) = 4 3 5 3 X X
Cm7M(9) = X 3 1 4 3 X
Cm9 = X 3 1 0 3 X
D7(#9) = X 5 4 5 6 X
Db9(#11) = X 4 5 6 4 4
Dm11 = X 5 5 7 6 5
Dm9 = X 5 7 9 6 5
E13 = X X 2 4 2 4
Eb7M(9) = X 6 5 7 6 X
Fm11 = 1 1 3 1 1 1
Fm9 = 1 3 5 1 1 1
G13 = 3 X 2 4 3 X
G7(#5) = 3 X 3 4 4 3
G7M(9) = 3 2 4 2 X X
Gm9 = 3 5 7 3 3 3
