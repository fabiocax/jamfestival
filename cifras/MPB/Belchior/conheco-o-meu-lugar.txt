Belchior - Conheço o Meu Lugar

[Intro] C  D  C  D
        C  D  E

E|-------------------------------------------------------------------------------|
B|-------------------------------------------------------------------------------|
G|--------------4-2-0-----------------4-2-0--------------------4-2-0-------------|
D|------0-2-0----------------0-2-0--------------------0-2-0------------0-2-------|
A|--3------------------2-3-----------------------2-3-----------------------------|
E|-------------------------------------------------------------------------------|

    G                              D
O que é que pode fazer, o homem comum
                                    Em
Neste presente instante, senão sangrar
                             A
Tentar inaugurar a vida comovida
                            D4    D  D9
Inteiramente livre, e triunfante?
      G                            D
O que é que eu posso fazer, com a minha
                      Em
Juventude - quando a máxima saúde hoje

         A           D4  D  D9
É pretender, usar a voz?
      G
O que é que eu posso fazer - um simples
      D
Cantador das coisas do porão? Deus fez
            Em
Os cães da rua pra morder vocês, que sob a
        A
Luz da lua, os tratam como gente é
                   D4  D  D9
Claro! - aos pontapés
         Em
Era uma vez um homem e seu tempo
          A
Botas de sangue nas roupas de Lorca
          Em
Olho de frente a cara do presente e sei
           A
Que vou ouvir a mesma história porca
     E                        A
Não há motivo para festa: ora esta! Eu

Não sei rir a toa!
         Em
Fique você com a mente positiva, eu
              A
Quero a voz ativa (ela é que é uma boa!)
                 Em
Pois sou uma pessoa
                A
Esta é minha canoa: eu nela embarco
           Em
Eu sou pessoa!
                                 A
A palavra "pessoa" hoje não soa bem
Pouco me importa!
 G                    D             G
Não! Você não me impediu, de ser feliz!
                      D              Em
Nunca jamais bateu a porta em meu nariz!
Ninguém é gente!
    A
Nordeste é uma ficção! Nordeste nunca
 D4    D  D9
Houve!
 G                    D            G
Não! Eu não sou do lugar dos esquecidos!
              D            Em
Não sou da nação dos condenados!
                           A
Não sou do sertão dos ofendidos!
Você sabe bem
    D            G    D      C
Conheço o meu luga___________ar!

----------------- Acordes -----------------
A = X 0 2 2 2 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D4 = X X 0 2 3 3
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
