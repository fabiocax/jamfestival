Belchior - Como Se Fosse Pecado

Int.: A D/A G/A D/A A D/A A G/A

A               D/A               A  D/A
É claro que eu quero o clarão da lua
A               D/A                A  D/A
É claro que eu quero o branco no preto
   A        D/A          A           D/A
Preciso, precisamos da verdade nua e crua
A             D/A             A
Mas não vou remendar nosso soneto
                                 D/E       A
(Batuco um canto concreto pra balançar o coreto)
                                      D
Por enquanto o nosso canto é entre quatro paredes
      A                      D
Como se fosse pecado, como se fosse mortal
           A                    D
(Segredo humano pro fundo das redes
            A                        D
Tecendo a hora em que a aurora for geral)
       A                 D
Por enquanto estou crucificado e varado

       A                      D
Pela lança que não cansa de ferir
           A                     D
Mas neste bar no Oeste Nordeste Sul, falo cifrado:
           A                         E7     Bm7 E7
"Hello, bandidos! (Bang!) É hora de fugir!"
               Bm7            E7                  A   A6 A7+ A6
Mas quando o canto for tão natural como o ato de amar
     Bm7                    E7               F#m
Como andar, respirar, dar a vez à voz dos sentidos
        Bm7                 E7
Virgem Maria, dama do meu cabaré
     A7+ D7+ A/C# F#7/9- Bm7   E7             F#m
Quero gozar toda a noite sobre tus pechos dormidos
       Bm7                     E7                F#m
Romã, romã quem dançar, quem deixar a mocidade louca
               Bm7                      E7
Mas daquela lourucra que aventura, a estrada
                  A       A/G     D/F#
E a estrela do amanhã e aquela felicidade - arma quente
                    F            A/E        A
(Quem haverá que aguente tanta nudez sem perder a saúde?)
                 C#m7    Bm7         C#7/9     F#5+/7   B7/9
A palavra era um dom, era bom, era conosco... Era uma vez
   D/F#                               F
Felicidade - arma quente (Com coisa quente é que eu brinco:
       A/E                         A   D
Take it easy, my brother Charles, Anjo 45)...
   C#m7 (A)            Bm7 (F#m)
Tá qualquer coisa meu irmão
         C#m7 (A)    Bm7 (F#m)
Mas use o berro e o coração
         C#7/13 C7/13 B7/13 Bb7/13 A
Que a vida vem no fim do mês

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A/E = 0 X 2 2 2 0
A/G = 3 X 2 2 2 X
A6 = 5 X 4 6 5 X
A7+ = X 0 2 1 2 0
B7/13 = X 2 X 2 4 4
B7/9 = X 2 1 2 2 X
Bb7/13 = 6 X 6 7 8 X
Bm7 = X 2 4 2 3 2
C#7/13 = X 4 X 4 6 6
C#7/9 = X 4 3 4 4 X
C#m7 = X 4 6 4 5 4
C7/13 = X 3 X 3 5 5
D = X X 0 2 3 2
D/A = X 0 X 2 3 2
D/E = X X 2 2 3 2
D/F# = 2 X 0 2 3 2
D7+ = X X 0 2 2 2
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
F#5+/7 = 2 X 2 3 3 2
F#7/9- = X X 4 3 5 3
F#m = 2 4 4 2 2 2
G/A = 5 X 5 4 3 X
