Belchior - Comentário a Respeito de John

[Intro]  G  C  G  C  G  C
         A  D

  G           C       G                 C    G
 Saia do meu caminho, eu prefiro andar sozinho
                  C            A   D
 Deixem que eu decida a minha vida
  G            C       G                C       G
 Não preciso que me digam, de que lado nasce o sol
              C          A    D
 Porque bate lá o meu coração

  C                          G
 Sonho e escrevo em letras grandes de novo
  C              D
 Pelos muros do país
   Em        C         A             D
 João, o tempo, andou mexendo com a gente sim
  Em              C       A                      C  G
 John, eu não esqueço, a felicidade é uma arma quente
   C       G    C       G  A  D
 Queeeeeente, queeeeeente


  C                  G
 Sob a luz do teu cigarro na cama
  C                      D
 Teu rosto rouge, teu batom me diz
  Em      C       A                 D
 João, o tempo andou mexendo com a gente sim
  Em              C                  A                         C  G
 John, eu nao esqueço(oh no, oh no), a felicidade é uma arma quente
  C        G    C      G  A  D
 Queeeeeente, queeeeente

----------------- Acordes -----------------
A = X 0 2 2 2 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
