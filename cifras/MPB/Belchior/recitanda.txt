Belchior - Recitanda

[Intro] G  C  G  C

G        C4
Graças a deus
         G         C
Eu perco sempre o juízo
   G        C
No lance de dez
    Am         D
No acaso do sucesso
      G         C
O paraíso é a palavra
   G        C
O paraíso é lá
   G        C
Mensagem de amor
    Am         D
A votação no congresso
            C
Vem viver comigo
             G
Vem correr perigo

C             Am
Vem, meu bem, meu bem
                           D
Que outros cantores chamam "baby"

     C
Vejo vir vindo no vento
         G
O cheiro da nova estação
      C
Sinto tudo na ferida
     G
Viva do meu coração
        C
Olha, o show já começou,
      G
É bom não se atrasar:
    C                  D
- Viver é melhor que sonhar!

     C
Esse jeito de deixar
          G
Sempre de lado a certeza
    C
Sem deixar o meu cigarro
      G
Se apagar pela tristeza
      C
É proibida a entrada,
          G
Mas ainda quero falar:
    C                   D
- Viver é melhor que sonhar!
    C                  G
- Viver é melhor que sonhar!
 C  G  C  D     C
          Nã-nã-nã
      G         D       C
Nã-nã-nã Nã-nã-nã Nã-nã-nã
      D        G
Nã-nã-nã Nã-nã-nã

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C4 = X 3 3 0 1 X
D = X X 0 2 3 2
G = 3 2 0 0 0 3
