Belchior - Almanaque

Eb                              Fm                          Eb
 Ô menina vai ver nesse almanaque como é que isso tudo começou
Eb                              Fm                                     Eb
 Diz quem é que marcava o tique-taque e a ampulheta do tempo que disparou
Eb                             Fm                            Eb
 Se mamava de sabe lá que teta    o primeiro bezerro que berrou
     Fm     F#°        Gm
 Me diz, me diz, me responde, por favor
Fm                   Bb
 Pra onde vai o meu amor

 Quando o amor acaba

Eb                            Fm                                Eb
 Quem penava no sol a vida inteira, como é que a moleira não rachou
Eb                             Fm                                   Eb
 Quem tapava esse sol com a peneira e quem foi que a peneira esburacou
Eb                            Fm                             Eb
 Quem pintou a bandeira brasileira que tinha tanto lápis de cor
     Fm     F#°        Gm
 Me diz, me diz, me responde, por favor

Fm                   Bb
 Pra onde vai o meu amor

 Quando o amor acaba

Eb                               Fm                             Eb
 Diz quem foi que fez o primeiro teto que o projeto não desmoronou
Eb                                 Fm                             Eb
 Quem foi esse pedreiro, esse arquiteto e o valente primeiro morador
Eb                                 Fm                                 Eb
 Diz quem foi que inventou o analfabeto e ensinou o alfabeto ao professor
     Fm     F#°        Gm
 Me diz, me diz, me responde, por favor
Fm                   Bb
 Pra onde vai o meu amor

 Quando o amor acaba

Eb                             Fm                                  Eb
 Quem é que sabe o signo do capeta, o ascendente de Deus Nosso Senhor
Eb                             Fm                              Eb
 Quem não fez a patente da espoleta explodir na gaveta do inventor
Eb                          Fm                             Eb
 Quem tava no volante do planeta que o meu continente capotou
     Fm     F#°        Gm
 Me diz, me diz, me responde, por favor
Fm                   Bb
 Pra onde vai o meu amor

 Quando o amor acaba

Eb                               Fm                                  Eb
 Vê se tem no almanaque, essa menina, como é que termina um grande amor
Eb                         Fm                              Eb
 Se adianta tomar uma aspirina ou se bate na quina aquela dor
Eb                               Fm                            Eb
 Se é chover o ano inteiro chuva fina ou se é como cair o elevador
     Fm     F#°        Gm
 Me diz, me diz, me responde, por favor
Fm                   Bb
 Pra que tudo começou

 Quando tudo acaba

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Eb = X 6 5 3 4 3
F#° = 2 X 1 2 1 X
Fm = 1 3 3 1 1 1
Gm = 3 5 5 3 3 3
