Belchior - Espacial

[Intro]

   C       G       Am      Em      F       D/F#      G
E|-------8-7-5-----------5--------------------------------------|
B|-8-7-8-------8-6-5---5---8-6-5-----------7-5------------------|
G|-------------------8-----------7-5-4-5-------7-5-5-4---4-5-7--|
D|---------------------------------------7-------------7--------|

   C       G       Am      Em      F       D/F#      G
E|-------8-7-5-----------5-------------------------------------------
B|-8-7-8-------8-6-5---5---8-6-5-----------7-5-----------------------
G|-------------------8-----------7-5-4-5-------7-5-5-4---4-5-7--5-4-5
D|---------------------------------------7-------------7-------------

C            G    Am        Em
Olha para o céu: tira teu chapéu
   F             D/F#    G7
Pra quem fez a estrela nova - que nasceu
C            G     Am        Em
Traz o teu sorriso novo espacial
   F            D/F#    G7    C   C7 E7
Pra quem fez a estrela artificial


   Am                   Am/G#
Eu sei que agora a vida deixa de ser vã
         Am/G                         Am/F#
Pois há mais luz na avenida e mais um astro na manhã
      Fm                        C            A7
Quem volta do seu campo,ao sol poente, vem dizer
          D7     Gº   A°    D/F#   G         G7
Que a estrela é diferente e faz o trigo aparecer

C            G    Am        Em
Olha para o céu: tira o teu chapéu
   F             D/F#    G7
Pra quem fez a estrela nova - que nasceu
C            G    Am        Em
Não é pra São Jorge, nem pra São João
   F            D/F#    G7    C
Pois não é outra lua e não é balão

G7                         C
Quem mora no Oriente não vai se incomodar
    A7                       Dm
Ao ver que no Ocidente a estrela quer passar
     Fm                    C         A7
Não há mais abandono nem reino de ninguém.
      D7                    G7
Se a terra já tem dono, no céu ainda não tem

                          C
Por isso vem; deixa o cansaço, apressa o passo
         G7
E vem correndo pro terraço e abre os braços
                    C  C7
Para o espaço que houver
             F            Fm             C
Quem não quiser deixar a terra em que vivemos
       A7          Dm          G
Pelos astros onde iremos vai ouvir
         C  C7         F
Ver e contar tantas estrelas
         Fm            C               A7         Dm7
Quantas forem nossas naves, noutros mares mais suaves
   G7           C
A voar, voar, voar

   C       G       Am      Em      F       D/F#      G
E|-------8-7-5-----------5--------------------------------------|
B|-8-7-8-------8-6-5---5---8-6-5-----------7-5------------------|
G|-------------------8-----------7-5-4-5-------7-5-5-4---4-5-7--|
D|---------------------------------------7-------------7--------|

   C       G       Am      Em      F       D/F#      G
E|-------8-7-5-----------5-------------------------------------------
B|-8-7-8-------8-6-5---5---8-6-5-----------7-5-----------------------
G|-------------------8-----------7-5-4-5-------7-5-5-4---4-5-7--5-4-5
D|---------------------------------------7-------------7-------------

C            G    Am        Em
Olha para o céu: tira o teu chapéu
   F             D/F#    G7
Pra quem fez a estrela nova - que nasceu
C            G    Am        Em
Não é pra São Jorge, nem pra São João
   F            D/F#    G7    C
Pois não é outra lua e não é balão

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Am/F# = 2 X 2 2 1 0
Am/G = 3 X 2 2 1 X
Am/G# = X X 6 5 5 5
A° = 5 X 4 5 4 X
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
Gº = 3 X 2 3 2 X
