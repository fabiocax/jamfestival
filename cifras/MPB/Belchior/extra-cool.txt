Belchior - Extra Cool

Introdução:  D A Bm G A  (2X)

Latino-América libre!
La Mamma Africaribe!

D
Extra cool,
A                            Bm      G  A
a década da decadência!
D
Onda blue,
A                         Bm      G  A
o verde não violência!
D
Soul e sul,
A                        Bm      G  A
Latino-América libre!
D
Que lundu,
A                         Bm        A       G       Bm    A      G            Bm    A      G
La Mamma Africaribe! ....yeah..yeah..hum..hum.humm…. hum..hum.humm


A              D
Ai! Ai! Ai! Ai! Quanto sol!
A                         Bm      G  A
o mundo nu para breve!
D
Tou em greve
A                              Bm      G  A
Cansongs em portunhol!
D
Olha a dança
A                                  Bm                       G  A
vingando a nossa esperança, çá, çá, çá!
D
Saia da roda a baixeza
A                         Bm      A       G      A     A7
- crime de lesa-beleza!
    Bm                            A               G     A
Armada só de luz, papoula a manhã!
                    Bm  G  A
Desarmement!
     Bm                            A                      G      A
Je chant quelle etoile rouge... En passant!
                Bm     A            G      A
Allons, enfants! Alons, enfants! (2x)

D
Tao eterno,
A                                      Bm                G  A
que tudo o mais vá pro inferno! Oh! Folia!
D
Tão moderno,
A                                 Bm    G  A
Pierrot, Le Fou! Anarquia!
D
Toque o deus Pan
A                                 Bm  G  A
new bossa e Cubanacán
D               A                             Bm    A
Fortaleza! A flor do heavy, a leveza,
   G                    A
e o beijo noir-neon de uma fã!
Bm                               A               G     A
Armada só de luz, papoula a manhã!
                    Bm  G  A
Desarmement!
     Bm                            A                      G      A
Je chant quelle etoile rouge... En passant!
                Bm     A            G         A
Allons, enfants! Alons, enfants!
                Bm     A            G
Allons, enfants! Alons, enfants!
                 A                      D
Desarmement! Alons, enfants!
                   G      A           D
Desarmement! Alons, enfants!
                  G         A          D
Desarmement! Desarmement!
                  G         A          D
Desarmement! Desarmement!........

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
G = 3 2 0 0 0 3
