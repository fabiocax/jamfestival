Belchior - Galos, Noites e Quintais

[Intro]  A  E  A  F#m  E4/7  E

  E
Quando eu não tinha o olhar lacrimoso
     F#
Que hoje eu trago e tenho
  A7
Quando adoçava o meu pranto e o meu sono
      E                 E7
No bagaço de cana de engenho
  E
Quando eu ganhava esse mundo de meu Deus
            F#
Fazendo eu mesmo o meu caminho
 A7
Por entre as fileiras do milho verde que ondeiam
        E                   E7
Com saudades do verde marinho
   A
Eu era alegre como um rio
    F#m
Um bicho, um bando de pardais

         Bm7          E7
Como um galo, quando havia
              Bm7                 E7
Quando havia galos, noites e quintais
     A
Mas veio o tempo negro e a força fez
   F#m
Comigo o mal que a força sempre faz
     Bm7                    E7
Não sou feliz, mas não sou mudo
                     A   F#m
Hoje eu canto muito mais

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm7 = X 2 4 2 3 2
E = 0 2 2 1 0 0
E4/7 = 0 2 0 2 0 X
E7 = 0 2 2 1 3 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
