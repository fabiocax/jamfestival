Fagner - Borbulhas de Amor

Intro 2x: Bm  E

Primeira Parte:
 A
Tenho um coração
                    E
Dividido entre a esperança
E a razão
 Bm              D
Tenho um coração
        Bm            E   Bm  E
Bem melhor que não tivera
A
Esse coração
                    E
Não consegue se conter
Ao ouvir tua voz
 Bm           D
Pobre coração
           Bm         E   Bm  E
Sempre escravo da ternura


Refrão:

                  A
Quem dera ser um peixe

Para em teu límpido
               E
Aquário mergulhar

Fazer borbulhas de amor
             Bm  D
Pra te encantar
    Bm              E
Passar a noite em claro
     Bm    E
Dentro de ti

    A
Um peixe

Para enfeitar de corais
        E
Tua cintura

Fazer silhuetas de amor
          Bm  D
À luz da lua
    Bm          E
Saciar esta loucura
     Bm    E
Dentro de ti

Base Solo 1:   A  E  Bm  G  D  D/E  E

Repete a Primeira Parte:

 A
Canta coração
                    E
Que esta alma necessita
De ilusão
 Bm           D
Sonha coração
       Bm             E   Bm  E
Não te enchas de amargura
A
Esse coração
                    E
Não consegue se conter
Ao ouvir tua voz
 Bm           D
Pobre coração
           Bm         E   Bm  E
Sempre escravo da ternura

Repete o Refrão:

                  A
Quem dera ser um peixe

Para em teu límpido
               E
Aquário mergulhar

Fazer borbulhas de amor
             Bm  D
Pra te encantar
    Bm              E
Passar a noite em claro
     Bm    E
Dentro de ti

    A
Um peixe

Para enfeitar de corais
        E
Tua cintura

Fazer silhuetas de amor
          Bm  D
À luz da lua
    Bm          E
Saciar esta loucura
     Bm    E
Dentro de ti

Segunda Parte:

F      G
  Uma noite
       Em            Am
Para unir-nos até o fim
F         G             Em
  Cara a cara, beijo a beijo
     Am
E viver
F       G                A
  Para sempre dentro de ti

Base do Solo 2: A  E  Bm  D  Bm  E  Bm  E

Refrão Final:

                  A
Quem dera ser um peixe

Para em teu límpido
               E
Aquário mergulhar

Fazer borbulhas de amor
             Bm  D
Pra te encantar
    Bm              E
Passar a noite em claro
     Bm    E
Dentro de ti

    A
Um peixe

Para enfeitar de corais
        E
Tua cintura

Fazer silhuetas de amor
          Bm  D
À luz da lua
    Bm          E
Saciar esta loucura
     Bm    E
Dentro de ti

    A
Um peixe

Para em teu límpido
               E
Aquário mergulhar

Fazer borbulhas de amor
             Bm  D
Pra te encantar
    Bm              E
Passar a noite em claro
     Bm    E
Dentro de ti

    A
Um peixe

Para enfeitar de corais
        E
Tua cintura

Fazer silhuetas de amor
          Bm  D
À luz da lua
    Bm          E
Saciar esta loucura
     Bm    E
Dentro de ti
F       G
  Para sempre
           A
Dentro de ti

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D/E = 0 0 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 3 3
