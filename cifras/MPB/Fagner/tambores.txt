Fagner - Tambores

Intro: (G G5- G4)

G G5-
Passei esse tempo todo andando comigo
G4 G
e só devo lhe dizer que tenho esse sentimento
G4 D F E
antigo / Uma vez por ano no natal
G D/A
eles compram meus lindos cabelos
F/A E/A
uma vez por ano no natal eles compram
G
meus lindos cabelos
F/A G
E pensam que me conhecem mas só me entristecem
(G G5- G4) G4 G5- G7
Tambores, tambores tambores

e um coração apaixonado
G4 G5- G7
(Tambores tambores tambores)Por esse pequeno

sentimento
e o meu pobre coração(tambores tambores tambores)
já não cansa de ter tantas saudades(tambores tambores
tambores)

(E)

----------------- Acordes -----------------
D = X X 0 2 3 2
D/A = X 0 X 2 3 2
E = 0 2 2 1 0 0
E/A = X 0 2 1 0 0
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
G = 3 2 0 0 0 3
G4 = 3 5 5 5 3 3
G7 = 3 5 3 4 3 3
