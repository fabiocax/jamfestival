Fagner - O Vinho

Intro: D  G  A  B7/4  B7 Em

B7             Em                   Am
Quando você chegar Veja o tempo passando
                   B7              Em
Pelas Flores do Mal Só o tempo não vê
                 C        D7       G   G7
Que eu estou esquecido esperando Você
  G             C                 D7               Bm
A noite já passou e eu detesto revelar que andei sofrendo
      Em                            C
Mas há um resto de saudade em seu olhar
        D7                     G
E uma promessa de prazeres ciumentos
   B7                  Em
Meu amor Não devemos tentar
        C           D7        G    B7
Enganar o amor Nem duvidar do vinho
          C              D        D7
Uma frase vulgar Pode surpreender
 F#|D        G   G7
Num imenso carinho

  G               C             D7                    B7
Pra que dizer que não Verdades são banais e mesmo assim
             Am      C            D7          G  D7
Por caprichos vãos mentiras levarão você de mim
G                                            D
Perdidos na escuridão Seus olhos na minha saudade
   C                                                 B7
Seu vestido está em minhas mãos Sua voz por toda a cidade
 E                     E7                  A         A7
Nessas canções que eu faço Permanece o meu destino
  D              F#|D        C                G
De adormecer em seus braços e sonhar como um menino
C                Cm                 G       F#|D  Em Em7
Quero só pensar em ti Em seu colo adormecer
   A7                  D7           G
E acordar com mais vontade de te viver

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
B7/4 = X 2 4 2 5 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
