Fagner - Romanza

Intro: D  E/D  G/D  D

     D        E/D
Espinhos, esporas
 G/D
Sobre a renda labirinto
               D
Pedras no meu peito

Aberto em chaga amor
D
Ai ferraduras de prata
E/D
Cascos tinindo o aurora
     G/D
Que cavaleiro castanho
 D
Come teu pelo moreno

Breu e cal manchas de sangue


Gavião cara-donzela

Cachorro que não se aquieta

Sobre a cama camarinha
E4                E7
Lençol cheiro de alfazema
G/A                  A7
Falas, copos de aguardente
E/F#                F#7
Facas rasgando o corpete
A/B               B7
Suspiros parto criança
F#/G#                 G#7
Hoje eu vi toque a defunto
C#m7                   A
Das dores doida de estrada
E
Eu d'olhos solto nos ares
G                D
Conheço a morte e a paixão

( D  E/D  G/D  D )

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/B = X 2 2 2 2 X
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E/D = X 5 X 4 5 4
E/F# = X X 4 4 5 4
E4 = 0 2 2 2 0 0
E7 = 0 2 2 1 3 0
F#/G# = 4 X 4 3 2 X
F#7 = 2 4 2 3 2 2
G = 3 2 0 0 0 3
G#7 = 4 6 4 5 4 4
G/A = 5 X 5 4 3 X
G/D = X 5 5 4 3 X
