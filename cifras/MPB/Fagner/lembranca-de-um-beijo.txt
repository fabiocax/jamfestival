Fagner - Lembrança de Um Beijo

Bm
  Quando a saudade
Invade o coração da gente
E pega a veia
                        F#
Onde corria um grande amor
            Em
Não tem conversa
                    F#
Nem cachaça que dê jeito
                 Em
Nem um amigo do peito
                   Bm
Que segure o chororô
Bm
  Quando a saudade
Invade o coração da gente
E pega a veia
                        F#
Onde corria um grande amor
            Em
Não tem conversa

                    F#
Nem cachaça que dê jeito
                 Em
Nem um amigo do peito
                   Bm
Que segure o chororô

Bm                   Em
  Que segure o chororô
   D                     E
Ô! Ô! Que segure o chororô
    Em
Saudade
        A7        D
Já tem nome de mulher
     F#
Só pra fazer do homem
            Bm  B7
O que bem quer
    Em
Saudade
        A7        D
Já tem nome de mulher
     F#
Só pra fazer do homem
            Bm
O que bem quer

Bm
O cabra pode ser valente
      F#
E chorar
Ter meio mundo de dinheiro
      Bm
E chorar
Ser forte que nem sertanejo
      F#
E chorar
           Em
Só na lembrança de um beijo
F#    Bm
E Chorar
Bm
O cabra pode ser valente
      F#
E chorar
Ter meio mundo de dinheiro
      Bm
E chorar

Ser forte que nem sertanejo
      F#
E chorar
           Em
Só na lembrança de um beijo
F#    Bm
E chorar

Bm
  Quando a saudade
Invade o coração da gente
E pega a veia
                        F#
Onde corria um grande amor
            Em
Não tem conversa
                    F#
Nem cachaça que dê jeito
                 Em
Nem um amigo do peito
                   Bm
Que segure o chororô
Bm
  Quando a saudade
Invade o coração da gente
E pega a veia
                        F#
Onde corria um grande amor
            Em
Não tem conversa
                    F#
Nem cachaça que dê jeito
                 Em
Nem um amigo do peito
                   Bm
Que segure o chororô

Bm                   Em
  Que segure o chororô
   D                     E
Ô! Ô! Que segure o chororô
    Em
Saudade
                   Bm
Já tem nome de mulher
     F#
Só pra fazer do homem
            Bm
O que bem quer
    Em
Saudade
                   Bm
Já tem nome de mulher
     F#
Só pra fazer do homem
            Bm
O que bem quer

Bm
O cabra pode ser valente
      F#
E chorar
Ter meio mundo de dinheiro
      Bm
E chorar
Ser forte que nem sertanejo
      F#
E chorar
           Em
Só na lembrança de um beijo
F#    Bm
E Chorar
Bm
O cabra pode ser valente
      F#
E chorar
Ter meio mundo de dinheiro
      Bm
E chorar
Ser forte que nem sertanejo
      F#
E chorar
           Em
Só na lembrança de um beijo
F#    Bm
E Chorar

----------------- Acordes -----------------
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
