Fagner - Frenesi

[ Fausto Nilo, Petrucio Maia & Ferreirinha ]


( E7+ F#/E Am7 E )

E7+          F#/E
A felicidade corre sem parar
A/E                E
Bela é uma cidade velha
E7+                   F#/E
Na velocidade a tarde leva o teu olhar
 A                    E
Longe descansar na estrela
E7                     A
  E um corpo passa por mim
Am       D7       E7+
Água do rio na areia
C#7           F#m7
 Adormecendo assim
           Am
Esta pedra em mim

 B7               E
E meu leito clareia
E7                 A
 Fosse paixão frenesi
Am       D7       E7+
Doce ilusão moça bela
C#7            F#m7
 A solidão mora aqui
                 Am  B7
E a cidade é sem fim
             E
Qual a tua janela
A/C#
Tudo igual
D7
Tal e qual
E  F#m   G°  E/G#
Fosse paixão

( F# E Am7 E7+ )

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A/E = 0 X 2 2 2 0
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
C#7 = X 4 3 4 2 X
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E7 = 0 2 2 1 3 0
E7+ = X X 2 4 4 4
F# = 2 4 4 3 2 2
F#/E = X X 2 3 2 2
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
G° = 3 X 2 3 2 X
