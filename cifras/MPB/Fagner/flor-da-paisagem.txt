Fagner - Flor da Paisagem

INTROD:D C#m G Bm Bm/A G F#
         Em A7 D

 D               C#m G   Bm   Bm/A
Teus zoi e a flor da paissagem
  G       F#        Bm  Bm/A
Sereno fim da viagem
 G                F#        Bm  Bm/A
Teus zoi e a cor da beleza
  G       F#     Em  A7
Sorriso da natureza
D           C#m  G   Bm  Bm/A
Azul de prata meu litoral
 G                F#           Bm Bm/A
Dois brincos de pedra rara
 G             F#     Bm Bm/A
Riacho de agua clara
 G                F#         Em  A7
Roupa com cheiro de mala
 G       Bm                 Em  G C
Zoim assim sao mais belos

                 Bm    Am  D
Que renda branca na sala
 G                   Bm          Em G
Quem ve nao enxerga a praia
 C            Bm   Am     D
Nos num lencol de cambraia
 D              C#m G   Bm  Bm/A
Teus zoi no fim da vereda
 G            F#         Bm Bm/A
Amor de papel de seda
 G            F#        Bm  Bm/A
Teus zoi clareia o rocado
 G            F#        Em   A7
Reluz teu cordao colado

   (  G Bm Em G C G/B Am Bm C Bm Am G D  )

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
Bm/A = X 0 4 4 3 X
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
