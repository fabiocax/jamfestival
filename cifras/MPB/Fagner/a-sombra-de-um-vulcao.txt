Fagner - À Sombra de Um Vulcão

Intro: Am  D7  G  Em
       F#m/5-  B7  Em
       E7  Am  D7  G  B7
       Em  C#°  F#m7/5-  B7

      Em                      Eº
Nunca houve uma mulher como você
      F#m7/5-        B7        Em D7
Em milhões  de anos luz de solidão
       G                       Am
Minhas noites novamente são azuis
       C7                        Bm7/5- E7
Minhas tardes são douradas de verão
   Am             D7
Você é o meu paraíso
     G          B7         Em
A pessoa que eu tanto preciso
       F#m7/5-      B7
Com loucura e paixão
  Em               G  E7
Eu rezo com o teu olhar

   Am              D7
Eu gozo com a tua voz
      G       B7        Em
Esse amor arrebenta com tudo
C#°
   Parece até que o mundo
                  F#m7/5- B7
Não sobrevive sem nós
      Em                     Dm G7
Nunca ouve uma mulher como você
      C7            B7              Em   D7
Entre tantas que já tive em minhas mãos
       G                         Am B7
Eu preciso acreditar que sou feliz
        C7                            Bm7/5- E7
Mas persigo os teus mistérios como um cão
   Am             D7
E tudo parece tão claro
  G      B7       Em
E tudo parece perfeito
    F#m7/5-             B7
Mas quando  acordo e me vejo
    Em     B7     G
O espelho diz que não
     Dm        G7          C
Quem sonha contigo molha a cama
        Am          B7                Em
Quem te ama dorme à sombra de um vulcão
     Dm        G7          C
Quem sonha contigo molha a cama
        Am          B7                Em   E7
Quem te ama dorme à sombra de um vulcão
C#°
   Parece até que o mundo
                  F#m7/5- B7
Não sobrevive sem nós
      Em          B7         Dm G7
Nunca ouve uma mulher como você
      C             B7              Em   D7
Entre tantas que já tive em minhas mãos
       G                         Am B7
Eu preciso acreditar que sou feliz
        C7                            Bm7/5- E7
Mas persigo os teus mistérios como um cão
   Am             D7
E tudo parece tão claro
  G      B7       Em
E tudo parece perfeito
    F#m7/5-             B7
Mas quando  acordo e me vejo
    Em     B7     G
O espelho diz que não
     Dm        G7          C
Quem sonha contigo molha a cama
        Am          B7                Em
Quem te ama dorme à sombra de um vulcão
     Dm        G7          C
Quem sonha contigo molha a cama
        Am          B7                Em
Quem te ama dorme à sombra de um vulcão
     Dm        G7          C
Quem sonha contigo molha a cama
    F#m7/5-         B7               Em
Quem te ama dorme à sombra de um vulcão

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
Bm7/5- = X 2 3 2 3 X
C = X 3 2 0 1 0
C#° = X 4 5 3 5 3
C7 = X 3 2 3 1 X
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F#m/5- = 2 X 4 2 1 X
F#m7/5- = 2 X 2 2 1 X
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
