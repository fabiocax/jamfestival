Fagner - Olha Pro Céu / São João Na Roça / Noites Brasileiras (Pot-Pourri)

Intro:

                F     D7  C    A7  Dm       G7     C
E|--------------------------------------------------------------------------------------|
B|-------------------------13-----------------------------------------------------------|
G|----------10-12-14-13-14------14-12-11-12---------------14-12-10----------------------|
D|-10-12-14-----------------------------------14-12-11-12----------12-10----------------|
A|---------------------------------------------------------------------------10--11--10-|
E|------------------------------------------------------------------------14------------|


Cm
Olha pro céu, meu amor
                 C7  Fm
Vê como ele está lindo
A#7               D#
Olha praquele balão multicor
Ab      Cm        G7
Como no céu vai sumindo

C
Foi numa noite, igual a esta

                   Dm
Que tu me deste o teu coração
G7            Dm
O céu estava, assim em festa
G7               C
Porque era noite de São João
Gm    A#         A7
Havia balões no ar
                Dm
Xóte, baião no salão
F        D7
E no terreiro
C       A7   Dm        G7
O teu olhar, que incendiou
         C
Meu coração
  Intro:(C Dm G C)2x C Bm Am G C

                                                   F           G       C       Bb
(A fogueira tá queimando em homenagem a são joão o forró
    Am  Dm F        C          G            C Bm Am G C
já começou vamos gente arrasta pé nesse salão)
                                                    F           G       C       Bb
(A fogueira tá queimando em homenagem a são joão o forró
    Am  Dm F        C          G            C
já começou vamos gente arrasta pé nesse salão)
            G               C         C7
Dança Joaquim com Zabé Luiz com iáiá dança jãojão com Raque
              F           G    C Am         Dm       G
e eu com sinhá traz a cachaça Mané eu quero vê quero vê quero
          C
vê palha voar

C        G          C      C7
Ai, que saudades que eu sinto
F                C
Das noites de São João
F              C       G/B       Am
Das noites tão brasileiras nas fogueiras
Dm    G         C     E7
Sob o luar do sertão
Am                   E7
Meninos brincando de roda
                 Am
Velhos soltando balão
Dm                  Am
Moços em volta á fogueira
F                 E7
Brincando com o coração
Dm                     Am
Eita, São João dos meus sonhos
E7                Am   Ab  G
Eita, saudoso sertão
Dança Joaquim com Zabé...

----------------- Acordes -----------------
A# = X 1 3 3 3 1
A#7 = X 1 3 1 3 1
A7 = X 0 2 0 2 0
Ab = 4 3 1 1 1 4
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
D# = X 6 5 3 4 3
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
