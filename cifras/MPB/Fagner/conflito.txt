Fagner - Conflito

Intro: Em D A

Em              Em/D        C
Ah, meu coração que não entende
       D       Bm       Em
O compasso do meu pensamento
Em        Am   Am/G     D
Se o pensamento se protege
         G                 C            B7
E o coração se entrega inteiro e sem razão
Em         Em/D        C        D      Bm     Em
Se o pensamento foge dela o coração a busca aflito
Em         Am Am/G     D           G         C          B7
E o corpo todo sai tremendo, massacrado e ferido do conflito

(Em   D   B7   Em   Am   D   Bm   C   D   A)

(Am)

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Am/G = 3 X 2 2 1 X
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
Em/D = X X 0 4 5 3
G = 3 2 0 0 0 3
