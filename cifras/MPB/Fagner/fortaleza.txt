Fagner - Fortaleza

F                            Gm
Velas brancas do mar vão surgindo
        C                     F
Com as primeiras estrelas do céu
                             Gm

Onde o verde da tarde é mais lindo
E                 F
azul só precisa de Deus
                     Gm
Se essa cidade é meu mundo
                       F
Mucuripe, jamais foste meu
Am                       Gm
Me envolveu teu encanto profundo
C                            F    F7
Qual jangada que o vento esqueceu
                       Bb
Mas quando eu canto a beleza
                 F7
Que ainda iremos fazer

                Gm
Sem desprezar a riqueza
C                         F
Fortaleza, eu só penso em você
   Gm             C
Eu só quero dizer
F               Dm
Que é a cidade é a luz
      Am
Que ilumina os meninos risonhos

Além da imaginação
           C   F
Te levo em meu coração

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
Gm = 3 5 5 3 3 3
