Fagner - Como Se Fosse

[Intro]  D  D4  C  G


G         D/F#
De azul carinho
            EmSus9
Vi a noite pintada
C                        Am
Fosse alegre como ave soltando
                       G/B  C
Fosse triste como ser virgem
                       Am
Fosse linda como a conquista
         D             G
Das estrelas, das estrelas
          D/F#
De azul carinho
                EmSus9
Veio o desenho pintado
C                     Am
Jeito de amar desesperado

                     C
Mais chorando que vivido
                D          G
Como se vivê-la fosse não vê-la

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D4 = X X 0 2 3 3
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
