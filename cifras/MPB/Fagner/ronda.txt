Fagner - Ronda

    C                    G/B              Gm/Bb                 A
De noite   eu rondo a  cidade      a te procurar  sem  te  encontrar
Bm7 A/C#              Dm                    Dm7M                Dm7
                      no meio de olhares espio   em todos  os bares  você
      G7      C                            A7     Dm7              Fm
não está                volto pra casa abatido    desencantado da vida
C7M            Am    Ab7   G7   C      Em7    Dm7        Dm7
um  sonho alegria me dá    nele  você está              ah se eu tivesse
  G7                          C         Dm7    E/G#
quem bem me quisesse  esse alguém me diria    desiste esta busca
    E7                Am    Ab7       C         G/B
é inútil  eu não  desistia          porém com perfeita paciência  volto
     Gm/Bb              A      Bm7 A/C#        Dm
a te buscar e hei de encontrar              bebendo com outras
  Dm7M                Dm7                G7      C
mulheres  rolando um dadinho e jogando bilhar         e nesse dia
  A7      Dm7                  Fm    C7M           Am       Ab7
então   vai dar na primeira edição       cena de sangue num bar
     G7          C
da avenida São João.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A7 = X 0 2 0 2 0
Ab7 = 4 6 4 5 4 4
Am = X 0 2 2 1 0
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
C7M = X 3 2 0 0 X
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Dm7M = X X 0 2 2 1
E/G# = 4 X 2 4 5 X
E7 = 0 2 2 1 3 0
Em7 = 0 2 2 0 3 0
Fm = 1 3 3 1 1 1
G/B = X 2 0 0 3 3
G7 = 3 5 3 4 3 3
Gm/Bb = 6 5 5 3 X X
