Fagner - Canção da Floresta

A                      A/E
Tombam árvores, morrem índios
                       D D9 D
Queimam matas, ninguém vê
                     E
Que o futuro está pedindo
                     A A7
Uma sombra e não vai ter
D                          E
Pensem em Deus, alertem o mundo
                    A
Pra floresta não morrer
A               A/E
Devastação é um monstro
                    D D9 D
Que a natureza atropela
                      E
Essas manchas de queimadas
                     A
Que hoje vemos sobre ela
             D      A
São feridas que os homens

E                 A
Fizeram no corpo dela

D
Use as mãos, mude uma planta
        E             A
Regue o chão, faça um pomar
                  E
Ouça a voz do passarinho
                  A
A floresta quer chorar
D                 A           E              A
A natureza está pedindo pra ninguém lhe assassinar

A                      A/E
Quando os cedros vão tombando
                 D D9 D
Dão até a impressão
                     E
Que os estalos são gemidos
                  A A7
Implorando compaixão
D                E
As mãos do homem malvado
                   A
Desmatou sem precisão

 A                     A/E
Mas quando Deus sentir falta
                       D  D9 D
Do pau que já foi cortado
                  E
O homem talvez procure
                 A A7
Por a culpa no machado
                 A
Ai Deus vai perguntar :
              E    A
"E por quem foi amolado ?"
  A                  A/E
Fáuna e flora valem mais
                     D D9 D
Do valor que o ouro tem
                 A
A natureza é selvagem
          E         A A7
Mas não ofende ninguém
 D                    A
Ela é a mãe dos seres vivos
   E           A
Precisa viver também
  A                       A/E
Ouça os índios, limpem os rios
                    D D9 D
Façam a Deus esse favor
                     A
Floresta é palco de ave
         E           A A7
Museu de sonho e de flor
   D               A
Vamos cuidar com carinho
      E             A
Do que Deus fez com amor

D
Use as mãos, mude uma planta
        E             A
Regue o chão, faça um pomar
                E
Ouça a voz do passarinho
                  A
A floresta quer chorar
D                A                  E            A  A4 A
A natureza está pedindo pra ninguém lhe assassinar

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/E = 0 X 2 2 2 0
A4 = X 0 2 2 3 0
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
