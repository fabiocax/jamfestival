Fagner - Semente

Bb              C/Bb   Cm/Bb
Os teus olhos distribuem
                     Bb
O que não existe no meus
Gm                       C7
As luzes que os meus possuem
Cm7                  Bb
São as migalhas dos teus

Gm                       Dm
Quem gasta no amor vinte anos
Eb7+             Bb
Menos amou na alegria
F/A  Fm/Ab G7         Cm7
Do que quem ama um só dia
   F               Bb
E morre de desenganos

Gm                      A
Teu sorriso é um jardineiro
D7                 Gm
Meu coração é um jardim

Cm       F            Bb
Saudade, imenso canteiro
  A                     D7
Que eu trago dentro de mim

Bb                C/Bb  Cm/Bb
A semente dos teus olhos
F                Bb
Caiu no meu coração
Gm                     C7
Deu uma árvore de abrolhos
Cm7     F            Bb
Deu uma fruta: a paixão

( Gm  Dm  Cm  F  G  C )

Am                       B
Teu sorriso é um jardineiro
E7                  Am
Meu coração é um jardim
Dm     G           C
Saudade, imenso canteiro
  B                     E7
Que eu trago dentro de mim

C                 D/C  Dm/C
A semente dos teus olhos
G                C
Caiu no meu coração
Am                    D7
Deu uma árvore de abrolhos
Dm7      G            C
Deu uma fruta: a paixão

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C/Bb = X 1 2 0 1 X
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
Cm/Bb = X 1 1 0 1 X
Cm7 = X 3 5 3 4 3
D/C = X 3 X 2 3 2
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Dm/C = X 3 X 2 3 1
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
Eb7+ = X X 1 3 3 3
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
Fm/Ab = 4 3 3 1 X X
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
