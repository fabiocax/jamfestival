Fagner - Palavras de Amor

[Intro] E  A/E  B/E  A/E  A

E     A/E        E
 Na divina claridade
      E7/9        A  E/G#  F#m
Que você se iluminou
F°              F#m
O calendário seria
F#m/E            D7+
Um dia de cada cor
   A9/C#                B7
Futuro, ai como eu queria
A/B               B7/9/11+
te cobiçar ventania
        B7        E B/D# C#m7
Num romance de amor
E      A/E            E
  E a lua ainda mais clara
    E7/9              A    E/G#
Queria escutar tua fala
        F#m  A/B
Com palavras     de


  E  A/E B/E A/E
Amor
      E  A/E B/E A/E
De amor
E       A/E             E
  Meu amor quando se cala
        E7/9
Fala mais que um
      A  E/G# C#m7
Pensador
       F°
Me ensinou que a vida
F#m F#m/E D7+
Vai
         A9/C#    B7
Onde a saudade ficou
    A/B                B7/9/11+
E enquanto a vida não pára
           B7
Não pára nunca esse
   E B/D# C#m7
Motor
E           A/E        E
  Outra alegria mais clara
  E7/9             A   E/G#
Seria escutar tua fala
        F#m  A/B
Com palavras    de
   E   A/E B/E A/E A
Amor
    E  A/E B/E A/E
De amor

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/B = X 2 2 2 2 X
A/E = 0 X 2 2 2 0
A9/C# = X 4 2 2 0 X
B/D# = X 6 X 4 7 7
B/E = X 7 9 8 7 7
B7 = X 2 1 2 0 2
B7/9/11+ = X 2 1 2 2 1
C#m7 = X 4 6 4 5 4
D7+ = X X 0 2 2 2
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E7/9 = X X 2 1 3 2
F#m = 2 4 4 2 2 2
F#m/E = X X 2 2 2 2
F° = X X 3 4 3 4
