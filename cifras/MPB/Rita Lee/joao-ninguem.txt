Rita Lee - João Ninguém

Introdução: G D7 G

  G                        G7  C     C#dim  D4/7       D7        G
João Ninguém virou um homem     poderoso pra chuchu
 G                G7  C       C#dim  D4/7     D7        G
Limousines, banquetes mil,       rodeados de urubu
  G                       G7  C     C#dim  D4/7  D7        G
João Ninguém é gente de bem cheio de puxa-saquice
  G             G7  C  C#dim  D4/7              D7        G
Ai daquele pobre diabo     que subiu no palco e disse:
G                      D             Am                             G 
Rei  pé-de-chinelo, até parece que o sangue é azul!" 

João Ninguém é dono da aldeia quem bobeou, dançou
  G            G7  C C#dim  D4/7                 D7    G
Desconfia até      da mãe quanto mais do tataravô
 G                    G7            C     C#dim  D4/7          D7        G
João Ninguém não perde um vintém     no boliche quatrocentão
  G               G7  C     C#dim  D4/7  D7        G
Sem talento pra ser feliz milionário por vocação
                                           G                     D           Am                            G 
Então eu digo que ele é um rei pé-de-chinelo até parece que o sangue é azul! 


Quanto mais tem mais quer!
G          D     Am            G
Quanto mais tem mais quer!

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C#dim = X 4 5 3 5 3
D = X X 0 2 3 2
D4/7 = X X 0 2 1 3
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
