Rita Lee - Músico Problema

Intro: A C#m D C#m D

(A   C#m   D   C#m  D)
Chega   atrasado   no   ensaio
Desafina, perde o tom
Cria caso com o empresário
Cria clima com o técnico de som
Ele é músico-problema

                          A G F
Sola na pausa, rouba a cena
               (F G)
Só ele aparece
                   A
Só ele acontece e leva
    (C D)  A G F
lucro

           (F  G)
É viralata


Persona non grata,
      A      (C D)
vive maluco
     D
Bota fora
                 A
Manda o cantor embora do grupo
     D
Bota fora
                 A
Manda o cantor embora do grupo
      D
Bota fora
                 A          (F  G)
Manda o cantor embora do grupo


(A C#m     D  C#m    D)
O guitarrista   abre o volume
E tome eco, delay, distorção
O batera splish splash de ciúme
O contrabaixista é contra a contratação
Porque ele é músico-problema
Disco solo nos planos e
       A  G  F
Cinema

                (F  G)
Só ele aparece

Só ele acontece e leva lucro
              A  G  F
e leva lucro

Persona non grata, vive maluco, maluco
     D
Bota fora
                 A
Manda o cantor embora do grupo
     D
Bota fora
                 A
Manda o cantor embora do grupo
      D
Bota fora
                 A
Manda o cantor embora do grupo

----------------- Acordes -----------------
A = X 0 2 2 2 0
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
