Rita Lee - Tudo Vira Bosta

riff 1:
                  B
e|----------------7------
B|----------------7------
G|----------------8------
D|--7--7--9--9----9------
A|--7--7--9--9----9------
E|--5--5--7--7----7------

riff 1 + B
O ovo frito, o caviar e o cozido
A buchada e o cabrito,
O cinzento e o colorido
A ditadura e o oprimido
Prometido e não cumprido
             F#
E o programa do partido
E    riff 1 + B
     Tudo vira bosta



riff 1 + B
O vinho branco, a cachaça
O chope escuro,
O herói e o dedo-duro
O grafite lá no muro,
Seu cartão e seu seguro
Quem cobrou ou pagou juro,
              F#
Meu passado e meu futuro
E    riff 1 + B
     Tudo vira bosta


riff 2: (tocado 2x durante o refrão)
    B7          A7           E
e|----------------------------------------------
B|----------------------------------------------
G|-------4-2--------2-0-------------------------
D|-----2----------0--------------2-0--------2-0-
A|---2----------0--------------2----------2-----
E|---------------------------0----------0-------

         B7
Um dia depois
A7                 E
    Não me vire as costas
             B7
Salvemos nós dois
A7            E
    Tudo vira bosta


riff 1 + B
Filé minhão, champinhão, Don Perrinhão,
Salsichão, arroz, feijão
Muçulmano e cristão,
A Mercedes e o Fuscão
A patroa do patrão,
              F#
Meu salário e meu tesão
E    riff 1 + B
     Tudo vira bosta


riff 1 + B
O pão-de-ló, brevidade da vovó,
O fondue, o mocotó
Paravarotti e xororó
Minha eguinha pocotó
Ninguém vai escapar do pó,
           F#
Sua boca e seu loló
E    riff 1 + B
     Tudo vira bosta


         B7
Um dia depois
A7                 E
    Não me vire as costas
             B7
Salvemos nós dois
A7            E
    Tudo vira bosta


         B7
Um dia depois
A7                 E
    Não me vire as costas
             B7
Salvemos nós dois
A7            E
    Tudo vira bosta


riff 1 + B
A rabada, o tutu, o frango assado,
O jiló e o quiabo
A prostituta e o deputado,
A virtude e o pecado,
Esse governo e o passado
                F#
Vai você que eu tô cansado
E    riff 1 + B
     Tudo vira bosta


         B7
Um dia depois
A7                 E
    Não me vire as costas
             B7
Salvemos nós dois
A7            E
    Tudo vira bosta


         B7
Um dia depois
A7                 E
    Não me vire as costas
             B7
Salvemos nós dois
A7            E
    Tudo vira bosta


          B7
Tudo vira bosta
A7              E
      Tudo vira bosta
          B7
Tudo vira bosta
A7              E
      Tudo vira bosta


          B
Tudo vira bosta

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
