Rita Lee - Já Te Falei

Intro: D Gm D (2x)


G                             D
Já te falei, ouvi dizer por aí
B                                 Em              A7
Já gritei, telefonei, cantei por toda cidade
                           D        Gm         D
Pelo beco, pelo meio da avenida central no jardim

G                          D
Já divulguei, anunciei por aí
B                             Em             A7
Por e-mail, por correio veio toda a verdade

Já mandei num megafone
     D      Gm        D
Pra toda a gente escutar

F#                  Bm
Notícia que se espalha

E
Pára em qualquer lugar

D               Gm                      D
Li no outdoor, pôs na cançao, deu no cinema
D           Gm       D
Que a vida vale a pena
D           Gm                   D
Na matinê, no botequim, na madrugada
D         Gm       D
Vida que vale a pena


Vocalizes...(Bm / D / Bm / D / E / G / D / Gm / D)

G                        D
Já escutei, e repeti por aí
B                                Em            A7
Coloquei cartazes nos murais de toda a cidade
                           D      Gm      D
Já berrei no microfone, à todo volume no ar

F#                   Bm
Palavra que se espalha
E
Pluma no vendaval

D            Gm                 D
Vi no gibi, foi por aí, li no poema
D           Gm       D
Que a vida vale a pena
D              Gm                   D
Li no jornal, vi na tv, foi pela antena
D           Gm       D
Que a vida vale a pena
D           Gm                   D
No futebol, no carnaval, na batucada
D         Gm       D
Vida que vale a pena

Vocalizes... (Bm / D / Bm / D / E / G / D / Gm / D)

D         Gm       D
Vida que vale a pena...

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
