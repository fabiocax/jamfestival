Rita Lee - Cor de rosa choque

C                      F/A
     Nas duas faces de E-va

G               C
     A bela e a fera

Bb              F                 G
    Um certo sorriso de quem nada quer

C        F/A       G              C
    Sexo frá-gil       não foge à luta

Bb              F              G
    E nem só de cama vive a mu-lher

G7                    Gm7
     Por isso não pro-voque

C7                 F
     É cor-de-rosa choque


        Dm7                  Gm7
Oh, oh, oh, oh, oh    Não provoque

C7                 F
     É cor-de-rosa choque

Dm7          Gm7
     Não pro-voque

C7                  F
      É cor-de-rosa choque

Dm7                    Gm7
      Por isso não pro-voque

C7                  F       G    G7/4(9)    G   G7/4(9)   G7(9)
      É cor-de-rosa choque

C                        F/A
     Mulher é bicho esquisito

G             C
     Todo mês sangra

Bb                F                  G
      Um sexto sentido maior que a razão

C               F/A       G              C
     Gata borra-lheira,       você é princesa

Bb                   F             G
     Dondoca é uma espécie em extinção

G7                    Gm7
     Por isso não pro-voque

C7                 F
     É cor-de-rosa choque

        Dm7                  Gm7
Oh, oh, oh, oh, oh   Não pro-voque

C7                  F
      É cor-de-rosa choque

Dm7           Gm7
      Não pro-voque

C7                   F
       É cor-de-rosa choque

Dm7                    Gm7
      Por isso não pro-voque

C7                  F
      É cor-de-rosa choque

        Em7                   Gm7
Oh, oh, oh, oh, oh    Não pro-voque

C7                   F
       É cor-de-rosa choque

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm7 = X 5 7 5 6 5
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
G7(9) = 3 X 3 2 0 X
G7/4(9) = 3 X 3 2 1 X
Gm7 = 3 X 3 3 3 X
