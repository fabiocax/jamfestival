Rita Lee - Copacabana Boy

D
Meu amor, boa sorte
                        Bm
Vai sem medo, sem destino certo
G
Espero que você
Bb      C
Volte para casa
D
São e salvo

D
Eu passo noites em claro
                        Bm
Rezo pra livrar você do mal
G
Copacabana boy
Bb      C
A saudade dói
D               D7
Eu te amo


G
São Jorge de janeiro
Bb      c
Meu guerreiro
D
Ao seu lado
G
Por isso o sol nunca desiste
Bm
Você não pode ficar triste
C                       Bb
O Rio continua lindo, lindo
 D
Lindo de morrer
Bm
Lindo de viver
G       Bb      C       D
Lindo, lindo

D
Eu não sei quantas mil vezes
                      Bm
Vi a mesma cena no cinema
G
Será que esta ferido
Bb      C
Na mão de um bandido
D
Num cativeiro

D
Meu amor prisioneiro
                        Bm
Onde quer que esteja, Deus te proteja
Bb
Cuide-se bem
C
Fique num zen
D               D7
Para sempre, amém

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
