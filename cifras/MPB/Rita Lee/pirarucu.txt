Rita Lee - Pirarucú

G                     D
   Minha terra tem pranetas,
                   G
Onde canta o uirapurú,
                  D
Tem morcego, borboletas,
                     G
Tem santinho, tem voodoo!

                    D
Progresso tá na menoparza,
                  G
Só um reberde sem carça,
                     D
Já num güento mais lorota,
                      G      C   G
Quero meu dinheiro de vorta!

                    D 
||: Euxinguxatuduxingu! 


Eupirucupirarucu! :||
               D
O exocete é tiete,
                   G
O destroyer num distrói eu,
                D
E o veneno das usina
                  G
Fede mais do que urina!

                   D
Entre russo e americano,
                     G
Prefiro gregos e troiano,
                    D
Pelo menos eles num fala
                 G    C   G
Que nóis é boliviano!

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
