Rita Lee - O gosto do azedo

De: Beto Lee

B       Bb              B  Bb
Para o sangue, sou o veneno
B           Bb        B    Bb
Eu mato, eu como, eu dreno
B               Bb            B  Bb
Para o resto da vida, sou extremo
A                F5+
Sou o gosto do azedo
D7+                 D7+/4
A explosão de um torpedo
A                F5+
Contaminação do medo
   D7+            D7+/4
Eu guardo o seu segredo

B7/4    B               A4 A
Sou o HIV que você não vê
B7/4    B               A4 A
Sou o HIV que você não vê

             E
Você não me vê,não
Mas eu vejo você

Sou a ponta da agulha
Tanto bato até que você fura
É a minha a sua captura
Sou dupla persona
Seu estado de coma
Sou o caos, sou a zona
Seu nocaute na lona

Eu sou o livre-arbítrio

Sua força é meu grande defeito
Sou a dor da tortura
Uma nova ditadura
Terminal da loucura
Sou o vírus sem cura

----------------- Acordes -----------------
A = X 0 2 2 2 0
A4 = X 0 2 2 3 0
B = X 2 4 4 4 2
B7/4 = X 2 4 2 5 2
Bb = X 1 3 3 3 1
D7+ = X X 0 2 2 2
E = 0 2 2 1 0 0
F5+ = X X 3 2 2 1
