Rita Lee - Fruto Proibido

F                              C
Não é nada disso alguém fez confusão
F                           C
Vou dar um tempo preciso distração
Bb                 G#            F#         F
As vezes cansa minha beleza essa falta de emoção e de
 F
sensação
F                         C
Quem foi que disse que eu devo me cuidar
F                        C
Tem certas coisas que a gente nao consegue controlar
Bb             G#              F#         F
Comer um fruto que é proibido você não acha

Irresistível

Bb                   G#        F#     F
Nesse fruto esta escondido o paraiso, paraiso
Bb                   G#        F#              F
Eu sei que o fruto é proibido mas eu caio em tentação

                  (C  F)
(acho que não)

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
G# = 4 3 1 1 1 4
