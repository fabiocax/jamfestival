Rita Lee - Arrombou a Festa - 2

G7                                                                                                   A7                      C     Dm      G7 C7 C#7 D7
AI, AI MEU DEUS, O QUE FOI QUE ACONTECEU. COM A MÚSICA POPULAR BRASILEIRA.
      G7
QUANDO A GENTE FALA MAL, A TURMA TODA CAI DE PAU.
     A7                              C           Cm           G7
DIZENDO QUE ESSE PAPO É BESTEIRA.

      C7                                                                                  D7
NA ONDA DISCOTHEQUE DA AMÉRICA DO SUL.“LENILDA" É MISS LANE,
                                                          C7
“ZULEIDE" É LADY ZU. PRA DEFENDER O SAMBA CONTRATARAM ALCIONE.
       D7                                                                                                C7
É BOA DE PISTON, MAS BOTA A BOCA NO TROMBONE. NO MEIO DISSO TUDO
                                                       D7
A FAFÁ VEM DAR UM JEITO. ALÉM DE MUITA VOZ, ELA TAMBÉM TEM MUITO PEITO.
           C7
E A MÚSICA PARECE BRINCADEIRA DE GAROTO.
           D7
POIS QUANDO LIGO O RÁDIO OUÇO ATÉ CAUBY PEIXOTO! CANTANDO:CONCEIÇÃO...

ESTRIBILHO

       C7                                                                                              D7
O SIDINEY MAGAL REBOLA MAIS QUE O MATOGROSSO. CIGANO DE ARAQUE,
                                                                  C7
FABRICADO ATÉ O PESCOÇO. E O CHICO NA PISCINA GRITA LOGO PRO GARÇON.
   D7                                                                                                 C7
AFASTA ESSE CÁLICE E ME TRAZ MOET CHANDON. COM TANTO BRASILEIRO POR AÍ METIDO
                        D7
A BAMBA. SUCESSO NO ESTRANGEIRO AINDA É CARMEN MIRANDA.
       C7
E A RITA LEE PARECE QUE NÃO VAI SAIR MAIS DESSA.
          C7
POIS PRA FAZER SUCESSO ARROMBOU DE NOVO A FESTA!
   G7                                                                        A7                         C                        Cm          G7
ZIRI, ZIRIGUIDUM, SKINDÔ, SKINDÔ, LELÊ. SAI DA FRENTE QUE EU QUERO É COMER
    A7                      C            Cm    G7                                   A7                     C  Cm
A MÚSICA POPULAR BRASILEIRA.  LADY LAURA, A MÚSICA POPULAR...
             G7              D7                                     A7                     C    Cm G F C
PARABÉNS A VOCÊ, PARABÉNS PARA A MÚSICA POPULAR...
       A7                  C Cm                    G7                         Cm
A MÚSICA POPULAR, AH, EU TE AMO. AH, EU TE AMO, MEU AMOR.
                                              G7                                                                                            A7
AI, SANDRA ROSA MADALENA AH, AH, AH, AH. O MEU SANGUE FERVE PELA MÚSICA
            C Cm         Gm7             C         Gm7                       C     Gm7
POPULAR.OH, FRICOTE, EU FIZ XIXI, FRICOTE, EU FIZ XIXI.
          A7                    C     Cm            G F/G C/G G                                                           A G
NA MÚSICA POPULAR BRASILEIRA,                    CORRE QUE LÁ VEM OS HÔMI!

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
C = X 3 2 0 1 0
C#7 = X 4 3 4 2 X
C/G = 3 3 2 X 1 X
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
F/G = 3 X 3 2 1 X
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
Gm7 = 3 X 3 3 3 X
