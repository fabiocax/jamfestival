Rita Lee - Balada do Louco  (Acústico MTV)

F         Fm          C/G
Dizem que sou louco
F           Fm     C/G
Por pensar assim
F                 Fm     C/G
Se eu sou muito louco
F          Fº
Por eu ser feliz
Am   Am/G   Am/F#    F7+
Mas louco é quem me diz
             C/G
E não é feliz
G4/7  G7     C/G
Não    é    feliz

F              Fm     C/G
Se eles são bonitos
F           Fm         C/G
Eu sou Sharon Stone
F            Fm     C/G
Se eles são famosos

F        Fº
I Am Rolling Stone
Am   Am/G   Am/F#    F7+
Mas louco é quem me diz
      C/G
E não é feliz
G4/7   G7     C/G   C7
Não    é   feliz

                  F          C/G
Eu juro que é melhor
                         F  C/G
Não ser um normal
                          D
Se eu posso pensar
                       G   G4   G5
Que Deus sou eu

F           Fm       C/G
Se eles têm três carros
F      Fm        C/G
Eu posso voar
F               Fm     C/G
Se eles rezam muito
F               Fº
Eu já estou no céu
Am   Am/G    Am/F#   F7+
Mas louco é quem me diz
         C/G
E não é feliz
G4/7   G7   C/G  C7
Não    é   feliz

                  F          C/G
Eu juro que é melhor
                         F  C/G
Não ser um normal
                           D
Se eu posso pensar
                      G   G4   G5
Que Deus sou eu

F                 Fm     C/G
Sim, sou muito louco
F                Fm    C/G
Não vou me curar
F             Fm     C/G
Já não sou o único
F                Fº
Que encontrou a paz
Am   Am/G    Am/F#   F7+
Mas louco é quem me diz
         C/G
E não é feliz
G            C
Eu sou feliz!

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am/F# = 2 X 2 2 1 0
Am/G = 3 X 2 2 1 X
C = X 3 2 0 1 0
C/G = 3 3 2 X 1 X
C7 = X 3 2 3 1 X
D = X X 0 2 3 2
F = 1 3 3 2 1 1
F7+ = 1 X 2 2 1 X
Fm = 1 3 3 1 1 1
Fº = X X 3 4 3 4
G = 3 2 0 0 0 3
G4 = 3 5 5 5 3 3
G4/7 = 3 5 3 5 3 X
G5 = 3 5 5 X X X
G7 = 3 5 3 4 3 3
