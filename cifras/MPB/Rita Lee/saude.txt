Rita Lee - Saúde

Intro: E4   E   E4   E   D4   D   D4   D

D            A
Me cansei de lero-lero
E                    A     G/B   Cº  A7/C#
   Dá licença mas eu vou sair do sério
D         A
Quero mais saúde
E                   D        A
   Me cansei de escutar opiniões
   E           E7      A7/4   A7
De como ter um mundo melhor
       D           D#º
Mas ninguém sai de cima
      A         F#
Nesse chove não molha
   Bm       C#m
Eu sei que agora
   D        C#7/4       F#m   F#m7+   F#m7   Eb7/5-  Dm7   F/G
Eu vou é cuidar mais de mim
A7+   F/G

F#7/4   E7/4
D         A
Como vai? Tudo bem
E                 A   G/B Cº    A7/C#
   Apesar contudo todavia mas porém
   D                    A
As águas vão rolar, não vou chorar
E                  D          A
   Se por acaso morrer do coração
E           E7     A7/4   A7
É sinal que amei demais
      D            D#º
Mas enquanto estou viva
A        F#
Cheia de graça
   Bm        C#m
Talvez ainda faça
   D        C#7/4   F#m   F#m7+   F#m7   Eb7/5-  Dm7   F/G
Um monte de gente feliz
A7+   F/G   A7+

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
A7/4 = X 0 2 0 3 0
A7/C# = X 4 5 2 5 X
Bm = X 2 4 4 3 2
C#7/4 = X 4 6 4 7 4
C#m = X 4 6 6 5 4
Cº = X 3 4 2 4 2
D = X X 0 2 3 2
D#º = X X 1 2 1 2
D4 = X X 0 2 3 3
Dm7 = X 5 7 5 6 5
E = 0 2 2 1 0 0
E4 = 0 2 2 2 0 0
E7 = 0 2 2 1 3 0
E7/4 = 0 2 0 2 0 X
Eb7/5- = X X 1 2 2 3
F# = 2 4 4 3 2 2
F#7/4 = 2 4 2 4 2 X
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
F#m7+ = X X 4 6 6 5
F/G = 3 X 3 2 1 X
G/B = X 2 0 0 3 3
