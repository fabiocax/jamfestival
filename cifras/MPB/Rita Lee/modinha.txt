Rita Lee - Modinha

INTRODUÇÃO:G
C                                                                                                      G/B
QUEM É QUE PODE SER GIGANTE, NESSE MUNDO TÃO PEQUENO.
G C                                                                                                              G/B
    COMO É QUE FAZ PRA GENTE, SER FELIZ E RICO AO MESMO TEMPO.
               Em     A7                         D     D/C D/B D/A
EU NÃO SEI, MAS EU VOU TENTAR.

G C                                                                                                         G/B
  TODO REMÉDIO QUE ME CURA, TEM UMA CONTRA-INDICAÇÃO.

  O QUE FAZ BEM PRA ALMA, PODE FAZER MAL PRO CORAÇÃO.
                    Em    A7                        D   D/C D/B D/A
DE QUEM TEM PRESSA DE CHEGAR.

G        D                                          G4 G  D                                    G
HUM! AI, QUEM ME DERA UM DIA    FICAR DE PAPO PRO AR.
C                     G/B     Bb          A4/7 A7 Am7 D4 D
TIRAR UM SOM NUMA VIOLA.
G          C                                                                                                       G/B
HUM! E QUANTO MAIS A GENTE GANHA, MAIS A GENTE VAI PERDER.

G C                                                                                                 G/B
  PORQUE ESSA VIDA TÁ FICANDO UM OSSO DURO DE ROER.
        Em           A7                    D D/C D/B D/A
E ENTÃO  ACHO BOM LEMBRAR.
G C                                                                                                 G/B
  QUE O PASSARINHO DA GAIOLA NÃO ESQUECE DE CANTAR.
G C                                                                                                      G/B
  QUE UMA CRIANÇA NUNCA BRINCA SE ELA APRENDE A BRINCAR
   Em              A7                      D D/C D/B D/A
E AMAR COMO TEM QUE SER.

G         D                                       G4 G    D                                    G
HUM! AI QUEM ME DERA UM DIA,     FICAR DE PAPO PRO AR.
C                   G/B      Bb            A4/7  A7 Am7 D4 D
TIRANDO UM SOM NUMA VIOLA.
C                   G/B       Bb           A4/7 A7 Am7 D4 D
TIRANDO UM SOM NUMA VIOLA.
C                   G/B        Bb    A4/7 A7 Am7 D4 D
TIRANDO UM SOM.

----------------- Acordes -----------------
A4/7 = X 0 2 0 3 0
A7 = X 0 2 0 2 0
Am7 = X 0 2 0 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/A = X 0 X 2 3 2
D/B = X 2 0 2 3 2
D/C = X 3 X 2 3 2
D4 = X X 0 2 3 3
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G4 = 3 5 5 5 3 3
