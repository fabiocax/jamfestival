Rita Lee - Refestança

INTRODUÇÃO: (G Bm D)

D                                                C
TCHÁ, TCHÁ, TCHÁ, TCHÁ, TCHÁ, TCHÁ, TCHÁ.
D                                               C                                  D
TCHÁ, TCHÁ,TCHÁ, TCHÁ, TCHÁ, TCHÁ, TCHÁ, É, É, É.
G7
REFESTANÇA DANÇA, DANÇA, DANÇA, DANÇA.

QUEM PODE DANÇAR.
C7
REFESTANÇA CANTA, CANTA, CANTA, CANTA.

QUEM PODE CANTAR.
D         G
NA HORA QUE AGORA QUANDO A BANDA TOCAR.
E7
SENHORAS E SENHORES, CRIANÇAS VAMOS VOAR.
 A
VOAR, VOAR, PODEM DESATAR OS CINTOS DE SEGURANÇA.
   D
QUE A ESPERANÇA, QUE A BONANÇA.


QUE A VONTADE É AMAR, AH, AH, AH.
G7
REFESTANÇA DANÇA, DANÇA, DANÇA, DANÇA.

QUEM PODE DANÇAR.
C7
REFESTANÇA CANTA, CANTA, CANTA, CANTA.

QUEM PODE CANTAR.
C7                                                  F
SÓ NÃO PODE QUEM NÃO QUISER.
A#                                                  D#
VER QUE O CÉU DA TERRA É AZUL.
G#
VER QUE O VERDE É VERDE.
G
QUE A VIDA VIAJA.
 C                                                D
QUE COM A VIDA A GENTE VAI, VAI, VAI, VAI.

ESTRIBILHO

----------------- Acordes -----------------
A = X 0 2 2 2 0
A# = X 1 3 3 3 1
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
D = X X 0 2 3 2
D# = X 6 5 3 4 3
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
G7 = 3 5 3 4 3 3
