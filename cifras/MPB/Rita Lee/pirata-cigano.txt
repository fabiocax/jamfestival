Rita Lee - Pirata Cigano

INTRODUÇÃO: E7+ F#m7 G#m7 Am7 D7 G7+ C7+ B4/7 B7

E7+ F#m7               G#m7            F#m7                E7+
SE VIVER É NAVEGAR, MEU BARCO LÁ SE FOI.
       D7            C#7          F#m7   B7                F#m7   B7
NA CORRENTEZA DA CHUVA, NUMA GARUPA.
                                D7    C#7                                  F#7       B7
QUE O VENTO LEVOU PRA BEM LONGE DO TEMPO.

E7+     F#m7                G#m7       F#m7                         E7+
CADA PORTO UM AMOR, EM CADA AMOR UM ADEUS.
          D7         C#7            F#m7  B7                     F#m7 B7
MEU BARCO SEGUE SOZINHO, MERO DESTINO.
                               E7+                  A7+                             G#7
EU QUERO AVISTAR DE DIA O SOL DE NOITE O FAROL.

             A7+        G#m7  C#7
SOU PIRATA CIGANO.
F#m7     B7              Bm7  E7
VELHO LOBO DO MAR.
              Am7   D7          G7+        E7
GASTO TUDO O QUE GANHO.

Am7                                   B4/7  B7  intodução
PELO PRAZER DE SONHAR!

----------------- Acordes -----------------
A7+ = X 0 2 1 2 0
Am7 = X 0 2 0 1 0
B4/7 = X 2 4 2 5 2
B7 = X 2 1 2 0 2
Bm7 = X 2 4 2 3 2
C#7 = X 4 3 4 2 X
C7+ = X 3 2 0 0 X
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
E7+ = X X 2 4 4 4
F#7 = 2 4 2 3 2 2
F#m7 = 2 X 2 2 2 X
G#7 = 4 6 4 5 4 4
G#m7 = 4 X 4 4 4 X
G7+ = 3 X 4 4 3 X
