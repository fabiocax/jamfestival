Rita Lee - A Gripe do Amor

               C7M             C°             Em7(b5)     A7
Não há vacina, nem vitamina, pega só de olhar
                      Dm7                       Fm6  Bb7                 Em7 Gm7 C7(9)
Não tem benzedeira, chá de erva cidreira       capaz de curar
                    F7M  Fm6                  Em7 Eb°                Dm7   G7
A gripe do amor,          a gripe do amor,      a gripe do amor

                    C7M                  C°               Em7(b5)    A7
Causa dependência, provoca furor, manemolência
                       Dm7   Fm6  Bb7             Em7    Gm7 C7(9)
O coração não bate, apanha, a alma faz manha, uh!

              F7M              Fm6        Em7           A7
Peguei a gripe do amor,     não adianta doutor
                 Dm7                             G7                               Gm7 C7(9)
to tendo piripaque, por favor não me salve, quero morrer de amor

G#6 G#7 (modula para C#7M)

                   C#7M                 C#°                Fm7(b5)  A#7
Se você me ama, vem ficar de cama, vem cuidar de mim

                 D#m7                    F#m6 B7             Fm7 G#m7 C#7(9)
Por uma semana, um mês, um ano,        a vida inteira

              F#7M             F#m6      Fm7          Bb7
Peguei a gripe do amor,     não adianta doutor
                 D#m7                           G#7                             G#m7 C7(9)
to tendo piripaque, por favor não me salve, quero morrer de amor

----------------- Acordes -----------------
A#7 = X 1 3 1 3 1
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
Bb7 = X 1 3 1 3 1
C#7(9) = X 4 3 4 4 X
C#7M = X 4 6 5 6 4
C#° = X 4 5 3 5 3
C7(9) = X 3 2 3 3 X
C7M = X 3 2 0 0 X
C° = X 3 4 2 4 2
D#m7 = X X 1 3 2 2
Dm7 = X 5 7 5 6 5
Eb° = X X 1 2 1 2
Em7 = 0 2 2 0 3 0
Em7(b5) = X X 2 3 3 3
F#7M = 2 X 3 3 2 X
F#m6 = 2 X 1 2 2 X
F7M = 1 X 2 2 1 X
Fm6 = 1 X 0 1 1 X
Fm7 = 1 X 1 1 1 X
Fm7(b5) = X X 3 4 4 4
G#6 = 4 X 3 5 4 X
G#7 = 4 6 4 5 4 4
G#m7 = 4 X 4 4 4 X
G7 = 3 5 3 4 3 3
Gm7 = 3 X 3 3 3 X
