Rita Lee - Lança Perfume

D       Bm7          Em7          A7    D      Bm7
Lança menina   Lança todo esse perfume, desbaratina
       Em7        A7
Não dá pra ficar imune
F       Dm7         Gm7          C7   A7/4(9)  A7(9)
Ao teu amor que tem cheiro de coisa maluca,
D           Bm7
Vem cá, meu bem
      Em7       A7    D        Bm7
Me descola um carinho eu sou neném
      Em7         A7
Só sossego com beijinho
F        Dm7     Gm7        C7       A7/4(9)
Vê se me dá o prazer de ter prazer comigo
           Ab7(#11)
Me aqueça
G7M             D7M
     Me vira de ponta cabeça
G7M            D7M      D7
     Me faz de gato e sapato
Gm7  C7       F         Dm7
E me deixa de quatro no ato

Gm7     C7        F        Em7
     Me enche de amor, de amor
A7   Em7    A7   Em7  A7       D
     Lança,           lança perfume
               Em7    A7   Em7  A7       D
Oh, oh, oh, oh lança,           lança perfume
               Em7    A7  Em7    A7       D
Oh, oh, oh, oh lança,     lança, lança perfume
Em7   A7   Em7   A7   B7/4(9)
Lan---ça         per--fume

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7(9) = 5 X 5 4 2 X
A7/4(9) = 5 X 5 4 3 X
Ab7(#11) = 4 X 4 5 3 X
B7/4(9) = X 2 2 2 2 2
Bm7 = X 2 4 2 3 2
C7 = X 3 2 3 1 X
D = X X 0 2 3 2
D7 = X X 0 2 1 2
D7M = X X 0 2 2 2
Dm7 = X 5 7 5 6 5
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
G7M = 3 X 4 4 3 X
Gm7 = 3 X 3 3 3 X
