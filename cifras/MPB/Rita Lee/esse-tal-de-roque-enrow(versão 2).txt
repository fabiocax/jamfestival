﻿Rita Lee - Esse Tal De Roque Enrow

Capo Casa 2

Rita Lee / Paulo Coelho

[Intro:] A G A G D E Eb D

     A
Ela nem vem mais pra casa, Doutor
      G
Ela odeia meus vestidos
       A
Minha filha é um caso sério, Doutor
      G
Ela agora está vivendo com esse tal de
 D            E    Eb  D    E    Eb
Roque enrow, roque en row, Roque en,

Ela não fala comigo, Doutor
Quando ele está por perto,
É um menino tão sabido, Doutor,
Ele quer modificar o mundo
Esse tal de roque enrow, roque en row


 E   A    G   F#      G   F# G
Ro quem é ele? Quem é ele?
G     F#     G       D
Esse tal de roque enrow?
     G   F#       G  F# G    F#    G     D
Uma mosca, um mistério, uma moda que passou,
G  F#       G F# G             Bb A  G E Eb
Ele, quem é ele? Isso ninguém nunca falou,

Ela não quer ser tratada, Doutor,
E não pensa no futuro,
E minha filha está solteira, Doutor,
Ela agora está lá na sala com esse tal de
Roque enrow, roque en row, Roque en,

Eu procuro estar por dentro, Doutor,
Dessa nova geração
Mas minha filha não me leva à sério, Doutor,
Ela fica cheia de mistério com esse tal de
Roque enrow, roque en row

Ro quem é ele? Quem é ele?
Esse tal de roque enrow?
Um planeta, um deserto, uma bomba que estourou
Ele, quem é ele? Isso ninguém nunca falou

Ela dança o dia inteiro, Doutor
E só estuda pra passar,
E já fuma com essa idade, Doutor
Desconfio que não há mais cura pra esse tal de
Roque enrow, roque en row,
 E    Eb D    E    C   D
Roque enrow, roque en row,

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Eb = X 6 5 3 4 3
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
