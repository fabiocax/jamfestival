Isadora Pompeo - Minha Morada

[Intro]  Em7  C9  Em7  C9

[Primeira Parte]

 Em7      C9
Jesus, cadê você?
 Em7      C9
Jesus, pode entrar
 Em7       C9                 G
Jesus, só vou ficar se você vier
          D/F#
Se você vier

[Pré-Refrão]

Em7
Olho para mim
              C9
E vejo um pequeno mortal
Em7
Olho para dentro e penso

      C9
Não quero ser igual
Em7
Olho para minhas mãos
    C9
Cansei do natural
Em7
Olho pro meu coração
            C9              D
Eu busco o sobrenatural de Deus

[Refrão]

 Em7
Toma as minhas mãos
    C9
Eu não preciso delas
        G
Se não for pra te tocar
   D/F#
Pode ficar com elas
 Em7
Toma meu coração
    C9
Eu não preciso dele
        G
Se não for pra te sentir
 D
Tira ele de mim

( Em7  C9  G  D/F# )

[Primeira Parte]

 Em7      C9
Jesus, cadê você?
 Em7      C9
Jesus, pode entrar
 Em7       C9                 G
Jesus, só vou ficar se você vier

[Pré-Refrão]

Em7
Olho para mim
              C9
E vejo um pequeno mortal
Em7
Olho para dentro e penso
      C9
Não quero ser igual
Em7
Olho para minhas mãos
    C9
Cansei do natural
Em7
Olho pro meu coração
            C9              D
Eu busco o sobrenatural de Deus

[Refrão]

 Em7
Toma as minhas mãos
    C9
Eu não preciso delas
        G
Se não for pra te tocar
   D/F#
Pode ficar com elas
 Em7
Toma meu coração
    C9
Eu não preciso dele
        G
Se não for pra te sentir
 D
Tira ele de mim
 Em7
Toma as minhas mãos
    C9
Eu não preciso delas
        G
Se não for pra te tocar
   D/F#
Pode ficar com elas
 Em7
Toma meu coração
    C9
Eu não preciso dele
        G
Se não for pra te sentir
 D
Tira ele de mim

( Em7  C9  G  D/F# )

[Ponte]

                      Em7
Eu encontrei minha morada
                C9
Eu amo minha morada
               G   D/F#
Tu és minha morada
                      Em7
Eu encontrei minha morada
                C9
Eu amo minha morada
               G   D/F#
Tu és minha morada

[Refrão]

 Em7
Toma as minhas mãos
    C9
Eu não preciso delas
        G
Se não for pra te tocar
   D/F#
Pode ficar com elas
 Em7
Toma meu coração
    C9
Eu não preciso dele
        G
Se não for pra te sentir
 D
Tira ele de mim
 Em7
Toma as minhas mãos
    C9
Eu não preciso delas
        G
Se não for pra te tocar
   D/F#
Pode ficar com elas
 Em7
Toma meu coração
    C9
Eu não preciso dele
        G
Se não for pra te sentir
 D
Tira ele de mim

[Final] Em7  C9  G  D/F#
        Em7  C9  G  D/F#

----------------- Acordes -----------------
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
