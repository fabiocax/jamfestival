﻿Isadora Pompeo - Preciso entender

Capo Casa 1

[Intro]  G  D/F#  C

     G                D/F#           C
Só quero entender o que devo fazer, Jesus
  G              D                  C
Preciso entender o que devo fazer, Jesus

 G                          D
Tenho caminhado em tua direção
                               Em7
Buscando saber o que fazer ou não
                                         C
Me perco em meus passos e não consigo me achar
                            G
Então eu choro e começo a clamar
                                  D/F#
Eu não quero que a minha vontade vença
                                  Em7
Mas quero que somente a tua prevaleça
                                    C
Mas eu preciso caminhar para algum lugar

Me diz Deus aonde eu devo estar

     G                D              C
Só quero entender o que devo fazer, Jesus
  G              D                  C
Preciso entender o que devo fazer, Jesus

 G
Meu pensamento fala tanto
        D/F#
E a minha voz está no canto
         C
Pois eu canto a ti
     G
Meu sentimento não entende
          D
Mas minha alma compreende
         C
Pois pertenço a ti
    G
As lutas vem chegando tentando me afogar
D
As incertezas vem tentado me derrubar
       Em7         C
O que fazer? Como vencer? Eu não consigo entender

     G                D/F#           C
Só quero entender o que devo fazer, Jesus
    G            D                  C
Preciso entender o que devo fazer, Jesus

----------------- Acordes -----------------
Capotraste na 1ª casa
C*  = X 3 2 0 1 0 - (*C# na forma de C)
D*  = X X 0 2 3 2 - (*D# na forma de D)
D/F#*  = 2 X 0 2 3 2 - (*D#/G na forma de D/F#)
Em7*  = 0 2 2 0 3 0 - (*Fm7 na forma de Em7)
G*  = 3 2 0 0 0 3 - (*G# na forma de G)
