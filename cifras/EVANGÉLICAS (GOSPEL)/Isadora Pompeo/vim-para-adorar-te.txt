Isadora Pompeo - Vim Para Adorar-te

D      A       Em     G
Luz do mundo vieste a terra
D             A        G
Para que eu pudesse te ver
D     A       Em        G
Tua beleza me leva a adorar-te
D        A      G
Quero contigo viver

            D
Vim para adora-te
             A
Vim para prostrar-me
           D              G
Vim para dizer que és meu Deus
               D                A
És totalmente amável,totalmente digno
          D             G
Tão maravilhoso és para mim

D      A       Em         G
Eterno Rei exaltado nas alturas

D    A      G
Glorioso no céu
  D       A      Em          G
Humilde vieste a terra que criaste
D        A        G
Por amor pobre se fez
   Em    F#m G              Em     F#m   G
Eu nunca saberei o preço dos meus pecados lá na cruz

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
