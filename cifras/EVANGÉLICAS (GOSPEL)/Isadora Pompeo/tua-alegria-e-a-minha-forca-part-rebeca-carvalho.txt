Isadora Pompeo - Tua Alegria É a Minha Força (part. Rebeca Carvalho)

[Intro] E  B4  C#m7  A9

[Primeira Parte]

E            B4
 Pai, eu sei
                         C#m7
Tua presença acrescenta paz
                         A9
Não há espaço para confusão
E          B4
 Pai, então
                                C#m7
Pra que me preocupar com a aflição?
                            A9
E com as dores do meu coração?

[Refrão]

                             E
Se tua presença só traz alegria

                       B4
Tua alegria é a minha força
                       C#m7
Tua alegria é a minha força
                       A9
Tua alegria é a minha força
                             E
Se tua presença só traz alegria
                       B4
Tua alegria é a minha força
                       C#m7
Tua alegria é a minha força
                       A9
Tua alegria é a minha força

[Primeira Parte]

E           B4
 Pai, eu sei
                         C#m7
Tua presença acrescenta paz
                         A9
Não há espaço para confusão
E          B4
 Pai, então
                                C#m7
Pra que me preocupar com a aflição?
                           A9
E com as dores do meu coração?

[Refrão]

                             E
Se tua presença só traz alegria
                       B4
Tua alegria é a minha força
                       C#m7
Tua alegria é a minha força
                       A9
Tua alegria é a minha força
                             E
Se tua presença só traz alegria
                       B4
Tua alegria é a minha força
                       C#m7
Tua alegria é a minha força
                       A9
Tua alegria é a minha força

                             E
Se tua presença só traz alegria
                       B4
Tua alegria é a minha força
                       C#m7
Tua alegria é a minha força
                       A9
Tua alegria é a minha força
                             E
Se tua presença só traz alegria
                       B4
Tua alegria é a minha força
                       C#m7
Tua alegria é a minha força
                       A9
Tua alegria é a minha força

[Ponte]

E      B4     C#m7     A9
 Santo, santo,     santo
E      B4     C#m7     A9
 Santo, santo,     santo

          E                        B4
Quero entrar onde os anjos cantam santo
                      C#m7
Onde os anjos cantam santo
                      A9
Onde os anjos cantam santo
          E                        B4
Quero entrar onde os anjos cantam santo
                      C#m7
Onde os anjos cantam santo
                      A9
Onde os anjos cantam santo

[Refrão]

                             E
Se tua presença só traz alegria
                       B4
Tua alegria é a minha força
                       C#m7
Tua alegria é a minha força
                       A9
Tua alegria é a minha força
                             E
Se tua presença só traz alegria
                       B4
Tua alegria é a minha força
                       C#m7
Tua alegria é a minha força
                       A9
Tua alegria é a minha força

----------------- Acordes -----------------
A9 = X 0 2 2 0 0
B4 = X 2 4 4 5 2
C#m7 = X 4 6 4 5 4
E = 0 2 2 1 0 0
