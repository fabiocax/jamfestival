﻿Isadora Pompeo - O Nome de Jesus

Capo Casa 3

           Em     C         G     D
Em teus braços   eu  vou me entregar
          Em  C             G       D
Os meus medos eu deixo aos pés do altar
           Em   C                  G     D
Todos os planos meus, hoje eu entrego a ti
         Em   C              G     D
Toma o teu lugar, Tu és bem vindo aqui
 C
Vem sobre nós
                 Em
Manda tua chuva nesse lugar
D                  C
Em tua presença  vou me derramar
                     G     D
E declarar que és Santo, Deus
C
O som do céu vamos ouvir
 Em              D
Sopra teu vento suave hoje aqui
 C
Vou declarar

                  G   D/F#
O único nome que salva
    Em  C  G  D
Jesus

           Em  C            G     D
Em teus braços eu    vou me entregar
   Em         C             G       D
Os meus medos eu deixo aos pés do altar
           Em    C               G     D
Todos os planos meus, hoje eu entrego a ti
   Em         C              G     D
Toma o teu lugar, Tu és bem vindo aqui
 C
Vem sobre nós
                 Em
Manda tua chuva nesse lugar
D                    C
Em tua presença  vou me derramar
                     G     D
E declarar que és Santo, Deus
C
O som do céu vamos ouvir
 Em              D
Sopra teu vento suave hoje aqui
 C                              G D
Vou declarar o único nome que salva
           C
O nome de Jesus,
          Em     D
o nome de Jesus
          C
O nome de Jesus,
           Em    D
 o nome de Jesus
            C
O nome de Jesus,
          Em     D
o nome de Jesus
           C
O nome de Jesus,
           Em    D
 o nome de Jesus

 C
Vem sobre nós
                 Em
Manda tua chuva nesse lugar
D                    C
Em tua presença  vou me derramar
                     G     D
E declarar que és Santo, Deus
C
O som do céu vamos ouvir
 Em              D
Sopra teu vento suave hoje aqui
 C                              G  D
Vou declarar o único nome que salva
   Em  C  G  D
Jesus

----------------- Acordes -----------------
Capotraste na 3ª casa
C*  = X 3 2 0 1 0 - (*D# na forma de C)
D*  = X X 0 2 3 2 - (*F na forma de D)
Em*  = 0 2 2 0 0 0 - (*Gm na forma de Em)
G*  = 3 2 0 0 0 3 - (*A# na forma de G)
G/B*  = X 2 0 0 3 3 - (*A#/D na forma de G/B)
