Isadora Pompeo - Minha Morada

[Intro]  Em   C   Em   C

[Primeira Parte]

 Em       C
Jesus, cadê você?
 Em       C
Jesus, pode entrar
 Em        C                  G
Jesus, só vou ficar se você vier
          D
Se você vier

[Pré-Refrão]

Em
Olho para mim
              C
E vejo um pequeno mortal
Em
Olho para dentro e penso

      C
Não quero ser igual
Em
Olho para minhas mãos
    C
Cansei do natural
Em
Olho pro meu coração
            C               D
Eu busco o sobrenatural de Deus

[Refrão]

 Em
Toma as minhas mãos
    C
Eu não preciso delas
        G
Se não for pra te tocar
   D
Pode ficar com elas
 Em
Toma meu coração
    C
Eu não preciso dele
        G
Se não for pra te sentir
 D
Tira ele de mim

( Em   C   G  D    )

[Primeira Parte]

 Em       C
Jesus, cadê você?
 Em       C
Jesus, pode entrar
 Em        C                  G
Jesus, só vou ficar se você vier

[Pré-Refrão]

Em
Olho para mim
              C
E vejo um pequeno mortal
Em
Olho para dentro e penso
      C
Não quero ser igual
Em
Olho para minhas mãos
    C
Cansei do natural
Em
Olho pro meu coração
            C               D
Eu busco o sobrenatural de Deus

[Refrão]

 Em
Toma as minhas mãos
    C
Eu não preciso delas
        G
Se não for pra te tocar
   D
Pode ficar com elas
 Em
Toma meu coração
    C
Eu não preciso dele
        G
Se não for pra te sentir
 D
Tira ele de mim
 Em
Toma as minhas mãos
    C
Eu não preciso delas
        G
Se não for pra te tocar
   D
Pode ficar com elas
 Em
Toma meu coração
    C
Eu não preciso dele
        G
Se não for pra te sentir
 D
Tira ele de mim

( Em   C   G  D    )

[Ponte]

                      Em
Eu encontrei minha morada
                C
Eu amo minha morada
               G   D
Tu és minha morada
                      Em
Eu encontrei minha morada
                C
Eu amo minha morada
               G   D
Tu és minha morada

[Refrão]

 Em
Toma as minhas mãos
    C
Eu não preciso delas
        G
Se não for pra te tocar
   D
Pode ficar com elas
 Em
Toma meu coração
    C
Eu não preciso dele
        G
Se não for pra te sentir
 D
Tira ele de mim
 Em
Toma as minhas mãos
    C
Eu não preciso delas
        G
Se não for pra te tocar
   D
Pode ficar com elas
 Em
Toma meu coração
    C
Eu não preciso dele
        G
Se não for pra te sentir
 D
Tira ele de mim

[Final] Em   C   G  D
        Em   C   G  D

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
