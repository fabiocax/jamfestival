Ministério Avivah - Graça

[Intro] B  E  G#m  E
        B  E  G#m  E

B
Sei que posso encontrar

Teus braços sempre abertos
E       G#m E
És meu pai
      B
Meus erros deixo para trás

O Teu perdão me abraça
E       G#m E
És meu pai

E                 B/D#
És tão bom pra mim, Senhor
E                     B/D#
Tua presença é o meu lar
E            B/D#
Toda culpa se desfaz

   G#m            F#
Eu encontrei Teu amor

B                C#m
Graça tão maravilhosa
                G#m
Que do céu nos alcançou
      E
No amor de Jesus
B              C#m
Rico em misericórdia
              G#m
Com bondade nos cercou
      E
No amor de Jesus

      E
Preciosa graça
  A     E
Doce o som
      E           B
Que salva o pecador
    E      E/G#
Perdido estava
   A       E
E me encontrou
      C#m     B      E
Das trevas, me salvou

A           B           C#m             E/G#
Nada vai me separar do Teu amor, do Teu amor

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
B/D# = X 6 X 4 7 7
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
