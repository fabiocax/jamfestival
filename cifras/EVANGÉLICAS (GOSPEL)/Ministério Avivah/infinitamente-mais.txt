Ministério Avivah - Infinitamente Mais

[Intro] D5  E5  D5  E5
        D5  E5  D5  E5
        D5  E5  D5  E5
        E5  D5  D5  C#5  C#5  B5
        D5  E5  D5  E5
        D5  E5  D5  E5
        D5  E5  D5  E5
        E5  Eb5  D5  Db5  B5

 E
Ainda que a figueira não floresça
D                     A
Ou que a tua luta seja longa
      E
Se as ondas do teu mar forem tão altas
      D                    A
E agitadas pelo vento derem contra a tua vida

C#m7                      D7M(9)
  Eu não sei porque escolheste amar
       E              D5  E5  D5  E5
E por nós se entregar

F#m7                          G#m7  C#m7
  Mas de uma coisa eu não esque--cerei
        D9/F#   E/G#      A      B
Que tu podes fazer,Jesus pode fazer

F#m7           C#m7   A              E
Infinitamente mais,   infinitamente mais
F#m7           C#m7   A        D     E
Infinitamente mais,   infinitamente mais

 E
Ainda que a figueira não floresça
D                     A
Ou que a tua luta seja longa
      E
Se as ondas do teu mar forem tão altas
      D                    A
E agitadas pelo vento derem contra a tua vida

C#m7                      D7M(9)
  Eu não sei porque escolheste amar
       E              D5  E5  D5  E5
E por nós se entregar
F#m7                          G#m7  C#m7
  Mas de uma coisa eu não esque--cerei
        D9/F#   E/G#      A      B
Que tu podes fazer,Jesus pode fazer

F#m7           C#m7   A              E
Infinitamente mais,   infinitamente mais
F#m7           C#m7   A        D     E
Infinitamente mais,   infinitamente mais

D7M
  Mais do que imaginamos
A/C#
  Além do que pedimos
Bm
  Se ao Senhor clamarmos
F#m
  Ele ouvirá
D7M
  Mais do que imaginamos
A/C#
  Além do que pedimos
D9/F#    E/G#
  Se ao Senhor clamarmos
A        B
  Ele ouvirá

C#m7                      D7M(9)
  Eu não sei porque escolheste amar
       E              D5  E5  D5  E5
E por nós se entregar
F#m7                          G#m7  C#m7
  Mas de uma coisa eu não esque--cerei
        D9/F#   E/G#      A      B
Que tu podes fazer,Jesus pode fazer

F#m7           C#m7   A              E
Infinitamente mais,   infinitamente mais
F#m7           C#m7   A        D     E
Infinitamente mais,   infinitamente mais

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
B = X 2 4 4 4 2
B5 = X 2 4 4 X X
Bm = X 2 4 4 3 2
C#5 = X 4 6 6 X X
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
D5 = X 5 7 7 X X
D7M = X X 0 2 2 2
D7M(9) = X 5 4 6 5 X
D9/F# = 2 X 0 2 3 0
Db5 = X 4 6 6 X X
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E5 = 0 2 2 X X X
Eb5 = X 6 8 8 X X
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
G#m7 = 4 X 4 4 4 X
