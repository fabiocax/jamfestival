Trazendo a Arca - Quando a Nuvem Move

Intro: F#m  E/F#  F#m  F#m  E  D  E  F#m  E  B  D

F#m         D              E     A
Eu só me levanto quando a nuvem move
 C#m G#m       A               B     F#m
Debaixo do manto, quando a nuvem move
               D
Vou aonde ela for

                F#m
Quando a nuvem move
                B/D#
Quando a nuvem move
 D              F#m
Quando a nuvem move
                B/D# D
Quando a nuvem move

F#m     E D        E   A
 Seguir a ti, te obedecer
         E/G# F#m      A     B     C#m
 Ouvir a Tua voz, cumprir o Teu querer
              G#m               F#m
 Ainda que eu não entenda a direção
        B
 Já decidi

         A                 B
Só me levanto quando a tua arca
               F#m
Se levanta do chão
       A                B                 F#m
Eu só sigo se a minha frente tua glória guiar

      A       B         C#
Se comigo tu fores, eu vou

Tom vai para C


Am         F              G      C
Eu só me levanto quando a nuvem move
 Em Bm       C               D     Am
Debaixo do manto, quando a nuvem move
               F
Vou aonde ela for

                Am
Quando a nuvem move
                D/F#
Quando a nuvem move
 F              Am
Quando a nuvem move
                D/F# F
Quando a nuvem move

Final Am - G - D/F#

      F  - G - Am

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
B/D# = X 6 X 4 7 7
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C# = X 4 6 6 6 4
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
E = 0 2 2 1 0 0
E/F# = X X 4 4 5 4
E/G# = 4 X 2 4 5 X
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G#m = 4 6 6 4 4 4
