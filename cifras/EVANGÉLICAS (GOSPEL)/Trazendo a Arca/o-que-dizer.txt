Trazendo a Arca - O Que Dizer

[Intro]  D7M  A7M(9)  A7M   D7M  A6/C#  A7M  A/G#
         Bm7  C#m7  Bm7  C#m7   D6(9)/A
 
       Bm7       C#m7            F#m7
O que dizer pra tocar o Teu coração 
              Bm7    C#m7             D#m7(5-)
E Te surpreender se tudo conheces de mim  
    C#m7     F#m7     Bm7   E4
Bem antes   de eu    falar 
              Bm7     C#m7                F#m7
Não tenho em mim   palavras pra Te convencer 
          Bm7    C#m7                D#m7(5-)
Se até o desejo de Te adorar vem de Ti 
       C#m7    F#m7         Bm7  Bm7(9)  E4
Sou   só       uma       criança  
      E4       Eb7(9/11+)   D7M
Me ensina o  que         fazer 
      E/G#           D#m7(5-)                E4/D       F#7(13-)
 Sinceramente eu não sei     porque me escolhestes pra Ti  
    Bm7(11)     C#m7                Bm7   E4  Eb7(9/11+)
E que valor os Teus olhos viram em mim 
 D7M     E/G#       D#m7(5-)              E4/D    F#7(13-)
Pode alguém como Tu       amar tanto alguém como eu 
     Bm7        C#m7       D6(9)/A
E me enviar pra ir em Teu nome

----------------- Acordes -----------------
A/G# = 4 X 2 2 2 X
A6/C# = X 4 2 2 5 2
A7M = X 0 2 1 2 0
A7M(9) = X 0 2 1 0 0
Bm7 = X 2 4 2 3 2
Bm7(11) = 7 X 7 7 5 X
Bm7(9) = X 2 0 2 2 X
C#m7 = X 4 6 4 5 4
D#m7(5-) = X X 1 2 2 2
D7M = X X 0 2 2 2
E/G# = 4 X 2 4 5 X
E4 = 0 2 2 2 0 0
E4/D = X 5 2 2 0 0
Eb7(9/11+) = X X 1 2 2 1
F#7(13-) = 2 X 2 3 3 2
F#m7 = 2 X 2 2 2 X
