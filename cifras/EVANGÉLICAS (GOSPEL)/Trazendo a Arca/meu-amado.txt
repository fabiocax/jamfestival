Trazendo a Arca - Meu Amado

(intro) F Am Bb A7M  Gm C7/9

F          Am
  És minha força
Bb         Bbm
  És minha rocha
F          Am
  És meu escudo
Bb         Bbm
  Minha cidadela
F          Am
  És meu socorro
Bb         Bbm
  Minha segurança
F          Am
  És meu refúgio
Bb         Bbm
  Minha fortaleza

Gm         Am     Gm        Am
És o meu amado o Cordeiro Santo

Gm7    Am      Bb        C
Santo  santo  santo és Senhor Jesus (2x)

Bb               Am
  Os meus olhos estão em Ti
Bb            Am  Gm Am   Bb
  A minha alma deseja Te adorar
                   Am
Em tua presença sou como criança
    Gm     Am
Procurando os Teus braços
    Bb       C
Pra me entregar
Bb9                        Am
  Quero encostar-me em Teu peito
Bb                   Am     Gm      Bb
  Só para ouvir as batidas do Teu coração
                   Am
E transbordando no Teu amor
Gm           Am        Bb    C
Declarar que Te amo Senhor (2x)

 F
Jesus

(intro)  F Am  Bb  Bbm C7/9
(volta para o inicio)

( F Am Bbm   Gm C7/9 )

----------------- Acordes -----------------
A7M = X 0 2 1 2 0
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bb = X 1 3 3 3 1
Bb9 = X 1 3 3 1 1
Bbm = X 1 3 3 2 1
C = X 3 2 0 1 0
C7/9 = X 3 2 3 3 X
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
