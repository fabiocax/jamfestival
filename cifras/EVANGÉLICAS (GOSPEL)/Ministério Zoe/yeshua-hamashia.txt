Ministério Zoe - Yeshua Hamashia

Dm             Bb9
Ele vem sua face brilha como sol
F7                 Am7
E como um Leão vem rugindo sobre gerações
Dm           Bb9              F7                     Am7
Quem é este, quem é este, que vem saltando sobre os montes?
Dm           Bb9              F7                    Am7
Quem é este, quem é este, que vem rugindo sobre as nações?
Dm      Bb9      F7        Am7
Yeshua, Yeshua, Yeshua Hamashia!
Dm      Bb9      F7       Am7
Yeshua, Yeshua, Yeshua Hamashia!
Dm      Bb9      F7        Am7
Yeshua, Yeshua, Yeshua Hamashia!
Dm                                     Bb9
Terra, terra, terra! Ouça a palavra do Senhor!
     F7                                   Am7
Podem vir, podem vir! Que aqui ainda tem profeta!
D5                                       Bb5
Terra, terra, terra! Ouça a palavra do Senhor!
    F5                                      A5
Podem vir, podem vir! Que aqui ainda tem profeta!

----------------- Acordes -----------------
A5 = X 0 2 2 X X
Am7 = X 0 2 0 1 0
Bb5 = X 1 3 3 X X
Bb9 = X 1 3 3 1 1
D5 = X 5 7 7 X X
Dm = X X 0 2 3 1
F5 = 1 3 3 X X X
F7 = 1 3 1 2 1 1
