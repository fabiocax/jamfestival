Ministério Zoe - O Maior Dos Tesouros

[Intro]  D/F#  Em  G  D
         D/F#  Em  G  D

     D/F#
Ele olhou pra mim e se apaixonou
     Em
Um amor sem fim
                                G         D
Que existia antes de tudo existir

   D/F#
E se manifestou, a eternidade em mim
       Em
Aquele que não tem começou ou fim
    G                        D
Se revelou em mim, e me amou

      Em
Doce sentimento
                                   G
Brotou na terra seca do meu coração

               Bm
E o que era morte e sequidão
               A
Só desespero e solidão
                     Em      G   Em   G
Hoje tenho Jesus só pra mim

     Em
Eu sou só um vaso de barro
             G
Que carrega um grande tesouro
 Bm
Só um simples vaso de barro
            A
Que carrega o maior dos tesouros
            Em                            G
E a excelência do poder que se manifesta
     Bm     A
É dele, é dele

[Solo]  Em  G  Bm  A

             Em
Eu posso ser atribulado
             G
Mas não fico angustiado
                    Bm      A
Perplexo, mas não desanimado

     Em
E quando me perseguirem
                 G
Não serei desamparado
           Bm
Mesmo que eu me sinta abatido
       A
Jamais serei destruído

                 Em
Eu morro um pouco todos os dias
             A                         G  A
Pra que a Sua vida se manifeste em mim

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
