Ministério Zoe - Primeiro Amor

Intro 2x: A E D A

A                                D9
Leva-me ao lugar onde ouvi a tua voz
           Bm7                A
Reacende a paixão do primeiro olhar
      A9                      D9
Dá-me novamente a graça de te amar
          Bm7             A       E
Só por te amar, só por te amar
F#M7    E                 D9
Faz-me lembrar a primeira vez
                   F#M7
A primeira jura de amor
            E                       D9    E
A primeira promessa (A primeira promessa)
F#M7           E            D
 Faz-me lembrar a primeira vez
                              F#M7
Que o meu coração queimou por Ti
                 E
Me perdoa se eu cresci

                 D        E
E me tornei independente
f#m7          E      D
 Quero ti amar mais
               F#M7
 E depender de Ti
       E                  D
 Quero voltar ao primeiro amor
                        F#M7 E D
Me apaixonar outra vez Senhor       REPETE 6X E FINALIZA NO A

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
Bm7 = X 2 4 2 3 2
D = X X 0 2 3 2
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
