Ministério Zoe - O Som Dos Salvos

Riff 1
E|--------------------------------------------------------|
B|--------------------------------------------------------|
G|-----------------7----------------8---------------------|  (2x)
D|--8--------------------5--------------------------------|
A|--------------------------------------------------------|
E|--------------------------------------------------------|

Riff 2 ( Bb  Gm  Bb  Gm )
E|--------------------------------------------------------|
B|--------------------------------------------------------|
G|---------------------------------10---------------------|  (4x)
D|--12-12-10-12-12-10-12-12-10-12-----12-10-12------------|
A|--------------------------------------------------------|
E|--------------------------------------------------------|

            Bb                               Gm
Eu posso escutar o som dos salvos que estão voltando pra casa
            Bb
Eu posso escutar de toda tribo, povo e raça
   Gm                     D#      Riff 1  Bb Gm D#
Uma nação se levanta pra adorar


   Bb
As coisas velhas já se passaram
  Gm
Tudo novo se fez
  D#
Não existirá mais morte ou dor
           F
E o cordeiro enxugará dos olhos toda lágrima

Riff 2 ( Bb  Gm  Bb  Gm )
E|--------------------------------------------------------|
B|--------------------------------------------------------|
G|---------------------------------10---------------------|  (4x)
D|--12-12-10-12-12-10-12-12-10-12-----12-10-12------------|
A|--------------------------------------------------------|
E|--------------------------------------------------------|

   Bb            Gm
E paz, pra sempre reinará
               Bb         Gm  D#
Paz, pra sempre reinará

Bb     Gm
Oh oh oh
      D#                    F
É o canto de liberdade na presença do meu Salvador
Bb     Gm
Oh oh oh
       Gm                         F
É o grito de admiração diante da beleza do grande Eu Sou

Bb
Nova Jerusalém que me espera
Gm
Meu verdadeiro Lar
         D#                      F
Ruas de ouro, Mar de Cristal e a arvore da vida
Bb                                    Gm
Junto com os querubins eu cantarei na presença do rei
          D#                          F
Junto com os querubins eu dançarei na presença do rei

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
D# = X 6 5 3 4 3
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
