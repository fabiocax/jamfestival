Ministério Zoe - Beija-me Com Tua Glória

[Intro] A  B  A  G#m7

A            B
A tua face queremos ver
 A              G#m7
Tua presença, queremos ter

       A          B          A         G#m7
Então vem, amado vem, então vem,amado vem
       A          B          A         G#m7
Então vem, amado vem, então vem,amado vem

( A  B  A  G#m7 )
( A  B  A  G#m7 )

A            B
A tua face queremos ver
 A              G#m7
Tua presença, queremos ter

       A          B          A         G#m7
Então vem, amado vem, então vem,amado vem

       A          B          A         G#m7
Então vem, amado vem, então vem,amado vem

       A                 B
Vem e beija-me com tua glória
       A                 G#m7
Vem e beija-me com tua glória

       A                 B
Vem e beija-me com tua glória
       A                 G#m7
Vem e beija-me com tua glória

 A               B
Deixa teu rio fluir
                    C#m
Deixa teu vento soprar
                    G#m7
Deixa teu fogo queimar
             A
Até me consumir

                 B
Deixa teu rio fluir
                    C#m
Deixa teu vento soprar
                    G#m7
Deixa teu fogo queimar
             A
Até me consumir

    A               B
De dentro pra fora vem queimar em mim
    A                G#m7
De dentro pra fora até me consumir

    A               B
De dentro pra fora vem queimar em mim
    A                G#m7
De dentro pra fora até me consumir

    A               B
De dentro pra fora vem queimar em mim
    A                G#m7
De dentro pra fora até me consumir

    A               B
De dentro pra fora vem queimar em mim
    A                G#m7
De dentro pra fora até me consumir

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
G#m7 = 4 X 4 4 4 X
