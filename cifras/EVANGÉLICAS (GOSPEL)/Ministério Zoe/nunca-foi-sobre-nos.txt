﻿Ministério Zoe - Nunca Foi Sobre Nós

Capo Casa 3

[Intro] C  G/B  Am7  G4  G

C                       G/B
Oh profundidade das riquezas
Am7                        G4       G
 Da sabedoria e do conhecimento de Deus
C                         G/B
 Quão insondáveis são os Teus juízos
             Am7              G4    G
Quão impenetráveis os Teus caminhos

C                          G/B
 Quem pois conheceu a mente do Senhor?
Am7                      G4    G
Ou quem foi o Seu conselheiro?
C                     G/B
Ou quem primeiro deu a Ele
 Am7            G4   G
Para ser restituído?

        C
Porque dEle, Por meio dEle

     Em                F9       G
Para Ele são todas as coisas
        C
Porque dEle, Por meio dEle
     Em                F9       G
Para Ele são todas as coisas

            C
Não, Nunca foi sobre nós
              Em
Nem sobre o que podemos fazer
           F9                         G
É tudo sobre Você, Tudo para Você, Jesus
            C
Não, Nunca foi sobre nós
              Em
Nem sobre o que podemos fazer
           F9                         G
É tudo sobre Você, Tudo para Você, Jesus

      C
Quem sou eu?
            Em
E o que eu tenho pra Te oferecer?
        F9                            G
É tudo sobre Você, Tudo para Você, Jesus
      C
Quem sou eu?
            Em
E o que eu tenho pra Te oferecer?
        F9                            G
É tudo sobre Você, Tudo para Você, Jesus

          C
Eu descobri que sem Ti
                Em
Sem Ti eu nada posso fazer
        F9                            G
É tudo sobre Você, Tudo para Você, Jesus
          C
Eu descobri que sem Ti
                Em
Sem Ti eu nada posso fazer
        F9                            G
É tudo sobre Você, Tudo para Você, Jesus

        C
Você é Santo, santo, santo
          Em
Só Tu és santo, santo, santo
        F9                            G
É tudo sobre Você, Tudo para Você, Jesus
        C
Você é Santo, santo, santo
          Em
Só Tu és santo, santo, santo
        F9                            G
É tudo sobre Você, Tudo para Você, Jesus

        C
Você é Santo, santo, santo
          Em
Só Tu és santo, santo, santo
        F9                            G
É tudo sobre Você, Tudo para Você, Jesus
        C
Você é Santo, santo, santo
          Em
Só Tu és santo, santo, santo
        F9                            G
É tudo sobre Você, Tudo para Você, Jesus

( C  Em  F9  G )

   C       Em
Hosana, hosana
   F9                 G
Hosana nas maiores alturas
   C       Em
Hosana, hosana
   F9                 G
Hosana nas maiores alturas
   C       Em
Hosana, hosana
   F9                 G
Hosana nas maiores alturas
   C       Em
Hosana, hosana
   F9                 G
Hosana nas maiores alturas

        C
Porque dEle, Por meio dEle
     Em                F9     G
Para Ele são todas as coisas
        C
Porque dEle, Por meio dEle
     Em                F9     G
Para Ele são todas as coisas
        C
Porque dEle, Por meio dEle
     Em                F9     G
Para Ele são todas as coisas
        C
Porque dEle, Por meio dEle
     Em                F9     G
Para Ele são todas as coisas]

            C
Não, Nunca foi sobre nós

----------------- Acordes -----------------
Capotraste na 3ª casa
Am7*  = X 0 2 0 1 0 - (*Cm7 na forma de Am7)
C*  = X 3 2 0 1 0 - (*D# na forma de C)
Em*  = 0 2 2 0 0 0 - (*Gm na forma de Em)
F9*  = 1 3 5 2 1 1 - (*G#9 na forma de F9)
G*  = 3 2 0 0 0 3 - (*A# na forma de G)
G/B*  = X 2 0 0 3 3 - (*A#/D na forma de G/B)
G4*  = 3 5 5 5 3 3 - (*A#4 na forma de G4)
