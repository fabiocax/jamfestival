Ministério Zoe - Nunca Foi Sobre Nós

[Intro] Eb  Bb/D  Cm7  Bb4  Bb

Eb                       Bb/D
Oh profundidade das riquezas
Cm7                        Bb4       Bb
 Da sabedoria e do conhecimento de Deus
Eb                         Bb/D
 Quão insondáveis são os Teus juízos
             Cm7              Bb4    Bb
Quão impenetráveis os Teus caminhos

Eb                          Bb/D
 Quem pois conheceu a mente do Senhor?
Cm7                      Bb4    Bb
Ou quem foi o Seu conselheiro?
Eb                     Bb/D
Ou quem primeiro deu a Ele
 Cm7            Bb4   Bb
Para ser restituído?

        Eb
Porque dEle, Por meio dEle

     Gm                Ab9       Bb
Para Ele são todas as coisas
        Eb
Porque dEle, Por meio dEle
     Gm                Ab9       Bb
Para Ele são todas as coisas

            Eb
Não, Nunca foi sobre nós
              Gm
Nem sobre o que podemos fazer
           Ab9                         Bb
É tudo sobre Você, Tudo para Você, Jesus
            Eb
Não, Nunca foi sobre nós
              Gm
Nem sobre o que podemos fazer
           Ab9                         Bb
É tudo sobre Você, Tudo para Você, Jesus

      Eb
Quem sou eu?
            Gm
E o que eu tenho pra Te oferecer?
        Ab9                            Bb
É tudo sobre Você, Tudo para Você, Jesus
      Eb
Quem sou eu?
            Gm
E o que eu tenho pra Te oferecer?
        Ab9                            Bb
É tudo sobre Você, Tudo para Você, Jesus

          Eb
Eu descobri que sem Ti
                Gm
Sem Ti eu nada posso fazer
        Ab9                            Bb
É tudo sobre Você, Tudo para Você, Jesus
          Eb
Eu descobri que sem Ti
                Gm
Sem Ti eu nada posso fazer
        Ab9                            Bb
É tudo sobre Você, Tudo para Você, Jesus

        Eb
Você é Santo, santo, santo
          Gm
Só Tu és santo, santo, santo
        Ab9                            Bb
É tudo sobre Você, Tudo para Você, Jesus
        Eb
Você é Santo, santo, santo
          Gm
Só Tu és santo, santo, santo
        Ab9                            Bb
É tudo sobre Você, Tudo para Você, Jesus

        Eb
Você é Santo, santo, santo
          Gm
Só Tu és santo, santo, santo
        Ab9                            Bb
É tudo sobre Você, Tudo para Você, Jesus
        Eb
Você é Santo, santo, santo
          Gm
Só Tu és santo, santo, santo
        Ab9                            Bb
É tudo sobre Você, Tudo para Você, Jesus

( Eb  Gm  Ab9  Bb )

   Eb       Gm
Hosana, hosana
   Ab9                 Bb
Hosana nas maiores alturas
   Eb       Gm
Hosana, hosana
   Ab9                 Bb
Hosana nas maiores alturas
   Eb       Gm
Hosana, hosana
   Ab9                 Bb
Hosana nas maiores alturas
   Eb       Gm
Hosana, hosana
   Ab9                 Bb
Hosana nas maiores alturas

        Eb
Porque dEle, Por meio dEle
     Gm                Ab9     Bb
Para Ele são todas as coisas
        Eb
Porque dEle, Por meio dEle
     Gm                Ab9     Bb
Para Ele são todas as coisas
        Eb
Porque dEle, Por meio dEle
     Gm                Ab9     Bb
Para Ele são todas as coisas
        Eb
Porque dEle, Por meio dEle
     Gm                Ab9     Bb
Para Ele são todas as coisas]

            Eb
Não, Nunca foi sobre nós

----------------- Acordes -----------------
Ab9 = 4 6 8 5 4 4
Bb = X 1 3 3 3 1
Bb/D = X 5 X 3 6 6
Bb4 = X 1 3 3 4 1
Cm7 = X 3 5 3 4 3
Eb = X 6 5 3 4 3
Gm = 3 5 5 3 3 3
