Ministério Zoe - Sou Prisioneiro do Teu Amor

[Intro] G   D   Em   A9

  G                                              D
Viajando na imensidão do teu amor
Bm                                    A
Eu descobri que não há limites
  G                                              D
Eu me tornei livre das cadeias da morte
Bm                                      A
E fui feito prisioneiro deste amor

G
Eu pensei que sabia viver
D
Mas descobri a razão de estar aqui
Bm                                                        A
Quando morri para o mundo e nasci para ti
G                                                       D
Do império das trevas para o reino do amor
Bm                              A
O reino do teu amor


G
Eu sei que nada, nada poderá me separar
D
Do teu amor que flui deste a eternidade
Bm                                                             A
Demonstrado com sangue derramado na terra

G
Sou prisioneiro do teu amor
D
Sou prisioneiro do teu amor
Bm
Sou prisioneiro do teu amor
A9
Sou prisioneiro do teu amor
G
Sou prisioneiro do teu amor
D
Sou prisioneiro do teu amor
Bm                      A9
Sou prisioneiro do teu amor

( G   D   Bm   A9 )

G
Eu sei que nada, nada poderá me separar
D
Do teu amor que flui deste a eternidade
Bm                                                               A
Demonstrado com sangue derramado na terra

G
Eu sei que nada, nada poderá me separar
D
Do teu amor que flui deste a eternidade
Bm                                                               A
Demonstrado com sangue derramado na terra

G
Eu sei que nada, nada poderá me separar
D
Do teu amor que flui deste a eternidade
Bm                                                                A
Demonstrado com sangue derramado na terra

G
Sou prisioneiro do teu amor
 D
Sou prisioneiro do teu amor
 Bm
Sou prisioneiro do teu amor
 A9
Sou prisioneiro do teu amor
G
Sou prisioneiro do teu amor
 D
Sou prisioneiro do teu amor
 Bm
Sou prisioneiro do teu amor
 A9
Sou prisioneiro do teu amor

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
