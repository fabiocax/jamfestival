Ministério Zoe - Ezequiel 37

[Intro]  Bm  D

Bm                     D
 Um vento frio sopra em meu ser
Bm                 D
 O medo tenta me assolar
Bm
 Não tenho para onde fugir
D                      Bm                  D  E
 Só morte, só morte, só morte,ao meu redor

F#m                          E
  Quero respirar mas não consigo
                               Bm
 Quero sair desse lodo mas não tenho forças
E          D9                Bm  D  E
 Só morte, só morte ao meu redor

Bm                    D
 Eu me entreguei, eu desisti
A               E
 Já não existo, eu morri


 Bm                 D                  A   E
Mas ouço uma voz ao longe dizendo pra mim
 (ossos secos se levantem)

[Solo]  Bm  D  A  E

Bm             D            A     E
 Vem dos quatro ventos ó espírito
Bm                 D                  A          E
 E sopra sobre mim, sopra sobre mim e eu viverei

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
