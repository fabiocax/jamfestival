Vencedores Por Cristo - RASGOU O VEU

Intro 8x: Em7(9) B7/4 (8x)

B7/4 Em7(9)   B7/4
Rendei graças a deus 
       Em7(9)  B7/4
Porque ele é bom 
     C7     Bm7
Porque a sua misericórdia 
    Em7(9) B7/4
É para sempre 
 
   Em7(9)   B7/4
Rendei graças a deus 
       Em7(9)  D7/4
Porque ele é bom 
     C7     Bm7
Porque a sua misericórdia 
    Em7(9)
É para sempre 
 
B7/4 Em7(9)   B7/4
Rendei graças a deus 
       Em7(9)  B7/4
Porque ele é bom 
     C7     Bm7
Porque a sua misericórdia 
    Em7(9) B7/4
É para sempre 
 
   Em7(9)   B7/4
Rendei graças a deus 
       Em7(9)  D7/4
Porque ele é bom 
     C7     Bm7
Porque a sua misericórdia 
    Em7(9)
É para sempre 
 
   Am7       D7/4
Satisfez a nossa sede 
G7       C7
Deu-nos água viva 
  C#m7(b5)   F#7/13-
Saciou a nossa fome 
  B7/4       B7 E7/4
Com o pão que vem do céu 
 
 Am7        D7/4
Venceu na cruz a morte 
   G7        C7
Invadiu de luz as trevas 
 C#m7(b5)   F#7/13-
Mudou a nossa sorte 
 B7/4
Rasgou o véu

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
B7/4 = X 2 4 2 5 2
Bm7 = X 2 4 2 3 2
C#m7(b5) = X 4 5 4 5 X
C7 = X 3 2 3 1 X
D7/4 = X X 0 2 1 3
E7/4 = 0 2 0 2 0 X
Em7(9) = X 7 5 7 7 X
F#7/13- = 2 X 2 3 3 2
G7 = 3 5 3 4 3 3
