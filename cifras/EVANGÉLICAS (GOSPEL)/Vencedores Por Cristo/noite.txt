Vencedores Por Cristo - Noite

[Intro] E9  C#m  A7M  B7  
        E9  C#m  A7M  B7

E9            C#m            A7M        B7
Noite sem descanso   viu Jesus amanhecer
E9              C#m            A7M          B7
Sol de um dia triste   sexta-feira  de horror
Bm7            E7            A7M          G7(13)  G7(13-)
Chefes,  sacerdotes  maltrataram pra valer
    C7M   A7(4)             A7          D7M
A  quem  é  da  própria  vida  o  autor
F#7(13)           F#7(13-)     B7(4/9)
Pelo  bem  que  fez,     pagou
E9                  C#m                A7M          B7
Sim,  mas  foi  Pilatos  quem  por  fim  o  entregou
E9                 C#m         A7M             B7
Cara  e  mãos  lavadas,  a  saída  que  escolheu
Bm7            E7              A7M            G7(13)  G7(13-)
E  o  povo  irado,  como  um  só  se  levantou
   C7M      A4                    A7                    D7M
Falou:   (será  que  a  voz  do  povo  é  a  voz  de  Deus?)
 F#7(13)                F#7(13-)    B7(4/9)  C7(4/9)
Cruz   e   morte   ao   benfeitor!
F9           Dm7           Bb7M                C7
Quase  meio-dia,  o  salvador  já  preso  à  cruz
F9                  Dm7           Bb7M      C7
Cruz,  horror  maldito  cruz de dura punição
Cm7             F7              Bb7M              C7  F7M
De  repente,  trevas  pois  fugiu  a  própria  luz

----------------- Acordes -----------------
A4 = X 0 2 2 3 0
A7 = X 0 2 0 2 0
A7(4) = X 0 2 0 3 0
A7M = X 0 2 1 2 0
B7 = X 2 1 2 0 2
B7(4/9) = X 2 2 2 2 2
Bb7M = X 1 3 2 3 1
Bm7 = X 2 4 2 3 2
C#m = X 4 6 6 5 4
C7 = X 3 2 3 1 X
C7(4/9) = X 3 3 3 3 3
C7M = X 3 2 0 0 X
Cm7 = X 3 5 3 4 3
D7M = X X 0 2 2 2
Dm7 = X 5 7 5 6 5
E7 = 0 X 0 0 1 0
E9 = 0 2 4 1 0 0
F#7(13) = 2 X 2 3 4 X
F#7(13-) = 2 X 2 3 3 2
F7 = 1 3 1 2 1 1
F7M = 1 X 2 2 1 X
F9 = 1 3 5 2 1 1
G7(13) = 3 X 3 4 5 X
G7(13-) = 3 X 3 4 4 3
