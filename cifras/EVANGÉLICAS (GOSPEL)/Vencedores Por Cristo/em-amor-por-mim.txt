Vencedores Por Cristo - Em Amor Por Mim

D             Bm      D/F#     G   D         B7 E7    A7/4
Este é o meu corpo Partido por ti traz salvação e dá a paz 
A7             F#m      Bm    Em        A7/4     D 
Toma e come e quando fizeres faze-o em amor por mim 

( E/D  C  A/B )

E            C#m       E         C C#m       A   F#m      B74
Este é  meu sangue vertido por ti Traz o perdão  e  liberdade
 B7            G#m      C#m     F#m      A/B       E
Toma e bebe E quando fizeres faze-o em amor por mim

A7    E
Aaaa aaah

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/B = X 2 2 2 2 X
A7 = X 0 2 0 2 0
A7/4 = X 0 2 0 3 0
B7 = X 2 1 2 0 2
B74 = X 2 4 2 5 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
E = 0 2 2 1 0 0
E/D = X 5 X 4 5 4
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G#m = 4 6 6 4 4 4
