Vencedores Por Cristo - O Que Você Fará ?

Intro: Am7  C7/9

Am7       C7/9   Bm7
Se agora chegar o tempo 
Bm7/9
De você se despedir 
Am7       C7/9   Bm7
Não só por algum momento 
    Bb7/9       Dm7
Mas pra sempre da vida partir 
 Dm7/G    G7  C7+     Am7
Qual será o seu procedimento? 
Dm7   Dm7/G     Bm7 Bb7/9
O que você fará? 


A9/6  G6     Bm7       G C G
Indo      sem nada levar 
A9/6 G6     Bm7      G C   G
Vendo     sem nada esperar 
A9/6 G6      Bm7      G  C G
Pranto    sem poder voltar 
A9/6 G6     Bm7      G6
Crendo    sem poder mudar sua sorte 

Am7       C7/9   Bm7
Agora é chegado o tempo 
Bm7/9
De você se decidir 
Am7       C7/9   Bm7
Não só por algum momento 
    Bb7/9       Dm7
Mas pra sempre o viver prosseguir 
 Dm7/G    G7  C7+     Am7
Qual será o seu procedimento? 
Dm7   Dm7/G     Bm7 Bb7/9
O que você fará? 

A9/6  G6        Bm7     G  C G
Cristo,      viver sem parar 
A9/6  G6       Bm7       G C  G
Cristo,      poder pra alcançar 
A9/6  G6       Bm7     G C  G
Cristo,      em quem descansar 
A9/6  G6         Bm7     G6     Am7   D9/7 Am7 Am7
Cristo,      quem pode mudar sua sorte

----------------- Acordes -----------------
A9/6 = 5 4 4 4 X X
Am7 = X 0 2 0 1 0
Bb7/9 = X 1 0 1 1 X
Bm7 = X 2 4 2 3 2
Bm7/9 = X 2 0 2 2 X
C = X 3 2 0 1 0
C7+ = X 3 2 0 0 X
C7/9 = X 3 2 3 3 X
D9/7 = X 5 4 5 5 X
Dm7 = X 5 7 5 6 5
Dm7/G = 3 X 0 2 1 1
G = 3 2 0 0 0 3
G6 = 3 X 2 4 3 X
G7 = 3 5 3 4 3 3
