Vencedores Por Cristo - Como é bom

Obs.: O Am14 é o lá menor à sétima, porém deve ser efetuada no mi
      (1a corda) executando-se o sol(3a casa).



Intro: C Bm Am C

G G4       G      G4
Ah! Como é bom poder
           Am         D D4 D
Aos pés da cruz depositar
G G4      G   G4
Este meu fardo
         Am  A7        D D4 D  D7/F#
Pesado e árduo de carregar

C               Em7            Am      D4 D
E não ter que andar ansioso de nada senão
C             Em7             Am14       D D4 D7/F#
A Deus tudo levar em grata e súplice oração
Em         F#7e5-  Am7        C             D  D4 D
E a paz de Deus então mente e coração guardará
            G
Em Cristo Jesus

C               Em7            Am     D4 D
E não ter que andar ansioso de nada senão
C            Em7         Am14            D D4 D7/F#
Sobre Ele lançar cada problema cada aflição
Em         F#7e5-  Am7        C             D D4 D
E a paz de Deus então mente e coração guardará
            G
Em Cristo Jesus

 G4        G       G4
Ah! Como é bom poder!
       G
Como é bom saber!

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D4 = X X 0 2 3 3
D7/F# = X X 4 5 3 5
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
G4 = 3 5 5 5 3 3
