Vencedores Por Cristo - Agora Vejo

Intro: C Am G/B A/C# Dm Dm7/C G/B Am Em D Dm7

Dm7                    F                   C F C F C
Passando jesus e os discípulos pelo caminho, 
Dm7                   F                 C   F  C F C
Olhou para um homem, um cego, sentado sozinho. 
    Am                   G/B     C                  A/C#
De quem era a culpa, senhor? foi ele ou seu pai que pecou? 
Dm         Dm7/C       G/B     Dm    Dm7/C       D
O que que nos leva a sofrer, e tanta aflição padecer? 
      Am                 Em          D
É a razão, e a razão desta dor conhecer? 
   Dm7                  F            C      F  C F C
Então, respondendo, jesus disse: eu sou a luz. 
  Dm7                     F             C       F C F C
Melhor trabalhar. sendo dia, eis que a noite vem. 
   Am             G/B    C             A/C#
Da sua saliva tornou com terra da qual misturou, 
    Dm      Dm7/C      G/B        Dm         Dm7/C    G/B
Aos olhos do cego estendeu a mão, fugindo-lhe a escuridão, 
       Am                   Em             D
Fez brilhar, fez brilhar, dar à luz novo dia. 
 G            A7/G        C/G         F       G   F C
Quem ele era, eu não sei. cumpriu meu maior desejo, 
G            A7/G           C/G        F     G    F C
E nem seu nome posso dizer. só sei que agora vejo.

----------------- Acordes -----------------
A/C# = X 4 X 2 5 5
A7/G = 3 X 2 2 2 X
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C/G = 3 3 2 X 1 X
D = X X 0 2 3 2
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Dm7/C = X 3 0 2 1 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
