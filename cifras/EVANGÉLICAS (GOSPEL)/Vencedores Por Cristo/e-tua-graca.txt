Vencedores Por Cristo - É Tua Graça

Introdução: D A Bm7 Bm7/A
            G A4 A D (G/B A)

      D           A
É Tua graça que liberta,
      Bm7          Bm7/A
É Tua graça que me cura,
      G            D/F#        Em7   A4  A
É Tua graça que sustenta minha vida!
        D             A
Por Teu sangue tenho acesso,
      Bm7        Bm7
A Tua graça preciosa!
   G         A4  A   D    (G/B  A/C#)
Te louvo, Te amo,   Jesus!

      D           A
É Tua graça que liberta,
      Bm7          Bm7/A
É Tua graça que me cura,
      G            D/F#        Em7   A4  A
É Tua graça que sustenta minha vida!
        D             A/C#
Por Teu sangue tenho acesso,
      Bm7        Bm7
A Tua graça preciosa!
   G         A4  A   D   D/C
Te louvo, te amo,   Jesus!

    Bb        F/C   C      D9 D
Tua graça é melhor   que a vi-da!
    Bb        F/C C    D9 D  D9 D
Tua graça é o que   me basta!
   Bb   F/C   C D9 D
Favor ime|----reci-do,
   Bb      C       A4  A
Do céu, do céu pra mim!

      D           A
É Tua graça que liberta,
      Bm7          Bm7/A
É Tua graça que me cura,
      G            D/F#        Em7   A4  A
É Tua graça que sustenta minha vida!
        D             A
Por Teu sangue tenho acesso,
      Bm7        Bm7
A Tua graça preciosa!
   G         A4  A   D   D/C
Te louvo, te amo,   Jesus!

    Bb        F/C   C      D9 D
Tua graça é melhor   que a vi-da!
    Bb        F/C C    D9 D  D9 D
Tua graça é o que   me basta!
   Bb   F/C   C D9 D
Favor ime|----reci-do,
   Bb      C       A4  A
Do céu, do céu pra mim!

      E           B/D#
É Tua graça que liberta!
      C#m7         C#m7/B
É Tua graça que me cura!
      A            E/G#        B4  B
É Tua graça que sustenta minha vida!
        E             B/D#
Por Teu sangue tenho acesso,
      C#m7       C#m7/B
A Tua graça preciosa!
   A         B4  B   E   C#m7
Te louvo, te amo,   Jesus
   A         B4  B   A
Te louvo, te amo,   Jesus

Fim:  A  F#m7  E

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A4 = X 0 2 2 3 0
B = X 2 4 4 4 2
B/D# = X 6 X 4 7 7
B4 = X 2 4 4 5 2
Bb = X 1 3 3 3 1
Bm7 = X 2 4 2 3 2
Bm7/A = 5 2 4 2 3 2
C = X 3 2 0 1 0
C#m7 = X 4 6 4 5 4
C#m7/B = 7 4 6 4 5 4
D = X X 0 2 3 2
D/C = X 3 X 2 3 2
D/F# = 2 X 0 2 3 2
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
Em7 = 0 2 2 0 3 0
F/C = X 3 3 2 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
