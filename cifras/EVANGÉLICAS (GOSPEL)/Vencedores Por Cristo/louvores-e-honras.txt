Vencedores Por Cristo - Louvores e Honras

D         F#m      G        A    F#7 
Louvores e honras ao nome de Cristo 
   Bm        Bm7     E7        A7 
convém, fica bem adorar ao Senhor 
         D         F#m         G        A7 F#7 
Quem já viu em sua vida o que nós temos visto 
    Bm     Bm7           Em        D 
Só pode dizer que ele é cheio de amor. 
 
        D          F#m       G     A7    F#7 
Cristo salva e sustenta com mão poderosa 
       Bm        Bm7       E7      A 
Seus eleitos e amados bem perto de si 
        D           F#m        G       A7    F#7 
Cristo chama, ele inflama de amor pela obra 
  Bm        Bm7         Em A7     D 
Renova, restaura, nos reclama pra si 
 
       D         D7         G    F#m B7 
Estou pronto pra ir Cristo amado 
      Em          A7           D   A7 
E seguir-te onde queres que eu vá 
      D        D7     G    Gm 
E servir com amor dedicado 
          D         A7          D 
Com meus lábios pra sempre louvar.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F#7 = 2 4 2 3 2 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
