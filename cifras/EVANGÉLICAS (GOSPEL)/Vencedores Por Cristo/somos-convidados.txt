Vencedores Por Cristo - Somos Convidados

A                 B/A
Somos convidados para adorar
   D/A                A         E/A   D/A
Ao único e verdadeiro Deus.
A                 B/A
Como Pai gracioso veio nos amor,
D/A                        A
Preocupado com os que são seus.

A                  B/A
Viu nossa miséria, nossa condição,
D/A                 A               E/A   D/A
Sem uma esperança, sem uma razão.
A                B/A
Com a sua graça, com o seu perdão
D/A                  A
Deu-nos vida nova em Jesus.

C#m7                   F#m7
Ele é o filho amado, prazer do nosso Pai,
D            A      C#sus         C#7
Tem em suas mãos o poder de nos guiar.
F#m         A/E    B7(9)            D
Juntos levantemos sempre as nossas mãos,
    Bm7   C#m7      D7M    D#°     D/E   E7       A     E/A     D/A
E a uma   voz    louvemos   a    Jesus      O Senhor

A                      E/A     D/A
O Senhor da Glória...
A                      E/A     D/A
Senhor da História...
A                      E/A     D/A
Rei da Vitória...

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/E = 0 X 2 2 2 0
B/A = X 0 4 4 4 X
B7(9) = X 2 1 2 2 X
Bm7 = X 2 4 2 3 2
C#7 = X 4 3 4 2 X
C#m7 = X 4 6 4 5 4
C#sus = X 4 6 6 6 4
D = X X 0 2 3 2
D#° = X X 1 2 1 2
D/A = X 0 X 2 3 2
D/E = X X 2 2 3 2
D7M = X X 0 2 2 2
E/A = X 0 2 1 0 0
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
