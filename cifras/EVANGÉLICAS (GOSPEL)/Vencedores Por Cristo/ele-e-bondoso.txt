Vencedores Por Cristo - Ele É Bondoso

D              B7    Em
Ele é Bondoso, Maravilhoso
A7       D     E7       A
Ele é Bondoso, Cristo Senhor
D                   B7     Em
Ele é o Grande Rei, Mestre Divino
A7       D     A   A7   D
Ele é Bondoso, Cristo Senhor
A      A7    D
É meu Amigo, sempre comigo
E    E7    A   A7
Onipotente é
D
Vinde prostrai-vos
B7     Em
Sim adorai-o
A7       D     A7       D
Ele é Bondoso, Cristo Senhor

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
