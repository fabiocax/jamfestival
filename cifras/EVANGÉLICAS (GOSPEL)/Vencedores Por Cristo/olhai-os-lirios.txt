Vencedores Por Cristo - Olhai Os Lírios

A    D        A       D
O Futuro que te espera Deus sabe de cor 
A       D        A      D    E D
O inverno, a primavera e o tempo que é melhor. 
A          D        A        D
Deixa, então, tristeza e solidão pra nunca mais 
A       D      A       D E  D A
E vê tanta beleza que Jesus à vida traz. 

D7      A      D      A
Até pequenas flores que breve vida tem 
  G               D         Bm
Deus veste em lindas cores da glória que Ele tem 
 Em     F    Bb        A
O amanhã não pode roubar perfume e cor 
 F#m         D    A    E Fm   D     A
Segredo é viver hoje em Cristo e        Seu amor
 
A        D      A       D
O Seu reino em meu viver primeiro eu vou buscar 
 A        D        A       D    E D
E tudo o mais virá depois em Cristo eu vou ganhar. 
A       D      A       D
Minha fé que não é vã em Cristo eu coloquei 
A        D    A     D    E D A
E por isso o amanhã a Ele eu entreguei. 

Refrão... 

A      D       A       D
Sua vida entrega hoje ao meu Senhor Jesus 
 A         D      A       D   E D
Confia as trevas do futuro à sua imensa luz 
   A         D           A         D
Pois muito mais que a simples flor, Deus vai cuidar de Ti 
 A     D           A     D  E D A
Agora e no futuro e em toda a sua vida aqui. 

Refrão...

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bb = X 1 3 3 3 1
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
