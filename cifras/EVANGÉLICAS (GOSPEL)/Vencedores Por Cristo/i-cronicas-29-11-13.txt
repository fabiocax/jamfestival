Vencedores Por Cristo - I Crônicas 29.11 - 13

[Intro] A9  A7+/9  D9/F#
        A9  A7+/9  D9/F#

A9          A7+/9          D9/F#
Tua, Senhor, é a grandeza e o poder
A9                A7+/9    D9/F#
A honra, a vitória e a majestade
Bm7          C#m7
Porque Teu é tudo
D7+                   D9/F#
Quanto há no céu e na terra

A9               A7+/9    Bm7
Teu, Senhor, é o reino, e Tu te
   C#m7        D7+   D/E    A9
Exaltaste por chefe sobre todos

( A9  A7+/9  D9/F# )
( A9  A7+/9  D9/F# )

   A        A9    A4       A
Riquezas e glória vem de Ti
A     A9          A4   A
Tu dominas sobre tudo
 A      A9          A4      A
Na Tua mão há força e poder
    D            Bm7
Contigo está o engrandecer
    D7+      D/E  A9
E a tudo dar for  ça

( A9  A7+/9  D9/F# )

A9          A7+/9    D9/F#
Agora, pois,      oh nosso Deus
  A9      A7+/9 D9/F#     Bm7
Graças te damos,     e louvamos
   D       D/E    A9
O Teu grandioso nome

A9          A7+/9    D9/F#
Agora, pois,      oh nosso Deus
  A9      A7+/9 D9/F#     Bm7
Graças te damos,     e louvamos
   D       D/E    A9
O Teu grandioso nome

( A9  A7+/9  D9/F# )

----------------- Acordes -----------------
A = X 0 2 2 2 0
A4 = X 0 2 2 3 0
A7+/9 = X 0 2 1 0 0
A9 = X 0 2 2 0 0
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
D/E = X X 2 2 3 2
D7+ = X X 0 2 2 2
D9/F# = 2 X 0 2 3 0
