Vencedores Por Cristo - Antes Mesmo Que Eu Fosse Alguém

Am                         Am7M
Antes mesmo que eu fosse alguém
       Am7               D9/F#
Tú me amaste me disseste vem
      D9/F                     A A9 A
Mesmo sendo tão rebelde e vão
        B             Bm     E  E7
Tú me amaste me estendeste a mão

       Am                Am7M
Já me deste tanto amor e paz
             Am7               D9/F#
Que eu só te peço uma coisinha a mais
           D9/F            E     A     F#m
Pra que eu possa cumprir a minha parte
   Bm        E      A      Dm E A A4 A Dm E Am
Ensina-me senhor a amar-te

                            Am7M
Antes mesmo que eu fosse alguém
       Am7               D9/F#
Tú me amaste me disseste vem
      D9/F                A
Mesmo sendo tão rebelde e vão
       B              Bm     E
Tú me amaste me estendeste a mão

       Am                Am7M
Já me deste tanto amor e paz
             Am7                 D9/F#
Que eu só te peço uma coisinha a mais
           D9/F            E     A     F#m
Pra que eu possa cumprir a minha parte
   Bm        E       A     F#m
Ensina-me senhor a amar-te
    Bm       E     A       F#m
Ensina-me senhor a amar-te
    Bm        E     A7M    A7M
Ensina-me senhor a amar-te

----------------- Acordes -----------------
A = X 0 2 2 2 0
A4 = X 0 2 2 3 0
A7M = X 0 2 1 2 0
A9 = X 0 2 2 0 0
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Am7M = X 0 2 1 1 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
D9/F = 1 X X 2 3 0
D9/F# = 2 X 0 2 3 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
