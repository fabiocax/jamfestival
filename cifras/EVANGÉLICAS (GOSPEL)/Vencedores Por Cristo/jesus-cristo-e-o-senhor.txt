Vencedores Por Cristo - Jesus Cristo é o Senhor

Intro: C Am Dm G7 }2x

 C         Am           Dm        G7
Glória pra sempre ao Cordeiro de Deus
  C          Am      Dm       G7
À Jesus o Senhor ao Leão de Judá 
    C       Am        Dm      G7        C  F C
À Raiz de Davi que venceu e o livro abrirá
  C     Am   F    Dm    C      Am        G7
O céu a terra e o mar e tudo o que neles há
  C    C7    F        G
O adorarão e confessarão
   C   Am   Dm G     C  F C
Jesus Cristo é o Senhor!
           G               C   
Ele é o Senhor! Ele é o Senhor!
                                      G
Ressurreto dentre os morto ele é o Senhor!
     C             C7        F              G
Todo joelho se dobrará, toda língua confessará
     C     Am   Dm G    G   F C
Que Jesus Cristo é o Senhor!

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
