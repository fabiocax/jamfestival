Vencedores Por Cristo - A Batalha É do Senhor

          F#m           F#m/E     F#m/D      Bm7
Quando os grandes homens de Judá viram o inimigo
    F#m                  F#m/E                F#m/D     D7 C#m7 Bm7
O Senhor mandou que eles não temessem, pois daria a vitória
               C#m7               D7 C#m7 Bm7
Lhes chamou a cantar uma nova canção,
           C#m7                 D/E
Abaixar a espada e erguer um louvor
  A          F#m              G                 Bm7     D/E    A
Cantem ao Senhor com alegre som, ergam as vozes para o Seu louvor
              F#m             G                  Bm7      D/E    A
Um canto vencedor na tribulação, crê somente a batalha é do Senhor
       F#m         F#m/E          F#m/D    Bm7
Quando algo mau te encontrar confia no Senhor
         F#m             F#m/E         F#m/D         D7 C#m7 Bm7
Creia em Deus que não te deixará, Ele sempre perto está
            C#m7               D7   C#m7        Bm7               C#m7       D/E
Você pode cantar uma nova canção,                   porque a batalha é do Senhor

       Em/A    D/A     Dm/A       A
Porque Ele é bom, Seu amor dura para sempre  (2x)
                 D/E
Para sempre e sempre.
   A         F#m               G                  Bm7    D/E     A
Cantem ao Senhor com  alegre som, ergam as vozes para o Seu louvor
               F#m             G                  Bm7     D/E   A
Um canto vencedor na tribulação, crê somente a batalha é do Senhor

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
D/A = X 0 X 2 3 2
D/E = X X 2 2 3 2
D7 = X X 0 2 1 2
Dm/A = X 0 X 2 3 1
Em/A = X 0 2 0 0 X
F#m = 2 4 4 2 2 2
F#m/D = 2 5 4 2 2 2
F#m/E = X X 2 2 2 2
G = 3 2 0 0 0 3
