Vencedores Por Cristo - Se Tão Somente


Intro: G7+  Am7 Bm7 Am7 D7

 G        C7+   Bm7   C7+     Bm7           Em7
Se tão somente a cris...to confessar-mos, os nossos  
 
  Am D4/7   D7    G   D/G     C/G   F#m7/5-         B7   Em7
Pecados,         ele é fiel,   ele é fiel               para nos  
 Am7 D7
Perdoar  
 
G   D/G    C/G    F#m7/5-        B7    Em7
Ele é fiel,   ele é fiel               para nos  
 
Am7    D7   C  G
per....do...ar.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
C/G = 3 3 2 X 1 X
C7+ = X 3 2 0 0 X
D/G = 3 X X 2 3 2
D4/7 = X X 0 2 1 3
D7 = X X 0 2 1 2
Em7 = 0 2 2 0 3 0
F#m7/5- = 2 X 2 2 1 X
G = 3 2 0 0 0 3
G7+ = 3 X 4 4 3 X
