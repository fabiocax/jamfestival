Vencedores Por Cristo - Glória Pra Sempre

 G           Em       Am         D
Glória pra sempre, ao cordeiro de Deus 
   Bm       Em       Am      D
A Jesus, o Senhor, ao leão de Judá 
   Bm      Em         C      D            G
A raiz de Davi, que venceu e o livro abrirá 
G       C   D     G          G          Em     F D
O céu, a terra e o mar, e tudo o que neles há 
G     G4      C    Cm
O adorarão, e confessarão
Cm G  Em     Am   D  G
Jesus Cristo é o Senhor! 

  G        D             G
Ele é o Senhor, Ele é o Senhor! 
                             D
Ressurreto dentre os mortos, Ele é o Senhor 
      G        G4         C                     Cm
Todo joelho se dobrará, toda língua confessará 
    G    Em   Am D    G
Que Jesus Cristo  é  o  Senhor!

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G4 = 3 5 5 5 3 3
