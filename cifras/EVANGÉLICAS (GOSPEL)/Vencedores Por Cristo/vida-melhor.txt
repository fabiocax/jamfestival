Vencedores Por Cristo - Vida Melhor

Intro:  A  E/G#  D/F#  Dm/F  A  F#m  A/E  B/D#  E

    A           E/G#
/: existe um caminho na vida, 
           D/F#   Dm/F    A
   uma estrada perdida sem nada levar. 
      
  F#m             A/E
   ilusões que nos cercam e mostram, 
         B/D#           E
   um tempo de vida pra se aproveitar. 
 
       A         E/G#
   entre tudo que tive e sempre busquei 
    D/F#   Dm/F     A
   em nada pude me encontrar; 
       F#m            A/E
   tantos planos perdidos no tempo, 
           B/D#         E
   há sempre um momento de parar e pensar. :/ 
  A           E/G#
E andando no tempo e na vida sem paz, 
   D/F#    Dm/F      A
Procurando algo pra preencher. 
 
  F#m            A/E
E lutando com tudo sem nada encontrar, 
  B/D#          E
Onde forças pra continuar. 
           
  A           E/G#
'té que um dia encontrei uma vida melhor, 
   D/F#    Dm/F   A
Algo novo que me aconteceu. 
            F#m          A/E
E não troco por nada no mundo não, 
     B/D#          E
Hoje eu quero é viver para deus. 
 A        E/G#     D/F#   Dm/F   A
  com jesus eu posso sim, ter uma vida real, 
     F#m       A/E  B/D#          E
  ele veio na hora certa, me fez novo afinal. 
 A        E/G#     D/F#    Dm/F    A
  com jesus eu canto sim, tenho uma vida real,

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/E = 0 X 2 2 2 0
B/D# = X 6 X 4 7 7
D/F# = 2 X 0 2 3 2
Dm/F = X X 3 2 3 1
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
F#m = 2 4 4 2 2 2
