Vencedores Por Cristo - Logo de Manhã

G         D    Em          C
Logo de manhã, quero te buscar,
Em/D      C    Am          D4 D7
Tua voz ouvir, Teu amor sentir.
G           D     Em        C
Estender as mãos para te louvar,
Em/D        C        Am      D7    G
Derramar meu coração sobre o Teu altar.

     Am       E        Am     E     Am
Pois Tu sabes bem tudo quanto há em mim:
F        E         Am   E7    F   D4 D7
Vou te seguir e te amar até o fim.

G          D      Em             C
E no fim do dia, quando o Sol se for,
Em/D    C    Am          D4  D7
Te adorarei, te darei louvor.
G              D      Em           C
Mesmo escura a noite, brilha a Tua luz.
Em             C            Am    D      G
Em Teus braços eu descanso, meu Senhor Jesus.
Em             C            Am    D      C  Cm7 G
Em Teus braços eu descanso, meu Senhor Jesus.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Cm7 = X 3 5 3 4 3
D = X X 0 2 3 2
D4 = X X 0 2 3 3
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
Em/D = X X 0 4 5 3
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
