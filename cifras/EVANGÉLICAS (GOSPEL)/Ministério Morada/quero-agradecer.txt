Ministério Morada - Quero Agradecer

B                D#m
Teu olhar me alcançou
             C#
Tua voz me atraiu
     G#m    B
Me chamou
               D#m
Curou minhas feridas
                   C#
Colocou em mim as vestes
      F#/A# B
De louvor
           D#m
Que amor é esse, Jesus?
        F#/A#
Que me deu nova vida
   C#                G#m
Mudou minha história
          B
Morreu em meu lugar
       D#m       C#
Mesmo sem eu merecer

F#         B
Quero agradecer
F#/A#           C#
Por tão grande amor
F#            B
O que posso fazer
          F#/A#      C#   B
Pra retribuir teu favor?

                  D#m
Teu olhar me alcançou
             C#
Tua voz me atraiu
      G#m   B
Me chamou
               D#m
Curou minhas feridas
                   C#
Colocou em mim as vestes
      F#/A#  B
De louvor
            D#m
Que amor é esse, Jesus?
        F#/A#
Que me deu nova vida
   C#                G#m
Mudou minha história
          B
Morreu em meu lugar
       D#m         C#
Mesmo sem eu merecer

F#         B
Quero agradecer
F#/A#           C#
Por tão grande amor
F#            B
O que posso fazer
          F#/A#      C#
Pra retribuir teu favor?

G#m
Entregar a minha vida
               F#/A#
Como oferta no altar
        B
O sacrifício sou eu
        C#
Eis-me aqui

G#m
Cure através de mim
F#/A#
Transforme através de mim
B
Salve através de mim
C#
Eis-me aqui

G#m
Cure através de mim
F#/A#
Transforme através de mim
B                 C#     D#
Salve através de mim

G#          C#
Quero agradecer
Fm              D#
Por tão grande amor
Fm             C#
O que posso fazer
          G#/C       D#
Pra retribuir teu favor?

     C#
Entregar a minha vida Senhor

----------------- Acordes -----------------
B = X 2 4 4 4 2
C# = X 4 6 6 6 4
D# = X 6 5 3 4 3
F# = 2 4 4 3 2 2
G# = 4 3 1 1 1 4
