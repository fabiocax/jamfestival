Ministério Morada - Fazer a Diferença

(intro) E B D A

E         B A  C#m       E             B                  A
Quero entender o significado da palavra, me entregar
E      B A C#m         E             B         A
Quero viver toda minha vida só pra Te adorar

G#m                   A  C#m                  B
Quero ser mais que um amigo, quero ser mais que um irmão (2x)

Eu quero Ser

    E   B         D     A
EU QUERO SER UMA MORADA PARA TI
E                 B           D      A   ( F#m F#m7/9 )
NÃO QUERO APENAS SER VISITADO, QUERO SER HABITADO

         C#m               B
Eu vou fazer a diferença por onde eu passar
          A                  G#m
Eu vou marcar essa história, reflexo de Deus

         F#m            G#m
Eu vou cantar, pular, pra todo mundo ver
     A                  B
Que vive meu Senhor, e também eu vou viver

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
F#m7/9 = X X 4 2 5 4
G#m = 4 6 6 4 4 4
