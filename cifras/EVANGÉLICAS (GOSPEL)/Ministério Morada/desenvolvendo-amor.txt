Ministério Morada - Desenvolvendo Amor

Em                    Bm
 Tua palavra é uma semente
Am
  Meu coração é uma terra boa
   Em                   Bm
Então Deus, faz essa semente brotar
 Am                         D  D#
Deus, faz ela crescer germinar

( Em  Bm  Am  )
( Em  Bm  Am  )

[Primeira Parte]

Em                    Bm
 Tua palavra é uma semente
Am
  Meu coração é uma terra boa
   Em                   Bm
Então Deus, faz essa semente brotar
 Am
Deus, faz ela crescer germinar


Em              Bm
  Teu espírito convence
Am
  Vou editar o meu comportamento
Em                Bm
  Deixar o velho homem pra trás
Am                       D   D#
  E aquelas coisas de criança

[Refrão]

Em          Bm           Am
Eu vou crescendo, absorvendo
         D
Desenvolvendo amor em Deus
Em          Bm           Am
Eu vou crescendo, absorvendo
         D
Desenvolvendo amor em Deus

Em          Bm           Am
Eu vou crescendo, absorvendo
         D
Desenvolvendo amor em Deus
Em          Bm           Am
Eu vou crescendo, absorvendo
         D                ( D  D#  Em )
Desenvolvendo amor em Deus

( Em  Bm  Am )

[Primeira Parte]

Em                    Bm
 Tua palavra é uma semente
Am
  Meu coração é uma terra boa
   Em                   Bm
Então Deus, faz essa semente brotar
 Am
Deus, faz ela crescer germinar

Em              Bm
  Teu espírito convence
Am
  Vou editar o meu comportamento
Em                Bm
  Deixar o velho homem pra trás
Am                       D   D#
  E aquelas coisas de criança

[Refrão]

Em          Bm           Am
Eu vou crescendo, absorvendo
         D
Desenvolvendo amor em Deus
Em          Bm           Am
Eu vou crescendo, absorvendo
         D
Desenvolvendo amor em Deus

Em          Bm           Am
Eu vou crescendo, absorvendo
         D
Desenvolvendo amor em Deus
Em          Bm           Am
Eu vou crescendo, absorvendo
         D
Desenvolvendo amor em Deus

[Ponte]

Bm                              Am
  Sim, eu serei como a árvore frondosa
Bm                                         Am
  E dos meus galhos sairão as frutas mais saborosas
Bm                       C                     D
  Pois estarei plantado junto aos ribeiros das águas da vida

Em                  Bm
  E nada me impedirá  de viver o melhor
  Am
Eu  conheço o verbo da verdade de cor
          Em                      Bm
Se foi a luz que me mostrou um caminho de dor
             Am
Não vai ter sombra que escureça o dia do meu crescimento no amor

Em
  Já não me ouço o que o mal me diz
Bm
  A respeito do que é ser feliz
Am
  Considero o amor e vivo como quem não tem cicatriz
Em
  Lanço fora tudo o que fiz
Bm
  Pra ter mais do que sempre quis
Am
  E viver do amor que reggae fortalece minha raiz

Em                   Bm
  Ninguém amou como Jah nos amou
Am
  Antes do sol nascer Jah nos amou
Em             Bm             Am
  Só permanecerá quem perseverar no amor

Em                       Bm
  Então gere um bom fruto  pra que não haja luto
Am
  Amor é mútuo e preenche o nada como tudo
Em                       Bm
  Alcança o mais profundo  revela o verdadeiro
Am                  D             D#
  Amor eterno puro simples e não passageiro

[Refrão]

Em         Bm           Am
  Vou crescendo, absorvendo
         D
Desenvolvendo amor em Deus
Em         Bm           Am
  Vou crescendo, absorvendo
         D
Desenvolvendo amor em Deus
Em         Bm           Am
  Vou crescendo, absorvendo
         D
Desenvolvendo amor em Deus
Em         Bm          Am
  Vou crescendo, absorvendo em Deus

( D  D#  Em )

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D# = X 6 5 3 4 3
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
