Ministério Morada - Puro e Simples

F
Olha, Deus
                 Bb9
O desenho que eu fiz pra ti
                  Dm
Esse aqui é o meu pai,
              Bb9            C
Essa aqui é a mamãe e o meu irmão
              F
E esse aqui é você
              Bb9
E esse aqui é você
              Dm    Bb9  C
E esse aqui é você

F
Sabe, Deus
               Bb9
A minha mãe me falou mais cedo
Que, qualquer coisa
Dm                                Bb9
Que me der medo, posso correr pra você

 C                F
Posso correr pra você
                 Bb9
Posso correr pra você
                 Dm     Bb9  C
Posso correr pra você

C                F
E quando chegar lá,
                     Bb9
Será que você vai me encontrar?
                         Dm
Se eu ficar no meu lugar secreto, discreto, bem quieto
                  Bb9     C
Esperando você me achar
C               F
E quando chegar lá,
                     Bb9
Será que você vai me encontrar?
                         Dm
Se eu ficar no meu lugar secreto, discreto, bem quieto
                  Bb9
Esperando você me achar

C                F
E quando chegar lá,
                     Bb9
Será que você vai me encontrar?
                         Dm
Se eu ficar no meu lugar secreto, discreto, bem quieto
                  Bb9
Esperando você me achar
C               F
E quando chegar lá,
                     Bb
Será que você vai me encontrar?
                         Dm
Se eu ficar no meu lugar secreto, discreto, bem quieto
                  Bb9     C
Esperando você me achar


F
Um, dois, três, quatro, cinco,
                  Bb9
Seis, sete, oito, nove, dez,
Onze, doze, treze, catorze, quinze,
           Dm
Dezesseis, dezessete, dezoito, dezenove,
Vinte, vinte e um, vinte e dois, vinte e três,
                Bb9
Vinte e quatro, vinte e cinco, vinte e seis,
                            C
Vinte e sete, vinte e oito, vinte e nove, trinta
C      F
Lá vou eu!

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bb9 = X 1 3 3 1 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
