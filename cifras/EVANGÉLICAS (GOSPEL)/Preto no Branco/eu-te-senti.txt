Preto No Branco - Eu Te Senti

Em              C      G            D9
Toda vez que eu cantei, eu Te senti
Em             C         G
Toda vez que eu precisei, eu Te senti
Em              C              G
Sempre que algo dava errado, eu Te senti
      F
Me falando algo mais
     C
E querendo fazer mais

Em            C        G          D9
Toda vez que eu chorei, eu Te senti
Em           C       G
Quando menos esperei, eu Te senti
Em               C           G
Quando já não me aguentava, eu Te senti
          F
Dando a coragem para viver
       C
Sendo mais que imaginei


Eu prometo a Você

G
Tudo que eu puder fazer
             C9
Pra ser tudo o que Você desejou
            Em                   D
Quero viver tudo que Você sonhou
           C
Ser muito mais Você, e menos eu
          G
Eu quero minha vida dedicar
            C9
E esperar o dia que Te encontrarei
             Em                  D            C
Te abraçar e te entregar o medo que eu carreguei
        F                       G
Mas ele nunca conseguiu me parar

        C9     Em  D         C9
Ô Ô Ô Ô Ô    Rilou    Narana Uuuuu

G
Tudo que eu puder fazer
             C9
Pra ser tudo o que Você desejou
            Em                   D
Quero viver tudo que Você sonhou
           C
Ser muito mais Você, e menos eu
          G
Eu quero minha vida dedicar
            C9
E esperar o dia que Te encontrarei
             Em                  D            C
Te abraçar e te entregar o medo que eu carreguei
        F                       C
Mas ele nunca conseguiu me parar

Eu juro

     F
Ele nunca conseguiu me parar
C
Se sua história é a mesma, cante lá lá lá

G
La lá lá lá lá lá lá
Lá lá lá lá
C9
La lá lá lá lá lá lá
Lá lá lá lá
Em                  D
La lá lá lá lá lá lá
Lá lá lá lá
C9                  G
La lá lá lá lá lá lá


                         C9
Ele nunca conseguiu me parar
                         Em
Ele nunca conseguiu me parar
          D        C9
Ele nunca conseguiu

La lá lá lá lá lá lá
Lá lá lá lá

----------------- Acordes -----------------
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
D9 = X X 0 2 3 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
