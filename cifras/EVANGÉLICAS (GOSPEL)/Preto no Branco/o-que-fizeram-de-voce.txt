Preto No Branco - O Que Fizeram de Você

[Intro]  A   A4   A   E/G#   A   A4   A   A4/E

E|-----------------------------------------------|
B|---2---2---2---0---2----3-3-2-3-2-0------------|
G|---2-----------------1----------------2--------|
D|---2-------------------------------------------|
A|---0-------------------------------------------|
E|-----------------------------------------------|

E|-----------------------------------------------|
B|---2---2---2---0---2-----3---3---2---3---2---0-|
G|---2------------------1------------------------|
D|---2-------------------------------------------|
A|---0-------------------------------------------|
E|-----------------------------------------------|

E|-----------------------0--------------0--------|
B|-----------------------------------------------|
G|----2--1---2----------------1----2-------------|
D|-----------------------------------------------|
A|-----------------------------------------------|
E|-----------------------------------------------|


   A                E/G#                   D/F#  D7+/F#  E9
  Dizem por aí que Deus mandou o recado errado
  A             E/G#                        D/F#  D7+/F#  A7/13
  E que por dinheiro faz do herdeiro um escravo
 D9             D#°                  C#m7
   Em nome de quem corromperam o sagrado?
       F#7/13-
   Eu não sei
 Bm7            A9/C#   Dm7        D/E    A9
   Minha fé me diz: só pode ser o deus errado
 F#7/13-               Bm7
   E o que fizeram de Você?

            D/E E/D    C#m7  F#7/13-
   Um curandeiro mercenário
                             Bm7
   E o que eles dizem que Tu és?
             D/E  E/D      C#m7  F#7/13-
   Algum mordomo, um secretário
                   Bm7
   Você não é isso aí
           D/E  E/D     C#m7  F#7/13-
   És Aba-Pai, Deus Elohim
                          Bm7
   Eu sei quem Tu és pra mim
   A9/C#   Dm7       D/E     A9
   Eterno Deus, Princípio e Fim

  (  A9  A5+/4  F#m7  E9  )

    A9                      E/G#          D/F#  D7+/F#  E9
   Dizem por aí que o sacrifício não foi pago
     A9         E/G#                   D/F#  D7+/F#  A7/13  A7/13-
   Que a fé da gente é insuficiente o saldo
  D9                 D#°                     C#m7
   Tanto sacrifício, outro Cristo, outro calvário
             F#7/13-
   Vai entender
     Bm7        A9/C#     Dm7      D/E   A9
   Graça é benefício vitalício consumado
  F#7/13-                Bm7
   E o que fizeram de você?
            D/E E/D    C#m7   F#7/13-
   Um curandeiro mercenário
                             Bm7
   E o que eles dizem que Tu és?
             D/E  E/D    C#m7    F#7/13-
   Algum mordomo, um secretário
                   Bm7
   Você não é isso aí
           D/E  E/D     C#m7  F#7/13-
   És Aba-Pai, Deus Elohim
                          Bm7
   Eu sei quem Tu és pra mim
   A9/C#   Dm7       D/E     A9  F#7/13-
   Eterno Deus, Princípio e Fim
                    Bm7
   Você não é isso aí
           D/E  E/D     F#6/9  D#7/9
   És Aba-Pai, Deus Elohim
                          Bm7
   Eu sei quem Tu és pra mim
   A9/C#   Dm7       D/E
   Eterno Deus, Princípio e Fim

   [Solo]  D  E/D  C#m7  F#7/13-  Bm7  D/E  A9

    D              E/D   C#m7        F#7/13-
   Desde a antiguidade, desde o princípio
    Bm7        D/E   A9
   Nunca se ouviu falar
    D               E/D     C#m7       F#7/13-
   De um Deus que trabalha para os que O amam
    Bm7        D/E   A9
   Nunca se ouviu falar
   D               E/D  C#m7       F#7/13-
   É sempre presente,  Todo inteligente
    Bm7        D/E   A9
   Nunca se ouviu falar

   De um Deus como o nosso Deus

   D              E/D   C#m7        F#7/13-
     Ha ha ha ha       he he he he he

    Bm7        D/E   A9  A5+/4  F#6/9
   Nunca se ouviu falar

                       F#6/9
   O que fizeram de Você?

----------------- Acordes -----------------
A = X 0 2 2 2 0
A4 = X 0 2 2 3 0
A4/E = 0 0 2 2 3 0
A7/13 = X 0 X 0 2 2
A7/13- = X 0 X 0 2 1
A9 = X 0 2 2 0 0
A9/C# = X 4 2 2 0 X
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
D#7/9 = X 6 5 6 6 X
D#° = X X 1 2 1 2
D/E = X X 2 2 3 2
D/F# = 2 X 0 2 3 2
D7+/F# = 2 X 0 2 2 2
D9 = X X 0 2 3 0
Dm7 = X 5 7 5 6 5
E/D = X 5 X 4 5 4
E/G# = 4 X 2 4 5 X
E9 = 0 2 4 1 0 0
F#6/9 = 2 1 1 1 X X
F#7/13- = 2 X 2 3 3 2
F#m7 = 2 X 2 2 2 X
