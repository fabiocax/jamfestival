Preto No Branco - Me Faz Voar

[Intro] Am7  C9  F9  D9

           Am                 C
Os meus pés já não tocam a terra
           F                      Dm
Sinto o vento de Deus em minhas mãos
            Am               C       F    Dm
Agora só sei viver se for na Tua dimensão
      Am            C
Envolvido na Tua presença
    F              Dm
Atraído pra Ti adorar
            Am                      C           F    Dm
Você me deu asas foi para que eu pudesse Te alcançar

   G/B    C
Me faz voar
 C/E  F
Me faz viver
      G/B    C
Pra Te adorar

  C/E  F
Pra me render
     G/B     C    C/E  F
Bem mais de Ti eu quero em mim
     G/B  C  C/E    F
Tua face eu quero ver

                  Am
Tua Glória me faz voar
    C     F
Oh oh ohhh
                  Am
Tua Glória me faz cantar
   C     F
Oh oh ohhh
                  Am
Tua Glória me faz viver
   C     F
Oh oh ohhh
                  Am
Tua Glória me faz dançar / pular

Pular, pular, pular, pular

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G/B = X 2 0 0 3 3
