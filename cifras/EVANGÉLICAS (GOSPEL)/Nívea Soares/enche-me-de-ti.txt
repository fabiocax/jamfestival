Nívea Soares - Enche-me de Ti

Intro.: C 63 51 C Ab7M Bb9 63
        C 63 51 C Ab7M Bb9 52 C

   C9                                          G
Ao Teu encontro eu vou aonde muitos não querem ir
   C9                     C/E                  G
Ao Teu chamado eu vou , o Teu perfume eu vou seguir

      C/E        F9            G/F      C/G   F/A
Até estar inteiramente em Teus braços e me perder
       (Dm7)    C/E         F9
Em Teu abraço e ser completamente transformado
     G4      G
Pelo Teu amor


     Ab7M     Bb/Ab  Ab7M       Bb9
Pois toda criação   aguarda ansiosa pela
C9                                Ab7M          Bb9
Manifestação da Tua glória em mim eis-me aqui Senhor
    Ab9/C      Bb/D    G4               G
Manifesta-te a mim manifesta-te através de mim



C9           53 40 41 Ab7M        Bb9      Fm
Enche-me de Ti        enche-me de Ti quero responder​
Gm7                    C9
   Ao Teu chamado para esta geração
              53 40 41 Ab7M        Bb9      Fm
Enche-me de Ti         enche-me de Ti quero ser​
Gm7                  C9          Bb/C Cm7 F/C
   Tua resposta para esta geração


   C9
Ao Teu encontro eu vou...

      C/E        F9            G/F
Até estar inteiramente em Teus braços...


C9           53 40 41 Ab7M        Bb9      Fm
Enche-me de Ti        enche-me de Ti quero responder​
Gm7                    C9
   Ao Teu chamado para esta geração
C             53 40 41 Ab7M        Bb9      Fm
Enche-me de Ti         enche-me de Ti quero ser​
Gm7                  C9
   Tua resposta para esta geração
         (na 2ª vez) C9          Ab7M Bb9

SOLO GUITARRA
(C9 Ab7M Eb9 Db9 2x)  C9 Ab7M Eb9 Db9 B9
(Cm Bb/D Eb9 Fm7 Eb/G Ab7M Bb9 G/B) 2x


C9             Ab7M        Bb9      Fm7(9)
Enche-me de Ti enche-me de Ti quero responder​..

----------------- Acordes -----------------
Ab7M = 4 X 5 5 4 X
Ab9/C = 8 X X 8 9 6
B9 = X 2 4 4 2 2
Bb/Ab = 4 X 3 3 3 X
Bb/C = X 3 X 3 3 1
Bb/D = X 5 X 3 6 6
Bb9 = X 1 3 3 1 1
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
C/G = 3 3 2 X 1 X
C9 = X 3 5 5 3 3
Cm = X 3 5 5 4 3
Cm7 = X 3 5 3 4 3
Db9 = X 4 6 6 4 4
Dm7 = X 5 7 5 6 5
Eb/G = 3 X 1 3 4 X
Eb9 = X 6 8 8 6 6
F/A = 5 X 3 5 6 X
F/C = X 3 3 2 1 1
F9 = 1 3 5 2 1 1
Fm = 1 3 3 1 1 1
Fm7 = 1 X 1 1 1 X
Fm7(9) = X X 3 1 4 3
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G/F = 1 X X 0 0 3
G4 = 3 5 5 5 3 3
Gm7 = 3 X 3 3 3 X
