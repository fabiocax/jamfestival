Nívea Soares - Me Entrego a Ti

[Intro] Bb  Eb7M/C  Gm  Gm/Eb  Gm11/Eb
        Bb  Eb7M/C  Gm  Gm/Eb  Gm11/Eb

Bb                    Eb7M/C  Gm
   Quebrantado me achego      aos teus pés
Gm/Eb  Gm11/Eb
 Outra vez
Bb                     Eb7M/C   Gm
   Tua voz preciso ouvir em meu coração
   Gm/Eb  Gm11/Eb
Guiando-me

Eb  Gm       Bb     Eb7M/C
Ooo    me entrego a Ti
Eb        Gm      F
Ao teu querer me rendo
Eb  Gm    Bb      Eb7M/C
Ooo    no Santo Lugar
     Eb     Gm      F
Minh`alma segura está


[Refrão]
Gm      Gm/Eb  Bb       Eb7M/C  Gm
   Vem com teu fogo em meu coração
        Gm/Eb   Bb     F
Santo Espírito enche-me de ti
Eb      Gm     Bb                    Eb7M/C
   Tua glória inunda meu ser e eu te amo
   Eb9
Te amo

[Ponte] Gm  Eb  Bb  Eb7M/C
        Gm  Eb  Bb  F  Eb
        Gm  Bb  Eb7M/C  Eb9

Bb               Eb7M/C  Gm
   Vazio eu me prostro   aos teus pés
Gm/Eb  Gm11/Eb
 Outra vez
Bb                       Eb7M/C  Gm
   O teu amor constrange o meu   coração
  Gm/Eb  Gm11/Eb
Curando-me

Eb  Gm       Bb     Eb7M/C
Ooo    me entrego a Ti
Eb        Gm      F
Ao teu querer me rendo
Eb  Gm    Bb      Eb7M/C
Ooo    no Santo Lugar
     Eb     Gm      F
Minh`alma segura está

[Refrão 2x]
Gm      Gm/Eb  Bb       Eb7M/C  Gm
   Vem com teu fogo em meu coração
        Gm/Eb   Bb     F
Santo Espírito enche-me de ti
Eb      Gm     Bb                    Eb7M/C
   Tua glória inunda meu ser e eu te amo
   Eb9
Te amo

[Passagem] Gm  Eb  Bb  Eb7M/C
           Gm  Eb  Bb  F  Eb
           Gm  Bb  Eb7M/C  Eb9

Gm      Gm/Eb  Bb       Eb7M/C  Gm
   Vem com teu fogo em meu coração
        Gm/Eb   Bb     F
Santo Espírito enche-me de ti
Eb      Gm     Bb                    Eb7M/C
   Tua glória inunda meu ser e eu te amo
   Eb9
Te amo

[Final] Gm  Eb  Bb  Eb7M/C
        Gm  Eb  Bb  F  Eb
        Gm  Bb  Eb7M/C  Eb9

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Eb = X 6 5 3 4 3
Eb7M/C = X 3 1 P3 P3 3
Eb9 = X 6 8 8 6 6
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
Gm/Eb = 3 6 5 3 3 3
