Nívea Soares - Aumenta o Fogo

[Intro] C#m7  B9/C#  C#m7  B9/C#
          C#m7  B9  A9/5b  F#m7  G#m7

E|-----------------------------------------------|
B|-----------------------------------------------|
G|-----------------------------------------------|
D|------4/6-4----------4/6-9-----------------|
A|--4-7-------7--2-4-7-------2-2-4-2-4----|
E|-----------------------------------------------|

         C#m7                A9/5b  A9
Aumenta o fogo do Teu Espírito
          F#m7    B9/G#                    C#m7
Aumenta a unção quero estar mais perto de Ti
           C#m7                              A9/5b  A9
Coloca em mi temor do Teu Santo Nome
               F#m7  B9/G#    C#m7
Desperta o meu amor por Ti Jesus
                    F#m7
Além das palavras
                 C#m7       B9
E das canções de amor

                   F#m7
Que a Tua vida
                C#m7    B9
Flua do meu interior
                     F#m7
Que os meus atos
                   C#m7  B9
Honrem o Teu amor
                A9/5b        A9
Para exaltar a Ti Senhor
                F#m7          B9
Para exaltar a Ti Senhor
       E9      E9/G#
Jesus Tu és
                   A9
Digno da minha vida
                  F#m7
Digno de tudo o que eu sou
B9  G#/C          ( C#m7  B9/C#  C#m7  B9 )
               Tu és

       C#m7  B/D#  E  F#m7       E9/G#
Jesus Tu és
                   A9
Digno da minha vida
                  F#m7
Digno de tudo o que eu sou

( B9  G#/C )
( C#m7  B/D#  E  F#m7  E9/G# )
( A9  F#m7  D  C#  B  A  B )

                   C#m7
Para Ti eu danço
           B/D#    E
Para Ti eu grito
                  A9
Para Ti eu vivo
     F#m7   B9
Jesus

( D  C#  B  F#  A  B )

[Final] C#m7  B/D#  E  F#m7  E9/G#
           A9   F#m7  D  C#  B  A  B

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
A9/5b = X 0 1 2 0 0
B = X 2 4 4 4 2
B/D# = X 6 X 4 7 7
B9 = X 2 4 4 2 2
B9/C# = X 4 4 4 2 2
B9/G# = 4 X 4 4 2 2
C# = X 4 6 6 6 4
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E9 = 0 2 4 1 0 0
E9/G# = 4 X 4 4 5 X
F# = 2 4 4 3 2 2
F#m7 = 2 X 2 2 2 X
G#/C = X 3 X 1 4 4
G#m7 = 4 X 4 4 4 X
