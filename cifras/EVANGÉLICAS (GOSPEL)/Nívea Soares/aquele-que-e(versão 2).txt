Nívea Soares - Aquele Que É

[Intro] Dm  C/E  F  G

         Dm7                           C/E
Posso ouvir o som das águas do rio de Deus que vem chegando
         Bb9                        F/A             Am
Posso ouvir o som da chuva que vem sobre a terra seca
         Dm7                                C
Posso sentir o vento do Espírito sobre o vale de ossos secos
         Bb9                                 F/A               Am          Dm7  C/E  F G9
Posso sentir o tremor dos montes ao ouvir a voz do todo poderoso nosso Deus

           Dm7  C/E           Bb  F/A
Aquele que era     Aquele que é
           Gm7     C4  C
Aquele que há de vir

              Dm  C/E             F  G
Ele vem sobre mim ele vem sobre ti

Dm       Bb              F           C/E
Esta e a geração dos que amam tua presença

Dm7      Bb         F/A           C4  C
Esta e a geração dos que buscam tua face

Dm7     C/E    F9     G
Jesus Jesus Jesus Jesus

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bb9 = X 1 3 3 1 1
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
C4 = X 3 3 0 1 X
D5 = X 5 7 7 X X
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
E5 = 0 2 2 X X X
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
F5 = 1 3 3 X X X
F9 = 1 3 5 2 1 1
G = 3 2 0 0 0 3
G5 = 3 5 5 X X X
G9 = 3 X 0 2 0 X
Gm7 = 3 X 3 3 3 X
