Nívea Soares - Enche Este Lugar

[Intro] D9  A11  Bm11  G9
        D9  A11  Bm11  G9
        D9  A11  Bm11  G9
        D9  A11  Bm11  G9
        D9  A11  Bm11  G9
        D9  A11  Bm11  G9

          D9          A11               Bm11  G9
Tu que estás assentado   sobre os querubins
          D9            A11                 Bm11  G9
Tu que estás entronizado   em meio aos serafins
          D9              A11                 Bm11  G9
Tu que estás aclamado por anjos, vem neste lugar

          D9          A11                    Bm11  G9
Tu que estás assentado   sobre um trono de glória
          D9            A11                          Bm11  G9
Tu que estás entronizado   sobre os louvores do seu povo
          D9                     A11
Tu que estás aclamado por tua igreja
             Bm11  G9
Vem neste lugar


             D9      A11
Vem neste luga - a - ar
              Bm11    G9
Enche este luga - a - ar
              D9      A11
Enche este luga - a - ar
               Bm11  G9
Enche a minha vida

              D9      A11
Vem neste luga - a - ar
              Bm11    G9
Enche este luga - a - ar
              D9      A11
Enche este luga - a - ar
               Bm11  G9
Enche a minha vida

( D9  A11  Bm11  G9 )
( D9  A11  Bm11  G9 )

          D9          A11               Bm11  G9
Tu que estás assentado   sobre os querubins
          D9            A11                 Bm11  G9
Tu que estás entronizado   em meio aos serafins
          D9              A11                 Bm11  G9
Tu que estás aclamado por anjos, vem neste lugar

          D9          A11                    Bm11  G9
Tu que estás assentado   sobre um trono de glória
          D9            A11                          Bm11  G9
Tu que estás entronizado   sobre os louvores do seu povo
          D9                     A11
Tu que estás aclamado por tua igreja
             Bm11  G9
Vem neste lugar

             D9      A11
Vem neste luga - a - ar
              Bm11    G9
Enche este luga - a - ar
              D9      A11
Enche este luga - a - ar
               Bm11  G9
Enche a minha vida

            D9      A11
Vem neste luga - a - ar
              Bm11    G9
Enche este luga - a - ar
              D9      A11
Enche este luga - a - ar
               Bm11  G9
Enche a minha vida

( D9  A11  Bm11  G9 )
( D9  A11  Bm11  G9 )

----------------- Acordes -----------------
A11 = X 0 2 2 3 0
Bm11 = X 2 2 4 3 2
D9 = X X 0 2 3 0
G9 = 3 X 0 2 0 X
