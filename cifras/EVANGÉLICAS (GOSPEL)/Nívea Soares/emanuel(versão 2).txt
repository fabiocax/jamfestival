Nívea Soares - Emanuel

D9                              D9/F#
Enquanto as lutas vêm me afogar
Bm7                           G
E a angústia tenta me sufocar
Bm7                  G          D9
O teu amor aquece o meu coração
D9/F#
Enquanto o medo quer me paralisar
Bm7                           G
E a incerteza me fazer voltar
Bm7              G                D9
A tua mão sustenta o meu caminhar

      G                                    Bm7
Ao meu lado tu estás sempre perto tu estás
D9                        Em7
Mesmo em meio a dor estás
          G                             Bm7
Na bonança tu estás tempestades tu estás
D9                     A9
Em todo tempo tu estás


Refrão 1:

D9  Bm7  A9                         G/B  G
Ema____nuel Deus conosco, deus bem perto

D9           A9                  G/B          G  Bm7
Ema____nuel     Rei humilde, exaltado poderoso és

Intro: G  Bm7  G  Bm7  G  Bm7  G  Bm7

Verso 3:

D9                                        Bm7           G
O soberano deus, o criador por amor se deu, se esvaziou

Bm7            G           D9
Sendo rico pobre se tornou

D9
Como um cordeiro ele se entregou
Bm7                             G
E minhas dores sobre si levou
Bm7             G        D9
Maravilhoso é o seu amor

D9 Bm7   A9                G/B          G
Ema____nuel deus conosco, deus bem perto
D9 Bm7   A9               G/B    G
Ema____nuel Rei humilde, exaltado

           Bm7   G       Bm7  G
Poderoso  és glorioso és
                Bm7  G     Bm7  G
Maravilhoso  és Jesus,  Jesus

(Refrão)

----------------- Acordes -----------------
A9 = X 0 2 2 0 0
Bm7 = X 2 4 2 3 2
D9 = X X 0 2 3 0
D9/F# = 2 X 0 2 3 0
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
