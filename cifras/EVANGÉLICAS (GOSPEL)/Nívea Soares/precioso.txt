﻿Nívea Soares - Precioso

Afinação DADGAD

INTRO :  Em7 D9 C9    Em7 D9 C9
                 Em7 D9 C9    Em7 D9 F9
                       Em7                                        Bm7
Não posso viver nenhum dia sem Tua Presença
   Am7/B     C9                G9/E
Jesus,     amado do meu coração
                       Em7                                        Bm7
Não posso viver nenhum dia sem o Teu Espírito
     Am7/B                    C9                         G9/E
Fluindo em mim, mostrando-me o Teu coração

    G  D/F#    G9/E   F9
Precioso é o Teu amor
       G9/E          G/B               C9
É melhor um só dia em Tua casa
              Am7                   D    D/C
Que mil dias em outro lugar


   G9/B C9    G9/E D9
Jesus,          santo
      C9 G9/E            Am7  D
Jesus,              meu dono

INTRO :  Em7 D9 C9    Em7 D9 C9
                 Em7 D9 C9    Em7 D9 F9

ESTROFE
PONTE
CORO
TAG IGUAL AO CORO

END :  Em7 D/F# G9    Am7 G/B D9
             Em7 D/F# G9   Am7 G/B F9 C/E  Gm7/D#

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Am7/B = X 2 2 0 1 5
Bm7 = X 2 4 2 3 2
C/E = 0 3 2 0 1 0
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
D/C = X 3 X 2 3 2
D/F# = 2 X 0 2 3 2
D9 = X X 0 2 3 0
Em7 = 0 2 2 0 3 0
F9 = 1 3 5 2 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G9 = 3 X 0 2 0 X
G9/B = 7 X X 7 8 5
G9/E = 0 2 0 2 0 3
