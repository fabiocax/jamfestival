Diante do Trono - Dono do Meu Coração

(intro)  G  D/F#  Em7  D/F#  G   D/F#  Em7  G/A  D

D         D/F#         G      D/F#
Rei do universo Te dou meu louvor
Em7        F#m7       G7M       A    D/F#
Com tantas bênçãos me cercas, Senhor
G          D/F#        Em7         Bm7
Só Tu és a fonte de alegria e prazer
D/F# G                 D/F#   Em7  G/A D
  Melhor que a própria vida é Te conhecer

D       D/F#    G       D/F#
Tu és o dono do meu coração
Em7     F#m7    G       A   D/F#
Tu és o dono do meu coração
         G               D/F#
e não há outro (e não há outro)
         G               Bm7
e não há outro (e não há outro)
D/F# G       D/F#    Em7     D/F#
  Só Tu és o dono do meu coração

   G       D/F#    Em7    A Bb
Só Tu és o dono do meu coração

Eb       Eb/G    Ab     Eb/G
Paz e família, abrigo e luz
Fm7           Gm7          Ab         Bb  Eb/G
Minhas necessidades supres todas em Jesus
Ab                 Eb/G        Fm7    Cm7
Mas nem o ouro e a prata podem satisfazer
Eb/G  Ab          Eb/G     Fm Ab/Bb Eb
   A sede da minh'alma é a Ti pertencer
Eb      Eb/G    Ab      Eb/G
Tu és o dono do meu coração
Fm7     Gm7     Ab      Bb   Eb/G
Tu és o dono do meu coração
         Ab              Eb/G
e não há outro (e não há outro)
         Ab              Cm7
e não há outro (e não há outro)
Eb/G Ab      Eb/G    Fm7     Eb/G
  Só Tu és o dono do meu coração
   Ab      Eb/G    Fm7   Bb  B
Só Tu és o dono do meu coração
E        E/G#      A         E/G#
Planos e sonhos Tu tens para mim
F#m7      G#m7        A       B    E/G#
Eu maravilhado digo: "Eis me aqui"
A         E/G#        F#m7        C#m7
Quero agradar-Te e cumprir Teu querer
E/G# A             E/G#       F#m7  A/B   E
     Caminhando em graça vou   obe  de  cer
E       E/G#    A       E/G#
Tu és o dono do meu coração
F#m7    G#m7    A             B  E/G#
Tu és o dono do meu coração
         A               E/G#
e não há outro (e não há outro)
         A               C#m7
e não há outro (e não há outro)
E/G#  A      E/G#    F#m7    E/G#
  Só Tu és o dono do meu coração
   A       E/G#    F#m7  B  C
Só Tu és o dono do meu coração
F           F/A        Bb        F/A
Em minha jornada intimamente Te amar
Gm7         Am7             Bb     C  F/A
Para que na glória quando o dia chegar
Bb                F/A            Gm7    Dm7
Ao contemplar Teu rosto eu venha reconhecer
F/A Bb          F/A       Gm7   Bb/C F
    O olhar tão doce que amou o meu ser

(intro meio) Bb  F/A  Gm7  F/A   Bb   F/A   Gm7  Bb/C   F

F       F/A     Bb      F/A
Tu és o dono do meu coração
Gm7     Am7     Bb      C    F/A
Tu és o dono do meu coração
         Bb              F/A
e não há outro (e não há outro)
         Bb              Dm7
e não há outro (e não há outro)
F/A  Bb      F/A     Bb      F
  Só Tu és o dono do meu coração
F/A  Bb      F/A     Bb      F
  Só Tu és o dono do meu coração
F/A Bb      F/A           Gm7           F/A
    Tu és o dono, Tu és o dono, Tu és o dono.
Bb      F/A     Gm7 Bb/C  F
Tu és o dono do meu coração

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/B = X 2 2 2 2 X
Ab = 4 3 1 1 1 4
Ab/Bb = X 1 1 1 1 X
Am7 = X 0 2 0 1 0
B = X 2 4 4 4 2
Bb = X 1 3 3 3 1
Bb/C = X 3 X 3 3 1
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
C#m7 = X 4 6 4 5 4
Cm7 = X 3 5 3 4 3
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Dm7 = X 5 7 5 6 5
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
Eb = X 6 5 3 4 3
Eb/G = 3 X 1 3 4 X
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
F#m7 = 2 X 2 2 2 X
F/A = 5 X 3 5 6 X
Fm = 1 3 3 1 1 1
Fm7 = 1 X 1 1 1 X
G = 3 2 0 0 0 3
G#m7 = 4 X 4 4 4 X
G/A = 5 X 5 4 3 X
G7M = 3 X 4 4 3 X
Gm7 = 3 X 3 3 3 X
