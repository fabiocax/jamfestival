Diante do Trono - Deus do Recomeço

Intro Pad: Em7
Band-in: C7+  Em  C7+  D (2x)

       C
Quanto tempo eu perdi
          Em
Longe do teu amor
      C
Mas agora reconheci
                  Em
Estou voltando Senhor

2º verso:

      C          Em
O passado vai ficar pra trás
     C                    Em
Dos meus pecados, não te lembras mais
   C                        D
A Tua Palavra me cura, me refaz


Refrão:

G
Deus do Recomeço
    Am
Perdão que eu não mereço
 C
Vira a página
    Em
Da minha história
  C            D
  Tudo novo se faz

1ª vez: C7+  Em  C7+  D
2ª vez: D
Final: Am  C  G  D (2x)
       C  C  G

2ª Parte:

       Am           C                   G    D
Se alguém está em Cristo, nova criatura é
    Am               C                  G    D
As coisas velhas passaram Tudo novo se fez
    Am            C                     G
O choro dura uma noite Mas alegria vem
            D                          Am   C   G   D   Volta Refrão
A alegria vem Tua alegria vem pela manhã

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C7+ = X 3 2 0 0 X
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
