Diante do Trono - Tetelestai

[Intro] Em7  C  GM7
        Em7  C  GM7

Em7    C
Minha dor
      GM7
Ele levou, Ele levou
Em7    C
Meu pecar
       Am
Ele levou, Ele levou
D                     Em
Minhas enfermidades levou
               CM7
Ele levou sobre si
                Em
Ele levou minha cruz

     G4  G
Eu adoro
    D/F#
Eu o amo

       C7M
Jesus Cristo
            G4       G
Único e suficiente Salvador
Meu Salvador

Em7     C
Minhas maldições
      GM7
Ele levou, Ele levou
Em7       C
Medos e aflições
      Am
Ele levou, Ele levou
D                Em
Minha condenação levou
                CM7
Ele levou sobre si
                 Em
Ele levou minha cruz

     G4  G
Eu adoro
    D/F#
Eu o amo
       C7M
Jesus Cristo
            G4       G
Único e suficiente Salvador
Meu Salvador

D                        G
Ele é o Cordeiro de Deus
C                         G
Que tira o pecado do mundo
D                         G
Tetelestai, está consumado

C                            G
Tetelestai, o preço foi pago
D                          G
Tetelestai, está consumado
C
Tetelestai, o preço foi pago

     G4  G
Eu adoro
    D/F#
Eu o amo
       C7M
Jesus Cristo
            G4       G
Único e suficiente Salvador
Meu Salvador

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C7M = X 3 2 0 0 X
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
G4 = 3 5 5 5 3 3
