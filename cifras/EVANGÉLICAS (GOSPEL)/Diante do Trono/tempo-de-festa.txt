Diante do Trono - Tempo de Festa

Intro 1 :A9 A9/F# D4/G F9 C/E F/D A/C# D D9/B D9/A E/G# E/C E/C# E/B D9/A F9 G9 F/A Em7/B F9 G9 D9/B E9


Intro 2: D9/A D7M/A G/A DM7/A   D9/A DM7/A F/A G/A D9/A D7M/A G/A D7M/A F/A G/A A9 G/A

A9                  A/C#
Este é um tempo de festa
D9               D9 D7M
Este é um tempo de louvor
     D9/B D7M/B E9
Prá celebrar aquele que primeiro
    A9 A9/F# E/G#
nos amou
     A9                 G/A
Transformou nosso choro em riso
    D9                 D9 D7M
Nos deu novas vestes de louvor
    D9/B  D7M/B  E9
Pra celebrar aquele que primeiro
A9
nos amou


A9/F  A/F#           C#m7
Nos tirou do império das trevas
       D            E
E nos deu perdão e paz
E         F°      F#m            C#m
Ar  -    ran -   cou todas as feridas
           Bm7
Nos fez felizes demais

D/A A      F#m6 F#m
Festa  alegria
       D           E/D
É uma dança de celebração
   F#m6           F#m   A4/E     A/E
Ao ú   -   ni  -  co   dig   -   no

  Bm7       Bm C#m D     E4 E
Jesus.      seu nome é Jesus

D/A A    F#m6 F#m
Festa alegria
      D           E/D
É um povo que se reúne aqui
F#m6        F#m       D/A     A
Di - an  -  te   do   tro  -  no
    Bm      (1°vez Bm C#m D)(2°vez E4 E)
Do rei,     do rei dos reis
           D/A A
Seu nome é Jesus

Passagem: D/A A Am G Am D/A    D/A A F G

A                   A/C#
Este é um tempo de festa
D                A/C#
Este é um tempo de louvor
    Bm7        D/E                 A      D/F# E/G#
Prá celebrar aquele que primeiro nos amou
     A                   G/A
Transformou nosso choro em riso
    D                 A/C#
Nos deu novas vestes de louvor
      Bm        D/E               A
Pra celebrar aquele que primeiro nos amou
E   F°  F#m              C#m7
Nos tirou do império das trevas
       D            E
E nos deu perdão e paz
E         F°      F#m            C#m
Ar  -    ran -   cou todas as feridas
           Bm7
Nos fez felizes demais

D/A A      F#m6 F#m
Festa  alegria
       D           E/D
É uma dança de celebração
   F#m6           F#m   A4/E     A/E
Ao ú   -   ni  -  co   dig   -   no

  Bm7       Bm C#m D     E4 E
Jesus.      seu nome é Jesus

D/A A    F#m6 F#m
Festa alegria
      D           E/D
É um povo que se reúne aqui
F#m6        F#m       D/A     A
Di - an  -  te   do   tro  -  no
    Bm      (1°vez Bm C#m D)(2°vez E4 E)
Do rei,     do rei dos reis
           D/A A
Seu nome é Jesus

                A                  D/A
Cantamos de alegria          ô  ô  ô ...
                A          A/C#
Dançamos de alegria          ô  ô  ô ...
               B              E/B
Pulamos de alegria           ô  ô  ô ...
                B          B/D#         E
Gritamos de alegria          ô  ô  ô ...

E/B B     G#m6 G#m
Festa  alegria
      E          F#/E
É uma dança de celebração
   G#m6           G#m   B4/F#    B/F#
Ao ú   -   ni  -  co   dig   -   no
 C#m7        C#m D#m E   F#4 F
Jesus.      seu nome é Jesus

E/B B    G#m6 G#m
Festa alegria
     E             F#/E
É um povo que se reúne aqui
G#m6        G#m       E/B     B
Di - an  -  te   do   tro  -  no
   C#m7 C#m7,D#m7 E      F#4   F#
Do rei,     do rei dos reis
                B                    E/B
Cantamos de alegria          ô  ô  ô ...
                B          B/D#
Dançamos de alegria          ô  ô  ô ...
               B              E/B
Pulamos de alegria            ô  ô  ô ...
                B          B/D#         E
Gritamos de alegria          ô  ô  ô ...
       B
Seu nome é Jesus

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A/E = 0 X 2 2 2 0
A/F# = 2 0 2 2 2 0
A4/E = 0 0 2 2 3 0
A9 = X 0 2 2 0 0
A9/F = 1 0 2 2 0 0
A9/F# = 2 X 2 2 0 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
B/D# = X 6 X 4 7 7
B/F# = X X 4 4 4 2
B4/F# = 2 2 4 4 5 2
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
C/E = 0 3 2 0 1 0
D = X X 0 2 3 2
D#m = X X 1 3 4 2
D#m7 = X X 1 3 2 2
D/A = X 0 X 2 3 2
D/E = X X 2 2 3 2
D/F# = 2 X 0 2 3 2
D4/G = 3 X X 2 3 3
D7M = X X 0 2 2 2
D7M/A = 5 5 4 2 2 2
D7M/B = X 2 0 P2 P2 2
D9 = X X 0 2 3 0
D9/A = X 0 X 2 3 0
D9/B = X 2 0 2 3 0
E = 0 2 2 1 0 0
E/B = X 2 2 1 0 0
E/C = X 3 2 1 0 0
E/C# = X 4 2 1 0 0
E/D = X 5 X 4 5 4
E/G# = 4 X 2 4 5 X
E4 = 0 2 2 2 0 0
E9 = 0 2 4 1 0 0
Em7/B = X 2 2 0 3 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
F#/E = X X 2 3 2 2
F#4 = 2 4 4 4 2 2
F#m = 2 4 4 2 2 2
F#m6 = 2 X 1 2 2 X
F/A = 5 X 3 5 6 X
F/D = X X 0 5 6 5
F9 = 1 3 5 2 1 1
F° = X X 3 4 3 4
G = 3 2 0 0 0 3
G#m = 4 6 6 4 4 4
G#m6 = 4 X 3 4 4 X
G/A = 5 X 5 4 3 X
G9 = 3 X 0 2 0 X
