Diante do Trono - Preciso de Ti

Intro 2x: G  D/F#  A9  E

E|--------------------------------------|
B|--------------------------------------|
G|--------------------------------------|
D|--------------------------------------|
A|---0h2---0----------------------------|
E|-0-----------2------------------------|

            A9            F#m7
Preciso de Ti preciso do teu perdão
            A9               F#m7
Preciso de Ti quebranta meu coração
     D                  Bm                 F#m
Como a corça anseia por água, assim tenho sede
 C#m        D                 Bm               E4 E
Como terra seca, assim é minh'alma preciso de Ti

    F#m      C#m   D              A/C#  Bm7
Distante de Ti, Senhor, não posso vi-ver
            A/E      E
Não vale a pena existir

C#m              D                     A/C#     Bm7
Escuta o meu clamor, mais que o ar que eu res-piro
  Bm/A      E/G# Bm7 A E
Preciso de Ti
 F#m      C#m   D              A/C#  Bm7
Distante de Ti, Senhor, não posso vi-ver
            A/E          E
Não vale a pena existir
C#m             D                       A/C#     Bm7
Escuta o meu clamor, mais que o ar que eu res-piro
   Bm/A      G D/F# A E
Preciso de Ti.
       F#m7           D
Não posso esquecer, o que fizeste por mim
      C#m         Bm7
Como alto é o céu, Tua misericórdia é sem fim
C#m     D                     Bm                F#m
Como um pai se compadece dos filhos assim Tu me amas
C#m      D        Bm                       E4 E
Afasta as minhas transgressões preciso de Ti

    F#m    C#m      D             A  Bm
Distante de Ti, Senhor, não posso vi-ver
           A          E
Não vale a pena existir
   F#m   C#m    D                        A     Bm
Escuta o meu clamor, mais que o ar que eu res-piro
   Bm        (1°VEZ E/G# Bm7 A E)   (2°VEZ G D/F# A E)
Preciso de Ti (2x).

      C#m                   D
E as lutas vêm tentando me afastar de Ti
  F#m        C#m            D
Frieza e escuridão procuram me cegar
            Bm      C#m   F#m  E      D
Mas eu não vou desistir: ajuda-me, Senhor!
         Bm      C#m                 G A/G G A/G F7M G/F E4 E
Eu quero permanecer contigo até o fim

    F#m    C#m      D             A  Bm
Distante de Ti, Senhor, não posso vi-ver
           A          E
Não vale a pena existir
   F#m   C#m    D                        A     Bm
Escuta o meu clamor, mais que o ar que eu res-piro
   Bm       (1°VEZ E/G# Bm7 A E)   (2°VEZ G D/F# A E)
Preciso de Ti (2x).

           G  D9/F#
Preciso de Ti
           F  G
Preciso de Ti
           A  D9/F#
Preciso de Ti
           F  G
Preciso de Ti
           A9
Preciso de Ti

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A/E = 0 X 2 2 2 0
A/G = 3 X 2 2 2 X
A9 = X 0 2 2 0 0
Bm = X 2 4 4 3 2
Bm/A = X 0 4 4 3 X
Bm7 = X 2 4 2 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D9/F# = 2 X 0 2 3 0
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E4 = 0 2 2 2 0 0
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
F7M = 1 X 2 2 1 X
G = 3 2 0 0 0 3
G/F = 1 X X 0 0 3
