Diante do Trono - Isaías 53

[Intro] F#m   Bm/F#  E/G# A  E4 E
        A/C# D   A/C#   Bm   G

 A              Bm7/A          E/G#             F#m  F#m E C#m E/G#
Não havia nEle beleza ou formosura que nos agradasse
 A             Bm          E4 E               F#m E/G# D A/C#
Era rejeitado, o mais humilhado  entre os homens
        Bm7                   C#m7       D
Homem de dores, sabe o que é padecer, desprezado
                  E4 E
Dele não fizemos caso

 A             A/C#           D
Ele levou sobre si as nossas dores
Bm7            Bm7/A         E/G#    E
Ele levou sobre si as nossas transgressões
        A         A5+                D7+                 Dm7
E nos olhávamos para Ele ,pensávamos que eram seus próprios pecados
               E9 E/D
que o levavam ali
           C#m7      E4  E
Mas foi por mim e por ti


( F#m  F#m  D9  C#7  D  E4 E )

 A                    Bm7/A           E            F#m E/G#
Ele foi transpassado, moído pelas nossas iniquidades
 A                    Bm    A/C#  D    E         F#m   E D E E/G#
Oprimido e humilhado, não abriu a boca como um cordeiro
               Bm                      C#m7
Seu castigo nos traz paz, Suas chagas a cura
            D                E4   E
Por suas pisaduras ,  fomos sarados

        A              A/C#         E/D D
Porque Ele levou sobre si as nossas dor es
 Bm           Bm7/A            E/G#   E
Ele levou sobre si as nossas transgressões
        A           A5+                 D7+                  Dm7
E nos olhavámos para Ele, pensávamos que eram seus próprios pecados
              E9  E/D
que o levavam ali
           C#m7       E4 A/C# D Bm E
Mas foi por mim e por ti

  A         A/C#             D
Quando derramou Sua alma na morte
  A        A/C#         D
Fez oferta pelo pecado e se alegrou
      A/C#    Bm7
Porque Ele nos viu
                E4             E  A/C#
Somos frutos do Seu penoso trabalho
          Bm7  Bm7/A             G D/F# E4 F#4
Somos Sua porção, Sua satisfação

 B              B/D#         F#/E   E
Ele levou sobre si as nossas dor   es
 C#m7       C#m7/B          F#/A#     F#
Ele levou sobre si as nossas transgressões
        B            B5+                 E7+                    Em7
E nos olhávamos para Ele, pensávamos que eram seus próprios pecados
               F#9 F#/E
que o levavam ali
           D#m7       F#4 F#
Mas foi por mim e por ti

[Final] G#m   F#   E   G#m  F#  E   F#   G#m

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A5+ = X 0 3 2 2 0
B = X 2 4 4 4 2
B/D# = X 6 X 4 7 7
B5+ = X 2 1 0 0 X
Bm = X 2 4 4 3 2
Bm/F# = X X 4 4 3 2
Bm7 = X 2 4 2 3 2
Bm7/A = 5 2 4 2 3 2
C#7 = X 4 3 4 2 X
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
C#m7/B = 7 4 6 4 5 4
D = X X 0 2 3 2
D#m7 = X X 1 3 2 2
D/F# = 2 X 0 2 3 2
D7+ = X X 0 2 2 2
D9 = X X 0 2 3 0
Dm7 = X 5 7 5 6 5
E = 0 2 2 1 0 0
E/D = X 5 X 4 5 4
E/G# = 4 X 2 4 5 X
E4 = 0 2 2 2 0 0
E7+ = X X 2 4 4 4
E9 = 0 2 4 1 0 0
Em7 = 0 2 2 0 3 0
F# = 2 4 4 3 2 2
F#/A# = 6 X 4 6 7 X
F#/E = X X 2 3 2 2
F#4 = 2 4 4 4 2 2
F#9 = 2 4 6 3 2 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G#m = 4 6 6 4 4 4
