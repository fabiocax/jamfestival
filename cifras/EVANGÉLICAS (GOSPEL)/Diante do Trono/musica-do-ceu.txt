Diante do Trono - Música do Céu

Intro :  D9 G9/D  Bm7/D  A9/D   D9  G9/D  Bm7/D  A9/D    PADS
                      D9  G9/D   Bm7     Asus A9     D9/F#  G9
                      Bm7 A9      G G9    Gmaj7 G   F9 F   G9 A9   D9  D9

                Em7
Existe um som que vem
     Gmaj7/A   A9            D9  G9/D   D9
da Tua           musica Oh Deus
                  Em7              A9
Som de um vento impetuoso
                   D9    Dsus4
Vento do Espírito

 D9    Bm7 A9  D        Em7         
          E -  xis - te um   rio que flui

dos Teus    louvores Oh altíssimo
                   Em7                A9
É como um som de muitas águas
                  D9    Dsus4    D9   A/C#   Bm7 Em7  Gmaj7/A  A9
 águas do Espírito


                 D9/F#
Quero dançar
      G9                          D9/B   A9
ao som da musica do céu
              D9/F#   G              Asus4  A9    G/E G/B   A/C# A
e responder ao Teu chamado
                   D9
Quero avançar
   G9                                    Bm7   A9
quando a Tua musica se mover
            Em7     Asus4 A9
obedecer ao  Teu compasso

reintro Bm7 A/C#  D9 D/F# G9  Bm7 A9  G9/11 A/E D/F#
              Bm7 A/C#  D9 D/F# G9  Em7  C9 D/B Asus

ESTROFE
CORO

MODULACAO :  Bm7 A/C# STOP    D A/E    A  B

                  E9/G#
Quero dançar
      A9                          E9/C#    B9
ao som da musica do céu
              E9/G#   A             Bsus4  B9   A/F# A/C#   B/D# B
e responder ao Teu chamado
                  E9
Quero avançar
   A9                                     C#m7   B9
quando a Tua musica se mover
            F#m7     Bsus4  D9 A/C#    4/4  E   E
obedecer ao  Teu compasso

 E     E   D A/C#   E      D9   A/C#  A/F# B     2X

 E     E   D A/C#   E      E    D  A/C#

                  B9
  Sopra Espírito
 A9              E9     E9
  Flua Teu Rio
                       B9            A9
  ao Som da musica do céu
   A9/F#            E9          E9
  louvores do altíssimo             2X

                    B9 (STOP)          A9
  ao Som da musica do céu
   A9/F#             E9         E9       E9
  louvores do altíssimo

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A/E = 0 X 2 2 2 0
A/F# = 2 0 2 2 2 0
A9 = X 0 2 2 0 0
A9/D = X X 0 2 0 0
A9/F# = 2 X 2 2 0 0
Asus = X 0 2 2 2 0
Asus4 = X 0 2 2 3 0
B = X 2 4 4 4 2
B/D# = X 6 X 4 7 7
B9 = X 2 4 4 2 2
Bm7 = X 2 4 2 3 2
Bm7/D = X 5 4 2 3 2
Bsus4 = X 2 4 4 5 2
C#m7 = X 4 6 4 5 4
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
D/B = X 2 0 2 3 2
D/F# = 2 X 0 2 3 2
D9 = X X 0 2 3 0
D9/B = X 2 0 2 3 0
D9/F# = 2 X 0 2 3 0
Dsus4 = X X 0 2 3 3
E = 0 2 2 1 0 0
E9 = 0 2 4 1 0 0
E9/C# = X 4 4 1 0 0
E9/G# = 4 X 4 4 5 X
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
F#m7 = 2 X 2 2 2 X
F9 = 1 3 5 2 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G/E = 0 2 0 0 3 3
G9 = 3 X 0 2 0 X
G9/11 = 3 X 0 2 1 3
G9/D = X 5 X 7 8 5
Gmaj7 = 3 X 4 4 3 X
Gmaj7/A = 5 X 4 4 3 3
