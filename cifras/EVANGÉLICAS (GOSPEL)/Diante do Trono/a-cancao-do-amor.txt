Diante do Trono - A Canção do Amor

Intro: E9  B4/D#  C#m7(11)  A6(9)
       E9/G#  B/D#  C7M(#11)  D6(9)

          E9              B/D#
Renova em mim a canção de amor
          C#m7            A6(9)
Renova em mim a canção de amor
           E                B            A6(9)  A9    B/A  A9
Renova em mim a canção de amor, por Ti Jesus
          E9              B/D#
Renova em mim a canção de amor
          C#m7            A6(9)
Renova em mim a canção de amor
          E/G#            B/D#         C7M(#11)   D6(9)
Renova em mim a canção de amor, por Ti Jesus

Intro: E9  B/D#  C#m7(11)  A6(9)
       E9/G#  B/D#  C7M(#11)  D6(9)

C#m7(9)           B/A      A9         ( A6(9)  A9  B )
Leva-me de volta, ao lugar do encontro

C#m7(9)              A7M    G#m7     G#m7(#5)   ( A9  B )
 Aos primeiros dias,        faz-me   de novo
C#m7(9)    B/D#   E   A7M   G#m7               A9     B
Leva-me de vol - ta,         ao lugar do encontro
C#m7(9)   B/D#   E    F#m7  G#m7       G#m7(#5)    A9
Aos primeiros  dias,        faz-me     de novo

       E/G#    A9         B           A9          A6(9)    A9
Leva-me ao  lugar onde a Tua voz era tão real (era tão real)
       E/G#   A/C#        B/D#            E4       E       B
Leva-me ao   lugar onde a Tua palavra queimava em meu coração
B4     B       F#m7        G#m7
Leva-me ao    lugar, de te amar
              A9          B
Leva-me ao lugar, de te amar
             A/C#      B4  B/D#  C#m7  C7M
Leva-me ao lugar, de te amar

          E9              B/D#
Renova em mim a canção de amor
          C#m7            A6(9)
Renova em mim a canção de amor
          E/G#              B            A6(9)  A9    B/A  A9
Renova em mim a canção de amor, por Ti Jesus
          E9              B/D#
Renova em mim a canção de amor
          C#m7            A6(9)
Renova em mim a canção de amor
          E/G#            B/D#         C7M(#11)   D6(9)
Renova em mim a canção de amor, por Ti Jesus

C#m7(9)    B/D#   E    A7M
Leva-me de vol - ta,  (leva-me de volta)
G#m7                      A9           B
 Ao lugar do encontro (lugar do encontro)
C#m7(9)   B/D#   E    F#m7
Aos primeiros  dias,       (aos primeiros dias)
G#m7       G#m7(#5)    A9
Faz-me     de novo

       E/G#    A9            B              A9           A6(9)    A9
Leva-me ao  lugar de simplesmente estar em adoração (em adoração)
       E/G#   A/C#              B/D#                 E4               E     B
Leva-me ao   lugar onde os Teus sonhos pra mim eram tudo o que eu queria viver
B4     B       F#m7        G#m7
Leva-me ao    lugar, de te amar
              A9          B
Leva-me ao lugar, de te amar
             A/C#       B/D#
Leva-me ao lugar, de te amar

Instrumental: C7M(#11)  D6(9)  E9  B
              G  A  C7M(#11)  D6(9)  D6(9)/B  A9

          E              B/D#
Renova em mim a canção de amor
          C#m7            A6(9)
Renova em mim a canção de amor
          E/G#              B       A9  ( A/C#  B/D#  E  A )
Renova em mim a canção de amor,   por Ti    Jesus
              E9           B/D#
Renova em mim a canção de amor
          C#m7            A6(9)
Renova em mim a canção de amor
          E/G#            B/D#        C7M(#11)     A9    C#4
Renova em mim a canção de amor, por Ti Jesus

          F#9             C#/F
Renova em mim a canção de amor
          D#m7            B6(9)
Renova em mim a canção de amor
         F#/A#            C#/F           B6(9)  B9  C#/B  B9
Renova em mim a canção de amor, por Ti Jesus
          F#9            ( C#  D#m7  F#/A#  B  C#/F )
Renova em mim a canção de amor
          D#m7            B6(9)
Renova em mim a canção de amor
         F#/A#            C#/F
Renova em mim a canção de amor, por Ti Jesus

      D7M(#11)  E6(9)          D#m7  C#
Por ti, Jesus...       Por ti, Jesus

( D7M(#11)  E  B  C#  A  B9 )

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A6(9) = 5 4 4 4 X X
A7M = X 0 2 1 2 0
A9 = X 0 2 2 0 0
B = X 2 4 4 4 2
B/A = X 0 4 4 4 X
B/D# = X 6 X 4 7 7
B4 = X 2 4 4 5 2
B4/D# = X 6 4 4 5 7
B6(9) = X 2 1 1 2 2
B9 = X 2 4 4 2 2
C# = X 4 6 6 6 4
C#/B = X 2 3 1 2 X
C#/F = X 8 X 6 9 9
C#4 = X 4 4 1 2 X
C#m7 = X 4 6 4 5 4
C#m7(11) = X 4 4 4 5 4
C#m7(9) = X 4 2 4 4 X
C7M = X 3 2 0 0 X
C7M(#11) = X 3 4 4 5 X
D#m7 = X X 1 3 2 2
D6(9) = X 5 4 2 0 0
D7M(#11) = X X 0 1 2 2
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E4 = 0 2 2 2 0 0
E6(9) = X 7 6 6 7 7
E9 = 0 2 4 1 0 0
E9/G# = 4 X 4 4 5 X
F#/A# = 6 X 4 6 7 X
F#9 = 2 4 6 3 2 2
F#m7 = 2 X 2 2 2 X
G = 3 2 0 0 0 3
G#m7 = 4 X 4 4 4 X
G#m7(#5) = X X 6 9 7 7
