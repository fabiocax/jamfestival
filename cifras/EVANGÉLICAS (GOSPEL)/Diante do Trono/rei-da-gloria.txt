Diante do Trono - Rei da Glória

Am          G           F           E
LALARAILA   LALARAILA   LALARAILA   LALARAILA
Am                  Dm                  Am
ABRAM-SE Ó PORTAIS, ABRAM-SE Ó PORTAS ANTIGAS
Am        Am/G   D7(9)/F# Am/E
PRA QUE O REI DA GLÓRIA   ENTRE (2X)

Am          G           F           E
LALARAILA   LALARAILA   LALARAILA   LALARAILA
Am                      Dm                 Am
QUEM É O REI DA GLÓRIA, O SENHOR FORTE E VALENTE
Am   Am/G  D7(9)/F#  Am/E
O SENHOR VALENTE NAS GUERRAS (2X)

Am          G           F           E
LALARAILA   LALARAILA   LALARAILA   LALARAILA
Am    G  Am  G Am   F          G      Am
ABRAM-SE Ó PORTAIS, ABRAM-SE Ó PORTAS ANTIGAS
Am        Am/G   D7(9)/F# Am/E
PRA QUE O REI DA GLÓRIA ENTRE
Am     Am/G D7(9)/F#   Am/E
QUEM É ESSE REI DA    GLÓRIA

Dm  C/E  F              G    E    /E  [STOP]
O           SENHOR DOS EXÉRCITOS
               Am         G
ELE É O REI DA GLÓRIA, DA GLÓRIA
       F          E
REI DA GLÓRIA, DA GLÓRIA

    Am    G/A  Am   G/A  Am     Am/G G Am/G G  Am/G
QUE ENTRE O    REI  DA   GLÓRIA
    Am/F  G/F  Am/F G/F  Am/F   Esus  E
QUE ENTRE O    REI  DA   GLÓRIA
FIM:  Am  G    Am    G   Am   Am

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am/E = 0 X 2 2 1 0
Am/F = 1 X 2 2 1 X
Am/G = 3 X 2 2 1 X
C/E = 0 3 2 0 1 0
D7(9)/F# = 5 7 5 9 5 6
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
Esus = 0 2 2 1 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/A = 5 X 5 4 3 X
G/F = 1 X X 0 0 3
