Diante do Trono - Santo Como Tu És

Intro:  C  Am7  F  Fm/G#   Dm7   G

C   F/C        C   F/C
Santo   como tu és
              C   F/C        C    C  G/B
Eu quero ser santo   como tu és

           Am7      G/B
Sei que um dia te verei
       F/C        C
Face à face e serei
Dm7  G   1ª    C    F   G
Dm7  G   2ª    C    C  C/E
Santo como tu és

         F          G/F
Por isso eu me santifico
      F         C/E
Cada dia me purifico
              Dm7            C     (2x)
Pois tenho te visto, te conhecido

         Bb   F/A   G
E tu és santo

C   F/C        C   F/C
Santo   como tu és

Eu quero ser santo   como tu és

           Am7      G/B
Sei que um dia te verei
       F/C       C
Face à face e serei
Dm7  G         C    C  C/E
Santo como tu és

         F          G/F
Por isso eu me santifico
      F         C/E
Cada dia me purifico
              Dm7            C      (2x)
Pois tenho te visto, te conhecido
         Bb   F/A   G
E tu és santo

INTER:   C   Bb   F/A   Gm/Bb   Dm7   C/E   F  F   G

         C   F/C              Em7  Am7
Foi para isto   que se manifestou
               F      Dm7
Pra tirar os pecados
            G     G/F
Pois ele é santo
        Em7    Am7            Em7    Am7
Se confessarmos   os nossos pecados
              Dm7    F
Vamos ser perdoados
            G
Pois ele é santo

         F          Em7
Por isso eu me santifico
      F         C
Cada dia me purifico
              Dm7            C     (2x)
Pois tenho te visto, te conhecido
         Bb   F/A  F/G  G
E tu és santo


ESPONTÂNEO E END:   Bb   F/A   G  G   C

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
Dm7 = X 5 7 5 6 5
E = 0 2 2 1 0 0
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
F/C = X 3 3 2 1 1
F/G = 3 X 3 2 1 X
Fm/G# = 4 3 3 1 X X
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G/F = 1 X X 0 0 3
Gm/Bb = 6 5 5 3 X X
