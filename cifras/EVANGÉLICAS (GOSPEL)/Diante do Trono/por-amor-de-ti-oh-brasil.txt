Diante do Trono - Por Amor de Tí, Oh Brasil

Intro:  DM7(9)   GM7(9)   Bm7(11)   GM7(9)
           DM7(9)   GM7(9)   Bm7(11)   F#m7(11)   Esus   E

DM7(9)                                          DM7(9)                                                     GM7(9)
Por amor de ti, ó brasil  não me calarei nem me aquietarei
Bm7                                                                        A/C#
Sobre os teus muros, ó brasil pus guardas
D                                                             Esus                      E          Esus         E
Que todo dia e toda noite jamais se calarão

DM7(9)                                           DM7(9)                                                   GM7(9)
Por amor de ti, ó brasil  não me calarei nem me aquietarei
Bm7                                                                                   A/C#
Vós que fareis lembrado o senhor não descanseis
D                                                                                  E               Esus            E
Nem deis a ele descanso até que nos ponha

E                                        A                                                                      F#m7
Por objeto de louvor na terra chamados por um novo nome
                                         DM7                Bm7                                   Esus                 E
Que a boca do senhor dirá              e a nossa justiça brilhará

   E/G#                 A                                         F#m7
Até que nos faça ser coroa de glória
                              DM7                                                 Bm7(9)        E      D/F#
Nas mãos do nosso deus por amor de ti, ó brasil
E/G#                    D/A        A      A
Não me calarei

Reintro:    DM7(9)    GM7(9)    Bm7(11)     F#m7(11)     Esus    E

(Verso 1)

(Verso 2)

E                                        A                                                                      F#m7
Por objeto de louvor na terra chamados por um novo nome
                                         DM7                Bm7                                   Esus                 E
Que a boca do senhor dirá              e a nossa justiça brilhará
   E/G#                 A                                         F#m7
Até que nos faça ser coroa de glória
                              DM7                                                Bm7(9)        E     Coro:   Bm7(9)   E
                                                                                                Instr.:   D/F#
Nas mãos do nosso deus por amor de ti, ó brasil
E/G#                    D/A        A   [stop - frase piano]
Não me calarei

Instrumental:  DM7(9)    GM7(9)     Bm7(11)     F#m7(11)     E    Esus      E

E                                        A                                                                      F#m7
Por objeto de louvor na terra chamados por um novo nome
                                         DM7                Bm7                                   Esus                 E
Que a boca do senhor dirá              e a nossa justiça brilhará
   E/G#                 A                                         F#m7
Até que nos faça ser coroa de glória
                              DM7                                                Bm7(9)        E     Coro:   Bm7(9)   E
                                                                                                Fim:     D/F#
Nas mãos do nosso deus por amor de ti, ó brasil
E/G#                    D/A        D/A    A
Não me calarei

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
Bm7 = X 2 4 2 3 2
Bm7(11) = 7 X 7 7 5 X
Bm7(9) = X 2 0 2 2 X
D = X X 0 2 3 2
D/A = X 0 X 2 3 2
D/F# = 2 X 0 2 3 2
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
Esus = 0 2 2 1 0 0
F#m7 = 2 X 2 2 2 X
F#m7(11) = 2 X 2 2 0 X
