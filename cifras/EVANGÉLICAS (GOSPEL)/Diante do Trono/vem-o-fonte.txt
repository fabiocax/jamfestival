Diante do Trono - Vem Ó Fonte

Intro: D D9 D D4
       D4 D D9

D                   A                        D
Vem, ó fonte de toda benção o meu canto afinar
D                      A                  D
Rios de misericórdia me convidam pra louvar
D9                                G              D
Ensina-me Tua melodia, línguas de fogo celestiais
D                    a                        D
Louvarei a Minha rocha que não se moverá jamais

D                               A         D
Perdido em densas trevas o Senhor me resgatou
D                             A         D
Prisioneiro do pecado Seu amor me libertou
 D9                                G                D
Minh'Alma canta um novo cântico, meu coração a solar
D                                 A            D
Tua graça me acompanha, sozinho nunca vou estar


G                   D
Vem, ó fonte sem fim!
G                       D
És meu Rei, tudo pra mim

Bm           D        A                  D
Tua noiva canta assim: "És a minha fonte"

D                  A              D
Devedor da Tua graça dia-a-dia eu serei
D                   A                         D
Tua bondade aprisione meu coração a Ti meu Rei

D9                        G                                D
Sinto que sou tão vacilante, pronto a deixar-te, oh meu Deus
D                      A                     D
Toma e sela o meu peito para a morada lá no céu
G                   D
Vem, ó fonte sem fim!
G                      D
És meu Rei, tudo pra mim
BM         D      G          A          D
Tua noiva canta assim: "És a minha fonte"

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
D4 = X X 0 2 3 3
D9 = X X 0 2 3 0
G = 3 2 0 0 0 3
