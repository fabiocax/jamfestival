Diante do Trono - Insaciável

 INTRO: Violão || E    |  C#m7(13) | A2 | F#m7    Bsus   ||     2x
          Orq. || E    |  C#m7(13) | A2 | F#m7    Bsus   ||
              |  C#m7 | B/D#  E   | A2 | E/G#    B      |  A/B    B  ||


E                 E/G#       A          B  C#m7
DESDE QUE TE ENCONTREI NUNCA MAIS FUI O MESMO
   G#m                A2        F#m7     Bsus   B    2:| A/B  B |
EM TUDO MAIS QUE PROCUREI SÓ ACHEI DESILUSÃO
       C#m7             C#m7/B
MAS EM TI DESCOBRI O SENTIDO DA VIDA
   A2                E/G#
PERDÃO E A PAZ QUE SAROU MINHAS FERIDAS
F#      /G# A2           Bsus
NADA NESTE MUNDO ME SATISFAZ
A/B             E     B
TE QUERO MAIS E MAIS

C#m7      G#m7 A                     B   C#m7
SOU INSACIÁVEL NA MINHA BUSCA POR TI
           G#m7  F#m7     A/B
DESESPERADAMENTE TE QUERO MAIS E MAIS

A            E/G#             B
NÃO POSSO PASSAR NEM UM DIA SEQUER
      C#m7  G#m7  A2
SEM VIVER O TEU AMOR
E         G#m7   F#m7      A/B               E
SOU INSACIÁVEL       APAIXONADO POR TI MEU JESUS

E              C#m7
TE AMO MAIS E MAIS
                F#m7
TE QUERO MAIS E MAIS
                A2
TE ADORO MAIS E MAIS
     A/B         E
TE ESPERO MAIS E MAIS

F             Dm7
TE AMO MAIS E MAIS
                Gm7
TE QUERO MAIS E MAIS
                Bb2
TE ADORO MAIS E MAIS
     Bb/C        F
TE ESPERO MAIS E MAIS

F             Dm7
TE AMO MAIS E MAIS
                Gm7
TE QUERO MAIS E MAIS
                Bb2
TE ADORO MAIS E MAIS
     F/A         Csus          C
TE ESPERO MAIS E MAIS


Dm7       Am7  Bb                       C   Dm7
SOU INSACIÁVEL    NA MINHA BUSCA POR TI
           Am7  Gm7        Bb/C
DESESPERADAMENTE  TE QUERO MAIS E MAIS
Bb           F/A              C
NÃO POSSO PASSAR NEM UM DIA SEQUER
      Dm7   Am7  Bb2
SEM VIVER O TEU AMOR
F         Am7  Gm7       Bb/C              F
SOU INSACIÁVEL     APAIXONADO POR TI MEU JESUS

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/B = X 2 2 2 2 X
Am7 = X 0 2 0 1 0
B = X 2 4 4 4 2
B/D# = X 6 X 4 7 7
Bb = X 1 3 3 3 1
Bb/C = X 3 X 3 3 1
Bsus = X 2 4 4 4 2
C = X 3 2 0 1 0
C#m7 = X 4 6 4 5 4
C#m7(13) = X 4 X 4 5 6
C#m7/B = 7 4 6 4 5 4
Csus = X 3 2 0 1 0
Dm7 = X 5 7 5 6 5
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
F#m7 = 2 X 2 2 2 X
F/A = 5 X 3 5 6 X
G#m = 4 6 6 4 4 4
G#m7 = 4 X 4 4 4 X
Gm7 = 3 X 3 3 3 X
