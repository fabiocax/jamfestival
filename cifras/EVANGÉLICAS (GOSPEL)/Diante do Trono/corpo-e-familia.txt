Diante do Trono - Corpo e Família

Intro: A  E   F#m  D  Bm  E

     A             D       A  E/G#     F#m      C#m
Recebi um novo coração do Pai     Coração regenerado
     D           A         Bm              Bm/A         E    D/F#  E/G#
Coração transformado,    Coração que é inspirado por Jesus

      A           D      A    E/G#      F#m             C#m
Como fruto deste novo coração       Eu declaro a paz de Cristo,
       D          A             Bm        Bm/A       E  E7
Te abençôo, meu irmão,      Preciosa é a nossa comunhão

       A                      E       C#7        F#m     D        Bm       E
Somos corpo, e assim bem ajustado  Totalmente ligados, unidos,  vivendo em amor
      A                       E         C#7        F#m         D        Bm          E
Uma família sem qualquer falsidade   Vivendo a verdade, expressando a glória do Senhor

       A                      E        C#7            F#m
Uma família, vivendo o compromisso Do grande amor de Cristo
              D        E      A        F#m           D      E      A
Eu preciso de ti,   querido irmão, precioso és para mim, querido irmão

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
Bm/A = X 0 4 4 3 X
C#7 = X 4 3 4 2 X
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
