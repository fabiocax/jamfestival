Diante do Trono - Quem Nos Separará?

Intro: G9  C6(9)  G9  C6(9)

   G9         Bm7 C6(9)      Dsus
Quem nos separará do amor de Cristo
  G9        Bm7    C6(9)       Dsus
Será tribulação, angústia ou perseguição
  Em7(9) Bm7    C6(9)           Dsus
Fome     ou nudez,  perigo ou espada
   G9                         C6(9)                     C6(9)
Quem nos separará do amor de Cristo

   G9            Bm7           Am7             Dsus
Porque estou bem certo de que nem a morte nem vida
        G9          Bm7          Am7                    Dsus
Nem anjos nem principados, nem coisas do presente,  Nem do porvir
      Em7(9)           Bm7      Am7        Dsus
Nem poderes,      nem alturas, nem profundidades
     Em7(9)            Bm7       Am7        Dsus        Em7(9)   Bm7
Nem qualquer     outra criatura, poderá nos separa do amor de Deus
      Am7         Dsus     Em7(9)   Bm7
Que está em Jesus Cristo (nosso Senhor)

      Am7         Dsus
Que está em Jesus Cristo

G9           Bm7  Am7            Dsus
Nada,        nada Poderá nos separa
G9           Bm7  Am7            F7(9/13)
Nada,        nada Poderá nos separa
     Em7(9)  Bm7        Am7           Dsus
Do amor de Deus,  Que está em Jesus Cristo
  Em7(9)   Bm7
Nosso Senhor
      Am7           Dsus      Dsus
Que está em Jesus Cristo.

( G9  C6(9)  G9  C6(9)  F7(9/13) ) (2x)

   G9            Bm7           Am7             Dsus
Porque estou bem certo de que nem a morte nem vida
        G9          Bm7          Am7                    Dsus
Nem anjos nem principados, nem coisas do presente,  Nem do porvir
      Em7(9)           Bm7      Am7        Dsus
Nem poderes,      nem alturas, nem profundidades
     Em7(9)            Bm7       Am7        Dsus        Em7(9)   Bm7
Nem qualquer     outra criatura, poderá nos separa do amor de Deus
      Am7         Dsus     Em7(9)   Bm7
Que está em Jesus Cristo (nosso Senhor)
      Am7         Dsus
Que está em Jesus Cristo

G9           Bm7  Am7            Dsus
Nada,        nada Poderá nos separa
G9           Bm7  Am7            F7(9/13)
Nada,        nada Poderá nos separa
     Em7(9)  Bm7        Am7           Dsus
Do amor de Deus,  Que está em Jesus Cristo
  Em7(9)   Bm7
Nosso Senhor
      Am7           Dsus      Dsus
Que está em Jesus Cristo.

( G9  C6(9)  G9  C6(9) )

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Bm7 = X 2 4 2 3 2
C6(9) = X 3 2 2 3 3
Dsus = X X 0 2 3 2
Em7(9) = X 7 5 7 7 X
F7(9/13) = 1 X 1 2 3 3
G9 = 3 X 0 2 0 X
