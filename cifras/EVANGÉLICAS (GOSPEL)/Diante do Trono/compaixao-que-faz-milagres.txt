Diante do Trono - Compaixão Que Faz Milagres

Intro:  Am7  G9(7M)/A

C                           Em7
Com os perdidos quem se importará?
C                       Em7
Muitos gritos, quem os ouvirá?
       C       Am7           Em7     Em7/D
Ao chamado de Cristo quem responderá?
      C          Am7      G        D/F#
Entregar     a vida sem olhar pra trás

  Am    Bm          Em
O campo branco já está
          C        Am           D
Para o Trabalho eu vou me preparar
    Am       Bm        Em
Em ações eu vou te honrar
      C            Am                D     Bm   (solo -voltar ao início)
E por amor a Ti Senhor vou me entregar

       C       Bm
Vem gerar em mim

    Em       Em/D
Tua compaixão
   C          Am        D
Capaz de realizar milagres
    C           D
Mostra o Teu amor
    Bm      Em
Através de mim
     Am      C            D
Revelando justiça e verdade
                   C  Em  Am  D  Em   (2x)
Às nações...   ôôô

C               Em
Quem se importará?
              Am
Quem nos ouvirá?
     D        Em
Quem responderá?
                 C
Sem olhar pra trás.      (2x - Quem se importará)

C                  D                 Em
Gera em mim compaixão que faz milagres.
Am                C              D
Gera em mim compaixão que faz milagres!   (2x - Gera em mim)

       C      Bm
Vem gerar em mim
    Em      Em/D
Tua compaixão
  C           Am        D
Capaz de realizar milagres
         C      D
Mostra o Teu amor
    Bm      Em
Através de mim
     Am      C          D
Revelando justiça e verdade   (2x - Vem Gerar em mim)
               C  Em  Am  D  Em
Às nações...   ôôô

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
Em/D = X X 0 4 5 3
Em7 = 0 2 2 0 3 0
Em7/D = X X 0 0 3 0
G = 3 2 0 0 0 3
