Diante do Trono - Tudo Para Mim

INTRO: A2/F# B7 E9 A2/C# A2/F# B7 E9 E9

                 A2/F# A9               EM7/9
Tua beleza me fascina    não posso explicar
                  A2/F# A9                     EM7/9
Tudo o que sinto agora   nem mil palavras poderiam expressar
                    A9                 Bsus9
És mais que uma canção bem mais que emoção
                C#m7      G#m7              F#m7           Bsus B         E9 E9
És mais que uma mera fantasia és meu dia a dia és a minha vida tudo para mim

                    A2/F# A9                      EM7/9
Se eu visse a eternidade     em um instante iria crer
                      A2/F# A9                        EM7/9 E9/G#
Que este hoje vale a pena    pois face a face vou te ver

                 A9                Bsus9                  E9       B/D#   C#m7             F#m7
Contigo quero estar preciso te abraçar anseio estar mais perto de ti meu Rei em meu dia a dia
                 Bsus B              E9
Em toda a minha vida   és tudo para mim


    E    /F#  /G#  A9 Bsus B              C#m7 G#m7              A9 Bsus
És tudo pa - ra   mim       és tudo para mim       és tudo para mim
   B          C#m7 B/C# G#m7              A9     Bsus  B           C#m7
És tudo para mim            és tudo para mim tu és Senhor a minha paz
     G#m7      A9          Bsus    B       C#m7
Meu amor meu refugio minha força tudo par mim
               F#m7        Bsus   B         E9 C#m7
Em meu dia a dia é a minha vida  tudo para mim
               F#m7        Bsus   B         E9 A9 G#m7 F#m7  Bsus B     E9
Em meu dia a dia é a minha vida  tudo para mim              tudo para mim

----------------- Acordes -----------------
A2/C# = X 4 2 2 0 X
A2/F# = 2 X 2 2 0 0
A9 = X 0 2 2 0 0
B = X 2 4 4 4 2
B/C# = X 6 X 4 7 7
B/D# = X 6 X 4 7 7
B7 = X 2 1 2 0 2
Bsus = X 2 4 4 4 2
Bsus9 = X 2 4 4 2 2
C#m7 = X 4 6 4 5 4
E = 0 2 2 1 0 0
E9 = 0 2 4 1 0 0
E9/G# = 4 X 4 4 5 X
F#m7 = 2 X 2 2 2 X
G#m7 = 4 X 4 4 4 X
