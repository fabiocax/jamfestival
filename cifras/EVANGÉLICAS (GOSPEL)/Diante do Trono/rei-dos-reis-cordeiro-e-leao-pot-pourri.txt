Diante do Trono - Rei Dos Reis / Cordeiro e Leão (Pot-Pourri)

Intro: Am  C (4x)

  E | ---------5----------------------------------5-----------|
  A | -------------/12---10---3----7--5--3------------------|
  D | ---------------------------------------------------------|
  G | ---------------------------------------------------------|
  B | ---------------------------------------------------------|
E | ----------------------------------------------------------|

Am                             C                             Am                              C
Rei dos reis, o Senhor dos senhores. Leão da tribo de Judá
                      Am                                    C                                  Am        C
Cordeiro de Deus que venceu. E assentado no trono está

Am                  C                              Am                             C
Santo, Santo é o seu nome. Em santidade governará
                      Am                                          C                            (stop)
Cordeiro de Deus que venceu. Seu domínio se estenderá

Intro: Am  C (2x)


Am                             C                             Am                               C
Rei dos reis, o Senhor dos senhores. Leão da tribo de Judá
                       Am                                   C                                  Am        C
Cordeiro de Deus que venceu. E assentado no trono está

Am                  C                               Am                            C
Santo, Santo é o seu nome. Em santidade governará
                       Am                                         C                            Am
Cordeiro de Deus que venceu. Seu domínio se estenderá

                   F                                               G
Até destruir as obras do diabo. Desfazer toda a opressão
                F                                              G
Até libertar o cativo. Tirar o encarcerado da prisão
                                 F
Seu reino é de justiça
               Dm                           Am
Rei dos reis. Reina sobre nós

                   F                                               G
Até destruir as obras do diabo. Desfazer toda a opressão
                 F                                             G
Até libertar o cativo. Tirar o encarcerado da prisão
                                 F
Seu reino é de justiça
               Dm                            Am
Rei dos reis. Reina sobre nós
               Dm                            Am
Rei dos reis. Reina sobre nós
               Dm                           F G F G
Rei dos reis. Reina sobre nós

Am                                                      F
Digno de adoração. Cordeiro e Leão
Am                                                      F
Digno de adoração. Cordeiro e Leão

Am                        G                           F                         E
Glória e honra e força, o poder, a riqueza, a sabedoria
Am                 G                                           F                                          E
A salvação pertence ao Senhor nosso Deus que se assenta no trono

                          Dm     F                       Am   C
O nosso Deus reina. O nosso Deus reina
                          Dm     F                        Am   C
O nosso Deus reina. O nosso Deus reina
                          Dm     F                        Am   C
O nosso Deus reina. O nosso Deus reina
                          Dm     F                        (Am   C)
O nosso Deus reina. O nosso Deus reina

Final: Am  C  (3x)
 D

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
