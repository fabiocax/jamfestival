Diante do Trono - Coração Igual Ao Teu

[Intro] Bb  F/A  Dm  F/A  Bb  Bb/D  Eb  F

         Bb                                                F
Se tu olhares senhor   pra dentro de mim
             Gm7             C
Nada encontrará de bom
       Bb9                  B9/C
Mas um desejo eu tenho
    F      C/E    Dm
De ser transformado
    Gm                 F/A
Preciso tanto do teu perdão
Bb9                     C
Dá-me um novo coração

 F                             F/A     Bb              Gm   C
Dá-me um coração igual ao teu, meu mes-tre
 F                       Gm      F/A    Bb
Dá-me um coração igual ao teu
         C      C/Bb            Am  Dm
Coração disposto a obedecer

     Am                        Dm
Cumprir todo o teu querer
 Gm      F     Bb        C             Bb   F/A  Dm  F/A  Bb  Bb/D  Eb  F
Dá-me um coração igual ao teu

          Bb                                                F
Se tu olhares senhor   pra dentro de mim
             Gm7             C
Nada encontrará de bom
       Bb9                  C
Mas um desejo eu tenho
    F      C/E    Dm
De ser transformado
    Gm                 F/A
Preciso tanto do teu perdão
Bb9                     C
Dá-me um novo coração

 F                             F/A     Bb              Gm   C
Dá-me um coração igual ao teu, meu mes-tre
 F                     Gm      F/A    Bb
Dá-me um coração igual ao teu
         C       C/Bb            Am  Dm
Coração disposto a obedecer
     Am                        Dm
Cumprir todo o teu querer
 Gm   F   Bb               C        F
Dá-me um coração igual ao teu

 F                             F/A     Bb              Gm   C
Dá-me um coração igual ao teu, meu mes-tre
 F                     Gm      F/A    Bb
Dá-me um coração igual ao teu
         C       C/Bb            Am  Dm
Coração disposto a obedecer
     Am                        Dm
Cumprir todo o teu querer
 Gm   F   Bb               C        F
Dá-me um coração igual ao teu

   C                                     Dm
Ensina-me a amar o meu irmão
     C                                      F                  F/A
A olhar com teus olhos, perdoar com teu perdão
Bb9                   C        C/Bb
Enche-me com teu espírito
Am               Dm                      C   Bb9
Endireita os meus caminhos, Oh Deus, dai-me
        C        F
 Um novo coração

Bb9                  C        C/Bb
Enche-me com teu espírito
Am               Dm                      C   Bb9
Endireita os meus caminhos, Oh Deus, dai-me
       C         D4  D
 Um novo coração

G                              G/B      C             Am   D
Dá-me um coração igual ao teu, meu mes-tre
 G                   F/A    G/B       C
Dá-me um coração igual ao teu
         D      D/C       Bm  Em
Coração disposto a obedecer
     Bm                       Em7
Cumprir todo o teu querer
 Am           G       C       D       G        C/D
Dá-me um coração igual ao teu

G                              G/B      C             Am   D
Dá-me um coração igual ao teu, meu mes-tre
 G                   F/A    G/B       C
Dá-me um coração igual ao teu
         D      D/C       Bm  Em
Coração disposto a obedecer
     Bm                       Em7
Cumprir todo o teu querer
 Am           G      C        D        G   Em7
Dá-me um coração igual ao teu

 Am      G      C        D            G
Dá-me um coração igual ao teu

     D      D/C       Bm  Em
Coração disposto a obedecer
     Bm                       Em7
Cumprir todo o teu querer
 Am            G     C     D          G
Dá-me um coração igual ao teu

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B9/C = X 3 4 4 2 2
Bb = X 1 3 3 3 1
Bb/D = X 5 X 3 6 6
Bb9 = X 1 3 3 1 1
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C/Bb = X 1 2 0 1 X
C/D = X X 0 0 1 0
C/E = 0 3 2 0 1 0
D = X X 0 2 3 2
D/C = X 3 X 2 3 2
D4 = X X 0 2 3 3
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
Gm = 3 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
