Diante do Trono - Águas do Jordão

[Intro]  G#m  E7M  G#m  E7M

  G#m
O Deus que abriu o mar
         E7M
E a pés secos nos fez passar
   G#m
Os deuses do Egito envergonhou
    E7M                  G#m
Seu braço forte nos libertou
    E7M                  F#
Seu braço forte nos libertou

  G#m
O Deus que mandou maná
     E7M
Nada nos deixou faltar
   G#m
De noite e de dia nos consolou
    E7M                G#m
Sua nuvem nunca nos deixou

    E7M                F#
Sua nuvem nunca nos deixou

  E
Agora Ele nos manda atravessar
  C#m
A terra prometida conquistar
 E                   F#
Vamos crer e não duvidar

            E7M          F#
Águas do Jordão Ele partirá
            C#m7           B
Muralhas cairão, Jericó será
                    E7M             F#
Entregue em nossas mãos, vamos confiar
                 G#m  F#
Gigantes vem ao chão

( G#m  E7M )

  E
Agora Ele nos manda atravessar
  C#m
A terra prometida conquistar
 E                   F#
Vamos crer e não duvidar

            E7M          F#
Águas do Jordão Ele partirá
            C#m7           B
Muralhas cairão, Jericó será
                    E7M             F#
Entregue em nossas mãos, vamos confiar
                 G#m  F#
Gigantes vem ao chão

            E7M          F#
Águas do Jordão Ele partirá
            C#m7           B
Muralhas cairão, Jericó será
                    E7M             F#
Entregue em nossas mãos, vamos confiar
                 G#m  F#
Gigantes vem ao chão

( G#m  E )
( G#m  E )

G#m
Reis e principados escutem
 E
Povos e nações, prestem atenção
G#m
Ordens de um novo governo
 E
Leis escritas no Monte de Sião

G#m
Reis e principados escutem
E7M
Povos e nações, prestem atenção
G#m
Ordens de um novo governo
 E                     C#m
Leis escritas no Monte de Sião

            E7M          F#
Águas do Jordão Ele partirá
            C#m7           B
Muralhas cairão, Jericó será
                    E7M             F#
Entregue em nossas mãos, vamos confiar
                 G#m  F#
Gigantes vem ao chão

            E7M          F#
Águas do Jordão Ele partirá
            C#m7           B
Muralhas cairão, Jericó será
                    E7M             F#
Entregue em nossas mãos, vamos confiar
                 G#m  F#
Gigantes vem ao chão

( G#m  E7M )
( G#m  E7M )
( G#m  F#/G#  E )

            G#m  F#/G#  E7M
Águas do Jordão
            G#m  F#/G#  E7M
Muralhas cairão
                    G#m  F#/G#  E7M
Entregue em nossas mãos
                 G#m  F#/G#  E7M  F#
Gigantes vem ao chão

            E7M          F#
Águas do Jordão Ele partirá
            C#m7           B
Muralhas cairão, Jericó será
                    E7M             F#
Entregue em nossas mãos, vamos confiar
                 G#m  F#
Gigantes vem ao chão

            E7M  F#
Águas do Jordão
            C#m7  B
Muralhas cairão
                    E7M             F#
Entregue em nossas mãos, vamos confiar
                 G#m  F#
Gigantes vem ao chão

( G#m  E7M )
( G#m  E7M )

----------------- Acordes -----------------
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
E = 0 2 2 1 0 0
E7M = X X 2 4 4 4
F# = 2 4 4 3 2 2
F#/G# = 4 X 4 3 2 X
G#m = 4 6 6 4 4 4
