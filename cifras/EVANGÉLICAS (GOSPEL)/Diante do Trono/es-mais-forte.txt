Diante do Trono - És Mais Forte

Intro 4x: C/F  C4/G C

 C/F        C/G   C/E
Por amor vieste aqui
     C/F       G        C
Pra morrer na cruz por mim
            F    G     Am
Por minha culpa se entregar
         F       G    C/F
Mas com poder ressuscitar

( C4/G   C/E )
( C/F   C4/G   C/E )

    F      G      C/E
És fiel em meu viver
   C       F      G    C/E
Em meio à paz ou ao sofrer
C     F       G    Am7  Am7/G
Teu amor me libertou
       F       G       C
Vivo estás em mim, Senhor


         C              G
És mais forte, és mais forte
        Am7          F
Que o pecado, que a morte
        C               G
Está escrito: Cristo é vivo!
   F         G   F/G
Tu Jesus, és o Senhor

( C4/G   C/E )
( C/F   C4/G   C/E )

        F        G   C
Sem princípio e sem fim
     F     G         C
Esperança eterna em mim
       F       G      Am7
Pra trazer ao mundo a luz
           F    G      C
Pagaste o preço lá na cruz

         C              G
És mais forte, és mais forte
        Am7          F
Que o pecado, que a morte
        C               G
Está escrito: Cristo é vivo!
   F         G     C/F
Tu Jesus, és o Senhor

( F  F/G   Am7 Am7/G   C/E C/F   G/B ) 2x

              F
Teu nome, oh Deus
        Am7       Am7/G   C      G
É exaltado! É exaltado! É exaltado!  (2x)

              F
Teu nome, oh Deus
       Am7    Am7/G  C/E      G/B
É exaltado! É exaltado! É exaltado!

         C              G
És mais forte, és mais forte
        Am7          F
Que o pecado, que a morte
        C               G
Está escrito: Cristo é vivo!
   F         G       (C)  FIM : (C/F)
Tu Jesus, és o Senhor 2x

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Am7/G = 3 0 2 0 1 0
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
C/F = X 8 10 9 8 8
C/G = 3 3 2 X 1 X
C4/G = 3 3 5 5 6 3
F = 1 3 3 2 1 1
F/G = 3 X 3 2 1 X
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
