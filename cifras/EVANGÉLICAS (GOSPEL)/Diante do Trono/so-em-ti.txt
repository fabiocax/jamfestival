Diante do Trono - Só Em Ti

Só em ti
Ministério de Louvor
Intro: Fm Eb Bb/D Eb Bb/D Cm Fm Eb Bb/D Cm Bb Ab Ab

Eb Fm Fm/Eb Bb/D Eb Bb/D Cm
Porque o teu amor é melhor do que a vi---------da

Os meus lábios te louvarão enquanto eu vi-----ver
Bb/D C/F Fm Fm/Eb Bb/D Eb Eb/G Ab Eb/G
Em teu nome levanta----rei mi-nhas mãos
Fm Bb Eb
A minha alma se fartará só em ti
Eb Bb/D Cm Ab Eb Bb
Só em ti, a minha alma se fartará
Cm Ab Eb/G Bb
Só em ti........só em ti meu Jesus
Cm Ab Eb/G Bb
Só em ti, a minha alma se fartará
Ab Bb Ab/C Bb/D Bb Eb
Só em ti........só em ti meu Jesus
Eb/D Cm Cm/Bb F/A Bb F/A G
Porque o teu amor é melhor do que a vi-----da

Gm/F Cm F G4/7 G
Os meus lábios te louvarão enquanto eu vi-----ver
F/A G/B Cm Cm/Bb F/A Bb Bb/D Eb Bb/D
Em teu nome levanta----rei mi-nhas mãos
Cm F Bb                          Bb F/A Gm Eb Bb F
A minha alma se fartará só em ti Só em ti, a minha alma se fartará
Gm Eb Bb/D F
Só em ti........só em ti meu Jesus
Gm Eb Bb/D F                      Eb F Eb/G F/A F Bb
Só em ti, a minha alma se fartará Só em ti.......só em ti meu Jesus
Com Poder
Am F/A C/G G                      Am F C/E G
Só em ti, a minha alma se fartará Só em ti.......só em ti meu Jesus
Am F C/E G                        F G F/A G/B G C
Só em ti, a minha alma se fartará Só em ti.......só em ti meu Jesus
C G/B Am C G                      Am F C/E G
Só em ti, a minha alma se fartará Só em ti.......só em ti meu Jesus
Am F C/E G                        F G F/A G/B G C
Só em ti, a minha alma se fartará Só em ti.......só em ti meu Jesus

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Ab/C = X 3 X 1 4 4
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bb/D = X 5 X 3 6 6
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
C/F = X 8 10 9 8 8
C/G = 3 3 2 X 1 X
C4/7 = X 3 3 3 1 X
Cm = X 3 5 5 4 3
Cm/Bb = X 1 1 0 1 X
Eb = X 6 5 3 4 3
Eb/D = X 5 5 3 4 3
Eb/G = 3 X 1 3 4 X
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
Fm = 1 3 3 1 1 1
Fm/Eb = X 6 X 5 6 4
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G4/7 = 3 5 3 5 3 X
Gm = 3 5 5 3 3 3
Gm/F = X X 3 3 3 3
