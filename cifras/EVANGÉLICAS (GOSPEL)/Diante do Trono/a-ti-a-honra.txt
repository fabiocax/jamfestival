Diante do Trono - A Ti a Honra

Intro: F9  Fsus4  F9  Fsus4

  F9       Fsus4         Dm         Gm7
A honra, a glória, sejam dadas só a Ti
  Dm        Bb7M         Gm          C
A força, riquezas, sejam dadas só a Ti
      Bb9
Vem lavar meu coração
        Dm9
De qualquer motivação
         C             F
Que não seja o teu louvor
     Gm4              Am
Pois tudo o que tenho e sou
C7/Bb         C
Vem de Ti Senhor!

       F9        Bb
A Ti a honra a a a
        Dm        Gm7
A Ti a glória a a a

          F9          C
Em minha vida e para sempre
       F9       Bb
És exaltado u ô ô
          Dm        Gm
E o teu domínio u ô ô
    F9                    C
É eterno em todas as gerações

Intro: F9  Bb  F9  Bb

   F9       Bb7M         Gm          C
A honra, a glória, sejam dadas só a Ti
      Bb9
Vem lavar meu coração
        Dm9
De qualquer motivação
         C             F
Que não seja o teu louvor
     Gm4              Am
Pois tudo o que tenho e sou
C7/Bb         C
Vem de Ti Senhor!
        F9       Bb
A Ti a honra a a a
        Dm        Gm7
A Ti a glória a a a
         F9           C
Em minha vida e para sempre
       F9       Bb
És exaltado u ô ô
          Dm        Gm
E o teu domínio u ô ô
    F9                   C
É eterno em todas as gerações

Ponte: F/A  Bb  Dm  C

   Dm       Bb9         Gm        C
Entronizado reina sobre tudo, Senhor
Dm          Bb9         Gm            C
Sou pequeno mas existo para o teu louvor
   F/A      Bb9          Gm      C
Entronizado reina sobre tudo Senhor
F/A         Bb9         Gm            C
Sou pequeno mas existo para o teu louvor

       F/A       Bb
A Ti a honra a a a
       Dm        Gm7
A Ti a glória a a a
         F9           C      C/Bb
Em minha vida e para sempre
       F/A      Bb
És exaltado u ô ô
           Dm       Gm
E o teu domínio u ô ô
   F9                     C
É eterno em todas as gerações

Final: F/A  Bb  Dm  C  F

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bb7M = X 1 3 2 3 1
Bb9 = X 1 3 3 1 1
C = X 3 2 0 1 0
C/Bb = X 1 2 0 1 X
C7/Bb = X 1 2 0 1 X
Dm = X X 0 2 3 1
Dm9 = X 5 7 9 6 5
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
F9 = 1 3 5 2 1 1
Fsus4 = 1 3 3 3 1 1
Gm = 3 5 5 3 3 3
Gm4 = 3 X 0 3 1 X
Gm7 = 3 X 3 3 3 X
