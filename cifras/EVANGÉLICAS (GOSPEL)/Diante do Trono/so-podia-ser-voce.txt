Diante do Trono - Só Podia Ser Você

                                               SÓ PODIA SER VOCE

INTRO: Am7 F9 C/E Dm7/9    Dm7/9 F9 C/E Dm7

               C9            F9                          C9                 C9                   F9 G9
Meu Gustavo, meu mel Pedacinho do Céu, Grande sonho se realizou.
          C9           F9                                                Am7 F9
Você é muito mais do que eu era capaz de escolher

G9                      Am7                 F9 Dm7     G9                               Am7
Só podia ser você, entre milhares.     Se outra vez Pudesse viver
            C9         F9                          C9      F9           G9       F9   F9 C9 Dm7
Te queria de novo, de novo e de novo Só podia ser você

               C9            F9                          C9                 C9                   F9 G9
Meu Gustavo, meu mel Pedacinho do Céu, Grande sonho se realizou.
         C9           F9                                                 C9  F9
Você é muito mais do que eu era capaz de escolher

G9                      Am7                 F9 Dm7     G9                               Am7
Só podia ser você, entre milhares.     Se outra vez Pudesse viver

           C9          F9                           C9     F9           G9       F9 Dm7 F9 Dm7
Te queria de novo, de novo e de novo Só podia ser você

                    C9/E F9                                 C9/E F9
Perdão se as vezes   me perco em meus Sonhos
                C9/E          F9                  Dm7                        C9         Bb9     F9
Se a realidade é melhor Você já está aqui, é bom te curtir. Te fazer Feliz

G9                      Am7                 F9 Dm7     G9                               Am7
Só podia ser você, entre milhares.     Se outra vez Pudesse viver
           C9          F9                           C9     F9           G9       F9 C/E Dm7 G9 F9 C/E Dm7 F9
Te queria de novo, de novo e de novo Só podia ser você

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Bb9 = X 1 3 3 1 1
C/E = 0 3 2 0 1 0
C9 = X 3 5 5 3 3
C9/E = X X 2 5 3 0
Dm7 = X 5 7 5 6 5
Dm7/9 = X 5 3 5 5 X
F9 = 1 3 5 2 1 1
G9 = 3 X 0 2 0 X
