Diante do Trono - Usa-me

INTRO: : D/F# E/G# A : A : Bm7 A/C# D : D :
       : D   E  F#m7 : C#m7 : D  D E :  F#m7 E  :

      A           E/A A         A/C#
AS NAÇÕES ESPERAM POR TI SENHOR
      D                    A/C#  E
PELOS PÉS DOS QUE ANUNCIAM BOAS NOVAS
      F#m7              C#m7 D  A/C#
PELOS OLHOS QUE SE COMPADECERÃO
      Bm7              Bm7/A E/G#
PELAS MÃOS QUE FERIDAS TOCARÃO
D/E          A        Estrofe:  C#m7 : D    Bm7 :
QUERO IR POR TI       Coro:     E/G# : F#m7 E D/E :

A     A/C#      D    A/C#
DÁ-ME O TEU CORAÇÃO
Bsus  B/D#        E    D/F#  E/G#
QUERO VER COMO TU VÊS
  A       C#m7       D        F#m7
E AMAR OS POVOS E AS VIDAS PERDIDAS
Bm7     A/C# D                 Esus  E
USA-ME  SIM  USA-ME COMO INSTRUMENTO

   : Bm7 A/C# D : D F#m7 E :
USA-ME

[ESTROFE]

A     A/C#      D    A/C#
DÁ-ME O TEU CORAÇÃO
Bsus  B/D#        E    D/F# E/G#
QUERO VER COMO TU VÊS
  A       C#m7       D        F#m7
E AMAR OS POVOS E AS VIDAS PERDIDAS
Bm7    A/C# D                 Esus   Coro:   D/F# E/G# :
USA-ME SIM  USA-ME COMO INSTRUMENTO  Ponte:  : Bm7 A/C# D Esus :
              D/F#  E/G#  D/A             Bm7  A/C#   D
MOSTRA-ME TUA GLÓ   -     RIA QUE ENCHE A TER    -   RA
    D  E   F#m7              Esus  E   E  E/D C#m7
TUA GLÓ -  RIA      ENTRE OS POVOS
F#sus   F# E/G# F#/A#  E/B    C#m7 B/D# E
TEUS TESOU   -      ROS
    E   F#  G#m7     G#m7 F#/A#  D#m7
E RIQUE -  ZAS
    E  B/D#    C#m7  C#m7 C#m7 C#m7/B
TUA GLÓ -  RIA
         F#sus  F#  :  F#/D#  E/G#  F#/A#  :
ENTRE OS POVOS

B     B/D#      E    B/D#
DÁ-ME O TEU CORAÇÃO
C#sus  C#/F       F#    E/G#   F#/A#
QUERO VER COMO TU VÊS
  B       D#m7       E        G#m7
E AMAR OS POVOS E AS VIDAS PERDIDAS
C#m7    B/D# E                 F#sus  Coro:   E/G#  F#/A# :
USA-ME  SIM  USA-ME COMO INSTRUMENTO  Fim:   : F# :
TAG:  : C#m7 B/D# E : E :
USA-ME
FIM:  : C#m7 B/D# E F#  : B ::

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
B = X 2 4 4 4 2
B/D# = X 6 X 4 7 7
Bm7 = X 2 4 2 3 2
Bm7/A = 5 2 4 2 3 2
Bsus = X 2 4 4 4 2
C#/F = X 8 X 6 9 9
C#m7 = X 4 6 4 5 4
C#m7/B = 7 4 6 4 5 4
C#sus = X 4 6 6 6 4
D = X X 0 2 3 2
D#m7 = X X 1 3 2 2
D/A = X 0 X 2 3 2
D/E = X X 2 2 3 2
D/F# = 2 X 0 2 3 2
E = 0 2 2 1 0 0
E/A = X 0 2 1 0 0
E/B = X 2 2 1 0 0
E/D = X 5 X 4 5 4
E/G# = 4 X 2 4 5 X
Esus = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
F#/A# = 6 X 4 6 7 X
F#/D# = X 6 X 6 7 6
F#m7 = 2 X 2 2 2 X
F#sus = 2 4 4 3 2 2
G#m7 = 4 X 4 4 4 X
