Diante do Trono - Nos Braços do Pai

Intro:  Bb  F/A  Gm7(11)   F/A  Bb  F/A  Gm7(11)

Primeira Parte:
 Bb     F/A   Gm7(11)  F/A
Pai, Estou aqui
Bb    F/A  Gm7(11)
Olha para mim
   F9   Eb9      Bb/D    F4   F
Desesperado por mais de Ti

Pré-Refrão:
   Bb/D   Eb               Bb9
A tua presença é o meu sustento
   Bb/D    Eb9        F    Gm7
A Tua   palavra meu ali - mento
           Cm7
Preciso ouvir
   Bb9/D Eb            Eb9/G   F9/A
A tua   Voz dizendo assim:

Refrão:

 Bb9        Eb/Bb Bb9
Vem filho ama -  do
 F9     Bb/F  F Eb9       F/Eb   Eb  F
Vem em meus braços descansar
Bb9      Eb/Bb Bb    F             Eb9
E bem segu -  ro te conduzirei ao Meu altar
  Bb9/D    Cm7     Gm7   F9/A
Ali   falarei  contigo
          Gm7      F9   Eb9
Com meu amor te envolverei
    Bb9/D        Cm7        Bb9/D      F9
Quero  o   -   lhar em teus olhos
        Gm7   F/A          Eb9
Tuas feridas sara    -    Rei

Pós-Refrão 2x:
 Bb9        Eb/Bb  Bb9
Vem filho ama  -  do
 F          Eb9       F/Eb        Eb
Vem como estás

( F/Eb  Eb  F )

Primeira Parte:
 Bb     F/A   Gm7(11)  F/A
Pai, Estou aqui
Bb    F/A  Gm7(11)
Olha para mim
   F9   Eb9      Bb/D    F4   F
Desesperado por mais de Ti

Pré-Refrão:
   Bb/D   Eb               Bb9
A tua presença é o meu sustento
   Bb/D    Eb9        F    Gm7
A Tua   palavra meu ali - mento
           Cm7
Preciso ouvir
   Bb9/D Eb            Eb9/G   F9/A
A tua   Voz dizendo assim:

Refrão 2x:
 Bb9        Eb/Bb Bb9
Vem filho ama -  do
 F9     Bb/F  F Eb9       F/Eb   Eb  F
Vem em meus braços descansar
Bb9      Eb/Bb Bb    F             Eb9
E bem segu -  ro te conduzirei ao Meu altar
  Bb9/D    Cm7     Gm7   F9/A
Ali   falarei  contigo
          Gm7      F9   Eb9
Com meu amor te envolverei
    Bb9/D        Cm7        Bb9/D      F9
Quero  o   -   lhar em teus olhos
        Gm7   F/A          Eb9    F9/A
Tuas feridas sara    -    Rei

Pós-Refrão 4x:
 Bb9        Eb/Bb  Bb9
Vem filho ama  -  do
 F          Eb9       F/Eb        Eb
Vem como estás

 Bb9   F9/A       Gm7(11)   F/A
Pai,        meu  Pai
Bb9/D     Eb9   Eb/F   F            Bb9    (Eb9/Bb   F/Bb)
 Meu   Papai,          A  -  ba    Pai 

Instrumental: C#/B  B  C#/B   B  B/C#  C#  B/C#   C#
              Dm7  C/E  F/Bb  F  Dm7  C/E  F  G

 C9   G9/B        Am7(11)   G/B
Pai,        meu  Pai
C9/E      F9    F/G  G      F/G  C9    (F9/C   G/C)
 Meu   Papai,        A  -  ba   Pai

 C9   G9/B        Am7(11)   G/B
Pai,        meu  Pai
C9/E    F9   F/G
 Meu Papai
       F9/G   C9   C
A|-  ba Pai

Solo:

E|---------------------------------------------------------------|
B|---------------------------------------------------------------|
G|---12-14-15~~~~---14~~~~~~--12~~~~~~--(12)\--------------------|
D|---------------------------------------------------------------|
A|---------------------------------------------------------------|
E|---------------------------------------------------------------|

     Bb       Eb/Bb        Bb                         Eb/Bb
E|---------|-------------|-----------------------------|------------|
B|---------|-------------|---------------11-10---------|------------|
G|-5~~-5-7-|8~~~~-8/12~~~|~p(10)~~~~--10-------10----10|/12-(12)p10-|
D|---------|-------------|------------------------12---|------------|
A|---------|-------------|-----------------------------|------------|
E|---------|-------------|-----------------------------|------------|

                   Dm                                     Em
E|-------------|--------------------------------------|----------------|
B|-------------|--------------------------11p10p8-----|----10h11-13\---|
G|------------9|-10~~--9h10-7~~~----9h10---------10-10|/12-------------|
D|-----9-10-12-|--------------------------------------|----------------|
A|--12---------|--------------------------------------|----------------|
E|-------------|--------------------------------------|----------------|

     Am7        Bb Am7  Dm7 Em7 F                                G
E|-----------------|----------------------------------------------|
B|-11~~11-10-------|------------------12-13-15----------12-13-15--|
G|-----------10----|-7----9--10-10-12-14-------10-12-14-----------|
D|-----------------|----------------------------------------------|
A|-----------------|----------------------------------------------|
E|-----------------|----------------------------------------------|

E|-12-13-15------------------------------------------------------|
B|---------------------------------------------------------------|
G|---------------------------------------------------------------|
D|---------------------------------------------------------------|
A|---------------------------------------------------------------|
E|---------------------------------------------------------------|

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Am7(11) = 5 X 5 5 3 X
B = X 2 4 4 4 2
B/C# = X 6 X 4 7 7
Bb = X 1 3 3 3 1
Bb/D = X 5 X 3 6 6
Bb/F = X 8 8 7 6 X
Bb9 = X 1 3 3 1 1
Bb9/D = X 5 3 3 1 1
C = X 3 2 0 1 0
C# = X 4 6 6 6 4
C#/B = X 2 3 1 2 X
C/E = 0 3 2 0 1 0
C9 = X 3 5 5 3 3
C9/E = X X 2 5 3 0
Cm7 = X 3 5 3 4 3
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Eb = X 6 5 3 4 3
Eb/Bb = X X 8 8 8 6
Eb/F = X 8 8 8 8 X
Eb9 = X 6 8 8 6 6
Eb9/Bb = X 1 X 3 4 1
Eb9/G = 3 X 1 3 4 1
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
F/Bb = X 1 3 2 1 1
F/Eb = X X 1 2 1 1
F/G = 3 X 3 2 1 X
F4 = 1 3 3 3 1 1
F9 = 1 3 5 2 1 1
F9/A = 5 X X 5 6 3
F9/C = X 3 X 5 6 3
F9/G = X X 5 5 6 3
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G/C = X 3 5 4 3 3
G9/B = 7 X X 7 8 5
Gm7 = 3 X 3 3 3 X
Gm7(11) = 3 X 3 3 1 X
