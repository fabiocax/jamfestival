Diante do Trono - Não Temas

Intro: Voz

Não te - mas, não te - mas
Não te - mas... não! Não te - mas.
D
na 1ª parte somente as vozes e percussão

Agindo eu, quem impedirá? (4X)

Refrão:
        D   Em7       D/F#  G
Agindo eu, quem impedirá?
       Bm7 Em7      A        D     G
Agindo eu, quem impedirá  -  á -   á?
        D   Em7      D/F#   G
Agindo eu, quem impedirá?
       Bm7  Em7     A     D
Agindo eu, quem impedirá?

         Em7
Quando andares pelas águas

F#m7
Não te afogarás
     Em7
Quando pelo fogo
Bm7
Não te queimarás
 Bm7        A  Bm7
(não te queimaras não!)
      G          D/F#
Não temas, sou contigo
       Em7 G/A           A      D     G
Agindo eu, quem impedi - rá  -  á  -  á?

Refrão:
        D   Em7       D/F#  G
Agindo eu, quem impedirá?
       Bm7 Em7      A        D     G
Agindo eu, quem impedirá  -  á -   á?
        D   Em7      D/F#   G
Agindo eu, quem impedirá?
       Bm7  Em7     A     D
Agindo eu, quem impedirá?

2ª Estrofe:

Refrão 2x:
        D   Em7       D/F#  G
Agindo eu, quem impedirá?
       Bm7 Em7      A        D     G
Agindo eu, quem impedirá  -  á -   á?
        D   Em7      D/F#   G
Agindo eu, quem impedirá?
       Bm7  Em7     A     D
Agindo eu, quem impedirá?

D
Não te - mas, não te - mas
Não te - mas... não! Não te - mas.

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm7 = X 2 4 2 3 2
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em7 = 0 2 2 0 3 0
F#m7 = 2 X 2 2 2 X
G = 3 2 0 0 0 3
G/A = 5 X 5 4 3 X
