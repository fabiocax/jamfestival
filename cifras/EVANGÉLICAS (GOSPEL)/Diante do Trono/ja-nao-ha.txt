Diante do Trono - Já Não Há

D          D/F#          G
JÁ   NÃO HÁ CONDENAÇÃO

                          Bm7                Asus      A    G/A
PARA AQUELES QUE ESTÃO EM CRISTO JESUS

D          A/C#           Bm7
JÁ   NÃO HÁ CONDENAÇÃO

        Bm7/A             G         G/A      D              G/A
PARA AQUELES QUE ESTÃO EM CRISTO JESUS

    Em7
UM POVO REDIMIDO

    F#m7                       G7M               A
COMPRADO PELO PRECIOSO SANGUE DO CORDEIRO

         Em7
SEUS PECADOS PERDOOU


        F#m7             Bm7             G7M
NOVAS VESTES PREPAROU PARA VIVEREM

Em7             G/A     Ab/Bb
CHEIOS DO PODER

Eb        Eb/G          Ab
JÁ NÃO HÁ CONDENAÇÃO

                          Cm7              Bbsus      Bb    Ab/Bb
PARA AQUELES QUE ESTÃO EM CRISTO JESUS

Eb          Bb/D           Cm7
JÁ   NÃO HÁ CONDENAÇÃO

        Cm7/Bb             Ab       Ab/Bb        Eb
PARA AQUELES QUE ESTÃO EM CRISTO JESUS

        Fm7
EU TAMBÉM FUI REDIMIDO

    Gm7                        Ab7M               Bb
COMPRADO PELO PRECIOSO SANGUE DO CORDEIRO

          Fm7
MEUS PECADOS PERDOOU

        Gm7              Cm7             Ab7M
NOVAS VESTES PREPAROU E EU VIVEREI

Fm7           Ab/Bb     A/B
CHEIO DO PODER


E        E/G#            A2
JÁ NÃO HÁ CONDENAÇÃO

                          C#m7               Bsus      B    A/B
PARA AQUELES QUE ESTÃO EM CRISTO JESUS

E         B/D#          C#m7
JÁ NÃO HÁ CONDENAÇÃO

        C#m7/B             A2
PARA AQUELES QUE ESTÃO

    A/B         E      E/G#      F#m
EM CRISTO JESUS

    A/B         E      E/G#      F#m        A/B
EM CRISTO JESUS

   C#m7    C#m7/B     A9     A/B
JESUS

      C#m7     C#m7/B     A9     A/B
SALVADOR

       C#m7      C#m7/B    (F#m   G#m   G7M   A9   A/B) 2X      E
REDENTOR

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/B = X 2 2 2 2 X
A/C# = X 4 X 2 5 5
A2 = X 0 2 2 0 0
A9 = X 0 2 2 0 0
Ab = 4 3 1 1 1 4
Ab/Bb = X 1 1 1 1 X
Ab7M = 4 X 5 5 4 X
Asus = X 0 2 2 2 0
B = X 2 4 4 4 2
B/D# = X 6 X 4 7 7
Bb = X 1 3 3 3 1
Bb/D = X 5 X 3 6 6
Bbsus = X 1 3 3 3 1
Bm7 = X 2 4 2 3 2
Bm7/A = 5 2 4 2 3 2
Bsus = X 2 4 4 4 2
C#m7 = X 4 6 4 5 4
C#m7/B = 7 4 6 4 5 4
Cm7 = X 3 5 3 4 3
Cm7/Bb = 6 3 5 3 4 3
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
Eb = X 6 5 3 4 3
Eb/G = 3 X 1 3 4 X
Em7 = 0 2 2 0 3 0
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
Fm7 = 1 X 1 1 1 X
G = 3 2 0 0 0 3
G#m = 4 6 6 4 4 4
G/A = 5 X 5 4 3 X
G7M = 3 X 4 4 3 X
Gm7 = 3 X 3 3 3 X
