Diante do Trono - Deserto de Revelação

[Intro]  G  D  Em  C  G  D  Em  C

G                    D
O sol tão quente, a dor
Em                    D
Medo, silêncio, incertezas
   C7M       Em/C       D
Serpentes mortais me ameaçam
  Am       Bm           C7M
O vale das sombras traz dúvidas

 G                    D
Luto pra não me esquecer
Em               D
Confiar e acreditar
C7M   Em/C     D
Tua palavra jamais passará
Am     Bm       C7M
Esse deserto florescerá

G
Eu não vou morrer nesse deserto

C7M        Em          D
Vou atravessar e chegarei
   Am     C7M      D
Na terra dos Teus sonhos
   C7M     Em      G        D
Promessas que não podem falhar

G
Eu não vou morrer nesse deserto
C7M        Em          D
Vou atravessar e chegarei
   Am     C7M      D
Na terra dos Teus sonhos
   C7M     Em      G        D
Promessas que não podem falhar

( G  D  Em  C )
( G  D  Em  C )

 G                    D
Luto pra não me esquecer
Em               D
Confiar e acreditar
C7M   Em/C     D
Tua palavra jamais passará
Am     Bm       C7M
Esse deserto florescerá

G
Eu não vou morrer nesse deserto
C7M        Em          D
Vou atravessar e chegarei
   Am     C7M      D
Na terra dos Teus sonhos
   C7M     Em      G        D
Promessas que não podem falhar

( G  D  Em  C )
( G  D  Em  C )
( C7M  G  D  Em  C7M  Em  D )

  C7M       G     D  Em
Deserto de revelação
C7M         G
Mostra-me quem Tu és
   D               Em
Lugar de encontro e manifestação
  C7M      Em     D
Deserto de revelação
C7M            G
Água da rocha e maná
   D          Em
Lugar de viver pela fé e adorar
  C7M      Em     D
Deserto de revelação
  C7M          G
Coluna de fogo, Tua nuvem
   D              Em
Lugar de esperar e mover em Tua direção
  C7M      Em     D
Deserto de revelação

( C7M  D  Em  C7M  D  G )

G
Eu não vou morrer nesse deserto
C7M        Em          D
Vou atravessar e chegarei
   Am     C7M      D
Na terra dos Teus sonhos
   C7M     Em      G        D
Promessas que não podem falhar

( G  D  Em  C )
( G  D  Em  C )

G
Eu não vou morrer nesse deserto
C7M        Em          D
Vou atravessar e chegarei
   Am     C7M      D
Na terra dos Teus sonhos
   C7M     Em      G        D  Cadd2
Promessas que não podem falhar

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C7M = X 3 2 0 0 X
Cadd2 = X 3 5 5 3 3
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
Em/C = 8 7 5 X 8 X
G = 3 2 0 0 0 3
