﻿Diante do Trono - Salmo 91

Capo Casa 1

Intro: A  Bm  D9  A

    A                    Bm
Senhor, Tu és minha esperança
     D9                   A      E/G#
És o meu refúgio, minha segurança
          D9
Quando ao meu redor
      F#m
Há inimigos sem fim
           D9
Posso estender as mãos
               E
E segurar em Ti

             D9
Nas Tuas promessas
          E
Na Tua Palavra
         D9
No Teu amor por mim

                     E
Que nunca me deixa só

                 Bm
À sombra de Tuas asas
                 D9
Eu posso descansar
                F#m
E sob as Tuas penas
                       E
Mal algum me alcançará
                  Bm
Tu és minha morada
           D9
Ó Altíssimo
               F#m
Livra-me, Senhor
        E          E/G#      A
Pois a Ti me apeguei com amor

Re-Intro: A  Bm  D9  A

    A                    Bm
Senhor, Tu és minha esperança
     D9                   A      E/G#
És o meu refúgio, minha segurança
          D9
Quando ao meu redor
      F#m
Há inimigos sem fim
           D9
Posso estender as mãos
               E
E segurar em Ti

             D9
Nas Tuas promessas
          E
Na Tua Palavra
         D9
No Teu amor por mim
                     E
Que nunca me deixa só

                 Bm
À sombra de Tuas asas
             A  D9
Eu posso descansar
                F#m
E sob as Tuas penas
                       E
Mal algum me alcançará
                  Bm
Tu és minha morada
           D9
Ó Altíssimo
               F#m
Livra-me, Senhor
        E          E/G#      A
Pois a Ti me apeguei com amor

        F#m          E         D9   Bm
Tua verdade é meu escudo e baluarte
          F#m
Eu não temerei
             E         D9     Bm
Pois Teus Anjos me sustentarão
      F#m     E          D9   Bm
Pisarei o leão e a áspide
       F#m        E           D D4 D D9
Tu me mostrarás a Tua salvação

( D  F#m  A  E  D  F#m  A  E/G# )

                 Bm
À sombra de Tuas asas
             A  D9
Eu posso descansar
                F#m
E sob as Tuas penas
                       E
Mal algum me alcançará
                  Bm
Tu és minha morada
           D9
Ó Altíssimo
               F#m
Livra-me, Senhor
        E          E/G#      A
Pois a Ti me apeguei com amor

----------------- Acordes -----------------
Capotraste na 1ª casa
A*  = X 0 2 2 2 0 - (*A# na forma de A)
Bm*  = X 2 4 4 3 2 - (*Cm na forma de Bm)
D*  = X X 0 2 3 2 - (*D# na forma de D)
D4*  = X X 0 2 3 3 - (*D#4 na forma de D4)
D9*  = X X 0 2 3 0 - (*D#9 na forma de D9)
E*  = 0 2 2 1 0 0 - (*F na forma de E)
E/G#*  = 4 X 2 4 5 X - (*F/A na forma de E/G#)
F#m*  = 2 4 4 2 2 2 - (*Gm na forma de F#m)
