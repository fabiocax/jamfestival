Diante do Trono - O Encontro Das Águas

INTRO.: Em D/F# D C   Em  D/F# C
G
Disse Jesus:
         C                               G           C/E
"Do nosso interior fluirão rios”
          D               Am                    G            (D/F#   Bm)
São fontes que jorram para vida eterna
                   Em
No deserto deste mundo
                      D
Na terra seca do meu coração
                     D/F#           G          Em
Águas vivas brooooootarão
                    Am                     C
É o Espírito Santo dentro de mim
                            G/B   C    Em  D
E de você, meu irmão
                           Bm
Quando as águas se encontram
 C9
Fontes a jorrar

                        Bm
Pouco a pouco se misturam
       Em         D
Unidade a gerar
      G/B           C9
Um rio poderoso
Bm/D                       Em
Que ninguém pode parar
           C               D/F#                 Em      (D/F#   Em)
No encontro das águas Deus está
           C                D/F#        ( C/F    C/E     D/F#)
No encontro das águas Deus está
 G
Rio de vida
Em
Rio de Deus
C            Am    Em D/F# D C
Rio de glória e poder
G
Rio de vida
Em
Rio de Deus
C          Am   Em D/F# D C
Rio de cura e amor
      F                     D                               Em D/F# D C  Em D/F# C  (2X)
No encontro das águas se encontra o senhor
                           Bm
Quando as águas se encontram
 C9
Fontes a jorrar
                        Bm
Pouco a pouco se misturam
       Em         D
Unidade a gerar
      G/B           C9
Um rio poderoso
Bm/D                       Em
Que ninguém pode parar
           C               D/F#               Em      (D/F#  Em)
No encontro das águas Deus está
           C                D/F#        ( C/F   C/E   D/F#)
No encontro das águas Deus está
 G
Rio de vida
Em
Rio de Deus
C            Am    Em D/F# D C
Rio de glória e poder
 G
Rio de vida
Em
Rio de Deus
C          Am   Em D/F# D C
Rio de cura e amor
      F                     D                     Em D/F# D C  Em D/F# C (2x)
No encontro das águas se encontra o senhor

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
Bm/D = X 5 X 4 7 7
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
C/F = X 8 10 9 8 8
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
