Laura Souguellis - Eu Não Estou Só

           F/C                  G/A      F/C        G
Existe um homem que me cativou como ninguém jamais fez
        F/C            G/A       F/C         G
Aquele lindo olhar levou embora tudo que não era meu
        F/C             G/A          F/C    G
E esse homem me fez ansiar tudo que vem do céu
         F/C             G/A           F/C             G
Nele eu posso esperar e descansar pra sempre em seus braços

          F     C                     G   Am
Eu encontrei aquele que primeiro me amou
          F        C                  G           Am
Me apaixonei pela luz que brilhou em minha escuridão

( F/C  G/A  F/C  G )
( F/C  G/A  F/C  G )

           F/C                  G/A      F/C        G
Existe um homem que me cativou como ninguém jamais fez
        F/C            G/A       F/C         G
Aquele lindo olhar levou embora tudo que não era meu

        F/C             G/A          F/C    G
E esse homem me fez ansiar tudo que vem do céu
         F/C             G/A           F/C             G
Nele eu posso esperar e descansar pra sempre em seus braços

          F     C                     G   Am
Eu encontrei aquele que primeiro me amou
          F        C                  G           Am
Me apaixonei pela luz que brilhou em minha escuridão

              F/C   G/A
Seu nome é Jesus,Jesus
F                C
Eu não estou só, eu não estou só
            G    Am
Ele está comigo
              F/C   G/A
Seu nome é Jesus,Jesus
F                C
Eu não estou só, eu não estou só
            G    Am
Ele está comigo

[Solo]

F                C
Eu não estou só, eu não estou só
            G    Am
Ele está comigo
F                C
Eu não estou só, eu não estou só
            G    Am
Ele está comigo

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
F/C = X 3 3 2 1 1
G = 3 2 0 0 0 3
G/A = 5 X 5 4 3 X
