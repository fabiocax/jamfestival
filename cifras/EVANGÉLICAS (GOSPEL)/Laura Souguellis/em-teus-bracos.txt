Laura Souguellis - Em Teus Braços

[Intro]

Parte 1

   F7M
E|---------0--------------------------------|
B|-----------------3-------1----------------|
G|-----2-------2-------2-------2------------|
D|---3---3---3---3---3---3---3---3----------|
A|------------------------------------------|
E|-1----------------------------------------|

Parte 2

   F7M
E|---------0--------------------------------|
B|-----------------3-------1----------------|
G|-----2-------2-------2-------2------------|
D|---3---3---3---3---3---3---3---3----------|
A|------------------------------------------|
E|-1----------------------------------------|


Parte 3

   C7M
E|------------------------------------------|
B|---------0--------------------------------|
G|-----0-----------0------------------------|
D|---2---2-----2-------2---2----------------|
A|-3---------3---3---3---3------------------|
E|------------------------------------------|

Parte 4

   C7M
E|-----------------------------0------------|
B|---------0-------0---1---3----------------|
G|-----0------------------------------------|
D|---2---2-----2----------------------------|
A|-3---------3---3---3---3---3--------------|
E|------------------------------------------|

[Primeira Parte]

( F7M  C7M )

F7M
    Segura estou nos braços
                         C7M
Daquele que nunca me deixou
F7M
    Seu amor perfeito sempre esteve
              C7M
Repousado em mim

[Pré-Refrão]

         G/B  Am
E se eu pas__sar pelo vale
            F7M           G
Acharei conforto em Teu amor
                Em7
Pois eu sei que és aquele
                   F7M    G
Que me guarda, me guardas

[Refrão]

               F7M
Em teus braços é meu descanso
               C7M
Em teus braços é meu descanso
               F7M
Em teus braços é meu descanso
               C7M
Em teus braços é meu descanso

[Primeira Parte]

F7M
    Segura estou nos braços
                         C7M
Daquele que nunca me deixou
F7M
    Seu amor perfeito sempre esteve
              C7M
Repousado em mim

[Pré-Refrão]

         G/B  Am
E se eu pas__sar pelo vale
            F7M           G
Acharei conforto em Teu amor
                Em7
Pois eu sei que és aquele
                   F7M    G
Que me guarda, me guardas

[Refrão]

               F7M
Em teus braços é meu descanso
               C7M
Em teus braços é meu descanso
               F7M
Em teus braços é meu descanso
               C7M
Em teus braços é meu descanso

               F7M
Em teus braços é meu descanso
               C7M
Em teus braços é meu descanso
               F7M
Em teus braços é meu descanso
               C7M
Em teus braços é meu descanso

[Segunda Parte]

F7M
    Recaio em Tua graça
C7M
    Recaio de novo em Tuas mãos
F7M
    Recaio em Tua graça
C7M
    Recaio de novo em Tuas mãos

F7M
    Recaio em Tua graça
C7M
    Recaio de novo em Tuas mãos
F7M
    Recaio em Tua graça
C7M
    Recaio de novo em Tuas mãos

[Pré-Refrão]

         G/B  Am
E se eu pas__sar pelo vale
            F7M           G
Acharei conforto em Teu amor
                Em7
Pois eu sei que és aquele
                   F7M    G
Que me guarda, me guardas

[Refrão]

               F7M
Em teus braços é meu descanso
               C7M
Em teus braços é meu descanso
               F7M
Em teus braços é meu descanso
               C7M
Em teus braços é meu descanso

               F7M
Em teus braços é meu descanso
               C7M
Em teus braços é meu descanso
               F7M
Em teus braços é meu descanso
               C7M
Em teus braços é meu descanso

[Final]

F7M
    O Toque mais puro

O amor mais seguro do mundo
C7M
    O Toque mais puro

O amor mais seguro do mundo
     F7M
É o Teu

O Toque mais puro

O amor mais seguro do mundo
C7M
    O Toque mais puro

O amor mais seguro do mundo
              F7M
É o Teu Jesus

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C7M = X 3 2 0 0 0
Em7 = 0 2 2 0 3 3
F7M = 1 X X 2 1 0
G = 3 2 0 0 3 3
G/B = X 2 0 0 0 X
