Corinhos Evangélicos - Nunca Esquecer

A                E
Oh, oh, oh, oh, nunca esquecerei
E               A
Oh, oh, oh, oh, nunca esquecerei
A               D
Oh, oh, oh, oh, nunca esquecerei
D                              A   E   A
Eu nunca esquecerei o que Deus fez por mim

A
Livrou-me da morte, do mundo, do mal
E
Eu nunca esquecerei
E
Livrou-me da morte, do mundo, do mal
A
Eu nunca esquecerei
A
Livrou-me da morte, do mundo, do mal
D
Eu nunca esquecerei

D                              A   E   A
Eu nunca esquecerei o que Deus fez por mim

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
