Corinhos Evangélicos - Que a Graça do Senhor Jesus

     D      D7      G            D       Bm     Em  A7
Que a graça do Senhor Jesus e o amor de Deus o pai
     D    D7   G           D    Bm         Em  A7
E a comunhão, comunhão do Espirito reine aqui
   D   D7      G            D    A7      D
E para sempre, para sempre, para sempre amém

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
