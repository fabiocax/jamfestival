Corinhos Evangélicos - Soberania

D9                  C/D      G/D          A/D      D     D/F#
Rei acima dos Reis, Senhor acima dos Senhores.
     G7+                       D/F#        C    A7/9/11
Domínio acima dos domínios, da terra.

D9                     C/D        G/D          A/D  D     D/F#
Trono acima dos céus, Justiça acima da Justiça
      G7+                       D/F#      Asus/E  A7
Vontade acima das vontades do homem.

D9            G/D A/D   D9
Sua mão escreve a história
F               Bb/F C/F  F
Tudo nEle tem seu  fim
F/Eb Bb/D  Bbm/Db Bb/C  F/C G/B   Bb  Asus  D
Jesus,           Jesus,              Jesus,       Jesus!


D9                 C/D   Bb7+  C7+     D7+/9
Rei acima dos Reis, Jesus, Jesus , Jesus!

----------------- Acordes -----------------
A/D = X X 0 6 5 5
A7 = X 0 2 0 2 0
A7/9/11 = 5 X 5 4 3 X
Asus = X 0 2 2 2 0
Asus/E = 0 X 2 2 2 0
Bb = X 1 3 3 3 1
Bb/C = X 3 X 3 3 1
Bb/D = X 5 X 3 6 6
Bb/F = X 8 8 7 6 X
Bb7+ = X 1 3 2 3 1
Bbm/Db = X 4 X 3 6 6
C = X 3 2 0 1 0
C/D = X X 0 0 1 0
C/F = X 8 10 9 8 8
C7+ = X 3 2 0 0 X
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D7+/9 = X 5 4 6 5 X
D9 = X X 0 2 3 0
F = 1 3 3 2 1 1
F/C = X 3 3 2 1 1
F/Eb = X X 1 2 1 1
G/B = X 2 0 0 3 3
G/D = X 5 5 4 3 X
G7+ = 3 X 4 4 3 X
