Corinhos Evangélicos - Vim Para Adorar-te

[Intro] E  B9  C#m  A9
        E  B9  A9

E    B9      F#m             A9
Luz do mundo viestes à terra
E        B9    A9
Pra que eu pudesse te ver
E   B9     F#m     A9
Tua beleza me leva a adorar-te
E     B9    A9
Quero contigo viver

       E
Vim para adorar-te
        B9
Vim para prostrar-me
      E/G#         A9
Vim para dizer que és meu Deus
         E
És totalmente amável
      B9
Totalmente digno


     E/G#      A9
Tão maravilhoso para mim

E    B9          A9
Eterno rei exaltado nas alturas
E   B9    A9
Glorioso nos céu
 E    B9       F#m       A9
Humilde vieste à terra que criaste
 E    B9    A9        A9/B
Por amor pobre se fez

       E
Vim para adorar-Te
         B9
Vim para prostrar-me
       E/G#         A9
Vim para dizer que és meu Deus
         E
És totalmente amável
       B9
Totalmente digno
      E/G#    A9
Tão maravilhoso para mim

  B      E/G#    A9
Eu nunca saberei o preço
   B     E/G#      A9
Dos meus pecados  lá  na cruz

----------------- Acordes -----------------
A9 = X 0 2 2 0 0
A9/B = X 2 2 2 0 0
B = X 2 4 4 4 2
B9 = X 2 4 4 2 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
F#m = 2 4 4 2 2 2
