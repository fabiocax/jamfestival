Corinhos Evangélicos - Deixa a Glória de Deus Brilhar

         E
Deixa a  glória de Deus brilhar,
                            A
Deixa a glória de  Deus bri lhar,
                            B7
Deixa a glória de Deus bri lhar,
                           E      B7
Deixa a glória de Deus bri lhar,
         E
Deixa a  glória de Deus brilhar,
         E7                      A
Deixa a  glória em seu rosto bri lhar.
         Am                 E            C#m
Deixa a  glória de Deus bri lhar em seu  rosto,
         F#m        B7       E      B7
Deixa a  glória de  Deus bri lhar.

     E
Foi  lá no cume do monte
                              F#m
Que o nosso Deus com Moisés fa lava;


Quando  ele desceu do monte
     B7                       E       B7
Não sabia que seu rosto bri lhava,
    E        E7                      A
Bri lhava, brilhava, o seu  rosto bri lhava;

Cantar 2x:

     A              E            C#m
Foi  lá no cume do  monte Sinai
             F#m          B7     E
Que o nosso  Deus com Moi sés fa lava.

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
