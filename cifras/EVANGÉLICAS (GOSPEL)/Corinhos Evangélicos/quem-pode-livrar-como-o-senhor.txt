Corinhos Evangélicos - Quem Pode Livrar Como o Senhor?

G            Bm            Am  C  D7  G
Quem pode livrar como o Senhor?
G            Bm            Am  C   D7 G
Ele é poderoso pra me guardar.

G          Bm      Am
Quando os meus inimigos
G          Bm        Am
Se levantaram contra mim
C      D        Bm    Em        C
O Senhor estendeu sua mão para mim
      D       G   G7
E me deu a vitória

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
