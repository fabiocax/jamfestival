Corinhos Evangélicos - Jesus É a Aliança

C          Am          F        G     G7
Jesus é a aliança, entre você e Deus
C          Am         Dm        E     E7
Jesus é a aliança, entre você e Deus

F                      G
Hoje ele te chama para renovar
E                      Am
Hoje ele te chama para restaurar
F                     G4 G    C
Hoje ele te chama para dele derramar

C          Am          F        G     G7
Jesus é a aliança, entre você e Deus
C          Am         Dm        E     E7
Jesus é a aliança, entre você e Deus

F                      G
Hoje ele te chama para renovar
E      E7                Am
Hoje ele te chama para restaurar

F                     G4 G    C
Hoje ele te chama para dele derramar

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G4 = 3 5 5 5 3 3
G7 = 3 5 3 4 3 3
