Corinhos Evangélicos - Celebrai Com Júbilo

[Intro] G  Em  C  D

[Solo]

D|---------------------------------------------|
B|---------------------------------------------|
G|---------------------------------------------|
D|-7---9-7-5----9-7-5----9-7-5-7---------------|
A|----------7--------5----------9-9-7-7-5-5-3--|
E|---------------------------------------------|

G
  Celebrai com júbilo ao Senhor
Em
  todos moradores da terra
C                 Am
  Servi ao Senhor com alegria
D
  Apresentai-vos a Ele com cânticos
G
  Sabei que o  Senhor Bom

Em
  E eterna é a sua bondade
 C            Am
  E a sua fidelidade
D
  De geração a geração

G
  Aleluia, glória e aleluia
Em
  Aleluia, glória e aleluia
C                  Am
  Aleluia, glória e aleluia
D
  Aleluia, glória e aleluia
Amem

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
