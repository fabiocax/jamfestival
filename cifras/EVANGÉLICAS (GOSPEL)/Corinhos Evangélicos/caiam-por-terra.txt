Corinhos Evangélicos - Caiam Por Terra 

G9               D/F#
Caiam por terra agora
    Em         C
Os inimigos de Deus
G9          D          C     C  D7
Seja estabelecida a casa do Senhor.

    G             D/F#
O Senhor é Deus que sara
    Em         C
Que cura e santifica
G           D/F#        Em C D
Ele tem nos dado nova vida

G9          D9
Ele tem nos dado um
     Em                C
Novo cântico, um novo espírito
   G             D/F#          C   D7
Com nossos lábios vamos proclamar.


G               D/F#
Caíram por terra agora
Em            C
Os inimigos de Deus
 G          D/F#          Em    C D7 G9
Está estabelecida a casa do Senhor

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
D9 = X X 0 2 3 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G9 = 3 X 0 2 0 X
