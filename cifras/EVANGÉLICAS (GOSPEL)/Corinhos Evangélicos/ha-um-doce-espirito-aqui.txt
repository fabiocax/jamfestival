Corinhos Evangélicos - Há Um Doce Espirito Aqui

G          C         Bm  Em
Há um doce Espírito aqui
      Am          D          C    G
E eu sei que é o Espírito de Deus
           C          Bm  Em
Já podemos todos perceber
       Am       D          C   G
A presença de Jesus o salvador

G  C/G   G        C/G    G
Doce presença, presença santa
Bm         Em  Am                    D
Vem sobre nós encher-nos com o teu poder

G   C/G    G      Bm            Em
Te adoraremos com tua presença aqui
       G  C/G          G    C#m/G#
Que renovará os nossos corações
     Am      D     G
Louvado seja o Senhor

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C#m/G# = X X 6 6 5 4
C/G = 3 3 2 X 1 X
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
