Corinhos Evangélicos - Meu Milagre

E
Quando os sonhos se perderam
E/G#
A esperança se acabou
C#m                               A     E/G#
Eu não posso crer, ainda não é o fim
F#m                             B
Mas eu ouço sua voz a me chamar
F#m                E/G#
Preciso te encontrar
F#m               E/G#
Se eu apenas te tocar
F#m                      B
O sobrenatural vai acontecer

E                   B
Meu milagre chegou, teu amor me alcançou
A                        E/G#
Meus sonhos não vão se perder
F#m                             B
Sei que és fiel pra me socorrer

E                   B
Meu milagre chegou, teu amor me alcançou

F#m             E/G#
Eu vou confiar, não vou desisitir
A                B                      E (2 parte entra no interlúdio)
Eu sei que o melhor de deus eu vou viver.

interlúdio:

(C#m  A  B  A)
(C#M  A  B)

Ataque: B E F#m D#m E F#m E

----------------- Acordes -----------------
A = X 0 2 2 2 0
E = 0 2 2 1 0 0
