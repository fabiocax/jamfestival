Corinhos Evangélicos - Aleluia

 E9     A/C#     B/D#     E9
Aleluia, aleluia, aleluia, aleluia
E/G#   F#m7(9)      B7(4)    B7    E9
Aleluia, aleluia, alelui....a, aleluia
E9       E/G#   A9  B/A  E/G# G# G#/C
A.........leuia, aleluia
C#m7(9)       F#   B9       E9
Aleluia, aleluia, aleluia, aleluia

----------------- Acordes -----------------
A/C# = X 4 X 2 5 5
A9 = X 0 2 2 0 0
B/A = X 0 4 4 4 X
B/D# = X 6 X 4 7 7
B7 = X 2 1 2 0 2
B7(4) = X 2 4 2 5 2
B9 = X 2 4 4 2 2
C#m7(9) = X 4 2 4 4 X
E/G# = 4 X 2 4 5 X
E9 = 0 2 4 1 0 0
F# = 2 4 4 3 2 2
F#m7(9) = X X 4 2 5 4
G# = 4 3 1 1 1 4
G#/C = X 3 X 1 4 4
