Corinhos Evangélicos - Eu Celebrarei

Em              C                    D      Em       C  D Em7
  Eu celebrarei,  cantando ao Senhor, e só Nele me alegrarei...
Em              C                    D      Em       C  D Em7
  Eu celebrarei,  cantando ao Senhor, e só Nele me alegrarei...

Em            C        D                  Em7
   Eu O louvo,  e adoro porque tem triunfado.
Em            C        D                  Em7
   Eu O louvo,  e adoro porque tem triunfado.

Em                               D
Santo é o Senhor Rei do Universo. Santo é o Senhor Rei do Universo.
Em                                       B7  Am Bm7
Santo é o Senhor Rei do Universo. Porque grande é...

Em                       D
Oo... Hosana nas alturas, Oo... hosana nas alturas.
Em                               B7  Am Bm7
Oo... Hosana nas alturas, porque grande és.

Em                           D
Bendito é, bendito o que vem.  Bendito é, bendito o que vem

Em                                 B7 Am Bm7
Bendito é, bendito o que vem. Em nome de Deus.

Em                       D
Oo... Hosana nas alturas, Oo... hosana nas alturas.
Em                               B7 Am Bm7
Oo... Hosana nas alturas, porque santo és.

        C   D Em
Por que santo és.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
