Corinhos Evangélicos - Medley da Páscoa

[Intro] Bm  A

         Bm
Pelo senhor marchamos sim
          Bm
E o seu exército poderoso é
      Em          A             Bm
Sua glória será vista em toda a terra

         Bm
Vamos cantar o canto da vitória
          Bm
Glória a deus, vencemos a batalha
     Em           A       Bm
Toda arma contra nós perecerá

    G             D
O nosso general é cristo
    G             D
Seguimos os seus passos
    G           Em   A  Bm
Nenhum inimigo nos resistirá

       G             D
O nosso general é cristo
    G             D
Seguimos os seus passos
    G           Em   A  Bm
Nenhum inimigo nos resistirá

( Bm )

       Bm
Com o messias marchamos sim
         Bm
Em suas mãos a chave da vitória
         Em                A         Bm
Que nos leva a possuir a terra prometida

          Bm
Vamos cantar  o canto da vitória
          Bm
Glória a deus, vencemos a batalha
     Em           A       Bm
Toda arma contra nós perecerá

    G             D
O nosso general é cristo
    G             D
Seguimos os seus passos
    G           Em   A  Bm
Nenhum inimigo nos resistirá
       G             D
O nosso general é cristo
    G             D
Seguimos os seus passos
    G           Em   A  Bm
Nenhum inimigo nos resistirá

O exército de deus

    E                G#          C#m
Eu posso escutar o exército de deus
     F#m             B
Marchando sobre a terra
     C             D
Em plano de guerra vai

   E                     G#          C#m
Já ouço o louvor que é a arma de vitória
    F#m         B
O exército de deus
    F#m         B
O exército de deus
    F#m         B  C
O exército de deus
           E  F#m  E/G#  E
Marchando vai

Cristo não está morto

E
Cristo não está morto, ele está vivo!
B                     C
Sinto em minhas mãos, sinto em meus pés
A         B         E
Sinto em todo meu ser

Quando estou com o povo de deus

         E               B/Eb      C#m         C#m7/B    E   Fº    F#m
Quando estou com o povo de deus eu sinto  a    maior alegria
                  F#m            F#m/F      F#m/E    F#m/Eb    B7(4)   B7            E
Quando estou com o povo de deus eu   sinto       a    real     harmonia
              Bm                       E7(9)
Que prazer ver o povo de deus louvando
A7M                                       D7(9)
Tendo assim um lugar todo santo
                   E              B/Eb      C#m7
Quando estou com o povo de deus
      E          F#m E/G#    A  B7(4)      B7         E
Eu sinto  a   maior                          das alegrias!

A alegria

E            E7
A alegria está no coração
     A                  E
De quem já conhece a jesus
E             C#m
A verdadeira paz só tem aquele
     F#7             B
Que já conhece a jesus

Refrão:
E                   E7
O sentimento mais precioso
     A               Am
Que vem do nosso senhor
      E C#m       F#m             B        E
É o amor que só tem quem já conhece a jesus

         E                   E7
Posso pisar numa tropa e saltar as muralhas
    A
Aleluiaaaa! aleluiaaa!
E                 E7
Ele é a rocha da minha salvação
    A                Am
Com ele não há mais condenação
         E                E7
Posso pisar numa tropa e saltar as muralhas
    A           E
Aleluiaaaa! aleluiaaa!

E                   E7
O sentimento mais precioso
     A               Am
Que vem do nosso senhor
      E C#m       F#7             B        E
É o amor que só tem quem já conhece a jesus
      E C#m       F#7             B        E
É o amor que só tem quem já conhece a jesus

Meu coração transborda

     E         A         G#m    C#m
Meu coração transborda de amor
            F#m B               E    A  E
Porque meu deus é um deus de amor
            E      A        G#m    C#m
Minh'alma está repleta de paz
         F#m  B            E    E7
Porque jesus é a minha paz

         A       Am         G#m       C#m
Eu digo aleluia, aleluia, aleluia,
           F#m      B            Bm   E
Aleluia, aleluia! eu digo por quê
         A       Am         G#m       C#m
Eu canto aleluia, aleluia, aleluia
           F#m      B        E
Aleluia, aleluia! aleluia! amém.

Há momentos

      E                             C#m
Há momentos, que só palavras não resolvem
                  F#m                   B7
Mas os gestos de jesus (os gestos de jesus)
                     E   B7
Transmitem amor por nós
      E                             C#m
Há momentos, que só palavras não resolvem
                  F#m                   B7
Mas os gestos de jesus (os gestos de jesus)
                     E   E7
Transmitem amor por nós

           A                Am
Foi no calvário (foi no calvário)
             E                    C#m
Que ele sem falar (que ele sem falar)
                    F#m               B7
Mostrou ao mundo inteiro (ao mundo inteiro)

            E               E7
O que era amar (o que era amar)

           A                Am
Foi no calvário (foi no calvário)
             E                    C#m
Que ele sem falar (que ele sem falar)
                    F#m               B7
Mostrou ao mundo inteiro (ao mundo inteiro)
            E   B7
O que era amar

         E                        C#m
Aqui no mundo, as desilusões são tantas
                    F#m                   B7
Mas existe uma esperança (existe uma esperança)
               E                 B7
O cristo voltará (o cristo voltará)
         E                        C#m
Aqui no mundo, as desilusões são tantas
                    F#m                   B7
Mas existe uma esperança (existe uma esperança)
               E   E7
O cristo voltará

           A                Am
Foi no calvário (foi no calvário)
             E                    C#m
Que ele sem falar (que ele sem falar)
                    F#m               B7
Mostrou ao mundo inteiro (ao mundo inteiro)
            E               E7
O que era amar (o que era amar)

           A                Am
Foi no calvário (foi no calvário)
             E                    C#m
Que ele sem falar (que ele sem falar)
                    F#m               B7
Mostrou ao mundo inteiro (ao mundo inteiro)
            E   B7  E
O que era amar

Jesus em tua presença

E              A               B    E
Jesus em tua presença, reunimo-nos aqui
                  A         B         E
Contemplamos tua face e rendemo-nos a ti
        C#m7      G#m
Pois um dia tua morte
        A       B     E
Trouxe vida a todos nós
       E7            A
E nos deu completo acesso
    B          E
Ao coração do pai.

     C#m         G#m     A      B    C#m
E o véu que separava já não separa mais
   C#m           G#m     A
A luz, outrora apagada agora brilha
   B               F#4
E cada dia brilha mais
A      B      E
Só prá te adorar
     A       B    C#m
E fazer teu nome grande
A     B        E     B     F#
E te dar o louvor que é devido
  E/B   A/B    E
Estamos nós aqui.

A      B      E G#m C#m B
Só prá te adorar
     A       B    C#m
E fazer teu nome grande
A     B        C#m      B    F#
E te dar o louvor que é devido
  A      B    E ( E4 E )
Estamos nós aqui.

Louvai ao senhor

   E         B       A         E
Louvai ao senhor, louvai ao senhor
      A         E         B ( E )
Bendizei o seu nome para sempre
A              E
Ele é a rocha da minha vida
   A              E
A força da minha salvação
F#m
Sua fidelidade é grande
A              B     E
Seu amor nunca acabará

És tu

E         B                  C#m7          A
És tu, única razão da minha adoração, ó jesus
E         B                 C#m7              A
És tu, única esperança que alegro em ter, ó jesus

F#m                    B4  B
Confiei em ti, fui ajudado
F#m                     B4  B
Tua salvação tem me alegrado

E                   B                C#m7       A  B
Hoje há gozo em meu coração, com meu canto te louvarei

F#          C#    D#m          B
Eu te louvarei, te glorificarei
F#          C#              B   ( B  F#  B  F#  B  F#  B  B )
Eu te louvarei, meu bom jesus
F#          C#    D#m          B
Eu te louvarei, te glorificarei
F#          C#              B   ( B  F#  B  F#  B  F#  B  B )
Eu te louvarei, meu bom jesus

( E )

E         B                  C#m7          A
És tu, única razão da minha adoração, ó jesus
E         B                 C#m7              A
És tu, única esperança que alegro em ter, ó jesus

F#m                    B4  B
Confiei em ti, fui ajudado
F#m                     B4  B
Tua salvação tem me alegrado
E                   B                C#m7       A  B
Hoje há gozo em meu coração, com meu canto te louvarei

F#          C#    D#m          B
Eu te louvarei, te glorificarei
F#          C#              B   ( B  F#  B  F#  B  F#  B  B )
Eu te louvarei, meu bom jesus
F#          C#    D#m          B
Eu te louvarei, te glorificarei
F#          C#              B   ( B  F#  B  F#  B  F#  B  B )
Eu te louvarei, meu bom jesus

( E )

E       A               B
Em todo tempo, te adorarei
E       A             B
Em todo tempo, te adorarei
E       A               B
Em todo tempo, te adorarei
E       A             B    F#
Em todo tempo, te adorarei

F#           C#    D#m          B
Eu te louvarei, te glorificarei
F#           C#              B ( B  F#  B  F#  B  F#  B  B )
Eu te louvarei, meu bom jesus
F#            C#   D#m          B
Eu te louvarei, te glorificarei
F#            C#             B
Eu te louvarei, meu bom jesus

Ele é exaltado

D                  D/F#                 G       G/B  A/C#
Ele é exaltado,  o rei é exaltado nos céus eu o louvarei
D                    D/F#                 G  A  G/B  A/C#  B/D#
Ele é exaltado, pra sempre exaltado o seu nome louvarei
Em  D       A/C#  G/B  A  D       D/F#       G
Ele é o senhor sua verdade vai sempre reinar
Em  D   A/C#  G/B  A      D        D/C   G/B  B/D#
Terra e céus      glorificam seu sa nto no   me
Em7       D       C        A        D    B  B5
Ele é exaltado, o rei é exaltado nos céus
E                  E/G#                 A9        C#m   B/D#
Ele é exaltado,  o rei é exaltado nos céus eu o louvarei

Vem esta é a hora

D   D9                 D4  D
Vem, esta é a hora da adoração
 A    A9           Em F#m G
Vem, dar a ele teu co_ra_ção
 D      D9                 D4  D
Vem, assim como estás para adorar
 A      A9              Em  F#m   G
Vem, assim como estás diante do pai
 D
Vem
 G                 D
Toda língua confessará o senhor
 G                 D
Todo joelho se dobrará
 G                  Bm
Mas aquele que a ti escolher
     Em            A
O tesouro maior terá

Porque ele vive

            A   A/C#              D
Porque ele vive,    posso crer no amanhã
            A               E
Porque ele vive, temor não há
            A
Mas eu bem sei, eu sei
             D
Que a minha vida
          A             E              A
Está nas mãos de meu jesus que vivo está

[segunda parte]

                        D
Deus enviou seu filho amado
        A         Bm  E
Pra morrer e perdoar
            A               D    B5
Na cruz morreu, por meus pecados
    C#5  F#m
Mas o sepulcro
         E/G#          D
Vazio está porque ele vive

[refrão]

            A                  D
Porque ele vive, posso crer no amanhã
            A               E
Porque ele vive, temor não há
            A
Mas eu bem sei, eu sei
             D   B5
Que a minha vida
   C#5    F#m           E/G#           G  F#5  G#5  E
Está nas mãos de meu jesus que vivo está

[segunda parte]

            A             D
E quando enfim, chegar a hora
          A             Bm   E
Em que a morte enfrentarei
            A            D    B5
Sem medo então, terei vitória
   C#5     F#m            E/G#            D
Verei na glória, o meu jesus, que vivo está

[refrão]

            A                  D
Porque ele vive, posso crer no amanhã
            A               E
Porque ele vive, temor não há
            A
Mas eu bem sei, eu sei
             D   B5
Que a minha vida
   C#5    F#m           E/G#
Está nas mãos de meu jesus que vivo está

[Solo] G

Há um rio

( G5 B5 C5 E5 D5 )
( G5 B5 C5 E5 D5 )
( G5 B5 C5 E5 D5 )
( G5 B5 C5 E5 D5 )

       G
Há um rio que corre do trono de deus a morada do
          C D
Todo poderoso.  (2x)

         C  D/C          Bm7 Em     Am7        C/D
Que nos guarda, que nos cura, que alegra o nosso
     G
Coração

 G    F      C      D    G      F    C         D
Sê exaltado e bendito, para sempre oh senhor

Celebrai a cristo

G    C     D       G        C D G
Celebrai a cristo, celebrai
G    C     D       G        C D G
Celebrai a cristo, celebrai
G    C     D       G        C D G
Celebrai a cristo, celebrai
G    C     D       G
Celebrai a cristo, celebrai

       D            Em
Ressuscitou, ressuscitou
    D              Em
E hoje vive, para sempre
       D            Em
Ressuscitou, ressuscitou
     D             Em
E hoje vive, para sempre

D               C   C#  D
Vamos celebrar, hei

D               C C#  D
Vamos celebrar, oooohhh
D                      C      D      G
Vamos celebrar, ressuscitou o meu senhor

Ouve-se o júbilo

Em
Ouve-se um júbilo em todos os povos
D
Os reis se dobraram ao senhor
C                     Am       Bm
Ouve-se um brado de vitória o dia do senhor chegou!
Em                               D
Ouve-se em todos os povos que um novo rei
          C           Am            Bm
Surgiu, impérios reconhecem que tua destra reinará!
Em           D             C           Am
Leão de judá,leão de judá, leão de judá
Bm
Prevaleceu
C          D       Em          C
E os povos virão e verão que a sião
D             Em            C      Am
Ascendeu sua lei pois a sua justiça
Bm
Governará

Ele é o leão

Em           D             C           Am
Leão de judá,leão de judá, leão de judá
Bm
Prevaleceu

Em
Ele é o leão da tribo de judá
      D
Jesus quebrou nossas cadeis e nos libertou
C             Am       Bm
Ele é a rocha da nossa vitória
Em
A nossa força em tempos de fraqueza
      D
É uma torre bem forte em tempos de guerra
C    Am           Bm
Oooh esperança de israel

Senhor deus está no meio de ti

  G         D                  Em
Senhor deus está no meio de ti
G          D         Em
Senhor deus te salva
G       D         Em
Sobre ti se alegrará
C                         D7
Ele em amor te renovará
C                      D7
E deixará sobre ti
         G D Em    G D Em
A alegria,   a alegria,
         C  D      C  D
A alegria, a alegria,
         G D Em  G D  Em  G
Deixará.  oooohh.


A terra vai um dia contemplar

    G                     Em
A terra vai um dia contemplar
 C   Am              D
Aquele que um dia virá
 G                   Em
Com autoridade julgará
         C  Am          D
Toda a terra se ajuelhará

G
Oh aleluia
             Em
Ele voltará
    C  Am
Oh aleluia
                D
Para nos levar

G                       Em
Uma nova terra assim será

     C   Am              D
E a paz nunca se findará
G                       Em
Oh como será maravilhoso
   C   Am             D
Contemplar o meu salvador

G
Oh aleluia
        Em
Ele voltará
     C    Am
Oh aleluia
                D
Para nos levar

Jesus virá

G                      C/G
Num  dia lindo  almejo eu  encontrar
D/G
A  eterna  glória  que
      C/E D   C/E  G
Prome ti  da  es   tá
G                          C/G
Gozo  e  alegria eu posso  então  sentir
D9                        C  G/B   Am7  G
Pois  jesus  cristo  já  está    por  vir

        G   C/G
Jesus virá outra vez aqui
         D9                        C  G/B  Am7 G    C/D
Jesus virá  jesus virá mais outra vez   aqui
 G                    C/G
Por isso todos juntos em  um só louvor  cantemos
           D7
Cantemos sempre ele  é
   C     G/B  Am7  G
O  nosso   senhor


G                         C/G
Ele veio ao mundo prá ser mártir  da  paz
D/G
Ele  foi tentado e  não
C/E   D    C/E  G
Pe   cou   ja mais
G                C/G
A sua vida foi exemplo de amor
 D9
Por  isso  eu canto
           C  G/B   Am7    G
Ele  é  o  nosso   senhor

Te agradeço

     D9             G
Por tudo o que tens feito
     D9               A
Por tudo o que vais fazer
     D9     D/C    G/B
Por tuas promessas e tudo o que és
     D/A         A
Eu quero te agradecer
                G  D9
Com todo o meu ser

[pré-refrão]

        G
Te agradeço
        D9
Meu senhor
        G
Te agradeço
        D9
Meu senhor

[refrão]

        D9                       D/C
Te agradeço por me libertar e salvar
 G/B
Por ter morrido em meu lugar
        D9  D/F#  G
Te agrade---ço
               D9  D/F#  G
Jesus, te agrade---ço
           D9  D/F#  G
Eu te agrade---ço
        Em   D9
Te agrade---ço

( G  D9 )

[primeira parte]

     D9             G
Por tudo o que tens feito
     D9               A
Por tudo o que vais fazer
     D9     D/C    G/B
Por tuas promessas e tudo o que és
     D/A         A
Eu quero te agradecer
                G  D9
Com todo o meu ser

[pré-refrão]

        G
Te agradeço
        D9
Meu senhor
        G
Te agradeço
        D9
Meu senhor

[refrão]

        D9                       D/C
Te agradeço por me libertar e salvar
 G/B
Por ter morrido em meu lugar
        D9  D/F#  G
Te agrade---ço
               D9  D/F#  G
Jesus, te agrade---ço
           D9  D/F#  G
Eu te agrade---ço
        Em   D9
Te agrade---ço

[pré-refrão]

        G
Te agradeço
        D9
Meu senhor
        G
Te agradeço
        D9
Meu senhor

[refrão]

        D9                       D/C
Te agradeço por me libertar e salvar
 G/B
Por ter morrido em meu lugar
        D9  D/F#  G
Te agrade---ço
               D9  D/F#  G
Jesus, te agrade---ço
           D9  D/F#  G
Eu te agrade---ço
        Em   D9
Te agrade---ço

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/B = X 2 2 2 2 X
A/C# = X 4 X 2 5 5
A7M = X 0 2 1 2 0
A9 = X 0 2 2 0 0
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
B = X 2 4 4 4 2
B/D# = X 6 X 4 7 7
B/Eb = X 6 X 4 7 7
B4 = X 2 4 4 5 2
B5 = X 2 4 4 X X
B7 = X 2 1 2 0 2
B7(4) = X 2 4 2 5 2
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
C# = X 4 6 6 6 4
C#5 = X 4 6 6 X X
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
C#m7/B = 7 4 6 4 5 4
C/D = X X 0 0 1 0
C/E = 0 3 2 0 1 0
C/G = 3 3 2 X 1 X
C5 = X 3 5 5 X X
D = X X 0 2 3 2
D#m = X X 1 3 4 2
D/A = X 0 X 2 3 2
D/C = X 3 X 2 3 2
D/F# = 2 X 0 2 3 2
D/G = 3 X X 2 3 2
D4 = X X 0 2 3 3
D5 = X 5 7 7 X X
D7 = X X 0 2 1 2
D7(9) = X 5 4 5 5 X
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
E/B = X 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E4 = 0 2 2 2 0 0
E5 = 0 2 2 X X X
E7 = 0 2 2 1 3 0
E7(9) = X X 2 1 3 2
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
F#4 = 2 4 4 4 2 2
F#5 = 2 4 4 X X X
F#7 = 2 4 2 3 2 2
F#m = 2 4 4 2 2 2
F#m/E = X X 2 2 2 2
F#m/Eb = X 6 0 6 7 5
F#m/F = X X 3 2 2 2
Fº = X X 3 4 3 4
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
G#5 = 4 6 6 X X X
G#m = 4 6 6 4 4 4
G/B = X 2 0 0 3 3
G5 = 3 5 5 X X X
