Corinhos Evangélicos - Cativar

(intro dedilhado) G Em Am D

     G           Em
Uma palavra tão linda
            Am                  D   D4
já quase esquecida me fez relembrar,
    G              Em
contendo sete letrinhas
            Am              D    D4
e todas juntinhas se lê cativar.

  G         Em
Cativar é amar,
    Am       D
é também carregar
  G              Em
um pouquinho da dor
      Am         D
que alguém vai levar.

   G            Em
Cativou,(cativou) disse alguém,(disse alguem)

   Am           D
laços fortes criou,
   G           Em        Am   D   G
responsável é você pelo que cati-vou.

     G         Em
No deserto tão só,
    Am         D
entre homens também,
 G              Em
vou tentar cativar,
  Am             D
viver perto de alguém.

  G            Em
Cativou, disse alguém,
   Am           D
laços fortes criou,
   G           Em        Am   D   G
responsável é você pelo que cati-vou.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
D = X X 0 2 3 2
D4 = X X 0 2 3 3
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
