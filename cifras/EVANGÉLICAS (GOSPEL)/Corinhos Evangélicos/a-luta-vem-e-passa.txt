Corinhos Evangélicos - A Luta Vem e Passa

   F            C                  F
A luta vem e passa, não desanime não
                      C                  F
Quem crer em Jesus Cristo já tem a salvação. (2x)
F7               Bb     C            F
É reino contra reino, nação contra nação
               C                  F
Filho contra pai e irmão contra irmão
F7                   Bb        C       F
Há crente que não gosta de passar tribulação
                   C                    F
Mas para entrar no céu é com luta meu irmão.
      F             C                F
Mas a luta vem e passa não desanime não,
             C                   F
quem crer em Jesus Cristo já tem a salvação.(2x)

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
