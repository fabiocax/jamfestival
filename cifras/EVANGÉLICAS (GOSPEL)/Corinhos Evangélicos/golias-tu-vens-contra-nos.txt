Corinhos Evangélicos - Golias Tu Vens Contra Nós

Dm               C9         Dm    C9
Golias tu vens contra nós com espadas e lanças
       Bb            Gm7            A7
Mas nós vamos contra ti em nome do Senhor

       Dm                 C
Quem és tu Golias, quem  és tu Golias
    Bb                         A7
Pra afrontar o exército do Deus vivo?

      Dm              Am
Tu já és um gigante vencido
    Dm              Am
O Senhor já te destronou
     Bb7+                  Gm7
Hoje mesmo o Senhor te entregará
         A7
Em nossas mãos...

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bb7+ = X 1 3 2 3 1
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
Dm = X X 0 2 3 1
Gm7 = 3 X 3 3 3 X
