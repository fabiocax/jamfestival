Corinhos Evangélicos - Eu Marquei Um Encontro Com Deus

[Intro] D  Bm  A

   D      D7        G     Gm
Nesta noite feliz, neste santo lugar
    D    Bm       A
Eu marquei um encontro com Deus
   D    D7      G     Gm
Seu amor é real, sua paz gozarei
    D     Em      D
Eu marquei um encontro com Deus

    A     G      D
Eu marquei um encontro com Deus
   A     G        D
Neste santo lugar de oração
   D    D7      G     Gm
Seu amor é real, sua paz gozarei
    D     A      D
Eu marquei um encontro com Deus

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
