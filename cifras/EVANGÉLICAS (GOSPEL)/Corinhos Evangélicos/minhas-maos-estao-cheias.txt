Corinhos Evangélicos - Minhas Mãos Estão Cheias

               D                 A
Minhas mãos estão cheias das tuas bençãos
                     G    A         D        A
minhas mãos estão cheias   das tuas bençãos
                   D                 A
minhas mãos estão cheias das tuas bençãos
                    G    A         D         D7
minhas mãos estão cheias   das tuas bençãos

                                  G
por isso  aquele que eu tocar abençoado será
                                    D                 Bm
tpor isso aquele que eu tocar abençoado será
                  Em     A          D
Minhas mãos estão cheias   das tuas bençãos

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
