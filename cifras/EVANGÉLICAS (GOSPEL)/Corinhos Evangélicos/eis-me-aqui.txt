Corinhos Evangélicos - Eis-me Aqui

        C                Em
Eis-me-aqui Senhor venho me render
         F                 F#º      G7
Eis-me aqui oh Deus minha vida oferecer.
       C                Em  A
E na cruz em meio a adoração
    F                 G#m     G  C
Elimina o que é da velha criação.

   G7     Am       Am/G      F F#º
Eu sei Senhor que é direito teu
         C         G        C7
Mudares tudo em alguém como eu
G         Am        Am/G       F     F#º
Isto é o melhor que tens pra minha vida
       C     A   Dm    G7   C
Só me resta em amor te obedecer.
       F    C   A   Dm  G    C
Só me resta em amor me oferecer.

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Am/G = 3 X 2 2 1 X
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F#º = 2 X 1 2 1 X
G = 3 2 0 0 0 3
G#m = 4 6 6 4 4 4
G7 = 3 5 3 4 3 3
