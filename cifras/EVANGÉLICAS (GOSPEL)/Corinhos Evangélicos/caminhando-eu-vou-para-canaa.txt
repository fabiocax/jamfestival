Corinhos Evangélicos - Caminhando Eu Vou Para Canaã

             A           E
Caminhando eu vou para Canaã
                          A
Caminhando eu vou para Canaã
              A7         D Dm
Caminhando eu vou para Canaã
       A                  E           A
Glória Deus caminhando eu vou para Canaã
             A              E
Se você não vai, não impeça eu ir
                             A
Se você não vai, não impeça eu ir
           A7          D    Dm
Se você não vai, não impeça eu ir
       A                  E           A
Glória Deus caminhando eu vou para Canaã
             A           E
Caminhando eu vou para Canaã
                          A
Caminhando eu vou para Canaã
              A7         D Dm
Caminhando eu vou para Canaã

       A                  E           A
Glória Deus caminhando eu vou para Canaã
             A              E
Se você não vai, não impeça eu ir
                              A
Se você não vai, não impeça eu ir
           A7          D    Dm
Se você não vai, não impeça eu ir
      A                  E            A
Glória Deus caminhando eu vou para Canaã

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
