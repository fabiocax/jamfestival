Corinhos Evangélicos - Eu Te Exalto

A               E         D   Bm7    E
Grande Deus Consolador, Príncipe da Paz
A            E       D      Bm7    D/F#        E
Forte Deus, vencedor, sempre vive e sempre reinará

       A              E
Eu te exalto, eu te exalto
    D            Bm7       D/F#          E
És tudo em minha vida, precioso és para mim
        A            E
Eu te exalto, eu te exalto
  D    C#m Bm  Bm7    D   C#m Bm  Bm7  D   C#m E4   E     A
Te Exalto  Cristo ,te exalto cristo ,te exalto cristo meu Rei

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
E = 0 2 2 1 0 0
E4 = 0 2 2 2 0 0
