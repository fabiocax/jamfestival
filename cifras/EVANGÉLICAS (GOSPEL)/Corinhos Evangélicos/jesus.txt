Corinhos Evangélicos - Jesus

C    Am      F      G
Jeêêsus Jeêêsus Jeêêsus
D    Bm      G      A
Jeêêsus Jeêêsus Jeêêsus

E   C#m     A       B
Jêêsus Jeêêsus Jeêêsus
F    Dm      Bb      C
Jeêêsus Jeêêsus Jeêêsus

G    Em      C       D
Jeêêsus Jeêêsus Jeêêsus
A    F#m     D       Em
Jeêêsus Jeêêsus Jeêêsus

B    G#m     E       F#
Jeêêsus Jeêêsus Jeêêsus

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
Bb = X 1 3 3 3 1
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G#m = 4 6 6 4 4 4
