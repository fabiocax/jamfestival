Corinhos Evangélicos - A Vida Só Tem Valor

C                          G9             Am7
A vida só tem valor para quem cultivar o amor
    F                  Em               Dm  G7/13
E expulsar sem direção a tristeza e toda dor.
   C                         G7                Am
A vida só tem valor para quem viver a paz,
    F                 Em               Dm  G7
E entender que vale  a pena lutar.

   C                          Em/B
Será bem melhor se dermos as mãos
   F                  G7
E juntos prosseguirmos,
C              Em/B
Na direção do Criador
                 F                   G7
Que nos deseja sempre unidos.

C                          Em7
Juntos com amor, vida com amor
F                   G7    C
Isto é o que é vida.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
Em/B = X 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
G7 = 3 5 3 4 3 3
G7/13 = 3 X 3 4 5 X
G9 = 3 X 0 2 0 X
