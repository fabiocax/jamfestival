﻿Fernandinho - Galileu

Capo Casa 1

[Intro] C
        Am  F7M  Am  F7M

Solo Intro (sem capotraste)

E|-------4-4-------------------4-4--------------------|
B|---4-6--------4-6--------4-6------------------------|
G|-6----------6-----3-3--6---------3-3----------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

[Primeira Parte]

C
  Deixou Sua glória

Foi por amor, foi por amor
Am          F7M          Am
   E o seu sangue, derramou
             F7M
Que grande amor


[Segunda Parte]

Dm7          F7M     C
    Naquela via dolorosa
         G4
Se entregou
Dm7           F7M
    Eu não mereço
          C            G4
Mas Sua graça me alcançou

       Dm7           F7M
Eu me rendo ao seu amor
       C             G4
Eu me rendo ao seu amor
       Dm7           F7M
Eu me rendo ao seu amor
       Am
Eu me rendo
       G4
Eu me rendo

[Pré-Refrão]

 Am
Deus Emanuel
    F7M
Estrela da Manhã
 C
Cordeiro de Deus
 G4
Pão da Vida
  Am
Príncipe da Paz
    F7M
O grande El Shaddai
 C
Santo de Israel
 G4
Luz do Mundo

[Refrão]

 Dm7   F7M    C      G4
Ga__lileu, Jesus, Jesus
 Dm7   F7M    C      G4
Ga__lileu, Jesus, Jesus
 Dm7   F7M    C      G4
Ga__lileu, Jesus, Jesus
 Dm7   F7M    C      G4
Ga__lileu, Jesus, Jesus

( Am  F7M  Am  F7M )

[Solo Intro]

E|-------4-4-------------------4-4--------------------|
B|---4-6--------4-6--------4-6------------------------|
G|-6----------6-----3-3--6---------3-3----------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

[Segunda Parte]

       Dm7           F7M
Eu me rendo ao seu amor
       C             G4
Eu me rendo ao seu amor
       Dm7           F7M
Eu me rendo ao seu amor
       Am                  G4
Eu me rendo, me rendo, me rendo, oh

[Pré-Refrão]

 Am
Deus Emanuel
    F7M
Estrela da Manhã
 C
Cordeiro de Deus
 G4
Pão da Vida
  Am
Príncipe da Paz
    F7M
O grande El Shaddai
 C
Santo de Israel
 G4
Luz do Mundo

[Refrão]

 Dm7   F7M    C      G4
Ga__lileu, Jesus, Jesus
 Dm7   F7M    C      G4
Ga__lileu, Jesus, Jesus
 Dm7   F7M    C      G4
Ga__lileu, Jesus, Jesus
 Dm7   F7M    C      G4
Ga__lileu, Jesus, Jesus

[Interlúdio]

F7M
Oh, oh, oh, oh
G4
   Oh, oh, oh, oh
Am                C
    Oh, oh, oh, oh

F7M
Oh, oh, oh, oh
G4
   Oh, oh, oh, oh
Am                C
    Oh, oh, oh, oh

( F7M  G4  Am  C )
( F7M  G4  Am  C )

F7M
Oh, oh, oh, oh
G4
   Oh, oh, oh, oh
Am                C
    Oh, oh, oh, oh

[Refrão]

 Dm7   F7M    C      G4
Ga__lileu, Jesus, Jesus
 Dm7   F7M    C      G4
Ga__lileu, Jesus, Jesus
 Dm7   F7M    C      G4
Ga__lileu, Jesus, Jesus
 Dm7   F7M    C      G4  F7M
Ga__lileu, Jesus, Jesus

----------------- Acordes -----------------
Capotraste na 1ª casa
Am*  = X 0 2 2 1 0 - (*A#m na forma de Am)
C*  = X 3 2 0 1 0 - (*C# na forma de C)
Dm7*  = X X 0 2 1 1 - (*D#m7 na forma de Dm7)
F7M*  = 1 3 3 2 1 0 - (*F#7M na forma de F7M)
G4*  = 3 X 0 0 1 3 - (*G#4 na forma de G4)
