Fernandinho - Rude Cruz

       A                     D
Rude cruz se erigiu, dela o dia fugiu
        E                   A
Como emblema de vergonha e dor

Mas contemplo esta cruz
        D
Porque nela Jesus
       E                 A
Deu a vida por mim, pecador

       E                   A
Sim, eu amo a mensagem da cruz
        D                  A
Até morrer eu a vou proclamar
                        D
Levarei eu também minha cruz
        A      E      A
Até por uma coroa trocar

A                          D
Nesta cruz padeceu e por mim já morreu

       E                     A
Meu Jesus, para dar-me o perdão
                              D
E eu me alegro na cruz, dela vem graça e luz
      E              A
Para minha santificação

A                         D
Eu aqui com Jesus, a vergonha da cruz
       E                 A
Quero sempre levar e sofrer
                           D
Cristo vem me buscar e com Ele, no lar
     E                       F#m   D  Bm C#m
Uma parte da glória hei de ter

         F#m      E/G#     Bm   C#m
Sim, eu amo a mensagem da cruz
       F#m      E/G#     Bm   C#m
Até morrer eu a vou proclamar
     G         A          D
Levarei eu também minha cruz
         A     E      A
Até por uma coroa trocar

       E                   A
Sim, eu amo a mensagem da cruz
        D                  A
Até morrer eu a vou proclamar
              A7         D
Levarei eu também minha cruz
        A      E      F#m  D
Até por uma coroa trocar

        Bm     E       F#m  D
Até por uma coroa trocar
        Bm     E       A
Até por uma coroa trocar

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
