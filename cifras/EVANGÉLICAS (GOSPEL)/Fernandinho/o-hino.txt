Fernandinho - O Hino

Intro: Dm  Bb9  F  C (2x)

E|----------------------3-----------|
B|--------6------------------6----5-|
G|---7-5------------7-------5-------|
D|--7------------8---------3--------|
A|-5------------8-----------------3-|
E|-------------6--------------------|

 Dm                          Bb9
Posso ouvir os passos do meu Rei
 F                   C
Posso escutar Seu coração
    Dm                   Bb9
Da minha escuridão me libertou
 F                       C
Posso ouvir o pai a me Chamar

           Dm           Bb9                  F
Eu ouço: "filho meu, chegou tua hora de brilhar
              C
Você nasceu pra mim"

           Dm           Bb9                  F
Eu ouço: "filho meu, chegou tua hora de brilhar
              C
Você nasceu pra mim
            Dm
Pra esse tempo"

Solo: Dm  Bb9  F  C (2x)

E|----------------------------------------------------------|
B|--------------------5b6r5----------------------5b6r7------|
G|-/7~~~--7/5~----------------/7~~~-7/5------------------/7-|
D|--------------5/7------------------------5/7--------------|
A|----------------------------------------------------------|
E|----------------------------------------------------------|

   Dm                   Bb9
Posso ouvir o santo som aqui
  F                      C
Anunciando sobre um grande Rei
    Dm                    Bb9
Rompendo as barreiras e grilhões
    F                        C
Eu quero ouvir o Pai a me chamar

           Dm           Bb9                  F
Eu ouço: "filho meu, chegou tua hora de brilhar
              C
Você nasceu pra mim"
           Dm          Bb9                  F
Eu ouço: "filho meu chegou tua hora de brilhar
              C
Você nasceu pra mim
           Dm7            Bb9                 F
Eu ouço: "filho meu, chegou tua hora de brilhar
              C
Você nasceu pra mim"
            Dm7         Bb9                  F
Eu ouço: "filho meu chegou tua hora de brilhar
              C
Você nasceu pra mim
            Dm7
Pra esse tempo"

Base: Dm7  Bb9  F  C (2x)

Solo 2:
E|-------10---------------10---------------10--------|
B|-10h13------------10h13------------10h13-----------|
G|---------------------------------------------------|
D|-----------12-12------------12-12------------12-12-|
A|---------------------------------------------------|
E|---------------------------------------------------|

E|-10-----10-----10-----10-----10------|
B|-13b15--13b15--13b15--13b15--13b15---|
G|-------------------------------------|
D|-------------------------------------|
A|-------------------------------------|
E|-------------------------------------|


Dm                        Bb9
Esse o hino dessa geração
                           F
Usa-nos Deus, aqui estamos

Queremos mais de Ti, Senhor
        C
O Teu amor nos alcançou     (4x)

Base: Dm Bb9 F C  (2x)

Solo 3:
E|-----10----10----10-----10----10----12b13------------|
B|-/10----10----10----10-----10----10-------13-10-8b10-|
G|-----------------------------------------------------|

E|-----10----10----10-----10----10----12b13---------------------|
B|-/10----10----10----10-----10----10-------13-15-15b17---13b15-|
G|--------------------------------------------------------------|

Interlúdio: Dm  Dm5+  Dm  Dm5+  (2x)

Dm
Ah! Jesus é Rei
Dm5+
Ah! me conquistou
Dm
Ah! me libertou
Dm5+
Pra viver uma nova história   (2x)

Dm
Ah! Jesus é Rei
Bb9
Ah! me conquistou
Dm
Ah! me libertou
Bb9
Pra viver uma nova história   (2x)

Ah! Jesus é Rei
Ah! me conquistou
Ah! me libertou
Pra viver uma nova história (2x)

Solo Base: Dm  Bb9  F  C (2x)

Solo 3:
E|-10----10----10----12----12----12----13----13----13----15------|
B|-13b15-13b15-13b15-15b17-15b17-15b17-16b18-16b18-16b18-18b20~~~|
G|---------------------------------------------------------------|

E|-10----10----10----12----12----12----13----13----13----15------|
B|-13b15-13b15-13b15-15b17-15b17-15b17-16b18-16b18-16b18-18b20~~~|
G|---------------------------------------------------------------|

E|--------|
B|-13b15--|
G|--------|

              Dm7
Vamos mudar o mundo
              Bb9
Vamos mudar o mundo
              F9
Vamos mudar o mundo
              C
Vamos mudar o mundo (4x)

       Dm7
Oh -Oooooooooooooh

----------------- Acordes -----------------
Bb9 = X 1 3 3 1 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Dm5+ = X X 0 3 3 1
Dm7 = X 5 7 5 6 5
F = 1 3 3 2 1 1
F9 = 1 3 5 2 1 1
