Fernandinho - Anjos e Querubins

[Intro]  G  C9  D  G  C9  D

E|---------------------------------------|
B|--12--------------12b13r12-------------|
G|-------12---14~--------------12----14~-|
D|---------------------------------------|
A|---------------------------------------|
E|---------------------------------------|

     G                 C9      D
Dos meus lábios sempre fluirão
   G         C9         D
Altares levantados a Ti
          Am         G/B  C9         G/B
Eu quero ter Teu cuidado, Tua proteção
         Am                    D
Aproveitar e dormir aos Teus pés
          G            C9  C9       D
Tuas Vestes cobrem o trono dos céus
        G            C9  C9     D
Tua voz é como voz de trovão

            Am                                D
Pois Tua é a glória, És a sarça que não queima
            Am        C9            D
Tua face para sempre resplandecerá

           Em          D
Coberto de glória e louvor
             Em               D
Coroado pelos anjos e querubins
             Am                       D
Adorado pelos homens, e amado nos céus
                Am            C9       D
Deus de misericórdia que é sempre fiel

                 Em          D
Cordeiro que resgata o pecador
                   Em              D
Que derramou Seu sangue em meu favor
              Am                     D
Eu venho renovar minha aliança Contigo
              Am                   D
Eu quero ofertar hoje o meu coração

( Em  C(C9)  D )
( Em  C(C9)  D )

          G            C9  C9       D
Tuas Vestes cobrem o trono dos céus
        G            C9  C9     D
Tua voz é como voz de trovão
            Am                                D
Pois Tua é a glória, És a sarça que não queima
            Am        C9            D
Tua face para sempre resplandecerá

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
