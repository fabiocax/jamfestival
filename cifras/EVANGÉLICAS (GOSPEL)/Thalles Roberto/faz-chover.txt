Thalles Roberto - Faz Chover

( G9  D/F#  G9  D/F#  G9  D/F# Em7   A9 )

E|------------------------
B|--7--5--3----3--5--2--3-
G|------------------------
D|------------------------
A|------------------------
E|------------------------

E|---------------5-2------
B|--7--5--3--333----3/2/3/2
G|------------------------
D|------------------------
A|------------------------
E|------------------------

E|------------------------
B|--7--5--3----3--5--2--3-
G|------------------------
D|------------------------
A|------------------------
E|------------------------



D9          Em7       D/F#
Deus faz chover outra vez
         A   Bb°     Bm7
Já não quero ser mais eu
     Em7          D/F#
Faz brotar, vem encher
      A9   Bb°   Bm7
Minha vida do senhor
        Em7                   A4         A9
Eu não quero mais ficar fora dessa chuva
D9          Em7       D/F#
Deus vem molhar, encharcar
       A     Bb°  Bm7
Meu deserto, meu ser
        Em7    D/F#     A9    Bb° Bm7
Sei que há uma fonte de águas vi--vas
       Em7               A4   A9
Que podem me saciar e me renovar

D9
Meu coração deseja ver
Em7
Teu braço me envolver
F9                     G9        D9
No teu olhar encontro amor...Que falta em mim

D9                     Em7
O meu desejo é conhecer o teu coração
F9
Quero ouvir o som da chuva
   G9         D9
Em meu jardim florescer

F9                   G9
Tão belo como a primavera vem
             D9
Traz vida ao meu viver

G9      D/F#
Te quero, te espero
G9     D/F#               G9
Desesperado pra ouvir tua voz
       D/F#
Me chama pra perto
   Em7              A9
Eu quero estar nos teus braços, pai

----------------- Acordes -----------------
A = X 0 2 2 2 0
A4 = X 0 2 2 3 0
A9 = X 0 2 2 0 0
Bb° = X 1 2 0 2 0
Bm7 = X 2 4 2 3 2
D/F# = 2 X 0 2 3 2
D9 = X X 0 2 3 0
Em7 = 0 2 2 0 3 0
F9 = 1 3 5 2 1 1
G9 = 3 X 0 2 0 X
