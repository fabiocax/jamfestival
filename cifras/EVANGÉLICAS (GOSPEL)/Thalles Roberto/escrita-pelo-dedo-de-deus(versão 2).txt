Thalles Roberto - Escrita Pelo Dedo de Deus

Dedilhado:
               C9
E|--------------------|
B|---------3----------|
G|----0------0--------|
D|---2---2-----2------|
A|--3------------3----|
E|--------------------|

               D9
E|--------------------|
B|---------5----------|
G|----0-------0-------|
D|---4--4-------4-----|
A|--5-------------5---|
E|--------------------|

        D7/F#   C9
E|---------5-----------|
B|-------3---3--3------|
G|-----0------0-0------|
D|----4---------2------|
A|--------------3------|
E|---------------------|


Obs: Na musica varia dedilhado e batida!

C9                                          D4/9               D4/F# C9
Jesus pode escrever a sua história de novo
C9                       D4/9      D4/F#   C9
Olha o que Ele fez comigo
C9                                D4/9                       D4/F#      C9
A chance de dar tudo errado era tudo o que eu tinha em mim
C9                            D4/9     D4/F#  C9
Mas olha o que Ele fez comigo
        C9                    D4/9       D4/F#    C9
No olhar eu carregava um pouco de morte
           C9                        D4/9       D4/F# C9
E a minha festa estava tão vazia de sorriso
         C9               D4/9          D4/F#  C9
E quando eu pensei que o rio ia secar
  C9                       D4/9    D4/F#  C9
Olha o que Ele fez comigo
    C9                       D4/9    D4/F#  C9
E vai fazer assim contigo hoje
    C9
Um dia já cansado eu disse:
  D4/9                   D4/F#     C9
"Pai não quero ser mais prisioneiro da maldade"
  C9                   D4/9     D4/F#  C9
E Ele me chamou de filho,
        C9                  D4/9              D4/F#    C9
Pedi perdão me humilhei chorei como eu chorei
    C9              D4/9    D4/F#  C9
E Ele foi fiel comigo
    C9                 D4/9       D4/F# C9
Arrancou aquela tristeza que doía,
    C9                          D4/9    D4/F#  C9
Me lavou com o Seu sangue perdoou
            C9            D4/9          D4/F#    C9
E a minha festa agora é cheia de sorriso
  C9                      D4/9    D11/F#   C9
Olha o que Ele fez comigo
   C9                          D4/9    D4/F#   C9
E vai fazer assim contigo hoje
C9                   D4/9       D4/F#  C9
Ele é meu melhor amigo
 C9                     F
Viver com Jesus é lindo


 G                 Dm7                  G
 Junte suas forças e clame a Deus
                       Dm7                               G
Ele escuta o grito do seu fraco coração
                     Dm7                                  G
Eu não tinha nada agora eu tenho vida
                                   Dm7                                     G
E uma história nova linda escrita pelo dedo de Deus
Dm7                                 G
   Escrita pelo dedo de Deus

----------------- Acordes -----------------
C9 = X 3 5 5 3 3
D11/F# = 2 X X 2 3 3
D4/9 = X 5 4 0 3 0
D4/F# = 2 X X 2 3 3
D7/F# = X X 4 5 3 5
D9 = X X 0 2 3 0
Dm7 = X 5 7 5 6 5
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
