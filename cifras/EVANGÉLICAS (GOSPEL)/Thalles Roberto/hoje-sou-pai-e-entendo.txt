Thalles Roberto - Hoje Sou Pai e Entendo

FORMA DE ACORDES:

F7M  Bb/D Ebm9/F Bb/D

E|--------------------------------------------------------------------- 
B|--1-----------------3-----------------5-----------------3------------ 
G|--2-----0--2--------3--------3--------5--------5--------3--------3--- 
D|--3--3--------------3-----7-----------5-----8-----------3-----7------ 
A|--------------------5--5--------------8--8--------------5--5--------- 
E|---------------------------------------------------------------------

Intro: FM7  Bb/D 3X


FM7           Bb/D
Pai, hoje sou pai e entendo
               FM7
O que o Senhor sente se alguém me maltrata,
              Bb/D
E o Senhor me vê chorando...
FM7                Bb/D
Pai, meu filho não faz idéia

              FM7
Que as coisas custam, tudo sai caro
               Bb/D
E dos pulos que tenho que dar...

         FM7    Bb/D
Ele só depende..    E brinca...
         FM7    Bb/D
Ele só depende...
                                FM7             Bb/D
E é assim que quero depender de Ti Papai do céu...

                 FM7                Bb/D
Por que quando corrijo, dói mais em mim que nele
           FM7                           Bb/D
E quando coloco no quarto de castigo, eu fico na sala chorando e pensando
      FM7                       FM7
O que é que posso fazer pra agradar meu filho quando o castigo acabar...
FM7                Bb/D                      FM7
Pois eu preciso do sorriso dele, mas é preciso corrigir também...
       Bb/D            FM7                                  Bb/D
Hoje eu entendo Deus, hoje eu entendo que o Senhor faz tudo pro meu bem!

               FM7                 Bb/D
Se eu que sou mau... Sei cuidar tão bem dele...
             FM7                  Bb/D
Imagina o Senhor... Como cuida de mim...


REFRÃO

 F             Cm7                             Bb7M
Deus, eu não daria meu filho pra morrer por ninguém
         Bb/D                     Am7
Só de pensar me dá desespero, mas o Senhor...
     Cm7                           Bb7M       Bb/D                       Am7
Entregou seu Filho pra morrer por mim... Que Amor foi esse que o Senhor sentiu?
      Bb/D         (C/F           Bb/D
Foi demais... Hoje eu sou pai e entendo...


REPETE TUDO...

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Bb/D = X 5 X 3 6 6
Bb7M = X 1 3 2 3 1
C/F = X 8 10 9 8 8
Cm7 = X 3 5 3 4 3
F = 1 3 3 2 1 1
F7M = 1 X 2 2 1 X
