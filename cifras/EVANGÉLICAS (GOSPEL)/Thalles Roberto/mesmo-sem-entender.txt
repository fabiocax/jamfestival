Thalles Roberto - Mesmo Sem Entender

          A7M(9)             G#m7
Mesmo sem entender, mesmo sem entender
       A7M(9)             G#m7
Eu confio em ti, mesmo sem entender
             F#m7(11)
Eu sei que é o        melhor pra mim
B4           A7M(9)
   Mesmo sem entender

 A7M(9)                               G#m7
Deus,   mesmo que eu não consiga entender,  e queira tudo do meu jeito
     A7M(9)                               G#m7
Eu até     choro e às vezes ate chego a dizer
                                 F#m7(11)
Por que é que tem que ser tão difícil     pra mim?
                          B4
Parece que é difícil só pra  mim

    A7M(9)                                         G#m7
Eu sei,    seus pensamentos são mais altos que os meus
                                 A7M(9)
O teu caminho é melhor do que o meu

                                G#m7
Tua visão vai além do que eu vejo
                                     F#m7(11)
O Senhor sabe exatamente o que é melhor       pra mim
                                    B4
E mesmo que eu não entenda o seu caminho eu confio

   A7M(9)                           G#m7
E Deus   porque sou tão pequenino assim
                                 F#m7(11)                           B4
Vou ficar quietinho aqui no seu colo     esperando o tempo certo de tudo
                       A7M(9)
Porque eu sei que vais        cuidar de mim
               G#m7
E o seu melhor      está por vir
               F#m7(11)                B4
Eu sei que é o          melhor pra mim
          A7M(9)
Mesmo sem entender

A7M(9)             G#m7
Mesmo sem entender, mesmo sem entender
       A7M(9)             G#m7
Eu confio em ti, mesmo sem entender
             F#m7(11)
Eu sei que é o        melhor pra mim
B4           A7M(9)
   Mesmo sem entender

 A7M(9)                               G#m7
Deus,   mesmo que eu não consiga entender,  e queira tudo do meu jeito
     A7M(9)                               G#m7
Eu até     choro e às vezes ate chego a dizer
                                 F#m7(11)
Por que é que tem que ser tão difícil     pra mim?
                          B4
Parece que é difícil só pra  mim

    A7M(9)                                         G#m7
Eu sei,    seus pensamentos são mais altos que os meus
                                 A7M(9)
O teu caminho é melhor do que o meu
                                G#m7
Tua visão vai além do que eu vejo
                                     F#m7(11)
O Senhor sabe exatamente o que é melhor       pra mim
                                    B4
E mesmo que eu não entenda o seu caminho eu confio

   A7M(9)                           G#m7
E Deus   porque sou tão pequenino assim
                                 F#m7(11)                           B4
Vou ficar quietinho aqui no seu colo     esperando o tempo certo de tudo
                       A7M(9)
Porque eu sei que vais        cuidar de mim
               G#m7
E o seu melhor      está por vir
               F#m7(11)                B4
Eu sei que é o          melhor pra mim
          A7M(9)
Mesmo sem entender

          A7M(9)             G#m7
Mesmo sem entender, mesmo sem entender
       A7M(9)             G#m7
Eu confio em ti, mesmo sem entender
             F#m7(11)
Eu sei que é o        melhor pra mim
B4           A7M(9)   E9
   Mesmo sem entender

----------------- Acordes -----------------
A7M(9) = X 0 2 1 0 0
B4 = X 2 4 4 5 2
E9 = 0 2 4 1 0 0
F#m7(11) = 2 X 2 2 0 X
G#m7 = 4 X 4 4 4 X
