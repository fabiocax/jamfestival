Thalles Roberto - Não Deixe o Pecado Entrar

(riff 1)
E|------------------------------------------------------|
B|------------------------------------------------------|
G|------------------------------------------------------|
D|--------------------------------3-----3---------------|
A|---3-4-5--3-4-5-5-5--3-4-5---------2----2-------------|
E|------------------------------------------------------|

(riff 1)
1,2,3,4,5,6,7,8,9,10 conto e
Não deixo o pecado entrar,

(C D5/A F) (fica repetindo essa sequencia)
Porque eu sei, que se ele entrar,
Minha vida vira de pernas pro ar,
O diabo é sujo mas em Cristo,
Eu derrubo é muita pressão,
Meu caminho certo decidi não posso voltar,

Bb7+
Foi Jesus me chamou,

A7               Dm7
E brilhou na escuridão,
G7            Bb7+
Olha bem para mim, e
 A7            Dm7           G7
Observa o que Deus faz,
    Bb7+             A7
Ele me aceitou quando eu tava,
     Dm7         G7           Bb7+
Só o pó, e hoje eu sou cheio de Deus,
A7
Do amor de Deus.

Introdução

10,9,8,7,6,5,4,3,2,1, Vai,vai,vai,vai,vai,vai (C D5/A F)

(C D5/A F) (fica repetindo essa sequencia)
Se o pecado entra em minha vida,
Vira tudo de pernas pro ar,
O diabo é sujo mas em Cristo,
Eu derrubo é muita pressão,
Meu caminho certo decidi não posso voltar,


Bb7+
Foi Jesus me chamou,
A7               Dm7
E brilhou na escuridão,
G7            Bb7+
Olha bem para mim, e
 A7            Dm7           G7
Observa o que Deus faz,
    Bb7+             A7
Ele me aceitou quando eu tava,
     Dm7         G7           Bb7+
Só o pó, e hoje eu sou cheio de Deus,
A7
Do amor de Deus.

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bb7+ = X 1 3 2 3 1
C = X 3 2 0 1 0
D5/A = 5 5 7 X X X
Dm7 = X 5 7 5 6 5
F = 1 3 3 2 1 1
G7 = 3 5 3 4 3 3
