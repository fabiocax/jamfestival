Thalles Roberto - Ele É o Amor

Intro: Am  Dm  Gm  C

Riff: Refrão
E|--------------------------------------------------|
B|--------------------------------------------------|
G|--------------------------------------------------|
D|--------------------------------------------------|
A|----------3-4-5----------1-2-3--------------------|
E|-----5-5------------3-3---------------------------|

Am     Dm          Gm
Tudo bem se voce me disser
      C
Que tem a sua fé,
Am     Dm          Gm       C
Que já tem a sua crença

Am     Dm          Gm
Só me escuta e
           C
Me deixa falar

Am     Dm          Gm               C
Qual é a tradução do nome Jesus Cristo

(Riff 1 Durante todo o refrão)
        Am  Dm Gm C                Am        Dm  Gm  C
Elé é o amor      forte mais que a luz do sol
       Am   Dm Gm C         Am          Dm  Gm  C
Ele me amou,      trovejou dentro de mim
        Am  Dm Gm C         Am         Dm  Gm  C
Ele me salvou     e dominou meu coração
           Am Dm Gm C                Am Dm Gm C
A minha vida agora, é Deus e nada mais

 Intro: Am  Dm  Gm  C
D|-----------------------------------------|
A|-----5-5-3-5-5-3-5-3-3-5-3-5-5-3---------|
E|-----------------------------------------|

( intercala entre D e C )
Batidão, coração, decisão
Mais que religião minha luz, o alivio
Fé em Deus, confiar, caminhar
Na certeza que Ele é pai, e eu sou filho

Riff 1 Durante todo o refrão

(Rap)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
Gm = 3 5 5 3 3 3
