Thalles Roberto - Ele É Contigo

Intro:
E|-7---------7----------|
B|-7---------7----------|
G|-7-(10x)---7h9--------|
D|-7---------7----7/9---|
A|-7---------7----5/7---|
E|-X--------------------|

D  E    A
Ele é contigo
D E    A
Onde você estiver
D  E    A
Ele é Abrigo, seu Amigo
    Bm        C#m
Deus provedor,   pode confiar,   pode confiar
           D E   A
Lara,la la la...

D  E    A
    Deus poderoso...

D  E   A
  O Deus que tudo pode fazer
D     E         A
e o controle do mundo (de tudo) está

    Bm                   C#m
Nas mãos desse Deus, nas mãos de Deus
    Bm
Nas mãos de Deus
D  E    A
  É maravilhoso...
D    E       A
  Saber que não há impossível pra Deus
D E   A      Bm              C#m
  Então, não duvida, confia em Deus, confia em Deus
   Bm
Confia em Deus

Final:
           D  E    A
Lara,la la la...          (3x)
          D  E   A
Lara,la la la...

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
