Thalles Roberto - A Alegria Está No Coração

 A    G D
 Aleluia   Aleluia Aleluia Aleluia

A9                              D9                  A9  G D
 A alegria está no coração, de quem já conhece a Jesus,
A9                                   B7              E
 A verdadeira paz só tem aquele, que já conhece a Jesus.
A9                    A7         D9              F
 Um sentimento mais precioso que vem do nosso Senhor
      A9                    E                       A9    G D
 É o amor           que só tem, quem já conhece a Jesus. (2x)

         A9                  A7
 Posso pisar numa tropa e saltar as muralhas
    D9       F
 aleluia, aleluia. (2x)
   A9                A7
 Cristo é a rocha da minha salvação,
     D9              B
 com Ele não ha mais condenação.
       A9                    E
 Posso pisar numa tropa e saltar as muralhas,

    D9       F
 Aleluia, aleluia. (2x)

    A9       D9  A9       A9       B7      E7
 Aleluia, aleluia        Aleluia, aleluia
A9                    A7         D9              F
 Um sentimento mais precioso que vem do nosso Senhor
      A9                   E                        A9
 É o amor          que só tem, quem já conhece a Jesus. (2x)
    A               G  E7  A
 lara ra ra rarar   la la la

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
A9 = X 0 2 2 0 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
D = X X 0 2 3 2
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
