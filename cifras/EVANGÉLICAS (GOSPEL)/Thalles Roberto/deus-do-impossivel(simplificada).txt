Thalles Roberto - Deus do Impossível

  G
Existem certas situações
Em
Difíceis de suportar
Am                                C
Olhamos pro lado e não
C                              D
Ninguém pode ajudar
G
Tudo parece impossível
Em
O mar quer nos afogar
As ondas vêm e querem derrubar
A nossa fé
Am                                  C
E nos levar ao desespero
                D
Ao desespero
Em
Pensamos que o nosso
Problema é grande

                                                 Am
E esquecemos da grandeza de Deus
Em
Desanimados de clamar pensamos
                        C
Que Ele não ouve
Em                              Am
E então, dá uma vontade de chorar
Em                                       C
Eu sei, não dá mais pra agüentar
Am                               D   C   D
Eu olho pro alto, vem o socorro
C   Am                  D
Meu Deus pode fazer Muito Mais
G                              Em
Ele te ouve, Cristo te ouve
C                             Am
Escuta as suas orações
D
Ele vai te atender
G
Deus do impossível
Em
Tudo é possível
Basta crer
C
Que Deus pode fazer
Am                    D
Infinitamente mais
G
Ele te ouve
Em                   C
Meu Deus pode fazer
Am                      D
Infinitamente mais
C
Muito mais

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
