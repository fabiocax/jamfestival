Thalles Roberto - Esse É Que É Deus

Intro: A7 D4/A Am11 A7 A7 D4/A Am11 D/A A7 D4/A Am11 A7 A7 D4/A Am11 E/B

A7
Esse é que é Deus
A7
Esse é que é Deus
D/A
Esse é que é Deus
A7
Esse é que é Deus

 E/B
O Que os profetas de baal
         D/A
Não poderão fazer
E/B
O Deus de Elias
E/B
Fez fogo do céu desse

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am11 = X 0 0 2 1 0
D/A = X 0 X 2 3 2
D4/A = X 0 0 2 3 3
E/B = X 2 2 1 0 0
