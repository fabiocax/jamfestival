Adhemar de Campos - Satisfaz / O Senhor É a Luz (Pot-Pourri)

INTRO: C7 F7 G7 F7 C7

C7
SÓ JESUS, A VIDA SATISFAZ, SÓ JESUS, A VIDA SATISFAZ

F7                        G7          F7        C7
SÓ JESUS, A VIDA SATISFAZ, SÓ JESUS, A VIDA SATISFAZ

C7
SÓ JESUS, A VIDA SATISFAZ, SÓ JESUS, A VIDA SATISFAZ

F7                        G7          F7        C7
SÓ JESUS, A VIDA SATISFAZ, SÓ JESUS, A VIDA SATISFAZ
(solo com a base: C7 F7 G7 F7 C7 B) 2x

           C7                          C7
NÃO TEMO O MAL QUE POSSA VIR, FORÇA NÃO HÁ PRA DESTRUIR

          C7        C7           C7
O AMOR, A FÉ NO CORAÇÃO QUE DO SENHOR RECEBI

       F7                   C7               G7              F7           C7
POIS É ELE QUE GUIA OS MEUS PASSOS A ELE ME ENTREGUEI E NELE CONFIAREI, Ô, Ô

           C7                          C7
NÃO TEMO O MAL QUE POSSA VIR, FORÇA NÃO HÁ PRA DESTRUIR

          C7        C7           C7
O AMOR, A FÉ NO CORAÇÃO QUE DO SENHOR RECEBI

        F7                  C7             G7                F7           C7
POIS É ELE QUE GUIA OS MEUS PASSOS A ELE ME ENTREGUEI E NELE CONFIAREI, Ô, Ô

C7       C7       F7       C7
ALELUIA, ALELUIA, ALELUIA, ALELUIA

      G7                  F7            C7
A ELE ME ENTREGUEI E NELE CONFIAREI, Ô, Ô

C7       C7       F7       C7
ALELUIA, ALELUIA, ALELUIA, ALELUIA

      G7                  F7           C7
A ELE ME ENTREGUEI E NELE CONFIAREI, Ô, Ô

----------------- Acordes -----------------
C7 = X 3 2 3 1 X
F7 = 1 3 1 2 1 1
G7 = 3 5 3 4 3 3
