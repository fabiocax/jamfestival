Adhemar de Campos - Fonte de Água Viva

 Em
Aquele que tem sede e busca
  D
Beber da água que Cristo dá
  C
Terá dentro de si uma fonte
            B4     Bm5-/7
Que jamais acabará   (2X)
 
              Em
Fonte de água viva é o Senhor
             D
Minha água é o Consolador
            C 
Espírito de vida e louvor
        B4   Bm5-/7
Preciso dele.
          Em
Tu és o Espírito de amor
           D
O meu guia e Consolador
            C         Am
Enche-me do fogo do Senhor
        B4    Bm5-/7  
Preciso de ti

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B4 = X 2 4 4 5 2
Bm5-/7 = X 2 3 2 3 X
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
