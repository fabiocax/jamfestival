Adhemar de Campos - Teu Amor

  G/B  A9      F#m5+
Viver assim te esperando
  G/B  A9                       Bm/A  Bm/E
           Tua volta tanto anseio
  G/B    A9
E quando o medo
  F#m5+
Vem junto com a incerteza
  A9      A4(7/9)
Te encontro
 G/B       A9
Não posso me esquecer
 F#m5+
Do sacrificio
 G/B A9                 Bm/A   Bm/E
         Teu naquela cruz
 G/B   A9       F#m5+                A9     A4(7/9)
E é por isso que hoje eu tenho a vida eterna

 D        D/C
Teu amor me conquistou
Em7
As tuas marcas nas mãos
A#             C#
Marcaram minha vida
 D                D/C
Teu suor de dor me quebrantou
Em7         A#                 C#
Agora tens senhor toda minha vida
D         D/C   Em7   A#
Meu redentor

D
Meu redentor, meu salvador
D/C
Ao teu amor não há nada igual
Em7
Meu redentor, meu salvador
Gm7
Meu grande amor

----------------- Acordes -----------------
A# = X 1 3 3 3 1
A4(7/9) = 5 X 5 4 3 X
A9 = X 0 2 2 0 0
Bm/A = X 0 4 4 3 X
Bm/E = X X 2 4 3 2
C# = X 4 6 6 6 4
D = X X 0 2 3 2
D/C = X 3 X 2 3 2
Em7 = 0 2 2 0 3 0
F#m5+ = 2 5 4 2 3 2
G/B = X 2 0 0 3 3
Gm7 = 3 X 3 3 3 X
