Adhemar de Campos - Tributo a Yehovah

[Intro] E  E7/G#  A  D7 
        A7  E/B  B7

E  E7/G#     A     D7 A7  E           B7
  Ie, Ie, Iehovah,         Ie, Ie, Iehovah
E  E7/G#     A     E/B   B     A
  Ie, Ie, Iehovah,  Ie, Ie, Iehovah

( Bm  E  Bm  E )
( Bm  E  Bm  E  B7 )

         E                 A7
Eu sou grato por tudo que tenho
     E                 B7
O tesouro maior desse mundo
        E           E7     A7   D7
Me foi dado como herança eterna
        E/B         B7         A   ( E  B7 )
Maior prova de um amor tao profundo
       E                  A7
Tenho vida, alegria todo tempo
        E                       B7
Tenho amigos, familia, muitos irmãos
       E                   A   D7
Foi Jesus, meu amigo verdadeiro
         E/B        B7         A ( E  B7 )
Que fez tudo ao me dar a salvação
      E                     A7
Louvarei ao Senhor em todo tempo
        E                  B7
Seu louvor estará continuamente
         E           E7         A  D7
Em meus lábios e também no coração
        E/B        B7              A7 ( E  B7 )
Jesus Cristo será sempre minha canção

E    E7    A7
Ie, Ie, Iehovah
E          B7
Ie, Ie, Iehovah
E   E7    A7   D7
Ie Ie, Iehovah
E/B  B7    A7   B7
Ie, Ie, Iehovah

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
E/B = X 2 2 1 0 0
E7 = 0 2 2 1 3 0
E7/G# = 4 X 2 4 3 X
