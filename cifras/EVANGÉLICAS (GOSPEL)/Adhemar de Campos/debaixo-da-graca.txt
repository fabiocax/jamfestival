Adhemar de Campos - Debaixo da Graça

                E9                   F#/E
Porque estás debaixo da graça
   F#m/E              E9   F#m  G#m7
E não debaixo da lei
        A            A#°           G#m    C#m
O pecado não mais terá domínio
         F#m B7      Bm  E7
Sobre vós, sobre vós.

             A        G#7   G#/C  C#m   C#m(#5)
Considerai-vos mortos ao pecado
   F#m   B7   E9
E vivos para Deus.

A   A/B  E9              B/D#
Te exaltamos, te exaltamos
           A/C#   B7     E9
Te exaltamos, oh Senhor.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A#° = X 1 2 0 2 0
A/B = X 2 2 2 2 X
A/C# = X 4 X 2 5 5
B/D# = X 6 X 4 7 7
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
C#m(#5) = X 4 2 2 2 5
E7 = 0 2 2 1 3 0
E9 = 0 2 4 1 0 0
F#/E = X X 2 3 2 2
F#m = 2 4 4 2 2 2
F#m/E = X X 2 2 2 2
G#/C = X 3 X 1 4 4
G#7 = 4 6 4 5 4 4
G#m = 4 6 6 4 4 4
G#m7 = 4 X 4 4 4 X
