Adhemar de Campos - Verbo Divino

     G     D/F#    Em  Em/D  C    D      G  C            G     D/F#
É JESUS O VERBO DIVINO    ENCARNAÇÃO DE DEUS  O PRÓPRIO DEUS QUE VEIO 
    Em            C   D/C           Bm Em          Am     D
AO MUNDO VEIO AO MUNDO   CRIADO POR ELE  TRAZENDO VIDA E LUZ A TODO 
 Dm  G7         C  D/C          Bm Em          Am     D          G
HOMEM  VEIO AO MUNDO CRIADO POR ELE  TRAZENDO VIDA E LUZ A TODO HOMEM 
 
D/F#      G   D/F#    Em             C    D         G C      G    
  NÓS TE RECEBEMOS JESUS POIS ENTRE NÓS VIESTE HABITAR OH REVELA-NOS  
D/F#  Em             C  D/C         Bm Em          Am   D     Dm  G7
TUA GLÓRIA  POIS ÉS FILHO  DO DEUS VIVO  CHEIO DE GRAÇA E VERDADE 
            C  D/C        Bm Em            Am    D      G
E ASSIM AO MUNDO  ANUNCIARE_MOS  POR NOSSA VIDA TUA VERDADE

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/C = X 3 X 2 3 2
D/F# = 2 X 0 2 3 2
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
Em/D = X X 0 4 5 3
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
