Adhemar de Campos - Magnifiquemos

D9               G/B      D9                    G/B 
Magnifiquemos ao Senhor / Ao Rei que é digno de louvor 
   A/C#     Bm          G/B   A4    E 
Excelso, supremo e mui digno de louvor 
   A/C#     Bm          G/B   A4    A7 
Excelso, supremo e mui digno de louvor 
 
    D9         G/B    Em              A4   A7 
Louvamos ao Senhor / Pois Seu nome é Santo 
    D9         G/B    Em              A4 
Louvamos ao Senhor / Pois Seu nome é Santo 
 
INTRO PARA PRÓXIMA MÚSICA OU FINAL: G  Em  A7 (D9)

----------------- Acordes -----------------
A/C# = X 4 X 2 5 5
A4 = X 0 2 2 3 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
G/B = X 2 0 0 3 3
