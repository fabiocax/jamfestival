Adhemar de Campos - Mistério da Existência

C7M/9                         Am7/9
Refletir no mistério da existência
Dm7/9                    Eb7M/9  G4/7(9)  G4/7(b9)
Na razão do homem estar aqui
C7M/9                   Am7/9
Perguntar se existe coerência
Dm7/9             D/F#          G4/7(9)   C4/7(9) 
Nas coisas que acontecem no viver
F7M                     C/E     Am7/9
Vai sentir no coração ausência
                   Dm7/9 G4/7(9)         C7M(9)    C4/7(9)          
Vai sentir na consciência que é preciso mudanças
F7M                         C(9)/G  G#º  
Vai querer encontrar uma resposta
                   Dm7/9 G4/7(9)     G#4/7(9)
Vai querer real proposta e a razão do viver
C#7M(9)                    A#m7(9)
Se pensar vai tomar consciência
                   D#m7(9)            E7M(9) G#4/7(9)  G4/7(b9)                                   
Vai saber que a existência tem explicação
C#7M(9)                   A#m7(9)
Vai saber que existe coerência
                 D#m7(9) D#/G   G#4/7(9) C#4/7(9)
E que nada é coincidência no viver

F#7M                    C#/F    A#m7(9)
Cristo é a razão da existência

            D#m7(9)  G#4/7(9)   C#7M(9)  C#4/7(9)
A real experiência que se deve ter
F#7M                          C#9/G#  Aº
Vai encontrar finalmente a resposta
                    D#m7/9 G#4/7(9)      C#7M(9)                 
Cristo é a única proposta e a razão do viver

----------------- Acordes -----------------
A#m7(9) = 6 4 6 5 X X
Am7/9 = X X 7 5 8 7
Aº = 5 X 4 5 4 X
C#/F = X 8 X 6 9 9
C#4/7(9) = X 4 4 4 4 4
C#7M(9) = X 4 3 5 4 X
C#9/G# = 4 X 6 6 4 4
C(9)/G = 3 X 5 5 3 3
C/E = 0 3 2 0 1 0
C4/7(9) = X 3 3 3 3 3
C7M(9) = X 3 2 4 3 X
C7M/9 = X 3 2 4 3 X
D#/G = 3 X 1 3 4 X
D#m7(9) = X 6 4 6 6 X
D#m7/9 = X 6 4 6 6 X
D/F# = 2 X 0 2 3 2
Dm7/9 = X 5 3 5 5 X
E7M(9) = X 7 6 8 7 X
Eb7M/9 = X 6 5 7 6 X
F#7M = 2 X 3 3 2 X
F7M = 1 X 2 2 1 X
G#4/7(9) = 4 X 4 3 2 X
G#º = 4 X 3 4 3 X
G4/7(9) = 3 X 3 2 1 X
G4/7(b9) = 3 X 3 1 1 X
