Adhemar de Campos - Vida Melhor

(intro) A  E/G#  D/F#  Dm/F  A  F#m  A/E  B/D#  E

     A                  E/G#
   Existe um caminho na vida,
                 D/F#     Dm/F     A
   Uma estrada perdida sem nada levar.
       F#m                  A/E
   Ilusões que nos cercam e mostram,
               B/D#                E
   Um tempo de vida pra se aproveitar.
         A               E/G#
   Entre tudo que tive e sempre busquei
      D/F#     Dm/F       A
   Em nada pude me encontrar;
          F#m                A/E
   Tantos planos perdidos no tempo,
                  B/D#                 E   
   Há sempre um momento de parar e pensar.

    Bb                  F/A
E andando no tempo e na vida sem paz,
     D#/G       D#m/F#       Bb
Procurando algo pra preencher.
   Gm                   Bb/F
E lutando com tudo sem nada encontrar,
     C/E               F
Onde forças pra continuar.
            Bb               F/A
'Té que um dia encontrei uma vida melhor,
     D#/G       D#m/F#    Bb
Algo novo que me aconteceu.
      Gm               Bb/F
E não troco por nada no mundo não,
        C/E                F
Hoje eu quero é viver para Deus.

 Bb             F/A       D#/G   D#m/F#    Bb
  Com Jesus eu posso sim, ter uma vida real,
página 1
       Gm         Bb/F C/E               F
  Ele veio na hora certa, me fez novo afinal.
 Bb             F/A       D#/G    D#m/F#      Bb
  Com Jesus eu canto sim, tenho uma vida real,

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/E = 0 X 2 2 2 0
B/D# = X 6 X 4 7 7
Bb = X 1 3 3 3 1
Bb/F = X 8 8 7 6 X
C/E = 0 3 2 0 1 0
D#/G = 3 X 1 3 4 X
D#m/F# = 2 X 1 3 4 X
D/F# = 2 X 0 2 3 2
Dm/F = X X 3 2 3 1
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
F/A = 5 X 3 5 6 X
Gm = 3 5 5 3 3 3
