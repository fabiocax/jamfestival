Adhemar de Campos - Louvemos Ao Senhor

[Intro] Gm  C  Gm  C  

Gm        C
Louvemos ao Senhor  
Gm        C
Louvemos ao Senhor  
 F      Em      Dm
Adoremos o seu santo monte  
 D/C    Bb
Nosso amado pai  
  B  Bbm  C
Seu nome é  Santo  

 F  Am     Bb  C           F   Am  Bb  C
Louvemos ao Senhor, pois seu nome é Santo  
 F  Am     Bb  C           F   Am  Bb  C
Louvemos ao Senhor, pois seu nome é Santo  

 F  Am       Bb  C
Magnifiquemos ao Senhor  
  F      Am       Bb  C
Ao rei que é digno de Louvor  
 F     Am       Bb  B  Bbm   C
Excelso, Supremo, e mui digno de Louvor  
 F     Am       Bb  B  Bbm   C
Excelso, Supremo, e mui digno de Louvor  

 F   D#    Bb     C   F  C
Hosana, Hosana, Hosana ao nosso Rei  
 F   D#    Bb     C   F  C
Hosana, Hosana, Hosana ao nosso Rei  

     F            Am
Cristo é a nossa vida, o motivo do Louvor  
 Bb   B  Bbm   C
Em nosso novo coração  
    F
Pois morreu a nossa morte  
   Am
Pra vivermos sua vida  
   Bb    B   Bbm   C
Nos trouxe grande salvação 

 G    F    C     D   G  D
Hosana, Hosana, Hosana ao nosso Rei  
 G    F    C     D   G  D
Hosana, Hosana, Hosana ao nosso Rei

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
Bb = X 1 3 3 3 1
Bbm = X 1 3 3 2 1
C = X 3 2 0 1 0
D = X X 0 2 3 2
D# = X 6 5 3 4 3
D/C = X 3 X 2 3 2
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
