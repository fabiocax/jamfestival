Anderson Freire - Não É o Fim

         G A
Não é o fim
              D9                       G   A                 D9
Tem jeito para o nosso amor, Fora de mim, Admitir que tudo terminou
      G         A           Bm              G           D9
Quem sabe a gente agora, Acerta pra valer, Tudo que no início

       A        A            G A                   D9
Não conseguimos fazer, Vou assumir, Sou réu confesso por amor
          G A             Bm                    G         A
Quebro em mim. O orgulho que nos afundou. Pro abismo do oceano
             D9           G                   D9
Mas ainda em mim restou. O último suspiro pra gritar
           A
Confesso amor.

 Refrão 2x:
    D9                      A
Que fui uma criança sem notar
Bm                                           G
Fiz a nossa historia um momento pra brincar. Cansei de ser menino descobri que era

você

G                                         D9
Minha estrutura. É a razão do meu viver. Preciso te mostrar que eu cresci
A                                            Bm
Estou no fundo de um abismo sem poder sair. Leva minha vida para a superfície agora
G                    A                  D9
E que Deus abençoe. Essa nossa história

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D9 = X X 0 2 3 0
G = 3 2 0 0 0 3
