Anderson Freire - Sete Trombetas


 Em                             C
  Uma mistura de Sangue com fogo
                                   Am
  A terça parte deste mundo queimará
                                                D9
  Eu quero estar distante, eu quero estar no céu
                      B                  Em
  Quando o anjo a primeira trombeta tocar
                                  C
  Um grande meteoro vai cair no mar
                                      Am
  e  na segunda vez que a trombeta tocar
                                   D9
  Levando a terça parte a destruição
                      B                Em
  Não quero estar presente nesta ocasião
                                     C
  Na terceira vez que a trombeta tocar
                               Am
  Uma grande estrela do céu cairá

                               D9
  Destruindo as fontes das águas
                 B                Em
  Envenenados muitos homens morrerão

                                 C
  A quarta vez o sol perderá a luz
                                     Am
  A lua não terá aquele brilho que seduz
                                        D9
  O dia vai ser noite, a noite vai ser dia
                      B                Em
  Eu quero estar na glória junto de Jesus...
            C         Am  D9           B      Em   D9  B
  Breve vira! Breve vira!  O meu   Jesus voltará

Refrão:

Em                            C                            Am
  Eu quero estar além de tudo, eu não pertenço a este mundo
                         D9       B                  Em
  Eu vou guardar a minha fé, pra não perder minha coroa,
                              C                           Am
  Eu quero  andar em santidade, pra ver a face do meu noivo
                       D9             B     Em
  E adorar na excelência do todo poderooooooso...


( B  )

Em                                        C
  E quando pela quinta vez a trombeta tocar
                                  Am
  A porta do inferno então se abrirá
                                  D9
  O mau será liberto para atormentar
                     B            Em
  Aqueles que não foram fiéis a Jeová
                                      C
  Os homens vão querer a morte se entregar
                             Am
  E certamente deles ela fugirá
                                  D9
  Desesperadamente vão gritar perdão
                   B            Em
  Durante cinco meses assim ficarão
                                       C
  E quando pela sexta vez a trombeta tocar
                                  Am
  milhares de anjos do inferno virão
                                 D9
  São eles cavaleiros da destruição,
                    B           Em
  Matando a terça parte da população.

Em                                      C
E quando o  anjo tocar a última trombeta
                                      Am
Haverá no céu grandes vozes que dirão:
                               D9                        B          Em
- Os reinos do mundo vieram a ser do nosso senhor e salvador Jesus Cristo
Em                             C
E ele reinará para todo sempre!
                                           Am
E os vinte e quatro anciãos que estão assentados em seus tronos
                        D9                       B                Em
prostrarão sobre seus rostos e  o adorarão em espírito e em verdade...oh! Aleluia!


Em                                            C
  Não quero estar aqui quando as trombetas tocarem
                                              Am
  Não quero estar aqui eu quero estar com Jeová
                                      D9
  Eu vou me preparar, eu vou me preparar
                  B                    Em
  Aqui não vou ficar, aqui não vou ficaaaaaaaaar!

Refrão

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
C = X 3 2 0 1 0
D9 = X X 0 2 3 0
Em = 0 2 2 0 0 0
