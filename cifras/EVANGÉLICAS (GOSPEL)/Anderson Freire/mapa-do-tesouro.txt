Anderson Freire - Mapa do Tesouro

[Intro] D#7M  F  G4  Gm

Gm         D#7M
Ninguém é super homem,Deus não te fez
Cm7               Dm
De aço, não leve em teus ombros, além
          Gm
Do que Ele te deixou
       D#7M            Cm7
O mundo é natural,a carne é mortal
    D4         D7     D#7M/G
É a realidade pra chegar num ideal
       F/A           D#7M/G
O inimigo luta pra você deixar a cruz
       F         Gm   D#7M/G
Sem ela ele sabe que você negou Jesus
       F/A          D#7M/G
O peso da tribulação não dá pra comparar
       F/A             G#
Com o peso da Glória que o céu vai revelar


( D#/G  F )

           Gm
Não despreze a sua cruz,ela tem sua
 Cm7     Cm7/A#           F/A
Medida Deus um dia calculou
        A#                A#/D
Ele conhece a força e a fraqueza
  D#7M/G            Cm
De um pecador,o calvário declarou
         Dm
Com o sangue de Jesus,para o mundo
  D#7M/G  F/A
Seu valor
           Gm
Não despreze a sua cruz,a renuncia
  Cm7       Cm7/A#           F/A
É o braço de equilíbrio pra você
        A#7M       A#/D
Hoje você não entende mas um dia
    D#7M             Cm
Irá saber,que as pegadas de Jesus
          D  D7
São o mapa do tesouro Deus tem recompensa
   D#7M/G F/A
Pra você

Passagem de tom: F#m E/G# A/C#

           B            A7M
O inimigo luta pra você deixar a cruz
      B        Gbm7(11)   A7M
Sem ela ele sabe que você negou Jesus
       G#m
O peso da tribulação não dá pra
A7M            B
Comparar,com o peso da Glória que o
       D  A/C# B5
Céu vai revelar
          C#m
Não despreze a sua cruz,ela tem sua
 F#m          F#m/E       B/D#
Medida Deus um dia calculou,
        E       E/G#
Ele conhece a força e a fraqueza
  A7M      E/G#          F#m
De um pecador,o calvário declarou
        G#m
Com o sangue de Jesus,para o mundo
   A7M   B
Seu valor
          C#m
Não despreze a sua cruz,a renúncia é
 F#m      F#m/E            B/D#
O braço de equilíbrio pra você
         E7M       E/G#
Hoje você não entende mas um dia
   A7M            E/G# F#m
Irá saber,que as pegadas de Jesus
        G#
São o mapa do tesouro,Deus tem recompensa
   A7M  B
Pra você
 A7M                   B
A cruz é a responsabilidade do fiel
 A/C#               B/D#
É o domínio sobre a carne a ponte

Para o céu
C#m        B     A
Diga sim pra Deus, não negue o que é
G#m       D        A/C#      B5
Seu,a trajetória certa Ele escreveu

----------------- Acordes -----------------
A = X 0 2 2 2 0
A# = X 1 3 3 3 1
A#/D = X 5 X 3 6 6
A#7M = X 1 3 2 3 1
A/C# = X 4 X 2 5 5
A7M = X 0 2 1 2 0
B = X 2 4 4 4 2
B/D# = X 6 X 4 7 7
B5 = X 2 4 4 X X
C#m = X 4 6 6 5 4
Cm = X 3 5 5 4 3
Cm7 = X 3 5 3 4 3
Cm7/A# = 6 3 5 3 4 3
D = X X 0 2 3 2
D#/G = 3 X 1 3 4 X
D#7M = X X 1 3 3 3
D#7M/G = 3 X 1 3 3 X
D4 = X X 0 2 3 3
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E7M = X X 2 4 4 4
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
F#m/E = X X 2 2 2 2
F/A = 5 X 3 5 6 X
G# = 4 3 1 1 1 4
G#m = 4 6 6 4 4 4
G4 = 3 5 5 5 3 3
Gbm7(11) = 2 X 2 2 0 X
Gm = 3 5 5 3 3 3
