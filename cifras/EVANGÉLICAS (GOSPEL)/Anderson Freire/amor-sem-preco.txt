﻿Anderson Freire - Amor Sem Preço

Capo Casa 3

INTRO: A9 (no violão, seria melhor uma dedilhada daí você mantém até a entrada da letra)


A9                   E9
   Se agente quer colher tem que plantar
A9                  E9
  Até mesmo para sorrir e para chorar
E7                        A9        G#m
   Pra viver um grande amor tem que deixar
F#m                       B9    A9   E9
De lado os sonhos que não cabem no peito


"non non non" (mantém em ' A9 ' e ' E ' de preferência em ' E9 ')


A9                   E9
  O meu lucro é perder pra te ganhar
A9                   E
    Não foi hora pra poder te encontrar
E7                        A9   E9
   Você é como uma dádiva do céu

Bb             A           E
Jóia lapidada pelas mãos de Deus


E9                    A9
 Pra ganhar tem que ceder
B9             E9
Não quero te perder
                  A9
Abro mão dos sonhos meus
B9             C9
Pra ficar com você
  G#m           A9
Não vou me arrepender
  E               F#m
Estou plantando pra colher
A9        B9      E9
Um amor sem preço                    (Repete 2x)

(REPETE A PARTIR DA 2ª ESTROFE)

Obrigado! >

----------------- Acordes -----------------
Capotraste na 3ª casa
A*  = X 0 2 2 2 0 - (*C na forma de A)
A9*  = X 0 2 2 0 0 - (*C9 na forma de A9)
B9*  = X 2 4 4 2 2 - (*D9 na forma de B9)
Bb*  = X 1 3 3 3 1 - (*Db na forma de Bb)
C9*  = X 3 5 5 3 3 - (*D#9 na forma de C9)
E*  = 0 2 2 1 0 0 - (*G na forma de E)
E7*  = 0 2 2 1 3 0 - (*G7 na forma de E7)
E9*  = 0 2 4 1 0 0 - (*G9 na forma de E9)
F#m*  = 2 4 4 2 2 2 - (*Am na forma de F#m)
G#m*  = 4 6 6 4 4 4 - (*Bm na forma de G#m)
