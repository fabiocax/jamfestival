Anderson Freire - Atitude de Cristão

Intro: Dm7   Em7   F7M   Fm9

Solo intro:


E|-------------------------|
B|-5---6-5-5---6-5-5---6-5-|
G|---7-------7-------7-----|
D|-------------------------|
A|-5---5---7---7---8---8---|
E|-------------------------|


              C9                    Dm7
Percebi na caminhada a escuridão do cego
                 G/B         G             C9
O silêncio de um mudo quando me afastei de ti
              C        Am7          Dm7
Paralítico da fé foi assim que me senti
      Em               F/A       Dm7         G/B
Mas entrei dentro do templo e o tesouro descobri


          C             Am7                  Dm7
Não foi prata, não foi ouro que me colocou de pé
                G/B                       C9
Foi ouvir a sua voz que ressuscitou minha fé
  C                  Am7                        F/A
Não que eu viva do passado, mas é sempre bom lembrar
                Dm7         G/B        C9    F/A
Do lugar onde eu caí, pra poder me levantar

     F/A              C9
Hoje não sou mais enfermo
                 Dm7
Decidi não ser assim
              F/A          G/B        C9
Atitudes como essa ninguém tomará por mim
      F/A         C                           Dm7
Minha vida é um milagre que o senhor na cruz gerou
                 F/A
Esse é o maior milagre
   G/B          C9
Obrigado, meu Senhor

Intro: Dm7   Em7  Fm

Solo Guitarra: C9  Em7  F7M  G

 F/A                C9
Minha mente está sarada,
                     Dm7
Minha alma encontrou paz
                    G/B
Tão bom te ouvir de novo
     G            C9
A cegueira nunca mais
                C        Am7        Dm7
Uma dose de atitude me ajudou a melhorar
      Em      F/A            Dm7      G/B
Eu vivia me arrastando hoje posso caminhar

         C9                       Am7        Dm7
Não foi prata não foi ouro que me colocou de pé
                               G/B       C9
Foi ouvir a tua voz que ressucitou minha fé
                     Am7                        F/A
Não que eu viva do passado mas é sempre bom me lembrar
                Dm7      G/B          C9   F/A
Do lugar onde eu caí pra poder me levantar

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
Dm7 = X 5 7 5 6 5
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F/A = 5 X 3 5 6 X
F7M = 1 X 2 2 1 X
Fm = 1 3 3 1 1 1
Fm9 = 1 3 5 1 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
