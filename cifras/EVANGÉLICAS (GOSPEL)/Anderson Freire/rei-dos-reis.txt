Anderson Freire - Rei Dos Reis

Am               F
Não tinha aparência
 Dm
Nenhuma beleza
   E                           Am
Agradou os olhos de quem o viu
Am            F
Era desprezado
Dm                     E
O mais rejeitado entre os homens

Foi assim que ele surgiu

Am              Am/G            F
Mas não foi desse jeito que João viu
                     Dm
Quando arrebatado ao céu subiu
                   E
Ele estava sobre um cavalo branco
        Am
Seus olhos são chama de fogo

        F
Há muitos diademas na sua cabeça
        Dm                                E
Tem um nome escrito que ninguém conhece senão ele mesmo
Am                              F
Ele está vestido com manto tingido de sangue Seguido pelo exército do céu
 Dm                    E
Montados num cavalo Com vestidura branca Puro
           F
João estava lá no céu e viu
               F#
Sair da sua boca uma espada aguda
            G
E na sua coxa está escrito assim
                   E
Rei dos Reis e Senhor dos senhores

 Am         Am/G
Cristo! Vitorioso
 F
Cristo! Adorado
    C                 E
Cristo! O cordeiro vivo está agora transformado
     Am          F
Num corpo de Glória
  F             C
Glória! Glória! Glória
             E
Ele é o todo poderoso Rei da Glória

( Am  F  Dm  E )

  Am
Seus olhos são chama de fogo
        F
Há muitos diademas na sua cabeça
        Dm                                E
Tem um nome escrito que ninguém conhece senão ele mesmo
Am                              F
Ele está vestido com manto tingido de sangue Seguido pelo exército do céu
 Dm                    E
Montados num cavalo Com vestidura branca Puro
           F
João estava lá no céu e viu
         F#
Sair da sua boca uma espada aguda
        G
E na sua coxa está escrito assim
                  E
Rei dos Reis e Senhor dos senhores

 Am         Am/G
Cristo! Vitorioso
 F
Cristo! Adorado
    C                 E
Cristo! O cordeiro vivo está agora transformado
     Am          F
Num corpo de Glória
  F              C
Glória! Glória! Glória
             E
Ele é o todo poderoso Rei da Glória

 Am  F   C         E
Aleluia  Glorioso Cristo
 Am  F   C         E
Aleluia  Adorado Cristo
 Am  F   C         E
Aleluia  Glorioso Cristo
 Am  F   C         E
Aleluia  Adorado Cristo

 Am         Am/G
Cristo! Vitorioso
F
Cristo! Adorado!
    C                 E
Cristo! O cordeiro vivo está agora transformado
     Am          F
Num corpo de Glória
  F              C
Glória! Glória! Glória
             E
Ele é o todo poderoso Rei da Glória

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am/G = 3 X 2 2 1 X
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
