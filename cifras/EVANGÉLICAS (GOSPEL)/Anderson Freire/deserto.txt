Anderson Freire - Deserto

B                      F#/A#                       G#m7
  Eu não preciso transformar pedras em pães para provar
                     F#                 E
Que no deserto Tu estás    Cuidando de Mim
                              B/D#                           C#m7
De um lugar alto não vou me lançar  Só pra demonstrar o Teu poder
                     F#
Eu não preciso ver fenômenos pra crer em Ti
B                    F#/A#                       G#m7
  Não troco nossa comunhão  Pelo prazer de conquistar
                A                              C#m7
Palácios, Riquezas  Como um deserto Isso vai passar
                     B/D#                                 E
Por isso resistindo estou  E quando a minha força se esgotar

O Teu anjo vai me alimentar
     G#m7             E          B              F#4   F#
Te adorar é o que sustenta-me de pé  Não vou perder a guerra
C#m7          E            B                   F#4   F#
Te louvar em meio as tentações  É mais que estraté---gia
          G#m7      E                     B
Posso não ver o amanhã          Mas hoje sei

            F#         C#m7                     E
Que esse deserto vai chegar ao fim   O Senhor está cuidando de mim
B                      F#/A#                         G#m7
  Eu não preciso transformar   pedras em pães para provar
                     F#               E
Que no deserto Tu estás  Cuidando de Mim
                              B/D#                           C#m7
De um lugar alto não vou me lançar  Só pra demonstrar o Teu poder
                  F#4          F#
Não preciso ver fenômenos pra crer em Ti
B                    F#/A#                        G#m7
  Não troco nossa comunhão  Pelo prazer de conquistar
                A                              C#m7
Palácios, Riquezas  Como um deserto Isso vai passar
                     B/D#                                 E
Por isso resistindo estou  E quando a minha força se esgotar

O Teu anjo vai me alimentar
     G#m7             E          B              F#4   F#
Te adorar é o que sustenta-me de pé  Não vou perder a guerra
C#m7          E            B                   F#4   F#
Te louvar em meio as tentações  É mais que estraté---gia
          G#m7      E               B
Posso não ver o amanhã    Mas hoje sei
            F#         C#m7               E
Que esse deserto vai chegar ao fim    O Senhor está cuidando de mim

Solo: E  F#  G#m7  Gº  E  F#

     G#m7             E          B                      F#4   F#
Te adorar é o que sustenta-me de pé          Não vou perder a guerra
C#m7          E            B                           F#4   F#
Te louvar em meio as tentações          É mais que estraté---gia
          G#m7      E                     B
Posso não ver o amanhã          Mas hoje sei
            F#         C#m7                     E
Que esse deserto vai chegar ao fim          O Senhor está cuidando de mim

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
B/D# = X 6 X 4 7 7
C#m7 = X 4 6 4 5 4
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
F#/A# = 6 X 4 6 7 X
F#4 = 2 4 4 4 2 2
G#m7 = 4 X 4 4 4 X
Gº = 3 X 2 3 2 X
