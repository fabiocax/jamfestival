Anderson Freire - Sem Merecer

[Intro] A  E  C#m  B  A

                 E                    B
Quem iria acreditar num lugar tão obscuro
                   C#m                  B
Onde a alma é o tesouro e o pecado é entulho
                    A                     B
Ocupando o mesmo espaço no interior da vida
                   E                     B
Eu não tinha esperança, eu não via um futuro

                 E                            B
Foi aí que eu ouvi do outro lado da minha porta
               C#m                     B
Uma voz dizendo abra, com você quero cear
                      A                     B
Não olhou pro meu passado simplesmente me amou
                      E                    B
Se assentou à minha mesa e de filho me chamou

     C#m          A                 E          B
Entregou-me os talentos que eu não posso enterrar

   C#m         A         E           B
Tesouros para sua linda casa ornamentar
     F#m                     E
Quebrou o odre velho, vinho novo derramou
       A                     B             E    B  E
O seu sangue justifica o meu verdadeiro valor

                A
O Senhor acreditou, extraiu o melhor de mim
                E            B            C#m
Assim como Salomão, fez com ouro de Parvaim
                    F#m                   A
Tão distante me buscou, de tesouro me chamou
                    C#m       B         E
Mesmo eu sendo pecador, como pode me usar
                     A
Só Tu sabes quem eu sou, minhas fraquezas entreguei
        B        E           B       C#m                      F#m7
Meu fracasso evitou, quando aperfeiçoou em minha vida o Teu poder
     B                         A
Como pode amar alguém sem merecer

     C#m          A                 E          B
Entregou-me os talentos que eu não posso enterrar
   C#m         A         E           B
Tesouros para sua linda casa ornamentar
     F#m                     E
Quebrou o odre velho, vinho novo derramou
       A                     B             A    E
O seu sangue justifica o meu verdadeiro valor

                A
O Senhor acreditou, extraiu o melhor de mim
                E            B            C#m
Assim como Salomão, fez com ouro de Parvaim
                    F#m                   A
Tão distante me buscou, de tesouro me chamou
                    C#m       B         E
Mesmo eu sendo pecador, como pode me usar
                     A
Só Tu sabes quem eu sou, minhas fraquezas entreguei
        B        E           B       C#m                      F#m7
Meu fracasso evitou, quando aperfeiçoou em minha vida o Teu poder
     B                         A     B  C#m  B  C#m  C#
Como pode amar alguém sem merecer

                B                            G#m
O Senhor acreditou, extraiu o melhor de mim
               F#            C#          D#m
Assim como Salomão, fez com ouro de Parvain
                    G#m7           F#     B
Tão distante me buscou, de tesouro me chamou
          G#m        D#m       C#      F#
Mesmo eu sendo pecador, como pode me usar
                     B                           G#m
Só Tu sabes quem eu sou, minhas fraquezas entreguei
                 F#         C#     D#m
Meu fracasso evitou, quando aperfeiçoou
                      G#m    F#
Em minha vida o teu poder
      B            C#          F#     G#m  F#
Como pode amar alguém sem merecer
      B            C#           F#
Como pode amar alguém sem merecer

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C# = X 4 6 6 6 4
C#m = X 4 6 6 5 4
D#m = X X 1 3 4 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
G#m = 4 6 6 4 4 4
G#m7 = 4 X 4 4 4 X
