Anderson Freire - Glória

Intro: Em  C  D  Am

Em
A glória do senhor vai encher esse lugar
C
Não há ninguém que impedirá
Am
Sua unção sobre nós vai invadir
        Bm
Pois algo diferente está acontecendo aqui

Em
É o poder de Deus

Que está quebrando cadeias
C
É o poder de Deus

Que está avivando a igreja
Am
É o poder de Deus

                       Bm
Que está agindo nesse lugar

Em
Já sinto a sua glória
C
Senhor derrama sua glória
Am
Ohh...
              Bm
E sobre nós haverá:
Em      C
Cura, renovação,
 G            D    (C)
Avivamento e restauração
Em          C             G
Toda obra do mal será dissipado
            D (C)
Pelo nome do senhor
Em              C
Transformação, libertação
 G            D
inundamento de Deus
Em    C      D    Am (C)
Vai acontecer neste lugar.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
