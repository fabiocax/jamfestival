Anderson Freire - Só Você

 G                    D/F#     Em
Sei que não é fácil prestar atenção
                        G/B             C
A igreja está tão cheia rola uma tensão
                    G/B          Am
Por isso decidi usar só um piano
 D9                    G
Pra cantar essa canção
 G                     D/F#     Em
Hoje é o dia mais feliz pra mim
                                     C
Ouvi você falar nesse altar, que "Sim"
                               G/B         Am
Embala o meu peito, me deixa até sem jeito
D9                 Em
Ai, é demais pra mim
                     D/F#      C         G/B
Sou de carne e osso pra não me emocionar
 Am7                           G/B          C      D9
Tudo o que pedi em oração, agora é tudo realização
   G                 D/F         C               G/B           Am7
O sol, agora será visto de manhã por duas pessoas em uma janela só

 G/B        C           D  G  D
Literalmente não estarei só
 G              D/F#            Em
Só você é a dona do meu coração
                D/F#           C
Razão que causa a minha emoção
        G/B               Am7      D9
A lógica do meu grande amor é você
      G            D/F#             Em
Só você pra preencher a minha canção
             D/F#             C
Piano e voz só foi uma questão
              G/B               Am        D9          Em  D/F  C  D9  Em  D/F  C  D9  Em
O que eu queria mesmo era ter só pra mim a tua atenção....

                     D/F#      C         G/B
Sou de carne e osso pra não me emocionar
 Am7                           G/B          C      D9
Tudo o que pedi em oração, agora é tudo realização
   G                 D/F         C               G/B           Am7
O sol, agora será visto de manhã por duas pessoas em uma janela só
 G/B        C           D  G  D
Literalmente não estarei só
 G              D/F#            Em
Só você é a dona do meu coração
                D/F#           C
Razão que causa a minha emoção
        G/B               Am7      D9
A lógica do meu grande amor é você
      G            D/F#             Em
Só você pra preencher a minha canção
             D/F#             C
Piano e voz só foi uma questão
              G/B               Am        D9         Em  D/F  C  D9  Em  D/F  C  D9  Em
O que eu queria mesmo era ter só pra mim a tua atenção...

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F = X X 3 2 3 2
D/F# = 2 X 0 2 3 2
D9 = X X 0 2 3 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
