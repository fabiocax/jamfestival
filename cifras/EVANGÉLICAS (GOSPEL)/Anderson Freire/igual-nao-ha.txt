Anderson Freire - Igual Não Há

  Bb9             F
E outro igual não há (igual não há)
  Gm              Eb9
E outro igual não há (igual não há)
  Bb9             F     Gm              Cm Dm Eb9
E outro igual não há, e outro igual não há há há
  Bb9             F     Gm      F       Eb F Eb
E outro igual não há, e outro igual não há

Bb9                          F/A         Gm
O que adianta ter as mãos e não poder tocar?
                             F       Eb9
O que adianta ter ouvidos e não escutar?
                      F                     Gm  F
O que adianta ter um corpo e não se movimentar?

Bb9                           F/A      Gm
O que adianta ter os olhos e não enxergar?
                               F        Eb9
Ter à sua volta bons aromas e não respirar?
                 F                   Eb9 F
Receber nome de Deus, porém morto estar


       Bb9                              F
O meu Deus se movimenta ao som da adoração
                               Gm
É muito forte o toque de Suas mãos
                               Eb9
Inclina Seus ouvidos pra me escutar
       Bb9                                 F
O meu Deus, ninguém consegue Dele se esconder
                        Gm
Se Ele não quiser aparecer
                                        Eb9
Faz Seu povo entender que em meio ao silêncio está

              Bb9                                         F
E Ele está aqui! Tem glória Dele aqui, tem cheiro Dele aqui
            D                   Gm
Sua resposta é fogo sobre o altar
                                      Eb9
E tudo que é mentira está sendo consumido, deixa eu adorar
            Bb9                                     F
Ele está aqui! Tem glória Dele aqui, baal tem que cair
             D                   Gm
Profetas da mentira irão se dobrar
                                  Eb9
Irão reconhecer, terão que confessar
       Cm             Dm             Eb9
Que só há um Deus em toda a Terra e céu
   F               Bb9
E outro igual não há

Bb9                           F/A      Gm
O que adianta ter os olhos e não enxergar?
                               F        Eb9
Ter à sua volta bons aromas e não respirar?
                 F                   Eb9 F
Receber nome de Deus, porém morto estar

       Bb9                              F
O meu Deus se movimenta ao som da adoração
                               Gm
É muito forte o toque de Suas mãos
                               Eb9
Inclina Seus ouvidos pra me escutar
       Bb9                                 F
O meu Deus, ninguém consegue Dele se esconder
                        Gm
Se Ele não quiser aparecer
                                        Eb9
Faz Seu povo entender que em meio ao silêncio está

              Bb9                                         F
E Ele está aqui! Tem glória Dele aqui, tem cheiro Dele aqui
           D                    Gm
Sua resposta é fogo sobre o altar
                                      Eb9
E tudo que é mentira está sendo consumido, deixa eu adorar
            Bb9                                     F
Ele está aqui! Tem glória Dele aqui, baal tem que cair
             D                   Gm
Profetas da mentira irão se dobrar
                                  Eb9
Irão reconhecer, terão que confessar
       Cm             Dm             Eb9
Que só há um Deus em toda a Terra e céu
   F               Bb9 F
E outro igual não há    (2x)

  Bb9             F     Gm              Cm Dm Eb9
E outro igual não há, e outro igual não há há há
  Bb9             F  Gm                        Eb9
E outro igual não há, e outro igual, igual não há
    Cm      Eb9     Bb9
Não há, não há, não há

----------------- Acordes -----------------
Bb9 = X 1 3 3 1 1
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
Eb = X 6 5 3 4 3
Eb9 = X 6 8 8 6 6
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
Gm = 3 5 5 3 3 3
