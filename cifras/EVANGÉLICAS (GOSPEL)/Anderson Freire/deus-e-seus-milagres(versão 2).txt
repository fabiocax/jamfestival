Anderson Freire - Deus e Seus Milagres

Intro: Am C9 G/B Dm Am C9 G

                  Am         C9               G/B                 Dm
E|---------------0---------3-----------------3-------------------1---------|
B|------------0--1---------3--------------0--3-------------------3---------|
G|------0--2-----2---2--0--0--------0--2-----0-------------------2---------|
D|---2-----------2---------2-----2-----------0----0h2--0-----0---0---------|
A|---------------0---------3-----------------2------------3----------------|
E|-------------------------------------------------------------------------|

                  Am         C9                G
E|---------------0---------3-----------------3---------------------------|
B|------------0--1---------3--------------0--3---------------------------|
G|------0--2-----2---2--0--0--------0--2-----0---------------------------|
D|---2-----------2---------2-----2-----------0----0h2--0-----------------|
A|---------------0---------3-----------------2------------3--------------|
E|-------------------------------------------0---------------------------|

        C9                              Am7
Quem se acha sábio entendido, me dê uma explicação.
        C9              G/B
Como alguém com apenas um cajado

                 Am7                 Em
Faz o mar se abrir e o povo passar, então?
                F                          Am
O arquiteto de todo universo somente ele poderia me explicar?
         Dm               Dm/C           G
Como se constrói muros de águas no meio do mar,
       C9                             Am7
Oh renomado médico da terra, me dê sua opinião
        C9               G/B
Como alguém do solo faz um lodo,
      Am7                Em
e reverte a cegueira em visão
            F
Como alguém já morto há quatro dias
                 Am
Ao ouvir o som da voz do meu Jesus

       Dm      Em        F            G         C9
Se levanta, abandona a tumba fria, e encontra a luz...
                              C9
Se é milagre, tem a ver com meu Deus
                      Dm
E não há quem possa explicar
                     Bb
De uma coisa tenho certeza
           F        C
Se é milagre vou adorar

não importa a multidão que duvida
             Am        Dm
Influenciado não vou ficar
                        F                 G      F       Dm
Quem duvida vai ficar confundido é milagre vou adoraaaaaaaar...

      C9                                 Am7
Como pode a queda da muralha?, sem haver explosão
     C9              G/B           Am7
Uma pedra insignificante na mão de Davi
                    Em
Fez gigante vir ao chão
      F
Me explique como hoje em dia,
              Am
um delinquente na prisão vira pastor
       Dm           Dm/C     G
Quem matava hoje prega o amor
      C9                          Am7
Oh, renomado médico da terra, me dê uma explicação
            C9             G/B
Como é que pode alguém desenganado
                Am7          Em
Se levantar do leito e fica sarado
      F
Como alguém já morto há quatro dias
                  Am
Ao ouvir o som da voz do meu jesus
    Dm        Em           F     G      C9
Se levanta, abandona a tumba fria e encontra

                                                                                                 C9
Se é milagre, tem a ver com meu Deus
                      Dm
E não há quem possa explicar
                     Bb
De uma coisa tenho certeza
           F        C
Se é milagre vou adorar

não importa a multidão que duvida
             Am        Dm
Influenciado não vou ficar
                           F
quem duvida vai ficar confundido
          G      F    Dm
é milagre vou adoraaaaar...


Am                         F
Sem essa de talvez, eu não vou desistir
                                    C9
Pois quem me prometeu é fiel e vai cumprir
                               G
Todas as promessas que ele me fez...

(e toda honra, toda glória seja dada ao Rei)
Am                           F
sem essa de talvez é só acreditar
                                   C9
Pois quem te prometeu nunca parou trabalhar
                                 G
Quando Deus se movimenta a gente tem milagre pra contar.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
Dm = X X 0 2 3 1
Dm/C = X 3 X 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
