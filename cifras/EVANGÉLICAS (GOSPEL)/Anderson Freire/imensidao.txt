Anderson Freire - Imensidão

Intro: D  D4  D  Bm  G  A

 D           Em              G
Meu sobrenatural é o teu normal
                    D
Teu simples é o meu absurdo
                Em              G
Não há ciência aqui pra te resumir
                     D  Em D/F#
Um pouco de Ti já é meu tu-do

   G                       D
Tu és grande, tão grande assim
Bm              Em   A           Am        D7
E escolheste a mim, fez moradia dentro de mim
   G   E7     C#7 F#m
Tu és minha imensidão
Am        B7    Em    A        D  Bm
E fez de mim, então, tua habitação

D             Em                G
És toda conclusão antes que se tenha

               D
Uma pergunta, uma resposta
    Bm         Em
És luz sobre o sol
              G
Vento sobre o ar
                    D    Em D/F#
Fonte das águas que se espalham

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C#7 = X 4 3 4 2 X
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D4 = X X 0 2 3 3
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
