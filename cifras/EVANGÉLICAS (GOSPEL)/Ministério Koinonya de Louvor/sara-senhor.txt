Ministério Koinonya de Louvor - Sara Senhor

C7+      D/C   F#7/11   B7         Em7
SARA SENHOR A NOSSA GERAÇÃO
  C         D           E
DERRAMA TUA GRAÇA E PERDÃO
  Am               D
QUEBRANDO AS MALDIÇÕES
   D#°            Em7
RESTAURANDO OS CORAÇÕES
   C          G/B      Am          G/B
SARANDO AS FERIDAS DA NOSSA NAÇÃO
  C           G/B     Am            G
SARANDO AS FERIDAS DA NOSSA NAÇÃO

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
C7+ = X 3 2 0 0 X
D = X X 0 2 3 2
D#° = X X 1 2 1 2
D/C = X 3 X 2 3 2
E = 0 2 2 1 0 0
Em7 = 0 2 2 0 3 0
F#7/11 = 2 4 2 4 2 X
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
