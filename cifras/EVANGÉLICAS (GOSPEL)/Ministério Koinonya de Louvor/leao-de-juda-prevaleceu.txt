Ministério Koinonya de Louvor - Leão de Judá Prevaleceu

Em7
Ouve-se o júbilo de todos os povos
    D
Os reis se dobraram ao Senhor
C                Am7
Ouve-se um brado de vitória
   B7
O dia do Senhor chegou
Em7
Ouve-se em todos os povos
        D
Que um novo rei surgiu
  C           Am7
Impérios reconhecem
         B7
Que sua destra reinará

          Em7             D
Leão de Judá, Leão de Judá
           C  Am7        B7 
Leão de Judá    prevaleceu
      Am7       Bm7       Em7
E os povos verão e virão
      Am7       Bm7       Em7
A Sião aprender sua lei
        C        Am7       B7
Pois a sua justiça  governará

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em7 = 0 2 2 0 3 0
