Ministério Koinonya de Louvor - Se Creres

C
Quando ouço notícias
F
Que provam minha fé
C
São tantas as vozes
F
Que tentam me destruir
Am                      G         F
Eu lembro que nunca é tarde pra Jesus
Am                      G         F
Eu ouço a sua palavra e ele me diz

Dm      C/E     F
Aquele que crê em mim
Dm  C/E      F         G               C
Ainda que esteja morto viverá
C           G/B           F
Se creres verás a glória de Deus

G       F
O preço já foi pago
G       F
Tudo foi consumado
Am      G
Bençãos me seguirão
F
Deus tem um plano pra mim

C       G/B      F
Eu creio já vejo a glória de Deus
C       G/B        F    Dm
Eu creio já vejo a glória de Deus

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
