Ministério Koinonya de Louvor - Celebrai a Cristo,celebrai

[Intro] G   C   D   G

G    C     D       G        C D G
Celebrai a Cristo, celebrai
G    C     D       G        C D G
Celebrai a Cristo, celebrai
G    C     D       G        C D G
Celebrai a Cristo, celebrai
G    C     D       G
Celebrai a Cristo, celebrai

       D            Em
Ressuscitou, ressuscitou
    D              Em
E hoje vive, para sempre 
       D            Em
Ressuscitou, ressuscitou
     D             Em
E hoje vive, para sempre 

D               C   C#  D
Vamos celebrar, hei  
D               C C#  D
Vamos celebrar, oooohhh
D                      C      D      G
Vamos celebrar, ressuscitou o meu Senhor

----------------- Acordes -----------------
C*  = X 3 2 0 1 0 - (*A# na forma de C)
C#*  = X 4 6 6 6 4 - (*B na forma de C#)
D*  = X X 0 2 3 2 - (*C na forma de D)
Em*  = 0 2 2 0 0 0 - (*Dm na forma de Em)
G*  = 3 2 0 0 0 3 - (*F na forma de G)
