Ministério Koinonya de Louvor - Eu Vou Louvar a Ti

 Am       C               G
Tua verdade é o que me sustenta
 Am       C                G
Tua palavra é onde firmo os meus pés
Am          C        Em         D
Dia a pós dia sou guiado passo a passo
C               Em      D
Pelo pastor da minha alma

Am       C               G
Uma canção ao vivo a ti farei
Am       C                  G
Porque a minha inspiração virá de ti
Am      C        Em           D
Não me importa onde esteja ou como for

C       D       G
Eu vou louvar a ti
C        D              G
Eu vou dançar na batida do teu coração
C     D      G
Vou te alegrar
Em              D
Com aplausos e canções de amor
     C          D
Com músicas de júbilo

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
