Ministério Koinonya de Louvor - Tu És Soberano

[Intro] E7M  G#m7  A  A/B 
        E7M  G#m7  A  A/B 

E7M      G#m7           C#m7
Tu és soberano sobre a terra
        G#m/B         A    A/B  E7M  A/B
Sobre os céus Tu és Senhor absoluto

 E7M      G#m7       C#m7
Tudo que existe e acontece
    G#m/B         A    F#m7     A/B  E7M
Tu o sabes muito bem   Tu és  tremendo

 F#m7     A9    E7M         
E apesar dessa glória que tens
 F#m7     A9    E7M
Tu te importas, comigo também
  C#m7          G#m7
E esse amor tão grande
 C#m7      F4  F#  A/B     
Eleva-me, amarra-me a Ti   
    
E7M      G#m7           C#m7
Tu és soberano sobre a terra
        G#m/B         A    A/B  E7M  A/B
Sobre os céus Tu és Senhor absoluto

 E7M      G#m7       C#m7
Tudo que existe e acontece
    G#m/B         A    F#m7     A/B  E7M
Tu o sabes muito bem   Tu és  tremendo

 F#m7     A9    E7M         
E apesar dessa glória que tens
 F#m7     A9    E7M
Tu te importas, comigo também
  C#m7          G#m7
E esse amor tão grande
 C#m7      F4  F#   B4     
Eleva-me, amarra-me a Ti   
B7       E7M
Tu És tremendo

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/B = X 2 2 2 2 X
A9 = X 0 2 2 0 0
B4 = X 2 4 4 5 2
B7 = X 2 1 2 0 2
C#m7 = X 4 6 4 5 4
E7M = X X 2 4 4 4
F# = 2 4 4 3 2 2
F#m7 = 2 X 2 2 2 X
F4 = 1 3 3 3 1 1
G#m/B = X 2 X 1 4 4
G#m7 = 4 X 4 4 4 X
