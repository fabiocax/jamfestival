Ministério Koinonya de Louvor - Em Espírito, Em Verdade 

[Intro] A  E  D  E
        A  E  D  E

      A             E
Em Espírito, em verdade
       D           A    E
Te adoramos,   Te adoramos
      A             E
Em Espírito, em verdade
       D           A    E
Te adoramos,   Te adoramos

       F#m  Fm       A/E  B
Rei do reis       e Senhor
        D      E       A    E
Te entregamos nosso   viver
       F#m  Fm       A/E  B
Rei do reis       e Senhor
        D     Bm7     E
Te entregamos nosso viver

           D       F#m         E
Pra Te adorar, oh rei dos reis
            D        F#m     E
Foi que eu nasci, ó rei Jesus
        C#         F#m
Meu prazer é te louvar
        E       D      E             A
Meu prazer é estar nos átrios do Senhor
        E    F#m    E       D          E      A
Meu prazer é viver na casa de Deus onde flui o amor

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/E = 0 X 2 2 2 0
B = X 2 4 4 4 2
Bm7 = X 2 4 2 3 2
C# = X 4 6 6 6 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
Fm = 1 3 3 1 1 1
