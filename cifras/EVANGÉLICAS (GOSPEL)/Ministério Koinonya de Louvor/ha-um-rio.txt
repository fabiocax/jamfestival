Ministério Koinonya de Louvor - Há Um Rio

(intro) Bm7 A9 D9 Bm7 A9  G

     Bm7         A9   D9
Há um rio de     agua viva
     Bm7       A9            D9
Há um rio      dentro do meu ser 
    Em    Bm7      A9      D9      A/C#        Bm7
É um rio   de     amor é um rio    de       perdão 
    Em             F#          Bm7          A9
É um rio que traz cura a toda a dor, a toda dor
     Bm7        A9   D9
Há um rio    de agua viva
     Bm7      A9             D9
há um rio     dentro do meu ser 

    Em     Bm7       A9       D9    A/C#       Bm7
É um rio   de     louvor é um rio   de    adoração 
    Em            A9         D D7
É um rio que traz vida onde for
        G   A9             D9   A/C# Bm7
Rio que cura,      rio que sa-   ra, 
       Em             A9         D     D7
Rio que traz vida abundante ao coração 
        G    A9             D9   A/C#  Bm7
Rio que cura,       rio que sa-    ra, 

        Em            A9          G   A9 G/B A9
Rio que traz vida abundante ao coração
C#m       B9   E4    E     C#m      B9     E4   E      C#m      B9     E4  E    C#m    B9 E4 E
Há         um  ri-o       há    um    ri-o      há       um   ri-o     há      um  ri-o

     C#m7       B9    E
Há um rio    de agua viva
     C#m7      B9           E
há um rio     dentro do meu ser 
    F#m    C#m7      B9       E   B/D#         C#m7
É um rio   de     louvor é um rio   de    adoração 
    F#m           B9         E E7
É um rio que traz vida onde for
        A9    B9           E   B/D#  C#m7
Rio que cura,      rio que sa-   ra, 
       F#m            B9          E    E7
Rio que traz vida abundante ao coração 
        A9     B9           E    B/D#  C#m7
Rio que cura,       rio que sa-    ra, 
        F#m      B9   A9    B9   A/C# B/D# F#m B9 E
Rio que traz vida abundante ao coração

----------------- Acordes -----------------
A/C# = X 4 X 2 5 5
A9 = X 0 2 2 0 0
B/D# = X 6 X 4 7 7
B9 = X 2 4 4 2 2
Bm7 = X 2 4 2 3 2
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
D7 = X X 0 2 1 2
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
E4 = 0 2 2 2 0 0
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
