Ministério Koinonya de Louvor - Nome Sobre Todo Nome

Introd.: G - C    (2x)Só violão

G                 C                G          C
Nome sobre todo nome e o nome de Jesus (2x)Vozes e violão, (2x) instrumentos
D         C    G        D            C  G
Acima de todo poder, Principado e potestade
 D        C      G                  D     C
Acima de todo dominio e de todo nome
                      G  A# C   G  A# C
No ceu na terra e no mar
G                 C                G   F C
Nome sobre todo nome e o nome de Jesus (2x)
D         C      G        D        C     G
Tribos e racas virao, Na presenca do Senhor
 D        C       G            D     C
Linguas povos e nacoes, Celebrarao
               G  A# C
O nome de Jesus
G                 C                G          F C
Nome sobre todo nome e o nome de Jesus (4x)
G                 C                G
Nome sobre todo nome: Jesus
Am               C       G      F C
Nome sobre todo nome: Jesus

----------------- Acordes -----------------
A# = X 1 3 3 3 1
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
