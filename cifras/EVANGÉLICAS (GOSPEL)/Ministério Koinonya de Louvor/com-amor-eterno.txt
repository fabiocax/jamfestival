Ministério Koinonya de Louvor - Com Amor Eterno

  A                  F#m
Mesmo que às vezes tenhas
   Bm              E7
Que passar por aflições
      A              C#m
Saiba que eu venci o mundo
  Bm                        E
E assim como Eu, te fiz vencedor
    A             E/G#
Se no vale tu passares
      F#m        C#m
Junto a ti eu estarei 
D                   A/C#   F#m
E se no deserto andares
  Bm7           E7
Tua sombra eu serei
  A               E/G#
Seja no calor do dia
       F#m          C#m
Ou se a noite fria está
  D              A/C#        F#m
Não temas filho meu,
 Bm                E       D/F#  E/G#
Minha mão te guardará
A        A/C#            D
Com amor eterno eu te amei
     E             C#/F        F#m
Meu poder eterno eu te entreguei
 E               D        D/F#   E/G#
Te dei a minha vida
A         A/C#        D
Com benignidade te atraí
         E             
Com meu próprio sangue 
C#/F   F#m 
Eu te remi
 E              D
Chamei-te pelo nome,
D/E E  A
Tu és meu.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
C#/F = X 8 X 6 9 9
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
D/E = X X 2 2 3 2
D/F# = 2 X 0 2 3 2
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
