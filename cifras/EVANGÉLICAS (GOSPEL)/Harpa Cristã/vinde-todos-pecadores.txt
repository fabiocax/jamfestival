Harpa Cristã - Vinde, Todos Pecadores

E            A    E      B7          E                 A
Vinde todos pecadores a jesus, o salvador;  vinde logo, sem
   E         B7          E   A  E
Temores, encontrar o redentor.
      B7    A     E            B7                  E
Sim eu sei; oh! eu sei, cristo salva o perdido pecador!
        B7    A     E              B7            E     A  E
Sim, eu sei; oh, eu sei, cristo salva  perdida pecador!

       E       A    E         B7            E
Dá aos fracos, fortaleza;  das montanhas planos faz,
           A  E             B7            E   A E
Ao deserto dá beleza, e aos crentes, luz e paz.

      E       A     E        B7           E
Nas fraquezas, ei-lo perto, dominando a força má;
            A      E          B7          E    A E
No caminho, guia certo, e sua graça sempre dá.

      E  A      E        B7          E
Tenho gozo mui superno pelo sangue que verteu,

             A       E             B7          E   A E
Nem o mundo, nem o inferno,  tira a vida que me deu.

          E    A      E       B7          E
Quando cristo vier na glória a buscar o povo seu,
          A     E       B7              E   A E
Cantaremos a história  do amor de deus, no céu.

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
