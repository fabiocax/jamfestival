Harpa Cristã - Deixai As Ilusões

   G                  C
O mundo de ilusão deixei,
   G                 C
A senda de pecado e dor,
    G                   C
Pra ir ao meu glorioso lar;
 D     G             C
Ali há gozo, paz e amor.

Coro
   G                  C
No mundo não está meu lar,
  C                   C
Aqui não posso descansar;
     G                C
Mas quero sempre avançar:
    D       G              C
No céu em breve hei de entrar

  G                    C
Alguns procuram m'impedir,

    G                C
Na senda gloriosa andar,
     G                C
Mas quero sempre avançar,
  D     G            C
Até no lar poder entrar.

 G                    C
Amigo que no mundo estás,
     G                C
Não queres me acompanhar,
   G               C
À terra do eterno amor?
   D    G           C
Jesus deseja te livrar.

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
