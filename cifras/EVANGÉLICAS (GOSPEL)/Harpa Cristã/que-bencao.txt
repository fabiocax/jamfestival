Harpa Cristã - Que Bênção

   G
Que bênção: a lua brilhando no espaço!
 C             G         D
Que bênção: as ondas que rolam no mar!
   G
Que bênção: as flores brotando nos campos!
 C           G        D         G
Mas a maior bênção eu vou te contar:


           C                 G           C       G          D          G
Jesus me salvou, dos pecados meus, Que bênção, Jesus! que bênção, meu Deus!
           C                 G           C       G          D          G
Jesus me salvou, dos pecados meus, Que bênção, Jesus! que bênção, meu Deus!

   G
Que bênção: as aves que cantam e encantam!
  C             G         D
Que bênção: crianças em simples brincar!
   G
Que bênção: rebanhos pastando entre os rios!

 C           G        D         G
Mas a maior bênção eu vou te contar:

  G
Que bênção: amigos que a mim procuram!
  C             G         D
Que bênção: a Pátria, a qual posso amar!
  G
Que bênção: família, saúde, fartura!
 C           G        D         G
Mas a maior bênção eu vou te contar:

----------------- Acordes -----------------
C*  = X 3 2 0 1 0 - (*A# na forma de C)
D*  = X X 0 2 3 2 - (*C na forma de D)
G*  = 3 2 0 0 0 3 - (*F na forma de G)
