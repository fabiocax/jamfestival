Harpa Cristã - Jesus À Porta do Coração

D            A7      D   Bm        Em        D
Quem está batendo assim, que enche-me de comoção?
           G         D      G      D    A7        D
Quem procurará por mim?  sinto bater    no   coração.
A7           D     A7     D  G        A        D     A7
Ouem bate, diz com mansidão: eu sou jesus, te vim salvar;
D      A7  D     G            D    A7    A
Abre, duro coração, dentro de ti quero cear.

 D              A7  D     Bm   Em  A7     D
Quem será? quem pode ser?  sinto bater no coração!
                G    D       G     D  A7   D
Que farei? que vou fazer?  no vale estou da decisão.

 D          A7   D    Bm      Em          D
Quem escuto eu bater? - eu sou jesus, o bom jesus:
            G   D   G      D A7       D
Abre logo, sem temer quero te dar a minha luz.

D            A7   D  Bm  Em             D
Entra, entra, salvador meu coração te quero dar,

         G     D    G      D    A7      D
Só em ti achei amor, mesmo em mim queiras morar.

A7     D      A7          G      D      G    A7
No coração , ó bom jesus, entra,trazendo teu perdão!
 A7    D D   G           D   A7       D
Concedendo tua luz, tenho, assim, a salvação.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
