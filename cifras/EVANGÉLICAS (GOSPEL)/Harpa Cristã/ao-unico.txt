Harpa Cristã - Ao Único

     C           G/B      Am
 Ao Único que é digno de receber
   F          C       Dm    C     G
A honra e a Glória a força e o poder
    C         C/E     F          F°
Ao rei eterno imortal invisível más real
  C         G           C   G/B
A Ele ministramos o louvor

     Am Em   F   G      C
 Adoramos a Ti ó rei Jesus
    Am Em   F   G      C
Adoramos a Ti ó rei Jesus

     F           G
 Adoramos o Teu Nome
        G#°       Am
Nos rendemos ao Teus Pés
      Dm            G
Consagramos todo o nosso
       C
Ser a Ti

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F° = X X 3 4 3 4
G = 3 2 0 0 0 3
G#° = 4 X 3 4 3 X
G/B = X 2 0 0 3 3
