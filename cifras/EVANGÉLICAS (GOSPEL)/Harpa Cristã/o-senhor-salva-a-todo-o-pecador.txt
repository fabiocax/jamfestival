Harpa Cristã - O Senhor Salva a Todo o Pecador

     F#    F#4     F#               B               F#
Aleluia! O Senhor Salva a todo pecador!
           C#             F#
Salvação! Salvação!
              F#    F#4      F#          B                     F#
Jesus Cristo tem poder para todo o mal vencer,
          C#             F#
Salvação! Salvação!

           F#
Salvação e redenção!
      C#                          F#
Aleluia! Cristo já me amou e me Salvou.
             F#
Glória dou e Aleluia,
          C# C#7            F# B/F# F#
A Jesus que me Salvou.

Eu confio em Jeová, Ele santidade dá;
Salvação! Salvação!
Tenho paz e vivo já, Sua graça em mim está.

Salvação! Salvação!

Falarei sempre o que sei, exaltando o meu Rei,
Salvação! Salvação!
Pois Seu sangue já verteu, pra tornar-me filho Seu,
Salvação! Salvação!

Cantaremos, sempre ali, ao sairmos nós daqui;
Salvação! Salvação!
Exaltando o Salvador, louvaremos com fervor
Salvação! Salvação!

----------------- Acordes -----------------
Capotraste na 1ª casa
B*  = X 2 4 4 4 2 - (*C# na forma de B)
B/F#*  = X X 4 4 4 2 - (*C#/G# na forma de B/F#)
C#*  = X 4 6 6 6 4 - (*D# na forma de C#)
C#7*  = X 4 3 4 2 X - (*D#7 na forma de C#7)
F#*  = 2 4 4 3 2 2 - (*G# na forma de F#)
F#4*  = 2 4 4 4 2 2 - (*G#4 na forma de F#4)
