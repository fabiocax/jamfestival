Harpa Cristã - A Última Hora

      D       G         D   Bm        Em              A7      D    A
Ao findar o labor desta vida quando a mor--te ao teu lado chegar,
       D          G         D   Bm       Em      A7          D    D7
Que destino há de ter a tua alma? Qual será no futu---ro teu lar?


REFRÃO:
      G          A         F#m7  Bm7       Em            A7       D    D7
Meu amigo, hoje tu tens a escolha: Vida ou mor---te qual vais aceitar?
     G      A         F#m7 Bm7    Em           A7        D    A
amanhã pode ser muito tarde: Hoje Cris---to te quer libertar.


            D       G         D     Bm      Em           A7        D    A
Tu procuras a paz neste mundo,  em praze|---res que passam em vão.
        D         G          D    Bm     Em          A7      D    D7
Mas, na úl---tima ho---ra da vida,  eles não mais te satisfarão.


(REFRÃO)


     D       G            D    Bm        Em         A7       D    A
Por acaso tu ris---te, ó amigo, quando ouvis---te falar de Jesus?
        D         G       D     Bm     Em      A7       D    D7
Mas somen---te Jesus pode dar-te salvação pela morte na cruz.


         D           G          D   Bm      Em        A7            D    A
Tens mancha---da tua alma e não podes contemplar o semblan---te  de Deus
      D             G        D     Bm     Em        A7        D    D7
Só os cren---tes de corações limpos   poderão ter o gozo nos céus.


     D           G           D   Bm       Em      A7          D    A
Se deci---des deixar teus pecados, e entregar tua vi---da a Jesus,
      D            G      D  Bm     Em          A7        D    D7
Trilharás, sim, na última hora um camin---ho brilhante de luz.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
F#m7 = 2 X 2 2 2 X
G = 3 2 0 0 0 3
