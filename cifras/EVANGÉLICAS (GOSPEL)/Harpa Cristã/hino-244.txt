Harpa Cristã - Hino 244

   G               G/B
Louvai a Jesus, amoroso
    D               G
Na cruz os pecados levou
    G                  G/B
Que nos enche de Santo gozo
    D                 G
Com sangue o céu nos ganhou

    C    G
Ao meu Senhor
    D                G   G/B
Que tenho no meu coração
   C      G/B
Eu dou louvor
     C      D        G    D
Pois Ele me deu salvação

   G               G/B
Louvai a Jesus poderoso
    D               G
Que veio livrar da prisão

   G              G/B
Um povo cativo, medroso
    D                 G
Que não tinha consolação

   G                  G/B
Louvai a Jesus, vosso guia
   D                G
No mundo de tribulação
  G                      G/B
E não tem temor, quem confia
   D                    G
Em Deus, mas real proteção

   G                    G/B
Louvai a Jesus, dai-Lhe glória
     D                G
Pois Rei e Senhor Ele é
   G                   G/B
Na cruz nos ganhou a vitória
   D                  G
Vencei com Jesus pela fé

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
