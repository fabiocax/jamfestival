Harpa Cristã - Revela a Nós, Senhor

           D  Em             F#m
Jesus, meu Rei,  Mestre e Senhor
Bm        Em  A           D
   O Teu amor   revela a mim
         D  Em        F#m
Enquanto eu    aqui viver
Bm        Em  A            D
   Até eu ver   da vida o fim

  A              D     Bm           Em
Revela a nós, Senhor Jesus, meu Salvador
      A           D7
As maravilhas mil do Teu divino amor
         A      D       D        G
E com veraz louvor, fervente gratidão
               D     Bm     Em     A   D
Eleva a TI, Jesus Senhor, o nosso coração

            D  Em        F#m
As bênçãos mil    do Teu amor
Bm             Em  A          D


   Qual esplendor    me cercarão
         D  Em         F#m
O Teu olhar    será, Jesus
Bm          Em  A         D
   A grata luz    do coração

  A              D     Bm           Em
Revela a nós, Senhor Jesus, meu Salvador
      A           D7
As maravilhas mil do Teu divino amor
         A      D       D        G
E com veraz louvor, fervente gratidão
               D     Bm     Em     A   D
Eleva a TI, Jesus Senhor, o nosso coração

          D  Em           F#m
Sorrisos Teus   verei brilhar
Bm           Em  A           D
   Se não andar    no mundo vil
    A   D  Em          F#m
Desfrutarei    prazer veraz
Bm           Em  A         D
   Tempo de paz    primaveril

  A              D     Bm           Em
Revela a nós, Senhor Jesus, meu Salvador
      A           D7
As maravilhas mil do Teu divino amor
         A      D       D        G
E com veraz louvor, fervente gratidão
               D     Bm     Em     A   D
Eleva a TI, Jesus Senhor, o nosso coração

           D  Em          F#m
E, quando for    no céu morar
Bm         Em  A           D
   E descansar   dos dias meus
   A    D  Em       F#m
Feliz viver    receberei
Bm            Em  A            D
   De Ti, meu Rei,  meu santo Deus!

  A              D     Bm           Em
Revela a nós, Senhor Jesus, meu Salvador
      A           D7
As maravilhas mil do Teu divino amor
         A      D       D        G
E com veraz louvor, fervente gratidão
               D     Bm     Em     A   D
Eleva a TI, Jesus Senhor, o nosso coração

----------------- Acordes -----------------
A*  = X 0 2 2 2 0 - (*G na forma de A)
Bm*  = X 2 4 4 3 2 - (*Am na forma de Bm)
D*  = X X 0 2 3 2 - (*C na forma de D)
D7*  = X X 0 2 1 2 - (*C7 na forma de D7)
Em*  = 0 2 2 0 0 0 - (*Dm na forma de Em)
F#m*  = 2 4 4 2 2 2 - (*Em na forma de F#m)
G*  = 3 2 0 0 0 3 - (*F na forma de G)
