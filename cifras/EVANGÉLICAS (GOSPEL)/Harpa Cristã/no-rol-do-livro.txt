Harpa Cristã - No Rol do Livro

G                        C               G
Teu nome, por inteiro, escrito está nos céus
             Em           A7           D7
No livro do cordeiro, no livro do bom deus,
            G        C           G
Teremos a vitória, olhando no senhor;
     C            G        D7      G
Por isso, damos glórias e gran louvor.

Refrão:
            G7      C              G
Teu nome escrito. irmão, está por deus,
     C        G       A7            D7
No livro da vida, no rol dos filhos seus,
     G               C              G
Teu nome escrito, irmão, está nos céus;
    C         G     D7  G
Oh! glória, aleluia! a---mém.

  G                   C          G
Alegres nós andamos na graça do senhor;

             Em     A7          D7
E nela nõs achamos poder consolador!
      G        C            G
De cristo recebemos, ajuda e vigor;
 C            G       D7        G
Avante, pois marchemos, sem mais temor.

      G               C          G
Nós criaturas, somos remidas por jesus,
                Em            A7      D7
No qual eleitos fomos. p'ra desfrutar a luz
                  G        C             G
Nós temos, por herança, jesus nos altos céus,
  C        G         D7   G
Também a esperança de ver a deus.

  G                     C             G
Contentes, levantemos os olhos para os céus,
                Em        A7          D7
Escrito nos já temos, os nomes, lá por deus;
  G                    C           G
Sentindo grande gozo, servindo ao senhor,
    C     G       D7    G
Ao todo-poderoso, ao redentor.

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
C = X 3 2 0 1 0
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
