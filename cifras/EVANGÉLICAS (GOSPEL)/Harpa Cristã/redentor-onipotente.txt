Harpa Cristã - Redentor Onipotente

 F                  C         F
Redentor onipotente,Poderoso Salvador,
F                   C                F
Advogado onisciente e Jesus, meu bom Senhor.

Refrão
      Bb     F
    Ó Amante da minh'alma,
     Bb              C
    Tu és tudo para mim!
     F
    Tudo quanto eu careço
     C                 F
    Acho, Jesus, só em Ti.

 F                      C           F
Um abrigo sempre perto,Para todo o pecador;
 F                       C             F
Um refúgio sempre aberto,É Jesus. meu Salvador.

 F                      C               F
Água viva! pão da vida!Doce sombra no calor,

 F                          C             F
Que ao descanso nos convida.É Jesus, meu Salvador!

 F                   C               F
Ó Cordeiro imaculado,Que Seu sangue derramou,
 F                    C              F
Meus pecados expiando,A minh'alma resgatou.

 F                    C              F
Fundamento inabalável!Rocha firme e secular!
 F                   C              F
Infalível! Imutável!Quem mo poderá tirar?

 F                     C                 F
O caminho que, seguro,Sempre para o céu conduz,
 F                          C               F
Quem a Cristo pronto segue,Quem tomar a sua cruz.

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
