Harpa Cristã - Tentado Não Cedas

D           A     Bm9       F#m
Tentado não cedas ceder é pecar
G             D     A         D
Melhor e mais nobre será triunfar
          A      Bm9          F#m
Coragem ó crente domina o teu mal
G            D      A          D
Deus pode livrar-te de queda fatal

Em Jesus tens a palma
A               D
Da vitória minh'alma
G             D
E também doce calma
A              D
Pelo sangue da cruz

           A    Bm9         F#m
Evita o pecado  procura agradar
G              D     A            D
A Deus  a quem deves no corpo exaltar

                  A     Bm9        F#m
Não manches teus lábios com impura vóz
G           D    A          D
Defende tua alma do vício atroz

             A     Bm9        F#m
Sê manso e benigno qual morto até.
G         D     A         D
Na rocha eterna firma tua fé
             A    Bm9           F#m
Veraz é teu dito  de Deus é teu ser?
G            D    A           D
T'espera a coroa  tu podes vencer

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm9 = X 2 4 6 3 2
D = X X 0 2 3 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
