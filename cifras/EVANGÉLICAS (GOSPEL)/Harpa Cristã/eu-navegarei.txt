Harpa Cristã - Eu Navegarei

[Intro] Am  G  F  E7

          Am                 G
Eu navegarei no oceano do Espírito
            F  Dm7                 E7
E ali adorarei    ao Deus do meu amor
          Am                 G
Eu navegarei no oceano do Espírito
            F  Dm7                 E7
E ali adorarei    ao Deus do meu amor

[Refrão]

            Am                     G
Espírito Espírito, que desce como fogo
                  F     Dm7               E7
Vem como em pentecostes,   e enche-me de novo
            Am                     G
Espírito Espírito, que desce como fogo
                  F     Dm7               E7
Vem como em pentecostes,   e enche-me de novo


         Am                   G
Eu adorarei ao Deus da minha vida
               F  Dm7               E7
Ao meu libertador    aquele que venceu
         Am                   G
Eu adorarei ao Deus da minha vida
               F  Dm7               E7
Ao meu libertador    aquele que venceu

[Refrão]

            Am                     G
Espírito Espírito, que desce como fogo
                  F     Dm7               E7
Vem como em pentecostes,   e enche-me de novo
            Am                     G
Espírito Espírito, que desce como fogo
                  F     Dm7               E7
Vem como em pentecostes,   e enche-me de novo

         Am               G
Eu adorarei ao  Deus da minha vida
                F  Dm7                    E7
Que me compreendeu    sem nenhuma explicação
         Am               G
Eu adorarei ao  Deus da minha vida
                F  Dm7                    E7
Que me compreendeu    sem nenhuma explicação

[Refrão]

            Am                     G
Espírito Espírito, que desce como fogo
                  F     Dm7               E7
Vem como em pentecostes,   e enche-me de novo
            Am                     G
Espírito Espírito, que desce como fogo
                  F     Dm7               E7
Vem como em pentecostes,   e enche-me de novo

          Am
Eu navegarei

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
