Harpa Cristã - Fala, Jesus Querido

A              D          E             A
Fala, Jesus querido;      fala-me, hoje sim!
F#m             Bm         E             A  E
Fala com Tua bondade;      fica ao pé de mim;
A            D            E              A
Meu coração aberto 'stá   p'ra Tua voz ouvir;
F#m            Bm          E                 A  E
Enche-me de louvores       e gozo p'ra Te servir.

A            E         D                A
Fala-me suavemente!    Fala, com muito amor!
F#m           Bm        E               A  E
Vencedor para sempre,   livre te hei de por,
A            E         D               A
Fala-me cada dia,      sempre em terno tom;
F#m              Bm        E             A  E   ( A )
Ouvir Tua voz eu quero     e neste mesmo som.

A                D          E             A
Para teus filhos fala,      e, no caminho bom,
F#m             Bm          E               A  E
Pela bondade os guia        a pedir o santo dom;

A             D           E               A
Quererão consagrar-se     para suas vidas dar.
F#m          Bm          E               A  E
Obedecendo a Cristo      e com fervor O amar.

A               D          E              A
Como no tempo antigo,      Tu revelaste a lei,
F#m              Bm          E             A   E
Mostra-me Tua vontade,       e à Tua santa grei;
A             D           E             A
Deixa-me gloriar-Te,      quero a Ti louvar,
F#m          Bm          E              A  E
Cantar alegremente       e sempre Te honrar.

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
