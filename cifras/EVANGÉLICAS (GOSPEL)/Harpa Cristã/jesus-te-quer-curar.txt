Harpa Cristã - Jesus Te Quer Curar

    D       A7      D           A7    D               A7
A terna voz do salvador te fala comovida; "ó vem ao médico de
  D                     A7   D
Amor, que dá aos mortos vi---da"!

                 G      D                     G     D
Cristo jesus te quer curar, e tem poder p'ra te curar,
                A7     D            A7      D
Dos males todos te livrar,  se nele confiares!

    D             A7   D               A7   D
Crê tu, a quem já satanás  há anos tem ligado;
                 A7       D                 A7   D
A fé te salva, vai-te em paz, de todo o mal curado!

   D            A7    D                   A7 D
Os surdos ouvem, cegos vêem, pois cristo é poderoso!
                  A7  D                  A7   D
Os coxos saram e andam bem,  por seu poder glorioso! ,

    D              A7 D                   A7   D
Sinais p'ra sempre seguirão aos verdadeiros crentes!

               A7    D               A7  D
Demônios, sim, expulsarão e curarão as gen--tes.

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
