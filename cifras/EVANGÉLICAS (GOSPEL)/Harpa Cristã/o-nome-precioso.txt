Harpa Cristã - O Nome Precioso

G                                  D7                 G
Teu  nome  é  precioso  amado  salvador  e  grande  glorioso
       A7        D7           G           Am         D7
Nos  enche  de  amor  nós  todos  te  adoramos  com  verdadeira
 G   G5+  C            G        C    G     D       G
Fé  tam---bém  nos  humilhamos  aos  teus  santos  pés.
  G                      A7    D7                    G
Quantas  bençãos  prometidas  no  teu  nome  ó  salvador!
                    Am       E7 Am      Am6
Hoje  tenho  vida, graça  é  con---ce----dida....

No  teu  nome  ó  sal--va--dor!

     G                          D7                      G
Teu  nome é sagrado nos  dá consolação por  ele  foi-nos  dado
    A7          D7     G          Am       D7            G
Do  céu  real  perdão  aviva  a  esperança  aumenta  nosso  amor
G5+  C         G    C  G        D     G
Dá  firme  confiança  e  mais  do  teu  vigor.

     G                                   D7                G
Teu  nome  é  nossa  vida  e  paz  ó  salvador  a  arca  oferecida

     A7      D7        G       Am        D7       G
Ao  pobre  pecador  por  ele  abençoada  é  toda  a  nação,
G5+  C         G     C   G    D   G
Que  foi  predestinada  à  grande  vocação.

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Am6 = 5 X 4 5 5 X
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G5+ = 3 X 1 0 0 X
