Harpa Cristã - 123 - Cristo Voltará

     C
Rude cruz se erigiu,
       F    Dm
Dela o dia fugiu,
       G        F         C    G
Como emblema de vergonha e dor;
       C
Mas contemplo esta cruz.
       F     Dm
Porque nela Jesus
      G        F       C
Deu a vida por mim, pecador.

        G                 C
Sim, eu amo a mensagem da cruz
     F        Dm        G    F
Té morrer eu a vou proclamar;
    C                   F    Dm
Levarei eu também minha cruz
       C     G      C    G
Té por uma coroa trocar.


        C
Desde a glória dos céus,
     F       Dm
O Cordeiro de Deus,
   G            F        C    G
Ao Calvário humilhante baixou;
     C
Essa cruz tem pra mim
    F        Dm
Atrativos sem fim,
       G      F        C
Porque nela Jesus me salvou.

      C
Nesta cruz padeceu
      F       Dm
E por mim já morreu,
      G         F          C    G
Meu Jesus, para dar-me o perdão
         C
E eu me alegro na cruz,
     F          Dm
Dela vem graça e luz,
     G        F    C
Para minha santificação.

    C
Eu aqui com Jesus,
     F       Dm
A vergonha da cruz
      G        F      C    G
Quero sempre levar e sofrer;
       C
Cristo vem me buscar,
      F      Dm
E com Ele, no lar,
    G        F            C
Uma parte da glória hei de ter.

----------------- Acordes -----------------
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
