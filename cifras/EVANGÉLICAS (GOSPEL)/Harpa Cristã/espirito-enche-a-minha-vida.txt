Harpa Cristã - Espírito, Enche a Minha Vida

[Intro] C  G  Am  F  G  C

C   Em      F             G
Espírito enche a minha vida
C   Em      F             G
Espírito enche a minha vida
Am           Em
Enche-me com teu poder
F             C
Pois em ti eu quero ser
  F      G           C
Espírito enche o meu ser

Am               Em                 F G Am
As minhas mãos eu quero levantar
F         G         Am
Em teu louvor te adorar
F               G                   F G Am
Meu coração eu quero derramar
F         G     C
Diante do teu altar


[Final]

F         G     C
Diante do teu altar
F         G     C
Diante do teu altar

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
