Harpa Cristã - Cristo, o Fiel Amigo

D       D7       G      D
Cristo Jesus é fiel amigo,
D   A       D
Ele só, Ele só.
D      D7        G       D
E nas fraquezas está comigo,
D   A       D
Ele só, Ele só.

D      D7        G       D
E nas lutas de cada dia,
D              G       A
Cristo nunca me deixa só;
D     D7         G       D
Pois Ele é meu seguro guia,
D    A      D
Ele só, Ele só.

D      D7        G       D
Não há amigo mais nobre e digno,
D         A           D
Não, não há; não, não há.

D      D7        G       D
Nem mais humilde e mais benigno,
D        A            D
Não, não há; não, não há.

D      D7        G       D
Ao pecador perdoar anela,
D   A       D
Ele só, Ele só;
D      D7        G       D
E pelos seus santos sempre vela,
D   A       D
Ele só, Ele só.

D      D7        G       D
Deus, em seu Filho, se há comprazido,
D   A       D
NEle só, nEle só;
D      D7        G       D
Mas sua glória me há repartido,
D    A        D
DEle só, dEle só.

D      D7        G       D
NEle nós temos um firme guia,
D     A       D
NEle só, nEle só;
D      D7        G       D
A noite enche de alegria,
D   A       D
Ele só, Ele só.

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
