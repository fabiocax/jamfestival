Harpa Cristã - A Minha Vida Jesus Já Transformou

      G                   G
A minha vida jesus já transformou
            D                          G
Já posso te saudar, com a paz do meu Senhor
      G                   G
A minha vida jesus já transformou
            D                          G
Já posso te saudar, com a paz do meu Senhor
          C                 G
Se tu me amas de todo o coração
               D                          G
Me mostra o teu sorriso e aperta a minha mão
          C                 G
Se tu me amas de todo o coração
               D                          G
Me mostra o teu sorriso e aperta a minha mão

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
