Harpa Cristã - Seu Precioso Sangue

Bb   F/C  Bb         F/C   Bb
jesus         me salvou        com seu  precioso
Cm7   F7     Cm7  F7            Cm7    F7
san---gue;  jesus      me comprou
                  Eb/Bb  Bb
com seu precioso  san---gue,
         F/C        Bb/D F/C  Bb
com seu poder   já   me   cu---rou;
       F/C   Bb/D Eb   G7/D Cm       E°
o meu peca---do   já   cra---vou na cruz!
 Bb  G7   Gm7   Bb/F    F7     Bb
jesus     p'ra   sempre me salvou!

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bb/D = X 5 X 3 6 6
Bb/F = X 8 8 7 6 X
Cm = X 3 5 5 4 3
Cm7 = X 3 5 3 4 3
Eb = X 6 5 3 4 3
Eb/Bb = X X 8 8 8 6
E° = X X 2 3 2 3
F/C = X 3 3 2 1 1
F7 = 1 3 1 2 1 1
G7 = 3 5 3 4 3 3
G7/D = X X 0 0 0 1
Gm7 = 3 X 3 3 3 X
