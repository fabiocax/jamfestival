Harpa Cristã - Jesus Cristo, Bem Amado

G                    D7          Am      D7    G
Glória ao salvador divino, nosso guia  e redentor,
                      D7     Am        D7       G
Glória seja ao deus trino! aleluia ao deus de amor!

       D7            G   A7           D7
Jesus cristo, bem amado,precioso salvador,
    G             D7          C      D7        G
Ele era ontem, é hoje, será sempre o deus de amor!

   G            D7        Am     D7  G
Alegrar-se todos devem pela grande salvação;
                         D7           Am     D7   G
Honra e glória a deus elevem, pois não dá seu galardão.

 G                  D7          Am  D7    G
Adorai a deus, com canto, ele é compassivo e bom,
                   D7         Am   D7       G
Ó divino espírito santo, desça sobre nós teu dom.

        G          D7       Am     D7    G
Ao teu nome seja glória, ó bendito e bom senhor!

                    D7    Am         D7  G
Tu nos guias p'ra vitória, aleluia ao redentor!

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
