Harpa Cristã - Fogo Divino

E             B         E                         F#7
Fogo divino, clamamos por ti; vem lá do alto, vem, desce
 B7      E                    B      E
AquI;o vem despertar-nos com teu fulgor:
         B                  E
Vem inflamar-nos com teu calor.

Refrão:
          A   E            F#m/E
Desce do al---to, bendito  fo---go,
 B                 E
Desce poder celestial!
         A    E           F#m/E
Desce do al---to, bendito fo---go,
 A          A/B      E
Vem, chama pentecostal!

E               B       E
Desce, espírito consolador, desce e enche-nos de santo
  B       E                B       E
Amor, desce ao mundo, nos mostra jesus;

                       B      E
Dá nos poder, vida, graça e luz.

Refrão:
          A   E            F#m/E
Desce do al---to, bendito  fo---go,
 B                 E
Desce poder celestial!
         A    E           F#m/E
Desce do al---to, bendito fo---go,
 A          A/B      E
Vem, chama pentecostal!

 E                     B        E                C#m
Arde em minh'alma, ó chama de amor  arde em meu peito e
 F#m7    B7     E              B7        E
Dá-me valor;consome todos os restos do mal;
  E/G#        C#m   B7    E
Desce já, ó fogo pentecostal

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/B = X 2 2 2 2 X
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
F#7 = 2 4 2 3 2 2
F#m/E = X X 2 2 2 2
F#m7 = 2 X 2 2 2 X
