Harpa Cristã - 291 - A Mensagem da Cruz

     F
Rude cruz se erigiu,
       Bb    Gm
Dela o dia fugiu,
       C        Bb         F    C
Como emblema de vergonha e dor;
       F
Mas contemplo esta cruz.
       Bb     Gm
Porque nela Jesus
      C        Bb       F
Deu a vida por mim, pecador.

        C                 F
Sim, eu amo a mensagem da cruz
     Bb        Gm        C    Bb
Té morrer eu a vou proclamar;
    F                   Bb    Gm
Levarei eu também minha cruz
       F     C      F    C
Té por uma coroa trocar.


        F
Desde a glória dos céus,
     Bb       Gm
O Cordeiro de Deus,
   C            Bb        F    C
Ao Calvário humilhante baixou;
     F
Essa cruz tem pra mim
    Bb        Gm
Atrativos sem fim,
       C      Bb        F
Porque nela Jesus me salvou.

      F
Nesta cruz padeceu
      Bb       Gm
E por mim já morreu,
      C         Bb          F    C
Meu Jesus, para dar-me o perdão
         F
E eu me alegro na cruz,
     Bb          Gm
Dela vem graça e luz,
     C        Bb    F
Para minha santificação.

    F
Eu aqui com Jesus,
     Bb       Gm
A vergonha da cruz
      C        Bb      F    C
Quero sempre levar e sofrer;
       F
Cristo vem me buscar,
      Bb      Gm
E com Ele, no lar,
    C        Bb            F
Uma parte da glória hei de ter.

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
