Harpa Cristã - Gozo Real

 E                      F#7              B7
Oh! que gozo real sinto no meu ser,  já tenho de deus
  E   B7              E                F#7
Perdão!       pois Jesus nos diz: que qualquer que crer
    B7      E   A
Recebe salvação

 E           C#7    F#7                B7
Se tu crês,   ó      vem  o  passo dar, vem o passo dar
     E                            E7   F#m
Sim, vem o passo dar! se tu crês, ó    vem o passo dar
 B7                  E   A E
Pois Jesus  te quer salvar

E                          F#7         B7         E   B7
Por amor foi Cristo, meu redentor,  da glória, aqui desceu
        E                F#7         B7           E  A E
Transforma a vida dum pecador,  na graça que nos deu

     E                F#7            B7
Que mercê, que amor o senhor mostrou morrendo na dura

 E B7          E            F#7          B7      E  A E
Cruz;sangue divinal por nós derramou,  o salvador Jesus!

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
C#7 = X 4 3 4 2 X
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#7 = 2 4 2 3 2 2
F#m = 2 4 4 2 2 2
