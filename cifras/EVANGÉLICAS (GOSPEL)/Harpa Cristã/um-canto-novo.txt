Harpa Cristã - Um Canto Novo

Ab               Bb          Eb                Ab
Há um canto novo no meu ser, É a voz de meu Jesus,
 Ab                     Bb      Eb                        Ab
Que me chama: "Vem em mim obter A paz, que eu ganhei na cruz".

Refrão:

 Ab              Eb                Ab
Cristo, Cristo, Cristo, nome sem igual;
Ab           Db    Eb               Ab
Enches o contrito, de prazer celestial.

Ab              Bb            Eb                 Ab
Preso no pecado eu me achei, Sem paz no meu coração;
 Ab                 Bb        Eb               Ab
Mas em Cristo eu já encontrei Doce paz e proteção.

Ab              Bb      Eb               Ab
Tenho Sua graça divinal,Sob as asas de amor,
Ab                   Bb         Eb                 Ab
E riquezas que fluem em caudal, Lá do trono do Senhor.


Ab                 Bb        Eb                   Ab
Pelas águas fundas me levou; Provas muitas encontrei;
 Ab               Bb       Eb                   Ab
Mas Jesus bendito me guiou Por Seu sangue vencerei.

 Ab                Bb       Eb                 Ab
Cristo, numa nuvem voltará, Baixará do céu em luz;
Ab                Bb       Eb                    Ab
Pelo Seu poder me levará, P'ra Seu lar, o bom Jesus.

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Bb = X 1 3 3 3 1
Db = X 4 6 6 6 4
Eb = X 6 5 3 4 3
