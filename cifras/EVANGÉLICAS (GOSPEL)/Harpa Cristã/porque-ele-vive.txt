﻿Harpa Cristã - 545 - Porque Ele Vive

Capo Casa 2

[Intro] G  G7/B  C  Cm

[Primeira Parte]

         G  G7/B             C   Cm
Deus enviou      seu filho amado
           G  Em        Am   D7
Pra me salvar    e perdoar
            G   G7/B             C   Cm
Na cruz morreu,      por meus pecados
           G     Em   Am    D     G   D7
Mas ressurgiu e vivo com o pai está

[Refrão]

           G     G7/B               C      Cm
Porque Ele vive,      posso crer no amanhã
           G     Em            Am  D7
Porque Ele vive,    temor não há
            G       G7/D
Mas eu bem sei, eu sei

             C    Cm
Que a minha vida
          G       Em    Am      D      G
Está nas mãos de meu Jesus que vivo está

[Segunda Parte]

           G     G7/B           C     Cm/D#
E quando enfim,       chegar a hora
          G    Em          Am  D7
Em que a morte    enfrentarei
            G   G7/B          C    Cm
Sem medo então,      terei vitória
           G        Em    Am       D      G  D7
Verei na glória, o meu Jesus, que vivo está

[Refrão]

           G     G7/B               C      Cm
Porque Ele vive,      posso crer no amanhã
           G     Em            Am  D7
Porque Ele vive,    temor não há
            G       G7/D
Mas eu bem sei, eu sei
             C    Cm
Que a minha vida
          G       Em    Am      D      G
Está nas mãos de meu Jesus que vivo está

[Segunda Parte]

           G     G7/B           C     Cm/D#
E quando enfim,       chegar a hora
          G    Em          Am  D7
Em que a morte    enfrentarei
            G   G7/B          C    Cm
Sem medo então,      terei vitória
           G        Em    Am       D      G  D7
Verei na glória, o meu Jesus, que vivo está

[Refrão]

           G     G7/B               C      Cm
Porque Ele vive,      posso crer no amanhã
           G     Em            Am  D7
Porque Ele vive,    temor não há
            G       G7/D
Mas eu bem sei, eu sei
             C    Cm
Que a minha vida
          G       Em    Am      D      G
Está nas mãos de meu Jesus que vivo está

                 G7/B               C      Cm
Porque Ele vive,      posso crer no amanhã
           G     Em            Am  D7
Porque Ele vive,    temor não há
            G       G7/D
Mas eu bem sei, eu sei
             C    Cm
Que a minha vida
          G       Em    Am      D      G
Está nas mãos de meu Jesus que vivo está

----------------- Acordes -----------------
Capotraste na 2ª casa
Am*  = X 0 2 2 1 0 - (*Bm na forma de Am)
C*  = X 3 2 0 1 0 - (*D na forma de C)
Cm*  = X 3 5 5 4 3 - (*Dm na forma de Cm)
Cm/D#*  = X X 1 0 1 3 - (*Dm/F na forma de Cm/D#)
D*  = X X 0 2 3 2 - (*E na forma de D)
D7*  = X X 0 2 1 2 - (*E7 na forma de D7)
Em*  = 0 2 2 0 0 0 - (*F#m na forma de Em)
G*  = 3 2 0 0 0 3 - (*A na forma de G)
G7/B*  = X 2 3 0 3 X - (*A7/C# na forma de G7/B)
G7/D*  = X X 0 0 0 1 - (*A7/E na forma de G7/D)
