Harpa Cristã - Além do Nosso Entendimento

G                   E7         A7
Muito Além do nosso entendi - mento
D7                      G
Alto mais que todo pensamento
               E7      A7
Glorioso em su-blime entendo
D7             G  D7  G
É o amor de Deus sem par.

       Em  Am   A7      D D7
Grande A  amor!Amor de Deus!
  G       Am     A7     D
Enche a terra e enche o céu!
E7      Am   Cm    G
Grande amor!     Amor que brange
D7                G   C   G
A todo mundo e atinge a mim!

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
