Harpa Cristã - Deus Velará Por Ti

[Intro]  G  C  Am  B7  C  D  G

    G                   D7    G   D7                G
1- Não desanimes  deus  proverá  Deus  velará  por  ti
                      D7     G  D7                 G   G7
   Sob Suas asas  te  acolherá  Deus  velará  por  ti.

   C      D    C       G   E7          Am   D          G
   Deus  cuidará  de   ti  no  teu  viver, no  teu  sofrer
           C        Am      B7    C        D       G
   Seu  olhar  te  acompanhará  Deus  velará  por  ti.

   G                     D7       G   D7                 G
2- Se  o  coração  palpitar  de  dor  Deus  velará  por  ti.
                          D7       G   D7                 G  G7
   Tu  já  provaste  teu  terno  amor, Deus  velará  por  ti.

   G                     D7     G   D7                 G
3- Nos  desalentos  nas  provações, Deus  velará  por  ti.
                         D7     G    D7                G   G7
   Lembra-te  dele  nas  tentações, Deus  velará  por  ti.


    G                    D7    G   D7                 G
4- Tudo  o  que  pedes  ele  fará; Deus  velará  por  ti.
                          D7   G   D7                 G   G7
   O  que  precisas  não  negará, Deus  velará  por  ti.

   G                     D7      G    D7                G
5- Como  estiveres, não  temas  vem! Deus  velará  por  ti.
                            D7    G     D7                G   G7
   Ele  te  entende  e  te  ama  bem!  Deus  velará  por  ti.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
