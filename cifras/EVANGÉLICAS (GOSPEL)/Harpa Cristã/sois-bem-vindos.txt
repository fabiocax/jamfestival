Harpa Cristã - Sois Bem-vindos

A
Nossas vozes jubilosas
D B7           E7
Elevamos com fervor
A
Pela vinda amistosa
E            B7    E7
Dos obreiros do Senhor.

Bm        E7             A
Sois bem vindos,sois bem vindos,
F#m  D       C# C#4 C#
campeões de Jeová!
A                 A7   D
Parabéns,mas não FINGI DOS
Bm         E7     A
a congregação te dá.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C# = X 4 6 6 6 4
C#4 = X 4 4 1 2 X
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
