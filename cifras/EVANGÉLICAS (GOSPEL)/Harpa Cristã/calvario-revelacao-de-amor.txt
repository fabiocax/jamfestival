Harpa Cristã - Calvário, Revelação de Amor

 F                                      D          Gm     C       F
No Calvário contemplamos  Nosso meigo Salvador! Oh! revelação de amor!
F                                            D         Gm    C         F     C
O mistério há tanto oculto,  Deus em Cristo revelou! Oh! revelação de amor!

Coro:
            F               C        F     C
Calvário! - Calvário!  - Oh! grande dor!
            F              Gm     C        F
Calvário! - Calvário!  - Oh! revelação de amor!

 F                                      D           Gm     C       F
Deus amou a este mundo  E seu Filho ofereceu.  - Oh! revelação de amor!
 F                                         D            Gm     C       F
O Senhor, na cruz morrendo,  De nós se compadeceu!  - Oh! revelação de amor!

 F                                              D          Gm     C       F
Deus, o Pai, olhou o Calvário  E seu Filho abandonou!  - Oh! revelação de amor!
 F                                               D          Gm     C       F
Mas sofrendo este abandono,  Cristo deu-nos seu perdão!  - Oh! revelação de amor!

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
