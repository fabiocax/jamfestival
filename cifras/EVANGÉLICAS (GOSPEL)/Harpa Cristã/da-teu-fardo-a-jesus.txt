Harpa Cristã - Dá Teu Fardo a Jesus

 F                   C7              Gm       C7       F  C
Só em jesus, queres tu descansar?   dá teu fardo a  Jesus
F                   C7           Gm       C7       F
 Paz e socorro não podes achar?   dá teu fardo a Jesus

        Bb/C      F          Bb          F           C7
Dá teu fardo a jesus,  as trevas fará em luz.  em  cristo um
         F                 Gm       C7      F  C
Abrigo, terás no perigo, ó dá teu fardo a Jesus

F              C7            Gm       C7          F        CF
Paz redentora tu'alma terá,  dá teu fardo a Jesus.    e  de
                   C7     Gm      C7        F
Inquietudes te libertará. dá teu fardo a Jesus

F                      C7            Gm       C7       F  C
Das grandes lutas não queiras fugir, dá teu fardo a  Jesus
 F                   C7        Gm       C7      F  C
Se santo gozo não podes fruir,dá teu fardo a Jesus

F                      C7            Gm     C7    F  C
Males te assaltam aos mil, pecador?  dá teu fardo a Jesus

 F                  C7              Gm       C7        F  C
Vais já cansado de penas e de dor?   dá teu fardo a  Jesus

F           C7                 Gm      C7     F  C
Cristo virá dissipar teu temor,  dá teu fardo a jesus
F                        C7           Gm      C7      F C
Pois, aos seus santos, dá sempre vigor, dá teu fardo a  Jesus

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bb/C = X 3 X 3 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
