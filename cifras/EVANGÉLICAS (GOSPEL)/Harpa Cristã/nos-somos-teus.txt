Harpa Cristã - Nós Somos Teus

G    C       G               D   A7   D
Fi--lho do eterno deus, nós so--mos teus!
G    C      G             D7      G
Com--pras--te-nos jesus, por tua cruz,

Rendemo-nos, senhor, cheios de fé e amor;
C         G     C     G    D7      G
Nós somos teus, fi--lho    de    deus!

G  C      G                D   A7      D
O teu caminho, ó deus, que le--va ao céu,
G    C       G     D7       G
Faze-nos percorrer até morrer.
C               G   D7                G
Não tem que recear todo o que nele andar;
C          G   C   G   D7     G
Tu és, senhor, o bom   pas---tor.

G   C       G            D   A7   D
A quem nos quis salvar vamos lou--var
G    C     G      D7           G
Com voz e coração, em santa união,

C                G      D7             G
Aceita, pois, senhor, nosso veraz louvor!
 C            G   C   G   D7     G
Graças, oh! deus, que so--mos teus!

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
