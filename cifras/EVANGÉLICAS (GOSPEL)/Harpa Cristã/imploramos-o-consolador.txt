Harpa Cristã - Imploramos o Consolador

G                    D                   G
Vem, ó senhor, e completa a obra tua em mim
                      D                  G
Torna a minha alma dileta, enchendo-me de ti

          D                 C  D
Ó manda agora, o bom consolador
               D                     G
Minh'alma te implora, o meu amado senhor!

G                    D                 G
Há tanto tempo te espero, ó caro salvador
                      D                 G
Mas hoje e mesmo eu quero o bom, consolador

                    D                  G
Tens me tirado p'ra fora  do mundo sedutor
                 D                 G
Ó vem selar-me agora glorioso redentor

                 D                  G
A ti pertence a glória, benigno salvador

                    D                     G
Sempre me dás a vitória, por teu imenso amor

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
