Harpa Cristã - Trabalhadores do Evangelho

D7M     Em7     F#m7    Em7
Trabalhadores do evangelho,
D7M             Em7
Em breve ceifareis;
A4                      A
Quer fracos vós sejais, ou velhos,
    Em7         A
Do céu vigor tereis.
coro
D7M
Os que esperam no senhor
D7M                     Em7
Novas forças obterão;
Em              A4      A
Como águias voarão,
G7M             A4      A
Subirão para as alturas;
D7M     Em7     F#m7    Em7
Correrão sem se cansar,
D7M             D#°     Em7
Sem fatigar hão de andar;

Em7             A4      A
Correrão sem se cansar,
G7M             C       D7M
Sem fatigar hão de andar;
Am      D7      G7M     Gm7
Corrarão sem se cansar,
D7M             A       D7M
Até no céu chegar!
D7M     Em7             F#m7    Em7
No bom trabalho, quanas vezes,
D7M             Em7
Estamos a falhar;
A4                      A
Sempre devemos nos revezes,
G7M     C       A
Em cristo confiar.
D7M     Em7     F#m7    Em7             A
Jesus está ao nosso lado,
D7M             Em7
Sua força nos dará;
A4              A
O nosso salvador amado,
        G7M     A
Sim, tudo suprirá.
D7M       Em7       F#m7        Em7
Em cristo sempre confiando,
D7M             Em7
Socorro vamos ter;
A4                      A
O salvador do séu baixando,
G7M     C       A
Virá nos socorrer!

----------------- Acordes -----------------
A*  = X 0 2 2 2 0 - (*G na forma de A)
A4*  = X 0 2 2 3 0 - (*G4 na forma de A4)
Am*  = X 0 2 2 1 0 - (*Gm na forma de Am)
C*  = X 3 2 0 1 0 - (*A# na forma de C)
D#°*  = X X 1 2 1 2 - (*C#° na forma de D#°)
D7*  = X X 0 2 1 2 - (*C7 na forma de D7)
D7M*  = X X 0 2 2 2 - (*C7M na forma de D7M)
Em*  = 0 2 2 0 0 0 - (*Dm na forma de Em)
Em7*  = 0 2 2 0 3 0 - (*Dm7 na forma de Em7)
F#m7*  = 2 X 2 2 2 X - (*Em7 na forma de F#m7)
G7M*  = 3 X 4 4 3 X - (*F7M na forma de G7M)
Gm7*  = 3 X 3 3 3 X - (*Fm7 na forma de Gm7)
