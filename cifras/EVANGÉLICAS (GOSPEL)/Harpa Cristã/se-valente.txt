Harpa Cristã - Sê Valente

INTRO: G G7 C Am G/D D G

G                   C        G
Na batalha contra o mal,sê valente!
         D                   D7
Seque em marcha triunfal,sê valente!
        G          G7
Olha o alvo que é Jesus,
       C             Am
Que a vitoria te conduz;
       C         G        D   G
Ó não deixes tua cruz,sê valente!

Refrão:
               Em          D
Sê valente!Pelejando por Jesus,
    Am             D          G
Sê valente!Nunca rejeitando a cruz!
                  G7
Firme sempre no amor,
     C        Am
Com idômito valor,

      C          G         D  G
Cheio do ConsoladoR, SÊ VALE NTE!

 G                   C         G
Se o maligno t’enfrentar, sê valente!
    D                    D7
Lutarás sem recuar, sê valente!
      G            G7
Seja aqui, ou onde for,
C             Am
Escudado no Senhor.
      C           G         D  G
Mostrarás o teu valor; Sê valente!

 G                  C        G
Co’altruísmo, com poder,sê valente!
        D                     D7
Franco, sem o mal temer, sê valente!
      G         G7
Aos caídos em redor.
    C             Am
Manifesta-lhes o amor;
    C           G         D  G
E serás um vencedor; Sê valente!

  G                 C         G
O Evangelho a proclamar, sê valente!
      D                          D7
No Brasil, em terra ou mar, sê valente!
    G          G7
Tua vida enobrecer!
       C           Am
Sempre com Jesus viver,
    C            G         D  G
E a ti também vencer; Sê valente!

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G/D = X 5 5 4 3 X
G7 = 3 5 3 4 3 3
