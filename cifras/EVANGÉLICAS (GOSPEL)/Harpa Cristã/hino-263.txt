Harpa Cristã - Hino 263

INTRO:A A7 D C A/C# E7 A

A                      E7
O culto tendo já terminado,
                      A
Te suplicamos, ó Salvador,
                          A7    D
Se em qualquer coisa foi perturbado,
     C     A/C#       E7   A
Que nos perdões, por Teu amor.


            A             E7
Gravar Tu queiras a sã doutrina,
                         A
Por Teu Espírito Consolador,
                      A7     D
Que nos sustenta, que nos ensina,
   C    A/C#        E7   A
E que nos enche do Teu amor.



         A                E7
Se nós aqui não mais regressarmos,
                         A
Certeza temos de ver na luz
                    A7     D
Os santos juntos e contemplarmos
    C      A/C#    E7    A
No céu, um dia, o bom Jesus!

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A7 = X 0 2 0 2 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
