Harpa Cristã - Opera Em Mim

D       A             A              D     A          D
Eis-me a teus pés, senhor, opera em mim! espero mais vigor
   A       D   A        D         G     D        A
Opera em mim!  eu gozo vou fruir se tua lei cumprir
D   A           D     A       D
O   dá-me teu amor: opera em mim!

D     A           A              D   A          D
Sedento, com ardor, opera em mim! te peço mais amor, opera
A      D  A        D        G    D        A
Em mim!  tu só me podes dar a perfeição sem par, ó bom
D A          D    A      D
Consolador: opera em mim!

D      A           A             D    A         D
Ó meu senhor jesus, opera em mim! por tua santa luz, opera
A      D  A       D        G    D       A
Em mim! revela-me o valor da morte do senhor
D A           D    A      D
Por mim, na dura cruz; opera em mim!


D     A            A              D   A               D
Não tardes mais, senhor, opera em mim! escuta o meu clamor
 A      D A        D        G    D       A
Opera em mim! no sangue de jesus, vertido lá na cruz
D A          D     A      D
Me guarda por favor, opera em mim!

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
