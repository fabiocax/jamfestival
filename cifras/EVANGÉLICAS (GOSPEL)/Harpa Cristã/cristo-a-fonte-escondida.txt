Harpa Cristã - Cristo, a Fonte Escondida

C                             F
Cristo nos deu uma fonte escondida.
G                           C   G
Que qualquer sede pode estancar.
C                         F     Fm
E cujas águas repletas de vida,
C                   G         C   G
Sempre de graça nos hão de fartar.

C           F
Água sairá, viva manará
   C                   Am              Bb   G
Da fonte que abriu o Senhor, água correrá!
   C                                  F   Fm
Se todos quiserem beber, não se esgotará;
  C                  G                 C   G   ( C )
Milhares de milhares venham; sempre sairá!

C                           F
Há muitas fontes de gozo no mundo,
G                            C   G
Nelas os homens vão sempre beber,

C                          F      Fm
Mas são cisternas e poços imundos,
C                   G        C   G
Que águas vivas não podem conter.

C                           F
Já muitos homens gastaram dinheiro,
G                        C    G
Sem a fonte de Deus encontrar;
C                            F     Fm
Mas Jesus Cristo é o nosso roteiro,
C                     G        C   G
Que para a fonte, nos há de guiar.

C                        F
Oh, Aleluia ao bom Pai supremo,
G                              C   G
Pelas bênçãos que a todos quer dar,
C                           F     Fm
E pela fonte de paz que nós temos,
C                  G      C    G
Em nosso ser, para nos alegrar!

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
