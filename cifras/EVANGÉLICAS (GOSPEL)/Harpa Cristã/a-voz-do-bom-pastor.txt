Harpa Cristã - A Voz do Bom Pastor

G                D4     D                  C/G   G
Escuta a voz do bom pastor em esplendorosa solidão,
                      G7   C               G   D     G
Chama ao cordeiro que em temor  vaga na densa escuridão.

Refrão:
         C        G          Em     D     G    C
Hoje vem a jesus, ao bondoso salvador, receber a sua luz.
 G                   D  C
Vem a cristo, o bom pas--tor.

G                D4  D                  C/G  G
Quem ajudar quer a jesus, os pecadores procu--rar, vá
           G7  C       G      D    G
Espalhar a sua luz, o evangelho proclamar.

G               D4   D             C/G   G
Triste deserto o mundo é, cercado de perigos mil; vem,
               G7     C            G    D  G
Chama cristo, vem com fé, vem para dentro do redil.


G              D4   D                       C/G   G
Escuta hoje, o bom pastor, vem no aprisco te   a--brigar,

Foge do lobo enganador, pois ele quer te devorar.

----------------- Acordes -----------------
C = X 3 2 0 1 0
C/G = 3 3 2 X 1 X
D = X X 0 2 3 2
D4 = X X 0 2 3 3
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
