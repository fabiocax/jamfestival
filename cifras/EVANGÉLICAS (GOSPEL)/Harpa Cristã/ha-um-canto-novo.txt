Harpa Cristã - Há Um Canto Novo

 G                    A7
Há um canto novo no meu ser
D           D9   G
É a voz de meu Jesus
G                        A7
Que me chama "vem em mim obter
D            D9        G
a paz que eu Ganhei na Cruz".

Refrão:
G                  D             G
Cristo,Cristo,Cristo nome sem iqual
G               C  D        D7     G
enches o contrito  de prazer cesletial.

G                    A7
Preso no pecado eu me achei,
D           D9     G
Sem paz no meu coração;
G                       A7
Mas em Cristo eu já encontrei

D          D9   G
Doce paz e proteção.

G                 A7
Tenho Sua graça divinal,
D           D7  G
Sob as asas de amor,
G                        A7
E riquezas que fluem em caudal,
D           D9   G
Lá do trono do Senhor.

G                    A7
Pelas águas fundas me levou;
D             D7   G
Provas muitas encontrei;
G                     A7
Mas Jesus bendito me guiou
D              D7   G
Por Seu sangue vencerei.

G                      A7
Cristo, numa nuvem voltará,
D          D7     G
Baixará do céu em luz;
G                   A7
Pelo Seu poder me levará,
D               D7    G
P'ra Seu lar, o bom Jesus.

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
D9 = X X 0 2 3 0
G = 3 2 0 0 0 3
