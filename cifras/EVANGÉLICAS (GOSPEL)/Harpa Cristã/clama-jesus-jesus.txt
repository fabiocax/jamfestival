Harpa Cristã - Clama: Jesus, Jesus!

G                                D7
O clama na tribulação: Jesus, Jesus,
     Am           D7      Am  D7   G
De todo o teu coração: Jesus, Jesus,
                           G6  G  C
Por certo, te socorrerá: Jesus, Jesus,
   C#º            G/D    D7     G
E gozo e paz te dará: Jesus, Jesus.

     G                             D7
Tu queres um bom salvador? Jesus, Jesus,
    Am         D7     Am D7    G
Que ama o vil pecador? Jesus, Jesus,
                              G6    G C
E que tem poder p'ra salvar? Jesus, Jesus,
   C#º          G/D   D7      G
E para o céu te levar? Jesus, Jesus.

    G                              D7
Há um que te pode curar: Jesus, Jesus.
  Am             D7         Am D7    G
E da corrupção te guardar.   Jesus,   Jesus.

                         G6  G  C
Só em nome da salvação: Jesus, Jesus.
   C#º           G/D     D7     G
O gozo, a paz, o perdão. Jesus, Jesus.

   G                            D7
Aceita, no teu coração: Jesus, Jesus;
  Am               D7     Am D7   G
Assim, tu terás salvação, Jesus, Jesus;
                         G6 G   C
Abraça, com terno amor: Jesus, Jesus;
     C#º             G/D   D7    G
Que chora por ti, pecador,Jesus,Jesus.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C#º = X 4 5 3 5 3
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
G/D = X 5 5 4 3 X
G6 = 3 X 2 4 3 X
