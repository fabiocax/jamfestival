Harpa Cristã - Olhai Pra o Cordeiro de Deus

Intro:  D D7 G Em D A D A

D                 G      A      D                    Bm   E7        A7
Livres de pecado vós quereis ficar?  Olhai prá o Cordei - ro de Deus!
D                 G        A        D   F#m       G      A        D      A
Ele morto foi na cruz, prá vos salvar;  Olhai prá o Cordeiro de Deus!

  D               Bm         A7
Olhai p'ra o Cordeiro de Deus,
   Em             A         D
Olhai p'ra o cordeiro de Deus,
   D7          G          Em
Porque só Ele vos pode salvar.
     D            A         D
Olhal p'ra o Cordeiro de Deus!

D                   G    A     D                   Bm   E7      A7
Se estais tentados, em hesitação,  Olhai prá o Cordei - ro de Deus!
D             G     A     D    F#m   G         A        D     A
Ele encherá o vosso coração.  Olhai prá o Cordeiro de Deus!


D                    G    A       D                   Bm   E7      A7
Se estais cansados e sem mais vigor,  Olhai prá o Cordei - ro de Deus!
D                G     A        D    F#m  G           A        D     A
Ele vos quer dar Seu divina! amor,  Olhai p'ra o Cordeiro de Deus!

D                 G     A       D                    Bm   E7      A7
Se na vossa senda sombras vêm cair,  Olhai prá o Cordei - ro de Deus!
D                   G    A        D    F#m   G           A        D     A
Ele, com Sua graça, tudo quer suprir.  Olhai p'ra o Cordeiro de Deus!

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
