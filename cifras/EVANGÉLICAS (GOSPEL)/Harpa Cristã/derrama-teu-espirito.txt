Harpa Cristã - Derrama Teu Espírito

  C                  F   G4    G                       C
Derrama sobre nós o teu espírito,gomo fizeste em jerusalém;
                F            C             G7
Á tua grei, ó manda o mesmo fogo, indispensável para nós
    C                     F           C
Também!  à tua grei, ó manda o mesmo fogo,
     G7                   C
Indispensável para nós também!

 C                 F   G4  G                   C
Derrama sobre nós o teu espírito como em casa do centurião,
                     F    C            G7
E dá-nos o poder da tua palavra fazendo a luz brilhar na
    C                         F       C
Escuridão. e dá-nos o poder da tua palavra
          G7                 C
Fazendo a luz brilhar na escuridão.

  C                F   G4 G
Derrama sobre nós o teu espírito, e dá-nos hoje, muitas
    C                   F             C
Conversões; ó deixa-nos sentir poder celeste,

      G7                 C                F            C
E vivifica os nossos corações; ó deixa-nos sentir poder celeste,
     G7               C
 e vivifica os nossos corações.

   C                   G4   G             F
Derrama sobre nós o teu espírito,e aos que sofrem dá tua
F  C                    G7 C                 G7
Proteção; a orar ficamos em amor unidos, para obter a
            C      F           C
Prometida unção,a orar ficamos em amor unidos,
       G7                C
Para obter a prometida unção.

C                             G4   G        F
Desperta, jesus cristo. os que dormem  o mui profundo sono
      C                    F          C
Do "jardim"; como operaste nos antigos tempos,
        G7                  C                 F
Com teu poder nos guia até o fim. como operaste nos antigos
  C            G7                  C
Tempos, com teu poder nos guia até o fim.

----------------- Acordes -----------------
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G4 = 3 5 5 5 3 3
G7 = 3 5 3 4 3 3
