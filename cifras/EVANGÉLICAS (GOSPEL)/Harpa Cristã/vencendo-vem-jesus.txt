Harpa Cristã - 525 - Vencendo Vem Jesus

     C
Já refulge a glória eterna
                     C7
De Jesus, o rei dos reis
         F            Fm
Breve os reinos deste mundo
     C           G
Seguirão as suas leis
     C
Os sinais da sua vinda
          Em          Am
Mais se mostram cada vez
   Dm     G     C        G
Vencendo vem Jesus!

C                     C7
Glória, glória, aleluia
F                  C    G
Glória, glória, aleluia
C                   E7  Am
Glória, glória, aleluia

   Dm     G     C     ( G )
Vencendo vem Jesus!
     C
O clarim que chama os crentes
               C7
À batalha já soou
          F
Cristo à frente do seu povo
     C              G
Multidões já conquistou
     C
O inimigo em retirada
     E7            Am
Seu furor já demonstrou
   Dm     G      C     G
Vencendo vem Jesus !
           C
Eis que em glória refulgente
                     C7
Sobre as nuvens descerá
       F
E as nações e os reis da terra
      C          G
Com poder governará
       C
Sim em paz e santidade
        E7       Am
Toda a terra regerá
   Dm     G     C     G
Vencendo vem Jesus !
       C
E por fim entronizado
                  C7
As nações irá julgar
        F
Todos grandes e pequenos
    C             G
O juiz hão de encarar
       C
E os remidos triunfantes
       E7         Am
Lá no céu irão cantar
   Dm     G   C       G
Venceu o rei Jesus!

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
