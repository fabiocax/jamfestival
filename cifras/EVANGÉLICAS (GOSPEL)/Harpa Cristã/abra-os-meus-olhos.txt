Harpa Cristã - Abra Os Meus Olhos

 G                    D7                       G
Abre os meus olhos em tua luz, teu rosto quero ver, jesus,
B7         Em    B7       Em   A7                    D
Põe em meu cora--ção teu amor, e dá-me a paz e teu fervor;
    G          D7                         G
Humildemente acudo a ti, porque tua terna voz ouvi
         G7    C       D7     G
Meu guia sê, espírito consolador.

G                 D7                      G
Abro os ouvidos para ouvir tua palavra a me ungir,
 B7       Em   B7       Em   A7                  D
Belas palavras de doce amor, ó! meu bendito salvador,
   G                D7                         G
Consagro a ti meu frágil ser!  para cumprir o teu querer,
          G7     C      D7      G
Enche meu ser, espírito consolador.

 G             D7                      G
Abre-me a mente para ver o teu amor em gran poder;
B7         Em    B7      Em     A7                  D
Dá-me tua graça, dá-me vigor; faz-me na luta, vencedor;

G             D7                       G
Sê tu o meu refúgio aqui aumenta minha fé em ti,,
         G7      C      D7     G
Sustenta-me, espírito consolador.

G                D7                    G
Abre-me a porta divinal do teu palácio de cristal
B7         Em      B7      Em     A7                     D
Pois o teu lindo rosto, senhor, quero no céu ver, d'esplendor;
G            D7                           G
E quando lá enfim entrar, teu santo nome vou louvar
         G7     C      D7     G
Pelo teu bom espírito consolador.

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
