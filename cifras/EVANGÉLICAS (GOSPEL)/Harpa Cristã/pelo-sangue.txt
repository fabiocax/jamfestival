Harpa Cristã - 192 - Pelo Sangue

       C                        F
Pelo mundo brilha a luz,  desde que morreu jesus,
     C            F        G            C
Pendurado lá na cruz do calvário!  os pecados carregou
      F                       C           F         G
E de culpa nos livrou, com o sangue que manou, no calvário!


Refrão:
     F     Fm        Em   Am              Dm    G
Pelo san--gue, pelo san--gue, somos redimidos, sim
        C        C7          F   Fm         Em   Am
Pelo sangue carmezim;  pelo san--gue, pelo san--gue,
       Dm        G          C
Pelo sangue de jesus, no calvário!


       C                        F
Antes, tinha mui temor,  mas, agora, tenho amor,
        C           F         G            C
Pois compreendo o valor do calvário;  eu vivi na perdição

      F                       C        F         G     C7
Mas achei a salvação  pela grande redenção: o  calvário


Refrão:
     F     Fm        Em   Am              Dm    G
Pelo san--gue, pelo san--gue, somos redimidos, sim
        C        C7          F   Fm         Em   Am
Pelo sangue carmezim;  pelo san--gue, pelo san--gue,
       Dm        G          C
Pelo sangue de jesus, no calvário!


       C                    F
És um grande pecador?  eis aqui teu salvador!
     C          F          G            C
Tema do bom pregador: o calvário, o cordeiro divinal
    F                        C            F        G     C7
Padeceu na cruz teu mal, e oferece graça tal, no calvário.


Refrão:
     F     Fm        Em   Am              Dm    G
Pelo san--gue, pelo san--gue, somos redimidos, sim
        C        C7          F   Fm         Em   Am
Pelo sangue carmezim;  pelo san--gue, pelo san--gue,
       Dm        G          C
Pelo sangue de jesus, no calvário!

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
