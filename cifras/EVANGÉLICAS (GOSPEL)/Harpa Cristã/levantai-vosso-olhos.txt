Harpa Cristã - Levantai Vosso Olhos

     C                        Am
Levantai os vossos olhos para cima,
    G
Ó remidos do Senhor Jesus!
    C                        F
A figueira mostra que se aproxima
    C           G     C     Bb
O verão: Brotos já produz!


Refrão:
F         C
Levantai, levantai!
       Dm                     G
Vossos olhos para o céu donde Jesus virá;
   F           C    Am
Levantai, levantai!
     Dm          G    C    G
A redenção breve se fará.



       C                       Am
Muitos dizem que Jesus está tardando,
     G
Para vir buscar o povo Seu;
         C                    F
Qual nos dias de Noé, estão pecando,
    C          G     C     Bb
Sem temer o bondoso Deus.


Refrão:
F         C
Levantai, levantai!
       Dm                     G
Vossos olhos para o céu donde Jesus virá;
   F           C    Am
Levantai, levantai!
     Dm          G    C    G
A redenção breve se fará.


        C                   Am
Muitos, como Faraó, estão dizendo:
        G
"Quem é o Senhor p'ra lhe ouvir?"
     C                 F
E também o coração endurecendo:
       C        G        C     Bb
Mas as pragas estão p'ra vir.


Refrão:
F         C
Levantai, levantai!
       Dm                     G
Vossos olhos para o céu donde Jesus virá;
   F           C    Am
Levantai, levantai!
     Dm          G    C    G
A redenção breve se fará.


     C                             Am
Ser arrebatado, eu, p'ro céu, quem dera!
        G
Pois a Igreja Cristo levará.
    C                           F
A figueira está em flor, é primavera,
     C             G     C     Bb
Levantai os vossos olhos já.


Refrão:
F         C
Levantai, levantai!
       Dm                     G
Vossos olhos para o céu donde Jesus virá;
   F           C    Am
Levantai, levantai!
     Dm          G    C    G
A redenção breve se fará.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
