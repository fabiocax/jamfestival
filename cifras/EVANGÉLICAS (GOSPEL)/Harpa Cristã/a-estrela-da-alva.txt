Harpa Cristã - A Estrela da Alva

C                             G               C
Mostra-te, estrela da manhã,  ó bondoso salvador!
         C°     F      Am   F       C         G7     C
 Sem tua luz, a vida é vã,  sem tua luz não temos vigor!
  F        C        G7     C
 Sem tua luz não temos vigor!
             G7  C       G7      Am    G/B
Ó estrela da al--va, estrela que sal---va
C           F       C       G7        C
A perdida alma que vaga sem ter direção!

 C                       G7              C
Cristo jesus é, do fiel, a estrela de fulgor
         C°   F  Am   F        C        G7        C
Também o bom emanuel, que de manhã nos enche de amor,
F        C         G7       C
Que de manhã nos enche de amor.

C                            G7              C
Bendita estrela dos hebreus, que o mundo iluminou,
        C°        F    Am     F      C        G7         C
E nos mandou os raios seus,  raios d'amor, que já nos salvou,

F         C       G7         C
Raios d'amor, que já nos salvou.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C° = X 3 4 2 4 2
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G7 = 3 5 3 4 3 3
