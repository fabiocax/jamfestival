Fernanda Brum - És Real Pra Mim

[Intro] G  D  G  D

         D
Como os rios querem encontrar o mar
        Bm
Como o pássaro quer encontrar o céu
                   G
Eu quero Te encontrar
           D
Quero Te encontrar

        D
Como a noite está esperando o Sol
        Bm
Como a terra está esperando a chuva
                G
Estou a Te esperar
            D
Estou a Te esperar

     G                                          (A)
Não sei como é o Teu rosto, nunca vi o Teu olhar

    Bm                         A9
Eu nunca Te toquei, mas meu desejo é Te abraçar
     G                         Em7
Não sei como explicar essa saudade de alguém
            D
Que eu nunca vi

   G                                         (A)
O vento é invisível mas eu sinto o seu soprar
      Bm                         A9
Sem questionar respiro o ar que não posso tocar
    G                           Em7
Eu não preciso ver pra crer que o Senhor
   D
É real

             Gmaj7
És real pra mim
Eu não Te vejo, mas me vês
             D
És real pra mim
Respiro o ar que o Senhor deu
             Gmaj7
És real pra mim
Ao som do mar posso Te ouvir
                    D
Não tem como Te explicar

            Gmaj7
O ar é invisível mas respiro
             Bm7
O som é invisível mas eu ouço
                     G
Tua presença eu não vejo
                     Em7                 D
Mas eu queimo por inteiro ao se aproximar de mim

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
D = X X 0 2 3 2
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
Gmaj7 = 3 X 4 4 3 X
