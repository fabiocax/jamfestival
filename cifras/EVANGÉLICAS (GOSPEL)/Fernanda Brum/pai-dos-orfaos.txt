Fernanda Brum - Pai Dos Órfãos

( F  C  Bb9  F )

F               Am Bb9           C
  Deus, eu não sei, dizer o que sinto
F               Am Bb9             C
  Deus, eu não sei, escrever a canção
Dm         Bb9          F             C
  Sei esperar, eu sei orar, e sigo ouvindo
Dm       Bb9      F                C
  A tua voz, que nunca me abandonará

F                     C9                   Dm
  Tu és pai dos órfãos, desce e resgata-nos
    Bb               F  C9
Preciso de Ti outra vez
F                   C9                                Dm
Amigo dos sem amigos, desce e visita-nos, desce e resgata-nos
    Bb               F
Preciso de Ti outra vez

F            Am  Bb9        C
  Quantas famílias choram agora

F            Am   Bb9                  C
  E quantos filhos, clamam pelos seus pais
Dm        Bb9           F               C
  A nossa dor Deus acolheu e trás na memória
Dm            Bb9      F                  C
  Ele derramará vinho novo nos filhos de Deus

F                     C9                   Dm
  Tu és pai dos órfãos, desce e resgata-nos
    Bb               F  C9
Preciso de Ti outra vez
F                   C9                                Dm
Amigo dos sem amigos, desce e visita-nos, desce e resgata-nos
    Bb               F   C
Preciso de Ti outra vez

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bb9 = X 1 3 3 1 1
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
