Fernanda Brum - Ele É Por Mim

Intro.: (D4 D) 3x G/B D/C G/B Bm7 D4 D

        D      G/B
Não faz mal eu sei que a dor do peito
       D              G/B
Cega a gente, quem um dia me ajudou
A9/C#    Bm7             Em7
Me   encolheu a mão e as lágrimas
  G/D          A4
Insistem em descer

     D       G/B
Tudo bem, eu sei em quem tenho crido
              D         G/B
E sei que Ele é fiel, jamais desamparou
A9/C#    Bm7             Em7       D/F#
Um filho seu e o que Ele confiou a mim
         G         D/F#          Em7
Eu vou levar até o fim pois eu estou
             D                 B5
Bem certo eu sei que Ele é por mim



         E         E/G#       A9  B5
Quem começou a boa obra é por mim
       E            E/G#        A9  B5
Aquele que venceu a morte é por mim
       C#m5         A9      D9
Aquele que subiu em glória, que mudou
  G              E  60 62 63 52
A história é por mim

     40  E         E/G#       A9  B5
Quem começou a boa obra é por mim
       E            E/G#        A9  B5
Aquele que venceu a morte é por mim
       C#m5         A9      D9
Aquele que subiu em glória, que mudou
  G              E  D9 C#m5 E 60 63 52
A história é por mim   (solo guitarra)

     40  E         E/G#       A9  B5
Quem começou a boa obra é por mim
       E            E/G#        A9  B5
Aquele que venceu a morte é por mim
       C#m5         A9      D9
Aquele que subiu em glória, que mudou
  G              E
A história é por mim
       C#m5         A9      D9
Aquele que subiu em glória, que mudou
  G              E
A história é por mim

----------------- Acordes -----------------
A = X 0 2 2 2 0
A4 = X 0 2 2 3 0
A9 = X 0 2 2 0 0
A9/C# = X 4 2 2 0 X
B5 = X 2 4 4 X X
Bm7 = X 2 4 2 3 2
C#m5 = X 4 6 6 5 4
D = X X 0 2 3 2
D/C = X 3 X 2 3 2
D/F# = 2 X 0 2 3 2
D4 = X X 0 2 3 3
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G/D = X 5 5 4 3 X
