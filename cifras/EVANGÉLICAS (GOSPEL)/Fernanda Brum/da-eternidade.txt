Fernanda Brum - Da Eternidade

Intro: Bbm  Db  Ab  Ebm

Primeira Parte:

 Bbm                     Gb
Deus que rasga o véu em meio à escuridão
   Ab                      Ebm       F
Revela os segredos do Seu Santo coração
 Bbm               Gb
Como esconder do amigo Abraão
     Ab                        Ebm      F
Os planos pra o futuro de uma nova geração?

Pré-Refrão:

  Ebm
Que não haja entre nós
    Gb
Segredos ou separação
      Ebm
Te escuto mesmo em silêncio

Gb            F
Um é o nosso coração

Refrão:

 Bbm         Gb
Fala comigo da eternidade
 Ab                  Ebm        F
Conta os mistérios que o mundo não entende
 Bbm               Gb
Rasga o silêncio, faz Tua vontade
 Ab             Ebm         F        Ebm  Gb
Fala ao ouvido desse meu aflito coração

(Repete Primeira Parte)

(Repete Pré-Refrão)

Refrão:

 Bbm         Gb
Fala comigo da eternidade
 Ab                  Ebm        F
Conta os mistérios que o mundo não entende
 Bbm               Gb
Rasga o silêncio, faz Tua vontade
 Ab             Ebm         F        Ebm  Eb7
Fala ao ouvido desse meu aflito coração

Base Solo:

Db/F  Gb  Eb7  Db  Ab/C
Gb  Db/F  Ebm  Db  Ab

Refrão:

 Bbm         Gb
Fala comigo da eternidade
 Ab                  Ebm        F
Conta os mistérios que o mundo não entende
 Bbm               Gb
Rasga o silêncio, faz Tua vontade
 Ab             Ebm         F        Ebm  Eb7
Fala ao ouvido desse meu aflito coração

Base Solo Final:

Db/F  Gb  Eb7  Db  Ab
Bbm

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Ab/C = X 3 X 1 4 4
Bbm = X 1 3 3 2 1
Db = X 4 6 6 6 4
Db/F = X 8 X 6 9 9
Eb7 = X 6 5 6 4 X
Ebm = X X 1 3 4 2
F = 1 3 3 2 1 1
Gb = 2 4 4 3 2 2
