Fernanda Brum - Águas do Trono

Intro: C9 G/B  G/Bb  F  C9    G  (G  A  B  D  C)

  C9     G/B           G/Bb          F
Louve ao cordeiro e tua vida a Ele entregue
       C9                 G                  (G  A  B  D  C)
Abra o coração levante as tuas mãos ele vai entrar
  C9     G/B             G/Bb          F
Deixe em ti fluir o Espírito de Deus
       C9                 G                  (G  A  B  D  C)
Abra o coração levante as tuas mãos ele vai reinar

  C9    G/B          G/Bb          F
As águas do trono te lava a iniquidade
  C9     G/B      G/Bb          F
O poder do sangue liberta de verdade
 C9       G/B       G/Bb          F
Oooooo..., uououo...., uououo..., Oooooo
 C9       G/B       G/Bb          F
Oooooo..., uououo...., uououo..., Oooooo
  C9     G/B            G/Bb          F
O amor de Deus é carinho é consolo

      C9                 G         ( G  A  B  D   C)
Sinta agora Deus todo o teu viver acariciar

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G/Bb = X 1 0 0 3 3
