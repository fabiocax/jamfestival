Fernanda Brum - Cura-me

[Intro] Bm  A  G  D/F#  Em  F#m  Bm

 Bm            G
Tanta amargura escondi
   Bm              G
O medo de não acertar
 Bm               G
Sonhos coloridos destruí
    Bm                     G
Que eu não quero mais lembrar
     Em
Não vou mais chorar
        A4     A
Foi o que decidi
     Em
Não vou mais sofrer
      G    A4  A
Pra que viver  assim

Bm
   Com imagens da infância

     G
Comecei a chorar
Bm
   Caí na caixa das lembranças
     G      A
Lembrei do Teu olhar
Bm
   Enchi meus olhos de esperança
     G
Comecei a cantar
Bm
   Entrei de vez naquela dança
     Em    A
Pra nunca mais voltar

 Bm            G
Cura-me em minhas lembranças
 Bm           G
Cura o meu altar
 Bm            G
Cura-me, sou Tua criança
      Em       A        Bm
Cura-me, cura-me, cura-me
      G      A     E4  E
Cura-me, Senhor Jesus
      G        A        Bm
Cura-me, cura-me, cura-me
      G      A     E
Cura-me, Senhor Jesus

Bm
   Com imagens da infância
     G
Comecei a chorar
Bm
   Caí na caixa das lembranças
     G      A
Lembrei do Teu olhar
Bm
   Enchi meus olhos de esperança
     G
Comecei a cantar
Bm
   Entrei de vez naquela dança
     Em    A
Pra nunca mais voltar

 Bm            G
Cura-me em minhas lembranças
 Bm           G
Cura o meu altar
 Bm            G
Cura-me, sou Tua criança
      Em       A        Bm
Cura-me, cura-me, cura-me
      G      A     E4  E
Cura-me, Senhor Jesus
      G        A        Bm
Cura-me, cura-me, cura-me
      G      A     E
Cura-me, Senhor Jesus

[Solo] G  A  Bm  G  A  E

E|--------------------------------------------------|
B|-10/12-12-14-14-15-15--13/15-17-17b19-17-15-17-15-|
G|--------------------------------------------------|
D|--------------------------------------------------|
A|--------------------------------------------------|
E|--------------------------------------------------|

E|-----------17-------17-------15-17-17b19-17b19r---|
B|-----17b19----17b19----17-------------------------|
G|--16----------------------------------------------|
D|--------------------------------------------------|
A|--------------------------------------------------|
E|--------------------------------------------------|

E|--------------------------------------------------|
B|--19b21-17----17--15b17------15b17----------------|
G|-----------19------------16---------16------------|
D|--------------------------------------------------|
A|--------------------------------------------------|
E|--------------------------------------------------|

 Bm            G
Cura-me em minhas lembranças
 Bm           G
Cura o meu altar
 Bm            G
Cura-me, sou Tua criança
      Em       A        Bm
Cura-me, cura-me, cura-me
      G      A     E4  E
Cura-me, Senhor Jesus
      G        A        Bm
Cura-me, cura-me, cura-me
      G      A     E
Cura-me, Senhor Jesus

[Final] G  A  Bm

----------------- Acordes -----------------
A = X 0 2 2 2 0
A4 = X 0 2 2 3 0
Bm = X 2 4 4 3 2
D/F# = 2 X 0 2 3 2
E = 0 2 2 1 0 0
E4 = 0 2 2 2 0 0
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
