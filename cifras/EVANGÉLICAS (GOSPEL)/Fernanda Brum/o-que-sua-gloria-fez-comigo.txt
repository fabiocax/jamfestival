Fernanda Brum - O Que Sua Glória Fez Comigo

[Intro]

E|---1---0---------------0---------------|
B|------------3----1----------3---1------|
G|---------------------------------------|
D|---------------------------------------|
A|---------------------------------------|
E|---------------------------------------|

E|-1---0---------------------------------|
B|----------1---------3----5----6--------|
G|---------------2-----------------------|
D|--------------------3----5----7--------|
A|---------------------------------------|
E|---------------------------------------|

E|---1---0---------------0---------------|
B|------------3----1----------3---1------|
G|---------------------------------------|
D|---------------------------------------|
A|---------------------------------------|
E|---------------------------------------|


E|-1--0----------------------------------|
B|--------1-------3----------------------|
G|------------2--------------------------|
D|----------------3----------------------|
A|---------------------------------------|
E|---------------------------------------|

[Intro] Bb9  C/Bb  Am6  Dm
        Bb9  C/Bb  Am6  Dm

Bb9            Gm7
    Eu me rasgo  por inteiro
Am6                Dm7
   Faço tudo, mas vem novamente
Bb9                 Gm7
    Eu mergulho na mirra ardente
Am6                      Dm7
    Mas peço que Tua presença aumente

( Bb9  Gm7  Am7/4 )
( Bb9  Gm7  Am7/4 )

E|----------------------------------|
B|-10-8-6-10/8----------------------|
G|--------------10-9-10-------------|
D|---------------------------10-12--|
A|-----------------------12---------|
E|----------------------------------|

E|----------------------------------|
B|-10-8-6-10/8----------------------|
G|--------------10-9-10-------------|
D|---------------------------10-12--|
A|-----------------------12---------|
E|----------------------------------|

Bb9            Gm7
    Eu me rasgo  por inteiro
Am                 Dm7
   Faço tudo, mas vem novamente
Bb9                 Gm7
    Eu mergulho na mirra ardente
Am                       Dm7
    Mas peço que Tua presença aumente

Bb9                      Gm7            C
    E se eu passar pelo fogo não temerei
                   Am               Bb9
Na Tua fumaça de glória eu entrarei
                     Gm7                   C4  C
 Longe do Santo dos Santos não sei mais viver

         Bb9                C/Bb
Quem já pisou no santo dos santos
            Am             Dm7
Em outro lugar não sabe viver
            Bb9             C
E onde estiver clama pela glória
             F4  F
A glória de Deus

  Bb9  C     Am  Dm   Bb9    C    Am  Dm
Gló___ória, glóóória, gló___ória, glóóória
 Bb9  C      Am  Dm Bb9  C     Am   Dm
Sa____anto, saaanto, sa____anto, saaanto
 Bb9 C      Am  Dm   Bb9 C      Am   Dm
Ka___adosh, kaaadosh, ka___adosh, kaaaadosh
 Bb9 C    Am   Dm7 Bb9 C    Am   Dm
Ho___oly, hooooly, Ho___oly, hooooly

         Bb9                C/Bb
Quem já pisou no santo dos santos
            Am             Dm
Em outro lugar não sabe viver
            Bb9             C
E onde estiver clama pela glória
             F4   F
A glória de Deus

  Bb9  C/Bb Am6  Dm   Bb9   C/Bb   Am6  Dm
Gló___ória, glóóória, gló___ória, glóóória
  Bb9  C/Bb Am6  Dm   Bb9   C/Bb   Am6  Dm
Gló___ória, glóóória, gló___ória, glóóória

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am6 = 5 X 4 5 5 X
Am7/4 = 5 X 5 5 3 X
Bb9 = X 1 3 3 1 1
C = X 3 2 0 1 0
C/Bb = X 1 2 0 1 X
C4 = X 3 3 0 1 X
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
F = 1 3 3 2 1 1
F4 = 1 3 3 3 1 1
Gm7 = 3 X 3 3 3 X
