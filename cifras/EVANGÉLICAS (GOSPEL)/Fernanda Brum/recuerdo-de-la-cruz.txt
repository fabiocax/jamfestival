Fernanda Brum - Recuerdo de La Cruz

   Am             E           Dm              F    G   C
Como vai a sua vida, é bom desabafar tudo o que sentes
     F                  Bm7/5-                E7  E4/7  E7
Não podes desanimar, precisa suportar até o fim
   Am             E           Dm              F    G   C
Imagine lá no Calvário, Jesus Cristo foi por ti crucificado
    F                        Bm                 E
Os cravos em suas mãos com amor no coração, tudo isso por você

Am     F/A        G       C          F          B            E   E/G#
É    difícil carregar a cruz que ele levou carregou sem reclamar
 Am    F           G      C            F
É impossível esquecer sua morte lá na cruz
      B        E         E/G#       Am
Lembranças de Jesus, lembranças de Jesus.

Am                E           Dm           F       G       C
Imagine lá no Calvário, Jesus Cristo foi por ti crucificado
    F                      Bm                    E
Os cravos em suas mãos com amor no coração, tudo isso por você


Am     F/A         G     C             F       B             E   E/G#
É    difícil carregar a cruz que ele levou carregou sem reclamar
 Am    F          G        C           F
É impossível esquecer sua morte lá na cruz
      B         E        E/G#        Am
Lembranças de Jesus, lembranças de Jesus.

Solo:  Bm G  A  D  G  C#m7/5-  F#7  F#/A#

 Bm     G         A     D               G      C#           F#   F#/A#
É    difícil carregar a cruz que ele levou carregou sem reclamar
 Bm    G          A       D            G
É impossível esquecer sua morte lá na cruz
    C#         F#      F#/A#         Bm
Lembranças de Jesus, lembranças de Jesus.

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
Bm7/5- = X 2 3 2 3 X
C = X 3 2 0 1 0
C# = X 4 6 6 6 4
C#m7/5- = X 4 5 4 5 X
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E4/7 = 0 2 0 2 0 X
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
F#/A# = 6 X 4 6 7 X
F#7 = 2 4 2 3 2 2
F/A = 5 X 3 5 6 X
G = 3 2 0 0 0 3
