Fernanda Brum - Tu Me Amas

Em         Em5+
 Levanta Messias
   D
 Assenta no trono
    C       G/B
 Levanta Messias
       Am7       Em
 Pois teu é o reino
       C
 Disseram jamais
       G/B
 Irias voltar
        C
 Te lançaram na cova
 G/B
 Feito dois ladrões
 C         G/B       Am      B9
 Mas é hora Messias de ressuscitar

 Em     D          C
 Vai, não peques mais

    G/B      Am
 Nem volte atrás
 G          F
 Judas morreu
 B9         Em
 Mas eu venci
  D      C
 Subi ao céu
 G/B        D
 E te perdoei

 Em    D        C
 Sai, vai navegar
 G/B         Am
 O mundo é o mar
   G          F
 Peixes são almas
  B9        Em
 Elas tem sede
  D         C
 Morrem de fome
 G/B      Am       B9
 Lança tua rede homem
 Em   Em5+    D
 Tu me amas Simão
            C
 Me amas Simão
             B9
 Me amas Simão Pedro
 Em    Em5+     D
 Tu és rocha Simão
             C
 És tocha Simão
                 B9
 E as chaves do Reino te dou
C     D             G/B
 Vai levar meus cordeiros
           Am     B9
 Aos pastos verdes
 Em            D
 Tu me amas Simão..

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
B9 = X 2 4 4 2 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
Em5+ = X X 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
