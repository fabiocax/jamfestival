Fernanda Brum - Sobrenatural

Intro:Bb  Gm   Bb  D#7  Bb  F Bb

Bb                   Gm
Tanto que lutei  tanto que eu chorei
   D#7         B/D              Cm            F
Mais em fim não me entreguei eu nunca estive só
     Bb                                  Gm                D#7
Milagres eu vivi  em meio a sequidão  mesmo eu prostado  ao chão
   B/D              Cm              F
Minha fé não entreguei.
  F+                      G         D#7     A#+   D#7                  G                                   F+
Deus em minha vida foi sobrenatural fez da minha história  além do que eu sonhei
  F+                   G             D#7            A#+  D#7                G           D#7              G
Hoje em meus dias entendo quem eu sou servo do Deus vivo,menina dos teus olhos,
    D#7        G        F+       A#+
a desejada noiva do senhor.

Sobe 1/2 tom:

  B+                        G#
Tanto que lutei  tanto que eu chorei

  E+                                   D#             C#              F#+
Mais em fim não me entreguei eu nunca estive só
  B+                                      G#                 E+             D#
Milagres eu vivi  em meio a sequidão  mesmo eu prostado  ao chão
   C#                         F#+
Minha fé não entreguei.
  F#+                  G#         E+           B+   E+                    G#                                 F#+
Deus em minha vida foi sobrenatural   fez da minha história além do que  eu sonhei
  F#+                 G#          E+                B+    E+                  G#
Hoje em meus dias entendo quem eu sou  servo do Deus vivo,
  E+                     G#        E+           G#     F#+       B+
Menina dos teus olhos  a desejada noiva do senhor
 B+             F#+                 G#
Eu aceito senhor  o teu  tempo
                     E+            F#+            B+
Todo teu ó senhor  é o meu tempo
  B+                  F#+
Tua vontade é que eu mais quero
 G#                 E+       F#+
Oh Deus fiel  Deus fiel.
 F#+                    G#        E+               B+
Deus em minha vida foi sobrenatural
  E+                    G#                               F#+
Fez da minha história além do que eu sonhei
F#+                  E+           F#+                B+
Hoje em meus dias entendo quem eu sou

   E+                  G#           E+                 G#
Servo do Deus vivo  menina dos teus olhos,
     E+             G#     F#+      B+
a desejada noiva do Senhor.

----------------- Acordes -----------------
A#+ = X 1 4 3 3 X
B+ = X 2 1 0 0 X
B/D = X 5 4 4 4 X
Bb = X 1 3 3 3 1
C# = X 4 6 6 6 4
Cm = X 3 5 5 4 3
D# = X 6 5 3 4 3
D#7 = X 6 5 6 4 X
E+ = 0 X 2 1 1 0
F = 1 3 3 2 1 1
F#+ = 2 5 4 3 2 X
F+ = X X 3 2 2 1
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
Gm = 3 5 5 3 3 3
