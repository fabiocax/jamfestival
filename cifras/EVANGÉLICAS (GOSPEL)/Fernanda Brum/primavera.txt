Fernanda Brum - Primavera

Intro: A#  D#  F  Gm  D#  (2x)

Gm          F6         D#   F6
 Quando chegar a primavera
Gm             F6                  D#   F6
 Milhares de flores vão surgir da terra
Gm            F6               D#   F6
 Florindo jardins, olhares, janelas
Gm                F6  Gm                F6
 Nos fazendo lembrar    que o dia está perto
               D#  F6
E vai logo chegar

D#          F
 E não haverá medo
D#         F
 A fome, doenças e guerras
Gm     F
 Acabarão preconceitos
Cm            F
 Seremos uma só nação

D#            Dm    D#           F
 Brancos, vermelhos, amarelos e negros
Cm        F
 Seremos todos irmãos

A#        Cm      Gm
 Dias melhores virão
        F4   F
Pode acreditar
A#        Cm             (2x)
 É só clamar ao Senhor
  Gm       F4 F           Bb
E Ele ouvirá   o teu coração

A#  D#  F  Gm  D#  (2x)

Gm      F6         D#
 No paraíso vai ter
        F6                Gm  F6  A#7+  F
A beleza, a essência da flor
     Gm  F6  A#7+  F
Da flor

Gm  F6  D#  F

A#  D#  F  Gm  D#  (4x)

----------------- Acordes -----------------
A# = X 1 3 3 3 1
A#7+ = X 1 3 2 3 1
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
D# = X 6 5 3 4 3
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
F4 = 1 3 3 3 1 1
F6 = 1 X 0 2 1 X
Gm = 3 5 5 3 3 3
