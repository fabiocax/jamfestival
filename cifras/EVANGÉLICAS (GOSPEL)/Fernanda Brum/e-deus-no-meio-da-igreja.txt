Fernanda Brum - É Deus No Meio da Igreja

Intr : Gm (G,G,F,D,F,G,D,F,G,Bb,G)       G, C/G, G, C/G, G, C/G, G (F,D,G)

       Gm (G,G,F,D,F,G,D,F,G,Bb,G)       G, F, D, Db, C, G, Gb, F... F, Gb G

                                                                                                    G                                                               F
Com alegria venho a casa de Deus...Com alegria dar o melhor de mim
            C                     G
Com alegria quero explodir em louvor...Com alegria meu louvor ao céu subirá.
                             F   C                  G
Com alegria chuva do céu cairá : É Deus no meio da igreja!!


    G    F   C   F           C     F                      G
Chuva    ,     glória ,     graça   : É Deus no  meio da igreja!!!
 G   F  C   F               C      F                    G
Cura   ,    vida   ,       fogo  :  É Deus no meio da igreja!!!      solo: Gm, Bb, C

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C/G = 3 3 2 X 1 X
D = X X 0 2 3 2
Db = X 4 6 6 6 4
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
Gb = 2 4 4 3 2 2
Gm = 3 5 5 3 3 3
