Fernanda Brum - Dia Feliz

Intro: Dm C9 Bb9 F (2X)

F                      Dm
Dia de sol dia de mar, Dia de amor de respirar
Gm                                      C9
Noite de Lua cheia ou vazia De aleluia, de alegria
F                       Dm
Dia feliz dia a cantar, O dom da vida que Deus nos dá
Gm                       C9
Esse é o dia que Ele fez Olhe para o céu louve outra vez
Dm                             C9
E o que há de bom nos seguirá, Tudo de bom nos guiará
Bb9                            C9
Não morreremos mas viveremos, Fomos criados para adorar

         F           Dm
Cristo Jesus A nossa luz
         Gm                 C9
Todos os dias Vem reinar Messias
         F             Dm         Gm
Cristo Jesus Pastor e guia Na melodia

          C9
Música Messias
Dm     C9
Vem Jesus
Bb9     F  Dm     C9    Bb9    F
Vem Jesus Meu Jesus A minha luz

----------------- Acordes -----------------
Bb9 = X 1 3 3 1 1
C9 = X 3 5 5 3 3
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
