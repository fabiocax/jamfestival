Fernanda Brum - Meu Eterno Namorado

[Intro] F  C  Bb  C  F  Dm  Bb

F                 C
Não há nada que você me faça
Gm            Bb
Prá que eu te ame mais
F            C
Não há que você me diga
Gm               Bb
Prá que eu te esqueça
Dm             C
Você é a maior prova
     Bb           F         Gm
Do carinho e do cuidado desse Deus
                      C
Que te fez prá ser só meu

F                     C
Depois de sonhar com teu sorriso
   Gm        Bb
Eu acordei e vi

F                 C
O teu nome estava bem guardado
    Gm          Bb
E protegido em Deus prá mim
Dm                C
   Você é o sonho mais real
     Bb          F
Que Deus me fez viver
    Gm             Bb
Gerações, futuras vão saber
      Dm      C
Sobre mim e você

F         C         Dm           Bb
As muitas águas não podem apagar
           F
O nosso amor
        C       Bb   F
Nem os rios afogá-lo
Gm           Dm           C
Onde você estiver é só chamar

Que eu também vou
      Gm
Meu amor
      Bb  C     F   ( F   C   Bb   Bb   C   F )
Meu eterno namorado                                     depois de sonhar...

      Bb  C     Dm  (da segunda vez)
Meu eterno namorado

Dm             C       Bb    F
      Corri na tua direção
Gm          F          C
    Na direção do teu sorriso
         Gm        F      C
Nos teus braços, no teu coração
         Gm           Dm    C   Eb   C#
Nos teus sonhos,   na tua visão

F#        C#        D#m      B
As muitas àguas nâo podem apagar
           F#
O nosso amor
        C#     B
Nem os rios afogá-lo
G#m          D#m           C#
Onde você estiver  é só chamar

Que eu também vou
     G#m                     B         C#     ( F#   C#   B   C# )
Meu amor, meu amor, meu eterno , meu eterno namorado

----------------- Acordes -----------------
B = X 2 4 4 4 2
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C# = X 4 6 6 6 4
D#m = X X 1 3 4 2
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
Gm = 3 5 5 3 3 3
