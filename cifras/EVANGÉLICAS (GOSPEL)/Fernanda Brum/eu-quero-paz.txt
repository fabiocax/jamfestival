Fernanda Brum - Eu Quero Paz

Intro: Am  C  D  Dm  Am  C  F  G

Am      G          F
 Não podia mais viver
          G            Am   G
Sem depender do Teu carinho
               F      G
Eu não ando sozinho não
Am     G           Dm
 Compreendo que viver
         Em          Am  G
É percorrer o Teu caminho
          F      G
Eu não desisto não

Am         C              Dm
 Eu quero paz, eu quero amor
                E
Eu quero ser feliz com meu Senhor
Am         Em             Dm
 Eu quero paz, eu quero amor

                E
Eu quero ser feliz

Am             G
 Sei que fico forte
          F           G
Quando estou com Jesus
                 Am           G
Sei que estou segura Ele me ajuda
     F              G
Ele me conduz, ele me conduz
Am            G
 Não faz diferença
          F        G
O que eu sempre fui
          Dm
Ele me mudou
       Em            F     G
E perdoou as minhas falhas

Am         C              Dm
 Eu quero paz, eu quero amor
                E
Eu quero ser feliz com meu Senhor
Am         Em             Dm
 Eu quero paz, eu quero amor
                E
Eu quero ser feliz

Am  G  F  G  (2x)

Am         G
 Nova criatura sou
        F                G
Eu sou de Jesus, eu sou de Jesus
Am         G      F      Em
 Nova criatura sou de Jesus

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
