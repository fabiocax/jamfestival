Oficina G3 - Consciência de Liberdade

Intro: A, G, A, G

A
Vejo tanta gente na sala de espera
      F#m
Disfarçando sua tensão
           G
São prisioneiros de guerra, Mesmo em tempos de paz,

     D
Esperando execução

 A
Passo sobre o muro, de olhos vendados
  F#m
Será que isso não tem fim?
        G
Mas alguém lá do alto trouxe a solução
 D
Ainda tem tempo pra mim!


                                                     A                  A4       A A9     F#m
Quero te dizer o segredo do sonho, de ver o que faz
    F#m6 F#m
 sentir
F#m G          D          C#m   F#m G          D
            Que há uma razão        pra ter um só
  C#m     F#m G D   A
caminho         ao céu....

A                   A4     A     A9
Existe tanta gente querendo aparecer
F#m                      F#m6 F#m
Mas não passam de seres humanos
    G               G4       G            D
O tempo passou e você não mudou os seus planos
   A                        A4    A       A9
Depois das nuvens cinzas o céu ainda é azul
     F#m              F#m6 F#m
A conversa com Deus é direta
      G
Num amor de verdade consegui encontrar
  D                    A
Consciência de liberdade...
      F#m    (E-F) G             D
liberdade...................liberdade.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A4 = X 0 2 2 3 0
A9 = X 0 2 2 0 0
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
F#m6 = 2 X 1 2 2 X
G = 3 2 0 0 0 3
G4 = 3 5 5 5 3 3
