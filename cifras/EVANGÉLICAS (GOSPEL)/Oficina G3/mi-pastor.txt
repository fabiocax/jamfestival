Oficina G3 - Mi Pastor

(Intro) E  A  E  F#  A
        E A E A

E
Mismo que luchas tristezas y opreciones
Abalem las estruturas de mi ser,
Mismo que cadenas vengan a me detener
y la obscuridad el medio dia aparecer.

A                       E
Puedo creer y te decir
   F#      A          E A E A
Puedo creer y responder

E
El Señor es mi Pastor nada me faltará
A
El Señor es mi Pastor
E
Nada me faltará
F#                      A
No temere mal alguno

             E
el está comigo

E
Mismo que vicios, el pasado y ls verguenza
vengan a traerme condenación
la pena fué cumplida la mano está extendida
es Jesús tu salida diga entonces:

Puedo creer....

E |------------------------------------------------------------|
B |------------------------------------------------------------|
G |--9/12-9-7-9--12-14-14-15-12-9---7~-7~-7~-7~-7~-7~-9-9~-----| (2x)
D |------------------------------------------------------------|
A |------------------------------------------------------------|
E |------------------------------------------------------------|

E |----------------------------------------------------------------------|
B |--12-17-15-12-10-12--10~-10~-10~--------------------------------------|
G |----------------------------------9-12-14--12-9--7~-7~/9--------------|
D |----------------------------------------------------------------------|
A |----------------------------------------------------------------------|
E |----------------------------------------------------------------------|

Mismo que vicios...

(Traduzido)

Meu Pastor

Mesmo que lutas, tristezas e opressões
Abalem a estrutura do meu ser.
Mesmo que cadeias venham me prender
E a escuridão ao meio-dia aparecer

Posso crer e te dizer
Posso crer e te responder
Que o Senhor é o meu Pastor
E nada me faltará
Que o Senhor é o meu Pastor
E nada me faltará
Não temerei mal algum
Ele está comigo

Ainda que os vícios, o passado e a vergonha
Venham me trazer condenação
A pena foi cumprida a mão está estendida
É Jesus sua saída, diga então:

Posso crer....

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
