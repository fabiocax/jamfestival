Oficina G3 - Naves Imperiais

[Intro] Bm  Em  G7M  A

E|-----x-9-10-12-9------------------------------------------------------|
B|---x-------------10--------------------------------10p7-------9/11~~--|
G|-x------------------9-7-6-7/9~~-------------6-7-9-------9-7-9---------|
D|-------------------------------------7-8-9----------------------------|
A|----------------------------------------------------------------------|
E|----------------------------------------------------------------------|

E|------------14-12-10-9-12-10-9-10p9--------------------9h-10-p9-------|
B|------10-12-------------------------10------------8/10-----------10~~-|
G|-(11)----------------------------------9-11/12------------------------|
D|----------------------------------------------------------------------|
A|----------------------------------------------------------------------|
E|----------------------------------------------------------------------|

 Bm
Somos como naves imperiais
   G                A
A serviço do nosso rei
 Bm
Vasos de guerra, onde só há ida

          G
E não há tempo para olhar
    A
Voltar para trás

 Bm                               G   A
Somos jovens que crêem no Deus vivo
  Em                  D           A
Que enviou Seu filho para nos salvar
      Bm  Em      G7M  A
Aleluia,  aa  aleluia  a  a

 Em                        D
Nosso comandante é Jesus Cristo
            A
Filho de Deus
      Bm  Em      G7M  A
Aleluia,  aa  aleluia  a  a

 Em                        D
Nosso comandante é Jesus Cristo
            A
Filho de Deus
      Bm  Em      G7M  A
Aleluia,  aa  aleluia  a  a

( Bm  Em  G7M  A )
( Bm  Em  G7M  A )

 Bm
Somos como naves imperiais
   G                A
A serviço do nosso rei
 Bm
Vasos de guerra, onde só há ida
          G
E não há tempo para olhar
    A
Voltar para trás

 Bm                               G   A
Somos jovens que crêem no Deus vivo
  Em                  D           A
Que enviou Seu filho para nos salvar
      Bm  Em      G7M  A
Aleluia,  aa  aleluia  a  a

 Em                        D
Nosso comandante é Jesus Cristo
            A
Filho de Deus
      Bm  Em      G7M  A
Aleluia,  aa  aleluia  a  a

 Em                        D
Nosso comandante é Jesus Cristo
            A
Filho de Deus
      Bm  Em      G7M  A
Aleluia,  aa  aleluia  a  a

 Em                        D
Nosso comandante é Jesus Cristo
            A
Filho de Deus

[Solo] Em  D  A
       Em  D  A
       Em  D  A
       Em  D  A

      Bm  Em      G7M  A
Aleluia,  aa  aleluia  a  a

 Em                        D
Nosso comandante é Jesus Cristo
            A
Filho de Deus
      Bm  Em      G7M  A
Aleluia,  aa  aleluia  a  a

 Em                        D
Nosso comandante é Jesus Cristo
            A
Filho de Deus
      Bm  Em      G7M  A
Aleluia,  aa  aleluia  a  a

( Bm  Em  G7M  A )

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G7+ = 3 X 4 4 3 X
