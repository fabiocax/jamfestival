Oficina G3 - Resposta de Deus

E9         F#m7             G          C9
Veio como um homem e entre nós Ele habitou
E9    F#m7          G       C9
E a sua morte a história mudou
C#m7     A            B7     G#m
Ensinou os homens a viver e amar
C#m  A      B7           G#m
E a vida eterna como alcançar.
E9     F#m7            G        C9
As suas palavras eram vida e poder
E9         F#m7        G          C9
E venceu a  morte por mim e por você
C#m7  A         B7     G#m              C#m
Nome sobre todo nome, Jesus o Filho de Deus
   A            B7        G#m          C#m
Autoridade e resposta, a todo que nEle crê.
C#m7  A        B7       G#m              C#m
Nome sobre todo nome, Jesus o Filho de Deus
     A         B7        G#m           A9    B7
Autoridade e resposta, a todo que nEle crê.


          A9        B7
O amor de Deus é Jesus
           A9         B7  C#m
Resgate de Deus, é Jesus
  A  B7  G#m  C#m A B7  G#m  C#m
Aleluia,   aleluia,    aleluia, aleluia
   A              B7        G#m         C#m
Nome sobre todo nome Jesus o filho de Deus
   A            B7       G#m          A9  B7
Autoridade e resposta a todo que nEle crê.

          A9       B7
O amor de Deus é Jesus
           A9        B7  C#m
ResgAté de Deus, é Jesus
            A9        B7
Resposta de Deus, é Jesus

Solo: C#m7  A9  B7  G#m

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
B7 = X 2 1 2 0 2
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
C9 = X 3 5 5 3 3
E9 = 0 2 4 1 0 0
F#m7 = 2 X 2 2 2 X
G = 3 2 0 0 0 3
G#m = 4 6 6 4 4 4
