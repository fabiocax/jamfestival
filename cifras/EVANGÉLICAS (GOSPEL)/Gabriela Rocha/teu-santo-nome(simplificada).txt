Gabriela Rocha - Teu Santo Nome

[Intro] C  G

[Primeira Parte]

              Am
Todo ser que vive
               Em
Louve ao nome do Senhor
          C
Toda criatura se derrame
     G
Aos Seus pés

               Am
Ao som da Sua voz
            Em
O universo se desfaz
              C
Não há outro nome
                      G
Comparado ao grande Eu Sou


[Pré-Refrão]

Am               C
  E mesmo sendo pó
              G
Com tudo que há em mim
    D
Confessarei
Am                  C
  Que céus e terra passarão
    G          D
Mas o Teu nome é eterno

[Refrão]

 Em               C
Todo joelho dobrará
              G
Ao ouvir Teu nome
           D
Teu Santo nome
 Em
Todo ser confessará
C                     G
  Louvado seja o Teu nome
           D
Teu santo nome

[Pré-Refrão]

Am               C
  E mesmo sendo pó
              G
Com tudo que há em mim
    D
Confessarei
Am                  C
  Que céus e terra passarão
    G          D
Mas o Teu nome é eterno

[Refrão]

 Em               C
Todo joelho dobrará
              G
Ao ouvir Teu nome
           D
Teu Santo nome
 Em
Todo ser confessará
C                     G
  Louvado seja o Teu nome
           D
Teu santo nome

 Em               C
Todo joelho dobrará
              G
Ao ouvir Teu nome
           D
Teu Santo nome
 Em
Todo ser confessará
C                     G
  Louvado seja o Teu nome
           D
Teu santo nome

 Em               C
Todo joelho dobrará
              G
Ao ouvir Teu nome
           D
Teu Santo nome
 Em
Todo ser confessará
C                     G
  Louvado seja o Teu nome
           D
Teu santo nome

[Final] C  Am  C

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
