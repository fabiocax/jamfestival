﻿Gabriela Rocha - Nossa Canção

Capo Casa 2

[Intro]  A9  D9  F#m  E4
         A9  D9  F#m  E4

A9   D9
  Somos estrangeiros
F#m               E4
   Cansados de andar

A9    D9
  Procurando abrigo
F#m            E4
   A terra, o lar

  A9
Que possamos ouvir
D9           F#m           E4
  Tua voz ecoando nos corações
 A9/C#              D9
Lança ao pó nossa glória
            F#m
Faz o teu trono

              E4
 Em nossa canção

( A9  D9  F#m  E4 )

A9      D9
  Somos o teu povo
F#m         E4
   Santa nação
A9   D9
  Sacerdócio Eterno
F#m             E4
   E pó desse chão

  A9
Que possamos ouvir
D9           F#m           E4
  Tua voz ecoando nos corações
 A9/C#              D9
Lança ao pó nossa glória
            F#m              E4
Faz o teu trono em nossa canção

    D9      A9/C#     F#m  E4
Alelu___ia,    alelu__-ia
    D9      A9/C#      E4
Alelu___ia,    Nosso   Deus

E|---------------------------------|
A|---------------------------------|
D|---------------------------------|
G|---------------------------------|
B|---10--9---7---5--10--9---7----9-|
E|---------------------------------|

E|---------------------------------|
A|---------------------------------|
D|---------------------------------|
G|---------------------------------|
B|---10--9---7---5--5---7----------|
E|---------------------------------|

             D9      A9/C#     F#m  E4
Cantamos alelu___ia,    alelu__-ia
    D9      A9/C#      E4
Alelu___ia,    Nosso   Deus

 A9              D9
Somos pó desse chão, Pai
            F#m              E4
Faz o teu trono em nossa canção
   A9/C#              D9
e lança ao pó nossa glória
            F#m              E4
Faz o teu trono em nossa canção

 A9              D9
Somos pó desse chão, Pai
            F#m              E4
Faz o teu trono em nossa canção
   A9/C#              D9
e lança ao pó nossa glória
            F#m              E4
Faz o teu trono em nossa canção

    D9     A9/C#     F#m  E4
Alelu___ia,    alelu__-ia
    D9     A9/C#      E4
Alelu___ia,    Nosso   Deus

             D9      A9/C#     F#m  E4
Cantamos alelu___ia,    alelu__-ia
    D9      A9/C#      E4
Alelu___ia,    Nosso   Deus

A9  D9            F#m            D9
      Faz o teu trono em nossa canção
         A9/C#  D9    F#m  E4
Lança o pó

----------------- Acordes -----------------
Capotraste na 2ª casa
A9*  = X 0 2 2 0 0 - (*B9 na forma de A9)
A9/C#*  = X 4 2 2 0 X - (*B9/D# na forma de A9/C#)
D9*  = X X 0 2 3 0 - (*E9 na forma de D9)
E4*  = 0 2 2 2 0 0 - (*F#4 na forma de E4)
F#m*  = 2 4 4 2 2 2 - (*G#m na forma de F#m)
