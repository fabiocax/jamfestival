Gabriela Rocha - Lugar Secreto

[Introdução 2x]  F  Am  G

[Primeira Parte]

F        Am                 G
  Tu és tudo o que eu mais quero
F        Am        G
  O meu fôlego, Tu és
F           Am         G
  Em Teus braços, é o meu lugar
        F       Am   G
Estou aqui,  estou aqui

F         Am          G
  Pai, eu amo Sua presença
F         Am             G
  Teu sorriso é vida em mim
F       Am           G
  Eu seguro em Suas mãos
           F       Am     G
Confio em Ti,  confio em Ti



[Refrão]

               F
Quero ir mais fundo
              Dm
Leva-me mais perto
              Am
Onde eu Te encontro
             Em
No lugar secreto
                  F
Aos Teus pés, me rendo
             Dm          Am  G
Pois a Tua glória quero ver

[Base Solo]
F  Dm  Am  Em
F  Dm  Am  G


[Primeira Parte]

F         Am          G
  Pai, eu amo Sua presença
F         Am             G
  Teu sorriso é vida em mim
F       Am           G
  Eu seguro em Suas mãos
           F       Am     G
Confio em Ti,  confio em Ti


[Refrão]

               F
Quero ir mais fundo
              Dm
Leva-me mais perto
              Am
Onde eu Te encontro
             Em
No lugar secreto
                  F
Aos Teus pés, me rendo
             Dm          Am  G
Pois a Tua glória quero ver

               F
Quero ir mais fundo
              Dm
Leva-me mais perto
              Am
Onde eu Te encontro
             Em
No lugar secreto
                  F
Aos Teus pés, me rendo
             Dm          Am  G
Pois a Tua glória quero ver


[Segunda Parte]

                    F
Tudo o que eu mais quero é Te ver
                     Dm
Me envolva com Tua glória e poder
          Am
Tua majestade é real
          Em
Tua voz ecoa em meu ser

                    F
Tudo o que eu mais quero é Te ver
                     Dm
Me envolva com Tua glória e poder
          Am
Tua majestade é real
          Em
Tua voz ecoa em meu ser

                    F
Tudo o que eu mais quero é Te ver
                     Dm
Me envolva com Tua glória e poder
          Am
Tua majestade é real
          Em
Tua voz ecoa em meu ser

                    F
Tudo o que eu mais quero é Te ver
                     Dm
Me envolva com Tua glória e poder
          Am
Tua majestade é real
          G
Tua voz ecoa em meu ser


[Refrão]

               F
Quero ir mais fundo
              Dm
Leva-me mais perto
              Am
Onde eu Te encontro
             Em
No lugar secreto
                  F
Aos Teus pés, me rendo
             Dm          Am  G
Pois a Tua glória quero ver

( F  Dm  Am  Em )
( F  Dm  Am  G )

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 3 3
