Gabriela Rocha - Ele Vem (Part. Gabriel Guedes)

[Intro] F#m  D9  A  E/G#
        F#m  D9  A  E/G#

[Primeira Parte]

        F#m       D9
Eu não vou me apegar
        A         E/G#
Com as coisas daqui
         F#m         D9
Pois eu sei, há um lugar
          A  E/G#
Que me espera
       F#m      D
Estrangeiro eu sou
       A       E/G#
O meu lar é o céu!
       F#m         D9
Meu Jesus, vem Buscar
       A    E/G#
A Sua Noiva


[Refrão]

       A
Ele vira num piscar de olhar
          E/G#
Quem estiver pronto com ele irá
         Bm
Na sua Glória com Ele morar
     F#m     E/G#
Ele vem Ele vem

              A
Não mais tristezas não mais temor
             E/G#
Junto com os anjos cantar eu vou
              Bm
Digno é o cordeiro que se entregou
     F#m    E/G#
Ele vem Ele vem

( F#m  D9  A  E/G# )
( F#m  D9  A  E/G# )

[Primeira Parte]

        F#m       D9
Eu não vou me apegar
        A         E/G#
Com as coisas daqui
         F#m         D9
Pois eu sei, há um lugar
          A  E/G#
Que me espera
       F#m      D
Estrangeiro eu sou
       A       E/G#
O meu lar é o céu!
       F#m         D9
Meu Jesus, vem Buscar
       A    E/G#
A Sua Noiva

[Refrão]

       A
Ele vira num piscar de olhar
          E/G#
Quem estiver pronto com ele irá
         Bm
Na sua Glória com Ele morar
     F#m     E/G#
Ele vem Ele vem

              A
Não mais tristezas não mais temor
             E/G#
Junto com os anjos cantar eu vou
              Bm
Digno é o cordeiro que se entregou
     F#m    E/G#
Ele vem Ele vem

[Ponte]

 D9
Tu és Senhor

O nosso Deus
 E
Digno tu és De receber
 F#m
Todo Louvor, Vamos clamar
       D
Então vem, Então vem

 D9
Tu és Senhor

O nosso Deus
 E
Digno tu és De receber
 F#m
Todo Louvor, Vamos clamar
       D
Então vem, Então vem

[Refrão]

       A
Ele vira num piscar de olhar
          E/G#
Quem estiver pronto com ele irá
         Bm
Na sua Glória com Ele morar
     F#m     E/G#
Ele vem Ele vem

              A
Não mais tristezas não mais temor
             E/G#
Junto com os anjos cantar eu vou
              Bm
Digno é o cordeiro que se entregou
     F#m    E/G#
Ele vem Ele vem

[Ponte]

 D9
Tu és Senhor

O nosso Deus
 E
Digno tu és De receber
 F#m
Todo Louvor, Vamos clamar
       D
Então vem, Então vem

 D9
Tu és Senhor

O nosso Deus
 E
Digno tu és De receber
 F#m
Todo Louvor, Vamos clamar
       D
Então vem, Então vem

[Final]

A              A/C#     D           Bm
O Rei está voltando! O Rei está voltando!
       E          E/G#        D   A  Bm    A
A trombeta está soando, o meu nome irá chamar
        A          A/C#      D          Bm
Sim, o Rei está voltando! O Rei está voltando!
 E/G#             D    C#m7  Bm  A
Aleluia!     Ele vem  me    bus car!

A              A/C#     D           Bm
O Rei está voltando! O Rei está voltando!
       E          E/G#        D   A  Bm    A
A trombeta está soando, o meu nome irá chamar
        A          A/C#      D          Bm
Sim, o Rei está voltando! O Rei está voltando!
 E/G#             D    C#m7  Bm  A
Aleluia!     Ele vem  me    bus car!

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
Bm = X 2 4 4 3 2
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
F#m = 2 4 4 2 2 2
