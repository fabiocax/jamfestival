Gabriela Rocha - Cuida de Mim

 F                    Bb                 Dm7/9    Bb
Pai eu preciso tanto de um abraço Seu
           Dm7/9    Bb
Deus amigo meu
      F                                                  Bb
Escolho caminhos,mais sei que não são os Seus
                    Dm7/9     Bb
Mas não me deixa só
                Dm7/9      Bb
Segura a minha mão

Gm                              Dm7/9
Sei que tudo o que eu preciso está
Gm                        Dm7/9
No projeto que Tu tens pra mim
Gm                   Dm7/9
Vem Espírito de Deus me leva
Gm                     Dm
Pro caminho certo a seguir
 F        Bb    Bb7
Eu creio


 F                    Bb                 Dm7/9      Bb
Pai eu preciso tanto de carinho Seu
           Dm7/9    Bb
Deus amigo meu
 F                                      Bb
Pai eu tenho medo, do escuro
                    Dm7/9     Bb
Mas não me deixa só
                Dm7/9      Bb
Segura a minha mão

Gm                              Dm7/9
Sei que tudo o que eu preciso está
Gm                        Dm7/9
No projeto que Tu tens pra mim
Gm                   Dm7/9
Vem Espírito de Deus me leva
Gm                     Dm7/9
Pro caminho certo a seguir
 Fsus
Eu creio

  Cm
Que o Senhor é o meu pastor
                 Gm
E nada me faltará
 Cm
Que o Senhor é minha força
              Gm
E eu sou fraco
Cm                                               Gm
Minha vida está em suas mãos agora
                Cm        Gm
Cuida de mim Pai, Cuida
               Cm        Gm
Cuida de mim Pai, Cuida
               Cm        Gm     Fsus
Cuida de mim Pai, Cuida


  Cm
Que o Senhor é o meu pastor
                 Gm
E nada me faltará
 Cm
Que o Senhor é minha força
              Gm          Fsus
E eu sou fraco
Cm                                               Gm
Minha vida está em suas mãos agora
                Cm        Gm
Cuida de mim Pai, Cuida
               Cm        Gm
Cuida de mim Pai, Cuida
               Cm        Gm
Cuida de mim Pai, Cuida
               Cm        Gm
Cuida de mim Pai, Cuida

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bb7 = X 1 3 1 3 1
Cm = X 3 5 5 4 3
Dm = X X 0 2 3 1
Dm7/9 = X 5 3 5 5 X
F = 1 3 3 2 1 1
Fsus = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
