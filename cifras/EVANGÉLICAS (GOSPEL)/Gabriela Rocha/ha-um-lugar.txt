Gabriela Rocha - Há Um Lugar

[Intro] C9  G9

         C9
Há um lugar de descanso em ti
         G9
Há um lugar de refrigério em ti
         C9            D/F#   Em7                   G9
Há um lugar onde a verdade reina, esse lugar é no Senhor

( C9  G9 )

        C9
Há um lugar onde as pessoas não me influenciam
        G9
Há um lugar onde eu ouço teu Espírito
        C9         D/F#          Em7                        G9
Há um lugar de vitória em meio à guerra, esse lugar é no Senhor

                    C9
 Esse lugar é no Senhor
                    G9
 Esse lugar é no Senhor

                    C9
 Esse lugar é no Senhor
                    G9
 Esse lugar é no Senhor

 ( C9  G9 )

        C9
Há um lugar onde a inconstância não me domina
        G9
Há um lugar onde minha fé é fortalecida
        C9          D/F#        Em7                       G9
Há um lugar onde a paz é quem governa, esse lugar é no Senhor

( C9  G9 )

         C9
Há um lugar onde os sonhos não se abortam
         G9
Há um lugar onde os medos nao te domina
         C9                D/F#            Em7
Há um lugar que quando se perde é que se ganha, esse lugar é no
   G9
Senhor

                   C9
Esse lugar é no Senhor
                   G9
Esse lugar é no Senhor
                   C9
Esse lugar é no Senhor
                   G9
Esse lugar é no Senhor

( C9 G9 )

C9
 És Tu es tudo o que eu preciso, Jesus!
G9
 És Tu es tudo o que eu preciso, Jesus!
C9
 És Tu es tudo o que eu preciso, Jesus!
G9
 És Tu es tudo o que eu preciso, Jesus!
           C9
De ti preciso!

----------------- Acordes -----------------
C9 = X 3 5 5 3 3
D/F# = 2 X 0 2 3 2
Em7 = 0 2 2 0 3 0
G9 = 3 X 0 2 0 X
