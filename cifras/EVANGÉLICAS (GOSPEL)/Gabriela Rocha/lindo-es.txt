Gabriela Rocha - Lindo És

[Intro] D#m

D#m
  Lindo, lindo, lindo és
B                          F#      C#
  Glória, glória eu Te dou,  Jesus,  Jesus
D#m
  Lindo, lindo, lindo és
B                          F#      C#
  Glória, glória eu Te dou,  Jesus,  Jesus
D#m
  Lindo, lindo, lindo és
B                          F#      C#
  Glória, glória eu Te dou,  Jesus,  Jesus

D#m
  Lindo, lindo, lindo és
B                          F#          C#
  Glória, glória eu Te dou,  eu te dou,  eu te dou

          D#m             B
Então Me leva a sala do trono

             F#
Mostra Tua beleza
               C#
Quero ver Tua face

         D#m             B
Então Me leva a sala do trono
             F#
Mostra Tua beleza
               C#
Quero ver Tua face
      D#m             B
Então Me leva a sala do trono
             F#
Mostra Tua beleza
               C#
Quero ver Tua face

D#m
  Lindo, lindo, lindo és
         D#m             B
Então Me leva a sala do trono
             F#
Mostra Tua beleza
               C#
Quero ver Tua face
          D#m             B
Então Me leva a sala do trono
             F#
Mostra Tua beleza
               C#
Quero ver Tua face

    D#m      F#
Santo ... Santo
        B             C#/F
É o Senhor Deus Poderoso
           G#m   F#           B
Digno de louvor, digno de louvor
         F#
Tu és Santo ... Santo
        B             C#/F
É o Senhor Deus Poderoso
            G#m   F#          B
Digno de louvor, digno de louvor
         F#
Tu és Santo ... Santo
        B             C#/F
É o Senhor Deus Poderoso
            G#m  F#           B
Digno de louvor, digno de louvor
         F#
Tu és Santo ... Santo
        B             C#/F
É o Senhor Deus Poderoso
            G#m   F#          B     F#
Digno de louvor, digno de louvor

----------------- Acordes -----------------
B = X 2 4 4 4 2
C# = X 4 6 6 6 4
C#/F = X 8 X 6 9 9
D#m = X X 1 3 4 2
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
