Kleber Lucas - Vou Seguir Com Fé

[Intro] G#7+  Gm  Gm7  C7+
        G#7+  Gm  Gm7  D#7+  F7+
        G#7+  Gm  Gm7  C7+
        G#7+  Gm7  F/G

       C7+                        Em7
O meu Deus é maior que os meus problemas
        F7+     F/G
Eu não temerei, com  Jesus eu vou além
 C7+                         Em7              F7+
Ainda que a figueira não floresça, e não haja frutos na videira
F/G              E/G#  Am7
Eu  não temerei, não, não

Am7                                  G/A                          F/A
  Pois sei que para além das nuvens,     o sol não deixou de brilhar
                           C9         G/B  Am7
  Só porque a terra escureceu,oh! oh! oh! oh! a minha vida está em Deus
G/A                           F/A     F/G
     Eu sei que tudo posso em Deus, é ele quem me fortalece


C7+       Em7                 F7+
Eu vou seguir com fé, com meu Deus eu vou
        F/G                C7+
Para a rocha mais alta que eu
           Em7            F7+
Eu sei prá onde vou, como águia vou
       F/G               C7+
Nas alturas sou filho de Deus

C7+       Em7                 F7+
Eu vou seguir com fé, com meu Deus eu vou
        F/G                C7+
Para a rocha mais alta que eu
           Em7            F7+
Eu sei prá onde vou, como águia vou
       F/G               C7+
Nas alturas sou filho de Deus

( G#7+  Gm  Gm7  C7+ )
( G#7+  Gm  Gm7  D#7+  F7+ )
( G#7+  Gm  Gm7  C7+ )
( G#7+  Gm7  F/G )

      C7+                      Em7           F7+
O meu Deus sabe tudo que eu preciso, prá sentir a paz
 F/G
Dentro do meu coração
 C7+                Em7                F7+
Ainda que a lua adormeça, e não haja o brilho das estrelas
F/G              E/G#  Am7
Eu  não temerei, não, não

Am7                                  G/A                          F/A
  Pois sei que para além das nuvens,     o sol não deixou de brilhar
                           C9         G/B  Am7
  Só porque a terra escureceu,oh! oh! oh! oh! a minha vida está em Deus
G/A                           F/A     F/G
     Eu sei que tudo posso em Deus, é ele quem me fortalece

C7+       Em7                 F7+
Eu vou seguir com fé, com meu Deus eu vou
        F/G                C7+
Para a rocha mais alta que eu
           Em7            F7+
Eu sei prá onde vou, como águia vou
       F/G               C7+
Nas alturas sou filho de Deus

C7+       Em7                 F7+
Eu vou seguir com fé, com meu Deus eu vou
        F/G                C7+
Para a rocha mais alta que eu
           Em7            F7+
Eu sei prá onde vou, como águia vou
       F/G               C7+
Nas alturas sou filho de Deus

( G#7+  Gm  Gm7  C7+  G#7+  Gm  Gm7  D#7+  F7+  G#7+  Gm  Gm7  C7+  G#7+  Gm7  F/G  G/A )

D7+       F#m7                 G7+
Eu vou seguir com fé, com meu Deus eu vou
        G/A                D7+
Para a rocha mais alta que eu
           F#m7            G7+
Eu sei prá onde vou, como águia vou
       G/A               D7+
Nas alturas sou filho de Deus

D7+       F#m7                 G7+
Eu vou seguir com fé, com meu Deus eu vou
        G/A                D7+
Para a rocha mais alta que eu
           F#m7            G7+
Eu sei prá onde vou, como águia vou
       G/A               D7+
Nas alturas sou filho de Deus

G/A                       D7+
Nas alturas sou filho de Deus
G/A                        Bb7+  Am7  D7+
Nas alturas sou filho de Deus

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Bb7+ = X 1 3 2 3 1
C7+ = X 3 2 0 0 X
C9 = X 3 5 5 3 3
D#7+ = X X 1 3 3 3
D7+ = X X 0 2 2 2
E/G# = 4 X 2 4 5 X
Em7 = 0 2 2 0 3 0
F#m7 = 2 X 2 2 2 X
F/A = 5 X 3 5 6 X
F/G = 3 X 3 2 1 X
F7+ = 1 X 2 2 1 X
G#7+ = 4 X 5 5 4 X
G/A = 5 X 5 4 3 X
G/B = X 2 0 0 3 3
G7+ = 3 X 4 4 3 X
Gm = 3 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
