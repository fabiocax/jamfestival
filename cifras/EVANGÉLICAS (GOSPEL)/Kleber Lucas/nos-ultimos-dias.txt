Kleber Lucas - Nos Últimos Dias


Em                                     C      G/B
Nos últimos dias será abundante a justiça
Am                D4       D             B7
Resplandecente na terra através da igreja
Em           D       C              D   B7
O livramento fluirá como tocha acesa
                                       C     G/B
Libertando dos cativos, curando os enfermos
Am               D4   D          B7/4     B7
Para o louvor da glória do Senhor
Em         D                 C              D        B7
Todas as nações verão, o livramento do Senhor
Em         D/E        C    D/E    Em    E4   E7
Através do poder que está sobre Sião
    Am       Am/G         D/F#       G              F#    B7    Em
E exaltarão o nome de Jesus,      e exaltarão o nome de Jesus
    C         D4         G  D  Em       Am            D4         G   C/E   G   C/E
E exaltarão o nome de Jesus,      e exaltarão o nome de Jesus

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am/G = 3 X 2 2 1 X
B7 = X 2 1 2 0 2
B7/4 = X 2 4 2 5 2
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
D = X X 0 2 3 2
D/E = X X 2 2 3 2
D/F# = 2 X 0 2 3 2
D4 = X X 0 2 3 3
E4 = 0 2 2 2 0 0
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
