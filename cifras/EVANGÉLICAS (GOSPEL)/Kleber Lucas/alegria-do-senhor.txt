Kleber Lucas - Alegria do Senhor

Intro: F#m A E F#m A

F#m                        D   E   F#m
O espírito de Deus se move em mim  Eu canto, pulo e louvo
F#m                      D     E   F#m
Eu sinto Seu poder me invadir   Eu canto, pulo e louvo
F#m                                 E
Eu sinto fogo nos meus pés (fogo em meus pés) Eu sinto fogo em minhas mãos (fogo em minhas mãos)
       Bm        C#m    D     E           F#m
Eu sinto fogo, sinto a glória Sinto o Seu poder em meu coração

F#m                                            E
A alegria do Senhor (A alegria do Senhor) A alegria do Senhor (A alegria do Senhor)
Bm            C#m  D   E       F#m
A alegria do Senhor         A Alegria

F#m                     D    E   F#m
Eu entendo Bartimeu Quando clamou Eu canto, pulo e louvo
F#m                       D    E  F#m
Eu entendo o Rei Davi Quando dançou Eu canto, pulo e louvo
Bm                F#m
Eu entendo Miriam Com o seu tamborim

Bm                F#m
Quando Deus nos dá vitória é assim
Bm                      C#m
Não dá pra esconder Eu canto, pulo e louvo

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
