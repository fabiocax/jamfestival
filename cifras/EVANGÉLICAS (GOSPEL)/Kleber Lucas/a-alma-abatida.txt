Kleber Lucas - A Alma Abatida

           Ab   Bbm7       Cm  Fm7      Bb4 Eb         Ab   Ab9  Eb     Ab    Bbm7
Se Tu Minh'alma  A Deus suplica e não recebe confiando fica, em suas promessas que são
     Cm   Fm7    Bbm7   Eb         Ab Ab7 Eb
 mui ricas e infalíveis pra te valer.
           C#    Eb      Cm  Fm7     Bbm7 Eb           Ab Ab7 Eb        C#  Eb
Porque te abates ó minha alma e te comoves perdendo a calma, Não tenhas medo em Deus
  Cm  Fm7         Bbm7 Eb         Ab Ab9 Eb
espera porque bem cedo Jesus virá,
         Ab  Bbm7         Cm  Fm7     Bb4 Eb         Ab   Ab9 Eb     Ab   Bbm7
Ele intercede por ti minh'alma espera Nele com fé em calma, Jesus de Todos teus males

Cm   Fm7       Bbm7 Eb          Ab Ab7 Eb
salva e te abençoa   dos altos céus,
           C#    Eb      Cm  Fm7     Bbm7 Eb           Ab Ab7 Eb        C#  Eb
Porque te abates ó minha alma e te comoves perdendo a calma, Não tenhas medo em Deus
   Cm  Fm7         Bbm7 Eb       C# Eb
 espera Porque bem cedo Jesus virá (2x)
         Ab   Bbm7      Cm    Fm7      Bb4  Eb      Ab   Ab9  Eb    Ab    Bbm7     Cm   Fm7
Terás em breve as dores findas no dia alegre da sua vinda se Cristo tarda espera ainda, mais
      Bb4   Eb      Ab Ab7 Eb
um pouquinho e o Verás


           C#    Eb      Cm  Fm7     Bbm7 Eb           Ab Ab7 Eb        C#  Eb
Porque te abates ó minha alma e te comoves perdendo a calma, Não tenhas medo em Deus
  Cm  Fm7         Bbm7 Eb       Ab
espera Porque bem cedo Jesus virá (2x)

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Ab7 = 4 6 4 5 4 4
Ab9 = 4 6 8 5 4 4
Bb4 = X 1 3 3 4 1
Bbm7 = X 1 3 1 2 1
C# = X 4 6 6 6 4
Cm = X 3 5 5 4 3
Eb = X 6 5 3 4 3
Fm7 = 1 X 1 1 1 X
