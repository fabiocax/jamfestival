Kleber Lucas - O Mistério

Intro: D Bm Em D/F# G D (2X).

               Em               A
O mistério revelado é Cristo em nós
   D4  D  A/C# Bm   Bm/A             E
Esperança da glória bendita de Deus Pai
            D                  A     D
Para que o mundo possa ver e crer.
               Em               A
O mistério revelado é Cristo em nós
   D4  D  A/C# Bm   Bm/A             E
Esperança da glória bendita de Deus Pai
            D                  A
Para que o mundo possa ver e crer.
      D               Em
Um só corpo e um só espírito
A      A/G         D4         D
Uma só fé, um só batismo de amor.
         D               Em
Ele é a nossa paz e nEle somos um
A        A/G             D4         D
Esta é a aliança que foi feita na cruz.

      Bm         Bm/A           E/G#
Somos povo do Senhor e ele nos uniu
     A          A7         D4 D
E é tempo de vivermos em unidade.
                 Em
Nem a morte ou a vida
A          A/G            D4           D
Coisas do presentes ou temores do amanhã.
Bm       Bm/A        E/G#
Nada poderá nos separar do amor de Deus
        A        A7           D4         D
Nenhuma luta é maior do que podemos suportar.

Introdução1 : D Bm Em D/F# G D
O mistério...
Introdução2: D D4 D
Um só corpo...
Final: D Bm Em D/F# G D - uma vez.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A/G = 3 X 2 2 2 X
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
Bm/A = X 0 4 4 3 X
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D4 = X X 0 2 3 3
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
