Kleber Lucas - Muito Mais de Deus

F               C/E
As misericórdias do Senhor
 Dm                   Bb         C
São as causas de não sermos consumidos
F                    C/E
Porque as Suas misericórdias não têm fim
  Dm                 Bb  C
Renovam-se cada manhã
F
O amor de Deus encheu
C/E                         Dm          Bb9/13   C
Nossos corações, que maravilha, que maravilha
F                    Am
Porque as suas misericórdias não têm fim
   Dm               Bb  C
Renovam-se cada manhã
  Bb                 Bb/C
Eu sei em quem tenho crido,
       Bb/D                   C
Também sei que é poderoso para fazer
 F                      C
Mais do que tudo que pedimos ou pensamos

 Dm                    Bb
Mais do que tudo que sonhamos
         Gm       F/A             G/B
Muito mais para mim e para você irmão
                C   F
Muito mais de Deus

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bb/C = X 3 X 3 3 1
Bb/D = X 5 X 3 6 6
Bb9/13 = 6 5 5 5 X X
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
G/B = X 2 0 0 3 3
Gm = 3 5 5 3 3 3
