Kleber Lucas - Vimos Adorar

Ab        Fm
Viemos adorar o Cristo que venceu
Db Bbm  C#              E
E derramar os nossos corações
  Db   Bm
Como expressão de amor
   Db         Bbm        Ab
Porque Ele se entregou por nós

Ab       Fm
Viemos adorar Jesus o Rei dos reis
Db   Bbm      Db      Eb
Só Ele é digno de adoração
Db      Bbm
O Seu trono é sublime
     Db          Bbm  Eb Ab  Gm
E o Seu reino resplandece luz

Fm         Eb            Db
Querubins, Serafins adoram
  Bbm        Ab     Gm
Prostrados diante do trono

     Fm            Eb        Db
E os vinte e quatro anciãos declaram
    Bbm    Fm    Bbm      Fm
Que o Leão de Judá, a Raiz de Davi
    D  E
Que venceu

  Db  Ab       Eb  Fm
Aleluia,    Aleluia
  Db         Fm        Bbm    Eb
Bendito é o Rei que vem em nome  do    Senhor!
  Db   Ab     Eb  Fm
Aleluia,   Aleluia
  Db     Fm          Bbm   Eb  Ab
Jesus Bendito Cristo Rei e  Sal va dor!

Ab        Fm
Viemos adorar o Cristo que venceu
Db  Bbm Db     Eb
E derramar os nossos corações
   Db       Bbm
Como expressão de amor
    Db     Bbm       Ab  Gm
Porque Ele se entregou por nós

Fm         Eb         Db
Querubins, Serafins adoram
 Bbm            Ab      Gm
Prostrados diante do trono
   Fm          Eb        Db
E os vinte e quatro anciãos declaram
    Bm     Fm    Bbm      Fm
Que o Leão de Judá, a Raiz de Davi
    D  E
Que venceu

  Db   Ab      Eb   Fm
Aleluia,    Aleluia
   Db         Fm      Bbm    Eb
Bendito é o Rei que vem em nome  do    Senhor!
  Db  Ab      Eb  Fm
Aleluia,   Aleluia
  Db      Fm         Bbm   Eb   Ab
Jesus Bendito Cristo Rei e  Sal  va  dor!

  Eb  Bb       F Gm
Aleluia,    Aleluia
  Eb                 Gm      F
Bendito é o Rei que vem em nome  do    Senhor!
  Eb  Bb       F Gm
Aleluia,   Aleluia
  Eb              Gm   F  Bb
Jesus Bendito Cristo Rei e  Sal va dor!

Eb                Gm   F  Bb
Jesus Bendito Cristo Rei e  Sal va dor!
Eb                Gm   F  Bb
Jesus Bendito Cristo Rei e  Sal va dor!

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Bb = X 1 3 3 3 1
Bbm = X 1 3 3 2 1
Bm = X 2 4 4 3 2
C# = X 4 6 6 6 4
D = X X 0 2 3 2
Db = X 4 6 6 6 4
E = 0 2 2 1 0 0
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
Gm = 3 5 5 3 3 3
