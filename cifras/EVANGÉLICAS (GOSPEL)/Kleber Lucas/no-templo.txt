Kleber Lucas - No Templo

Am7   F7
Eu entrarei no templo,
    Am                  G
Com alegria e com hinos de louvor.
     Am7    F7
Adorarei no templo,
  Am                    G
O Deus de toda vida Soberano, Criador.  (2X)

 Am7                     F7
Que enviou Seu filho por amor,
   Am                         G
Pra salvar o pobre homem pecador.
Am7                       F7
Que nos resgatou da morte para vida,
Am                     G
Ele é Senhor do reino que nos deu guarida.

        Am7
Eu entrarei...


Am7                      F7
Todo Poderoso, Grande Eu Sou,
        Am                         G
Como o sangue do Seu filho nos comprou.
Am7                  F7
Com amor eterno Ele nos amou,
Am                     G
Toda nossa vida existe para o seu louvor. (2X)



----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
F7 = 1 3 1 2 1 1
G = 3 2 0 0 0 3
