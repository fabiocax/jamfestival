Kleber Lucas - Profeta da Esperança

Intro:  Dm  A#  F  C

          Dm        A#      F
Eu sou a luz que ilumina o mundo
    C      Dm   A#         C              Dm  A#   F   C
Eu sou candeia que vai brilhar na escuridão
          Dm        A#        F
Eu sou a luz que ilumina o mundo
    C      Dm   A#         C
Eu sou Profeta da Esperança
         Dm         Am
Dos castelos, das praças
     Gm        Dm            A#        F
Das ruas, dos morros, das esquinas da vida
     C         Dm
Dos becos e favelas
        A#          C          Am            Dm
Das prisões, dos aflitos, dos órfãos e das viúvas
Gm       A#    Am
Eu sou a luz
     A#         C         Dm        Gm
O Senhor me salvou, me curou, me ungiu

     A#         C      Dm          C
O Senhor me chamou eu ouvi sua voz
        Dm   A#   F   C  2X
Eu vou, eu vou

Refrão:
F   Gm      Dm       A#
Do mundo eu sou o farol
F   Gm      Dm       A#
Que brilha na escuridão
F   Gm      Dm         A#
A luz de uma nova manhã
     Gm
Clareou
                      Dm  A#   F   C
Eu vou lançar as redes
                      Dm  A#   F   C
Eu vou cuidar de vidas
                   Dm  A#   F   C
Eu vou na Babilônia
                    Dm  A#   F   C
pra libertar cativos

----------------- Acordes -----------------
A# = X 1 3 3 3 1
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
