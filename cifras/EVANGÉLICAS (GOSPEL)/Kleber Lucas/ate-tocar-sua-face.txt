Kleber Lucas - Até Tocar Sua Face

Intro: Am  Dm  E7

Am
 Já estou posicionado entre as rochas
                             Dm
 Me encostei pra não cair ao chão

 Sei que não resistiria
             E7
 Ao ver Tua face, oh Senhor

Am
 Mais Te peço que me deixe ver
                     Dm
 O vulto da Tua shekinah
                               E7
 Se cair, levantarei querendo mais Te adorar

            Am            Dm
 O que me importa é Te adorar
        G           C       F
 Eu só quero Te adorar Senhor


 Até tocar Tua face oh Deus
    Dm           E7
 Eu quero Te adorar
        Am         Dm
 Eu só quero Te adorar
        G           C
 Eu só quero Te adorar
     F                    Dm
 Eu quero Tua glória refletindo
     E7     Am  Dm  E7
 Em meu louvor

     A7       Dm         G
 Na fenda da rocha eu estou
            A              Dm
 Pra Te adorar nada mais importa
        G           C  E7
 Eu só quero Te adorar

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
