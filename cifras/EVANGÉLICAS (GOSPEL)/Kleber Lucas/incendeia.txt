Kleber Lucas - Incendeia

Intro 2x:  Cm   Ab    Bb   Fm

        Cm                              Gb
Se teus olhos forem bons, o teu corpo terá luz
        Ab                        Fm
Se a chama se apaga, as trevas reinarão
Cm                      Gb                        Ab
Se o nosso fogo for pra queimar o nosso irmão com o ódio da inquisição
                 Fm
Deus morrerá em nós
    Cm                                 Gb
Mas se o nosso fogo for como o fogo de Atos 2
        Ab                    Fm
Então, por favor, Senhor, incendeia-nos

(refrão 2x)

       Cm             Fm
Incende      e     e      ia-nos

(repete tudo)


Solo:  Cm  Gb  Ab  Fm

Cm
Luz, luz, luz, Viver na luz
Gb                                    (4x)
Luz, luz, luz
                    Ab                           Fm
Não vale a pena andar na escuridão,   Eu quero Deus

(Refrão 4x)

     Cm                     Fm
Incende      e        e      ia-nos

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
Fm = 1 3 3 1 1 1
Gb = 2 4 4 3 2 2
