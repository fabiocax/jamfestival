Kleber Lucas - Corro Pra Você

Intro: A/C# D9  (3X)

D9                 A9/C#
Jamais poderíamos beber
                        Bm7
De uma outra fonte e saciar nossa alma
                    E    A9/C#
Que provou o Teu amor
D9                A9/C#
Nunca poderíamos sonhar outro sonho
                Bm7
Que não fosse o Teu
                    E
Nunca poderíamos cantar

Se não fosse pra Deus

A9/C#        D9       E     F#m
Corro pra Você, eu te busco, Deus
A9/C#        D9       E     F#m
Corro pra Te ver, quero conhecer


Bm              A9/C#
A beleza do Teu meigo olhar
    D9                   A9/C#
A firmeza da Tua doce voz
Bm                  A9/C#  D9
Ao abrirmos nossos  co -  rações
        E
Para ouvirmos o que Tens pra nós
          A9/C#        D9    A9/C#    A     D9   E/A   A9     A9/C#
1º vez Corro pra você, corro pra você,   corro pra você
          A9/C#        D9    E/A     A9      D9   E/A    A9   D9                    E/A    A9
2º vez Corro pra você, corro pra você,                        eu corro pra você,Jesus,
           D9             E/A    A9         A/C#
eu corro pra te ver, corro pra te ver        A beleza...

D9                         A9/C#
Eu Te busco e Te encontro, Pai
                     Bm7
Sou criança e quero colo
                                E                          A9/C#
Eu preciso ouvir Teu coração bater e me dizer... (do Teu amor)
D9                  A9/C#
Sob Tuas asas eu descanso
                         Bm7
Sou coberto por Teu santo manto
                         E
No frio me aquece e no calor é a minha sombra

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A9 = X 0 2 2 0 0
A9/C# = X 4 2 2 0 X
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
E/A = X 0 2 1 0 0
F#m = 2 4 4 2 2 2
