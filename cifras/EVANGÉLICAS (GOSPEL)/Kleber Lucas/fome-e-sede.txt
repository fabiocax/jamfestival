Kleber Lucas - Fome e Sede

Intro: E  B  B/D#  C#m  B  A  B  (2x)

E            B        B/D#  C#m
Eu tenho fome, eu tenho sede
   B            A
Preciso Te encontrar
E         B               B/D# C#m
E neste mar, já lancei a rede
       B         A  E/G# F#m
E nada pude alcançar
        C#m        B
Estou desesperado, Deus me ouve
       A
Eu já não sei o que fazer
   B
E nem pra onde ir

E               B     B/D#  C#m
Há tanta gente me esperando
         B     A
Estão carentes do Teu amor

E                B     B/D#
Mas eu não vou sair daqui
     A
Se Você não for comigo
E              B   B/D# C#m
Preciso ver a Tua face
          B     A
Sentir o toque da unção
E             B    B/D#
Não adianta prosseguir
      A                E/G# F#m
Se Você não for comigo

                 B
Eu não posso enganar Teu povo
     E              B/D#    C#m     E/G#
E levá-los pra nenhum lu.........gar
F#m            B
Este rebanho é Teu, Senhor
          A
E eu não vou sair daqui
      B            B/D#
Se Você não for comigo

      C#m
Se Você não for
      B
Se Você não for
      A               C#m B A
Se Você não for comigo

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
B/D# = X 6 X 4 7 7
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
F#m = 2 4 4 2 2 2
