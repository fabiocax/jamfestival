Kleber Lucas - O Nosso Deus É Fiel

Intro: Bb9  Bb7M  Eb9  Eb7M

Bb9        Bb7M
     Sim o Teu amor
Ab/Bb                             Eb7M
     É o que de melhor eu posso descrever
Cm7        Cm7/Bb       F/A
    Quando penso no caminho que andei
          Bb9          (1ªvez: Bbsus4)
    Pra chegar aqui

Gm7                        F/A
    Eu vi milagre acontecer, Eu vi sinais no céu
Gm7                      F/A
    Teu livramento me alcançou,
                   Eb9
    A minha sorte mudou
             Bb/D       Cm7        Cm7/Bb   F/A
    Eu vi Teu balsamo curar a minha dor, Senhor

Gm7                  F/A
   O impossível aconteceu, Bateu na minha porta

Gm7                       F/A
   Ninguém acreditou, mas eu não desisti
Eb9          Bb/D       Cm7     Cm7/Bb
    Meu choro durou uma noite
           Eb9             F7(9)   F
    Mais a minha alegria chegou

Bb9               Eb/Bb
   O nosso Deus é fiel
F/A              Gm7
   E não nos deixará sós
Cm7                Eb9
   O nosso Deus é fiel,
            F         Bb9
   Não nos deixará sós

 Solo: | Gm7             F/A | Bb9    (Bb/D) Eb9 |
      | Cm7 Cm7/Bb      F/A | Gm7  (eu vi milagre...)

----------------- Acordes -----------------
Ab/Bb = X 1 1 1 1 X
Bb/D = X 5 X 3 6 6
Bb7M = X 1 3 2 3 1
Bb9 = X 1 3 3 1 1
Bbsus4 = X 1 3 3 4 1
Cm7 = X 3 5 3 4 3
Cm7/Bb = 6 3 5 3 4 3
Eb/Bb = X X 8 8 8 6
Eb7M = X X 1 3 3 3
Eb9 = X 6 8 8 6 6
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
F7(9) = X X 3 2 4 3
Gm7 = 3 X 3 3 3 X
