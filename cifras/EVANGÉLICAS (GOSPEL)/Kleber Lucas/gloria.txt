Kleber Lucas - Glória

Intro 2x: Am7  F  C/G  Em7

Am7          F
  Ao erguemos nossas mãos, Pai
 C/                   Em7
Para ministrar amor, para libertar da dor
Am7            F
  Seja a Tua mão, seja a Tua voz
   C/G             Em7
 libertar, a libertar

Am7          F
  Os enfermos são curados
 C/G             Em7
Pelo Teu poder, pelo Teu poder
Am7             F
  És o mesmo Deus, faz milagre sim
     C/G           Em7
Eu creio sim, eu creio sim

Am                 F
  Teu amor vai operar em nós

 C               Em
Sobre nós e através de nós
Am                   F
  No Teu nome, oh Jesus
      C  E
Há cura para Tua glória

  Am7     C/G    Dm   Am    F        E
Glória, glória, gló - o - ria, para Tua glória
  A       C/G    Dm   Am    F        E         Am  (F Em G Am7)
Glória, glória, gló - o - ria, para Tua glória

 Em7              Dm         F
Pois não há impossível para Deus
        Dm7   C         E         (2X)
Agindo Deus quem impedirá?

Am7  C/G  Dm  F  E
"O Espírito do Senhor Deus está sobre mim,
Porque Ele me ungiu para pregar a boa notícia a salvação,
Ele me enviou a curar os quebrantados de coração."

      A                                   C/G
O castigo que nos traz a paz estava sobre Ele
                 Dm              F           E
E por suas chagas eu fui curado, eu fui sarado

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
C/G = 3 3 2 X 1 X
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
