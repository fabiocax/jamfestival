Renascer Praise - O Milagre da Espera

Introdução: D  :  C/D : G/D  :  Gm6/D  :  D  : Bm/G# : Em7  G  :  Asus4   :  A
            G/           D
Dá-me um sinal do teu favor
            G          A    Bm7
Mostra-me ó Deus que comigo estás
           Em           A
Vem fortalecer meu coração
  Bm7        Em         Asus4   A
Preciso de Ti pra não desmaiar..
          G/B                  D
Arranca de mim este pranto de dor
             G      A    Bm7
Me veste com vestes de louvor
          Em          A
Com óleo da alegria me unge
          Em      Em/D    C  Asus
Com Teu Espírito vem me curar
            D    F#m7      G
Não haverá impossíveis para o meu Deus
         D      F#m7   G
Suas promessas Ele me fará viver

           C#m7  5b F#7    Bm7
A Espera Não pode Matar A Esperança
               Em        Asus4      A                         G     : A      : Bm
Minhas lágrimas Ele enxugará                                  Com milagres.....

           Bb       C/Bb     D/A
Muito mais do que os olhos podem ver
       Gm         C/G    D/F#
Muito além do que dá pra sonhar
       Em         Asus      Bbsus
Seus milagres Ele fará viver

      Eb/G    Cm7          Fm7
Não haverá impossíveis para o meu Deus
        Eb/G   Cm7    Fm7
Suas promessas Ele me fará viver
             Dm7  5b G7      Cm7
A espera não pode matar a esperança
                Fm7        Bbsus4 Bb
Minhas lágrimas Ele enxugará

      Cm       Bb         Ab
Com milagres                     (3x)

         Fm7    Eb/G     Bbsus                       Bsus        C#m7        B/C# A/C#
Com milagres                                Com milagres                    (3x)
          F#m7          Bsus                         C#m7      B/C#       A/C#
Com milagres                                Com milagres
      C#m7    C#m7/B          F#/A#               F#m7  E/G# A Bsus
Com milagres                                Com milagres
         E
Com milagres

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
Ab = 4 3 1 1 1 4
Asus = X 0 2 2 2 0
Asus4 = X 0 2 2 3 0
B/C# = X 6 X 4 7 7
Bb = X 1 3 3 3 1
Bbsus = X 1 3 3 3 1
Bbsus4 = X 1 3 3 4 1
Bm = X 2 4 4 3 2
Bm/G# = X X 6 4 3 2
Bm7 = X 2 4 2 3 2
Bsus = X 2 4 4 4 2
C = X 3 2 0 1 0
C#m7 = X 4 6 4 5 4
C#m7/B = 7 4 6 4 5 4
C/Bb = X 1 2 0 1 X
C/D = X X 0 0 1 0
C/G = 3 3 2 X 1 X
Cm = X 3 5 5 4 3
Cm7 = X 3 5 3 4 3
D = X X 0 2 3 2
D/A = X 0 X 2 3 2
D/F# = 2 X 0 2 3 2
Dm7 = X 5 7 5 6 5
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
Eb/G = 3 X 1 3 4 X
Em = 0 2 2 0 0 0
Em/D = X X 0 4 5 3
Em7 = 0 2 2 0 3 0
F#/A# = 6 X 4 6 7 X
F#7 = 2 4 2 3 2 2
F#m7 = 2 X 2 2 2 X
Fm7 = 1 X 1 1 1 X
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G/D = X 5 5 4 3 X
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
Gm6/D = X 5 5 3 5 X
