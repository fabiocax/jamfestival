Renascer Praise - Dependo de Ti

Intro: Db9(7+)  Bb/Eb  F (3x)
       Ab/Db  Bb/Eb  Csus

     F          Bb7+(9)     Am7          Dm7
Não ter que depender, de aparência ou opiniões
    Gm7         F/A       G/B               Bb/C     C/Bb
Nem mesmo precisar de homens ou       lugar     estar
      Am7     Dm7       Gm7        Bb/C  C#dim
Mas ter em Teu altar e no Teu imenso amor
 Dm7        Bbm/Db            F/C              Bm7(5b)
Suprimento, solução,          porta aberta, vitória  certa
 Gm7           Bb/C       C/D D7
Dom de Deus é depender de Ti.
       Gm7              Bb/C    F4   F Db9(7+) Csus
Não há solidão que nos possa resistir
  F      F/Eb          Bb/D    F/C         Bb F/A
Dependo de ti,        dependo de Ti,     Jesus
Gm7         F/A        Bm7(5b)
Não importa  qual a condição
       Bbm6          F/A    Bb     C2   C/D   D7
Ainda que os que me amam não consigam me ajudar

     Gm7       Bb/C      C/D D7
Eu dependo, dependo só de Ti
     Gm7       Bb/C  1. ( Bb7+(9) Gm7(9) Dm7(9/11) )
Eu dependo, dependo só de Ti

( Ab9(7+)  Fm7  Csus )
( Bb7+  Gm7  Dm7(9/11) )
( Ab7(7+)  Fm7  Csus  C9+(7/5+) )

      Gm7    2. C/D     D7(9b)
 Eu dependo, dependo só de Ti
      Gm7        Bb/C        Dm/G C/F Eb6(9) Csus Bm7(5b)
 Eu dependo, dependo só de Ti
      Gm7      Bb/C        Db9(7+) Csus F
 Eu dependo, dependo só de Tí!!

----------------- Acordes -----------------
Ab/Db = X 4 6 5 4 4
Am7 = X 0 2 0 1 0
Bb = X 1 3 3 3 1
Bb/C = X 3 X 3 3 1
Bb/D = X 5 X 3 6 6
Bb/Eb = X 6 X 7 6 6
Bb7+ = X 1 3 2 3 1
Bbm/Db = X 4 X 3 6 6
Bbm6 = 6 X 5 6 6 X
C#dim = X 4 5 3 5 3
C/Bb = X 1 2 0 1 X
C/D = X X 0 0 1 0
C/F = X 8 10 9 8 8
C2 = X 3 5 5 3 3
Csus = X 3 2 0 1 0
D7 = X X 0 2 1 2
Dm/G = 3 X X 2 3 1
Dm7 = X 5 7 5 6 5
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
F/C = X 3 3 2 1 1
F/Eb = X X 1 2 1 1
F4 = 1 3 3 3 1 1
Fm7 = 1 X 1 1 1 X
G/B = X 2 0 0 3 3
Gm7 = 3 X 3 3 3 X
