Renascer Praise - Novo Dia, Novo Tempo

[Intro] Bbm  Ab/C  Db  Eb7
        Bbm  Ab/C  Db  Eb7
        Bbm  Ab/C  Db  Eb7
        Bbm  Ab/C  Db  Eb7

  Bbm          Ab/C  Db            Eb
Quando um novo dia começa, ninguém vê
 Bbm       Ab/C       Db   Eb7
Mas a meia noite tudo já mudou
Bbm          Ab/C       Db  Eb7
No tempo  de Deus, vai aparecer
Bbm       Ab/C       Db       Eb
O que Ele já mostrou ao que crê

( Bbm  Ab/C  Db  Eb )
( Bbm  Ab/C  Db  Eb )

     Bbm            Ab/C Db          Eb
Nem tudo o que acontece, dá pra entender
   Bbm       Ab/C       Db     Eb
Andando pela fé a gente vai vencer

Bbm         Ab/C          Db           Eb
O que quero mais, é me encher do Teu poder
  Bbm              Ab/C     Db         Eb
Jesus, se  Estás comigo, a quem eu temerei?

      Gb        Db
Novo dia, novo tempo
   Gb        Ab  Bbm
A fé faz apare|--cer
        Gb             Db
O impossível, o inatingível
      Gb         Ab   Bb
De repente vai acontecer

      Gb        Db
Novo dia, novo tempo
   Gb          Ab  Bbm
A fé  faz a pare|--cer
        Gb             Db
O impossível, o inatingível
      Gb         Ab   Bb
De repente vai acontecer

( Bbm  Ab/C  Db  Eb )
( Bbm  Ab/C  Db  Eb )

 Bbm           Ab/C Db            Eb
Quando um novo dia começa, ninguém vê
 Bbm        Ab/C       Db   Eb7
Mas a meia noite tudo já mudou
Bbm          Ab/C       Db  Eb7
No tempo  de Deus, vai aparecer
Bbm       Ab/C      Db       Eb
O que Ele já mostrou ao que crê

     Bbm           Ab/C  Db          Eb
Nem tudo o que acontece, dá pra entender
   Bbm        Ab/C       Db     Eb
Andando pela fé a gente vai vencer
Bbm          Ab/C          Db           Eb
O que quero mais, é me encher do Teu poder
   Bbm              Ab/C     Db         Eb
Jesus, se  Estás comigo, a quem eu temerei?

      Gb        Db
Novo dia, novo tempo
    Gb        Ab  Bbm
A fé faz apare|--cer
        Gb             Db
O impossível, o inatingível
      Gb         Ab   Bb
De repente vai acontecer

      Gb        Db
Novo dia, novo tempo
    Gb          Ab  Bbm
A fé  faz a pare|--cer
        Gb             Db
O impossível, o inatingível
      Gb         Ab   Bb
De repente vai acontecer

( Bbm  Ab/C  Db  Eb )
( Bbm  Ab/C  Db  Eb )
( Bbm  Ab/C  Db  Eb )
( Bbm  Ab/C  Db  Eb )

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Ab/C = X 3 X 1 4 4
Bb = X 1 3 3 3 1
Bbm = X 1 3 3 2 1
Db = X 4 6 6 6 4
Eb = X 6 5 3 4 3
Eb7 = X 6 5 6 4 X
Gb = 2 4 4 3 2 2
