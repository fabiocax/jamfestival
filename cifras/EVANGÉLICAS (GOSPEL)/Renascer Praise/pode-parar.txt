Renascer Praise - Pode Parar

Intro: F7+  Em7  Dm7  Em7  F7+  Em7  Dm7  Gm7  C7
       F7+  Em7  Dm7  Em7  F7+  Em7  Am7  E9+(5+)

      Am7
Pode parar
          Em7                    F7+
Pode até tentar, mas não vai conseguir
               Em7               Dm7
Eu tenho uma promessa não vou desistir
               Em7             Am7        E9+(5+)
To firme nessa rocha e não caio não, não, não
    Am7
Vou avisar
            Em7                  F7+
Aqui só tem guerreiro e conquistador
                Em7              Dm7
Quem preparou a forca nela se enforcou
                  Em            Am7   E9+(5+)
A porta que Deus abre ninguém fecha não

   F7+    G            Am7
O Todo Poderoso está comigo

 Dm7          G7        C9(7+)
Agindo Deus ninguém impedirá

  Bm7(5b)           E7     Am  D7
O Deus que tira o povo do Egito
   F7+        Em7   Am7
É o mesmo que abriu o mar

   F7+     G            Am7
Firmado na visão e na palavra
 Dm7          G7           C9(7+)
Eu venço os gigantes pela unção
   Bm7(5b)      E9+(5+)     F7+    Em7
Não temo o inimigo, o cordeiro está comigo
     Dm7       Em7        Am7 E9+(5+)
Sou mais que vencedor então

      Am7
Pode parar
          Em7                    F7+
Pode até tentar, mas não vai conseguir
               Em7               Dm7
Eu tenho uma promessa não vou desistir
               Em7             Am7        E9+(5+)
To firme nessa rocha e não caio não, não, não

    Am7
Vou avisar
            Em7                  F7+
Aqui só tem guerreiro e conquistador
                Em7              Dm7
Quem preparou a forca nela se enforcou
                  E9+(5+)          Am7 E9+(5+)
A porta que Deus abre ninguém fecha não

         Am7    E9+(5+)
E o fogo vai cair
           F7+  Em7
Unção que vai descer
             Dm7  Em7
E a igreja receber
    F7+              Em7           Am7
É o povo de Deus avançando em poder

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
C7 = X 3 2 3 1 X
D7 = X X 0 2 1 2
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F7+ = 1 X 2 2 1 X
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
Gm7 = 3 X 3 3 3 X
