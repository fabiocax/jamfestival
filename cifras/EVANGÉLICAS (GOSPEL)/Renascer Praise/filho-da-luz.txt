Renascer Praise - Filho da Luz

[Intro] F  Em  F  C  F  Em  F  C/G

          Am        G/B              C
Minha esperança e o meu socorro vem de Jesus
             Am         G/B           C
Que se derramou e aceitou morrer por mim na cruz
            F         G              Em           F
Não posso mais me abater, nem me deixar perder a fé
             F             G
Mesmo que a dor eu não suporte mais
         F     G
Vou contigo Jesus
        G#     Gsus  G
Vou contigo Jesus

                C            C/F
O seu nome é Jesus, filho da luz
             Am                  G
Quando não tenho mais força é Jesus
           F              C
Ando pela fé, eu não vou mais parar

         Dm                G                C
Sei pra onde eu vou, eu sei aonde quero chegar

    Bb              Fm     C
Aleluuuia   ao teu nome Jesus
    Bb       Fm       C
Aleluuuia   Filho da Luz
    Bb              Fm     C
Aleluuuia   ao teu nome Jesus
    Bb       Fm       Gsus G
Aleluuuia   Filho da Luz

                C            C/F
O seu nome é Jesus, filho da luz
             Am                  G
Quando não tenho mais força é Jesus
           F              C
Ando pela fé, eu não vou mais parar
         Dm          G
Sei pra onde eu vou,
                C            C/F
O seu nome é Jesus, filho da luz
             Am                  G
Quando não tenho mais força é Jesus
           F              C
Ando pela fé, eu não vou mais parar
         Dm                G                C
Sei pra onde eu vou, eu sei aonde quero chegar

[solo] Bb  Fm  C
       Bb  Fm  C

         Bb    Fm        C
Oh,oh,oh,oh,   oh,oh,oh,oh
         Bb    Fm        C
Oh,oh,oh,oh,   oh,oh,oh,oh

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C/F = X 8 10 9 8 8
C/G = 3 3 2 X 1 X
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
G/B = X 2 0 0 3 3
Gsus = 3 2 0 0 0 3
