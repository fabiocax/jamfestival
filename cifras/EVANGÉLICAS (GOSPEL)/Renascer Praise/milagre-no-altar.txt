Renascer Praise - Milagre No Altar

Intro: D  G  A

    D           G             A
Atrás das nuvens brilha o sol
D         G            A
Luz que vem para me guiar
   D           G                 A
Socorro presente em meio da aflição
         D/F#        G  A
Sei que em Ti posso confiar (2X)

 D        G                  A4
Ainda que esteja andando em dor
 D       G                A
Sei onde posso Te encontrar
D         G             A4
Jamais serei envergonhado
        D/F   G        A
Minha cura está no altar

D   G       A
Jesus eu creio

        D            G         A
Há um milagre me esperando no altar
      D       G       F#     Bm7
Tua glória está neste lugar, eu sei
 Em  D/F# G A     D
Vem sobre  mim poder


       Gm                  D/F#
Nada vai me impedir de viver o meu milagre
          Gm                   D
Tua palavra diz há cura hoje aqui
        Em7
Não importa a multidão
        D/F#
Vou atravessar em fé
  D/F#         G      A       Bb
 vou    vi  - ver,  vou viver...

Eb/G         Cm  Bb
Jesus eu creio
            Eb/G                G# Bb
Há um milagre me esperando no altar
      Eb/G    G#  G/B     Cm7
Tua glória está neste lugar, eu sei
Fm7        Bb  Cm
Vem sobre mim  poder
Fm7        Bb  Cm
Vem sobre mim  poder
Fm7        Bb  Eb/G
Vem sobre mim  poder

----------------- Acordes -----------------
A = X 0 2 2 2 0
A4 = X 0 2 2 3 0
Bb = X 1 3 3 3 1
Bm7 = X 2 4 2 3 2
Cm = X 3 5 5 4 3
Cm7 = X 3 5 3 4 3
D = X X 0 2 3 2
D/F = X X 3 2 3 2
D/F# = 2 X 0 2 3 2
Eb/G = 3 X 1 3 4 X
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F# = 2 4 4 3 2 2
Fm7 = 1 X 1 1 1 X
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
G/B = X 2 0 0 3 3
Gm = 3 5 5 3 3 3
