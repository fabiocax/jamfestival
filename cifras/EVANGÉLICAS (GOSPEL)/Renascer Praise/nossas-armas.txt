Renascer Praise - Nossas Armas

Dm7
AS ARMAS DA NOSSA MILÍCIA
Am7
SÃO PODEROSAS EM GUERRA
         Dm7
A UNÇÃO DE CONQUISTA ESTÁ SOBRE NÓS
Am7                                             (Am7    C ) AO INÍCIO
QUEM PODERÁ NOS VENCER ?

        Bb              C            C/D                Dm7
O SENHOR DOS EXÉRCITOS É O NOSSO GENERAL
Bb                              C
QUEBREMOS O VASO, LEVANTEMOS A TOCHA
Bb                              C
TOQUEMOS BEM ALTO AS NOSSAS TROMBETAS

Bb                      C                 Dm7
TRAGADA FOI A MORTE PELA VIDA
Bb                     C                       C/D                    Dm7
VENCIDO O INIMIGO PELO SANGUE DO CORDEIRO
Bb                           C
MAIOR É O QUE ESTÁ EM NÓS

                C/D                                 Dm7
DO QUE AQUELE QUE NO MUNDO ESTÁ

Bb                                  C/Bb
NAS NOSSAS MÃOS O SENHOR ENTREGOU
Bb                  C/Bb
O INIMIGO E SUAS HOSTES
Gm7             Gm7/F
HOJE É DIA DE VITÓRIA                                                   3x
                 Eb7+                               Csus                 Dm7 : Am7 :
CONQUISTEMOS A TERRA EM NOME DO SENHOR  : Dm7 : Bb Csus :

AS ARMAS ...

O SENHOR ...

(Repete Côro 2x)
Bb                                                  C
CONQUISTEMOS A TERRA EM NOME DO SENHOR (12x)
                                                FINE            Bb      C       Dm7

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am7 = X 0 2 0 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C/Bb = X 1 2 0 1 X
C/D = X X 0 0 1 0
Csus = X 3 2 0 1 0
Dm7 = X 5 7 5 6 5
Eb7+ = X X 1 3 3 3
Gm7 = 3 X 3 3 3 X
Gm7/F = X X 3 3 6 3
