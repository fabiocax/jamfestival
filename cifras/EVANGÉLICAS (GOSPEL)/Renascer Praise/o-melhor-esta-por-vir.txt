Renascer Praise - O Melhor Está Por Vir

[Intro] C  G  Dm  F

C     G         Dm
Eu vou olhar para ti jesus
     F
Vou adiante
C                  G              Dm    F
Sei que o melhor de deus está por vir

C                 G                  Dm          F
Sigo os seus passos, teus caminhos nada temerei
C                    G                        Dm            F
Maior é o que esta em mim, do que aquele que no mundo está

Am             G/B                 C      F
Mesmo que a tempestade queira intimidar
Am                      G/B                C       F
De maõs dadas contigo sobre as águas eu vou andar

C                        G                 Dm F
Eu vou viver os sonhos, tudo que prometeu

C                       G           Dm                 F
 Eu vou viver os sonhos, as promessas que me deu  ínicio

        Am                  G            Am   G/B
Teus milagres vou viver, basta só acreditar
     F                   C/E                     Dm F
Tua palavra é fiel,  já vou celebrar, me alegrar

C           G
Vou viver
Am         F
Vou viver
C          G         Am      G/B      F      3× C
Vou viver  as promessas que me deu, eu vou viver

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
