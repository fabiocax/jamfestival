Renascer Praise - Deus da Família

Intro: F C/E Dm7 C/E F C/E Dm7 C F C/E  Dm7  G4  G7

 C      G/C   F/C     C    G/B
Senhor abençoa , a minha família
 Am            Gsus4   G   E/G#
Vem ser como um muro ao redor
 Am       E/G#     C/G      Am/F#
De noite e de dia, Tua nuvem nos guie
  Dm       F/G     G7
E seja unido o nosso andar

 C     G/C       F/C      C    G/B
Faz-nos conhecidos por sermos Teu povo
 Am           G   sus4 G      E/G#
Prospera Tua obra em nossas mãos
 Am    Ab5+      C/G    Am/F#
Espírito enche-nos de paz e saúde
 Dm7         F/G   G7
Em Tuas promessas  viver
C     G/C     F      Gsus4 G
Deus da família, espero em Tí

Am              Gsus4  G   E/G#
Reina prá sempre em meu lar
Am    Ab5+      C/G       Am/F#
Faz nos conhecidos por sermos benditos
Dm7           F/G  G7
Mostra em nós Teu poder
 C     G/C       F     G
Deus da família, eu creio em Ti
Am        Gsus4    G      E/G#
De Ti vem a graça de Ter um lar
 Am   E/G#     C/G      Am/F#
Governa Tua glória, em nós manifesta
Dm7   F/G  G    C
Deus abençoe  meu lar !
Interlúdio: F   C/E  Dm7  C    F    C/E   Dm7   F/G
F  C/E          Dm7      F/G    C    F/C C
Ôôôô.....      Deus, abençoe meu lar!!!!!

----------------- Acordes -----------------
Ab5+ = 4 7 6 5 4 X
Am = X 0 2 2 1 0
Am/F# = 2 X 2 2 1 0
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
C/G = 3 3 2 X 1 X
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
E/G# = 4 X 2 4 5 X
F = 1 3 3 2 1 1
F/C = X 3 3 2 1 1
F/G = 3 X 3 2 1 X
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G/C = X 3 5 4 3 3
G4 = 3 5 5 5 3 3
G7 = 3 5 3 4 3 3
Gsus4 = 3 5 5 5 3 3
