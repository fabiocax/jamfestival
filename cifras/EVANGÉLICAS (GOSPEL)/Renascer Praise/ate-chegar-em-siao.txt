Renascer Praise - Até Chegar Em Sião

[Intro] C  G  Am  F  C  G  Am  F
        C  G  Am  F  C  G  Am  F
        C  G  Am  F  C  G  Am  F

Riff 1
E|-0h1--0-------------------------------------|
B|---------3----1-----------------------------|
G|--------------------------------------------|  
D|--------------------------------------------|
A|--------------------------------------------|
E|--------------------------------------------|

C     G          Am   F(Riff 1)
 Acredito na promessa
C           G           Am  F(Riff 1)
 Eu sei que ela vem de Deus
C           G/B           Am   F(Riff 1)
 O mundo aqui não me interessa
C        G          D/F#     F
 Sou um cidadão do céu...do céu


C                        G
 Com meu Deus eu vou saltar muralhas
Am                      F(Riff 1)
 Com meu Deus vou derrubar gigantes
C                  G              Am            F(Riff 1)
 Nesse mundo tão amargo e escuro, eu sou sal e luz

C                       G                    Am
 E se eu não der conta dessa guerra
Am               F(Riff 1)
 Ele manda o seu anjo forte
C                              G
 Com Ele não tem batalha perdida
                    Am                          F (Riff 1)
Vitória garantida, pelo sangue de Jesus...de Jesus

C         G      Am             F(Riff 1) C
 Eu vou além do rio, eu vou saltando os  montes
        G      Am       F(Riff 1)
Vou erguer bem alto as minhas mãos
C           G       Am            F
 Eu vou cantar bem forte, o hino da vitória
C        G       Am
 Até chegar em Sião
    F(Riff 1) Am
Chegar em   Sião

F  C         G       Am
   Ôô ôô chegar em Sião
F  C         G       D/F#    F
   Ôô ôô chegar em Sião... Sião

C     G          Am   F(Riff 1)
 Acredito na promessa
C           G           Am  F(Riff 1)
 Eu sei que ela vem de Deus
C           G/B           Am   F(Riff 1)
 O mundo aqui não me interessa
C        G          D/F#     F
 Sou um cidadão do céu...do céu

C                        G
 Com meu Deus eu vou saltar muralhas
Am                      F(Riff 1)
 Com meu Deus vou derrubar gigantes
C                  G              Am            F(Riff 1)
 Nesse mundo tão amargo e escuro, eu sou sal e luz

C                       G                    Am
 E se eu não der conta dessa guerra
Am               F(Riff 1)
 Ele manda o seu anjo forte
C                              G
 Com Ele não tem batalha perdida
                    Am                          F (Riff 1)
Vitória garantida, pelo sangue de Jesus...de Jesus

C         G      Am             F(Riff 1) C
 Eu vou além do rio, eu vou saltando os  montes
        G      Am       F(Riff 1)
Vou erguer bem alto as minhas mãos
C           G       Am            F
 Eu vou cantar bem forte, o hino da vitória
C        G       Am
 Até chegar em Sião
    F(Riff 1) Am
Chegar em   Sião

A fé não se explica
Mas traz felicidade
Eu sigo em frente no caminho da verdade
Espalhando o amor
Falando do salvador
E levando sua palavra por onde quer que eu vou
Fazer a diferença é necessário, é fato
Porém o que eu mais quero é estar a seu lado
Seu amor derramado por mim naquela cruz
Me mostrou o quanto eu te amo meu Jesus

Vem do alto o poder que me capacita
Se liga, isso vai mudar a sua vida
Medita nas promessas que ele deixou na vida
Pra crescer e ser um vencedor
Tem que ser, tem que estar
Pronto pra batalha
Confia, porque ele nunca falha
O que te ofereço sempre é muito bom
Revolução e restauração através do som
Vamo lá, chegando em Sião

C         G      Am             F(Riff 1) C
 Eu vou além do rio, eu vou saltando os  montes
        G      Am       F(Riff 1)
Vou erguer bem alto as minhas mãos
C           G       Am            F
 Eu vou cantar bem forte, o hino da vitória
C        G       Am
 Até chegar em Sião
    F(Riff 1) Am
Chegar em   Sião

C                        G
 Com meu Deus eu vou saltar muralhas
Am                      F(Riff 1)
 Com meu Deus vou derrubar gigantes
C                  G              Am            F(Riff 1)
 Nesse mundo tão amargo e escuro, eu sou sal e luz

C                       G                    Am
 E se eu não der conta dessa guerra
Am               F(Riff 1)
 Ele manda o seu anjo forte
C                              G
 Com Ele não tem batalha perdida
                    Am                          F (Riff 1)
Vitória garantida, pelo sangue de Jesus...de Jesus

C         G      Am             F(Riff 1) C
 Eu vou além do rio, eu vou saltando os  montes
        G      Am       F(Riff 1)
Vou erguer bem alto as minhas mãos
C           G       Am            F
 Eu vou cantar bem forte, o hino da vitória
C        G       Am
 Até chegar em Sião
    F(Riff 1) Am
Chegar em   Sião

F  C         G       Am
   Ôô ôô chegar em Sião
F  C         G       D/F#    F
   Ôô ôô chegar em Sião... Sião

[Final] C  G  Am  F(Riff1)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D/F# = 2 X 0 2 3 2
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
