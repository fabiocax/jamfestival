Renascer Praise - O Senhor É a Minha Luz

INTRO.:  G D Em C G D C

G      D  C
O Senhor é a minha luz
G      D        C
E a minha salvação
G      D       Em         C
O Senhor é a minha fortaleza
G9         D         C
A quem então eu temerei?

Am        Am/G     D/F#
Se contra mim um exército se acampe
Am      Am/G       D/F#
Mesmo assim não temerei
Am      Am/G      D/F#
Confiarei na força do meu Deus
      C Bm Am   C     D       G
Esperarei  O Senhor agir por mim.

FINAL DA MUSICA....G D Em C G D C Bm Em D  G

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am/G = 3 X 2 2 1 X
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G9 = 3 X 0 2 0 X
