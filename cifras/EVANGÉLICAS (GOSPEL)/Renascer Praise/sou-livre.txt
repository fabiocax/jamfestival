Renascer Praise - Sou Livre

(intro)  Bm7   D  G7+/9

Bm7                  D
Tuas marcas eu trago em meu corpo
     G7+/9
Alto preço pagou por mim
    Bm7            D   G7+/9
Tuas chagas trouxeram-me cura eu tudo posso em ti

Bm7                D
O teu sangue anulou a sentença
    G7+/9
Tenho acesso ao trono do pai
     Bm7           D         G7+/9
Já não há mais o que me separa tua glória habita em mim
    A4             A          Em7
Nada pode frustar os teus planos tudo o que desejou pra mim
A4          A         G7+/9
Me levanto na liberdade, do meu deus

    D         G7+/9         Bm7          A
Posso viver, posso louvar o melhor de deus chegou pra mim

    D/F#   G7+/9            Em7     A4 A
Posso dançar e celebrar o filho de deus me libertou
     Em7        G7+/9       D    A
Sou livre,              sou livre

----------------- Acordes -----------------
A = X 0 2 2 2 0
A4 = X 0 2 2 3 0
Bm7 = X 2 4 2 3 2
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em7 = 0 2 2 0 3 0
G7+/9 = 3 2 4 2 X X
