Renascer Praise - O Mesmo Sentimento

Intro: B  B7+  E/B  B  B7+  E/B

B       B7+         E/B
Faz-me senhor ouvir a tua voz
B       B7+         E/B
Voz que lança fora todo medo
B       B7+         E/B
Me ensina a te buscar, ter o mesmo sentimento
B       B7+         E/B
Esvaziar-me e assumir a forma de servo
                 C#m7    G#m7            F#
Me humilhar     diante de ti
        C#m7       G#m7                          F#
E buscar ser mais semelhante a ti

B         B7+   F#/A#
Veste-me com a tua santidade
G#m7   F#       E
Enche-me da unção do teu poder

B          B7+  F#/A#
Veste-me com a tua santidade


G#m7   F#             E
Usa-me da maneira que quiser

G#m7  F#      E            G#m7   F       E
Usa - me  usa-me  veste -me
G#m7  F#      E            G#m7   F       E
Veste -me veste-me  usa-me  senhor

----------------- Acordes -----------------
B = X 2 4 4 4 2
B7+ = X 2 4 3 4 2
C#m7 = X 4 6 4 5 4
E = 0 2 2 1 0 0
E/B = X 2 2 1 0 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
F#/A# = 6 X 4 6 7 X
G#m7 = 4 X 4 4 4 X
