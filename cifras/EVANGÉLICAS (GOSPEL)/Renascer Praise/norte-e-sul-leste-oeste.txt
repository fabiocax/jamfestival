Renascer Praise - Norte e Sul, Leste Oeste

G        E7           Am7
 Vem meu Brasil minha gente brasileira
D7                 G
 O evangelho do Senhor é a palavra verdadeira
D7        G       E7           Am7
Vem renascer deixa de marcar bobeira
                    D7    G
Jesus Cristo é liberdade ,Ele é a nossa bandeira!
    C                                  D7
Pra sempre serei de Jesus  pela vida inteira
Dm7                                     C    G7
Porque Sua palavra é luz,   é verdade certeira.
     C                                 D7
Meu Deus a ti agradeço por ter os teus anjos perto de mim
Dm7                    G7               C      G7
E a   bela bandeira de Cristo carrego comigo a sorrir
C                                     D7
E agora essa massa chamada Brasil,  pra Jesus vai ter que louvar
Dm7              G7       C                  G7
Do norte, sul, leste oeste , parado ninguém vai ficar.
   C                    D7
Do novo a 3º idade todo mundo na palma da mão

   F
E ver a nação brasileira
               C        G7
Essa gente festeira cantando o refrão
C           D7
Vamos, com fé vai melhorar
Dm7         G7     C          G7
E Deus irá reinar por todo esse país
 C        D7                      Dm7    G7    C
Louvem  pedindo muita unção , de todo coração, feliz!

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
D7 = X X 0 2 1 2
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
