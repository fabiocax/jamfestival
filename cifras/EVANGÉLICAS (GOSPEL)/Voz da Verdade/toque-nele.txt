Voz da Verdade - Toque Nele

D                             G             D
Não importa quem eu sou,só importa quem Ele é.
                                    A
Se eu sou fraco,Ele e forte se eu caio ele está em pé.
    G          D       A         D
É Jesus de Nazaré, o homem galileu
     G                     D   A       D  G  A
Que mais que um simples homem, Ele e Deus
        D G A G             D  G A G   A
Toque Nele....Jesus, Toque nele....Jesus
C                           F
Muitos tocaram suas vestes,foram por Ele curados
   A#                        A
Hoje chegou o seu dia, Ele esta bem do seu lado
   C                  F
Tenha fé neste nome, nome de glória e poder
      A#                          A
Não duvides, mais creia , que a cura você vai receber
          D G A  G
Toque Nele....Jesus
 A#   C          F           A#    C        F
Ja senti sua vietudi em mim,ja senti o mal sai

A#        C      F           Dm    A#   C      F    A
A liberdade ja soou e livre estou,Jesus me curou
              D G  A                 D  G  A
Se eu sou fraco,Ele e forte,sou pequeno Ele e grande
          D G A                    D G A
Sou um homem,Ele e Deus,se Ele é Deus tudo pode.


        Final: Dm G A# C D G A G D
      Introdução: C G D

----------------- Acordes -----------------
A = X 0 2 2 2 0
A# = X 1 3 3 3 1
C = X 3 2 0 1 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
