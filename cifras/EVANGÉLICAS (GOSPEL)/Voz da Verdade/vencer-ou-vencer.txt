Voz da Verdade - Vencer Ou Vencer

 F            C       Dm  A#
Ele está pertinho de você
 F         C         Dm   A#
Algo novo vai acontecer
F         C          Dm   A#
Basta somente nele crêr
F          C          Dm  A#
Uma nova vida vai nascer
 F              C                Dm A#
A paz que ele traz modifica o mundo
F             C            Dm A#
Quando ele chega resolve tudo
F                   C           Dm  A#
Num segundo ele mostra o seu poder
F               C         Dm    A#
O seu lema é vencer ou vencer

----------------- Acordes -----------------
A# = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
