Voz da Verdade - Águas

 A
Beba desta água cristalina
                      D                 A
Pois a fonte é lá do céu,é jesus de nazaré
                                      D
Beba, e sede acabará, tua alma se alegra,
               A
Num sorriso remédio
Fm                         C#m
Agua que se transforma numa fonte,
                   F#m
Bem dentro do seu ventre
                           E
Vai saltando para a vida eterna
Fm                    C#m
Água que inunda a minh alma
                    F#m
Que transforma meu viver
                     E    G  A
Que me faz nascer de novo
          D              A
Vem derramar água sobre mim

          C                     G
Vem derramar, inundar a minha alma,
             A
Até chegar a ti
          D               A
E faz chover chuva de montão
          D               A
E faz chover chuva de montão
         C
E faz chover, envolvendo corpo
   G                         A
E alma e o meu espírito em louvor

----------------- Acordes -----------------
A = X 0 2 2 2 0
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#- = 2 4 4 2 2 2
F#m = 2 4 4 2 2 2
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
