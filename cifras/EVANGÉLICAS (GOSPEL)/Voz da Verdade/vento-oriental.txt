Voz da Verdade - Vento Oriental

  F                          Am
Sinto um vento, debaixo das asas
        Bb               Am
Que me faz voar no infinito
           F               Am
Quando eu vôo, eu vôo bem alto
            Bb     Am          F
E sinto do lado a mão do Criador.

             C                Gm
Eu vôo por cima de todos os mares
            C                 Dm
Salto montanhas, me sinto no ar
           C                  F
A força bendita que vem do oriente
                Bb        Am            F
Que grita tão forte, pra cima e pra frente.

 F                          Gm
Em pentecostes este vento atuou
                C          Am
Espalhou pelo mundo o Seu amor

               F            C
Que joga pra cima todo sentimento
              Gm                    Dm
E em cada momento, prova estar com você.

Gm                  Dm
Você que estava a cair
Gm                      Dm
Você que pensava em desistir
F      Am     Bb
Há um vento forte,
F           Am       Bb
Que livra você da morte.
F                        Gm
Que o faz voar em meio a luz
                 Dm     Gm
Que o joga pra cima, Jesus.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
