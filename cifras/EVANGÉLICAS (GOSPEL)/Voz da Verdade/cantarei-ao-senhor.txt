Voz da Verdade - Cantarei Ao Senhor

 Gm                           D#
Cantai ao Senhor e louvai ao rei
 A#                     F
E toda a Sua majestade
Gm                             D#
Faz tremer o céu, faz tremer a terra
          A#                             F
Faz a luz brilhar na escuridão

Gm                         D#
Louve ao Senhor porque Ele é bom
         A#                            F
Seu amor dura para sempre
                 Gm                 D#
Entre os querubins o Senhor está

   A#         F        C
Glória! Glória! Glória!

    Am                  F                         C
Cantarei ao Senhor enquanto eu viver

      Am              F                G
Cantarei louvores ao meu rei
         Am                 F              C
Pois Você só me tem feito o bem
       F           G                 C
Cantarei Louvarei Seu nome
       F           G                 C
Cantarei Louvarei Seu nome

   A#         F        C
Glória! Glória! Glória!

[Final] D#  A#  F

----------------- Acordes -----------------
A# = X 1 3 3 3 1
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D# = X 6 5 3 4 3
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
