Voz da Verdade - Sabedoria de Deus

C6                            E                      B
Os pássaros cantam lá no céu
                                         Em
Sem receber nenhum cachê
                     B7
E Deus sustenta a todos
                           Em  C   Em
Mesmo sem ter o que comer
                     C6   B         B7/D#
E as formiguinhas tão sábias
                  Em
Recolhem tudo no verão
                      B
E quando vem o inverno
                      Em         Am     Em
Deus já proveu a salvação
                        B
E a chuva cai tão bonita
                                    Em                                   B7
Vai misturando com a terra Dela produz coisas tão lindas
                        Emaj7
Que o homem sempre espera

                  B
Em tudo isso vejo Deus
              Ebdim        Em
Até no meu duvidar
Am                         D
Porque eu posso aprender
G
Aprender a confiar

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
B7/D# = X X 1 2 0 2
C = X 3 2 0 1 0
C6 = 8 X 7 9 8 X
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Ebdim = X X 1 2 1 2
Em = 0 2 2 0 0 0
Emaj7 = X X 2 4 4 4
G = 3 2 0 0 0 3
