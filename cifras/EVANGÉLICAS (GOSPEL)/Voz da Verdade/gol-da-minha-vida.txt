Voz da Verdade - Gol da Minha Vida

   C              G             F
TENTEI GANHAR O MUNDO COM UMA BOLA
  C                G         F
FAZER MEUS GOLS E SER UM CAMPEÃO
      C              G           F
MAIS JESUS CRISTO MUDOU O MEU CAMINHO
   C                    G          F
GANHEI O MUNDO COM A PALAVRA DO SENHOR
   Em                              F
CANTEI AOS TRISTES, LIBERTEI ENCARCERADOS
    Em                          F
FIZ O INFELIZ SENTIR O QUANTO É AMADO
  Dm                            Am
CUREI DOENTES ENCHUGANDO SUAS FERIDAS
  Dm      F            C
ESTE É O GOL DA MINHA VIDA
  Dm      F           G  C
ESTE É O GOL DA MINHA VIDA

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
