Voz da Verdade - Quantas Vezes

A                             F#m
Quantas vezes você vive a lamentar
      A                         F#m
Sem saber o que fazer e o que pensar
         D          E
Quantas vezes você chora
         A               F#m
Sem ter lágrimas pra derramar
     D   E              A
É difícil, é difícil demais.
         A                   F#m
Quantas vezes você vive sem viver
        A                       F#m
Quantas vezes você ri pra não chorar
         D            E
Quantas vezes os seus pés
        A        F#m
Estão prestes a cair
    D           E      A
É difícil, é difícil demais.


      D E                A
Só JESUS resolve os problemas
      D E               A
Só JESUS derruba as barreiras
       D       C#m        F#m
Só JESUS dá sentido a sua vida
       D       A          E              A
E faz dela colorida, e o sol passa a brilhar.

F#m             C#m
No seu nome estarão
F#m                 C#m
Seus problemas resolvidos
F#m                   c#m     D
Pra sua vida ele dá sentido
E   A   F#m D E   A F#m   D
Vida melhor e mais feliz
E          A  F#m   D  E              A
Vida que é vida, JESUS,  seu nome é JESUS
 A      D       / D E F#m /
JESUS, JESUS.
  A                        E
É ele quem resolve os problemas
  A     D       A        E        A
JESUS, JESUS,  nele você pode confiar

FINAL: D  A

----------------- Acordes -----------------
A = X 0 2 2 2 0
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
