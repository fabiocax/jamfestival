Voz da Verdade - Quando a Noite Chegava

Intro: D, E, D/F#, E/G#, D, A, E7

D9                     G9
Quando a noite chegava, jundo dela a solidão
F#7                 Bm7      G9      A7
Como num encento Jesus apareceu e habitou em meu
     D9
coração

D9                    G9
Hoje não temo a morte, gosto da vida natural
   F#7              Bm7          G9  A7     D9
Jesus com sua humildade, fez-me ver tanta bondade

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm7 = X 2 4 2 3 2
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E7 = 0 2 2 1 3 0
F#7 = 2 4 2 3 2 2
G9 = 3 X 0 2 0 X
