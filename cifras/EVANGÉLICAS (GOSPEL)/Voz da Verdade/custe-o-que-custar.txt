Voz da Verdade - Custe o Que Custar

intro: : Bm A G : Bm A G : Bm A G F#m Em A Bm : 2X

D                       A
Custe o que custar, para frente devo andar
     Em              D/F#         G              A
Com Jesus nesta caminhada, eu não vou poder parar
D                       A
Custe o que custar, em pé eu vou ficar
     Em               D/F#           G            A
As barreiras e as montanhas, vou poder ultrapassar
G       A       Bm   A  G      A      D
Com Jesus eu vencerei, com poder e proteção
   G     A       F#     Bm
No seu nome a vitória é certa
Em        D/F#     C
Ele é o Verbo da criação
Em           D/F#        A9
Por isso eu canto esta canção
G      A       Bm  A  G     A        D
Ele vive e eu vivo, no poder de sua fé
  G     A       F#     Bm
O meu Deus é Jesus de Nazaré

Em        D/F#     C
Ele é o Verbo da criação
Em           D/F#        A
Por isso eu canto esta canção
Bm    A     G                  A
Eu sou príncipe, sou filho de Rei   x4

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
