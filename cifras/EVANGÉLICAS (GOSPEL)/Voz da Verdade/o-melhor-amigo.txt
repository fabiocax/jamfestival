Voz da Verdade - O Melhor Amigo

[Intro] Am  G  C

    C              F         C          Dm      G
Disseram haver no mundo um amigo  procurei mas não
       C                    C7        F  Fm
Encontrei  mas um dia eu olhei para cruz
       C          G         C
Encontrei o meu amigo  é Jesus

( G  F/A  G/B )

        C        F    C          Dm   G
Meu melhor amigo é Jesus  minha vida ele
    C
Conduz
                F        Fm     C           Am
Estendeu a sua mão e me deu salvação  meu melhor
      G    C
Amigo é Jesus

     C              F           C          Dm
Pecador que ouve a voz de redenção  vinde já

      G           C                          C7
Entregar seu coração  deixe agora os seus pecados
      F Fm       C         G7         C
No altar  pois Jesus está pronto a perdoar

( G  F/A  G/B )

        C        F
Meu melhor amigo é Jesus
       Dm          C
Minha vida ele conduz
                 F        Fm     C
Estendeu a sua  mão e me deu salvação
        Am       G     C
Meu melhor amigo é Jesus

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G7 = 3 5 3 4 3 3
