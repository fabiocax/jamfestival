Voz da Verdade - Debaixo Das Asas

[Intro] F  Dm  C/G  Bb

F
 Quando eu olhar bem nos teus olhos
Dm
 Te abraçar é o meu desejo
Bb                            F
 Poder dizer: Jesus eu amo você

Poder estar assim tão perto
Dm
 Esse momento é o que eu preciso
Bb                                      F
 Meu coração é teu, Senhor de Ti não desisto

Gm             F/A
 Tanta saudade eu sentirei de Ti
 Bb
Nunca mais eu esquecerei
Gm           F/A
 Do teu amor, do teu carinho

Bb
 Daquele Homem tão lindo

                   F
Toque agora o meu coração
                     Bb
Faz a minh`alma ir até o céu
                     Dm
Quero sentir a tua proteção
            C/G   Bb
Debaixo das asas
                        F
Onde estiveres quero estar também
                   Bb
Contigo junto eu irei além
                     Dm
Cantando glórias ao Rei dos reis
     C/G          Bb
Pra todo sempre amém

[Solo 1] F  Dm  C/G  Bb
         F  Dm  C/G  Bb

F
 Quando eu olhar bem nos teus olhos
Dm
 Te abraçar é o meu desejo
Bb                            F
 Poder dizer: Jesus eu amo você

Poder estar assim tão perto
Dm
 Esse momento é o que eu preciso
Bb                                      F
 Meu coração é teu, Senhor de Ti não desisto

Gm             F/A
 Tanta saudade eu sentirei de Ti
 Bb
Nunca mais eu esquecerei
Gm           F/A
 Do teu amor, do teu carinho
Bb
 Daquele Homem tão lindo

                   F
Toque agora o meu coração
                     Bb
Faz a minh`alma ir até o céu
                     Dm
Quero sentir a tua proteção
            C/G   Bb
Debaixo das asas
                        F
Onde estiveres quero estar também
                   Bb
Contigo junto eu irei além
                     Dm
Cantando glórias ao Rei dos reis
     C/G          Bb
Pra todo sempre amém

[Solo 2] F  Dm  C/G  Bb
         F  Dm  C/G  Bb
         F  Dm  C/G  Bb
         F  Dm  C/G  Bb

[Final] F  Dm  C/G  Bb

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C/G = 3 3 2 X 1 X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
Gm = 3 5 5 3 3 3
