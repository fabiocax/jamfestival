Voz da Verdade - Jesus Vive

[Intro] D  Em  D  Em  D

D                        A/C#          G/B
É madrugada do terceiro dia, um novo amanhecer ao
         D         A7
Mundo começa a nascer
         D                    A/C#            G/B
Lá no sepulcro onde Cristo jazia não há mais corpo
                           D        A
Algum, o vivente entre os mortos subiu
        G9                  F#m7              G9
Dois varões com roupas resplandescentes anunciam
                 F#m7         G9   Em
Que o Mestre ressuscitou e Maria      e as que lá
   D   A/C#    Bm  Em           A7
Estavam    presentes      correm proclamando o que
       D     A7
Se passou dizendo

       D           A/C#        G/B           D  A7
Jesus vive, Jesus vive, Jesus vive, Ressuscitou

        D           A/C#        G/B           D A
Jesus vive, Jesus vive, Jesus vive, Ressuscitou

            D            A/C#        G/B
Chegando a tarde daquele dia seus discípulos se
              D       A7
Encontram no mesmo lugar
            D                 A/C#       G/B
Todos mui tristes pelo acontecido, esquecendo que
              D        A
Ele havia de ressuscitar
      G9                F#m7                 G9
De repente o Mestre se faz presente, a minha paz
            F#m7               G9
Esteja com todos vós, vede as minhas mãos e os meus
            D  A/C#  Bm  Em         A7
Pés que eu sou o    mesmo,      ide, proclamai
            D           A7
Entre as nações que Eu vivo

       D           A/C#        G/B           D  A7
Jesus vive, Jesus vive, Jesus vive, Ressuscitou
       D           A/C#        G/B           D
Jesus vive, Jesus vive, Jesus vive, Ressuscitou

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F#m7 = 2 X 2 2 2 X
G/B = X 2 0 0 3 3
G9 = 3 X 0 2 0 X
