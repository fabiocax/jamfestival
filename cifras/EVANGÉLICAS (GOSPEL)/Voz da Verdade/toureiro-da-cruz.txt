Voz da Verdade - Toureiro da Cruz

Dm                 C
Veio do céu deixou sua glória, veio em busca
    A#
da vitória
A#           A
Ele nasceu para vencer
Dm
Ele era Deus e era homem, veio revelar seu
A#
nome
A#           A
E resgatar um povo seu
Gm     C              Gm
A sua chegada ao mundo, mexeu com a
            Dm
estrutura do mal
Gm          Dm      A
Chegou para defender, o prêmio que
                   A
reconciliava o homem e Deus
Gm     C                 A#
Ele foi caluniado, perseguido e insultado

                   A#
Mesmo assim nunca cedeu
Dm           C               A#
Destemido e valente caminhava para frente
              A
Para o prêmio receber
Gm      Dm       Gm         Dm
E o grande dia veio, a grande batalha chegou
Gm           Dm
Milhões para assistir
A#
O homem Jesus com o destino das almas nas
A
mãos
C         F       C               F
Entrou na arena-Olé! de frente ao inimigo seu-Olé!
A     Dm       A     Dm
Abriu a capa, do sangue de Deus
C         F       C           F
A cor vermelha, daquele sangue o atraiu
A     Dm        A        Dm
E o adversário, em busca dele o perseguiu
C         F       C            F
Abriu seus braços, deixou a espada o atingir
A          Dm   A#
Seu sangue jorrava, matando o pecado, tirando a maldade
            A             Dm
Comprando o homem da força do mal
A#
E toda platéia dos céus e da terra Do mar e o
A       Dm    C  A#  A  Dm
Universo  gritavam:Olé!

----------------- Acordes -----------------
A = X 0 2 2 2 0
A# = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
