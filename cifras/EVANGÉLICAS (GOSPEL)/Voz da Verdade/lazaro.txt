Voz da Verdade - Lázaro

C#             A#m
Lázaro, sai para fora
             F#          C#
E viva agora o amor de Jesus
C#             A#m
Sai do sepulcro gelado
                              F#
Que alguém do seu lado diz: estou aqui

(passagem)  C#  B  B  F# (2x)  C#M

C#m                              F#m
Talvez estejas morto dentro de Ti mesmo
  B                           E               G#m
Abalado na estruTura dos teus próprios senTimentos
C#m                                  F#m
Uma pedra no caminho, ela é grande demais
B                         E               G#m
Mas escute esta voz: sou Jesus não chores mais

C#             A#m
Lázaro sai para fora

             F#   D#m         G#
Ó morte vai embora, eu sou Jesus
C#             A#m
Vem conhecer a minha vida
           F#   D#m           G#
Faço ela colorida, pro seu viver

E                       C#m
Vem conhecer o meu carinho
                  A  F#m      G#m
Você não está sozinho, estou aqui
E                       C#m
Vai e grite ao mundo inteiro
                      A             F#m         G#m
Que sou o Deus verdadeiro, que faço o morto reviver

C#             A#m
Sai, sai para fora
             F#                      C#
E viva agora o amor de Jesus
C#             A#m
Sai do sepulcro gelado
             F#                      C#
Que alguém do seu lado diz: estou aqui

(passagem)  C#  B  B  F#  C#  B  B  G#

E                       C#m
Vem conhecer o meu carinho
                  A  F#m      G#m
Você não está sozinho, estou aqui
E                       C#m
Vai e grite ao mundo inteiro
                      A             F#m         G#m
Que sou o Deus verdadeiro, que faço o morto reviver

C#             A#m
Sai, sai para fora
             F#                      C#
E viva agora o amor de Jesus
C#             A#m
Sai do sepulcro gelado
             F#                      C#
Que alguém do seu lado diz: estou aqui

(passagem)  C#  B  B  F#  C#  B  B  F#

----------------- Acordes -----------------
A = X 0 2 2 2 0
A#m = X 1 3 3 2 1
B = X 2 4 4 4 2
C# = X 4 6 6 6 4
C#m = X 4 6 6 5 4
D#m = X X 1 3 4 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
G# = 4 3 1 1 1 4
G#m = 4 6 6 4 4 4
