Voz da Verdade - Eu Navegarei

E        Am                  G
Eu navegarei, no oceano do espírito
          F  Em Dm           E
E ali adorarei ao deus de meu amor

   Am                            G
Espírito, espírito, que desce como fogo
            F                        E
Vem como em pentecostes e enche-me de novo

E        Am                   G
Eu adorarei, ao deus de minha vida
           F  Em Dm             E
Que me compreendeu sem nenhuma explicação

   Am                            G
Espírito, espírito, que desce como fogo
            F                        E
Vem como em pentecostes e enche-me de novo

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
