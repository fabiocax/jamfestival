Voz da Verdade - O Hino do Soldado

D           A      Em                Bm
Soldados feridos, mas sempre invenciveis
G          D/F#             A
crentes caminhando rumo ao céu.
D             A     Em             Bm
rostos flamejantes como a própria luz.
G             A          D
estes são soldados de Jesus.
Bm              G    A     D
Sempre prontos para enfrentar
Bm            G       A      D
o inimigo no céu na terra e mar
Bm                   G     A       D
Com força é fé destemidos na batalha.
   Bm                     G  A     D
No nome de Jesus eles saltam muralhas.
  A                   D
sofrem aflições como bom soldado.
    G              D         A
Carregam nas suas costas o amor.
                   D
Pois a batalha produz um peso eterno.

     G      D       A      D
De gloria excelente no senhor.
     Bm
Ás vezes de joelho, as vezes de pé
Em
caminham não por vistas, caminham pela fé.
       A                        D
pois além dos limites da fronteira
G                 F#7
Veram Jesus de nazaré.

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
F#7 = 2 4 2 3 2 2
G = 3 2 0 0 0 3
