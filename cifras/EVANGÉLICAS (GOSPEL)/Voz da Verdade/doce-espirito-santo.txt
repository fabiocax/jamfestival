Voz da Verdade - Doce Espírito Santo

Intro: F  Bb  C  F  Bb  C

F             C      Dm     Bb9     C        F   F7
Doce,(doce) Espírito Santo, Vem pousar em   mim
Bb            C            Dm         Bb    C           F   C
Desce,(desce) sobre a minh'alma, Doce calma  quero sentir    (2x)

Gm
Vem, vem voando pra minh'alma
     Bb      C          Gm
Liberdade eu quero  gritar

Gm
Vem como um rio envolvente
       Bb    C        Gm
Que me leva  até   Jesus

F  Bb       C
Jesus eu te amo
F  Bb         C
Jesus eu te adoro

       F    Bb    C
Gosto muito de você
    Bb            C       F
Navegar no teu rio é viver
    Bb            C         F
Navegar no teu rio é viver

Vem, vem voando...

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bb9 = X 1 3 3 1 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
Gm = 3 5 5 3 3 3
