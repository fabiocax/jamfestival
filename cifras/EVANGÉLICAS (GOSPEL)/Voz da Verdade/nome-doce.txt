Voz da Verdade - Nome Doce

E                A                    E G A
Foi ele e não outro que me trouxe a paz
E               A                B
É ele e não outro que nos satisfaz, Jesus
G#m                     C#m
Seu nome é doce calma,preenche a minha alma
   F#m                          B
Quando há um vazio dentro de mim
    G#m                         C#m
E ele que sustenta quanto falta-me o pão
   F#m                  B
É paz e alegria na tristeza a solução
E     C#m E     C#m   E         C#m       A  G A
Nome doce,lindo nome,grande é o nome de Jesus
E     C#m E            C#m E         C#m      A
Nome doce,lindo lindo nome,grande é o nome de Jesus
B
É Pai da Eternidade e Príncipe da Paz,é o Todo Poderoso

É o grande EU SOU
E     C#m E      C#m E         C#m      A    G A
Nome doce,lindo nome,grande é o nome de Jesus

E     C#m E            C#m E         C#m      A
Nome doce,lindo lindo nome,grande é o nome de Jesus
B
É Pai da Eternidade e Príncipe da Paz,é o Todo Poderoso

É o grande EU SOU

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G#m = 4 6 6 4 4 4
