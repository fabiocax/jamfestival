Voz da Verdade - Sai o Sol

Intro: D  Bm  G  A

       D       Bm        G           A
Sai o sol na manhã tão bonito e tão forte
     D        Bm         G          A
E o dia inteirinho Deus será minha sorte
       D            Bm       G          A
Sai a tarde, vem a noite e a lua tão brilhante no céu
        D        Bm             G        A
E as estrelas reluzentes vão rasgando o véu
               D
Dizendo Deus é real
        Bm      G       A
Deus é real, é real, é real
                 D
Dizendo Deus é real
        Bm     G        A
Deus é real, é real, é real

       D         Bm         G            A
No sorriso de criança ele mostra a esperança

       D             Bm             G        A
E no brilho de seus olhos mostra a luz e bonança
       D            Bm       G        A
Num abraço de um irmão ele mostra a emoção
        D          Bm       G              A
Numa Lagrima de Saudade no amor sinto sua mão

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
G = 3 2 0 0 0 3
