Voz da Verdade - Onde Estavas Tu?

F# C#  D  A F#  C#  D E A
 A le lu ia,A  le lu i a
A
Onde estavas tu
           E
Quando fundava a terra
    A
Se tens inteligência
        E
Faz de mim saber
A
Onde estavas tu
        E
Quando fiz o mar
  A
Quanto as trevas
             E        G
Onde está o seu lugar
                  D
Dei luz pro Orion
                      B
Produzi constelações


Enquanto os meus filhos jubilavam
                E     G
Cantando o meu nome
                  D
Quem guia a ursa
                           B
Com todos os seus filhos

Quem fez casa pra cruz
       E
Sou Jesus
F# C# D  A F# C# D E A
 A le lu ia,A  le lu i a
    A
Um dia me fiz homem
       E
E não tive um berço
 A
Fui da estebaria
          E
Morar na Galiléia
   A
Estava num mundo
 E
Muito feliz
   A
Estava tão contente
     E             G
Por tudo que fiz
                   D
Me deram uma cruz
                         B
Foi a minha recompensa

Mas tudo o que fiz
         E     G
Valeu a pena
                    D
Me deram uma cruz
                         B
Foi a minha recompensa

mas tudo que fiz
         E
Valeu a pena
F# C# D A F# C# D E A
 A le luia,A le lu i a
G# D# E B G# D# E F# B
 A le luia,A le lu i  a

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C# = X 4 6 6 6 4
D = X X 0 2 3 2
D# = X 6 5 3 4 3
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
