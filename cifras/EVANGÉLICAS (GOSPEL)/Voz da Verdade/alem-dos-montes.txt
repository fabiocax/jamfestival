Voz da Verdade - Além Dos Montes

Intro: G, C9, D, Em7 - 4 VEZES   D/F#, C9, G9

G9             Em7
Há uma esperança, além dos montes
Am7     Am/G         D/F#
Muito além dos pensamentos
G9                     Em7
Essa esperança que traz paz e confiança
Am7   Am/G       D/F#
E coragem para prosseguir
C9                  G/B  C9               G/B
Quantas quedas eu já tive, mas não canso de lutar
C9            G       D/F#  Em7
Eu escalo a montanha e chego lá
       Am7 Am/G   C9         D7
Nem a morte me impedirá de ao céu chegar
               F7+        C9  Am7    D        C D
2ª vez: Nem a morte me impedirá de ao céu chegar


           C9           G9       C9       G9
Não mais choro nem mais guerra, só amor e muita paz

      C9        D   Em7
Caminhando pra Jerusalém
        C9        G9          C9        G9
Onde a vida mata a morte, pois Jesus é nossa sorte
      C9        D   Em7
Caminhando pra Jerusalém

SOLO: A, D9, A, D9, A, D9, E, F#m7, D9, A,
D9, A, D9, E, F#m7

           D9           A9       D9       A9
Não mais choro nem mais guerra, só amor e muita paz
      D9        E   F#m7
Caminhando pra Jerusalém
        D9        A9          D9        A9
Onde a vida mata a morte, pois Jesus é nossa sorte
      D9        E   F#m7
Caminhando pra Jerusalém

FINAL: D9, E, D/F#, E/G#, D9, E, D9, A9

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
Am/G = 3 X 2 2 1 X
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
Em7 = 0 2 2 0 3 0
F#m7 = 2 X 2 2 2 X
F7+ = 1 X 2 2 1 X
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G9 = 3 X 0 2 0 X
