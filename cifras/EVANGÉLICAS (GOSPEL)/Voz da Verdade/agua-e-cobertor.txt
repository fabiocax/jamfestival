Voz da Verdade - Água e Cobertor

G                                            D/F#
Quantas vezes eu quis te ajuntar debaixo das asas
          C                               Em
Quantas vezes você preferindo ao mundo se foi
          G                           D/F#
Quantas vezes no deserto eu fui tua água
          C                                 Em
Quantas vezes no inverno eu fui o Teu cobertor
     G                         D
Todo dia Eu faço um céu diferente
     C                  G
Mas você finge que não vê
        G                              D/F#
Faço o sol brilhar no dia muito mais quente
      C                    G
Mas você não sente e não crê
   C            Ebº (OU Ebdim) G             Em
Tento chamar a atenção fazendo brilhar uma luz
       Am                D7
Mas você esqueceu o Meu nome
                G
O Meu nome é Jesus

   Em        C   D7              G
Te amo, te quero,  Meu nome é Jesus
   Em         C   D7              G
Te amo, te espero,  Meu nome é Jesus
Bm            C   Bm            Am
Venha como estás, vem pro meu amor
Bm             C
Pois Eu sou Jesus
    C/D            G
Tua água e o cobertor
G                      Em
Sempre te amei, sempre te amarei
     C                     C/D            (G)
Sou Jesus, Teu Senhor, Teu amor e Teu Rei

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C/D = X X 0 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
Ebº = X X 1 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
