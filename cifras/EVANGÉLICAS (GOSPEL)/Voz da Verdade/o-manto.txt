Voz da Verdade - O Manto

 C         Em         Am       Em   F
Deixa-me tocar em seu manto Senhor para que eu
 G       C  G7
possa sarar
 C          Em         Am       Em  F
Deixa-me tocar em seu manto Senhor para que eu
 G       C
possa sarar

Am       Em    F            C
Se você ainda não tem essa luz
 Am             Em        G7                   C G7
Cobre-te com o manto que foi disputado lá na cruz

 C          Em
Deixa-me tocar...

 Am         Em     F                     C
Para que temer se temos um Deus que é Jesus?
Am                Em   G7              C  G7
Eu conheço a sua força e também a sua luz


 C          Em
Deixa me tocar...

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
