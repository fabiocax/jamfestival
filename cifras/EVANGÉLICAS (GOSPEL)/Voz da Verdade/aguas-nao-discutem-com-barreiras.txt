Voz da Verdade - Águas Não Discutem Com Barreiras

 Intro 4x: Bm  G
E|------9---9--9------5---5--5------|
B|-------7---7--7------3---3--3-----|
G|--------6---6--6------2---2--2----|
D|----------------------------------|
A|----------------------------------|
E|----------------------------------|

Bm            A             G
Águas não discutem com barreiras
Bm        A        G
Simplesmente ultrapassam
Bm        A          G
Crente não olha o problema
Bm           A           G
Simplesmente avança pra vencer

         D          A         G
Porque águas só descançam no mar
         D             A         G
E a minha alma só descança em Jesus

      Em                    A
E não há barreira que me impeça
      G                   A
Vencerei com o poder da cruz

( D  G ) (4x)
Riff 1:
E|-----------|
B|-----------|
G|--------7--|
D|--------7--|
A|--------5--|
E|--3-2-0----|

D                G     D                  G   (Riff 1)
Sou filho da promessa, sou mais que vencedor
D                G    D                 G    (Riff 1)
Pois Ele me escolheu, sou ungido do Senhor
D            G     D                  G      (Riff 1)
Ele é maravilhoso, conselheiro e Deus forte
D                  G    D                  G
Sigo o curso deste rio, aonde está a minha sorte

           Em             Bm
Não vou parar, eu vou correr
           Em              Bm
Não tenho tempo pra descançar
           Em               Bm
Meu rio que corre ao seu encontro
       G                A      D
Pois Deus me espera no seu altar  (bis)

( G  A  Em ) (4x)

( C  G  D  Em  C  G  D  A )
Riff 2:
E|------------------|
B|------------------|
G|------------------|
D|---5--7-------2---|
A|---5--7-------2---|
E|---3--5--3-2--0---|

(Riff 2)                (Riff 2)                         (Riff 2)
      Eu não vou parar       e ninguém vai me segurar,       eu chego lá

( G  D  A  Em )

Refrão 4x:
D                G
Nele está a vida, nele está a paz,
D                    G
Sigo o curso deste rio
                 D        G
Sou filho da promessa

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
