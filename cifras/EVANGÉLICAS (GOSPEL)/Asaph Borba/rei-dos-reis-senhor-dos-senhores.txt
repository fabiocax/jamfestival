Asaph Borba - Rei dos reis, Senhor dos senhores

(intro) G  D/F  Am  C

  G                 D/F#
Jesus, meu Senhor! Jesus, Salvador!
  Em        C                 G  D
Jesus, Teu nome é sobre todo nome

  G                 D/F#
Jesus, Bom Pastor! Jesus, meu amor!
  Em   C                 G    D
Jesus, exaltado És sobre tudo

         G
Tu És o Rei dos reis, Senhor dos senhores!
D/F#
Rei dos reis, Senhor dos senhores!
Em                C                   Am  C  D ( G )
Rei dos Reis, Senhor dos senhores, Jesus!

    Em                Bm        C                    G
Um dia todo joelho se dobrará e toda língua confessará
  Em                Bm
O Teu governo sobre as nações
     C                  D          E
Que juntas virão Te adorar, Te adorar

         A
Tu És o Rei dos reis, Senhor dos senhores!
         E/G#
Rei dos reis, Senhor dos senhores!
F#m           D                       Bm    E
Rei dos Reis, Senhor dos senhores, Jesus!

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F = X X 3 2 3 2
D/F# = 2 X 0 2 3 2
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
