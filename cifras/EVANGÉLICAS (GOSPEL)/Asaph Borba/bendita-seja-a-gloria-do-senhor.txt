Asaph Borba - Bendita Seja a Glória do Senhor

  Intro:  F4   F   F4  F
                                 F                              C
  Levantou-me o Espírito  -  Ouvi Sua voz
             Gm7                 C                   F
  Proclamando com poder e grande glória 
                                     F                                     C
  Restaurando minha vida  -  Na presença do Senhor 
          Gm7          C             F
  Através de Sua graça e amor

         C                Bb/C           F
  Bendita seja a glória do Senhor
         C               Bb/C           F
  Bendita seja a glória do Senhor
         Bb              F                  A7             Dm7
  Bendita seja a glória - Bendita seja a glória
         Gm7          Bb/C           F4  F
  Bendita seja a glória do Senhor

                       F                                    C
  Com Sua palavra  -  Encheu meus lábios
      Gm7                  C      F
  E fez de mim um sacerdote
                      F                              C
  Para profetizar  -  Vida e salvação
          Gm7      C             F
  Esperança a todo coração

  Bendita seja glória do Senhor...

          Dm7                 C                   Bb                     F/A
  Sua glória enche a Terra  -  Sua glória enche a igreja
          Gm7                 C               F
  Sua glória enche a casa e a família   
          Dm7                C                 Bb                    F/A
  Sua glória enche a vida  -  Sua glória enche o templo
          Gm7             C4               F4    F
  Sua glória enche todas as nações

  Bendita seja a glória do Senhor... (2X)

  Dm  C/E  F   Dm  C/E  F   Gm7  F/A  C  Gm7  F/A  C
  Glória           Glória           Glória           Glória

   Bb9 F   Bb9  F    Bb9  F   Bb9  C  F
  Glória,  Glória    Glória,  Glória

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bb = X 1 3 3 3 1
Bb/C = X 3 X 3 3 1
Bb9 = X 1 3 3 1 1
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
C4 = X 3 3 0 1 X
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
F4 = 1 3 3 3 1 1
Gm7 = 3 X 3 3 3 X
