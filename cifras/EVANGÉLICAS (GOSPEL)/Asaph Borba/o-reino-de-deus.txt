Asaph Borba - O Reino de Deus

 D            G
O Reino de Deus não é firmado em obras
A              D
feitas por homens quaisquer,
  Bm         E     E7
não tem sua base em ouro ou poder
A          D
e nem tampouco em saber.
D           G
Não é feito de comidas e bebidas,
  A    F#m  Bm
mas sim de muitas vidas
G      Em    A
que se tornaram filhos de Deus
F#m         Bm
que se tornaram filhos de Deus
G       Em  A
que se tornaram filhos de Deus
  D
em Jesus.
  Am
O   Reino de Deus é de paz e justiça
      Em
e   a alegria no Espírito Santo.
  Bm
O   Reino de Deus é o amor,
    Am
e   o poder da Palavra e o louvor,
      Em
é   a vitória e a vida em Jesus
      E7
é   a vitória e a vida em Jesus
         A7
o   Reino é a cruz.
 D            G
O Reino de Deus não é firmado em obras...

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
