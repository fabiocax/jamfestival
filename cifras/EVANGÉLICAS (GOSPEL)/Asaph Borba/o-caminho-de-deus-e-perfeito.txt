Asaph Borba - O Caminho de Deus É Perfeito

      Dm
O Caminho de Deus é perfeito
      Bb
Na palavra do Senhor há poder
        Gm                A7                 Dm
Ele é a arma, o escudo de todos os que nele confiam
         Dm
Deus é a minha fortaleza e a minha força
    Bb
Com ele passo pelo meio de um batalhão
        Gm                    A7           Dm
Ele adestra minhas mãos, me prepara para guerrear

F                  C     Gm             Bb
Porque quem é Deus        senão o Senhor
F                    C     G             Bb A7
E quem é o rochedo         senão nosso Deus

     Dm         Bb      Gm   A7      Dm
Aleluia,   aleluia,    aleluia,   aleluia
  Dm
Persegui os inimigos e os alcancei
     Bb
Consumi e os atravessei
       Gm    A7
Sob os pés do senhor caíram
        Dm
Não mais se levantaram

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
