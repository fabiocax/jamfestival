Asaph Borba - Minh'Alma Engrandece

F   Bb   Am7     Dm7(9)
A minh'alma engrandece ao Senhor

Gm7    C4   F    Bb    C4   C
E o meu espírito se alegra em Deus meu Salvador

F   Bb  Am7    Dm7(9)
Pois com poder tem feito grandes coisas

Gm7   C4    F
E com misericórdia demonstrado amor

Bb  C/Bb    Am7 Dm7(9)
Santo, Santo

Gm7    C4   F7  Gm7 G#m6 Am7
Santo és Senhor

Bb  C/Bb    Am7 Dm7(9)
Santo, Santo

Gm7    C4  F
Santo és Senhor

Bb  C/Bb
Tem enchido nossas lâmpadas
Am7  Dm7(9)
Com o óleo do Espírito

Gm   G7    C
Tem feito sua vide florescer

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C/Bb = X 1 2 0 1 X
C4 = X 3 3 0 1 X
Dm7(9) = X 5 3 5 5 X
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
G#m6 = 4 X 3 4 4 X
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
