Asaph Borba - Como É Precioso Irmão

C      Am         Dm          G
Como é precioso irmão, estar bem junto a ti
C       Am        Dm        G
E juntos lado a lado, andarmos com Jesus

   Am          Em      F   G    C
E ex-pres-sar-mos o a-mor que um dia Ele nos deu
     Am       D      F    G    C
Pelo sangue do Calvário, Sua vida trouxe a nós

 Am       Em       F   G    C
Ali-an-ça no_ Se-nhor eu tenho com você
  Am        D           G
Não existem mais barreiras em meu ser

   Am            Em      F
Eu  sou livre pra te amar pra te aceitar
   C          F       G  C    C7
E para te pedir: “perdoa-me irmão"

    F    E    Am         F
Eu sou um com você, no amor do nosso  Pai
   C   G         C   C7
Somos um  no  amor   de   Jesus

    F    E    Am      F
Eu sou um com você, no amor do nosso  Pai
   C    G     C
Somos um no amor de Jesus

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
