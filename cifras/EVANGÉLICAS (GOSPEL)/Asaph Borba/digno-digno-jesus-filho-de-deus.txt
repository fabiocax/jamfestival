Asaph Borba - Digno, Digno, Jesus Filho de Deus

 D     A  Bº  Bm    G    A        D   G/A
Digno, Di - gno, Jesus, Filho de Deus
 D     A  Bº  B      G    A        D   
Digno, Di - gno, Jesus, Filho de Deus
 D     D/F#  G        A       D
Porque foste morto e com teu sangue
    G      A            D
Compraste para o nosso Deus
 D             G    F#    Bm 
Homens que proce - dem de toda tribo
            Em7    G/B  G/A                            
Língua, povo e na - ção

  A7            D                A/C#
E para o nosso Deus, os constituístes
F#/A#        Bm
Reino e sacerdotes
                   Am7
E reinarão sobre a Terra
D     G  A/G     F#m7   Bm7
A-le-lui-a, a-le-lui- a
                   Em7      A4  A  D
E reinarão sobre a Terra, alelu - ia

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A/G = 3 X 2 2 2 X
A4 = X 0 2 2 3 0
A7 = X 0 2 0 2 0
Am7 = X 0 2 0 1 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
Bº = X 2 3 1 3 1
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em7 = 0 2 2 0 3 0
F# = 2 4 4 3 2 2
F#/A# = 6 X 4 6 7 X
F#m7 = 2 X 2 2 2 X
G = 3 2 0 0 0 3
G/A = 5 X 5 4 3 X
G/B = X 2 0 0 3 3
