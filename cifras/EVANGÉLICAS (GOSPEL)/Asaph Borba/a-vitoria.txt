Asaph Borba - A Vitória

          G                        C7+                          G
Quanta coisa tenho feito parao meu próprio prazer   tenho andado a
                 C7+                   B7     Em
Procura do meu próprio bem viver   enquanto existe tanta gente
Bm                 C            A/C#           D
ansiosa por aí não te conhecendo assim como eu conheço a Ti
      G                    C7+                G                 C7+
O chamado que um dia Tu fizeste a mim  e ao qual sem Hesitar eu disse
      B7 Em                       Bm                  C
Sim resoou em meus ouvidos como da primeira vez e a ti jesus eu
A/C#      D                   C7+                   Bm
Novamente digo sim    eis-me aqui eu livre estou a teu dispor
     C           D           G  B       Em              Bm
Para onde Tu quiseres me enviar Me coloco submisso a Ti Senhor
C             D        G
Para o Teu querer em mim realizar    ­ eis-me aqui ...

----------------- Acordes -----------------
A/C# = X 4 X 2 5 5
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C7+ = X 3 2 0 0 X
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
