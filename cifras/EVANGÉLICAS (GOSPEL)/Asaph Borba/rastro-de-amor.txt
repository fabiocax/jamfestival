Asaph Borba - Rastro de Amor

A                    D/F#      E4       E          A
Quem vê de longe não sabe, não sabe o quanto eu andei
A                    D/F#      E4       E          A
Quem vê de longe não sabe, não sabe o quanto eu chorei
D/F#                  E4      E        E/G#            F#m7
Quem vê de longe não sabe o caminho, estreito no qual passei
        D                  A         Bm7                E4      E
Para seguir as pegadas de CRISTO, o rastro de quem me amou

A                    D/F#      E4       E          A
Quem vê de longe não sabe, não sabe o que eu já vivi
A                    D/F#      E4       E          A
Quem vê de longe não sabe, não sabe o quanto eu aprendi
D/F#                  E4      E        E/G#            F#m7
Quem vê de longe não sabe o caminho, que DEUS me ajudou a trilhar
        D                  A         Bm7                E4      E
Pra deixar as pegadas na areia, e assim outros pudessem passar
Bm7                         A
E assim outros pudessem passar

C#m7        D    Bm7             E4  E   C#m7        D   Bm7           E4   E
Rastros de amor, foi o que eu segui,    rastros de amor quero deixar aqui
C#m7            F#m7               D                     E4     E
Acima de todo o brilho do mundo o exemplo é o que deve ficar
Bm7                 C#m7                      D               E4      E
Para que aqueles que seguem meus passos nunca venham a se desviar

C#m7         D    Bm7           E4  E     C#m7      D    Bm7          E4   E
Rastros de amor, foi o que eu segui,    rastros de amor quero deixar aqui
C#m7            F#m7                D                    E4     E
Acima de todo o brilho do mundo o exemplo é o que deve ficar
Bm7                  C#m7                      D            E4      E
Para que aqueles que seguem meus passos nunca venham a se perder
Bm7                 A
E como JESUS possam ser

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E4 = 0 2 2 2 0 0
F#m7 = 2 X 2 2 2 X
