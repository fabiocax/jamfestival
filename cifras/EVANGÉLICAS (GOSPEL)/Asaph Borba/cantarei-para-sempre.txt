Asaph Borba - Cantarei Para Sempre

                   Em  Am     Em
Cantarei para sempre
       Am   B   Em           B      Em
As Tuas misericórdias, oh Senhor
   Am       D       G    Em
Os meus lábios proclamarão
     Am          D     G    Em
A todas as gerações
     Am      B     Em
A Tua fidelidade
   G    Em
Oh Senhor

      Em   Am   Em
Celebram os céus
       Am   B   Em
As Tuas maravilhas
   B       Em
Oh Senhor
    Am      D      G      Em
Na assembléia dos santos
     Am    D   G     Em
A Tua fidelidade
   Am                       B        Em
Pois quem no céu é comparado
     B        Em
A Ti Senhor

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
