Asaph Borba - Bendizei Ao Senhor

              C      F
Bendizei ao Senhor
        C       F
Vós todos os servos do Senhor
        C       F
Que assistis na casa do Senhor
C
Nas horas da noite

        Bb      F
Erguei as vossas mãos
             G
Para o santuário
         C       G
E bendizei ao Senhor
      C
Senhor

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
