Asaph Borba - Alto Preço

Capo Casa 2

Intro: F#m  Bm (4x)

   F#m                       C#m    D                               F#m
Eu sei que foi pago um alto preço Para que contigo eu fosse um meu irmão
F#m               C#m
Quando Jesus derramou sua vida
D                  E                             F#m
Ele pensava em ti, Ele pensava em mim, Pensava em nós

   F#m                       C#m    D                               F#m
Eu sei que foi pago um alto preço Para que contigo eu fosse um meu irmão
F#m               C#m
Quando Jesus derramou sua vida
D                  E                             F#m
Ele pensava em ti, Ele pensava em mim, Pensava em nós
      D                       A         D               E        A
E nos via redimidos por seu sangue,       Lutando o bom combate do Senhor
       D                 D                        A                       E
Lado a lado trabalhando, sua Igreja edificando E rompendo as barreiras pelo amor

      A                 E        F#m         C#m
E na força do Espírito Santo nós proclamamos aqui
    D           F#m                     Bm         E
Que pagaremos o preço de sermos um só coração no Senhor
       A                   E           F#m       C#m
E por mais que as trevas militem e nos tentem separar
    D               F#m       Bm      E     A
Com nossos olhos em Cristo, unidos iremos andar

         A                 E        F#m         C#m
E na força do Espírito Santo nós proclamamos aqui
    D           F#m                     Bm         E
Que pagaremos o preço de sermos um só coração no Senhor
       A                   E           F#m       C#m
E por mais que as trevas militem e nos tentem separar
    D               F#m       Bm      E     F#m
Com nossos olhos em Cristo, unidos iremos andar
 Bm       E     F#m
Unidos iremos andar
 Bm     E      A
Unidos iremos andar.

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
