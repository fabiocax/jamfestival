Asaph Borba - No Teu Coração Oh Pai

            F               C/E
No Teu coração, oh Pai
           Dm7       Dm7/C
Existe uma fonte
          Gm         Gm/F
Que jorra sem cessar
          Csus
Vida abundante.
            F           C/E
No Teu coração, oh Pai
                  Dm7    Dm7/C
Brota um rio de amor
             Gm      Gm/F
De graça, esperança
               Csus   C
E de plena alegria, Senhor!
   C#°     Dm C/E         F
No Teu coração existe um lugar
         F/A          Bb     G7
Pra cada um dos Teus filhos
                  Csus C
A quem vieste salvar
   C#°     Dm C/E         F   ( F4  F )
No Teu coração encontro a Paz
      F/A     Bb
Verdadeiro descanso
 Gm     Csus  C7
Vida e luz:

          F      C/E   Dm7    C  (2x)
Encontro Jesus!

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C#° = X 4 5 3 5 3
C/E = 0 3 2 0 1 0
C7 = X 3 2 3 1 X
Csus = X 3 2 0 1 0
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Dm7/C = X 3 0 2 1 1
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
F4 = 1 3 3 3 1 1
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
Gm/F = X X 3 3 3 3
