Asaph Borba - O Nosso General e Cristo

[Intro] C#m7  A7M  F#m7  G#7

[Primeira Parte]

         C#m7
Pelo Senhor marchamos sim   
          A7M
E o seu exército poderoso é   
      F#m7       B9       G#m7   C#m7  G#7
Sua glória será vista em toda a terra 

[Segunda Parte]

         C#m7
Vamos cantar o canto da vitória
          A7M
Glória à Deus vencemos a batalha
     F#m7         B9    G#m7   C#m7
Toda arma contra nós pere - cerá
      
[Refrão]

   A            E
O nosso general é Cristo   
    A             E
Seguimos os seus passos   
    A           F#m7   G#m7 A     B9   C#m7
Nenhum inimigo nos    re   sis - ti - rá

   A            E
O nosso general é Cristo   
    A             E
Seguimos os seus passos   
    A           F#m7   G#m7 A     B9   C#m7
Nenhum inimigo nos    re   sis - ti - rá
  
[Segunda Parte]
 
          C#m7
Com o Messias marchamos sim   
         A7M
Em suas mãos a chave da vitória   
         F#m7             B9     G#m7   C#m7  G#7
Que nos leva a possuir a terra prome - tida      

          C#m7
Com o Messias marchamos sim   
         A7M
Em suas mãos a chave da vitória   
         F#m7             B9     G#m7   C#m7  G#7
Que nos leva a possuir a terra prome - tida    

[Refrão]

   A            E
O nosso general é Cristo   
    A             E
Seguimos os seus passos   
    A           F#m7   G#m7 A     B9   C#m7
Nenhum inimigo nos    re   sis - ti - rá

   A            E
O nosso general é Cristo   
    A             E
Seguimos os seus passos   
    A           F#m7   G#m7 A     B9   C#m7
Nenhum inimigo nos    re   sis - ti - rá

[Segunda Parte]
 
          C#m7
Com o Messias marchamos sim   
         A7M
Em suas mãos a chave da vitória   
         F#m7             B9     G#m7   C#m7  G#7
Que nos leva a possuir a terra prome - tida      

          C#m7
Com o Messias marchamos sim   
         A7M
Em suas mãos a chave da vitória   
         F#m7             B9     G#m7   C#m7  G#7
Que nos leva a possuir a terra prome - tida    

[Refrão]

   A            E
O nosso general é Cristo   
    A             E
Seguimos os seus passos   
    A           F#m7   G#m7 A     B9   C#m7
Nenhum inimigo nos    re   sis - ti - rá

   A            E
O nosso general é Cristo   
    A             E
Seguimos os seus passos   
    A           F#m7   G#m7 A     B9   C#m7
Nenhum inimigo nos    re   sis - ti - rá

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7M = X 0 2 1 2 0
B9 = X 2 4 4 2 2
C#m7 = X 4 6 4 5 4
E = 0 2 2 1 0 0
F#m7 = 2 X 2 2 2 X
G#7 = 4 6 4 5 4 4
G#m7 = 4 X 4 4 4 X
