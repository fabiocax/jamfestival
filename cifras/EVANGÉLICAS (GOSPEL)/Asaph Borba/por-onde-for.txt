Asaph Borba - Por Onde For

Introdução: {A  D}4X  E7  A
     E    F#m     D    A              E   F#m   D   A
Tudo que posso farei                 para agradecer-te
     E      F#m     D            A      E   F#m (E  F#m E/G# A E/G# F#m E) D
O que fizeste por mim               é prova do amor maior
            A          D           A      D
Quando chorei me abraçou, não me deixou só, nunca me abandonou,
         A            D         C#m       Bm     E7
Um novo nome me deu, no Livro escrito está, eu sou herdeiro de Deus!
               A                 E       A         Bm     E7
                    Eu vou levar o Seu amor, por onde for, eu vou levar!
               A               E        A     Bm    E7
                    Vou adorar o Seu nome, onde estiver vou adorar!
                      D   A      D       A
                    Batendo palmas, com corpo expressar,
                     D    A       E7      A (Introdução)
                    Em alta voz (Jesus) eu vou louvar!
        E  F#m  D A             E      F#m    D   A
Se alguém me perguntar:       “Por que o brilho no olhar?"
      E  F#m    D      A      E   F#m  (E F#m E/G# A E/G# F#m E) D
Então responderei (Venceu!): “Prova do amor maior!"
            A          D           A     D#°
Quando chorei me abraçou, não me deixou só, nunca me abandonou
    D    C#m         Bm        A         Bm        E7
Um novo nome me deu, no Livro escrito está,      eu sou herdeiro de Deus!

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
D#° = X X 1 2 1 2
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
