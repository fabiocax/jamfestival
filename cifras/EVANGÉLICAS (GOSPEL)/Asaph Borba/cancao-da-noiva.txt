Asaph Borba - Canção da Noiva

     E          A/E           E   A/E
O Espírito  e  a noiva dizem vem
   G#m7          C#m7        B7/4  B7
O Espírito  e  a noiva dizem vem
   A                B/A   G#m7    C#m7
És bem-vindo, entre nós, senhor jesus
   F#m7          B7          E
O Espírito e a noiva dizem vem

   Am                  E
Já ouço a voz do meu amado
    Am                  E
Sua voz mais doce que o mel
   G#m7                     C#m7
Ninguém é mais formoso do que ele
   F#7                B7/4  B7
Ninguém na terra ou no céu

    Am                       E
Eu sou, sim, eu sou do meu amado
    Am          E
E o meu amado é meu
  G#m7             C#m7
Leva-me a sala do banquete
   F#7                     B7/4  B7
Sua bandeira sobre mim é o amor

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/E = 0 X 2 2 2 0
Am = X 0 2 2 1 0
B/A = X 0 4 4 4 X
B7 = X 2 1 2 0 2
B7/4 = X 2 4 2 5 2
C#m7 = X 4 6 4 5 4
E = 0 2 2 1 0 0
F#7 = 2 4 2 3 2 2
F#m7 = 2 X 2 2 2 X
G#m7 = 4 X 4 4 4 X
