Isaías Saad - Espírito Vem

[Intro]  F  Am  G  Em
         F  Am  G  Em
         F  Am  G  Em
         F  Am  G  Em

[Primeira Parte]

F         Am  G
  Pai nosso
        Em          F
Todo o céu adora a Ti
      Am  G
Cantamos
      Em            F
Vem e enche este lugar
         Am  G
Posso ouvir
          Em            F  Am
O som do céu tocando aqui
G           Em            F
  O som do céu tocando aqui

F         Am  G
  Pai nosso
        Em          F
Todo o céu adora a Ti
      Am  G
Cantamos
      Em            F
Vem e enche este lugar
         Am  G
Posso ouvir
          Em            F  Am
O som do céu tocando aqui
G           Em            F
  O som do céu tocando aqui

[Refrão]

F           G
  Espírito Vem
Am               Em
  Quebranta meu ser
F           G
  Espírito Vem
Am              Em
  Faz o céu descer
F           G
  Espírito Vem
Am               Em
  Quebranta meu ser
F           G
  Espírito Vem
Am              Em
  Faz o céu descer

[Segunda Parte]

F        Am  G
  Rei Jesus
     Em          F
Exaltamos o teu nome
        Am  G
Tua glória
        Em          F
Faz tremer terra e céu
       Am  G
Aviva-nos
         Em             F  Am  G
Queremos o teu reino aqui
         Em             F
Queremos o teu reino aqui
F        Am  G
  Rei Jesus
     Em          F
Exaltamos o teu nome
        Am  G
Tua glória
        Em          F
Faz tremer terra e céu
       Am  G
Aviva-nos
         Em             F  Am  G
Queremos o teu reino aqui
         Em             F
Queremos o teu reino aqui

[Refrão]

F           G
  Espírito Vem
Am               Em
  Quebranta meu ser
F           G
  Espírito Vem
Am              Em
  Faz o céu descer
F           G
  Espírito Vem
Am               Em
  Quebranta meu ser
F           G
  Espírito Vem
Am              Em
  Faz o céu descer
F           G
  Espírito Vem
Am               Em
  Quebranta meu ser
F           G
  Espírito Vem
Am              Em
  Faz o céu descer
F           G
  Espírito Vem
Am               Em
  Quebranta meu ser
F           G
  Espírito Vem

[Final]  Am  Em  F  G  Am  Em  F

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
