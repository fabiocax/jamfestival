Isaías Saad - Outro Nome Não Há

D/F#  G
Seu nome
D/F# G Am G D/F# Em
É sobre todo nome
D/F#  G
Sua glória
D/F# G Am G D/F# Em
Precede a criação
G/B  C
E o seu
G/B C D C G/B Em
Louvor ecoará
    G          C
Em nossos corações
G/B C D C G/B Em       C
 E aqui nesse lugar

D/F#  G
Sua face
D/F# G Am G D/F# Em
Brilha mais do que o sol

D/F#  G
Sua graça
D/F# G Am G D/F# Em
Transborda em amor
G/B C
E o Rei
G/B C D C G/B Em
Traz cura em suas mãos
    G              C
O Rei dos reis e Senhor
G/B C D C G/B Em      C
Outro igual não há

            G
Eu olho pro céu e vejo meu Rei vir
          Em
Luz do mundo que me estende a mão
                  G/B
Outro nome não há
                  C
Outro nome não há
           Em         C
Jesus meu Senhor, whoa oh

        G
Entronizamos o invencível Rei
             Em
Montes se prostram e nós te exaltamos
                  G/B
Outro nome não há
                  C
Outro nome não há
           Em            C
Jesus meu Senhor, whoa oh

D/F#  G
A fé
D/F# G Am G D/F# Em
Em meio à perdição
D/F# G
Sua cruz
D/F# G Am G D/F# Em
Demonstra salvação
  C
Jesus
            Em
A morte esmagou
    G           C
Em Cristo eu estou
                 Em  C
Outro igual não há

            G
Eu olho pro céu e vejo meu Rei vir
              Em
Luz do mundo que me estende a mão
                  G/B
Outro nome não há
                  C
Outro nome não há
           Em         C
Jesus meu Senhor, whoa oh
        G
Entronizamos o invencível Rei
                Em
Montes se prostram e nós te exaltamos
                  G/B
Outro nome não há
                  C
Outro nome não há
           Em            C
Jesus meu Senhor, whoa oh

    G
Diante do Rei, a Terra extremesse
Em
Correntes se quebram Céus e Terra cantam
C
Santo é o Senhor
C
Santo é o Seu nome
Em            C
Jesus, Jesus, Jesus

            G
Eu olho pro céu e vejo meu Rei vir
              Em
Luz do mundo que me estende a mão
                  G/B
Outro nome não há
                  C
Outro nome não há
           Em         C
Jesus meu Senhor, whoa oh
        G
Entronizamos o invencível Rei
                Em
Montes se prostram e nós te exaltamos
                  G/B
Outro nome não há
                  C
Outro nome não há
                  Em
Outro nome não há
                  C
Outro nome não há
     G                C
Jesus meu Senhor, whoa oh

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
