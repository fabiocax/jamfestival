Isaías Saad - Oceanos

C#m                 B/D#   E
Tua voz me chama sobre as águas
              B          A
Onde os meus pés podem falhar
C#m                   B/D#  E
E ali Te encontro no mistério
         B           E
Em meio ao mar, confiarei

A      E        B
Ao Teu nome clamarei
A            E         B
 E além das ondas olharei
               A
Se o mar crescer
            E          B
Somente em Ti descansarei
             A
Pois eu sou Teu e Tu és meu

( C#m  B/D#  E  B  A )


C#m                 B/D#     E
Tua graça cobre os meus temores
            B       A
Tua forte mão me guiará
C#m               B/D#   E
Se estou cercado pelo medo
       B                 A
Tu és fiel, nunca vais falhar

A      E        B
Ao Teu nome clamarei
A            E         B
 E além das ondas olharei
               A
Se o mar crescer
            E          B
Somente em Ti descansarei
             A
Pois eu sou Teu e Tu és meu

( C#m  B/D#  E  B  A )
( C#m  B/D#  E  B  A )
( C#m  A  E  B )
( C#m  A  E  B )

C#m                 A
Guia-me pra que em tudo em Ti confie
          E
Sobre as águas eu caminhe
    B
Por onde quer que chames
C#m                   A
Leva-me mais fundo do que já estive
         E
E minha fé será mais firme
B
Senhor, em Tua presença

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
B/D# = X 6 X 4 7 7
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
