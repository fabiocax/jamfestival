Isaías Saad - Pra Onde Eu Irei?

[Intro]  F#m7  D  A  E

F#m7      D9
Criador do Universo
A9            E
Dono do meu Ser
F#m7          D9
Rei do meu coração
A9          E
És minha canção

D9          A9       F#m7   E
Desde sempre Me amou
D9          A9       F#m7   E
Desde sempre Me chamou

    D9
Pra onde Eu Irei?
    F#m7
Pra onde fugirei?
         A9             E
Se já está em todo Lugar


        D9
Não fugirei
        F#m7
Entregarei
          E
Meu coração em Tuas mãos

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
D = X X 0 2 3 2
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
F#m7 = 2 X 2 2 2 X
