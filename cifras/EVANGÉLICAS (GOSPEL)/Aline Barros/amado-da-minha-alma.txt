Aline Barros - Amado da Minha Alma

intro: C / Em / C / Em / Dm / Fm / C

 C      G/B       F/A     Fm          C
quero tocar o coração do amado da minhalma
           G           F/A
ser muito mais que uma voz
Fm               Dm        G              Dm     F/G   G
cantando uma canção, realizar teus sonhos    é desejo meu
     C    G/B            Dm
meu Jesus,   quero te adorar
            F     G      C    G/B           Dm
com um coração sincero, Jesus    nome sem igual
         F    G    ( C / Em / C / Em)
tu és o meu amado                 volta ao inicio...

solo: Dm / G / Am / Dm / G

Am              F
eu quero te adorar...
C               G
eu quero te adorar...

Am              F       C              G
eu quero te adorar..., queremos te adorar

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
F/G = 3 X 3 2 1 X
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
