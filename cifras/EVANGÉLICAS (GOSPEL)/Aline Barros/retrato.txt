Aline Barros - Retrato

Intro:  A  F#m  D  E7  A  E7

A                       E7/G#
Uma folha de papel
       D                  A
Um lápis para rabiscar
D                          F#m
Eu vou fazer um desenho
Bm                       E7
Tão bonito vai ficar
A                       C#m    D                           A
O retrato de alguém     é o que eu vou fazer
D                          A
Vai se parecer comigo
E7                              A
E quem sabe com você

D                                   C#m
Primeiro eu desenho os olhos
E7                           A
Depois eu faço o nariz

      C#                    F#m
Na boca ponha um sorriso
       B7                     E7
Pro rosto ficar bem feliz

A               F#m
Alguém prá ser feliz assim
              A            Bm
Precisa ter uma razão
               Bm/A
Só pode ser alguém
         E7/G#              E7               A
Que tem o amor de Deus no coração.

( A  F#m  D  E7  A  E7 )

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
Bm/A = X 0 4 4 3 X
C# = X 4 6 6 6 4
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
E7/G# = 4 X 2 4 3 X
F#m = 2 4 4 2 2 2
