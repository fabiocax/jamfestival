Aline Barros - Manancial (Eu tenho sede de Ti)

[Intro] D9  A  Bm7/E  G  D/F#

      D         Bm7
Manancial  fonte de vida
               G             Bm7
De onde eu preciso e quero beber
          Em     A A4 A
Todos os dias
                 D A/C#         Bm7             G
Bem mais que um desejo Tu és essencial pra sustentar
         D/F#     A A4 A
A minha vida Senhor Jesus

          D       A
Eu tenho sede de Ti
             Bm7      G
Minh'alma anseia por Ti
            Bm            Em                A
Só me satisfaço quando estou junto aos teus pés
          D/F#      A/C#
Eu tenho sede de Ti

             Bm7      G
Minh'alma anseia por Ti
       Bm          Em        A    G (D/F# G A)
Manancial,única fonte de vida tu és

[Solo] Am  Em  G  D/F#
       Am  Em  D

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A4 = X 0 2 2 3 0
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
Bm7/E = X 7 7 7 7 7
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D/G = 3 X X 2 3 2
D9 = X X 0 2 3 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
