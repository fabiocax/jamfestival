Aline Barros - Sou Feliz

Intro:  A9  G9  D/F#  G  D
       G  D  A9  G9  Bm  A

A
Ele é o pão da vida
     C#m
Filho do Deus criador
   D
O seu reino é de justiça
      D/E
No seu nome, há poder
       A
Se fez homem,. veio ao mundo
    C#m
Pra curar a minha dor
    D
Alcançou a minha vida
    B/D#
Entrou no meu coração
        D   C#m  Bm
Me deu paz e alegria

      D/E
Estou seguro em suas mãos.
Bm    C#m   D          D/E
Canto as    sim.
A   C#m  Bm    A
Sou feliz com Jesus
F#m  C#m      D       D/E
O meu Rei é a minha luz,
A   C#m  Bm    A
Sou Feliz com Jesus
F#m   C#m      D     D/E
O meu Rei é a minha luz
      A               C#m
Jesus Cristo poderoso, soberano e fiel
   D
Ele é o melhor amigo
    D/E
Você pode acreditar
   A
Ele quebra as cadeias
      C#m
Quebra todas  maldição
     D
Nos faz livre do pecado
     B/D#
Traz vitória ao derrotado
        D      C#m  Bm
E eu sou prova
          D/E    Bm  C#m  D  D/E
Desta verdade.
(refrão)
Intro:  A  G  D/F#  G  D  G
A
Se fez homem, veio ao mundo
     C#m
Pra curar a minha dor
     D
Alcançou a minha vida
   B/D#
Entrou no meu coração
        D   C#m  Bm
Me deu paz e alegria
      D/E
Estou seguro em suas mãos
Bm    C#m   D          D/E
Canto as    sim
A   C#m  Bm    A
Sou feliz com Jesus
F#m   C#m    D       D/E
O meu Rei é a minha luz.
Bb  Dm     Eb   Bb
Sou Feliz com Jesus
Gm   Dm       Eb  Eo      Eb/F
O meu Rei é a minha,   minha, luz
Cm  Dm  Eb  Eb/F    Bb  Dm   Cm     Bb
                    Sou feliz com Jesus
Gm   Dm     Eb       Eb/F
O meu Rei é a minha luz.
Bb Dm     Eb     Bb
Sou Feliz com Jesus
Gm   Dm       Eb        Eb/F
O meu Rei é a Minha, luz.  (Sou feliz...)

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
B/D# = X 6 X 4 7 7
Bb = X 1 3 3 3 1
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D/E = X X 2 2 3 2
D/F# = 2 X 0 2 3 2
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
Eb/F = X 8 8 8 8 X
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G9 = 3 X 0 2 0 X
Gm = 3 5 5 3 3 3
