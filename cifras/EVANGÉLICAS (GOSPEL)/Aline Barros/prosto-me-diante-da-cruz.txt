Aline Barros - Prosto-me Diante da Cruz

BASE: D9 A/C# Bm7 D/F# G9 A9 G9

D/F# G9      A9  Bm7 D/F# G9   A9
Ó    DEUS ME SONDAS, CO NHECES ME
D/F#  G9     A9 Bm7 G9        A9
MESMO QUANDO FALHO, EU SEI ME AMAS
D/F# G9    A9 Bm7 D/F# G9     A9
  ESTÁS PRESENTE,  A   ME CERCAR
D/F#  G9   A9  Bm7
  EM  TODO TEMPO
G9        A9      G9        A9
EU SEI ME AMAS,   EU SEI ME AMAS

D9      A/C#         Bm7
PROSTRO-ME DIANTE DA CRUZ
       D/F#         G9
VEJO O SANGUE DE JESUS
                 A9
NUNCA HOUVE AMOR ASSIM
D9      A/C#         Bm7
SOBRE A MORTE JÁ VENCEU

    D/F#            G9
SUA GLÓRIA O CÉU ENCHEU
              A9
NADA IRÁ ME SEPARAR

INTERLÚDIO 1ª VEZ:    Em7 G9 A9       Em7 G9 A4 A9

D/F# G9     A9   Bm7  D/F#  G9    A9
  TU ÉS MEU GUI---A,  MEU   PROTETOR
D/F# G9      A9   Bm7   G9        A9    G9        A9
TUA   MÃO ME FIR---MA,  EU SEI ME AMAS, EU SEI ME AMAS

         G9       A9      Bm7        G9    Bm7   A9
O VÉU RASGOU, O CAMINHO ABRIU   TUDO CONSUMADO ESTÁ

D/F# G9     A9  Bm7   D/F#  G9  A9  D/F# G9   A9  Bm7
E    QUANDO TU---DO,   SE   ACABAR, ESTAREI SEGU---RO
G9          A9    G9          A9
POIS SEI ME AMAS, POIS SEI ME AMAS

MODULAÇÃO: B

E9         B/D#      C#m7         E/G#         A9
PROSTRO-ME DIANTE DA CRUZ  VEJO O SANGUE DE JESUS
                 B9            E9         B/D#               C#m7
NUNCA HOUVE AMOR ASSIM SOBRE A MORTE JÁ VENCEU
    E/G#            A9          C#m7      B9
SUA GLÓRIA O CÉU ENCHEU    NADA IRÁ ME SEPARAR
         A9       B9      C#m7      A9    C#m7 B9
O VÉU RASGOU, O CAMINHO ABRIU  TUDO CONSUMADO ESTÁ              FIM: E

----------------- Acordes -----------------
A/C# = X 4 X 2 5 5
A4 = X 0 2 2 3 0
A9 = X 0 2 2 0 0
B = X 2 4 4 4 2
B/D# = X 6 X 4 7 7
B9 = X 2 4 4 2 2
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
D/F# = 2 X 0 2 3 2
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E9 = 0 2 4 1 0 0
Em7 = 0 2 2 0 3 0
G9 = 3 X 0 2 0 X
