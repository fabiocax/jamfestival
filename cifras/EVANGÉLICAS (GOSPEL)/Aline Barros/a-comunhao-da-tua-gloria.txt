Aline Barros - A Comunhão da Tua Glória

INTRO   ::  A   :  E/G#   :   D    :   E   :
 A                          E/G#              D              E/G#               A
A minha vida está em tuas mãos,  Amado meu, Senhor
 A                          E/G#              D            E/G#         A
Todo o meu melhor eu quero      te entregar
 A                          E/G#           D           E/G#         A
Eu ouço a emoção da tua voz  a me falar, Senhor
 A                                     E/G#      D            E/G#
Nem sei como expressar a ti         o meu amor
 D                              E                 E4     E
Tu és a fonte de toda inspiração
 A                 E/G#           D      E
És cada nota da canção
 D                                E                 E4        E
És o Regente de glória e majestade
 D                                   E     A/E    E
A Ti pertence o meu louvor

 CORO
              A      A4                  A
Senhor,          o meu coração

                A4   A       E              E4   E2    E            Bm7
Deseja a comunhão                   da tua glória
          Bm       Bm7                        D          E                A
Senhor,         eu quero fluir no teu Espírito

A   A4    A    A4        A        A     E
Senhor,          o meu coração
                      E4     E7    E                         Bm7
Deseja   a   comunhão   da tua glória
                Bm   Bm7                       D         E
Senhor,         eu quero fluir no teu Espírito
   INTRO   ::  A   :  E/G#   :   D   :   E   :   ( 2 x )

     A    Bm7   A            D       E    A  E/G#   D
Senhor,          o meu      cora -  ção
                       E         D      A         A    Bm7
Deseja a comunhão   da tua glória
          Bm        Bm7                        D          E                A
Senhor,         eu quero fluir no teu Espírito

        A   A4    A    A4        A        A     E
Senhor,          o meu coração
                      E4     E7    E                      A     D         A     D             E   A
Deseja   a   comunhão   da tua glória

       A     D                 E           A
Glória          Oh!  Senhor.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/E = 0 X 2 2 2 0
A4 = X 0 2 2 3 0
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E2 = 0 2 4 1 0 0
E4 = 0 2 2 2 0 0
E7 = 0 2 2 1 3 0
