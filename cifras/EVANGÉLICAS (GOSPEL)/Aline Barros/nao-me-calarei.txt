Aline Barros - Não Me Calarei

Intro:
E|-------------------------------------------------------------|
B|---6------6-------10------10-------5-----5-------11-----11---|
G|-------------------------------------------------------------|
D|-------------------------------------------------------------|
A|---5------5-------8--------8-------3-----3-------10-----10---|
E|-------------------------------------------------------------|

Dm                     F
 Eu sei porque eu vivo
                        C
 Eu sei pra quem eu canto
                            Gm
 Ninguém jamais vai me enganar
Dm                         F
 Eu sei quem sou em Cristo
                      C
 Ouvi a voz do mestre
                       Gm
 Eu vou anunciar a salvação



 G  A#  C
           Eu não vou parar meus pés
 G  A#  C
           Eu não vou calar minha voz
 G  A#  C
           Continuo a cantar
G       A#   C
  Não me calarei

Refrão:
Dm
Subirei o monte
F
Descerei ao vale
  C                Gm
E o meu Deus me guiará
Dm                  F
Vou saltar muralhas e vencer gigantes
 C               Gm
Eu não quero mais parar
Dm               F
Mesmo no deserto eu não tenho medo
 C                    Gm
Pois eu sei a quem clamar

  A#   F   C
  Hei! Hei! Eu não vou parar meus pés
  A#   F   C
  Hei! Hei! Eu não vou calar minha voz
Solo Teclado:
            (2x)
E|--------------|--------------|
B|--------------|--------------|
G|--------------|--------------|
D|--3-3-3-3-3-3-|--3---3-3-3-6-|
A|--3-5-3-5-3-5-|--3h5-6-6-6-6-|
E|--3-3-3-3-3-3-|--3---3-3-3-3-|

Não vou parar meus pés
Não vou calar minha voz
Eu vou ouvir a voz do meu Senhor

( Dm  F   C  Gm )

Refrão:
Dm
Subirei o monte
F
Descerei ao vale
   C               Gm
E o meu Deus me guiará
Dm                  F
Vou saltar muralhas e vencer gigantes
 C                  Gm
Eu não quero mais parar
Dm               F
Mesmo no deserto eu não tenho medo
 C                    Gm
Pois eu sei a quem clamar

----------------- Acordes -----------------
A# = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
