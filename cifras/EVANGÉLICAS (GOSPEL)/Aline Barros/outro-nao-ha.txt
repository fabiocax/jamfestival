Aline Barros - Outro Não Há

Introdução  D9,
            D9  Am  C  Em  D9 D9  Am  C  Em  D9

D9            Bm7            C9                Em
  Rei humilde,   Sua glória Ele deixou Nos visitou aqui
D9             Bm7              C9
  Deus bondoso,   Sua graça nos alcançou
Em            G             D9
Outro amor igual ao Seu não há

           Am           C9
Eu sempre irei cantar as obras do Seu grande amor
   Em               G        D9
Eu sei que em breve Ele voltará
           Am           C9
Bem alto proclamar: Não há outro mediador
       Em           G
Só o amor de Cristo pode salvar

D9                  A/C#
Outro não há, nem jamais haverá

     C9
Um Deus que cuida dos Seus
  G
Seu eterno amor não falha
D9                  A/C#
Outro não há, nem jamais haverá
      C9
Um Deus que cuida dos Seus
       G
E por toda eternidade vou cantar

D9     Am               C9  Em  D9
  Lá lá lá lá lá lá lá lá

D9           Am           C9
  Eu sempre irei cantar as obras do Seu grande amor
   Em               G        D9
Eu sei que em breve Ele voltará
           Am           C9
Bem alto proclamar: Não há outro mediador
       Em           G
Só o amor de Cristo pode salvar

D9                  A/C#
Outro não há, nem jamais haverá
     C9
Um Deus que cuida dos Seus
  G
Seu eterno amor não falha
D9                  A/C#
Outro não há, nem jamais haverá
      C9
Um Deus que cuida dos Seus
       G
E por toda eternidade vou cantar

Solo:
D9  A/C#  C9  G
D9  Am  C9  Em  D9
D9  Am  C9  Em  C9  Em  C9  G  D9

----------------- Acordes -----------------
A/C# = X 4 X 2 5 5
Am = X 0 2 2 1 0
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
D9 = X X 0 2 3 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
