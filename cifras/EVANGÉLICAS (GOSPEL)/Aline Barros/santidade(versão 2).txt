Aline Barros - Santidade

Intro: solo: {13-13-11-11-10-10-06} (2x)
             {13-13-11-11-10-10-06-29-27}


Bb4           Bb  Bb9          Bb
O meu melhor quero Te dar,
Gm                               D#
Me derramar aos Teus     pés.
Bb4        Bb        Bb9         Bb
Ser Tua imagem  e  semelhança
Gm                  D#
É o desejo do meu coração.
Cm                       Dm
Se o meu corpo errar o caminho
 D#                        Dm
Meu coração clamará por Ti.
   D#                               F  Bb
Abraça-me com Tua misericórdia

 Cm          Dm        D#               D
Vem envolver, Tua face quero ver.


 Gm           F                  D#        Dm
Quero vestir as roupas da santidade
Cm        Dm       D#              D
E mergulhar no Santo dos Santos.
Gm       F          D# Dm
Tenho sede de Deus.
 Cm              Dm   D#           F               Bb
Quero conhecer, quero exalar mais de Deus.


Final: || Bb | Gm | F9 | F | D# | Bb ||

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bb4 = X 1 3 3 4 1
Bb9 = X 1 3 3 1 1
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D# = X 6 5 3 4 3
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
F9 = 1 3 5 2 1 1
Gm = 3 5 5 3 3 3
