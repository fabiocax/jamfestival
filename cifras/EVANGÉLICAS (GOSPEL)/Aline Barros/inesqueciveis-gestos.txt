Aline Barros - Inesquecíveis Gestos

[Intro] C  D/F#  Em  Am  Cm  G

       C                           D          G
Quando olho em teus olhos, eu não temo o que vier
       G                       Bm         Em
Quando era impossível, nosso início foi a fé
      C                          Bm         G
Como corda de três dobras, somos eu, você e Deus
    Am         G/B       C        D
O amor e a luz do céu em nós acendeu

         C                    D       G
Quanto choro já segurou, para me encorajar
        G                      B7       Em
Quantos erros assumiu, pra verdades revelar
          Am                     G/B
Mesmo eu sendo sua metade, você só me aceitou
       C                              G             D
Por saber que Deus é o todo, que completa o nosso amor

          G4 G       D     Em
É inesquecível cada gesto seu

         C           G
Não tem como uma história linda
   A7             D
Assim não ser de Deus
                  G4 G   D     Em
Mais do que eu buscava Ele me deu
        Am            G/B
Nosso amor não tem limites
     A7           D         G
Escalamos o impossível com Deus

        C   Em       Am            G         A   D
Nossos filhos são herança, frutos da perseverança
         C  Em          A
Os teus gestos são lembranças
              Am    G    C
De quando eu descobri te amar

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G4 = 3 5 5 5 3 3
