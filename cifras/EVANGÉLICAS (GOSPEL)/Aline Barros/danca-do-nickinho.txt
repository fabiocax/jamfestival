Aline Barros - Dança do Nickinho

  Fm
   E balançando a cabeça, ele chegou
D# Fm
   Nickinho uma criança do senhor
D# Fm
   E dando glória, dando glória sem cessar
G# Fm
   Dançando, era nickinho a louvar

   Fm
Mexendo a cabeça e os braços sem parar
       G#
Dando volta, dando volta
      D#
Não parava de pular
  Fm
E mexe pra lá e mexe pra cá
  Gm                   D#
E todas as pessoas começaram a pular

Fm                        Cm
Nickinho, não pare, por favor

        C#      Bbm       C
Essa alegria já me contagiou
       Fm
Como nickinho
                   Cm
Eu também quero louvar
         C#
Chegou a hora
       Bbm      C
Vamos todos celebrar

Dm             Bb
Vem, vem ta querendo
          F        C
Vim louvar como nickinho
Vem dançar
Dm       Bb
A Deus você vai agradar
         F            C
Se com o coração o adorar

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bbm = X 1 3 3 2 1
C = X 3 2 0 1 0
C# = X 4 6 6 6 4
Cm = X 3 5 5 4 3
D# = X 6 5 3 4 3
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G# = 4 3 1 1 1 4
Gm = 3 5 5 3 3 3
