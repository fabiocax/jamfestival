Aline Barros - Pisa Na Muralha

C                                F
Deus disse a Josué se vence pela fé,
                  G              C
somente quem tem fé,somente pela fé.
C                                F
Eu ponho essa muralha lá de Jericó,
               G                 C
debaixo do teu pé,debaixo do teu pé.

C             G
Pisa na muralha,
              C
pula na muralha,
                 G
por cima da muralha,
                          C
o Deus que mandou não falha  (2x)

C                                   F
Vem marchar comigo em volta  de Jericó
              G              C
muralha vira pó, a ela vira pó.

C                                 F
Vem tocar trombeta  mi-fá-sol-lá-si

e ela vai cair, e ela vai cair.

G
PISA
C
PISA
G
PISA
C
PISA,PISA
G
PISA
C
PISA,PISA,PISA
G
PISA
C
PISA

----------------- Acordes -----------------
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
