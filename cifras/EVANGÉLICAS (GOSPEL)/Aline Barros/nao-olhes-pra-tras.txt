Aline Barros - Não Olhes Pra Trás

INTRO: | Bb | Eb | Bb | Eb | 2x

        Bb                      Eb
Quantas vezes, não tem mais esperança
             Bb                  Eb
E sabe que sozinho não dá pra prosseguir
         Bb                       Eb
Não se entregue, nem tudo está perdido
            Bb                  Eb
Existe uma saída, você vai conseguir

      Bb                Eb
Jesus Cristo pode te ajudar
              Bb                     Eb
Mudando a sua vida, te ensina a caminhar
      Bb                  Eb
Jesus Cristo sabe teus problemas
            Bb                 Eb
E tem a solução que pode te salvar

  Bb                  Eb
Caminha ho! Ho! Ho! Caminha

              Bb                     Eb
Sem olhar pra trás, e em Deus achará paz

       B                  E
Muitas vezes em meio a tormenta
             B                   E
A tentação é grande de se voltar atrás
        B                      E
Quantas vezes quer se perder o rumo
               B                  E
Se anda sem destino, pra nenhum lugar

        B               E
Jesus Cristo pode te ajudar
              B                     E
Mudando a sua vida, te ensina a caminhar
        B                 E
Jesus Cristo sabe teus problemas
            B                  E
E tem a solução que pode te salvar

  B                   E
Caminha ho! Ho! Ho! Caminha
              B                      E
Sem olhar pra trás, e em Deus achará paz

  C                   F
Caminha ho! Ho! Ho! Caminha
              C                      F
Sem olhar pra trás, e em Deus achará paz

----------------- Acordes -----------------
B = X 2 4 4 4 2
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
E = 0 2 2 1 0 0
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
