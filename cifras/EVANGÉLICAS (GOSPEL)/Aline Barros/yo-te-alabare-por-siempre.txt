Aline Barros - Yo Te Alabaré Por Siempre

Intro: D C#m/D Bm A

D           C#m/D
  Eres Dios,     Santo Dios
Bm                G
Yo te alabaré por siempre

G                     A
Tu me conociste antes de nacer
D          C#m/D         Bm
Y conoce mi      momento de morir
     G                 A
Eres Alfa y Omega, Principio y final
D              F#m   G
Por siempre te canta-ré

G                  A
Te encomiendo me espíritu
D    C#m/D               Bm
Solo tu    me puedes satisfacer
     G                 A
Eres Alfa y Omega, Principio y final

    D          F#m   G
Por siempre te canta-ré

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
