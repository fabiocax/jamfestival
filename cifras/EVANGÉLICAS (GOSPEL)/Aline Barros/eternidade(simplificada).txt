﻿Aline Barros - Eternidade

Capo Casa 5

[Intro] F9  Am  G  F9  Am  G

Am                  C   Am                C
Até onde vai o Teu amor? Será que tem um fim?
Am                  C  Am              C
E onde foi que começou   o Teu amor por mim?
F9                     C
Antes do início, Tu já eras o amor
       Am                            G
E esse amor que não tem fim naquela cruz me eternizou
   F9                    C
O céu tem uma altura e o fundo do mar tem fim
        Am                   G           F9  Am    G
Mas não haverá limites esse teu amor por mim (ooh, ooh)

         C             F9          Am            G
De eternidade em eternidade Deus me ama, Deus me ama
         C/E           F9          Am            G
De eternidade em eternidade Deus me ama, Deus me ama

( F9  Am  G  F  Am  G )


F9                     C
Antes do início, Tu já eras o amor
       Am                            G
E esse amor que não tem fim naquela cruz me eternizou
   F9                    C
O céu tem uma altura e o fundo do mar tem fim
        Am                   G           F9  Am    G
Mas não haverá limites esse Teu amor por mim (ooh, ooh)

         C             F9          Am            G
De eternidade em eternidade Deus me ama, Deus me ama
         C/E           F9          Am            G
De eternidade em eternidade Deus me ama, Deus me ama
(Ele me ama)

  C/E                         F9
E não me amas mais, por tudo que já acertei
  Am                        G
E nem me amas menos, pelas vezes que errei
 C/E                            F9
Antes do início e até depois do fim, vais me amar
Am                           G                    F9   Am    G
Estou bem certo de que nada irá me separar do Teu amor (ooh, ooh)
       F9    Am   G
Do Teu amor (ooh, ooh), Senhor
       F9          Am            G
Do Teu amor, do Teu amor, do Teu amor
       F9    Am   G
Do Teu amor (ooh, ooh)

          C            F9          Am            G
De eternidade em eternidade Deus me ama, Deus me ama
         C/E           F9          Am            G
De eternidade em eternidade Deus me ama, Deus me ama
          F9  Am   G        F9   Am    G
(Deus me ama) ooh, ooh

----------------- Acordes -----------------
Capotraste na 5ª casa
Am*  = X 0 2 2 1 0 - (*Dm na forma de Am)
C*  = X 3 2 0 1 0 - (*F na forma de C)
C/E*  = 0 3 2 0 1 0 - (*F/A na forma de C/E)
F*  = 1 3 3 2 1 1 - (*A# na forma de F)
F9*  = 1 3 5 2 1 1 - (*A#9 na forma de F9)
G*  = 3 2 0 0 0 3 - (*C na forma de G)
