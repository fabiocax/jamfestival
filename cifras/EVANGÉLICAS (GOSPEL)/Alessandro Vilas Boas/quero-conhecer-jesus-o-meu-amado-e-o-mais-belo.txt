Alessandro Vilas Boas - Quero Conhecer Jesus (O Meu Amado É o Mais Belo)

[Intro] F9  G4(9)  Am7  Em7

         F9                   G4(9)
O meu orgulho me tirou do jardim
          Am7                      Em7
Tua humildade colocou o jardim em mim
            F9
E se eu vendesse tudo o que tenho
     G4(9)                Am7  Em7
Em troca do amor eu falharia
         F9                 G4(9)
Pois o amor não se compra, nem se merece
    Am7               Em7
O amor se ganha, de graça o recebe

     F9              G4(9)
Eu quero conhecer Jesus
     Am7             Em7
Eu quero conhecer Jesus (e ser achado Nele)
     F9              G4(9)
Eu quero conhecer Jesus

     Am7             Em7
Eu quero conhecer Jesus

( F9  G4(9)  Am7  Em7 )
( F9  G4(9)  Am7  Em7 )

         F9                   G4(9)
O meu orgulho me tirou do jardim
          Am7                      Em7
Tua humildade colocou o jardim em mim
            F9
E se eu vender tudo o que tenho
     G4(9)                Am7  Em7
Em troca do amor eu falharia
         F9                 G4(9)
Pois o amor não se compra, nem se merece
    Am7               Em7
O amor se ganha, de graça o recebe

     F9              G4(9)
Eu quero conhecer Jesus
     Am7             Em7
Eu quero conhecer Jesus (e ser achado Nele)
     F9              G4(9)
Eu quero conhecer Jesus
     Am7             G/B
Eu quero conhecer Jesus

      F9            Am7
Meu Amado é o mais belo
       G/B  C          G/B
Entre mi___lhares e milhares
      F9            Am7
Meu Amado é o mais belo
       G/B  C          G/B
Entre mi___lhares e milhares
      F9            Am7
Meu Amado é o mais belo
       G/B  C          G/B
Entre mi___lhares e milhares

[Solo] F9  Am7  G/B  C  G/B
       F9  Am7  G/B  C  G/B

( F9  G4(9)  Am7  Em7 )

F9             G4(9)  Am7      Em7
Yeshu-u-u-u-a, a-a-a------a, a-a-a-a
    F9             G4(9)  Am7      Em7
Yeshu-u-u-u-a, a-a-a------a, a-a-a-a
    F9             G4(9)  Am7      Em7
Yeshu-u-u-u-a, a-a-a------a, a-a-a-a

Am            A9        Em    F  G    Am
Vem saltando sobre os montes de Jerusalém
Am            A9   Em
Sou tua noiva apaixonada
   F         G       Am
Te esperando para dançar

Am A9 Em
Yeshua
F  G     Am
Yeshua

----------------- Acordes -----------------
A9 = X 0 2 2 0 0
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
F9 = 1 3 5 2 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G4(9) = 3 X 0 2 1 3
