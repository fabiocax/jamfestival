﻿Alessandro Vilas Boas - Real e Constante

Capo Casa 2

[Intro]  Em  G  Am  C

Em                        G
O que direi sobre os seus olhos?
          Am                        C
Eles são sempre tão profundos e perfeitos
Em                       G
O que direi sobre o seu jeito?
        Am                        C
Você é sempre tão real e tão constante

 Em    G  Am           C
Jesus,   justo e verdadeiro
 Em    G  Am           C
Jesus,    ultimo e primeiro

( Em )

Em                     G
O que direi sobre sua boca?
      Am                           C
Suas palavras são verdades tão singelas

Em                       G
O que direi sobre o seu jeito?
        Am                        C
Você é sempre tão real e tão constante

       Em    G  Am           C
Há um Jesus,   justo e verdadeiro
 Em   G    Am        D
Jesus,  ultimo e primeiro

( Em  G  D  C )
( Em  G  D  C )
( Em  G  D  C )
( Em  G  D  C )

 Em               G                    D
Jesus tu és o primeiro, onde tudo foi feito
   C
É tudo sobre Ti!
 Em             G               D          C
Tu és o meu consolo, caminho seguro, meu grande salvador

( Em  G  D  C )

 Em               G                    D
Jesus tu és o primeiro, onde tudo foi feito
   C
É tudo sobre Ti
 Em             G               D          C
Tu és o meu consolo, caminho seguro, meu grande salvador

( Em  G  D  C )
( Em  G  D  C )
( Em  G  D  C )
( Em  G  D  C )

----------------- Acordes -----------------
Capotraste na 2ª casa
Am*  = X 0 2 2 1 0 - (*Bm na forma de Am)
C*  = X 3 2 0 1 0 - (*D na forma de C)
D*  = X X 0 2 3 2 - (*E na forma de D)
Em*  = 0 2 2 0 0 0 - (*F#m na forma de Em)
G*  = 3 2 0 0 0 3 - (*A na forma de G)
