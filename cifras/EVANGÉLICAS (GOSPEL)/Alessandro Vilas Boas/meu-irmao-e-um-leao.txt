Alessandro Vilas Boas - Meu Irmão É Um Leão

[Intro]  Am  C  D/F#
         Am  C  D/F#

       Am            C
Meu irmão é um leão
                            D/F#
E em sua boca uma espada afiada
   Am            C
Jesus é um leão
                            D/F#
E em sua boca uma espada afiada

       Am    G/B  C    Em  D/F#  G
E Ele nunca me   deixará
 Am    G/B  C       Em     D/F#   G
Nunca me   deixa fugir do seu   amor
 Am    G/B  C     Em  D/F#  G
Nunca me   deixará
 Am    G/B  C       Em     D/F#   G
Nunca me   deixa fugir do seu   amor


         Am
Eu vejo fogo em seus olhos
  F
Amor em seu coração
 C                         G
Paixão em ti, paixão em ti
         Am
Eu vejo fogo em seus olhos
  F
Amor em seu coração
 C                         G
Paixão em ti, paixão em ti

       Am    G/B  C    Em  D/F#  G
E Ele nunca me   deixará
 Am    G/B  C       Em     D/F#   G
Nunca me   deixa fugir do seu   amor
 Am    G/B  C     Em  D/F#  G
Nunca me   deixará
 Am    G/B  C       Em     D/F#   G
Nunca me   deixa fugir do seu   amor

         Am
Eu vejo fogo em seus olhos
  F
Amor em seu coração
 C                         G
Paixão em ti, paixão em ti

               Am   F
Deus tu és a chama
               C    G
Deus tu és a chama

       Am
I see fire in your eyes
 F
Love in your heart
 C                             G
Passion in you, passion in you

                  Am  F
God, you're the flame
                  C   G
God, you're the flame

        Am
Maravilhoso, fiel
    F
Perfume derramado
 C                G
Santo, noivo desejado
        Am
Maravilhoso, fiel
    F
Perfume derramado
 C                G
Santo, noivo desejado

[Final]  Am

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
