Alessandro Vilas Boas - Quero Conhecer Jesus

[Intro]  C  D  Em  D
         C  D  Em  D

E|--------------------------------------|
B|--------------------------------------|
G|---0-------0-------0-------0----------|
D|-----2-------4-------5-------4--------|
A|-3-----3-5-----5-7-----7-5-----5------|
E|--------------------------------------|

         C                    D
O meu orgulho me tirou do jardim
          Em                       D
Tua humildade colocou o jardim em mim
            C
E se eu vendesse tudo o que tenho
     D                    Em  D
Em troca do amor eu falharia
         C                  D
Pois o amor não se compra, nem se merece
    Em                D
O amor se ganha, de graça o recebe


     C               D
Eu quero conhecer Jesus
     Em              D
Eu quero conhecer Jesus e ser achado Nele

( C  D )

         Em       D
E ser achado nele

         C                    D
O meu orgulho me tirou do jardim
          Em                       D
Tua humildade colocou o jardim em mim
            C
E se eu vendesse tudo o que tenho
     D                    Em  D
Em troca do amor eu falharia
         C                  D
Pois o amor não se compra, nem se merece
    Em                D
O amor se ganha, de graça o recebe

     C               D
Eu quero conhecer Jesus
     Em              D
Eu quero conhecer Jesus
     C               D
Eu quero conhecer Jesus
     Em              D
Eu quero conhecer Jesus e ser achado Nele

E|--------------------------------------|
B|-3-5---3-5---3-5---3-5-7-8-7-8-12-7---|
G|--------------------------------------|
D|--------------------------------------|
A|-2-3---2-3---2-3---2-3-5-7-5-7-10-5---|
E|--------------------------------------|

    C   D Em  D
Yeshua---a----a!
    C   D Em  D
Yeshua---a----a!

        C             Em
O Meu Amado é o mais belo
          G          D/F#
Entre milhares e milhares
        C             Em
O Meu Amado é o mais belo
          G          D/F#
Entre milhares e milhares
        C             Em
O Meu Amado é o mais belo
          G          D/F#
Entre milhares e milhares
        C             Em
O Meu Amado é o mais belo
          G          D/F#
Entre milhares e milhares

E|--------------------------------------|
B|-3-5---3-5---3-5---3-5-7-8-7-8-12-7---|
G|--------------------------------------|
D|--------------------------------------|
A|-2-3---2-3---2-3---2-3-5-7-5-7-10-5---|
E|--------------------------------------|

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
