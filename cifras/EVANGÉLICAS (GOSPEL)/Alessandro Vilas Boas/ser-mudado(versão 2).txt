Alessandro Vilas Boas - Ser Mudado

Am       C
Vem Jesus
              G
Será que é pedir demais
Am       C
Nova fome
             G
Uma nova paixão por ti

                   Am        C
Não é que eu não seja grato
                    G
Por tudo que tens feito em mim
              Am            C
Mas meu coração tem chorado
     G
Por ti
           Am           C
Meu coração tem clamado
     G
Por ti


Am          C
Ser tocado por tu glória
G
É o meu desejo

É o que eu mais quero Deus
Am           C
Ser mudado por tua face
G
É o meu anseio
                  Am   C   G
É o meu maior prazer

                Am         C
Não é que eu não seja grato
                    G
Por tudo que tens feito em mim
             Am            C
Mas meu coração tem chorado
     G
Por ti
          Am           C
Meu coração tem clamado
     G
Por ti

               Am
Eu não trouxe nada pra mesa
               C
Eu não trouxe nada pra mesa
             G
Apenas uma fome e uma sede

Uma fome e uma sede

               Am
Eu não trouxe nada pra mesa
               C
Eu não trouxe nada pra mesa
            G
Apenas uma fome e uma sede

Uma fome e uma sede

               Am
Eu não trouxe nada pra mesa
               C
Eu não trouxe nada pra mesa
            G
Apenas uma fome e uma sede

Uma fome e uma sede

               Am
Eu não trouxe nada pra mesa
               C
Eu não trouxe nada pra mesa
            G
Apenas uma fome e uma sede

                                  Am
Eu nunca olhei pra alguém como eu olho pra ti
           C           G
Como eu quero a ti Jesus

                                  Am
Eu nunca olhei pra alguém como eu olho pra ti
           C               G
Como eu quero meu amado Jesus

       Am
Casa comigo
       C         G
Casa comigo Jesus

       Am
Casa comigo
       C
Casa comigo
       G
Casa comigo

Am          C
Ser tocado por tua glória
G
É o meu desejo

É o que eu mais quero
Am          C
Ser mudado por tua face
G
É o meu anseio

É o meu maior prazer

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
G = 3 2 0 0 0 3
