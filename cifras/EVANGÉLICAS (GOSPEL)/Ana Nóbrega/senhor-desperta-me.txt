Ana Nóbrega - Senhor, Desperta-me

[Intro] Em7/C  D9
        Em7/C  D9
        Em7/C  D9
        Em7/C  D9

G           C9            Em7/C
 Preciso contemplar tua glória outra vez
        C9               G
Envolve-me, oh pai, como o senhor já fez
     Em7/C           C9          D
No começo,     no princípio  no primeiro amor
G          C9         Em7/C
  Desejo caminhar em tua direção
       C9           G
Anseio conhecer mais do teu coração
      Em7/B          C9       D
Tenho sede,     tenho fome  de ti, senhor

           C           D
Vem queimar teu fogo em mim, sustenta-me
        C        D
Nessa doce comunhão, envolve-me

            C       D             C  D
Se eu de ti me desviar, atrai-me mas não me deixes ir

            C  D          C    D
Senhor, desperta-me      desperta-me oh Deus
           C             D
Não me deixes dormir  acorda-me do sono oh Deus
   C    D
Desperta-me

( Em  A )
( Em  A )
( Em  A )
( Em  A )

D           G             Bm
 Preciso contemplar tua glória outra vez
        G              D
Envolve-me, oh pai, como o senhor já fez
     Bm7/F#             G           A
No começo,       no princípio   no primeiro amor
D          G        Bm
  Desejo caminhar em tua direção
       G            D
Anseio conhecer mais do teu coração
      Bm7/F#         G        A
Tenho fome,     tenho sede  de ti, Senhor

           G           A
Vem queimar teu fogo em mim, sustenta-me
        G        A
Nessa doce comunhão, envolve-me
            G       A             G  A
Se eu de ti me desviar, atrai-me mas não me deixes ir

           G           A
Vem queimar teu fogo em mim, sustenta-me
        G        A
Nessa doce comunhão, envolve-me
            G       A             G  A
Se eu de ti me desviar, atrai-me mas não me deixes ir
                     G           A
Se eu de ti me desviar, atrai-me mas não me deixes ir

( Em )

G         A   Em
  Senhor, desperta-me (5X)

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
Bm7/F# = X X 4 4 3 5
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
Em/C = 8 7 5 X 8 X
Em7/B = X 2 2 0 3 0
G = 3 2 0 0 0 3
