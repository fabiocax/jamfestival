Kemuel - Faça Morada

[Intro] F9  G  Am7
        F9  G

[Primeira Parte]

          C9
Eis-me aqui
                            Am7(11)
Tantas fraquezas diante da Tua grandeza
          G9
O que se vê o coração sente
            F9
O corpo responde, minh'alma não mente
        C9
Sonda agora este coração
          G9
Veja, Senhor, se não tenho razão
             Am7(11)
Mas eu não quero ser tão racional
          F9
Espírito Santo pode se achegar


[Refrão]

           C9              G/B
E fazer morada, e fazer morada
          Am7            F9
Espírito Santo, mora em mim
           C9                        G/B
O meu coração é o Teu lugar, faça morada
          Am7          G9         F9
Eu Te convido a nunca mais me deixar aqui

[Segunda Parte]

                C9
Faça morada em mim
                G/B
Faça morada em mim
                Am7  G9    F9
Faça morada em mim.....em mim

[Primeira Parte]

          C9
Eis-me aqui
                            Am7(11)
Tantas fraquezas diante da Tua grandeza
          G9
O que se vê o coração sente
            F9
O corpo responde, minh'alma não mente
        C9
Sonda agora este coração
          G9
Veja, Senhor, se não tenho razão
             Am7(11)
Mas eu não quero ser tão racional
          F9
Espírito Santo pode se achegar

[Refrão]

           C9              G/B
E fazer morada, e fazer morada
          Am7            F9
Espírito Santo, mora em mim
           C9                        G/B
O meu coração é o Teu lugar, faça morada
          Am7          G9               F9
Eu Te convido a nunca mais me deixar aqui

[Segunda Parte]

                C9
Faça morada em mim
                G/B
Faça morada em mim
                Am7  G9    F9
Faça morada em mim.....em mim

                C9
Faça morada em mim
                G/B
Faça morada em mim
                Am7  G9    F9
Faça morada em mim.....em mim

[Refrão]

           C9                        G/B
O meu coração é o Teu lugar, faça morada
          Am7          G9               F9
Eu Te convido a nunca mais me deixar aqui

( C9  G/B  Am7  G9  F9 )

                C9
Faça morada em mim
                G/B
Faça morada em mim
                Am7  G9    F9
Faça morada em mim.....em mim

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Am7(11) = 5 X 5 5 3 X
C9 = X 3 5 5 3 3
F9 = 1 3 5 2 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G9 = 3 X 0 2 0 X
