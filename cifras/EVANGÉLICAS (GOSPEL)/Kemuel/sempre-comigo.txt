Kemuel - Sempre Comigo

[Intro] F  G

E|-------------------------------|
B|-------------------------------|
G|----0--------------------------|
D|-------------------------------|
A|---------3------------2----3---|
E|-1----1------------3----3------|

F9                        G                   F9                          G
  Toda melodia que eu entoar é para o adorar,   cada lágrima que eu derramar é para o adorar
F9                        G                   F9                          G
  Toda melodia que eu entoar é para o adorar,   cada lágrima que eu derramar é para o adorar
F9                              G         F9                       G
  Na alegria ou na dor,eu cantarei, e em meio às lutas não me calarei

F9                                  G
  As batidas do meu coração, tú conheces o compasso
      F9                                       G
Olha dentro de mim, não preciso fingir, me conheces
       F9                G           F9                G
Eu preciso de ti, me conheces eu preciso de ti, me conheces


          F               C                 G
O meu deitar, o meu respirar, toda a minha vida é pra te adorar
          F               C                 G                Am               F
O meu deitar, o meu respirar, toda a minha vida é pra te adorar,   pra te adorar

              C9               G               Am              F
Estarás, andarás sempre comigo,  estarás, andarás sempre comigo
              C9               G               Am
Estarás, andarás sempre comigo,  estarás, andarás sempre comigo

F9                                  G
  As batidas do meu coração, tú conheces o compasso
      F9                                       G
Olha dentro de mim, não preciso fingir, me conheces
       F9                G           F9        G
Eu preciso de ti, me conheces eu preciso de ti

                Dm             Am                    G
De ti eu quero mais, e mais,e mais, de ti eu quero mais, e mais, e mais
                F             Am               G4    G
De ti eu quero mais, e mais,e mais, de ti eu quero mais
                Dm  Am   G4  G                 Dm  Am   G4  G
De ti eu quero mais,    mais,  de ti eu quero mais,    mais
               F/A Am  G4  G                 Dm  Am G       Am F  C
De ti eu quero mais,  mais,  de ti eu quero mais,    de ti

G                           F   G4 G
 Como eu preciso de ti, senhor

          F               C                 G                Am
O meu sentar, o meu respirar, toda a minha vida é pra te adorar
          F               C                 G                Am   F
O meu deitar, o meu respirar, toda a minha vida é pra te adorar

              C9               G               Am              F   C
Estarás, andarás sempre comigo,  estarás, andarás sempre comigo
             G                  F                        G         Am7/F  G  C
Eu nunca estou só, a tua forte mão me sustentou até aqui, eu sei

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
F9 = 1 3 5 2 1 1
G = 3 2 0 0 0 3
G4 = 3 5 5 5 3 3
