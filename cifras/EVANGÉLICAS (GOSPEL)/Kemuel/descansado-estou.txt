Kemuel - Descansado Estou

[Intro] Bm9  A9  Bm9  A9  Bm9  A9  G

 E       Bm9
As preocupações
   A9
Querem me afligir
 E      Bm9
As decepções
         A9
Minha esperança querem roubar
 E      Bm9
E acusações
    A9
Vem para me entristecer
 E      Bm9
Confiar em Ti
      A9
É o que posso fazer

 E       Bm9
Nada mais me importa

    A9         E
Quando estou em tua presença Deus
 E       Bm9
Nada mais me importa
               A9
O que eu mais quero é te adorar

( Bm9  A9  Bm9  A9  Bm9  A9  G  )

 E       Bm9
As preocupações
   A9
Querem me afligir
 E      Bm9
As decepções
         A9
Minha esperança querem roubar
 E      Bm9
E acusações
    A9
Vem para me entristecer
 E      Bm9
Confiar em Ti
      A9
É o que posso fazer

 E       Bm9
Nada mais me importa
    A9         E
Quando estou em tua presença Deus
 E       Bm9
Nada mais me importa
               A9         B4 B
O que eu mais quero é te adorar
  Bm9     A9       E
Descasado estou em ti Senhor
 E             Bm9
Mais no esconderijo do Altíssimo
      A9
Eu quero viver
    E       Bm9
Onde a sombra do Onipotente
    A9
Me faz descansar
 E             Bm9
Mais no esconderijo do Altíssimo
      A9
Eu quero viver

( A  A4aum  A5  A9/E  E )
( C#  D#  E  A  G# )

    E       Bm9
Onde a sombra do Onipotente
    A9
Me faz descansar
 E             Bm9
Mais no esconderijo do Altíssimo
     A9     (A9 G F#m G E)
Eu quero viver
    E       Bm9
Onde a sombra do Onipotente
   F#m   E/G# A9 B9
Me faz descansar
 E             Bm9
Mais no esconderijo do Altíssimo
     A9
Eu quero viver
    E       Bm9
Onde a sombra do Onipotente
   F#m    E/G#  A9 B9
Me faz descansar
  F#m   E/G# A9 B9
Descansar
F#m  E/G#   A9   C#m
Descansado estou
F#m  E/G#   A9
Descansado estou

----------------- Acordes -----------------
A = X 0 2 2 2 0
A5 = X 0 2 2 X X
A9 = X 0 2 2 0 0
A9/E = 0 X 2 2 0 0
B = X 2 4 4 4 2
B4 = X 2 4 4 5 2
B9 = X 2 4 4 2 2
Bm9 = X 2 4 6 3 2
C# = X 4 6 6 6 4
C#m = X 4 6 6 5 4
D# = X 6 5 3 4 3
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
