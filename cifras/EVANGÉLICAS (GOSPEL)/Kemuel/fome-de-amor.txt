Kemuel - Fome de Amor

[Intro]

Riff
        Em             D          Bm          C
E|------10s7----|-----7s5---|-----5s2---|-----5s3---|
B|-----------10-|---------7-|---------5-|---------5-|
G|--------------|-----------|-----------|-----------|
D|--------------|-----------|-----------|-----------|
A|-7-7----------|-5-5-------|-2-2-------|-3-3-------|
E|--------------|-----------|-----------|-----------|

Em    D        Bm             C
A contagem do tempo que se acaba
Em    D           Bm       C
Estações que anunciam o recado
Em    D         Bm          C       Em  D  Bm  C
E o fim que está se aproximando, sinais

Em             D       C
Doenças, crises e guerras
A expansão do medo

Em             D      C
Tudo que se construiu

Logo se acabará
Em           D   C
Todo conhecimento de nada valerá
Em          D       C
E do final em diante, tudo eterno será

( Em  D  Bm  C )
( Em  D  Bm  C )
( Em  D  Bm  C )

Em          G/B                    C
Fome de amor, muitos com fome de Amor
                   Em/C   G/B             A
Estão precisando de amor, da Fonte que é o amor

Em             D       C
Doenças, crises e guerras
A expansão do medo
Em             D      C
Tudo que se construiu

Logo se acabará
Em           D   C
Todo conhecimento de nada valerá
Em          D       C
E do final em diante, tudo eterno será

( Em  D  Bm  C )
( Em  D  Bm  C )
( Em  D  Bm  C )

Em          G/B                    C
Fome de amor, muitos com fome de Amor
                   Em/C   G/B             A
Estão precisando de amor, da Fonte que é o amor

( Em  Bm  C  Am )

Em
Menos razão mais compaixão
Bm
Menos acusação mais perdão
C
Menos guerra, ódio, mais Paz
Am
Menos religião, mais amor

Em          G/B                    C
Fome de amor, muitos com fome de Amor
                   Em/C   G/B             A
Estão precisando de amor, da Fonte que é o amor

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
Em/C = 8 7 5 X 8 X
G/B = X 2 0 0 3 3
