David Quinlan - Um Lugar Para Dois

Intro: G D Am D

G
Um lugar para dois
           D
Só eu e você, Jesus
   Am
Meu amado e eu
               D
Só eu e você, Jesus
   G
Um lugar para dois
                D
Só eu e você, Jesus
                Am
Eu quero ouvir teu coração
                              D
Sentir o toque de tuas mãos, Jesus

        F
Corações arrebatados

        Am
Corações entrelaçados
G               D
O meu e o teu, o teu e o meu
F
Corações apaixonados
   Am                   D
Vivendo os nosso sonhos juntos
F
Os teus olhos em meus olhos
        Am
Tua voz em meus ouvidos
           G           D
Nossos corações batendo a um
        F
Corações apaixonados
        Am              D
Vivendo a eternidade juntos

Am           Em          G
Capturado fui pelo teu amor
        D/F#
Amado senhor,
Am      Em     C         D
Jesus, (Jesus) eu te adoro (eu te adoro)
Am          Em           G
Capturado fui pelo teu amor
        D/F#
No meu coração
Am      Em     C       D
Jesus (Jesus), eu te amo (eu te amo)
F
Sobre as estrelas do universo
Am
Abraçado por tua glória
G                    D
Me rendo a ti, por tanto amor
F
A noiva e o noivo
Am              D
Vivendo a eternidade juntos

F
Sobre as estrelas do universo
Am
Abraçado por tua glória
        G                        D
Me rendo a ti, por tanto amor
        F
A noiva e o noivo
        Am              D
Por uma eternidade juntos


C            Em    D       Am
Sou do meu amado e ele é meu
C            Em    D
Sou do meu amado jesus

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
