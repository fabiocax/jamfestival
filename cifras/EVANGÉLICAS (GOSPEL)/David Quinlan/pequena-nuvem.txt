David Quinlan - Pequena Nuvem

Intro: Am  F  C  G

   Am
Eu vejo uma pequena núvem
      F
do tamanho da mão de um homem
     C                        G
mas este é o sinal que a tua chuva vai descer
   Am
Eu vejo uma pequena núvem
      F
do tamanho da mão de um homem
     C                        G
mas este é o sinal que a tua chuva vai descer
      Am           F
faz chover, faz chover
  C                     G
abre as comportas dos céus
         Am           F
e faz chover, faz chover
  C                     G
abre as comportas dos céus


   Am
Eu vejo uma pequena nuvem
      F
do tamanho da mão de um homem
     C                        G
mas este é o sinal que a tua chuva vai descer
   Am
Eu vejo uma pequena nuvem
      F
do tamanho da mão de um homem
     C                        G
mas este é o sinal que a tua chuva vai descer
      Am           F
Faz chover, faz chover
C                      G
abre as comportas dos céus
         Am           F
e faz chover, faz chover
C                      G
abre as comportas dos céus
         Am           F
e faz chover, faz chover
C                      G
abre as comportas dos céus
         Am           F
e faz chover, faz chover
C                       G   Am F C G
abre as comportas dos céus
       Am            F
se pedirmos Jesus virá
      C          G
como chuva descerá
       Am            F
se pedirmos Jesus virá
         C
então clame, então grite
       G
então chame por ele
       Am           F
Faz chover, faz chover
 C                      G
abre as comportas dos céus
         Am           F
e faz chover, faz chover
 C                     G
abre as comportas dos céus
         Am           F
e faz chover, faz chover
 C                     G
abre as comportas dos céus
         Am           F
e faz chover, faz chover
  C                    G
abre as comportas dos céus
         Am  ( F  C  G  Am )  (2x)
e faz chover

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
