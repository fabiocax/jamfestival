David Quinlan - Livre Acesso

G                D/F#                  Em     D/F#
Senhor eu não sou nada diante do seu poder
G       D/F#               Em
Nem merecedor do teu imenso amor
  C             D           Bm           Em
Através do teu filho tenho livre acesso a ti
       C                   Am
Que me fez chegar aos teus pés
  F                  D
Me humilhar diante de ti
Em                 Bm
Deixa o teu rio, passar em minha vida
     C     G/B   Am       C             D
E curar minhas feridas, sarar as minha dores
Em
Livra-me ó Deus,
      Bm
das cadeias que me prendem
  C      G/B   Am
Toca em minha'alma,
  C                D
faz de mim o teu querer

 G
Senhor

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
