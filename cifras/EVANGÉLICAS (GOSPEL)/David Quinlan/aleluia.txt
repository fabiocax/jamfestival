David Quinlan - Aleluia

[INT] ->  E  A  B  C#m  B/D#

    E
Aleluia
    A
Aleluia
    B
Aleluia
    C#m  B/D#
Aleluia

[OBS] -> [Nas ministrações segue os mesmos acordes]

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
B/D# = X 6 X 4 7 7
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
