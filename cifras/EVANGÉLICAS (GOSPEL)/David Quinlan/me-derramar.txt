David Quinlan - Me Derramar

Intro: G  D  Em  C

        G  D
Eis-me aqui outra vez
Em                C
Diante de Ti abro meu coração
      G         D
Meu clamor Tu escutas
   Em               C
E fazes cair as barreiras em mim
   G                D
És fiel, Senhor, e dizes
   Em                C
Palavras de amor e esperança sem fim
     G     D
Ao sentir Teu toque
     Em              C
Por Tua bondade libertas meu ser

Ponte:
   Am      G    C
No calor deste lugar

D
Eu venho ...

Refrão:
 G            D
Me derramar, dizer que Te amo
Em             C
Me derramar, dizer Te preciso
G             D
Me derramar, dizer que sou grato
Em             C
Me derramar, dizer que És formoso.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
