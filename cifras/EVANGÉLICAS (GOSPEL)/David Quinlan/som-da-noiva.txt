David Quinlan - Som da Noiva

G
  Este é o som da sua noiva
C9
  Este é o som da sua igreja
G/B    Am   Am/G      F       D
Apaixonada,     fascinada, por Ti

       G                   C9
Vem Jesus, e toma o Teu lugar
G/B     Am   Am/G          F   D
  Vem Jesus,       vem dançar

G
 Corações apaixonados,
C9
 Corações entrelaçados,
  G/B     Am
o meu e o Teu,
Am/G            F    D
    Meu Amado e eu


G
 Eu ouço o som da Minha Noiva
C9
 Eu ouço o som da Minha Igreja
G/B     Am  Am/G     F         D
Apaixonada,     fascinada, por mim

       G                      C9
Estou aqui, vim tomar o Meu lugar
G/B      Am   Am/G       F   D
   Estou aqui,    vim dançar


(G C9 G/B Am Am/G F D)
Ministração

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am/G = 3 X 2 2 1 X
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
