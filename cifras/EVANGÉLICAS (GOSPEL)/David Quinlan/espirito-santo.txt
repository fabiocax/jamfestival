David Quinlan - Espírito Santo

[Intro] Ab  Eb  Bb
        Ab  Eb  Bb
        Ab  Cm  Bb  Gm
        Ab  Cm  Bb

[Primeira parte]

Cm
Como um vento impetuoso vem
Bb
Soprar sobre mim
Cm
Como um fogo consumidor vem
Bb
Transformar o meu ser
Cm                  Ab
Como um rio de vida vem
Gm                   Bb
Fluir de dentro de mim
        Fm             Ab    Bb
Com um vulcão em erupção vem


Irrompe de mim

[Refrão]

Eb              Fm                          Cm
Espírito Santo, Espírito Santo, Espírito Santo
Bb            Ab
És bem-vindo aqui
Eb              Fm                          Cm
Espírito Santo, Espírito Santo, Espírito Santo
Bb              Gm            Ab
Eu quero mais, mais e mais de Ti

( Cm  Eb  Bb  Gm )
( Ab  Eb  Bb )

[Segunda parte]

Cm             Ab       Eb
Como um vento impetuoso vem
Gm
Soprar sobre mim
Cm            Ab        Eb
Como um fogo consumidor vem
Bb
Transformar o meu ser
Cm                  Ab
Como um rio de vida vem
Gm                   Bb
Fluir de dentro de mim
        Fm            Ab    Bb
Com um vulcão em erupção vem

[Solo] Cm

( Ab  Eb  Bb  Gm )
( Ab  Eb  Bb )

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
Eb = X 6 5 3 4 3
Fm = 1 3 3 1 1 1
Gm = 3 5 5 3 3 3
