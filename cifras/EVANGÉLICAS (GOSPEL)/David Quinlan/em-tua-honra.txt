David Quinlan - Em Tua Honra

E       B        F#m         D         A
em tua honra senhor que aprendi a adorar
           E    B            F#m        D
somente a ti a ti jesus meu rei que ofereço o
     A              E  D           A
meu ser e o meu viver. é neste lugar de
        E  D           A
 adoração, é neste lugar onde os joelhos se
  E     F#m
 dobram que eu levanto minhas mãos a ti e
                             D
abro meu coração todos meus dias de
      A                E  D          A
sacrifícios ofereço a ti. o meu louvor é para
E  D             A             E
ti a minha adoração ofereço a ti.

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
