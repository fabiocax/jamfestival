David Quinlan - Yo Soy Libre

D
Los ciegos verán por ti los mudos cantarán
G
Los cojos saltarán los muertos vivirán
Bm                    A
Las tinieblas huirán por ti yo grito
D
Yo soy libre, (Yo soy libreeee)

CORO:
D
Libre para Correr (Libre para correr)
G
Libre para Danzar (Libre para danzar)
Bm                          A
Libre para vivir por ti (Libre para vivir por ti)
D
 Yo soy libre, (Yo soy libreee)

D
OoOoOh (OoOoOh)

G
OoOoOh (OoOoOh)
Bm
OoOoh (OoOoh)
A
OoOoh (OoOoh)

D
Libre (libreee)

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
G = 3 2 0 0 0 3
