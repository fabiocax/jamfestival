Ministério Apascentar de Louvor (Toque No Altar) - Casa do Oleiro

Intro 2x: G  D  Bm  A9

 G         D         Bm         A9
Deus tem renovo para minha geração
 G        D           Bm            A9
Deus é oleiro, somos barro em suas mãos
       G            D      Bm          F#m
E se cairmos pelo chão ele não desiste não
       G            D           A9
Nos tornar um vaso novo é sua intenção
Bm      F#m         G   D/F#
Ele é a casa de restauração
Bm         F#m          G    A9
Ele não descarta o meu coração
    D                      A9
Eu vou descer, à casa do oleiro vou
                   Bm
É o tempo que Deus preparou
F#m              G
Pra me fazer de novo
    D                      A9/E
Eu vou descer, à casa do oleiro vou

D/F#
Ficar no centro do senhor
                 G
Pra ser um vaso novo
 D
Molda, senhor, minha geração
 A9
Quebra, senhor, minha geração
 Bm                                  Em
Cura, senhor, minha geração com teu fogo

----------------- Acordes -----------------
A9 = X 0 2 2 0 0
A9/E = 0 X 2 2 0 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
