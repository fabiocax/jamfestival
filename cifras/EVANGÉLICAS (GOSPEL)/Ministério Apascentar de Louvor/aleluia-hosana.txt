Ministério Apascentar de Louvor (Toque No Altar) - Aleluia, Hosana

[Intro] E  B/E  A/E  Am/E

E                B/E
Glória, majestade, força, honra e poder
    A/E          Am/E
Sejam dados a Jesus, soberano rei dos reis
E               B/E
Na cruz venceu a morte e o pecado destruiu
  A/E           Am/E        B/D#
Com seu sangue nos comprou nossa dívida pagou

 G#m7     C#m7
Aleluia, hosana
     G#m7   C#m7
Glórias ao Cordeiro
    D  A/C#     F#m7     B9
Nós te adoramos e a ti celebramos
  G#m7    C#m7
Com ações de graças
    G#m7    A9
Danças e alegrias


   F#m7    B9      A9      E/G#
Pois convém louvar-te, ó Senhor!
      F#m7     B9        E      B/E  A/E
Das trevas
Pois convém louvar-te, o Senhor!

C#m7      B/D#      E     F#
Das trevas trouxe-nos pra tua luz
F#m7   G#m7     C#m7
Para contigo reinar
F#m7    G#m7      C#m7    F#/A#
Livre acesso nós temos ao pai
F#/A       B9   C9  Bb/C
Por seu sangue, ó Jesus

[Solo] Am7  Dm7  Am7  Dm7
       Eb9  Bb/D  Gm7  C9
       Bb9  Am7  Dm7  Am7
       Bb9  Gm7  C9  Bb9

 Am7     Dm7
Aleluia, hosana
     Am7   Dm7
Glórias ao Cordeiro
    D#9  Bb/D      Gm7     C9
Nós te adoramos e a ti celebramos
Bb9  Am7    Dm7
Com ações de graças
    Am7   Bb9
Danças e alegrias

   Gm7    C9      Bb9      F/A
Pois convém louvar-te, ó Senhor!
   Gm7    C9      Bb9
Pois convém louvar-te, ó Senhor!

[Final] D#9  Bb/D  Gm7  C9  F

----------------- Acordes -----------------
A/C# = X 4 X 2 5 5
A/E = 0 X 2 2 2 0
A9 = X 0 2 2 0 0
Am/E = 0 X 2 2 1 0
Am7 = X 0 2 0 1 0
B/D# = X 6 X 4 7 7
B/E = X 7 9 8 7 7
B9 = X 2 4 4 2 2
Bb/C = X 3 X 3 3 1
Bb/D = X 5 X 3 6 6
Bb9 = X 1 3 3 1 1
C#m7 = X 4 6 4 5 4
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
D#9 = X 6 8 8 6 6
Dm7 = X 5 7 5 6 5
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
Eb9 = X 6 8 8 6 6
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
F#/A = X 0 4 3 2 X
F#/A# = 6 X 4 6 7 X
F#m7 = 2 X 2 2 2 X
F/A = 5 X 3 5 6 X
G#m7 = 4 X 4 4 4 X
Gm7 = 3 X 3 3 3 X
