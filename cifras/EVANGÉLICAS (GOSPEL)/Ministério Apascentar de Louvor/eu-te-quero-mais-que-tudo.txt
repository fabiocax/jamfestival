Ministério Apascentar de Louvor (Toque No Altar) - Eu Te Quero Mais Que Tudo

       Am7           G/B
Eu te quero mais que tudo
       Am7           G/B
Eu te quero mais que tudo
       Am7           G/B
Eu te quero mais que tudo
       Am7           G/B        Em7   D/F#   G
Eu te quero mais que tudo Senhor

       G/B   C9
Porque sem ti Senhor,
    D/C
Sem ti
      Bm7    Em7
Nada sou
           Am7     G/B
Nada posso fazer
          Am7     G/B
Sem a tua presença
               Am7   G/B
Não consigo viver

              Am7   D9
Sem a tua presença

Final:  C9  D9  D4  D  G

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Bm7 = X 2 4 2 3 2
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
D/C = X 3 X 2 3 2
D/F# = 2 X 0 2 3 2
D4 = X X 0 2 3 3
D9 = X X 0 2 3 0
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
