Ministério Apascentar de Louvor (Toque No Altar) - Com Vestes de Louvor

E           F#     E
 Com veste de louvor
      F#    E
Com Alegria
          F#       E
Estou trazendo a arca
E         F#    E
 Eu me purifiquei
          F#   E
Eu já me preparei
             F#     E
Porque amo tua presença
Bm         A   G               A Bm
 E com a multidão eu vou te Celebrar
            A      G
Com brados e com danças
Bm            A    G            A   Bm               A   G
 Pois não há nada que me dê maior prazer que tua presença

               Bm
Pra ti eu cantarei

                A
Pra ti eu dançarei
                 F#m     G
Teu espírito se move em mim
             Bm
Eu me humilharei
                 A
Desprezível me farei
               F#m       G
Diante de ti Senhor meu Deus
                 D
Não posso me conter
                       E/D
Tua alegrarei está em mim
                   G  A
E eu amo Tua presença

Em      F#m         G
 Me alegrarei Diante de ti
             A
Porque amo tua presença.
Em      F#m         G
 Me alegrarei Diante de ti
               A
Porque te amo Senhor

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E/D = X 5 X 4 5 4
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
