Ministério Apascentar de Louvor (Toque No Altar) - Selo do Teu Amor

INTRO: Am  G  C F  Am  G  Bb

   C        F/A      G/B
Senhor, sei que estas junto a mim
    C         F        G
Me guardas com o selo do Teu     amor
    F               C/E
Me volto a todo instante a pensar em Ti
     Dm
Não posso esquecer
      Fm6       G
Teu cuidado de pai por mim
C               F/A  G/B
Ainda que circunstancias digam não
 C           F/A  G/B
Contigo não há mais o que temer
F              C/E
E Tu serás pra sempre o mesmo Deus

    Dm       Fm          G
Eu não abro mão de Te buscar mais e mais


F     G          C
__Mesmo que tudo pareça contrário
F     G               C
__Estou seguro e descanso em Teus braços
F     G          C
__Mesmo que tudo pareça contrário
F                    C
__Vejo tuas Mãos  guiando os meus passos
    Dm
 E me conduzindo
    Fm        C
Ao centro da Tua vontade

F       G        C
__Meus pensamentos não se comparam aos Teus
F        E          Am
__E os Teus caminhos são bem mais altos que os meus
F      E     Am
__Minha esperança jamais será frustrada
Dm        G      E/G#         A
Meu futuro esta     firmado em Tuas mãos


G     A         D
Mesmo que tudo pareça contrário
G      A              D
__Estou seguro e descanso em Teus braços
G     A          D
__Mesmo que tudo pareça contrário
G        A            D
__Vejo tuas Mãos  guiando os meus passos
    Em
 E me conduzindo
    Gm     A    D
Ao centro da Tua vontade

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
Fm = 1 3 3 1 1 1
Fm6 = 1 X 0 1 1 X
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
Gm = 3 5 5 3 3 3
