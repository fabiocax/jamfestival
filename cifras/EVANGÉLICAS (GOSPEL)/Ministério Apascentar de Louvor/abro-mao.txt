Ministério Apascentar de Louvor (Toque No Altar) - Abro Mão

[Intro] F#m  E  D
        F#m  E  D

F#m          E       D      E
   A quem tenho eu nos céus além de Ti
F#m          E             Bm7         E4
   E não há na terra alguém que eu queira mais que a Ti

Bm7          C#m  Bm7           E4  E
   Estou apaixonado,   desesperado de amor

F#m          E       D     E4  E
   Eu estou disposto a morrer por Ti
F#m         E          Bm7     E4
   E construirei no cume do monte um altar

Bm7              C#m
   O sacrifício sou eu
Bm7              E
   O sacrifício sou eu


   C#m
Abro mão dos meus sonhos
   F#m
Abro mão dos meus planos
   Bm7            E4 E/D
Abro mão da minha vida por Ti
   C#m             F#m
Abro mão dos prazeres e das minhas vontades
   Bm7            E
Abro mão das riquezas por Ti

       F#m   E D C#m
Estou apaixonado

F#m          E       D      E
   A quem tenho eu nos céus além de Ti
F#m          E             Bm7         E4
   E não há na terra alguém que eu queira mais que a Ti

Bm7          C#m  Bm7           E4  E
   Estou apaixonado,   desesperado de amor

F#m          E       D     E4  E
   Eu estou disposto a morrer por Ti
F#m         E          Bm7     E4
   E construirei no cume do monte um altar

Bm7              C#m
   O sacrifício sou eu
Bm7              E
   O sacrifício sou eu

   C#m
Abro mão dos meus sonhos
   F#m
Abro mão dos meus planos
   Bm7            E4 E/D
Abro mão da minha vida por Ti
   C#m             F#m
Abro mão dos prazeres e das minhas vontades
   Bm7            E
Abro mão das riquezas por Ti
   C#m
Abro mão dos meus sonhos
   F#m
Abro mão dos meus planos
   Bm7            E4 E/D
Abro mão da minha vida por Ti
   C#m             F#m
Abro mão dos prazeres e das minhas vontades
   Bm7            E
Abro mão das riquezas por Ti

       F#m   E D C#m
Estou apaixonado

( F#m  E  D  C#m )
( F#m  E  D  C#m )

  F#m   E    D    C#m
O que fazer? pra onde ir?
      F#m      E         D  C#m
Se só Tu tens as palavras de vida eterna?
  F#m   E    D    C#m
O que fazer? pra onde ir?
      F#m      E         D  C#m
Se só Tu tens as palavras de vida eterna?
  F#m   E    D    C#m
O que fazer? pra onde ir?
      F#m      E         D  C#m
Se só Tu tens as palavras de vida eterna?

       F#m   F#m/E D C#m F#m
Estou apaixonado

----------------- Acordes -----------------
Bm7 = X 2 4 2 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E/D = X 5 X 4 5 4
E4 = 0 2 2 2 0 0
F#m = 2 4 4 2 2 2
F#m/E = X X 2 2 2 2
