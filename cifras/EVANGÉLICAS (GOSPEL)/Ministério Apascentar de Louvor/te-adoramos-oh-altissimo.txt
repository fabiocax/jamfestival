Ministério Apascentar de Louvor (Toque No Altar) - Te Adoramos, Oh Altíssimo

INTRO:

B(9)          B4(9)
__Deus de toda criação
F#/A#    B(add9)       F#/A#
__adorado entre os povos__
G#m         G#m/F#       E    B/D#
__Grande é o Teu      poder
C#m              F#4
__és perfeito em glória.

B(add9)          B4(9)
__Os céus revelam Tua grandeza
F#/A#           B(add9)       F#/A#
__e o firmamento anuncia  __
G#m      G#m/F#           E    B/D#
__as obras de    Tuas     mãos
C#m          F#4
__sobre toda terra

A          E/G#          G           D/F#       F
Vem me inundar,       __nos envolver,     __com Tua

E4             E
Presen - - - ça
      B4      B           B4         B
Te adora - - mos oh! Altís - - - simo
      F#4        F#        F#4        F#
Te exalta - - - mos oh! Cordei - - - ro
      B4        B        G#m7             F#4    F#
Tu és dig - - no, Tu és San - - - to, Senhor
    B4         B          B4       B
Tua Gló - - ria enche o tem - - plo
       F#4      F#       F#4     F#
e minha vi - - da por inteiro se
   G#m7         E      F#   B4    B
Completa ao     ver Sua majestade.

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
B(9) = X 2 4 4 2 2
B(add9) = X 2 4 4 2 2
B/D# = X 6 X 4 7 7
B4 = X 2 4 4 5 2
B4(9) = X 2 4 6 5 2
C#m = X 4 6 6 5 4
D/F# = 2 X 0 2 3 2
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E4 = 0 2 2 2 0 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
F#/A# = 6 X 4 6 7 X
F#4 = 2 4 4 4 2 2
G = 3 2 0 0 0 3
G#m = 4 6 6 4 4 4
G#m/F# = X X 4 4 4 4
G#m7 = 4 X 4 4 4 X
