Ministério Apascentar de Louvor (Toque No Altar) - Bondade e Paz

E      F#m            D
 Sua bondade e paz eu viverei
E           F#m               D
Aonde quer que eu vá
E      F#m    D
 A misericórdia me seguirá,
E     F#m      D
 Seguro estou
  E       F#m          D
Quando o Senhor me restaurou,
E      F#m        D
Voltei a sonhar
E      F#m        D
Já posso cantar o seu favor,
E      F#m    D
Pois livre sou

A     B7               G#m7     C#m7
Fui escolhido pelo seu amor
A            B7            E
Imagem do grande eu sou

A        B7                B             C#m              D
Estou firmado no Reino que me prometeu

E          D                      A        C
Para te adorar foi que eu nasci
E          D            A                  C
É o meu prazer, Vou celebrar
E        D            A                    C
És minha rocha,O meu louvor,
   D   A/C#
Te amo
  C     B            E
Te amo    Senhor!

E     D              A
És minha rocha,O meu louvor,
D   A/C#
Te amo
C    B
Te amo
D    A/C#
Te amo
C    B
Te amo
D    A/C#
Te amo
C        B          E
Te amo, Senhor!

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#m7 = 4 X 4 4 4 X
