Ministério Apascentar de Louvor (Toque No Altar) - Extraordinário

         Am       G               F
Pelo Teu nome os fracos encontram força
         Am     G            F
Pelo Teu nome cativos são libertos
     Em                               F
O fracasso de ontem dá lugar à dupla honra
                G
Ao som do Teu nome

         Am        G              F
Pelo Teu nome o inimigo tem que sair
         Am     G            F
Pelo Teu nome doentes são curados
     Em                       F
A derrota se transforma em vitória
                G
Ao som do Teu nome

  C                F
O sobrenatural, o extraordinário
   Dm              F           G
Só Tu podes fazer, Deus de maravilhas

  Am                   G
Milagres e sinais que rompem o impossível
   Dm              F       G       F (Dm F quando for para o solo de guitarra)
Só Tu podes fazer, Deus de maravilhas

                        Am
Nunca se viu um Deus assim
              G
Tu és incomparável
       Dm                  F
Não há outro igual, não há outro igual
       Am           G
Não há outro igual, Deus de maravilhas

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
