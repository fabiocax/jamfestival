Ministério Apascentar de Louvor (Toque No Altar) - Glória da Segunda Casa

Intro:
              A
Por que a glória
             E
Da segunda casa
       F#m                D
Será maior que a primeira
           A            E          F#m              D
Diz o senhor, diz o Senhor, diz o Senhor, diz o Senhor (1º Vez)

         A                 E          F#m             D
Diz o Senhor, O Grande EU SOU, diz o Senhor, diz o Senhor,      (2º Vez) (Refrão 2x)

        A
Eu sei que já
       E
Posso sorrir
          F#m7              D
É o meu tempo de cantar
       A
O meu DEUS

          E
Me restaurou
       F#m           D
Vale a pena confiar
 Bm                A/C#
Não ficará sem resposta
 D                E
Tudo que eu clamar
        Bm          A/C#
Meu choro virou alimento.
       D      Bm         E
Pro meu milagre brotar

            A
Por que a glória
            E
Da segunda casa
       F#m     E         D
Será maior que a primeira
          A
Diz o senhor,
              E           F#m            E        D
O Grande EU SOU, diz o Senhor, diz o Senhor     (Refrão 2x)

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
