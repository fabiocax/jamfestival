Ministério Apascentar de Louvor (Toque No Altar) - Restitui

(intro) Bm7  G

Bm7                   G
Os planos que foram embora
                  Bm7
O sonho que se perdeu
                    G
O que era festa e agora
          D/F#      Em7
É luto do que já morreu
                            F#m7/9
Não podes pensar que este é o teu fim
                       G
Não é o que Deus planejou
  D/F#         Em7
Levante-se do chão!
            A
Erga um clamor !

A4  A G                              Em7
Restitui! Eu quero de volta o que é meu

A4 A G                             Em7  A
Sara-me! E põe teu azeite em minha dor
      G                            A A4 A
Restitui! E leva-me às águas tranqüilas
     G  Em7                A4    A
Lava-me e refrigera a minh'alma
      Bm7     G
Restitui

(solo)
E|---------------------------------------------------------------------------------------
B|------------------------------------------------------------------------7-5------------
G|--7-6-4--4-6/7--7-7-7-7-6-4-0-7-6-4-4-6/7-7-7-7-7-6-4-0-7-6-7-7-7-7-7-7-----7-6--0-2-4-
D|---------------------------------------------------------------------------------------
A|---------------------------------------------------------------------------------------
E|---------------------------------------------------------------------------------------

(teclado)  B C# D  A

            Em7
E o tempo que roubado foi
A
Não poderá se comparar
Em7
A tudo aquilo que o Senhor
A
Tem preparado ao que clamar
Em7              D/F#
Creia porque o poder de um clamor
      G      A4
Pode ressuscitar

A4  A G                              Em7
Restitui! Eu quero de volta o que é meu
A4 A G                             Em7  A
Sara-me! E põe teu azeite em minha dor
      G                            A A4 A
Restitui! E leva-me às águas tranqüilas
     G  Em7                A4    A
Lava-me e refrigera a minh'alma
      Bm7     G   Bm7    G
Restitui

----------------- Acordes -----------------
A = X 0 2 2 2 0
A4 = X 0 2 2 3 0
B = X 2 4 4 4 2
Bm7 = X 2 4 2 3 2
C# = X 4 6 6 6 4
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
E = 0 2 2 1 0 0
Em7 = 0 2 2 0 3 0
F#m7/9 = X X 4 2 5 4
G = 3 2 0 0 0 3
