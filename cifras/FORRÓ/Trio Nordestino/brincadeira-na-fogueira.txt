Trio Nordestino - Brincadeira Na Fogueira

    Am                         Dm
Tem tanta fogueira, tem tanto balão
Tem tanta brincadeira
               E                  Am
Todo mundo no terreiro faz adivinhação
                                    A7
Meu são João eu não, meu são João eu não
               Dm
Eu não tenho alegria

             Am                  E7
Só porque não vem, só porque não vem
                Am
Quem tanto eu queria

   Am    E                    Am
Danei a faca no tronco da bananeira
      A7             Dm
Não gostei da brincadeira
         E7         Am
Santo Antonio me enganou

      E                         Am
Sai correndo lá pra beira da fogueira
        A7      Dm                  Am
Vê meu rosto na bacia, a água se derramou

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
