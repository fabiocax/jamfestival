Banda Magníficos - Encanto

Intro: A  D  E
E|---5---5------------------5---5---------------5---5-------|
B|-5---5-------10-9/7-5---5---5---------------5---5---------|
G|---------6-7--------------------6-7-6-6/4-----------------|
D|----------------------------------------------------------|
A|----------------------------------------------------------|
E|----------------------------------------------------------|

E|----------------------------------------------------------|
B|-----10-9/7-5---10-12-12/14-12-12------10-10/12-10-10p9---|
G|-6-7----------9-------------------9-11------------------9-|
D|----------------------------------------------------------|
A|----------------------------------------------------------|
E|----------------------------------------------------------|

   A     F#m         D            E
Melhor assim cada um vai pro seu lado
   A          F#m                 D          E
Está desfeito não tem jeito o encanto foi quebrado
     A   F#m       D          E
Não tô afim de amar sem ser amado

    A          F#m                D         E
Eu não aceito no meu peito um amor despedaçado
F#m                 Bm
Você não soube entender os meus apelos
E                         A            E
Apaixonadamente me entreguei em suas mãos
F#m                  D                    Bm
Vai ser inútil dizer, que tudo foi sem querer
                    E                                     A
Agora quer meu perdão, joga a culpa no coitado do seu coração
                 F#m                  D
Vai ser inútil dizer, tô proibido a você
                      E
Não vou te dar meu perdão!!!

Refrão:
             A         D      E
Cristal quebrado não cola jamais
             A         D         E
Sonhos feridos não curam não saem
                D              E
Melhor me esquecer não volto atrás
         F#m       D          E
Baby eu juro te amar nunca mais

Solo: (Intro)

F#m                 Bm
Você não soube entender os meus apelos
E                         A            E
Apaixonadamente me entreguei em suas mãos
F#m                  D                    Bm
Vai ser inútil dizer, que tudo foi sem querer
                    E                                     A
Agora quer meu perdão, joga a culpa no coitado do seu coração
                 F#m                  D
Vai ser inútil dizer, tô proibido a você
                      E
Não vou te dar meu perdão!!!

(Refrão)

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
