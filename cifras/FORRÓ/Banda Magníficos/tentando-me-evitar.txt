Banda Magníficos - Tentando Me Evitar

Intro: Em  C  D  G  D

 Em                               Bm
Ei, há muito tempo que eu estou a te esperar
                          A
Sinto vontade de poder te apertar
                          C            D
Sinto saudade dos momentos de nós dois...
 Em                               Bm
Ei, não jogue fora tanto amor, tanta paixão
                         A
Você precisa escutar seu coração
                       D
Sei que ele ainda bate por nós dois
 G                                              B
Volta amor, sei que ainda existe chances pra recomeçar
                                       Em
Um grande amor não pode assim logo se acabar
          C
Sei que você não me esqueceu
          D
Estava escrito em teu olhar

 G
Volta amor, não tenha medo, vem depressa, sai da
 B
solidão...
                                 Em
Porque eu preciso confortar teu pobre coração
          C                  D
Sei que você não me esqueceu
                      G     Em
Só tá tentando me evitar...
C     D               G
Só tá tentando me evitar...

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
