Banda Magníficos - Aí Eu Quero Ver

Gm
Não vá embora

Preciso tanto te dizer

Não jogue fora
                            Cm
O amor que eu sinto por você
                              D7
É verdadeiro não aceito te perder

Não vá, Não vá

Cm
Porque tem que ser assim
        F
Será que esse amor foi pouco
     Bb                      Gm
Ou você só quis viver uma aventura
     Cm                          D7
Eu pensei que dessa vez era pra sempre

                   Gm
Mais eu vejo foi loucura
      G7
Me deixar iludir por você

   Cm                      F
Um dia sei que vai me procurar
          Bb                   Gm
Quando a solidão bater na sua porta
      Cm                    (Eb) D7
vai sentir o mesmo que eu sinto agora
               Gm      D7
E vai querer voltar

[Refrão]
            Gm
Aí eu quero ver

Seu coração bater
                  Cm
Queimar tudo por dentro
                   F
Passar o que eu passei

Chorar como eu chorei
                 Bb                 D7
Dizer que está sofrendo, sofrendo doendo
            Gm
Aí você vai ver

Que não é bom perder
             Cm
Um amor verdadeiro
      Eb                     D7
Dessa vez eu quero ver você provar
                  Gm
Do seu próprio veneno.

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
