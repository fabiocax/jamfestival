Banda Magníficos - Animal Faminto

(intro) F

F
Toda vez em que você pensar em mim
Am
Acredite que eu não sei viver assim
Bb
Telefona pelo menos pra dizer
     C
Que você também não pode me esquecer
F
Na verdade tua ausência dói demais
        Am
Eu lhe juro que não sei se sou capaz
      Bb
De agüentar mais um minuto dessa espera
            C
Dessa eterna sensação de solidão
C#°                 Dm          Am
Ah meu coração       libera esse teu grito
Dm                      Am
Solta tua voz de       animal faminto
            Gm
Diz que troca a vida inteira
               C
Por um pouco de emoção
        Gm                          C     Bb    Am    Gm    F
Diz pra ela que estou louco de paixão

     F
Te amo demais
Se eu tenho você
   A
O resto tanto faz
Nada vai mudar
   Bb
O rumo desse amor
          Bb                 Gm      C
Porque a vida só tem graça desse jeito
F
Te amo demais
                            A
Não sei mais viver tão longe de você
Volta por favor
   Bb
Aonde você for
    Gm                                       C
Eu preciso te encontrar no meu caminho

Bb
Sabe meu amor
        C                                  Dm    Am   Bb   C
Eu não posso mais viver aqui sozinho

F                         Am
Mais uma vez é madrugada
          Bb                               C
E a saudade não me deixa descansar
F                             Am
Meu coração fera assustada
     Bb                          C    Bb   Am   Gm   F (refrão)
Tá querendo você perto pra sonhar


----------------- Acordes -----------------
Capotraste na 1ª casa
Bb*  = X 1 3 3 3 1 - (*B na forma de Bb)
C*  = X 3 2 0 1 0 - (*C# na forma de C)
Dm*  = X X 0 2 3 1 - (*D#m na forma de Dm)
C#°*  = X 4 5 3 5 3 - (*D° na forma de C#°)
F*  = 1 3 3 2 1 1 - (*F# na forma de F)
Gm*  = 3 5 5 3 3 3 - (*G#m na forma de Gm)
A*  = X 0 2 2 2 0 - (*A# na forma de A)
Am*  = X 0 2 2 1 0 - (*A#m na forma de Am)
