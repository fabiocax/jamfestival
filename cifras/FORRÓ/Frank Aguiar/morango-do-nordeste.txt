Frank Aguiar - Morango do Nordeste

(intro 2x) B F# C#m G#m F#

B
Estava tão tristonho
                             F#   C#m
Quando ela apareceu

Seus olhos que fascinam
                    G#m  F#  B
Logo estremeceu

B
Os meus amigos falam
                         F#     C#m
Que eu sou demais

Mas é somente ela
                   G#m   F#
Que me satisfaz

B
É somente ela

                     F#   C#m
Que me satisfaz

É somente ela
                    G#m  F#  B
Que me satisfaz

B
Você só colheu
                         F#   C#m
O que você plantou

Por isso é que eles falam
                           G#m      F#   B
Que eu sou um sonhador

Me diz o que ela
                     F#    B
Significa pra mim

Se ela é um morango
                  F#        B
Aqui do nordeste

Tu sabes não desisto
                       F#       B
Sou cabra da peste

Apesar de colher
                       F#    C#m
As batatas da terra

Com essa mulher
                         G#m   F#  B
Eu vou até pra guerra

 B                              F#
AAAAAAAIIII, é amoooor
                C#m
Ai ai ai é amor
     G#m  F#
É amor

(intro)

(repete tudo)

----------------- Acordes -----------------
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
