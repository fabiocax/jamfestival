Frank Aguiar - Homenagem Às Sogras

E                   B
Não malhe a sogra
                   E
Não xingue ela
                    B
Sogra é boazinha
                              E
Você vai precisar dela

(intro) E B E B E B E B E

E           B
O povo diz
                                    E
Que ela tem língua de faca
                       B
Que é uma jararaca
                      E
Difícil de suportar

                   B
Só que eu acho

                               E
Que isso é mentira pura
                                    B
Pois a sogra é uma doçura
                            E
Você pode se casar

         B
Dizem também
                                  E
Que a sogra pega no pé
                                 B
Genro e nora sendo mole
                                E
Ela faz o que bem quer

                  B
Só que eu acho

Que a sogra
                          E
É uma boa campanha
                                          B
Mais uma mãe que você ganha
                         E
Além da sua mulher

Não malhe a sogra...

              B
Falar de sogra
                          E
Isso já virou mania
                          B
Toda hora, todo dia
                                     E
Tem alguém pra lhe malhar

                  B
Eu não permito
                                   E
Falar dela em minha frente
                                     B
Pois sogra é o melhor prêmio
                                   E
Que um ser pode ganhar

                B
A sogra é boa
                                   E
Tem predicados completos
                               B
Ama a nora cria os netos
                                 E
E adora o genro que tem

                  B
Empresta cheques
                          E
Dá casa para morar
                            B
Um dia vou me casar
                             E
Pra ter sogra também

Não malhe a sogra...

----------------- Acordes -----------------
B = X 2 4 4 4 2
E = 0 2 2 1 0 0
