Frank Aguiar - Rapariga

(intro 2x) Am E Am E Am

Am
Fui convidado
                              Dm
Pra cantar uma festinha
                         E
De um velho amigo
                         Am
Imigrante português

         Dm
Era casado

Com uma bonita moça
   E
Aquela que o cara
              Am E Am
Se sente rei

Como misturei

                 Dm
Forró com Fado

A turma toda
         E                Am
Começou a chamegar

               Dm
E a linda moça
                                 Am
Me chamou pra ver a lua
           E
E o português
                             Am
Ficou doido a reclamar

                         E
Cadê minha rapariga
                           Am
Onde é que ela está
                                  Dm
Eu não me casei com ela
                  E            Am
Pra o cantador carregar

                      E
Rapariga, rapariga
                                   Am
Não me faça assim sofrer
                       Dm
Eu te amo, eu te adoro
             E             Am
Não me bote pra moer

Cadê minha rapariga...

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
