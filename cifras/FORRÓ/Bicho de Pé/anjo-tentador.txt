Bicho de Pé - Anjo Tentador

Am                        F        Bm7/5-
Quanto tempo eu parei te esperando
                     E7     Am
E você não conseguia me ver
                                  Bm7/5-
Pois quem nunca teve amor sincero
          E7                Am
Não percebe quando acontecer

    A7              Dm
Talvez não seja tarde
                  Am
Meu peito ainda arde
               E7           Em7/5-
Perdido nesse mar de paixão
      A7      Dm
Mas tenha paciência
                  Am
Com tanta concorrência
                  F      E7      Am
Você não sai do meu coração

     A7          Dm
Já fui em tantos lugares
                  Am
Emprestei mil olhares
                  Bm7/5-    E7 Em7/5-
Querendo me falar de a-mor
Bb7   A7      Dm
Mas nunca acontece
                 Am
Você é que aparece
                F   E7   Am
Na forma de anjo tentador
                  E7
Eu estou te esperando
         Am             E7
Te esperando, Te esperando
                                Am
E você não consegue me ver

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bb7 = X 1 3 1 3 1
Bm7/5- = X 2 3 2 3 X
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
Em7/5- = X X 2 3 3 3
F = 1 3 3 2 1 1
