Bicho de Pé - Conselho Aos Meninos

Am                  E7
Se você quiser conquistar
                   Am
Aquela mulher do forró
                        E7
Atenção pro que eu vou falar
                     Am E7 Am
E você não fica mais só
Am
Chegue de mansinho perto dela
                               E7
Com muita delicadeza pra ela não se acanhar
Vá bem bonitinho, perfumado
Pois um homem arrumado
                    Am
É bem mais fácil de notar
De uma piscadinha assim de lado
       A7                   Dm
Pegue na mãozinha dela e convide pra dançar
                      Am
Diga com o rostinho colado

           E7
Quero ser seu namorado
                   Am   A7
Que eu duvido ela negar
Dm                  Am
Diga com o rostinho colado
              E7
Quero ser seu namorado
             Am   E7 Am
Ela vai te aceitar      (depois peça...)
          E7            Am A7
Me dá um beijo,vem me beijar
                    Dm
Pois é nessa brincadeira
Com você a noite
E7                 Am
Inteira que eu vou me acabar...

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
