Bicho de Pé - Eu e Você

Intro:  43 42 53 65 63 65 54 53 51
        (F  G/F Bbm6 C7)

F                     Dm
Se eu te der a mão me leva
                   G7(13)
para qualquer lugar onde a gente
G7(b13)      C7(9)      C7(b9)
possa ficar a sós e namorar

F              Dm
Uma rede na varanda para embalar
G7(13)   G7(b13)        C7(9)   C7(b9) C#°
abraçados ver o pôr-do-sol numa praia
Dm7    Am7      Bb          C7  C#°
beijar até de manhã, de manhã

      Dm7       Am7  Bb
Fazer amor eu e você
               C7
não há nada melhor

F       G/F
Eu e você
Bbm6            C7
não há nada tão bom

F     G/F     Bbm6    C7

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Bb = X 1 3 3 3 1
Bbm6 = 6 X 5 6 6 X
C#° = X 4 5 3 5 3
C7 = X 3 2 3 1 X
C7(9) = X 3 2 3 3 X
C7(b9) = X 3 2 3 2 X
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
F = 1 3 3 2 1 1
G/F = 1 X X 0 0 3
G7(13) = 3 X 3 4 5 X
G7(b13) = 3 X 3 4 4 3
