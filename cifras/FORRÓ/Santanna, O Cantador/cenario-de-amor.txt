Santanna O Cantador - Cenário de Amor

Em                                            F#m7(b5)
Nos braços de uma morena quase morro um belo dia
         B                       Em
Inda me lembro meu cenário de amor
                                       B                    Am      B            Em
Um lampião aceso o guarda roupa escancarado, vestidinho amassado debaixo de um batom
              E7                    Am           D7                     G
Um copo de cerveja, uma viola na parede, e uma rede convidando a balançar
    B          Em                     B                F#m7(b5)        B7        Em
No cantinho da cama um rádio a meio volume, o cheiro de amor e de perfume pelo ar

        B                                  Em
Numa esteira o meu sapato pisando o sapato dela
                                       B                F#m7(b5)    B7        Em
Em cima da cadeira, aquela minha bela cela ao lado do meu velho alforge de caçador
          B                                    Em
Que tentação, minha morena me beijando feito abelha
                                     B            F#m7(b5)       B7        Em
A lua malandrinha pela brechinha da telha, fotografando o meu cenário de amor

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F#m7(b5) = 2 X 2 2 1 X
G = 3 2 0 0 0 3
