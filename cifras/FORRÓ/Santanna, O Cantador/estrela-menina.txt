Santanna O Cantador - Estrela Menina

Intro: E  A  B  E  Bm  A  E
       C#m  E  B  A#  A  B  E (Baixos da Sanfona)

E           Bm           A             E
Nem toda estrela tem o brilho dos teus olhos
E          Bm          A         E
 nem toda flor cheira igual a você
           G#                       C#m
 nem todo beijo tem o calor do teu beijo
          F#7            B            E
 que me alucina que me ensina a te querer
E         Bm      A            E
 nem toda mão acaricia como a tua
E            Bm           A            E
 nem toda ausência fez de mim um sonhador
           G#                       C#m
 nem todo corpo tem o calor do teu corpo
           F#7   B              E
que fez de mim escravo do teu amor

Refrão:


E         A       B         E
Espera menina que eu chego lá
E             A          B          C#m
Espera, me espera que eu vou te esperar
             G#7               F#m
Quem sabe um dia espiando uma fonte
              A       B           E
Feliz eu te conte os segredos do mar.

(Repete tudo )


----------------- Acordes -----------------
A = X 0 2 2 2 0
A# = X 1 3 3 3 1
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#7 = 2 4 2 3 2 2
F#m = 2 4 4 2 2 2
G# = 4 3 1 1 1 4
G#7 = 4 6 4 5 4 4
