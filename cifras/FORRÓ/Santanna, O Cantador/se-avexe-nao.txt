Santanna O Cantador - Se Avexe Não...

C        Dm7
ôôô... sha, na, na, na, na, na, ná
G        C                       G
ôôô... sha, na, na, na, na, na, ná

G         C
Se avexe não, amanhã pode acontecer tudo
            Dm7
Inclusive nada
          G         Dm7                G                C
Se avexe não, a lagarta rasteja até o dia em que cria asa.
          C                                           Dm7
Se avexe não, que a burrinha da felicidade nunca se atrasa
          G        Dm7                                G    C  C7
Se avexe não, que ela para na porta da frente da sua casa, ô ô

          F   F°                         C/B     Em
Se avexe não, toda caminhada começa no primeiro passo
       Am                                      Dm7
A natureza não tem pressa, segue o seu compasso
          G7           C   C7
Inexorávelmente chega lá,  ô ô


          F     F°                       C/B   Em
Se avexe não, observe quem vai subindo a ladeira
Am                              Dm           G7                                C     G
Seja princesa ou seja lavadeira, pra ir mais alto vai ter que suar

C              Dm
Ôô... coisa boa é namorar
G                C         G
Ôô... coisa boa é namorar
C              Dm
Ôô... coisa boa é namorar
G                C         G
Ôô... coisa boa é namorar

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C/B = X 2 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F° = X X 3 4 3 4
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
