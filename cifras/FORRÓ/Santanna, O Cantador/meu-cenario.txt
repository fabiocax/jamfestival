Santanna O Cantador - Meu Cenário

Intro: Gm Cm F Bb D Gm D Gm

Gm                                       D
Nos braços de uma morena quase morro um belo dia,
                                 Gm
Inda me lembro meu cenário de amor,
                                     D                    Cm        D           Gm
Um lampião aceso o guarda roupa escancarado, vestidinho amassado debaixo de um batom,
              G                   Cm            F                     Bb
Um copo de cerveja, uma viola na parede, e uma rede convidando a balançar,
     D          G                      D                 Cm          D          Gm
No cantinho da cama um rádio a meio volume, o cheiro de amor e de perfume pelo ar,
       D                                    Gm
Numa esteira o meu sapato pisando o sapato dela,
                                       D                                      Gm
Em cima da cadeira, aquela minha bela cela ao lado do meu velho alforge de caçador,
          D                                   Gm
Que tentação, minha morena me beijando feito abelha,
                                    D             Cm             D         Gm
A lua malandrinha pela brechinha da telha, fotografando o meu cenário de amor.

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
