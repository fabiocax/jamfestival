Santanna O Cantador - Siá Filiça

Intro: G  D  C  G  C  G  A  D  G

G
Cadê a lenha da fogueira Siá Filiça
                             D
Cadê o milho pra assar

Cadê aquele teu vestidinho de chita
                                 G
Que tu vestia pra dançar
          F#m     Em
Cadê aquele sanfoneiro
                              B
Que eu pedia pra tocar
                               C
A canção da minha terra
                           D
Um forró de pé-de-serra
                            G
Que eu ajudava a cantar
                       F#m          B
Quando me lembro disso tudo Siá Filiça

                       Em    D
Me dá vontade de chorar

( G  D  C  G  C  G  A  D  G )

Cadê aquele balãozinho Siá Filiça
                                D
Que coloria o meu lugar

Minha esperança ainda dorme Siá FiIiça
                                  G
E eu com pena de acordar
                                   Em
Quebrar panela no terreiro
                              B
E a fogueira pra pular
                                     C
Uma quadrilha bem marcada
                                  D
 um belo São João de latada
                                G
Que era bom pra namorar
                        F#m         B
Quando me lembro disso tudo Siá Filiça
                          E    D
Me dá vontade de chorar

( G  D  C  G  C  G  A  D  G ) (2x)

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
