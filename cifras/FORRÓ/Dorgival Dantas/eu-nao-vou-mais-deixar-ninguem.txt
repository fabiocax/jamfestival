Dorgival Dantas - Eu Não Vou Mais Deixar Ninguém

          A         E           Bm          E
Eu não vou mais deixar ninguém me fazer sofrer outra vez
       F#m   C#m            D         E
Posso amar você, mas não esqueci o que fez

( D  E  A  D )
( Bm  E  D  A )
( D  E  A  D )
( Bm  E  D  A )

          E          F#m                          Bm
Se um dia você se lembrar como foi nossa primeira vez
            D           E                       A
E sentir vontade de chorar pode ser saudade, talvez
             E              D                        C#m Bm
Algo que te faz lembrar de mim, que ninguém mais sabe fazer
             F#m             E                        A
Um simples detalhe, coisa assim, que nada te faz esquecer
       C#           D                C#m   Bm
Só lembrar de nós dois, até quando eu não sei
      F#m         E                        A
Pode ser que depois tudo isso se acabe de vez

         C#     D                           C#m   Bm
Ou quem sabe você deixe tudo e queira voltar pra mim
        F#m         E                             A  D A
São surpresas do amor que até hoje ninguém pode dizer
   E D E    G Bm
Afinal, é o amor

A                       E      Bm                  F#m
  Mas você não tem que acreditar em tudo que acabei de dizer
D                         A                            E
Tem que consultar seu coração pra depois não se arrepender (se arrepender, se arrepender)
Bm                                            E
  O que mais quero de verdade hoje é ter você comigo

[Refrão]

                    A         E           Bm          E
Mas não vou mais deixar ninguém me fazer sofrer outra vez
       F#m   C#m            D         E
Posso amar você, mas não esqueci o que fez
                   F#m      C#m        D           E
Eu não vou mais deixar ninguém me fazer sofrer outra vez
       F#m   C#m            D         E
Posso amar você, mas não esqueci o que fez

( D  E  A  D )
( Bm  E  D  A )
( D  E  A  D )
( Bm  E  D  A )

A                       E      Bm                  F#m
  Mas você não tem que acreditar em tudo que acabei de dizer
  D                         A                            E
  Tem que consultar seu coração pra depois não se arrepender (se arrepender, se arrepender)
Bm                                            E
  O que mais quero de verdade hoje é ter você comigo

[Refrão]

                    A         E           Bm          E
Mas não vou mais deixar ninguém me fazer sofrer outra vez
       F#m   C#m            D         E
Posso amar você, mas não esqueci o que fez

                   F#m      C#m        D           E
Eu não vou mais deixar ninguém me fazer sofrer outra vez
       F#m   C#m            D         E
Posso amar você, mas não esqueci o que fez

( D  Dm  A )

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C# = X 4 6 6 6 4
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
