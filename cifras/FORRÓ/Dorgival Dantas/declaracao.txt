Dorgival Dantas - Declaração

Gm      F       C    Gm      F    C
Hoje lembrei de voce, queria te encontrar
Gm        F      C      F          C/E           Gm    Bb
Em pouco tempo talvez, falaria mil coisas que o meu coração, quer dizer,
F      C/E       Gm          Bb          F       C/E     Gm           Bb
Eu não posso guardar só pra mim, tire um tempo prometo voce vai gostar,
       D#        Bb    F
Cada frase que poder ouvir...

Gm     F          C  Gm         F      C
Será marcante pra nós, ninguém precisa saber
Gm        F      C
Em pouco tempo à sós
Bb C  F         C         Gm     Bb         F         C     Gm   Bb
 Saberá quanto vale uma declaração, que so fala de amor e paixão
     F         C           Gm    Bb      D#            Bb    F
Cada gesto por menor que eu te fizer, acredite vem do meu coração
   Gm            Bb     F
Acredite vem do meu coração...

(solo)


Gm     F          C  Gm         F      C
Será marcante pra nós, ninguém precisa saber
Gm        Dm      C
Em pouco tempo à sós...
Bb C  F         C         Gm     Bb         F         C     Gm   Bb
 Saberá quanto vale uma declaração, que so fala de amor e paixão
     F         C           Gm    Bb      D#            Bb    F
Qualquer gesto por menor que eu te fizer, acredite vem do meu coração
   Gm            Bb     F
Acredite vem do meu coração...

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
D# = X 6 5 3 4 3
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
