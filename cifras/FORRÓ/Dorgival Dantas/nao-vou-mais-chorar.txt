Dorgival Dantas - Não Vou Mais Chorar

A                   E                    Bm
Eu não vou mais chorar, não vou mais chorar
D                  A
Sofro até te esquecer
                   E                Bm
Mas eu não vou chorar, não mais chorar
D
Você só me fez sofrer

(A E Bm D)

  A       E     F#m D   A       E      F#m D
Amor vou te deixar,    mas não vou chorar
 A    E      F#m D    A     E       F#m D
Vai doer em mim,    sempre que lembrar

Bm                        F#m        E
Todos os momentos que eu tive com você
Bm                        F#m         E
Ficarão pra sempre dentro do meu coração
Bm                             F#m         E
Sei que não vai ser fácil pra mim te esquecer

Bm                         D
Mas, ficar chorando por você?
            A
Choro mais não!

A                   E                    Bm
Eu não vou mais chorar, não vou mais chorar
D                  A
Sofro até te esquecer
                   E                Bm
Mas eu não vou chorar, não mais chorar
D                  A
Você só me fez sofrer
A                   E                    Bm
Eu não vou mais chorar, não vou mais chorar
D                  A
Sofro até te esquecer
                   E                Bm
Mas eu não vou chorar, não mais chorar
D                  A
Você só me fez sofrer

(A E Bm D)

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
