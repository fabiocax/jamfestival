Dorgival Dantas - Por Que Amor?

[Intro] C  G  Am  F
        C  G  F

C                    G
Por que amor você insiste em evitar
    Bb                   A7
Não acredita que ainda sou
  Dm7               Em7
Apaixonado por você
     Fm
Não faz nenhum sentido
        F     Em     Am7  G
Você fugir de mim assim
    D
Se tudo que eu mais preciso
        G
É de você perto de mim
C
Olha amor
        G
Você poderia tentar

            Bb            A7
Me dar uma chance, por favor
           Dm7           Em7
Não vou fazer você chorar
    Fm
Só vou te dar carinho
          C    G    Am  G
Só quero te fazer feliz
 D
Ficar pertinho de você
        G
É tudo que eu sempre quis

        F
Por que te amo tanto
    G                        Em
Me sinto tão sem graça sem você
  Am                  D
Você passou a ser na minha vida
                G
A única razão para viver
           F
Não faz assim comigo
   G                          Em
Esquece tudo, volte e vem de vez
    Am                    D
Preciso de você na minha vida
                        G
Pra gente se amar mais uma vez
        F
Por que te amo tanto
    G                        Em
Me sinto tão sem graça sem você
  Am                  D
Você passou a ser na minha vida
                G
A única razão para viver
           F
Não faz assim comigo
   G                          Em
Esquece tudo, volte e vem de vez
    Am                    D
Preciso de você na minha vida
                            C
Pra gente se amar mais uma vez

[Solo] G  Am  F
       C  G  F

C
Olha amor
        G
Você poderia tentar
            Bb            A7
Me dar uma chance, por favor
           Dm7           Em7
Não vou fazer você chorar
    Fm
Eu vou te dar carinho
          C    G    Am  G
Só quero te fazer feliz
 D
Ficar pertinho de você
        G
É tudo que eu sempre quis

        F
Por que te amo tanto
    G                        Em
Me sinto tão sem graça sem você
  Am                  D
Você passou a ser na minha vida
                G
A única razão para viver
           F
Não faz assim comigo
   G                          Em
Esquece tudo, volte e vem de vez
    Am                    D
Preciso de você na minha vida
                        G
Pra gente se amar mais uma vez
        F
Por que te amo tanto
    G                        Em
Me sinto tão sem graça sem você
  Am                  D
Você passou a ser na minha vida
                G
A única razão para viver
           F
Não faz assim comigo
   G                          Em
Esquece tudo, volte e vem de vez
    Am                    D
Preciso de você na minha vida
                            C   F  Fm  C
Pra gente se amar mais uma vez

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D = X X 0 2 3 2
Dm7 = X 5 7 5 6 5
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
