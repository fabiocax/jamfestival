Dorgival Dantas - A Praia

 C  G Am          C    G  Am
Meu amor, não me deixe aqui
 C   G Am         C G  Am
Por favor, volte para mim
 Dm         C     G
Minha vida sem você
 Dm          C     G
Não terá nenhum valor

 C            G     Am
Nada mais me dá prazer
          Dm     C            G
Se não tiver de volta o seu amor
 C  G     Am          C  G  Am
Meu  coração te quer tanto bem
 C       G           Am
Diz que morre de paixão
          C   G   Am
Mas não quer ninguém

 Dm           C       G
Saiba que eu sempre quis

 Dm          C    G
Ficar perto de você
  C              G     Am
Que eu sonho em ser feliz
            Dm           C      G
E nos meus sonhos eu só vejo você

     C  G      Am
Na praia, nós dois
        Dm          C           G
Sob o claro das estrelas e da lua
    C  G    Am        Dm          C       G
A areia do mar é tão linda com você toda nua

[Final] C  G  Am
        C  G  Am

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
G = 3 2 0 0 0 3
