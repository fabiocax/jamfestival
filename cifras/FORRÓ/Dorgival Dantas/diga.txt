Dorgival Dantas - Diga

A    E                   F#m    D
Diga se lhe deixei faltar amor?
A    E                   F#m    D
Diga quantas vezes te fiz chorar?
A    E                   F#m    D
Diga o que foi que fiz com você?
             Bm          F#m
O que tem de errado em mim?
           E
Preciso saber
              Bm         F#m
Por favor não minta pra mim
              E
Tem que me dizer
A    E                 F#m    D
Diga o que eu tenho que fazer?
A    E                 F#m    D
Diga prometo que eu vou tentar
A    E                   F#m       D
Diga não tem mais pra quê esconder
             Bm         F#m          E
Mesmo que vá doer em mim preciso saber

             Bm          F#m              E
Se deixou de gostar de mim tem que me dizer
A         E
Oh meu amor
 Bm                           D
Eu não sei porque você fez isso comigo
A         E
Só sei dizer
 Bm                     D
Que você acabou de acabar comigo
A         E
Oh meu amor
 Bm                           D
Eu não sei porque você fez isso comigo
A          E
Só sei dizer
 Bm                     D
Que você acabou de acabar comigo
A                     Bm
Mas vou tentar te esquecer
                 D                 A
Vou procurar alguém que me faça feliz
                      Bm
Eu vou tentar te esquecer
                    D                 A
Vou em busca de alguém que me faça feliz

[Final] A  Bm  D
        A  Bm  D

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
