Dorgival Dantas - Chega de Mentiras

C          G          Am
Chega de mentiras por favor
                 F         C
Melhor abrir o jogo de uma vez
C              G          Am   F
Pra que ficar tentando me enganar?
C          G            Am
Assim você aumenta a minha dor
               F               C
Não contribui em nada pra nós dois
             G          Am   G
E acha que é melhor se separar.
F              G          Am  G
Desculpa se eu não te fiz feliz
F              G        Am  G
Me diz alguma coisa por favor
F               G        C
E se não era pra ficar aqui
C    G    Am   F   G   C
Por que levou o meu coração?


C    G    Am   F   G   C
E me deixou nesta solidão

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
