Os Nonatos - Até Sofrendo É Bom

G
Esta noite de Lua eu gostaria
Em
De pegar um cinema com você
Am                             C
Caminhar pela praia de mãos dadas
                               D
Em seu colo brincar feito um bebê

Refrão:

            G
Porque pra mim
                                   Em
A graça desse amor é dar o ar da graça
                                 Am
Na sala de aula, no carro ou na praça
                                  C
Sair do trabalho entrar num barzinho
               D
pra curtir um som

              G
Pra ficar junto
                                       Em
A gente dribla todos que estão de vigília
                                 Am
A falta de dinheiro, medo da família
             C D                   G
Estando com você...até sofrendo é bom

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
