Os Nonatos - Amiga Confidente

(intro) C Dm Em Dm Em Dm

C                                        G/B
Amiga, sempre que atendeste aos meus chamados.
        G                            C
Tu me amparaste com teu ombro e tua mão
          Am                    G
Jamais fechaste teus ouvidos aguçados
                                   C
Quando eu queria te abrir meu coração.
                                   G/B
Nunca estivemos tempo algum indiferentes
           G                          C
Nós somos hoje tão sinceros quanto antes
          Am                        G
Pois aprendemos que os amigos confidentes
          Em                             C
São mais fiéis que dois irmãos ou dois amantes.

C                                   G/B
Tu nada fazes sem contar primeiro a mim

        G                                C
Sabes porque da minha lágrima e meu sorriso
         Am                          G
Da mesma forma que aprendeste dizer sim
                                  C
Me dizes não se por acaso for preciso
                                   G/B
Mesmo sem tempo para os meus telefonemas
            G                      C
Tu não me deixas esperando pra depois
             Am                               G
Eu sempre encontro à solução pra os meus problemas
      Em                          C
Na terapia da conversa entre nós dois.

C                                    G/B
Quando eu viajo, tu não ficas sem saber.
         G                         C
Quando regresso, tu parece que advinhas.
          Am                              G
Quando estou triste, tu também demonstras ter.
                                        C
A mesma angústia das tristezas que são minhas.
                                G/B
Até na troca de presentes e cartões
        G                        C
O obrigado é mais feliz na tua voz
         Am                     G
Nossa amizade sem segundas intenções
          Em                          C
Resiste a tudo e é mais forte do que nós.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
