Rastapé - In My Life

In My Life - Rastapé

Intro: G Am 2x

G
Quando vejo o nascer do sol
                                          Am
lembro da primeira vez que ouvi o som da tua voz
      C                      D
tantas palavras que o sol se foi

G
Não me esqueço do sorriso
                                        Am
muito menos desse jeito lindo de me encarar
       C                        D
por favor me diga onde é que está


C                        D
Só voce para aquecer a alma

C                       G            D  2x
Só voce pra me acalmar, me acalma


G                                       Am
Oh my woman, I wanna have you in my life
G                                       Am      2x
Oh my woman, I wanna have you all the time


repete tudo

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
