Rastapé - Bicho do Mato

[Intro] Gm  Gm  F  Gm  C7

     Gm
Só foi o triângulo tilingar
                 C7
A zabumba bater
           D7       Gm
Para poeira levantar

        C7       Gm
É de noite sinhá
                   C7             Gm
Que é pra morena chamegar
          C7     Gm
É de noite sinhá
                   C7       Gm
Que é pra poeira levantar

Não tenho medo de macaco
Porque sou bicho do mato
                              C7
E se pegar meu bacamarte

         D7                Gm
Macacada tem vez não


Cobra criada cangaceiro
Bandoleiro
                       C7
Boto fogo no terreiro
              D7                Gm
E nas quebradas do sertão

        C7       Gm
É de noite sinhá
                   C7             Gm
Que é pra morena chamegar
          C7     Gm
É de noite sinhá
                   C7       Gm
Que é pra poeira levantar

Mais cabra macho
No fogão da Paraíba
                               C7
Quando surge uma intriga
              D7             Gm
Não tem medo de apanhar

    C7     Gm
É de noite sinhá
           C7         Gm
Que é pra morena chamegar

----------------- Acordes -----------------
C7 = X 3 2 3 1 X
D7 = X X 0 2 1 2
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
