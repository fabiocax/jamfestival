Rastapé - Menina

Introdução - (F# Bm G A D) 4 VEZES


D             F#
Passe o tempo q passar
         G                       A
a vida inteira quero estar com você
D             F#
Na areia na beira mar
           G                  A
seja onde for nos vamos brincar
Bm            A                Bm
E acende a fogueira pra comemorar
            G         A      Bm
o som da sanfona não pode faltar
           A                 Bm
e nesse zueira nós vamos cantar
              G           A     D
essa canção que me faz viajar
     G      A       Bm
Menina dos pés no chão

           G      A     Bm
Pele queimada com o verão
              G       A      Bm
Andando nas dunas do meu coração
         G       A      Bm
espero você em outro verão

1º Parte F# Bm G A D 1X

(Repete tudo desde o início)


2º Parte  F# Bm G A D
                F# Bm G A (Segue o Refrão)
Bm            A                Bm
E acende a fogueira pra comemorar...


Contribuição: Rafael Capitanio e Carlos Capitanio Jr.

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
