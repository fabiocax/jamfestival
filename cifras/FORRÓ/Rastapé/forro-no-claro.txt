Rastapé - Forró No Claro

 Intro (B Em)

  Em         B                 Em
   Nesse forró a gente tem vergonha
                    B                         Em
   de dançar agarradinho aqui tem muito lampião
        B                     Em
   aceso, aqui tem muito lampião
        B                     Em
   aceso, aqui tem muito lampião

  Em                               B
   Apaga o lampião, esconde o lampião
                                   Em         (2x)
   que a dança só é boa na escuridão

    Em        B               Em
   A mulherada tá pegando fogo
                     B
   tá que nem uma fogueira
                       Em
   pra queimar meu coração

       B                       Em
   acesa, pra queimar meu coração
       B                       Em
   acesa, pra queimar meu coração

             repete tudo

----------------- Acordes -----------------
B = X 2 4 4 4 2
Em = 0 2 2 0 0 0
