Rastapé - Amor de São João

      G               Bm
O meu amor, mentiu pra mim
        F                       E
É tão difícil ter que terminar assim
C              Am
Ofendido, magoado
C                             D7
Desilufido feito um cão abandonado
   G     Bm
Aiai, amor
         F                  E
Tenho certeza que você me abandonou
      C           Am
Tá doendo, tá difícil
C                                    D
Ver nossa história caminhar pro precipício

(refrão 2x)
G           D              C
Eu te vi na festa de São João
   D                        G
Dançando agarradinho sem parar

G           D                 C
Mas você insiste em dizer que não
     D                       G
Que não era você que estava lá

G        D       Em
Ai, Que posso fazer
            C
Se eu quero você
                 D
Você só quer mentir pra mim

G        D          Em
Ai, não dá pra entender
           C
por puro prazer
                D           G
Você maltrata o meu amor assim

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
