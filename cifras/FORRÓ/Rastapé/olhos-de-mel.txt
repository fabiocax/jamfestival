Rastapé - Olhos de Mel

D               A               Bm
Quando chega o verão a saudade aperta
   G    A    D
É hora de partir
            A                    Bm
Vendo sua imagem nesta estrada deserta
    G            A
Lembrando o seu nome
Em                A                 Em
E quando chega a noite morro de desgosto
             A                  Em
Vendo o seu rosto sempre a me seguir
                    A                 Em
Um dia desses por acaso te encontro à toa
        A                 Em          Bm
E numa boa vou voltar pra mim...então vem

D          A                Bm
Vem lua menina dos olhos de mel
          G   A  D
Dos olhinhos de mel

                 A
Quando a lua te mandar um beijo
              Bm
Todo o meu desejo
    G      A  D
Alcançou o céu

D        A                   Bm   G  A D
E vem do sol a luz que te ilumina
D                A                        Bm    G A D
Diz que canta a lua quando a noite se aproxima

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
