Desejo de Menina - Rolou Sentimento

F
Eu bem que te avisei
    C
Pra gente não brincar
Gm
Brincar de se entregar
Bb                                Dm   C
É muito perigoso, alguém pode chorar

F
Mas você se entregou
     C
Sem medo se jogou
Gm
Voou para os meus braços
Bb                   Dm   C7
Você brincou com o amor

F                     C
O que você quer me falar?
           Gm          Bb  C7
Se for de amor pode parar

F                     C           Gm
Você pra lá e eu pra cá, melhor assim
   Bb          C      F
E vê se não liga pra mim
F
Rolou sentimento é melhor parar
                           C
Senão alguém pode se machucar
          Gm
Não era pra deixar
     Bb       C        F
O coração se apaixonar   (2x)

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
