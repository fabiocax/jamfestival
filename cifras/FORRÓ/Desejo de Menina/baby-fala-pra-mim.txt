﻿Desejo de Menina - Baby, Fala Pra Mim

(capo 1ª casa)

(intro) Bb F Gm Eb

(ordem do dedilhado)
              A                E                F#m               D9
|----------------|----------------|----------------|----------------|
|------------3---|------------0---|------------3---|------------4---|
|------3--------3|------2--------2|------3--------3|------3--------3|
|---3-----3------|---3-----3------|---5-----5------|---5-----5------|
|1---------------|3---------------|5---------------|6---------------|
|----------------|----------------|----------------|----------------|


|----------------|
|------------4---|
|-----3---------3|
|--1-----1-------|
|1---------------|
|----------------|


(riff 1)
E|---------------|
B|--11--10--8--6-|
G|---------------|
D|---------------|
A|---------------|
E|---------------|

( A E   F#m   D9 )

                A                 E
Será que tudo que por nossa vida passa
             F#m             D9
tem motivos? E que só o coração
           A                  E
É quem conhece o verdadeiro amor?
        F#m                  D9
E as palavras tem poder ? Então

(refrão)
               A
Baby falá pra mim
  E     F#m      D9
Que... gosta de mim
               A
Baby fala pra mim
  E       F#m         D9
Que... o tempo não mudou
                 A
seu jeito de gostar
E   F#m         D9
E que    ainda gosta de mim

(a natureza muda e deixa ser composta (1-e-e-2-e-e-1-e-e-2-e-e)
e passa a ser simples (1-e-2-e-1-e-2-e) ou (1-e-e-e-2-e-e-e) )

é tocado um tempo por acorde; na parte simples, é dois tempos por acorde)

( A )
                A                 E
Será que tudo que por nossa vida passa
             F#m             D9
tem motivos? E que só o coração
           A                  E
É quem conhece o verdadeiro amor?
        F#m                  D9
E as palavras tem poder ? Então

(refrão)
               A
Baby falá pra mim
  E     F#m      D9
Que... gosta de mim
               A
Baby fala pra mim
  E       F#m         D9
Que... o tempo não mudou
                 A
seu jeito de gostar
E   F#m         D9
E que    ainda gosta de mim

( A  E   F#m   D9 )

                     A               E
Eu sei que um gesto vale mais mil palavras
             F#m            D9
Mas é sempre bom a gente ouvir
          A                    E
Um "Eu te amo"... Um "Adoro você"
           F#m                 D9
Por isso que  ro ouvir você dizer

(refrão)
               A
Baby falá pra mim
  E     F#m      D9
Que... gosta de mim
               A
Baby fala pra mim
  E       F#m         D9
Que... o tempo não mudou
                 A
seu jeito de gostar
E   F#m         D9
hum-um-um-um...

hum...

(refrão)

(riff - durante a música)
E|--------------------
B|--11--10--8--6------
G|--------------------
D|--------------------
A|--------------------
E|--------------------

----------------- Acordes -----------------
Capotraste na 1ª casa
A = X 0 2 2 2 0
Bb = X 1 3 3 3 1
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
Gm = 3 5 5 3 3 3
