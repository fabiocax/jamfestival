Desejo de Menina - Estórias

Introd.: A ( D  E  D  C#m  Bm  E  A ) 2x

E|--------------------------7-7------5-5-------4-4----------------  =
B|---------------------7--------5---------3---------2-------------    =
G|---------6-6-7-7-9-9---9--------7---------6---------4~--4-2-----      =- 2x
D|-7-7-9-9----------------------------------------------------4-7-      =-
A|----------------------------------------------------------------    =
E|----------------------------------------------------------------  = 

*Obs:
- Essa intro. é repetida varias vezes na musica.
- Acordes sublinhados são executados rápido.

                 E/G#         F#m                  F#m/E
Foram versos de amor foram cartas foram parques de nós
             D         F                    A         D  E  A
Foram marcas foi o coração quem pediu pra dizer adeus.
                 E/G#            F#m                 F#m/E
Foram sonhos de amor foram noites   eu no sonho a pensar que você
    D               F                         A
não vem nem me encontrar, nem me ouvir, nem dizer meu bem.


Refrão

A E/G# F#m                          C#m         D
          Foram tantos sonhos pra viver, que me custa entender
           A     A E/G# F#m
que hoje é nada.
                      C#m             D
Tudo aquilo que eu consigo ser não consegue esquecer
      A           B/A   E    A   ( D E D C#m Bm E A ) 2x
a namorada que me faz estremecer.

              E/G#           F#m               F#m/E
Foram juras e foram carinhos,   foram beijos e brigas sozinhos
   D              F               A          D E A
eu sei que não se pode evitar a palavra amor.
              E/G#           F#m                F#m/E
Sempre muito, sempre um pouco   assim são as historias,
          D           F                   A
são loucos caminhos estradas em formas de coração ... ( Refrão )


Solo:A D E D F#m 
E|--------------------------7-7------5-5~---5-7-7h9--
B|---------------------7--------5--------------------
G|---------6-6-7-7-9-9---9--------7------------------
D|-7-7-9-9-------------------------------------------
A|---------------------------------------------------
E|---------------------------------------------------

           Foram tantos sonhos pra viver, que me custa entender
           A    A E/G# F#m
que hoje é nada.
                     C#m             D
Tudo aquilo que eu consigo ser não consegue esquecer
      A           B/A   E    A   ( D E D C#m Bm E A ) 4x
a namorada que me faz estremecer.

----------------- Acordes -----------------
A = X 0 2 2 2 0
B/A = X 0 4 4 4 X
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
F#m/E = X X 2 2 2 2
