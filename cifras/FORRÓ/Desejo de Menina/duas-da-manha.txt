Desejo de Menina - Duas Da Manhã

F                      C
Alô, como é que você está?
             Am
Sei que são duas da manhã, tá tarde demais
F
Decidi ligar

F                                      C
Sei que está namorando, parece estar feliz
      Am
Meus parabéns, felicidades
F
É isso ai

C                                      G
Mas se algum dia esse cara te fizer chorar
G                                           Am
Pode me ligar qualquer hora e em qualquer lugar
Am                                  G
Pode ser a cobrar, não vou me importar


C                                               G
Se o destino fez assim com a gente eu não vou chorar
                            Am
Deu sabe o que faz, mas sei lá
                G                F
Deixe o tempo passar, mas antes de desligar
   Fm
Confesse

C                                    G
Você não me esqueceu e não adianta negar
                                     Am
Encontrei sua amiga e ela veio me contar
                                  F
Que falou com você, você falou de mim
                                       C
Disse que não é fácil esquecer o amor assim

                                         G
Por isso que eu liguei, liguei pra te avisar
                                          Am
Vou terminar o namoro e por você eu vou lutar
                                    F
Pode não me querer, pode não me aceitar
                                       C
Pode passar dez anos mas eu vou te esperar

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
