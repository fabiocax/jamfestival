Desejo de Menina - Baby (vem logo me ver)

 G        D/F#        Em             C
Baby a saudade só me faz pensar em você
           G        D/F#        Em              C
Essa é a minha realidade como está não dá pra viver
      Bm                 C
Quando penso em você eu sinto você em mim
   Bm                             C
Me dizendo que pra sempre vai me amar
   G        D/F#      Em            C
Baby meu desejo é viver só pra te amar
           G            D/F#         Em
O que eu sinto o que eu penso só me faz querer
        C
Te encontrar
     Bm                                C
Pra dizer que eu não quero mais viver de ilusão
       Bm                      C
Seu amor está guardado em meu coração
            G     D/F#    Em
Pra te esperar pois te amo
   C   G     D/F#     Em
Pra te dizer eu te amo

   C      G    D/F#    Em
Tudo em mim diz te amo
   C           D
Vem logo me ver

----------------- Acordes -----------------
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
