Desejo de Menina - Chuva No Meu Coração

Introd.: C F C F Dm G C F

     C        F          C                       F            Dm
Olho ao meu redor, tanta gente e eu me sinto tão só. No céu o sol,
         G       C      F         Dm            G       C    F
chuva no meu coração...  No céu o sol, chuva no meu coração...

     C        F          C                F
Como vou trabalhar, pensamento em outro lugar
         Dm            G       C    Bb C7
No céu o sol, chuva no meu coração...

F                                                         C
 E as flores do nosso jardim já não tem perfume e nem cor, não são mais
      F
belas. Dia e noite pra mim tanto faz, meu relógio parou em você,
Dm                   G
  Me traz de volta a paz.

C F  C F  Dm G C  G ... ( Acordes Arranjo )


      C         F         C           F              Dm
Eu te amo, só você é quem vai poder fazer, brilhar o sol,
          G       C     F          Dm            G       C     Bb C7
dentro do meu coração... Brilhar o sol dentro do meu coração...

F                                                         C
 Você é à flor do meu jardim, quando chega tudo ganha cor, isso é amor.
F
 Sinfonia é ouvir tua voz, tua presença perfuma meu ar
Dm                 G
  É bom demais te amar...

      C         F         C           F              Dm             G
Eu te amo, só você é quem vai poder fazer, brilhar o sol, dentro do meu
    Am               F               Dm            G       F
coração... Vou, iê, iê...  Brilhar o sol dentro do meu coração... Uh...
          Dm            G       C     F C F C
Brilhar o sol dentro do meu coração...

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
