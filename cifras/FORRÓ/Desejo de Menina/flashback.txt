Desejo de Menina - Flashback

G                             D
Ninguem consegue parar o pensamento
               C                  G
Vem mesmo no vento sem freio ou brack
            C            F C                       G
É por isso amor, que de vez em quando, rola um flashback
         C               F C                       G
É por isso amor, que de vez em quando, rola um flashback

G                                                    D
Essa paixão nao tem, hora lugar e vem, brincar de prazer
            C                G
E me traz voce, pareço um muleque
            C            F C                       G
É por isso amor, que de vez em quando, rola um flashback
         C               F C                       G
É por isso amor, que de vez em quando, rola um flashback

             D               C               G
Coisas do passado, parece errado, reviver denovo
         C             D                       G
Ha no coração, interrogação, como é que se esquece

            C                                       G
É por isso amor, que de vez em quando, rola um flashback
         C               F C
É por isso amor, que de vez em quando...

(solo)

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
