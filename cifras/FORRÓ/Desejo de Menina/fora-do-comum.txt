Desejo de Menina - Fora do Comum

Em               C
A nossa história é longa
              G
Mas vou resumir
                  D
Pergunta sem resposta
               Em
Te respondo sim

              C
Entrada sem saida
           G
Do meu coração
                   D
Depois de enfeitiçado
              Em
Nessa ilusão

            C
Eu sei que pra você
            G
É facil superar

             D
Pra outra pessoa
                  Em
Em qualquer lugar
             C
A tática perfeita
         G
Vou elaborar
           D
Em outra ocasião
            C
Agente se reencontrar
   D
Se amar...

Em             C
Você brincou com fogo
              G
E depois caiu fora
               D
Saiu da minha vida
                    Em
O que que eu faço agora?
                 C               G
Menina estou ao ponto de enlouquecer
          D
Fora do comum
              Em
Não posso te esquecer

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
