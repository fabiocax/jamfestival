Desejo de Menina - Fica Comigo

Intro: Dm Bb F C

      Dm       Bb
Sem você sofro tanto
        F   C
Meu coração já não aguenta mais
         Dm        Bb                       F
Penso em ti quero voar e acabar com essa distancia
          C
Que nos separa

        Bb      Gm                            F
Quero viver pra sempre junto a ti não importa nada
  C                          Bb
E esquecer o silencio que rouba minhas palavras
    C
Sem ti não importa nada

Dm         Bb       F       C
Quero você perto de mim (de mim)
 Dm            Bb     F   C                       Gm
Aceite o meu pedido e diz que vai aceitar ficar comigo

Bb              C
Eu não quero ser só seu amigo

          Dm               Bb             F
O que vou fazer se os seus olhos refletem mim
C
A luz da minha alma
         Dm        Bb                       F
Penso em ti quero voar e acabar com essa distancia
          C
Que nos separa

        Bb      Gm                            F
Quero viver pra sempre junto a ti não importa nada
  C                          Bb
E esquecer o silencio que rouba minhas palavras
    C
Sem ti não importa nada

Dm         Bb       F       C
Quero você perto de mim (de mim)
 Dm            Bb     F   C                       Gm
Aceite o meu pedido e diz que vai aceitar ficar comigo
Bb              C
Eu não quero ser só seu amigo

Eb            Bb           F
Não da pra evitar o que eu sinto (não minto)
Eb                Bb   Gm             A
Quando eu digo te quero e que eu te espero

Dm         Bb       F       C
Quero você perto de mim (de mim)
 Dm            Bb     F   C                       Gm
Aceite o meu pedido e diz que vai aceitar ficar comigo
Bb              C
Eu não quero ser só seu amigo

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
