Flávio José - Novilha Rainha

Gm           Cm
Por causa de você
      D7                      Gm
meu coração ta numa peinha de nada
                               D7
eu não esqueço mas aquela vaquejada
                              Gm      G7
a onde o nosso amor tão lindo terminou
         Cm          D7
nem percebi que um alguém
                      Gm
que estava correndo na pista
                                     D7
perdeu o boi mas, não perdeu você de vista
                               Gm    G7
ficou tentando conquistar o seu amor
           Cm
mas mesmo assim
      D7                       Gm
ainda vou correr atráz do seu amor
                          D7
eu desafio esse cara corredor

                             Gm      G7
que num olhar tentou tirar você de mim
       Cm
tudo farei
          D7                    Gm
pra ter você e me sentir um campeão
                              D7
você vai ser a minha abelha rainha
        Cm            D7          Gm
como troféu eu vou ganhar seu coração

----------------- Acordes -----------------
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
