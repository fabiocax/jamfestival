Flávio José - Saudade da Boa

[Intro] E7  Am  D7  G  C  F  B7  Em
        E7  Am  D7  G  C  F  B7  Em

Em                  B
  Quando penso em você
                     C
Meu olhar se enche d'água
                      D
Não tenho um pingo de mágoa
                G   E7
É só saudade da boa

    Am                      D7
Que fica na lembrança de um beijo
    G                         C
Do abraço que ninguém me deu igual
   F                   B7             Em   E7
Da noite que valeu por todas que eu vivi

  Am                        D7
O tempo foi passando e eu fiquei

    G                       C
Por todos os amores aonde andei
   F                           B7     Em
Tentei, mas nunca deu pra esquecer de ti

    B7          Em
Ai! ai! meu coração
                 B7                         Em
Passa dia, mês e ano, e não consigo te esquecer
    B7          Em
Ai! ai! meu coração
                 B7                    Em
Ainda bate apaixonado com saudade de você

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
