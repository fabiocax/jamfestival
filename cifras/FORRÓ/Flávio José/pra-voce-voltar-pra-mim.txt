Flávio José - Pra Você Voltar Pra Mim

[Intro] C G7 Am Fm C D7 G7 C G7

E|-----------------------------------|--------------------------|
B|------------5--5-5-------5------|--------------------------|
G|--------5-7--------7-5-7---5--|--5-4-------------5-----|
D|--5-5-5-------------------------|------8-7-6---6-8---5-|
A|----------------------------------|--------------------------|
E|----------------------------------|--------------------------|

E|---------------------------------------|
B|---------------------------------------|
G|------5---5-4---5---5-4------------|
D|--5-7---7-----5---------7-6-5-5--|
A|---------------------------------------|
E|---------------------------------------|

     C                      G#º                 Am
 Você hoje faz questão de dizer que já me esqueceu
       A                   A7                Dm
 Fala prá todo mundo que seu maior erro fui eu
         G7                                        C   Am
 Na verdade eu nem sei o que fiz prá você me deixar

       D7                                      G7
 Me desculpe chorar, mas é que sinto tanta saudade
          C                 G#º               Am
 Do seu corpo sobre a nossa cama juntinho ao meu
       A7                                        Dm
 Das carícias, dos beijos e abraços que você me deu
           G7                                    C   Am
 Você pode até não mais voltar, mas procure entender
     D7                       G                  C
 Que ninguém vai te amar como eu, isso você vai ver
     E7                                     Am
 As palavras que saem de mim vem do meu coração
     E7                                         Am
 São palavras sensatas, sinceras, não tem uma em vão
         F               G7                C  Am
 É uma pena você me escutar e não me entender
        D7                     G7            C        G7
 Quer ninguém vai te amar como eu isso você vai ver

   C             G        Am    A7
 Se amanhã você vier me procurar
           Dm         A            Dm
 E não me achar é porque já te esquecí
          E7                   Am         Fm      C
 Não vá pensar que eu nunca te amei, sabes que até chorei
       D7             G7
 Prá você voltar prá mim
 Se amanhã

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
G#º = 4 X 3 4 3 X
G7 = 3 5 3 4 3 3
