Flávio José - Minha Mãe

F                                          Dm
Eu sei que a estrada é longa e tudo tem um fim
                              Gm
Mas eu te peço Não chores por mim
                           C
Eu te prometo, voltarei um dia.

     Gm                              F
Eu vou/  Seguir a minha sina de bom cantador
                             Gm
Rezando a oração que você me ensinou
                                  C
No embalo da canção que já me fez dormir

   Gm                                   F
E aí / Nas estradas da vida por onde eu andar
                            Gm
Nos becos e avenidas do meu caminhar
                 C                     F F7
Pra sempre minha mãe eu vou lembrar de ti


Bb                C        Am               Dm
Mãe eu nunca esquecerei/  Teu carinho e tua fé
Gm                C         F                F7
Mas a vida é complicada/  E você sabe como é
Bb                    C
Vai-se um dia e vem o outro
Am                      Dm
A gente cresce, o mundo obriga
Gm                          C
Se eu pudesse, eu voltaria, mãe
Gm                     F
De novo pra sua barriga

F7          Bb   (Bb Am Gm)
Agente cria asas                        REFRÃO
Gm                                Am
Depois que vira um passarinho que voar
                                    Gm
As pernas pelo mundo e o coração no lar
                                F
Nos braços da saudade tenta ser feliz

F7           Bb  (Bb Am Gm)
Agente cria asas                         REFRÃO
Gm                                Am
Depois que vira um passarinho que voar
                                    Gm
As pernas pelo mundo e o coração no lar
                C                 F
Nos braços da saudade tenta ser feliz

Solo- F  Dm  Gm  C /  F  Dm  Gm  C  (Repete tudo outra vez)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
Gm = 3 5 5 3 3 3
