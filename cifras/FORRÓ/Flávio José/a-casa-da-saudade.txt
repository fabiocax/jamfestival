Flávio José - A Casa da Saudade

Intro: D F#m G D G D E7 A7
       D F#m G D Gm D A7 D A7

  D               F#m
A casa que vovô morou
             G                D
O meu pai herdou e passou pra mim (bis)

    G                  D
Mas para arranjar amor novo
          A7                D
Numa casa velha, aí é tão ruim (bis)

   D                   F#m
Pensei em me casar com ela
            G                  D
Mas na casa velha ela não quis não (bis)

G                    D
Era o que eu mais queria
           A7           D
Por isso doía o meu coração (bis)


   D                 F#m
Troquei a minha casa velha
          G                  D
Numa casa nova pra casar com ela (bis)

 G                 D
Aí eu fiz um bom negócio
               A7              D
Pois ganhei de volta o coração dela (bis)

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
