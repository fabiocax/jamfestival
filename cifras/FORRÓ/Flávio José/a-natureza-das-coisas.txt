Flávio José - A Natureza Das Coisas

[Intro] D  B7  Em  A7
        D  A7  D7  G
        F#m  Em  A7  D  A7

 D B7 Em         A7 D       A7
Ô    xalalalalá ô  xalalalalá
D B7 Em         A7      D         A7 D7 G F#m Em A7 D A7
Ô    xalalalalá ô coisa boa é namorar
         D
Se avexe não
                      B7             Em  B7
Amanhã pode acontecer tudo inclusive nada
         Em
Se avexe não
    A7                                    D    A7
A lagarta rasteja até o dia em que cria asa
         D
Se avexe não
                        B7             Em    B7
Que a burrinha da felicidade nunca se atrasa
         Em
Se avexe não

 A7                             D   D7
Amanhã ela para na porta da sua casa
         G
Se avexe não
     G#°                          F#m
Toda caminhada começa no primeiro passo
  Bm                                Em
A natureza não tem pressa segue seu compasso
 A7                    D  D7
Inexoravelmente chega lá
         G
Se avexe não
                            F#m
Observe quem vai subindo a ladeira
 Bm                       Em
Seja princesa ou seja lavadeira
    A7                        D
Pra ir mais alto vai ter que suar

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G#° = 4 X 3 4 3 X
