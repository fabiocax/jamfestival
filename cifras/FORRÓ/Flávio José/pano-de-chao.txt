Flávio José - Pano de Chão

Am
Quando o amor bateu na porta lá de casa

Meu desejo criou asa
                   E
e foi morar no coração

Foi tão gostoso parecia um mar de rosa

Eu de verso, tu de prosa
               Am   E
Eu amor, vc paixão
            Am
Mas, veio o tempo, companheiro do cansaço
                    A7
Foi causando um embaraço
                 Dm
Descompasso no viver
                                Am
Aí você que era tão meiga e carinhosa
                E
Foi ficando buliçosa

                     Am   A7
e pôs meu peito pra doer
          Dm
Aí você mudou
         E                     Am
Pisou na bola e fez mim pano de chão
                                  E
Pisou meu peito, machucando o coração

Pisou na minha vida
                        Am            E
Mas, não pisa em meu querer (2x)
   A                     D

Refrão 2x:
 Quando amor, for lá em casa
           A
erre o endereço
         D                A
Na minha casa não tem recomeço
         E                  A
Eu não mereço sofrer tanto assim

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
