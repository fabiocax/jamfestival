Flávio José - Me Diz, Amor

[Intro] Dm  Bb  C  D
        Dm  Bb  C  Dm

         Dm                    Bb
Me diz, amor, como é que vai ficar
C       F   A
Meu coração
         Dm                      Em7(b5)
Sem teu amor, sem teu calor, sem teu carinho
A      Dm
Na solidão

Dm                    Bb
Me diz, amor, como é que vai ficar
C       F   A
Meu coração
         Dm                      Em7(b5)
Sem teu amor, sem teu calor, sem teu carinho
A      Dm
Na solidão


         C                F
Só de pensar me dá um arrepio
             F7             Bb
Tudo é tão vazio sem você, amor
      Em7(b5)       A    Dm
Não imagino nem por um segundo
           Bb                A
Viver pelo mundo sem o teu calor

          C                  F
Quando me deito, quando me levanto
           F7                 Bb
Procuro no canto e você não está
        Em7(b5)      A     Dm
Não faça isso nem de brincadeira
                      Bb           A         Dm
Que a saudade é traiçoeira e vai matando devagar

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
Em7(b5) = X X 2 3 3 3
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
