Limão Com Mel - Baby vou mal

 INTRO: C#m  A  E  B  (4X)

  C#m              A
  Faz o que quer comigo
           E                       B
  Quero de novo teu carinho, teu calor
          C#m              A
  Sem teu beijo não faz sentido
          E                       B
  Corro perigo sem você, sem teu amor
  C#m                  A
  Diz que ainda me ama, que vai voltar
 E                 B
  Que nunca pensou em me deixar
  C#m                   A
  Que sentir minha falta não foi legal
E                        B
  Fala a verdade, abre o jogo
                   C#m           A
  Diz que sem meu amor (baby vou mal)
                   E                   B
  Sem sentir meu calor (baby, baby vou mal)

                   C#m           A
  Que quando me deixou (baby vou mal)
             E                     B
  Que vai voltar me querendo, implorando, dizendo:
         C#m             A
  Não me deixe (baby vou mal)
         E                     B
  Não me deixe (baby, baby vou mal)
         C#m             A
  Não me deixe (baby vou mal)
             E                     B
  Que vai voltar me querendo, implorando, dizendo:
             C#m                A
  Baby vou mal(faz o que quer comigo)
           E                       B
 (Quero de novo teu carinho, teu calor)
          C#m              A
  Sem teu beijo não faz sentido
          E                       B
  Corro perigo sem você, sem teu amor
  C#m                  A
  Diz que ainda me ama, que vai voltar
E                 B
  Que nunca pensou em me deixar
  C#m                    A
  Que sentir minha falta não foi legal
E                        B
  Fala a verdade, abre o jogo
                   C#m           A
  Diz que sem meu amor (baby vou mal)
                   E                   B
  Sem sentir meu calor (baby, baby vou mal)
                   C#m           A
  Que quando me deixou (baby vou mal)
             E                     B
  Que vai voltar me querendo, implorando, dizendo:
         C#m             A
  Não me deixe (baby vou mal)
         E                     B
  Não me deixe (baby, baby vou mal)
         C#m             A
  Não me deixe (baby vou mal)
             E                     B
  Que vai voltar me querendo, implorando, dizendo:
             C#m  A  E  B  C#m  A  E  B
  Baby vou mal

SOLO: C#m  A  E  B  (2X)

           C#m  A
 (Sem teu amor)
            E   B
 (Sem teu calor)
                   C#m  A
 (Que quando me deixou)
             E                     B
  Que vai voltar me querendo, implorando, dizendo:
         C#m             A
  Não me deixe (baby vou mal)
         E                     B
  Não me deixe (baby, baby vou mal)
         C#m             A
  Não me deixe (baby vou mal)
             E                     B
  Que vai voltar me querendo, implorando, dizendo:
           C#m
  Baby vou mal
A                  E                   B
  Sem sentir meu calor (baby, baby vou mal)
                   C#m           A
  Que quando me deixou (baby vou mal)
             E                     B
  Que vai voltar me querendo, implorando, dizendo:
         C#m             A
  Não me deixe (baby vou mal)
         E                     B
  Não me deixe (baby, baby vou mal)
         C#m             A
  Não me deixe (baby vou mal)
             E                     B
  Que vai voltar me querendo, implorando, dizendo:
           C#m B         A   B
  Baby vou mal (Baby vou mal)
           C#m B         A   B  C#m  B C#m
 (Sem meu amor, baby vou mal)

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
