Limão Com Mel - Homem da lua

E                                         B
Olha meu amor ha quanto tempo que eu estou aqui
       F#m           A           B
Vivendo na lua e você aí longe de mim
E                                       B
Olha meu amor mesmo distante vive no meu coração
             F#m        A           B
Nunca te esqueci tô fingindo de viver
    C#m                      G#m
Ah! se eu soubesse por onde você está
   A                       B
Em um foguete eu iria te buscar

REFRÃO
                E
Sou o homem da lua
                F#m
Vivo aqui na solidao
                  A
Longe do seu coração
    B     E    B
E dor no peito

                E
Sou o homem da lua
                 F#m
Minha vida é te amar
                      A
E só volto a terra um dia
B              E
Por teus beijos

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
