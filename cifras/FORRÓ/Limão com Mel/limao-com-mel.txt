Limão Com Mel - Limão Com Mel

D                    G
Quero você por inteiro
         D    G  A
Só pra mim
D                  G
Vem Amor, vem ligeiro,
          A
 me seduzir

Bm                         F#m
Não dá pra dividir com ninguém
G               A
E so meu o teu meu Amor
Bm                   F#m
Vai ser marcação cerrada
G                A
Aonde você for eu vou

Refrão:
G                           A
Quero você de janeiro a janeiro

D         A        Bm
Eu vou pegar no seu pé
G                 A
Quero você meu homem
            D       D7
Pra ser tua mulher

G                               A
Quero suas pernas em minhas pernas
D                       Bm
Quero sua boca em minha boca
G                       A
Ser teu violão tu dedilhando
             D       G   A
Me deixa louca

D                       G
Você tem direito sobre mim
             D         G  A
Pra fazer o que quizer
D                       G
Você abre todos os caminhos
          A
Pra me fazer mulher
Bm                        F#m
Jamais vou me cansar dessa paixão
               G                  A
Você é meu primeiro e derradeiro Amor

Bm                    F#m
Eu ando com seus pés me dê a mão
G               A       A7
Aonde você for eu vou

Refrão:
G                           A
Quero você de janeiro a janeiro
D         A        Bm
Eu vou pegar no seu pé
G                 A
Quero você meu homem
            D       D7
Pra ser tua mulher

G                               A
Quero suas pernas em minhas pernas
D                       Bm
Quero sua boca em minha boca
G                       A
Ser teu violão tu dedilhando
             D       G   A
Me deixa louca

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
