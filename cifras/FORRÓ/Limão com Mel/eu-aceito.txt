Limão Com Mel - Eu Aceito

Intro: C#m A B G#m7 C#m A B G#m

E
Ouço uma cançao que me lembra voce
B
Ando pela ruas pareço te ver
A
Em cada rosto vejo
B
Um sorriso seu
E
Procurei quem ocupasse o seu lugar
B
E mais forte que eu tentei disfarçar
A                     B
Mas sem voçe nao dar nao aceito
F#m        E        B
Sei que o erro foi meu
F#m          E                B
Mas me der outra chance pelo amor de Deus
     E                B                C#m
So liguei pra agente por as coisas no lugar

      E                       A
To precisndo urgentemente conversar
     E                       F#m
Nao tem sentido eu perdida desse geito
     E            B                  C#m
Diz pra mim que voce tambem ira me procurar
                B                A
Que me ama me adora e que quer ficar
B                                  A
Meu mundo sem teu mundo ja nao e perfeito
B            A
So fala eu aceito

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
G#m7 = 4 X 4 4 4 X
