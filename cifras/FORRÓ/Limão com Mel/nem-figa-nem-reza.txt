Limão Com Mel - Nem Figa, Nem Reza

[Intro] C9  F

 C9                                    F
Eu desejo que ninguém te chame de rainha
                                         Am
Que a boca que você beijar depois da minha
        G                   F
Eu desejo que não te dê prazer

Am   G                           F
Eu espero que você encontre um norte
            G                     Dm
Mas que no amor você não tenha sorte
        Em                        F
E se fizer amor com outro mesmo assim
           G                            C9
Que faça amor com ele, mas pensando em mim

                                      F
Pode falar que praga de ex amor não pega
                                 Am
Cuidado pra seu coração não vacilar

      G       F
Faz figa e reza
                                      C9
Pra quando der saudade a praga não pegar

                                       F
Pode falar que praga de ex amor não pega
                                        Am
Mas sei que qualquer hora vai se descuidar
       G         F
Nem figa, nem reza
                                  C9 ( F  Am  F )
Vai impedir você de vir me procurar

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C9 = X 3 5 5 3 3
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
