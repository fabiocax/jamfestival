Limão Com Mel - Espetáculo

Intro: C G Am G Em F  Am G F (3X)

Am                                    Dm   E
Vi na cena um roteiro o coração apaixonado, eu não consigo mais
                Am
tirar você do pensamento.  uôô uôô
                                     Dm  E
O picadeiro tá tomado, o espetáculo tá demais, eu já não sei
                Am
viver sem o teu amor.
      Dm                           Am                  C
Por você eu faço tudo, pra te ver feliz. Sou seu palhaço, sou
       G                       Am
malabarista, sou mágico, sou ator.
   Dm                         Am
As palmas são seu prazer, dou um show só pra você.
     F       Em         Dm                F
Represento no palco uma história de amor feita só pra mim e
   E
você.
Refrão -------------   C                      G       Am       F
Preste bem atenção ao espetáculo do meu coração o

 C          G
público é você,
            Dm         G
como vale a pena ser sua paixão. (2x)
Am         G       F
  uo uo uo, uo uo uo oo, o nosso amor virou um espetáculo
Am         G       F
  uo uo uo, uo uo uo oo

de um coração apaixonado.--------------------  (2x) 

Am G F (2x)

Solo: Am G F (4x)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
