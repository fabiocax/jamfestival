Limão Com Mel - Ultima Lua

Dm                    C
Troque sussurros no ouvido
             Dm
E chuva de carícia
    C                Bb
E aquele beijo que excita
             C               Dm
Fração de segundo pra amor chegar
      C               Bb   C
E a gente não querer parar
Dm                C
Era nossa ultima lua
                Bb
E a gente não prévia
C                    Bb
Que ao amanhecer do dia
          C              Bb
Aquela mensagem no meu celular
         C          Bb
Ia fazer a gente terminar
C
Se eu soubesse

Bb            C
Que ontem a noite era
          Dm
Era nossa última vez
       C             Bb
Teria feito amor outra vez
       C              Dm
Teria feito amor outra vez

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
