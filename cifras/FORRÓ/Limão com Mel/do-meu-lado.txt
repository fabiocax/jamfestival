Limão Com Mel - Do Meu Lado

  E E4                                        C#m
Cadê você? Teu perfume? Teu sorriso? Aonde está você?
                                  F#m                                       B B4
E esse amor que eu preciso pra poder viver? Sem você eu sinto falta do seu beijo,

Tô morrendo de desejo.
    E E4                                       C#m
Me faz sofrer sem você perto de mim, Eu não posso viver.
                                F#m                                         B B4
No meu quarto eu convivo com a solidão, Te procuro e só te encontro no meu coração.

Preparação p/refrão

         C#m
Por que você foi embora?
         G#m
Por que você me deixou,
      A                          B
Terminando nossos sonhos de uma vez?
         C#m
Foi tão linda a nossa história,

         G#m
Foi tão lindo o nosso amor,
       A                 B
Por você faria tudo outra vez.

REFRÃO
        E                        C#m
DOI DEMAIS DORMIR E ACORDAR SEM TER VOCÊ
        A            B
DO MEU LADO, DO MEU LADO.
        E                          C#m
MAS NO PEITO ME ALUSCINA, FAZ MEU CORAÇÃO DOER
        F#m                   B
QUANDO LEMBRO DE VOCÊ DO MEU LADO.                   (2x)

Solo: E C#m G#m A B (2x)  Volta p/preparação do refrão....


----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
B4 = X 2 4 4 5 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
E4 = 0 2 2 2 0 0
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
