Limão Com Mel - Esse Amor é 1000

C
Nosso caso de amor nunca foi ilusão, sou louco por você
                   G            Dm
Te entreguei minha vida,diz pra mim agora o que que
                F                      C
Eu vou fazer se não tiver você no meu caminho, me
       G                Dm                  F
Sinto sozinho, to tão sozinho, tão sozinho
C
Tantas cartas de amor escrevi pra você, foram tantas
                    G          Dm
Loucuras de um só coração, já tentei mas não consigo nem
           F                         C
Imaginar viver sem te amar não vale apena não vale
 G             Am
Apena ai ai ai ai, coração bate mais forte chega
     Em                                        F
Machucar solidão bateu na porta mas não vou deixar
                C    G/B  Am
Vou ficar com você iê iê iê
                                   Em
Eu já decidi não vou deixar você fugir, chega de

                          F
Viver sozinho andando por aiiii
       G                 G#          A#
Eu não quero mais sofrer, longe de você

Refrão 2x:

                     C             G
Porque esse amor é 1000, eita paixão
                  F            Dm            C
Eu jogo tudo de lado to apaixonado adeus solidão
                    C               F
Porque esse amor é 1000 e ninguem segura
     C        Dm    G       C
É desejo é paixão eita loucura

----------------- Acordes -----------------
A# = X 1 3 3 3 1
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
G/B = X 2 0 0 3 3
