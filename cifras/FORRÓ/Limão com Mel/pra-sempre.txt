Limão Com Mel - Pra sempre

(intro)

E |--------------3-3--3-5-5/7-3-7p5-5/3--3-5-7-8--10-10--8-7--3---
B |-------3--3h5------------------------------------------------5-
G |---------------------------------------------------------------
D |-0h2-5---------------------------------------------------------

E |-3-5-5/7-10-5/7-10-5/7--5p3-5--7-5---|
B |-----------------------------------3-|
G |-------------------------------------|
D |-------------------------------------|

  G                                  C9
Senti no peito o amor surgir, quando olhei pra você
                   Am                                G
Eu logo senti que o meu coração ia ser todo seu pra sempre

E |-3-2-3-5--7--8--10--10/12-10--|
B |-3----------------------------|
G |-0----------------------------|
D |------------------------------|


     G                           C9
Meu corpo sente a falta do seu e isso é bem mais,
                    Am                                   G
Mais forte que eu e tudo me faz lembrar de você cada instante

              Am7
E |----7---5p3-5-----|
B |-5/7--------------|
G |------------------|
D |------------------|

Am         Bm          C            G
Sinto teu perfume e a saudade entre nós

E |---7-5-3-2--3-2h3p2--3--|
B |-5---------------------5|
G |------------------------|
D |------------------------|

Em          Am        A/C#                C
Lembro teu sorriso e som da tua voz ( tua voz )

E |---8-7-5-7--8/10-10/12-12|
B |-5-----------------------|
G |-5-----------------------|
D |-5-----------------------|

G                                  C9
Nosso amor não dá pra esquecer eu já me entreguei
                Am                                 G
Já me apaixonei e tudo que sei é que meu coração reclama

E |-7p5---5p3--2-3-3/5---|
B |----------------------|
G |----------------------|
D |----------------------|

    G                          C9
Meu corpo sente a falta do seu e isso é bem mais
                    Am                                  G
Mais forte que eu e tudo me faz lembrar de você cada instante

                  Am
E |-3--5--7--5--3/5------|
B |-3--5--7--5--3/5------|
G |----------------------|
D |----------------------|

Am           Bm         C9         G
Sinto teu perfume e a saudade entre nos

E |-5h7--5--3/5------|
B |-5h7--5--3/5--3---|
G |------------------|
D |----------------7-|

Em          Am          A/C#                    C     D
Lembro teu sorriso e o som da tua voz ( tua voz )

E |-3p2---2-3-5-7-7/8-10-10-|
B |-----5-------------------|
G |-------------------------|
D |-------------------------|

G                                      C9
Senti no peito o amor surgir, quando olhei pra você
                  Am7                                 G
Eu logo senti que o meu coração ia ser todo seu pra sempre,
      G
pra sempre

E |--------------3-3--3-5-5/7--7p5-5/3--3-5-7-8--10-10--8-7--3---
B |-------3--3h5-----------------------------------------------5-
G |--------------------------------------------------------------
D |-0h2-5--------------------------------------------------------

E |-3-5-5/7-10-5/7-10-5/7--5--7--5--3-5-|
B |------------------------5--7--5--3-5-|
G |-------------------------------------|
D |-------------------------------------|

Am           Bm         C9            G
Sinto teu perfume e a saudade entre nos

E |-5h7--5--3/5------|
B |-5h7--5--3/5--3---|
G |------------------|
D |----------------7-|

Em          Am          A/C#                C
Lembro teu sorriso e o som da tua voz ( tua voz )

E |-3p2---2-3-5-7-7/8-10-10-|
B |-----5-------------------|
G |-------------------------|
D |-------------------------|

G
Nosso amor não dá pra esquecer
    C9
Eu já me entreguei já me apaixonei
   Am                              G
E tudo que sei é que meu coração reclama

G             C9     Am   G
Seu amor, Seu amor, Seu amor
G             C4     Am   G
Seu amor, Seu amor, Seu amor

E |-----8-----------------------------5b¹--3-5/7--|
B |-3/5----------------------------3--------------|
G |--------2b½-----------------2-4----------------|
D |------------5--0h2-5-0h2-5---------------------|


----------------- Acordes -----------------
A/C# = X 4 X 2 5 5
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C4 = X 3 3 0 1 X
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
