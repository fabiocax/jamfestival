Limão Com Mel - Incertezas

Intro: C G Dm F C G (2x)
      (C G Dm Fm C G)

C
Saudade ta me machucando não estou mais
G                          Dm
agüentando sem você aqui, eu sinto tanto
          F              C                     G
a sua falta não machuca volta vem viver pra mim.
C                                       G
Não vou vivendo essa tristeza com essa incerteza
                 Dm                    F
se você me quer, pra você dou a minha vida, quero
    C                         G
ser teu homem e te fazer mulher.

E                                 Am
Tenta entender eu quero ter você de vez aqui no
         D7                    Dm
coração, te fazer feliz é que eu sempre quis,
F                   G
por favor não diga não.

E                                    Am
Se você deixar, eu posso te provar, o quanto é
           F      C              G            C        G
grande meu amor, eu te amo aconteça o que for.

(Refrão)
                 C               G
Eu vou te encontrar, e me entregar e do teu
Dm                 F    C                   G
corpo inteiro provar, e te dizer que sem você,
Dm                   F
não consigo mais viver.
                 C               G
Eu vou te encontrar, e me entregar e do teu
Dm                 F    C                   G
corpo inteiro provar, e te dizer que sem você,
Dm                   F          G     C
não consigo mais viver sem o teu amor.

E
Tenta entender...

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
