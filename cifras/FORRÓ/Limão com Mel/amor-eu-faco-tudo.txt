Limão Com Mel - Amor Eu Faço Tudo

E               C#m
Chega de fingir que não me quer
       E            B
Não vá enganar seu coração
      A      F#m     B     E
 Não vá, não vá, não vá... Vem
    B               C#m
 depressa logo me dizer
        B
Que me ama
             A        E        B
Não pode esconder Não vá, não vá
A
Não faça loucura
             E
Pense bem direito pense nesse amor
               B
dentro do seu peito
A                       E
Não queira fazer tudo errado
                         B
Eu te quero aqui do meu lado

                A
Eu preciso de você
A                         E
Eu sei que você está apaixonada
                        B
Só que está dando uma mancada
                        B
Não vá trair seu próprio coração
               E
 Amor eu faço tudo Tudo
         B
Tudo por você
                 A
Te amar te dar prazer
       E         B
Eu te amo de verdade
               E
 Amor eu faço tudo
G#             C#m
Tudo pra você mudar
B               A
O seu modo de pensar
B                 C#m
Quebra esse seu orgulho
B         E
E vem me amar

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G# = 4 3 1 1 1 4
