Limão Com Mel - Incondicionalmente

Intro: C  Em  Am  F  G#  A#  G

C    Em             Am
Eu sinto a tua falta
C    Em             Am
Está me machucando
F                             Dm
Lembranças de nós dois
             F                 G
Só me fazem chorar
          C
Me envolvi demais
             Em               Am
Fui um bobo ao imaginar
                 Em                  F
Que esse amor era pra sempre
            Dm
Incondicionalmente
                  C                     Em              Am
Mais deixou meu coração, numa estrada abandonada
Em            F                                  Dm G
Uma estrela apagada entre nuvens na escuridão

   Dm                                Am
Estou sofrendo, morrendo por dentro
                  F           G                    C   G
Mais vou ter que aceitar, o fim da nossa história.

Refrão:
                   C                    G
Tudo na vida passa, como mágica
Am             Em
Não quero mais chorar
            F              Dm           G   G# G
É coisa de momento, sentimento um dia acaba
                    C                  G
Vou mudar o meu destino , pra melhor
   Am                Em
Vou ser feliz em outro lugar
         F#              A#                 C
Encontrar um coração que saiba amar

Intro: C  A#  G#  A#  C  G#  A#  G#  G

Refrão:
                   C                    G
Tudo na vida passa, como mágica
Am             Em
Não quero mais chorar
            F              Dm           G   G# G
É coisa de momento, sentimento um dia acaba
                    C                  G
Vou mudar o meu destino , pra melhor
   Am                Em
Vou ser feliz em outro lugar
         F#              A#                 C
Encontrar um coração que saiba amar
                   C                    G
Tudo na vida passa, como mágica
Am             Em
Não quero mais chorar
            F              Dm           G   G# G
É coisa de momento, sentimento um dia acaba
                    C                  G
Vou mudar o meu destino , pra melhor
   Am                Em
Vou ser feliz em outro lugar
         F#       A#         C
Encontrar um coração que saiba amar
         F#       A#            C
Encontrar um coração que saiba amar
         F#       A             C
Encontrar um coração que saiba amar
       Em           C
Eu sinto tua falta...

----------------- Acordes -----------------
A = X 0 2 2 2 0
A# = X 1 3 3 3 1
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
