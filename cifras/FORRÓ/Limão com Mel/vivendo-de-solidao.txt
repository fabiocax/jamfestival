Limão Com Mel - Vivendo de solidão

[Intro] Am  F  C  E  E7
        Am  F  C  E  E7

Am                                  F
Preso num apartamento vivendo de solidão
G
Eu me encontro aqui mais uma vez me enchendo
  E
De inspiração
Am
Então eu desabafo as magoas e as tristezas
  F          C                      E
Nesse violão, parece que ele chora, com pena
        E7
Desse coração

Am
O tempo passa o dia inteiro fico a te
F             G
Imaginar com tanta gente ao meu redor
          E
Mais só você não está

Am
Eu olho pra janela vejo a noite, sinto medo,
  F                    G
Frio a me envolver e me pego a chorar por
                  E          E7
Por perceber que não tenho você

[Refrão]

   Am             F                 C
Aqui perto de mim, me sinto tão só
        Dm7            E    Am
Sem teu carinho pra me aquecer
            F   E        Am
Tento esconder de mim a solidão
            F          C
Mas eu não consigo tirar você
   Dm7  E   Am
Do meu coração

[Solo] Am  F  C  E  E7

Am
E o medo que um dia você possa
              F                G
Me dizer que não me ama mais e toda

Aquela nossa historia de amor
  E
Você jogar pra trás
Am
Aumenta cada dia essa distancia
       F      C
Entre nós, eu pego o telefone
   E                      E7
Ao menos posso ouvir sua voz

Am
O verdadeiro amor, o tempo e a distancia
                F        G
Nada pode separar, sou como sol você a lua
        E
É tão difícil de se encontrar

Am
Fico desesperado pois não sei o que
          F      G
Eu vou fazer só não quero ter que imaginar
               E            E7
Que um dia eu não vou ter você

[Refrão]

   Am             F                 C
Aqui perto de mim, me sinto tão só
        Dm7            E    Am
Sem teu carinho pra me aquecer
            F   E        Am
Tento esconder de mim a solidão
            F          C
Mas eu não consigo tirar você
   Dm7  E   Am
Do meu coração

 F               E
Só queria ter você

 Am             F                 C
Aqui perto de mim, me sinto tão só
        Dm7              E    Am
Sem teu carinho pra me aquecer
            F   E        Am
Tento esconder de mim a solidão
            F          C
Mas eu não consigo tirar você
   Dm7  E   Am
Do meu coração

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm7 = X 5 7 5 6 5
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
