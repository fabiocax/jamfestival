Limão Com Mel - Aflição

 G          D    Em       Bm
Mais uma canção, uma inspiração
C           G         A           D
nasce dessa sensação que você me dá
G           D  Em       Bm
Cada vez maior essa aflição
C        G       A          D
analise bem você veja como dói
         C     G   D        Em
Eu sou aquela luz que se apagou
         C     G   D        Em
aquele vento forte que não levou
    C        G         A        G     D
a poeira do chão e as nuvens do céu
G                D      Em         Bm
Sei que não tem preço é grande seu valor
C              G            A          D
pois na vida agente sempre joga pra vencer
G          D  Em         Bm
Novamente vou a te procurar
C              G        A           D
pois meu rumo sem você não tem direção

          C     G   D        Em
Eu sou aquela luz que se apagou
         C     G   D        Em
aquele vento forte que não levou
    C        G         A        G     D
a poeira do chão e as nuvens do céu

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
