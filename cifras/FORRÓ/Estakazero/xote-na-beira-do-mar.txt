Estakazero - Xote na beira do mar


F#m                   E
O abrir do seu sorriso
                      F#m
Eu comparo areia branca
                     E
E o gingado desse xote
                   F#m
Com o balanço do mar
                     E
vejo um corpo delicado
                       F#m
E um rostinho de criança
                      E
Balançando suas tranças
                     F#m
Me chamando pra dançar
                   C#m
Nos braços da sereia
                    D
sob a luz da lua cheia

                E
Dançando na areia
                      F#m
Quero um beijo defrutar

     C#m               D
Eê esse olhar de querubina
    E                   F#m
Eê um xote na beira do mar
  C#m                       D
Eê vou dançar com essa menina
 E                         F#m
Eê no seu suor vou me banhar

F#m                      E
Com o corpo bem juntinho
                        F#m
vem um cheiro no cangote
                   E
E arrastado vem xote
                   F#m
Por debaixo do luar
                      E
Meu coração vai batendo
                          F#m
Vai batendo a propria sorte
                         E                     F#m
E a brisa sopra mais forte pra esse calor cessar

----------------- Acordes -----------------
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
