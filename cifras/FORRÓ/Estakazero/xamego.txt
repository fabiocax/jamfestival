Estakazero - Xamego

O xamego da prazer, o xamego faz sofrer
               G7                 C
O xamego as vezes doi, as vezes nao
               G7            C
O xamego as vezes roi o coracao
               G                 C
Todo mundo quer saber o que è o xamego
                       G7                    C
Ninguem sabe se ele e branco, se e mulato ou negro       (2x)

                        G               C
Quem nao sabe o que e xamego pede pra vovo
                G7                 C
Que ja tem 70 anos e ainda quer xodo
                 G7                   C
E reclama noite e dia por viver tao so     (2x)

                      G                     C
Ai que xodo, ai que xamego, ai que chorinho bom
               G7                 C
Toca mais um bocadinho sem sair do tom

                 G7                   C
Meu cumpade chega aqui, ai que xamego bom
               G7                C
Ai que xamego bom, ai que xamego bom

----------------- Acordes -----------------
C = X 3 2 0 1 0
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
