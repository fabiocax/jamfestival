Estakazero - Acorda

(intro) E B F#m B C#m G#m F#m B E

E     B     F#m     B      C#m B F#m B  F#m B (tem que ser rápido)
Dere rerere derere rere rere  re  re re
E        B          F#m        B
Esperei o tempo todo  pra falar com você
E            B               F#m              B
Que a melhor coisa do mundo está do seu lado e você não ver
E              B          F#m             B
E eu me lembro cada noite dançamos juntos no forró

E             B             F#m          B
Nossos corpos tão juntinhos e o coração alí tão só
F#m          B       C#m   B  F#m  B F#m (tem que ser rápido)
Quem você quer nem te viu chegar
F#m          B          C#m       G#m
Ta todo mundo olhando a gente dançar
F#m
Passa a noite num segundo
B
O amor é um sono profundo


(refrão)
E           B
Então acorda
F#m                 B
Que a festa já acabou
     E       B
Então acorda
F#m               B
Que o dia amanheceu tão lindo
E      B
Acorda
F#m                  B
Que a realidade sou eu
      E      B
Então acorda
F#m             B                E
A corda que nos prendeu se chama destino

(intro)  E B F#m B C#m G#m F#m B E

E     B     F#m     B      C#m B F#m B  F#m B (tem que ser rápido)
Dere rerere derere rerere  re  re re

----------------- Acordes -----------------
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
