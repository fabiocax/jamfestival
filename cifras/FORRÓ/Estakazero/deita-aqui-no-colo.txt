Estakazero - Deita Aqui No Colo

Intro: F#m  D  Bm  C#

F#m
Quando te vejo
       D               Bm      C#
Meu coração bate de desejo
F#m                  D
Queria te roubar um beijo
                Bm         C#
Pra gente se apaixonar

F#m                 D           Bm       C#
Fico querendo me jogar nos teus braços
F#m                   D
Me envolver nessa paixão
             Bm         C#
Pra  gente se amar

F#m         D                Bm
Vem com jeitinho  vem de mansinho
             C#                  F#m
Que eu devagarinho  vou te dar carinho


F#m         D             Bm
Vem me abraçar  vou te ninar
               C#              F#m
Deita aqui no colo   quero te amar

----------------- Acordes -----------------
Bm = X 2 4 4 3 2
C# = X 4 6 6 6 4
D = X X 0 2 3 2
F#m = 2 4 4 2 2 2
