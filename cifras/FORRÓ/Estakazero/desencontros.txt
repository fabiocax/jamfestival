Estakazero - Desencontros

Intro:
B
Lá, lá, lá, lá, lá, lá ,lá
D#m
Lá, lá, lá, lá, lá, lá ,lá
E
Lá, lá, lá, lá, lá, lá ,lá
F#
Lá, lá, lá, lá, lá, lá ,lá

B               D#m
Se agente não se encontra
        E               F#
Onde está você? Onde está você?
G#m                 D#m
Quanto tempo, tanto tempo
              E   F#
longe sem te ver

B               D#m
Se agente não se encontra

        E               F#/A#
Onde está você? Onde está você?
G#m                 D#m
Quanto tempo, tanto tempo
              E   F#
longe sem te ver

B            D#m
Você tem a sua rotina
G#m                  D#m
E é nela onde eu queria estar
G#m              D#m
Isso tudo não me desanima
      E                    F#
Mas quanto tempo tenho que esperar?

B            D#m
Você tem a sua rotina
G#m                  D#m
E é nela onde eu queria estar
G#m              D#m
Isso tudo não me desanima
      E                    F#
Mas quanto tempo tenho que esperar?

B             F#
Foram tantos desencontros
B                       F#
E nos encontros não deu pra mostrar
B                  F#         (F# F E)
Tudo aquilo que você merece
E                         G#m
Tudo aquilo que eu tenho pra te dar
E           Fº          B      F#
Então eu vou esperar A cantar...

B
Lá, lá, lá, lá, lá, lá ,lá
D#m
Lá, lá, lá, lá, lá, lá ,lá
E
Lá, lá, lá, lá, lá, lá ,lá
F#
Lá, lá, lá, lá, lá, lá ,lá

----------------- Acordes -----------------
B = X 2 4 4 4 2
D#m = X X 1 3 4 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
F#/A# = 6 X 4 6 7 X
Fº = X X 3 4 3 4
G#m = 4 6 6 4 4 4
