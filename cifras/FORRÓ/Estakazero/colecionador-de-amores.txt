Estakazero - Colecionador de Amores

Am                F
Eu me apaixono todo dia
                  C
Eu me apaixono toda hora
               G
Não quero compromisso
                       Am
Pego um beijo e vou me embora  (bis)

Solo: (Am F C G) 2x

Am
Hoje é o seu dia de sorte aproveita
  F          G        Am
O que você quiser vai rolar
Am
Hoje é o seu dia de sorte aproveita
    F          G       Am
Amanhã tem outra no seu lugar

    G              Am
Sou colecionador de amores

   G                 Am
Se me quiser vai ser assim
      G                  Am
Posso ser seu príncipe encantado
    F
Por uma noite apenas
 G             Am
Posso te fazer feliz

Am                F
Eu me apaixono todo dia
                  C
Eu me apaixono toda hora
               G
Não quero compromisso
                       Am
Pego um beijo e vou me embora  (bis)

Solo: (Am F C G) 2x

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
