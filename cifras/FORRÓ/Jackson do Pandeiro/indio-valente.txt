Jackson do Pandeiro - Índio Valente

G#                 C#       G#/D#
Chegou a tribo de índio valente
C#              G#
De índio antropófago
C#/F         G#
Que come a gente

G#
Não vim aqui pra brincar
C#              G#/D#
Vim cumprir meu dever
G#          C#
O cacique mandou
         D#
A gente comer
           G#
Essa gente comer

----------------- Acordes -----------------
C# = X 4 6 6 6 4
C#/F = X 8 X 6 9 9
D# = X 6 5 3 4 3
G# = 4 3 1 1 1 4
G#/D# = X 6 6 5 4 4
