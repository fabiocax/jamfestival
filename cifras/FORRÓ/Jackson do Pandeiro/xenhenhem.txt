Jackson do Pandeiro - Xenhenhém

 G#                     D#7/Bb
A dança do momento é o xenhennhém
 D#7                    G#
Agora só se fala em xenhennhém
               F/A     F7/A   Bbm7
Quem dança uma vezinha arranja um bem
 D#7                     G#
E nunca mais esquece o xenhennhém

refrão

(G# , D#7)Bbm7,D#7,G#
xenhennhém xenhennhém xenhennhém xenhennhém ennhém nhennhém

 G#                    D#7/Bb
no clube só se ouve o xenhennhém
 D#7                    G#
porque é uma brasa o xenhennhém
              F/A    F7/A    Bbm7
mulher de mini-saia é o que mais tem
 D#7                       G#
dançando e cantando o  xenhennhém


refrão


         G#               D#7/Bb
eu canto a noite inteira o xenhennhém
           D#7          G#
sou fã número um do xenhennhém
            F/A   F7/A  Bbm7
escolho a mulher que me convém
           D#7         G#
e vou me acabar no xenhennhém

----------------- Acordes -----------------
Bbm7 = X 1 3 1 2 1
D#7 = X 6 5 6 4 X
D#7/Bb = 6 X 5 6 4 X
F/A = 5 X 3 5 6 X
F7/A = 5 X 3 5 4 X
G# = 4 3 1 1 1 4
