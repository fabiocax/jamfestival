Jackson do Pandeiro - Rosalina

Rosalina
G,E7/G#,Bm/A,
 Rosalina é um tesouro
D7/A,D7/F#,G
Rosalina é uma flor
G/B              D/F#
Rosalina é minha vida
C/E     D7      G
Rosalina é meu amor
E7/G#           Bm/A
Por incrível que pareça
D7/A                 G
Eu não gostei de ninguém
E/G#              A7
Mais Rosalina possui

A/G            C6/9,D/C
Um jeitinho que ninguém tem
Em7/B                A7
As outras que me desculpe
C/E       D7       G
Mais a ela eu quero bem

G,G#0,Bm/A
Pode chover canivete
D7                  G/B
E o sangue dar na canela
E7/G#             A/G
Eu gosto da Rosalina
D/C                 A9
E tenho paixão por ela
Em7/B                A7
Quero terminar meus dias
C/E      D7      G
Deitado no colo dela

----------------- Acordes -----------------
A/G = 3 X 2 2 2 X
A7 = X 0 2 0 2 0
A9 = X 0 2 2 0 0
Bm/A = X 0 4 4 3 X
C/E = 0 3 2 0 1 0
C6/9 = X 3 2 2 3 3
D/C = X 3 X 2 3 2
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
D7/A = 5 X 4 5 3 X
D7/F# = X X 4 5 3 5
E/G# = 4 X 2 4 5 X
E7/G# = 4 X 2 4 3 X
Em7/B = X 2 2 0 3 0
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
