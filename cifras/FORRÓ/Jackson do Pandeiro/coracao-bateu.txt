Jackson do Pandeiro - Coração Bateu

Intro 2x: F  Eb  Bb

Refrão 2x:
F         Eb       Bb    F
Coração bateu, coração bateu
          Eb
coração bateu
              Bb         F
Ele bate mais forte que eu

  Bb
O bicho que mata o homem
C7              Eb
Mora debaixo do peito
Bb/D                   Bb
Não tem perna, não tem braço
       C7          F
Quando ama não tem jeito

Bb
Ele é bem redondinho

      C7       Eb
No feitio de maça
Bb/D                Bb
Quando deita com saudade
   C7            F
Não dorme até de manhã

Coração bateu...

Bb
Quem quer bem dorme na rua
C7              Eb
Na porta do seu amor
      Bb/D       Bb
Na calçada faz a cama
C7               F
Do sereno o cobertor

Coração bateu...

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bb/D = X 5 X 3 6 6
C7 = X 3 2 3 1 X
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
