Jackson do Pandeiro - Bodocongó

C C/G                    D7
Eu fui feliz lá no bodocongó
G7                      C  C7
Com meu barquinho de um remo só
F6
Quando era lua
F#m7/5
Com meu bem
C/E
Remava à toa
Am7/4              Dm7
Ai ai ai que coisa boa
G7               C
Lá no meu bodocongó
( C  F )
Bodo Bodo Bodo congó bodo bodocongó
Meu canário verde
F/A G7
Meu curió
F7+   F#m7/5   C/E
Bodocongó bodo bodocongó

A7               D7
Ai!Ai! Campina Grande
G7                C
Eu vivo aqui tão só

----------------- Acordes -----------------
Am7/4 = 5 X 5 5 3 X
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
C/G = 3 3 2 X 1 X
C7 = X 3 2 3 1 X
D7 = X X 0 2 1 2
Dm7 = X 5 7 5 6 5
F = 1 3 3 2 1 1
F#m7/5 = 2 X 2 2 1 X
F/A = 5 X 3 5 6 X
F6 = 1 X 0 2 1 X
F7+ = 1 X 2 2 1 X
G7 = 3 5 3 4 3 3
