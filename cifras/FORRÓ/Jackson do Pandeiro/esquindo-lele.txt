Jackson do Pandeiro - Esquindô Lelê

       D    A
Esquindô lêlê
           D/F#         A
Eu nunca amei na minha vida
       D    A
Esquindô lêlê
           D/F#            A
E eu não sabia o que era amar
       D    A
Esquindô lêlê
          D/F#         A
Se estou amando a ana rosa
        A/C#        D
Ó linda flor maravilhosa
         E7     A
Ô maravilhosa flor

       D    A
Esquindô lêlê
        D/F#        A
Ela me chama de benzinho

       D    A
Esquindô lêlê
         D/F#         A
Eu gosto dela e quero bem
       D    A
Esquindô lêlê
     D/F#          A
Mais tive uma má noticia
       D    A
Esquindô lêlê
                    D/F#    A
Que quase que eu chorei de dor
       D    A
Esquindô lêlê
       D/F#      A/C#
Existe uma rosa feia
     D           F#m7
Querendo botar areia
      Bm7           A
Nesse nosso lindo amor

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
Bm7 = X 2 4 2 3 2
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
E7 = 0 2 2 1 3 0
F#m7 = 2 X 2 2 2 X
