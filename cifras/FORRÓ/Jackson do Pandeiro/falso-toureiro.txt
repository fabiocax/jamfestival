Jackson do Pandeiro - Falso Toureiro

 G/F,C/E
Fui ver uma tourada
D7/F#, Am/C, G7
Lá na Escada de um toureiro de valor
E7/G#, E7/B,A7
Veja o senhor, mas quando na hora marcada
C9,D7,G
Que trapalhada, o toureiro não chegou.
Gm7,C7/G,Gm7
Me apresentaram como seu substituto
F#m7,F, Bb,
Fiquei maluco vendo aquela confusão
D#, Cm/G,Gm7,
Ouvi as moças me aplaudir lá da bancada
Bb/F,F6,Asus4  ,Bb
Senti uma pontada bem dentro do coração
A0,Gm7                  Gm6,Gm7
Caí na arena e recebi tanta chifrada
F#m7,F, Bb,
Quase que morro nas suas ponta afiada
Bb7, D#, Cm/G,Gm7,
Bati a mão na faca e gritei para o poleiro:

Bb/F,F6,Asus4  ,Bb
Eu mato o cara que disse que eu sou toureiro!

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am/C = X 3 2 2 5 X
Asus4 = X 0 2 2 3 0
Bb = X 1 3 3 3 1
Bb/F = X 8 8 7 6 X
Bb7 = X 1 3 1 3 1
C/E = 0 3 2 0 1 0
C7/G = 3 X 2 3 1 X
C9 = X 3 5 5 3 3
Cm/G = X X 5 5 4 3
D# = X 6 5 3 4 3
D7 = X X 0 2 1 2
D7/F# = X X 4 5 3 5
E7/B = X 2 2 1 3 X
E7/G# = 4 X 2 4 3 X
F = 1 3 3 2 1 1
F#m7 = 2 X 2 2 2 X
F6 = 1 X 0 2 1 X
G = 3 2 0 0 0 3
G/F = 1 X X 0 0 3
G7 = 3 5 3 4 3 3
Gm6 = 3 X 2 3 3 X
Gm7 = 3 X 3 3 3 X
