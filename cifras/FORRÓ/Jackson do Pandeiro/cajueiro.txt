Jackson do Pandeiro - Cajueiro

C/E A7 D7 G7 G7/9 C/G ou C
Cajueiro, êê, cajueiro ê-á
A7/C# A7/9   D7/A
Cajueiro pequenino
G7                 C C/E
Todo enfeitado de flor
F7 F#º C/G
Eu também sou pequenino
A7        Dm7 G7 C
Carregadinho de amor.
A7/C# A7/9  D7
Tradicional cajueiro
G7                 C C/E
Dos meus avós traz lembranç;a
F7 F#º C/G
Testemunha evocativa
A7         Dm7 G7 C
Dos meus tempos de criança.
A7/C#      A7/9  D7
Cajueiro não dá coco
G7           C C/E
Coqueiro não dá limão

F7 F#º C/G
O amor quando é de gosto
A7        Dm7 G7 C
Não produz ingratidão

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7/9 = 5 X 5 4 2 X
A7/C# = X 4 5 2 5 X
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
C/G = 3 3 2 X 1 X
D7 = X X 0 2 1 2
D7/A = 5 X 4 5 3 X
Dm7 = X 5 7 5 6 5
F#º = 2 X 1 2 1 X
F7 = 1 3 1 2 1 1
G7 = 3 5 3 4 3 3
G7/9 = 3 X 3 2 0 X
