Jackson do Pandeiro - Rosa

F6,D7/F#,F7/A,Gm7
Rosa, Rosa, vem ô Rosa
C/G,C7/G, F6
Estou chamando por você
D7/F#,F7/A ,Gm7,
Eu vivo lhe procurando
C/G , C7/G, F6
Você faz que não me vê
D7/F#, F7/A,Gm7
Eu vivo lhe procurando
C7, F7
E nem sinal de você.
(D7/F#,Gm7, C7/9-, F,D/F#, Gm7,C7,F/A
Rosa danada
Minha morena faceira
Minha flor de quixabeira
Não posso mais esperar.
(F#º,Gm7, C7/9, F,D/F#, Gm7,C7,F
Fique sabendo
Se casar com outro homem
O tinhoso me consome

Mas eu lhe meto o punhá.

F6,D7/F#,F7/A,Gm7
Comprei um papel florado
C/G,C7/G, F6
um envelope pra mandar dizer
D7/F#,F7/A ,Gm7,
numa carta bem escrita
C/G , C7/G, F6
o que sinto por você
D7/F#, F7/A,Gm7
a carta está esperando
C7, F7
porque não sei escrever.
F6,D7/F#,F7/A,Gm7
A coisa pior da vida
C/G,C7/G, F6
É querer bem a mulher
D7/F#,F7/A ,Gm7,
A gente deita na rede
C/G , C7/G, F6
Maginando por que é
D7/F#, F7/A,Gm7
Com tantas no mei do mundo
C7, F7
Só uma é que a gente quer.

----------------- Acordes -----------------
C/G = 3 3 2 X 1 X
C7 = X 3 2 3 1 X
C7/9 = X 3 2 3 3 X
C7/9- = X 3 2 3 2 X
C7/G = 3 X 2 3 1 X
D/F# = 2 X 0 2 3 2
D7/F# = X X 4 5 3 5
F = 1 3 3 2 1 1
F#º = 2 X 1 2 1 X
F/A = 5 X 3 5 6 X
F6 = 1 X 0 2 1 X
F7 = 1 3 1 2 1 1
F7/A = 5 X 3 5 4 X
Gm7 = 3 X 3 3 3 X
