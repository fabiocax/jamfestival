Jackson do Pandeiro - Olha o Vento

Bb               Gm                   F
Olha o vento que vem, olha o vento que vai
   D#   Bb/D    Cm       Bb
(Segura, morena, senão você cai)
      Bb6/G                  Cm7
Segura, morena, cuidado que o vento
              F           Bb
Vem fazendo estrago no canavial
         B°               Cm
No meu jardim balançou a roseira
D#/G    Dm7      F7/C      Bb
Sai na carreira, que vem temporal
Bb              Gm                 F
Olha o vento que vem, olha o vento que vai
  D#       Bb/D    Cm       Bb
 (Segura, morena, senão você cai)
     Bb          Gm           F
Esse vento que vem é o vento que vai
D#       Bb/D    Cm        Bb
(Segura, morena, senão você cai)
       Gm7                      Cm7
Segura, morena, que o vento é de proa

         F               Bb
Balança a canoa pra lá e pra cá
        B°                Cm
Segura no leme, cuidado na vida
D#           Bb/D      F         Bb
Que o vento te leva pro fundo do mar

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bb/D = X 5 X 3 6 6
Bb6/G = 3 5 3 3 3 3
B° = X 2 3 1 3 1
Cm = X 3 5 5 4 3
Cm7 = X 3 5 3 4 3
D# = X 6 5 3 4 3
D#/G = 3 X 1 3 4 X
Dm7 = X 5 7 5 6 5
F = 1 3 3 2 1 1
F7/C = X 3 3 2 4 X
Gm = 3 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
