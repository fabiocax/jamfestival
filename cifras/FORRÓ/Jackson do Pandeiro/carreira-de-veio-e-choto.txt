Jackson do Pandeiro - Carreira de Véio É Choto

Intro: A A7/C# D6 E7 A


A          A7/C#      D6         E7       A
Carreira de véio é chôto, quando cai é morto (refrão)

A               A6
Um velho milionário, residente em paquetá
             F#m7        F#m7/E   E7/B
Namorou minha irmã na intenção de casar,
 E7/B                             E7
Porém quando me contaram a triste situação
          D6            E7          A
Eu disse logo: "meu pai, não dê autorização"


tabrefrão
 A                A6
Minha prima passeava comigo em copacabana
                  F#m7        F#m7/E   E7/B
Quando apareceu um velho todo metido a bacana

 E7/B                                   E7
Dizendo que era rico e tinha um bom apartamento,
          D6            E7             A
Mas a moça muito viva, gritou no mesmo momento
refrão


 A                 A6
A filha do meu vizinho casou-se com um ancião,
                     F#m7       F#m7/E    E7/B
Só porque tinha dinheiro que emprestava à nação,
 E7/B                                E7
Mas logo no outro dia procurou se desquitar
        D6            E7          A
E anunciou no rádio que podia se casar

(refrão)

----------------- Acordes -----------------
A = X 0 2 2 2 0
A6 = 5 X 4 6 5 X
A7/C# = X 4 5 2 5 X
D6 = X 5 4 2 0 X
E7 = 0 2 2 1 3 0
E7/B = X 2 2 1 3 X
F#m7 = 2 X 2 2 2 X
F#m7/E = X X 2 2 5 2
