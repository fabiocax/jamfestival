Saia Rodada - Laranjunha

 Bm              Gbm
Tá ficando complicado demais
A              E
Não dá pra esconder
Bm                  Gbm
O nosso amor tá aumentando
                 A
Eu já estou te amando
           E
E quero só você

Bm              Gbm
Tá ficando complicado demais
A              E
Não dá pra entender
Bm                  Gbm
O nosso amor tá aumentando
                 A
Eu já estou te amando
           E
E quero só você


Bm                    Gbm
Mas o problema é seu marido
                A                  E
Que estava procurando um dia nos flagrar
Bm             Gbm                   A                E
Eu já fiquei sabendo, que ele anda dizendo que vai me matar
Bm               Gbm                     A                 E
Imagine se ele soubesse o que eu faço contigo quando faço amor
Bm                Gbm
Eu te faço de laranjinha
                 A
Chupo você todinha
          E
E você gostou

Bm     Gbm
Vamos embora amor
A                     E
Deixe esse cara, por favor
Bm     Gbm
Vamos embora amor
A                     E
Deixe esse cara, por favor

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
E = 0 2 2 1 0 0
Gbm = 2 4 4 2 2 2
