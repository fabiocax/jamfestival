Saia Rodada - Amar você é tão bom

INTRO - (A  F#m  Bm  E)

     A                              F#m                        Bm                                E
 Adoro ver a lua cheia assim de madrugada, quando estou com você
     A                              F#m                       Bm                           E                           »» 2x
 é lindo ver o dia amanhecendo na calçada, e eu querendo te ver

Refrão
 A         F#m      Bm            E                  A
    E o céu,  e o mar, se parecem com você a brilhar
                      F#m                   Bm
    Em seus olhos eu vejo estrelas
                       E                              A                            »»2x
    Em seu sorriso vou me lambuzar de batom
       F#m            Bm   D                       E
    Amor, adoro vê-la amar você é tão bom

INTRO - (A  F#m Bm E)

Repete do início ao fim

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
