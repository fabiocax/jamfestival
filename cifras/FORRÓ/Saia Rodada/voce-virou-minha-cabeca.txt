Saia Rodada - Você Virou Minha Cabeça

           Dm
 Estou apaixonado por você
                Bb
 o meu maior desejo é te ter
C                   F    A7
 você virou minha cabeça...    REFRÃO
           Dm
 você invadiu meu coração
            Bb
 com a força de um furacão
            C                  F    A7
 vem pra mim antes que eu enlouqueça...


F                    C
 por você eu rodo o mundo
               Bb                F
 por nenhum segundo eu perco a razão
  Dm                C
 faço um monte de besteira
            Bb                C
 fico na doideira por essa paixão


      F               C
 sou louco por teu sorriso
              Bb                 F
 e tudo que preciso é com você sonhar
 Dm                C
 quero roubar o seu beijo
               Bb                   C
 sempre que te vejo só penso em te amar...


           Dm
 Estou apaixonado por você
                Bb
 o meu maior desejo é te ter
C                   F    A7
 você virou minha cabeça...    REFRÃO 2X
           Dm
 você invadiu meu coração
            Bb
 com a força de um furacão
            C                  F    A7
 vem pra mim antes que eu enlouqueça...    (INTRO)


F                    C
 por você eu rodo o mundo
               Bb                F
 por nenhum segundo eu perco a razão
  Dm                C
 faço um monte de besteira
            Bb                C
 fico na doideira por essa paixão

      F               C
 sou louco por teu sorriso
              Bb                 F
 e tudo que preciso é com você sonhar
 Dm                C
 quero roubar o seu beijo
               Bb                   C
 sempre que te vejo só penso em te amar...


 REFRÃO 2X

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
