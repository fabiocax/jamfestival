Saia Rodada - E Vai Quebrar a Cara

C
Pense bem
F               C
Pra depois não chorar
 F                 C
Eu sei que vai sofrer
                 Am                Dm   G
Quando se arrepender vai querer voltar
              C
Olha que tudo bem
F                    C
Mais o que eu posso fazer
F                       C             Am        Dm   G
Se vai deixar quem te adora e quem te ama pra valer
    F        Fm    C
Só peço não saia agora
   Am       Dm
Porque ir embora
           F              G
Se ao seu lado é o meu lugar
F          Fm
Esfrie a cabeça

      C           Am
Eu te amo, não esqueça
      Dm                         F                 G
E se sair por essa porta nunca mais eu vou te perdoar

C
E vai quebrar a cara
       G                           Am
Quando ficar cara a cara com a solidão
 F       C              Am         Dm   G
Seu coração vai querer voltar pra mim
                      C
Vai ver que ainda me ama
                     G                  Am
Quando se deitar na cama e não me encontrar
F          C              G           C
Ai vai lembrar, foi você quem quis assim... (2x)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
