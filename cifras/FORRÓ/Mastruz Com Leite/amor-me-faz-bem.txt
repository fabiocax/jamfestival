Mastruz com Leite - Amor Me Faz Bem

Intro: D Em A D B7 Em A D Em A D B7 Em A D A D

D                   Em       A
Tô precisando de chamego, eu tô
                   D
Tô precisando de calor
B7                 Em
Tô precisando de carinho
              A
Um xodó arretadinho
                   D
Alguém pra me dar amor(bis)

D7         G
Amor verdadeiro
          A       F#m
Pra nunca mais acabar
 B7         Em
Amor por inteiro
        A        D
Gostoso de arrepiar

    D7          G
Meu bem, vem ligeiro
    A         Bm
Não posso esperar
          G
Meu fogo acesso
          A             D
Prontinho pra te incendiar
(Repete refrão)

D7          G
Pode ter certeza
           A           F#m
Se eu encontrar esse alguém
         B7       Em
Vai ser amor todo dia
        A          D
Porque amor me faz bem
    D7        G
Meu bem vem ligeiro
        A       Bm
Se você quer também
                    G
Vai ser amor,muito amor
          A           D
Não vai sobrar pra ninguém
Intro:D Em A D B7 Em A D Em A D B7 Em A D A D
(Repete tudo e introdução)

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
