Mastruz com Leite - Anjo de Guarda

C                         Am
Quando estiver só pode chamar que eu vou
           F                        C    G
Há! com você eu vou por qualquer estrada
           C            Am
Nada de chorar eu choro por você
          F                       G    G F# F
Sou tudo por você topo qualquer parada

F                             C
Sou poeira que o vento não levou
                              G
Sou caminho que só o amor pisou
                 C
Estou nessa caminhada
F                              C
Sou alguém que você olha e não vê
              G                C
Mas eu sigo você como anjo de guarda

G                C
Se precisar de alguém

        Am                       F
Pode chamar que eu vou há! com você eu vou
                C    G
Por qualquer estrada
          C             Am
Nada de temer eu vou te defender
          F                        G
Sou tudo por você pra ficar do teu lado

F                             C
Sou poeira que o vento não levou
                              G
Sou caminho que só o amor pisou
                 C
Estou nessa caminhada
F                              C
Sou alguém que você olha e não vê
              G                C
Mas eu sigo você como anjo de guarda

G                C
Se precisar de alguém
        Am                       F
Pode chamar que eu vou há! com você eu vou
                C    G
Por qualquer estrada
          C             Am
Nada de temer eu vou te defender
          F                        G    G F# F
Sou tudo por você pra ficar do teu lado
F                             C
Sou poeira que o vento não levou
                              G
Sou caminho que só o amor pisou
                 C
Estou nessa caminhada
F                              C
Sou alguém que você olha e não vê
              G                C
Mas eu sigo você como anjo de guarda

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
