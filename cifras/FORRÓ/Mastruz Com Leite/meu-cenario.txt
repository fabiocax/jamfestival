Mastruz com Leite - Meu Cenário

Intro: A F#m D E A F#m D E A

A                     E
Vejo uma gaivota pelo céu
                    D
O sol sumindo sob o véu
             E          A  E
De uma nuvem branca no azul
 A                     E
Todo o universo escurecendo
                 D
A noite então nascendo
       E             A  Abm F#m E
Belo cenário para o amor

                  D
A lua começa a surgir
                   E
Uma estrela a refletir
                    A  Abm F#m E
E eu sozinha aqui estou

                D
Onde esta você agora?
                  E
Te quero, venha embora
             A  E
Te amo, meu amor

A
Vem, que a noite
               C#m
Espera por nós dois
                 D
Deixe tudo pra depois
      E            A     E
Vem comigo para o amor (amor)
A
Vem, não há sentido
         C#m
Eu sem você
                     D
Orgulho, brigas para quê?
               E
Se o que eu procuro
          A
Acho em você(2x)

Intro: A F#m D E A F#m D E A


----------------- Acordes -----------------
A = X 0 2 2 2 0
Abm = 4 6 6 4 4 4
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
