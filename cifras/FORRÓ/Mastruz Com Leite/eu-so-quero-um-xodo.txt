Mastruz com Leite - Eu Só Quero Um Xodó

[Intro] C  Bb  F  G7
        C  Bb  F  G7  C

 C                 Am    Em
Que falta eu sinto de um bem
      Dm         G7   C   G7
Que falta me faz um xodó
      C           Am     Em
Mas como eu não tenho ninguém
     Dm          G7       C
Eu levo a vida assim tão só
        Bb       C
Eu só quero um amor
       Bb         D7
Que acabe o meu sofrer
     Am      D7
Um xodó pra mim
         Am    D7
Do meu jeito assim
       F      G7    C
Que alegre o meu  viver


( Bb  F  G7  C )
( Bb  F  G7  C )

 C                 Am    Em
Que falta eu sinto de um bem
      Dm         G7   C   G7
Que falta me faz um xodó
      C           Am     Em
Mas como eu não tenho ninguém
     Dm          G7       C
Eu levo a vida assim tão só
        Bb       C
Eu só quero um amor
       Bb         D7
Que acabe o meu sofrer
     Am      D7
Um xodó pra mim
         Am    D7
Do meu jeito assim
       F      G7    C
Que alegre o meu  viver

( Bb  F  G7  C )
( Bb  F  G7  C )

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G7 = 3 5 3 4 3 3
