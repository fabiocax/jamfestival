Mastruz com Leite - Como Posso Te Esquecer

Intro:  Bb  Dm  Gm  Dm  Eb   F  Bb  F

Bb           Gm       Cm                  F
Quanto tempo faz meu amor que eu quero te ver
Bb         Gm      Cm               F
Toda minha vida eu só esperei por você
Bb         Dm   Eb                    F
Mas você divide essa paixão dentro de mim
Bb          Dm       Eb                 F
Teu comportamento me faz sofrer tanto assim

Refrão:
Bb                 Dm     Gm            Dm
Como posso te esquecer se você foi o primeiro
    Eb                                  Cm               F
Que mexeu com a minha cabeça, o meu coração, meu corpo inteiro
 Bb                Dm            Gm          Dm
Como posso te esquecer se o meu amor é verdadeiro
       Eb                  F                Bb  F
Mas eu sei que a minha esperança não vai acabar


Bb           Gm       Cm                  F
Quanto tempo faz meu amor que eu quero te ver
 Bb        Gm      Cm               F
Toda minha vida eu só esperei por você
Bb         Dm   Eb                    F
Mas você divide essa paixão dentro de mim
Bb          Dm       Eb                 F
Teu comportamento me faz sofrer tanto assim

(Refrão)

Intro: Bb Dm Gm Dm Eb F Bb F

Bb           Gm       Cm                  F
Quanto tempo faz meu amor que eu quero te ver
Bb         Gm      Cm            F
Toda minha vida eu só esperei por você
Bb         Dm   Eb                    F
Mas você divide essa paixão dentro de mim
Bb          Dm       Eb                 F
Teu comportamento me faz sofrer tanto assim

(Refrão 2x)

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
