Mastruz com Leite - Refém Dessa Paixão

Intro: Bb F (Gm Bb Dm)2x  Gm Bb F 2x

(Bb F Gm Eb)4x
Quando lembro o que aconteceu
Desentendimentos entre nós, tudo em um minuto se perdeu
Mas ainda ouço sua voz.
Juro que até tentei mudar, mas você feriu minha razão
                                                  Gm  F
e agora tenho que tentar esquecer você e essa paixão (essa paixão)


(refrão)

(Bb F Gm Eb)4x
Tenho que encontrar uma saída pra essa dor
Quero esquecer que fui refém dos teus abraços.
Eu preciso fugir dessa lembrança e desse amor,
Não quero lembrar que já estive em teus braços
Deletei seus email-s e suas fotografias
Quero exilar do pensamento o teu sorriso
Meu coração precisa te arrancar da minha vida

Não quero lembrar que conheci o paraíso,
      Bb         F        Gm      Gm
com você, com você, com você


(Bb F Gm Eb)4x
E quando eu estou aqui sozinha,
Lembro nosso amor que se perdeu.
Você vem dizer que a culpa é minha,
Mas erros você também cometeu
Tento dormir e o sono não vem,
Eu não sei convencer meu coração
Não consigo amar um outro alguém
                               Gm  F
Porque virei refém dessa paixão

(refrão)

(Bb F Gm Eb)4x
Tenho que encontrar uma saída pra essa dor
Quero esquecer que fui refém dos teus abraços.
Eu preciso fugir dessa lembrança e desse amor,
Não quero lembrar que já estive em teus braços
Deletei seus email-s e suas fotografias
Quero exilar do pensamento o teu sorriso
Meu coração precisa te arrancar da minha vida
Não quero lembrar que conheci o paraíso,
      Bb         F        Gm         F        Gm
com você, com você, com você, com você, com você ...

(Gm Bb Dm) Gm Bb F 2x

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
