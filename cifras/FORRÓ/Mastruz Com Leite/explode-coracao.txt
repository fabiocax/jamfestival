Mastruz com Leite - Explode Coração

Em                   D
Explode coração
                 Em               D
Nessa noite de fogueira de balão
              Em             D
Feita para enfeitar nossa união
                    B
Para sempre eu e você
Em           D
Nesse São João
                        Em       D
Quero queimar na fogueira da paixão
              Em           D
De amor incendiar meu coração
              B
Até ele derreter
             Em                   D
Quero um sanfoneiro la dos cafundo
             Em                  D
Vem de pé-de-serra pra tocar forró
              Em                D
Sentir o teu cheiro, cheiro sedutor

             Em               D         B
Provar o teu beijo, beijo matador, uohohoh
Em
Foi Santo Antônio quem falou
                      D
E São Pedro confirmou, confirmou, confirmou, ohohoh
Em
Que nessa noite tão linda serias meu amor, amor

( B )

----------------- Acordes -----------------
B = X 2 4 4 4 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
