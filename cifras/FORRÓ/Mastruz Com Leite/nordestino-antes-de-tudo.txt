Mastruz com Leite - Nordestino Antes de Tudo

Intro: F  Gm  C  F  Dm  Gm  C  F  Gm  C  F  Dm  Gm  C  F

                  Gm
O cearense pode viver
C          Am
Fora do Ceará
     D           Gm
O baiano sem a Bahia
        C              F
Pernambucano sem Pernambuco
        Dm                  Gm
O nordestino só não pode ficar
        C                       F
Sem rapadura e farinha pra mastigar
        Dm                  Gm
O nordestino só não pode ficar
         C                    F
É sem forró e mulher pra chamegar
Introdução:F Gm C F Dm Gm C F
                       Gm
Melhor lugar pra ser feliz

        C              Am
Pro maranhense é São Luís
     D           Gm
Pro amor é o coração
            C                F
Pra Campina Grande é o São João
        Dm                 Gm
O Nordestino só não pode ficar
        C                     F
É sem fogueira e batata pra assar
        Dm                 Gm
O Nordestino só não pode ficar
         C                    F
É sem forró e mulher pra chamegar

Intro:F  Gm  C  F  Dm  Gm  C  F

              Gm
Vixe que povo forte
       C                      Am
Bem maior que o Rio Grande do Norte
     D              Gm
Bóia-fria do sangue quente
        C               F
Esse Brasil também é da gente
        Dm                 Gm
O Nordestino só não pode ficar
        C                  F
É sem café e rede pra balançar
        Dm                 Gm
O Nordestino só não pode ficar
         C                    F
É sem forró e mulher pra chamegar

Intro:F Gm C F Dm Gm C F

                     Gm
Aracajú, Maceió, Teresina
          C                  Am
Tudo é nordeste, tem a mesma sina
             D           Gm
Nem sempre o sul é a solução
     C                       F
O problema é a saudade do sertão
        Dm                 Gm
O Nordestino só não pode ficar
         C                       F
Sem vaquejada e vaqueiro pra aboiar
        Dm                 Gm
O Nordestino só não pode ficar
         C                    F
É sem forró e mulher pra chamegar
Introdução:F Gm C F Dm Gm C F
                     Gm
Na palma da mão calo seco
            C        Am
Na palma da mão o destino
           D          Gm
O nosso destino é ter fé
             C         F
Forró sempre foi nosso hino
        Dm                 Gm
O Nordestino só não pode ficar
           C                            F
Sem o forrozão Mastruz com Leite pra dançar
        Dm                 Gm
O Nordestino só não pode ficar
          C                           F
Sem Padim Ciço, Frei Damião pra abençoar

Intro:F Gm C F Dm Gm C F

                     Gm
Na palma da mão calo seco
            C        Am
Na palma da mão o destino
           D          Gm
O nosso destino é ter fé
             C         F
Forró sempre foi nosso hino
        Dm                 Gm
O Nordestino só não pode ficar
           C                            F
Sem o forrozão Mastruz com Leite pra dançar
        Dm                 Gm
O Nordestino só não pode ficar
          C                           F
Sem Padim Ciço, Frei Damião pra abençoar

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
