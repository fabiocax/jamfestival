Mastruz com Leite - Olhinhos de fogueira

Intro: A  E  A  A7  D
       D  C#m  Bm  A
       D  C#m  Bm  E  A

     A
Explodiu meu coração
      F#m
Feito bomba no São João
       D
Quando deslizei as mãos
      E
Pelas tuas costas nuas

     A
Vi estrela no salão
     F#m
Luar cheio de balão
      D
Teus olhinhos de fogueira
       E
Me queimando de paixão


       A
Chão batido de chuvinha
       F#m
Só pra ver tuas perninhas
  D
Tuas perninhas grossas
   E
Dançar São João na roça

A              F#m
Roça de milho, roça meu filho
D              E
Roça assim teu corpo em mim
A            F#m
Roça mulata, roça a batata
D              E
Roça assim teu corpo em mim

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
