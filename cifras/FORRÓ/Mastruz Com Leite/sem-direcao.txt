Mastruz com Leite - Sem Direção

Intro.:   (Em* D A)4x
          Bm Em Bm F#m
          Bm Em Bm A

    Bm  A       Bm
 Estou sem direção,
           A         Bm
 Sem estação pra parar
 Bm   A      Bm
 Mar de solidão
          A      Bm
 Um coração a vagar
Em*      D       A
 Saio andando sozinho
Em*        D       A
 pensando te encontrar
       Bm         G D A
 Em algum lugar...
       Bm         G D A
 Em algum lugar...
          Bm          G          A
 Chega a noite o meu quarto é escuro

        Bm      G     A
 Nem a lua pode iluminar
        Bm          G         A
 Meu caminho uma estrada sem rumo
        Bm           G        A
 Nem estrelas vão poder me guiar
       D   A Bm G
 Só você...
       D   A B
 Só você...
       G
 Meu amor
    F#m            Bm
 Eu sempre amei demais
       G    F#m           B
 Só você devolve a minha paz
Em*        D       A
 Vou caminhando sozinha
Em*       D         A
 Tentando te encontrar
       Bm         G D A
 Em algum lugar...
       Bm         G D A
 Em algum lugar...
     E               A E
 Me diz onde você está
               B
 Eu vou te buscar
 Não importa o lugar
                 C#m
 Eu quero encontrar
                A
 E de novo te amar
         G#m    F#m   B4 B
 Não me deixe sozinha
     E                    A  E
 Me diz que eu quero atracar
                 B
 No teu porto seguro
 No teu céu no teu mar
                   C#m
 Viver tudo outra vez
                   A
 Não deixar naufragar
         G#m       F#m B
 Nosso amor tão bonito
                  C#m  A  E  B
 Eu só quero te amar...
      C#m   A  E  B
 te amar...
      F#   C#  D#m  B
 te amar...
      F#   C#  D#
 te amar...

  Em*
  022003
  > nessa parte fica só o instrumental --> (B A#m D#m)


----------------- Acordes -----------------
A = X 0 2 2 2 0
A#m = X 1 3 3 2 1
B = X 2 4 4 4 2
B4 = X 2 4 4 5 2
Bm = X 2 4 4 3 2
C# = X 4 6 6 6 4
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
D# = X 6 5 3 4 3
D#m = X X 1 3 4 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G#m = 4 6 6 4 4 4
