Mastruz com Leite - Brincar de Amar

[Intro]  F#m  D  Bm  E
         F#m  D  Bm  E

  A                F#m        C#m
Quero saber o que sinto por você
F#m                   D
De onde vem tanto querer
E                   F#m
Nem eu mesma sei por que
      Bm          E
Explicar essa paixão
A                   F#m       C#m
Não sei dizer onde tudo começou
F#m            D
Esse caso de amor
E                 A (A7)
Só sei que aconteceu

   D                 E
E quando estou em algum lugar
         C#m             F#m
Eu não posso ficar com você

D                 E
Tudo eu faço pra me controlar
       F#m      (F#m/E)
Vou enlouquecer
D                E
Fico esperando você me olhar
   C#m             F#m
Chamar para fugir com você

D                A
Pra outro lugar, pra a gente se amar
E                      A
Pra a gente brincar de viver

D                A
Pra outro lugar, pra a gente se amar
E                      F#m
Pra a gente brincar de viver

[Final]  F#m  D  Bm  E
         F#m  D  Bm  E

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
F#m/E = X X 2 2 2 2
