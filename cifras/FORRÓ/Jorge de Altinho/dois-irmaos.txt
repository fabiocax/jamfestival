﻿Jorge de Altinho - Dois Irmãos

Intro:C - F - C - Bb - Am - Gm - F


                                   C                                        F
Minha mãe teve dois filhos frutos de amor e paixão
                                      F7                               Bb
Brincamos crescemos juntos a margem do riachão
                          C
O destino separou ele foi ser um doutor
                                    F
E eu fiquei para ser peão
                                       C                              F
Hoje quando passa um carro pra cidade a rodar
                                             F7                                  Bb
Meus olhos se enchem de água e dá vontade de chorar


                           C
Separação é sentença com certeza ele pensa
                                       F
Um dia em qualquer voltar
                                C                              F
Eu já mandei uma carta e ele me respostou
                              F7                             Bb
Dei um aboio na serra que a boiada parou
                          C
Essa terra vale ouro

                                                                                   F
Não dou meu chapéu de couro no seu nome de doutor
                                      C                                      F
Quando tem forró por perto uma novena ou cantoria
                           F7                               Bb
Sinto logo sua falta era minha companhia
                             C
Pensando no tabuleiro se encosta no travesseiro
                                F                            C
E ver amanhecer o dia aqui no céu azulado

                               F                                  F7
Vejo o vôo do gavião pelas nuvens de fumaça
                   Bb                             C
Ele vê o avião na luz daquela cidade
                                                                       F
Não encontro a claridade do luar do meu sertão
                                C       F
Eu risco no meu cavalo ele risca no papel
                                F7                Bb
Ele faz conta pras provas já eu conto meus troféus

                                 C
Faculdade não me agrada
                       F
Minha luva de vaquejada eu não troco em seu anel
                                   C                 F
Ele pensa em se formar já eu penso em formar pasto
                           F7                                          Bb
Ele conhece das letras do gado eu conheço o rastro
                                         C
Tenho orgulho em lhe dizer
                            F
Que o boi pode se perder que dá trabalho, mas acho!
        C           F
Cada um tem seu destino tem a sua profissão
        F7      Bb
Eu nasci pra ser vaqueiro doutor é o meu irmão
        C
Assim vivemos no sonho de volta a banho
           F
Nas águas do riachão

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
Gm = 3 5 5 3 3 3
