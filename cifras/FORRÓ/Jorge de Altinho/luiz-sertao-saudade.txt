Jorge de Altinho - Luiz, Sertão, Saudade

Intro 4x: E A E

E
A lua não é mais a mesma no céu do sertão

E o sol chorando com seus raios te procura em vão
F#m                           A                       E
Lá num canto da parede/ Um chapéu de couro e um gibão ficou
A                      E
Aquela sanfona branca, que tanta gente alegrou

 F#m            A               E      |
Só a saudade doendo Gonzaga deixou    | Refrão.
 F#m            A               E      |
Só a saudade doendo Gonzaga deixou    |

E
Te vejo num voar alegre de um beija-flor

Nas águas de um açude cheio, na voz de um cantador.
F#m                        A                 E
Nas borboletas no caminho, Num arco-íris multicor

F#m                 A                   E
No riacho do navio/ Que, de tristeza, secou.

 F#m            A               E      |
Só a saudade doendo Gonzaga deixou    | Refrão.
 F#m            A               E      |
Só a saudade doendo Gonzaga deixou    |

E                                   F#m
Juazeiro, assum preto, asa branca e mais
B7                                        E
Cantigas que fizeram o rei, o trovador da paz
                                F#m
Gênio, mito, tua obra te imortalizou
                          B7                   E
Pois nos quatro cantos do Brasil o teu canto ecoou

Am                                            E
A voz da chuva, a esperança/ De um povo sonhador.
Am                                      E
Pelas estradas da vida/ Passaste semeando o amor

          F#m            A               E
Mais Hoje só a saudade doendo Gonzaga deixou
     F#m            A                     E
Hoje só a saudade doendo, Luiz Gonzaga deixou
F#m            A               E
Só a saudade doendo Gonzaga deixou
F#m            A               E
Só a saudade doendo Gonzaga deixou

Solo E A E (4X)

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
