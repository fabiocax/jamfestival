Jorge de Altinho - Esperando Na Janela

[Intro] E  B7  E  B7  E

  E                      F#m                   B
Ainda me lembro do seu caminhar seu jeito de olhar
              E                                F#m
Eu me lembro bem fico querendo sentir o seu cheiro
            B            E
É daquele jeito que ela tem
                            F#m             B
O tempo todo eu fico feito tonto sempre procurando
             E                               F#m
Mas ela não vem e esse aperto no fundo do peito
               B                 E
Desses que o sujeito não pode aguentar
                           F#m
E esse aperto aumenta meu desejo
               B                  E
Eu não vejo a hora de poder lhe falar

                        F#m          B
Por isso eu vou na casa dela, ai, ai

                       E       C#m
Falar do meu amor pra ela, vai
                     F#m           B
Tá me esperando na janela, ai, ai
                    E
Não sei se vou me segurar

                        F#m          B
Por isso eu vou na casa dela, ai, ai
                       E       C#m
Falar do meu amor pra ela, vai
                     F#m           B
Tá me esperando na janela, ai, ai
                    E
Não sei se vou me segurar

( E  B7  E  B7  E )

  E                      F#m                   B
Ainda me lembro do seu caminhar seu jeito de olhar
              E                                F#m
Eu me lembro bem fico querendo sentir o seu cheiro
            B            E
É daquele jeito que ela tem
                            F#m             B
O tempo todo eu fico feito tonto sempre procurando
             E                               F#m
Mas ela não vem e esse aperto no fundo do peito
               B                 E
Desses que o sujeito não pode aguentar
                           F#m
E esse aperto aumenta meu desejo
               B                  E
Eu não vejo a hora de poder lhe falar

                        F#m          B
Por isso eu vou na casa dela, ai, ai
                       E       C#m
Falar do meu amor pra ela, vai
                     F#m           B
Tá me esperando na janela, ai, ai
                    E
Não sei se vou me segurar

                        F#m          B
Por isso eu vou na casa dela, ai, ai
                       E       C#m
Falar do meu amor pra ela, vai
                     F#m           B
Tá me esperando na janela, ai, ai
                    E
Não sei se vou me segurar

( E  B7  E  B7  E )

----------------- Acordes -----------------
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
