Jorge de Altinho - Filho do Cariri

Intro: G C D Bm Em Am D7 G (C D G)

G
Eu nasci no Cariri, Eu me criei por aqui.
                 Am
Eu fui menino treloso

Por cima de pedras, espinhos
                  D7                      G
Entre veredas, caminhos/ Sobrevivi de teimoso

G
Ganhei o sol como herança/ Que acendeu minha esperança
    G7            C
Não vivo sem esse chão
                     G
Na seca, é dura essa vida
                   Am            D7       G  G7
Mas minha terra querida/ Mora no meu coração
C                     G
Na seca, é dura essa vida

                   Am            D7       G
Mas minha terra querida/ Mora no meu coração

G7                 C   (C Bm Am)
Aqui eu sou mais feliz,
Am        D        Bm
Do que em outro lugar
Em                Am    D                G
Aqui escuto o concriz/  De manhã cedo cantar
G7              C  (C Bm Am)
Não saio do Cariri
Am        D         Bm
Se o Deus daí é o daqui
Em                     Am
E não tem guerra, nem tanque.
                        D                       G  (C D G)
Não tem trator que me arranque, Aqui do meu Cariri

G
Meu céu é mais estrelado, Meu povo é mais animado
                     Am
Com um cabra bom tocador
                                            D7
O bode, pulando, berra/ Como quem diz nessa terra,
                    G
"Só fica quem tem amor"

G
O açude quase na lama
                      G7                C
Mas o preá faz a cama na sombra da macambira
                      G
Que Deus do céu nos ajude
                    Am             D7         G
Pois não faltando saúde, o resto, agente se vira

Refrão, solo G C D Bm Em Am D7 G (C D G) finaliza.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
