Xand Avião - Forma Discreta

  G#                     A#
Em frente ao espelho eu ensaiei
 Cm                   A#
Uma forma discreta de dizer
  G#                 A#
Conheço bem sua timidez
 Cm                         A#
Não vou ser tão direto com você

 G#                  A#                    Cm
Já imaginei sua reação segurando a minha mão
                A#
O sorriso vai olhar pra mim
 G#                          A#
Depois que eu tocar seu coração
              Cm                    A#
Se existir um não, vai se tornar um sim

                             G#
Deixa a sua mãe virar minha sogra
A#                      Cm
E eu ser o genro do seu pai

             A#             G#
Quero o seu irmão como cunhado
             A#              Cm        Cm A#
Só te peço isso, o resto eu corro atrás

                             G#
Deixa a sua mãe virar minha sogra
 A#                      Cm
E eu ser o genro do seu pai
              A#            G#
Quero o seu irmão como cunhado
            A#                      Cm
Só te peço isso, o resto eu corro atrás

----------------- Acordes -----------------
A# = X 1 3 3 3 1
Cm = X 3 5 5 4 3
G# = 4 3 1 1 1 4
