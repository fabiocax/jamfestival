Xand Avião - Sábado À Noite

[Intro] Cm  F  Cm  F  Bb  Eb  Gm

      Cm                F        
E|-----3-------------------------|
B|-------4-------------------6/4-|
G|---5---------------2--3--5-----| (3x)
D|-------------------------------|
A|-3-----------------------------|
E|-------------------------------|

 Cm                           F
Seu repertório de desculpas, eu conheço bem
     Cm                          F
Quando ele acaba, você tenta inverter a culpa
     Cm                          F          Bb
Sem argumentos, a saída é levantar a voz e dizer
Eb                Gm
Que não quer discutir
Cm                                 F
Dos seus erros, eu garanto que o pior
                                  Bb
É achar que eu vou sempre estar aqui


           Cm                    Eb
Sábado à noite, você pode achar mil bocas pra te consolar
       Bb
Mas quando chegar o domingo à tarde, de mim vai lembrar
                       Cm                       Eb
E vai sentir saudades, aí cê vai me procurar, de novo
                      Bb              F
Vai querer voltar e eu não vou mais, não vou mais, voltar

    Cm                          F
Seu repertório de desculpas, eu conheço bem
     Cm                          F
Quando ele acaba, você tenta inverter a culpa
     Cm                        F              Bb
Sem argumentos, a saída é levantar a voz e dizer
         Eb        Gm
Que não quer discutir
         Cm                        F
Dos seus erros, eu garanto que o pior
                           Bb
É achar que eu vou sempre estar aqui

           Cm                    Eb
Sábado à noite, você pode achar mil bocas pra te consolar
       Bb
Mas quando chegar o domingo à tarde, de mim vai lembrar
                       Cm                       Eb
E vai sentir saudades, aí cê vai me procurar, de novo
                      Bb              F
Vai querer voltar e eu não vou mais, não vou mais, voltar

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
