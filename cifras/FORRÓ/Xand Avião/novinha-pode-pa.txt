Xand Avião - Novinha Pode Pá

[Intro] C9  Am  Bm  Em7

   C
Relaxa, você tá na minha e eu também te quero
                                 Bm7
Larga de besteira, o nosso caso é sério
                       Em7
E me entrega logo o seu coração
                         C
Já não aguento de tanta ilusão
                              Am7
Você tá na minha e eu também te quero
                                 Bm7
Larga de besteira, o nosso caso é sério
                        Em7
E me entrega logo o seu coração
                          C
Já não aguento de tanta ilusão
                               Am7
E eu vou praí porque a noite tá fria
                        Bm7                     Em7
Se tá sozinha precisa de mim pra te abraçar, pra te beijar

                                         C
Não faz biquinho porque eu sei que tá a fim
                        Am7                         Bm7
Já tô chapando qual é a tua, quem me falou foi o teu olhar
                          Em7
Quando o safadão apagou a luz
C                        Am7                          Bm7
Eu te agarrei novinha pode pá, e te beijei novinha pode pá
                                     Em7
Te conquistei novinha, novinha, novinha
C                         Am7                         Bm7
Eu te agarrei novinha pode pá, e te beijei novinha pode pá
            Em7
Novinha, novinha

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
Em7 = 0 2 2 0 3 0
