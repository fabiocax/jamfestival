Dominguinhos - Siá Mariquinha

[Intro] Em  Am  Em  B7  E7
          Am  Em  B7  E

Em                         Am                               B7
A saudade que se guarda das coisas da vida
                       Em
Que a gente gozou
Am                     Em
Pode inté se arrelembrar
B7                                             Em
Tantas coisas velhas que já se passou
Em                                   Am
Quanto mais passado o tempo
                            B7
Mais o amor aumenta
                        Em
Mais saudade vem
Am                      Em
Mode a gente arrelembrar
                     B7                               E
Dos amor querido que a gente quis bem


E
Siá Mariquinha, Maroquinhazinha
                 B7                                E
Sua velha casinha nos tempos de amor
          A                              E
E a ventania de riba da serra
               B7                        E
Pegou a casinha e escangalhou
A  E     B7                                               E
Ai, ai, Siá Mariquinha, isto não é brinquedo
A                                  E
Me diga se a saudade mata
                        B7
Se a saudade mata
                        E
Qu´eu já to com medo

Em                       Am
Minha pobre Mariquinha
         B7                                   Em
Sua casinha tinha, um pé de jatobá
Am                     Em            B7
Onde toda tarde fria sabiá subia
Em
Pru mode cantar
Am                      B7
E o riacho lá da serra
Que vinha por terra
                    Em
Rodiando a volta
Am                            Em
Ah,quanta saudade morta
                      B7
Ninguém dá jeito
                    E
O jeito é cantar

E
Siá Mariquinha, Maroquinhazinha
                 B7                                E
Sua velha casinha nos tempos de amor
          A                              E
E a ventania de riba da serra
               B7                        E
Pegou a casinha e escangalhou
A  E     B7                                              E
Ai, ai, Siá Mariquinha, isto não é brinquedo
A                                  E
Me diga se a saudade mata
                        B7
Se a saudade mata
                        E
Qu´eu já to com medo

 ( Em  Am  Em  B7  E7 )
( Am  Em  B7  E )

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
