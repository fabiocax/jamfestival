Dominguinhos - Onde Está Você

[Intro] Em  A7  Em  G7M  A7  Em  A7
        Em  A7  Em  G7M  A7  Em  A7

[Primeira Parte]

Em      A7   Em
Onde está você
Em    D      C      D    G7M
  Apareça aqui pra me ver
               A7    Em  A
Que eu vou gostar demais
 Em   A       Em         D    C
Sabes onde estou pois nada mudou
       D      G        A7   Em  A
Venha me dizer onde você andou

Em      A    Em
Onde está você
Em    D      C      D    G
  Apareça aqui pra me ver
               A7    Em  A7
Que eu vou gostar demais

 Em   A7      Em         D    C
Sabes onde estou pois nada mudou
       D      G7M      A7   Em  A7
Venha me dizer onde você andou

[Segunda Parte]

Em          D            Em
  Eu andei sem te encontrar
     D           Bm7    A7             Em
Em quase todo lugar eu perguntava por ti
      A7               Em
Teus passos sempre segui
          D        C7
Querendo te encontrar
     B7           Em
Só pra falar de amor

            A7      Em
Frases que nunca falei
      A7            C7
Carinhos que nunca fiz
    D7               G7+
Beijos que nunca te dei
    F#m7    B7    Em
O amor que te neguei
      Em/D          C
Mas agora quero te dar
        B7    Em
E te fazer feliz

( Em  A7  Em  G7M  A7  Em  A7 )
( Em  A7  Em  G7M  A7  Em  A7 )

Em      A7   Em
Onde está você
Em    D      C      D    G7M
  Apareça aqui pra me ver
               A7    Em  A
Que eu vou gostar demais
 Em   A       Em         D    C
Sabes onde estou pois nada mudou
       D      G        A7   Em  A
Venha me dizer onde você andou

Em      A    Em
Onde está você
Em    D      C      D    G
  Apareça aqui pra me ver
               A7    Em  A7
Que eu vou gostar demais
 Em   A7      Em         D    C
Sabes onde estou pois nada mudou
       D      G7M      A7   Em  A7
Venha me dizer onde você andou

[Segunda Parte]

Em          D            Em
  Eu andei sem te encontrar
     D           Bm7    A7             Em
Em quase todo lugar eu perguntava por ti
      A7               Em
Teus passos sempre segui
          D        C7
Querendo te encontrar
     B7           Em
Só pra falar de amor

            A7      Em
Frases que nunca falei
      A7            C7
Carinhos que nunca fiz
    D7               G7+
Beijos que nunca te dei
    F#m7    B7    Em
O amor que te neguei
      Em/D          C
Mas agora quero te dar
        B7    Em
E te fazer feliz

[Primeira Parte]

Em      A7   Em
Onde está você
Em    D      C      D    G7M
  Apareça aqui pra me ver
               A7    Em  A
Que eu vou gostar demais
 Em   A       Em         D    C
Sabes onde estou pois nada mudou
       D      G        A7   Em  A
Venha me dizer onde você andou

[Final] Em  A7  Em  G7M  A7  Em  A7
        Em  A7  Em  G7M  A7  Em  A7

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
Em/D = X X 0 4 5 3
F#m7 = 2 X 2 2 2 X
G = 3 2 0 0 0 3
G7+ = 3 X 4 4 3 X
G7M = 3 X 4 4 3 X
