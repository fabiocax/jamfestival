Dominguinhos - Doidinho, Doidinho

A                     A7      D7
Vai ter forró
            A     A7    D7
Na casa do biu
          A     A7    D7
Você vai ver
             A   D   E
O que nunca viu

         A
Linda morena
      Bm         C#m
Com a saia bem rodada
    Bm      C#m
Dá uma rodopiada
       Bm          A
Que machuca o coração

        Em     D        C#m
E eu tô lá doidinho, doidinho
     D    C#m
Piso direitinho

      Bm             A
Não passo o pé pela mão

        Em     D        C#m
E eu tô lá doidinho, doidinho
     D    C#m
Piso direitinho
      Bm             A
Não passo o pé pela mão
D     E
Oh oh

Vai ter forró...

         A
De madrugada
      Bm      C#m
O forró tá animado
       Bm          C#m
Já tem cabra embriagado
        Bm           A
Que não sabe nem dançar

        Em     D        C#m
E eu tô lá doidinho, doidinho
     D         C#m
Toco direitinho
      Bm         A
Pra morena me olhar

        Em     D        C#m
E eu tô lá doidinho, doidinho
     D         C#m
Toco direitinho
      Bm         A
Pra morena me olhar
D  E
Oh oh

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
