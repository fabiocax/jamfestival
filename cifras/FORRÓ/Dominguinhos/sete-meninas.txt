Dominguinhos - Sete Meninas

Intro: Bb  C7  F  Bb  B°  F/C  C#°
      (Dm7 C7  F  G   F   C7   F)

                    F
Sábado de noite eu vou
Bb               F
Vou pra casa do Zé
Bb                  F
Sábado de noite eu vou
Dm7      Gm7          C7    F
Dançar o coco e arrastar o pé

                 G7
E a beleza de Maria
               Dm7
Ela só tem pra dar
                   G7
O corpinho que ela tem
                   F
Seu andar requebradinho
            Bb
Mexe com a gente

           F   Bb         F
E ela nem nem, e ela nem nem
Bb         F   Bb         F
E ela nem nem, e ela nem nem

                     G7
Mas ela vem dançar o coco
                Dm7
E eu vou me arrumar
                   F       Bb
Na umbigada já ganhei seis
                F
Agora vou inteirar

           Bb          C7    F
São sete meninas, são sete fulô
            Em7(b5)   A7          Dm
São sete umbigada certeira que eu dou
           Bb          C7    F
São sete meninas, são sete fulô
             Bb       C7          F
São sete umbigada certeira que eu dou

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bb = X 1 3 3 3 1
C#° = X 4 5 3 5 3
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Em7(b5) = X X 2 3 3 3
F = 1 3 3 2 1 1
F/C = X 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
Gm7 = 3 X 3 3 3 X
