Dominguinhos - Seja Como Flor

Intro: ( C F C Dm Em Dm C F )
       ( C Dm Em Dm C F C Ab7M )

 C         F
Canta, coração
 C         Bb7M
Festa no olhar
 C         Bb7   A7  Ab7(#11)  G7
Um menino grande eu sou
 C         F
Sede de viver
 C         Bb7M
Fome de sonhar
 Am7      Dm7     G7    C  C7
Amanhã eu vou ver meu amor

F
Se o sol brilhar
C             Gm7 C7
Vai ser bom demais
F
Mas se chover

         C  Gm7 C7
Tá tudo bem
F
Amanhã eu vou
 C         Eb6
Seja como flor
                    D7
No peito eu tenho o sol
       C#7     C7   F   C
De um grande amor

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Ab7(#11) = 4 X 4 5 3 X
Ab7M = 4 X 5 5 4 X
Am7 = X 0 2 0 1 0
Bb7 = X 1 3 1 3 1
Bb7M = X 1 3 2 3 1
C = X 3 2 0 1 0
C#7 = X 4 3 4 2 X
C7 = X 3 2 3 1 X
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Eb6 = X X 1 3 1 3
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G7 = 3 5 3 4 3 3
Gm7 = 3 X 3 3 3 X
