Dominguinhos - Retrato da Vida

          E7       G#°      Am7
Esse matagal sem fim
       F#m7/5-     B7/#5  Em7
Essa estrada, esse rio seco
E7           G#°      Am7
Essa dor que mora em mim
       D7/9              G7M
Não descansa e nem dorme cedo
    F#m7/5-  B7/#5  Em   Em7/D      C7      B7/#5
O retrato da minha vida         é amar em segredo
E7         G#°     Am7
Não quer saber de mim
       F#m7/5-  B7/#5 Em7
E eu vivendo da tua vida
        Dm7     G7   C7M
Deus no céu e você aqui
      F#m7/5-  B7/#5     Em7
A esperança é quem me abriga
      C7M                     G7M
Esses campos não tardam em florir
        F#m7/5-  B7/#5   Em7
Já se espera uma boa colheita

  Am7     D7     G7M
E tudo parece seguir
  C7M     F#m7  B7/#5  Em7
Fazendo a vida tão direita
Am7     D7       G7M     F#m7/5-   B7/#5    Em7
Mas e você o que faz     que não repara no chão
Am7       D7        G7M   F#7              B7
Por onde tem que passar e pisa em meu coração
       E7             Am7        F#m7/5-   B7/#5   Em7
O teu beijo em meu destino   era tudo o que eu queria
        A#°          Em
Ser teu homem, teu menino
       C7      B7/#5  Em7
O ser amado de todo dia

----------------- Acordes -----------------
A#° = X 1 2 0 2 0
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
B7/#5 = X 2 X 2 4 3
C7 = X 3 2 3 1 X
C7M = X 3 2 0 0 X
D7 = X X 0 2 1 2
D7/9 = X 5 4 5 5 X
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
Em7/D = X X 0 0 3 0
F#7 = 2 4 2 3 2 2
F#m7 = 2 X 2 2 2 X
F#m7/5- = 2 X 2 2 1 X
G#° = 4 X 3 4 3 X
G7 = 3 5 3 4 3 3
G7M = 3 X 4 4 3 X
