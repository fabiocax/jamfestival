Dominguinhos - Pense N'eu

Intro: D F#m Em D (G D) D F#m Em D

D      A      G      D    Am  D
Pense n'eu quando em vez coração
       Am            D
Pense n'eu vez em quando
        Am           G
Onde estou, como estarei
       A              D
Se sorrindo ou se chorando
       C               D
Se sorrindo ou se chorando
        G    A            D
Pense n'eu.... / vez em quando
        G    A            D
Pense n'eu.... / vez em quando

        A             G           D
Tô na estrada, tô sorrindo apaixonado
                    Am         D
Pela gente e pelo povo do meu país/ olêrê...



     Am         D7            G
Tô feliz pois apesar do sofrimento
                   A              D
Vejo um mundo de alegria bem na raíz/ vamos lá...

     A         G         D
Alegria muita fé e esperança
                 Am           D
Na aliança pra fazer tudo melhor/ e será...

        D7                G
Felicidade o meu nome é união
                A            D
E povo unido é beleza mais maior

Solo D F#m Em D (G D) D F#m Em D

Repete tudo outra vez

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
