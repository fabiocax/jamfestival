Zezo - Meu Cenário

[Intro] Bbm  Eb  Fm
        C  Fm  C  Fm

[Primeira Parte]

                     Fm
Nos braços de uma morena
                     C
Quase morro um belo dia
                                 Fm
Ainda me lembro meu cenário de amor
                                         C
Um lampião aceso  o guarda-roupa escancarado
                C7                   Fm
Vestidinho amassado debaixo de um batom
               F7                  Bbm        C
Um copo de cerveja uma viola na parede e uma rede
                   Fm
Convidando a balançar
                                         C
Num cantinho da cama  um rádio a meio volume

                 C7                    Fm
E um cheiro de amor e de perfume pelo ar

[Segunda Parte]

        C                                   Fm
Numa esteira o meu sapato pisando o sapato dela
                                      C
Em cima da cadeira aquela minha bela sela
                C7                   Fm
Ao lado do meu velho alforje de caçador
          C           C7                      Fm
Que tentação minha morena me beijando feito abelha
                                      C
A lua malandrinha pela brechinha  da telha
        C7                    Fm
Fotografando meu cenário de amor

        C                                   Fm
Numa esteira o meu sapato pisando o sapato dela
                                      C
Em cima da cadeira aquela minha bela sela
                C7                   Fm
Ao lado do meu velho alforje de caçador
          C           C7                      Fm
Que tentação minha morena me beijando feito abelha
                                      C
A lua malandrinha pela brechinha  da telha
        C7                    Fm
Fotografando meu cenário de amor

( Bbm  Eb  Fm )
( C  Fm  C  Fm )

[Primeira Parte]

                     Fm
Nos braços de uma morena
                     C
Quase morro um belo dia
                                 Fm
Ainda me lembro meu cenário de amor
                                         C
Um lampião aceso  o guarda-roupa escancarado
                C7                   Fm
Vestidinho amassado debaixo de um batom
               F7                  Bbm        C
Um copo de cerveja uma viola na parede e uma rede
                   Fm
Convidando a balançar
                                         C
Num cantinho da cama  um rádio a meio volume
                 C7                    Fm
E um cheiro de amor e de perfume pelo ar

[Segunda Parte]

        C                                   Fm
Numa esteira o meu sapato pisando o sapato dela
                                      C
Em cima da cadeira aquela minha bela sela
                C7                   Fm
Ao lado do meu velho alforje de caçador
          C           C7                      Fm
Que tentação minha morena me beijando feito abelha
                                      C
A lua malandrinha pela brechinha  da telha
        C7                    Fm
Fotografando meu cenário de amor

        C                                   Fm
Numa esteira o meu sapato pisando o sapato dela
                                      C
Em cima da cadeira aquela minha bela sela
                C7                   Fm
Ao lado do meu velho alforje de caçador
          C           C7                      Fm
Que tentação minha morena me beijando feito abelha
                                      C
A lua malandrinha pela brechinha  da telha
        C7                    Fm
Fotografando meu cenário de amor

[Final] Bbm  Eb  Fm
        C  Fm  C  Fm
        C  Fm  C  Fm

----------------- Acordes -----------------
Bbm = X 1 3 3 2 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Eb = X 6 5 3 4 3
F7 = 1 3 1 2 1 1
Fm = 1 3 3 1 1 1
