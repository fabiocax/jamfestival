Zezo - Negue

Bm                        Em
Negue o seu amor o seu carinho
          F#            Bm     G F#7
Diga que você ja me esqueceu
Bm                     Em
Pise machucando com jeitinho
        F#7              Bm   Bbº
Esse coração que ainda é seu
A                        D
Diga que me pranto é corvadia
           G          F#7             Bm    B7
Mas não esqueça que você foi minha um dia

Em                 F#
Diga que já não me quer
G                F#
Negue que me pertenceu
     Bm      F#7     Bm
Eu mostro a boca molhada
    Em     Bm
E ainda marcada

      F#7   Bm   G F#7
Pelo beijo seu

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
Bbº = X 1 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
F#7 = 2 4 2 3 2 2
G = 3 2 0 0 0 3
