Gabriel Diniz - Coração Teimoso

[Intro] F#m  A  E  Bm

[Primeira Parte]

F#m                         D
Alguém me disse que você falou
                               A
Que se deu mal e que nunca pensou
                               E
Que eu ia fazer tanta falta assim
                             Bm
Tava bebendo e só falava em mim

[Pré-Refrão]

                             D
Tentei demais e você não deixou
                           A
Me expulsou e me mandou sumir
                         E
Trocou o certo pelo duvidoso

                             Bm
Coração teimoso só dá nisso aí

                             D
Tentei demais e você não deixou
                           A
Me expulsou e me mandou sumir
                         E
Trocou o certo pelo duvidoso
                             F#m
Coração teimoso só dá nisso aí

[Refrão]

         D         A
Quem chorava hoje ri
        E         F#m
Que sorria hoje chora
         D              A
Pra mim passou o tempo ruim
       E        F#m
O seu começou agora

         D         A
Quem chorava hoje ri
        E         F#m
Que sorria hoje chora
         D              A
Pra mim passou o tempo ruim
       E        F#m  D
O seu começou agora

( F#m  A  E  Bm  F#m )
( A  Bm )

[Pré-Refrão]

                             D
Tentei demais e você não deixou
                           A
Me expulsou e me mandou sumir
                         E
Trocou o certo pelo duvidoso
                             Bm
Coração teimoso só dá nisso aí

                             D
Tentei demais e você não deixou
                           A
Me expulsou e me mandou sumir
                         E
Trocou o certo pelo duvidoso
                             F#m
Coração teimoso só dá nisso aí

[Refrão]

         D         A
Quem chorava hoje ri
        E         F#m
Que sorria hoje chora
         D              A
Pra mim passou o tempo ruim
       E        F#m
O seu começou agora

         D         A
Quem chorava hoje ri
        E         F#m
Que sorria hoje chora
         D              A
Pra mim passou o tempo ruim
       E        F#m
O seu começou agora

         D         A
Quem chorava hoje ri
        E         F#m
Que sorria hoje chora
         D              A
Pra mim passou o tempo ruim
       E        F#m
O seu começou agora

         D         A
Quem chorava hoje ri
        E         F#m
Que sorria hoje chora
         D              A
Pra mim passou o tempo ruim
       E        F#m  D
O seu começou agora

[Final] F#m  A  E  Bm  D  E
        F#m  A  E  Bm  D  E  F#m

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
