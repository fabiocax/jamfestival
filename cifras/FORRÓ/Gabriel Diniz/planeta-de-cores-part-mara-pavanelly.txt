Gabriel Diniz - Planeta de Cores (part. Mara Pavanelly)

[Intro] Dm  Gm  C  Gm
        Dm  Gm  C  Gm

[Primeira Parte]

Dm                    Gm
Ei, não deixa como está
                 C
Ser mais uma história infinita
      Gm                   Dm
Quem sabe até vivemos uma vida

                 Gm
Se nos sentimos sós
                 C
Isso não quer dizer que já é tarde
       Dm
Sem futuro nenhum pra nós
 Gm                 Am Bb
Mas se não lembrares  mais
          C   Dm
Melhor recomeçar

                 Gm
Seguir o meu destino
                 C
Mesmo com toda dor que há aqui dentro
        Gm                     Dm
Não podemos bloquear nossos caminhos
                Gm                             C
Talvez descobriras que nossa história não foi só um minuto
       Dm
Sem futuro nenhum pra nós

[Pré-Refrão]

Am Bb                C  Dm
  Mas, se agora não dá mais
       C                                Gm
Pra viver, amar como antes, talvez com medo de querer

[Refrão]

         F                       C
Mas não ver que o amor não se esconde
            Gm                      Dm
Mesmo em silêncio pode se ouvir ao longe
        Bb                  F
Não se foge, não se pode negar o amor
             Eb                    Dm    Gm
Só entregar-se, a esse planeta de cores

( Dm  Gm  )

Dm                 Gm
É difícil pra nós dois
                  C
Também é muito injusto cobrar-nos
          Gm                         Dm
Se a lembranças sempre vem, nos emocionam
                  Gm
As coisas que eu dizia
              C                     Dm
E hoje com remorso me lembro que fazia você chorar

[Pré-Refrão]

Am Bb                C  Dm
  Mas, se agora não dá mais
       C                                Gm
Pra viver, amar como antes, talvez com medo de querer

[Refrão]

         F                       C
Mas não ver que o amor não se esconde
            Gm                      Dm
Mesmo em silêncio pode se ouvir ao longe
        Bb                  F
Não se foge, não se pode negar o amor
             Eb                    Dm    Gm
Só entregar-se, a esse planeta de cores

            F                           C
Mas porque não ver que o amor não se esconde
            Gm                      Dm
Mesmo em silêncio pode se ouvir ao longe
        Bb                  F
Não se foge, não se pode negar o amor
             Eb                    Dm    Gm
Só entregar-se, a esse planeta de cores

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
