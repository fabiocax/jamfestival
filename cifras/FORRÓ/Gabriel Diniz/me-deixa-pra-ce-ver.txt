Gabriel Diniz - Me Deixa Pra Cê Ver

[Intro]  A9  E  Bm  D  F#m  E

  A9
Toda vez que a gente briga
 E
Você toca nessa ferida
Bm
Diz que vai embora e que é o fim
D                              E
Depois liga e quer voltar pra mim

Bm                            D
Esse vai e volta é porque sabe
                      E
Que eu não vivo sem você, iê iê
                   Bm
Experimenta me deixar mais uma vez
D              E
Só mais uma vez

   A9
Me deixa procê ver

                              E
A besteira que eu vou acabar fazendo
                                Bm
Vou me enforcar com papel higiênico
               D                       F#m
Me jogar de cima da casinha de um cachorro
         E
Ai, socorro

   A9
Me deixa procê ver
                             E
Eu vou me afogar num aquário vazio
                           Bm
Me envenenar tomando Danoninho
               D                         F#m
Me jogar na frente de um carrinho de mercado
         E
Ai, coitada
                          D
Se acha mesmo que eu vou morrer
 E             A9
Me deixa procê ver

[Solo]  A9  E  Bm  D  F#m  E

Bm                         D
Esse vai e volta é porque sabe
                    E
Que eu não vivo sem você, iê iê
               Bm
Experimenta me deixar mais uma vez
             E
Só mais uma vez

   A9
Me deixa procê ver
                              E
A besteira que eu vou acabar fazendo
                                Bm
Vou me enforcar com papel higiênico
               D                       F#m
Me jogar de cima da casinha de um cachorro
         E
Ai, socorro

   A9
Me deixa procê ver
                             E
Eu vou me afogar num aquário vazio
                           Bm
Me envenenar tomando Danoninho
               D                         F#m
Me jogar na frente de um carrinho de mercado
         E
Ai, coitada

   A9
Me deixa procê ver
                             E
Eu vou me afogar num aquário vazio
                           Bm
Me envenenar tomando Danoninho
               D                         F#m
Me jogar na frente de um carrinho de mercado
         E
Ai, coitada

                          D
Se acha mesmo que eu vou morrer
 E             A9
Me deixa procê ver

----------------- Acordes -----------------
A9 = X 0 2 2 0 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
