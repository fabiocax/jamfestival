Gabriel Diniz - Minha Cara de Preocupação

Dm      Bb
Lá vem você de novo,
F                 C
com essa historia de ser eu.
Dm            Bb
Quer ter carro importado,
F               C
vacilou, que pra você não deu.
Dm                   Bb
To pegando, farreando só pegando mulherão,
F                      C
e você só consegue ser domador de dragão
Dm                   Bb
To pegando, farreando só pegando mulherão,
F                      C
e você só consegue ser domador de dragão
Dm      Bb                   F
Ei, peraê, deixe de me invejar,
                        C                    Dm
eu to fazendo o que, que você vem me incomodar.
     Bb                  F
Peraê, quer fazer igual a mim,

              C
então vai ter, vai ter que rebolar um pouquinho.

(Dm Bb F C)
rebola, rebola, rebola, rebola, rebola, rebola
rebola, rebola, rebola, rebola, rebola, rebola

Dm           Bb                F
Ta falando de mim, tem problema não
                    C                 Dm
olha aqui pra minha cara de preocupação,
          Bb                            F
To lindo, gostoso, cheio de dinheiro no bolso
                     C
sei que estou te incomodando, se preocupa invejoso.

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
