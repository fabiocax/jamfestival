Gabriel Diniz - Vai Trair 100

C#m
É
                 E
Dois a zero pra você
               B
Acabou de me perder
               F#
A primeira eu aceitei
                        C#m
A segunda eu não aceito mais

C#m                            E
Duas traições pra mim é o bastante
                                 B
Pra acabar com aquilo que era importante
                        F#
O amor e o fogo que me acendia
                                 C#m
Acabo de jogar um balde de água fria

                    E
Depois de tudo que fez, me quer de volta

       B                     F#
Eu não uso colete à prova de decepções
                       E   F#
Pra aguentar as suas traições

                  C#m
Quem não sabe o que quer
                 E
Não merece o que tem
              B9
Quem trai uma vez
                                F#
Pode ter certeza que vai trair 100

                C#m
Quem não sabe o que quer
                 E
Não merece o que tem
              B9
Quem trai uma vez
                                F#
Pode ter certeza que vai trair 100

                      C#m E B F#
Você vai acabar sem ninguém

----------------- Acordes -----------------
B = X 2 4 4 4 2
B9 = X 2 4 4 2 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
