﻿Cavaleiros do Forró - Se Reí Pra Lá

      Introdução: 2 Vezes

   Guitarrista 1, Base: E G#m E G#m C#m E F#


   Guitarrista 2, Solo:

  E|-11-9----------11-9----------11-9----------11-9--------]
  B|------12-9---9------12-9---9------12-9---9------12-9---]
  G|-------------------------------------------------------]
  D|-------------------------------------------------------]
  A|-------------------------------------------------------]
  E|-------------------------------------------------------]




  E|------9-11-11-12-11----12-11----12-11----12-11-------9b]
  B|-9-12---------------12-------12-------12----------12---]
  G|-------------------------------------------------------]
  D|-------------------------------------------------------]
  A|-------------------------------------------------------]
  E|-------------------------------------------------------]



   O Guitarrista 1 Começa Sozinho!

            B          E                C#m                F#
 Nunca pensei me enganar tanto com a mulher como foi com você
       B            E           C#m                  F#
 Você pensou que poderia ir e voltar me deixando a dispor


   O Guitarrista 2 entra agora!

  E|-11-9------------11-9------------11-9------------11-9--------------]
  B|------12-9-9---9------12-9-9---9------12-9-9---9------12-9-9-------]

 Mais uma dessas foi bem diferente não foi as amigas não foi o batente


  E|------9-11-12-11----12-11----12-11----12-11-------9b]
  B|-9-12------------12-------12-------12----------12---]

 Você abusou da minha conivência e ficou sem perdão


  E|-11-11-9------------11-9------------11-9------------11-9-----------]
  B|---------12-9-9---9------12-9-9---9------12-9-9---9------12-9-9----]

 Desfila com outro não tem cerimônia sai com meus amigos


  E|------9-11-12-11----12-11----12-11----12-11-------9b]
  B|-9-12------------12-------12-------12----------12---]

 Eu viro vergonha mais outra mulher ocupou seu lugar e o jogo virou


  E|-11-11-9------------11-9------------11-9------------11-9-----------]
  B|---------12-9-9---9------12-9-9---9------12-9-9---9------12-9-9----]

 Sê volta dengosa porque estou na moda isso é papa nos olhos


  E|------9-11-12-11----12-11----12-11----12-11-------9b]
  B|-9-12------------12-------12-------12----------12---]

 Deixe logo de prosa, se dá fora é um crime virei o xerife  aprenda minha lei


 REFRÃO!!!

             E             G#m         E                           G#m
 Se rei pra lá se rei para lá essa sua boca não mais vou beijar    |   Guitarrista 2,
          E                    C#m              F#               2X    Toca a Introdução
 Em orgulho de homem amor de mulher nunca vai mandar               |   durante o Refrão!


 Repete Introdução
 Repete a música apartir da parte: Sê volta dengosa...
 Repete a Introdução Para encerrar.



----------------- Acordes -----------------
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
