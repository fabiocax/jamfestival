Cavaleiros do Forró - A Música do Dia

Introdução: A D F#m E

A
Neguinha, eu nunca falo o que sinto por você
                           D             E
na frente dos amigos, eu pareço ser tão duro
                               D
comentam que eu estou muito seguro
    Bm                      E
e pensam até que eu não amo você
        Bm          Bm/A                      A
Pura bobagem, o meu coração só quer te ver amada
       F#m                           E
te ama mais que a ele mesmo e por direito
           D        E            A
esse meu jeito de durão é só faixada

A            E                              A
Porque eu te amo, aumenta esse volume loucutor
     F#m                        E
que agora eu vou falar pro meu amor

  D                E               A
tudo que eu não falei por tantos anos
A              E                               A
Por que eu te amo, falei mas não deixei de te amar
 F#m                         E
estou usando o rádio pra mostrar
D                E               A
ao mundo inteiro o quanto eu te amo!

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
Bm/A = X 0 4 4 3 X
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
