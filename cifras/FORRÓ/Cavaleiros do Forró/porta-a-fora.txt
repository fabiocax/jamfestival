Cavaleiros do Forró - Porta À Fora

C
Deixa a porta aberta quando for sair
Em
Quem sabe muda de idéia e vai querer voltar
Dm                             G
Vai ver que de repente a raiva passa
Em
Que eu vou estar aqui
Am
Esperando por você
     F
Não feche a porta
     G
Não diga nada
       Dm                    G
Mas se for embora vou enlouquecer

Refrão 2x:

              F
Se vai prosseguir

                     G
Tenha coragem e não para trás
           Em
Porque vai ver
                               Am
Um homem apaixonado que está chorando até demais
   F                                     G
E vai pedir pra você por favor não ir embora
   F                                     G
E vai pedir pra você nao sair de porta afora

*Depois de repetir o Refrão voltar pro início

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
