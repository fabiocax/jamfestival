Cavaleiros do Forró - Amor de Momento

[Intro]  F#m  D  A  E

A                       E             F#m
Quem me quiser é pra ficar só por prazer
                          D            A
Já me cansei de ouvir chorar meu coração
                        E            F#m
Por quantas vezes dei amor sem receber
                      D        E
Quando levei a sério foi decepção

        A                           E                   F#m
Faz um tempo que eu não sei o que é sofrer por ninguém
                  C#m                 D
Pode vê na minha cara que eu estou bem
                      A             E
Hoje eu vou e fico e nem sinto saudade
         A                        E              F#m
Nunca é tarde todo tempo é tempo pra se arrepender
                      C#m        D         A         E
Foi errando que eu aprendi a viver, e me amar de verdade


                        D                     A
Se me quiser vai ser assim, não vai gostar de mim
              E                  F#m
Só amor de momento é melhor pra você
                        D                     A
Se me quiser vai ser assim, não vai gostar de mim
              E                  F#m
Fique no passa tempo que não vai sofrer

----------------- Acordes -----------------
A = X 0 2 2 2 0
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
