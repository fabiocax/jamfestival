Cavaleiros do Forró - Não Pegue Esse Avião

Intro: G  Em  Am  D  G

G
Espere não vá...
Em
Agora acabou,
    Am
Me deixa falar
  D
Adeus eu já vou.

Intro: G  Em  Am  D  G

     G
Eu preciso te dizer que ela,
     Em
Não é quem você pensa não
     C                         D
Que pena já não pode ouvir, já vai saindo o avião...
G                   Em
Espere meu amor, não pegue esse avião

     C                           D
Naõ deixe quem te ama aqui, não leve o meu coração.
    G                        Em
Eu tenho tanto pra falar, eu tenho uma explicação
  C                         D
Você precisa acreditar, você tá fazendo confusão.
  Bm           Em                C
Não adianta me pedir pra esquecer,
                   D
Eu não aceito uma traição
Bm                  Em
Eu vi você beijando ela,
                          C
Até presente você deu pra ela,
                                 D
E por uma mulher mais nova você me trocou.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
