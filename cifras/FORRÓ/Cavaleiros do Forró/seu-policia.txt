Cavaleiros do Forró - Seu Polícia

 A9                E     D9               A9
Seu polícia não fecha o Porta Malas por favor.
 A9             E       D9                 A9
Seu polícia entenda foi a mulher que me deixou.

Introdução: A9, E, F#m, D9, A9, E, F#m, D

 A9                                      E
Amigo encoste esse carro na porta de um bar.
                               D9                      A9    E
Abre o porta malas e põe pra tocar, essa canção de saudade.
 A9                          E
Põe o volume tanto e deixa tocar.
                            D9
Quem sabe assim ela vai escutar.
E                  A9   A, G#m, F#m
Do outro lado da cidade.

E                                 D9
Garçon derruba ai 3 caixas de cerveja.
                      F#m                 E
Que eu só saio dessa mesa depois que clarear.

 E                                  D9
E se a polícia chegar pedindo o silêncio.
                     A9
Deixa que ela eu me entendo.
             E           A9  E
Agora eu já sei o que falar.

Refrão:
A9               E      D9                 A9
Seu polícia não fecha, o porta malas por favor.
A9              E      D9                A9
Seu polícia entenda, é só uma canção de amor.
A9               E      D9                 A9
Seu polícia não fecha, o porta malas por favor.
A9              E        D9                 A9
Seu polícia entenda, foi a mulher que me deixou.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
D = X X 0 2 3 2
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
