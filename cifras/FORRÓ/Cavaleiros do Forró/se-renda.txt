Cavaleiros do Forró - Se Renda

   A                             E
E quando a gente perde um grande amor
                       F#m
É que se aprende a dar valor.
            C#m
Por favor, entenda
   Bm
Então pra que fingir,
    A                 E
Melhor admitir, aqui agora, meu amor, se renda
      A                        E
Não quero nem pensar que sem mesmo começar
           F#m                  C#m
A nossa história pode ter um fim
       Bm             D       E       A   E
Se eu fosse você não ia tão longe de mim

Intro: A  E  F#m  C#m  Bm  A  E

    A                 E                 F#m  F#7
Eu não me arriscaria tanto se eu fosse você

     Bm                 D                E
Pois cartas viradas, podem te levar à perder
    A                            E            F#m   F#7
Pra que tanto orgulho, se tudo pode acabar em nada
      D
Quem hoje é incapaz de amar,
              A                     E
Amanhã pode estar chorando apaixonada
           C#m
Se você soubesse, o que eu sinto
             F#m
nem por um momento eu agiria assim
             Bm                  D
Por quê nas voltas que o mundo dá,
            A                        E
Você pode voltar correndo atrás de mim
   A                             E
E quando a gente perde um grande amor
                       F#m
É que se aprende a dar valor.
            C#m
Por favor, entenda
   Bm
Então pra que fingir,
    A                 E
Melhor admitir, aqui agora, meu amor, se renda
      A                        E
Não quero nem pensar que sem mesmo começar
           F#m                  C#m
A nossa história pode ter um fim
       Bm             D       E       A   E
Se eu fosse você não ia tão longe de mim

       Bm             D       E       A  C#m
Se eu fosse você não ia tão longe de mim
       Bm             D       E       A  C#m
Se eu fosse você não ia tão longe de mim
       Bm             D       E       A  C#m
Se eu fosse você não ia tão longe de mim
       Bm             D       E       A  E
Se eu fosse você não ia tão longe de mim

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#7 = 2 4 2 3 2 2
F#m = 2 4 4 2 2 2
