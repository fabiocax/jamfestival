Márcia Fellipe - Coração Vagabundo

G
Quando eu chorava apaixonada por você
Em
Quando eu fazia mil loucuras pra te ver
C
Você sorria
Am
E só sabia
B7
Me usar

G
Eu era uma válvula de escape em sua vida
Em
Feito um pneu reserva, última alternativa
C
Seu passatempo
Am
Improvisado
B7
Pra relaxar


C
Eu fazia tudo por você e você nada
Em
Vazio, me trocava por bebidas e baladas
C                                 Am
Agora que você provou do seu próprio veneno
B7
Deixaram esse seu coração bandido doendo

C               Em                D
Você vem de novo me amar, mas não me iludo
C                    B7
Volta pra bebida, pra balada, pra o seu mundo

Em
Vai curar tua dor com cachaça
C
Vai curar tua dor com cachaça
G                  D
Eu não sou remédio pra coração vagabundo
C                 B7
Eu não sou remédio pra coração vagabundo (2x)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
