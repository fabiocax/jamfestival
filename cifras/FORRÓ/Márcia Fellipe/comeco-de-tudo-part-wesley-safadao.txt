Márcia Fellipe - Começo de Tudo (Participação Wesley Safadão)

F                                    Am
Desde o começo de tudo você em primeiro lugar
         A#
Na minha vida
          Dm                 C
Depois era em mim que eu ia pensar
F                                       Am
Tava estampado nas fotos e declarações escritas
           A#
Um amor sem medidas
          Dm
Eu não pensava que um dia
           C                    A#
Essas recordações iam me fazer chorar


A#        C       Dm
E o que mais me dói é saber
            Am                 Gm
Que da sua boca saiu tantas mentiras
            Dm            Am
Falando de amor, jurando amor

        A#
Nunca pensei que você me trairia

A#        C       Dm
E o que mais me dói é saber
            Am                 Gm
Que da sua boca saiu tantas mentiras
            Dm            Am
Falando de amor, jurando amor
       A#                 C
Nunca pensei que você me trairia


  Dm                      A#
Se ainda tiver vergonha nessa cara
    F            C
Desapareça e não me ligue mais
  Dm                      A#
Se ainda tiver vergonha nessa cara
    F                     C
Por favor me esqueça e não me ligue mais

  Dm                      A#
Se ainda tiver vergonha nessa cara
    F            C
Desapareça e não me ligue mais
  Dm                      A#
Se ainda tiver vergonha nessa cara
              F                     C
Por favor me esqueça e não me ligue mais

----------------- Acordes -----------------
A# = X 1 3 3 3 1
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
