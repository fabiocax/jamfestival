Márcia Fellipe - Quatro Sorrisos

F#                    D#m
Eu quero aproveitar você
                     B9
Cada segundo que eu tiver ao seu lado
  Bm                                   F#
Quero te amar como se não houvesse amanhã
                                   D#m
Já decorei seus quatro tipos de sorrisos
                                 B9
Tem um sem graça que me tira o juízo
                  Bm
E o exagerado e aquele depois do amor
                 F#            D#m
Me pedindo um abraço
                                    G#
Você desperta um lado meu que é só seu
                                    B9
E eu desperto um lado seu que é só meu
          C#
Que é só meu
F#                           G#
E quando chego perto da sua boca

                            B9
A gravidade vai perdendo a força
                                  Bm
Quando eu te beijo até o relógio para
                            F#
O nosso amor já tá fazendo mágica
                             G#
E quando chego perto da sua boca
                            B9
A gravidade vai perdendo a força
                                  Bm
Quando eu te beijo até o relógio para
                            F#     G#   B9   Bm7
O nosso amor já tá fazendo mágica

D#m7                                      G#
   Você desperta um lado meu que é só seu
                                    B9
E eu desperto um lado seu que é só meu
          C#
Que é só meu
F#                           G#
E quando chego perto da sua boca
                            B9
A gravidade vai perdendo a força
                                  Bm
Quando eu te beijo até o relógio para
                            F#
O nosso amor já tá fazendo mágica
                             G#
E quando chego perto da sua boca
                            B9
A gravidade vai perdendo a força
                                  Bm
Quando eu te beijo até o relógio para
                            F#     G#   B9   Bm
O nosso amor já tá fazendo mágica

----------------- Acordes -----------------
B9 = X 2 4 4 2 2
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
C# = X 4 6 6 6 4
D#m = X X 1 3 4 2
D#m7 = X X 1 3 2 2
F# = 2 4 4 3 2 2
G# = 4 3 1 1 1 4
