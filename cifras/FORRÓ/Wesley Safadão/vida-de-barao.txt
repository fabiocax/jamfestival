Wesley Safadão - Vida de Barão

Intro: Cm Bm Cm Bm Cm Bm Cm Bm
       A#m Am A#m Am A#m Am A#m  (2x)

   Cm
Comprei um jet ski e um 4x4
   A#
tô na casa de praia com as gatinhas do meu lado
   Cm
A midia me conhece, comigo é na pressão
    A#
Preocupação é zero, tenho vida de barão
    Cm
Comprei uma Ranger Hover e um fuscão tunado
 A#
Casa nos States eu sou mesmo exagerado
  Cm
Uma conta poupança gorda pra chuchu
  A#                                     Cm
Cinco namoradas me dizendo I love you

( Cm  D  D# )


D#                                 D
Chega a mulherada vai rolar um festão no meu apê
D#                                D  D# D  D#  D
Preparei um churrasco o som tá ligado agora é só fazer..

( Gm  F  D )

Gm           D#               A#
Pererê, pererê, parará, parará
           F                       Gm
Chega mulherada que o bixo vai pegar
             D#               A#
Pererê, pererê, parará, parará

           F                      Gm
Vida de barão eu não posso reclamar...

( A  A#  Cm )

----------------- Acordes -----------------
A = X 0 2 2 2 0
A# = X 1 3 3 3 1
A#m = X 1 3 3 2 1
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D# = X 6 5 3 4 3
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
