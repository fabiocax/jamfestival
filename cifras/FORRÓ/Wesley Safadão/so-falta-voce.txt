Wesley Safadão - Só Falta Você

(intro) A  E  F#m  D (2x)  A  E  Bm  D (2x)

A                    E             F#m
Posso até fazer de tudo pra você voltar
                D                 A
E mudar minha rotina só pra te agradar
                     E                F#m
Se você não está por perto não sei me cuidar
               D                A
Eu te amo de verdade pode acreditar

  A                E               F#m
Você é caminho, a vida meu porto seguro
                   D                   A
Meu destino meu passado presente e futuro
                   E                 F#m
Fiz promessas de joelhos pra te esquecer
                   D                   A
Nem um santo me atendeu e eu ja sei porque

    Bm                D           E
Meu amor não vejo a hora de você voltar

    Bm                D           E         E7
E me tirar dessa agonia de te esperar   ah ah aah.

A               E                F#m
To de olho na estrada esperando você
                D               A
Coração ta preparado pra te receber
                E              F#m
To usando o perfume que você adora
                D                      A
E a canção que você gosta tá tocando agora

A               E                F#m
To de olho na estrada esperando você
                D               A
Coração ta preparado pra te receber
                E              F#m
To usando o perfume que você adora
                D                      A
E a canção que você gosta tá tocando agora

     E      Bm      D     A
Meu bem querer só falta você
     E      Bm      D     A
meu bem querer só falta você
     E      Bm      D     A
Meu bem querer só falta você
     E      Bm      D     A
Meu bem querer só falta você

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
