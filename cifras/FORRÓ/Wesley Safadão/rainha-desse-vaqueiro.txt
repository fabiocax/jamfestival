Wesley Safadão - Rainha Desse Vaqueiro

A               E                          D
Ô mulher você é linda, és a linda das mais lindas,
                 A
Igual a você não tem
                      E                      D
És a flor que solta o cheiro, rainha desse vaqueiro,
                 A
Você só me faz o bem
                 E                     D
Meu coração é só teu, já reconheci que eu sem você não
       A
Sou ninguém
                 E                        D
Teu abraço me esquenta, seu cheiro me alimenta, ô
                A
Mulher querida amada
                E                     D
Você é minha paixão, a deusa dessa canção, estrela da
        A
Minha estrada
                 E                          D
Valeu eu te conhecer e o que eu sinto por você nem a

              A
Mão do tempo apaga
                E                           D
Quando você ma abraça, sorrindo diz em voz baixa, as
                  A
Coisas lindas que faz
                     E
Lá no quarto ou no banheiro, tento olhar seu corpo
   D                         A
Inteiro, bem feitin lindo demais
                    E                       D
Só faz a paixão crescer, que mulher como você todo
             A
Homem anda atrás
                      E                     D
Eu vou falar pro seus pais, que eu te amo demais e não
            A
Quero nem saber
                   E                       D
Se eles não concordar, for contra nós se casar, eu vou
           A
Carregar você
                        E
Nem que o céu caia em pedaços, só pra lhe ter em meus
D                            A
Braços não tenho medo de morrer
                  E                         D
Mulher tu és um encanto, de todos filmes românticos tu
                A
És a mais linda cena
                 E                         D
Foi deus e a natureza, quem te deu tanta beleza, tão
                 A
Deslumbrante e serena
                E                          D
És uma tremenda gata, daquelas que a morte mata,
                      A
Depois vai chorar com pena
                 E
Sou vaqueiro apaixonado, quero estar sempre ao seu
D                       A
Lado, lhe amar constantemente
                  E                   D
Tudo em você me atrai, amor é lindo demais esse
           A
Romance da gente
                E                     D
Você é minha paixão, vem amor me dê a mão e vamos ser
           A
Feliz pra sempre

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
