Wesley Safadão - Pra Ser Safadão

Bm

E|-2-0--------------------|
B|-----3-2----------------|
G|---------4--------------|
D|------------------------|
A|------------------------|
E|------------------------|


Bm                G
Ela me deu um gelo
                         D
Mas se arrependeu do erro

Quando viu que ia me perder
                       A
Correu atrás, pediu perdão
                              F#
Só que eu não quis mais nem saber


 Em
Já tava curtindo solteiro
              Bm
Na maior azaração

G               D
Pra ser safadão
                     A
Tem que ser bom de papo
                   Bm
Tem que ter disposição
G               D
Pra ser safadão
                 A
Tem que ter atitude
                       Bm
E sempre um trident na mão

                           G
Rasguei o meu papel de trouxa
                 D      A
E a mulherada ficou louca

Parti pra pegação
                Bm
E hoje tem curtição
               A
Parti pra pegação
                             Bm
Pra dar um refresco pro coração
                G
Pega esse chiclete
            A               Bm
E taca um beijo aqui no safadão

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
