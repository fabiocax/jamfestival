Wesley Safadão - Quebrou a Cara

Intro:  E B F#m A

E             B
Te dei meu coração,
           F#m                   A                   E         B
Já mandei cartas, te dei flores e você ainda quer brigar comigo? hum hum
         F#m            A                      E                B
Eu me humilho me declaro e você em troca só me dá castigo? huu hum
        A                B   B7
Eu não aguento não, o meu pobre coração
C#m                G#m
Hoje acordei mais cedo
                 A
Me olhei pelo espelho e vi
        B    B7
Tava morrendo de amor
C#m             G#m                       A B
Quando digo que é verdade vc diz que estou mentindo

              E                    B                    C#m
Eu resolvi parar, Dá um basta nisso de uma vez por todas

               G#m              A
Por favor não ligue, já estou com outra
                        E                    B
Que me carinho, amor e atenção... E você não deu não
          E                   B             C#m
Sua mae falou, que quando me perdesse iria dar valor
                G#m        A
Falei que não é digna do meu amor
         B             E         B
Pagou pra ver quebrou a cara

              E                   B                     C#m
Eu resolvi parar, Dá um basta nisso de uma vez por todas
               G#m              A
Por favor não ligue, já estou com outra
                        E                    B
Que me carinho, amor e atenção... E você não deu não
          E                   B             C#m
Sua mae falou, que quando me perdesse iria dar valor
                G#m        A
Falei que não é digna do meu amor
         B             E
Pagou pra ver quebrou a cara

   B    F#m             A             E
Huu Huu Huum... Pagou pra ver quebrou a cara
   B    F#m      A
Huu Huu Huum  Hummm

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
