Wesley Safadão - Zuar e Beber

F             Am                  F
Eu vou zuar e beber vou locar uma Van
           G                   F
E levar a mulherada lá pro meu AP
                    Am
Que é pra gente beber

Introdução: F Am G 2 x

         F                    Am
Hoje tem farra Vou fazer o movimento
                F                        G
Lá no meu apartamento Entrou, gostou , gamou, quer mais...
  F                         Am                    F
Já preparei, abasteci a geladeira Tá lotada de cerveja
                G
O muído vai ser bom demais...
        Dm       Am              Dm      Am
O prédio vai balançar Quando a galera dançar
      Dm       Am          G
E a cachaça subir Fazer zum, zum...

        Dm         Am             Dm       Am
Não tem hora pra parar O cheiro de amor no ar
           Dm     Am          G
Vai todo mundo pirar E ficar nu (todo mundo nu!)... a
F            Am                  F
Eu vou zuar e beber vou locar uma Van
           G                  F
E levar a mulherada lá pro meu AP
                   Am                 F                Am
Que é pra gente beber E depois paragadá...pa ra ra, pa ra ra
              F                    Am
E depois paragadá, pa ra ra, pa pa ra... (2X)

Intro 2x: Am F G

         F                    Am
Hoje tem farra Vou fazer o movimento
                F                        G
Lá no meu apartamento Entrou, gostou , gamou, quer mais...
        F                    Am                 F
Já preparei, abasteci a geladeira Tá lotada de cerveja
                G
O muído vai ser bom demais...
          Dm      Am              Dm     Am
O prédio vai balançar Quando a galera dançar
      Dm        Am        G
E a cachaça subir Fazer zum, zum...
        Dm          Am          Dm          Am
Não tem hora pra parar O cheiro de amor no ar
        Dm        Am           G
Vai todo mundo pirar e ficar nu (todo mundo nu!)...

F             Am                  F
Eu vou zuar e beber vou locar uma Van
           G                   F
E levar a mulherada lá pro meu AP
                  Am                  F                Am
Que é pra gente beber E depois paragadá...pa ra ra, pa ra ra
              F                    Am
E depois paragadá, pa ra ra, pa pa ra... (2x)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
