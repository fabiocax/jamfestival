Wesley Safadão - Eu Tô Pagando

Introdução;:. Bm A 2x

 Bm
Meu carrão zerado de mulher lotado

Wísk importado, o som ligado
                    A
Hoje é farra, é só zueira, é só zueira...
 Bm
Eu tô bem na fita, só mulher bonita

Taca elas cagita na birita
                 A
E não perde a saideira...
Bm
Chego no bar, chamo o garçom
                      Am
E digo logo: Aumenta o som!

Traga a bebida e traga gelo (E pique gelo!)
Bm
Assim tá bom, tá muito bom eu tô pagando seu garçom

                A                   E
Hoje à noite é desmantelo e liso aqui só meus cabelos...

A             Bm
Eu tô pagando

Quem paga é quem tem razão
A             Bm
Eu tô pagando

Eu quero é agitação
A             Bm
Eu tô pagando

Deixa o som tocar aí
    D                      E
Se eu pago é pra me divertir...
A             Bm
Eu tô pagando

Meu dinheiro é pra luxar
A             Bm
Eu tô pagando

Deixa a vida me levar
A             Bm
Eu tô pagando

Deixa o som tocar aí
    D                   E
Se eu pago eu quero é curtir...

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
