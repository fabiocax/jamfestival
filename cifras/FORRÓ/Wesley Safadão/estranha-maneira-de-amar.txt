Wesley Safadão - Estranha Maneira de Amar

G               C           G                  C
  Parece que o vento leva noticias de mim pra você
G               C           G                  C
  Parece que o vento traz noticias de você pra mim
D9                                     C9
  Parece que as noites se tornam mais frias longe de você

                     Em
    E a danada da saudade, vem
D9
  Chega e mostra
               C
Quando a gente gosta saída não tem

     Em                  A                   A9
  Perdi as contas que a gente se deixou e voltou
            C                D
E cada reencontro se amando mais
          C                           Am           D
Mas na verdade é que o amor não nos deixa sendo rivais


Refrão:
          C     G                 C                   G
E por que   será   essa nossa estranha maneira de amar?
        Em       Bm
Pra que    brigar?
                 C           D       G
Se a gente se separa mas nunca se deixa

          C      G                  C                  G
E por que   será    essa nossa estranha maneira de amar?
        Em       Bm
Pra que    brigar?
                 C           D       G
Se a gente se separa mas nunca se deixa

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
D9 = X X 0 2 3 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
