Wesley Safadão - Disco Voador

 F#m                    E                D
Ah se eu pegasse uma Carona num Disco voador
      F#m                     E                D  (D A)
Eu ia Ate o infinito e gritaria apaixonado estou
        Bm                     F#m                     E
Transformaria o Arco-iris em forma de um coração pra ela
    Bm                  F#m                     E
Ia até o jardim do éden e traria um buque de flores
               F#m
Nos quatro cantos do mundo,
       E                     A              D
Escreveria o seu nome pro planeta saber desse amor
       F#m                  E                A                     D     (D A)
Meu pensamento ultrapassa a realidade mais é verdade quando falo de amor.
F#m         A          E
Deixa pelo menos eu sonhar...

(Refrão 2x)
                D
Eu só queria voar
                  E
Pra ir buscar no céu,

                F#m
Uma estrela pra ela
                  D
A se eu soubesse voar
                  E
Pra ir buscar no céu,
                 F#m
Uma estrela pra ela


(Final)

F#m                     E                  D
Ah se eu pegasse uma carona num disco voador .

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
