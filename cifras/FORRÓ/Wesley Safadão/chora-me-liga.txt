Wesley Safadão - Chora, Me Liga

Introdução: C - G - Am - F (2x)

   C                          G
e|------------------------------|
B|-5555-------------------------|
G|------5555------------------7-|
D|-----------5555--5-7-5-7-5----|
A|------------------------------|
E|------------------------------|

             Am              F
e|-----------------------------|
B|-----------5--\5-555-5-6-5---|
G|---------------------------5-|
D|-5-77-5-7--------------------|
A|-----------------------------|
E|-----------------------------|


C
Não era pra você se apaixonar

     G
Era só pra gente ficar
          Am                   F
Eu te avisei meu bem eu te avisei
C
Você sabia que eu era assim
               G
Paixão de uma noite que logo tem fim
       Am                    F
Eu te falei meu bem eu te falei

                         Dm
Não vai ser tão fácil assim
                 F
Você me ter nas mãos
                  C
Logo você que era acostumada
     G/B                 Am
A brincar com outro coração

                    Dm
Não venha me perguntar
                F
Qual a melhor saída
                     Dm
Eu sofri muito por amor
          F            G
Agora eu vou curtir a vida

     C
     Chora, me liga, implora
                   G
     Meu beijo de novo
R               Am
e    Me pede socorro
f                         F
r    Quem sabe eu vou te salvar
ã
o    C
     Chora, me liga, implora
                G
     Pelo meu amor
                 Am
     Pede por favor
                   F              G       C
     Quem sabe um dia eu volto a te procurar

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
