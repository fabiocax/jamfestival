Wesley Safadão - Cotovelo No Gelo (Prova Bêbada Viva)

[Intro] G  D  Em7  C
        G  D  Em7  C

[Primeira Parte]

               Am
Oito quilos a menos
                C
A barba tá crescida
                 G
Auto estima no chão

Até meu coração
      D
tá errando as batidas

[Segunda Parte]

                   Am
To tomando whisky puro
                      C
Porque o baldinho de gelo

       G                        D
Tá ocupado com meu braço dentro
                      Am            C
Curando a dor de Cotovelo tão doída
                     D
Eu sou prova bêbada viva

[Refrão]

                     G
Que de amor ninguém morre
               D
A saudade maltrata
               Em7
Mas matar não mata
               C
Mas matar não mata

                     G
Que de amor ninguém morre
               D
A saudade maltrata
               Em7
Mas matar não mata
               C
Mas matar não mata
            G
Sofre hoje, e amanhã passa

( D  Em7  C )

               Am
Oito quilos a menos
                C
A barba tá crescida
                 G
Auto estima no chão

Até meu coração
      D
tá errando as batidas

[Segunda Parte]

                   Am
To tomando whisky puro
                      C
Porque o baldinho de gelo
       G                        D
Tá ocupado com meu braço dentro
                      Am            C
Curando a dor de Cotovelo tão doída
                     D
Eu sou prova bêbada viva

[Refrão]

                     G
Que de amor ninguém morre
               D
A saudade maltrata
               Em7
Mas matar não mata
               C
Mas matar não mata

                     G
Que de amor ninguém morre
               D
A saudade maltrata
               Em7
Mas matar não mata
               C
Mas matar não mata
            G
Sofre hoje, e amanhã passa

[Final] G  D  Em7  C
        G  D  Em7  C

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
