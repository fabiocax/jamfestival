Wesley Safadão - Onde Tem Ódio Tem Amor

Dm
Tá falando que eu virei passado
           Bb
Que já "tá" até de novo namorado
Cm              C
 Será? Será? Será?

Dm
Diz que criou foi nojo de mim
              Bb                     Cm
Agora pra você eu "tô" morto enterrado
               C
Será? Será? Será?

Dm
Diz que me odeia
Bb
Mas teu coração me ama
Cm
Diz que me quer longe
             C
Mas sonha nós dois de conchinha na cama


Dm
Todo mundo sabe
Bb
Que por mim você é louca
Cm
Você saiu da minha vida

Mas o meu nome
   A                 Dm
Não saiu da sua boca

Dm
Onde tem ódio tem amor
Bb
Onde tem fumaça tem fogo
Cm
Do jeito que as coisas estão andando

Você não "tá" me enganado
A                      Dm
Nós vamos é voltar de novo

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
Dm = X X 0 2 3 1
