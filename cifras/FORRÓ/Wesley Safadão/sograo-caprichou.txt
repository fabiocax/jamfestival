Wesley Safadão - Sogrão Caprichou

Intro: F Bb C Dm

   Dm
No espelho do quarto, prepara o arsenal
Dm
Vem de vermelho, tô passando mal
     A
Ela dança envolvente, mexendo a cintura
   A
Mané quando olha derrapa na curva

    Dm
Faz carinha de santa, despede do pai
      Dm
Volto cedo, juro! Sogrão, eu cuido
     A
Ele nem imagina o que a filha é capaz
         A
Tem que ser censurado o trupé que ela faz



          Bb       C     Dm
Perto de papai você é santinha
                     A                Dm
Quando o sogrão nâo tá, você perde a linha
           Bb      C     Dm
Perto de papai você é santinha
                     A                Dm
Quando o sogrão nâo tá, você perde a linha

              Bb                  F
Não vai embora não, deita aqui na cama
                     C                       Dm
Se seu pai te perguntar, você tá com Luan Santana
                Bb                     F
Vou falar bem baixinho, que é pra você saber
             C
Sogrão caprichou na hora de fazer você hein

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
