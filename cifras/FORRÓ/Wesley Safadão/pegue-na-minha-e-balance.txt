Wesley Safadão - Pegue Na Minha e Balance

Intro:. Dm A reperta varias vezes

Dm                  Em
 Sei que você gosta de dançar
 F                 G
 reboladinha no meio do salão
  Dm            Em                F
 Não se sinta só pegue na minha mão
 G
 Pegue na minha mão

Dm
 Corpo suado sentindo a pulsação
 Em                              F
 Não se sinta só pegue na minha mão
 G
 Pegue na minha mão

Dm                               A
 Mas se você quiser algo mais emocionante
Dm        A         Dm
 Pegue na minha e balançe

Dm        A         Dm
 Pegue na minha e balançe
Dm        A         Dm
 Pegue na minha e balançe
 Dm        A         Dm
 Pegue na minha e balançe
Dm        A         Dm
 Pegue na minha e balançe
Dm
 Pegue na minha, Pegue na minha
A
 Pegue na minha, Pegue na minha
Dm
 Pegue na minha, Pegue na minha
A
 Pegue na minha, Pegue na minha
Dm
 Pegue na minha, Pegue na minha

----------------- Acordes -----------------
A = X 0 2 2 2 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
