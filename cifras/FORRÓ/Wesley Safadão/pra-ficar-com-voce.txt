Wesley Safadão - Pra Ficar Com Você

[Intro]  Dm  C

Dm
Passe o tempo que passar
                                                  C
Sempre vou estar aqui te esperando (te esperando)
Dm
Nossa hora vai chegar
                                           C
Pra gente realizar nossos planos (nossos planos)

             Dm                                           F
Deixa o povo falar, duvidar do nosso amor (nosso amor)
           C
Na distância, estou sempre presente
              G
O que importa é o que a gente sente

               Dm                                           F
Ninguém vai conseguir destruir o que vivemos
                 C
Porque a gente se ama, a gente se ama

A gente se ama

                           Dm
Se eu tiver namorando, eu separo
                       F
Se eu tiver casado, eu descaso
          C
Pra ficar com você, pra ficar com você
Pra ficar com você

----------------- Acordes -----------------
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
