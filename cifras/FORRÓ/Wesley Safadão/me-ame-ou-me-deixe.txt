Wesley Safadão - Me Ame Ou Me Deixe

Am
Tá doendo, tá

Mas eu preciso te falar
F                   C
Cansei de amar sozinho
              G
e dar meus carinhos

Am
Só pra te lembrar

Se coloque em meu lugar
F                   C
Suas palavras ao vento
              G
Não tem sentimento

Am
Quando você diz que me quer
F                      C
O meu coração fica maluco

             G
e enche de duvida

Am
Realmente se me quiser
F                  C
Vê se muda de atitude
               G
Aceite é sua culpa

  Dm                      F
Porque não depende só de mim
                    C
Para o nosso amor viver
                 G
eu te dou meu coração
                  Dm
É só você me perceber

                   F
Não depende só de mim
                    C
Para o nosso amor durar
                 G
em nome dessa paixão
                        Dm  F
Você também tem que me amar

    C
Me ame ou me deixe
               G
Me ame ou me deixe

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
