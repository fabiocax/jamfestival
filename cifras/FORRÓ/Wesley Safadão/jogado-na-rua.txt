Wesley Safadão - Jogado Na Rua

(D# A# Cm G# D# A# Fm G# A#)

        D#
Não tá fácil ficar
    A#
sem teu amor aqui
        Fm
Que saudade que dá
            G#     D#
chego a ter dó de mim
     D#
Meu amor é assim
            A#
bruto e sincero demais
      Fm
Sentimento sem fim
      G#       D#     D#7
amo você ninguém mais
      G#
Me deixou assim
                Cm
sombra na escuridão

     G#            Fm
Arrancou de mim, a paz
                A#    D#
o sorriso e a razão
      A#
É no som da viola
              Cm
que o peito chora
        G#
Nessas noites de lua
           D#
jogado na rua
      A#   Fm
Ai, Amor
         G#
Nessas noites de lua
           D#
jogado na rua

----------------- Acordes -----------------
A# = X 1 3 3 3 1
Cm = X 3 5 5 4 3
D# = X 6 5 3 4 3
D#7 = X 6 5 6 4 X
Fm = 1 3 3 1 1 1
G# = 4 3 1 1 1 4
