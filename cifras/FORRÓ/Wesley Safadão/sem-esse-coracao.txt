Wesley Safadão - Sem Esse Coração

(intro) Gm  Bb  F  C  Gm

E|------------------------------
B|-------------3-1------4/3-1---
G|---------2-3----3/2--------3--
D|-5-5-5-5------------5-------5~
A|------------------------------
E|------------------------------

      F                         Gm  Bb
Eu queria, amar um pouco menos você
          F                              Gm  Bb
O meu coração, só apanha e vai parar de bater
         F        C          F      Gm  Bb
Se eu pudesse, tirava esse meu coração
       F                 C       F        Gm
Pra deletar a saudade e o toque das suas mãos

    D#9         F
E parava de sofrer
    D#9         F
E parava de sofrer


(refrão)
Gm                  D#9
E mesmo assim, sem esse coração
         Bb                    F
Eu acho que vou continuar te amando
Gm                  D#9
E mesmo assim, sem esse coração
         Bb                    F
Eu acho que vou continuar chorando

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D#9 = X 6 8 8 6 6
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
