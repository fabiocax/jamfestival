Wesley Safadão - Jejum de Amor

Intro: E  C#m  B  A  E  B  C#m  B  A  E

Solo:

E|------------------------------------------------------------|
B|-----------5-4-5--5-5-5-5-4-4-4-2-0--0-2-0-2-0-0p2-0--------|
G|--4-4-4--4--------------------------------------------------|
D|-----------------------------------------------------4-4p6--|
A|------------------------------------------------------------|
E|------------------------------------------------------------|

E|------------------------------------------------------------|
B|-----------5-4-4p7-5--5-5-5-5-4-4-4-2-0---------------------|
G|--4-4-4--4------------------------------1-2-1-2-4-4p6-4-----|
D|--------------------------------------------------------4-6-|
A|------------------------------------------------------------|
E|------------------------------------------------------------|

             C#m                 B
Não sou de ficar mais de uma semana sem amor
A                           E
Sem um beijo, sem um corpo, tá difícil

             C#m           B
Deixa eu te falar, eu vou fazer jejum de amor
A
Pra provar o que eu sinto

E              B             C#m
Quer saber? Eu to arrependido
                             G#m
Porque não falei no seu ouvido
                     A
As frases de amor que não falei
            E
Em nosso cobertor

      B        C#m                   G#m
Sentimento proibido, até parece impossível
                    A                       B
Quer saber? Eu necessito ter de volta seu calor

            C#m
Sua boca é minha
           A
Domina a minha
               E                            B
E o que é que as duas estão fazendo tão sozinhas? (2x)

                        C#m B A E B C#m B A E
Ai, ai, ai, boca minha

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
G#m = 4 6 6 4 4 4
