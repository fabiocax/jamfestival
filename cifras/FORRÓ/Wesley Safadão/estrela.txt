Wesley Safadão - Estrela

E
  Sempre quando chega a noite
A
Estrelas vem iluminar
E
  E me fazem sonhar
  A
Imaginar você aqui
              B
Preciso de você
             E   A
Só pra me guiar

E
  Minha estrela não se esconda
 A
Pois sei que, eu vou te encontrar
E                     A
  Vamos viajar e acender de vez
                              B
A nossa paixão, virar constelação

            E   B
Num só coração

Refrão:
    E                B
Estrela, por favor escute
                   C#m
O que eu vou falar
           A                        E
Quero te amar mesmo que o sol aparecer
              B                 C#m
Eu não vou mudar mais de opinião
                A
Não me deixe aqui no chão

    E                B
Estrela, por favor escute
                   C#m
O que eu vou falar
           A                        E
Quero te amar mesmo que o sol aparecer
              B                  C#m
Eu não vou mudar mais de opinião
                A
Não me deixe aqui no chão
B7
  Estrela do meu coração...

(repete tudo)

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
