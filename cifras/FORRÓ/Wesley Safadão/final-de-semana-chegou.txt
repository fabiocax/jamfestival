Wesley Safadão - Final de Semana Chegou

Bm      G                  A
Alô, o final de semana chegou
                           Bm
Em casa é que eu não vou ficar
           G                   A
É sexta-feira, eu vou sair, vou farrear

          Bm                      G
É sexta-feira, vou sair, vou farrear
                  D
Tô a fim de paquerar
                     A
Vou chamar logo as gatinhas
                     Bm
Vou rodar de bar em bar
                   G
Vou ligar meu paredão
                     D
Vou botar muita pressão
                       A          (A,G#,G) ----> caída
E as gatinhas muito loucas vão ficar


G
Vou misturar wísk, Red Bull
              D
Eu tô na mídia
                                         Em
Pra curar a ressaca meia dúzia de latinhas

Eu vou curtir, eu vou rasgar, eu vou beber
    G                          A
Eu sou o Safadão por isso eu boto é pra fuder!

Bm      G                  A
Alô, o final de semana chegou
                           Bm
Em casa é que eu não vou ficar
           G                   A
É sexta-feira, eu vou sair, vou farrear  (2x)

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
