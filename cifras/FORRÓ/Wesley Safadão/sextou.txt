Wesley Safadão - Sextou

Solo intro

Parte 1:
E|------------------------------------------
B|--8-8p6--4p3-4p3-4p3-4p3-4p3--------------
G|------------------------------5-----------
D|------------------------------------------
A|------------------------------------------
E|------------------------------------------ (2x)

Parte 2:

E|------------------------------------------
B|--9-9p8--6p4h6----9-9p8-6p4h6-------------
G|------------------------------------------
D|------------------------------------------
A|------------------------------------------
E|------------------------------------------

Parte 3:


E|------------------------------------------
B|--4--3h4p3h4p3h4p3h4--/8------------------
G|------------------------------------------
D|------------------------------------------
A|------------------------------------------
E|------------------------------------------

Cm
Toda segunda feira acordo cedo para trabalhar
G#
À noite a faculdade não me deixa descansar
Fm
É só correria, tem problema todo dia
Bb             G
Oh que agonia, oh que agonia
Cm
A minha vida amorosa tá uma decepção
G#
Tô largado, tô jogado, vivendo de solidão
Fm
Já cansei de fantasia, não suporto mais mentiras
Bb             G
Oh que agonia, oh que agonia
G#
Mas eu sei que o meu sofrimento pode se acabar
Cm
Vou é reunir o meus amigos pra bebemorar
G#        Fm          Bb
O meu fim de semana chegou
G
A festa começou

Refrão:

Cm      G#
Sextou, sextooou
Bb                      G
Hoje ninguém me acha, o celular descarregou
Cm      G#
Sextou, sextooou
Bb                    G
Só chego de manhã agarrado com outro amor
Cm      G#
Sextou, sextooou
Bb                     G
Hoje ninguém me acha, o celular descarregou
Cm     G#
Sextou, sextooou
Bb                    G
Só chego de manha agarrado com novo amor

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
