Wesley Safadão - Tarde Demais

Intro: C G F Fm (4X)

C                    G          Am
Só tem um jeito de você me entender
                   F
Uma maneira que te faça enxergar
C                    G         Am
Eu já cansei de discutir com você
                   F
Não adianta mais falar por falar
C                     G        Am
Há muito tempo que eu venho pensando
     F
Analisando nós dois
C                     G        Am
Perdi meu tempo com você só brigando
           F
Não vou deixar pra depois

G
Você não vai mudar


É fácil presumir
Dm                          C
A sua vida de aventura dura pouco tempo
 F      Em    Dm
Até se arrepender

G
Depois lá vem você

Pedindo pra voltar
Dm                                       C
Como já fez milhões de vezes vem se lamentando
             Dm      Am          F Em Dm
Sofrendo, chorando é digno de você
Dm Em F
Não sei se dá pra ver
                     C     G
Mas acho que estou mudando

C                        G                Am
Houve um tempo em que eu não podia nem te ver
                     A7         Dm
Agora posso estar de frente a você
                               G   F G
Que meu coração não sente mais nada
C                G                   Am
Me desculpa te falar mas tenho que dizer
                     A7         Dm
Que hoje mesmo acreditando em você
                           G
Você se apaixonou por mim na hora errada
F     G C
Tarde demais
        G     F
Tchau, tchau amor
 Fm      C
Tarde demais
        G       F
Tchau, tchau já vou
 Fm        C
Não volto atrás...

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
