Wesley Safadão - Amor Errado

     Cm
Agora você é
                                  G#
Uma página virada em minha história
                                  Eb
Uma simples lembrança em minha memória
                               Bb
O livro que eu nunca mais vou ler
           Cm
Agora você é
                                    G#
O que eu tenho vergonha em meu passado
                                 Eb
Dos amores que eu tive o mais errado
                                 Bb
O que eu mais me arrependi de fazer
            Cm
Foi amar você!

Cm                                       G#
Tanto que eu te amei te levei muito a serio

Eb                                    Bb
E sem nenhuma malícia te dei o meu coração
Cm                                      G#
Eu fui mais que seu amor eu vivi sua vida
Eb                                   Bb
Fui muito além dos limites de uma paixão

G#                                 Cm
Mas você me enganou você me fez chorar
                                     G#
Jogou na minha cara que só quis me usar
G#                                  Cm
Feito um animal ferido de tanto apanhar
                  G#                                      Bb
Eu sofri mais esqueci que um dia você foi a luz da minha vida

           Cm
Agora você é
                                  G#
Uma página virada em minha história
                                  Eb
Uma simples lembrança em minha memória
                               Bb
O livro que eu nunca mais vou ler
           Cm
Agora você é
                                    G#
O que eu tenho vergonha em meu passado
                                 Eb
Dos amores que eu tive o mais errado
                                 Bb
O que eu mais me arrependi de fazer

            Cm
Foi amar você
Cm               G#                     Cm
Tanto que eu te amei te levei muito a serio


----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
Eb = X 6 5 3 4 3
G# = 4 3 1 1 1 4
