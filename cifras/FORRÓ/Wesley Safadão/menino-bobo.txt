Wesley Safadão - Menino Bobo

Intro: Bb   F   D# (4x)

Bb                            F           Gm
Já vi que não tem jeito pra você nem pra mim
                   Dm             D#         Cm
Quantas vezes me falou que era o fim de nós dois
  F
E eu ficava chorando
Bb                       F           Gm
Com raiva muitas vezes jurei te esquecer
                    Dm         D#
Em outras bocas eu buscava prazer
            Cm             F
Mas quando eu ia me acostumando
D                               Gm
Lá vem você dizendo que quer voltar
                                C
Que não consegue viver sem me amar
                               F
E eu feito um menino bobo te aceito
D                                 Gm
Eu não tenho forças pra dizer que não

                                C
Toda vez que você vem pedir perdão
                              F
Eu perdoou e esse é o meu defeito
Bb
Sempre quando vou me acostumando
Dm
Quando a minha dor está passando
 D#        Cm         F
Você vem e faz minha cabeça
Bb
Sempre quando vou te esquecendo
Dm
Quando nosso amor está morrendo
 D#        Cm          F
Você vem e faz minha cabeça
D#              F            Bb
  Fique de uma vez ou me esqueça

(Bb  F  D#)(2x)

Bb                           F           Gm
Já vi que não tem jeito pra você nem pra mim
                   Dm             D#         Cm
Quantas vezes me falou que era o fim de nós dois
 F
E eu ficava chorando
Bb                       F           Gm
Com raiva muitas vezes jurei te esquecer
                   Dm         D#
Em outras bocas eu buscava prazer
             Cm           F
Mas quando eu ia me acostumando
D                               Gm
Lá vem você dizendo que quer voltar
                               C
Que não consegue viver sem me amar
                               F
E eu feito um menino bobo te aceito
D                                 Gm
Eu não tenho forças pra dizer que não
                                C
Toda vez que você vem pedir perdão
                              F
Eu perdoou e esse é o meu defeito
Bb
Sempre quando vou me acostumando
Dm
Quando a minha dor está passando
 D#         Cm         F
Você vem e faz minha cabeça
Bb
Sempre quando vou te esquecendo
Dm
Quando nosso amor está morrendo
 D#         Cm         F    F#
Você vem e faz minha cabeça

B
Sempre quando vou me acostumando
D#m
Quando a minha dor está passando
 E         C#m          F#
Você vem e faz minha cabeça
B
Sempre quando vou te esquecendo
D#m
Quando nosso amor está morrendo
 E          C#m         F#
Você vem e faz minha cabeça
E              F#           B
 Fique de uma vez ou me esqueça

(B  F#  E)

B                           F#            B
Já vi que não tem jeito pra você nem pra mim

----------------- Acordes -----------------
B = X 2 4 4 4 2
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D# = X 6 5 3 4 3
D#m = X X 1 3 4 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
Gm = 3 5 5 3 3 3
