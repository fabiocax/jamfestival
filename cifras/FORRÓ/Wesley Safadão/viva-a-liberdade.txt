Wesley Safadão - Viva a Liberdade

Intro: C#m A E B

           A
Não tá mas doendo
           B                    C#m
Aquele sentimento lá dentro do peito
         A                     B              C#m
O meu coração, voltou agora tá batendo do meu jeito

           A                G#m
Não sinto falta mas do seu olhar
       A
Já esqueci teu jeito de amar
          A
Eu vi no espelho, toda minha paz
          B
Eu tanto fiz, que agora tanto faz

REFRÃO:
C#m                A                E
Agora, viva a liberdade, nada de saudade

                B
Eu tô bem, sem ninguém
             C#m           A                E
Se você não sabe, a felicidade não depende de alguém

               B                        C#m A E B (2x)
Que não te faz bem, agora tô melhor sem ôô

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
G#m = 4 6 6 4 4 4
