Wesley Safadão - Amor Incondicional

D
Eu sei que muitos vão dizer
                    G
Que eu não sirvo pra você.

D
Eu sei que muitos vão tentar
              G
Destruir nosso amor.

        Bm
Mas mesmo assim...
          G
Eu sei que nós.

D
Vamos seguir em frente
                               G
Porque a gente foi feito um pro outro.

D
Eu quero é estar com você

                   G
E não importa o que pensem os outros.

D
Mesmo que o mundo inteiro se vire contra nós
G
E a família e os amigos se afastem de nós,

       Bm
Mesmo assim...
       Bm
Se for com você..
             A
Eu vou até o fim....

Refrão 2x:
               D
Amor Incondicional
                A                Bm
Eu faço o impossível se preciso for
                 A                 G
Eu brigo com o mundo defendo esse amor
                   Bm                   A
Não me importo nem um pouco com o que vão dizer iê iê.

              D      A    Bm   G
Porque amo você..... uô...uô....

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
G = 3 2 0 0 0 3
