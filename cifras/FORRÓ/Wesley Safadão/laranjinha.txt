Wesley Safadão - Laranjinha

[Intro]  Bm  F#m  A  E

Bm            F#m
Tá ficando complicado demais
 A                E
Não dá pra esconder
Bm                   F#m
O nosso amor tá aumentando
                A
Eu já estou te amando
                 E
Eu quero só você
Bm               F#m
Tá ficando complicado demais
 A                 E
Não dá pra esconder
Bm                  F#m
O nosso amor tá aumentando
                A
Eu já estou te amando
                 E
Eu quero só você


Bm                       F#m
Mas o problema é seu marido
              A                 E
Ele tá procurando um dia nos flagrar
Bm                 F#m                  A
Eu já fiquei sabendo que ele anda dizendo
                E
Que vai me matar

Bm                 F#m                  A
Imagina se ele soubesse o que eu faço contigo
               E
Quando eu faço amor
Bm              F#m
Eu te faço de laranjinha
                  A
Eu chupo você todinha
              E
E você gostou

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
