Wesley Safadão - Sem Rumo Ao Léu

C                   G                Am
Chega eu não quero mais ficar com você
            Em               F
Complicado entender mas entenda
                    Em        Dm
Que eu não posso viver por viver
               G                   C
Ta difícil conviver com a indiferença

                   G         Am
E hoje o livro do amor um engano
               Em        F
Foi borrar a letra do papel
                 Em             Dm
Como um pássaro que perdeu seu bando
             G
Voando sem rumo ao céu

F          G                  Am
Te vejo afastar de mim a cada dia
F            G                   Am
A sua vida está tão distante da minha

F              G       Em
Na incerteza do talvez
             Am
O amor por si se desfez
 F                   G               C
Não posso mais continuar errando outra vez

Refrão:

C    G   Am                             F
Juro tentei, fiz de tudo pra viver a paixão
                               C                G
E de graça me entreguei pra você, dei o meu coração
                Am                                 F
Se o sonho acabou, não me culpe pois cumpri meu papel
                            C                          G
Feito folha seca o vento levou esse amor, sem rumo ao léu
C                   G                Am
Chega eu não quero mais ficar com você...

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
