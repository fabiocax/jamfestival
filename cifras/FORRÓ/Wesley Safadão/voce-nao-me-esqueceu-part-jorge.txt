Wesley Safadão - Você Não Me Esqueceu (part. Jorge)

Intro 2x: Em C D

E|---------------------------------------------|
B|---------------------------------------------|
G|----5h7-5---4p2------------------------------|
D|--5-------------5---5h7-5/7~-----------------|
A|---------------------------------------------|
E|---------------------------------------------|

E|---------------------------------------------|
B|---------------------------------------------|
G|----5h7-5---4p2------------------------------|
D|--5-------------5-4--------------------------|
A|---------------------5/7---------------------|
E|---------------------------------------------|

Primeira Parte:

          Em
Hoje eu acordei


Peguei meu telefone
                         C9
E vi suas mensagens apaixonadas
                                   D
Ontem também eu mandei várias pra você
                                    Em
Infelizmente as minhas não foram enviadas

Segunda Parte:

                  C9
Tantas frase tão lindas
           D                              Em
Por coincidência as suas foram iguais as minhas
                                 C9
Em uma delas se sentia tão sozinha
              D                       Em
Que era impossível em um minuto me esquecer

Pré-Refrão:

            Am7
Quando um grande amor se vai
     D
Os dois sofrem iguais
          G                       Em
Mas a distância nunca separa quem ama
           Am7
Mesmo que venha a se envolver
            D
Com outras pessoas pra esquecer
       Em
A lembrança faz a gente ter saudade

Refrão:

                C
Você não me esqueceu
           D
Nem muito menos eu
           Em
Nem muito menos eu
                C
Você não me esqueceu
           D
Nem muito menos eu
           Em
Nem muito menos eu

Solo 2x: C  D  Em

E|---------------------------------------------|
B|---------------------------------------------|
G|----5h7-5---4p2------------------------------|
D|--5-------------5---5h7-5/7~-----------------|
A|---------------------------------------------|
E|---------------------------------------------|

E|---------------------------------------------|
B|---------------------------------------------|
G|----5h7-5---4p2------------------------------|
D|--5-------------5-4--------------------------|
A|---------------------5/7---------------------|
E|---------------------------------------------|

Pré-Refrão:

            Am7
Quando um grande amor se vai
     D
Os dois sofrem iguais
          G                       Em
Mas a distância nunca separa quem ama
           Am7
Mesmo que venha a se envolver
            D
Com outras pessoas pra esquecer
       Em
A lembrança faz a gente ter saudade

Refrão 2x:

                C
Você não me esqueceu
           D
Nem muito menos eu
           Em
Nem muito menos eu
                C
Você não me esqueceu
           D
Nem muito menos eu
           Em
Nem muito menos eu

Solo 2x: C  D  Em

E|---------------------------------------------|
B|---------------------------------------------|
G|----5h7-5---4p2------------------------------|
D|--5-------------5---5h7-5/7~-----------------|
A|---------------------------------------------|
E|---------------------------------------------|

E|---------------------------------------------|
B|---------------------------------------------|
G|----5h7-5---4p2------------------------------|
D|--5-------------5-4--------------------------|
A|---------------------5/7---------------------|
E|---------------------------------------------|

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
