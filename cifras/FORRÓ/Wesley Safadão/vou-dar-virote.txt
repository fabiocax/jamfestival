Wesley Safadão - Vou Dar Virote

Intro: C#m A E B

Primeira Parte:

C#m                                            A
Amava alguém, me enganei, me apaixonei pela pessoa errada
                  E                           B      G#
Pra mim você não vale nada, pra mim você não vale nada
 C#m                                      A
Foi só ilusão, decepção, acreditava nas suas palavras
                  E                           B      G#
Pra mim você não vale nada, pra mim você não vale nada

Pré-Refrão:

          A
Eu vou beber até o dia clarear
               C#m
Vem pra minha mesa que hoje é open bar
   F#m             A             B
A noite não tem hora pra acabar


Refrão 2x:

              C#m                A
Eu vou dar virote, eu vou dar virote
          E                       B
Sou o Patrão to estourado e essa vida é pra quem pode
              C#m                A
Eu vou dar virote, eu vou dar virote
         E                         B
E pode chamar o samu, que hoje eu vou tomar glicose

( C#m  E  B ) (2x)

Primeira Parte:

C#m                                            A
Amava alguém, me enganei, me apaixonei pela pessoa errada
                  E                           B      G#
Pra mim você não vale nada, pra mim você não vale nada
 C#m                                     A
Foi só ilusão, decepção, acreditava nas suas palavras
                  E                           B      G#
Pra mim você não vale nada, pra mim você não vale nada

Pré-Refrão:

          A
Eu vou beber até o dia clarear
               C#m
Vem pra minha mesa que hoje é open bar
   F#m             A             B
A noite não tem hora pra acabar

Refrão 2x:

              C#m                A
Eu vou dar virote, eu vou dar virote
          E                       B
Sou o Patrão to estourado e essa vida é pra quem pode
              C#m                A
Eu vou dar virote, eu vou dar virote
         E                         B
E pode chamar o samu, que hoje eu vou tomar glicose

Final 4x: C#m  E  B

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G# = 4 3 1 1 1 4
