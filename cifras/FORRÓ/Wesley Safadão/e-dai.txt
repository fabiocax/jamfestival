Wesley Safadão - E Daí?

    D                    Bm
E daí, se eu quiser farriar
                 G
Tomar todas num bar
              A
Sair pra namorar
              D
O que é que tem?
                Bm
Foi você quem falou
                G
Que a paixão acabou
                                    A
Que eu me lembre, eu não sou de ninguém

(intro) D  Bm  G  A

    D             Bm
E Daí, você me deixou
                G
Mais o tempo passou

                 A         D
E o mundo não parou, tô aqui
               Bm
Confesso Fraquejei
                G
Muito tempo chorei
                 A           Em
Só Deus sabe o quanto eu sofri
Mais não fui me humilhar
                 A
Nem pedir pra voltar
                        Em
O que você tá fazendo aqui?
Se já não me quer
                 A
Então sai do meu pé
Eu faço o que eu quiser

(refrão)
 D                    Bm
E daí, se eu quiser farriar
                 G
Tomar todas num bar
              A
Sair pra namorar
              D
O que é que tem?
                Bm
Foi você quem falou
                G
Que a paixão acabou
                                    A
Que eu me lembre, eu não sou de ninguém (2x)

(repete tudo)

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
