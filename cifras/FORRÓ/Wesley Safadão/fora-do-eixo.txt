Wesley Safadão - Fora do Eixo

Introdução: A,E,F#m,E,D (2x)

      A             E
Você briga, sai do eixo
        D
nem da tempo de me explicar
       A                 E
diz bobagens de cair o queixo
        D
será que não posso me atrasar

        E                     F#m
Cinco minutos cheguei atrasado,
      D                A
já recebo apelido engraçado
       Bm                      D
desgraçado é palavra de luxo
       E                     A
nesse seu vocabulário sujo.

     E                F#m
Me irrito e digo que cansei

      D                 A
você fala quem cansou fui eu
      E                 F#m
não aguento mais ficar aqui
        D
você fica nervosa, entra em minha frente
      E                      D      A  D A
e me diz que não sou nem besta de sair

      E                   F#m               D
"E depois de ouvir tantas besteiras me morde as orelhas
         A         E                  F#m
e quer me beijar, e diz tantas coisas sem nexo
               D                     A
depois já quer sexo e começa a gritar
     Bm           D
vagabundo, safado, traíra,
             E                 A
acabou minha ira eu só sei te amar"        (2x)

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
