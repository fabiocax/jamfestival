Wesley Safadão - Revanche

Intro: A  E  F#m  D

A
Hoje te encontrei sem querer
E
Pensei que tudo iria desabar
F#m
Lembrei de quando fiquei sem você
D
Pra aquele mundo não quero voltar,
A
Mas veja só o que aconteceu
E
Seu mundo se distanciou do meu
F#m
E agora que quase me acostumei
D
Você de repente apareceu

A
Querendo me propor uma nova chance

E
Querendo reviver nosso romance
F#m
Mas só eu sei o que eu passei em vão
D
Chegou a hora da revanche então...
A
Esqueça vá procurar seu caminho
E
Que hoje eu to bem melhor sozinho
F#m
Cansei de me iludir e ser usado
D
Só lembre do que foi bom do passado

A
E quando a saudade lhe doer
E
Recorde o quanto que me fez sofrer
F#m
Aí quem sabe um dia possa dar valor
D
A quem um dia realmente te amou...

A
Hoje te encontrei sem querer
E
Pensei que tudo iria desabar
F#m
Lembrei de quando fiquei sem você
D
Pra aquele mundo não quero voltar,
A
Mas veja só o que aconteceu
E
Seu mundo se distanciou do meu
F#m
E agora que quase me acostumei
D
Você de repente apareceu
A
Querendo me propor uma nova chance

E
Querendo reviver nosso romance
F#m
Mas só eu sei o que eu passei em vão
D
Chegou a hora da revanche então...

A
Esqueça vá procurar seu caminho
E
Que hoje eu to bem melhor sozinho
F#m
Cansei de me iludir e ser usado
D
Só lembre do que foi bom do passado

A
E quando a saudade lhe doer
E
Recorde o quanto que me fez sofrer
F#m
Ai quem sabe um dia possa dar valor
D
A quem um dia realmente lhe amou...

A
E quantas vezes que apertou o coração
E
Vendo suas fotos jogadas no chão
F#m
Mas aprendi que o verdadeiro amor
D
É o amor sem sofrimento...

A
Esqueça vá procurar seu caminho
E
Que hoje eu to bem melhor sozinho
F#m
Cansei de me iludir e ser usado
D
Só lembre do que foi bom do passado
A
E quando a saudade lhe doer
E
Recorde o quanto que me fez sofrer
F#m
Ai quem sabe um dia possa dar valor
D
A quem um dia realmente ti amou...

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
