Calcinha Preta - A Casa Caiu

Intro 2x: C D Bm Em

   Em                        C
Eu descobri que tem um outro amor
    D                     Em
Todo esse tempo só me enganou
    Em                    C
Mas a verdade chega com o tempo
                D
Revelando fingimentos
          Em
Você não presta
 B
Agora eu saquei porque não larga o celular
    Em
Com medo que ele possa te ligar
       C           D
E não adianta disfarçar
            Bm        Em
Hoje eu vou te desmascarar
   C
Voce não vale nada

     Am
Voce não tem coração
   B
Já descobri a sua armação

Refrão 2x:
         C
A casa caiu
            D
A farsa acabou
               Bm
Pra mim e pra ele
            Em
Jurou seu amor
         C
A casa caiu
              D
Perdeu seu harém
           Bm
Queria as duas
              Em
Ficou sem ninguém

(Repete Tudo)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
