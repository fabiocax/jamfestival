Calcinha Preta - Como Eu Te Amei

F#m                                  C#m
Quando acendeu pra mim o farol da paixão
F#m                         C#m
Meus olhos navegaram em sua direção
         B                          F#m
Milhas e milhas do cais, em busca daquela paz
            C#m
Que meu coração sonhou

             E                   G#m
Pensando que era amor eu me arrisquei demais
            A                 B
Deixei tudo para trás e me lançei no mar

De quem não soube amar

E
Te amei, por todos os sete mares
G#m
Te amei, foi amor de verdade
   A                 B
Te amei, como eu te amei


F#m                                  C#m
Quando acendeu pra mim o farol da paixão
F#m                         C#m
Meus olhos navegaram em sua direção
         B                          F#m
Milhas e milhas do cais, em busca daquela paz
            C#m
Que meu coração sonhou

             E                   G#m
Pensando que era amor eu me arrisquei demais
            A                 B
Deixei tudo para trás e me lançei no mar

De quem não soube amar

E
Te amei, por todos os sete mares
G#m
Te amei, foi amor de verdade
   A                 B
Te amei, como eu te amei

E
Te amei, como ninguém no mundo
G#m
Te amei, sentimento profundo
   A                 B
Te amei, como eu te amei

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
