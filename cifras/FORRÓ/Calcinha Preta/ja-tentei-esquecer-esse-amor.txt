Calcinha Preta - Já Tentei Esquecer Esse Amor

Refrão:
E        A        B          E
Eu já tentei esquecer esse amor
              A        B   C#m
Te ver com outro foi arrasador
       F#m          B
Não aguentei, me retirei
       A        B              E
Não queria me cruzar com teu olhar.

C#m     G#m     A                        B
Dói demais, saber que hoje tem um outro amor
C#m    G#m      A                   B
E eu aqui, tentando afogar a minha dor
F#m
Em cada rosto eu te vejo
E             B      C#m
Solidão faz mal pra mim.

         F#m       A
Eu não aceito, um outro alguém

               B
Te amando no meu lugar
     A      B    E
Não dá pra aguentar!

Refrão:
         A         B        E
Ele não tem nada haver com você
          A          B         C#m
Viver com dó ao seu lado, pra que?
          F#m            B
Deixa ele ai, volta pra mim
      A           B             E
Você sabe que só eu que sei te amar.

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
