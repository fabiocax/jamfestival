Calcinha Preta - Pensão Alimentícia

C               G         Dm
Que foi que eu fiz pra você,
F                                C      (2x)
mandar os "homi" aqui vir me prender.

solo: C,G,Dm,F (2x)

C             G                  Dm
Tudo era tão lindo um conto de fadas
           F                  C
Tão maravilhoso a gente se amava
                G               Dm
Foi nessa brincadeira que aconteceu
                F                 C
Nasceu um lindo filho que é seu e meu.
               G                  Dm
No final de semana agente ia à praia
           F                 C
Saia pro forró, caía na gandaia
          G                Dm
Um amor assim eu só ví na tv,

                        F                       G
Mas já que a gente terminou nao tem mais nada a ver.
         Dm
Sou cachaçeiro, sou cabra raparigueiro,
                   C
Mas eu nao sou vagabundo.

Eu sou do mundo!
Dm                                  F
Sou de responsa, eu sou mais um brasileiro
                C
Com pensão para pagar e vou pagar.
Dm
Mas nao é justo que pensão alimenticia
                C
Vire caso de policia.

Isso complica!
        Dm                    F              G7
Tá atrasada mas você não precisava me denunciar.


C               G         Dm
Que foi que eu fiz pra você,
F                                C      (4x)
mandar os "homi" aqui vir me prender.

----------------- Acordes -----------------
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
