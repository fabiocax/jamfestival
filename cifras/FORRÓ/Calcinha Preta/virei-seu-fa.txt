Calcinha Preta - Virei Seu Fã

     C                             G
Você sempre esta comigo em todo lugar
    Dm               G        C
Agradeço a Deus por você, existir
                                G
De repente escuto, alguém me chamar
    Dm              G        C
É você na porta do meu, camarim

C                  C7
No seu rosto uma lágrima
        G
Na mão caneta e papel
        Dm            G           C
Nesse instante eu me sinto,.. no céu

C                  C7
É o meu maior presente
       G
Meu motivo pra cantar
      Dm          G               C
Sem você não é possível....... não dá.


  C                           G
Muito obrigado, por você existir
         Dm            Am       G
Sem teu amor,..o que seria de mim?
       C                        G
É pra você que eu canto, essa canção
          Dm       G        C
Calcinha Preta eu virei seu fã
Solo:
E|----------14-15-17-----------|
B|--15-17-------------15-17~~--|
G|-----------------------------|
D|-----------------------------|
A|-----------------------------|
E|-----------------------------|

 C
Dias e noites ,
                   G
seja em qualquer lugar
             Dm           C
Eu sempre estarei.....contigo
E|----------14-15-17-----------|
B|--15-17-------------15-17~~--|
G|-----------------------------|
D|-----------------------------|
A|-----------------------------|
E|-----------------------------|

C                             G
Nada neste mundo, vai nos separar
               Dm                C
Agradeço a Deus que bom, te encontrar.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
