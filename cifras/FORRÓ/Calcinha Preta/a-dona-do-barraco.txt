Calcinha Preta - A Dona do Barraco

Intro:  Bm A G Gb Bm

Bm
Ela é glamourosa e está com tudo em cima
A
Fogosa, sedutora meu Deus que coisa linda
         Bm
Usa salto 15, gosta da ostentação
G                                          Gb
Mas tem que ter pegada pra ganhar seu coração
                Bm
É vuco, vuco, vuco toda vez que a gente ama
                                          A
E tome vuco, vuco qualquer dia quebra a cama
       G                                Gb
A chapa fica quente no hotel ou no barraco
                                           Bm
Quando a gente se pega o buraco é mais embaixo

Refrão 2x:
Bm                                 A
Ela venceu na vida e merece destaque

                                 Bm
É super poderosa, tem personalidade
                                     A
Na rua ela desfila, por baixo pega fogo
               G                Bm
A dona do barraco é a dama do jogo

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
G = 3 2 0 0 0 3
Gb = 2 4 4 3 2 2
