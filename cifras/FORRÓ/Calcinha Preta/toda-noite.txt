Calcinha Preta - Toda Noite

F    C     Gm              Bb
Toda noite, eu tenho tanto medo
F       C   Gm                 Bb
A dor invade, machucando o meu peito

F    C     Gm              Bb
Toda noite, eu tenho tanto medo
F       C   Gm                 Bb
A dor invade, machucando o meu peito

Dm    C                                Bb
Eu preciso de um alguém que saiba o que é amor
          C    Dm
Dentro do coração
               C            Bb
Alguém que não queira só me seduzir
   C
E, sim cuidar de mim

F                C
Será que você só existe nos meus sonhos

Gm                   Bb
Só em conto de fadas é que eu te encontro
F                     C
Preciso amor que você seja real
         Gm                Bb
Pra me fazer feliz e, muito me amar

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
