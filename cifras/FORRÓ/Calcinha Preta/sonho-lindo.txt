Calcinha Preta - Sonho Lindo

Riff realizado pela guitarra durante os versos, no acorde F:

    F
e|-----------------|
B|-----------------|
G|-----------------|
D|--0-2-3---3-2-0--|
A|-----------------|
E|-----------------|


C            G                 Am  F
Como foi tão bom agente se encontrar
C             G                Am F
Eu tava precisando de alguém assim
C              G                 Am
Num canto reservado pra gente se amar
C        G             Am       F
Que esse amor resista até o fim
C           G                 Am
Tocar o seu corpo é viajar no céu

             F                 C
Beijar a sua boca tem sabor de mel
               G                       Am
Você é o sonho lindo que eu sonhei pra mim(2X)
       F             Am
Vem me amar, vem pra mim
       F             Am
Vem me amar, diz que sim
         F             G
Que esse amor não ter fim
F               G                       Am
Você é o sonho lindo que eu sonhei pra mim! uououo
F               G                        C
Você é o sonho lindo que eu sonhei pra mim!

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
