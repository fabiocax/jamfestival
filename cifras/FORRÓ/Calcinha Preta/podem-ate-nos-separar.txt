Calcinha Preta - Podem Até Nos Separar

D
Se eu pudesse apertar um botão
Bm
E te arrancar de vez do coração
D
Se fosse só eu te mandar embora
Bm
E te arrancasse da minha memória

D
Mas eu não sou nenhum computador
Bm
Que com um clique deleta o amor
D
E eu não sou nenhum celular
Bm                              G
Pra te excluir só basta apertar

G        A              D    F#m  Bm
Posso até dizer que não te quero mais
G         A           D F#m   Bm
Difícil é o meu coração entender

            Em                       A
Por que o nosso amor não pode acontecer

Refrão:
        D
Podem até nos separar
F#m         Bm             B7
Mas nosso amor nunca vai acabar
Em
Podem até dizer que não
          A
Mas você nunca vai sair
        D
Do meu coração

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
