Calcinha Preta - Por que tocou meu coração?

(intro) F#  G#  C#  D#m  Fm  C#  G#  A#  A#m G#

C#        G#/C        A#m
Por que tocou meu coração
       A#m/G#      F#
Mim deixar em suas mãos
            C#/F
Mim diz pra onde foi
           D#m         G#
Que eu não sei o que fazer
C#      G#/C        A#m
Você chegou e mim ganhou
      A#m/G#           F#
Nunca fui de ninguém assim
            C#/F
Mim diz pra onde foi
          D#m         G#
Que eu te quero outra vez

(refrão)
C#         D#m           G#
Eu preciso te encontrar

         C#      Cm    A#m
Com você quero fica....r
              D#m
Eu não vejo a hora de
  F#         G#
Poder te abraçar

C#         D#m           G#
Eu preciso te encontrar
         C#      Cm    A#m
Com você quero fica....r
              D#m
Eu não vejo a hora de
  F#         G#     F# G#
Poder te abraçar

C#        G#/C        A#m
Por que tocou meu coração
       A#m/G#      F#
Mim deixar em suas mãos
            C#/F
Mim diz pra onde foi
           D#m         G#
Que eu não sei o que fazer
C#      G#/C        A#m
Você chegou e mim ganhou
      A#m/G#           F#
Nunca fui de ninguém assim
            C#/F
Mim diz pra onde foi
          D#m         G#
Que eu te quero outra vez

(refrão)
C#         D#m           G#
Eu preciso te encontrar
         C#      Cm    A#m
Com você quero fica....r
              D#m
Eu não vejo a hora de
  F#         G#
Poder te abraçar

C#         D#m           G#
Eu preciso te encontrar
         C#      Cm    A#m
Com você quero fica....r
              D#m
Eu não vejo a hora de
  F#         G#     D#m Fm
Poder te abraçar

F#           G#
Saiba que só seu amor
        C#     Cm    A#m
que não quero te perder
        D#m
que não há amor no mundo
    F#        G#
não há amor maior

(refrão)
C#         D#m           G#
Eu preciso te encontrar
         C#      Cm    A#m
Com você quero fica....r
              D#m
Eu não vejo a hora de
  F#         G#
Poder te abraçar

C#         D#m           G#
Eu preciso te encontrar
         C#      Cm    A#m
Com você quero fica....r
              D#m
Eu não vejo a hora de
  F#         G#
Poder te abraçar

(solo)
C#  G#/C  A#m  A#m/G#  F#  C#/F  D#m  G#
C#  G#/C  A#m  A#m/G#  F#  C#/F  D#m  G#  D#m  Fm

E|---------------------------------------------------------------------------------------4/6-4-
B|---2-4-4/6-4-2-1-2~---1-2-1-------2-2/4-2~--4/6-4--2-2/4--2-1/2~----2--4-4/6-4-6-7-6-4-------
G|-1-------------------------1-1/3~-------------------------------1----------------------------
D|---------------------------------------------------------------------------------------------
A|---------------------------------------------------------------------------------------------
E|---------------------------------------------------------------------------------------------

E|--2-1-2-1---------------4/6-4-1/2-1---------------------1-2-4-4/6~---------------------------
B|----------2-----2-2/4-2~-------------2------------1-2-4--------------------------------------
G|------------1/3-------------------------1/3~--1-3--------------------------------------------
D|---------------------------------------------------------------------------------------------
A|---------------------------------------------------------------------------------------------
E|---------------------------------------------------------------------------------------------

F#           G#
Saiba que só seu amor
        C#     Cm    A#m
que não quero te perder
        D#m
que não há amor no mundo
    F#        G#    G#  A
não há amor maior

 D         Em           A
Eu preciso te encontrar
         D      C#m    Bm
Com você quero fica....r
              Em
Eu não vejo a hora de
  G          A
Poder te abraçar

D          Em           A
Eu preciso te encontrar
         D      C#m    Bm
Com você quero fica....r
              Em
Eu não vejo a hora de
  G           A
Poder te abraçar

D          Em           A
Eu preciso te encontrar
         D      C#m    Bm
Com você quero fica....r
              Em
Eu não vejo a hora de
   D  C#m     Bm    D  Em   F#m
Poder te abraçar

----------------- Acordes -----------------
A = X 0 2 2 2 0
A# = X 1 3 3 3 1
A#m = X 1 3 3 2 1
A#m/G# = 4 X 3 3 2 X
Bm = X 2 4 4 3 2
C# = X 4 6 6 6 4
C#/F = X 8 X 6 9 9
C#m = X 4 6 6 5 4
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D#m = X X 1 3 4 2
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
G#/C = X 3 X 1 4 4
