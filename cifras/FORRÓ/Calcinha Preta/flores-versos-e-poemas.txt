Calcinha Preta - Flores, Versos e Poemas

[Intro] Em  Bm  A

Em                       Bm
Será que não valeu a pena
                        D
Flores, versos e poemas
                   A
E aquele nosso amor que era pra sempre acabou
Em                   Bm
Me diz o que aconteceu
                      D
Será que o errado fui eu
           A
Porque você fugiu de mim

G                                  Em
O seu cheiro ainda está em minha roupa
                                 D
O seu beijo tá marcado em minha boca
                   Bm
Desde que você se foi

                      A
Só lembranças de nós dois

G                               Bm
Eu devia te arrancar dentro do peito
                              D
Mas não quero esse é o meu defeito
                             A
A saudade tá doendo mas não vou desistir

         Em D   A
Porque eu amo você
         Em Bm   A
Porque eu amo você
      Em D   A
Porque eu amo você
         Em Bm    A
Porque eu amo você
                    Em
Não quero te perder

[Solo] Em  G  D  A

Em                       Bm
Será que não valeu a pena
                        D
Flores, versos e poemas
                   A
E aquele nosso amor que era pra sempre acabou
Em                   Bm
Me diz o que aconteceu
                      D
Será que o errado fui eu
           A
Porque você fugiu de mim

G                                  Em
O seu cheiro ainda está em minha roupa
                                 D
O seu beijo tá marcado em minha boca
                   Bm
Desde que você se foi
                      A
Só lembranças de nós dois

G                               Bm
Eu devia te arrancar dentro do peito
                              D
Mas não quero esse é o meu defeito
                             A
A saudade tá doendo mas não vou desistir

         Em D   A
Porque eu amo você (eu amo você)
         Em Bm   A
Porque eu amo você (eu amo, eu amo)
      Em D   A
Porque eu amo você (amo você)
         Em Bm    A
Porque eu amo você

         Fm D#   Bb
Porque eu amo você (eu amo você)
         Fm Cm   Bb
Porque eu amo você (eu amo, eu amo)
      Fm D#   Bb
Porque eu amo você (amo você)
         Fm Cm   Bb
Porque eu amo você

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
