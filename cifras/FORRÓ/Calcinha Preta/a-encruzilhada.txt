Calcinha Preta - A Encruzilhada

Intro 2x: Am G

            G                 Am
 Ajuda meu coração, me tira desta solidão
               C                   D
 Me diga que um dia nos dois vamos ser
                   G    D
 muito mais que amantes
            G                        Am
 Então você vai saber, que não vai se arrepender
              C              D                G       D
 Eu vou te amar todo dia dormir e acordar com você

Am                           G
 Você sempre passa sem falar comigo,
Am                         G
 me olha com cuidado sabe disfarçar
Am                                 G
 A vontade é de agarrar e te dá um beijo,
Am                         G
 mas o meu desejo sabe esperar

      Am              G
 E o meu coração perdido nesta estrada,
 Am                    G
 sem direção nessa encruzilhada
      G               Am
 E a dor aperta minha solidão
   C                    D                   G
 e quanto mais solto eu quero bem mais eu te amo
            Am
 Tenho um sentimento louco por você
   C                   D             G    D
 queria um jeito de te amar sem te perder
                  G                 Am
 Ajuda meu coração, me tira desta solidão
               C                   D
 Me diga que um dia nos dois vamos ser
                   G    D
 muito mais que amantes
            G                        Am
 Então você vai saber, que não vai se arrepender
           C             D                 G       D
 Eu vou te ama todo dia dormi e acorda com você

Intro 2x: Am G

      Am              G
 E o meu coração perdido nesta estrada,
 Am                    G
 sem direção nessa encruzilhada
      G               Am
 E a dor aperta minha solidão
   C                    D                   G
 e quanto mais souto eu quero bem mais eu te amo
            Am
 Tenho um sentimento louco por você
   C                   D             G    D
 queria um jeito de te amar sem te perder
                  G                 Am
 Agita meu coração, me tira desta solidão
               C                   D
 Me diga que um dia nos dois vamos ser
                   G    D
 muito mais que amantes
            G                        Am
 Então você vai saber, que não vai se arrepender
              C             D                 G       D
 Eu vou te amar todo dia dormir e acordar com você

            G                 Am
 Ajuda meu coração, me tira desta solidão
               C                   D
 Me diga que um dia nos dois vamos ser
                   G    D
 muito mais que amantes
            G                        Am
 Então você vai saber, que não vai se arrepender
              C              D                G       D
 Eu vou te amar todo dia dormir e acordar com você

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
