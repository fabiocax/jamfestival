Calcinha Preta - Quer Namorar Comigo?

Intro: A E

               F#m
Quer namorar comigo?
D           E              C#m
Balance a cabeça e diz que sim
          F#m             Bm
Abra um sorriso e vem pra mim
        E                   A
Dê um sinal me chama que eu vou
               F#m
Quer namorar comigo?
D                E           C#m
Vou me dá com loucuras pra você
                F#m         Bm
Hoje eu criei coragem pra dizer
             E                 A
Que o que eu sinto por você é amor
 C                                     A
Faz algum tempo que você vive em meus sonhos
C                               E   E7
E eu esperando esse momento chegar ah ah

              D           A
Você tem um minuto pra pensar
            D           A
Apenas um segundo pra falar
            G                       E    E7
E a vida inteira pra mostrar que me quer
              D           A
Você tem um minuto pra pensar
            D             A
Apenas um segundo eu te digo
            G                 E
O que mais quero é te fazer feliz
                  A
Quer namorar comigo?

Solo: ( F#m  D  E  A  F#m  Bm  E  A  A7 )

C                                     A
Faz algum tempo que você vive em meus sonhos
C                               E   E7
E eu esperando esse momento chegar ah ah
              D           A
Você tem um minuto pra pensar
            D           A
Apenas um segundo pra falar
            G                       E    E7
E a vida inteira pra mostrar que me quer
              D           A
Você tem um minuto pra pensar
            D             A
Apenas um segundo eu te digo
            G                 E
O que mais quero é te fazer feliz
                 A
Quer namorar comigo?

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
