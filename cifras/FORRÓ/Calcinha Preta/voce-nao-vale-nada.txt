Calcinha Preta - Você Não Vale Nada

(intro) Cm   Bb   Cm     Ab   G   Cm
            Cm   Bb   Cm     Ab   G   Cm

e I------------------------------------------------------------------------------------------------

G I-5-8-8-8-5-8-8-8-5-8-5-7-7-------7-------------8-----5-8-8-8-5-8-8-8-5-8-5------------8-7-5-----
D I---------------------------------------------------------------------------6~-5-----------------
A I------------------------------------------------------------------------------------------------
E I------------------------------------------------------------------------------------------------

(refrão)
                           Cm                                   G
Você não vale nada, mas eu gosto de você!
                                                                Cm
Você não vale nada, mas eu gosto de você!
                                                         G
Tudo que eu queria era saber por que
                                                         Cm
Tudo que eu queria era saber por que
                                                 Ab           G
Você não vale nada, mas eu gosto de você!

                                                                Cm
Você não vale nada, mas eu gosto de você!
                                                         G
Tudo que eu queria era saber por que
                                                         Cm
Tudo que eu queria era saber por que

                            Bb                                   Eb
Você brincou comigo, bagunçou a minha vida.
                     G                                Cm
Esse  sofrimento não tem explicação.
                             Bb                             Eb
Já fiz de quase tudo tentando te esqueçer.
                              G                                          Cm
Vendo a hora morrer não posso me acabar na mão.
                                Bb                                 Eb
Seu sangue é de barata, a Boca é de vampiro.
                        G                                Cm
Um dia eu lhe tiro de vez meu coração.
                           Bb                             Eb
Aí já não lhe quero Amor me dê ouvidos
                              G                                       Cm  Dm    Eb Fm   Bb
Por favor me perdoa tô morrendo de paixão

                 Eb              Bb
Eu quero ver você sofrer
                Fm               Cm
Só pra deixar de ser ruim
               Ab                                    Fm
Eu vou fazer você chorar, se humilhar
                            G
Ficar correndo atrás de mim
               Eb              Bb
Eu quero ver você sofrer
                Fm               Cm
Só pra deixar de ser ruim
               Ab                                    Fm
Eu vou fazer você chorar, se humilhar
                            G
Ficar correndo atrás de mim

(refrão)
(intro 1x)
(refrão)

Cm  Dm    Eb Fm   Bb

                        Eb              Bb
Eu quero ver você sofrer
                Fm               Cm
Só pra deixar de ser ruim
               Ab                                    Fm
Eu vou fazer você chorar, se humilhar
                            G
Ficar correndo atrás de mim
               Eb              Bb
Eu quero ver você sofrer
                Fm               Cm
Só pra deixar de ser ruim
               Ab                                    Fm
Eu vou fazer você chorar, se humilhar
                            G
Ficar correndo atrás de mim

(refrão)
(intro 2x)

----------------- Acordes -----------------
A = X 0 2 2 2 0
Ab = 4 3 1 1 1 4
B = X 2 4 4 4 2
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
Eb = X 6 5 3 4 3
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
