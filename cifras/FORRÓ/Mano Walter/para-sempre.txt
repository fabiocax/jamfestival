Mano Walter - Para Sempre

Intro: Dm A# F C

F      A#
Vou te amar
                F
Sei que vou te amar
        C
Para sempre

       A#
Vou te amar
                F
Sei que vou te amar
        C
Para sempre


F
E aconteça o que for
                       C
Estarei sempre ao seu lado

                 A#
Seremos eternos namorados
       F  C
Eu te juro

F
Vou te encher de amor
                  C
Te guardar no pensamento

Aproveitar contigo
              A#
Os melhores momentos
                      F      E
Agora no presente e futuro

Dm
Quero viver com você
                   A#
Pro resto da minha vida

Prometo te amar
                 Dm
Sem pensar em despedida
  A#
O amor que Deus uniu
               C
Ninguém vai separar


F      A#
Vou te amar
                F
Sei que vou te amar
        C
Para sempre

       A#
Vou te amar
                F
Sei que vou te amar
        C
Para sempre

F      A#
Vou te amar
                F
Sei que vou te amar
        C
Para sempre

       A#
Vou te amar
                F
Sei que vou te amar
        C
Para sempre.

----------------- Acordes -----------------
A# = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
