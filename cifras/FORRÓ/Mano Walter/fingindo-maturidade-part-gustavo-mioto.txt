﻿Mano Walter - Fingindo Maturidade (part. Gustavo Mioto)

Capo Casa 3

[Intro] D9

[Primeira Parte]

D9
  Tá achando que eu não to nem aí

Só por que mudei meu jeito de agir
G
  Tá percebendo a diferença né

Faz tempo que eu não pego no seu pé

D9
  Eu nem reclamo quando você vai sair

Pra onde vai, e com quem, to nem aí
G
  Acha que eu já não te amo

É tudo parte do meu plano


[Pré-Refrão]

    Em
Tá dando certo
             G                      Bm
Tanto é que já passou da hora de você ir
                A9
E você ainda está aqui

[Refrão]

         G                    A9
Eu aprendi que pra te prender
                      Bm   D9
É só deixar a porta aberta
            G                   A9
Nem que eu morra engasgado de ciúme
                Bm
Sufocado de saudade
                            D9
Eu vou fingindo essa maturidade

         G                    A9
Eu aprendi que pra te prender
                      Bm   D9
É só deixar a porta aberta
            G                   A9
Nem que eu morra engasgado de ciúme
                Bm
Sufocado de saudade
                            D9
Eu vou fingindo essa maturidade

( G  A9  Bm  D9 )

[Pré-Refrão]

    Em
Tá dando certo
             G                      Bm
Tanto é que já passou da hora de você ir
                A9
E você ainda está aqui

[Refrão]

         G                     A9
Eu aprendi que pra te prender
                      Bm   D9
É só deixar a porta aberta
            G                   A9
Nem que eu morra engasgado de ciúme
                Bm
Sufocado de saudade
                            D9
Eu vou fingindo essa maturidade

         G                     A9
Eu aprendi que pra te prender
                      Bm   D9
É só deixar a porta aberta
            G                   A9
Nem que eu morra engasgado de ciúme
                Bm
Sufocado de saudade
                            D9
Eu vou fingindo essa maturidade

[Final] G  A9  Bm  D9
        G  A9  Bm

----------------- Acordes -----------------
Capotraste na 3ª casa
A9*  = X 0 2 2 0 0 - (*C9 na forma de A9)
Bm*  = X 2 4 4 3 2 - (*Dm na forma de Bm)
D9*  = X X 0 2 3 0 - (*F9 na forma de D9)
Em*  = 0 2 2 0 0 0 - (*Gm na forma de Em)
G*  = 3 2 0 0 0 3 - (*A# na forma de G)
