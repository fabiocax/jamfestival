Mano Walter - Sou Viciado Sim

[Intro]  A#m  F#  C#  G#
         A#m  F#  C#  G#

Dm          Bb
Sou viciado sim
                  F9
Meu vicio é vaquejada
          C                        Dm
É derruba boi dançar forró tomar gelada
              Bb                F9
Sei que desça vida eu não levo nada
                C                    Dm
Em quanto tiver vida eu vou pra vaquejada

Dm          Bb                         F9
Toda segunda feira é o fim da minha trajetória
             C                   Dm
Cansei dessa vida eu largo tudo agora
Dm         Bb                     F9
A Deus vaquejada cavalo, luva e espora
              C                 Dm
Mais antes de quarta já muda história

Dm           Bb                 F9
Vai me dando frio no pé da barriga
             C                      Dm
Lembrando emoção que tem dentro da pista
Dm         Bb                      F9
Do pé da cancela do boi rolando no chão
                    C                        Dm   Bb
Quando lembro do forró, vaquejada não deixo não

Dm         Bb
Sou viciado sim
                 F9
Meu vicio é vaquejada
          C                         Dm
É derruba boi dançar forró tomar gelada
              Bb                 F9
Sei que desça vida eu não levo nada
                C                 Dm
Em quanto tiver vida eu vou pra vaquejada

Dm          Bb
Sou viciado sim
                  F9
Meu vicio é vaquejada
          C                        Dm
É derruba boi dançar forró tomar gelada
              Bb                F9
Sei que desça vida eu não levo nada
               C                    Dm
Em quanto tiver vida eu vou pra vaquejada

Dm           Bb                         F9
Toda segunda feira é o fim da minha trajetória
             C                   Dm
Cansei dessa vida eu largo tudo agora
Dm         Bb                     F9
A Deus vaquejada cavalo, luva e espora
              C                   Dm
Mais antes de quarta já muda história
Dm            Bb                 F9
Vai me dando frio no pé da barriga
               C                   Dm
Lembrando emoção que tem dentro da pista
Dm            Bb                      F9
Do pé da cancela do boi rolando no chão
                  C                          Dm   C
Quando lembro do forró, vaquejada não deixo não

Dm           Bb
Sou viciado sim
                 F9
Meu vicio é vaquejada
          C                         Dm
É derruba boi dançar forró tomar gelada
                Bb               F9
Sei que desça vida eu não levo nada
                C                 Dm
Em quanto tiver vida eu vou pra vaquejada

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
