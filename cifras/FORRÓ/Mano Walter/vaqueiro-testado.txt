Mano Walter - Vaqueiro Testado

[Intro] G  D  Am  Em

       G                      D
Vai começar mais uma festa de gado
       Am                  Em
Lá no jequi já tudo preparado
        G                     D
Já tem vaqueiro fazendo a inscrição
        Am                    Em
Outros tirando o cavalo do caminhão

               G                      D
Solte o boi na pista que o povo quer ver
             Em                   Bm
O vaqueiro botando o cavalo pra correr
             C                       G
Se a boiada é bruta, o vaqueiro vai soar
              A                  D C B A D
Pra chegar na faixa e o boi derrubar

[Refrão]


      G
Valeu boi, viva a festa do gado
      D
Valeu boi, e o povo grita animado
      Am
E as 4 patas do boi tão pra cima
                     C                      D
E o locutor diz e rima: Eita vaqueiro testado!

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
