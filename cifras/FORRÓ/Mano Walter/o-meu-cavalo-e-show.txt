Mano Walter - O Meu Cavalo É Show

[Intro]  D  A  E  F#m
         D  A  E  F#m
         D  A  E  F#m
         D  A  E  F#m

         D                            A
O meu cavalo é show, o meu cavalo é show
         E                     F#m
O meu cavalo é show, é show papai

         D                            A
O meu cavalo é show, o meu cavalo é show
         E                     F#m
O meu cavalo é show, é show papai

         D                            A
O meu cavalo é show, o meu cavalo é show
         E                     F#m
O meu cavalo é show, é show papai

  D                            A
Chega na disputa o touro sai voando

                E                 F#m
E vai se aproximando do primeiro call
       D                          A
Eu me deito na sela e meu cavalo sai
          E                F#m
Chamo na caióta e o touro cai
     D                     A
E a galera grita em voz alta
      E           F#m
Eita cavalo show papai

         D                            A
O meu cavalo é show, o meu cavalo é show
         E                     F#m
O meu cavalo é show, é show papai

         D                            A
O meu cavalo é show, o meu cavalo é show
         E                     F#m
O meu cavalo é show, é show papai

         D                            A
O meu cavalo é show, o meu cavalo é show
         E                     F#m
O meu cavalo é show, é show papai

         D                            A
O meu cavalo é show, o meu cavalo é show
         E                     F#m
O meu cavalo é show, é show papai

( D  A  E  F#m )
( D  A  E  F#m )
( D  A  E  F#m )
( D  A  E  F#m )

         D                            A
O meu cavalo é show, o meu cavalo é show
         E                     F#m
O meu cavalo é show, é show papai

         D                            A
O meu cavalo é show, o meu cavalo é show
         E                     F#m
O meu cavalo é show, é show papai

         D                            A
O meu cavalo é show, o meu cavalo é show
         E                     F#m
O meu cavalo é show, é show papai

         D                            A
O meu cavalo é show, o meu cavalo é show
         E                     F#m
O meu cavalo é show, é show papai

      D                        A
Eita cavalinho não é de brincadeira
     E                                  F#m
Não perde uma carreira com ele o touro cai
            D                     A
É manso na porteira, e tenso é ligeiro
       E                                    F#m
Pode chamar sem medo, que a pancada é pra trás

          D             A
É pá, é prancha, é pá, bufo
    E             F#m
É show, é show, papai

         D                            A
O meu cavalo é show, o meu cavalo é show
         E                     F#m
O meu cavalo é show, é show papai

         D                            A
O meu cavalo é show, o meu cavalo é show
         E                     F#m
O meu cavalo é show, é show papai

         D                            A
O meu cavalo é show, o meu cavalo é show
         E                     F#m
O meu cavalo é show, é show papai

         D                            A
O meu cavalo é show, o meu cavalo é show
         E                     F#m
O meu cavalo é show, é show papai

( D  A  E  F#m )
( D  A  E  F#m )

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
