Mano Walter - Modo Avião

[Intro] E  F#  G#m  E  F#  B

E                                       B
Juntamos a vontade e o desejo
               C#m              F#
Seu beijo foi o tiro certeiro
E               F#                   B
Você virou minha vida do avesso
                E                  F#
Me levantou do último tropeço

[Pré-Refrão]

                          E     F#     B
Será que eu mereço tanto amor?
                             E                 F#       B
Cê até parece ser o meu anjo protetor

[Refrão]

B                                 E         F#            B
 Eu que emendava paixão atrás de paixão

                        E                        F#        B
Eu que tinha janela aberta pra decepção
                                E                   F#
Você chegou e me tirou do modo avião
G#m                               E                F#            B
Nosso amor é daqueles de um entre um milhão

B                                 E         F#            B
 Eu que emendava paixão atrás de paixão
                        E                        F#        B
Eu que tinha janela aberta pra decepção
                                E                   F#
Você chegou e me tirou do modo avião
G#m                               E                F#            B
Nosso amor é daqueles de um entre um milhão

----------------- Acordes -----------------
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
