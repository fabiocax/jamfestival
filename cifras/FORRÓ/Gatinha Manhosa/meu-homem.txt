Gatinha Manhosa - Meu Homem

[Intro] G  Am  C  D  G  C  D

G
Nada, nada me faz te esquecer
            Em
Não é um sonho um dia eu terei você
        Am
Ao meu lado eternamente a me amar
C                                 D
Como nunca em minha vida amei alguém
G
Mais não sei se o amor pra te chegou
Em
Lentamente aos teus olhos me mostrou
      Am
Não consigo viver longe de você
C                  D    G
Então vem nesta canção pra lhe dizer
G
Quando o amor chegar


E começar te envolver
         Am
E meu corpo te procurar

Querendo te aquecer
 C
Tenha a mão somente
 D
O meu telefone
    G
Que eu irei até você
G
Quando eu te encontrar

Olho no lho eu vou dizer
 Am
Que eu nasci pra te amar

Que eu nasci pra ter você
 C
Porque eternamente tens
           D
Que ser meu homem pra
G
Nunca te perde

[Solo] G  F  Dm  G  Cm

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
