Gatinha Manhosa - Meu Anjo Azul

Intro: Fm  C#  Eb

Ab           Eb
 Hoje eu lembrei
   F                            Ab
Pensei, gostei de tudo entre nós
         Eb          F
És o meu sol, meu céu

Mas nada mais me faz sorrir

Ab           Eb
 Hoje eu lembrei
  F
E sei que ando meio só
Ab           Eb
  Foi tudo que eu sonhei
  F                    C7
E nada existe ao meu redor

          Fm
Meu anjo azul

      C#
Agora sei que não vivo,
       Eb      C7
sem o seu amor

          Fm
Meu anjo azul
                     C#
Te coloquei nos meus sonhos
                Eb      C7
e carrego seja aonde for
          Fm
Meu anjo azul
                Ab
Você me faz pensar demais
    Eb               C7
em ter você sempre comigo
          C#
Meu anjo azul
            Eb
Por onde estás

Ab           Eb
 Hoje eu lembrei
    F                            Ab
Pensei, gostei de tudo entre nós
         Eb           F
És o meu sol, meu céu

Mas nada mais me faz sorrir
Ab           Eb
 Hoje eu lembrei
  F
E sei que ando meio só
Ab           Eb
  Foi tudo que eu sonhei
  F                    C7
E nada existe ao meu redor

           Fm
Meu anjo azul
      C#
Agora sei que não vivo,
       Eb      C7
sem o seu amor

          Fm
Meu anjo azul
                     C#
Te coloquei nos meus sonhos
                Eb      C7
e carrego seja aonde for
          Fm
Meu anjo azul
            Ab
Você me faz pensar demais
    Eb               C7
em ter você sempre comigo
          C#
Meu anjo azul
            Eb
Por onde estás

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
C# = X 4 6 6 6 4
C7 = X 3 2 3 1 X
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
