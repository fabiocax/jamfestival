Gatinha Manhosa - Agenda

D7M                                         G7M
Sei que para você sou apenas mais um número

De uma agenda qualquer
         F#m7               Bm7             Em7 A7
Com o nome trocado para ninguém saber quem é
    D7M                                 G7M
E quando me atende falando de um assunto

Que não tem nada a ver
F#m7                 Bm7
já sei que está dizendo que sua namorada
Em7   A7
Está perto de você

G7M                                         F#m7
Ninguém vai entender porque te aceito assim
                 Bm7                   G7M
Mas ficar sem você é bem pior para mim

Conselhos de amor

                F#m7
Cansei de escutar
                  Bm7
Que não se apaixonou
                   G7M
Não pode me julgar
                         A7(4/9)
Dizer que eu estou errada

              G7M
Deixa eu errar
                                 F#m7
Para este amor quero me entregar
                   Bm7        G7M
Quem sabe o meu dia vai chegar
                                    Em7 A7(4/9)
Porque a vida sem você não faz sentido
               G7M
Deixa eu tentar
                            F#m7
É perigoso mas vou arriscar
                    Bm7          G7M
Com toda a minha força vou lutar
             A7(4/9) A7      D7M (A7)
Para você ficar só comigo

D7M                                         G7M
Sei que você pensa que é apenas mais um número
Numa agenda qualquer
    F#m7               Bm7             Em7 A7
Com o nome trocado para ninguém saber quem é
 D7M                                 G7M

E quando eu te atendo falando de um assunto
Que não tem nada a ver
F#m7                 Bm7
É que minha namorada está sempre do meu lado
Em7   A7
Mas eu quero você

G7M                                         F#m7
Ninguém vai entender porque te aceito assim
              Bm7                   G7M
Mas ficar sem você é bem pior para mim
Conselhos de amor
        F#m7
Cansei de escutar
                 Bm7
Que não se apaixonou
                  G7M
Não pode me julgar
Dizer que estou errado
           G7M
Deixa eu errar
                                F#m7
Para este amor quero me entregar
                   Bm7        G7M
Quem sabe o meu dia vai chegar
                                   Em7 A7(4/9)
Porque a vida sem você não faz sentido
              G7M
Deixa eu tentar
                            F#m7
É perigoso mas vou arriscar
                    Bm7          G7M
Com toda a minha força vou lutar
  A7(4/9) A7      D7M (A7)
Para poder ficar só contigo

              G7M
Deixa eu errar
                                 F#m7
Para este amor quero me entregar
                   Bm7        G7M
Quem sabe o meu dia vai chegar
                                    Em7 A7(4/9)
Porque a vida sem você não faz sentido

              G7M
Deixa eu tentar
                                F#m7
É perigoso mas vou arriscar
                   Bm7        G7M
Com toda a minha força vou lutar
                             Em7 A7(4/9)
Olha, eu vou ficar só contigo

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7(4/9) = 5 X 5 4 3 X
Bm7 = X 2 4 2 3 2
D7M = X X 0 2 2 2
Em7 = 0 2 2 0 3 0
F#m7 = 2 X 2 2 2 X
G7M = 3 X 4 4 3 X
