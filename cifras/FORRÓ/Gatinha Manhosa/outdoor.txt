Gatinha Manhosa - Outdoor

(intro 2x)  C G F Dm F G

 C                   F       C  F   C                     F               G
As vezes fico só pensando em você. Já não sei mais o que faço pra te esquecer.
 Am                  Em           F          Dm            F            Dm       G
Não sei mais o que inventar pra enganar meu coração que sofre calado por essa paixão.
 C                   F       C  F   C                     F           G
Ai como eu desejo com um beijo seu. Escrevi seu nome na palma da minha mão.
 Am                  Em           F         Dm        F              Dm          G         A
Não dava mais pra esconder o que sinto por você. Me tira o sono rolo na cama vejo o amanhecer.
 Bm      G                  D            A               Bm    G        A                 Bm
Vou gritar pra todo mundo ouvir que sou louco por você. Escrever em outdoor pra todo mundo ler.
         G          D                 A           Bm
Quantas vezes eu tentei falar, mas o medo me consome.
   G                     A                       Bm
Dá um frio dentro do peito quando alguém fala seu nome.
 Bm     G            D                  A           Bm         G         A
Vou gritar pra todo mundo ouvir que sou louco por você. Escrever em outdoor pra todo mundo ler.

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
