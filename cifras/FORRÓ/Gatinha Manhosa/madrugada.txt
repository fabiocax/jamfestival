Gatinha Manhosa - Madrugada

(intro) C F C F G

 C                          Am          G
Quando penso em você estou só a enlouquecer.
 C                         Am          G
A saudade que eu sinto é demais. Dá um desespero.
 F                           G
Amor aonde é que você está que não vem me ver?
 F             C      F             C
Onde andarás, amor.Vou te procurar onde for.
 F            G             Am
Onde andarás, amor.Vou te encontra.
 C                  G            F              C       G
Madrugada,olho lá fora e não te vejo e volto a chorar.
 C                  G             Am           F     C
Eu vejo seu rosto sorrindo e tento te abraçar. Não é você.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
