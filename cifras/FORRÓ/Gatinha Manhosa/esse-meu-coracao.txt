Gatinha Manhosa - Esse Meu Coração

( C Em F G )
na solidão da noite
tudo me faz lembrar você
os nossos momentos
nossos encontros e desencontros
marcaram pra sempre nossas vidas

C                  G/B           Am
tudo faz lembrar você aqui
                 Em                  F
pego sua foto e vou dormir
                          C             F/G
na cama ainda sinto teu perfume
C                        G/B                 Am
você sempre sera minha paixão
                               Em
mesmo assim distante
            F                            C                      F/G
o coração chama por você so quer te amar
                   C
mais uma vez

       G/B            Am
me sinto tão sozinho
            Em                 F
to precisando de carinho
              C                            F/G
estou a ponto de enlouquecer
                   C      G/B               Am
mais uma vez estou em tuas mãos
     Em                 F
te pesso pra voltar
                C               F/G
vem me tirar da solidão
                          C    Em   F          G
esse meu coração         ama demais
                          C    Em   F          G
esse meu coração         sofre demais
              Am          Em
sem ter você
                 F                             G
volta pra mim meu grande amor.


E-mail: wagneracn@oi.com.br

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F/G = 3 X 3 2 1 X
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
