Gatinha Manhosa - Amor ou amizade

A                   E            D
Solidão abre essa porta manda embora essa
                Bm              E
Saudade que não faz nenhum sentido
            C#m                          F#m
Eu já encontrei outra pessoa, estamos vivendo numa boa
            Bm                 D           E
Pra que lembrar e revirar as coisas do passado
     A                   F#m          A                F#m
Coração me diz se é verdade, se é paixão, amor ou amizade
       Bm                                     E
Se for amizade senhor o que ela vai pensar de mim
               A  E            F#m
Se me ver chorando ou me lamentando
                       Bm D          C#m     F#m
Sofrendo por um outro amor que só deixou saudade
    C#m       F#m          Bm     A          D       E
É amor ou amizade, vai antiga paixão sai do meu coração!
A                       Bm
Pra que chorar o que passou passou
D           E           A     D
Vou me entregar a esse novo amor

A                       Bm
Pra que chorar o que passou passou
        D                  E                    A   D
Não adianta mais querer colar o que quebrou quebrou

(solo)
     A                   F#m          A                F#m
Coração me diz se é verdade, se é paixão, amor ou amizade
       Bm                                     E
Se for amizade Deus o que ela vai pensar de mim
               A  E            F#m
Se me ver chorando ou me lamentando
                       Bm D          C#m     F#m
Sofrendo por um outro amor que só deixou saudade
    C#m       F#m          Bm     A          D       E
É amor ou amizade, vai antiga paixão sai do meu coração!
A                       Bm
Pra que chorar o que passou passou
D           E           A     D
Vou me entregar a esse novo amor
A                       Bm
Pra que chorar o que passou passou
        D                  E                    A   D
Não adianta mais querer colar o que quebrou quebrou

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
