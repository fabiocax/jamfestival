Gatinha Manhosa - Tudo de Novo

(intro) A  G  D  E

A                                                 A7
Eu quero tudo de novo sentir seu corpo suado e molhado de
 D     G  D          A         D A             E       E4  E
beijos      eu quero sempre mais   ah como eu quero mais
A                                           A7
 Eu quero tudo de novo olhos nos olhos queimando de tanto
 D     G  D          A         D  A             E
desejo      eu quero sempre mais    ah como eu quero mais
   A  A7
você

  D                                      A A7 D
A roupa que eu quero vestir essa noite é você pra descobrir
                          A  A7  D                    E
as estrelas do nosso prazer     como se o tempo e a vida           (2x)

   C#m               F#m  Bm              E
ficassem parados pra nós  numa viajem de baixo dos nossos
   A
lençóis


Bm              E                   A    Bm              E
numa viajem de baixo dos nossos lençóis  numa viajem de baixo
               A
dos nossos lençóis

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E4 = 0 2 2 2 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
