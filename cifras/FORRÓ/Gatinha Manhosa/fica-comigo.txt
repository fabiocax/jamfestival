Gatinha Manhosa - Fica Comigo

C       G           Am
Amo você,  fica comigo
       Dm                G7                 C      G7
O seu jeito de ser me pegou, sem notar sem querer
C         G              Am
Quero você, mas deixa comigo
      Dm              G7                 C     E7
Dá o teu coração, te devolvo em dobro a paixão
     Am                  D7
Já provei na maçã que é você que eu quero
       G                 Bb                   F      Dm
Tudo bem sou teu fã eu confesso te espero amanhã
 G7                  C         Am
 O  nosso amor  é lindo, tão lindo
                    F               C         G7
Nada pode ser mais lindo, do que o nosso amor
                    C       Am
O  nosso amor  é lindo, tão lindo
                    F               C          G7
Nada pode ser mais lindo, do que o nosso amor
C      G          Am
Amo você, fica comigo

      Dm                G7                   C       E7
Uma frase um papel uma noite um motel só nos dois
       Am                     D7
Com você, minhas cartas ficam todas na mesa
         G                      Bb               F      Dm
Eu não posso, eu não quero nenhuma surpresa pra nós
 G7              C                           Am
O nosso amor é lindo (Pra você que eu canto essa canção,
  F                  C        Am             F
dá pra mim todo seu coração, te devolvo em dobro a paixão)
 G7             C                             Am
O nosso amor é lindo (Pra você que eu canto essa canção,
 F                  C              Am               F       G7

dá pra mim todo seu coração, te devolvo em dobro a paixão)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
