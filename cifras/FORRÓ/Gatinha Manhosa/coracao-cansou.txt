Gatinha Manhosa - Coração Cansou

[Intro]  A  B  E  G#m  A  Am

E
Não vou mais te procurar
B                    F#m
Nem sequer você vai ouvir falar
                   A
Que saiu da minha boca
             E
A palavra saudade
                    B
Nunca mais vai me prender
                       F#m
Eu não vou deixar você fazer de mim
             A
O que bem entende
G#m             C#m
Não me chame mais de amor
G#m               C#m
Não me importo se você chorar
A
Eu não ligo, eu acho pouco

        B
pra quem nunca soube amar
E
E pode esquecer os beijos que eu te dei
G#m
As idas e voltas, esquece também
A
Do meu amor
F#m               B
O coração que usou cansou
E
Pode escrever o que eu te falei
G#m
Depois da balada é que a saudade vem
A
Você vai ver
F#m
Que vai tentar
B                E
Mas não vai me esquecer

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
