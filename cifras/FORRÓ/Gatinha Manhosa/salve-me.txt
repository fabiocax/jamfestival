Gatinha Manhosa - Salve Me

Intro:C  Gm  Bb  Dm
      C  Gm  Bb  Dm

C                          Gm
 O que é que eu faço pra mudar
                    Bb
Corro atrás da esperança
                           Dm
Só que você você não vai voltar
C                        Gm
 Sobrevivo mesmo sem tentar
                    Bb
Cheia de nós na garganta
                        Dm
Penso em você ao nem pensar

F                   C
 Pouco a pouco o coração
               Bb            Dm
Vai perdendo a fé perdendo a voz
      G
Salva me do vazio

      Dm
Salva me da solidão
      F
Salva me desse frio
        C
Vem depressa, vem me salvar
       G
Salva me do vazio
      Dm
Salva me da escuridão
       F
Salva me desse frio
        C                  C  Gm  Bb  Dm
Não me deixe sozinha mais
C                    Gm
 Me proponho a continuar
                   Bb
Porém o amor é a palavra
                      Dm
Que deixa a gente duvidar
C                       Gm
 Sobrevivo mesmo sem tentar
                   Bb
Cheia de nós na garganta
                        Dm
Penso em você ao nem pensar

F                   C
 Pouco a pouco o coração
               Bb             Dm
Vai perdendo a fé perndendo a voz


       G
Salva me do vazio
      Dm
Salva me da solidão
      F
Salva me desse frio
        C
Vem depressa, vem me salvar
       G
Salva me do vazio
      Dm
Salva me da escuridão
       F
Salva me desse frio
        C                  C  Gm  Bb  Dm
Não me deixe sozinha mais
       G
Salva me do vazio
      Dm
Salva me da solidão
      F
Salva me desse frio
      C
Vem pressa, vem me salvar
       G
Salva me do vazio
      Dm
Salva me da escuridão
       F
Salva me desse frio
        C
Não me deixe sozinha mais...

Repete Refrão...

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
