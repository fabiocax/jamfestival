Aviões do Forró - Diz Pra Mim

Bm
Pra você tanto faz, tudo bem,
                       A
se eu liguei, se vou chorar
Bm
Quase sempre é assim,
                            A
é inútil eu tentar te encontrar
Em
Me acostumei com seu jeito,
         D                    A
seus defeitos, sua forma de amar
        Em
Tento fugir, mas não consigo
         D                            A
é como vicio que eu nem sei mais controlar
Diz pra mim
              G               D
Que vai me ligar, preciso te ver
         A
Diz pra mim aonde eu te espero

              G                  D
Pra ninguém notar, pra não te perder
       A
Quero fugir, mas sempre eu me entrego
      Em     D    A
Só pra você

Em                             Bm
já tentei outro alguém, me enganei,
                    A
ninguém faz como você
Em
bem ou mal, tou legal,
       Bm                       A
quero mais ficar perto, teu prazer
       Em
me acostumei com seu jeito,
         D                   A
seus defeitos, sua forma de amar
         Em
tento fugir, mas não consigo
            D                             A
é como um vicio que eu nem sei mais controlar
Diz pra mim

             G                D
Que vai me ligar, preciso te ver
        A
Diz pra mim aonde eu te espero
              G                  D
Pra ninguém notar, pra não te perder
        A
Quero fugir, mas sempre eu me entrego
         G    D    A
Só pra você

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
