Aviões do Forró - Você Não Vale Nada

Cm                                   G
Você não vale nada, mas eu gosto de você!
                                      Cm
Você não vale nada, mas eu gosto de você!
                                  G
Tudo que eu queria era saber Porquê?!?
                                   Cm
Tudo que eu queria era saber Porquê?!?

Cm                                   G
Você não vale nada, mas eu gosto de você!
                                      Cm
Você não vale nada, mas eu gosto de você!
                                  G
Tudo que eu queria era saber Porquê?!?
                                   Cm
Tudo que eu queria era saber Porquê?!?

               Bb                        Eb
Você brincou comigo, bagunçou a minha vida.
                 Fm       G         Cm
E esse meu sofrimento não tem explicação.

                 Bb                    Eb
Já fiz de quase tudo tentando te esquecer.
                Fm            G           Cm
Vendo a hora morrer não posso me acabar na mão.

             Bb                        Eb
Seu sangue é de barata, a boca é de vampiro.
               Fm               Cm
Um dia eu lhe tiro de vez meu coração.
               Bb                    Eb
Aí não mais te quero Amor não dê ouvidos
               Fm                  Cm
Por favor me perdoa Tô morrendo de paixão...

          Eb          Bb             Fm          Cm
Eu quero ver você sofrer Só pra deixar de ser ruim
          Ab                      Fm
Eu vou fazer você chorar, se humilhar
         G
Ficar correndo atrás de mim....(2x)

Cm                                   G
Você não vale nada, mas eu gosto de você!
                                      Cm
Você não vale nada, mas eu gosto de você!
                                  G
Tudo que eu queria era saber Porquê?!?
                                   Cm
Tudo que eu queria era saber Porquê?!?  (2x)

( Cm G ) (4x)

(repete tudo)

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
Eb = X 6 5 3 4 3
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
