Aviões do Forró - Quem Ama Cuida

Intro: B, F#, G#m, Ebm, E, F#, B, F#

     B
Não ligue se eu lhe perguntar
                F#
Nem vá se estressar se eu quiser saber
     G#m
Com quem e aonde você tá
                Ebm
Que hora vai voltar, e o que vai fazer...
     E
Não tô pegando no seu pé
                       B
É que quando a gente quer a gente vai à luta
             C#m
Mas não desligue o celular, eu vou te rastrear
F#                 B       F#
Porque quem ama cuida

B
Ei não é ciúme, eu confio em você

            F#
E vou tá sempre onde você estiver
    G#m
E mesmo estando ausente, eu vou estar presente
      Ebm
Toda hora que você quiser...
    E
Se me chamar eu vou e se for pra fazer amor
                 B
Eu largo tudo, tudo e vou à luta
           C#m
Mas não desligue o celular, eu vou te rastrear
F#                B      F#
Porque quem ama cuida...

                   B
Eu vou cuidar de você
                 F#            G#m
Todo dia, toda hora, e todo momento
                   Ebm                 E
Você jamais vai duvidar do meu sentimento
           C#m               F#
É de dar inveja tanto amor assim

B
Eu vou cuidar de você
                F#           G#m
Todo dia, toda hora, e todo momento
                 Ebm               E
Eu sou capaz de ler até seu pensamento
        F#                      B
Só pra saber se tá pensando em mim

----------------- Acordes -----------------
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
Ebm = X X 1 3 4 2
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
