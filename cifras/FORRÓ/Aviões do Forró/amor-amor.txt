Aviões do Forró - Amor, Amor

INTRODUÇÃO: C#m, A, G#

C#m
Não estou sofrendo, não estou morrendo
A                G#m
Nem tô correndo atrás de um namorado
C#m
Não estou chorando, nem me arrastando
A                      G#m
Cala essa boca, você está muito enganado
C#m
Não estou sofrendo, não estou morrendo
A                G#m
Nem tô correndo atrás de um namorado
C#m
Não estou chorando, nem me arrastando
A                     G#m
Cala essa boca, você está muito enganado
F#m                       E       B
Tá pensando que já é dono do meu bem-querer
F#m                                  E            B
Só porque eu te olhei não quer dizer que eu quero amar

   F#m    G#   G#7
você

Refrão
       C#m
Amor, amor
                                            B
Se pensa que é assim que é só chamar que eu vou
                                       F#m
O que você quiser, é só pedir, que eu dou
                                     G#     G#7
É que eu não resisto ao teu poder de sedução
       C#m
Amor, Amor
                           B
Se toca de uma vez e tenta entender
                            F#m
Debaixo desta roupa vive uma mulher
                                   G#
E dentro deste corpo bate forte um coração
       C#m
Comigo não

INTR: C#m,A,G#

C#m
Poderosa

Atrevida
A               G#
Ninguém se mete mais na minha vida
C#m
Se eu tô dançando, tô te tocando
A                    G#
Não significa que eu estou me apaixonando
F#m                        E       B
Tá pensando que já é dono do meu bem-querer
F#m                                   E            B
Só porque eu te olhei não quer dizer que eu quero amar
   F#m
você

Refrão
       C#m
Amor, amor...

Refrão
C#m
Poderosa

Atrevida
A                 G#m
Ninguém se mete mais na minha vida
C#m
Se eu tô dançando, tô te tocando
A                      G#
Não significa que eu estou me apaixonando


Essa é D +

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G# = 4 3 1 1 1 4
G#7 = 4 6 4 5 4 4
G#m = 4 6 6 4 4 4
