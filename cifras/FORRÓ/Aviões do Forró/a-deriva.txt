Aviões do Forró - À Deriva

Aviões do Forró - Ao vivo - Vol.3

SOLO: 3X base A B E D#m C#m B A ...)

C#m
És tudo em minha vida
      A
Sem você não sou nada
      B
Te amaria de novo

Já não tenho palavras
      A                   B
Por favor me abraça e me diz
             C#m
Que não vive sem mim

C#m
Meu bem não tenha medo
      A
Suas caricias me acalmam

      B
É verdade eu te sinto por toda madrugada
      A                 B            C#m
Como se tudo em minha mente fosse explodir

A                 B
Só sei que quero levar-te comigo
         E       D#m      C#m   B
Pois todo azul do mar dei a ti
A                     B
Eu sempre esperei e quero esse amor
E       D#m      C#m     B
Porque contigo sou feliz
A             B
Contigo sou feliz

                   E
Nós dois estamos perdidos
       B           C#m
Em um barco sem destino
      B              A
Naufrago de amor proibido
  E              A           B
Atracados pelos mares da paixão
  E              B       C#m
Perdidos, em um barco á deriva
        B            A
Almas gêmeas de uma vida
E                 A           B         C#m
Esperando de uma vez a nossa terra prometida

SOLO: (base A B E D#m C#m B A B C#m)

         C#m                        A
Quero encontrar um caminho a nossa felicidade
    B
Dependendo um do outro, nos amar de verdade
    A                 B
Somos um coração q se ama
           C#m
E não vai naufragar


A                 B
Só sei que quero levar-te comigo
         E       D#m      C#m   B
Pois todo azul do mar dei a ti
A                     B
Eu sempre esperei e quero esse amor
E       D#m      C#m     B
Porque contigo sou feliz
A             B
Contigo sou feliz

                   E
Nós dois estamos perdidos
       B           C#m
Em um barco sem destino
      B              A
Naufrago de amor proibido
  E              A           B
Atracados pelos mares da paixão
  E              B       C#m
Perdidos, em um barco á deriva
        B            A
Almas gêmeas de uma vida
E                 A           B         C#m
Esperando de uma vez a nossa terra prometida

SOLO
FINAL

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
D#m = X X 1 3 4 2
E = 0 2 2 1 0 0
