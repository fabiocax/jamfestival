Aviões do Forró - Choro

A, E/G#, F#m

F#m     C#m     D                   A, E/G#, F#m
Choro, choro, choro com saudade de você
F#m     C#m     G                        A
Choro, choro, choro e você não quer nem saber...

Intro: F#m, C#m, D, A (4x)
A, Aº, Bm

Bm                                 F#m
Tá difícil, complicado pra você voltar
Bm                      D                  A
Tiro pelo jeito quando passa sem olhar pra mim
Bm                                     F#m
Se abraçando e se beijando como fazia comigo
Bm                          D                 E
Não quer nem saber se tô sofrendo, não tá nem aí...

D                      E                    F#m
Pisa no meu coração, maltrata, quer me ver chorando

               F#m, E, D
E eu te amando
D                          E               F#m
Como pode alguém ser desse jeito? Vou me acabando
                E
Vivo chorando...
D                        E
Você só faz isso porque gosto de você
D                           E
Se fosse o contrário não deixaria fazer...

F#m     C#m     D                   A, E/G#, F#m
Choro, choro, choro com saudade de você
F#m     C#m     G                        A
Choro, choro, choro e você não quer nem saber...

----------------- Acordes -----------------
A = X 0 2 2 2 0
Aº = 5 X 4 5 4 X
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
