Aviões do Forró - Interior da Bahia

G# C# Bbm

Bbm
Mudou de telefone e eu não sabia
                             G#
Logo depois que a gente terminou
Faz um mês, Faz um mês

Bbm
E foi morar na casa de uma amiga
                   G#
Lá no interior da Bahia

Bbm
Eu já tô começando a botá fé
                            G#
Que dessa vez você foi de verdade
E não quer saber de mim

Bbm
Eu vou botar seu nome no jornal

Bbm7                     G#
Você vai ser notícia mundial
                 Eb
Vou gritar por você

G#
Amor!
C#
Eu tô gritando pra ver se cê aparece
C#m
Só me desculpa esse desespero

G#
Amor!
C#
A onde você for eu vou
C#m
No interior no exterior
Cm
Amor

----------------- Acordes -----------------
Bbm = X 1 3 3 2 1
Bbm7 = X 1 3 1 2 1
C# = X 4 6 6 6 4
C#m = X 4 6 6 5 4
Cm = X 3 5 5 4 3
Eb = X 6 5 3 4 3
G# = 4 3 1 1 1 4
