Aviões do Forró - Por Causa Dela

    {  A                                          E
    {  Deixei de beber, por causa dela
    {                                             A
    {  Parei de fumar, por causa dela
2x{                                                  D
    {  Farra nem pensar, por causa dela
    {                    A              E                  A
    {  Ela me deixou e hoje eu sofro por ela

                                         E
Voltei a beber, por causa dela
                                         A
Voltei a fumar, por causa dela
                                          D
To na farra, pra esquecer ela
                       A              E                 A
Mas se ela voltar, eu deixo tudo por ela


     { Fiz tudo por ela
     {                                  E
     { Nada, nada ela me fez

 2x{ Se ela voltar
     {                                A
     { Eu faço tudo outra vez

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
