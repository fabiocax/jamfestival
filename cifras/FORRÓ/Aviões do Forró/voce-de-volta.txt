Aviões do Forró - Você de Volta

Intro.: G D/F# C Cm

G                 D/F#
Tudo que eu quero é você de volta
C                        Cm
Estou te esperando vem bater na minha porta
G           D/F#
Eu amo você só sei te querer
C                      Cm
Minha vida tem sentido se tiver você

G D/F# C Cm 3x

G
Eu vivo buscando em alguém
G                               Em           D
Alguma coisa que eu sei que só existe em você
G
Eu quero a metade de mim que é você
G                                Em          D
O riso que irradia o mundo e que me faz viver

C                                 Em
Eu... Eu não agüento mais essa saudade
C                     Em               D D4 D
Essa solidão que me invade e me faz ver

Refrão:

G                 D/F#
Tudo que eu quero é você de volta
C                        Cm
Estou te esperando vem bater na minha porta
G           D/F#
Eu amo você só sei te querer
C                      Cm
Minha vida tem sentido se tiver você

Repete Refrão

G
Eu vivo buscando em alguém
G                               Em           D
Alguma coisa que eu sei que só existe em você
G
Eu quero a metade de mim que é você
G                                Em          D
O riso que irradia o mundo e que me faz viver
C                                 Em
Eu... Eu não agüento mais essa saudade
C                     Em               D D4 D
Essa solidão que me invade e me faz ver

Refrão:

G                 D/F#
Tudo que eu quero é você de volta
C                        Cm
Estou te esperando vem bater na minha porta
G           D/F#
Eu amo você só sei te querer
C                      Cm
Minha vida tem sentido se tiver você

Repete Refrão 2x

G D/F# C Cm 2x

----------------- Acordes -----------------
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D4 = X X 0 2 3 3
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
