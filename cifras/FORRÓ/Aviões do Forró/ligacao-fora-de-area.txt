Aviões do Forró - Ligação Fora de Área

C#m
Ai, como eu queria estar perto de você
A                                    E
Não agüento mais um só minuto sem te ver
             B
Por que foi embora? Volta!

C#m
Tudo está tão triste desde que você se foi
A                                         E
São tantas lembranças que eu tenho de nós dois
           B
Onde está você?

F#m
Quando chega à noite bate aquele desespero
C#m                                   A
Rolo pela cama agarrada ao travesseiro
    F#m         E   B
Não dá pra segurar
F#m
Pego o celular na esperança de encontrá-lo

C#m                                      A
Mas a ligação me diz que está fora de área
F#m         B
Quero te amar

E                B             C#m
Ai, que vontade de ficar com você
                     G#m      A      F#m
Em uma palavra só queria dizer:
       B
- Eu te amo, te amo, te amo
 E                  B        C#m
Quero falar olhando no seu olhar
                       G#m        A      F#m
E dar um fim nessa saudade que está
 B                               E
Queimando, queimando, queimando

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
