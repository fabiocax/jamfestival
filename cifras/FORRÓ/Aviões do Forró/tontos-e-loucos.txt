Aviões do Forró - Tontos e Loucos

E|-5-5-5--4-2-0----------------|  |
B|---------------4-2-0---------|  |
G|---------------------2-4-----|  |
D|-----------------------------|  |
A|-----------------------------|  |
E|-----------------------------|  |
                                  | X2
E|-7-7-7--5-4---4--------------|  |
B|------------7---7--5--4-4/5--|  |
G|-----------------------------|  |
D|-----------------------------|  |
A|-----------------------------|  |
E|-----------------------------|  |
                                  
                              
E|-----------------------------|    |--------------|
B|--5-5-5-5--4---4-4-4---------|    |--5-4-5-4-5---|
G|-------------6-------6-------|(X2)|--------------|
D|-----------------------------|    |--------------|
A|-----------------------------|    |--------------|
E|-----------------------------|    |--------------|

C#m                           B
O tempo nunca fez, eu te esquecer
         F#m                 C#m
Não apagaram as marcas desse amor
        A                                 B
Ainda sinto o sabor do teu beijo em minha boca
       E
Ainda sinto tua mão,
                    C#m
acariciando a minha pele...
         A        B      C#m
E eu não quero seguir assim...
A            B                C#m
Estando com ela e pensando em ti...
  A                 B
Comigo aconteceu igual...
     E                 C#m
Não deixo de pensar em ti...
    A                             B
Tem dias que acordo, contigo na cabeça...
E                C#m
Chamando o teu nome...
         A         B      C#m
E eu não quero seguir assim...
A            B                 C#m
Estando com ela e pensando em ti...
C#m                                  B
Que tontos, que loucos que somos nós dois
             A              C#m
Estando com outro e nos amando...

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
