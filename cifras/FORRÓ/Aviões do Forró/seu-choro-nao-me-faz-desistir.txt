Aviões do Forró - Seu Choro Não Me Faz Desistir

 A                             Bm C#m  D
Seu choro não me faz desistir
                                    A
Eu vou embora, cair fora dessa relação
                  Bm C#m  D
Se depender de mim
                                    A
Eu louco subo até nas asas de um Avião
                E
Só pra não te ver mais, nunca mais

 Bm               F#m
Amor, se você me deixar
                D    Am                    E
Nem o céu, e o mar será maior que minha solidão
 Bm                     F#m
Juro, que eu mudo o meu jeito
              D            Am                  E
Mas não tem direito de me trocar por uma outra paixão

Refrão ------------- 


A
Eu não te quero mais
                 D
Não gosto de você
                                           A
Você não tem mais nada a ver com minha decisão
                      D
Eu vou embora desse Avião
               E
Vou bem pra longe
A
Eu não te quero mais
                  D
Não gosto de você
                                            A
Você não tem mais nada a ver com minha decisão
                      D
Eu vou embora desse Avião
               E                 A
Vou bem pra longe do seu coração

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
