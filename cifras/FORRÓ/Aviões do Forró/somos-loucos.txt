Aviões do Forró - Somos Loucos

Intr.: Bm G A

D
Essa morena mais gostosa
                      Bm
que eu encontrei para amar
aha, aha, aha
D
Quando passa,
para tudo
              Bm
faz a galera delirar
aha, aha, aha


Mexe a bundinha
Faz dedinho na boquinha
      G
pega na minha mão
          A
e vai na sua cinturinha

     Bm
Vai descendo até o chão
Põe a mão na minha mão
   G
Sobe, sobe, sobe
        A
que explodiu meu coração
Bm
mexe a bundinha
Faz dedinho na boquinha
G
pega na minha mão
        A
e vai na sua cinturinha
Bm
Vai descendo até o chão
Põe a mão na minha mão
G
Sobe, sobe, sobe
       A
que explodiu meu coração
Em           Em/D#
Gosto tanto de te ouvir dizer
              A7
Que sou teu gato
Em          Em/D#
Gosto tanto de te ver
               A7
Fazendo amor no carro

Refrão

Bm
Nós somos loucos
        A
Muito loucos
Bm
Nós somos loucos
        A
Muito loucos
Bm
Nós somos loucos...

D
Essa morena mais gostosa...

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
Em/D# = X X 1 0 0 0
G = 3 2 0 0 0 3
