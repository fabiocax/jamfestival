Aviões do Forró - Não Deu

(Cm  A#  Cm)

      Cm
Não deu pra te esquecer
      G#                                         D#          A#   Cm
Não deu pra deixar de te amar, não deu, não deu

            Cm                        G#                                   D#                    A#
Tentei fugir de você, me afastar me esconder, mas não deu, foi tudo em vão
                       Fm                                  G#                            G
Brigar com coração, que só pensa em te amar, não tem como evitar
                                G#                                        A#
Vamos passar uma borracha, esquecer tudo que passou
                       Cm
A gente não merece sofrer tanto assim
                              G#                                   A#                  Cm
Pra quer ficar alimentando solidão, se eu gosto de você e você gosta de mim
      Fm                               G#                G   Cm
Desculpa, mas o culpado de tudo isso é você

       Cm
Não deu pra te esquecer

       G#                                         D#          A#  G
Não deu pra deixar de te amar, não deu, não deu    (2X)

INTRO: Cm   G#   D#   A#  G

            Cm                        G#                                   D#                    A#
Tentei fugir de você, me afastar me esconder, mas não deu, foi tudo em vão
                       Fm                                  G#                            G
Brigar com coração, que só pensa em te amar, não tem como evitar
                                G#                                        A#
Vamos passar uma borracha, esquecer tudo que passou
                       Cm
A gente não merece sofrer tanto assim
                              G#                                   A#                  Cm
Pra quer ficar alimentando solidão, se eu gosto de você e você gosta de mim
      Fm                               G#                G   Cm
Desculpa, mas o culpado de tudo isso é você

       Cm
Não deu pra te esquecer
       G#                                         D#          A#  G
Não deu pra deixar de te amar, não deu, não deu    (2X)

INTRO: Cm   G#   D#   A#  G

      Cm
Não deu

----------------- Acordes -----------------
A# = X 1 3 3 3 1
Cm = X 3 5 5 4 3
D# = X 6 5 3 4 3
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
