Aviões do Forró - Vaqueiro Desmantelado

Dm                   A#                      F
Eu tô na vida de solteiro já larguei a de casado
         C                   Dm
Hoje eu sou vaqueiro desmantelado

Dm                   A#                      F
Eu tô na vida de solteiro já larguei a de casado
         C                   Dm
Hoje eu sou vaqueiro desmantelado

Dm                A#                 F
E Começou a vaquejada uma disputa acirrada
                    C                        Dm
É gado, cavalo e vaqueiro, vaqueiro cavalo e gado

Dm            A#                   F
Também a mulherada entra na competição
                      C                       Dm
Pra ver quem derruba boi e conquistar meu coração
                   A#                  F
E eu só tangendo o gado, só tangendo o gado

            C                          Dm
Levando a vaqueira pra fazer o amor no mato


Dm                   A#                      F
Eu tô na vida de solteiro já larguei a de casado
         C                   Dm
Hoje eu sou vaqueiro desmantelado
              A#                  F
Só tangendo o gado, só tangendo o gado
            C                          Dm
Levando a vaqueira pra fazer o amor no mato
Dm                A#                 F
E Começou a vaquejada uma disputa acirrada
                    C                        Dm
É gado, cavalo e vaqueiro, vaqueiro cavalo e gado

Dm            A#                   F
Também a mulherada entra na competição
                      C                       Dm
Pra ver quem derruba boi e conquistar meu coração
                   A#                  F
E eu só tangendo o gado, só tangendo o gado
            C                          Dm
Levando a vaqueira pra fazer o amor no mato

Dm                   A#                      F
Eu tô na vida de solteiro já larguei a de casado
         C                   Dm
Hoje eu sou vaqueiro desmantelado

----------------- Acordes -----------------
A# = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
