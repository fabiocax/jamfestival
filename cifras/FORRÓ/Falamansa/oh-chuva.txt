Falamansa - Oh! Chuva

   C#m                 G#m
Você que tem medo de chuva
   C#m               G#m
Você não é nem de papel
       A              E
Muito menos feito de açúcar
   A                 G#7
Ou algo parecido com mel

       C#m                   G#m
Experimente tomar banho de chuva
        C#m              G#m
E conhecer a energia do céu
       A               E
A energia dessa água sagrada
             A                  G#7
Que nos abençoa da cabeça aos pés

     C#m      A           G#7    C#m
Oh chuva, eu peço que caia devagar
    A           G#7         C#m
Só molhe esse povo de alegria, alegria

 A           G#7     C#m  A           G#7
Para nunca mais chorar, para nunca mais cho

     C#m      A           G#7    C#m
Oh chuva, eu peço que caia devagar
    A           G#7      C#m
Só molhe esse povo de alegria, alegria
 A           G#7     C#m  A           G#7
Para nunca mais chorar, para nunca mais cho

C#m   A   G#7
Iô, iô
C#m   A   G#7
Iô, iô
C#m   A   G#7
Iô, iô
C#m   A   G#7
Iô, iô
C#m   A   G#7
Iô, iô
C#m   A   G#7
Iô, iô

   C#m                 G#m
Você que tem medo de chuva
   C#m               G#m
Você não é nem de papel
       A                E
Muito menos feito de açúcar
   A                   G#7
Ou algo parecido com mel

       C#m                   G#m
Experimente tomar banho de chuva
        C#m              G#m
E conhecer a energia do céu
       A               E
A energia dessa água sagrada
             A                 G#7
Que nos abençoa da cabeça aos pés

     C#m      A           G#7    C#m
Oh chuva, eu peço que caia devagar
    A           G#7         C#m
Só molhe esse povo de alegria, alegria
 A           G#7     C#m  A           G#7
Para nunca mais chorar, para nunca mais cho

     C#m      A           G#7    C#m
Oh chuva, eu peço que caia devagar
    A           G#7         C#m
Só molhe esse povo de alegria, alegria
 A           G#7     C#m  A           G#7
Para nunca mais chorar, para nunca mais cho

C#m   A   G#7
Iô, iô
C#m   A   G#7
Iô, iô
C#m   A   G#7
Iô, iô
C#m   A   G#7
Iô, iô
C#m   A   G#7
Iô, iô
C#m   A   G#7
Iô, iô

     C#m      A           G#7    C#m
Oh chuva, eu peço que caia devagar
    A           G#7         C#m
Só molhe esse povo de alegria, alegria
 A           G#7     C#m  A           G#7
Para nunca mais chorar, para nunca mais cho

     C#m      A           G#7    C#m
Oh chuva, eu peço que caia devagar
    A           G#7         C#m
Só molhe esse povo de alegria, alegria
 A           G#7     C#m  A           G#7
Para nunca mais chorar, para nunca mais cho

C#m   A   G#7
Iô, iô
C#m   A   G#7
Iô, iô
C#m   A   G#7
Iô, iô
C#m   A   G#7
Iô, iô
C#m   A   G#7
Iô, iô
C#m   A   G#7
Iô, iô

----------------- Acordes -----------------
A = X 0 2 2 2 0
Ab = 4 3 1 1 1 4
C#m = X 4 6 6 5 4
Eb = X 6 5 3 4 3
G#7 = 4 6 4 5 4 4
G#m = 4 6 6 4 4 4
