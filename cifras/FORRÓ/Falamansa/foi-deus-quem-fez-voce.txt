﻿Falamansa - Foi Deus Quem Fez Você

Capo Casa 1

                  C                 Em      C7
Foi Deus que fez o céu, o rancho das estrelas
       F                                   Em  C7
Fez também o seresteiro para conversar com elas
       F       Em                        D4(7)  D7
Fez a lua que prateia minha estrada de sorrisos
Em    F             Em                         D7  G
E a serpente que expulsou mais de um milhão do paraíso
                    C
Foi Deus quem fez você
                  Em  C7
Foi Deus que fez o amor
      F                                  Em  C7
Fez nascer a eternidade num momento de carinho
    F          Em                 D4(7)  D7
Fez até o anonimato dos afetos escondidos
Em    F         Em                     D7  G7
E a saudade dos amores que já foram destruídos
      C  F  C
Foi Deus
                     Em
Foi Deus que fez o vento

                   Am
Que sopra os teus cabelos
                     Em
Foi Deus quem fez o orvalho
                  F         G7
Que molha o teu olhar, teu olhar
                     C
Foi Deus que fez a noite
               Am
E o violão plangente
                   Em
Foi Deus que fez a gente
              F  G7          C  Am
Somente para amar, só para amar
 Dm  G  C  F  C
Só para amar

----------------- Acordes -----------------
Capotraste na 1ª casa
Am*  = X 0 2 2 1 0 - (*A#m na forma de Am)
C*  = X 3 2 0 1 0 - (*C# na forma de C)
C7*  = X 3 2 3 1 X - (*C#7 na forma de C7)
D4/7*  = X X 0 2 1 3 - (*D#4/7 na forma de D4/7)
D7*  = X X 0 2 1 2 - (*D#7 na forma de D7)
Dm*  = X X 0 2 3 1 - (*D#m na forma de Dm)
Em*  = 0 2 2 0 0 0 - (*Fm na forma de Em)
F*  = 1 3 3 2 1 1 - (*F# na forma de F)
G*  = 3 2 0 0 0 3 - (*G# na forma de G)
G7*  = 3 5 3 4 3 3 - (*G#7 na forma de G7)
