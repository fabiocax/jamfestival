Falamansa - Pra Viver Mais

[Intro] C  Bm  Em

( D  Bm  Em )

      C                       B7
Se a voz agrada, acalma o seu interior
           Em
Se a batida que sai é de um coração
        C                       B7
Que ás vezes sem vontade não quer nem bater
      Em
E ás vezes dispara sem saber porque

( C  B7  Em )

Eu sei, e por favor entenda
Dancei, talvez agora aprenda
Eu sei, e peço: compreenda
Dancei, talvez agora aprenda a viver mais

C                          B7
E se a felicidade não quer nem me ver

 Em
Aí me distraio com essa canção
        C                     B7
Que, ás vezes, sem maldade, fala de você
      Em
E, ás vezes, não sabe mais o que dizer

----------------- Acordes -----------------
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
