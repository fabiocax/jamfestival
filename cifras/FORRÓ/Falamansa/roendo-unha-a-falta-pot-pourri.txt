Falamansa - Roendo Unha / A Falta (Pot-Pourri)

Intro: Gm D7 (2x) Gm Cm D7 Gm

           D7
Quando Vim Vim cantou
          Gm
Corri pra ver você
          D7
Atrás da serra o sol
           Gm       G7
Tava pra se esconder              (2x)
          Cm
Quando você partiu
             Gm
Eu não me esqueço mais
         D7
Meu coração, amor
         Gm
Partiu atrás

                   Gm                                           D7
Com os óio na ladeira, quando vejo uma poeira penso logo que é você

D7                                                                   Gm
Fico de orelha alevantada para o lado da estrada que atravessa o massapê
Gm                                                                        D7                 2x
Óia que eu já tô roendo unha, a saudade é testemunha do que agora eu vou dizer
D7                                                                      Gm
Quando na janela eu me debruço meu cantar é um soluço a galopar no massapê

SOLO: Gm D7 (2x) Gm Cm D7 Gm

      Gm Bb Cm D (2x)

Gm                        Eb
Não suporto mais te ver assim
                       Cm
Tão solitário e tão carente e eu ausente
        D
olha a vida que deram pra gente.
Gm                           Eb
Não me importo mais em ser assim
                      Cm
Tão complicado,mas valente,sigo em frente,
      D
faço tudo que for diferente

Gm
Mas correr atrás já é demais,
         Eb
se você corre para trás,
                 Cm
eu tenho direito de seguir em frente.
     D
Que atrás vem um monte de gente.
Gm
E se algum momento o sentimento te diz
Eb                           Cm
que eu não te amo, não tem cabimento eu não aguento.
       D
Não ter o seu reconhecimento.

                    Gm                    Eb
Eu sinto falta de você,sei a falta que é você
                 Cm                       D
Sinto falta de você ,sei a falta que é você iê iê iê      2x

Gm                       Eb
Não suporto mais viver assim
                    Cm
pela metade sem maldade
                  D
e a realidade, é o meu coração que não mente
Gm                           Eb
Não me importo mais em ser assim
                     Cm
tão isolado e tão distante
                D
é importante saber um do outro o que sente
Gm
Mas correr atrás já é demais,
         Eb
se você corre para trás,
                 Cm
eu tenho direito de seguir em frente.
     D
Que atrás vem um monte de gente.
Gm
E se algum momento o sentimento te diz
Eb                           Cm
que eu não te amo, não tem cabimento eu não aguento.
       D
Não ter o seu reconhecimento.

                    Gm                    Eb
Eu sinto falta de você,sei a falta que é você
                 Cm                       D
Sinto falta de você ,sei a falta que é você iê iê iê     2x


SOLO: Gm Eb Cm D (2x)

                    Gm                    Eb
Eu sinto falta de você,sei a falta que é você
                 Cm                       D
Sinto falta de você ,sei a falta que é você iê iê iê     4x

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Eb = X 6 5 3 4 3
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
