Falamansa - Na Medida

(Em Bm Em C B) repete a musica toda, escutem antes pra pegar a batida

Bem que tentou
Até queria ajudar mas errou
Ao partir e me deixar
Foi na medida
Acertou em cheio meu coração
Valeu a tentativa
Valeu a intenção
Vai, faz bem em me deixar
Eu me encontrei e hoje to numa boa
E por isso a gente canta
Dizem que o mal espanta
Quem sou eu pra duvidar

----------------- Acordes -----------------
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
Em = 0 2 2 0 0 0
