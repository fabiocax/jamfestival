Falamansa - Amigo Velho

[Intro]

E|-----------------------------------------------------------0-0--|
B|-----------------------------------------------0-----------1-1--|
G|---------0---------------------0-----------------2-0-------0-0--|
D|-----------2-0-------------0-2---2 0-----------------------2-2--|
A|---------------0-2-3-3---3---------------------------0-2-3-3-3--|
E|-3-3---3-------------------------------3-3---3------------------|

G        C
  Amigo velho
              G
Eu te desejo sorte
           C
Te desejo tudo de bom
                   G
Tô com você até a morte
                     C
Eu sei você faria o mesmo
                    G
Amigo você faria o mesmo

                     C    G
Eu sei você faria o mesmo

       C                   G
Amigo velho, eu te desejo paz
        C
Desejo tudo de bom
                  G
Acreditando cada dia mais
                     C
Eu sei você faria o mesmo
                       G
Se estivesse em meu lugar
                    C
Amigo você faria o mesmo
                          G
Eu tenho alguém com quem contar

        C
Das histórias vividas com você
        G
E as risadas que ainda vamos dar
       C
Das batalhas vencidas sem saber
           G
Que ainda tinha uma guerra pra lutar
        C
Saiba que estou aqui quando sofrer
      G                        C
Estendendo a mão pra te ajudar

                     G
Eu sei você faria o mesmo
                    C
Amigo você faria o mesmo
                     G
Eu sei você faria o mesmo
       C            G
Amigo velho, amigo velho
       C            G
Amigo velho, amigo velho

( C  G  C  G )

       C                   G
Amigo velho, eu te desejo paz
        C
Desejo tudo de bom
                  G
Acreditando cada dia mais
                     C
Eu sei você faria o mesmo
                       G
Se estivesse em meu lugar
                    C
Amigo você faria o mesmo
                          G
Eu tenho alguém com quem contar

        C
Das histórias vividas com você
        G
E as risadas que ainda vamos dar
       C
Das batalhas vencidas sem saber
           G
Que ainda tinha uma guerra pra lutar
        C
Saiba que estou aqui quando sofrer
      G                        C
Estendendo a mão pra te ajudar

                     G
Eu sei você faria o mesmo
                    C
Amigo você faria o mesmo
                     G
Eu sei você faria o mesmo
       C            G
Amigo velho, amigo velho
       C            G
Amigo velho, amigo velho

( C  G  C  G )
( C  G  C  G )
( C  G  C  G  Am  G )

----------------- Acordes -----------------
C = X 3 2 0 1 0
G = 3 2 0 0 0 3
