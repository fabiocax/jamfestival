Falamansa - Lixo No Lixo

Intro: Bm A G F#7

 Bm                  A
 Se no dia em que o mar enlouquecer
G                F#7
 O dia em que o sol se esconder
 Bm               A
 O dia em que a chuva não conter
G             F#7
 O choro que caí, pra te dizer
 Bm                    F#7
 Que acabou o mundo e não sobrou mais nada
 Bm           F#7
 Sujou a sua terra

 Poluiu a água

Bm              F#7              G  F#7
 E não há uma chance de sobreviver

 Bm                  A
 Se no dia em que o solo empobrecer

G          F#7
 O céu deixar de azul ser
Bm               A
 O dia em que a lua vir nascer
G         F#7
 Tarde demais pra te dizer
Bm               F#7
 Que ainda resta um sonho, uma esperança
Bm               F#7
 Atrás de um sorriso de qualquer criança
Bm           F#7              G   F#7
 Só há uma chance de sobreviver

    Bm
 E você
           F#7
 Salvou o mundo?
       Bm           F#7
 Ou se acabou com ele
         Bm          F#7
 O teu chão era imundo
       Bm             F#7
 Você soube cuidar dele?
          Bm                        F#7
 Jogando lixo no lixo, no lixo, no lixo
          Bm                        F#7
 Jogando lixo no lixo, no lixo, no lixo

(Intro)

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
F#7 = 2 4 2 3 2 2
G = 3 2 0 0 0 3
