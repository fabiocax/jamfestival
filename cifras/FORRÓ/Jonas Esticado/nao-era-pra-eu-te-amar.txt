Jonas Esticado - Não Era Pra Eu Te Amar

[Intro] E  G#m   F#

E
Eu te falei que era só

Por uma vez
G#m                 F#
Só uma noite uma aventura

E não se envolver
E
Mas quando te beijei
                           G#m
Senti algo explodir por dentro
             F#
Foi tão de repente
               E
A paixão me flechou
                        G#m
Foi um tiro certeiro de amor
Que acertou no meu peito

        F#
Coração não aguentou

[Refrão]
                  E
Não era pra eu te amar
                     G#m
Não era pra me apaixonar
                    F#
Mas você fez tão perfeito
                  E
Não era pra eu te amar
                     G#m
Não era pra me apaixonar
             F#
Não era, não era

( E  G#m  F#  E )


----------------- Acordes -----------------
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
