Jonas Esticado - O Nosso Caso Estourou

[ Intro ]  C  G  C  G

C
Ei, a partir de hoje
                           F
Não vamos mais ficar escondidos
Não vamos precisar mais
                 Dm
Viver um amor proibido
Já liguei pros seus pais
                           F
Hoje a noite vai ter um jantar
             G
E eu vou tá lá

        F
Não precisa mais ficar com medo
                          G
Eu já revelei nosso segredo
Em
O meu sogro já marcou a hora

     Am
Já brinquei até com minha sogra
   F
No celular
          C
Chegou a hora de falar
De assumir
     G
Toda verdade

                   C
O nosso caso estourou
Tá na boca do povo
                              G
É o mais comentado de toda cidade
Os seus pais uma hora
                            Dm
Ou outra iam descobrir a verdade
                         F
O melhor é a gente assumir
     G
Assumir
                    C
O nosso caso estourou
Tá na boca do povo
                               G
É o mais comentado de toda cidade
Os seus pais uma hora
                              Dm
Ou outra iam descobrir a verdade
                       F
O melhor é a gente assumir
     G
Assumir

        F
O nosso caso estourou
        G
Não se fala em outra coisa
   Am
Na cidade

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
