Jonas Esticado - Meu Coração Pegou Ar (part. Matheus e Kauan)

[Intro] Bb  F  Cm  Eb

[Primeira Parte]

         Eb                F
Pode sentar na primeira cadeira
             Gm
Do primeiro bar que você encontrar
        Eb                 F
Pode pedir uma dose caprichada
                Gm
de vergonha na cara

Cê vai precisar

[Pré-Refrão]

        Eb                  F
Não pensou em nada, quando deu mancada
   Gm
E saiu por aí dando voltas em camas erradas

Eb
Olha a besteira que cê fez
            F
Nem virou amor, ja virou ex

[Refrão]

                  Bb
Meu coração pegou ar
                  F
Tomou Birra de você
                         Cm
Minha boca ja dando uns beijos aqui
                   Eb
E você dando seus pt's por aí

                  Bb
Meu coração pegou ar
                  F
Tomou Birra de você
                         Cm
Minha boca ja dando uns beijos aqui
                   Eb
E você dando seus pt's por aí
    F
Ja deu perca total de mim

( Bb  F  Cm  Eb )

[Primeira Parte]

         Eb                F
Pode sentar na primeira cadeira
             Gm
Do primeiro bar que você encontrar
        Eb                 F
Pode pedir uma dose caprichada
                Gm
de vergonha na cara

Cê vai precisar

[Pré-Refrão]

        Eb                  F
Não pensou em nada, quando deu mancada
   Gm
E saiu por aí dando voltas em camas erradas
Eb
Olha a besteira que cê fez
            F
Nem virou amor, ja virou ex

[Refrão]

                  Bb
Meu coração pegou ar
                  F
Tomou Birra de você
                         Cm
Minha boca ja dando uns beijos aqui
                   Eb
E você dando seus pt's por aí

                  Bb
Meu coração pegou ar
                  F
Tomou Birra de você
                         Cm
Minha boca ja dando uns beijos aqui
                   Eb
E você dando seus pt's por aí
    F                     Bb
Ja deu perca total de mim

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
