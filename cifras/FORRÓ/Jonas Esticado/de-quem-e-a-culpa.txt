Jonas Esticado - De Quem É a Culpa

Dm               F
    Exagerado   sim
                  Gm
Sou mais você que eu
            A#
Sobrevivo de olhares
                         Dm
E alguns abraços que me deu
                    F
E o que vai ser de   mim
                  Gm
E o meu assunto não muda
                A#                Dm
Minha cabeça não ajuda, loucura, tortura
                        C
E que se dane a minha postura
                       Gm
Se eu mudei você não viu
                            A#
Eu só queria ter você por perto


            Dm
Mas você sumiu
                          C
É tipo vício que não tem mais cura
                     Gm
E agora de quem é a culpa
           A#
A culpa é sua por ter esse sorriso
Gm                                      A#
   Ou a culpa é minha por me apaixonar por ele
    F
Só isso
                                       C
Não finja que eu não to falando com você
                         Gm
Eu tô parado no meio da rua
                            A#
Eu tô entrando no meio dos carros
                          F
Sem você a vida não continua
                                       C
Não finja que eu não to falando com você
                                Gm
Ninguém entende o que eu tô passando
                            A#
Quem é você que eu não conheço mais
                               F
Me apaixonei pelo que eu inventei de você

F,C,Gm,A#

----------------- Acordes -----------------
A# = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
