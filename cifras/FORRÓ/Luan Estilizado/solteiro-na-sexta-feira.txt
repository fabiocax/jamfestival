Luan Estilizado - Solteiro Na Sexta-feira

Solo intro:

E|-----------------------------
B|---19-19-19-15-15------------
G|------------------18-16------
D|-----------------------------
A|-----------------------------
E|-----------------------------

E|-----------------------------
B|---20-20-20-15-15------------
G|------------------16-14------
D|-----------------------------
A|-----------------------------
E|-----------------------------

E|-----------------------------
B|---19-19-19-15-15------------
G|------------------16-14------
D|-----------------------------
A|-----------------------------
E|-----------------------------


E|-----------------------------
B|-----------------------------
G|---18-18-19-18-14-16---------
D|-----------------------------
A|-----------------------------
E|-----------------------------

 Cm
Fez besteira, fez besteira
B
Me deixou solteiro
                   Cm
Eim plena sexta feira   (2x)

Cm        B
Em casa eu, não fico de bobeira
                             Cm
Vou me arrumar cair na bagaçeira
Cm
Já combinei com as gatinhas
               B
Elas tão preparadas

                             Cm
Wisky e red bul e cerveja gelada
                         B
Beijar na boca / fazer amor

Aproveitar a noite
                     Cm
Que solteiro hoje eu tô
Fm           A
Já tá ligando no inibido
E
Eu sei que é a mulher
                   B
O esquema é conhecido

Cm
Vai morrer de ligar
                B
Vai morrer de ligar
                                      Cm
Só na segunda, pra você que eu vou voltar

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
Cm = X 3 5 5 4 3
E = 0 2 2 1 0 0
Fm = 1 3 3 1 1 1
