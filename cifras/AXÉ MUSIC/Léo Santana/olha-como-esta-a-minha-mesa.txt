Léo Santana - Olha Como Está a Minha Mesa

[Intro] Bm  A  Bm  A

[Primeira Parte]

   Bm
Onde eu tava com a cabeça

Querendo me amarrar
A
Ia fazendo besteira

Mas consegui me salvar
  Bm
Quase que eu vendi meu paredão
                    A
Pra fazer chá de panela, por causa dela

[Pré-Refrão]

  Bm
Quis botar uma coleira em mim

                    A
Quis proibir minha gela, com a galera
G                      F#7
Adivinha quem falou já era!

[Refrão]

Bm                 A
   Agora eu tô solteiro
                      Bm
E olha como tá minha mesa
                    A
Quatro novinhas descendo no chão
                 Bm
Com whisky na cabeça
                    A
Quatro novinhas descendo no chão
                 Bm
Com whisky na cabeça

                A
Agora eu tô solteiro
                      Bm
E olha como tá minha mesa
                    A
Quatro novinhas descendo no chão
                 Bm
Com whisky na cabeça
                    A
Quatro novinhas descendo no chão
                 Bm  A  Bm  A
Com whisky na cabeça

[Primeira Parte]

   Bm
Onde eu tava com a cabeça

Querendo me amarrar
A
Ia fazendo besteira

Mas consegui me salvar
  Bm
Quase que eu vendi meu paredão
                    A
Pra fazer chá de panela, por causa dela

[Pré-Refrão]

  Bm
Quis botar uma coleira em mim
                    A
Quis proibir minha gela, com a galera
G                      F#7
Adivinha quem falou já era!

[Refrão]

Bm                 A
   Agora eu tô solteiro
                      Bm
E olha como tá minha mesa
                    A
Quatro novinhas descendo no chão
                 Bm
Com whisky na cabeça
                    A
Quatro novinhas descendo no chão
                 Bm
Com whisky na cabeça

                A
Agora eu tô solteiro
                      Bm
E olha como tá minha mesa
                    A
Quatro novinhas descendo no chão
                 Bm
Com whisky na cabeça
                    A
Quatro novinhas descendo no chão
                 Bm  A  Bm  A  Bm
Com whisky na cabeça

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
F#7 = 2 4 2 3 2 2
G = 3 2 0 0 0 3
