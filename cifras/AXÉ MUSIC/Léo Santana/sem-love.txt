Léo Santana - Sem Love

[Intro] G  Bm  Am  D

E|-3-7--14-10-14--14-8-12--|
B|-------------------------|
G|-------------------------|
D|-------------------------|
A|-------------------------|
E|-------------------------|

[Primeira Parte]

 G
Hoje o clima tá legal
Bm
 O vento tá batendo
 Am                   D
Lua na janela e você toda sensual

[Pré-Refrão]

G              Bm
 Netflix tá rolando só o primeiro episódio

      Am
Por que não consigo ficar perto de você

[Refrão]

     G
Sem love
     Bm
Sem love
     Am
Sem love
           D                   G
Eu não consigo ficar perto sem amor
     Bm
Sem love
     Am
Sem love
           D                    G
Eu não consigo ficar perto sem love

( Bm  Am  D )

E|-3-7--14-10-14--14-8-12--|
B|-------------------------|
G|-------------------------|
D|-------------------------|
A|-------------------------|
E|-------------------------|

[Segunda Parte]

         G
Eu fico louco te desejo
   Bm
Arrumo um pretexto pra tá
 Am            D
Perto de você só eu e você
       G
Agarradinho
        Bm
Bem juntinho
     Am
Coladinho
D
Até de manhã

[Pré-Refrão]

G              Bm
 Netflix tá rolando só o primeiro episódio
Por que não consigo ficar perto de você

[Refrão]

     G
Sem love
     Bm
Sem love
     Am
Sem love
           D                   G
Eu não consigo ficar perto sem amor
     Bm
Sem love
     Am  D
Sem love

 G
Love
 Bm
Love
     Am
Sem love
           D                   G
Eu não consigo ficar perto sem amor
     Bm
Sem love
     Am  D
Sem love

[Primeira Parte]

 G
Hoje o clima tá legal
Bm
 O vento tá batendo
 Am                   D
Lua na janela e você toda sensual

[Pré-Refrão]

G              Bm
 Netflix tá rolando só o primeiro episódio
Por que não consigo ficar perto de você

[Refrão]

     G
Sem love
     Bm
Sem love
     Am
Sem love
           D                   G
Eu não consigo ficar perto sem amor
     Bm
Sem love
     Am
Sem love
           D                    G
Eu não consigo ficar perto sem love
 Bm
Love
 Am
Love
           D                   G
Eu não consigo ficar perto sem amor
 Bm
Love
 Am
Love
           D                    G
Eu não consigo ficar perto sem love

[Final] Bm  Am  D
        G  Bm  Am  D  G

E|-3-7--14-10-14--14-8-12--|
B|-------------------------|
G|-------------------------|
D|-------------------------|
A|-------------------------|
E|-------------------------|

E|-3-7--14-10-14--14-8-12--|
B|-------------------------|
G|-------------------------|
D|-------------------------|
A|-------------------------|
E|-------------------------|

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
G = 3 2 0 0 0 3
