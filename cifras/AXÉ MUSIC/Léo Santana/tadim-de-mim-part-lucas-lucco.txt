Léo Santana - Tadim de Mim (part. Lucas Lucco)

[Intro] Fm  C  Fm  C

[Primeira Parte]

     Fm
Ô bebê
               C                       Fm
Se você me deixar, o que que eu vou fazer?
                  C                Fm
Confesso, tô com muito medo de sofrer

[Segunda Parte]

                       C             C5  Eb5  E5  F5
Imagina eu largado no meio da balada
                     C               C5  Eb5  E5  F5
Com aquelas novinha mal intencionada
                       C            C5  Eb5  E5  F5
Lembrando de você recusando cantada

[Pré-Refrão]


Bb                     C
Eu não pedi pra me deixar sozinho
     Bb                    C
Mas fazer o que, se foi você quem quis assim?

[Refrão]

Fm
Ah, tadim de mim
 C              Fm
Vou ter que sofrer
                       C
Na balada, enchendo a cara

Vendo as novinha descer

Fm
Ah, tadim de mim
 C              Fm
Vou ter que sofrer
                       C
Na balada, enchendo a cara

Vendo as novinha descer

F5    Ab5
  Descer
Ab5   G5
  Descer
F5      C5 Eb5  E5  F5
  Descer e eu  de   olho

F5    Ab5
  Descer
Ab5   G5
  Descer
F5      C5 Eb5  E5  F5
  Descer e eu  de   olho

( Fm  C  Fm  C )

[Primeira Parte]

     Fm
Ô bebê
               C                       Fm
Se você me deixar, o que que eu vou fazer?
                  C                Fm
Confesso, tô com muito medo de sofrer

[Segunda Parte]

                       C             C5  Eb5  E5  F5
Imagina eu largado no meio da balada
                     C               C5  Eb5  E5  F5
Com aquelas novinha mal intencionada
                       C            C5  Eb5  E5  F5
Lembrando de você recusando cantada

[Pré-Refrão]

Bb                     C
Eu não pedi pra me deixar sozinho
     Bb                    C
Mas fazer o que, se foi você quem quis assim?

[Refrão]

Fm
Ah, tadim de mim
 C              Fm
Vou ter que sofrer
                       C
Na balada, enchendo a cara

Vendo as novinha descer

Fm
Ah, tadim de mim
 C              Fm
Vou ter que sofrer
                       C
Na balada, enchendo a cara

Vendo as novinha descer

F5    Ab5
  Descer
Ab5   G5
  Descer
F5      C5 Eb5  E5  F5
  Descer e eu  de   olho

F5    Ab5
  Descer
Ab5   G5
  Descer
F5      C5 Eb5  E5  F5
  Descer e eu  de   olho

Fm
Ah, tadim de mim
 C              Fm
Vou ter que sofrer
                       C
Na balada, enchendo a cara

Vendo as novinha descer

Fm
Ah, tadim de mim
 C              Fm
Vou ter que sofrer
                       C
Na balada, enchendo a cara

Vendo as novinha descer

F5    Ab5
  Descer
Ab5   G5
  Descer
F5      C5 Eb5  E5  F5
  Descer e eu  de   olho

F5    Ab5
  Descer
Ab5   G5
  Descer
F5      C5 Eb5  E5  F5
  Descer e eu  de   olho

----------------- Acordes -----------------
Ab5 = 4 6 6 X X X
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C5 = X 3 5 5 X X
E5 = 0 2 2 X X X
Eb5 = X 6 8 8 X X
F = 1 3 3 2 1 1
F5 = 1 3 3 X X X
Fm = 1 3 3 1 1 1
G5 = 3 5 5 X X X
