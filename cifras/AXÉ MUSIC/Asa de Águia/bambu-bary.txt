Asa de Águia - Bambu Bary

INTRO: Em A Em A Em A D

D      Em
EVEBARY NA RUA
A                                 D
VEJA QUE COISA LOUCA

D      Em
EVEBARY NA RUA
A                              D
COCOBAMBU É BOCA
                Em
BAMBU BARY
        A                        Em
VOCÊ TAMBÉM É COCO
 A                                 Em
TÁ TODO MUNDO LOUCO
     A                                    D
CHEIO DE AMOR PRA DAR
                                             G
CHEGUE PRA CÁ MEU BEM

                                                 A
VEM CÁ QUE EU QUERO VER
                               F#m
VOCÊ ME ABRAÇAR
                        Bm
NESSA AVENIDA
                                     Em
VOLTEI PRA TE BEIJAR
                                    A
SÓ QUERO TEU AMOR
                        C
EVEBARY É ASA
                       D
NA NOSSA VIDA

 G
VOLTEI
                                  D  Bm
VOLTEI PARA FICAR
                           E
PORQUE AQUI, MEU BEM
                                 A
AQUI É MEU LUGAR

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
