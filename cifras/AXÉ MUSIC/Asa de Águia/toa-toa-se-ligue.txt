Asa de Águia - Tôa Tôa - Se Ligue!

 G      C
Tô a tôa na vida
            D     C
Tô a tôa de cara
            G
Tô a tôa na vida
    C       D
Zazuê tá danada
        G
Ouvi dizer meu bem
              C
Que o nosso amor nasceu
         G
No carrossel de flor
      C
Adocicante mel
           G
Mas sou feliz
                      C
Se a liberdade ainda voa
                    D
Pelas nuvens lá do céu

        G
Ouvi dizer também
              C
Que o mar do sonho azul
        G
Devora teu calor
          C
Entre sorrisos mil
          G
Corre no vento da lembrança
       C
Do destino dessa vida
            D
Que hoje eu sou
                  C
E hoje eu vivo à toa!
     G
Tô a tôa - se ligue
     C
Tô a tôa - se ligue
     G              D
Tô a tôa - se ligue

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
