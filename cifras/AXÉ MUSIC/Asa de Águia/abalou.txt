Asa de Águia - Abalou

E                      A
eu olho pra voçê não deixo de lembrar
   B
daquela noite linda de luar
E                          A
voçê disse pra mim que eu era seu amor
  B                        E
agora não consigo te esquecer

E                        A
eu guardo essa paixão aqui no coração
    B                  E
de novo o fogo vai arder
                      A
me dê a sua mão não tente sufocar
 B
vamos ficar juntos pra valer

A                     E
A noite foi embora o sol apareceu
      B                    E
e a gente sem pressa de chegar

    A                 E
deitados na areia olhando o azul do mar
     B                     E
benzinho eu só quero te amar

abalou chega chega meu bem

A     E
abalou
B             E
chega chega vem vem
A            E
abalou meu bem
A         B
chega chega

E
chega de ti ti ti           /
A                           /
chega de pó pó pó           /2X
B                     E     /
chega de blá blá blá eh     /

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
E = 0 2 2 1 0 0
