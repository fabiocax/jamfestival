Asa de Águia - Alimente

  C                                 D
Todo dia quero viver viajando na fantasia
F                                     C
Na mania de merecer o valor de te conquistar
                                         D
Todo dia penso em você balançando nessa folia
F                                           C
Na verdade quero te ver do meu lado pra te beijar

Am                                     D
Todo dia quero viver navegando nessa viagem
G                                                 C
Tudo é bom de você me quer vou levar pra cama seu mel
Am                                          D
Nessa noite te convidei jantando a luz de velas
F                                           G
Revirando tudo na casa e amanhã vem tudo de novo
            C          F        G
Eu vou lhe dizer alimente o nosso amor
            C          F        G
Eu vou lhe dizer alimente nossa flor

           Am                        G
Eu vou lhe dizer alimente toda essa loucura
                        REPETE A MÚSICA
                   Am      C   D
Alimente alimente hey hey
Alimente alimente hey hey
Alimente alimente hey hey
F                     G
Alimente toda essa loucura
INTRODUÇÃO 2x - FINALIZAÇÃO

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
