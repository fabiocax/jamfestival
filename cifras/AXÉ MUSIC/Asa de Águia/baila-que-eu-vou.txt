Asa de Águia - Baila que eu vou

Intro: D G D Em A

D
É amor todo dia
                   G C
É verão na alegria aê
D
É calor todo dia
                 G
É verão na bahia aê
C
Quero seguir
Bm
Sempre a cantar
           D
Folhas no vento a bailar
C
Pra conseguir
Bm               A
Basta sentir o luar


Baila que eu vou
                        D
Baila que eu vou encontrar você
 A
Baila que eu vou
                      D
Baila que eu vou mergulhar no mar
          F#m
Ainda me lembro
              D
Que um dia joguei
        F#m                  G
Essa mensagem sempre vai navegar
          F#m                  E
No vai e vem das ondas eu desejei
          G   D
Você pra mim.

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
