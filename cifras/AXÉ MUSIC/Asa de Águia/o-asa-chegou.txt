Asa de Águia - O Asa Chegou

[Intro] Bm  A

        Bm
Tome cuidado que eu te pego menina
               A
Você está na galera
      Bm
Não adianta sai de baixo meu amigo
                 A
Que essa mina é minha

                  Bm  A
Mas eu só quero você
                    Bm  A
Quero sentir teu calor

         Bm
Segure a mão na minha mão
                              A
Vou te levar comigo até o infinito
          Bm
Não tenha medo


A dor que sente
                              A
É bem menor do que ele faz contigo

                  Bm  A
Mas eu só quero você
                   Bm  A
Quero sentir teu calor

Bm                         A
Vai subir o lance da escadaria
G                                   Bm
Pode até acreditar sei que vai conseguir
                               A
Abre as asas e voa além do horizonte
   G                                F#
Se ligue no novo milênio que vai chegar

Bm       A                 Bm
Iê iê iê ô Asa de Águia chegou
        A                  Bm
Iê iê iê ô Asa de Águia chegou

( Bm  A  Bm  A )

Bm                         A
Vai subir o lance da escadaria
G                                   Bm
Pode até acreditar sei que vai conseguir
                               A
Abre as asas e voa além do horizonte
   G                                 F#
Se ligue no novo milênio que vai chegar

Bm       A                 Bm
Iê iê iê ô Asa de Águia chegou
        A                  Bm
Iê iê iê ô Asa de Águia chegou

( Em  F#  G  F#  Em  F#  G  F# )
( Em  F#  G  F#  Em  F#  G  F# )
( Em  F#  G  F#  Am  Bm  A )

                Bm  A
Asa de Águia chegou
                Bm  A
Asa de Águia chegou
                Bm  A
Asa de Águia chegou

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
