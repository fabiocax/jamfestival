Asa de Águia - Jamalaika

A        E
JAH JAH JAH
 D        A
AMA LÁ E CÁ
         D
BAHIA JAMAICA
E       A
JAMALAIKA
                 E
BALANÇO DÁ NO BALANÇO DÁ
   F#m              D
CONTAS NO PASSO MARCADO NO PÉ
     A               E
BRAÇOS QUE VÃO E QUE VOLTAM
    D              A
UM HOMEM E UMA MULHER
                        E
NO CORPO AS TRANÇAS AS DANÇAS
F#m             D
REGGAE, LUNDU, CONTAS CORAIS
 A              E
GOSTO, DO MAR, GOSTO DE DANÇAR

        D              A
QUANDO CANTO POSSO VIAJAR
   F#m                C#m
VIAJAR NAS MONTANHAS AZUIS
D                A
E VER SEU ROSTO LINDO
E        F#m
NAS NUVENS QUAL ESTRELA
F     G   A
POSSO VIAJAR

----------------- Acordes -----------------
A = X 0 2 2 2 0
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
