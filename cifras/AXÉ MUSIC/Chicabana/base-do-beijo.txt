Chicabana - Base do Beijo

(intro 4x) Bb D#9

(Bb D#9)
Quando eu te pegar você vai ver, você vai ver
Ai de ti, ai de ti.
                                  Bb
Vai se amarrar só querer saber de mim
                Cm  D#        Bb
Você vai se dar bem   e eu também
                Cm  D#        Bb
Você vai se dar bem   e eu também

        Bb                 C       D#                Bb
Comigo é na base do beijo, comigo é na base do amor
   Bb                    C                     D#                      Bb
Comigo não tem disse me disse, não tem chove não molha, desse jeito que sou      (2x)

Bb                  C                       D#
Quando amo é pra valer, quando amo é pra valer
                                          Bb
Dou carinho me entrego, faço o amor acontecer

                                             C
Quando amo é pra valer, quando amo é pra valer
                   D#                       Bb
Dou carinho me entrego, faço o amor acontecer

Bb          C
Vamos namorar
           D#
Beijar na boca
            Bb
Vamos namorar
            C  D#
Beijar na boca

(repete tudo)

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D# = X 6 5 3 4 3
D#9 = X 6 8 8 6 6
