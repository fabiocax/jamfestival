Chicabana - Pôr-do-sol?

[Intro] A  E  F#m  D
        A  E  F#m  D

A                        E                   F#m
    Você não sabe quanto tempo eu te esperei
                  D               A
Meu pequeno talismã... ãã... ãã
                E                           F#m
O presente mais precioso que ganhei de Deus
                     D
Minha estrela da manhã... ãã

   Bm
Não vá
             D
Não não, não vá
            F#m                        Bm
Achar que alguém te esqueceu, pois não há
              D
Não não, não há
E
Alguém que te queira mais que eu


[Refrão]

A                  E          F#m
E quando o sol se pôr, meu amor
           D             A
Lembra que eu sempre estarei com você
       E         F#m
Um sinal de nós dois
            D             A
No nosso amor, o pôr do sol

( A  E  F#m  D )

A                        E                   F#m
    Você não sabe quanto tempo eu te esperei
                  D               A
Meu pequeno talismã... ãã... ãã
                E                           F#m
O presente mais precioso que ganhei de Deus
                     D
Minha estrela da manhã... ãã

  Bm
Não vá
             D
Não não, não vá
            F#m                        Bm
Achar que alguém te esqueceu, pois não há
              D
Não não, não há
E
Alguém que te queira mais que eu

A                  E          F#m
E quando o sol se pôr, meu amor
           D             A
Lembra que eu sempre estarei com você
       E         F#m
Um sinal de nós dois
            D             A
No nosso amor, o pôr do sol

A                  E          F#m
E quando o sol se pôr, meu amor
           D             A
Lembra que eu sempre estarei com você
       E         F#m
Um sinal de nós dois
            D               C#m
No nosso amor, o pôr do sol

               F#m                 C#m
Eu não vejo a hora de poder te ver
              F#m                          Bm
Pra poder ficar bem de pertinho e te dizer
E
O quanto eu te amo

A                  E          F#m
E quando o sol se pôr, meu amor
           D             A
Lembra que eu sempre estarei com você
       E         F#m
Um sinal de nós dois
            D             A
No nosso amor, o pôr do sol

( A  E  F#m  D )
( A  E  F#m  D )

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
