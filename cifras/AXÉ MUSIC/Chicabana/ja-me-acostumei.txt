Chicabana - Já Me Acostumei

Intro: C F G

C                                           Am
Te falo a ultima vez não sei se vou me arrepender
                                                        F
Eu sei que é bem melhor assim a nossa história chega ao fim
                                                C
Não lembro mais se era bom eu só sinto a dor, a dor, a dor

C                                             Am
O que me fez e o que falou como fingiu e me enganou
                                             F
As duvidas que provocou como mentiu falou de mim
                                         C
A nossa história chega ao fim só ficou a dor

                 Am
Ah uh uh uh uh uh
                 G  F
Ah uh uh uh uh uh
               C  G
Ah uh uh uh uh uh


Refrão:
F                   C               Em                  Am
Mas, é passado já ficou pra traz não quero mais ouvir sua voz
F                  Am                F                     G
O seu sorriso se desfez de nós já me acostumei pela ultima vez
F                   C               Em                  Am
Mas, é passado já ficou pra traz não quero mais ouvir sua voz
F                  Am                F                     G       C
O seu sorriso se desfez de nós já me acostumei pela ultima vez, Adeus

                 Am
Ah uh uh uh uh uh
                 G  F
Ah uh uh uh uh uh
               C  G  F
Ah uh uh uh uh uh

C                                           Am
Passou a sensação não sei ser enganado é tão ruim
                                                     F
Só sei que vai se arrepender e vai querer voltar pra mim
                                            C
Mais saiba que não sou mais seu te desejo a dor

                 Am
Ah uh uh uh uh uh
                 G  F
Ah uh uh uh uh uh
               C  G  F
Ah uh uh uh uh uh

Refrão:
F                   C               Em                  Am
Mas, é passado já ficou pra traz não quero mais ouvir sua voz
F                  Am                F            G       C
O seu sorriso se desfez de nós já me acostumei pela ultima vez
F                   C               Em                  Am
Mas, é passado já ficou pra traz não quero mais ouvir sua voz
F                  Am                F            G       C
O seu sorriso se desfez de nós já me acostumei pela ultima vez, Adeus

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
