Chicabana - Eu Vou Voar

E|-7-8-9-10----12-12----10--|--10-11-10-12-----15-15--12----|7h8888-7-8-10|---88-7---------|
B|----------10------------10|---------------------------12--|-------------|---------10-8~~-|
G|--------------------------|-------------------------------|-------------|----------------|
D|--------------------------|-------------------------------|-------------|----------------|
A|--------------------------|-------------------------------|-------------|----------------|
E|--------------------------|-------------------------------|-------------|----------------|

G                          D
Eu preciso encontrar um amor
C                           G
Que me deixe arrasado na cama
                            D
Um amor que me desperte desejo
                             C
Um amor que me cale com um beijo
                         G
Um amor que me mova montanhas

G                          D
Um amor que não tenha capricho
                          C
Um amor que me jogue no lixo

                           G
Um amor que esmague meu peito
                     D
Pode ser um amor infiel
                      C
Ou então me leve pro céu
                          G
Um amor que me pegue de jeito

(refrão 2x)
G        D
Eu vou voar, atrás desse amor
     D D# Em
Vou encontrar, seja onde for
                C                  D                    G
Eu quero esse amor, eu quero esse amor, eu quero esse amor

       C
Um amor que bagunce minha vida
       D
Que sufoque e não deixe saída
       G                       Em
Ou então que acalme o meu coração
      C                              D
Não importa eu quero encontrar esse amor
                    G
É melhor que a solidão.

(repete tudo)

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
D# = X 6 5 3 4 3
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
