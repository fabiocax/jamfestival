Chicabana - Tentativas Em Vão

Intro: F#m   D

D                                                  F#m
Se eu soubesse o que fazer pra tirar você da minha cabeça
 D                                             F#m
Um lado diz que quer ficar com você o outro diz esqueça
D                                                  F#m
Se eu soubesse o que fazer pra tirar você da minha cabeça
 D                                             F#m
Um lado diz que quer ficar com você o outro diz esqueça
Bm                                   F#m
Mas saiba que o meu coração não é de papel
   G                                A
Que a chuva molha e as palavras se apagam
 Bm                       F#m
A minha mente gira feito um carrossel
    G
Tentando encontrar a saída
   A                      A
Pra tirar você da minha vida
 A             D                         Bm
Tentativas em vão tentar tirar você do coração

                            G
É como viver sem respirar
                                A
É como querer apagar a chama de um vulcão

 A             D                         Bm
Tentativas em vão tentar tirar você do coração
                            G
É como viver sem respirar
                                A
É como querer apagar a chama de um vulcão

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
