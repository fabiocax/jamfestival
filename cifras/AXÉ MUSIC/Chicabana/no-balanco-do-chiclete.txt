Chicabana - No Balanço do Chiclete

(intro)

-------------------------------------------------
--8-5-5--88-7-5------------------7--7-7-7-7-7-8-5
----------------7--7-7-7-7-7-7-7----------------

   C                 G

Tu ta ta tu tu ta ra Ra

      D                                   Em
No balanço do chiclete, chicleteiro vai dançar  (3x)

 C                  G                  D                 Em
To chegando vem pra ca vem curtir meu som vem comigo balançar
C                               G                    D                Em
Eu to chegando olha o chiclete ai, Wilson aumenta o som pra galera sacudir

   C                G
Tu ta ta tu tu ta ra Ra
     D                                    Em
No balanço do chiclete, chicleteiro vai dançar  (3x)


( C  G  D  Em )

( C  G  D D#m Em )          C                  G                     D
Quando eu chego agitando a galera vem fazer a festa e trás a multidão

D D#m Em                    C                 G             D        D#m       Em
Quando eu chego pra cantar todo mundo faz muito barulho e o meu coração que diz assim

C                    G
Tu ta ta tu tu ta ra Ra
       D                                 Em
No balanço do chiclete, chicleteiro vai dançar

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
D#m = X X 1 3 4 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
