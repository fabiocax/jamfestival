Ivete Sangalo - Mega Beijo

Livre como um passarinho
              A
Pelo céu vou voar
  G           E
Querendo Ter você
                  A
Com muito amor pra dar
 D
Vou encontrar no meu caminho
        G       A
Posso sentir no ar
   D           A
Vontade de viver
    F#
Pra me apaixonar
 D
Sol bem quente de verão
   C
Espelho azul do mar
    E          D              A
Refletindo um dia tão lindo

    A                   E
Pra quem ganhar meu coração
     G          D
Vai logo, logo perceber
    A            C#        D      D4
Que vai viver e amar no paraíso  ôôô...
 A           C        F7
vai viver e amar no paraíso
                F
Se nessa que eu tô
          G
Você me pega amor
    C                 A
Me leva pro céu que eu vou
    C
No mega beijo
    G
No beijo mega
  D                 A
Adoça minha vida amor
                F
Se nessa que eu tô
         G       A
Você me pega, amor
    C                          A
Me leva pro céu que eu vou
        C
No mega beijo
        G
No beijo mega
  D                 A
Adoça minha vida amor

----------------- Acordes -----------------
A = X 0 2 2 2 0
C = X 3 2 0 1 0
C# = X 4 6 6 6 4
D = X X 0 2 3 2
D4 = X X 0 2 3 3
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
F7 = 1 3 1 2 1 1
G = 3 2 0 0 0 3
