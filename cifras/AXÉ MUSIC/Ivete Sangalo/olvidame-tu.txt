Ivete Sangalo - Olvidame Tu

Em                    D   C                  G
Todas nuestras tardes son bajo estrellas escondidas
Em               D         Am7
luces que mi corazón se pensaría...
Em            D
Desnudarme como soy
C                   G
siendo así como la arena
Em                   D              Am7
que resbale en tu querer por donde quiera.

G            D
Darte para retenerte,
C                D
recelar si no me miras
C                      D                    E
con tus ojos, tu boca, tu sabia que es mía, mía.

C#7                  F#m
Responde a mi nombre si te lo susurran
E7                   A
arranca del todo mi piel que es tan tuya

     C#7             F#m        B7      E
Que arda mi cuerpo si no estás conmigo amor.

         A      B7  A#m
Olvídame tú que yo no puedo
           A         B7       E
no voy a entender el amor sin tí.
         A      B7     E    A#m         D#m
Olvídame tú que yo no puedo dejar de quererte
                  F#m       B7
por mucho que lo intente no puedo
         E
ólvidame tú.

Em                   D    C               G
Que bonito cuando el sol derrama sobre nosotros
Em                 D            Am7
esta luz que se apagó y que se perdía.
Em                   D
Si tu quieres quiero yo
C                  G
palpitar de otra manera
Em                  D               Am7
que nos lleve sin timón lo que nos queda.

G                   D   C               D
Sentiremos tal vez frío si no existe poesía
C                     D                     E
en tus ojos, tu boca, tu sabia que es mía, mía.

C#7                  F#m
Y el tiempo nos pasa casi inadvertido
E7                    A
golpéa con fuerza lo tuyo y lo mío
C#7                   F#m     B7       E
Que pena ignorarlo y dejarlo perdido amor.

         A      B7     A#m
Olvídame tú que yo no puedo
            A        F7       E
no voy a entender el amor sin tí
         A      B7      E
Olvídame tú que yo no puedo
A#m         D#m
dejar de quererte
                  F#m        B7
Por mucho que lo intente no puedo
         E
olvídame tú.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A#m = X 1 3 3 2 1
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
C#7 = X 4 3 4 2 X
D = X X 0 2 3 2
D#m = X X 1 3 4 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
F7 = 1 3 1 2 1 1
G = 3 2 0 0 0 3
