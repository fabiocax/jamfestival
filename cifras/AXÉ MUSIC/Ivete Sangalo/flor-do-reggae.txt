Ivete Sangalo - Flor do Reggae

Intro 2x: C#  D#m  C#  B  C#  D#m  F#  G# C#

  C#
Um brilho de amor chegou na ilha inteira
                              Bb
E a Lua que traz o amor é Lua cheia
   D#m                    F#m                    C# G#
Um grito de dor que vem do peito de quem amou alguém
  C#
O reggae me traz saudades de quem me beijou
                                Bb
E agora tá tão distante em outra ilha
 D#m
O amor me chamou de flor
  F#m                               C#    D#m
E disse que eu era alguém pra vida inteira

  C#                              D#m
R    Como se eu fosse flor, você me cheira
  e G#                              A#m
f    Como se eu fosse flor, você me rega

 r C#                              D#m
ã    E nesse reggae eu vou a noite inteira
   o G#                           C#
     Porque morrer de amor é brincadeira

----------------- Acordes -----------------
A#m = X 1 3 3 2 1
B = X 2 4 4 4 2
Bb = X 1 3 3 3 1
C# = X 4 6 6 6 4
D#m = X X 1 3 4 2
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
G# = 4 3 1 1 1 4
