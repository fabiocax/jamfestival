Ivete Sangalo - Deusas do Amor

Intro.:

B                            C#m
Na na, na na, na na na na, na na
F#
Tô de pernas pro ar
B                            C#m   F#
Na na, na na, na na na na, na na


B
  Cheguei com tudo pra te ver
   C#m               F#
De pernas de fora e agora é alegria
B
  Extravasando com você
C#m                F#
Vem me abraçar, alisar que eu sou de Vênus


G#m
   Deusa do amor

D#m                          E
   Tô poderosa, linda e sensual
          C#m
Vem com calor que ao meu lado
 F#
Tudo é carnaval


B             E              C#m
   Eu tô, eu tô, eu tô maravilhosa
F#
   Tô poderosa
B             E
   Eu tô, eu tô
                C#m
Mulheres são de Vênus
            F#
Pra brilhar,  fazer a festa - [ 2x ]


B                             C#m
Na na, na na, na na na na, na na
F#
Tô de pernas pro ar
B                             C#m
Na na, na na, na na na na, na na
     F#
Extravasa faz a festa

----------------- Acordes -----------------
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
D#m = X X 1 3 4 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
