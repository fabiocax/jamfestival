Ivete Sangalo - Tum Tum Goiaba

Intro: C9 A# F G

      C9    A#
Fruta doce
F         C9          A# F
Fruto dos meus sonhos
           C9      A#
Cheira tão madura
F            C9      A# G
Flor de todo encanto

     C9    A#
Fruta doce
F         C9          A# F
Fruto dos meus sonhos
           C9      A#
Cheira tão madura
F            C9      A#
Flor de todo encanto
G                    Em
Bago de jaca, manga, pinha

        A       Dm
Vou te dar na boca
G                    Em
Bago de jaca, manga, pinha
        A           Dm
Vou te dar na boquinha
G                   Em     A
Tum, tum, tum, tum, goiaba
           Dm    G
Tum, tum, goiaba
           Em    A
Tum, tum, goiaba
           Dm
Tum, tum, goiaba

----------------- Acordes -----------------
A = X 0 2 2 2 0
A# = X 1 3 3 3 1
C9 = X 3 5 5 3 3
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
