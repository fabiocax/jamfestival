Ivete Sangalo - Vale Mais

A                                 E
Oh oh oh oh oh oh oh  oh oh  (2x)

             F#                             B
La la la laaaaaaaa     la la la laaaaaaa
         E
Queria abrir meu coração
                    A
Contar a verdade sobre mim
      E
Parar de me sentir tão só
              A
Sair um pouco e descobrir
                 C#m                                  D
Que vale mais correr atrás de um sonho
                     A
E ver o lado bom
                         B
Tentar sorrir e ser feliz
                        A
Eu vou mostrar

                           E
Você é o meu sonho bom
            A
Realizar
                                               E
As coisas que pensei pra nós
                  A
Te dar a mão
                                       E
Deixar o tempo nos levar
        F#
E viajar
            B
E viajar

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
