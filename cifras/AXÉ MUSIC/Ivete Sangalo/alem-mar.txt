Ivete Sangalo - Além Mar

Intro:C F Am G

      C  C7M            Dm  Dm/C#
Tudo o que eu preciso eu já tenho aqui
             Am A9              C
Abre teu sorriso, faz me divertir
            C  C7M            Dm  Dm/C#
Água cristalina, corpo de vulcão
          Am A9          C  C7
Toda natureza, lua de verão

2a parte:
   F                  Fm                    C7M  C/D
Você é o meu grande amor, minha vida, meu sim
     F                Fm                     C7M  C/D
Não posso te ver infeliz que eu começo a chorar
    F
Eu vou garimpar esse amor
    Am
Sentir o calor da paixão
   F               Dm           Bb G
E ainda só quero viver pra te amar

            Am                     G                     F
Te adoro, te venero, eu te amo, eu te quero coração
      Am G
Além mar
     Am                     G                     F
Te adoro, te venero, eu te amo, eu te quero coração
      Am G
Além mar

Solo de Gaita
C F Am G

Repete 2a parte
Repete Refrão (Ivete)
Repete Refrão (Durval e Ivete 2x)
Solo final de Gaita
C F Am G (2x)

----------------- Acordes -----------------
A9 = X 0 2 2 0 0
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C/D = X X 0 0 1 0
C7 = X 3 2 3 1 X
C7M = X 3 2 0 0 X
Dm = X X 0 2 3 1
Dm/C# = X 4 3 2 3 X
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
