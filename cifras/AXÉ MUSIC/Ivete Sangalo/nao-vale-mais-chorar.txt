Ivete Sangalo - Não Vale Mais Chorar

(intro) A  E  F#m  D

     A  E  F#m D
    Hôô HôôHôôôôHôôôô { 4x }

(refrão)
A  E  F#m  D
Não vale mais chorar por ele
Ele jamaaais te amou (Jamais te amou)
Não vale mais chorar por ele
Ele jamaaais te amou (Jamais te amou)

( A E F#m D )
Você sofreu, você chorou
Ele nem te ligou
Você sofreu, você chorou
Ele nem te ligou

( A   E   F#m   D )
Quantas vezes vi você na solidão
Chorando, sofrendo, era só desilusão

Quem ama protege, faz tudo pelo o amor
Seu coração está sofrendo por falta de amor
Ele não te quis não soube ser feliz
Quando percebeu já era tarde de mais
É como cristal quebrado que não cola jamais
Ele tinha seu amor não soube dar valor
Agora ele chora por falta do seu calor
Por isso estou aqui para conquistar o seu amor

(refrão 2x)
( A   E   F#m   D )
Hôô HôôHôôôôHôôôô

( A   E   F#m   D )
Você sofreu, você chorou
Ele nem te ligou
Você sofreu, você chorou
Ele nem te ligou

( A  E   F#m   D )
Quantas vezes vi você na solidão
Chorando, sofrendo, era só desilusão
Quem ama protege, faz tudo pelo o amor
Seu coração está sofrendo por falta de amor
Ele não te quis não soube ser feliz
Quando percebeu já era tarde de mais
É como cristal quebrado que não cola jamais
Ele tinha seu amor não soube dar valor
Agora ele chora por falta do seu calor
Por isso estou aqui para conquistar o seu amor

(refrão)
( A   E   F#m   D )
Hôô HôôHôôôôHôôôô

----------------- Acordes -----------------
A = X 0 2 2 2 0
A- = X 0 2 2 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
