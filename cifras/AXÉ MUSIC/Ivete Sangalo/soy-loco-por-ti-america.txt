Ivete Sangalo - Soy Loco Por Ti, America

Intro: F#m B E F  F#m B E F
       F#m Am E

                   A/B                    E
Soy loco por ti, america yo voy traer una mujer playera
                   A/B                       E
Que su nombre sea marti, que su nombre sea marti
                     A/B                E
Soy loco por ti de amores tengo como colores la espuma
                A/B                         E
Blanca de latinoamerica y el cielo como bandeira
       F#m  Am      E
Y el cielo como bandeira
                    A/B                       E
Soy loco por ti, america soy loco por ti de amores
                   A/B                      E
Sorriso de quase nuvem, os rios cancoes, o medo
                    A/B                       E
O corpo cheio de estrelas o corpo cheio de estrelas
                  A/B                  E
Como se chama a amante desse pais sem nome


Esse tango, esse rancho
             A/B                        E
Esse povo dizei-me, arde o fogo de conhece-la
   F#m  Am      E
O fogo de conhece-la
                   A/B                        E
Soy loco por ti america soy loco por ti de amores

Solo: C#m Bm Em Dm C#m Bm Dm E

                       A/B
El nombre del hombre muerto ya no se puede
   E
Decirlo, quien sabe
                     A/B                        E
Antes que o dia arrebente, antes que o dia arrebente
                       A/B
El nombre del hombre muerto
                   E                              A/B
Antes que a definitiva noite se espalhe em latinoamerica
                          E         F#m        Am        E
El nombre del hombre es pueblo,el nombre del hombre es pueblo
                  A/B                         E
Soy loco por ti america soy loco por ti de amores
                        A/B                        E
Espero que amanha que cante el nombre del hombre muerto
                    A/B
Nao seja palavras tristes
                      E
Soy loco por ti de amores
                 A/B                           E
Um poema ainda existe com palmeiras, com trincheiras
                             A/B
Cancoes de guerra quem sabe cancoes do mar, ai,
 G#m  F#m     E       F#m   Am    E
Hasta te comover, ai hasta te comover
                   A/B                        E
Soy loco por ti america soy loco por ti de amores
                 A/B                            E
Estou aqui de passagem, sei que adiante um dia vou morrer
                    A/B                        E
De susto, de bala vicio, de susto, de bala ou vicio
                   A/B                    C#m
Num precipicio de luzes entre saudade, solucos,
           C#7
Eu vou morrer de brucos
      F#m                    B              E
Nos bracos, nos olhos, nos bracos de uma mulher
      F#m      Am     E
Nos bracos de uma mulher
                 A/B   C                    C#m
Mais apaixonado ainda dentro dos bracos da camponesa
       C#7       F#m                    B
Guerrilheira manequim, ai de mim, nos bracos
             E         F#m   Am           E
De quem me queira nos bracos de quem me queira
                  A/B                        E
Soy loco por ti america soy loco por ti de amores

----------------- Acordes -----------------
A/B = X 2 2 2 2 X
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C#7 = X 4 3 4 2 X
C#m = X 4 6 6 5 4
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
