Ivete Sangalo - Quanto Ao Tempo

B                C#m
Lágrimas não são forever (forever)
Dores já não são together (together)
Quando a gente ama espera (espera)
            B
Um dia assim chegar
G#7
Chegou

C#m   F#9
É,   eu já sei como iluminar a nossa fonte
D#m   G#9
É,   eu já consigo ir além do horizonte
C#m   F#9
É,   e os detalhes tão pequenos de
B                     G#7
nós dois Ficaram pra depois, depois, depois, depois, depois
C#m   F#9
É,   sonhei de tudo como um dia de domingo
D#m    G#9
É,   o que vier para nós dois será bem vindo

C#m   F#9
É,   só não demore quanto ao tempo
        B
pra chegar
G#7
Chegou

C#m
Quanto ao tempo te esperei
F#9
E o passado assim passou
D#m
Hoje o céu mudou de tom
G#9
Pra falar do nosso amor
C#m
Acho que chorei igual
F#9
Como a chuva no quintal
D#m
Acho que sonhei do bom
G#9
Hoje o céu mudou de tom

C#m  F#9
É,   eu já sei como ir além do horizonte
D#m  G#9
É,   eu já consigo iluminar a nossa fonte
C#m  F#9
É,   e os detalhes tão pequenos de
B                     G#7
nós dois Ficaram pra depois depois depois depois depois
C#m  F#9
É,   sonhei de tudo como um dia de domingo
D#m  G#9
É,   o que vier para nós dois será bem vindo
C#m  F#9
É, só não demore quanto ao tempo
        B
pra chegar
G#7
Chegou

C#m    F#9
My eyes to see you
D#m
But I need
G#9
To believe it's true
C#m
Maybe my love likes crazy
F#9
Love is you, is you, is you,
D#m               G#9
And me and you
C#m
If I don't know how good it is
                                   B
Só não demore quanto ao tempo pra chegar

----------------- Acordes -----------------
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
D#m = X X 1 3 4 2
F#9 = 2 4 6 3 2 2
G#7 = 4 6 4 5 4 4
G#9 = 4 6 8 5 4 4
