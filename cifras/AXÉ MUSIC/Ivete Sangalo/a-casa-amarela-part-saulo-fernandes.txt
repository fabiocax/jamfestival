Ivete Sangalo - A Casa Amarela (part. Saulo Fernandes)

A
Papai pintou
                         E
A casa de amarelo
A
A frente toda
                         E
Parece um castelo
A
Lá no jardim
                        E
Anão e cogumelo
A
Mamãe achou legal
                   E
E até discreto
A                          E
Ah, foi um sonho
                          A
Hum, que sonhei
                          E
Ah, foi um sonho

Me tratavam
                      A
Como um rei
                            A
Lá do meu quarto
Dá pra ver
Os girassóis
E
Lá da varanda
Grandes portas
De cristais
B
Mamãe vestida
De rainha
E de condão
D
Papai valente
        E
Enfrentando
            A
O dragão

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
