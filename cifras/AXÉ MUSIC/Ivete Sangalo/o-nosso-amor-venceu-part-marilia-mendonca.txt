Ivete Sangalo - O Nosso Amor Venceu (part. Marília Mendonça)

[Intro] G  Em7  C  G/B
        Am7  G  D

E|---------7-7h8-7--------------7-7h8-7-----|
B|-7/8-7-8---------8-7--7/8-7-8---------8-7-|
G|------------------------------------------|
D|------------------------------------------|
A|------------------------------------------|
E|------------------------------------------|

[Primeira Parte]

  G
Agora longe de você
   D
Vivendo toda essa loucura
     C                 Am7
Mostrando essa independência
               D
A verdade é você quem eu quero


E pro meu coração não existe outra cura

[Pré-Refrão]

C               D
  Tantas coisas   ditas sem pensar

Em7              C
    Meu orgulho sua vaidade
G                               D
  Coisas que só descobri com o tempo
     C                Em7
E sozinha eu rezo baixinho
   C
Na esperança de fazer
          D
O nosso amor vencer

[Refrão]

     G
E o nosso amor venceu
    D
De tanto que eu pedi a Deus
       Em7           C
E ninguém vai conseguir
         D
Nos separar outra vez

     G
E o nosso amor venceu
    D
De tanto que eu pedi a Deus
       Em7           C
E ninguém vai conseguir
         D
Nos separar outra vez

[Segunda Parte]

    G
Eu não me sinto preparada
     D
Pra ver você ficar com outra pessoa
        C                 Am7
Ver desmoronar o que nós dois construímos
        D
Uma história de amor

Que entre quatro paredes não tinha censura

[Pré-Refrão]

C               D
  Tantas coisas   ditas sem pensar
Em7              C
    Meu orgulho sua vaidade
G                               D
  Coisas que só descobri com o tempo
     C                Em7
E sozinha eu rezo baixinho
   C
Na esperança de fazer
          D
O nosso amor vencer

[Refrão]

     G
E o nosso amor venceu
    D
De tanto que eu pedi a Deus
       Em7           C
E ninguém vai conseguir
         D
Nos separar outra vez

     G
E o nosso amor venceu
    D
De tanto que eu pedi a Deus
       Em7           C
E ninguém vai conseguir
         D
Nos separar outra vez

[Solo] G  D  Em7  C  D

E|------------------------------------------|
B|------------------------------------------|
G|------------------------------------------|
D|------------------------------------------|
A|------------------3/5-5-5-5-5-5-5/7-5-----|
E|-5-5/7-7-7-5-5/7--------------------------|

E|------------------------------------------|
B|------------------------3/5-3-1-1/3-------|
G|-----------2/4\2-0----0-------------2-----|
D|-4/5-4-2-4---------2----------------------|
A|------------------------------------------|
E|------------------------------------------|

E|------------------------------------------|
B|------------------------------------------|
G|---------------4-5-7-5~-------------------|
D|---------4-5-7----------------------------|
A|-5-5-5-7----------------------------------|
E|------------------------------------------|

[Pré-Refrão]

C               D
  Tantas coisas   ditas sem pensar
Em7              C
    Meu orgulho sua vaidade
G                               D
  Coisas que só descobri com o tempo
     C                Em7
E sozinha eu rezo baixinho
   C
Na esperança de fazer
          D
O nosso amor vencer

[Refrão]

     G
E o nosso amor venceu
    D
De tanto que eu pedi a Deus
       Em7           C
E ninguém vai conseguir
         D
Nos separar outra vez

     G
E o nosso amor venceu
    D
De tanto que eu pedi a Deus
       Em7           C
E ninguém vai conseguir
         D
Nos separar outra vez

[Final] G  Em7  C  G/B
        Am7  G  D  G

E|---------7-7h8-7--------------7-7h8-7-----|
B|-7/8-7-8---------8-7--7/8-7-8---------8-7-|
G|------------------------------------------|
D|------------------------------------------|
A|------------------------------------------|
E|------------------------------------------|

E|------------------------------------------|
B|------------------------------------------|
G|------------------------------------------|
D|-2/4-2-0----------------------------------|
A|---------3-2-0----------------------------|
E|---------------3~-------------------------|

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
