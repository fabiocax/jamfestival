Carlinhos Brown - Shalon

Intro: F G7/F Bbm6 C F Bb Eb F F9 Bb Eb F


F
O MUNDO INDIVISÍVEL
                                  G7/F
RALÉ, RELIGIÕES
                           Bb
E MANIFESTAÇÕES
Eb7(9)                            F
            VIDAS COMUNS!
F
NA LENTE DO INVISÍVEL
                                            G7/F
AONDE OS PINGOS VÃO
                                         Bb
É, POR DESTINO, AO CHÃO!
Eb7(9)                                    F
           QUEM VEM RETORNARÁ!

                    F                                                                                   Gm7
ME DÊ A MÃO DE SABÃO, DE SABÃO, DE SABÃO, DE SABÃO

              C7/4                         C7                            F
DE VIOLÃO, VIOLA, VIOLÃO, VIOLA, VIOLÃO
                F                                                                                                   Gm7
E DE SHALOM, DE SHALOM, DE SHALOM, DE SHALOM, DE SHALOM
                  C7/4                                C7                                       F
ME DÊ A MÃO DE FAVO DE FEIJÃO E DEIXE A MISSA LÁ

Eb                                            F
NA MISSA ALAH DE SHALOM
Eb                                             F
NA DE KRISHNA DE SHALOM
Eb                                       F
NA DE OXALÁ DE SHALOM
Eb                                     F
NA DE BUDA DE SHALOM
             F
VEM MARIA, VEM AQUI
                       C
ENCHER A VIDA DE PAZ
SOBRE A FAMÍLIA O PÃO
                        F
LIVRA OS PRESOS DA PRISÃO
CORRE ESTRADÃO CAMINHÃO
                         C
CÓRREGOS PRO LITORAL
ESCORRE ESTA LENDA CANÇÃO
             F
DÁ MARGEM PRO MARGINAL

                                       F
PODE COLHER DA CALMA
                                              G7/F
AO LESTE DAS MANHÃS
                                       Bb
O MUNDO É PURO E SÃO
Eb7(9)                                F
      IGUAL TEU RUMO LUZ

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bbm6 = 6 X 5 6 6 X
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
C7/4 = X 3 3 3 1 X
Eb = X 6 5 3 4 3
Eb7(9) = X 6 5 6 6 X
F = 1 3 3 2 1 1
F9 = 1 3 5 2 1 1
G7/F = 1 X X 0 0 3
Gm7 = 3 X 3 3 3 X
