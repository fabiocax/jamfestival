Carlinhos Brown - Seo Zé

Bm
O Brasil não é só
       F#
Verde, Anil e Amarelo
     F#7
O Brasil também é
       Bm
Cor de Rosa e Carvão

Patrimônio de Antônio
  F#
Anônimo nômade
F#7              Bm
Homem que rompe Adão com facão
Bm
Seo Zé
               F#
Tá pensando em boi
      G F#     Bm
Bananeira Sangrou
                      F#
Mais um pro Baião de dois

     G     F#     Bm
Lampião findou cabôco

Vamos chamar
       F#
Brás Cubas
    F#7       Bm
Pra dançar quadrilha
              F#
Pra subir pra Cuba
    F#7    Bm
Com toda família
                F#
Se encontrarmos Judas
  F#7      Bm
Celebrando Budas
           F#
Perfilamos mulas
     F#7     Bm
Pra abalar Belém
    Bm
Seo Zé
               F#
Tá tangendo em boi
       G   F#   Bm
E a porteira cerrou
 Bm                   F#
Quem foi nunca mais se foi
    G   F#      Bm
A roseira flororô
                      F#   G  F#  Bm
Seo Zé tá pensando em boi

Seo Zé tá pensando

----------------- Acordes -----------------
Bm = X 2 4 4 3 2
F# = 2 4 4 3 2 2
F#7 = 2 4 2 3 2 2
G = 3 2 0 0 0 3
