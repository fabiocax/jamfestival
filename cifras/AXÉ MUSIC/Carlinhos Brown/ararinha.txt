Carlinhos Brown - Ararinha

Intro: D7M(9)  G#º  Em7  Asus4  A7

D7M(9)
Ela pula, ela sabe a bula
           Em7       Gm7
Ela samba,  ela canta
    F#m7  Fº      Em7       Asus4  A7
Ela anda    na ponta dos pés
D7M(9)
Ela é minha cara, ela é jóia rara
      Em7    Gm7    F#m7  Fº     Em7      Asus4  A7
Ela é linda,     ararinha    é o meu amor

G7M   A/G           Em7          Gm7
Vou batendo asas na alma, quando vôo
  G7M    A/G             Gm/Bb     Asus4  A7
E vejo o cristo aos meus pés, redentor

D7M(9)
Ela é minha, ela sabe a gíria
    Em7   Gm7     F#m7   Fº       Em7       Asus4  A7
Ela gira,     ela ginga,    é como uma flor

D7M(9)
Ela brinca, ela sai na brisa
     Em7  Gm7    F#m7   Fº      Em7      Asus4  A7
Ela anima,    ararinha      é o meu amor

  G7M   A/G         Em7         Gm7
O sol a esquentar a pena, na canção
  G7M     A/G          Gm/Bb  Asus4 A7
O chão só quer achar o céu na vida

( D7M(9)  Em7  Gm7  F#m7  Fº  Em7  Asus4 A7 )

( D7M(9)  Em7  Gm7  F#m7 )

       Fº              Em7       Asus4  A7
Só com ela eu sou, ela é meu amor
    G7M    D6(9)
Fly love

----------------- Acordes -----------------
A/G = 3 X 2 2 2 X
A7 = X 0 2 0 2 0
Asus4 = X 0 2 2 3 0
D6(9) = X 5 4 2 0 0
D7M(9) = X 5 4 6 5 X
Em7 = 0 2 2 0 3 0
F#m7 = 2 X 2 2 2 X
Fº = X X 3 4 3 4
G#º = 4 X 3 4 3 X
G7M = 3 X 4 4 3 X
Gm/Bb = 6 5 5 3 X X
Gm7 = 3 X 3 3 3 X
