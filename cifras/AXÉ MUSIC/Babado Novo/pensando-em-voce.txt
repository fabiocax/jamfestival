Babado Novo - Pensando Em Você

A                 E              F#m
Estava satisfeita em te ter como amigo
            C#m                 D       A
Mas o que será que aconteceu comigo?
Bm                   E
Aonde foi que eu errei
A                     E                 F#m
Às vezes me pergunto se eu não entendi errado
                C#m              D       A
Grande amizade com estar apaixonada
Bm                         E
Se for só isso logo vai passar

D
Mas quando toca o telefone será você?
Dm
O que eu estiver fazendo eu paro de fazer
A
E se fica muito tempo sem me ligar
Aº
Arranjo uma desculpa pra te procurar

Bm        A          D         E
Que tola, mas eu não consigo evitar

(refrão)
              D       E      C#m
Porque eu só vivo pensando em você
F#m        Bm             E                 A   A7
É sem querer, você não sai da minha cabeça mais
        D         E      C#m
E eu só vivo acordada a sonhar
F#m       Bm                                        E
Imaginar, às vezes penso ser um sonho impossível

Ou uma ilusão terrível
   D              E        C#m
Será? Eu já pedi tanto em oração
F#m                     Bm
Que as portas do seu coração
     E             A          A7
Se abrissem pra eu te conquistar
         D               E    C#m
Mas que seja feita a vontade de Deus
F#m            Bm
Se Ele quiser então

Não importa quando onde ou como
Dm         E7       A
Eu vou ter seu coração

Solo: A  E  F#m  C#m  D  A  Bm  E

A                 E            F#m
Faço tudo pra chamar tua atenção
                         C#m          D
De vez em quando eu meto os pés pelas mãos
                 A
Engulo a seco o ciúme
                    Bm
Quando outra apaixonada quer
               E
Tirar de mim sua atenção
A              E        F#m
Coração apaixonado é bobo
                          C#m
Sorriso seu ele derrete todo
D               A
O seu charme o seu olhar
Bm                     E
Sua fala mansa me faz delirar

D
Mas quando a coisa aconteceu e foi dita
Dm
Qualquer mínimo detalhe era pista
A
Coisas que ficaram para trás
Aº
Coisas que você nem lembra mais
D
Mas eu guardo tudo aqui no meu peito
Dm
Tanto tempo estudando seu jeito
A
Tanto tempo esperando uma chance
Aº
Sonhei tanto com esse romance
Bm         A              D          E
Que tola, mas eu não consigo evitar

Refrão:
              D       E      C#m
Porque eu só vivo pensando em você
F#m        Bm             E                 A   A7
É sem querer, você não sai da minha cabeça mais
        D         E      C#m
E eu só vivo acordada a sonhar
F#m       Bm                                        E
Imaginar, às vezes penso ser um sonho impossível

Ou uma ilusão terrível
   D              E        C#m
Será? Eu já pedi tanto em oração
F#m                     Bm
Que as portas do seu coração
     E             A          A7
Se abrissem pra eu te conquistar
         D               E    C#m
Mas que seja feita a vontade de Deus
F#m            Bm
Se Ele quiser então

Não importa quando onde ou como
Dm         E7       A
Eu vou ter seu coração

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
