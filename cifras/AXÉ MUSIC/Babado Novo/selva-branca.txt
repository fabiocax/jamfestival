Babado Novo - Selva Branca

Am
   Pra te espiar
F           Dm           G
   eu dou a volta no seu muro
        E7
Eu pulo seu muro
Am
   Pra te encontrar
F           Dm           G
   Eu dou a volta no seu mundo
           E7                 Am(preparação)
   Eu mudo seu mundo

F
Faço o que quiser de brincadeira
Dm
   carrossel no céu
      Am
selva Branca
Dm
   e nascer em cada estrela a novidade

Am
   que o muro do seu mundo era saudade
F
   porque não dizer posso derreter
Dm                                        E7
   moranguinho no copinho esperando por você

 Am                            |
Quanto mais sorvete           |

 Em                            |
Quero seu calor               |

Quanto mais desejo de amor    |  Refrão
 Am                            |
Quanto mais desejo            |

 Em                            |
Quero seu calor               |

Quanto mais sorvete de amor   |

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
