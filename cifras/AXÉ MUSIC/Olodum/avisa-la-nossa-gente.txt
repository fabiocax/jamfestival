Olodum - Avisa lá / Nossa Gente

[Intro]  Dm Bb C

  Dm                           C
Avisa lá que eu vou chegar mais tarde, o yê
          Dm                          C
Vou me juntar ao Olodum que é da alegria
Bb                  Dm
 É denominado de vulcão
       Bb                      Dm
O estampido ecoou nos quatro cantos do mundo
   Bb                        Dm
Em menos de um minuto, em segundos
  Dm                                     C
Nossa gente é quem bem diz é quem mais dança
   Dm                       C
Os gringos se afinavam na folia
   Bb                        Dm
Os deuses igualando todo o encanto toda a transa
       Bb                       Dm
Os rataplans dos tambores gratificam
     Dm                     C
Quem fica não pensa em voltar

    Dm              C
Afeição a primeira vista
  Bb                   Dm
O beijo-batom que não vai mais soltar
  Bb                       Dm
A expressão do rosto identifica
      Dm
Avisa lá, avisa lá, avisa lá ô ô
 Bb    C        Dm
Avisa lá que eu vou
 Dm
Avisa lá, avisa lá, avisa lá ô ô
 Bb    C        Dm
Avisa lá que eu vou

( Dm Bb C )

  Dm                           C
Avisa lá que eu vou chegar mais tarde, o yê
          Dm                          C
Vou me juntar ao Olodum que é da alegria
Bbm                  Dm
 É denominado de vulcão
      Bbm                     Dm
O estampido ecoou nos quatro cantos do mundo
   Bbm                       Dm
Em menos de um minuto, em segundos
  Dm                                     C
Nossa gente é quem bem diz é quem mais dança
   Dm                       C
Os gringos se afinavam na folia
   Bbm                        Dm
Os deuses igualando todo o encanto toda a transa
       Bbm                      Dm
Os rataplans dos tambores gratificam
     Dm                     C
Quem fica não pensa em voltar
    Dm              C
Afeição a primeira vista
  Bbm                  Dm
O beijo-batom que não vai mais soltar
  Bbm                     Dm
A expressão do rosto identifica
      Dm
Avisa lá, avisa lá, avisa lá ô ô
 Bbm   C        Dm
Avisa lá que eu vou
 Dm
Avisa lá, avisa lá, avisa lá ô ô
 Bbm   C        Dm
Avisa lá que eu vou

----------------- Acordes -----------------
A#m = X 1 3 3 2 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
