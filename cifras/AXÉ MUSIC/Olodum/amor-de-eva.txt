Olodum - Amor de Eva

Dm                C
Olodum traga seu axé pra mim
                   Bb
Que eu quero reconstituir
              A
O que a humanidade acabou
   Dm                           C
A fauna e a flora, natureza que Deus criou
             Bb      A
O que eu queria ver era o desabrochar de uma flor
Dm                        C
Eu queria ter um mundo de paz e de amor
     Bb
O karma que o homem traz
        A
Animalidade do pavor (Eu)
        Dm
vou seguindo meu caminho
     C              Bb
Semeando o amor emanando a minha raça
        A
Como se fosse um beija-flor



(Refrão)

      Bb                 Am
Se entrega me leva pras nuvens que eu quero
                    Bb  Am
Encontrar o amor de Eva
     Bb                Am
Me encontra expõe seu sorriso que eu quero
                 Bb    Am
Guardá-lo na lembrança.

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
