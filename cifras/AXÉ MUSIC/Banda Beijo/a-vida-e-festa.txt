Banda Beijo - A Vida É Festa

Em             D
Amor, quero sentir o teu perfume
C              B7
Amor, quero curtir felicidade
Em             D
Amor, quero sentir se tem ciúme
C              B7
Amor, quero viver na amizade

Am               Em
A vida é festa, é alegria
C                                     B7
Deixa a tristeza de lado e vem me abraçar
Am              Em
O que nos resta é fantasia
C                                  B7
Dengo, sossego, chamego, delícia e luar

              Em
Quê, quê, quê, ô, estrela do amor
   D
Ê, ah, vem me iluminar

C                            B7
A felicidade da gente é uma semente
Plantada no olhar
               Em
Quê, quê, quê, ô, estrela do amor
   D
Ê, ah, é sol de verão
C                                   B7
É o som de mil tambores batendo gostoso
No seu coração

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
