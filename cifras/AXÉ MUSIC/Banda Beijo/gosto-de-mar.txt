Banda Beijo - Gosto de Mar

Intro: (F Bb F C)

 (  F            Bb           F   C ) - segue igual a introdução
Eu vou pegar de leve em tua mão
Quero roubar de vez seu coração
Eu vou te lambuzar com meu prazer
O que que eu faço prá te conhecer?

    Vem morena, me namora
R   Quero ficar ao seu lado
E   Bem juntinho, bem gostoso
f   Na delícia desse gozo
R   Teu suor tem gosto de mar
Ã   Vem comigo
O   Navegar...

Eu vou pegar de leve em tua mão
Quero roubar de vez seu coração
Eu vou te lambuzar com meu prazer
O que que eu faço prá te conhecer?


REfRÃO

Dm                   Am   Bb                     C  A7
   Nesse clima de arrepiar   faz meu corpo se arder
Dm                    Am          Bb
   Nesta chama que o amor nos traz
                                C       C7
Deixa a terra estremecer estremecer...

Solo: igual a Intro, Refrão, do início, Refrão Fade out



----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
