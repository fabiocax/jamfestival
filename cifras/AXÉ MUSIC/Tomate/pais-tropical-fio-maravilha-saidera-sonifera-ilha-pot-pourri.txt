Tomate - Pais Tropical / Fio Maravilha / Saidera / Sonífera Ilha (Pot Pourri)

 C   F
Moro
      G        C  F
Num país tropical

      G        Am  G
Abençoado por Deus
     F        G   Am
E bonito por natureza
  G    F
Que beleza
    G   C                   F
Em fevereiro (em fevereiro)
 G        C                 F
Tem carnaval (tem carnaval)
             G             Am  G
Eu tenho um fusca e um violão
           F
Eu sou Flamengo
             G              Am  G  F
E tenho uma nêga chamada Tereza


F
"Sambaby", "Sambaby"
                        C
Eu posso não ser um Band Leader
(Pois é)
F
Mas lá em casa o namorado, gato
                       C
Papagaio, todo mundo me respeita
(Póis é)
F
Essa é a razão da simpatia
                 G
Do poder, do algo mais e da alegria

Am                 Dm7   Em7        Am    Dm7  Em7
Fio maravilha, nós gostamos de você
Am            Dm7     Em7        Am    Dm7  Em7
Fio maravilha faz mais um pra gente ver

         Am            Dm
Tem um lugar diferente
      Em          Am   Dm   Em
Lá depois da saideira
     Am           Dm
Quem é de beijo, beija
           Em        Am   Dm  Em
Quem é de luta, capoeira
          Am           Dm
Tem um lugar diferente

      Em          Am   Dm  Em
Lá depois da saideira
     Am               Dm
Tem homem que vira macaco
      Em             Am   Dm  Em
E mulher que vira freira

     Am
Comandante! Capitão
       Dm          Em
Tio! Brother! Camarada
    Am
Chefia! Amigão
       Dm         Em
Desce mais uma rodada


       Am      Dm  Em
Desce mais, é
       Am        Dm  Em
Desce mais, ê ê

E|--------------------------------------|
B|--------------------------------------|
G|--------------------------------------|
D|----3-2---3-2-0------3-2---3-2-0------|
A|--0-----0-------3-0------0-------3-0--|
E|--------------------------------------|

          Am
Não posso mais viver assim do seu ladinho
         Dm                               Am
Por isso colo meu ouvido no radinho de pilha
          G         F           E
Pra te sintonizar sozinha, numa ilha

       Am
Sonífera ilha
               Dm
Descansa meus olhos
               E
Sossega minha boca
            A   A7
Me enche de luz
           Dm
Sonífera ilha
G7          C       F
   Descansa meus olhos
              E
Sossega minha boca
             Riff
Me enche de luz

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
