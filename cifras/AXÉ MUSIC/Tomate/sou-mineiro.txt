Tomate - Sou Mineiro

Intro: F#m E A E F#m E A Bm C#

F#m         Bm    E          A
Nasceu na Bahia, festa todo dia
           F#m            Bm
O som que faz a mente delirar
             C#              F#m
Partiu de Salvador direto pra BH (bis)

         E                A
E o povo todo caiu nessa folia
         E                 F#m
Mistura boa, de Minas com Bahia
          E                    A
Bato no peito com orgulho pra dizer
          Bm          C#         F#m
Eu sou mineiro, micareteiro ate morrer

                     E         A
Bom de mais da conta sô, sou mineiro
                      E         F#m
Bom de mais da conta sô, micareteiro

                      E         A
Bom de mais da conta sô, sou mineiro
                      E         F#m
Bom de mais da conta sô, micareteiro

      E      A
É paixao, é amor
     E        F#m
Emoçao que chegou
     Bm   E      A
Sou ator da multidão
      E           F#m
Brasileiro de coração
        Bm          C#         F#m
Sou mineiro, micareteiro ate morrer

                      E         A
Bom de mais da conta sô, sou mineiro
                      E         F#m
Bom de mais da conta sô, micareteiro
                      E         A
Bom de mais da conta sô, sou mineiro
                      E         F#m
Bom de mais da conta sô, micareteiro

intro

F#m         Bm    E          A
Nasceu na Bahia, festa todo dia
           F#m            Bm
O som que faz a mente delirar
             C#              F#m
Partiu de Salvador direto pra BH

         E                A
E o povo todo caiu nessa folia
         E                 F#m
Mistura boa, de Minas com Bahia
          E                    A
Bato no peito com orgulho pra dizer
          Bm          C#         F#m
Eu sou mineiro, micareteiro ate morrer

                     E         A
Bom de mais da conta sô, sou mineiro
                      E         F#m
Bom de mais da conta sô, micareteiro
                      E         A
Bom de mais da conta sô, sou mineiro
                      E         F#m
Bom de mais da conta sô, micareteiro

      E      A
É paixao, é amor
     E        F#m
Emoçao que chegou
     Bm   E      A
Sou ator da multidão
      E           F#m
Brasileiro de coração
        Bm          C#         F#m
Sou mineiro, micareteiro ate morrer

                      E         A
Bom de mais da conta sô, sou mineiro
                      E         F#m
Bom de mais da conta sô, micareteiro
                      E         A
Bom de mais da conta sô, sou mineiro
                      E         F#m
Bom de mais da conta sô, micareteiro
                      E         A
Bom de mais da conta sô, sou mineiro
                      E         F#m
Bom de mais da conta sô, micareteiro
                      E         A
Bom de mais da conta sô, sou mineiro
                      E         F#m
Bom de mais da conta sô, micareteirooooo

Intro

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C# = X 4 6 6 6 4
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
