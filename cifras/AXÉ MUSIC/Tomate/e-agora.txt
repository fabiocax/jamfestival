Tomate - E Agora?

G            C         D
O telefone já não toca mais
G              C          D
Estou sentindo a falta de você
G               C             D
Meu bem já sabes que te amo de mais
G                  C       D
Queria está ao lado pra saber
C                      D             Em
Se um dia a gente ainda vai se encontrar
           Bm
Pra falar, conversar
C
Dos momentos bons
Am                D C Bm Am
Que não consigo esquecer

G
E agora ?
                       C            D
Me diz o que é que eu faço com esse amor

G
E agora ?
                      C     D
Porque é que tudo isso se acabou
Em
E agora ?
                     F  E     Am            D
O que é que eu faço com meu coração que está chorando

solo: C D Bm F E Am G/B Cm

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
