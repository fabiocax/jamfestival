Claudia Leitte - Bem Vindo Amor

Intro 2x: C C7 F Fm

C                            C7
  Quero fazer cem canções
                     F
Rolar mil vezes na cama
            Fm
E rir até doer
C                  C7
  Faça uma ligação
                           F
Tire esse frio daqui de dentro

Quando não for assim
Fm                     C     C7
Mande uma mensagem pra mim

F                   Fm
  Ainda lembro o gosto
                      C   C7
Do beijo que você me deu

F                      Fm
  Mar se o céu for você
                   C
Se você for pra mim
                  C7
Entrego tudo a Deus

Eu digo que sim
F                    Fm
Mar se o céu for você
                   C
Se você for pra mim
                  C7
Entrego tudo a Deus

Eu digo:
          F
Bem vindo amor
         Fm
Pequeno, eu vou
              C
Cair nos seus braços

Quero enlouquecer
C7
Deixa acontecer
          F
Bem vindo meu amor...
        Fm
Pequeno eu vou...
  C       C7
Sorrindo!
              F
Bem vindo meu amor
         Fm
Pequeno, eu vou
              C
Cair nos seus braços

Quero enlouquecer
C7
Deixa acontecer
          F
Bem vindo meu amor...
        Fm
Pequeno eu vou...
  C       C7  F  Fm  C  C7  F  Fm
Sorrindo!

----------------- Acordes -----------------
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
