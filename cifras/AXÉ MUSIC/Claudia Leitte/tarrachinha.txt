Claudia Leitte - Tarrachinha

C       Am               Dm
Virou, mexeu, tirou ondinha
G
Vai na tarraxinha (4x)

C                  G
Tô salivando de desejo
C                   G
Essa dancinha me pegou
C                    G
Ritmo quente, envolvente
F        G          C
Vou remexer esse calor

(repete Refrão)

C       Am          Dm
Dança solto ou agarrado,
       G          C
Dança homem e mulher
C        Am       G
Na dança da tarraxinha

                        C
Só não dança quem não quer

C       Am               Dm
Vou pra frente e vou pra trás
        G             C
Pra esquerda e pra direita
C        Am       G
Num salão apertadinho
                  G
Todo mundo se ajeita

(repete Refrão)

C
É gostoso o jeito que a gente faz
G
Aperta mais, aperta mais
C
É nesse embalo que o povo vai
G
Aperta mais, aperta mais (2x)

(repete a música toda, menos a última parte)
(repete Refrão)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
