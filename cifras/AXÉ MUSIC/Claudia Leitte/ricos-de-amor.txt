Claudia Leitte - Ricos de Amor

 Cm
Ouvi dizer que os opostos se atraem
        Eb
Mas ha exceções porque somos iguais
        Bb
Não posso querer nada mais
         F
Já Tenho você, saúde e paz
         Cm
Se Lembra um tempo atrás
          Eb
De quando eu saí da casa dos seus pais
           Bb
Com uma mão na frente e outra atrás
     F
Só roupa e uns trinta reais
    Cm
Hô ô ô ô
    Eb
Hô ô ô ô
                             Bb
Só eu, você e o nosso amor

                              F
Só eu, você e o nosso amor
 Cm                                                        Eb
Deposita aí mil beijos na minha conta,

                                                       Bb
O Meu cofre de Carinho eu te dou,

                                                               F
Se Juntar nós dois, a gente faz a soma,
                                Cm
Estamos ricos de amor
                                                                Eb
Você vale mais que qualquer diamante,
                                                                               Bb
Mas que qualquer prêmio que alguém já ganhou,

                                                               F
Faz cachorro quente com refrigerante,

                                  Cm
Já somos ricos de amor

     Eb
De amor,
                   Bb
A gente só precisa de amor
        F
De amor,

         Cm
De amor, de amor, de amor, de amor

          Eb
De amor, de amor, de amor, de amor

           Bb
De amor, de amor, de amor, de amor

           F
De amor, de amor, de amor, de amor

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
