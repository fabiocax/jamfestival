Claudia Leitte - Prece

Intro: B(add9) / A(add9) / E(add9) / B(add9) / B(add9)

B(add9) F#/A#
Tô a beira da loucura
G#m7 D#m7
A saudade me matando
Emaj7 D#m7
Você veio me dizer
C#m7 F# E/G# F#/A#
Que não está mais me amando
B(add9) F#/A#
Tô morando num vazio
 G° G#m7
E pago condomínio a solidão
 D#m7 C#m7 Amaj7 B(sus9)
Sonho com o seu perdão
 E(add9)
Quando você se achou
 B/D#
Eu me perdi
Eb° C#m7
Olha eu não suporto essa tristeza aqui em mim

 Amaj7
Tive tempos ruins
 E/G# F#m7 F#m7/E
Espero tempo bom
 F#m7 F#m7/E D6 B(sus9)
Felicidade em fim

(refrão)

A/C# B/D# E(add9) B/D#
É ruim eu deixar de ter
 (Na 2x G#7 ) C#m7
Meu corpo explorado
 G#m11
Teu rosto colado
 Amaj7 E/G#
Um beijo molhado eu sei
F#m7 B(sus9)
Você , yhê iee. 2X


 F#m7 G#m11
Me deixei guiar
 A(add9)
Porque você
 G#m11
Foi me levar
 F#m7 F#m7/E
E agora o que ficou
Eb° C°
Sozinha estou...

(refrão)

A/C# B/D# E(add9) B/D#
É ruim eu deixar de ter
 (Na 2x G#7 ) C#m7
Meu corpo explorado
 G#m11
Teu rosto colado
 Amaj7 E/G#
Um beijo molhado eu sei
F#m7 B(sus9)
Você , yhê iee. 2X

----------------- Acordes -----------------
A(add9) = X 0 2 2 0 0
A/C# = X 4 X 2 5 5
Amaj7 = X 0 2 1 2 0
B(add9) = X 2 4 4 2 2
B/D# = X 6 X 4 7 7
C#m7 = X 4 6 4 5 4
D#m7 = X X 1 3 2 2
D6 = X 5 4 2 0 X
E(add9) = 0 2 4 1 0 0
E/G# = 4 X 2 4 5 X
Eb° = X X 1 2 1 2
Emaj7 = X X 2 4 4 4
F# = 2 4 4 3 2 2
F#/A# = 6 X 4 6 7 X
F#m7 = 2 X 2 2 2 X
F#m7/E = X X 2 2 5 2
G#7 = 4 6 4 5 4 4
G#m11 = 4 X 1 4 2 X
G#m7 = 4 X 4 4 4 X
G° = 3 X 2 3 2 X
