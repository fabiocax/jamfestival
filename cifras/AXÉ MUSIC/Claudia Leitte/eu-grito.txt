Claudia Leitte - Eu Grito

Introdução: A C#m A C#m A C#m A B
           Uh,Uh,Uh, Ô,Ô,Uh,Ô,Ô, [3x] Uh,Uh,Uh,Uh,Uh,Uh

       E                         C#m
Seu corpo colado ao meu
        A                          B
Sua boca calando a minha
       E                 C#m
E a gente quase perdeu
     A                   B
Lá fora a chuva caía

B
Só um telefonema
              C#m
Pra você dizer que vai ter outra vez
A
Faz tempo eu me apaixonei
           Am
A noite acabou
      B
E eu sei


              E
Você é o meu amor
    E4                            E
E eu grito de felicidade amor
    E4                  C#m
É lindo o que a gente faz
          A         B
Eu quero mais (eu quero mais)[2x]

(intro)
(repete)

             E
Você é o meu amor
    E4                            E
E eu grito de felicidade amor
    E4                  C#m
É lindo o que a gente faz
          A         B
Eu quero mais (eu quero mais)
             E
Você é o meu amor
   E4                         E
E eu grito de felicidade amor
    E4
É lindo eu quero sempre mais
                  B
Um pouco mais (eu quero mais)

(solo)
A C#m A C#m A C#m A B

             E
Você é o meu amor
   E4                            E
Eu grito de felicidade amor

E é lindo eu quero sempre mais

Um pouco mais e mais(eu quero mais)
             E
Você é o meu amor
    E4                            E
E eu grito de felicidade amor
    E4                  C#m
É lindo o que a gente faz
          A         B
Eu quero mais (eu quero mais)

A C#m A C#m A C#m A C#m B E
Uh,Uh,Uh, Ô,Ô,Uh,Ô,Ô, [3x]

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
E4 = 0 2 2 2 0 0
