Claudia Leitte - Despenteia

Intro: C  G

C         Em
Viver a vida
F
Com emoção
G                               C
Levar a sério as coisas do coração
      Em    F      G
Ser feliz feliz
C         Em
Poder sonhar
 F
Seja como for
 G                             C
Andar sorrindo ao lado do meu amor
     Em    F      G    F
Pedir bis bis
         C                G
Imaginar um dia de sol (de sol)
       C            F
Cabelos ao vento

           C                Am
Sentir-se linda no final
      G                    C
Fazer da vida um grande carnaval
C
Se despenteia
         Em
Se despenteia
F                         G
Viajar, dançar, correr, entrar no mar!
C
Se despenteia
         Em
Se despenteia
F                      G
Admirar as estrelar, fazer amor
C
Se despenteia
         Em
Se despenteia
F                     G          C
Tudo que é bom de verdade, despenteia!

Passagem: C  G

C         Em
Viver a vida
F
Com emoção
G                               C
Levar a sério as coisas do coração
      Em    F      G
Ser feliz feliz
C         Em
Poder sonhar
 F
Seja como for
 G                             C
Andar sorrindo ao lado do meu amor
     Em    F      G    F
Pedir bis bis
         C                G
Imaginar um dia de sol (de sol)
       C            F
Cabelos ao vento
           C                Am
Sentir-se linda no final
      G                    C
Fazer da vida um grande carnaval
C
Se despenteia
         Em
Se despenteia
F                         G
Viajar, dançar, correr, entrar no mar!
C
Se despenteia
         Em
Se despenteia
F                      G
Admirar as estrelar, fazer amor
C
Se despenteia
         Em
Se despenteia
F                     G          C
Tudo que é bom de verdade, despenteia!

Final: C  G

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
