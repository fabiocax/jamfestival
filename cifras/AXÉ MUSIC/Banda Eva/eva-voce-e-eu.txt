Banda Eva - Eva, você e eu



Bm                 D
Levo a minha vida, ou ou ou
Em                Bm
Sem o seu abraço, ou ou ou
C
Mas eu não me acho
G        Bm            D
Acho que fui eu que errei
Bm               D
E na despedida, ou ou ou
Em                 Bm
Um beco sem saída, ou ou ou
C                 D         G    D
Te dei a minha vida, você me perdeu
Bm        D     Em    D             C
Eu estava acostumada, a ser sua namorada
D7         Bm        D7
Não consigo te esquecer
Bm      D        Em   D                  C
Alguma coisa me diz, que eu só vou ser feliz

D7               G  D
Com você ao meu lado
E            B          C
Vou te encontrar na avenida,
                Am
Tão lindo, tão lindo
 Bm           G
Como Deus te fez
E       B        C                  Am
E te beijar na saída do bloco mais lindo
D7        Bm       B
Eva, você e eu

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
