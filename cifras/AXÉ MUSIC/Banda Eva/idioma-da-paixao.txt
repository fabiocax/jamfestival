Banda Eva - Idioma da Paixão


G             Em/G          Cadd9
Quero falar a lingua do teu coração
   Am7       C      D7       D
idioma da paixão eu vou aprender
G
 te entrego a minha vida
       Cadd9
sem ter hora de partida
       Am7         C           D
sem pensar na despedida, sem sofrer

G             Em/G          Cadd9
Quero falar a lingua do teu coração
   Am7       C      D7       D
idioma da paixão eu vou aprender
G
 te entrego a minha vida
       Cadd9
sem ter hora de partida
       Am7         C           D
sem pensar na despedida, sem sofrer


G                     Cadd9
Fica pertinho de mim amor
Am7            C      D
me derreto sim, pra você
G                          Cadd9
faz um denguinho de mim, iô, iô
Am7                    C     D  G  D
me ama como se fosse a primeira vez

         G
Assim eu vou
               Cadd9
me leva nesse balanço
                Am7
adormeço com teu canto
        C              D
em teus braços como uma flor

         G                     Cadd9
Vai me regando assim, me dando seu calor
     Am7       D            G
me enraizando entre nesse amor
G               Cadd9
deixa eu entrar no teu coração
       Am7          C
que eu jogo a chave fora
      D
nunca mais vou ver a solidão

G       Bm  C
Iô, iô, iô
         D              G
Eu nunca mais vou ver a solidão

G       Bm  C
Iô, iô, iô
          D               G
Amor, não deixe assim meu coração

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
Cadd9 = X 3 5 5 3 3
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em/G = 3 X 2 4 5 X
G = 3 2 0 0 0 3
