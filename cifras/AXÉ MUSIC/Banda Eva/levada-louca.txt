Banda Eva - Levada Louca

Intro:Bm7  Em7  A

Bm7               Em7   A
 Chegue, chegue, chegue    4x

Bm7                 Em7
Tem festa no Candeal
           A    Bm7
Batuque no Canjerê
                        Em7
Eu vou levar meu timbal
            A   Bm7
Tocar samba pra você
                   Em7
Tem festa no Candeal
           A     Bm7
Batuque no Canjerê
                        Em7
Eu vou lavar meu timbal
            A     Bm7
Tocar samba pra você

         A
Não fico quieto quando vejo num boteco
                                        Bm7
Tamborim com reco-reco, xequerê a chacoalhar
            Em7
Bate, bate, bate
  A                  Bm7
Esquenta a mão nesse couro
              Em7
Cante, cante, cante
 A                  Bm7
Acorda que o dia nasceu
            Em7
Bate, bate, bate
  A                  Bm7
Esquenta a mão nesse couro
              Em7
Cante, cante, cante
 A                  Bm7
Acorda que o dia nasceu

       A
Levada louca
       Bm7
Levada louca
       A            Bm7
Levada louca pra dançar

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm7 = X 2 4 2 3 2
Em7 = 0 2 2 0 3 0
