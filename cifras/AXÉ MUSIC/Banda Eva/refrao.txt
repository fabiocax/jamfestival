Banda Eva - Refrão

Intro: A   -   C#m7   -   F#m   -   E   -   B/D#   -   D/E

A                       D7M
Queria ter você por perto
        Dm7                A
Pois tudo com você da certo
                       D7M
Seria bom te ver sorrindo
        Dm7                A        G/A
Pois tudo com você é lindo
D7M                               E/D
O amor vai balançar essa cidade
                   A  C#m7   F#m
Encher de felicidade e cantar
E           B/D#
Aquela música
D/E
Que te fiz pra namorar

A    
La-la-la-la-la-la-ia  


A7M
La-la-la-la-la-la-ia  

D7M 
La-la-la-la-la-la-ia  

Dm7
La-la-la-la-la-la-ia  

A                   D7M
Queria desvendar segredos
         Dm7               A
Pois com você não tenho medo
                   D7M
Queria uma noite a toa
Dm7                  A      G/A
Ficar contigo numa boa
D7M                                 E/D
Nosso amor vai balançar essa cidade
                 A   C#m7   F#m
Encher de felicidade e cantar
E          B/D#
Aquela música
D/E
Que te fiz pra namorar

A    
La-la-la-la-la-la-ia  

A7M
La-la-la-la-la-la-ia  

D7M 
La-la-la-la-la-la-ia  

Dm7
La-la-la-la-la-la-ia  


Ps: Você pode substituir o C#m7 pelo E/G# ...fica ao seu critério..

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7M = X 0 2 1 2 0
B/D# = X 6 X 4 7 7
C#m7 = X 4 6 4 5 4
D/E = X X 2 2 3 2
D7M = X X 0 2 2 2
Dm7 = X 5 7 5 6 5
E = 0 2 2 1 0 0
E/D = X 5 X 4 5 4
E/G# = 4 X 2 4 5 X
F#m = 2 4 4 2 2 2
G/A = 5 X 5 4 3 X
