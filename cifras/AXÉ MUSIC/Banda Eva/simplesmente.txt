Banda Eva - Simplesmente

                  Bm7             C#m7             F#m7
Quando você me beijar pétalas de rosa do céu irão chover
                  Bm7             C#m7             F#m7
Quando você me beijar pétalas de rosa do céu irão chover
                  Bm7             C#m7             F#m7
Quando você me beijar pétalas de rosa do céu irão chover
                  Bm7             C#m7             F#m7
Quando você me beijar pétalas de rosa do céu irão chover

      Bm7        C#m7             F#m7
Simplesmente,            seu admirador
                           Bm7
Troco tudo, faço qualquer coisa
            C#m7                   F#m7
Pulo de asa delta pra ganhar o seu amor
      Bm7        C#m7             F#m7
Simplesmente,            seu admirador
                           Bm7
Troco tudo, faço qualquer coisa
            C#m7                   F#m7
Pulo de asa delta pra ganhar o seu amor


                  Cm7             C#m7             F#m7
Quando você me beijar pétalas de rosa do céu irão chover
                  Cm7             C#m7             F#m7
Quando você me beijar pétalas de rosa do céu irão chover
                  Cm7             C#m7             F#m7
Quando você me beijar pétalas de rosa do céu irão chover
                  Cm7             C#m7             F#m7
Quando você me beijar pétalas de rosa do céu irão chover

              D      E                 F#m7                 Bm7
A tarde vai cair          A noite vai rolar  Madrugada vai surgir
            C#m7                   F#m7
Tem um dia lindo e a gente não se larga mais
              D      E                 F#m7                 Bm7
A tarde vai cair          A noite vai rolar  Madrugada vai surgir
            C#m7                   F#m7
Tem um dia lindo e a gente não se larga mais

----------------- Acordes -----------------
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
Cm7 = X 3 5 3 4 3
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m7 = 2 X 2 2 2 X
