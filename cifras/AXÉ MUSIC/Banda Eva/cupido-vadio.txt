Banda Eva - Cupido Vadio

Intro:   F  Bb   C   F   Bb    C   F    G    C

         F                Bb        C             F
Eu já descobri que a minha vida é sua, a vida é uma só
        Bb        C                F
Sou seu ursinho, sua gatinha, seu xodó
     G                     C
Só durmo agarradinha com você
       F               Bb       C            F
É meu cobertor nas noites de frio e nas de calor
    Bb      C         F                    G
Também sinto frio, me leva que eu vou apaixonada
           C
Fissurada nesse amor! oh ! oh!
 F                      Bb     C               F
Quem me flechou foi um cupido vadio e ele acertou
      Bb         C         F
Meu coração de jeitinho me leva que eu vou
     G                 C
Apaixonada, fissurada nesse amor oh! oh!
              F          Bb            C       F
Mas eu só quero te amar, não deixe essa canoa virar

                Bb         C          F
Eu vou remar nas águas cristalinas do mar
              G                         C
Vou navegar e me entregar de vez essa paixão!
          F              Bb             C      F
Mas eu só quero te amar, não deixe essa canoa virar
                 Bb        C         F
Eu vou remar nas águas cristalinas do mar
             G                           C
Vou navegar e te entregar de vez meu coração!
 F                Bb               C   F      G         C
Oh ! oh! oh! oh!  xálalalalala... oh! oh! oh! oh!oh oh! oh!
A) Introdução
B) Estrofe 1
C) Refrão - Final

----------------- Acordes -----------------
B = X 2 4 4 4 2
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
