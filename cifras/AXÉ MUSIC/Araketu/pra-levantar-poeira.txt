Araketu - Pra Levantar Poeira


   Gm
É no swing do Ara Ketu
             F
Que eu vou assim
           D
Se quer dançar
               Gm    F
Venha atrás de mim

  Gm    G  F          Gm
 E o meu coração palpita
                  Cm
 Se te vejo na avenida
     F            Gm     F
 A dançar no lindo ARA.

 Gm       G   F          Gm
E pra ficar de bem com a vida
                 Cm
Vou cantar com alegria

       F             Gm
Pois melhor coisa não há.


  Gm               G
Quem dançou no Ara Ketu
     Cm          F
Balançoooooooooou


 a vontade que se tem
                Bb        Dm     Gm
 é de não parar mais, não não não não.
                   G

 Coração a bater forte
              Cm              F
 Vou curtir, dançar, pular, sorrir
                  D       Gm
 O teu som é sinfonia e magia.


              G        Cm
 Tá que tá que tá gostoso
                   F
 Tá que tá, tá que tá
                   Bb
 É pra levantar poeira
      D         Gm
 E bailar, e bailar

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
