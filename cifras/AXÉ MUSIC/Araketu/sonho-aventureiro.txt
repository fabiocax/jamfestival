Araketu - Sonho Aventureiro

         G
Tô de partida, desarmado
                 D
E no peito o coração
C                        G
Não levo nada, deixo um sonho
      Am          D
Aventureiro de emoção.

G
Esqueço tudo
                             D
Pra viver livremente nova paixão
C                        G
Vagando afora encontro o mundo
               Am   D              G
Como um livro aberto esperando a lição.

C
Foi por amor
                          Em
Todo esse tempo que construí

                             Am
Tudo que passou e me fez sentir
         D        G
Momentos de felicidade.

   C
Ao longe, a saudade apertou
   Em
A dor, uma navalha cega
       Am
Que o amor deixou
     D           G
Em pedaços de bondade.(repete)

G              D
Da vida sou pioneiro

Meu amor
                    G
Faço do prazer o primeiro
    Em
sedutor
                    Am
Te quero de corpo inteiro
          C                   Em
Pra que o sonho conquiste o luar. (repete)

Refrão:

G                              Am
Vem, vem, vem, se balançar meu bem
     D                  Em
Vem, vem, que o araketu tem, tem
                  C
O dom de eternizar
              D
Toda fonte de luz
                    G
Que é pra gente se amar.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
