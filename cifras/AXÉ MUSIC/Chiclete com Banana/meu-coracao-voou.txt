Chiclete Com Banana - Meu Coração Voou

C                  Am        F                  G
Meu coração voou voou voou voou atrás desse amor que é você
C                  Am        F                  G
Meu coração voou voou voou voou atrás desse amor
      C         Am      F           G
Pararara Lelelele lelelele leleleleleleee

C                                     G
   Todo dia toda hora a procura de voceeeee
C                                                G
   Cada passo desta estrada, em seus sonhos vou viverrr
Dm                    F
  Passo o tempo imaginando flutuando pelo mar
Dm                      F                          G
  Seu perfume misturando esse aroma pelo ar, pelo arrr
Dm                      F
  E assim eu vou te amando me perdendo nesse olhar
Dm                     F                             G
  Aos delírios navegando pelas nuvens pelo ar, pelo aarrr

C                  Am        F                  G
Meu coração voou voou voou voou atrás desse amor que é você

C                  Am        F                  G
Meu coração voou voou voou voou atrás desse amor
      C         Am      F           G
Pararara Lelelele lelelele leleleleleleee

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
