Chiclete Com Banana - De Bem Com a Vida

Intro: B G#m B F#7

B                    G#m
Não vou dizer não à vida
B                  G#m
Não tenho porque parar
B                  G#m
Se o momento é agora
G          F#7       D
Não vou deixar escapar

B                      G#m
Se tens a faca e o queijo
B                    G#m
Então por que não cortar?
B                        G#m
Se o bem e o mal andam juntos
E      F#7           B
Então no que acreditar?

E        F#7            B
Tenho um segredo prá isso

E        F#7       B
Que só o tempo lhe dá
E       F#7         B
E não se pode ter medo
G          F#7           B
Nem de seguir nem de sonhar

G#m                        F#
Quero ser herói e não bandido
G#m                  F#7
Já perdi o medo de pecar
G#m                        F#7
Quero estar curado e não ferido
E           F#7
Se a chuva cair
E            F#7
Não vai me molhar

----------------- Acordes -----------------
B = X 2 4 4 4 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
F#7 = 2 4 2 3 2 2
G = 3 2 0 0 0 3
G#m = 4 6 6 4 4 4
