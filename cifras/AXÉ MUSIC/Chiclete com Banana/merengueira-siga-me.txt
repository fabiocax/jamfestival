Chiclete Com Banana - Merengueira Siga-me

(Andréa Lessa/ Augusto Cesar/ Pio Medrado)
(Intro: Dm A)

Dm                 A
Se tem merengue aí
Eu vou, eu vou, eu tô
                  Dm
Quero me divertir, dançar
Com meu amor
                   A
Se tem merengue aí
Eu vou, eu vou, eu tô
                     Dm
Olha, pra que tristeza

               Gm
Toca o seu timbau pra valer
                 Dm
Mostra o seu astral na moral
                 A#
E vem dançar merengue

  A              Dm
É festa, é carnaval    (bis)

Dm
Essa menina quando passa
             A
Todo mundo bole
                             Dm
Todo mundo bole, todo mundo bole (4x)

          A                  Dm
Eu quero ver minha nega mexer, bailar
       Gm            A         Dm
Vamos balançar, venha chicletear
         A                     Dm
Seja de frente de lado ou de banda
                     A#
Esse som mexe com a gente
       A            Dm
Vem comigo, vem dançar
Siga-me, siga-me
(Dm A)

----------------- Acordes -----------------
A = X 0 2 2 2 0
A# = X 1 3 3 3 1
Dm = X X 0 2 3 1
Gm = 3 5 5 3 3 3
