Chiclete Com Banana - Preciso dormir princesa

(Bell Marques/Wadinho Marques)


 A                     D                  A   D E7
Jogue a vida arrisque tudo e curta esse amor
    A                    D                   A   D E7
Se livre dessa cara amarrada e saia como entrou
A                  D                A   D E7
Um cochicho apimentado quero lhe falar
    A                  D                   A   D E7
Eu não sei se apago a luz ou deixo como está

    A  Bm D
Procure
    E          A        Bm  D
na sombra da flor de espinho
    E           A     Bm  D
no peixe de um mar sozinho
    E          A  Bm  D  E
Desnude esse amor


     A  Bm D
Tristeza
    E       A          Bm  D
Reflete um rio no meu rosto
    E        A        Bm  D
Pintura de amor e desgosto
   E          A  Bm  D  E
Você quem pintou

  A   D                           A        D
E eu...  perco meu tempo prá lhe convencer
          E            A      Bm D
Fico com medo do dia amanhecer
        E     A  Bm  D  E
Quero amar você

   A   Bm D     E       A       Bm D
É tarde...  preciso dormir princesa
  E        A     Bm  D
rainha de toda beleza
    E         A  Bm  D  E
sonhar esse amor

E eu...

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
