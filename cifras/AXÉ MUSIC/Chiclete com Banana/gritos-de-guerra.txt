Chiclete Com Banana - Gritos de Guerra

         Dm                    C
Vou caminhando entre flores e guerras
          Bb             C       Dm
Vou deslizando entre O bem e o mal
                                 C
Um pouco louco entre monstros e feras
         Bb         C      Dm
Sou cavaleiro do juízo final

       C                      Dm
A esperança é uma flecha de fogo
           C             Dm
Que faz arder o meu coração

    G        A       Dm
Eu canto e grito de novo
Bb                   A
Paz nesse mundo e união

Dm           C
Eô... Eô...

                A
Ai ai ai... ai ai  2x

           Dm        C                Dm
A minha espada é a voz com que eu canto
        Bb           C          Dm
Voando leve, livre como um pardal
                    C             Dm
Você me beija e eu me perco no encanto
          G          A       Dm
Olhe pra vida, é fantasia real

       C                      Dm
A esperança é uma flecha de fogo
           C              Dm
Que faz arder no meu coração

    G        A       Dm
Eu canto e grito de novo
Bb                   A
Paz nesse mundo e união

Dm           C
Eô... Eô...
                A
Ai ai ai... ai ai

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
G = 3 2 0 0 0 3
