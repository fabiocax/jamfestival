Chiclete Com Banana - Por um beijo seu

(Sérgio Passos)

Intro: (Em G C B7)

Em                   Am
Já me pediu a Ponta do Humaitá
                         Em
A estrela d'alva, eu me lembro já te dei
                           B7
E o Farol da Barra também disse que é só seu
C                         B7
Troquei num beijo que me deu
Em                   Am
Copacabana, eu prometi e vou te dar
                             Em
Em Maceió, eu já cerquei a Praia do Francês
                        C
E o Camaleão quando passar
               B7                  E
A gente vai ficar juntinho de uma vez
              F°        F#m7
Tudo que eu quero dessa vida é seu amor

     B7                 E
Só quero seu amor e um beijo seu
          C#m7      F#m7
E eu só quero dessa vida é seu amor
     B7                       E
Só quero seu amor e um beijo seu

solo: (Em G C B7)

Em                     Am
Disse que quer a Ilha de Itamaracá
                          Em
A lua nova no quarto minguante namorar
                                   B7             C
Não sei o que é que eu faço, ela quer o Pelourinho
                         B7
E o Campo Grande quer todinho
   Em                      Am
O rio São Francisco tô querendo desviar
                             Em
Pra molhar a flor do nosso amor que quer brotar
                       C
No Nana Banana te abraçar
                   B7                  E
E a gente então ficar juntinho de uma vez
Tudo que eu quero dessa vida é seu amor...

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
C#m7 = X 4 6 4 5 4
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F#m7 = 2 X 2 2 2 X
F° = X X 3 4 3 4
G = 3 2 0 0 0 3
