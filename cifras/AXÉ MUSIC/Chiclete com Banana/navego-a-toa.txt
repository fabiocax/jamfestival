Chiclete Com Banana - Navego à Toa

(intro) C Dm7 G7 C Gm7 G7 C

C                   G7   G#m Am
Vem, amor, me dê a mão
                         G
Vem fugir pra aquela estrela
   C                 G   G#m Am
E deixe a dor do coração
                     G
Não passar de brincadeira
D#m7 Dm                Am7
Você é um pecado no Bolero de Ravel
F              G              C
E a beleza do mar beijando o céu

  C                Dm7
Navego à toa, numa boa
              G7       C
Navego no teu doce sorriso
                Dm7             G7      C
Entre os seus segredos, com os seus delírios

                           Dm7
Me alucina em saber que um dia
        G7                C
Em seus braços eu posso voar
                       Dm7
E ainda molhar seus cabelos
          G7              C          G7
Com o perfume da água do mar, ai ai ai

     C
Pode ser, ai ai ai
                                     Dm7
Que esse amor seja o mais doce sofrimento
                            G7
Não me faça infeliz nesse momento
Dm7            G          C  G7
Se o ciúme é próprio do amor

     C
Pode ser, ai ai ai
                             Dm7
Que o amor nunca tenha feito parte da sua vida
                           G7
Mas te peço uma chance, querida
Dm7               G            C  G7
Todo espinho faz parte da sua flor

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
D#m7 = X X 1 3 2 2
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G#m = 4 6 6 4 4 4
G7 = 3 5 3 4 3 3
Gm7 = 3 X 3 3 3 X
