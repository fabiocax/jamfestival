Chiclete Com Banana - Vigiando você

(Nando Cordel)

Intro: (Dm Bb C F A)

Dm                  Gm  C
Eu sou tão apaixonado
               F  A  Dm
E saio pra vigiar
                    Gm   A
Toda hora eu me pergunto
                    Dm A Dm
Meu amor onde andará?

                   Gm  C
A paixão é tão intensa
                   F  A  Dm
E não sei como conter
                     Gm  A
Parece que eu sou domado
                  Dm  D7
Totalmente por você


      Gm                    Dm
Ô ô ô ô, cada vez eu te quero
                 A
Meu amor é sincero
                      Dm  D7
Esse amor vai me devorar
      Gm                     Dm
Ô ô ô ô, não se zangue comigo
                   A
Eu preciso de abrigo
                            Dm
Ser feliz é pra quem sabe amar

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
