Chiclete Com Banana - Cidadão

 (Intro: G C)

         G       C            G       C
Fazer o que cidadão, fazer o que cidadão
         G       C            D
Fazer o que cidadão, fazer o quê? (2x)

   G          C        G   C
O broto reclamando da vida
   G        C      G  C
A cara feia do patrão
   G           C      G   C
Entrar num beco sem saída
   Am              D
Saindo pela contramão

   G        C         G   C
A prestação já tá vencida
  G       C        G  C
E a televisão quebrou
   G           C        G   C
E lá vou eu pro fim da fila

   Am                   D
Atrás do cheque que voltou

(G C)
Fazer o que cidadão, fazer o que cidadão
                               D
Fazer o que cidadão, fazer o quê? (2x)


 Am          C        D
A cigana um dia me falou
    Am            C          D
Que tudo vai dar certo meu amor

   Am         C          D
Será que a cigana tem razão
   Am           C            D
Será que leu errado a minha mão

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
