Chiclete Com Banana - Ele não monta na lambreta

(Bell e Wadinho Marques)


Em
Vejam vocês o cara que eu conheci
Que só namora com o broto no portão
       D
Mão na mão
                   Em
Pra não ter complicação

Ele acha virgindade uma legal
E se assusta quando ouve um palavrão
     D                 Em          D
E só ver mulher nua na TV

      C                 Em            D
Ele garante que sabe beijar
           C
Mas dá chilique na hora...
   B
na hora h

       Em           D
Ele não monta na lambreta         :
        Em          D             :
Ele não monta na lambreta         : Refrão
        Em          D     C   D   :
Ele não monta na lambreta         :
       Em
De minissaia não gosta
E acha imoral
Biquini curto, asa delta

Ou até fio dental
          D
Ele é demais
mais, mais, mais
         Em
Ele é fingido

Não dança uma lambada
só valsa de estros
é um temcri de galocha
me tira o astral
         D
Ele é demais
mais, mais, mais
         Em         G
Ele é fingido
         D             C
Fez juramento de castidade
         Em           G
Pro seu amor
           D
E alem de tudo
          C
pediu sua mão
        B
E ela negou
           Em              D
Quando vê mulher, ele fica doido
           Em              D
Quando vê mulher, ele fica doido
         C
Mas dá chilique na hora...
  D
na hora h

(Repete Refrão)

----------------- Acordes -----------------
B = X 2 4 4 4 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
