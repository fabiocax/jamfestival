Chiclete Com Banana - Chiclete Chopp Com Banana

[Intro]  E  A

Entre o vai e vem que rola muita pedra vai rolar
Faço samba com teu cheiro e me calo pra te amar
De janeiro a janeiro quebra coco, quebra mar
Vou rezar lá no terreiro e fevereiro te encontrar

      E           A      E    A    E         A   B7
Você gosta do Guns N'Roses, eu gosto do Ilê Aiê
                                                       E B7
Chiclete chopp com banana, em Copacabana vou amar você
      E           A  E   A    E           A     B7
Você gosta do Caetano, eu gosto do Gilberto Gil
                                                    E
Você gosta da Timbalada mas eu pego baba na Boca do Rio
           E                        B7
Vumbora imbora que a carona já chegou
                                 E
Vem nesse toque, que é de felicidade
                                  B7
Vamos imbora, vem comigo, vem agora

           A    B7           E
Então vem, depois é muito tarde
        B7                        E
Ê, ê, ê, dança aí que eu quero ver
                       B7                      E
Faço samba com teu cheiro e me calo pra te amar
                      B7                         E
Vou rezar lá no terreiro e fevereiro te encontrar
      E         A       E     A    E             A     B7
Você gosta do Elvis Presley, eu gosto mesmo é do Falcão
                                                           E  B7
Vira volta, volta, vira e mexe, pega, deita e rola no Camaleão
   E            A     E  A     E          A   B7
O rock do Barão é pop, pop mesmo é eu te amar
                                                                     E
Se você gosta do Filhos de Gandhi, eu gosto de Armandinho, Dodô e Osmar

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
