Chiclete Com Banana - Que gata é essa

(Beto garrido/Alexandre Peixe)

E|----------------------------------------------
A|----------------------------------------------
D|----------------------------------------------
G|----10-------------10------------------11-----
B|--11------------10------8-8-8--11-10-8----8-10
E|-------10--10-8-------------------------------

E|-----------------------------------------
A|-----------------------------------------
D|-----------------------------------------
G|----10-------------10------------------11
B|--11------------10------8-8-8--11-10-8---
E|-------10--10-8--------------------------



Gm
Vixi Maria turbinada, gata manhosa danada
                                    F
Gosta de uma feijoada, tem corpo de avião


Só dorme de madrugada brindou com uma limonada
                                  Gm    F
Danada, foi batizada a musa da estação

Gm
Ela curte Reggae e axé, devora abará, acarajé
                                       F
Andando tem muita fé, não reza comigo, não

Seu sorriso é alto astral

è louca por carnaval
                                        Gm    F
Seu bronze é sensual, chiclete é sua paixão

          Bb
Que gata é essa!?
                            F
Que coisa bonita no sol de verão
                             D#
Tatuada no peito com o meu coração
                             D
Ela muda de cor como um camaleão

          Bb
Que gata é essa!?
                            F
Que coisa bonita no sol de verão
                             D#
Tatuada no peito com o meu coração
             D
Eu sou camaleão

   Bb                  F
Adoro ver quando ela passa
           D#                       D
Tudo tem graça, me faz me faz enlouquecer
       Bb               F
Adoro ver, estrela pequenina
                  D#                       D
Seu brilho me fascina, quando eu olho pra você

Gm
Cabelo cheira a mangaba, corpo gosto de goiaba
                                       F
Como se fosse salada, mexe com o meu coração

Abala a rapaziada, não se incomoda com nada
                                          Gm    F
Ouve Rock nas quebradas e também dança o baião

Gm
Formada, já é mestrada, adora uma lambada fala
                                         F
Inglês nas parada, que gata é essa meu irmão!

Malhando fica suada, zangada bem apanhada
                                   Gm    F
Danada, foi batizada a musa da estação

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
D = X X 0 2 3 2
D# = X 6 5 3 4 3
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
