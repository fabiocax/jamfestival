Chiclete Com Banana - Mulher Amada

(Saul Barbosa/Gerônimo)

e|--------7-4/5-----------------7-4/5-----------------7-4/5---5-8-|-
B|--5-5-8--------8-7-5-----5-5-8------8-7-5-----5-5-8-------7-----|-
G|----------------------------------------------------------------|-
D|----------------------------------------------------------------|-
A|----------------------------------------------------------------|-
E|----------------------------------------------------------------|-

-|--7-8-7-5--------
-|----------8-7-5--
-|-----------------
-|-----------------
-|-----------------
-|-----------------

notação: / = slide


Em                   B
Subi a avenida cantando

                         Em
Pensando encontrar meu amor
Em                          B
E o brilho em teus olhos sorrindo
               C
Me pegou de repente a paixão
         B                   Em
De uma flor perfumada se abrindo

       D
Amada minha mulher
                       G
Formosa por ser companheira
        D
És o espelho da fé
                     G
Na luta de ser verdadeira
G|-A|-Bb   B
Abraça  este teu homem
                     Em
E sempre fiel toda a vida
           C                 Am                 B
Pra tudo e para toda hora a nossa paixão vai rolar
C B    C B
Ê a... Ê a

E
Amor, Amor
       F#m         B
Não me canso de falar
       F#m         B
Não me canso de dizer
        E            B
Não me canso de te amar

E
Amor, Amor
       F#m         B
Não me canso de falar
       F#m         B
Não me canso de dizer
   E
Te amo

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
