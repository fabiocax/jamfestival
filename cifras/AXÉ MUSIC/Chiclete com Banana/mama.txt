Chiclete Com Banana - Mama

Cm          Fm
Ah iê iê iê Mama
      G
Ô iô mama
       Cm     G
sarapamama

   Cm
Óh Mama
           Fm
Hoje é seu dia
        G
Em todo canto dessa terra mamãezinha
                       Cm     G
Estão festejando o seu dia

   Cm
Óh Mama
                   Fm
Hoje não é dia de chorar
      G
Até o vento tá brigando com a tristeza

                   Cm
Que lhe fez assim chorar

Cm          Fm
Ah iê iê iê Mama
      G
Ô iô Mama
       Cm     G
sarapamama

----------------- Acordes -----------------
Cm = X 3 5 5 4 3
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
