Chiclete Com Banana - Eu fui atrás de um caminhão

    G                   Bm
 Eu fui atrás de um caminhão
                    C                     D
 Fazer meu carnaval, e o carnaval é feito no coração
      G                Bm
 Gostei, Chiclete é emoção,
                  C                    D
 Meu bem naquele ano eu me tornei Camaleão

(solo)
E|----------------|--------------|----------------|
B|----------------|--------------|----------------|
G|----------------|-----------5~-|--8--7--8--7-5-7|
D|--5-7-8--7--5---|-5-7-8-7-5----|----------------|
A|----------------|--------------|----------------|
E|----------------|--------------|----------------|

E|----------------|--------------|--------------------|
B|----------------|--------------|--------8--7-10-8~~~|
G|----------------|-----------5~-|--8--7--------------|
D|--5-7-8--7--5---|-5-7-8-7-5----|--------------------|
A|----------------|--------------|--------------------|
E|----------------|--------------|--------------------|


Gm
 Encontrei o meu amor,

 Na cidade de salvador
        F               D#
 Tão romântica, tão histórica
               D
 E o Bomfim abençoou

Gm
 No farol de Itapuã,
                           F               D#
 O baiano fez o seu trio elétrico, trouxe luz e som
                  D
 E pelo mundo se espalhou

Cm                           Gm
 Só pra lhe dizer que é Liberdade, morena
Cm                                Gm
 Praça Castro Alves para o nosso amor é pequena
Cm                                 Gm
 Quero lhe encontrar na praia da Ribeira é verão
Cm                               D
 Esse chão é meu e eu não vou parar

     G                   Bm
 Eu fui atrás de um caminhão
                    C                     D
 Fazer meu carnaval, e o carnaval é feito no coração
      G                Bm
 Gostei, Chiclete é emoção,
                  C                    D
 Meu bem naquele ano eu me tornei Camaleão

----------------- Acordes -----------------
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D# = X 6 5 3 4 3
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
