Chiclete Com Banana - Sétimo Céu

(Geraldo Azevedo/Fausto Nilo)

Intro: (A E A E C# F#m C# F#m D)

A
Eu e você
             E
No mundo da lua-de-mel
 A
Você e eu
         E
Voando no sétimo céu, ô ô ô ô
C#
O dê no que dê
              F#m
A gente não quer mais parar
C#                    F#m E
Aconteceu, eu quero denovo

  A                    E
Quero você, ainda que faça chorar

  A                      E
Quero você, sorrindo querendo ficar, ô ô ô
 C#                      F#m
Dá pra sentir o teu coração bater no meu
 C#                       F#m          D
Dá pra saber aonde esse amor vai desaguar

                E
Pois quem tem amor
      D           A
Pode rir ou chorar
                E
Pois quem tem amor
      D           A
Pode rir ou chorar
           E
Quem tem amor
      D           A
Pode rir ou chorar
                E
Pois quem tem amor
      D           A
Pode rir ou chorar

----------------- Acordes -----------------
A = X 0 2 2 2 0
C# = X 4 6 6 6 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
