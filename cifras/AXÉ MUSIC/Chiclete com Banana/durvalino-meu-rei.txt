Chiclete Com Banana - Durvalino, meu Rei

Intro: (A D E / E D A)

(A D E / E D A)
Quem me dera ter um barco novo?
Quem me dera ter um barco novo?
Pescar peixe na Bahia
Sem a sua companhia, meu rei
Jogar o anzol nesta praia
Jogar o anzol, saia da malha
É federal, mormaço de praia
                            A
Cara de pau, é o rei da gandaia
           F#m
U, tê, rê, rê,
          B
Lá, rá, rá
           F#m
U, tê, rê, rê,
          B
Lá, rá, rá
 F#m                         B
Suba essa escada que eu não conto a ninguém

    F#m                     B
Se vista de pantera, se descubra, neném
            F#m
U, tê, rê, rê,
          B
Lá, rá, rá
            F#m
U, tê, rê, rê   oh oh
          B (Bb A)
Lá, rá, rá
(A D E / E D A)
Alô, Durvalino, meu rei,  |
Pega essa onda            |2x
Deixe de milonga          |
Que eu quero é dançar     |


Obs.: Para o som ficar mais fiel ao tocado por Bell, ao vivo,
      as pestanas do acorde F#m devem ser feitas na casa 9 e a
      pestana do acorde B deve ser feito na casa 7.

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
Bb = X 1 3 3 3 1
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
