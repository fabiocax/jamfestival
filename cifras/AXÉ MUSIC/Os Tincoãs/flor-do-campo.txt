Os Tincoãs - Flor do Campo

Intro: G Am7 D G D7


   G
Morena dos olhos negros
     Em7       Am
Seu olhar é matador
                  D
Cheirosa como canela
                  G
Bonita como uma flor

Refrão (2x)

G
Oh, meu amor
Em7           Am
Não me faça assim
                 D           G
Pois a flor do campo é o alecrim


Gm               G
Oh, Maria, oh Maria
Gm   D7              Gm
Oh Maria meu amor, vem cá
Gm         Dm     Gm
Oh Maria, minha namorada
            D7             G
Borboleta rocha de asas douradas


Refrão (2x)

G
Oh, meu amor
Em7           Am
Não me faça assim
                 D           G
Pois a flor do campo é o alecrim

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
