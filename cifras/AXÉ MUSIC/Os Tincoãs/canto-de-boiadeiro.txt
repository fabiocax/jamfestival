Os Tincoãs - Canto de Boiadeiro

D           G               D
  Boiadeiro vai, já vai pra aruanda
D           G               D
  Boiadeiro vai, já vai pra aruanda
D       Em     A            D
  Adeus, Olorum, protetor de Umbanda
D       Em     A            D
  Adeus, Olorum, protetor de Umbanda
D           G               D
  Boiadeiro vai, já vai pra aruanda
D           G               D
  Boiadeiro vai, já vai pra aruanda
D       Em     A            D
  Adeus, Olorum, protetor de Umbanda
D       Em     A            D        D7
  Adeus, Olorum, protetor de Umbanda

G         D                   Bm
Adeus, meus irmãos de fé
Em                A
Adeus, gente amiga

G         D                   Bm
Cantei, dancei, comi e bebi
Em                 A7
Saravá, chegou a hora da partida

D           G               D
  Boiadeiro vai, já vai pra aruanda
D           G               D
  Boiadeiro vai, já vai pra aruanda
D       Em     A            D
  Adeus, Olorum, protetor de Umbanda
D       Em     A            D
  Adeus, Olorum, protetor de Umbanda
D           G               D
  Boiadeiro vai, já vai pra aruanda
D           G               D
  Boiadeiro vai, já vai pra aruanda
D       Em     A            D
  Adeus, Olorum, protetor de Umbanda
D       Em     A            D        D7
  Adeus, Olorum, protetor de Umbanda

G         D                   Bm
Adeus, meus irmãos de fé
Em                 A
Adeus, gente amiga
G         D                   Bm
Cantei, dancei, comi e bebi
Em                 A7
Saravá, chegou a hora da partida

D           G               D
  Boiadeiro vai, já vai pra aruanda
D           G               D
  Boiadeiro vai, já vai pra aruanda
D       Em     A            D
  Adeus, Olorum, protetor de Umbanda
D       Em     A            D
  Adeus, Olorum, protetor de Umbanda
D       Em     A            D
  Adeus, Olorum, protetor de Umbanda
D       Em     A            D
  Adeus, Olorum, protetor de Umbanda
D       Em     A            D
  Adeus, Olorum, protetor de Umbanda
D       Em     A            D
  Adeus, Olorum, protetor de Umbanda

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
