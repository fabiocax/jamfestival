Jauperi - Um Amor Que Sinto Por Você

Cai o mundo eu vou deixar cair

A bola eu vou deixar rolar

Mas não vou esquecer
                                A
Do amor que eu sinto por você

             E
Sem ter você aqui no ombro
                F#m
Sem ter você pra me abraçar
                A
sem ta com você no meu tom
                 E
Quem é que vai me levantar
                 E
Sem ter você na minha vida
                F#m
Sem ter você pra me amar
               A
Sem ta com você na saída

                E
Sem ter você quando eu voltar

 G#m    C#m
A cor    do entardecer
  F#m              B
Faz doer e voce onde andara
 G#m    C#m
O amor   veio em mim dizer
   A                      B
Que é você e em você é o meu lugar

Sha
E|-------------------------------|
B|---------9---------------------|
G|---9--11---11--9---------------|
D|-----------------11--9---------|
A|-------------------------------|
E|-------------------------------|

Padabaiabapaban

E                    A
Cai o mundo eu vou deixar cair
E             A
A bola eu vou deixar rolar
E             A                   (2x)
Mas não vou esquecer
E             A
Do amor que eu sinto por você

               E
Só com voce sou alegria
                 F#m
Só ao seu lado eu tenho paz
              A
você em mim é poesia
                  E
E quem sou eu pra querer mais

 G#m    C#m
A cor    do entardecer
  F#m              B
Faz doer e voce onde andara
 G#m    C#m
O amor   veio em mim dizer
   A                      B
Que é você e em você é o meu lugar

Sha
E|-------------------------------|
B|---------9---------------------|
G|---9--11---11--9---------------|
D|-----------------11--9---------|
A|-------------------------------|
E|-------------------------------|

Padabaiabapaban

E                    A
Cai o mundo eu vou deixar cair
E             A
A bola eu vou deixar rolar
E             A                 (2x)
Mas não vou esquecer
E             A
Do amor que eu sinto por você

E           A
Amor que eu sinto por você
E             A
Um amor que eu sinto por você

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
