Jauperi - É D'oxum

C7+                        Dm
Nessa cidade todo mundo é D'Oxum
 G                        C7+
Homem, menino, menina, mulher
C7+                      Dm
Toda essa gente irradia magia

  Dm7             F
Presente na água doce
  Dm7              F
Presente na água salgada      (bis)
          F/G
Em toda cidade brilha

C7+                            Dm
....Seja tenente ou filho de pescador
 G                         C7+
....Ou importante desembargador
C7+                             Dm
Se dar presente é tudo uma coisa só


   Dm7             F
A força que mora n'água
 Dm7                F
Não faz distinção de cor      (bis)
       F/G          C7+
E toda cidade é D'Oxum

     Dm7     C7+       Dm7      F/G
É D'Oxum...é D'Oxum...é D'Oxum
          C7+
Eu vou navegar...eu vou navegar
                       Dm7
Nas ondas do mar...eu vou navegar
            C7+
....Eu vou navegar
     C7+      Dm
É D'Oxum...é D'Oxum
           G      C7+
É D'Oxum...é D'Oxum..

----------------- Acordes -----------------
C7+ = X 3 2 0 0 X
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
F = 1 3 3 2 1 1
F/G = 3 X 3 2 1 X
G = 3 2 0 0 0 3
