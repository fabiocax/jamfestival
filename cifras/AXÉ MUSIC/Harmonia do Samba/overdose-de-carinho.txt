Harmonia do Samba - Overdose de carinho

D  Bm Em A7           D   Bm  Em
Vem cá  Beija a minha boca
    A7               D  Bm  Em
Meu Deus, que coisa louca
      A7         Am     D7
O arrepio que me dá
G   Em  Am   D7            G     Em   Am
Quando Te tenho do meu jeitinho
      D7      G  Em  Am   D7     G     A7
Overdose de carinho Até pode me matar
D Bm Em A7              D   Bm   Em
Eu sei Sei que não sou perfeito
          A7             D
Mas com você faço com jeito
                  Am      D7
Sem ter medo de errar, meu amor
G Em Am  D7          G Em Am
Amo Já disse que te amo
  D7        G Em  Am
E é nesse oceano
              Dm   G7
Que eu quero navegar

                     C
Vou levantar a minha vela
      Am            Dm      G7
Pra poder partir com ela
                  C
Continuar esse romance
     Am      Dm   G7
Em pleno alto mar
                       C
Nos dois juntos nessa jornada
        Am         Dm
O vento leva ao horizonte
     G7             C
Não sei onde vou chegar

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
