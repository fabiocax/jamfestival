Harmonia do Samba - Daquele Jeito

   E                  C#m              F#m         B
Eu quero te botar no colo, te fazer carinho, te dar cafuné
E                  C#m              F#m         B
Provar que te amo, te adoro, vim pra ser seu homem e você minha mulher
 C#m                    G#m
Deixa eu te amar neném, te amar neném
F#m            B
Te amar neném, te amar neném
E                  C#m              F#m         B
Você é tudo que eu preciso, amor da minha vida não pode faltar
E                  C#m              F#m         B
Peguei a estrela mais linda que existe no mundo e guardei pra te dar
C#m                    G#m
Deixa eu te amar neném, te amar neném
F#m            B
Te amar neném, te amar neném

E                  C#m              F#m         B
Daquele jeito, me abraça, me beija, me arranha, me chama de seu preto
E                  C#m              F#m         B
Daquele jeito, te pego, te amasso e te mostro porque eu sou do gueto

( B Cm C#m )     C#m                           G#m
Daquele jeito, carinho e respeito, meu dengo é amor que eu quero dar
 F#m                                     B
Daquele jeito, O mundo inteiro já sabe, nasci pra te amar.(2x)

   E                  C#m              F#m         B
Eu quero te botar no colo, te fazer carinho, te dar cafuné
E                  C#m              F#m         B
Provar que te amo, te adoro, vim pra ser seu homem e você minha mulher
 C#m                    G#m
Deixa eu te amar neném, te amar neném
F#m            B
Te amar neném, te amar neném
E                  C#m              F#m         B
Você é tudo que eu preciso, amor da minha vida não pode faltar
E                  C#m              F#m         B
Peguei a estrela mais linda que existe no mundo e guardei pra te dar
C#m                    G#m
Deixa eu te amar neném, te amar neném
F#m            B
Te amar neném, te amar neném

E                  C#m              F#m         B
Daquele jeito, me abraça, me beija, me arranha, me chama de seu preto
E                  C#m              F#m         B
Daquele jeito, te pego, te amasso e te mostro porque eu sou do gueto
( B Cm C#m )     C#m                           G#m
Daquele jeito, carinho e respeito, meu dengo é amor que eu quero dar
 F#m                                     B
Daquele jeito, O mundo inteiro já sabe, nasci pra te amar.(2x)

----------------- Acordes -----------------
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
Cm = X 3 5 5 4 3
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
