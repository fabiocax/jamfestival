Harmonia do Samba - Desafio

[solo]

E|--------------------------------|
B|--------------------------------|
G|--------4-4----4-4----55----55--|
D|------5----5-5----5-4----44----4|
A|--------------------------------|
E|--------------------------------|

 G        E7        Am
Se você aceitar o desafio
          D7                        G
Eu vou cantar um samba pra você dançar
              E7       Am
Esse swing provoca arrepio
             D7                 G
Pegue o cavaco o show vai começar

               Em     Bm      C   D    G
Vou pedir pra você vibrarr (vibrar, vibrar)
G              Em     Bm
Vou pedir pra você mandar

 Am     D7           G
Mandar meu cavaco chorar
                           G D7
"Mandei meu cavaco chorar"

[solo]

E|--------------------------------------|
B|--------------------------------------|
G|-----------4-----4-4------------------|
D|-------------5-5-----5-5-4------------|
A|--------------------------------------|
E|--------------------------------------|

G      Em         Bm
E a galera não vacilou
 C              G    Em
Fez a poeira subir
               Am
Sem sair do compasso
         D7            G
Mais uma vez eu quero ouvir
   G                 D7
"Mandei meu cavaco chorar"

[solo]

E|--------------------------------|
B|--------------------------------|
G|--------4-4----4-4----55----55--|
D|------5----5-5----5-4----44----4|
A|--------------------------------|
E|--------------------------------|

 G        E7        Am
Se você aceitar o desafio
          D7                        G
Eu vou cantar um samba pra você dançar
              E7       Am
Esse swing provoca arrepio
             D7                 G
Pegue o cavaco o show vai começar

               Em     Bm      C   D    G
Vou pedir pra você vibrarr (vibrar, vibrar)
G              Em     Bm
Vou pedir pra você mandar
 Am     D7           G
Mandar meu cavaco chorar
                           G D7
"Mandei meu cavaco chorar"

[solo]

E|--------------------------------------|
B|--------------------------------------|
G|-----------4-----4-4------------------|
D|-------------5-5-----5-5-4------------|
A|--------------------------------------|
E|--------------------------------------|

G      Em         Bm
E a galera não vacilou
 C              G    Em
Fez a poeira subir
               Am
Sem sair do compasso
         D7            G
Mais uma vez eu quero ouvir
   G                 D7
"Mandei meu cavaco chorar"

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
