Harmonia do Samba - Batida de Rua

(F  C)
Eu no meio da praça
Fazendo pirraça
Liberando emoção
Vou seguindo com fé
Com deus no meu coração
O povo está todo lá
                         Dm
Esperando o harmonia passar
    Am
É fiel em cada rosto
                Dm
Uma grande emoção
                Am
Todo mundo cantando,
                                 Bb
E batendo bem forte na palma da mão
               Am
Prepare seu corpo
                     Gm
Esse som vai levar você

C
Deixa tudo acontecer

(F  C)
Vou subir vou descer
Eu não quero nem saber
Tô que tô colado aqui
Tô que tô colado

Sinta a batida de rua
Que leva a galera
Ta no sangue na veia
A menina libera
A moçada dançando
No maior astral
Esse groove é gostoso
Esse groove é legal

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
