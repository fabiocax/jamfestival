Harmonia do Samba - Batom Vermelho

Intro: Bm7 - E7 - C#m7 - F#7 - Bm - C#m - D7

A       A#                        A
Preciso tanto do seu colo seu calor
          A#                         A
A noite é nossa então vamos fazer amor
A  D7+     Bm      C#m7
E  no  teu corpo viajar
A  D7+     Bm      C#m7
A         A#                          A
São essas curvas que me levam lá no céu
       A#                       A
Da sua boca sentir o sabor do mel
A  D7+     Bm         C#m7
E  no teu  corpo me afogar
A  D7+     Bm      C#m7

D                          E7
Ao acordar eu vejo no espelho
       C#m7                      F#7
Uma mensagem linda de batom vermelho

D                E7       C#m7        F#7
Como se fosse um sonho um passe de mágica
D                             E7
E no lençol ainda sinto o cheiro
         C#m7                            F#7
Do nosso amor que foi tão lindo e verdadeiro
  D            E7              A
Apenas uma noite pra se apaixonar

         D
|Agora a saudade bateu
|            E7
|No peito um vazio doeu

 |              C#m7
|Eu não sei se vou agüentar

 |  F#7
|Esta situação

 |          D
|O destino há de querer
|               E7
|Que um dia reencontre você

 |        C#m7
|E então ira perceber

 |                F#7
|Que nascemos um pro outro

Introdução

*fica mais bonito o refrão se tocado com uma batida de mais swing

----------------- Acordes -----------------
A = X 0 2 2 2 0
A# = X 1 3 3 3 1
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
D7 = X X 0 2 1 2
D7+ = X X 0 2 2 2
E7 = 0 2 2 1 3 0
F#7 = 2 4 2 3 2 2
