Netinho - Onde Você Se Esconde

Intro: A         D

A
Onde você se esconde

Aonde te encontrar
D
Em um lugar bem longe

Em minha cabeça

A
Onde você se esconde

Que nome você tem
D
Se você mora em Londres

Se você tem alguém

C#m                  F#m
Meu amor me dê um sinal

C#m                  F#m
Dê um toque paranormal
B                  B7
Quero toda sua alegria
E     F#m7   Gº     E/G#
Dias, dias, dias e dias

A
Será que é um sonho

Será que é real
D
E vai marcar prá sempre

A minha vida

A
Será que é do bem

Será que é cruel
D
Um anjo descuidado

Que vai cair do céu

C#m                  F#m
Meu amor me dê um sinal
C#m                  F#m
Dê um toque paranormal
B                  B7
Quero toda sua alegria
E     F#m7   Gº     E/G#
Dias, dias, dias e dias

D        Dm         A
Eu quero encontrar você
D         Dm
Pra gente namorar
E F#m7    Gº E/G#
A vida inteira
D        Dm         A
Eu quero namorar você
D          Dm
Pra gente se encontrar
E F#m7    Gº E/G#
A vida inteira

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
Gº = 3 X 2 3 2 X
