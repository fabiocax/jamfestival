Netinho - Extrapolou

C/D     G
Extrapolou
         D               Em
Errou na dose, na dose do amor
          Bm              C
Vento que leva, dessa vez deixou
           G            Am
Um grande amor não pode acabar
           D  C/D
Um grande amor

G    D    Am  C       G
   Ê ê ô ô ô ô  xálalala
G    D    Am  C
   Ê ê ô ô ô ô

G                     D
Mesmo que chova ou faça sol
             Am  C
Eu quero ter você  ô ô ô
G               D
Não fico mais nem um minuto

              Am C
Meu bem sem te ver  ô ô ô
G                  D
E se meu barco naufragasse
           Am C
No meio do mar  ô ô ô
G           D
Seria um peixinho meu amor
           Am  C
Prá te encontrar

Am                  D
Faço qualquer loucura eu me arrisco
Bm                   Em
Te dava o céu só prá ficar contigo
Am             C                        D
Mudava até meu jeito de falar com você
Am                       D
Um beijo é pouco eu quero mais que isso
Bm                     Em
É por inteiro o amor que eu preciso
Am              C                   D C/D
Desculpe se exagerei mas amo você

        G
Extrapolou
         D              Em
Errou na dose, na dose do amor
          Bm              C
Vento que leva, dessa vez deixou
           G            Am
Um grande amor não pode acabar
           D  C/D
Um grande amor

G    D    Am  C       G
   Ê ê ô ô ô ô  xálalala
G    D    Am  C
   Ê ê ô ô ô ô

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C/D = X X 0 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
