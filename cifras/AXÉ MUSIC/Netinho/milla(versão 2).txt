Netinho - Milla

   A     B              G#m                  C#m
Oh Mi....la , mil e uma noites de amor com voc�,
              A                 B
Na praia, no barco, no farol apagado,
                  G#m                       C#m
Num moinho abandonado, em mar grande alto astral,
             A                 B
L� em Hollywood pra de tudo rolar,
                 G#m                  C#m
Vendo estrela caindo, vendo a noite passar,
        A       B              A  B
Eu e voc�..ee..ee, na ilha do sol
             E    B
Na ilha do sol
E            A    B           E
Tudo come�ou    h� um tempo atr�s
            A    B
Na ilha do sol
E                       A    B           E      A  B   
O destino te mandou de volta para o meu cais
        G#m          A              E    
No cora��o ficou lembran�as de n�s dois,
        B           E              
Como ferida aberta, como tatuagem

   A     B              G#m                  C#m
Oh Mi....la , mil e uma noites de amor com voc�,
              A                 B
Na praia, no barco, no farol apagado,
                  G#m                       C#m
Num moinho abandonado, em mar grande alto astral,
             A                 B
L� em Hollywood pra de tudo rolar,
                 G#m                  C#m
Vendo estrela caindo, vendo a noite passar,
        A       B              A  B
Eu e voc�..ee..ee, na ilha do sol
             E    B
Na ilha do sol

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
D# = X 6 5 3 4 3
E = 0 2 2 1 0 0
G#m = 4 6 6 4 4 4
