Netinho - Pra Sempre Eu Vou Te Amar

Intro: Cm  F  Cm  F

Gm                  Am
Quanto tempo longe de você
    Bb                Cm
A paixão ainda está em mim
         Eb          F
O que eu quero agora é vencer
Dm              Gm
O medo de te perder
Cm                   Eb           F
Apagar as mágoas e os erros do passado
Gm               Am
Você me ensinou o dom de amar
        Bb         Cm
Me acolheu me fez flutuar
        Eb         F
Quero ouvir você dizer pra mim
Dm         Gm
Diga me esquecer
         Cm            Eb             F F7
Que me importa agora é só você entender


                 Bb                 C
Então me diga agora, pra sempre eu vou te amar
             Gm           Eb        Cm             F
E grite bem alto, que me ama, que deseja e que me quer
 F7            Bb                   C
Então me diga agora, pra sempre eu vou te amar
            Gm           Eb       Cm               F
E grite bem alto, que me ama, que deseja e que me quer
ouuuu ouuu ououoooo

    Gm                 Am
Eu disse pra mim mesmo hoje eu sei
    Bb           Cm
Pra sempre te amarei
     Eb            F
Se adormeço sonho com você
  Dm          Gm
Delírios de  prazer
   Cm                  Eb          F    F7
Saudade que devora, me faz enlouquecer
                 Bb                 C
Então me diga agora, pra sempre eu vou te amar
             Gm           Eb        Cm             F
E grite bem alto, que me ama, que deseja e que me quer
 F7            Bb                   C
Então me diga agora, pra sempre eu vou te amar
            Gm           Eb       Cm               F
E grite bem alto, que me ama, que deseja e que me quer...

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
Gm = 3 5 5 3 3 3
