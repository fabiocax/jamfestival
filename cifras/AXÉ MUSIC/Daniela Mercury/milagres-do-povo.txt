﻿Daniela Mercury - Milagres do Povo

        F          Cm
Quem é ateu e viu milagres como eu
              Gm
Sabe que os deuses sem Deus
      Dm                    D#7+
Não cessam de brotar, nem cansam de esperar
      F7             Cm
E o coração que é soberano e que é senhor
     Gm                      Dm
Não cabe na escravidão, não cabe no seu não
     D#7+
Não cabe em si de tanto sim
   F7                             Cm7       F7
É pura dança e sexo e glória, e paira para além da história
 G  C     G     F7+
Ojuobá ia lá e via

G   C   G
Ojuobahia
         C      G       F7+
Xangô manda chamar Obatalá guia
G       C      G           F7+/A
Mamãe Oxum chora lagrimalegria
 G        C        G       F7+/A
Pétalas de Iemanjá Iansã-Oiá ia
G   C     G     F7+/A
Ojuobá ia lá e via
G   C   G
Ojuobahia
Gm
Obá
      F                  Cm
É no xaréu que brilha a prata luz do céu
           Gm                   Dm
E o povo negro entendeu que o grande vencedor
    D#7+
Se ergue além da dor
       F7         Cm
Tudo chegou sobrevivente num navio
        Gm
Quem descobriu o Brasil?
        Dm                D#7+
Foi o negro que viu a crueldade bem de frente
    F7                      Cm7               F7
E ainda produziu milagres de fé no extremo ocidente
G   C     G     F7+
Ojuobá ia lá e via
G   C   G
Ojuobahia

----------------- Acordes -----------------
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
Cm7 = X 3 5 3 4 3
D#7+ = X X 1 3 3 3
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
F7+ = 1 X 2 2 1 X
F7+/A = 5 X 3 5 5 X
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
