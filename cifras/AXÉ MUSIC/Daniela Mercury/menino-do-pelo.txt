Daniela Mercury - Menino do Pelô


(Am G)
Todo menino do Pelô sabe tocar tambor
Todo menino do Pelô sabe tocar tambor
F                                      Em
Sabe tocar, sabe tocar, sabe tocar tambor
F                                      Em
Sabe tocar, sabe tocar, sabe tocar tambor
         (Am G)
Eu quero ver
O menino subindo a ladeira, o menino subindo a ladeira
        Dm
Sem violência
                  Bm5-/7
Com toda a "malemolência"
   E7           Am                  E7
Fazendo bumba bumba, fazendo bumba, bumba
         (Am G)
Eu quero ver, eu quero ver
O menino subindo a ladeira, o menino subindo a ladeira
        Dm
Sem violência

                Bm5-/7
Com toda "malemolência"
   E7            Am                 E7 (Am G)
Fazendo bumba bumba, fazendo bumba bumba

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm5-/7 = X 2 3 2 3 X
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
