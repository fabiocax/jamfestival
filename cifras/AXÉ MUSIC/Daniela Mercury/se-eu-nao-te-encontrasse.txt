Daniela Mercury - Se Eu Não Te Encontrasse

(intro) C Em F D G Em7

G                  Em7    G            Em7
Se eu não te encontrasse, nem sentisse esse amor
C9         Am7       C9                   D
eu jamais iria supor que a vida é o maior bem...
G               Em7   G              Em7
Se eu jamais te visse desde o dia em que eu vi,
Am7       C        Em7        G/D     C
não veria que você é o que faltava em mim.
Am6+          Am Am9 Am/D Am6+        Am Am9 Am/D
Onde há tanta faaalsidade e terror no aaaar,
Bm    B/Eb  Em   Em/D  C9          D
eu só vejo  a verdade  ao olhar no teu olhar.

  G         Em7
E quero agradecer...
G             G/B    C
Tantas coisas me fez ver.
Am     G/B  C             D     C         Eb F
E o destino quis que eu te encontrasse...


   Bb             Gm7     Bb           Gm7
Se eu não te encontrasse, nem sentisse esse amor
Eb9       Cm7        Eb9                  F
eu jamais iria supor que a vida é o maior bem...

Bb       Gm7
E quero agradecer...
Bb            Bb/D   Eb
Tantas coisas me fez ver.
Cm     Bb/D Eb          F       Eb
E o destino quis que eu te encontrasse...

Eb
O nosso amor tem grande emoção
Bb/D
e nos alegra o coração.
Gm              F     Eb         (Gm/Eb)    Gm       F         Eb
O ódio e o medo têm poder destruidor, só conduzem a inútil agressão,
    Cm         Bb/D          Eb    F G
Mas ouça o coração que tem razão.

C                 Am7     C            Am7
Se eu não te encontrasse, nem sentisse esse amor
F9         D7        F9                   G
eu jamais iria supor que a vida é o maior bem...

F
O nosso amor tem grande emoção
Em7
e nos alegra o coração.
F
O nosso amor tem grande emoção
Em7                   Am   Am/G
E o medo só causa destruição,
    Dm         Em7           F      G  G#
Mas ouça o coração que tem razão.

  C#        Bbm7
E quero agradecer...
C#            C#/F   F#
Tantas coisas me fez ver.
Ebm   C#/F F#  Ebm       C#/F# F#
ouça o coração que tem a ra----zão.
Bbm7   C#/G# F#          G#          C# Bbm7  F# G# C#
E o destino  quis que eu te encontrasse...

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am/D = X X 0 2 1 0
Am/G = 3 X 2 2 1 X
Am6+ = X 0 2 0 1 0
Am7 = X 0 2 0 1 0
Am9 = X 0 2 4 1 0
B/Eb = X 6 X 4 7 7
Bb = X 1 3 3 3 1
Bb/D = X 5 X 3 6 6
Bbm7 = X 1 3 1 2 1
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C# = X 4 6 6 6 4
C#/F = X 8 X 6 9 9
C#/F# = X X 4 6 6 4
C#/G# = 4 X 3 1 2 X
C9 = X 3 5 5 3 3
Cm = X 3 5 5 4 3
Cm7 = X 3 5 3 4 3
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
Eb9 = X 6 8 8 6 6
Ebm = X X 1 3 4 2
Em = 0 2 2 0 0 0
Em/D = X X 0 4 5 3
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
F9 = 1 3 5 2 1 1
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
G/B = X 2 0 0 3 3
G/D = X 5 5 4 3 X
Gm = 3 5 5 3 3 3
Gm/Eb = 3 6 5 3 3 3
Gm7 = 3 X 3 3 3 X
