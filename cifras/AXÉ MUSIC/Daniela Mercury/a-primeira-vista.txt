Daniela Mercury - À primeira vista

 C               G/B       Am
Quando não tinha nada eu quis
                  Am/G         F
Quanto tudo era ausência, esperei
             C        D7
Quando tive frio, tremi
                        G     (G E4 G)
Quando tive coragem, liguei

C               G/B     Am
Quando chegou carta, abri
            Am/G         F
Quando ouvi Prince, dancei
                 C         D7
Quando o olho brilhou, entendi
                     G
Quando criei asas, voei    (G E4 G)

 C             G/B      Am
Quando me chamou, eu vim
               Am/G           F
Quando dei por mim, estava aqui

            C           D7
Quando lhe achei, me perdi
             G           C     (G E4 G)
Quando vi você, me apaixonei

C
Ôooh
       Am     Am/G
Amara, dzaia, zoi, ei
  F          D7
Dzaia, dzaia, ain, in, in, ingá
         G
Num, man an

( G  E4  G )

( C  G/B  Am  Am/G  F  C  D7  G )

( G  E4  G) 2x

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am/G = 3 X 2 2 1 X
C = X 3 2 0 1 0
D7 = X X 0 2 1 2
E4 = 0 2 2 2 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
