Saulo Fernandes - Vida Labirinto

A
Hoje eu só quero um abraço
                     G
E aquele sorriso imenso
                  A
Pra poder seguir viagem
             Bm
Sem perder a fé
Bm7
Levar esse lugar
                Bm5+
Pra onde Deus quiser
Dm7
O sonho acalentar
     Em7
Vida agradecer
Bm7
Massa que eu tenho você
              Am7
Nas horas sem fim
A
E quando a vida labirinto

Bm7
Me tira a paz
Gm                   Bm7
No meio do sonho... tinha um ser humano
Gm
Mas, louco é o amor
Bm7
A confusão das coisas sábias
A       Gm         Bm7
E eu sei do nosso bem
A
Porque eu estou sempre aqui
G
Porque sei que estás também
A
Porque eu estou sempre aqui
G
Como sei que estás também

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am7 = X 0 2 0 1 0
Bm = X 2 4 4 3 2
Bm5+ = 7 10 9 7 8 7
Bm7 = X 2 4 2 3 2
Dm7 = X 5 7 5 6 5
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
