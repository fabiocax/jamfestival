Saulo Fernandes - Leve-me Ao Mar

E|-------------------------------------------------------------------------------------------|
B|----------2------------------------2-----------------------2-------------------------------|
G|----------2------------------------2-----------------------2-------------------------------|
D|-------2-----2------------------2-------2----------------2-----2---------------------------|
A|----0-----------0---2---4------------------------------0-----------0----2u--4--------------|
E|------------------------------2----------2-----0----2--------------------------2----0---2--|

Lá lá lá iá, Lá lá iá...
Não me leve a mal
Leve-me ao mar
Leva-me à sorte
Não diga que é fardo

E
Fuja comigo
F°
Pra algum silêncio
F#m
Me leve pra onde é deserto em mim
D7M
Onde me esconde um segredo

E                        A A4
Livra-me do que pode ser medo
Em7/G         F#m7
Me abrace a alma
F7M   E
Não me diga só como fazer
Em7/G           F#m7
Me ajude a aprender
F7M
Me leve pra viver

tudo que o for
Lá lá lá iá, Lá lá iá...

D7M         C#m7
Faça amor comigo
      Bm7      F°
Até a última canção
F#m
Não diga que é pecado
Me leve pra ser livre
D7M       C#m7
Lava-me da lágrima
 Bm7    E          A
Que vier do coração

Lá lá lá iá, Lá lá iá...

----------------- Acordes -----------------
A = X 0 2 2 2 0
A4 = X 0 2 2 3 0
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
D7M = X X 0 2 2 2
E = 0 2 2 1 0 0
Em7/G = 3 X 2 4 3 X
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
F7M = 1 X 2 2 1 X
F° = X X 3 4 3 4
