Timbalada - Asanhanshu

[Intro] B  C#m

B            C#m
Obaluaê
E               B
Babalorixa-ê
B                  C#m
Babalorixá, atotô
E               B
Babalorixa-ê

B
Ê Nirê, Nirê
Ê Nirê, Nirê

C#m                  B
Babaolorum xexê salerojá
C#m                  B
Babaolorum xexê salerojá
  C#m           B
Aê nirê, Nirê ô

  C#m           B
Aê nirê, Nirê ô
  C#m           B
Aê nirê, Nirê ô
  C#m           B
Aê nirê, Nirê ô

             C#m                E    B
Meu padrinho é obaluê orixá ê
             C#m                E    B
Meu padrinho é obaluê orixá ê
             C#m                E    B
Meu padrinho é obaluê orixá ê
             C#m                E    B
Meu padrinho é obaluê orixá ê

B
Ê Nirê, Nirê
Ê Nirê, Nirê

C#m                  B
Babaolorum xexê salerojá
C#m                  B
Babaolorum xexê salerojá
  C#m           B
Aê nirê, Nirê ô
  C#m           B
Aê nirê, Nirê ô
  C#m           B
Aê nirê, Nirê ô
  C#m           B
Aê nirê, Nirê ô

B
Babaluaê, Babaluaê, Babaluaê, Babaluaê, Babaluaê

Atotô Babá
Atotô
Azassum
Atotô omolú
Babalodê alorê minazú didê
Olorum modupê
Olorum didê
Kalofé
Kalofé

C#m                  B
Babaolorum xexê salerojá
C#m                  B
Babaolorum xexê salerojá
  C#m           B
Aê nirê, Nirê ô
  C#m           B
Aê nirê, Nirê ô
  C#m           B
Aê nirê, Nirê ô
  C#m           B
Aê nirê, Nirê ô

----------------- Acordes -----------------
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
