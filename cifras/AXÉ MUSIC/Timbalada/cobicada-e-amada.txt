Timbalada - Cobiçada e Amada

G
Encontrei você na rua
          D
Toda arrumada
     C
Pra onde você vai, amor?
G
Vou descendo agora
                        D
Vou pra Timbalada
       C
Vem, timbaleira

Cobiçada e amada
G                       F
Bem maneiro gemer
               Em
Alegria florar
            D
Sou timbaleiro de
         C            G
Batuque, bragada


   Em          D
Ô, ô, ô, lá laia
        C
É tão bonito ver
               G
Timbau rufar
   Em          D
Ô, ô, ô, lá laia
        C
É tão bonito ver
              G
Timbalançar

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
