Timbalada - Tô Solto, Mas Danço Agarrado

       G                   D
Tô solto, mas danço agarrado(3X)
          C            D
Simbora timbaleiro do timbau
       G
Quebrado

  D        Em
Ai, ai, Neguinha
  D     Em
te tenho amor
      Am             C
Vamos namorar
    D    G
Você chegou (2X)

Estava preocupado porque não veio
      G
Me ver
                     Am
Na minha porta não apareceu

C                       D
Poxa, o quê foi que aconteceu?
         Am      D       G
É primavera e nem notícias deu

G
Entrei na onda do carinho e do

Amor
                        Am
Entrei na sala do seu professor
                             D
Cadê o meu metrô, que trem levou?
           Am     D       G
Na plataforma ainda não chegou

          C             G
Esperei você fazer cabelo
         C              G
Esperei você pra me dengar (mãe)

         C               G
Esperei você à noite, inteiro
          C            G
Esperei você me fazer calor
        Am    C
Vamos namorar
         G
Você chegou

          C          G
Esperei você dizer desejos

          C          G
Esperei você pra me abraçar
         C                G
Esperei você como o tempo espera
          C              G
Esperei você o tanto que for
      Am             C
Vamos namorar
         G
Você chegou

    D     Em
Ai, ai, neguinha
   D      Em
Te tenho amor
       Am           C
Vamos namorar
         G
Você chegou

   G
Hoje é pra lhe encher de beijos
 C
Hoje é pra lhe dar amor
      Am       C
Vamos namorar
          G
Você chegou

    D      Em
Ai, ai, neguinha
    D      Em
Te tenho amor
      Am       C
Vamos namorar
         G
Você chegou

       G             D
Tô solto, mas danço agarrado
       G             D
Tô solto, mas danço agarrado
       G             D
Tô solto, mas danço agarrado
             C           D
Simbora timbaleiro do Timbau
       G
Quebrado

G: 330023
D: 2320XX
Em:000220
Am:01220X
C: 01023X



----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
