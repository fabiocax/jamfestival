Timbalada - Mimar Você

A                  C#m7    D                   E
Eu te quero só pra mim, você mora em meu coração
A               C#m7  D                   E
Não me deixe só aqui, esperando mais um verão
D                                       E
Te espero meu bem, pra gente se amar de novo
A  E  E/G# F#m  D                E         A A7
Mimar você,   nas quatro estações relembrar
D                     E       A E E/G#   F#m
O tempo que passamos juntos, bem bom viver
D                                  E
Andar de mãos dadas, na beira da praia
                               A
Por esse momento eu sempre esperei

Repete  do  início

A              C#m7     D               E
Lá lá lá lá lá lá lá lá lá lá lá lá la
A              C#m7     D               E
Lá lá lá lá lá lá lá lá lá lá lá lá lá


Essa musica você toca arpejando.!

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
F#m = 2 4 4 2 2 2
