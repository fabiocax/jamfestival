Jammil e Uma Noites - Surpresa Boa

G            D
Surpresa boa não é o que  agente não espera
Em                     Bm
Surpresa boa é a que a gente não esquece
C                  Am
e você veio assim nem uma dó de mim
D
tomando conta dos meus sentimentos
G                   D
A gente faz o que o coração pede
Em                      Bm
Deus dá pra gente tudo que a gente merece
C                    Am                 D
e Deus te deu pra mim e você veio assim tomando conta dos meus pensamentos
C    D     G              Em
Aliás você roubou minha razão
C           Am    D
cuida bem de mim cuida da gente então oôoô
G              D          Em                  Bm
Tomara que eu te faça bem Tomara que a gente viva em paz
C                G                Am                  D
Tomara que eu te faça feliz porque você me faz feliz demais

G              D          Em                  Bm
lê    lê    lê    lê    lê    lê    lê    lê    lê lê lê
C                G                Am                  D
lê    lê    lê    lê    lê    lê    lê    lê    lê lê lê
(bis)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
