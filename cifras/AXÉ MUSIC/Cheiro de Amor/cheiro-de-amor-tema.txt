Cheiro De Amor - Cheiro de Amor (Tema)

C            G            Am     F,G
Sim, é um jardim "mui" delirante
C             G
Com mil e um lances
Am                F   G
De brilho e de cor

F       E
É a mistura mais pura
F           E                        Dm   G
É a mais pura mistura do Cheiro de Amor

C                   G
Quem, quem sabe um dia
             Am         F G
A gente se encontra lá
C           G
Com amor e prosa
              F      G F#
Nos dias de carnaval


F           E
Sentir a alegria de poder estar
F             E               Dm
Sentindo a magia no meio das rosas
      G,G7
Fantasia

C           G              F
Ô, ô, ô, ô, ô, ô, ô, ô, ô, ô
G           C
Cheiro de Amor!
            G              F   G
Ô, ô, ô, ô, ô, ô, ô, ô, ô, ô

C           G              F
Ô, ô, ô, ô, ô, ô, ô, ô, ô, ô
G           C
Cheiro de Amor!
            G              F   G
Ô, ô, ô, ô, ô, ô, ô, ô, ô, ô

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
