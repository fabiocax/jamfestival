Cheiro De Amor - Chama da Paixão

(intro) D A Bm G D Gm A7

          D
E você foi
                                      Bm
O meu primeiro, meu segundo ou meu terceiro amor
                                 G
Talvez o último, penúltimo que conquistou
                         A
E acendeu a chama dessa paixão
       D
Me fez feliz
                                   Bm
Por muito e muito tempo o bastante que durou
                                  G
Mas veio a chuva e essa chama logo se apagou
                           A  ( Ab )
Derretendo a chama no meu coração
G      A/G           G      A/G
Voa, gira girassol, tempo voa vai buscar
     Bm       A/B         Bm           A/B
E assim seu coração, bateu por outra pessoa

G
Voa...
D     A  G          Gm7            D
Bye bye adeus, meu coração você perdeu
      A  G          Gm7                       D
Bye bye e agora, ainda te quero mas to indo embora

Bye...

(solo) D   A G   Gm   D   A   Gm

           D                                 Bm
Pode dizer que tudo que fiz por você foi uma grande ilusão
                                G
Mas era só abrir a porta do meu coração
                                  A    ( Ab )
foi então que resolveu me abandonar, (Lai á)

Voa...
Bye...

(solo)

         D            Bm
Xalalala xalalalalalalalala
                   G
Xalalalalalalalala
                 A
Xalalalalalalalalalá

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/B = X 2 2 2 2 X
A/G = 3 X 2 2 2 X
A7 = X 0 2 0 2 0
Ab = 4 3 1 1 1 4
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
