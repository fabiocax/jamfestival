Psirico - Tem Xenhenhém

Gm7                 D#9
A minha balada só acaba
                     Bb                      F
Quando a cachaça termina, quando o dj vai embora
                        Gm7
Quando eu arrasto uma mina
Gm     Bb   F/A    Gm
Se prepara pra arrochar!

( Gm7  D#  Bb  F/A )

( Gm7  D#  Bb  F7 )

Gm7                 D#9
A minha balada só acaba
                     Bb                      F
Quando a cachaça termina, quando o dj vai embora
                        Gm7   D/A
Quando eu arrasto uma mina

Gm7              D
E depois, xenhenhém

Gm7              D
E depois, xenhenhém (2x)

Gm7                     D#9
Vem minha galera, minha moçada
                  Bb              F/A
Minha turma, meu povão ( Xenhenhenhenhem)
Gm7                      D#9                   Bb         F/A
Tem mulher boa, gente a toa, coisa linda no salão  (thuruthuthuthu)
 Gm7                      D#
Bota, cinto e chapéu essa moda é arrochaneja
Bb                          D#                D/A
Eu vou perguntar,levante a mão, quem quer cerveja

Gm7              D
E depois, xenhenhém
Gm              D
E depois, xenhenhém (2x)

( Gm7  D#  Bb  F/A )

( Gm7  D#  Bb  F7 )

Gm7                 D#9
A minha balada só acaba
                     Bb                      F
Quando a cachaça termina, quando o dj vai embora
                        Gm7   D/A
Quando eu arrasto uma mina

Gm7              D
E depois, xenhenhém
Gm7              D
E depois, xenhenhém (2x)

Gm7                     D#9
Vem minha galera, minha moçada
                  Bb              F/A
Minha turma, meu povão ( Xenhenhenhenhem)
Gm7                      D#9                   Bb         F/A
Tem mulher boa, gente a toa, coisa linda no salão  (thuruthuthuthu)
 Gm7                      D#
Bota, cinto e chapéu essa moda é arrochaneja
Bb                          D#                D/A
Eu vou perguntar,levante a mão, quem quer cerveja

Gm7              D
E depois, xenhenhém
Gm7              D
E depois, xenhenhém (2x)

Gm7            D#
Se me traz o amor, mexe comigo
  Bb                            F/A
Agora vem me aquecer, estava aflito
Gm7            D#                     Cm
Se me traz o amor, nada mais vai me faltar
                  Dm     D7
Agora é só comemorar     uhhhhhh

Gm7              D
E depois, xenhenhém
Gm              D
E depois, xenhenhém (4x)

( Gm7  D#  Bb  F/A )

( Gm7  D#  Bb  F7 )

Gm7     Bb   F/A      G
Se prepara pra arrochar!

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D# = X 6 5 3 4 3
D#9 = X 6 8 8 6 6
D/A = X 0 X 2 3 2
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
F7 = 1 3 1 2 1 1
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
