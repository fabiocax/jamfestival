Psirico - Outras Flores Part. Jorge e Mateus

G       D        Em             C                 D
Outra flores virão outras formas de conquistar você
G        D         Em     C                     D
Se eu pudesse escolher, novamente e de novo eu escolho você


Am                          D
Não me olha assim assim não vale
G           D                C
Eu me perco nesse charme
Seu sorriso é minha risada
Am                          D
Desculpa mas não vamos ter final feliz
G          D             F       D
Por que amor verdadeiro nunca acaba

G                         D
Nossos olhos se beijam na troca de olhar
Em                         C
é tão raro e perfeito esse jeito de amar
 G                     D
Também temos defeitos, mas dá pra levar ´

C      D
é assim

G                     D
Entre brigas e beijos, ciume e carinho
Em                       C
O Amor Tira as pedras do nosso caminho
G                      D
Esse laço entre agente não vai se romper
C
Somos fortes

Am                                    D
Que o nosso amor seja eterno enquanto dure
                  G
E que dure eternamente

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
