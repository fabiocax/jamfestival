Biquini Cavadão - Dançar pra não Dançar


D               D
Dance, dance, dance
G              A
Gaste o tempo comigo
D                Em
Não, não tenha juízo
A               A7
Dê-se ao luxo de estar
       A7
sendo fútil  agora
D              D
Dance, dance, dance
G           A
Faça como Isadora
D           Em
Que ficou na história
A                 A7          Em
Por dançar como bem quisesse Yeah
G            A
Um movimento qualquer

Em                  A
sobe a cabeça e os pés
Em
Sinta o corpo
Em
Você está solta
  Em
E pronta pra vir
D            G
Dance, dance, dance
D             A
Passe as horas comigo
D          Em
Nesse duplo sentido
A             A7
E no barato de ser um ser vivo ainda
D               G
Dance, dance, dance
D              A
Num programa de índio
D             Em
Vá rodar o cachimbo
A
Que é pra paz não
 A7                     Em
dançar na  tribo Yeah
G              A
Um movimento qualquer
Em                  A
Sobe a cabeça e os pés
Em
Sinta o corpo
Em
Você está solto
   Em               D
E pronta pra vir me amar
      D
me amar....

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
