Biquini Cavadão - Carta aos Missionários

Intro: D  A  Em  G

Solo Intro:
|-10-------------------------------|
|----10---8--8--7------------------|
|-----------------7----------------|  (4x)
|----------------------------------|
|----------------------------------|
|----------------------------------|


D                   A
Missionários de um mundo pagão
Em                G
Proliferando ódio e destruição
Bm           G         A
Pelos quatro cantos da terra
Bm           G            A           Bm    G         Bm  G  A
A morte, a discórdia, a ganância, e a guerra   (e a guerra...)
D              A       Em    G
Missionários e missões su_icidas

D           A       Em         G
Crianças matando, crianças inimigas
Bm         G      A    D
Generais de todas as nações
                      A   Bm
Fardas bonitas, condecorações
              G       A     Bm
Documentam a nossa história
                G      A         D      A  Em  G
Com seu rastro sujo de sangue e glória
D  A  Em  G
D                  A
Vindo de todas as partes
  Em              G
E indo pra lugar algum
D                A
Assim caminha a raça humana
Em           G
Se devorando, um a um
  Bm           G   A
Gritei para horizonte
   Bm           G    A
Ele não me respondeu
  Bm          G   A
Então fechei os olhos
    Bm    G        D   A  Em  G   D   A  Em  G
Sua voz assim me bateu
(D C C F C F)  D
    A              Em            G      A   Bm
Na, nana, nana, nana,       oh oh oh oh oh ah...
    G      A    Bm      G     A
Na, nana, nana, nana, na nana nana nana nana...
D  A  Em  G      D   A  Em  G   D

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
