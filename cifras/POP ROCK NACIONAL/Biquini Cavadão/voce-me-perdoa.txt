﻿Biquini Cavadão - Você Me Perdoa?

Intro e Verso: D D7M D D7M G G7M G G7M

Quando eu mentir,
Quando eu fingir,
E te trair com outra pessoa
Você me perdoa?
Quando eu mudar,
E o encanta acabar,
E eu não for mais a mesmas pessoa
Você me perdoa?


                F#m
Por mais que te doa
               Bm
A verdade não cabe no amor
  Em
Falar também não é fácil,
 F#7
Me escuta por favor!?

    Bm             Bm/A
Eu juro, juro pra você
                   G
Que vai ser pra sempre
                 F#7
Até o amanhecer,
 Bm             Bm/A
Juro, juro pra você
                 G
Que vai ser pra sempre,
         F#7           Em
Mas só até o amanhecer

Se tirei teu chão,
Implorei perdão,
E tudo foi atoa.
Você me perdoa?
Se deixei nós dois sempre pra depois,
Perceber que o tempo voa.
Você me perdoa?

Por mais que te doa,
A verdade não cabe no amor
Falar também não é fácil,
Me escuta por favor?!

Eu juro, juro pra você,
Que vai ser pra sempre,
Até o amanhecer.

 Bm             Bm/A
Juro, juro pra você
                 G
Que vai ser pra sempre,
         F#7
Mas só até o amanhecer
  Em
Me perdoa
Até o amanhecer
G
Me perdoa

Intro

Por mais que te doa,
A verdade não cabe no amor
Falar também não é fácil
Me escuta por favor

    Bm             Bm/A
Eu juro, juro pra você
                   G
Que vai ser pra sempre
                 F#7
Até o amanhecer,
 Bm             Bm/A
Juro, juro pra você
                 G
Que vai ser pra sempre,
         F#7
Mas só até o amanhecer

Bm
Me perdoa
            Bm/A
Até o amanhecer
G
Me perdoa
               F#7
Até o amanhecer

 Bm             Bm/A
Juro, juro pra você
                 G
Que vai ser pra sempre,
                      F#7
Mas só até o amanhecer
Em
Me perdoa
Até o amanhecer
G                     Bm
Vai me perdoar.

----------------- Acordes -----------------
Bm = X 2 4 4 3 2
Bm/A = X 0 4 4 3 X
D = X X 0 2 3 2
D7M = X X 0 2 2 2
Em = 0 2 2 0 0 0
F#7 = 2 4 2 3 2 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G7M = 3 X 4 4 3 X
