Biquini Cavadão - Timidez

E               A
Toda vez que te olho,
          E
Crio um romance
            A
Te persigo, mudo
         E
todos instantes

Falo pouco pois não
A               E
sou de dar indiretas
                    A
Me arrependo do que digo
            E
em frases incertas
                  A                B9
Se eu tento ser direto, o medo me ataca
                 A9
sem poder nada fazer
                    B9                   A9
Sei que tento me vencer e acabar com a mudez

                       B9
Quando eu chego perto, tudo esqueço
            A9
e não tenho vez
                  B9                  A9
Me consolo, foi errado o momento, talvez,
                       B9                   C#m
Mas na verdade, nada esconde essa minha timidez
             B9               A9
Eu carrego comigo a grande agonia
               B9               C#m
De pensar em você, toda hora do dia
             B9                 A9
Eu carrego comigo, a grande agonia
                   B9                  C#m  B9 A9
Na verdade nada esconde essa minha timidez
                  B9                   C#m
Na verdade nada esconde essa minha timidez

( B9 A9 B9 A9 E F Am F Am )

F              Am
Talvez escreva um poema
F             Am
No qual grite o seu nome
F          Am
Nem sei se vale a pena
F         Am
Talvez só telefone
F             Am
Eu me ensaio, mas nada sai
F                  Am
O seu rosto me distrai
           B9
E, como um raio,
                   A9
eu encubro , eu disfarço ,
                  B9
eu camuflo, eu desfaço
                A9
Eu respiro bem fundo,
                  B9
hoje eu digo pro mundo
               A9
Mudei rosto e imagem,
               B9
mas você me sorriu,
                   A9
Lá se foi minha coragem,
            B9    A     E
você me inibiu...ooo

                    B9                   A9
Sei que tento me vencer e acabar com a mudez
                       B9
Quando eu chego perto, tudo esqueço
            A9
e não tenho vez
                  B9                  A9
Me consolo, foi errado o momento, talvez,
                       B9                   C#m
Mas na verdade, nada esconde essa minha timidez
             B9               A9
Eu carrego comigo a grande agonia
               B9               C#m
De pensar em você, toda hora do dia
             B9                 A9
Eu carrego comigo, a grande agonia
                   B9                  C#m  B9 A9
Na verdade nada esconde essa minha timidez
                  B9                   C#m  B9 A9
Na verdade nada esconde essa minha timidez
                   B9                  C#m
Na verdade nada esconde essa minha timidez
                  B9                   C#m
Na verdade nada esconde essa minha timidez

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
Am = X 0 2 2 1 0
B9 = X 2 4 4 2 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
