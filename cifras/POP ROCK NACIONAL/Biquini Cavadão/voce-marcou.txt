Biquini Cavadão - Você Marcou

B
Você marcou
     E
Não foi
B
Você sorriu
    E
Mentiu

C#m
 E agora que é bem tarde
F#
 O show já terminou
C#m
 Mas nunca é tão tarde
F#
 Pra onde você foi?
B
Você falou
    E
Magoou

B
E aquela do Cazuza
              E
Nunca mais tocou

C#m
E agora que é bem tarde
F#
O show já terminou
C#m
Mas nunca é tão tarde
F#
Pra onde você foi?

 B
Foram-se as flores, as cores
                 E
Das tardes, das ruas
Beijos pelas ruas
 B                                 E
Lembro das datas, das cartas e as marcas
Que um dia você me deixou
C#m
 Mas quando é bem tarde
F#
 O show já terminou
C#m
 Mas nunca é tão tarde
F#                  Solo: B E B E
 Pra onde você foi?

C#m
 Mas quando é bem tarde
F#
 O show já terminou
C#m
 Mas nunca será tarde
F#
 Pra onde você foi?

 B
Foram-se as flores, as cores
                 E
Das tardes, das ruas
Beijos pelas ruas
 B                                 E
Lembro das datas, das cartas e as marcas
Que um dia você me deixou
C#m  F#                 B  F# G#m
        O show já terminou
      E    B           C#m
        O show já terminou
B F# G#m                E B C#m
        O show já terminou
      F#                B
        O show já terminou

----------------- Acordes -----------------
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
