Biquini Cavadão - Vital e Sua Moto

  G              Am                   Bm            Am
 Vital andava a pé e achava que assim estava mal
  G               Am                    Bm          Am
 De um ônibus pra outro aquilo para ele era o fim
C                 D                       Em
 Conselho de seu pai: "Motocicleta é perigoso, Vital.
C             D                            Em          D
 É duro de negar, filho, mas isto dói bem mais em mim"

G                     Am                 Bm        Am
 Mas vital comprou a moto e passou a sentir total
G             Am          Bm          Am
 Vital e sua moto mas que união feliz
C             D           Em
 Corria e viajava era sensacional
C                D                      Em     D
 A vida em duas rodas era tudo que ele sempre quis

 C                    D
Vital passou a se sentir total
 C                  D
Com seu sonho de metal

 C                    D
Vital passou a se sentir total
 C                 D
Com seu sonho de metal

 G                  Am                        Bm               Am
 Os Paralamas do Sucesso iam tentar tocar na capital, na capital
 G                 Am                  Bm        Am
 E o Biquini Cavadão pra lá então se encaminhou
C                 D                     Em
 Ele foi com sua moto, ir de carro era baixo astral
C                     D                  Em           D
 Minha prima já está lá e é por isso que eu também vou

( G  Am  Bm  Am )

 G                  Am                        Bm               Am
 Os Paralamas do Sucesso vão tentar tocar na capital, na capital
 G             Am          Bm        Am
Vital e sua moto, mas que união feliz
G                  Am                        Bm      Am
 Os Paralamas do Sucesso vão tentar tocar na capital
 G             Am          Bm        Am
Vital e sua moto, mas que união feliz

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
