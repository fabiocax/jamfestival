Biquini Cavadão - Soltos Pelo Ar

D
Soltos pelo ar
                     Bm
Vestidos do lado avesso

Não há mais o que falar
                       G
Entre nós não há segredos

Soltos pelo ar
                 D
Soltos pelo ar

D
Tenho meu olhar e minhas visões
                            Bm
E sei reparar nas suas aflições
               G
Quando seu mundo se desfaz, isso é que é viver
       D
Pode ser que o céu se abra pra você


D
São os seus caminhos, a sua liberdade
                   Bm
Nossa afirmação de cumplicidade
G
Tudo que é demais, deixa pra depois
D
Pode ser que o céu se abra pra nós dois

G
Pode o mundo desabar
B
Deixa a chuva cair
A
Que tudo seja exatamente

Como nós não planejamos

D
Soltos pelo ar
                     Bm
Vestidos do lado avesso

Não há mais o que falar
                       G
Entre nós não há segredos

Soltos pelo ar
                 D
Soltos pelo ar

D
São os seus caminhos, a sua liberdade
                   Bm
Nossa afirmação de cumplicidade
G
Tudo que é demais, deixa pra depois
D
Pode ser que o céu se abra pra nós dois

G
Pode o mundo desabar
Bm
Deixa a chuva cair
A
Que tudo seja exatamente

Como nós não planejamos

D
Soltos pelo ar
                     Bm
Vestidos do lado avesso

Não há mais o que falar
                       G
Entre nós não há segredos

Soltos pelo ar
                 D
Soltos pelo ar

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
D = X X 0 2 3 2
G = 3 2 0 0 0 3
