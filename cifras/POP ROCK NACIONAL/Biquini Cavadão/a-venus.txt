Biquini Cavadão - A Vênus

(Gm,Eb,F,Dm)
Meu cavalo não é alado, minha lança ferro
A armadura sem encanto mais hoje encerro
A dor gritos e ecos, esse povo ainda é meu
Meus homens por amor defendam o que é seu
Se eu não voltar meu sangue volta em praça
Se eu não vencer eu enfraquecerei quem ameaça
A paz, o a amor de quem não sabe guerrear
E eu te invoco princesa que suas lágrimas de adeus
Se tornem bênçãos do luar

(G,D,Am,C)
Você vem ser, na estrada a guia da visão
Você vem ser, a minha luz na escuridão
Você vem ser, no eu a solitária flor
Você vem ser, a Vênus a deusa do amor

(Gm,Eb,F,Dm)
A minha volta caem milhares descansem em paz
Ao som de espadas sobre o sol não sou capaz
De ouvir clamor em piedade sem nada a fazer

Não sou o dono da verdade e nem eu quero ser
Caem os inimigos chorem cada um dos seus
A grande vitória agora é voltar pra casa e ver
Sorrisos lágrimas de festas sete dias sem parar
E quanto a você princesa e suas lágrimas de adeus
Seriam por me ver voltar

(G,D,Am,C)
Você vem ser, na estrada a guia da visão
Você vem ser, a minha luz na escuridão
Você vem ser, no eu a solitária flor
Você vem ser, a Vênus a deusa do amor


(Gm,Dm) (Gm,Eb,F)
Meu povo agora é corpo pelo chão
E os que restaram vagão por ajuda em vão
Desesperado eu procuro por você
Só espero que me escute quando imploro por, por você
Vem ser a deusa do amor

(G,D,Am,C)
Você vem ser, a Vênus a deusa do amor
Você, aonde está você a deusa do amor
Você vem ser, a Vênus a deusa do amor
Você, aonde está você a deusa do amor

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
