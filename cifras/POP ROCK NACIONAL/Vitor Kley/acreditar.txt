Vitor Kley - Acreditar

(B  F#  G#  E)
Minha vida, é mais ou menos assim
Tem um início, tem um meio, mas ainda não sei o meu fim

(B  F#  G#  E)
Só quero ter você
Ao meu lado
Olhando o céu estrelado
Ser sempre assim

(B  F#  G#  E)
O tempo para ao olhar pro seu olhar
Entro no paraíso ao avistar o seu sorriso e eu me lembro da nossa vida
Daquela canção que te fez chorar
E eu me esqueço da nossa história,
Daquele velho conto de ninar que um dia eu quis acreditar

(B  F#  G#  E)
Te levar pro lugar mais bonito
Alcançando as estrelas ou além do infinito eu sei (2x)

----------------- Acordes -----------------
B = X 2 4 4 4 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
G# = 4 3 1 1 1 4
