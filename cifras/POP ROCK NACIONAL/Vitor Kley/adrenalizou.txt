﻿Vitor Kley - Adrenalizou

Capo Casa 2

[Intro] C7M/G  G6(9)  C7M/G  G6(9)

[Primeira Parte]

          C7M/G
Montanha russa, paraquedas
                          G6(9)
Salto na atmosfera, só assim pra te sentir chegar
        C7M/G
Último dia da novela, onda boa que me leva
        G6(9)
Olha você vindo me provocar

   C7M/G
Dona de grandes feitos, acelera os batimentos
     G6(9)
Não sei como vou controlar
            C7M/G
Com você vivo dias intensos, tortos e direitos
   G6(9)             D4(9)/A
Quando paro pra reparar


[Refrão]

         Am7(9)        C7M/G
Adrenalizou o meu coração
          G5                       D4(9)/A
Menina bonita quando eu toquei na tua mão
         Am7(9)        C7M/G
Adrenalizou o meu coração
          G5(9)                    D4(9)/A  Am7(9)  C7M/G
Menina bonita quando eu toquei na tua mão
     G5    D4(9)/A
Na tua mão
        Am7(9)  C7M/G
Na tua mão
     G5    D4(9)/A
Na tua mão

( C7M/G  G6(9) )

[Segunda Parte]

         C7M/G
Olhando filme de terror, roubei um disco voador
     G6(9)
N maneiras de te encontrar
          C7M/G
É bungee jump, barco à vela
                            G6(9)
Ou no olho de tandera, aquarela doce, água do mar

       C7M/G
Dissolvi meus pensamentos em vários pigmentos
 G6(9)
Te convido pra dançar, caminhar
    C7M/G
Na luz da passarela, planar de asa delta
  G6(9)             D4(9)/A
Voar no céu da sua boca

[Refrão]

         Am7(9)        C7M/G
Adrenalizou o meu coração
          G5                       D4(9)/A
Menina bonita quando eu toquei na tua mão
         Am7(9)        C7M/G
Adrenalizou o meu coração
          G5(9)                    D4(9)/A
Menina bonita quando eu toquei na tua mão

         Am7(9)        C7M/G
Adrenalizou o meu coração
          G5                       D4(9)/A
Menina bonita quando eu toquei na tua mão
         Am7(9)        C7M/G
Adrenalizou o meu coração
          G5                       D4(9)/A  Am7(9)  C7M/G
Menina bonita quando eu toquei na tua mão

     G5    D4(9)/A
Na tua mão
        Am7(9)  C7M/G
Na tua mão
     G5    D4(9)/A
Na tua mão

         Am7(9)
Adrenalizou

----------------- Acordes -----------------
Capotraste na 2ª casa
Am7(9)*  = X X 7 5 8 7 - (*Bm7(9) na forma de Am7(9))
C7M/G*  = 3 X 2 4 1 X - (*D7M/A na forma de C7M/G)
D4(9)/A*  = 5 5 4 0 3 0 - (*E4(9)/B na forma de D4(9)/A)
G5*  = 3 5 5 X X X - (*A5 na forma de G5)
G5(9)*  = 3 X 0 2 3 3 - (*A5(9) na forma de G5(9))
G6(9)*  = X X 5 4 5 5 - (*A6(9) na forma de G6(9))
