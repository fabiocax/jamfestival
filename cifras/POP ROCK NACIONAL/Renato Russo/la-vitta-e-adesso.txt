Renato Russo - La Vitta È Adesso

Intro: A9  E/G#  D9  F#m  B7/G#  D9  C#m  Bm  Bm/A  E/G#  E

    A9                                 E/G#
La vita è adesso nel vecchio albergo della terra e ognuno in una
D9                      F#m                 B7/G#    (E)baixo
Stanza e in storia di mattini più leggeri e cieli
  D9                     C#m
Smarginati di speranza e di silenzi da ascoltare
 Bm           Bm/A          E/G#   E
E ti sorprenderai a cantare ma... non sai perché

    A9                          E/G#
La vita è adesso nei pomeriggi appena freschi che ti viene
D9                   F#m              B7/G#(E)baixo
Sonno e le campane girano le nuvole e piove
 D9                       C#m
Sui capelli e sopra i tavolini dei caffè all'aperto
Bm           Bm/A          E/G#
E ti domandi incerto chi sei tu...
 E            B   B/A     B/G#   B/F#
Sei tu...    sei tu...    sei tu...


E9                  A/E         E9                E   ( E D# C# )baixo
Sei tu che spingi avanti il cuore ed il lavoro duro
C#m               A9         B               E
Di essere uomo e non sapere cosa sarà il futuro
      E          A/E                 E                    E   ( E D# C# )baixo
Sei tu nel tempo che ci fa più grandi e soli in mezzo al mondo
C#m         A          B                       E
Con l'ansia di cercare insieme un bene più profondo

C#m                B                 A            C#m
E un altro che ti dia respiro e che si curvi verso te
C#m            B              A                   B
Con una attesa di volersi di più senza capire cos'è
E       B/D#  C#m       C#m      A             B        E  ( E D# C# )baixo
E tu che mi ricambi gli occhi... in questo instante immenso
C#m              A           B                      E
Sopra il rumore della gente dimmi se questo ha un senso

    A9                          E/G#
La vita è adesso nell'aria tenera di un dopocena e musi
D9                          F#m            B7/G#    (E)baixo
Di bambini contro i vetri e i prati che si lisciano
   D9                           C#m
Come gattini e stelle che si appicciano ai lampioni millioni
Bm           Bm/A          E/G#
Mentre ti chiederai dove sei tu
E            B   B/A     B/G#   B/F#
Sei tu...    sei tu...    sei tu...

E               A/E             E                     E   ( E D# C# )baixo
Sei tu che porterai il tuo amore per cento e mille strade
C#m                 A              B                  E
Perchè non c'è mai fine al viaggio anche se un sogno cade
      E                 A/E         E                           E   ( E D# C# )baixo
Sei tu che hai un vento nuovo tra le braccia mentre mi vieni incontro
C#m              A       B                   E
E impanerai che per morire ti basterà un tramonto

C#m           B              A           C#m
In una gioia che fa male di più della malinconia
C#m             B            A                  B
E in qualunque sera ti troverai non ti buttare via
F#m    C#/Eb    D#m      D#m        C#      B   A#m   A#m
E non lasciare andare un giorno... per ritovar te stesso
D#m                 B         C#               C5     B5
Figlio di un cielo così bello perché la vita è adesso...
A#5  G#5   A#5  (B5 C5 C#5)
Adesso...     adesso...

(base do solo) F#  C#  D#m  D#m

----------------- Acordes -----------------
A = X 0 2 2 2 0
A#5 = X 1 3 3 X X
A#m = X 1 3 3 2 1
A/E = 0 X 2 2 2 0
A9 = X 0 2 2 0 0
B = X 2 4 4 4 2
B/A = X 0 4 4 4 X
B/D# = X 6 X 4 7 7
B/F# = X X 4 4 4 2
B/G# = 4 X 4 4 4 X
B5 = X 2 4 4 X X
Bm = X 2 4 4 3 2
Bm/A = X 0 4 4 3 X
C# = X 4 6 6 6 4
C#/Eb = 11 X 11 10 9 X
C#5 = X 4 6 6 X X
C#m = X 4 6 6 5 4
C5 = X 3 5 5 X X
D# = X 6 5 3 4 3
D#m = X X 1 3 4 2
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E9 = 0 2 4 1 0 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
G#5 = 4 6 6 X X X
