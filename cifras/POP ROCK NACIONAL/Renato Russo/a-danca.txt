Renato Russo - A Dança

Introd.: E7

 E7
 Não sei o que é direito, só vejo preconceito
 C
 E a sua roupa nova é só uma roupa nova
 E7
 Você não tem idéias, pra acompanhar a moda
 C
 Tratando as meninas, como se fossem lixo
 E7
 Ou então espécie rara só a você pertence
 C
 Ou então espécie rara que você não respeita
 E7
 Ou então espécie rara que é só um objeto
 C
 Pra usar e jogar fora depois de ter prazer


 C7                  Am
 Você é tão moderno, se acha tão moderno

 Bm                    Cm G
 Mas é igual a seus pais é só questão de idade
 F                    D
 Passando dessa fase tanto fez e tanto faz


 E7
 Você com as suas drogas e as suas teorias
 C
 E a sua rebeldia e a sua solidão
 E7
 Vive com seus excessos mas não tem mais dinheiro
 C
 Pra comprar outra fuga sair de casa então
 E7
 Então é outra festa é outra sexta-feira
 C
 Que se dane o futuro você tem a vida inteira
 E7
 Você é tão esperto você está tão certo
 C
 Mas você nunca dançou com ódio de verdade


 C7                 Am
 Você é tão esperto você está tão certo
 Bm               Cm  G
 Que nunca vai errar, mas a vida deixa marcas
 F             D                 E7
 Tenha cuidado se um dia você dançar
 C7                     Am
 Nós somos tão modernos só não somos sinceros
 Bm                     Cm G
 Nos escondemos mais mais é só questão de idade
 F                    D
 Passando dessa fase tanto fez e tanto faz
 C7                 Am
 Você é tão esperto você está tão certo
 Bm                   Cm
 Que voce nunca vai errar
 G                       F
 Mas a vida deixa marcas tenha cuidado
 D
 Se um dia você dançar

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
