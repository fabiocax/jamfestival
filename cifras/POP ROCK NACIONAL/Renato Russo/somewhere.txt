Renato Russo - Somewhere

Intro: Gm  Eb  Cm  Cm/Bb  Ab6  G7/9

G7        C         Am
There's a place for us
F7+  G6     C         Am
Somewhere a place for us
F7+       G6        C    Am
Peace and quiet and open air
F7+       Bb7+
Wait for us
Gm  Eb    D7  G7/9
Somewhere
G7        C        Am
There's a time for us
F7+  G6   C        Am
Someday a time for us
F7+  G6            C        Am
Time together with time to spare
F7+     Bb7+
Time to learn
Gm      Eb
Time to care

Cm Cm7/Bb Ab6
Someday, somewhere
Gm6              C7/9
We'll find a new way of living
Fm6                  Bb7+   Gm   Eb
We'll find there's a way of forgiving
D7         G7/9
Somewhere
G7        C        Am
There's a time for us
F7+  G6   C        Am
Someday a time for us
F7+    G6          C       Am
Time together with time to spare
F6      Bb7+
Time to learn
Gm      Eb
Time to care
Cm Cm7/Bb Ab6
Someday, somehow
Gm               C7/9
We'll find a new way of living
Fm6                  Bb7+   Gm   Eb
We'll find there's a way of forgiving
D7          G7/9
Somewhere
G7         C        Am
There's a place for us
F7+ G6       C         Am
A time and a place for us
F7+     G6       C              Am
Hold my hand and we're half way there
Gm       Eb
Hold my hand
              Cm7 Cm7/Bb
And I'll take you there
Ab6
Somehow
Ab     Bb     C
Someday, somewhere

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Ab6 = 4 X 3 5 4 X
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bb7+ = X 1 3 2 3 1
C = X 3 2 0 1 0
C7/9 = X 3 2 3 3 X
Cm = X 3 5 5 4 3
Cm/Bb = X 1 1 0 1 X
Cm7 = X 3 5 3 4 3
Cm7/Bb = 6 3 5 3 4 3
D7 = X X 0 2 1 2
Eb = X 6 5 3 4 3
F6 = 1 X 0 2 1 X
F7+ = 1 X 2 2 1 X
Fm6 = 1 X 0 1 1 X
G6 = 3 X 2 4 3 X
G7 = 3 5 3 4 3 3
G7/9 = 3 X 3 2 0 X
Gm = 3 5 5 3 3 3
Gm6 = 3 X 2 3 3 X
