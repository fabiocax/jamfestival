Renato Russo - Like a Lover

(intro) C C/F

C                 C4
Like a lover the morning sun
C                 D/C        Bm7
Slowly rises and kisses you awake
      A4      A7       D9
Your smile is soft and drowsy
                 G4 G         F
As you let it play   upon your face
G/F        C                         C4           C
Oh, how I dream I might be like the morning sun to you
                      C4
(I might be like the morning sun)

C                 C4
Like a lover the velvet moon,
C                   D/C                     Bm7
Shares your pillow and watches while you sleep
     A4     A7       D9
Your eyes is soft and drowsy

                  G4 G         F
As you let it play   upon your face
G/F        C                         C4           C
Oh, how I dream I might be like the velvet sun to you
                      C4
(I might be like the velvet sun to you)

(intro)

C                                D/C
How I envy a cup that knows your lips
                  Bm7
Let it be me, my love
                                     Em7
And a table that feels your fingertips
                               F#m5-/7
Let it be me, let me be your love
                    Am7                   Bb
Bring an end to the endless days and nights
          G
Without you

( C C4 C G#/C )

C#                 C#4
Like a lover the velvet moon,
C#                D#/C#       Cm7
Slowly rises and kisses you awake
     Bb4     Bb7       D#9
Your smile is soft and drowsy
                  G#4 G#         F#
As you let it play   upon your face
G#/F#        C#                         C#4           C#
Oh, how I dream I might be like the velvet moon to you
                      C#4
(I might be like the velvet moon)

C#                                D#/C#
How I envy a cup that knows your lips
                  Cm7
Let it be me, my love
                                     Fm7
And a table that feels your fingertips
                               Gm5-/7
Let it be me, let me be your love
                    Bbm7                   B
Bring an end to the endless days and nights
          G#
Without you

C#                   C#4
I might be like the velvet moon to you (4x)

(instrumetal) C# C#4

----------------- Acordes -----------------
A4 = X 0 2 2 3 0
A7 = X 0 2 0 2 0
Am7 = X 0 2 0 1 0
B = X 2 4 4 4 2
Bb = X 1 3 3 3 1
Bb4 = X 1 3 3 4 1
Bb7 = X 1 3 1 3 1
Bbm7 = X 1 3 1 2 1
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
C# = X 4 6 6 6 4
C#4 = X 4 4 1 2 X
C/F = X 8 10 9 8 8
C4 = X 3 3 0 1 X
Cm7 = X 3 5 3 4 3
D#/C# = X 4 5 3 4 X
D#9 = X 6 8 8 6 6
D/C = X 3 X 2 3 2
D9 = X X 0 2 3 0
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
F#m5-/7 = 2 X 2 2 1 X
Fm7 = 1 X 1 1 1 X
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
G#/C = X 3 X 1 4 4
G#/F# = 2 X 1 1 1 X
G#4 = 4 6 6 6 4 4
G/F = 1 X X 0 0 3
G4 = 3 5 5 5 3 3
Gm5-/7 = 3 X 3 3 2 X
