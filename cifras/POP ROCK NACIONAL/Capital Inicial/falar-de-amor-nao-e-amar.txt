Capital Inicial - Falar de Amor Não É Amar

[Intro] D  A  Em  G
        D  A  Em  G
        D  A  Em  G
        D  A  Em  G

E|-----12--14--12----12--14--12----12--14--12-------------------12------|
B|--15------------15------------15--------------1212-15-12-1414----14-15|
G|----------------------------------------------------------------------|
D|----------------------------------------------------------------------|
A|----------------------------------------------------------------------|
E|----------------------------------------------------------------------|

D                 A              Em          G
 Eu segui os seus passos achando que você soubesse onde ir
D                 A               Em            G           D   D
 Eu me vi em seus sonhos, perdido sem saber que direção seguir

Em              A          Em            A
Mundos tão estranhos nas palavras que eu ouvi
Em                A        Em                A
No fundo dos seus olhos afogado em gelo eu descobri


  G               A           A             D    D
Falar de amor não é amar, não é querer ninguém
  G               A
Falar de amor não é amar alguém

( D  A  Em  G )
( D  A  Em  G )

D                A                  Em        G
 Eu caí em em pedaços, um grão de areia carregado por marés
D                A                  Em           G               D   D
 Derreti em seus lábios, sentindo o chão sumir embaixo dos meus pés

    Em        A          Em              A
Os dias esquecidos no verão que eu inventei
    Em          A           Em             A          A
Eu sei que você vive das mentiras que eu acreditei

  G               A           A             D    D
Falar de amor não é amar, não é querer ninguém
  G               A         D     D
Falar de amor não é amar alguém
  G               A           A             D    D
Falar de amor não é amar, não é querer ninguém
  G               A          D
Falar de amor não é amar alguém

( D  A  Em  G )
( D  A  Em  G )

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
