Capital Inicial - Giulia

(Dinho Ouro Preto)

Introdução: (F# G#m A#m G#m)

*Esse riff toca-se durante a música toda
menos no refrão.
|-------------|
|-------------|
|-3s4-6-4-3---|
|-----------4-|
|-------------|
|-------------|

F#                  G#m
Agora eu já posso morrer em paz
            A#m                    G#m
Eu tenho um milhão de contas pra pagar
              F#    G#m  A#m  G#m
Mas agora tanto faz
F#
Seu cabelo é o sol

           G#m
Seus olhos cor de anis
    A#m
Dos dedos dos pés
        G#m
À ponta do nariz
F#    G#m
perfeição
A#m                G#m
A melhor coisa que eu já fiz

Refrão:
F#             G#m
Quando você me olha assim
A#m                G#m
A minha resposta é sempre sim
F#          G#m
Quando você sorri pra mim
A#m                           G#m        C#
Parece que o mundo não é um lugar tão ruim

(F# G#m A#m G#m) -Toca-se o riff-
Nanan nara nana Nanan nara nana

F#
Eu não toco o chão
            G#m
Eu começo a flutuar
A#m
Estou nas nuvens
G#m   F#           G#m  A#m  G#m
Já as posso até tocar
F#                                G#m
Eu não quero que ela conheça a violência ou a dor
       A#m                    G#m
Só meu mundo imaginário de prazer e amor
F#      G#m
Perfeição
A#m                G#m
A melhor coisa que eu já fiz

Refrão

----------------- Acordes -----------------
A#m = X 1 3 3 2 1
C# = X 4 6 6 6 4
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
