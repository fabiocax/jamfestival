Capital Inicial - Paz No matadouro

Intro: (D D4 C D) 2X

D                C            D
 Eu estou certo, de que nada sei
                C            D
 Tampouco perto, de onde nasci
                            C   D
 este concreto sob os meus pés
                       C    D
 tão longe do que plantei
Bm7                       E
 Estou curioso a seu respeito
Bm7                       E
 Mas tão cansado de esperar
Bm7               E
 Leve o seu gato, pra longe daqui
Bm7                   E              Bm7  E
 solte seus cachorros, no matadouro
Bm7                       E
 Eu quero paz, no matadouro
D                C            D
 Eu estou certo, de que nada sei

                C            D
 Tampouco perto, de onde nasci
                   C D                    C   D
 de resto é tanto pó, na frente do meu nariz

*Agora entra a distorção

G                        F                G
 Pegue o caminho mais longo, entre dois mundos
G                   F                G
 alcance com os dedos os botões corretos
G                  F                G
 indique com os olhos, mas não fale nada
G                  F              G
 Faz o que veio fazer, e siga em paz
Bm7                       E
 Estou curioso a seu respeito
Bm7                       E
 Mas tão cansado de esperar
Bm7               E
 Leve o seu gato, pra longe daqui
Bm7                   E              Bm7  E
 solte seus cachorros, no matadouro
Bm7                       E
 Eu quero paz, no matadouro

----------------- Acordes -----------------
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D4 = X X 0 2 3 3
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
