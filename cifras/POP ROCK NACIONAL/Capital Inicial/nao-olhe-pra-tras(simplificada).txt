Capital Inicial - Não Olhe Pra Trás

Intro 2x:  G  Am7  C


G             Am7
Nem tudo é como você quer
Bm7                     Am7
Nem tudo pode ser perfeito
G           Am7
Pode ser fácil se você
Bm7                     Am7
Ver o mundo de outro jeito
C                          G4/B
Se o que é errado ficou certo
                         Am7
As coisas são como elas são
C                       G4/B
Se inteligência ficou cega
                 Am7
De tanta informação



Refrão:
G                Am7
Se não faz sentido
            C
Discorde comigo
               G
Não é nada demais
              Am7
São águas passadas
                C
Escolha uma estrada
                           G  Am7 C
e não olhe, não olhe pra trás


G                            Am7
Você quer encontrar a solução
Bm7                    Am7
Sem ter nenhum problema
G                  Am7
Insistir em se preocupar demais
Bm7                    Am7
Cada escolha é um dilema
C                  G4/B                      Am7
Como sempre estou   mais do seu lado que você
C                          G4/B
Siga em frente em linha reta
                        Am7
E não procure o que perder


Refrão:
G                Am7
Se não faz sentido
            C
Discorde comigo
               G
Não é nada demais
              Am7
São águas passadas
                C
Escolha uma estrada
                           G
e não olhe, não olhe pra trás

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
G = 3 2 0 0 0 3
G4/B = X 2 0 0 1 X
