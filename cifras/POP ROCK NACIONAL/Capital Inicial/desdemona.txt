Capital Inicial - Desdemona

(intro) D Em Bm

     D                         Em   G
Você quer o meu amor e a minha morte
         D                    Em  G
Se estou vivo até hoje só por sorte
       D             Em   G
Sou presa fácil, desprotegida
D            Em      G
Você é minha bala perdida

( D Em Bm ) (2x)

  D
Estou para você
        Em            G
Como um anjo para a cruz
  D
Você está em mim
         Em           G
Como as trevas para a luz

    D                     Em   G
Meu peito já sentiu tua facada

       D                Em  G
Eu preciso de ajuda, de ajuda
A                  G
Você cospe no meu rosto
                  A
Eu sempre vou te amar            (2x)
                 G
Caso um dia você tente
           A
Tente me matar
A                  G
Você cospe no meu rosto
           A      Em (na 2ª vez)
Tente me matar

( D Em Bm ) (2x)

D                     Em  G
Com um beijo na minha boca
      D           Em    G
Sem veneno na sua língua
    D          Em      G
Com um olhar jogado ao vento
    D          Em
Na sua tela, a minha tinta1

( D Em Bm ) (2x)

     D
Descobrindo a nova terra
    Em           G
história que nos liga

      D
Com a vida segue a morte
       Em           G
Ou uma vida desconhecida
D             Em          G
Subo ao céu e desço ao inferno
D
Para mim nós somos
     Em           G
Como dois sonhos eternos

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
