Capital Inicial - Dançando Com a Lua

    D      G           D           G
Cuidado comigo eu não sou nenhum santo
    D      G           D           G
É tudo para sempre mas só por enquanto
 F#m      Bm       F#m      Bm
Certas palavras dizendo o oposto
    Em         G           Em         A
Vendi a minha alma pra me ver no seu rosto.

    D          G      D          G
Os outros que mentem, eu levo a fama
      D    G     D           G
Meus 15 minutos duram uma semana
     F#m           Bm         F#m        Bm
Bem feito pra mim mesmo quem foi que mandou
  Em           G           Em      A
Agora é muito tarde foi embora acabou

D                    G           D
  Dançando onde ninguém pode me ver
                 G           D
Dançando pra tentar me esquecer

                G           D
Dançando pra fazer você voltar
                G           A
Dançando com a lua em seu lugar

    D        G       D        G
Me sobe a cabeça me joga no chão
  D            G       D      G
Quase um desespero de estimação.
   F#m         Bm       F#m      Bm
Pergunto a mim mesmo por que é assim.
   Em         G           Em              A
Igual pra todo mundo mas dói bem mais em mim
    D      G           D           G
Parece uma festa que não terminou
    D      G           D           G
Eu sou o convidado que nunca chegou
   F#m      Bm     F#m        Bm
A tarde me acalma a noite me consome
 Em       G            Em          A
Outro problema que eu batizo com seu nome

D                  G           D
Dançando onde ninguém pode me ver
                 G           D
Dançando pra tentar me esquecer
                G           D
Dançando pra fazer você voltar
                G           A
Dançando com a lua em seu lugar

D                  G           D
Dançando onde ninguém pode me ver
                 G           D
Dançando pra tentar me esquecer
                G           D
Dançando pra fazer você voltar
                G           A     D
Dançando com a lua em seu lugar

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
