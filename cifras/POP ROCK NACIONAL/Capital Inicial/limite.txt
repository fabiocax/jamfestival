Capital Inicial - Limite

De:Fê Lemos, Dinho, Bozzo Barretti


Introd.: C7+ D7 Bm7 Em7


C7+
 Olhe em frente
     D7
Você gosta do que vê?
      Bm7
Você sempre vê
    Em7
As coisas do limite
    C7+
Porque de lá
    D7
Se vê melhor
Bm7
Sinta a cidade respirar
  Em7                  C7+ D7 Bm7 Em7
Escute meu coração sufocar


   C7+        D7
Eu já escutei o suficiente
Bm7                Em7
Pra saber o que ouvir
      C7+
Já me escondi
D7
Eu me protegi
       Bm7      Em7
Mas de nada serviu
        C7+
Não me deixe sozinho
      D7
Nesse mundo hostil
        Bm7          Em7 D7 Bm7
Não me deixe sozinho


C7+    D7    Em7
Que....ro fazer contato
C7+     D7       Em7
 Um acidente na contramão
C7+   D7     Em7
Que...ro sentir o impacto
C7+       D7          Em7     (introd.)
 Frente a frente em colisão


C7+            D7
 Eu não quero quantidade
C7+        Bm7
 Eu quero qualidade
  Em7
E fico pensando
C7+           D7
Como seria te encontrar um dia
Bm7
Sem orgulho, vaidade
Em7                    C7+   D7
Quase sem nenhuma proteção
  Bm7      Em7
(proteção)
      C7+       D7
Com a confiança de uma criança
Bm7               Em7
 Sem todo esse aparato
   C7+              D7
Um pouco de simplicidade
Bm7        Em7
Nessa confusão
   C7+              D7
Um pouco de simplicidade
           Bm7 Em7 D7 Bm7
Nessa confusão


C7+    D7    Em7
Que....ro fazer contato
C7+     D7       Em7
 Um acidente na contramão
C7+   D7     Em7
Que...ro sentir o impacto
C7+       D7        Em7     (introd.)
 Frente a frente em colisão

----------------- Acordes -----------------
Bm7 = X 2 4 2 3 2
C7+ = X 3 2 0 0 X
D7 = X X 0 2 1 2
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
