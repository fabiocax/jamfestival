Tihuana - Que Ves

Intro (4x):  Am       Dm    G      Am

E|---------5-55---3-3-----0---------|
B|---------6-66---3-3-----3---------|
G|-----2/7-7-77---0-0-----2---------|
D|---2-----7-77---0-0-----2---------|
A|-0-------5-55---2-2-----0---------|
E|----------------3-3-----0---------|

Am         G            Dm     F
Olhando no olho posso viajar
Am         G                    Dm     F
Falta em mim o truque saber aceitar
Am         G                 Dm     F
Estamos em busca de um ponto final
Am         G            Dm     F
Pra poder chegar de uma só vez

Am   G   Am               G
Que ves. Que ves quando me ves
Am        G             Dm F    (2x)
Quando a mentira acabar


(intro)

Am         G                Dm     F
A prece pra Deus pode nos enganar
Am         G                    Dm     F
O bem e o mal ainda podem se encontrar
Am         G                    Dm     F
O fim se aproxima com vista para o mar
Am         G              Dm     F
Cruzando a vida sem se preocupar

Am   G   Am               G
Que ves. Que ves quando me ves
Am        G             Dm F    (2x)
Quando a mentira acabar

Am         G                    Dm     F
O fim se aproxima com vista para o mar
Am         G              Dm     F
Cruzando a vida sem se preocupar

Am   G   Am               G
Que ves. Que ves quando me ves
Am        G             Dm F
Quando a mentira acabar
Am   G   Am               G
Que ves. Que ves quando me ves
Am        G             Dm F
Quando a mentira acabar

(três batidas rápidas - Am G Am )

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
