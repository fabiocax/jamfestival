Tihuana - Minha Rainha

Intro: ( D5 G5 ) 3x   G5 ( C#5 D5 )

E|---------------------------------------------------|
B|---------------------------------------------------|
G|--4----------4-------------------------------------|
D|----5---5-7-------7-5---7--5-----------------------|
A|---------------------------------------------------|
E|---------------------------------------------------|


E|---------------------------------------------------|
B|---------------------------------------------------|
G|--4-------------4----------------------------------|
D|----5------5-7-----7--5----------------------------|
A|-------------------------7--5----------------------|
E|---------------------------------------------------|

E|---------------------------------------------------|
B|---------------------------------------------------|
G|--4-----------------4------------------------------|
D|----5-----5-7--5-7-----7--5--5-------5-------------|
A|---------------------------------------------------|
E|---------------------------------------------------|


 D D# E F

 ( F5    B5 C5 )
Todo baralho tem um Rei
E também tem sua Rainha
Eu sou valete eu sei
Mas também quero ter a minha
Mesmo toda errada
Mesmo sendo diferente
Quando a luz apaga
Ela é a minha Silvia Saints

 D5
Mas com um olho aberto, o outro fechado
( D5 E5 D5 C5 )
Um olha pra frente, o outro pro lado
 D5
E quando me olham, ficam juntinhos
( G5 C5 )
Um olho no gato e outro no passarinho
 D5
Seu cabelereiro é o advogado
( D5 E5 D5 C5 )
Cabelo tá preso ou tá solto e armado
 D5
Mas ela é linda, só falta uns dentinhos
( G5 C5)                                  G
Mas com o que sobram tem muito carinho


Refrão (2x)

   G               D
Ninguém consegue entender
   G                 D
Eu sou escravo sem querer
  G                 D        (C#5 D)
Do que ela mais sabe fazer
(C#5 D)
Ohhh Ohhhh


D D# E F

            ( F5 B5 C5 )
(Vai Digão...)
A vida não é fácil, mas difícil essa menina
Eu fico só na dela e ela me azucrina
Melhor morrer de azia, que de barriga vazia
Mas nada é do jeito como a gente queria

 D5
E como dois lados que são diferentes
( D5 E5 D5 C5 )
Mas que pertencem a mesma moeda
 D5
Jekyll e Hyde, Hulk e Benner
( G5 C5 )
Gata borralheira e a Cinderela
 D5
Pergunto pra ela se tá tudo bem
( D5 E5 D5 C5 )
Ela diz que sim, mas eu sei que nem
 D5
Se eu quero ir para um lado ela corre pro outro
( G5 C5 )                                 G
Não faço por mal, ela faz por desgosto


Refrão (2x)
   G               D
Ninguém consegue entender
   G                 D
Eu sou escravo sem querer
  G                 D
Do que ela mais sabe fazer
 (C#5 D)
Ohhh Ohhhh

*Solo
*Refrão (2x)

 D5
Tihuana e Raimundos quebrando na alta
( D5 E5 D5 C5 )
Não tira de dentro se não leva falta
 D5
Se liga muleque sou cabra da peste,
( G5 C5 )
Corre na veia o sangue nordeste

 D5
É doido, nós somos do tempo de caloi extra light e shape Powell Peralta!

----------------- Acordes -----------------
B5 = X 2 4 4 X X
C#5 = X 4 6 6 X X
C5 = X 3 5 5 X X
D = X X 0 2 3 2
D# = X 6 5 3 4 3
D5 = X 5 7 7 X X
E = 0 2 2 1 0 0
E5 = 0 2 2 X X X
F = 1 3 3 2 1 1
F5 = 1 3 3 X X X
G = 3 2 0 0 0 3
G5 = 3 5 5 X X X
