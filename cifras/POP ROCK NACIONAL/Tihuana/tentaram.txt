Tihuana - Tentaram

(Belex/Rick Bonadio)

(A-C#m)
Tentaram me afastar
De perto de você
Tentaram me vendar
Pra que eu não pudesse ver
(F#m-C#m)
Tentaram me enganar
Tentaram me ferir
Tentaram questionar
Tentaram rir de mim
Tentaram me ver mal
(A-C#m-F#m-E)
*Foi quando sem querer
Acreditar no destino
Saímos por cima, sem ter que nos explicar
A ninguém...

(A-C#m-D-Dm)
É, eu sei que ainda é cedo

Mas tem que ser agora
Preciso te ver
É, vencemos nossos medos
E só pela vontade de viver

(A-C#m)
Cortaram as entradas
Pra chegar em você
Cortaram nossos sonhos
Temos muito que aprender
(F#m-C#m)
Cortaram o meu gás
Cortaram minha luz
Cortaram os sinais
E a minha fé por Jesus
Cortaram muito mais...

*Foi quando sem querer...

----------------- Acordes -----------------
A = X 0 2 2 2 0
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
