Ira! - Entre Seus Rins

(intro) A G#m F#m B

(uah-uah distorcido rythmic)

         A     G#m    F#m      E
E |---------------------------------------------------------
B |---------------------------------------------------------
G |--------------------------------------------------------- (4x)
D |---------------------------------------------------------
A |-----7b~----6b~-----4b~---2-2-2p4-2p4--------------------
E |--5------4-------2---------------------------------------

      A G#m F#m         E           A G#m F#m
Te Amo           Isso eu posso te dizer
        E           D    E
como eu gosto de você
                      ( A G#m F#m E )  (intro)
como eu gosto de você

        A G#m F#m       E            A G#m F#m
Te Quero         isso é tudo que eu sei

        E
          D    E
que eu gosto de você
                         A
ah , como eu gosto de você

D             A D                        E
O que eu sinto        não é difícil explicar
        B E
é o amor
                   A
como uma fonte á jorrar
        A
pura emoção

D            A D
E o meu sonho
                    E
nem consigo me lembrar
            B E
más o certo
                   A
é que você estava lá
       A
sonho real
        E
sonho real

A          D                     A
Seu Beijo minhas mãos em seu quadril
 D                 E
madrugada tão febril
                    A
ah como eu gosto de você

(intro)

      A G#m F#m             E          A G#m F#m
Meu exílio         é o seu corpo inteiro
         E              D    E
é o meu pais estrangeiro
                   A
ah como eu gosto de você

(refrão 2x)
A              G#m F#m
Me deu o dedo
                            A
eu quis o braço e muito mais
               G#m F#m                      D
agora estou afim      de ficar entre seus rins
E
de ficar entre seus rins

( A G#m F#m B )  (4x)
de ficar entre seus rins

(versão 2)

(intro 4x) A G# F#m E

   A G# F#m      E         A G# F#m
Te amo, isso eu posso te dizer
         E         D
como eu gosto de você
E                   A G# F#m E
como eu gosto de você
    A G# F#m      E         A G# F#m
Te quero isso é tudo que eu sei
         E       D
que eu gosto de você
E                         A
ah , como eu gosto de você
D                                 E
O que eu sinto não é difícil explicar
                            A
é o amor como uma fonte á jorrar, pura emoção
D
E o meu sonho
                     E
nem consigo me lembrar
                              A
mas o certo é que você estava lá
                   E
sonho real, sonho real
      A G# F#m      E         A G# F#m
Seu beijo minhas mãos em seu quadril
     E          D
madrugada tão febril
E                      A
ah , como eu gosto de você

(intro 2x)

  A G# F#m          E       A G# F#m
Meu exílio é o seu corpo inteiro
        E          D
és meu país estrangeiro
E                       A
ah , como eu gosto de você

A              F#m
Me deu o dedo
                            A
eu quis o braço e muito mais
                F#m                      D E                     (A G# F#m E)
agora estou afim de ficar entre seus rins  de ficar entre seus rins

(intro 4x)

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
G#m = 4 6 6 4 4 4
