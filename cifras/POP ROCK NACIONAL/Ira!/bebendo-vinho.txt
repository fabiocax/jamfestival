Ira! - Bebendo Vinho

Intro: G

Solo intrudução:
    (G)                       G   G G G G G G G (Paletadas para baixo)
E|--3------------------------3---3-3-3-3-3-3-3-------------|
B|--3------------------------3---3-3-3-3-3-3-3-------------|
G|---------------------------------------------------------|
D|---------------------------------------------------------|
A|--2------------0-0-0--2-0--2---2-2-2-2-2-2-2-------------|
E|--3--3-3-3-----------------3---3-3-3-3-3-3-3-------------|

  C                      G
Eu vivo sozinho e apaixonado
             D
Não tenho ninguém
           C   G
Aqui do meu lado
 C                     Bm
Meu cachorro Vênus foi roubado
              Am        C  G     C G
Fiquei um pouco preocupado


Refrão 2x:
 C
Vou me entorpecer
          G
Bebendo vinho
         D
Eu sigo só
          G
No meu caminho

Solo 2x: A Bm  G G

      C                  G
Chove prá caramba aqui no Rio
        D
Penso no Sul
        C  G
Aquele frio
    C                Bm
A TV diz que vai fazer sol
             Am         C  G     C G
Não sei se é bom ou é pior

Refrão 2x:
 C
Vou me entorpecer
          G
Bebendo vinho
         D
Eu sigo só
          G
No meu caminho

Solo 2x: A Bm  G G

  C                         G
A rádio toca um velho Rock and Roll
        D
Fico pensando
         C G
Aonde estou
 C                  Bm
Nada satisfaz nessa hora
        Am         C G    C G
Se é assim eu vou embora

Refrão 2x:
 C
Vou me entorpecer
          G
Bebendo vinho
         D
Eu sigo só
          G
No meu caminho

C                         G
Vou me entorpecer bebendo vinho
        D
Eu sigo só

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
