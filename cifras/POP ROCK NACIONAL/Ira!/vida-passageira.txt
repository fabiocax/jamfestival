﻿Ira! - Vida Passageira

Introdução:

              A   A#     A     E        A    A#    A                   A   A#  (4x)
E|---------------------------------------------------| ------------|-5---6----|
B|---------------------------------------------------| ------------|-5---6----|
G|---------------------------------------------------| ------------|-6---7----|
D|-------7777-8888-7777777---------7777-8888-7777777-| -----5---5--|-7---8----|
A|-------7777-8888-7777777-2~~-----7777-8888-7777777-| -5/7---7----|-7---8----|
E|-------5555-6666-5555555-0~~-----5555-6666-5555555-| ------------|-5---6----|

A
Do alto da montanha
   A7+
Ou em um cavalo em verde vale
  A7                   D
E tendo o poder de levitar
D                         C#m7
É como em um comercial de cigarros
                                    Bm7
Em que a verdade se esquece com uns tragos
                  E
Sonho difícil de acordar

A7                           A7+
É quando teus amigos te surpreendem
                   A7
Deixando a vida de repente
                    D
E não se quer acreditar


D
Mas essa vida é passageira
C#m
Chorar eu sei que é besteira
Bm                                 E
Mas, meu amigo, não dá prá segurar

                A   A7
Não dá prá  segurar
               Bm
Não dá prá segurar
               D
Não dá prá segurar
   C#m        Bm            E          A
Desculpe meu amigo, mas não dá prá segurar

    B                          B7+
Vou dar então um passeio pelas praias da Bahia
       B7                                      E
Onde a lua se parece com a bandeira da Turquia
E                          D#m
É o planeta inteiro que respira
                       C#m
Sinais de vida em cada esquina
                   F#
Tanta gente que se anima

  E                        D#m
É quando teus amigos te surpreendem
                   C#m
Deixando a vida de repente
                    F#
E não se quer acreditar
E
Mas essa vida é passageira
D#m
Chorar eu sei que é besteira
C#m                        F#
Mas, meu amigo, não dá prá segurar

                B
Não dá prá  segurar
               C#m
Não dá prá segurar
               E
Não dá prá segurar
   D#m        C#m           F#         B
Desculpe meu amigo, mas não dá prá segurar
               C#m
Não dá prá segurar
               E
Não dá prá segurar
   D#m        C#m           F#         B
Desculpe meu amigo, mas não dá prá segurar

               C#m
Não dá prá segurar
               E
Não dá prá segurar
   D#m        C#m           F#         B
Desculpe meu amigo, mas não dá prá segurar

----------------- Acordes -----------------
A = X 0 2 2 2 0
A# = X 1 3 3 3 1
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
B7+ = X 2 4 3 4 2
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
D#m = X X 1 3 4 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
