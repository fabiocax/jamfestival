Ira! - Mariana Foi Pro Mar

(intro) G7
                         C
Mariana foi pro Mar
Am                   F                           G
       Deixou seus bens mais valiosos com o cachorro
         C               Am
E foi viajar, foi de coração
          F                      G                 C
Pois o marido saiu pra comprar cigarros e desapareceu
              Am             F                    G
Foi visto no Japão, com a vizinha, sua ex-melhor amiga
                C
Mariana foi ao chão
F                         G
E ela pensou por muitas vezes
              Em                     Am
Se usava sua mauzer ou o gás de seu fogão
                 F                         G
Mas seu último direito, ela viu que era um erro
  G7           C      A
Mariana foi pro mar

             D      Bm
Mariana se cansou
G                           A                     D
Olhou o que restava de sua vida, sem direito a pensão
                   Bm          G
Sem um puto pra gastar, sempre foi moça mimada
     A                        D D7
Mas tinha em si a vocação do lar
G                     A                        F#m
E foi numa tarde de domingo que ganhou tudo no bingo
                      Bm
Sorte no jogo azar no amor
G                     A                   F#m
E sua bagagem estava pronta, parecia que sabia
                        Bm                   G                   A
Do seu prêmio de consolação     Mudou o itinerário, trocou o funerário
                 D     B
Pelo atraso do avião
                E              C#m
Uma lágrima de sal, uuuuuuuuuuuuhhh
    A                      B                    E
percorre o seu rosto  misturando-se ao creme facial
                 C#m             A              B                      E E7
Onde foi que ela errou, se acreditava na sinceridade De sua vida conjugal
A                       B                    G#m                    C#m
E se ela pensava muitas vezes Se usava uma pistola ou o gás do seu fogão
A                     B                 G#m                   C#m
Mas ela mudou o itinerário, trocou o obituário Pelo atraso do avião
A                     B                 G#m                      C#m
Hoje ela desfila pela areia Com total desprezo pelos machos de plantão
                  A                     B                     A
Ela está bem diferente, ama ser independente Mariana foi pro mar aaaaaaaaaaa
B            D          A     E    A           B     D        A    E E4 E E4 E
Aaaaaaaaa    aaaaaaaa,  aaaaaaaa   aaaaaaaaaa  aaaaaaaaaa aaaaaaaaaa

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
E4 = 0 2 2 2 0 0
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G#m = 4 6 6 4 4 4
G7 = 3 5 3 4 3 3
