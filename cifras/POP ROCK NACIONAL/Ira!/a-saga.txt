Ira! - A Saga

[Intro]  Bm   C#m  D  G   F#  Bm
         C#m  D    G  F#  Bm

Bm      A
Caindo, cai
          D
Descalço, vai
               G F# Bm
Na beira da calçada

         A
Olhando, vê
          D
Assiste a quê
              G F# Bm
Anda atrás de nada

        C#m
Catando lixo
           D
Correndo o risco

           G F# Bm
Fazendo arruaça

         C#m
Chutando lata
         D
Xingando placa
           G F#
Fazendo ameaça
 G F#   G F#
Passa, passa

Bm             C#m
Cambaleando na praça
              D
Cabeceando vidraça
                      G F# Bm
Descarregando essa desgraça

                   C#m
Passando carro que passa
                   D
Cuspindo sarro e cachaça
                  G F# Bm
Desarrasou-a na fumaça

                  C#m
Sacode o trapo da roupa
                 D
Sacode saco de estopa
                          G F# Bm
Se esconde atrás de uma fachada

                 C#m
Suspenso no meio fio
                     D
Pergunta onde é o brasil
                       G F#
Procura e não encontra nada
G F#   G F#
Nada, nada

[Solo]  C#m  D   G  F#  Bm
        C#m  D   G  F#  Bm

         A
Praça da sé
          D
Cabeça em pé
             G F# Bm
De olhar pra lua

        A
Na são joão
            D
Vai vendo o chão
           G F# Bm
Derrubar a rua

         C#m
Tomando chuva
        D
Tomando sol
               G F# Bm
Tomando uma porrada

          C#m
Não tem lugar
            D
Qualquer lugar
        G F#  G F#  G F#
É a sua casa

Bm                 C#m
Puxa a carroça a cavalo
                   D
Fuçando o resto no ralo
                      G F# Bm
Desengonçando essa carcaça

                       C#m
Quem vê não quer nem saber
                       D
Quem sabe não quer nem ver
                         G F# Bm
Quem chega perto perde a graça

                  C#m
Sai do terreno baldio
               D
Perambulando vadio
                    G F# Bm
Em meio a próxima parada

                    C#m
Treme de medo ou de frio
                     D
Pergunta onde é o brasil
                        G F#
Procura e não encontra nada
G F#   G F#
Nada, nada

Bm        C#m       D      G F# Bm
Lá lá lá lá lá lá lá lá lá lá lá
          C#m       D      G F# Bm
Lá lá lá lá lá lá lá lá lá lá lá
          C#m       D      G F# Bm
Lá lá lá lá lá lá lá lá lá lá lá
          C#m       D      G F#      (G F#)  Bm
Lá lá lá lá lá lá lá lá lá lá lá

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
