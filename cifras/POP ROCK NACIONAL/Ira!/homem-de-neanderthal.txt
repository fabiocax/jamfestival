Ira! - Homem De Neanderthal

Intr.:  C Am Em F G
        C Am Em C D

     G             Bm
Não passo de um amador
   Am                 D
Em busca da vida perfeita
      G                 Bm
Pelo menos estou tão distante
         Am                     D
Da truculência executiva de um "yupi”
         Am          C          D
Da truculência executiva de um "yupi”

REFRÃO: -----------------------------

Em                        D
Homem de Neanderthal já fui,
  G                    C
E acho isso muito natural
D            C         G  Em
Diferença alguma vai fazer

        Am          D            G
Se este ano não sobrar para o natal
C            D          Em
Diferença alguma vai fazer
        Am          D            G
Se este ano não sobrar para o natal
-----------------------------------------------
Repete intro


     G                  Bm
Não passo de um compositor
     Am                D
Em busca da canção perfeita
G                   Bm
Falo de amor sem rimar
        Am                              D
E nunca mais meu bem vou lhe pedir desculpas
        Am                C             D
E nunca mais meu bem vou lhe pedir desculpas

REFRÃO (2X)
termina praticamente igual à introdução

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
