Ira! - Você Ainda Pode Sonhar

 Intr.: G C D7 G C D7

A         A7       D          Dm
Pense num dia com gosto de infância
     A          A7        D       F
Sem muita importância procure lembrar
A         A7        D         F
Você por certo vai sentir saudades
  A         A7       D
Fechando os olhos verá
Bb                C
Doces meninas dançando ao luar
F                   Bb
Outras canções de amor
C                 G                 D
Mil violinos um cheiro de flores no ar

G           C      D7
Você ainda pode sonhar
G           C      D7
Você ainda pode sonhar

G           C      D7   D  A
Você ainda pode sonhar, ah ah ah

           A7          D     Dm
Feche seus olhos bem profundamente
      A         A7    D         F
Não queira acordar, procure dormir
A         A7     D             F       A
Faça uma força, você não está velho demais
     A7          D
Pra voltar e sorrir

Bb               C
Passe voado por cima do mar
F              Bb
Para a ilha rever
C                 G                 D
Vá saltitando sorrindo a todos que ver

G           C      D7
Você ainda pode sonhar
G           C      D7
Você ainda pode sonhar
G           C      D7   D  A
Você ainda pode sonhar, ah ah ah

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
