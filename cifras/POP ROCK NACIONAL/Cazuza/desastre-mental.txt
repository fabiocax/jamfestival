Cazuza - Desastre Mental

(intro) G5 F5 G5 Bb5 C5 C5 Bb5 C5 Eb5 G5

Gm
Baby eu lamento
Mas não tenho tempo
        Cm              Bb7
Pra sentir as tuas dores
    A7                 D7
As minhas eu já não aguento

Gm
Minha vista torta
Já não se importa
        Cm                   Bb7
Não me conte um bando de mentiras
           Eb7          D7
Quando eu for fechar a porta

(refrão) G5 F5 G5 Bb5 C5 C5 Bb5 C5 Eb5 G5

Aqui ninguém entra

Daqui ninguém sai
Somos sobreviventes
De um desastre mental

Gm
Não é que eu não ligue
De correr o perigo
    Cm         Bb7   A7  D7
De nunca te achar direito
Gm
Eu quero de qualquer jeito
Eu tenho que me salvar
    Cm                            Bb7
Não vá me convencer que está com medo,
          Eb7               D7
Que está tarde ou que está cedo

(refrão) G5 F5 G5 Bb5 C5 C5 Bb5 C5 Eb5 G5

Aqui ninguém entra
Daqui ninguém sai
Nós Somos sobreviventes
De um desastre mental

Cm
Prefiro te manter ao lado direito
Do meu peito por esta razão
Você não navega,
É uma queda de avidão no meu coração
                Eb7                D7
Não vá me provocar no fim da festa não

(refrão) G5 F5 G5 Bb5 C5 C5 Bb5 C5 Eb5 G5

Aqui ninguém tá morto
E daqui ninguém sai
Nós somos sobreviventes
De um desastre mental.

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bb5 = X 1 3 3 X X
Bb7 = X 1 3 1 3 1
C5 = X 3 5 5 X X
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
Eb5 = X 6 8 8 X X
Eb7 = X 6 5 6 4 X
F5 = 1 3 3 X X X
G5 = 3 5 5 X X X
Gm = 3 5 5 3 3 3
