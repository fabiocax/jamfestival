Cazuza - Boa Vida

F                         Dm
Eu nunca mais quero outra vida
Bb               C
É, eu ando um bocado mudado
F                         Dm
Eu nunca mais quero outra vida, eu não!
Bb                        C
Olha só como eu tô bem tratado

Bb                 A
É que os tempos mudaram
    G            F               C
E agora eu ando muito bem acompanhado
(É, eu ando, sim!)

F                         Dm
Eu nunca mais quero outra vida
Bb                    C
Jogado na rua, feito um vira-lata
F               Dm
O amor um dia chega, irmão!

Bb                  C
Mesmo pr'um cara pirado

Bb                   A
Que só sabe ficar bebendo pinga
G              F
Cantando rock, contando vantagem

G
Agora a gente só vive grudado
C
Pela rua, aos beijos e abraços
G
Todo mundo repara
C
E mesmo os meus amigos mais canalhas
   Bb
Me dão razão quando eu falo

F                              Dm
Que eu nunca mais quero outra vida
Bb                  C
Me machucar pela pessoa errada
F                 Dm
O amor tem cartas já marcadas
Bb                 C
E eu nunca tive vocação pra otário

Bb              A
É, os tempos mudaram
    G           F                 C
E agora eu ando muito bem acompanhado
(Eu ando sim...)

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
