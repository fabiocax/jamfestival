Cazuza - Blues da Piedade

[Intro] B7  E  G  A  E

E7
  Agora eu vou cantar pros miseráveis
G7
  Que vagam pelo mundo derrotados
A7
  Dessas sementes mal plantadas
        B7
Que já nascem com caras de abortadas
E7
  Pra pessoas de alma bem
pequena
C7
  Remoendo pequenos problemas
A7
  Querendo sempre aquilo
           E7
Que não têm pra quem vê a luz
     C7
Mas não ilumina suas mini-certezas

A
  Vive contando dinheiro
       E7
E não muda quando é lua cheia
A7          E7
  Pra quem não sabe amar
A7
  Fica esperando
            E7
Alguém que caiba no seu sonho
        A7
Como varizes que vão aumentando
        B7
Como insetos em volta da lâmpada
 A7             F#7
Vamos pedir piedade
    A7      E7
Senhor, piedade
  G7              F#7      E7
Pra essa gente careta e covarde
 G7             F#7
Vamos pedir piedade
    A7      E7
Senhor, piedade
A7
  Lhes dê grandeza e um pouco
 E7
De coragem

[Solo] E  G  A  E

E7
  Quero cantar só para as
Pessoas fracas
C7
  Que tão no mundo e perderam a viagem
A7
  Quero cantar os blues
          E7
Com o pastor e o bumbo na praça

Vamos pedir piedade
      C7
Pois há um incêndio sob a chuva rala
A7
  Somos iguais em desgraça
       B7
Vamos cantar o blues da piedade

G                 F#
  Vamos pedir piedade
    A       E
Senhor, piedade
G                  F#       E
 Pra essa gente careta e covarde
 G              F#
Vamos pedir piedade
    A       E
Senhor, piedade
A                        E
  Lhes dê grandeza e um pouco de coragem

[Solo] E  G  A  E  G  A  B7

A               E
  Pra quem não sabe amar
A                            E
  Fica esperando alguém que caiba nos seu sonho
        A
Como varizes que vão aumentando
        B7
Como insetos em volta da lâmpada

 G              F#
Vamos pedir piedade
    A       E
Senhor, piedade
G                   F#       E
  Pra essa gente careta e covarde
G                 F#
  Vamos pedir piedade
    A       E    A
Senhor, piedade
                       E
Lhes dê um pouco de coragem

 E  G  A                                   E
Ha...... lhes dê grandeza e um pouco de coragem

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
C7 = X 3 2 3 1 X
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F# = 2 4 4 3 2 2
F#7 = 2 4 2 3 2 2
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
