Cazuza - Cobaias de Deus

Intro.: (Dm Bb)

Riff. tocado após Dm Bb
B|----5h6-5--/
G|----5---5--/
A|----5---5--/

Dm       Bb    Dm
Se você quer saber como eu
       Bb  Dm
me sinto
         Bb     Dm
Vá a um laboratório, ou num
        Bb  Dm
labirinto
C    C°     C               C°
Seja atropelado por esse trem
      C
da morte
            C°
Vá ver as cobaias de Deus

   G              C
Andando na rua pedindo perdão
C°                   G
Vá a uma igreja qualquer
                           C
Pois lá se desfazem em sermão
Dm    Bb         Dm
Me sinto uma cobaia, um
      Bb     Dm
rato enorme
             Bb     Dm
Nas mãos de Deus, mulher
                Bb  Dm
De um Deus de saia
C       C°     C
Cagando e andando
           C°  C
Vou ver o ET
Dm          Bb        Dm
Ouvir um cantor de blues
              Bb   Dm
Em outra encarnação
C         C°
Nós, as cobais de Deus
      C          C°       C C°
Nós somos as cobaias de Deus
Dm  Bb          Dm
Me tire dessa jaula, irmão,
     Bb     Dm
não sou macaco
           Bb  Dm       Bb  Dm
Desse hospital  maquiavélico
C          C°     C
Meu pai e minha mãe, eu
            C° C
estou com medo
Dm           Bb    Dm
Porque eles vão deixar a
       Bb     Dm
sorte me levar
             Bb Dm
Você vai me ajudar, traga a
Bb    Dm
garrafa
        Bb     Dm             Bb
Estou desmilinguido, cara de boi
   Dm
lavado
C           C°      C
Traga uma corda irmão, irmão
  C° C
acorda!
Dm       Bb Dm
Nós as cobaias, vivemos
Bb     Dm
muito sós
C         C°        C
Por isso Deus tem pena, e
          C°     C
nos pões na cadeia
Dm     Bb   Dm
E nos faz cantar, dentro de
Bb      Dm
uma cadeia
C            C°   C
E nos pões numa clínica e nos
C°    C
faz voar
           C°
Nós as cobaias de Deus
    C                    C°
Nós somos as cobaias de Deus
    C                    C°
Nós somos as cobaias de Deus...

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C° = X 3 4 2 4 2
Dm = X X 0 2 3 1
G = 3 2 0 0 0 3
