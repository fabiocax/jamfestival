Cazuza - Carente Profissional

A           A/G
Tudo azul no céu desbotado
D9                     C9
E a alma lavada sem ter onde secar
A             A/G             D9
Eu corro, eu berro, nem dopante me dopa
C9
A vida me endoida
E      D9       A/C#     E       D9 A/C#
Eu mereço um lugar ao sol, mereço
E           D9          A/C#      E  D9 A/C#
Ganhar pra ser, carente profissional
A                       A/G
Seu eu vou pra casa vai faltando um pedaço
D9                      C9
Se eu fico eu venço, eu ganho pelo cansaço
A                    A/G
Dois olhos verdes da cor da fumaça
D9            C9
E o veneno da raça
E      D9     A/C#      E        D9 A/C#
Eu mereço um lugar ao sol, mereço

E              D9      A/C#      E     D9 A/C#
Ganhar pra ser carente profissional, carente
A                    A/G
Levando em frente um coração dependente
D9              C9
Viciado em amar errado
A                    A/G
Crente que o que ele sente é sagrado...
D9                   C9
E é tudo piada, tudo piada
E      D9      A/C#     E        D9 A/C#
Eu mereço um lugar ao sol, mereço
E               D9      A/C#      E    D9     A/C#      A
Ganhar pra ser, carente profissional, carente profissional.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A/G = 3 X 2 2 2 X
C9 = X 3 5 5 3 3
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
