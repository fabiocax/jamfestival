Cazuza - Certo Dia Na Cidade


 C
 Já nem sei quanto tempo faz
 Ele foi como quem se distrai
 F
 Viu na cor de um som a cor que atrai
C                                  Bb
 Foi num solo que não volta atrás
C
 Tchau, mãezinha, fui beijar o céu
 A vida não tem tamanho
   F
 Tchau, paizinho, eu vou levando fé
C
 É tudo luz e sonho
         F  Em  Dm   C
 É tudo luz  e  so...nho
 F   Em   Dm   C   F   Em   Dm   C   G   Dm
 Eu vou viver, vou sentir tudo
 Eu vou sofrer, eu vou amar demais
F              Em
 Ei, garoto, a força que me conduz

F
 É leve e é pesada
       Em
 É uma barra de ferro jogada no ar
F         Em        F
 Eu vou levando fé
                G
 Eu vou levando fé

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
