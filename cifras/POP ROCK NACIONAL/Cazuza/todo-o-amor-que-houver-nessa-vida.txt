Cazuza - Todo o Amor Que Houver Nessa Vida

[Intro] Gm  F  Gm  F
        Gm  F  Gm  F
        Gm  F  Gm  F
        Gm  F  Gm  F

Gm                         Eb
  Eu quero a sorte de um amor tranqüilo
Cm                  F7
  Com sabor de fruta mordida
Gm           Eb         Cm
  Nós, na batida, no embalo da rede
   Eb                Ab
Matando a sede na saliva
D4/7
  Ser teu pão, ser tua comida
F4/7
  Todo amor que houver nesta vida
Am     Eb7                      Gm  F  Gm  F
  E algum trocado pra dar garantia
Gm                  Eb
  E ser artista no nosso convívio

Cm7                      F7
  Pelo inferno e céu de todo dia
Gm          Eb                Cm
  Pra poesia que a gente não vive
         Eb                 Ab
Transformar o tédio em melodia

D4/7
  Ser teu pão, ser tua comida
F4/7
  Todo amor que houver nesta vida
Am     Ab7   Gm              F  Gm  F
  E algum veneno anti monotonia

Gm                     Eb
  E se eu achar a sua fonte escondida
Cm                       F7
  Te alcanço em cheio o mel e a ferida
Gm     Eb                Cm
  E o corpo inteiro como um furacão
       Eb                   Ab
Boca, nuca, mão e a tua mente, não

D4/7
  Ser teu pão, ser tua comida
F4/7
  Todo amor que houver nessa vida
  Am       Ab7          Gm
E algum remédio que me dê alegria
  Am       Ab7          Gm
E algum remédio que me dê alegria

( Gm  F  Gm  F )

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Ab7 = 4 6 4 5 4 4
Am = X 0 2 2 1 0
Cm = X 3 5 5 4 3
Cm7 = X 3 5 3 4 3
D4/7 = X X 0 2 1 3
Eb = X 6 5 3 4 3
Eb7 = X 6 5 6 4 X
F = 1 3 3 2 1 1
F4/7 = 1 3 1 3 1 X
F7 = 1 3 1 2 1 1
Gm = 3 5 5 3 3 3
