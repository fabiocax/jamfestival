Cazuza - Esse Cara

Intro: Bb

Bb7+        Am7  D7            Gm7
Ah, esse cara tem      me consumido
Fm7     Bb7             Eb7+
A mim e a tudo que eu quis
   Abm7     Db7       Gb7+
Com seus olhinhos infantis
Gm7      C7              F7
Com os olhos de um bandido
Bb7+      Am7   D7           Gm7
Ah, esse cara tem    me consumido
Fm7    Bb7              Eb7+
A mim e a tudo que eu quis
    Abm7     Db7       Gb7+
Com seus olhinhos infantis
       Gm7     C7      F7
Com os olhos de um bandido
   Fm7             Bb7+             Eb7+
Ele está na minha vida porque quer
         Ebm7              Ab7    Db7+
Eu estou para o que der e vier

   Dm7/5-    G7    Cm7
Ele chega ao anoitecer
         B7+
Quando vem a madrugada
    Bbm7    Eb7
Ele some
           Ab7+
Ele é quem quer
Abm7  Db7          Gb7+          F7+
Ele       é      um      homem     e eu     sou
                 Bb7+
Apenas uma mulher

----------------- Acordes -----------------
Ab7 = 4 6 4 5 4 4
Ab7+ = 4 X 5 5 4 X
Abm7 = 4 X 4 4 4 X
Am7 = X 0 2 0 1 0
B7+ = X 2 4 3 4 2
Bb = X 1 3 3 3 1
Bb7 = X 1 3 1 3 1
Bb7+ = X 1 3 2 3 1
Bbm7 = X 1 3 1 2 1
C7 = X 3 2 3 1 X
Cm7 = X 3 5 3 4 3
D7 = X X 0 2 1 2
Db7 = X 4 3 4 2 X
Db7+ = X 4 6 5 6 4
Dm7/5- = X X 0 1 1 1
Eb7 = X 6 5 6 4 X
Eb7+ = X X 1 3 3 3
Ebm7 = X X 1 3 2 2
F7 = 1 3 1 2 1 1
F7+ = 1 X 2 2 1 X
Fm7 = 1 X 1 1 1 X
G7 = 3 5 3 4 3 3
Gb7+ = 2 X 3 3 2 X
Gm7 = 3 X 3 3 3 X
