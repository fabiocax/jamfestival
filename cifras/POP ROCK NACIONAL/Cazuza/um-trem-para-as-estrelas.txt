Cazuza - Um Trem Para As Estrelas

[Intro] C7M  C7M/B

C7M                      C7M/B
   São sete horas da manhã
C7M                    C7M/B
   Vejo o Cristo da janela
C7M                 C7M/B
   O sol já apagou sua luz
C7M                       C7M/B
   E o povo lá embaixo espera
C7M                 C7M/B
   Nas filas de pontos de ônibus
C7M                 C7M/B
   Procurando aonde ir

C7M                    C7M/B
   São todos seus cicerones
 C7M                 C7M/B
Correm pra não desistir
Am7                      Gm7  G#m7
   Dos seus salários de fome

Am7                        Gm7
   E a esperança que ele têm
Fm7                 Eb7M
   Nesse filme como extras
Dm7                     C7M
   Todos querem se dar bem

                     Am7
Num trem para as estrelas
               Gm7
Depois dos navios negreiros
              F7M
Outras correntezas
                     Am7
Num trem para as estrelas
              Gm7
Depois dos navios negreiros
              F7M
Outras correntezas

( Am7   Gm7   F7M )

C7M                        C7M/B
   Estranho o teu Cristo, Rio
C7M                      C7M/B
   Que olha tão longe, além
C7M                       C7M/B
   Com os braços sempre abertos
C7M                      C7M/B
   Mas sem proteger ninguém
C7M                  C7M/B
   Eu vou forrar as paredes
C7M                    C7M/B
   Do meu quarto de miséria
C7M                     C7M/B
   Com manchetes de jornal
C7M                        C7M/B
   Pra ver que não é nada sério
Am7                      Gm7   G#m7
   Eu vou dar o meu desprezo
Am7                     Gm7
   Pra você que me ensinou
Fm7                        Eb7M
   Que a tristeza é uma maneira

Dm7                      C7M
   Da gente se salvar depois
                     Am7
Num trem para as estrelas
              Gm7
Depois dos navios negreiros
              F7M
Outras correntezas
                     Am7
Num trem para as estrelas
              Gm7
Depois dos navios negreiros
              F7M
Outras correntezas

( Am7  Gm7  F7M )
( Am7  Gm7  F7M )

                     Am7
Num trem para as estrelas
              Gm7
Depois dos navios negreiros
              F7M
Outras correntezas
                     Am7
Num trem para as estrelas
              Gm7
Depois dos navios negreiros
              F7M
Outras correntezas

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
C7M = X 3 2 0 0 X
C7M/B = X 2 2 0 0 0
Dm7 = X 5 7 5 6 5
Eb7M = X X 1 3 3 3
F7M = 1 X 2 2 1 X
Fm7 = 1 X 1 1 1 X
G#m7 = 4 X 4 4 4 X
Gm7 = 3 X 3 3 3 X
