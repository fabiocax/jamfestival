Lulu Santos - Como É Grande o Meu Amor Por Você

Intro: A7+ E7/5+/C# E B/C# C#7 F#9/6 D6/E E7
A7+ E7/5+/C# E B/C# C#7 F#m G#m6 A B9/Bb G5+ E7 A7 E7 C#7

                 A7+        E7/5+/C#
Eu tenho tanto pra lhe falar
           E   B/C#      C#7
Mas com palavras não sei dizer
        F#9/6   B7       E7+  F#7
Como é grande o meu amor por você
                 A7+        E7/5+/C#
E não há nada pra comparar
        E B/C#     C#7
Para poder lhe explicar
F#m G#m6   A    B9/Bb     B7           E7  A7  E7
Como  é  grande  o meu   amor por você
            F#7/9       B7
Nem mesmo o céu nem as estrelas
            E9/7+      C#7
Nem mesmo o mar e o infinito
      F#7/9             B7
Não é maior que o nosso amor

          E9/7+  C#7
Nem mais bonito
       F#7/9       B7
Me desespero a procurar
       E9/7+         C#7
Alguma forma de lhe falar
        E/F#   C#7/5+ F#7       B7  C7  B7  F°
Como é grande o meu  amor por você
           A7+             E7/5+/C#
Nunca se esqueça, nem um segundo
                 E B/C#       C#7
Que eu tenho o amor  maior do mundo
F#m  G#m6  A  B9/Bb         B7        E7  A7  E7 C#7
Como  é   grande o    meu  amor por você
Assobiando: A7+ E7/5+/C# E B/C# C#7 F#9/6 D6/E  E7
A7+ E7/5+/C# E B/C# C#7 F#m G#m6 A B9/Bb  G5+ E7 A7 E7

             F#7/9      B7
Nem mesmo o céu nem as estrelas
            E9/7+      C#7
Nem mesmo o mar e o infinito
      F#7/9           B7
Não é maior que nosso amor
          E9/7+  C#7
Nem mais bonito
       F#7/9       B7
Me desespero a procurar
       E9/7+         C#7
Alguma forma de lhe falar
        E/F#   C#7/5+ F#7       B7  C7  B7  F°
Como é grande o meu  amor por você
           A7+             E7/5+/C#
Nunca se esqueça, nem um segundo
                 E B/C#      C#7
Que eu tenho o amor  maior do mundo
F#m  G#m6  A  B9/Bb         B7       D/E  E7
Como  é   grande o    meu  amor por você
           A7+             E7/5+/C#
Nunca se esqueça, nem um segundo
                 E B/C#       C#7
Que eu tenho o amor  maior do mundo
F#m G#m6  A   B9/Bb     B7       E7 B7 E7 C#7
Como é   grande o meu  amor por você

Assobiando: A7+ E7/5+/C#  E B/C# C#7 F#9/6 D6/E E7
             A7+ E7/5+/C# E  B/C#  C#7
             F#m G#m6 A B9/Bb F#m G#m6 A B9/Bb F#m G#m6 A B9/Bb G5+ E7 A7 E7

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
B/C# = X 6 X 4 7 7
B7 = X 2 1 2 0 2
B9/Bb = 6 3 3 6 P7 7
C#7 = X 4 3 4 2 X
C#7/5+ = X 4 X 4 6 5
C7 = X 3 2 3 1 X
D/E = X X 2 2 3 2
D6/E = X 7 7 7 7 7
E = 0 2 2 1 0 0
E/F# = X X 4 4 5 4
E7 = 0 2 2 1 3 0
E7+ = X X 2 4 4 4
E9/7+ = X 7 6 8 7 X
F#7 = 2 4 2 3 2 2
F#7/9 = X X 4 3 5 4
F#m = 2 4 4 2 2 2
F° = X X 3 4 3 4
G#m6 = 4 X 3 4 4 X
G5+ = 3 X 1 0 0 X
