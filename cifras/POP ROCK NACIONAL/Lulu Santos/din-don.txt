Lulu Santos - Din Don

Intro 4x: G  D7/F#  F5-  Eb

G D7/F# Em7
        Tá tudo em cima
     Am7/9
muchacha menina
F|5-         Dm
    Tudo em cima
     Bb
pra gente ir na praia
   Dm
Eu vou mergulhando
G/Bb     G/B
eu vou andando
  E7/4     E7   E7/5+ F5-   Am
Nadando no sol, quei  mando
C/D
A pele preta

(Intro)


G D7/F# Em7
        Deito na areia
   Am7/9
na tua toalha
F|5-      Dm            Bb
    É um jeito de esquecer a batalha
    Dm                 G/Bb      G/B
Vem queimar comigo e esquece o perigo
  E7/4         E7   E7/5+ F5-    Am
O crime e o castigo da    onda
D7/4
Quando amanheço
C7/4
quando entardeço
D#7/4           G#
Quando acontece com você
Am      D7   G7+
Uma vontade imensa
D#º     Dm   Dm7/9   G7/6 G7/5+ G7
Já vai anoitecer

( C  G/B  Bb  Am7/4  G#  G/F )

(Intro)

(Segunda Parte)

(Intro)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7/4 = 5 X 5 5 3 X
Am7/9 = X X 7 5 8 7
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C/D = X X 0 0 1 0
C7/4 = X 3 3 3 1 X
D#7/4 = X X 1 3 2 4
D#º = X X 1 2 1 2
D7 = X X 0 2 1 2
D7/4 = X X 0 2 1 3
D7/F# = X X 4 5 3 5
Dm = X X 0 2 3 1
Dm7/9 = X 5 3 5 5 X
E7 = 0 2 2 1 3 0
E7/4 = 0 2 0 2 0 X
E7/5+ = 0 X 0 1 1 0
Eb = X 6 5 3 4 3
Em7 = 0 2 2 0 3 0
F5- = 1 3 3 4 1 1
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
G/B = X 2 0 0 3 3
G/Bb = X 1 0 0 3 3
G/F = 1 X X 0 0 3
G7 = 3 5 3 4 3 3
G7+ = 3 X 4 4 3 X
G7/5+ = 3 X 3 4 4 3
G7/6 = 3 X 3 4 5 X
