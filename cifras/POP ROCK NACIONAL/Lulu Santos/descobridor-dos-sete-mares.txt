Lulu Santos - Descobridor Dos Sete Mares

[Intro]  C#m  F#7/4  F#7   C#m  F#7/4  F#7 C#m  F#7/4  F#7

     C#m
Uma luz azul me guia
       F#7/4  F#7   C#m      F#7/4  F#7
Com a firmeza e os lampejos
    C#m  F#7/4  F#7
Do farol
        C#m   F#7/4  F#7
E os recifes lá de  cima
       C#m        F#7/4  F#7
Me avisam os perigos de
  C#m   F#7/4  F#7
Chegar
           C#m     F#7/4  F#7
Angra dos Reis e Ipanema
    C#m       F#7/4  F#7    C#m
Iracema e Itamaracá
         C#m    F#7/4  F#7
Porto Seguro e São  Vicente
         C#m
Braços abertos

            F#7/4  F#7
Sempre a esperar
           G#m7+  C#4  C#
Pois bem cheguei
                      F#m7
Quero ficar bem a vontade
       A/B
Na verdade eu sou
   G#m7+  C#4  C#
Assim
                      F#m7
Descobridor dos sete mares
     B       Bº
Navegar eu quero

C#m   F#7/4  F#7
Uma lua me ilumina
        C#m     F#7/4      F#7
Com a clareza e o brilho do
  C#m   F#7/4  F#7
Cristal
                C#m  F#7/4  F#7
Transando as cores  dessa  vida
       C#m    F#7/4  F#7
Vou colorindo a alegria de
  C#m  F#7/4  F#7
Chegar
        C#m    F#7/4  F#7
Boa viagem e Ubatuba
         C#m
Grumari mengo e
  F#7/4  F#7
Guarujá
          C#m   F#7/4  F#7
Praia vermelha, Ilha  Bela
         C#m
Braços abertos
       F#7/4  F#7
Sempre a esperar
      B7       E7+    D4+/9-
Pois bem, cheguei
                      A7+
Quero ficar bem a vontade
       A/F#       Gº
Na verdade eu sou
   E7+  D4+/9-
Assim
                      A7+
Descobridor dos sete mares
     A/F#    G#º   C#m  B9
Navegar eu quero

     C#m
Uma luz azul me guia
       F#7/4  F#7   C#m      F#7/4  F#7
Com a firmeza e os lampejos
 C#m  F#7/4  F#7
Do farol
        C#m   F#7/4  F#7
E os recifes lá de  cima
       C#m        F#7/4  F#7
Me avisam os perigos de
  C#m   F#7/4  F#7
Chegar

           C#m F#7/4  F#7
Angra dos Reis e  Ipanema
    C#m     F#7/4  F#7
Iracema e Itamaracá
    C#m   F#7/4    F#7
Portão e porto de galinhas
         C#m
Braços abertos
       F#7/4  F#7
Sempre a esperar
      G#m7+   C#m4  C#m
Pois bem cheguei
                      F#m7
Quero ficar bem a vontade
       A/B
Na verdade eu sou
   G#m7+  C#m4  C#m
Assim
                      F#m7
Descobridor dos sete mares
     B       Bº
Navegar eu quero

----------------- Acordes -----------------
A/B = X 2 2 2 2 X
A/F# = 2 0 2 2 2 0
A7+ = X 0 2 1 2 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
B9 = X 2 4 4 2 2
Bº = X 2 3 1 3 1
C# = X 4 6 6 6 4
C#4 = X 4 4 1 2 X
C#m = X 4 6 6 5 4
C#m4 = X 4 4 6 5 4
D4+/9- = X 5 4 7 4 4
E7+ = X X 2 4 4 4
F#7 = 2 4 2 3 2 2
F#7/4 = 2 4 2 4 2 X
F#m7 = 2 X 2 2 2 X
G#m7+ = X X 6 8 8 7
G#º = 4 X 3 4 3 X
Gº = 3 X 2 3 2 X
