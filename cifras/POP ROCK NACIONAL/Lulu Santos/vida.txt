Lulu Santos - Vida

        A7M                  F#m
Quando eu era pequeno eu achava a vida chata
       E4/7       E7
Como não devia ser
      Bm         G6       Bm          G6
Os garotos da escola só a fim de jogar bola

      ( Bm      E7              D     D/E )
E eu queria ir tocar guitarra na TV
Aí veio a adolescência e pintou a diferença
Foi difícil de entender
A garota mais bonita também era a mais rica
Me fazia de escravo do seu bel prazer
Quando eu saí de casa minha mãe me disse:
Baby, você vai se arrepender
Pois o mundo lá fora num segundo te devora
                                    D
Dito e feito, mas eu não dei o braço a torcer
D              E/D    D7M            E/D
Hoje eu vendo sonhos, ilusões de romance
  C#m         F#7/9-     Bm  C#m     Dm6
E toco a minha vida por um troco qualquer

        A7M                          C#m         F#7
É o que chamam de destino, e eu não vou lutar por isso
     Bm     C#m   Dm6  A7M
Que seja assim enquanto é

    Bm     C#m   Dm6
Que seja assim enquanto       (2x)
    Bm     C#m   Dm6   A7M
Que seja assim enquanto é

----------------- Acordes -----------------
A7M = X 0 2 1 2 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
D/E = X X 2 2 3 2
D7M = X X 0 2 2 2
Dm6 = X 5 X 4 6 5
E/D = X 5 X 4 5 4
E4/7 = 0 2 0 2 0 X
E7 = 0 2 2 1 3 0
F#7 = 2 4 2 3 2 2
F#7/9- = X X 4 3 5 3
F#m = 2 4 4 2 2 2
G6 = 3 X 2 4 3 X
