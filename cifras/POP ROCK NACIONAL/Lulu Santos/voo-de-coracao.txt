Lulu Santos - Vôo de Coração

Intro: C Dm7 C/E G4/7

C        D7       Fm     C/G
No canto da sala, no seu holograma
Dm/F  Dm5-/7    C  Dº
Você parece sorrir
C       D7            Fm      C/G        Dm/F
Você me pisca o olho, você me manda um beijo
         Dm5-/7     C
Parece estar mesmo aqui
       G                  F     C
Mas eu só no apartamento, escrevendo
  Dm7         C/G      G4/7   G7
Memórias num velho computador
C         D7        Fm       C/G
Nas ondas do tempo, vertigem do momento
Dm/F       Dm5-/7     C  E7
Vôo de coração, meu amor
Dm/F       Dm5-/7
Vôo de coração
Am    E/G#    Gm6        D/F#    Fm6
Um mistério e um sorriso vão revelar

      C             Dm7  Fm6
As histórias que um belo dia
  C                D7     Fm          C/G
Contarão nossos momentos, nesse apartamento
Dm/F       Dm5-/7    C    Dm7        Fm6  C
Vôo de coração, meu amor, vôo de coração

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
C/G = 3 3 2 X 1 X
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
Dm/F = X X 3 2 3 1
Dm5-/7 = X X 0 1 1 1
Dm7 = X 5 7 5 6 5
E/G# = 4 X 2 4 5 X
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
Fm6 = 1 X 0 1 1 X
G = 3 2 0 0 0 3
G4/7 = 3 5 3 5 3 X
G7 = 3 5 3 4 3 3
Gm6 = 3 X 2 3 3 X
