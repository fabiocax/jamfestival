Lulu Santos - Fico Assim Sem Você

[Intro] F  Am  Bb  C7

   F                     Am                            Bb                    C7
Avião sem asa,fogueira sem brasa sou  eu   assim sem você
   F                          Am                          Bb                    C7
Futebol sem bola, piu-piu sem frajola sou  eu assim sem você
             Bb                             C7
Por que  é  que tem que ser assim?
          C#º                        Dm
Se o meu desejo não tem fim
             G7
Eu te quero a todo instante nem mil alto-falantes
C7
Vão poder falar por mim
F                                 Am
Amor sem beijinho, buchecha sem claudinho
        Bb                   C7
Sou eu assim sem você
F                                Am                            Bb                   C7
Circo sem palhaço, namoro sem amasso sou eu assim sem você
        Bb                    C7
To louco pra te ver chegar

       C#º                       Dm
To louco pra te ter nas mãos
   G7
Deitar no teu abraço, retomar o pedaço
         C7
Que falta no meu coração

             Dm                 Am         Bb                              F      A7
Eu não existo longe de você e a solidão é o meu pior Castigo
                     Dm                     Am
Eu conto as horas pra poder te ver
                Bb                      C7
  Mas o relógio tá de mal comigo por que?    Iê iê iê iê
      F                           Am                         Bb                   C7
Neném sem chupeta, romeu sem julieta sou eu assim sem você
 F                            Am                                Bb                  C7
Carro sem estrada, queijo sem goiabada  Sou eu assim sem você
             Bb                            C7              C#º                       Dm
Por que  é  que tem que ser assim? Se o meu desejo não tem fim
            G7
Eu te quero a todo instante nem mil alto-falantes
  C7
Vão poder falar por mim

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C#º = X 4 5 3 5 3
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G7 = 3 5 3 4 3 3
