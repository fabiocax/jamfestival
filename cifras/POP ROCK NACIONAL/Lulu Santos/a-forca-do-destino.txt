Lulu Santos - A Força do Destino

G
Hoje fazem 20 anos
C
Que eles se encontraram
       G          C      D
Num arrasta! Oh, oh, ah, ah!
 G
Com som de vitrola a pilha
C                    G
Cuba-libre, hi-fi e gim-tônica
C         D
Oh, oh, ah, ah!

F                   Am
Fulminante neste instante
    C        D      G
O encontro que se deu
F                  C
E ali no meio do arrasta
Am     D      G
Uma chama ardeu


  G
E eles se amavam tanto
C                               G        C        D
Quanto, achavam, ninguém mais ousou, ah, ah, oh, oh!
 G
E que neste ou noutro mundo
  C                   G
Nada é maior que seu amor
     C       D
Ah, ah, oh, oh!

F                  Am
E fizeram tantos planos
C      D       G
Para mais adiante
F              Am
De abandonar aquela vida
C        D       G
Por um mundo grande
F            Am
E até hoje falam nisso
C     D      G
Sem sequer notar
F              Am
Com as mesmas pessoas
C     D    G
No mesmo lugar

Am                C
Só ninguém sabe explicar
                  D#° Em
Onde anda aquela chama
Am
Só ninguém lhes respondeu
C                D
O que é que se perdeu
         C         D
O que é que se perdeu
      G
Se perdeu

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D#° = X X 1 2 1 2
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
