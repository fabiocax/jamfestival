Lulu Santos - Vale de Lágrimas

Intro: C

C
Deixa eu lhe dizer
Am
O que eu passei
Em
Desde que você
           G        G F#
Se desapegou de mim
F                         Am
Eu zanzei pelas ruas, um mulambo Sonâmbulo,
    Am7     D
Insone e insano
                     G#  G
Queria me atirar no mar
                C
Só para me afogar
               Am
Que ainda é melhor
               Em
Que ser um devedor

                G        G F#
Nas contas do amor
F                               Am           Am7            D
Preferia um deserto atravessar Sob o sol e as noites sem luar
                           G#  G
Do que dar meu braço a torcer
            C
Que você não está

Que você não vem
              C
Faça-me um favor

Volta para mim
         Am            Em
É o que sei dizer, nada mais
       G       F#
Se não repetir
F                    Am
Que zanzei pelas ruas, um mulambo
 Am7                   D
Sonâmbulo, insone e insano
                      G#   G
Queria me atirar no mar
              C
Só pra me afogar
               Am
Que ainda é melhor
               Em
Que ser um desertor
                G   F#
Nos campos do amor
F                           Am                 Am7          D
Preferia um deserto atravessar Sob o sol e as noites sem luar
                           G# G
Do que dar meu braço a torcer
                    C
Que você não está

Que você não vem
              C
Faça-me um favor

Volta para mim

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
