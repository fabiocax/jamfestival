Lulu Santos - Questão de Estilo

  A
Não sei de ninguém que não quis provar, que prova e não pede bis
            D Dm           A F#7       Bm                            G
Também não sei de quem não dá, dá uma chance, pinta o lance, se é romance
        Ab        A
Já não dá pra largar
A
Não vá me dizer que você não deu, deu tudo pelo seu amor
          D Dm            A F#7           Bm                        G
Por meu amor, sou réu confesso, me apaixonei, me viciei, me intoxiquei
            Ab     A  A7
Mas sou um cara feliz
          D              Bb            A    A7
Me ajuda seu doutor, meu caso é muito sério
       D          Bb
Agora não é in, amar assim
    B7             F        G   A
Morrer de mal de amor já não é bem
F       G     A
É papo firme essa é a confusão que adotou
                                     D  Dm
O cara que só sabe amar, morrer de amor

         A                    F#7               Bm             A
Não é moderno, modelo do meu terno, logo que casei, gosto é assim
A7
Me ajuda....
A
Não dá pra entender quem quer condenar quilo que só dá prazer
         D Dm             A   F#7
Só pode ser questão de estilo
         Bm                           G
Dá uma chance, pinta o lance, se é romance
       Ab         A F#7              Bm
Já não dá pra largar diz que eu sou mal
                      G
Nariz de pau, paranormal
         Ab          A F#7        Bm                      G
Mas você vai me adotar, me apaixonei, me viciei, me intoxiquei
           Ab      A
Mas sou um cara legal

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Ab = 4 3 1 1 1 4
B7 = X 2 1 2 0 2
Bb = X 1 3 3 3 1
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
F#7 = 2 4 2 3 2 2
G = 3 2 0 0 0 3
