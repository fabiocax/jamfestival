Lulu Santos - Toda Forma de Amor / Um Certo Alguém / O Último Romântico (Pot-Pourri)

[Intro]

E|----------------------------------------------|
B|-----------------------------0----------------|
G|------0-----0h2---------0---------2--0--------|
D|-0-2-----2-------0--2-------------------2--0--|
A|----------------------------------------------|
E|----------------------------------------------|

G
Eu não pedi pra nascer
Em
Eu não nasci pra perder
D                             C
Nem vou sobrar de vítima das circunstâncias

G
Eu tô plugado na vida
Em
Eu tô curando a ferida
D                         C
Às vezes eu me sinto uma bala perdida


G
Você é bem como eu
Em
Conhece o que é ser assim
D                              C
Só que dessa história ninguém sabe o fim

G
Você não leva pra casa
Em
E só traz o que quer
D                         C
Eu sou um homem, me diz você qual é

G
E a gente vive junto
G5+
E a gente se dá bem
        C
Não desejamos mal
  Cm       D
A quase ninguém

G
 E a gente vai à luta
G5+
   E conhece a dor
        C
Consideramos justa
Cm      D       G
Toda forma de amor

G       B7       Em
Quis evitar teus olhos
       Em7+     F  Am
Mas não pude reagir
F         D7
Fico à vontade então

G        B7    Em
Acho que é bobagem
      Em7+     F  Am
A mania de fingir
F       D7
Negando a intenção

Am                   Cm
Quando um certo alguém
    G          Em
Cruzou o teu caminho
    Bm       D
E mudou a direção

G         B7        Em
Chego a ficar sem jeito
      Em7+           F  Am
Mas não deixo de seguir
F     D7
A tua aparição

Am                   Cm
Quando um certo alguém
   G            Em
Desperta o sentimento
  Bm               D
É melhor não resistir e se entregar

G       Em
Me dê a mão
    Bm           D
Vem ser a minha estrela
G          Em
Complicação
 Bm           D
Tão fácil de entender

G          Em
Vamos dançar,
  Bm          D
Luzir a madrugada
  G       Em
Inspiração
Bm          D
Pra tudo que eu viver

Am                   Cm
Quando um certo alguém
   G            Em
Desperta o sentimento
  Bm               D
É melhor não resistir e se entregar

( D Gm D Gm D F#m Bm F#m Gm )

E|------------------------------5--10--12/14--12/10--5--7/2--3/5--5--3-----3/5--5----|
B|---3--5--7--8--7--5--5/8--7-------------------------------------------6------------|
G|-----------------------------------------------------------------------------------|
D|-----------------------------------------------------------------------------------|
A|-----------------------------------------------------------------------------------|
E|-----------------------------------------------------------------------------------|

 D          F#m           Bm  F#m
Faltava abandonar a velha escola
   D           F#7  Bm  Am D7
Tomar o mundo feito  coca-cola
  Em          B7           Em         B7
Fazer da minha vida sempre o meu passeio público
     Em         C        Eb7       B7    D  Gm D Gm
E ao mesmo tempo fazer dela o meu caminho só, único

D          F#m            Bm   F#m
Talvez eu seja o último romântico
D          F#7         Bm  Am   D7
Dos litorais deste Oceano Atlântico
Em          B7           Em         B7
Só falta reunir a zona norte à zona sul
Em         C             Eb7
Iluminar a vida já que a morte cai do azul

   Bm        Bm7M
Só falta te querer
     Bm7         E7
Te ganhar e te perder
Em7
Falta eu acordar
         C                  F#7
Ser gente grande pra poder chorar

         Em     D A7
Me dá um beijo, então
       D F#m  G
Aperta minha mão
Em      F#7        Bm           B7
Tolice é viver a vida assim sem aventura
Em   D  A7
Deixa ser
D    F#7   G
Pelo coração
Em           D  A7  Bb7          C7    D7
Se é loucura então melhor não ter razão

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
Bb7 = X 1 3 1 3 1
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
Bm7M = X 2 4 3 3 2
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
Eb7 = X 6 5 6 4 X
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
Em7+ = X X 2 4 4 3
F = 1 3 3 2 1 1
F#7 = 2 4 2 3 2 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G5+ = 3 X 1 0 0 X
Gm = 3 5 5 3 3 3
