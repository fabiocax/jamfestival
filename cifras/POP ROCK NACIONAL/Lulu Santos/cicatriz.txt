Lulu Santos - Cicatriz

Ninguém me garante que eu vou ser feliz
Mas ninguém me impede de tentar sê-lo

Dm       C
Cê foi cruel comigo
Dm C                                                 G
  Justo na hora em que eu mais precisava sempre de abrigo

F              C  Dm    C                Am          D     B7
Parece até castigo.. será que a vida precisa sempre rimar
Em           C
Teu texto é idôneo e correto
    Em                  A7
Me sobra o papel do imbecil completo
C            Am                     D   B7
Te ouvindo dizer você não sabe me amar
  Em         C
Você só anda dentro da lei
  Em                      A7
E no teu livro o resto é bofe gay
C                 Am     D        B7
Você faz tudo parecer tão simples

     G                        F
Ninguém me garante que eu vou ser feliz
         C                   G
Mas ninguém me impede de tentar sê-lo
F               C    Dm C                     Am
Eu fui cruel contigo,   justo na hora em que você
                  G
Recobrava os sentidos
F                  C               Dm
Eu  te afoguei no líquido, líquido, líquido
      C                     Am         D    B7
Desse pântano escuro que eu chamo de amor
 Em                  C
Eu sempre choro faço cara de vítima
  Em                  A7
A tua paciência pode até ser infinita
        C          Am            D   B7
Mas eu reconheço é triste me aturar
          Em                    C
Porque eu pego, pego, pego pego no teu pé
    Em                 A7               C
Eu quero saber quem é mais que você me quer
              Am     D   B7
Você faz um olhar aflito
    G                         F
Ninguém me garante que eu vou ser feliz
         C                   G
Mas ninguém me impede de tentar sê-lo

(solo)

 Em          C
Teu texto é idôneo e correto
    Em                   A7           C            Am
Me sobra o papel do imbecil completo te ouvindo dizer
                   D   B7
Você não sabe me amar

 Em                    C
Porque eu pego, pego, pego pego no teu pé
    Em                 A7               C
Eu quero saber quem é mais que você me quer
              Am     D   B7
Você faz um olhar aflito
    G                         F
Ninguém me garante que eu vou ser feliz
         C                   G
Mas ninguém me impede de tentar sê-lo

É mole..

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
