Lulu Santos - Melô do Amor

(G   C7+)  F    Bb7+   Ab__Bb

Cm7              Dm7    Bb    Cm7    Dm7
Tudo o que eu cantar   aqui   agora
Cm7        Dm7      C#m7    Cm7    Ab__Bb
Você pode pensar que  ja  ouviu
Cm7                 Dm7    Bb     Cm7    Dm7
Pois é sobre o que sempre  se renova
Cm7             Dm7    C#m7    Cm7     Bb7+
Toda vez que a sorte   lhe  sorriu

G                           C7+
Dê um jeito de telefonar, apareça
G                   C7+
Desejo ouvir a sua voz
F7                           Bb7+
Recordando como a gente era feliz

G                              C7+
Preciso tanto lhe ouvir soar novamente
G                  C7+
No som do meu receptor

F7                                 Bb7+  Ab__Bb
Nem que seja nessa antiga melô do amor

Cm7   Dm7   Bb     Cm7   Dm7
Uhhhh.....
Cm7   Dm7   C#m7   Cm7   Ab__Bb
Uhhhh.....

Cm7              Dm7    Bb    Cm7    Dm7
Tudo o que eu cantar   aqui   agora
Cm7        Dm7      C#m7    Cm7    Ab__Bb
Você pode pensar que  ja  ouviu
Cm7                 Dm7    Bb9    Cm7    Dm7
Pois é sobre o que sempre  se renova
Cm7             Dm7    C#m7    Cm7     Bb7+
Cada vez que a sorte   lhe  sorriu

G                         C7+
Dê um jeito de telefonar, apareça
G                   C7+
Desejo ouvir a sua voz
F7                           Bb7+
Recordando como a gente era feliz

G                               C7+
Preciso tanto lhe ouvir soar novamente
G                   C7+
No som do meu receptor
F7                                 Bb7+  Ab__Bb
Nem que seja nessa antiga melô do amor

Cm7   Dm7   Bb     Cm7   Dm7
Uhhhh.....
Cm7   Dm7   C#m7   Cm7   Bb.
Uhhhh.....

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bb7+ = X 1 3 2 3 1
Bb9 = X 1 3 3 1 1
C#m7 = X 4 6 4 5 4
C7+ = X 3 2 0 0 X
Cm7 = X 3 5 3 4 3
Dm7 = X 5 7 5 6 5
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
G = 3 2 0 0 0 3
