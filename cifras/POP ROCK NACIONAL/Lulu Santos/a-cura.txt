Lulu Santos - A Cura

[Intro]  D  Bm  A7  G

E|-5-2------10------3/2-0---|
B|-----3--8----8-10-------3-|
G|--------------------------|
D|--------------------------|
A|--------------------------|
E|--------------------------|

D    Bb    Bm              Em
Existirá,  em todo porto tremulará  (se hasteará)
D           Bm    F#m    A7
A velha bandeira da vida
D      Bb   Bm        Em
Acenderá   todo farol iluminará
D          A7        D   G
Uma ponta de esperança

D     Bb    Bm            Em
E se virá, será quando menos se esperar
 D           Bm     F#m    A7
De onde ninguém imagina

 D     Bb  Bm        Em
Demolirá, toda certeza vã, não sobrará
 D           A7     D   G
Pedra sobre pedra

Em       Bm            D          A7
Enquanto isso não nos custa insistir
        Em       Bm        D           A
Na questão do desejo, não deixar se extinguir
 C    G        A       Bm
Desafiando de vez a noção
 E7                     G         A7
Na qual se crê que o inferno é aqui
D      Bb
Existirá
Bm             Em
E toda raça então experimentará
 D        A         G
Para todo o mal, a cura

D  G    A   G
   Iê, iê, na-na-ná

D     Bb   Bm             Em
 Existirá, em todo porto se hasteará

D            Bm    F#m    A7
 A velha bandeira da vida

D     Bb    Bm        Em
 Acenderá, todo farol iluminará


D          A7     D    G
Uma ponta de esperança


D  G    A   G
   Iê, iê, na-na-ná

Em       Bm            D          A7
Enquanto isso não nos custa insistir
        Em       Bm           D           A
Na questão do desejo, não deixar se extinguir
 C    G        A       Bm
Desafiando de vez a noção
 E7                     G         A7
Na qual se crê que o inferno é aqui
D      Bb
Existirá
Bm             Em
E toda raça então experimentará
 D        A         G
Para todo o mal, a cura

D  G    A   G
   Iê, iê, na-na-ná

F5  E5  C5  D

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bb = X 1 3 3 3 1
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C5 = X 3 5 5 X X
D = X X 0 2 3 2
E5 = 0 2 2 X X X
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
F5 = 1 3 3 X X X
G = 3 2 0 0 0 3
