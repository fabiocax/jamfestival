CPM 22 - Por quê?

Intro: A  E  D  E  F#  D

A                                           F#  B
 Quanto mais me preocupar com as coisas que acontecem
A                                           F#  B
 Quanto mais eu fracassar nos problemas que aparecem
D                    A                    D
 Eu não teria mais motivos pra viver, mas eu não me preocupo
           E
porque não penso assim

   A          D       F#         E
As vezes, um dia, até pode acontecer
    A         D         F#           E
São coisas da vida, não tem o que fazer
   A         D           F#       E
Só quero acordar e ver o dia clarear
    A             D       F#          E
São coisas que um dia talvez irá esquecer
   F#               E     F#             E         B  C#  D  A  E
Ninguém tem nada a ver, resolva seus problemas     B  C#  D


A                                           F#  B
 Quanto mais me preocupar com as coisas que acontecem
A                                          F#  B
 Quanto mais eu fracassar no problemas que aparecem
D                       A
 Será que isso tudo um dia irá passar ?
     D                         E
Por quê na vida sempre tem que ser assim ?

   A          D       F#         E
As vezes, um dia, até pode acontecer
    A         D         F#           E
São coisas da vida, não tem o que fazer
   A         D           F#       E
Não há mais do que nada que eu não possa superar
    A             D       F#          E
São coisas que um dia talvez irá esquecer
   F#               E     F#             E         B  C#  D  A  E
Ninguém tem nada a ver, resolva seus problemas     B  C#  D

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C# = X 4 6 6 6 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
