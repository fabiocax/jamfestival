CPM 22 - Irreversível

(intro) Bb5 Am5 C5 (2x)
        Bb5 Am5 G5)
(solo) F5 Am5 Bb5 C5

E|--------------------------------------8-8-8-8|
B|-6-6-6-8-8-8-8-8-8--6-6-6-8-8-8-8-8-8--------|
G|---------------------------------------------|
D|---------------------------------------------|
A|---------------------------------------------|
E|---------------------------------------------|

Bb5    C5         F5
A vida me sorri então
Bb5           C5           F5
Recolho os cacos que deixei no chão
Bb5         C5
Milhares de recordações
F5          D5           Bb5        C5
transformam tudo em canções
E essa daqui é pra você!


F5       Am5        Bb5
se eu pudesse desfazer
          C5          F5
Tudo de errado entre nós
      Am5           Bb5
e apagar cada lembrança sua
     C5
que ainda existe em mim
D5          C5           Bb5
Eu sei que nada que eu diga vai trazer
D5         C5            Bb5
O longe pra mais perto de mim dessa vez
D5         C5            Bb5
por que gostar de alguém vai ser sempre assim
D5  C5         Bb5
           Irreversívellllll

(intro)

Bb5     C5         F5
A vida ri de mim então
Bb5          C5            F5
Percebo o quanto é triste te esperar em vão
Bb5        C5              F5
Mas acho forças pra cantar
                D5            Bb5    C5
Quem sabe você possa me escutar
Eu só queria te dizer!

F5       Am5        Bb5
se eu pudesse desfazer
          C5          F5
Tudo de errado entre nós
      Am5           Bb5
e apagar cada lembrança sua
     C5
que ainda existe em mim
D5          C5           Bb5
Eu sei que nada que eu diga

Vai trazer
D5      C5            Bb5
O longe pra mais perto de mim dessa vez
D5        C5            Bb5
por que gostar de alguém vai ser sempre assim
D5  C5         Bb5
           Irreversívellllll

Bb5
A cada passo que eu dou pra frente
C5
Sinto o meu corpo indo pra trás
D5
e a cada hora que vivo sem sentido
Bb5
Parece me fazer te querer cada vez mais
Bb5
Eu trago em mim apenas um sorriso
C5
Braços abertos pra te receber
D5
Mas acabo sempre triste e sozinho
C5
Procurando uma maneira de entender
Bb5
Se é irreversível para mim
C5
então é reversível pra você
D5
se tudo tem que ser assim
Bb5           (É melhor ouvindo nessa parte, é só sequir a sequência da musica Bb5 C5 D5 ...)
então deixa ser.

mas só queria te dizer

F5       Am5        Bb5
se eu pudesse desfazer
          C5          F5
Tudo de errado entre nós
      Am5           Bb5
e apagar cada lembrança sua
     C5
que ainda existe em mim
D5          C5           Bb5
Eu sei que nada que eu diga

Vai trazer
D5        C5            Bb5
O longe pra mais perto de mim dessa vez
D5        C5            Bb5
por que gostar de alguém vai ser sempre assim
D5  C5      Bb5
Irreversível é só o fim pra mim.

( Bb5 Bb5 Bb5 Am5 Am5 Am5 C5   C5 C5 C5 C5 C5 C5 )
                (3x)
E|-1--1--1--1--1--1-|-------8-------|
B|------------------|-5-5-----7-5-7-|
G|------------------|-----7---------|
D|------------------|---------------|
A|------------------|---------------|
E|------------------|---------------|

----------------- Acordes -----------------
Am5 = X 0 2 2 1 0
Bb5 = X 1 3 3 X X
C5 = X 3 5 5 X X
D5 = X 5 7 7 X X
F5 = 1 3 3 X X X
G5 = 3 5 5 X X X
