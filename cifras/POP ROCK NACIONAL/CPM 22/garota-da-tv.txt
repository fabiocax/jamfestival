CPM 22 - Garota da TV

Intro: A, E, F#, D

(riff 1) introdução (8 vezes)........verso 2 e refrão tb riff 1
          A              E                F#m               D
E||------------------------------------------------------------------||
B||------------------------------------------------------------------||
G||-----------------9-9-9-9-9-9-9---------------------7-7-7-7-7-7-7--||
D||--7-7-7-7-7-7----7-7-7-7-7-7-7----4-4-4-4-4-4-4----7-7-7-7-7-7-7--|| 
A||--7-7-7-7-7-7----7-7-7-7-7-7-7----4-4-4-4-4-4-4----5-5-5-5-5-5-5--||
E||--5-5-5-5-5-5---------------------2-2-2-2-2-2-2-------------------||


(solo 1)intro (8 vezes)
E||-------------||
B||-------------||
G||---6-7-9-7-6-||
D||-7-----------||
A||-------------||
E||-------------||



(riff 2) verso 1
        A             F#m              D
E||-------------------------------------------||
B||-------------------------------------------||
G||-------------------------------7-7-7-7-7-7-||        (2 VEZES)
D||-7-7-7-7-7-7----4-4-4-4-4-4----7-7-7-7-7-7-||
A||-7-7-7-7-7-7----4-4-4-4-4-4----5-5-5-5-5-5-||
E||-5-5-5-5-5-5----2-2-2-2-2-2----------------||


solinho 2
e||--------------||
B||----10-10-10~-||
G||-13-----------||
D||--------------||
A||--------------||
E||--------------||


(riff 3) final

        parte 1  (3 vezes)        parte 2
E||---------------------------|-------------||
B||---------------------------|-------------||
G||-------7--------7--------7-|---9-----7---||
D||-7-7-4-7--7-7-4-7--7-7-4-7-|-4-7-4-7-7---||
A||-7-7-4-5--7-7-4-5--7-7-4-5-|-4-7-4-7-5---||
E||-5-5-2----5-5-2----5-5-2---|-2---2-5-----||


intro: batera, riff 1 e solo 1 ....................1ª vez

A         F#m                D                          :      (verso 1)
DE TUDO AQUILO, EU NÃO TINHA NADA....RESPOSTA NÃO DADA  :
A     F#m          D                                    :       (riff 2)
FOI ASSIM QUE COMEÇOU....DIAS CINZENTOS                 :


A    E         F#m                        :
ONDE NADA ME FARIA                        :               (verso 2)
     D              A                     :
ESQUEÇER AQUELE DIA                       :                (riff 1)
      E             F#m                   :
A RESPOSTA NÃO ME IMPORTA MAIS            :
E                                         :
EU NÃO SOU UM PERDEDOR                    :


(PARADA)
A, E, F#m, D  (2 vezes)(uma batida em kda nota...na 2ª vez, o solinho 2 entra)


A       E                  F#m            :
DIAS E MESES PENSANDO EM VOCÊ             :             (refrão)
      D              A                    :
PROCURANDO ENTENDER                       :             (riff1)
        E                  F#m            :
DIAS E MESES TENTANDO ENTENDER            :
          D                A              : (aqui nessa parte, o solo 1 tb é tocado)
O QUE EU SINTO POR VOCÊ                   :
             E                   F#m      :
TENTANDO ENTENDER, TENTANDO ENTENDER      :<-------------(2 vezes)

O QUE EU SINTO POR VOCÊ                   :


A, A, F#m, D :2X            :
                            :     (aqui só é tocado na 2ª vez)= final
F#m, F#m, E, F#m, A, D      :
GAROTA DA TV!               :

....:repete:....2ª vez

intro
verso 1
verso 2
parada
refrão - "tentando entender... ...o q eu sinto por vc" - 4 vezes
final: (riff 3)=parte 1(3 vezes).....(riff 3)=parte 2

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
