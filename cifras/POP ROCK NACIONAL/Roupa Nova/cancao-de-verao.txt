Roupa Nova - Canção de Verão

Intro: F7M E7M Eb7M E7M F7M (3x)
       F7M E7M Eb7M E7M F7M C7

E|-5h6-8-6-5-------5-----|
B|-----------8-6-8---6---|
G|-----------------------|
D|-----------------------|

E|-5h6-8-6-5-----10-11-8-|
B|-----------8-6---------|
G|-----------------------|
D|-----------------------|

F                 C/E
É como o sol de verão
            Bb/D   Bb
Queimando no peito
C4/7              C7              F7+  C7(4/9) C7/9
Nasce um novo desejo em meu coração
F             C/E
É uma nova canção

          Bb/D    Bb
Rolando no vento
C4/7                  C7             F7+  F7  F7/9
Sinto a magia do amor na palma da mão
     Bb        Bb7+
É verão, bom sinal
     Bb6    Bb7M     Bb        C/Bb      F7/9   F7
Já é tempo      de abrir o coração e sonhar-ah
     Bb        Bb7+
É verão, bom sinal
     Bb6    Bb7M      Bb       C/Bb         F4/7   F7
Já é tempo      de abrir o coração e sonhar-ah
Bb7M         F7M        Ab7M        Eb7M
PA PARARA  RAPA PARA RA PA RA RA RA
Bb7M         F7M        Ab7M        Eb7M
PA PARARA  RAPA PARA RA PA RA RA RA
      C4/7 C7
e sonhar
      D4/7 D7      (Bm   F#m  Am  G)
E sonhar

----------------- Acordes -----------------
Ab7M = 4 X 5 5 4 X
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bb/D = X 5 X 3 6 6
Bb6 = X 1 3 0 3 X
Bb7+ = X 1 3 2 3 1
Bb7M = X 1 3 2 3 1
Bm = X 2 4 4 3 2
C/Bb = X 1 2 0 1 X
C/E = 0 3 2 0 1 0
C4/7 = X 3 3 3 1 X
C7 = X 3 2 3 1 X
C7(4/9) = X 3 3 3 3 3
C7/9 = X 3 2 3 3 X
D4/7 = X X 0 2 1 3
D7 = X X 0 2 1 2
E7M = X X 2 4 4 4
Eb7M = X X 1 3 3 3
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
F4/7 = 1 3 1 3 1 X
F7 = 1 3 1 2 1 1
F7+ = 1 X 2 2 1 X
F7/9 = X X 3 2 4 3
F7M = 1 X 2 2 1 X
G = 3 2 0 0 0 3
