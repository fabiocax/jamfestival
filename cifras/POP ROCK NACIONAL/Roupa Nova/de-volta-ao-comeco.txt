﻿Roupa Nova - De Volta Ao Começo

Capo Casa 3

E          A/E
E o menino com o brilho do sol na menina dos olhos
          E                             A/E
Sorri e estende a mão entregando o seu coração
                     E
E eu entrego o meu coração
           A/E                                      E
E eu entro na roda e canto as antigas cantigas de amigo irmão
                 A/E
As canções de amanhecer lumiar e escuridão
     A/B          Am/B                              E/B
E é como se eu despertasse de um sonho que não me  deixou viver
      A/B                                         E
E a vida explodisse em meu peito com as cores que eu não sonhei
     A/B                         Am/B                  E/B
E é como se eu descobrisse que a força esteve o tempo todo em mim
         A/B                                A          E
E é como se então de repente eu chegasse ao fundo do fim

A             E/B
De volta ao começo

A            E
Ao fundo do fim
A            E       A  E
De volta ao começo

----------------- Acordes -----------------
Capotraste na 3ª casa
A*  = X 0 2 2 2 0 - (*C na forma de A)
A/B*  = X 2 2 2 2 X - (*C/D na forma de A/B)
A/E*  = 0 X 2 2 2 0 - (*C/G na forma de A/E)
Am/B*  = 4 X 4 3 1 1 - (*Cm/D na forma de Am/B)
E*  = 0 2 2 1 0 0 - (*G na forma de E)
E/B*  = X 2 2 1 0 0 - (*G/D na forma de E/B)
