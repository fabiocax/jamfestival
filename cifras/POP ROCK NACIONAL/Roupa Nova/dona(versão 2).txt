Roupa Nova - Dona

(intro) G  Bm7(11) C9 D4(7/9)

 G   Bm7(11) C9
Dona
 D4(7/9)     G   Bm7(11) C9  D4(7/9)
Desses traiçoeiros
 G    Bm7(11) C9
Sonhos
 D4(7/9)     G   Bm7(11) C9  D4(7/9)
Sempre verdadeiros
 G   Bm7(11) C9
Dona
 D4(7/9)   G   Bm7(11) C9  D4(7/9)
Desses animais
 G   Bm7(11) C9
Dona
     D4(7/9)  G   Bm7(11) C9  D4(7/9)
Dos seus   ideais

 G     C/G      D5/G        C/G          G
Pelas ruas onde andas onde mandas todos nós

       C/G         D5/G       C/G       C
Somos sempre mensageiros esperando tua voz
        C6        C7M            C6             C
Teus desejos, uma ordem, nada é nunca, nunca é não
        C6           C7M         D4(7/9)
Porque tens essa certeza dentro do teu coração
 G       C/G          D5/G          C/G          G
Tã, tã, tã, batem na porta, não precisa ver quem é
        C/G        D5/G          C/G         C
Prá sentir a impaciência do teu pulso de mulher
      C6             C7M      C6            C
Um olhar me atira à cama, um beijo me faz amar
       C6              C7M          D4(7/9)        G   Bm7(11) C9  D4(7/9)
Não levanto, não me escondo porque sei que és minha dona

 A   C#m7 D
Dona
 E4(7/9)     A   C#m7 D  E4(7/9)
Desses traiçoeiros
 A  C#m7 D  E4(7/9)     A  C#m7 D  E4(7/9)
Sonhos     sempre verdadeiros
 A   C#m7 D E4(7/9)   A   C#m7 D  E4(7/9)
Dona       desses animais
 A   C#m7 D     E4(7/9) A   C#m7 D  E4(7/9)
Dona       dos seus  ideais

A        D/A            E/A          D/A           A
 Não há pedra em teu caminho, não há ondas no teu mar
        D/A            E/A           D/A       D
Não há vento ou tempestade que te impeçam de voar
         D6             D7M            D6           D
Entre a cobra e o passarinho, entre a pomba e o gavião
       D6             D7M          E4(7/9)         A
Ou teu ódio ou teu carinho nos carregam pela mão
      D/A        E/A        D/A        A
És a moça da cantiga, a mulher da criação
      D/A          E/A           D/A        D
Umas vezes nossa amiga, outras, nossa perdição
     D6            D7M             D6          D
O poder que nos levanta, a força que nos faz cair
         D6            D7M           E4(7/9)          A   C#m7 D     E4(7/9)
Qual de nós ainda não sabe que isto tudo te faz dona.

(parte instrumental) C  F7M/C  C7M(9)

     A   C#m7 D     E4(7/9)
Ohh dona.

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm7(11) = 7 X 7 7 5 X
C = X 3 2 0 1 0
C#m7 = X 4 6 4 5 4
C/G = 3 3 2 X 1 X
C6 = 8 X 7 9 8 X
C7M = X 3 2 0 0 X
C7M(9) = X 3 2 4 3 X
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
D/A = X 0 X 2 3 2
D4(7/9) = X 5 5 5 5 5
D5/G = X X 0 0 2 2
D6 = X 5 4 2 0 X
D7M = X X 0 2 2 2
E/A = X 0 2 1 0 0
E4(7/9) = X X 2 2 3 2
F7M/C = X 3 3 2 5 X
G = 3 2 0 0 0 3
