Roupa Nova - Terra do Amor

Intro: A G D A  A G D E

 A         E      D
Terras de santas diferenças
 A      E      D
De dores, desavenças
 A       E        D
De milionários e sonhadores
A      E        D
Em milhões de crenças
 A      E        D                  A
Todos querem um mundo melhor pra viver
 E       D
Com sua gente
 A      E      D
Todos querem sair da pior

  E
Quem aqui nasce tem que dar valor
D
E sentir um calor diferente

E
Quem aqui nasce tem que resolver
D           E
Todo dia agradecer

 A         E      D
Terra de praia e sol
 A        E      D
Belas mulheres, samba e futebol
 A         E    D
Do cristo redentor
           E      E/G#
Terra do amor - wo wo wo ...  2x

 A G D A  A G D E
REPETE TUDO UMA VEZ

SOLO SAX  A E D  4x

  E
Quem aqui nasce tem que dar valor
D
E sentir um calor diferente
E
Quem aqui nasce tem que resolver
D           E
Todo dia agradecer


 A         E      D
Terra de praia e sol
 A        E      D
Belas mulheres, samba e futebol
 A         E    D
Do cristo redentor
           E     E/G#   G D A
Terra do amor - wo wo wo ...  2x

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
G = 3 2 0 0 0 3
