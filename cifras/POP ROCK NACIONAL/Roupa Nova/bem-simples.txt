Roupa Nova - Bem Simples

Intro:  B9  A9  B9  A9

D        Bm7   F#m7      G7M   F#m7
Tudo bem simples,tudo natural
        G7M        D       G/D
Um amor moreno, fruto tropical
D        Bm7  F#m7          G7M    F#m7
Todas as cores que eu puder te dar
          G7M             D         Am7 D7
Toda a fantasia que eu puder sonhar
Gm7    C7(9)   F7M       Bb7+   Dm/B
Eu pensei te dizer tanta coisas
           E7              A7M   Em7 A7
Mas prá quê,se eu tenho a música
D        Bm7      F#m7
Bom é bem simples
              G7M    F#m7
Sem nos complicar
            G7M             B9 A9 B9 A9
E bastante tempo prá te amar

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7M = X 0 2 1 2 0
A9 = X 0 2 2 0 0
Am7 = X 0 2 0 1 0
B9 = X 2 4 4 2 2
Bb7+ = X 1 3 2 3 1
Bm7 = X 2 4 2 3 2
C7(9) = X 3 2 3 3 X
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Dm/B = X 2 X 2 3 1
E7 = 0 2 2 1 3 0
Em7 = 0 2 2 0 3 0
F#m7 = 2 X 2 2 2 X
F7M = 1 X 2 2 1 X
G/D = X 5 5 4 3 X
G7M = 3 X 4 4 3 X
Gm7 = 3 X 3 3 3 X
