Roupa Nova - Amo Em Silêncio (Silence Is Golden)

Intro: C  G7  (Riff 1)
Riff 1:
E|---1-0--------------|
B|-------3----1-0-1---|
G|--------------------|
D|--------------------|
A|--------------------|
E|--------------------|

Riff 2:
                    C
E|---------------0---|
B|---------------1---|
G|----0-2-0------0---|
D|--2-------2-0--2---|
A|---------------3---|
E|---------------0---|

Riff 3:
                                  G#
E|-----------------------------4--|
B|--------------------------0--4--|
G|--------0--------------------5--|
D|----0-2---3-2-0-------0-2----6--|
A|--3-------------3---3--------6--|
E|-----------------------------4--|


C          F           C        F
Guardo no peito esse amor por você
        C           G        C   G7
E a canção que eu quero cantar
C      Dm     C/E     F
É um poema deixado no ar
       C        G        C
São palavras querendo viver

(Riff 2)

C         Em    Dm        G    C
Amo em silêncio    no meu coração
C        Em      Am
Amo em silêncio, amo
Dm        G     C
Guardo essa paixão

C              F          C      F
Sou como a estrela que o dia não vê
       C      G        C   G7
Que espera a noite chegar
C         Dm        C/E    F
Hoje meu mundo é só eu e você
         C             G           C
E é nos sonhos que eu vou te encontrar

C        Em        Dm     G   C
Amo em silêncio    no meu coração
C        Em      Am
Amo em silêncio, amo
Dm        G     C
Guardo essa paixão

( C  G7  C  G# ) (Riff 3)

C#      F#         Db/F           F#
Meu coração não quer mais se guardar
        C#     G#        C# G#
Bate forte e quer ter você
C#          D#m        Db/F   F#
Sou como o sol que precisa nascer
          C#       G#       C#
E nas sombras não pode brilhar

C#       Fm      D#m      G#   C#
Amo em silêncio    no meu coração
C#       Fm      Bm
Amo em silêncio, amo
D#m      G#     C#
Guardo essa paixão
D#m       G#     C#
Guardo essa paixão
D#m     G#   C#
No meu coração

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C# = X 4 6 6 6 4
C/E = 0 3 2 0 1 0
D#m = X X 1 3 4 2
Db/F = X 8 X 6 9 9
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
G7 = 3 5 3 4 3 3
