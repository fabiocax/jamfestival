Roupa Nova - Betty e Lou

Intr.: (A)

A           D          A    G
Betty fala bem três idiomas
A              D        E
Lou só fala a língua do amor
A           D          A     G
Betty só depois do casamento
A            D         E
Lou me ama até no elevador
C#m      F#m             C#m
É um jantar na casa dos pais
      F#m
No sofá da sala
 D           A           E
Betty deixa tudo quase azul...
C#m      F#m      C#m
Só isso não me satisfaz
       F#m
Eu não vou pra casa
 D         A             E D A/C#
Toda madrugada acaba em Lou

 D   E
Sei não...

A            D            E
Ah, se eu tiver que escolher
         F#m       E     D
Ir pra Bahamas ou pro Polo Sul
A          E      D          A
Se por um lado a Betty me agrada
 B                     E  D E
Por outro eu gosto da Lou
A                 D      E
Ah, se eu tiver que escolher
          F#m      E     D
Ir pra Bahamas ou pro Polo Sul
A          E      D          A
Se por um lado a Betty me agrada
 E               A         Intr.
Por outro eu gosto da Lou

A            D            A    G
Betty tem ouvidos pra Bethoven
A               D         E
Lou arranha os discos dos Stones
C#m      F#m           C#m
É, cada qual tem sua emoção
      F#m
É questão de estilo
  D             A            E
E eu sou meio chique, meio blue
C#m      F#m          C#m
São sentimentos tão desiguais
    F#m
Do amor ao cio
   D             A              E  D A/C#
A soma dos meus lados: Betty e Lou
 D   E
Sei não...


Refrão várias vezes

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
