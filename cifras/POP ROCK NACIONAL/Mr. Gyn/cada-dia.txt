Mr. Gyn - Cada dia

Intro.: Em - D (3x) - C9


[Obs.: Seguir essa sequência de notas até o refrao. ]

G                      D                C9
Sem tempo pra pensar , sem rumo sem lugar
Histórias pra contar , pra tentar explicar
Jovem demais ,errei conheci o mundo eu sei
E nada me fez esquecer , e tudo que hoje eu sou
Eu conquistei só pra você.

Em - D - C9
Há pouco tempo a gente foi feliz  , viveu na lua
Vamos jogar pra lá a decisão agora é sua .

Refrão: (Em , D , C9 ) Repete 5X

O nosso final nunca existiu , o ele entre nós a
vida nao partiu
E só caminharmos juntos sem olhar pra traz

E nos termos ,cada dia , um pouco mais .

----------------- Acordes -----------------
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
