Mr. Gyn - Quanto Tempo

A9
QUANTO TEMPO AGENTE LEVOU?
E
PRA SE OLHAR SE OLHAR E PRA SE DESCOBRIR
A9
QUANTAS VEZES VOCÊ ME NEGOU
E
UMA CHANCE PARA ME OUVIR

            D                            A
E NÃO POSSO FICAR, TANTO TEMPO ASSIM
A9
OUTRO DIA OUVI ALGUÉM DIZENDO
E
QUE O AMOR AS VEZES ENLOUQUECE
A9
TUDO O QUE AGENTE ESTAVA VIVENDO
E
VOCÊ AGORA FINGE QUE ESQUECE
D                                            A        E
E NÃO POSSO FICAR, TANTO TEMPO ASSIM


A                                                 E
QUANTO TEMPO SE PRECISA TER
     D                                    E
PARA SABER O QUE SE SENTE ?
A                                                E
QUANTO TEMPO SE PRECISA TER
        D                                               E
PARA SABER O QUE VAI SER DA GENTE
D                                                           Dm7
PRECISO SABER, PRA ME RESOLVER

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
D = X X 0 2 3 2
Dm7 = X 5 7 5 6 5
E = 0 2 2 1 0 0
