Mr. Gyn - Vira Amor Depois

INTRO: G  Am  C  D

 G                             Am
Tudo que começa lindo vai crescendo,
        C             D
amadurecendo e vira amor depois

 C                    G
Tudo cinza tudo tão igual,
   C                     G
Michel acessa a rede social (em seu computador)
C                         G
De repente, um encontro fatal,
Am                   D
uma menina anjo o paralisou
   C                       G
Clicou logo na foto dela, sua vida pesquisou
 Am                                  D
Viajou em tantos sonhos e com outro clique se apresentou.

 G                             Am
Tudo que começa lindo vai crescendo,

        C             D
amadurecendo e vira amor depois

C                    G
Ana Paula aceitou o tal,
    C                    G
desconfiada mais achou legal (tudo que ele falou)
      C                       G
O primeiro encontro foi especial,
Am                              D
pois pro seu aniversário ele a convidou
     C                        G
Ana Paula não foi sozinha, e duas amigas levou (o corpo bambeou)
Am                            D
Michel não perdeu tempo e um beijo dela roubou

 G                             Am
Tudo que começa lindo vai crescendo,
        C             D
amadurecendo e vira amor depois

C                     G
Viajaram juntos nada mal,
C                             G
já na estrada risos e alto astral
C                   G
Mr.Gyn no volume total
     Am                             D
e a trilha dessa historia um para o outro então cantou

  G                           Am                     C
" O melhor lugar do mundo é a sua companhia, ah meu Deus
                         D
Como eu queria e quero você perto de mim

       G
até o fim "

OBS.: O Anderson toca com capotraste, se quiser usar é só transpor a música em 2 tons e meio abaixo.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
