Mr. Gyn - A Minha Paz

[Intro]  D  G  D  A  D  G  D  A

 D                                    G
Mais um dia vem chegando passo as horas a contar
Bm                            G  A
Quanto tempo se passou desde que você se foi
D                                G
Passo as mãos pelos cabelos procurando algum sinal
Bm                                G   A
De que essa longa viagem está chegando a um final

Bm            G9           D         A
Mais eu não sei como chegar até você já não posso esperar
Bm         G9         D        A
Essa ilusão que me deixou preso em você

D        A           Bm           G9
Vai buscar a minha paz que eu perdi a muito
A         D
Tempo atrás
          A          Bm     G9          A
Já não posso mais viver, viver se não for com

   D
Você

[Final]  D  G  D  A  D  G  D  A

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
G = 3 2 0 0 0 3
G9 = 3 X 0 2 0 X
