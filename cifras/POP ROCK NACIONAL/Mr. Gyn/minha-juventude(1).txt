Mr. Gyn - Minha Juventude

Intro: A Bm F#m D A Bm E

A       B       F#m
EU TINHA APENAS 8 ANOS
        D              A         B              E
QUANDO NÓS NOS CONHECEMOS LÁ NA RUA DA MINHA CASA
A              B           F#m
A GENTE BRINCAVA E APRONTAVA
        D                 A         B      A
E ATÉ ME MATRICULEI NA ESCOLA ONDE ELA ESTUDAVA
A               B          F#m
E FORAM MUITOS ANOS FELIZES
           D        A              B      E
DE SORRISOS E AMIZADE ATÉ QUE UM DIA ACONTECEU
A                    B         F#m
EU OLHEI PRA ELA E DEI UM BEIJO
         D        A         B     A
NAQUELA LINDA BOCA E ELA CORRESPONDEU
                F#m       D           A
E MEU MUNDO GIRAVA, E EU NEM ACREDITAVA
              B      E
QUE AQUILO ERA TÃO BOM

A               B              F#m         D
MINHA JUVENTUDE BROTAVA, E EU ME APAIXONAVA
         B            A
PELA VIDA E POR MEU SOM
A       B           F#m
E AÍ VEIO O DIA-A-DIA
        D          A         B                  E
E O MEU SONHO CRESCIA DE VIVER COM ELA PRA SEMPRE
A            B     F#m           D            A
O DIA INTEIRO ESTUDANDO, TRABALHANDO E CANTANDO
          B             A
PRA TER DINHEIRO PRA GENTE
A               B       F#m
E QUANDO EU JÁ ME FORMAVA
       D             A      B             E
DE REPENTE ELA VEIO E FALOU NA MINHA FRENTE
A               B          F#m
EU NÃO QUERO MAIS FICAR CONTIGO
    D              A          B            A
AMANHÃ TALVEZ TE LIGO QUERO SER INDEPENDENTE"
                   F#m
E MEU MUNDO EXPLODIA
        D          A           B              E
VEIO UM MAR DE ÁGUA FRIA E CONGELOU MEU CORAÇÃO
A                     F#m
NÃO SEI ONDE ME ESCONDO
         D           A          B          A
EU SÓ SEI FICAR COMPONDO JUNTO COM MEU VIOLÃO
A               B             F#m
MAS VEIO MINHA MÃE DE MANHAZINHA
          D          A         B             E
ME GRITOU PRA LEVANTAR, E EU ACORDEI ASSUSTADO
A               B       F#m
CONTEI O QUE HAVIA SONHADO
        D         A           B                A
ELA VEIO DO MEU LADO E SUSSUROU NO MEU OUVIDO
                      F#m
TOMA SEU MULEQUE OTÁRIO
      D                A           B              E
TRABALHOU GANHOU SALÁRIO E FOI GASTAR COM AQUELA VACA
A             F#m
AGORA ESTÁ AÍ JOGADO
       D          A       B           A
EU TINHA TE AVISADO E VOCÊ ME IGNORAVA"
A               B        F#m
AGORA ESTOU MESMO SEM NADA
           D            A            B      E
SEM MINHA MÃE SEM NAMORADA, DESPREZADO E CARENTE
A         B           F#m           D          A
JÁ TENTEI ATÉ O SEMINÁRIO VOU COMPLETAR MEU CALVÁRIO
           B          A
SÓ FALTA SER PRESIDENTE

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
