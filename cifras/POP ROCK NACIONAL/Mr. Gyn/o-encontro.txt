Mr. Gyn - O Encontro

Intro: Em C D (4x)

Em              C       D          Em
Parece um misterio a desvendar
                C              D                 Em
Converso com quem nem posso tocar
                C       D      Em
Eu sei que  o destino vai fazer
                   C                D   Em
Com que nossas vidas se encontrem

                 C              D      Em
Como vou saber quem é você
                C            D  Em
Já me enganei tantas vezes
                C            D
Só vendo seu olhos e ouvir
           Em    C            D
Ouvir você falar, ouvir você falar

                 Em        D        C
Então fale o que quer dizer o amor

        Em                    D     C
Então diga se gostou de mim assim
        Em                     D             C
E nos mostre onde vamos ser felizes

(refrão)

G           D           C
Vem, viajar nesse segredo
      C5b                  G
E deixe o medo de se levar
                 D                  Am   C ( Em D C) 4x
Vem completar meu mundo aqui

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C5b = X 3 4 5 5 3
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
