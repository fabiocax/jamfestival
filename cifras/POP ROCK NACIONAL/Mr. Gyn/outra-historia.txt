Mr. Gyn - Outra história

Intro.: Bm7 , G , A

Hoje lembrei de coisas que me fazem bem
Ai senti que nao sou nada sem alguém
Quantas vezes fiquei louco de tanto esperar
Uma chance , um encontro pra voltar a respirar ?

Eu vou fazer tudo de novo por você
Ganhar o céu tocar a lua e poder
Ver na imensidão do espaço
Respostas pra nós dois
Lhe sentir como um abraço
E repetir tudo depois

Refrao

Outra história , com o final pra resolver
Eu nao posso imaginar dois caminhos pra escolher

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm7 = X 2 4 2 3 2
G = 3 2 0 0 0 3
