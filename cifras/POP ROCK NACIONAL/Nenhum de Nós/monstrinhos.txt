Nenhum de Nós - Monstrinhos

Composição: Thedy

   (G  D  C  D)2X
Um dia se odiaram
     G               D
Começaram uma guerra particular
 C                G             A         C
Sem medidas, sem saber onde ia parar ,  Parar
    G                D
Os dois se conheciam todas as fraquezas
  C              G               A        C
Sabiam onde atingir com toda certeza   Certeza
Em                             A
Tudo começou sem motivaçoes aparentes
  G                         B
Agora eles estavam armados até os dentes
   (G  D  C  D)2X      (G  D  C  D)2X
Um dia se odiaram   Um dia se odiaram
        G           D C D             G       D C D
E até acharam divertido alimentar os monstrinhos
  G                D
Diziam um ao outro coisas de doer

  C                     G             A             C
Palavras muito duras, difíceis de esquecer, De esquecer
    G                     D
Acabaram concordando que já não havia volta
C                 G               A           C
Agora alguém teria que sair pela porta, Pela porta
Em                             A
Sair de uma vez pra nunca mais voltar
        Em                             A
Nem esperar que o tempo tivesse chance de curar
   Em                                      A
Feridas tão profundas que eles conseguiram abrir
        G                            B
Em um desejo tolo, louco, de se destruir
    (G  D  C  D)2X
Mas um dia se odiaram
    (G  D  C  D)2X
Um dia se odiaram

E até acharam divertido alimentar os monstrinhos

  (G  D  C  D)2X
Um dia se odiaram  (VÁRIAS VEZES)

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
