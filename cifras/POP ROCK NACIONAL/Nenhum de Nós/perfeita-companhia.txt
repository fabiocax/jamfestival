Nenhum de Nós - Perfeita Companhia

[Intro]  A7  Am7  A7M  A7M

A7                                           Am7
Aqui de cima do telhado eu vejo a lua, vejo espaço
                          A7M
E mais um pedaço de sua casa a brilhar

A7M
Essa noite eu posso ver estrelas
Am7
Telhas secas  e seguras
        A7M
Não há porque se preocupar

 A7  Am7  A7M  A7M
Eu não vou escorregar
Dm                A
Se  você vai demorar
Bm                G
Eu espero aqui em cima
Dm                A
Vendo estrelas e luar


                       B7
A minha perfeita companhia
E                (A7  Am7  A7M  A7M)
Eu vou te esperar

A7                                         Am7
Aqui de cima do telhado eu vejo a rua, vejo asfalto
                             A7M
E carros de farol alto a me cegar

A7M
Esta noite eu só enxergo nuvens
Am7                                           A7M
Aqui de cima do telhado eu sinto o vento e a chuva a me molhar

A7                          Am7
No céu escuro e pesado a se aproximar

C
As telhas vão ficar úmidas
Cº                 G                       Em
Luzes apagadas,  a sua casa eu não enxergo mais
         C                   Cº
Mas eu sei que você continua lá
       G        E
A brilhar

Dm                A
Se  você vai demorar
Bm                G
Eu espero aqui em cima
Dm                A
Vendo estrelas e luar

                       B7
A minha perfeita companhia
E                (A7  Am7  A7M  A7M)
Eu vou te esperar

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
A7M = X 0 2 1 2 0
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
Cº = X 3 4 2 4 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
