Nenhum de Nós - Vou Deixar Que Você Se Vá

Intro: Bm F#m Bm F#m Fm Em A Em A F#7   (2x)

e|------------------------------------------------------------------|
B|-0-2-3-2-0---------0-2-3-2-0--------------------------------------|
G|-----------2-0---------------2-0-----------4-2-4-2---------4-2-3--|
D|---------------4-----------------4-3-2-4-2---------4-2-4-2--------|=====>2x
A|------------------------------------------------------------------|
E|------------------------------------------------------------------|

Bm                  F#m
Minhas mãos estão cansadas
Bm                  F#m
Não tenho mais onde me agarrar
Em         A
Tudo já se foi
Em          A F#7(2x)
Amizade, carinho e amor

Bm            F#m
Não há mais porque lutar
Bm            F#m

Minhas mãos estão cansadas
Em               A
Não vou mais lhe segurar
Em               A       Em A F#7
Vou deixar que você se vá

Refrão:
B       F#7      C#m    E
Não vou mais lhe segurar
B       F#7      C#m    E
Vou deixar que você se vá
B       F#7      C#m    E
Não vou mais lhe segurar
B       F#7      C#m    E   Em
Vou deixar que você se vá...aaa...

(Bm F#m Bm F#m)
e|----------------------|
B|-0-2-3-2-0------------|
G|-----------2-0--------|
D|---------------4-3-2--|
A|----------------------|
E|----------------------|

Em               A
Procure o seu caminho
Em               A
Eu aprendi a andar sozinho
Em               A
Isso foi há muito tempo atrás
Em               A    F#7
Mas ainda sei como se faz

Bm                  F#m
Minhas mãos estão cansadas
Bm                  F#m
Não tenho mais onde me agarrar
Em               A
Não vou mais lhe segurar
Em               A       Em A F#7
Vou deixar que você se vá

Refrão:

Solo: (B F#7 C#m E)2x

|-7-6-7-6-4-6-----6-7-9-7-6-6-4-4----7-6-7-9-11-7-6-6---4h6p4-4h6p4-2h4p2-2h4p2----------------|
|-------------------------------------------------------------------------------2h3p2-2h4p2-4--|
|-------------3/6------------------------------------------------------------------------------|
|----------------------------------------------------------------------------------------------|
|----------------------------------------------------------------------------------------------|
|----------------------------------------------------------------------------------------------| 

Refrão:

----------------- Acordes -----------------
