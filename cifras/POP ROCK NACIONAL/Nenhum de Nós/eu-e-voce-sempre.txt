Nenhum de Nós - Eu e Você Sempre

Intro: E

  E                      Bm        Bm       A
Logo, logo, assim que puder, vou telefonar
A              Am    E
Por enquanto tá doendo e quando a saudade
Bm                        A                 Am      E
Quiser me deixar cantar, vão saber quem andei sofrendo
 E                    B                 A             E
E que agora, longe de mim você possa enfim ter felicidade
                         B                   A
Nem que eu faça um tempo ruim não se sinta assim
          E                          Bm
Só pela metade. Ontem demorei pra dormir
Bm                 A           Am       E
Tava assim, sei lá, meio passional por dentro
                             Bm
Se eu tivesse o Dom de fugir pra qualquer lugar
A           Am     E
Ia feito um pé de vento, sem pensar no que
       B               A                        E
Aconteceu nada nada é meu, nem meu, nem o pensamento

                         B
Por falar em nada que é meu
            A               E
Encontrei o anel que você esqueceu
E                         G         B
Aí foi que o barraco desabou        nessa que meu barco
            A               Am/C        E
Se perdeu nele está gravado só você e eu

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Am/C = X 3 2 2 5 X
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
