Nenhum de Nós - Desejo

Intro: G Eb7+ (2x)

G
 Você falou: "Alguma coisa está errada em minha vida"
Am
 É que eu te amo mais do que eu devia
G               Bm  Em                     C
 Nem mesmo eu sabia   impedir o que eu sentia
G               Bm       Em              C
 Se eu soubesse tentaria   sentir menos, eu queria.
G                            Eb7+
 Não desejo, não, uma vida vazia (2x)
G             Em           Cm
 Aprendi com tantos erros,  isso foi demais pra mim
G                            Eb7+
 Não desejo, não, uma vida vazia

Intro
G
 Você falou: "Alguma coisa está errada em minha vida"
Am
 Se eu preciso me enganar pra ser feliz eu tenho que me jogar

G              Bm  Em               C
 Ao invés de cair,   por favor, paciência
G            Bm  Em                  C
 Espere por aí,   se eu voltar é porque eu entendi
G                            Eb7+
 Não desejo, não, uma vida vazia (2x)
G             Em           Cm
 Aprendi com tantos erros,  isso foi demais pra mim
G                            Eb7+      G
 Não desejo, não, uma vida vazia
G
 Você falou: "alguma coisa está errada em sua vida"
G                            Eb7+      G
 Não desejo, não, uma vida vazia (4x)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
Eb7+ = X X 1 3 3 3
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
