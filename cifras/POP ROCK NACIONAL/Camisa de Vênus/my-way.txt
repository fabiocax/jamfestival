Camisa de Vênus - My Way

  C                    Em           Am          A7
E agora que o fim esta perto e eu encaro esse momento
      F                                 G            C
Meus amigos, eu vou confessar os meus pecados e sentimentos
C              C7           F                         Fm
Vivi a mil por hora e por caminhos que eu nem lembro agora
  C                 G             F  C
E mais, bem mais eu sei, I did it my way

  C                  Em               Am             A7
Remorsos, eu tenho alguns, mas mesmo assim são muito poucos
   F                                             G                C
Eu fiz o que tinha que fazer enquanto voces gritavam: "bota pra foder"
                   C7            F                Fm
Eu planejei cada jogada, cada trepada, por essa estrada
  C                 G             F  C
E mais, bem mais eu sei, I did it my way

         C                  Em            Am            A7
Naqueles tempos eu era um menino que já sabia do meu destino
      F                                 G               C
E caminhando de norte a sul eu vi muita gente, tomar no cu
        C7            G            F  C
Eu entendi e não esqueci, I did it my way

  C              Em            Am             A7
Andei, sorri, chorei e me entreguei ao meu trabalho
   F                               G                         C
E agora que passou o tempo eu acho chato, chato, chato pra caralho
    C              C7          F                Fm
Não ter o que prometer e não saber mais o que dizer
   C       G             F  C
Oh não! Oh não! I did it my away

                C                      Em
Pra que serve o homem, o que é que ele tem?
               Am             A7
Ou é um puta barão ou João ninguém
         F                               G                  C
Fazer as coisas que desejou e comer as mulheres com quem sonhou
        C7           G            F          C
Eu me fodi, mas resisti, I did it my, my, my way

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
