Camisa de Vênus - Estranho No Ninho

Intro:  G   D   A
        G   D   A

A
A cada passo que dei

Apaguei as pegadas
                    E
Pra que nem eu soubesse
                     A
Como fazer pra voltar
                       D
Que a trilha do desafio
                     A
Nunca chegasse ao fim
                        E D
E agora todo esse esforço
                      A
Parece estranho pra mim
                       G D
Alguns amigos da estrada
                  A
Já não correm comigo
                         G D
Seus sonhos e seus segredos
                   A
Já não correm perigo
                        D
Cruzes plantadas no chão
                          A
São as flores deste jardim
                      E  D
A morte exibe suas formas
                    A
Tão estranhas pra mim

Houve um tempo em que eu

Lhe conhecia inteira
                E
E não havia poeira
                        A
Que eu não pudesse soprar
                 D
Mas ainda não sei
                A
Como ficamos assim
                      E D
Até a cor dos seus olhos
                      A
Hoje é estranha pra mim

Solo:  G   D   A
       G   D   A

A
As vezes penso que fiz

Alguma coisa importante
                        E
Que vai mudar num instante
                    A
O que sempre foi igual
                  D
Sinto que é a saída
                          A
Do que eu não estava a fim
                      E D
Mas é somente a chegada
                        A
De algo estranho pra mim
                     G  D
Pessoas ficam me olhando
                        A
Com um sorriso estampado
                   G   D
Outras gritam e apontam
                       A
Erguendo o punho cerrado
                          D
Pensam saber da minha vida
                  A
Se sou bom ou ruim
                 E  D
Tantas mãos acenando
                       A
Todas estranhas pra mim

( A )

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
