Camisa de Vênus - Amanhã

Intr.: E

E                                       :       
Eu vou pra casa eu ver na TV            :
    C                     D    E        :                       
Zé coice de mula contra rubão bebe      : REFRÃO
E                                       :
Eu vou pra casa eu não posso perder     :
    C                     D    E        :
Zé coice de mula contra rubão bebe      :

                                D        E
Lá do fim da paraíba venho zé coice de mula 
                        D       E  
Seu peso era galo seu punho certeiro 
                        
Se tornou campeão na luta com valdemar 
    C                     D        E  
O levando a nocaute no assalto primeiro, mas

REFRÃO     
                                    D       E                               
Quando chiquinho maluco venceu zé coice de mula 
                             D       E 
Ele caiu três vezes mas em todas levantou
                                 
Vendo o feitiço crescendo mas o juiz já gritou 
      C                          D      E               
E só não ganhou a luta porque o juiz roubou  

REFRÃO

SOLO: E... C D E E... C D E...

         E                      D     E     
Foi uma surra do zé que vez mendigo morrer
                                D           E              
Rolava sangue adoidado ninguém quis interromper
                 
O zé já tentou parar as vezes penso em esquecer 
       C                D        E     
Mas o nome do jogo é apanhar e bater

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
