KLB - Juro Por Deus

Introdução: E4 E

          B
Juro por Deus,
      A                                   E      E4 E
Eu faria qualquer coisa pra ouvir sua respiração
          B
Juro por Deus,
        A                                   E         C#m
Toda a noite eu deixo a porta aberta pra você entrar
       A                                 E
Outra vez eu vejo o sol nascendo e você não está
          B     A
Juro por Deus,
          E     B
Juro por Deus

           E
'Tô sem palavras pra dizer
          C#m
'Tô sem motivos pra sonhar
           F#m
Eu 'tô tentando te esquecer
          B
Mas juro que não dá
       E
Separação tem nada a ver
        C#m
É sofrimento pra nós dois
            F#m
Se é tão difícil perdoar
              A             B          E
Deixa eu te amar e o resto fica pra depois

          B
Juro por Deus...

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
E4 = 0 2 2 2 0 0
F#m = 2 4 4 2 2 2
