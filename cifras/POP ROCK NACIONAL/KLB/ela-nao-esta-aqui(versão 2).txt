KLB - Ela Não Está Aqui

D
Ta difícil esquecer
              Em
Tirar você de mim
                       G       A
Nos meus olhos dá pra ver
                   D       P/
Seu adeus doendo assim

                     *D
Não pensei que este amor
             Em
Me pudesse machucar
                  G         A
E uma lágrima de dor
                  D
Hoje cai do meu olhar

                      Em
→ Baby, te vejo tão longe
               G
De mim tão distante
            D
Além do horizonte
                    Em
Baby, eu grito seu nome
            G
Saudade responde
     A         D
Ela não está aqui

P/                   *D
Quando o sol vem me tocar
                    Em
Parecendo um beijo seu
                   G        A
Deixo o sonho me levar
                        D
Pra acordar nos braços seus     →

[em * começa o Samba]

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
