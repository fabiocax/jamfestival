Pedra Letícia - Eu To Na Seca

(intro) A  D  A  E  A  D  A E A

A                 D                    A         E
Eu to na seca e não há ninguem que me queira
A                 D                    A    E    A
Cê me prometa que dessa noite não vai passar
A                 D                    A         E
Eu to na seca e não há ninguem que me queira
A                 D                    A    E    A
Se não for hoje, amanhã eu vou ter que pagar

Bm                            E
Já fiz até promessa pra ver se alguém me dá
C#m                              F#m
Pedi Santo Expedito, joguei flores no mar
Bm                             E
Pra ganhar seu beijo, eu juro com certeza.
D                             E
Que vou a pé de Porto Alegre até Fortaleza

Bm                           E
Parece que toda mulher só pensa em me esnobar

C#m                         F#m
Nem a baranga do colégio nunca quis me dar
Bm                           E
Se for pra rua com os amigos fico na saudade
D                               E
Ô Roger se tá te sobrando, empresta a Zoraide

Bm             E
Eu to na seca já faz quase 1 ano
C#m                         F#m
Aceita a minha oferta e eu finjo que te amo
Bm                       E
Se você quiser eu posso ser sentimental
   D                       E
É só você abrir o zíper e segurar meu pé

Bm                    E
Mas não se assuste, amor não vá embora.
   C#m                    F#m
Me dá mais uma chance, me dá mais meia hora
   Bm                   E
Eu tô no desespero, eu não aguento mais
    D                       E
De novo no banheiro, com a Juliana Paes

B                 E                    B         F#
Eu to na seca e não há ninguem que me queira
B                 E                    B    F#   B
Cê me prometa que dessa noite não vai passar
B                 E                    B         F#
Eu to na seca e não há ninguem que me queira
B                 E                    B    F#   B
Se não for hoje, amanhã eu vou ter que pagar

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
