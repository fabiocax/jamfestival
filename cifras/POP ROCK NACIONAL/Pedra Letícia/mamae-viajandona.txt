Pedra Letícia - Mamãe Viajandona

       B            E
A sua mãe (sua mãe)
                 B   E
foi trabalhar na zona
            B    E
franca de Manaus
                   B    E
vendendo disco da Madonna

           B   E           B        E
Nas horas vagas estudava Nietzche (vê se pode)
  B                     E                  B     E
Ouvia o seu preferido pop star o tal de Ritchie
            F#7
No seu albergue tinha um vídeo
                     E
que passava os filmes do Schwarzenegger
   F#7
Depois de um porre apaixonado sem engov sonhou
        E                 B  E
Pas-de-deux com Baryshnikov

----------------- Acordes -----------------
B = X 2 4 4 4 2
E = 0 2 2 1 0 0
F#7 = 2 4 2 3 2 2
