Pedra Letícia - Três É Demais

[Intro]  A  G  D
         A  G  D

A                G
  Meu bem a nossa relação
D
  Tá precisando assim de uma pimenta
A            G
  Eu tenho a soluçã
D
  É só você deixar de ser tão ciumenta
A                    G
  Meu bem, que tal a gente ousar?
D
  Tentar assim uma coisa diferente
A          G
  Vamo barbarizar
D
  Deixar o clima um pouco assim mais quente

E                           D
 Que ninguém vai saber se você chamar

E                      D
 Aquela sua amiga inocentemente pra jantar

A               G
  Meu bem pode confessar
D
  Qualquer mulher já pensou nisso um dia
A           G
  Se embora vamo lá
D
  Quem sabe até você revelaria
A                    G
  Meu bem não vá se preocupar
D
  Você vai ser pra sempre a minha musa
A              G
  Alguém pra me ajudar
D
  A desabotoar e tirar a sua blusa

E                     D
  Se você preferir só vou olhar
E                     D
  Prometo não atrapalhar

A              G
  Você e sua amiga
D                E
  Você não vai fazer o que não quiser
A                 G
  Eu não te deixaria
D                E
  E nem te trocaria por uma qualquer
A              G
  Você e sua amiga
D              E
  Hoje em dia todo mundo faz
A                G
  Eu te convenceria
D                       E
  Que dois é bom, mas três é bom demais

( A  G  D  E )

A              G
  Meu bem eu não vo desistir
D
  A gente pode ver alguém que cobre um preço
A              G
  Pra nos satisfazer
D
  E nunca mais pisar no nosso endereço
A                  G
  Meu bem na minha posição
D
  Eu já percebi você não me engana
A            G
  Não venha me dizer
D
  Que nunca imaginou um outro alguém na nossa cama

E                    D
  Só faço isso por você
E                       D
  Sem interesse o meu desejo é só o seu prazer

E                      D
  Só não venha me falar
E                            D
  Que você prefere um outro cara pra te ajudar

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
