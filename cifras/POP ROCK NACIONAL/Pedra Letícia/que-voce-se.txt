Pedra Letícia - Que Você Se

F#m                                   Bm
Tudo que você me fez eu lhe desejo em dobro
                             F#m
Você vai pagar por tanto desaforo
                                 E
De lhe ver sorrir enquanto eu chorava em vão
    F#m
Eu quero que você se dane
            Bm
Que você se lasque

Que você se engasgue
              F#m
Que você se engane
                                   E
Que você me chame pra que eu possa dizer não
    A
Eu quero que seu carro quebre
               B
Que seu peito caia
                                D
Que a chuva estrague seu dia de praia

                              E
Que um prego rasgue a sua minissaia
                              A
Que seu novo amor no final te traia
Que você tropece
            B
Que você tropique
                                 D
Que se estrumbique, dê o seu chilique
                                 E
Que você comece, depois perca o pique
           G#6/F                    (F#m Bm F#m E)
Que alguém copie o seu vestido chique
   G#6/F
Eu quero que você se dane
             Bm
Que você se lasque

Que você se engasgue
              F#m
Que você se engane
                                    E
Que você me chame pra que eu possa dizer não
   A
Eu quero que seu time perca
              B
Que seu voo atrase
Seu café esfrie
              D
Que você não case
                               E
Seu melhor perfume pela tampa vase
                                   A
Que lhe falte voz pra completar a frase
Que você se estrepe
               B
Que você se estranhe
Que você não ganhe
                 D
Que lhe fure o estepe
                                E
Que lhe falte gás no seu refrigerante
               G#6/F
Que nunca mais cante
                  (A B D E)
Que nunca mais trepe
   A
Eu quero que seu time perca
              B
Que seu voo atrase
Seu café esfrie
              D
Que você não case
                               E
Seu melhor perfume pela tampa vase
                                   A
Que lhe falte voz pra completar a frase
Que você se estrepe
               B
Que você se estranhe
Que você não ganhe
                 D
Que lhe fure o estepe
                                E
Que lhe falte gás no seu refrigerante
Que nunca mais cante
Que nunca mais
A
Quero que seu time caia
               B
Que seu peito vase
                                D
Que seu novo amor no final se atrase
                              E
Você seja estepe, você seja amante
               G#6/F
Que nunca mais cante
                F#m
Que nunca mais trepe

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#6/F = 1 3 1 1 1 1
