Frejat - Segredos

G
 Eu procuro um amor
     C
que ainda não encontrei
G              D      C
 Diferente de todos que amei
G                                      C
 Nos seus olhos quero descobrir uma razão para viver
        G                    D    C
E as feridas dessa vida eu quero esquecer
      Em                        D
Pode ser que eu a encontre numa fila de cinema
         Em     D            C
Numa esquina ou numa mesa de bar

    G                D       C
Procuro um amor que seja bom pra mim
      G          D    C
Vou procurar eu vou até o fim
      Em                     D
E eu vou tratá-la bem pra que ela não tenha medo

        C             G           Am
Quando começar a conhecer os meus segredos


       G                   C
 Eu procuro um amor uma razão para viver
        G                    D     C
E as feridas dessa vida eu quero esquecer
      Em                      D
Pode ser que eu gagueje sem saber o que falar
           Em           D              C
Mas eu disfarço e não saio sem ela de lá

     G                D            C
Procuro um amor que seja bom pra mim
         G       D         C
Vou procurar eu vou até o fim
      Em                     D
E eu vou tratá-la bem pra que ela não tenha medo
        C             G         Am
Quando começar a conhecer os meus segredos

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
