Frejat - Intimidade Entre Estranhos

Intro: D9

D9                  D9/C#
Do outro lado, tarde da noite
D/C                                     G/B
Eles perdem a linha outra vez
Gm/Bb           D
Ela jura que agora acabou
E/G#                       G
Ele sabe a bobagem que fez
D9                  D9/C#
Os gritos, a porta batendo
D/C                             G/B
Ela quer que ele diga por quê
Gm/Bb                   D
Não quero escutar, mas escuto
E/G#                                    G   A
E eu sei que eles sabem que eu sei

D                                       D7+
Nada é tão lento quanto o tempo aqui dentro

D7                              G
Eu e eles e a nossa dor
D                                       D7+
Nada é tão denso quanto o tempo em silêncio
D7                        G
Eu e eles no elevador

D9                      D9/C#
Do outro lado e eu sei que eles sabem
D/C                             G/B
Que eu andei bebendo de novo
Gm/Bb                   D
Que eu gritei pra lua o seu nome
E/G#                      G
Que foi outra noite sem sono
D9                        D9/C#
Eles devem ter se assustado
          D/C                   G/B
Quando ouviram o vidro quebrando
Gm/Bb                D
E eu escondo a mão enfaixada
E/G#                                    G
Quando o elevador vai chegando

D9/F#              Gm6
Intimidade entre estranhos
Gm/Bb                G
Perfume e pasta de dente
D9/F#                   Gm6
E um outro cheiro qualquer
Gm/Bb                       G           A
Que a gente faz que não sente

D                                       D7+
Nada é tão lento quanto o tempo aqui dentro
D7                              G
Eu e eles e a nossa dor
D                                       D7+
Nada é tão denso quanto o tempo em silêncio
D7                      G   Gm6      D G Gm6 D
Eu e eles                no elevador

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
D/C = X 3 X 2 3 2
D7 = X X 0 2 1 2
D7+ = X X 0 2 2 2
D9 = X X 0 2 3 0
D9/C# = X 4 0 2 3 0
D9/F# = 2 X 0 2 3 0
E/G# = 4 X 2 4 5 X
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
Gm/Bb = 6 5 5 3 X X
Gm6 = 3 X 2 3 3 X
