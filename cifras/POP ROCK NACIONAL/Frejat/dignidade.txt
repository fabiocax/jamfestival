Frejat - Dignidade

Introdução: A  E  D  A  F#m

A     D          A    D
De você eu não quero um centavo
F#m    E       F#m      E
Muito menos ser o seu escravo
A     D          A    D
Parece difícil acreditar
F#m    E       F#m      E
Que eu tenha algum amor pra dar

D
Já fiz de tudo
Bm
Entrei no teu mundo
A
Só pra te mostrar
D
Que o que eu sinto
Bm
É muito mais fundo

E
Parece que vai estourar, baby, baby

A       D              A    D
Não quero que você faça minha cama
F#m    E       F#m      E
Apesar d\'eu gostar de conforto
A     D          A    D
Porquê amar desse jeito, baby
F#m    E       F#m      E
É o mesmo que estar morto

D
Com tanta discussão
Bm            E
Falei demais, perdi a razão
D
Eu falo, eu grito, eu xingo
Bm
Porque eu preciso
E
Eu não vivo sem você não

A               F#m
Dinheiro pra mim não tem valor
E
Quando o assunto é amor
A               F#m
Vale muito mas não vale nada
E                                    A  E  D  A  F#m
E eu nunca tive nada de mão beijada

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
