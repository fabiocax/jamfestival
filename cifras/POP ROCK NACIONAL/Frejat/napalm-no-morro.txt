Frejat - Napalm no Morro

{Intro:} E E4 E E4 E

E
Eu corto o pão francês
   G              F#m
Em dois pedaços iguais
E
Como são sempre iguais
    G                 A
Os dias que eu vou viver

B
Há um ano eu sonho com bichos
A
Que me perseguem com fome
B
Com todo tipo de armas
A
Quero mulheres que somem

Eu desarrumo os jornais

Atrás de balas perdidas no morro
Eu trinco os dentes de raiva
E escovo os dentes de novo

Eu ponho o terno no armário
Sinto o corpo todo coçando
No fundo eu sei o que passa
Tá tudo só começando

E9
Alguém me segura
D9
Que se eu entro na briga
C#m
Taco napalm no morro
D9
Desço matando a polícia

A minha roupa me aperta
Meia, sapato e calor
Com ela a vida é tão certa
Terças e sextas de amor

Mas quando eu chego bem perto
Eu fecho os punhos, me mordo
Eu trinco os dentes de raiva
E escovo os dentes de novo

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
E4 = 0 2 2 2 0 0
E9 = 0 2 4 1 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
