Frejat - O Amor É Quente

A                               A7M
Não adianta nem tentar me perturbar
                       A7
Me confundir, me alterar
             D         A4/D
Eu tô invicto, incólume, convicto
      A                             A7M
E dessa meta ninguém vai me desviar
                        A7
Nem um acúmulo de nuvens ou de carros
    D        E7/D                   D  A4/D
O inesperado tá pintando na esquina

      A     E                         D  A4/D
Sai da frente que o amor é quente (2x)


A                                 A7M
Não adianta nem tentar me ignorar
                                                  A7  D  A4/D
Nem monitorar os céus tão explorados e tão vazios

               A                      A7M
E não adianta nem tentar me perseguir
                         A7
Me destruir, me distrair
                             D    E7/D  D  A4/D
Nem uma bobeira, nem uma saideira

              A    E                  D  A4/D
Sai da frente que o amor é quente (2x)

Base do solo: A  A7M  A7  D  E7/D  D  E7/D

A5
Chuva de pedra do fundo da terra
         E5
Não vai me frear, não vai me parar
       F#5                          D  A4/7
O inesperado tá pintando na esquina

             A     E                  D  A4/D  A
Sai da frente que o amor é quente (4x)

----------------- Acordes -----------------
A = X 0 2 2 2 0
A4/7 = X 0 2 0 3 0
A4/D = X 5 7 7 5 5
A5 = X 0 2 2 X X
A7 = X 0 2 0 2 0
A7M = X 0 2 1 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E5 = 0 2 2 X X X
E7/D = X 5 X 4 5 4
F#5 = 2 4 4 X X X
