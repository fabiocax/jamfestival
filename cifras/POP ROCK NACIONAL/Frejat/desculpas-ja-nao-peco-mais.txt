Frejat - Desculpas já não peço mais

D#9                    C#9  G#
Cansei de não gostar de mim
D#9                                   C#9  G#
Daqui pra frente não vai ser mais assim
D#9                    C#9  G#
Serei o meu melhor amigo
B             C#           D#9
Inimigos como eu não preciso
     D#              C#9   G#
Então vêm, eu te convido
D#9                        C#9  G#
Quer dividir comigo, o paraíso
D#9                         C#9  G#
O inferno é meu velho conhecido
   B     C#             D#9
Te mostro meu esconderijo
  Eb             Eb9/C#
Desculpas já não peço mais
C#9                  G#
Fui ao inferno procurando paz
Eb                   Eb9/C#
Não vou desperdiçar alegria

C#9  G#                    D#9
Amor, não precisa de filosofia
D#9                    C#9    G#
Cada ruga em mim te merece
D#9                     C#9    G#
Não confunda os desejos, agradece
D#9                        C#9   G#
E os presentes que eu te dei
B             C#                     D#9
Sao espinhos que em carinho  transformei
  Eb             Eb9/C#
Desculpas já não peço mais
C#9                  G#
Fui ao inferno procurando paz ...

----------------- Acordes -----------------
B = X 2 4 4 4 2
C# = X 4 6 6 6 4
C#9 = X 4 6 6 4 4
D# = X 6 5 3 4 3
D#9 = X 6 8 8 6 6
Eb = X 6 5 3 4 3
Eb9/C# = X 4 1 3 4 1
G# = 4 3 1 1 1 4
