Frejat - Seu Amorzinho

Intro: Am  D7/F#  G11  Em  Am  D7/F#  G11  E/G#

G                    E/G#
Foi quando a ficha caiu
              Am          Bb9
Que então eu vi você diferente
F7+                             E
Não quero mais a sua boa intenção
          G                  E/G#
Mas se você fosse um grande mal pra mim
       Am              Bb9
Tudo agora doeria bem menos
F7+                            E
Ou se eu nunca tivesse sido feliz

F                D7/F#           Am     F7+
Hoje eu não sou mais o seu amorzinho
Dm                              Am
Não quero saber do seu medo da vida
F                D7/F#           Am     F7+
Eu vou descobrir outros caminhos

Dm                               E
E não tô com pressa de achar a saída

 Am D7/F# G11 Em Am D7/F# G11 E/G#
Não

G                      E/G#
Não tente mais me agradar
          Am             Bb9
Pra disfarçar o seu desejo
   F7+
O sonho ficou lá pra trás
          E
E não há lágrima que dê jeito
          G               E/G#
Mas se você fosse má pra mim
        Am             Bb9
Tudo agora seria mais fácil
F7+                                E
Se ao menos eu não tivesse sido feliz


F                D7/F#           Am     F7+
Hoje eu não sou mais o seu amorzinho
Dm                              Am
Não quero saber do seu medo da vida
F                D7/F#           Am     F7+
Eu vou descobrir outros caminhos
Dm                               E
E não tô com pressa de achar a saída

 Am D7/F# G11 Em Am D7/F# G11 E/G#
Não                             x5

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb9 = X 1 3 3 1 1
D7/F# = X X 4 5 3 5
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F7+ = 1 X 2 2 1 X
G = 3 2 0 0 0 3
G11 = 3 5 5 5 3 3
