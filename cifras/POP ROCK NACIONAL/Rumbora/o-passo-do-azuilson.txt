Rumbora - O Passo Do Azuilson

(intro) Am E Am E

Em                  Am
Relaxar que ainda tenho
             C    Bm
 a noite inteira
 Em              Am
sem problema entro de
             C   D
qualquer maneira
 Em              Am
conhecido com um ar
         C  Bm
dissimulado
 Em
não me importa quero
 Am               C   D
estar do outro lado
 C
o que me satisfaz vira
            D
meu compromisso

vou dar um jeito nisso

(refrão)
    Em
vou do passo do azuílson
C       D             Em C
bem amanhã eu cuido disso
        D           Em
vou no passo do azuílson
C      D        Em
sem embaço sem enguiço
  C       D          Em
vou no passo do azuílson

(intro)

 Em                     Am
já tô dentro e agora ninguém
          C  Bm
mais me tira,   se
 Em               Am
encontro quem eu quero
          C  D
a sorte vira
 Em                  Am
vou andando  pelo escuro
        C   Bm
a procura
 Em                      Am
nem que dure a noite inteira
         C   D
vou te achar
   C
interessado desperto disposto
      D
pra ação, pintou confusão

(refrão 2x)
          D         C    D Em
vou do passo do azuílson
                 ( Am E Am E )

(vocalização) Uuu....

 Em                Am
retornando para a base
        C    Bm
vou sozinho
 Em                Am
precurando ainda luz
           C    D
pro meu caminho
 Em            Am
pela sombra descobri
            C   Bm
que a madrugada
 Em             Am
ia se tão boa quanto
       C   D
eu sonhava
C
sentada na porta de casa
            D
pra minha alegria
quem eu tanto queria

(refrão 2x)
         D         Em
vou do passo do azuÍlson

(solo) Em

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
