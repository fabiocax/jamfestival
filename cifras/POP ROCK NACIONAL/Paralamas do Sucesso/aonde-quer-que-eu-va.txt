﻿Os Paralamas do Sucesso - Aonde Quer Que Eu Vá

Capo Casa 5

[Primeira Parte]

[Riff 1]

E|--------------------------------|
B|--------------------------------|
G|--------------------------------|
D|--------------------------------|
A|-0h2----------------------------|
E|--------------------------------|

[Riff 2]

E|--------------------------------|
B|--------------------------------|
G|-2p0-------0--------------------|
D|-0---2-0h2----------------------|
A|--------------------------------|
E|--------------------------------|

[Riff 3]


E|--------------------------------|
B|--------------------------------|
G|--------------------------------|
D|--------------------------------|
A|-0-2-3--------------------------|
E|--------------------------------|

Em7             G
    Olhos fechados
Em7               G
    Pra te encontrar
Em7                     G
    Não estou ao seu lado
Em7               G
    Mas posso sonhar

[Refrão]

C9                      Am7
   E aonde quer que eu vá
        G  D11/F#  Em7
Levo você no    olhar
C9                    Am7
   Aonde quer que eu vá
                   G  D11/F#  Em7
Aonde quer que eu vá

[Riff 4]

E|--------------------------------|
B|--------------------------------|
G|--------------------------------|
D|-----0--------------------------|
A|-0h2----------------------------|
E|-------0------------------------|

[Segunda Parte]

Em7                 G
    Não sei bem certo
Em7             G
    Se é só ilusão
Em7                  G
    Se é você já perto
Em7            G
    Se é intuição

[Refrão]

C9                      Am7
   E aonde quer que eu vá
        G  D11/F#  Em7
Levo você no    olhar
C9                    Am7
   Aonde quer que eu vá
                   G  D11/F#  Em7
Aonde quer que eu vá

[Terceira Parte]

A5(9)           F#m
      Longe daqui
            C#m
Longe de tudo
      D                A5(9)
Meus sonhos vão te buscar
           F#m
Volta pra mim
                C#m
Vem pro meu mundo
    D                 A5(9)
Eu sempre vou te esperar
     F#m     C#m  D  C  B7
Larará, lararára

[Segunda Parte]

Em7                 G
    Não sei bem certo
Em7             G
    Se é só ilusão
Em7                  G
    Se é você já perto
Em7            G
    Se é intuição

[Refrão Final]

C9                      Am7
   E aonde quer que eu vá
        G  D11/F#  Em7
Levo você no    olhar
C9                    Am7
   Aonde quer que eu vá
                   G  D11/F# Em7
Aonde quer que eu vá

 C9      Am7     G  D11/F#  Em7
Lá, larará, larará
 C9      Am7     G
Lá, larará, larará
 D11/F#            Em7
Aonde quer que eu vá

 C9      Am7     G  D11/F#  Em7
Lá, larará, larará
 C9      Am7     G  D11/F#   Em7
Lá, larará, larará        larárá
 C9      Am7     G
Lá, larará, larará
 D11/F#            Em7
Aonde quer que eu vá

----------------- Acordes -----------------
Capotraste na 5ª casa
A5(9)*  = X 0 2 2 0 0 - (*D5(9) na forma de A5(9))
Am7*  = X 0 2 0 1 3 - (*Dm7 na forma de Am7)
B7*  = X 2 4 2 4 2 - (*E7 na forma de B7)
C*  = X 3 2 0 1 0 - (*F na forma de C)
C#m*  = X 4 6 6 5 4 - (*F#m na forma de C#m)
C9*  = X 3 2 0 3 3 - (*F9 na forma de C9)
D*  = X X 0 2 3 2 - (*G na forma de D)
D11/F#*  = 2 X X 2 3 3 - (*G11/B na forma de D11/F#)
Em7*  = 0 2 2 0 3 3 - (*Am7 na forma de Em7)
F#m*  = 2 4 4 2 2 2 - (*Bm na forma de F#m)
G*  = 3 2 0 0 3 3 - (*C na forma de G)
