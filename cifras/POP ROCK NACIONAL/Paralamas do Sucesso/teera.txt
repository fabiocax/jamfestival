Os Paralamas do Sucesso - Teerã

Intro: ( Dm  Bb  A7 )

Dm                                         Bb           A7
    Por quanto tempo ainda vamos ver
Dm                         Bb        A7
    Fotografias pela manhã
    Dm
Imagens de dor, lições do passado
Bb                     A7           Dm        Bb     A7
Recentes demais pra esquecer

Dm                     Bb        A7
    E o futuro o que trará
Dm                                  Bb        A7
    Para as crianças em Teerã
        Dm
Brincar de soldados por entre os escombros
Bb                    A7              Dm
Os corpos deitados não fingem mais

            Bb                                    C                   C#º
E as marcas de sangue no chão são lembranças

Dm                            A7
    Difíceis de apagar
      Bb                                C                C#º
Será que ainda existe razão pra viver
            ( Dm     Bb   A7 )
Em Teerã

Dm                                           Bb         A7
    Por quanto tempo ainda vamos ter
Dm                                Bb            A7
    Nas noites frias e nas manhãs
    Dm
Imagens de dor em rostos marcados
Bb                      A7                Dm            Bb   A7
Pequenos demais pra se defender

Dm                  Bb        A7
E o futuro o que trará
Dm                                    Bb            A7
Se essas crianças vão sempre estar
      Dm
Pedindo trocados nos vidros fechados
Bb                        A7            Dm        Bb        A7
Sentando no asfalto sem perceber

                Bb                                    C                 C#º
Que as marcas de sangue no chão são lembranças
Dm                            A7
    Difíceis de apagar
      Bb                                C                C#º
Será que ainda existe razão pra viver
            ( Dm     Bb   A7 )
Em Teerã

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C#º = X 4 5 3 5 3
Dm = X X 0 2 3 1
