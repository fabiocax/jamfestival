Os Paralamas do Sucesso - Taubaté Ou Santos

INTRODUÇÃO ( B7+ E7+)
       B7+
Eu medito e peço muito
       E7+
Pra sempre te ter bem junto
       B7+
Sem tentar fugir do assunto
          C#m      F#
Pra onde fores vou atrás

           B7+
E com tua luz tão clara
           E7+
Eu te entenda mais um pouco
           B7+
Que ideais não sejam sonhos,
           C#m      F#
Mas que possam ser reais

      C#m          F#
Quero te pedir perdão

    C#m          F#
Por viver assim aos prantos
    C#m          F#
Quem sabe eu ainda te encontro
    C#m          F#
Lá em Taubaté ou Santos

       B7+
Sob tua luz tão clara
        E7+
Simples calma irresoluta
      B7+
O meu coração dispara
      C#m          F#
Numa convulsão tão bruta
       Ebm
O que nossas energias solicitam
       E9
O que pede espaço pra acontecer
        Ebm
Tanto tempo nesse estado e luzes brilham
                     F#
Assim mesmo antes do amanhecer

  C#m          F#
Quero te pedir perdão
    C#m          F#
Por viver assim aos prantos
    C#m          F#
Quem sabe eu ainda te encontro
    C#m          F#
Lá em Taubaté ou Santos

( B7+ E7+)

----------------- Acordes -----------------
B7+ = X 2 4 3 4 2
C#m = X 4 6 6 5 4
E7+ = X X 2 4 4 4
E9 = 0 2 4 1 0 0
Ebm = X X 1 3 4 2
F# = 2 4 4 3 2 2
