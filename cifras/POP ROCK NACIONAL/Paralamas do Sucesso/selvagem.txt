Os Paralamas do Sucesso - Selvagem

  Intro: D G

E|-------------------------|-----------|
B|-------------------------|-----------|
G|-7-5-7-5-7-7-5-7-7-5-----|-7-5-7-5-7-| (6x)
D|-7-5-7-5-7-7-5-7-7-5-----|-7-5-7-5-7-|
A|-5-5-5-5-5-5-5-5-5-5-----|-5-5-5-5-5-|
E|-------------------------|-----------|

(Alterne as notas com uma palhetada em cada: Am  D)

     Am                    D
A polícia apresenta suas armas
        Am                        D
Escudos transparentes, cacetetes, Capacetes reluzentes
Am                   D                Am          D
E a determinação de manter em seu lugar
    Am                    D
O governo apresenta suas armas
        Am                  D
Discurso reticente, novidade inconsistente

Am                               D                     Am             D
 E a liberdade cai por terra aos pés de um filme de Godard
     Am                  D
A cidade apresenta suas armas
 Am                   D
Meninos nos sinais, mendigos pelos cantos
     Am                            D
E o espanto está nos olhos de quem vê
                      Am    D
O grande monstro a se criar
   Am                     D
Os negros apresentam suas armas
   Am                  D
As costas marcadas, as mãos calejadas
     Am                 D
  E a esperteza que só tem quem tá

(Intro)

              Am   D
Cansado de apanhar
    Am                   D
A polícia apresenta suas armas
 Am                      D
Escudos transparentes, cacetetes
             Am
capacetes reluzentes
            D                           Am  D
E a determinação de manter tudo em seu lugar
    Am                  D
O governo apresenta suas armas

  Am                  D
Discurso reticente, novidade inconsistente
Am                      D
  E a liberdade cai por terra
                        Am   D
Aos pés de um filme de Godard
    Am                   D
A cidade apresenta suas armas
 Am                   D
Meninos nos sinais, mendigos pelos cantos
      Am                          D
E o espanto está nos olhos de quem vê
                      Am    D
O grande monstro a se criar
   Am                     D
Os negros apresentam suas armas
     Am                 D
As costas marcadas, as mãos calejadas
Am                         D
  E a esperteza que só tem quem tá
C             Am
Cansado de apanhar

(Intro)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
