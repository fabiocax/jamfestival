Os Paralamas do Sucesso - O Beco

Introdução: Bm A

INTRO E SOLOS ENSTRE REFRÃO

E|----------------------------------------------------|
B|-7-7-7-7-7----5-------------------------------------|
G|------------7------7-----6-----7---4~---------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

E|----------------------------------------------------|
B|-7-7-7-7-7----5-------------------------------------|
G|------------7------7-----6-----7---4~---------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

E|----------------------------------------------------|
B|-7-7-7-7-7----5---5--8----5--8~---------------------|
G|------------7---------------------------------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|


    Bm                       A       Bm
No beco escuro explode a violência
              A
Eu tava preparado
                Bm                    A
Descobri mil maneiras de dizer o teu nome
          Bm              A
Com amor, ódio, urgência
                      Bm   A
Ou como se não fosse nada


    Bm                       A       Bm
No beco escuro explode a violência
             A
Eu tava acordado
            Bm                 A
Ruínas de igrejas, seitas sem nome
    Bm             A                     Bm  A
Paixão, insônia, doença, liberdade vigiada


    Bm                       A       Bm
No beco escuro explode a violência
                 A
No meio da madrugada
     Bm                A
Com amor, com ódio, urgência
                      Bm    A
Ou como se não fosse nada

     Bm                  A
Mas nada perturba o meu sono pesado
Bm                   A
Nada levanta aquele corpo jogado
Bm                     A
Nada atrapalha aquele bar ali na esquina
        Bm
Aquela fila de cinema
A
Nada mais me deixa chocado
Bm
Nada

Introdução

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
