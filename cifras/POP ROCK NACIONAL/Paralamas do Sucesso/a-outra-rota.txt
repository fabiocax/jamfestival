Os Paralamas do Sucesso - A Outra Rota

A                 F#m   Dm A
Eu vou fechar as contas      e me mandar
       F#m                Dm
Me ajoelhar, pedir perdão
A
Depois te perdoar
            F#m                 Dm
Você não merece o que eu te fiz
A
Só pra te machucar
A7                     D  E
Como se o forte fosse eu


A                F#m Dm A
Eu tou em outra rota      pra um outro lugar
                    F#m
Eu quero as coisas certas
    Dm         A
Eu quero te falar



            F#m              Dm
Você não merece o que eu te fiz
A
Tentando te mudar
A7         D
Como se o certo fosse eu


A           B/A         D/A
Agora somos só nós dois     olhando pros lados
A          B/A         D/A
Depois de tanta estupidez
A           B/A         D/A
Agora somos só nós dois     olhando pros lados
A  B/A                  D/A
  Já nem te vejo mais


A        F#m             Dm
E acreditar, e acreditar
           A
Mesmo sem ver as provas
                     F#m    Dm
De cada corte corre sangue
          A
E a vida se renova
                F#m        Dm
Você sabe o que eu já fiz
A
E do que eu fui capaz
A7        D             A F#m Dm A
Mas fica tudo entre nós

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
B/A = X 0 4 4 4 X
D = X X 0 2 3 2
D/A = X 0 X 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
