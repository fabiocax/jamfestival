Os Paralamas do Sucesso - Cuide Bem do Seu Amor

(capo 2ª casa)

(riff de abertura)

E|------------------------------------------|
B|------------------------------------------|
G|------------------------------------------|
D|---------4--------------------------------|
A|---4-4h6----------------------------------|
E|-7----------------------------------------|

   F#m
A vida sem freio me leva, me arrasta, me cega
    A
No momento em que eu queria ver
      F#m
No segundo que antecedo o beijo, a palavra que destrói o amor
        A
Quando tudo ainda estava inteiro, no instante em que desmoronou

F7M                  A
  Palavras duras em voz de veludo

F7M              A
  E tudo muda, adeus velho mundo
F7M              F#m    E      D#m7(5-)
  Há um segundo tudo estava em paz

A        A/G            F#m6         F7M(6)
  Cuide bem do seu amor,      seja quem for
A        A/G            F#m6         B7  E   Em   Dm7(9) E
  Cuide bem do seu amor,      seja quem  for

(riff)

   F#m
E cada segundo, cada momento, cada instante
          A
É quase eterno, passa devagar
        F#m
Se seu mundo for um mundo inteiro, sua vida, seu amor, seu lar
       A
Cuide tudo que for verdadeiro, deixe tudo que não for passar

F7M                  A
  Palavras duras em voz de veludo
F7M              A
  E tudo muda, adeus velho mundo
F7M              F#m    E      D#m7(5-)
  Há um segundo tudo estava em paz

A        A/G            F#m6         F7M(6)
  Cuide bem do seu amor,      seja quem for
A        A/G            F#m6         B7  E   Em   Dm7(9) E
  Cuide bem do seu amor,      seja quem  for

(solo)

F7M                  A
  Palavras duras em voz de veludo
F7M              A
  E tudo muda, adeus velho mundo
F7M              F#m    E      D#m7(5-)
  Há um segundo tudo estava em paz

A        A/G            F#m6         F7M(6)
  Cuide bem do seu amor,      seja quem for
A        A/G            F#m6         B7 E
  Cuide bem do seu amor,      seja quem for

A        A/G            F#m6         F7M(6)
  Cuide bem...                             do seu amor...
A    A/G    F#m6    F    G    A

(tabs)

(solo)
   F#m
E|------------------------------------------|
B|------------------------------------------|
G|------------------------------------------|
D|---------4-4h6-4--------------------------|
A|---4-4h6---------6-4-------2---------4-4-2|
E|-7-------------------7-2-4---4-2-4h7------|

   A
E|------------------------------------------|
B|------------------------------------------|
G|------------------------------------------|
D|---------4-----4--------------------------|
A|-4-2-4h6---4h6---6-4-2-4/6--2-4---2-------|
E|--------------------------------4---------|

   F#m
E|------------------------------------------|
B|------------------------------------------|
G|------------------------------------------|
D|-----4/6----------4-----------------------|
A|-4-2------4-2-4h6---6-4-2-----------------|
E|------------------------------------------|

   A
E|------------------------------------------|
B|------------------------------------------|
G|------------------------------------------|
D|---------------4h6-4----------------------|
A|-------2-2h4-6-------6-4-2-4-2------------|
E|---2-4------------------------------------|

(revisão - Philippe Lobo)

----------------- Acordes -----------------
Capotraste na 2ª casa
A*  = 5 7 7 6 5 5 - (*B na forma de A)
A/G*  = 3 X 2 2 2 X - (*B/A na forma de A/G)
B7*  = X 2 1 2 0 2 - (*C#7 na forma de B7)
D#m7(5-)*  = X X 1 2 2 2 - (*Fm7(5-) na forma de D#m7(5-))
Dm7(9)*  = X 5 3 5 5 X - (*Em7(9) na forma de Dm7(9))
E*  = 0 2 2 1 0 0 - (*F# na forma de E)
Em*  = 0 2 2 0 0 0 - (*F#m na forma de Em)
F*  = 1 3 3 2 1 1 - (*G na forma de F)
F#m*  = 2 4 4 2 2 2 - (*G#m na forma de F#m)
F#m6*  = 2 X 1 2 2 X - (*G#m6 na forma de F#m6)
F7M*  = 1 X 2 2 1 X - (*G7M na forma de F7M)
F7M(6)*  = 1 X 2 2 3 X - (*G7M(6) na forma de F7M(6))
G*  = 3 5 5 4 3 3 - (*A na forma de G)
