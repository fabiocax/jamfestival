Os Paralamas do Sucesso - Que País É Esse

Intodução - (Em  C  D)

Tom - Em

Solo violão
E|---7---7--7p---7---5---3---3---3p---3---5-------
B|-5---5---5p---5---3---5---5---5p---5---3--------
G|------------------------------------------------
D|------------------------------------------------
A|------------------------------------------------
E|------------------------------------------------

Solo do Baixo
E|------------------------------------------------
B|------------------------------------------------
G|------------------------------------------------
D|------------------------------------------------
A|-7-7-5-7-7-5-7-7-0-3-3-3-0-5-5-5----------------
E|------------------------------------------------



Toque o solo ate entrar o vocal !!!

(Em  C  D)
Nas favelas, no senado
Sujeira pra todo lado
Ninguém respeita a Constituição
Mas todos acreditam no futuro da nação
Que país é este
Que país é este
Que país é este
(Em C  D)
No Amazonas, no Araguaia iá, iá,
Na Baixada Fluminense
Mato Grosso, nas Gerais e no
        Nordeste tudo em paz
Na morte eu descanso, mas o
        sangue anda solto
Manchando papéis, documentos fiéis
Ao descanso do patrão
Que país é este
Que país é este
Que país é este
Que país é este
(Em  C  D)
Terceiro mundo, se for
Piada no exterior
Mas o Brasil vai ficar rico
Vamos faturar um milhão
Quando vendemos todas as almas
Dos nossos índios em um leilão
Que país é este
Que país é este
Que país é este
Que país é este...

---------------------------------------------------------

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
