Os Paralamas do Sucesso - Tendo a Lua

D           A/D                 C/D  G/D
Eu hoje joguei tanta coisa fora
D               A/D     C/D     G/D
Eu vi o meu passado passar por mim
 D                A/D
Cartas e fotografias
           C/D       G/D
Gente que foi embora
D       A/D               C/D   G/D
A casa fica bem melhor assim
D                   A/D
O céu de Ícaro tem mais poesia
          C/D     G/D
Que o de Galileu
D                   A/D
E lendo os teus bilhetes
          C/D        G/D
Eu penso no que fiz
  D                     A/D
Querendo ver o mais distante
     C/D       G/D
Sem saber voar

 D               A/D
Desprezando as asas
     C/D        G/D
Que você me deu
 D       A/D
Tendo a Lua
         C/D
Aquela gravidade
        G/D
Onde o homem flutua
 D           A/D
Merecia a visita
              C/D
Não de militares
           G/D
Mas de bailarinos
      D
E de você e eu

E           B/E                  D/E  A/E
Eu hoje joguei tanta coisa fora
E                     B/E
E lendo os teus bilhetes
          D/E        A/E
Eu penso no que fiz
 E               B/E
Cartas e fotografias
           D/E       A/E
Gente que foi embora
E       B/E                   D/E  A/E
A casa fica bem melhor assim

----------------- Acordes -----------------
A/D = X X 0 6 5 5
A/E = 0 X 2 2 2 0
B/E = X 7 9 8 7 7
C/D = X X 0 0 1 0
D = X X 0 2 3 2
D/E = X X 2 2 3 2
E = 0 2 2 1 0 0
G/D = X 5 5 4 3 X
