Os Paralamas do Sucesso - Capitão de Indústria

Intr.: G D/G C/G D/G Gm7 Eb/G C/D


Verso 1:

G  D/G         C/G            D/G
Eu    às vezes fico a pensar
         Gm7            Eb/G
Em outra vida ou lugar
         C/D
Estou cansado demais


Verso 2:

G  D/G          C/G           D/G
Eu    não tenho tempo de ter
         Gm7          Eb/G
O tempo livre de ser
        C/D
De nada ter que fazer



Refrão:

Eb7+  Ebm               Bb7+
É        quando eu me encontro perdido
    Gm7            Eb7+  Ebm
Nas coisas que eu criei
         Bb7+
E eu não sei
Eb7+  Ebm           Bb7+
Eu        não vejo além de fumaça
   Gm7             Ab7+
O amor e as coisas livres, coloridas
C/D
Nada poluídas


Verso 3:

G  D/G           C/G            D/G
Ah,    Eu acordo pr'á trabalhar
         Gm7             Eb/G
Eu durmo p'rá trabalhar
         C/D
Eu corro pr'á trabalhar

(Verso 2)
(Refrão)
(Solo: Em Am Em Am Cm/Eb C/D)
(Verso 3)
(Verso 2)
(Refrão)
(Verso 3)

( G D/G C/G D/G Gm7 Eb/G C/D )

----------------- Acordes -----------------
Ab7+ = 4 X 5 5 4 X
Am = X 0 2 2 1 0
Bb7+ = X 1 3 2 3 1
C/D = X X 0 0 1 0
C/G = 3 3 2 X 1 X
Cm/Eb = X X 1 0 1 3
D/G = 3 X X 2 3 2
Eb/G = 3 X 1 3 4 X
Eb7+ = X X 1 3 3 3
Ebm = X X 1 3 4 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
Gm7 = 3 X 3 3 3 X
