Os Paralamas do Sucesso - Só Prá Te Mostrar

Intro: D E/D Gm/D D

           D            E/D
Não quero nada que não venha de nós dois
             Gm/D
Não creio em nada
                D
Do que eu conheci antes de te conhecer
  Bm7            E7/9
Queria tanto te trazer aqui
   Gm/Bb                         D
Prá te mostrar,só prá te mostrar porque

    Gm7               D/F#
Não há nada que ponha tudo em seu lugar
   Gm7               Bm7    E/D  Gm/D  D
Eu sei, o meu lugar está aí

         Bm7         E7/9
Não vejo nada mesmo quando acendo a luz
             Gm/Bb
Não creio em nada

       D                               Bm7
Mesmo que me provem certo como dois e dois
                              E7/9
As plantas crescem em nosso jardim
 Gm/Bb                           D
Prá te mostrar,só prá te mostrar porque

    Gm7               D/F#
Não há nada que ponha tudo em seu lugar
       Gm7             D
Eu sei ,o meu lugar está aí

----------------- Acordes -----------------
Bm7 = X 2 4 2 3 2
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
E/D = X 5 X 4 5 4
E7/9 = X X 2 1 3 2
Gm/Bb = 6 5 5 3 X X
Gm/D = X 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
