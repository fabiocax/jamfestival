Os Paralamas do Sucesso - Esqueça o Que Te Disseram Sobre o Amor (Vai Ser Diferente)

Introdução: G D C D

        G                        D          C   D
Desculpas é que eu não vou pedir
               G                         D        C   D
Pelo que quero e o que não quero fazer
          G           D
Outro dia eu apareço
                 C                 D             G   D
Enquanto isso vamos nos entender
  C             D           G          D
Esqueça o que te disseram
        C                D      G   D C D
Sobre casa filhos e televisão
         G                  D          C
É preciso sangue frio pra ver
                       D
Que o sangue é quente
                 G         D   C  D
E que vai ser diferente
        G         D         C            D         G   D C D
   Ah! Ah! Ah! Ah! Ah! Ah! Vai ser diferente

        G         D         C            D         G   D C D
   Ah! Ah! Ah! Ah! Ah! Ah! Vai ser diferente
           G                D              C  D
Pode ser o que você nunca viu
           G                D              C   D
Pode ser o que você tem na mão
           G            D                       C
Pode ser exatamente o que eu digo
                    D        G D C
E também pode não
             D               G        D
Então esqueça seus sonhos
       C                    D             G D C D
Esqueça as regras e a exceção
            G            D    C      D
É mais real cru e fascinante
            G              D       C       D               G
É mortal passível de ressurreição... Ah! Ah! ...

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
