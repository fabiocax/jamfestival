Os Paralamas do Sucesso - Vovó Ondina É Gente Fina

(intro) A F#m Bm E A E

A
Silêncio meninos!
D
Toquem mais baixo
C#m                                               F#m
Que o velhinho aqui de baixo está doente de dar dó
       Bm           E                              A            E
E o rock rolava na casa da vovó
       A                                             D
Chamaram a polícia - mas que barra!
     C#m                                               F#m
Desliga essa guitarra que a coisa está indo de mal a pior
       Bm           E                              A            E
São trinta soldados contra uma vovó
D                                   A
É gente fina - vovó Ondina
         F#m
É gente fina - vovó Ondina
            Bm           E                              A            E
São trinta soldados contra uma vovó


A                             D
Estamos na rua desalojados
C#m                                               F#m
Pra ganhar alguns trocados

Temos que tocar forró
Bm           E
Vovó Ondina é gente fina
A            E
Valeu vovó!!!

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
