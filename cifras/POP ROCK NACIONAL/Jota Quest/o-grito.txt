Jota Quest - O Grito


Intro  Am F Dm Em Am F Dm G

Em    Dm          Am  Am/G
E agora, fazer o quê
Em    Dm            Am         Am/G
com a liberdade? Desorganizar o quê?
Dm   G        Am   Am/G
No meio dessa confusão?
Dm
As luzes que cegam…
               F7+
As luzes que guiam…

(Refrão)

Am
   Pra onde é que vai a alma?
F
   Pra onde irão as palavras
   Dm      Am    Am/G
Depois do grito?

Am
   Pra onde irão as palavras?
F
   Pra onde é que vai a alma
   Dm      Am   Am/G
Depois do tiro?

Em    Dm          Am  Am/G
Onde andará o futuro?
Em    Dm            Am         Am/G
Liberdade ainda que tarde
Dm   G              Am
No coração de quem vai
                  Am/G
No coração de quem vem
Dm
Terra da boa esperança
           F7+
Meu último bem! Último bem!

Am
   Pra onde é que vai a alma?
F
   Pra onde irão as palavras
   Dm      Am    Am/G
Depois do grito?
Am
   Pra onde irão as palavras?
F
   Pra onde é que vai a alma
   Dm      Am   Am/G
Depois do tiro?
             Am  Am/B  Am/C  F7+ F Am Em
Depois do tiro...

(Am F Dm Em)
  oh... oh...   oh...oh....

Am
   Pra onde é que vai a alma?
F
   Pra onde irão as palavras
   Dm      Am    Am/G
Depois do grito?
Am
   Pra onde irão as palavras?
F
   Pra onde é que vai a alma
   Dm      Am   Am/G
Depois do tiro?
              (Am  F Dm Am Am/G)
Depois do tiro...

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am/B = 4 X 4 3 1 1
Am/C = X 3 2 2 5 X
Am/G = 3 X 2 2 1 X
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F7+ = 1 X 2 2 1 X
G = 3 2 0 0 0 3
