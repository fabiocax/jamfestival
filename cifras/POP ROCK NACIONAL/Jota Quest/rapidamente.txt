Jota Quest - Rapidamente

Em7/9 : A7/13 : :: toda a música usa-se esta sequência harmônica


Rapidamente Tantas pessoas Vão passando E vão pensando
Grande e fria a cidade Que abriga tantos corações a explodir
Na correria do centro chuva Esperando o céu abrir
Procuro você no meio da multidão Mas eu não posso ver
Rapidamente
Os carros passam E os faróis
Não te iluminam Tantos lugares pra gente ir
A noite sempre alguém por perto
Pra rolar de rir E dançar pela avenida
Com os olhos abertos Esperando o sol sair

----------------- Acordes -----------------
A7/13 = X 0 X 0 2 2
Em7 = 0 2 2 0 3 0
Em7/9 = X 7 5 7 7 X
