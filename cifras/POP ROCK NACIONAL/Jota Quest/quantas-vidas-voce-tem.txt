Jota Quest - Quantas Vidas Você Tem

Intro: (E C#m7 G#m7 A E B) (2x)

    E
Meu amor
    C#m7                      G#m7
Vamos falar sobre o passado depois
            A             E
Porque o futuro está esperando
         B
Por nós dois

    E
Por favor
          C#m7                G#m7
Deixe meu último pedido pra trás
     A                E
E não volte para ele nunca
      B
Nunca mais

       C#m7             A
Porque ao longo desses meses

             E          G#7
Em que eu estive sem você
      C#m7         A            E      D
Fiz de tudo pra tentar te esquecer a a a
     C#m7         A
Já matei você mil vezes
      E             G#7
E seu amor ainda me vem
        C#m7          A    B     E
Então me diga quantas vidas você tem

Intro: (E C#m7 G#m7 A E B) (2x)

    E
Meu amor
    C#m7                      G#m7
Vamos falar sobre o passado depois
            A             E
Porque o futuro está esperando
         B
Por nós dois

    E
Por favor
          C#m7                G#m7
Deixe meu último pedido pra trás
     A                E
E não volte para ele nunca
      B
Nunca mais

       C#m7             A
Porque ao longo desses meses
             E          G#7
Em que eu estive sem você
      C#m7         A            E      D
Fiz de tudo pra tentar te esquecer a a a
     C#m7         A
Já matei você mil vezes
      E             G#7
E seu amor ainda me vem
        C#m7          A    B     E        D
Então me diga quantas vidas você tem  a a a

       C#m7             A
Porque ao longo desses meses
             E          G#7
Em que eu estive sem você
      C#m7         A            E      D
Fiz de tudo pra tentar te esquecer a a a
        C#m7          A
Então me diga quantas vidas
     C#m7         A
Já matei você mil vezes
      E             G#7
E seu amor ainda me vem
C#m7  G#m7    A    B
Diga quantas vidas
C#m7  G#m7    A    B
Diga quantas vidas
C#m7  G#m7     A  B      E
Diga quantas vidas você tem

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
G#7 = 4 6 4 5 4 4
G#m7 = 4 X 4 4 4 X
