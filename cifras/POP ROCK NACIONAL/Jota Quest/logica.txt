Jota Quest - Lógica

Intro 2x:  A  E  G  D

A      E   G       D
Lá, lá, lá, lá, lá, lá, lá, lá
A      E       G     D
Lá, lá, lá, lá, láááááá (2x)

A          E                G                D
Exagero de vida, Exagero de amor, Exagero de ego,
             A
Exagero da falta
          E                 G
Falta de vida, de amor, de valor.
                    D
Falta de graça e de grana
            A
Falta dos irmãos.
                  E
A vida é dura e injusta
                  G
A vida é crua e assusta

                  D
A vida é jogo e é luta
                  A
A vida é fogo e é cruz

A vida passa
  E
A vida é pouco.
  G
A vida é sua
         D
A vida é pouco a pouco...

D         E        D/F#
  Entre a vida e a morte
      E        D
  Onde está a lógica?
           E         F#m7
  Entre a guerra e a paz
     G
  O amor não tem lógica

A      E   G       D
Lá, lá, lá, lá, lá, lá, lá, lá
A      E       G     D
Lá, lá, lá, lá, láááááá  (2x)

A         E
 Amor por tudo
G        D
 Amor profundo
A              E          G     D
 Um amor maior que o mundo
A            E
 Amor sem paz
G         D
 Amor que dói demais
A           E      G   D
 Amor de mãe e pai

           E       D/F#
  Entre a vida e a morte
      E        D
  Onde esta a lógica?
           E         F#m7
  Entre a guerra e a paz
     G
  O amor não tem lógica
 (O amor não tem lógica)

A      E   G       D
Lá, lá, lá, lá, lá, lá, lá, lá
A      E
Lá, lá, lá, lá,
     G           D
(O amor não tem lógica)

A      E   G       D
Lá, lá, lá, lá, lá, lá, lá, lá
A      E       G     D
Lá, lá, lá, lá, láááááá

A      E   G       D
Lá, lá, lá, lá, lá, lá, lá, lá
A      E
Lá, lá, lá, lá,
     G           D
(O amor não tem lógica)

( A G D/F# E A Am7M A )

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am7M = X 0 2 1 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
E = 0 2 2 1 0 0
F#m7 = 2 X 2 2 2 X
G = 3 2 0 0 0 3
