Jota Quest - Ela É

 G               Em
 Eu a olhei em silêncio
     C              D
 Guardei na memória o perfume
     G                   Em
 E aquele seu jeito de olhar
C               D
 Me colocou em mal costume.

Am                D
Hoje eu durmo e acordo
        G     Em
E penso nela...
 Am                  D
 Sonho espero e exploro
                G   Em
 Os sentimentos dela

 G                  Em
 Mas eu guardei o segredo
 C                  D
 Por muito tempo em meu coração

        G              Em
 E não sei se irei revelar
     C               D
 O amor que sinto é como um vulcão

 Am                      D
 Mostra a grandeza e a beleza
              G     Em
 Que dedico a ela...
Am                D
 Mas se explode é força é calor
              G     Em
 Pode afastar ela.


     REFRÃO
 G
 Ela é
      Em
 O que eu sempre quis
   C
 É sem exagero
    D
 Um anjo pra mim
G
 Ela é
   Em
 O beijo na boca
   C
 A paixão mais louca
     D
 Que eu já senti

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
