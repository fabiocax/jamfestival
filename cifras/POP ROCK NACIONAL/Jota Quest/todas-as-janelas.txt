Jota Quest - Todas As Janelas

Rogério Flausino


Intro :  A   E/G#   F#m   F#m7/E   D7M   A/C#   Bm7   A   -  2x


D7M       C#m7     Bm7                A
   Todo amor,        Todo o meu apreço
D7M              C#m7           Bm7           A
   Todo recomeço,     Toda boa intenção
D7M            C#m7    Bm7                             A
   Toda idéia nova,       Todas as noites que eu não dormi
D7M          C#m7              Bm7                            A
   Toda nova informação,      Tantas saudades do que ainda não vivi
D7M               C#m7               Bm7                          A
   Vencer os contra-tempos,         Passar mais tempo juntos
D7M                C#m7                Bm7                   A
   Consciência e paciência,          Intensas modificações

D7M              C#m7                             F#m
   A mais completa certeza,       uh, uh, uh, uh

                                                   D7M
   De que tudo vai dar certo,     eh,  eh
                       C#m7                Bm7                       REFRÃO
   A mais completa certeza
                                                Intro    1x
   De que tudo vai dar certo,   eh,  eh

D7M                  C#m7       Bm7                A
   Pra quem vem de longe,      Para quem faz rir
D7M               C#m7
   Pra quem está no centro das decisões,
 Bm7                         A
   Pra quem espera  e  sempre alcança
D7M             C#m7         Bm7                   A
   Pra quem é sem-terra,     Pra quem `ta sem tempo
D7M                      C#m7
   Pra quem `ta muito louco,  n`uma  boa,
 Bm7                         A
   Pra toda essa nossa gente no sufoco

D7M              C#m7                             F#m
   A mais completa certeza,       uh, uh, uh, uh
                                                            D7M
   De que tudo vai dar certo,   até o fim
                     C#m7                 Bm7                       REFRÃO
   A mais completa certeza
                                                 A
   De que tudo vai dar certo,   eh,  eh
               D
La, la, la, la, la...
A
La...     yeah!
               D
La, la, la, la, la        Deixe a luz entrar
A                                                          D
   (Todas as janelas,    da sua casa aberta)
A                                                 D
   (Toda a injustiça   desse país,   a  espera)
A
La...     yeah!
               D                                          (A D)
La, la, la, la, la        Deixe a luz entrar...           (fade out)

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
D7M = X X 0 2 2 2
E/G# = 4 X 2 4 5 X
F#m = 2 4 4 2 2 2
F#m7/E = X X 2 2 5 2
