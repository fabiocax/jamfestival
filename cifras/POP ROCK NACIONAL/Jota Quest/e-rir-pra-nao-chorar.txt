Jota Quest - É Rir Pra Não Chorar

INTRO: (E7/9+)

(E7/9+)
To pensando muito sério
Em mudar meu raciocínio
To querendo ficar zen
Mas não tenho patrocínio
Falta o líquido e o certo,
Falta até água da chuva
Pra lavar essa sujeira
Pra levar essa minha angústia

Eu sei que eu decidi assim
Não vou ficar parado aqui
Sem fazer nada
Eu te aconselho a vir também
Porque já não dá mais pra deixar pra lá

            (A7)
Tem gente que ta puro lixo
E quem ta com a mão mais suja?

O empresário ou o político?
O acusado, ou quem acusa?

         (B7/9)
Eu peço a atenção ao povo
Quando for eleger de novo
Se lembrem de tudo
E pensem no futuro

(E7/9+)
É, tá brabo!
É rir pra não chorar       (coro 4x)

        (E7/9+)
É que agora veio à tona
O que já tava acontecendo
Acabou não tem mistério
O que estavam escondendo
Não se sabe de onde vem
Só que é muita grana suja
Dividiram entre eles
O que era de outros mil

Eu sei que eu decidi assim
Não vou ficar parado aqui
Sem fazer nada
E eu te aconselho a vir também
Por que já não dá mais pra deixar pra lá

             (A7)
Não é possível que essa troup
Depois dessa, saia impune
Que, senão, Deus nos ajude
Que eu tenha força e atitude
             (B7/9)
Eu peço a atenção ao povo
Quando for eleger de novo
Se lembrem de tudo
E pensem no futuro

(E7/9+)
É, tá brabo!
É rir pra não chorar       (coro 4x)  SOLO em (E7/9+)

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
B7/9 = X 2 1 2 2 X
E7/9+ = X 6 5 6 7 X
