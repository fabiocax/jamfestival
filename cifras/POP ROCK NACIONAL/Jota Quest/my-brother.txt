Jota Quest - My Brother


Intro: Cm7/9  F#m7  G#m7

Cm7/9
Se liga aí my brother Não dá moleza não
Fm7 (G7/b13 G7 )
Trilha a tua estrada A tua direção
Cm7/9
Se liga aí my brother Não perca tempo não
Fm7 (G7/b13 G7 )
Levanta essa cabeça e vai Intro
Cm7/9
O caminho não se encontra A gente mesmo faz
Fm7
E quando a estrada e longa
( G7/b13 G7 )
A gente vive mais
Cm7/9                  Fm7
Se liga aí my brother Siga o seu coração
( G7/b13 G7)
O grande lance é ser feliz

Fm7
E o que for
Gm7
Na vontade vale
Ab7M
E o que for
Gm7
De verdade vale
Fm7
E o que for
Gm7             Ab7M                 Bbm7
Numa boa vale, o grande lance é a gente ser feliz

----------------- Acordes -----------------
Ab7M = 4 X 5 5 4 X
Bbm7 = X 1 3 1 2 1
Cm7 = X 3 5 3 4 3
Cm7/9 = X 3 1 3 3 X
F#m7 = 2 X 2 2 2 X
Fm7 = 1 X 1 1 1 X
G#m7 = 4 X 4 4 4 X
G7 = 3 5 3 4 3 3
G7/b13 = 3 X 3 4 4 3
Gm7 = 3 X 3 3 3 X
