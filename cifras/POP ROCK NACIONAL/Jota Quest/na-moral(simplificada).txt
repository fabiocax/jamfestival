Jota Quest - Na Moral

Versão Ao Vivo Rio de Janeiro


Intro: (Cm,Ab,Bb) Dm7,Am7,Gm7

       Dm7   Am7       Gm7
Na moral, na moral (só na moral)
Dm7   Am7    Gm7
Na moral!

   Dm7        Am7    Gm7
Vivendo de folia e caos
  Dm7   Am7      Gm7
Quebrando tudo, pra variar
   Dm7          Am7     Gm7
Vivendo entre o sim e o não
  Dm7  Am7   Gm7
Levando tudo na moral
 Dm7         Am7   Gm7
Uma manchete de jornal

   Dm7 Am7    Gm7
Não vou deixar me abalar
     Dm7        Am7   Gm7
Mais uma noite, carnaval
Dm7 Am7      Gm7   Am7
No Brasil, só na moral

Dm7   Am7       Gm7
Na moral, na moral (só na moral)
Dm7   Am7   Gm7   Am7
Na moral!

Dm7           Am7      Gm7
Viver entre o medo e a paz
  Dm7 Am7      Gm7
Pode fazer pensarmos mais
   Dm7             Am7   Gm7
No que a gente tem que fazer
  Dm7  Am7     Gm7
Pra ficar vivo, pra variar

Bb7M           Am7          C/D
Quando tudo parece não ter lógica
Bombas de amor, tiros de amor, drogas de amor
Bb7M           Am7       Gm7
Qualquer paranóia vai virar prazer de viver...
       Dm7   Am7          Gm7
Na moral, na moral (só na moral)
Dm7   Am7    Gm7    Am7
Na moral

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Am7 = X 0 2 0 1 0
Bb = X 1 3 3 3 1
Bb7M = X 1 3 2 3 1
C/D = X X 0 0 1 0
Cm = X 3 5 5 4 3
Dm7 = X 5 7 5 6 5
F = 1 3 3 2 1 1
Gm7 = 3 X 3 3 3 X
