O Rappa - Na Horda

[Intro] Gm  Cm  Gm  Cm
        Gm  Cm  Gm  Cm
        Gm  Cm  Gm  Cm
        Gm  Cm  Gm  Cm

E|--------------------------------------------------|
B|--------------------------------------------------|
G|--------------------------------------------------|
D|--------------------------------------------------|
A|-------------------------------3/5-5/6--6-6/5-5/3-|
E|-3-6-8/10-8--10-8/6-6/5---3-6---------------------|

E|--------------------------------------------------|
B|--------------------------------------------------|
G|--------------------------------------------------|
D|--------------------------------------------------|
A|-------------------------------3/5-5/6--6-6/5-5/3-|
E|-3-6-8/10-8--10-8/6-6/5---3-6---------------------|

 Gm
Nascer todos os dias

 Cm
Tem tempo e tem hora
 Gm
Não se arrependa
 Cm
Não corra das pedradas de outrora
 Gm
Tudo passo com toda a sorte
Cm
Uma reza bem forte
 Gm                         Cm
Vamos achar nosso lugar nessa horda

 Gm
Nascer todos os dias
 Cm
Tem tempo e tem hora
 Gm
Não se arrependa
 Cm
Não corra das pedradas de outrora
 Gm
Tudo passo com toda a sorte
Cm
Uma reza bem forte
 Gm                         Cm
Vamos achar nosso lugar nessa

             Gm
Ta na força do empenho
                    Cm
Chama palavra desistência
                      Gm
Como ciência de um engenho
               Cm
Moer estrelas sem sentença
                  Gm
Quem diria o que seria
                    Cm
Nas mãos das unhas do destino
                    Gm          Cm
Isso é repleto, repleto de toda a guia

( Gm  Cm  Gm  Cm )
( Gm  Cm  Gm  Cm )

               Gm
Mergulho nos planos que tenho
                 Cm
Quando caio cai tudo que traço
              Gm
Chama a resistência dos cantos
           Cm
E o céu se abre quando eu passo

               Gm
Mergulho nos planos que tenho
                 Cm
Quando caio cai tudo que traço
            Gm
O mundo morrendo por foto
             Cm
E o povo querendo um abraço

( Gm  Cm  Gm  Cm )
( Gm  Cm  Gm  Cm )

 Gm
Nascer todos os dias
 Cm
Tem tempo e tem hora
 Gm
Não se arrependa
                  Cm
Não corra das pedradas de outrora
 Gm
Tudo passo com toda a sorte
     Cm
Uma reza bem forte
 Gm                         Cm
Vamos achar nosso lugar nessa

               Gm
Mergulho nos planos que tenho
                 Cm
Quando caio cai tudo que traço
              Gm
Chama a resistência dos cantos
           Cm
E o céu se abre quando eu passo

               Gm
Mergulho nos planos que tenho
                 Cm
Quando caio cai tudo que traço
              Gm
Chama a resistência dos cantos
           Cm
E o céu se abre quando eu passo

( Gm  Cm  Gm  Cm )
( Gm  Cm  Gm  Cm )

 Gm
Nascer todos os dias
 Cm
Tem tempo e tem hora
 Gm
Não se arrependa
                  Cm
Não corra das pedradas de outrora
 Gm
Tudo passo com toda a sorte
     Cm
Uma reza bem forte
 Gm                         Cm
Vamos achar nosso lugar nessa horda

 Gm
Nascer todos os dias
 Cm
Tem tempo e tem hora
 Gm
Não se arrependa
                  Cm
Não corra das pedradas de outrora
 Gm
Tudo passo com toda a sorte
     Cm
Uma reza bem forte
 Gm                         Cm
Vamos achar nosso lugar nessa

               Gm
Mergulho nos planos que tenho
                 Cm
Quando caio cai tudo que traço
              Gm
Chama a resistência dos cantos
           Cm
E o céu se abre quando eu passo

               Gm
Mergulho nos planos que tenho
                 Cm
Quando caio cai tudo que traço
            Gm
O mundo morrendo por foto
             Cm
E o povo querendo um abraço

( Gm  Cm  Gm  Cm )
( Gm  Cm  Gm  Cm )

               Gm
Mergulho nos planos que tenho
                 Cm
Quando caio cai tudo que traço
              Gm
Chama a resistência dos cantos
           Cm
E o céu se abre quando eu passo

               Gm
Mergulho nos planos que tenho
                 Cm
Quando caio cai tudo que traço
            Gm
O mundo morrendo por foto
             Cm
E o povo querendo um abraço

( Gm  Cm  Gm  Cm )
( Gm  Cm  Gm  Cm )

               Gm
Mergulho nos planos que tenho
                 Cm
Quando caio cai tudo que traço
              Gm
Chama a resistência dos cantos
           Cm
E o céu se abre quando eu passo

               Gm
Mergulho nos planos que tenho
                 Cm
Quando caio cai tudo que traço
             Gm
O povo precisando de um abraço
             Cm
O povo precisando de um abraço

( Gm  Cm  Gm  Cm )
( Gm  Cm  Gm  Cm )

----------------- Acordes -----------------
Cm = X 3 5 5 4 3
Gm = 3 5 5 3 3 3
