O Rappa - A Noite


Bm
A NOITE!
                                  A
QUANDO O CALOR SE MISTURA
                    Bm                                    A
COM A LUZ DA TV PRETO E BRANCO
Bm
A NOITE!
                     A
EU QUIETO DENTRO DE CASA
                         Bm                     A
OUVINDO RAJADAS DE BALAS
Bm
A NOITE!
                                     A
FATOS RUINS DO JORNAL
                         Bm                  A
SE UNEM AO MEU CANSAÇO
Bm
A NOITE!

                                    A
E O MESMO CORPO CANSADO
                          Bm                                       A
AS VEZES SE PERDE EM FRENTE A SAÍDA
   E
MESMO ASSIM EU PARO E AGRADEÇO
POR EU NÃO FAZER DO RANCOR MINHA VIDA
E POR AINDA ACREDITAR NO PODER DO AMOR
                                                  G
REVOLUCIONÁRIO E SALVADOR
                                   A
AMOR QUE ME TIROU

A ARMA DA MÃO
Bm                                                A
E MEU DEU MAIS ESSA CANÇÃO

A ARMA DA MÃO
Bm                                                A
E MEU DEU MAIS ESSA CANÇÃO
 G                                                  A
AMOR O AMOR QUE ME TIROU
                         Bm
E EU QUIETO DENTRO DE CASA
                          A
OUVINDO RAJADA DE BALA
                         Bm                        A
O TIRO HUM! HUM! HUM! HUM!
                         Bm
E O CALOR SE MISTURANDO
                         A                           Bm                             A
COM A LUZ DA TV PRETO E HUM!HUM! HUM!HUM!

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
