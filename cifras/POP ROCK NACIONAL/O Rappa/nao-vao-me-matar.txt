O Rappa - Não Vão Me Matar

Intro: Em B Am B

Em           B              Am
Eu não quero mais saber de sofrer não   |
   B                                    | 4x
Não quero não                           |

Em                      B
Me contam histórias diferentes
 Am                     B
Achando que eu vou acreditar
Em                      B
Enganam o povo com promessas
 Am                     B
Tentando induzi-los a acreditar
Em                      B
Que a consciência do ser humano
 Am                  B
De repente pode até falhar
Em                      B
Quem bate esquece, quem apanha

 Am                  B
E quem apanha quer se vingar
 C                  B         C
Criticam uma raça tão bonita, que é
            B
Capaz de aguentar
Em                  B           Am
Torturas, humilhações à parte, podem pisar
        B
Mas não vão me matar

Em
Cantando a verdade, falando da vida             |

B                                               |
Contando história, falando de amor              | 

Brigando com a vida, pra ser mais feliz         |
B                                               |
Não vão me matar                                |


Em           B              Am
Eu não quero mais saber de sofrer não   |
   B                                    | 4x
Não quero não                           |


Repete mais 1 vez toda a musica

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
C = X 3 2 0 1 0
Em = 0 2 2 0 0 0
