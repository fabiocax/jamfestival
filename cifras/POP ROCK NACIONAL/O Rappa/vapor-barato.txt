O Rappa - Vapor Barato

Intro 2x:

 Am      G/A
Uauau.. Uauauauau..
 F       Dm  E7
Uauau.. Uauauauau..

Primeira parte:
 Am                G/A
Sim, eu estou tão cansado
     F
Mas não pra dizer
Dm                     E7
  Que eu não acredito mais em você
Am
  Com minhas calças vermelhas
G/A
  Meu casaco de general
  F
Cheio de anéis
Dm                     E7
  Eu vou descendo por todas as ruas

Am
  Eu vou tomar aquele velho navio
G/A                            F
  Eu vou tomar aquele velho navio
                Dm  E7
Aquele velho navio
Am
  Eu não preciso de muito dinheiro
  G/A
Graças a Deus
F
E não me importa
Dm                  E7
  E não me importa não

Refrão:
               Am
A minha Honey Baby
 G/A   F      Dm    E7
Baby, Baby , Honey Baby
                Am
Oh Minha Honey Baby,
 G/A   F     Dm    E7
Baby, Baby, Honey Baby

Segunda parte:

 Am                G/A
Sim, eu estou tão cansado
     F
Mas não pra dizer
Dm                     E7
  Que eu estou indo embora
Am
  Talvez eu volte
    G/A
Um dia eu volto quem sabe
 F
Mas eu preciso
Dm                 E7
  Eu preciso esquecê-la
Am
  A minha grande
   G/A
A minha pequena
   F
A minha imensa obsessão
Dm                  E7
  A minha grande obsessão

Refrão:
               Am
A minha Honey Baby
 G/A   F      Dm    E7
Baby, Baby , Honey Baby
                Am
Oh Minha Honey Baby,
 G/A   F      Dm   E7
Baby, Baby, Honey Baby

(repente intro 2x)

(repete primeira parte e refrão)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Dm = X X 0 2 3 1
E7 = 0 2 0 1 0 0
F = 1 3 3 2 1 1
G/A = X 0 5 4 P3 3
