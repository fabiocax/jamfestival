O Rappa - Coincidências e Paixões

Intro.: Am Dm Em Am Dm Em (2x)

      Am
Tem razão que tem paixão
      Dm                   Em
Tem razão que fala com a voz do coração (2x)

       Am                             Dm
Considerando a gente como fruto de algo maior,
  Em
maior do que tudo
 Am                                  Dm
A gente começa então a entender que não,
                    Em
que não, que não seria um absurdo

      Am
Coincidências e paixões de repente acontecerem
             Dm
Coincidências e paixões
  Em
Uh Uh


     Dm              G
O destino pode mudar como o vento
  Am
Nada é tão planejado assim
  Dm          G           Am
Certo,inatingível como eles parecem dizer

Dm G Am Dm G  Am (2x)

Ôôô, ai ai ai... Uô uôô ai ai ai...

   Am
Betinho, Gandhi, Charlie Chaplin,
           Dm                 Em
Albert Sabin, Bob Marley, Dona Zica,
          Am
Grande Otelo, Cartola, Che Guevara,
             Dm                    Em
Mãe Menininha, Dalai Lama e Rei Dadá pow pow (2x)

Am Dm Em Am Dm Em (2x)

Tem Razão quem fala fala com a voz do coração(7x)

Obs: A música fica mais parecida tocando todos os acordes com pestana.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
