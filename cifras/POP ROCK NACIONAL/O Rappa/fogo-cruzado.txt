O Rappa - Fogo Cruzado

INTRO(Dm,C)

         Dm  C  Dm  C
Eu tô no fogo cruzado
           Dm  C  Dm  C
Vivendo em fogo  cruzado
         Dm                 C
E eu me sinto encurralado de novo
   Dm                      C
No Gueto o medo abala quem ainda corre atrás
    Dm                      C
Do fascínio que traz o medo da escuridão
        Dm   C          Dm   C
Que é a vida,  Que é a vida

   Dm            C
No Gueto o medo ilude e seduz
 Dm           C
Com o poder da cocaína
      Dm                  C                Dm      C
Que comanda o sucesso das bocas de fumo da esquina


       Dm
Mas a favela não é mãe
        C
De toda dúvida letal
       Dm
Talvez seja de maneira
        C
Mais direta e radical
Dm
O sol que assola
      C
Esses jardins suspensos
   Dm         C        Bb          A
Da má distribuição, Da má distribuição

      Dm
Que arranham o céu
           C
Mas não percebem o firmamento
       Dm
Que se banham à beira-mar
           C
Mas não se limpam por dentro
         Dm
Que se orgulham do Cristo
    C                      Dm                      C
De braços abertos, mas não abrem as mãos pra novos ventos

       Dm   C   Dm   C
Tô no  fogo    cruzado
           Dm  C   Dm    C
Vivendo em fogo    cruzado

        Dm                           C
Entre a Bélgica e a Índia, entre a Jamaica e o Japão
        Dm                         C
Entre o Congo e o Canadá, onde a guerra nunca tá
        Dm                     C
Entre o norte e o sul, entre o mínimo e o Maximo
    Dm                 C
Denominador comum, denominador comum

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
