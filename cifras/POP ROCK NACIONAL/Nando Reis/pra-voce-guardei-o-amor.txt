Nando Reis - Pra Você Guardei o Amor

[Intro] D  A   Bm  Bm7/A
        G  D9/F#  Em7  A7  A7/4  A7

[Primeira Parte]

  D                          A
Pra você guardei o amor que nunca soube dar
            Bm
O amor que tive e vi, sem me deixar
 Bm7/A    G
Sen______tir, sem conseguir provar
D9/F#    Em7              A7  A7/4   A7
     Sem entregar e repartir

  D                          A
Pra você guardei o amor que sempre quis mostrar
            Bm
O amor que vive em mim, vem visitar
 Bm7/A    G
Sor______rir, vem colorir, solar
D9/F#    Em7               A7  A7/4  A7
     Vem esquentar e permitir


     D                         A
Quem acolher o que ele tem e trás
                         Bm
Quem entender o que ele diz no giz
                Bm7/A    G
Do gesto, o jeito      pronto num piscar dos cílios
D9/F#  Em7                       A7  A7/4       A7
     Que o convite do silêncio exibe em cada olhar

[Refrão]

     D                A             Bm
Guardei, sem ter porque, nem por razão
                 Bm7/A   G
Ou coisa outra qual____quer
D9/F#  Em7                     A7
     Além de não saber como fazer
            G               A7
Pra ter um jeito meu de me mostrar
D                 A          Bm
Achei vendo em você, explicação
              Bm7/A  G
Nenhuma isso re____quer
        D9/F#  Em7                 A7
Se o cora_____ção bater forte e arder
           G         A7
No fogo o gelo vai queimar

[Segunda Parte]

  D                         A
Pra você guardei o amor que aprendi vem dos meus pais
    Bm                   Bm7/A  G                      D9/F#   Em7
O amor que tive e recebi e     hoje posso dar livre e fe______liz
                                A          A7/4       A7
Céu, cheiro e o ar na cor que o arco íris risca ao levitar

 D                         A
Vou nascer de novo, lápis, edifício, tevere, ponte
 Bm
Desenhar no seu quadril
 Bm7/A  G
Meus   lábios beijam signos feito
D9/F#  Em7                              A7    A7/4   A7
      Se nos trilho a infância terço o berço do seu lar

[Refrão]

     D                A             Bm
Guardei, sem ter porque, nem por razão
                 Bm7/A   G
Ou coisa outra qual____quer
D9/F#  Em7                     A7
     Além de não saber como fazer
            G               A7
Pra ter um jeito meu de me mostrar
D                 A          Bm
Achei vendo em você, explicação
              Bm7/A  G
Nenhuma isso re____quer
        D9/F#  Em7                 A7
Se o cora_____ção bater forte e arder
           G         A7
No fogo o gelo vai queimar

[Final] D  A  Bm  Bm/A
        G  D9/F#  Em7  A7  A7/4  A7

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
A7/4 = X 0 2 0 3 0
Bm = X 2 4 4 3 2
Bm/A = X 0 4 4 3 X
Bm7/A = X 0 4 P2 3 P2
D = X X 0 2 3 2
D9/F# = 2 X 0 2 3 0
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 3 3
