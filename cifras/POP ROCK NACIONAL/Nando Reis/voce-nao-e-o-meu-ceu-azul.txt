Nando Reis - Você Não É o Meu Céu Azul

(D  D4  D  D4)

 D
Você não é o meu céu azul
  D7+
Não sou a nuvem que você quer desenhar
  D7
A tempestade que vai alagar as ruas
   G
É só outra chuva que vai tudo molhar
     Gm
Noé fez a arca    Enfrentou o dilúvio
 D          Bm
A ira de deus
  Em
Todos se salvaram
 A                      D           D D4 D D4
Menos você e eu

   D
Sou a lagarta depois o casulo

  D7+
Dois olhos furam a borboleta
  D7
Creêm que a miragem dos óculos escuros
   G
Podem apagar a fúria em brasa acesa
   Gm
A carcaça feita de sequilho
  D             Bm
Burra muralha de gesso
   Em
Embaça a fumaça
  A                 D        D4  D   D4
Desse nevoeiro...
 D
Na gargalhada falta o sorriso
  D7+
O oceano engole o rio do choro
  D7
Na madrugada você está dormindo
  G
Na lousa abstrata do seu sonho
  Gm
Exalta a palavra o grifo
  D           Bm
Pedindo socorro
  Em
Sua encruzilhada
A                       D   D4  D
Sem medalha ou louros
(Em   D)
Ah Ah Ah Ah............

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D4 = X X 0 2 3 3
D7 = X X 0 2 1 2
D7+ = X X 0 2 2 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
