Nando Reis - O Seu Lado de Cá

Nando Reis

INTRO : E   A    E   A    E  C  D E   A   E   A

E
Na minha frente eu vejo o mar
      A
Na frente do mar está a ilha
E
Na minha frente eu vejo o mar
     A
Na frente do mar está a ilha

F      G   Fm6/Ab   G     E  A  E  A   E  A
Ter    é     co    -     mo  dar

E                A
Lá onde nasce o dia
E                A
Lá onde cresce o mar
F#m                 A
Longe o mundo, a ilha

 E
Vendo o seu lado de cá
 A
Pelo seu lábio de maré
 E
Deixo o seu nome chamar
 A
Até ter fim
ouvindo

F      G    Fm6/Ab  G      E  A  E  A  E  A
Ter    é       co   -    mo    dar

----------------- Acordes -----------------
A = X 0 2 2 2 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
Fm6/Ab = 4 5 3 5 3 X
G = 3 2 0 0 0 3
