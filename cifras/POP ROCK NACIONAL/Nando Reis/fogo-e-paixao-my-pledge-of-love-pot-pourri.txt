Nando Reis - Fogo e Paixão / My Pledge Of Love (pot-pourri)

 C
Você é luz
               E7
é raio estrela e luar
   Am
Manhã de sol
              Gm   C7
Meu iaiá meu ioiô
  F          G
Você é sim
           C   Am
Nunca meu não
             Dm
Quando tão louca
             G               C   G7
Me beija na boca me ama no chão

 C
Você é luz
               E7
é raio estrela e luar

   Am
Manhã de sol
              Gm   C7
Meu iaiá meu ioiô
  F         G
Você é sim
           C   Am
Nunca meu não
             Dm
Quando tão louca
             G               C
Me beija na boca me ama no chão

( C9  B9  Bb9 ) (2x)

C
 I woke up this morning baby
E
 I had you on my mind
Am
 I woke up this morning baby
C7
 You know that I felt so fine
F           Fm
 You know I need you
C                Am
 You know that I love you
Dm          G7
 This is my pledge of love to

C             Bb
 My pledge of love (My pledge of love)
C             Bb
 My pledge of love (My pledge of love)
C                  Bb         C
 Darling, darling, darling to you (My pledge of love)
Bb        C    Bb
 Yeah, to you, yeah

C
 I woke up this morning baby
E
 I had you on my mind
Am
 I felt so much love this morning
C7
 Little girl, little girl you feel so fine
F           Fm
 You know I need you
C                Am
 You know that I love you
Dm                 G7
 Darling this is my pledge of love

C             Bb
 My pledge of love (My pledge of love)
C             Bb
 My pledge of love (My pledge of love)
C                  Bb         C
 Darling, darling, darling to you (My pledge of love)
Bb        C    Bb
 Yeah, to you, yeah...

-Agente vai p´ra segundinha

            ( A )
 Sky gettin' dark above me
 Baby I need your lovin'
 Got to have all of your lovin'
 Baby I want your lovin'
 Baby (baby), baby (baby)
 Baby (baby), baby (baby)
 Baby (baby), baby (baby)
 Baby (baby), baby .

C
 I woke up this morning baby
E
 That's when I caught your name
Am
 I felt so much love this morning
C7
 Little girl, little girl for shame
F           Fm
 You know I need you
C                Am
 You know that I love you
Dm          G7
 This is my pledge of love to

 C             Bb
 My pledge of love (My pledge of love)
C             Bb
 My pledge of love (My pledge of love)
C                  Bb         C
 Darling, darling, darling to you (My pledge of love)
Bb        C    Bb                  C        Bb                 C         Bb                    C
 Yeah, to you,(My pledge of love) ,to you, (My pledge of love),to you, (My pledge of love)to you

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B9 = X 2 4 4 2 2
Bb = X 1 3 3 3 1
Bb9 = X 1 3 3 1 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
C9 = X 3 5 5 3 3
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
