Nando Reis - Back In Vânia

Intro: D7  G (3X)

 D7     G            D7            G
O muro cor-de-rosa leva a Bertioga
D7      G         D7             G
Lá de Casemiro trouxe a flor azul
D7       G        D7                  G
Eu não vim de Marte, porém sou vermelho
D7       G       D7               G       C#7  C7
Grotta a terra roxa no Pomar de Jahu
C7
Cristina não é santa, só em nome de rua

Vai pra Ubatuba vem do Frei Galvão

Paro lá no Perez indo pra Sununga
 A
Lázaro namora a Domingas
             G       D7
Eu nasci no verão de 63
    D7       G      D7        G
Tenho 5 filhos, fiz uma família

  D7        G      D7           G
Trouxe de Saturno um anel de leão
  D7         G      D7              G
Onde, hoje, moram minha mãe Cecília,
   D7     G        D7               G   C#7   C7
Cássia e Marcelo? - dentro do meu coração
 C7
Dorme na ladeira o Senhor do Tempo

Sua ampulheta trago em minhas mãos

Gira o planeta, encombre a luz da Lua
  A
Se uma estrela deu o Sol eu quero uma constelação
  G         D7         ( D7  G )(2X)
De Astros-Reis
    D7      G       D7        G
Eu sou muito prosa ela silencia
  D7         G       D7              G
Ouvindo "Se oriente" me lembrei do Japão
  D7        G     D7         G
Nosso casamento, nossa sacristia
  D7       G     D7            G      C#7  C7
Foi apostilaria o topo de um vulcão
  C7
Entrei para o convento lá em São Domingos

Entrei sai correndo, pelo o amor de deus

Entrei para as paradas com meus oito amigos
   A
Entrei saí cheguei parti chorei sorri amei sofri matei morri
  G                D7
E ela estende sua mão
    D7        G        D7          G
A minha vida nunca foi um mar de rosas
    D7     G        D7       G
Embora seja rosa o meu céu azul
    D7      G       D7         G
Sal é cocaína para o Mar Vermelho
    D7     G       D7          G     C#7  C7
Nado na piscina da Agostinho Cantú
  C7
Não sei ainda quanto tempo temos de vida

A vida já vivida já nos deu razão

Teve sofrimento, e tanta alegria
   A
Mas o que eu desejo é que o tempo
                  G
Venha lento, dia a dia

Entre dentro com o vento
                    A
Seja fresco como a brisa

Como beijo o seu peito
G             G
O amor que contagia

Pra que eu diga que a vida
 D7       G         D7    G    D7  G
Sem ela, não tem razão

----------------- Acordes -----------------
A = X 0 2 2 2 0
C#7 = X 4 3 4 2 X
C7 = X 3 2 3 1 X
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
