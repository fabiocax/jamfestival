Nando Reis - Coração Vago

Intro: E F#m A E

E                                                         F#m
Eu sei que não basta simplesmente mais uma vez dizer "I love you”
A                                                           E
Até mesmo porque "I love you " nunca soa bem - meu inglês é péssimo
E                                                                                         F#m
Eu sei que eu gostaria de agir de um modo diferente mas você nunca pediu pra que eu fosse outro
A                                                                         E
Mas quem desejaria que outro alguém além do seu amor, pra sua dor fosse remédio?
          F#m                                                       A               E
Então eu preciso te dizer que agora eu sei que até mesmo um grande amor pode não bastar
    F#m                                      A                        E
Aprendi com você que no dia a dia o grande amor abrange sonho e vida real
     F#m                                   G#m                             A            B
E eu quero te dizer que eu mudei que já cansei de fingir, fugir, mentir, sumir, não encarar

(Refrão)

E                                   F#m
Não tenho dúvida, é com você que eu quero viver

A                                         E
Mas outras dúvidas eu tenho e elas me atrapalham
E                                           F#m
Não é um conceito ou um defeito, é só o meu jeito de ser
A
Sou um sujeito imperfeito
Mas o lado esquerdo do peito
       B
Por inteiro te ofereço
             E
Meu coração vago

(Intro)

          F#m                                                       A               E
Então eu preciso te dizer que agora eu sei que até mesmo um grande amor pode não bastar
    F#m                                      A                        E
Aprendi com você que no dia a dia o grande amor abrange sonho e vida real
     F#m                                   G#m                             A            B
E eu quero te dizer que eu mudei que já cansei de fingir, fugir, mentir, sumir, não encarar

(Refrão)

E                                   F#m
Não tenho dúvida, é com você que eu quero viver
A                                         E
Mas outras dúvidas eu tenho e elas me atrapalham
E                                           F#m
Não é um conceito ou um defeito, é só o meu jeito de ser
A                                    E
Lama vira terra dura, quando a chuva alaga
E                                   F#m
Não tenho dúvida, é com você que eu quero viver
A
Sou um sujeito imperfeito
Mas o lado esquerdo do peito
Por inteiro te ofereço
E espero que aceite
        B
Que se deite neste leito
             E        A Am E
Meu coração vago

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
