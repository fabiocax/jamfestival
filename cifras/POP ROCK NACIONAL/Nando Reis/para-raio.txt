Nando Reis - Para Raio

(Intro) D (riff)

G
Passa o tempo, lento ensaio,  Espero você aqui
                            C            G
Paro dentro, entro e saio,  Falta você aqui
 G
Calo invento, quieto falo,  Trago você aqui
                              C            G   D (riff)
Lato intenso em detalhes,   Quero você aqui
 G
Abro e vejo da janela, Fogos de artifício
                      C                G
Estrelas em mosaico,   Vidro opaco a luzir
 G
Vasto imenso, feito em partes,  Fácil para construir
                            C                    G
Rasgo ao meio em metades,  Acho você dentro de mim

Am             D             Am
 Assim como o sal feito no mar

                   D        Bb9
 Azul como o céu e a imensidão
              C9              Bb9
 Que enche o pulmão de pleno ar
             C9    G    D (riff)
 Achei seu lugar meu

 G
Seu brinquedo imaginário, Feito pra te distrair
                            C            G
Paro dentro, entro e saio,  Falta você aqui
 G
Noite inteira, fim de tarde, Meu calendário marca o infinito
                            C                    G
Em trincheiras que escavo,  Acho você dentro de mim

Am             D             Am
 Assim como o sal feito no mar
                   D        Bb9
 Azul como o céu e a imensidão
              C9              Bb9
 Que enche o pulmão de pleno ar
             C9    G    D (riff)
 Achei seu lugar meu

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb9 = X 1 3 3 1 1
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
G = 3 2 0 0 0 3
