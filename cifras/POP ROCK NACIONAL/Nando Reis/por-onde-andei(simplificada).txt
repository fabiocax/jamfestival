Nando Reis - Por Onde Andei

[Intro] C  G  Dm  F
        C  G  Dm  F

            C
Desculpe estou um pouco atrasado
       G
Mas espero que ainda de tempo
      Dm                          F
De dizer que andei errado e eu entendo
          C
As suas queixas tão justificáveis
            G
E a falta que eu fiz nessa semana
 Dm                            F
Coisas que pareceriam óbvias até pra uma criança

[Refrão]

    C
Por onde andei
            G
Enquanto você me procurava

   Bb5(9)                 F
Será que eu sei que você mesmo é tudo
                  C    G Dm F
Aquilo que me faltava?

         C
Amor eu sinto a sua falta
             G
E a falta é morte da esperança
        Dm
Como o dia em que roubaram o seu carro
    F
Deixou uma lembrança
              C
Que a vida é mesmo coisa muito frágil
       G
Uma bobagem uma irrelevância
  Dm                      F
Diante da eternidade do amor, de quem se ama

[Refrão]

    C
Por onde andei
            G
Enquanto você me procurava
      Bb5(9)                  F
E o que eu te dei, foi muito pouco ou quase nada
    C                     G
E que eu deixei, algumas roupas penduradas

   Bb5(9)                 F
Será que eu sei que você mesmo é tudo
                  C    G Dm F
Aquilo que me faltava?

----------------- Acordes -----------------
Bb5(9) = X 1 3 3 1 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 3 3
