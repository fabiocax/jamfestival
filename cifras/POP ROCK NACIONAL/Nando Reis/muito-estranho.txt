Nando Reis - Muito Estranho

Intro:   C  D

 G              G5(7M)                 C
Hum! Mas se um dia eu chegar muito estranho
 Am7                D                   G    C  D
Deixa essa água no corpo lembrar nosso banho
 G              G7                  C
Hum! Mas se um dia eu chegar muito louco
 Am7                D                  G    G5(7)
Deixa essa noite saber que um dia foi pouco

Refrão:
       C          G4/B   Am7
Cuida bem de mim
          D         A
Então misture tudo dentro de nós
 C              D                G
Porque ninguém vai dormir nosso sonho

 G          G5(7M)               C
Hum! Minha cara prá que tantos planos

     Am7                D                   G    C/G
Se quero te amar e te amar e te amar muitos anos
 G           G7               C
Hum! Tantas vezes eu quis ficar solto
 Am7               D                    G    G5(7)
Como se fosse uma lua a brincar no teu rosto

Refrão:
       C         G4/B   Am7
Cuida bem de mim
          D         A          C
Então misture tudo dentro de nós
            D                    G
Porque ninguém vai dormir nosso sonho
F7M(11+)      C#m7(5-) Cm(7M)      G     (2x)
         Não vai não          não vai não

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
C#m7(5-) = X 4 5 4 5 X
C/G = 3 3 2 X 1 X
Cm(7M) = X 3 5 4 4 3
D = X X 0 2 3 2
F7M(11+) = 1 3 3 2 0 0
G = 3 2 0 0 0 3
G4/B = X 2 0 0 1 X
G5(7) = 3 5 3 X X X
G5(7M) = 3 X 4 0 3 X
G7 = 3 5 3 4 3 3
