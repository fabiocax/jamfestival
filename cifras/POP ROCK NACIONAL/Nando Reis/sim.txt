Nando Reis - Sim

A                             E
Sim, desde que eu te vi eu te quis
           F#m
Eu quis te raptar
          D
Eu fiz um altar
        A
Pra te receber

         Em   F#m G
Como um anjo
                D
Que caiu lá do céu
           A       Bm            D
Não estava voando, andando distraiu-se

[Refrão]

 A
Sim, e agora
           C#m7
Eu quero voltar lá do céu

                   Bm
Eu quero estar de volta
                                     D
Eu quero ter você quando estiver de volta
                    A
Eu quero você para mim

A   Em F#m G
Não dou,
          D
Pra ficar só
A       Em   G
Sim, não dou, não

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
