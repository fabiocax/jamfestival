Nando Reis - De Mãos Dadas

(Nando Reis)

Intro : G

  D
Vamos passear de mãos dadas,
  G
andar é muito bom
  D
pra quem quer ficar junto.
  G
Eu atravessei na frente

e ela viu que eu levo um pente
                D
no bolso de trás.
  C
E se por acaso chover,
                 G
quero ver o seu cabelo molhado.
  C
Não há nada mais bonito


do que o seu sorriso
                        G
o frio que te faz me abraçar.

D   C                    G
Oh! de mãos dadas vamos passear
D   C                      G
Oh! vamos passear de mãos dadas

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
