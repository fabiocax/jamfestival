﻿Nando Reis - Ainda Gosto Dela

Capo Casa 4

Intro: B

 Bm
Hoje acordei sem lembrar
    B
Se vivi ou se sonhei
 Bm
Você aqui nesse lugar
       B
Que eu ainda não deixei

       A
Vou ficar?
  Em      (riff 1)
Quanto tempo
         A
Vou esperar
          Em        F#        F#7
E eu não sei o que vou fazer não

 Bm
Nem precisei revelar

     B
Sua foto não tirei
 Bm
Como tirei pra dançar
    B
Alguém que avistei

         A
Tempo atrás
Em       (riff 1)
Esse tempo está
     A
Lá trás
          Em            F#        F#7
E eu não tenho mais o que fazer, não

Bm                A
Eu ainda gosto dela
    Em
Mas ela já não gosta tanto assim
   Bm                   A
A porta ainda está aberta
 Em
Mas da janela já não entra luz

Bm                  A
E eu ainda penso nela
          Em
Mas ela já não pensa mais em mim
        A
Em mim não

Bm
Ainda vejo o luar
      B
Refletido na areia
Bm
Aqui na frente desse mar
     B
Sua boca eu beijei

        A
Quis ficar
 Em    (riff 1)
Só com ela eu
        A
Quis ficar
    Em       F#    F#7
E agora ela me deixou

Bm                A
Eu ainda gosto dela
    Em
Mas ela já não gosta tanto assim
   Bm                   A
A porta ainda está aberta
 Em
Mas da janela já não entra luz
Bm                A
Eu ainda gosto dela
    Em
Mas ela já não gosta tanto assim
   Bm                   A
A porta ainda está aberta
     Em                          A
Pra que ela entre e traga a sua luz

(volta ao 1º e 2º verso)
(refrão)

( Bm  A  Em  Em ) (5x)


(riff 1)
E|--------------------------------|
B|-5-3---7--5---------------------|
G|-----4--------------------------|
D|-2-0---4--2---------------------|
A|-----2--------------------------|
E|--------------------------------|

----------------- Acordes -----------------
Capotraste na 4ª casa
A*  = X 0 2 2 2 0 - (*C# na forma de A)
B*  = X 2 4 4 4 2 - (*D# na forma de B)
Bm*  = X 2 4 4 3 2 - (*D#m na forma de Bm)
Em*  = 0 2 2 0 0 0 - (*G#m na forma de Em)
F#*  = 2 4 4 3 2 2 - (*A# na forma de F#)
F#7*  = 2 4 2 3 2 2 - (*A#7 na forma de F#7)
