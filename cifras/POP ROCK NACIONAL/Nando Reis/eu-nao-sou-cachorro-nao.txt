Nando Reis - Eu Não Sou Cachorro Não

A
 Eu não sou cachorro não
                     E7
 Pra viver tão humilhado

 Eu não sou cachorro não
                     A
 Para ser tão desprezado

 Tu não sabes compreender
         A7             D
 Quem te ama, quem te adora
                    A
 Tu só sabes maltratar-me
       E7             A
 E por isso eu vou embora
                  E7
 A pior coisa do mundo
                   A
 É amar sendo enganado
                           E7
 Quem despreza um grande amor

                   D
 Não merece ser feliz
                     A
 Nem tão pouco ser amado
 Tu devias compreender
         A7           D
 Que por ti tenho paixão
              A
 Pelo nosso amor
               E7
 Pelo amor de Deus
 Eu não sou
           A  50 52 54 D
 Cachorro não
              A
 Pelo nosso amor
               E7
 Pelo amor de Deus
                      A
 Eu não sou cachorro não

  Solo: A A7 D A E7 A

 A                E7
 A pior coisa do mundo
                   A
 É amar sendo enganado
                           E7
 Quem despreza um grande amor
                   D
 Não merece ser feliz
                     A
 Nem tão pouco ser amado

 Tu devias compreender
         A7           D
 Que por ti tenho paixão
              A
 Pelo nosso amor
               E7
 Pelo amor de Deus

 Eu não sou
           A  50 52 54 D
 Cachorro não
              A
 Pelo nosso amor
               E7
 Pelo amor de Deus
                      A
 Eu não sou cachorro não.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
