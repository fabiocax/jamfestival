Nx Zero - Nessa Cidade

                                    Am
Quando é preciso a gente tem que fazer
             Bb                     F
Sem levar a sério e nem entrar em crise
 Dm                     Am
Como um bailarino que dança pra não morrer
 Bb                     C
Praticando a arte de saber viver
Dm                    Am
As coisas não tão diferentes
Bb                     F
As coisas mudam com a gente
Dm                     Am
A gente muda até encontrar a paz
            Bb                   Am
Pra não brigar com o mundo nunca mais

[Passagem]
E|-10-8---------------------|
B|------10--10-8-3----------|
G|--------------------------|
D|--------------------------|
A|--------------------------|
E|--------------------------|


(Vamo viver)
 Dm                        Am
Vamo, vamo, vamo quando a gente quer
         Bb                 F
A gente dá um jeitim de chegar no fim
           Dm                    Am
Se a liberdade estiver na nossa mente
              Bb                     F
Levo ela pro mundo e o mundo leva a gente
             Dm Am                           Bb
E a gente toca     com mais amor e menos estresse
          F                     Dm
Porque a vida não precisa ser ruim
Am                            Bb
  Nossos sonhos podem ser verdade
     Am                      Dm
Se abrirmos mão da nossa vaidade
Am                    Bb     C    Dm
  Pra não ser só mais um nessa cidade
Am              Bb    F           Dm
  Quando amanhecer a gente continua
    Am                            Bb
No céu azul que eu nem sei descrever
            F                           Dm
Tomando um vinho que eu nem sei pronunciar
   Am                                 Bb
Depois de passar a noite inteira com você
          F                Dm
Baby, o que mais posso querer?
Am              Bb F               Dm Am Bb F
  Quando amanhecer,  quando amanhecer

Dm                        Am
Vamo, vamo, vamo quando a gente quer
         Bb                 F
A gente dá um jeitim de chegar no fim
           Dm                    Am
Se a liberdade estiver na nossa mente
              Bb                     F
Levo ela pro mundo e o mundo leva a gente
             Dm Am                          Bb
E a gente vai     com mais amor e menos estresse
          F                     Dm
Porque a vida não precisa ser ruim
Am                            Bb
  Nossos sonhos podem ser verdade
      Am                      Dm
Se abrirmos mão da nossa vaidade
Am                    Bb     C    Dm
  Pra não ser só mais um nessa cidade
Am              Bb    F           Dm
  Quando amanhecer a gente continua
    Am                            Bb
No céu azul que eu nem sei descrever
            F                           Dm
Tomando um vinho que eu nem sei pronunciar
   Am                                 Bb
Depois de passar a noite inteira com você
          F                Dm
Baby, o que mais posso querer?
Am              Bb F               Dm Am
  Quando amanhecer,  quando amanhecer
              Bb                 F            Dm  Am
Quando amanhecer,  quando amanhecer (a gente vê)
              Bb  F
Quando amanhecer
  Dm                      Dm           Bb F
Quando é preciso a gente tem que fazer

( Dm  Am  Bb  F )

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
