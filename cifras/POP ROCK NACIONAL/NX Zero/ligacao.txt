Nx Zero - Ligação

[Intro]

E|-------------------------|
B|-------8--------8--------|
G|---9---9----9---9--------|
D|-7---7-7--7---7-7--------|
A|-------------------------|
E|-------------------------|

[Primeira Parte]

E                        B
  Por cada noite sem dormir
                 G#m
Cada dia que passou
Por cada vez
 F#                      E
Cada vez que me senti assim
 E                    B
Pela grana que eu gastei
                   G#m
Pelo tempo que perdi

Que foi em vão
  F#                         E
E eu fiquei sem ter pra onde ir

[Pré-Refrão]

                  B
Cada briga sem razão
                   G#m
Cada verso sem refrão
               F#
Você virou as costas
Pra quem te era bom
E                      B
  Mas não vou ficar aqui
                     G#m
Me lamentando com você
                 F#
Essa é a última vez
                   E
Que faço essa ligação
Escute bem porque

[Refrão]

G#m               E
  Quando eu desligar
                B                   F#
Você não vai saber mais nada sobre mim
             G#m
Chegamos ao fim
           E
O ultimo alô
                  B
É na verdade um adeus
                  F#
Esqueça aqueles planos
Eles não são mais seus

( G#m  B  G#m  B )

[Segunda Parte]

E                       B
  Por cada hora que passou
                      G#m
E as mentiras que contou
             F#                       E
Por alguém que talvez deixei de conhecer
                       B
Pelas cartas que escrevi
                   G#m
A tatuagem que eu fiz
        F#                        E
Pra marcar o que hoje quero esquecer

[Pré-Refrão]

                  B
Cada briga sem razão
                   G#m
Cada verso sem refrão
               F#
Você virou as costas
Pra quem te queria bem
E                      B
  Mas não vou ficar aqui
                     G#m
Me lamentando com você
                 F#
Essa é a última vez
                   E
Que faço essa ligação
Escute bem porque

[Refrão]

G#m               E
  Quando eu desligar
                B                   F#
Você não vai saber mais nada sobre mim
             G#m
Chegamos ao fim
           E
O ultimo alô
                  B
É na verdade um adeus
                  F#
Esqueça aqueles planos
Eles não são mais seus

[Ponte]

E                              C#m
  Pra você o amanhã nunca existiu
                               G#m
Esqueceu tudo o que vivemos ontem
            F#                   E
Jogou fora dias meses de lembranças
                          C#m
Nosso tempo você disperdiçou
Não há mais nada a fazer

[Refrão]

G#m               E
  Quando eu desligar
                B                   F#
Você não vai saber mais nada sobre mim
             G#m
Chegamos ao fim
           E
O ultimo alô
                  B
É na verdade um adeus
                  F#
Esqueça aqueles planos
                   G#m  E
Eles não são mais seus

                   B   F#
Eles não são mais seus
                   G#m  B
Eles não são mais seus
             G#m  F#
Não são mais
              G#m
Não são mais seus

----------------- Acordes -----------------
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
