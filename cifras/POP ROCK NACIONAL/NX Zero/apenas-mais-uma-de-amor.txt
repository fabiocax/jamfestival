Nx Zero - Apenas Mais Uma de Amor

[Intro]

E|------------------------------|
B|-8/10/13\10---10\6------------|
G|------------------------------|
D|------------------------------|
A|------------------------------|
E|------------------------------|

E|------------------------------|
B|-8/10/13\10-10/15-15-13-------|
G|------------------------------|
D|------------------------------|
A|------------------------------|
E|------------------------------|

[Primeira Parte]

    F
Eu gosto tanto de você
      Bb
Que até prefiro esconder

Dm                            C
  Deixo assim ficar sub-entendido
           Gm      Am  Bb    C    F
Como uma ideia que e--xiste na cabeça
   C           Gm     Am  Bb      C    F  F4  F
E não tem a menor preten-são de acontecer

   F                   Bb
Eu acho isso tão bonito de ser abstrato, baby
Dm                        C
  A beleza é mesmo tão fugaz
        Gm      Am  Bb    C    F
É uma ideia que e--xiste na cabeça
   C           Gm     Am  Bb      C    F  F4  F
E não tem a menor preten-são de acontecer

[Refrão]

Bb              C/E
  Pode até parecer fraqueza
          F        F/E    Bb
Pois que seja fraqueza então
Gm            Am    Bb
  A alegria que me dá
      C            F  F4  F
Isso vai sem eu dizer
Bb                     C/E
  E se amanhã não for nada disso
     F       F/E      Bb                   Gm
Caberá só a mim esquecer (e eu vou sobreviver)
                 Am        Bb
O que eu ganho e o que eu perco
     C  F              F4  F
Ninguém  precisa saber

[Primeira Parte]

    F
Eu gosto tanto de você
      Bb
Que até prefiro esconder
Dm                            C
  Deixo assim ficar sub-entendido
           Gm      Am  Bb    C    F
Como uma ideia que e--xiste na cabeça
   C           Gm     Am  Bb      C    F  F4  F
E não tem a menor preten-são de acontecer

[Refrão]

Bb              C/E
  Pode até parecer fraqueza
          F        F/E    Bb
Pois que seja fraqueza então
Gm            Am    Bb
  A alegria que me dá
      C            F  F4  F
Isso vai sem eu dizer
Bb                     C/E
  E se amanhã não for nada disso
     F       F/E      Bb                   Gm
Caberá só a mim esquecer (e eu vou sobreviver)
                 Am        Bb
O que eu ganho e o que eu perco
     C  Dm          Bb
Ninguém   precisa saber

( F  C )

Gm                 Am        Bb
  O que eu ganho e o que eu perco
     C             F
Ninguém precisa saber

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
F/E = 0 X X 2 1 1
F4 = 1 3 3 3 1 1
Gm = 3 5 5 3 3 3
