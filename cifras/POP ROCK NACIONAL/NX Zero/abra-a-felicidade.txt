Nx Zero - Abra a Felicidade

Intro: Bb9  C9  Eb  Bb9
E|----13--16--10--8--10---------10----------|
B|-13------------------11--13------11------|  (2x)
G|-----------------------------------------|
D|-----------------------------------------|
A|-----------------------------------------|
E|-----------------------------------------|

Bb5              C5
Alô, aumenta o som!
                         Eb
Tem alguém aí me ouvindo?
                Bb5
Um novo dia chegou
Bb5
E eu quero, pra começar
C5                            Eb5
Dizer que a onda é de quem chegar
                       Bb5
Então não to sozinho nessa


Bb5
Vem curtir comigo o dia já vem
C5
Abra a felicidade você também
Eb5                Bb5
Vamos sentir algo novo
Bb5
Vem curtir comigo, isso faz tão bem
           C5                    Eb5
Abra um sorriso no rosto de alguém
                      Bb5
Pra eu sentir como é bom

Refrão:
Bb5                       C5
Abra a felicidade que tem aí
Abra a felicidade que tem aí
Eb5                       Bb5
Abra a felicidade que tem aí
Abra a felicidade que tem aí

( Bb5  C5  Eb5  Bb5 ) (2x)

(MV Bill)
Bb5
Dá licença
Han Han Chega aí
C5
Que eu tenho uma parada pra você ouvir
Eb5
Hoje eu acordei num clima bacana
Bb5
Alegria transbordou e me lembrou que alguém me ama
Bb5
Veja só: ontem eu tava perdido, é...
C5
Hoje eu tô sem problema e muito leve
Eb5
Até pode acontecer o mesmo com você
Bb5
Eu tô rindo à toa, quem mais vai querer?

Bb5                    C5
Oh Quero ver o sol brilhar
      Eb5
Sem parar
                   Bb5
Será que é pedir demais?
Bb5              C5
Hoje eu quero sair
                      Eb5
Com meus amigos por aí
                  Bb5
A vida é muito curta

Pra que desperdiçar?

(Pitty)
Bb5                        C5
Se você ouve o seu coração
                    Eb5
O mundo cabe na sua mão
Quem planta amores pelo caminho
Bb5
Não encontra nenhum espinho
Bb5                          C5
E quem sabe eu te encontro lá
                         Eb5
Existe tanto a compartilhar
Eu já ergui a ponte
  Bb5
É só atravessar

----------------- Acordes -----------------
Bb5 = X 1 3 3 X X
Bb9 = X 1 3 3 1 1
C5 = X 3 5 5 X X
C9 = X 3 5 5 3 3
Eb = X 6 5 3 4 3
Eb5 = X 6 8 8 X X
