Ultraje a Rigor - O Fusquinha Do Itamar

(intro)

A  G  A  G

Essa é uma das muitas histórias que acontecem comigo
primeiro, foi o fusca do Juscelino
depois veio o DKW, o Simca Chambord,
tudo isso sem falar no tremendo tapa que eu levei
com a história da carroça do Collor
e agora,
isso!

                       A
Não vai dar prá compensar
                      F#
com o fusquinha do Itamar
                 G
o carrão último tipo
                         E7
que era o que eu ia comprar



                         A
eu pensei que dando um trato
                      F#
pondo nele uns vidro-bolha
                  F#
uns pneu de tala larga
                  E7
ia ser uma boa escolha

                     A
mas o fusca se eu equipo
                          F#
ainda assim não chega aos pés
                  G   E7
do modêlo último tipo
                    A
que no fusca dá de dez

D       Db C  B  Bb A
vrruum, um um um um
D       Db C  B  Bb A
vrruum, um um um um

                   A
e agora, o que fazer? (e agora o que fazer?)
                   F#
foi uma escolha suicida (foi sim, foi sim)
                      G
não vou conseguir vender (bem feito, bem feito)
                     E7
é um carro prá toda vida (se fudeu, se fudeu)

                 A
é seguro, mas é feio
                      F#
é mais simples mas é lento
                 G
não é nem bela viola
                 E7
ainda é pão bolorento


                     A
e o carro que eu queria
                   F#
ainda é o tôpo da linha
                 G   E7
dá vontade de matar
                         A
só de olhar pro meu fusquinha

A
Bye bye fusca
Bye bye fusca

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D = X X 0 2 3 2
Db = X 4 6 6 6 4
E7 = 0 2 2 1 3 0
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
