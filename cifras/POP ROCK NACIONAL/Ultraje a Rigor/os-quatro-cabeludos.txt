Ultraje a Rigor - Os Quatro Cabeludos

Intro: E

Tudo começou quando Lili foi a esquina
E a turma de outra rua
Se empolgou com a menina
  A
Lili, meio sem jeito
Sorriu alegremente
    E
Mas viu que os olhares
Eram bem diferentes
   F#        B        G#        C#
Um cara esquisito seu braço segurou
     F#         B        E       B
E um beijo da Lili o atrevido roubou
   E
Eu vinha no meu carro em doida disparada
Com quatro cabeludos pra topar qualquer parada
     A
Foi quando, de repente a cena eu avistei
       E
E o freio do carango bruscamente eu pisei

    F#             B        G#           C#
Sem mesmo abrir as portas e sem botar as mãos
  F#          B          E           E7
Pulamos todos quatro para entrar em ação
   A
Brigamos muito tempo
Rasgamos nossas roupas
  E
Fugimos da polícia que já vinha feito louca
  A
Porém maldita hora que eu fui olhar pra trás
  F#                     B
A cena que eu vi não esqueço nunca mais
  E
Lili toda contente na esquina conversava
Com o cara esquisito que a pouco lhe beijava
   A
Estava indiferente à aquela confusão
   E
Lili era bonita mas não tinha coração
        F#          B
Então juramos todos quatro
   G#        C#      F#           B
Palavra de rapaz que por garota alguma
     E              C#
Não brigamos nunca mais
    F#           B
Que por garota alguma
     E             C#m
Não brigamos nunca mais
    F#           B
Que por garota alguma...

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C# = X 4 6 6 6 4
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F# = 2 4 4 3 2 2
G# = 4 3 1 1 1 4
