Ultraje a Rigor - Nada a Declarar

        A                           E
Eu tô sentindo que a galera anda entendiada
 E                             A
Não tô ouvindo nada, não tô dando risada
    A               E
E aê, qual é? Vamô lá, moçada!
   E                     A
Vamô mexe agitar, vamô dá uma detonada!

A                    E
Esse nosso papo anda tão furado
                                   A
É baixaria, dor de corno e bunda pra todo lado

     D/F#                      A             E
Eu quero me esbaldar, quero lavar a alma
                           A
Quem sabe, sabe; quem não sabe bate palma

D/F#                       A
E pra celebrar a nossa falta de assunto

E                     A
Vamô todo mundo cantá junto (Ueba!)

A                      E
Eu não tenho nada pra dizer
                             A
Também não tenho mais o que fazer
                      D/F#
Só pra garantir esse refrão
E                 A
Eu vou enfiar um palavrão (Ah..Cu)

De novo... Cu

SOLO: A

A                                   E
Mas eu tô vendo que a galera anda entendiada
                                 A
Não tá fazendo nada, e eu não tô dando risada
    A             E
E aí, qual é? Vamô lá, moçada!
                  A
Vamô agitá, vamô dá uma detonada!

A                       E
Esse nosso povo anda tão chutado
                                         A
Quando não é um vereador roubando, é um deputado

    D/F#                        A           E
Eu quero me esbaldar, quero lavar a alma
                           A
Quem sabe, sabe; quem não sabe bate palma

D/F#                       A
E pra celebrar a nossa falta de assunto
E                      A
Vamô todo mundo cantá junto

Refrão?
A                      E
Eu não tenho nada pra dizer
                             A
Também não tenho mais o que fazer
                      D/F#
Só pra garantir esse refrão
E                 A
Eu vou enfiar um palavrão (Ah..Cu)

De novo... Cu

Solo: A E  (3x)

----Eu quero me esbaldar, quero lavar a alma----

Refrão:
A                      E
Eu não tenho nada pra dizer
                             A
Também não tenho mais o que fazer
                      D/F#
Só pra garantir esse refrão
E                 A
Eu vou enfiar um palavrão (Ah..Cu)

De novo... Cu

    D/F#                        A           E
Eu quero me esbaldar, quero lavar a alma
                           A
Quem sabe, sabe; quem não sabe bate palma

D/F#                       A
E pra celebrar a nossa falta de assunto
E                      A
Vamô todo mundo cantá junto

----------------- Acordes -----------------
A = X 0 2 2 2 0
D/F# = 2 X 0 2 3 2
E = 0 2 2 1 0 0
