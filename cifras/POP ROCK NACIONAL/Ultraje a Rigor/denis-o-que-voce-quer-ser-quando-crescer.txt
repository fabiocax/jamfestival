Ultraje a Rigor - Dênis, o Que Você Quer Ser Quando Crescer?

D            E                  :
O quê você quer ser?                :
    D                E              : Refrão
Que é que você quer ser?            :
    D            E              B   :
O quê você quer ser quando crescer? :


  E                         F#
'Cê não responde quando eu quero saber
B
Fica me olhando com essa cara de pau
E                    F#
Mas não parece preocupar você
B
'Cê dá a impressão que nunca vai se dar mal
E                    F#
Eu me interesso pelo seu futuro
B
'Cê não parece nem ligar pra dinheiro
E                       F#
  Será que sempre você vai ficar duro

   A
Será que um dia você vai se casar
     Ab
ou será que vai ficar solteiro?

Refrão

E                       F#
Só quer saber de farra todos os dias
B
É até bom 'cê ser assim sociável
E                       F#
Mas fique atento com as más companhias
B
Você precisa ser mais responsável
E                      F#
Você não pode ser tão relaxado
B
Que a vida às vezes é uma competição
E                        F#
Eu sempre te tratei com todo cuidado
A                      Ab
Eu confio em você, não vá me deixar na mão

Refrão

E7                      D7
Eu nunca soube o que você queria ser
E7                    D7
Mas eu sabia que não tinha o que temer
F                     B
Você me dá tanta alegria, eu tenho muito orgulho de você

Refrão

B7
Dênis...

----------------- Acordes -----------------
A = X 0 2 2 2 0
Ab = 4 3 1 1 1 4
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
