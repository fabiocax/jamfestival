Ultraje a Rigor - O Monstro de Duas Cabeças

Intro: G# A     G# A

G# A
         Tem uma que é tensa
G# A
         tem uma que pensa
G# A
         tem uma que é louca
G# A
         outra é careta

G# A
         Tem uma que é chata
G# A
         tem uma pontuda
G# A
         uma é careca
G# A
         outra é cabeluda

G# A
         Tem uma que puxa

G# A
         tem uma que empurra
G# A
         tem uma que é grande
G# A
         tem uma que é burra

D              A          E                 A
Eu sou um monstro   de duas cabeças
D                     A       E                         A
uma cabeça de bagre,    uma cabeça de teta
D                A        E                  A
uma é só trabalho, outra é só alegria
D                       A           E     E7        A    (G# A)
uma me enche o saco e a outra...  esvazia

G# A
         Tem uma que manda
G# A
         outra obedece
G# A
         tem uma que sobe
G# A
         tem uma que cresce

G# A
         Uma cabeça falha
G# A
         a outra também falha
G# A
         uma cabeça ajuda
G# A
         a outra atrapalha

G# A
         Uma tá cansada
G# A
         outra tá no pique
G# A
         uma quer que eu vá
G# A
         outra quer que eu fique

D              A          E                 A
Eu sou um monstro   de duas cabeças
D                     A       E                         A
uma cabeça de bagre,    uma cabeça de teta
D                A        E                  A
uma é só trabalho, outra é só alegria
D                       A           E     E7        A    (G# A)
uma me enche o saco e a outra...  esvazia

D                                  A
Num dia eu tô pra cima noutro eu tô pra baixo
D                                    A
mas eu não posso cortar nenhuma cabeça
D                            A
Se pelo menos elas duas concordassem
D                                          A
Mas uma é séria e a outra só pensa em sacanagem

D              A          E                 A
Eu sou um monstro   de duas cabeças
D                     A       E                         A
uma cabeça de bagre,    uma cabeça de teta
D                A        E                  A
uma é só trabalho, outra é só alegria
D                       A           E     E7        A    (G# A)
uma me enche o saco e a outra...  esvazia

G# A
         Tem uma que manda
G# A
         tem uma que cresce
G# A
         tem uma que sobe
G# A
         tem uma que desce

G# A
         Tem uma que puxa
G# A
         tem uma que empurra
G# A
         tem uma que é grande
G# A
         tem uma que é burra


D              A          E                 A
Eu sou um monstro   de duas cabeças
D                     A       E                         A
uma cabeça de bagre,    uma cabeça de teta
D                A        E                  A
uma é só trabalho, outra é só alegria
D                       A           E     E7        A    (G# A)
uma me enche o saco e a outra...  esvazia

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
G# = 4 3 1 1 1 4
