Ultraje a Rigor - Rebelde sem causa

(intro) A5 F5 C5 E5

(continua base violão 1)

(solo - violão 2 - 2x)

   Am                 F                 C                E
E|----------5-5-0-0-|-1-1-----------1-|----3-3-----3-3-|----------|
B|------------------|-----1-1-----1---|----------------|-0000000--|
G|-2-2--5-5---------|-----------------|-0-0-----0-0----|----------|
D|------------------|---------3-3-----|----------------|----------|
A|------------------|-----------------|----------------|----------|
E|------------------|-----------------|----------------|----------|

Am               F
Meus dois pais me tratam muito bem
C                     E
(O que é que você tem que não fala com ninguém?)
Am                  F
Meus dois pais me dão muito carinho
C                   E
(Então porque você se sente sempre tão sozinho?)

Am                      F
Meus dois pais me compreendem totalmente
C                        E
(Como é que cê se sente , desabafa aqui com a gente!)
Am                       F
Meus dois pais me dão apoio moral
C                      E
(Não dá pra ser legal , só pode ficar mal !)

Am F
MAMA MAMA MAMA MAMA
C E
(PAPA PAPA PAPA PAPA)

D                F
Minha mãe até me deu essa guitarra
C                        G
Ela acha bom que o filho caia na farra
D                     F
E o meu carro foi meu pai que me deu
C                   G
Filho homem tem que ter um carro seu
D                       F
Fazem questão que eu só ande produzido
C                      G
Se orgulham de ver o filhinho tão bonito
D                         F
Me dão dinheiro prá eu gastar com a mulherada
C                   E
Eu realmente não preciso mais de nada

(solo base)
   A5     F5     C5      E5     A5      F5      C5      E5
E|----------------------------|-----------------------------|
B|----------------------------|-----------------------------|
G|----9--12-------------------|----9--12--------------------|
D|-7--------10---9-9-9-10-12--|-7--------10---9-9-9-10-12---|
A|----------------------------|-----------------------------|
E|----------------------------|-----------------------------|

   A5     F5     C5      E5     A5      F5      C5      E5
E|----------------------------|-----------------------------|
B|----------10-8--------------|----------10-12--12-10-------|
G|----9--12-------7-7-7-------|----9--12--------------12-10-|
D|-7--------------------10-9--|-7---------------------------|
A|----------------------------|-----------------------------|
E|----------------------------|-----------------------------|

Dm Am
Meus pais não querem
C G
Que eu fique legal
Dm Am
Meus pais não querem
C E
Que eu seja um cara normal

Am F
Não vai dar , assim não vai dar
C E
Como é que eu vou crescer sem ter com quem me revoltar
Am F
Não vai dar , assim não vai dar
C E
Pra eu amadurecer sem ter com quem me rebelar.

----------------- Acordes -----------------
A5 = X 0 2 2 X X
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C5 = X 3 5 5 X X
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
E5 = 0 2 2 X X X
F = 1 3 3 2 1 1
F5 = 1 3 3 X X X
G = 3 2 0 0 0 3
