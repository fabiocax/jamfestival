Legião Urbana - Marcianos Invadem a Terra

Intro: G  D/F#  Em  C  Am  Bm  Em  C  Am  Bm  Em

G      D/F#        Em     C
 Diga adeus e atravesse a rua
        Am      Bm      Em
 Voamos alto depois das duas
            C        Am           Bm        Em
 Mas as cervejas acabaram e os cigarros também

    D           C        G         D
 Cuidado com a coisa coisando por aí
                C              G           D
 A coisa coisa sempre e também coisa por aqui
                    C            G            D
 Seqüestra o seu resgate, envenena a sua atenção
                   C        G           D
 É verbo e substantivo/adjetivo e palavrão

     G4         G
 E o carinha do rádio não quer calar a boca
    G4            G
 E quer o meu dinheiro e as minhas opiniões

 G4
 Ora, se você quiser se divertir
   G4          G
 Invente suas próprias canções

G         D/F#   Em      C
 Será que existe vida em Marte?
              Am
 Janelas de hotéis
    Bm      Em
 Garagens vazias
     C
 Fronteiras
    Am
 Granadas
    Bm    Em
 Lençóis

D          C         G     D
 E existem muitos formatos
                C            G     D
 Que só têm verniz e não tem invenção
               C            G       D
 E tudo aquilo contra o que sempre lutam
              C             G         D
 É exatamente tudo aquilo o que eles são

     G4      G
 Marcianos invadem a Terra
         G4                     G
 Estão inflando o meu ego com ar
    G4                G
 E quando acho que estou quase chegando
        G4             G
 Tenho que dobrar mais uma esquina
    C                  G
 E mesmo se eu tiver a minha liberdade
     C                    G
 Não tenho tanto tempo assim
    C                  G
 E mesmo se eu tiver a minha liberdade:
     C            D       G
 "Será que existe vida em Marte?" Yeah!

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G4 = 3 5 5 5 3 3
