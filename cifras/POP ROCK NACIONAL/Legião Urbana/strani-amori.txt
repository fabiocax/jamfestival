Legião Urbana - Strani Amori

Intro: C F G C Am F G

       C
 Mi dispiace devo andare via
      F7+
 Ma sapevo che era una bugia
        G
 Quanto tempo perso dietro a lui
        C
 Che promette poi non cambia mai
         F7+       G7/4      Am
 Strani amori mettono   nei guai
         Dm7         G7/4  G7
 Ma in realtà siamo noi
      C
 E lo aspetti ad un telefono
     F7+
 Litigando che sia libero
        G
 Con il cuore nello stomaco
       C
 Un gomitolo nell'angolo

       F7+                G7/4  Am
 Li da solo, dentro un brivi....do
        Dm7        G7/4
 Ma perché lui non c'è, e sono
  C              Dm7
 Strani amori che    fanno crescere
 G/B        C             F
 E sorridere   tra le lacrime
             G7/4          Am
 Quante pagine     li da scrivere
 Dm7            G/B
 Sogni e lividi da dividere
 C             G/B
 Sono amori che    spesso a quest'età
 E             Am           Am/G
 Si confondono dentro quest'anima
 F         G/B   E       Am
 Che s'interroga senza decidere
 Dm7              G/B
 Se è un amore che    fa per noi
 D
 E quante notti perse a piangere
      G7+
 Rileggendo quelle lettere
         A
 Che non riesci più a buttare via
         D
 Dal labirinto della nostalgia
            G      A4/7      Bm
 Grandi amori che finis....cono
       Em                A4/7  A7
 Ma perché restano, nel cuore
 D          Em
 Strani amori   che vanno e vengono
 A/C#       D
 Nei pensieri   che li nascondono
 G7+           A4/7         Bm
 Storie vere che    ci appartengono
 Em            A/C#
 Ma si lasciano     come noi
         E       A/B
 Strani amori fragili,
       B7        E
 Prigioneri liberi
         A         B4/7      A#m7/5-
 Strani amori mettono    nei guai
         D#m7/5-       B
 Ma in realtà  siamo noi
   E             B/D#
 Strani amori fragili
      Fm7     A#m7/5-
 Prigioneri liberi
 A      B     G#m           A#m7/5-
 Strani amori che non sanno vivere
 D#m7/5-      B
 E si perdono dentro noi
       E
 Mi dispiace devo andare via
        A
 Questa volta l'ho promesso a me
           B
 Perché ho voglia di un amore vero
       A#m7/5-  A  B  E
 Senza te

----------------- Acordes -----------------
A = X 0 2 2 2 0
A#m7/5- = X 1 2 1 2 X
A/B = X 2 2 2 2 X
A/C# = X 4 X 2 5 5
A4/7 = X 0 2 0 3 0
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Am/G = 3 X 2 2 1 X
B = X 2 4 4 4 2
B/D# = X 6 X 4 7 7
B4/7 = X 2 4 2 5 2
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D#m7/5- = X X 1 2 2 2
Dm7 = X 5 7 5 6 5
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F7+ = 1 X 2 2 1 X
Fm7 = 1 X 1 1 1 X
G = 3 2 0 0 0 3
G#m = 4 6 6 4 4 4
G/B = X 2 0 0 3 3
G7 = 3 5 3 4 3 3
G7+ = 3 X 4 4 3 X
G7/4 = 3 5 3 5 3 X
