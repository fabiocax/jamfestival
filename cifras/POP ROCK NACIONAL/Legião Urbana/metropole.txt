Legião Urbana - Metrópole

Intro:  E5 F5 G5 E5 F5 C5 B5

 A5         D5          G5      C5
"É sangue mesmo, não é mertiolate"
   A5           D5
E todos querem ver
         G5        C5
E comentar a novidade.
    A5           D5       G5             C5
"É tão emocionante um acidente de verdade"
       A5           D5
Estão todos satisfeitos
            G5        C5
Com o sucesso do desastre:
  E5 F5 G5 E5 F5 C5 B5
Vai passar na televisão
 A5         D5        G5         C5
"Por gentileza, aguarde um momento.
 A5         D5          G5        C5
Sem carteirinha não tem atendimento -
    A5           D5         G5           C5
Carteira de trabalho assinada, sim senhor.

 A5        D5              G5         C5
Olha o tumulto: façam fila por favor.
  E5 F5 G5 E5 F5 C5 B5
Todos com a documentação.
 A5            D5          G5            C5
Quem não tem senha não tem lugar marcado.
 A5        D5              G5         C5
Eu sinto muito mas já passa do horário.
   A5              D5             G5       C5
Entendo  seu problema mas não posso resolver:
          A5      D5             G5        C5
É contra o regulamento, está bem aqui, pode ver.
E5
Ordens são ordens.
 A5       D5        G5        C5
Em todo caso já temos sua ficha.
    A5          D5         G5         C5
Só falta o recibo comprovando residência.
        A5             D5          G5          C5
Pra limpar todo esse sangue, chamei a faxineira -
    A5            D5         G5         C5
E agora eu vou indo senão perco a novela
  E5 F5 G5 E5 F5 C5 B5
Eu não quero ficar na mão"

----------------- Acordes -----------------
A5 = X 0 2 2 X X
B5 = X 2 4 4 X X
C5 = X 3 5 5 X X
D5 = X 5 7 7 X X
E5 = 0 2 2 X X X
F5 = 1 3 3 X X X
G5 = 3 5 5 X X X
