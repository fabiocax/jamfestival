Legião Urbana - Teorema

[Intro] A  A9  A
        A  A9  A

 A
Não vá embora
 D
Fique um pouco mais
     G
Ninguém sabe fazer
D                 A
O que você me faz

É exagero
   D
E pode até não ser
G
O que você consegue
     D
Ninguém sabe fazer

   Bm              E
Parece energia mas é só distorção

   F#m                       D    E  Bm  E  F#m
E não sabemos se isso é problema
      D    E   A
Ou se é a solução

( A  A9  A )
( A  A9  A )

 A
Não tenha medo
      D            G
Não preste atenção

Não dê conselhos
     D             A
Não peça permissão
                D           G
É só você quem deve decidir

O que fazer
     D
Pra tentar ser feliz

   Bm              E
Parece energia mas é só distorção
     F#m                D   E  Bm  E  F#m
E parece que sempre termina
    D    E   A
Mas não tem fim

 A
Não vá embora
 D
Fique um pouco mais
     G
Ninguém sabe fazer
D                 A
O que você me faz

É exagero
   D
E pode até não ser
G
O que você consegue
     D
Ninguém sabe fazer

   Bm                  E
Parece um teorema sem ter demonstração
     F#m                D    E  Bm  E  F#m
E parece que sempre termina
     D   E   A
Mas não tem fim

[Final] A9  A
        A9  A

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
