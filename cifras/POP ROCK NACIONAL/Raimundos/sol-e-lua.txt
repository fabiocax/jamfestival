Raimundos - Sol e Lua

( Digão / Denis Porto )

F#m               E        F#m
Um grude um piche na minha mente
F#m           E          F#m
Sou pescador de peixe na rede
F#m             E     F#m
Será que amanheceu o dia
F#m            E     F#m
Arrepia sem nenhum esforço

A                      D
Uma o sol e a outra a lua
Meu dia inteiro
A                       D
Uma de roupa e a outra nua
Ah, meu Deus!
                  E
Clareia a luz do dia

F#m            E      F#m
Nascida mesmo sem semente

F#m          E     F#m
Subindo de ré na parede
F#m           E     F#m
Primeira uma não queria
F#m             E        F#m
E a segunda me lasca o pescoço

A                      D
Uma o sol e a outra a lua
Meu dia inteiro
A                       D
Uma de roupa e a outra nua
Ah, meu Deus!
                  B              E
Clareia a luz do dia na praia vazia

B                                   E
Como se fosse possível juntar o nascer e pôr do sol
B                                   E
Come se fosse possível isso tudo ficar melhor
E eu...

A                      D
Uma o sol e a outra a lua
Meu dia inteiro
A                       D
Uma de roupa e a outra nua
Meu dia inteiro
A                      D
Uma o sol e a outra a lua
Meu dia inteiro
A                       D
Uma de roupa e a outra nua
Ah, meu Deus!
                  E            A
Clareia a luz do dia mais uma vez

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
