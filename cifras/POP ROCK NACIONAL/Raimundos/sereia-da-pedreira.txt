Raimundos - Sereia da Pedreira

[Intro]

E|---------------------------------|  
B|---------------------------------|  
G|-7--7-7-7-7-7-7-7--2-5-2-5-2-5-2-|
D|---------------------------------|  
A|-5--5-5-5-5-5-5-5----------------|  
E|---------------------------------|  

E|---------------------------------|
B|---------------------------------|
G|-7--7-7-7-7-7-7-7----------------| 
D|---------------------------------|
A|-5--5-5-5-5-5-5-5----------------|
E|------------------5-8-5-8-5-8-5--|

   G5           D#5                    G5
É dose vê-la pelada e não fazê nada é dose
        D#5                        C5
Menina pare com isso antes que eu goze
           F5                       G5
Porra que diabo que eu não faço por um irmão

 F#5  F5                                A#5
Tentação é vê ela molhadinha na minha frente
                           F5
Fico imaginando seu rabo quente
Me dizendo: Sim! Vem que hoje eu sou só pra você
    G5          F5                          G5
Ilusão é nesse mundo querer ser dono de alguém
         D#5                       A#5
Minha sereia linda eu só te quero bem
                             F5
E se você me quiser vem que tem
                                    G5
Por que eu rezo pra você a noite inteira
       F5                        G5
Pra repetir aquela manhã de quinta-feira
           F5
Quando eu sinto o seu cheiro
       A#5                     D#5                  A#5
Minha sereia da pedreira eu fico com tanta saudade de você
       G5             D#5                     G5
Se eu fosse um cara amargo arrombava esse cu doce
            D#5                      C5
Mil sonhos na bagagem são tudo que trouxe
           F5                           G5
Pra alimentar o que nesse mundo existe de maior
     F#5 F5                              A#5
Me alucina o jeito que me olha aquela menina
                                     F5
Para fingir de lago eu tenho uma piscina
                             G5   F5
Tenho maconha pra decorar o lugar
     G5                       F5
Vou pedir a Deus pra que pra sempre
                    G5
Tome conta de três filhos meus
         F5                       A#5  D#5
Zezé Di Camargo, Chitãozinho e Xororó
                  A#5
Ontem sonhei com você
             F5                          G5
Por isso só penso em te comer a noite inteira
       F5                        G5
Pra repetir aquela manhã de quinta-feira
           F5
Quando eu sinto o seu cheiro
       A#5                     D#5                     A#5
Minha sereia da pedreira eu fico com tanta saudade de você
             F5                          G5
Por isso só penso em te fuder a noite inteira
     F5                        G5
E repetir aquela manhã de quinta-feira
           F5
Quando eu sinto o seu cheiro
       A#5                   D#5                    A#5  F5  A#5
Minha sereia da pedreira eu fico com tanta saudade de você

----------------- Acordes -----------------
A#5 = X 1 3 3 X X
C5 = X 3 5 5 X X
D#5 = X 6 8 8 X X
F#5 = 2 4 4 X X X
F5 = 1 3 3 X X X
G5 = 3 5 5 X X X
