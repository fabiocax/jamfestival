Raimundos - Reggae do Manero

[Solo]  Bm  Em

E|------------------------------|------------------------------------|
B|-8--8--8--8--8--8-10-8 10-7---|--8--8--8--8--8--8-10-8-10-7--------|
G|------------------------------|-------------------------------9-7--|
D|------------------------------|------------------------------------|
A|------------------------------|------------------------------------|
E|------------------------------|------------------------------------|

E|------------------------------------------------------12----|
B|-8--8--8--8--8-8-10-8-10-7--------------------------------12|
G|-----------------------------9--7------9-7---9-7------------|
D|-----------------------------------9-9-----9-----9-9--------|
A|------------------------------------------------------------|
E|------------------------------------------------------------|


E|------------------------------------------------|
B|-8--8--8--8--8--8-10-8-10-7---------------------|
G|-----------------------------9-7----------------|
D|---------------------------------9--------------|
A|------------------------------------------------|
E|------------------------------------------------|


E|------------------------------------------------------------|
B|-8--8--8--8--8--8-10-8-10-7---------------------------------|
G|------------------------------9--7-------9--7---9-7--7------|
D|------------------------------------9-9-------9-----9-9-----|
A|------------------------------------------------------------|
E|------------------------------------------------------------|

             D
Se eu uso a manga da camisa que é dobrada
                 F#m                     Bm
A calça bag bem rasgada é porque eu sou fulêro
                D
Seu eu vou pro centro no domingo do perfume
                     F#m                      Bm
Eu uso um pingo que deixa fedendo o prédio inteiro
         D
Pente redondo tem cê me pergunta eu lhe respondo
         G
Eu tomo pinga com a Dominga dançando curtindo o Wando
           D
Eu não consigo nem levanta pra mudar o disco
          G
Um bicho velho cheio de risco mal serve pra abanar
D
Eu tô comendo bem no restaurante morte lenta
        G
A cozinheira é uma nojenta que vive limpando a venta
        Bm                                    A
No avental eu tô passando mal tô com saudade mainha

Bm      Em                Bm
Ô mãe! vê se manda um dinheiro
                 D
Que eu tô no banheiro
                           Em
E não tem nem papel pra cagar  (não tem, não tem)

Bm     Em                  Bm
Ô mãe! esse seu filho é maneiro
               D
Aqui no estrangeiro nenhuma mulher
              Em
Que me dá   (ninguém, ninguém)

 F#m               Bm
Meu cabelo eu não sei quem rapô
F#m                    Bm
Entupiu a privada entupiu ai meu Deus
G   D  Em
 Oh oh     ah ah...uh

 D
Cê é bonito

Cê é bonito
      F#m
Cê é bonito demais
                 D
Ocê é um cara manêro

Cê é bonito

Cê é bonito
               F#m
Cê é bonito demais
                           D
Bonito mais que o mundo inteiro

[Solo]  Bm  Em  D  Em

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
