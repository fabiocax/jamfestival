Raimundos - I Don't Care

[ AAGA GAGF G  (2x) ]

A             F        G
I don't care (he don't care)
A             F        G
I don't care (he don't care)
A             F         G
I don't care about this world
A             F         G
I don't care about that girl
A             F        G
I don't care (he don't care)

[ AAGA GAGF G  (2x) ]

I don't care...

[ AAGA GAGF G  (2x) ]

A
I don't care I don't care

G
I don't care I don't care
F
I don't care I don't care

[ AAGA GAGF G  (2x) ]

I don't care (he don't care)
I don't care (he don't care)
I don't care about these words
I don't care about that girl
I don't care

[ AAGA GAGF G  (1x) AAGA ]

----------------- Acordes -----------------
A = X 0 2 2 2 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
