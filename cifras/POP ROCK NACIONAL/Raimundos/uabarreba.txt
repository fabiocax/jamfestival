Raimundos - Uabarrêba

         C5
Não ha barreira,
    D5                   F5                   D5
que possa segurar a pororoca que vem la do mar
         C5
dessa maneira
             D5                       F5                   D5
voce pode rir, eu quero ver o cabra macho que não vai chorar
         C5
Nem barulheira,
       D5                F5                   D5
que possa impedir se bate sono eu quero dormir
         C5
E nem porteira,
           D5                   E5                     D5
forte de porte que suporte, essa boiada ainda vai fugir
F5
tudo vai ruir,
       C5
a labareda,
D5             F5                        D5
sobe o vento morde fura o céu aquecendo o ar

          C5         D5                  e5                    D5
Sonhei contigo 24 no bicho com esse olho doido como quer enxergar,
F5                    D5       C5     A5
que ce faz aqui? se adianta
refrao:
D5                    G5      C5
Todas empinadas estao lindas demais,
D5                             C5             F5
cantam em verso e trovas as ações que atraem a furia do planeta
D5                           G5        C5
tao plantando o odio, querem colher a paz,
 D5                          C5  Bb5        G5
safo pelo gongo antes que o fim passe por aqui

C5   D5   F5  D5
             C5
Ve se num arreia,
              D5               F5                 D5
sacode essa poeira que é besteira num querer mudar
            C5
sol com peneira,
             D5                F5                  D5
voce sabe disso, é pior que maçarico pra te queimar
           C5
ô sua parteira,
             D5       F5                  D5
faça a sua parte e raparte antes de partir
           C5
nossa chuteira
                 D5         E5                        D5
agora é passe livre bola na rede é o que me faz sorrir
F5                            D5 C5 A5...
que ce faz por mim? se levanta

 refrao
G5          F5                          A5
Nao adianta, eu li, ta escrito o que é melhor,
                    D5
a bula pra curar pavor,
                        G5
aperta o cinto vai subir,
C5                       A5
a minha treta hoje foi menor,
                                   D5
eu sigo o meu caminho e nao corro só
                                 G5
prezo todos meus amigos que se foram ,
                                C5
mamae quero saber onde isso vai dar
                           A5
nossos filhos nao merecem pó,
                                D5
eu sei, o que acredito é em dó maior
                                   G5
se lembra que o mundo não se fez sozinho,
                                  F5
esta louco de cara falei pronto e fim
D5 C5    A5

----------------- Acordes -----------------
A5 = X 0 2 2 X X
Bb5 = X 1 3 3 X X
C5 = X 3 5 5 X X
D5 = X 5 7 7 X X
E5 = 0 2 2 X X X
F5 = 1 3 3 X X X
G5 = 3 5 5 X X X
