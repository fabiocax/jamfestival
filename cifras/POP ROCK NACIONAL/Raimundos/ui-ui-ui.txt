Raimundos - Ui, ui, ui


D5 C5 D5  D5 C5 A5

D5                   C5
Quem botou Bob no rabo do porco
D5                       F5
Quem fez maldade com meu bem
  D5                      C5
Tiraram o acento o cocô virou coco
   D5                         F5
Puseram mais dois zeros, 1 agora é 100

D5  C5  D5
Ui, ui, ui
D5  C5  A5
Ui, ui, ui

D5                        C5
Olha só que doido, que maluco louco
     D5                      F5
Fiz força no banheiro pra cagar, peidei.

       D5                   C5
Me desfiz no banheiro no maior esforço
   D5                       F5
E no melhor da festa fui peidar, caguei.

D5  C5  D5
Ui, ui, ui


F5*                          G#5
Certa vez me apaixonei por uma garota
F5*                        D#5
E nessa garota eu nunca confiei
F5*                              G#5
Porque ela me dizia: - "bota a mão no fogo."
   F5*                       D#5
Eu pus a mão no fogo: _ Aí! Me queimei!

F5 D#5 F5

 F5                   D#5
(Ui, ui, ui eu me queimei) eu me queimei
     F5                     G#5*
Fiz força no banheiro pra cagar, peidei
 F5                   D#5
(Ui, ui, ui eu me queimei) eu me queimei
  F5                        G#5*
E no melhor da festa fui peidar, caguei.

F5 D#5 F5   F5 D#5 C5   D5 C5 D5   D5 C5 A5

----------------- Acordes -----------------
A5 = X 0 2 2 X X
C5 = X 3 5 5 X X
D#5 = X 6 8 8 X X
D5 = X 5 7 7 X X
F5 = 1 3 3 X X X
G#5 = 4 6 6 X X X
