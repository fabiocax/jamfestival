Raimundos - Bicharada


Intro: C5 A5 G5 E5 (2X)

  C5  A5       G5   E5
O que que você acha solta um peido e dá um prensadão
     C5  A5      G5   E5
A calcinha da devassa vem fedendo igual a rapa de buceta
   C5 A5         G5        B5             C5
As pacas me entorpecem, as lhamas me enlouquecem
    F#5                      F5
e a morte chega forte como a tosse do velhote
        E5
que não foge mas se esconde na escuridão

Riff1(toque durante próximo verso)
e|--------------------------------------------------------------------|
B|--------------------------------------------------------------------|
G|--------------------------------------------------------------------|
D|---------------2---2---2---2----------------5-5-5-5-----------------|
A|--------2------2---2---2---2---------2------5-5-5-5---------2-------|
E|-0-2-3-5-5-3-2-0-3-0-3-0-3-0--0-2-3-5-5-3-2-3-3-3-3--0-2-3-5-5-3-2--|
                               |-toque isto 2 vezes--|


"O assum-preto come o sabiá
 O curió também já mandou lá
 E toda a bicharada quando soube da enrabada
 fez uma fila do tamanho do jatobá"

E5 E5* D5 E5* D5 C5 D5 C5

       E5 E5* D5 E5* D5 C5 D5 C5
O passarinho
       E5 E5* D5 E5* D5 C5 D5 C5
tão bonitinho
       E5 E5* D5 E5* D5 C5 D5 C5
é viadinho

(riff1)
"Quando é que ela vem?
 Eu sempre sinto que ela não vem
 A cabeça do meu pinto diz que ela não vem"

E5 E5* D5 E5* D5 C5 D5 C5

       E5 E5* D5 E5* D5 C5 D5 C5
O passarinho
       E5 E5* D5 E5* D5 C5 D5 C5
tão bonitinho
       E5 E5* D5 E5* D5 C5 D5 C5
é viadinho


B5 C5 D5 C5(2X)

E5* F5* G5* F5*(2X)

C5 A5 G5 B5 C5 F#5 F5  E5


Acordes:

  C5 |A5 |G5 |E5 |B5 |F#5|F5 |E5*|D5 |F5* |G5* |
e|---|---|---|---|---|---|---|---|---|----|----|
B|---|---|---|---|---|---|---|---|---|----|----|
G|-5-|-2-|---|---|-4-|---|---|-9-|-7-|-10-|-12-|
D|-5-|-2-|-5-|-2-|-4-|-4-|-3-|-9-|-7-|-10-|-12-|
A|-3-|-0-|-5-|-2-|-2-|-4-|-3-|-7-|-5-|-8--|-10-|
E|---|---|-3-|-0-|---|-2-|-1-|---|---|----|----|

----------------- Acordes -----------------
