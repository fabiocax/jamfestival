Raimundos - Gordelícia

[Intro] E5  G5  A5  C5
        E5  G5  A5  B5
        C#5 B5  F#5
        C#5 B5  F#5
        C#5 B5  F#5
        C#5 B5

Em
Essa menina é uma delícia
         G                     Am
E que delícia esse excesso de fofura
                   C
Me alucina, me fissura

     Em
Tua raba é toda dura
     G                             Am
Enrubesce a minha cabeça é uma loucura
                     B
Eu não consigo mais parar


       Em                  G
Toda cremosa, lambuzada e suada
                Am                      C
Levemente temperada o teu fogo não se apaga

        Em                       G
Sobe e desce, me judia, sento a peia
                     Am
E nós chamamos de sereia
                     B
Que é pra não contrariar

   Em            G
Esteja onde estiver
                      Am          C
Ela é uma classe diferente de mulher
                  Em
Essa menina já virou notícia
         G
Ela é a gordelícia
 Am              B
Deixa eu te mostrar

G#m      E              B
Essa mulher não quer parar
           F#            G#m
Pegou no tranco agora aguenta
           E             B        Eb
Põe lenha na fogueira e bota pra suar
 G#m     E              B
Respira fundo ela quer mais
       F#            G#m
Soltando fogo pelas venta
         E            B          Eb
Debaixo das coberta até o couro gastar

      Em                  G
Toma cachaça na balada todo dia
                   Am                     C
Na segunda ela muscula pra perder sua barriga

        Em                         G
E ela briga com a balança, a sua maior inimiga
         Am                   B
Ela é o topo da cadeia alimentar

       Em                       G
Calça colada com marquinha bronzeada
                Am                    C
E o bezerro se confunde no decote da danada

       Em                         G
Nós reboca a fogueteira, chuta a bola e cabeceia
     Am                    B
Ela maltrata o caboclo até assar

 Em         G
Seja aonde for
                    Am               C
Quando ela passa você sente o seu calor
                   Em          G
E pro teu corpo você olha sem pudor
                    Am           B
E na loucura você chama de meu amor

[Solo] C#5 B5  F#5
       C#5 B5  F#5
       C#5 B5  F#5
       C#5 B5  F#5
       G#m  E  B  F#
       G#m  E  B  Eb

Bm    G               D
Essa mulher não quer parar
           A             Bm
Pegou no tranco agora aguenta
           G             D         F#
Põe lenha na fogueira e bota pra suar
 Bm   G             D
Respira fundo ela quer mais
          A          Bm
Soltando fogo pelas venta
         G          D            F#
Debaixo das coberta até o couro gastar

----------------- Acordes -----------------
A*  = X 0 2 2 2 0 - (*G# na forma de A)
A5*  = X 0 2 2 X X - (*G#5 na forma de A5)
Am*  = X 0 2 2 1 0 - (*G#m na forma de Am)
B*  = X 2 4 4 4 2 - (*A# na forma de B)
B5*  = X 2 4 4 X X - (*A#5 na forma de B5)
Bm*  = X 2 4 4 3 2 - (*A#m na forma de Bm)
C*  = X 3 2 0 1 0 - (*B na forma de C)
C#5*  = X 4 6 6 X X - (*C5 na forma de C#5)
C5*  = X 3 5 5 X X - (*B5 na forma de C5)
D*  = X X 0 2 3 2 - (*C# na forma de D)
E*  = 0 2 2 1 0 0 - (*D# na forma de E)
E5*  = 0 2 2 X X X - (*D#5 na forma de E5)
Eb*  = X 6 5 3 4 3 - (*D na forma de Eb)
Em*  = 0 2 2 0 0 0 - (*D#m na forma de Em)
F#*  = 2 4 4 3 2 2 - (*F na forma de F#)
F#5*  = 2 4 4 X X X - (*F5 na forma de F#5)
G*  = 3 2 0 0 0 3 - (*F# na forma de G)
G#m*  = 4 6 6 4 4 4 - (*Gm na forma de G#m)
G5*  = 3 5 5 X X X - (*F#5 na forma de G5)
