Raimundos - Nêga Jurema

PRE-INTRO(2x):
E|-------------------------------|     
B|-------------------------------|
G|-------------------------------|     
D|-5---3h5-5---3h5-5---3h5-2-2---|     
A|-3-6-----3-6-----3-6-----0-0---|     
E|-------------------------------|

Intro: (A G F# G)4x  A

     C                         A
Nêga Jurema veio descendo a ladeira
                   G                        F
Trazendo na sua sacola um saco de Maria Tonteira
        C                    G
E a mulecada avisou a rua inteira:
                     F         G           C
"Vem correndo que a feira já está pra começar"
              A
"Mas olha as nuvens esse tempo não ajuda
                      G                          F
Pelo menos as minhas mudas eu já sei que vão brotar",

        A
Dizia a Nêga quando vieram os soldados
              G                       F
Se dizendo avisados e começaram a atirar
            C                      A
Pois foi Antônio, filho de José Pereira,
                    G                         F
Que no meio da bagaceira olhou pro céu e a rezar
                  C                          G
Pediu pra Santo Antônio, São Pedro ou Padim Cícero
                     F          G        C'
Ou pros filhos do Caniço que viessem ajudar
Sem Acorde
Foi no pipoco do trovão
 C                                            F
Que se armou a confusão e ninguém pôde acreditar
   G
Que aquilo fosse verdade, foi por toda a cidade,
                   C
Cresceu em todo lugar
   C'
Na igreja das alturas, barzinho, prefeitura,
                                         F
No engenho de rapadura nasceu mato de fumá
  G
E foi com a santa Malícia
que driblou-se a polícia
                   C
e fez a guerra acabar
        C'
FUMÊ FUMÁ
Não é flor de intestino é um matinho nordestino
                       F
que a senhora vai queimar
 G
Faz um bem pra diarréia para o véio e para a véia,
                  C
faz o morto suspirar
  C'
Faz um bem para as artrites, febre ou conjutivite
                       F
Faz qualquer mal se curar
 G
CUMÊ CAGÁ
BEBÊ FUMÁ
                                               C
São as leis da natureza e ninguém vai poder mudar.

Guitarra Base:(C A C A)
Guitarra Solo: Acompanha a basa em C e em A faz:


   1º A          2ºA
E|-------------------------------|     
B|-----5-----------5-------------|
G|-3-5---------3-5---5/0---------|     
D|-------------------------------|     
A|-------------------------------|     
E|-------------------------------|

----------------- Acordes -----------------
A = X 0 2 2 2 0
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
