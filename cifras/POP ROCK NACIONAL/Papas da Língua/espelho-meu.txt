﻿Papas da Língua - Espelho Meu

Intro

     Am       G      D         Am       G      D
E|--2-3-5--5-3-2--3-2---------2-3-5--5-3-2-5-----------|
B|--------------------3-3--------------------3---------|
G|-----------------------------------------------------|
D|-----------------------------------------------------|
A|-----------------------------------------------------|
E|-----------------------------------------------------|

     Am       G      D         Am       G      D
E|--2-3-5--5-3-2--3-2---------2-3-5--5-3-2-0-----------|
B|--------------------3-3--------------------3---------|
G|-----------------------------------------------------|
D|-----------------------------------------------------|
A|-----------------------------------------------------|
E|-----------------------------------------------------|


(Am G D)
Perdi os anos todos
em cada ano mais
um pouco da virtude
que todo homem traz
(Am G D)
Gastei o tempo todo
a desprezar paixões
do rancho imenso e fundo do orgulho
eu fui o capataz (2x)
Em D/F# C
Ah, onde está o meu você
Em D/F# C
Ah, onde está o meu você
Am
sei que ninguém vai perdoar
nem mesmo há o que perdoar
G
é mesmo assim
D D/C D/B Am
E desejei amanhecer em paz
Em
em outros tempos atrás
D D/C D/B
pedindo a Deus que para amar
Am Em D D/C D/B Am
não fosse tarde demais
G F C
Ah, espelho meu
G F C
Ah , existe alguém
D# F G
mais triste do que eu

(Solo igual à introdução)

Sonhei os sonhos todos
Imaginando o céu
e dele fui seu prisioneiro
em cela de papel (2x)

Joguei o jogo de jogar
com a e i o u
e penetrei naquelas almas
olhos de Capitu (2x)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D# = X 6 5 3 4 3
D/B = X 2 0 2 3 2
D/C = X 3 X 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
