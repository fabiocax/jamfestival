Papas da Língua - Viajar

(intro)  Bm7 G7+ F#m7 Em7 G/A

(no acorde "Em7" aperte o dedo 4 na casa Sol)

Bm7            G7+                     F#m7
Eu quero lhe dizer  vou contar pra você
              Em7 G/A
A minha alegria
Bm7              G7+
Você vai perceber
                  F#m7
E se quiser vai ver
              Em7    G/A
Tudo o que eu vi um dia
Bm7               G7+
Fui pro lado de lá
              F#m7
Fui para desvendar
              Em7 G/A
A minha fantasia
Bm7                G7+              F#m7
Tudo que aprendi  tudo que conheci

                    Em7 G/A
Valeu a pena descobrir

(refrão 2x)
(Bm7  G7+  F#m7  Em7  A)
Quero lhe falar  é bom viajar
Tocar pelo mundo à fora
Nova Iorque é bom  Paris é demais
Algo que eu não vou esquecer jamais
oh não  (jamais):Repete durante a musica!

(Bm7 G7+ F#m7 Em7 G/A)

(parte 2)
Fui pro lado de lá
Então eu percebi
Que o sonho se realiza
É só você ter fé
e sempre acreditar
que tudo pode conquistar

(refrão 2x
(parte 2)
(refrão 2x)
( Bm7 G7+ F#m7 Em7 (G/A) Bm7 )

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm7 = X 2 4 2 3 2
Em7 = 0 2 2 0 3 0
F#m7 = 2 X 2 2 2 X
G/A = 5 X 5 4 3 X
G7+ = 3 X 4 4 3 X
