Papas da Língua - Canção Pra Ela

Intro 4x:  E  C#m7  A  B9

E               C#m7
Tela branca de cinema
A         B9
Minha grande tela
E                C#m7
Os amores que passavam
A              B9
Iam numa caravela
E            C#m7
Lá estava ela...
A         B9
Sobre a janela
E       C#m7
Ela era linda
A       B9
E eu amava ela
C#m7
Eu não sei onde foi que eu errei


Quanto mais eu penso nela
A
Mais eu fico sem ninguém

E         C#m7    A            B9
Ela me amava e eu amava ela
E       C#m7   A   B9                     (2x)
Ela se chamava ela

Intro 4x: ( E  C#m7  A  B9 )

E           C#m7    A
Uma bela rosa casa
B9
Era aquela
E        C#m7                A
Lá no fundo um arco-íris
 B9
Uma aquarela
E             C#m7        A
E era como um sonho
B9
A felicidade dela
E      C#m7    A
Ela era linda
 B9
E eu amava ela
C#m7
Ai, ai quanta falta ela me faz

Quanto mais eu fico sem
A
Mais ela me vem...

E         C#m7    A            B9
Ela me amava e eu amava ela
E       C#m7   A   B9                     (2x)
Ela se chamava ela

( E  C#m7  A  B9 )

Hoje o sol está tão lindo
Chegou a primavera
Branca ou amarela
Qual a cor do meu amor sem ela
Sob a luz da vela
Uma canção pra ela
Qual é a palavra aquela
Pra rimar com ela

C#m7
Eu não sei onde foi que eu errei

Quanto mais eu penso nela
A
Mais eu fico sem ninguém

E         C#m7    A            B9
Ela me amava e eu amava ela
E       C#m7   A   B9                     (2x)
Ela se chamava ela

E           C#m7      A
Eu sem ela já não posso
 B9
Mais viver e ela
E         C#m7    A
Ela se chamava ela
G
O amor é lindo
   Em         C    D    G
E eu sou bacana

----------------- Acordes -----------------
A = X 0 2 2 2 0
B9 = X 2 4 4 2 2
C = X 3 2 0 1 0
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
