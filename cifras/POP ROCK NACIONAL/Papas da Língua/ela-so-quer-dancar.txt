Papas da Língua - Ela Só Quer Dançar

         G
Para de sofrer
        D
Para de chorar
                 Am                C
Hoje não tem pra ninguém, nem pra você.

       G                 D
Pode esquecer, pode enlouquecer.
                  Am           C
Hoje não vem ninguém pra ver você.

   G        D
Olha seu corpo
   Am       C
Flutua no chão
   G       D
Linda estrela
  Am       C
Dona do lugar


         G
Pode acreditar
         D
Pode agradecer
             Am              C
Hoje você vai olhar, vai só olhar.

Refrão:
        G        D           C
Ela só quer dançar, só quer dançar.
        G        D           C
Ela só quer dançar, só quer dançar.
        G        D           C
Ela só quer dançar, só quer dançar.
        G        D           C
E a noite vai passar, e a noite vai.

( G  D  Am  C )

         G
Para de sofrer
        D
Para de chorar
                 Am                C
Hoje não tem pra ninguém, nem pra você.

       G                 D
Pode esquecer, pode enlouquecer.
                  Am           C
Hoje não vem ninguém pra ver você.

   G        D
Olha seu corpo
   Am       C
Flutuar no chão
   G       D
Linda estrela
  Am       C
Dona do lugar

         G
Pode acreditar
         D
Pode agradecer
             Am              C
Hoje você vai olhar, vai só olhar.

Refrão 5x:
        G        D           C
Ela só quer dançar, só quer dançar.
        G        D           C
Ela só quer dançar, só quer dançar.
        G        D           C
Ela só quer dançar, só quer dançar.
        G        D           C
E a noite vai passar, e a noite vai.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
