Papas da Língua - Eu Sei

[Intro] C  G  Em  Bm
        C  G  Em  Bm

C     G    Em            D
  Eu sei, tudo pode acontecer
C     G    Em                    D
  Eu sei, nosso amor não vai morrer
       Em      Bm7   C           G
Vou pedir aos céus, você aqui comigo
       Em     Bm7   C              D
Vou jogar no mar, flores pra te encontrar

C      G       Em             D
  Não sei porque você disse adeus
C      G     Em                D
  Guardei o beijo que você me deu

       Em       Bm7  C           G
Vou pedir aos céus, você aqui comigo
       Em     Bm7   C              D
Vou jogar no mar, flores pra te encontrar

 C            G       C         G
You say good-bye, and I say hello
 C            G       C         D
You say good-bye, and I say hello

( C  G  Em  Bm )

C      G       Em             D
  Não sei porque você disse adeus
C      G     Em                D
  Guardei o beijo que você me deu

       Em       Bm7  C           G
Vou pedir aos céus, você aqui comigo
       Em     Bm7   C              D
Vou jogar no mar, flores pra te encontrar
 C            G       C         G
You say good-bye, and I say hello
 C            G       C         D
You say good-bye, and I say hello

[Final] C  G  Em  Bm7

----------------- Acordes -----------------
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
