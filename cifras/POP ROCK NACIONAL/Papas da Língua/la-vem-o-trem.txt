Papas da Língua - Lá Vem o Trem

Intro:  G  C
Am  D (4x)

       Ah aaaah  uhuuu
      G           C
tô bolado na estação
     Am          D
esperando por você
        G          C
mais você não dá sinal
      Am        D
e eu fico sem saber
        Em   Am G   C
se você vem
        Em   Am G   G
se você vem
        G            C
faz um tempo que eu penso
         Am        D
em te levar pra passear
       G              C
mais você não me diz nada

      Am        D
e eu fico a pensar
        Em   Am G   C
será que dá
        Em   Am  G  G
será que dá
      G            C
você não me dá resposta
       Am            D
e me deixa ao deus dará
        G          C
já não sei como fazer
       Am           D
pra poder te conquistar
        Em   Am G   C
não vou parar
        Em   Am G   G
não vou parar
C            G          D   Em
 passo meus dias sem saber
C         G            D    Em
 se eu terei a chance de dizer
C             G           D   Em
 que a minha vida é com você
C         G           D/F# D/F#
 faço de tudo pra te ter

Intro:  G  C
Am  D (4x)
       Ah aaaah  uhuuu


----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
