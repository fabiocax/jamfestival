Skank - Te Ver

(intro 2x) B C#m

(refrão 2x)
B
  Te ver e não te querer
C#m
  É improvável é impossível
B
  Te ter e ter que esquecer
C#m
  É insuportável é dor incrível

B                                 C#m
  É como mergulhar num rio e não se molhar
B                                 C#m
  É como não morrer de frio no gelo polar
B                              C#m
  É ter o estômago vazio e não almoçar
B                                        C#m
  É ver o céu se abrir no estio e não se animar


B
  Te ver e não te querer
C#m
  É improvável é impossível
B
  Te ter e ter que esquecer
C#m
  É insuportável é dor incrível

( B C#m ( (2x)

B                               C#m
  É como esperar o prato e não salivar
B                                C#m
  Sentir apertar o sapato e não descalçar
B                                    C#m
  É ver alguém feliz de fato sem alguém para amar
B                               C#m
  É como procurar no mato estrela do mar

B
  Te ver e não te querer
C#m
  É improvável é impossível
B
  Te ter e ter que esquecer
C#m
  É insuportável é dor incrível

( B C#m ) (2x)

B                             C#m
  É como não sentir calor em Cuiabá
B                          C#m
  Ou como no Arpoador não ver o mar
B                                   C#m
  É como não morrer de raiva com a política
B                             C#m
  Ignorar que a tarde vai vadia a mítica

B                             C#m
  E como ver televisão e não dormir
B                                 C#m
  Ver um bichano pelo chão e não sorrir
B                                      C#m
  É como não provar o nectar de um lindo amor
B                                      C#m
  Depois que o coração detecta a mais fina flor

(refrão 2x)

B
  Te ver e não te querer
C#m
  É improvável é impossível
B
  Te ter e ter que esquecer
C#m
  É insuportável é dor incrível

----------------- Acordes -----------------
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
