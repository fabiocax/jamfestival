Skank - Pra Sempre

Intro: C  Bb  F  C  (2x)
       C  Bb  F  G

C                 F
Acho que agora vejo
C                   F
Acho que eu encontrei
C                   F
O que havia e sempre procurei
C                F
Não veio das montanhas
C              F
Nem tava nesse mar
C                      F
Mas de um clarão no seu olhar

Dm                  C
Tive histórias tristes
    G
eu só bem sei
Dm               C             G
mas que o tempo fez transformar

C                         F
Só assim pra sentir o beijo
C                     F
Só assim para reconhecer
C                           F
A diferença entre os outros e você
C                      F
Como vale arriscar na vida
C                 F
como é bom amadurecer
C                            F
e te encontrar assim por merecer
Dm                  C
Tive histórias tristes
  G
e só eu bem sei
Dm             C            G
e que o tempo fez transformar
Dm                 C
Em vitórias, brilhos
          G
que me levam a crer
Dm        C             G
neste forte impeto de amar

( C  Bb  F  C )

( C  Bb  F  G )

Amar você pra sempre
Amar você pra sempre
Amar você pra sempre
Amar você pra sempre

C                F
Sei que agora vejo
C                 F
Sei que eu encontrei
C                    F
O que havia e sempre procurei

C               F
Tendo você comigo
C                F
Há muito pra sonhar
C                  F
E o universo inteiro
C         F
pra alcançar

Dm                 C
Tive histórias tristes
G
e só eu bem sei
Dm               C         G
e que o tempo fez transformar

Dm                 C
Em vitórias, brilhos
    G
que me levam a crer
Dm            C          G
neste forte impeto de amar

( C  Bb  F  C )

( C  Bb  F  G )

Amar você pra sempre
Amar você pra sempre
Amar você pra sempre
Amar você pra sempre

Dm                  C
Tive histórias tristes
 G
e só eu bem sei
Dm            C            G
e que o tempo fez transformar

Dm                C
Em vitórias, brilhos
          G
que me levam a crer
Dm         C            G
neste forte impeto de amar

( C  Bb  F  C )

( C  Bb  F  G )

Amar você pra sempre
Amar você pra sempre
Amar você pra sempre
Amar você pra sempre

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
