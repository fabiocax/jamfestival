Skank - Rebelião

Todos os versos são tocados com Sol Maior (G), não
há variação, exceto pelo refrão, que tem praticamente
os mesmos acordes da introdução.

Intro1: G
Intro2 (power): G5 F5 C5 A#5 (3x)

 G
 Nem todo o arsenal das guarnições civis
 Nem trezentos fuzis m-16
 Nem as balas do Clinton, as bulas do Papa
 Nem os tapas dos que guardam leis
 Nada disso vai fazer a gente acatar
 O absurdo ad aeternum desse lugar
 Décimo círculo do último inferno
 Infecto, sem luz, sem letra, sem lei
                   G5 F5 C5 A#5
 E pronto pra queimar

G5 F5 C5 A#5
                   G5 F5 C5 A#5
 E pronto pra queimar


 Inferno de Dante diante de cada um
 Da hora em que começa a manhã
 Até a hora em que a cela se esfria, suja e sombria
 E a lua livre meio que zomba de nós
 Nem todo o aparato da Santa Inquisição
 Nem a dancinha do padre na sua televisão
 Bi Babulina chegou com gasolina e colchão
 E a esperança é mato no coração
                   G5 F5 C5 A#5
 E pronto pra queimar

G5 F5 C5 A#5
                   G5 F5 C5 A#5
 E pronto pra queimar

 Não há solução, nem mesmo hipocrisia
 Não há qualquer sinal de melhorar um dia
 Se você não se importa eu vou dinamitar
 A porta, a porra dessa masmorra
 Nem a educação do colégio Rousseau
 Pode dar conta do que aqui se passa
 Flores do mal! Luz do horror!
 Farol da barra dessa desgraça
                   G5 F5 C5 A#5
 Só serve pra queimar

----------------- Acordes -----------------
A#5 = X 1 3 3 X X
C5 = X 3 5 5 X X
F5 = 1 3 3 X X X
G = 3 2 0 0 0 3
G5 = 3 5 5 X X X
