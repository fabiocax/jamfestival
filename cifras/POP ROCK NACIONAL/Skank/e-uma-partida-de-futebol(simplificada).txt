Skank - É Uma Partida de Futebol

Intro 4x: A  D  G  A

Refrão:

 A
Bola na trave não altera o placar

Bola na área sem ninguém pra cabecear

Bola na rede pra fazer o gol
      G
Quem não sonhou
                          A
Em ser um jogador de futebol


Primeira Parte:

      A
A bandeira no estádio é um estandarte


A flâmula pendurada na parede do quarto

O distintivo na camisa do uniforme
     G                                A
Que coisa linda é uma partida de futebol

( A  D  E ) (2x)


Segunda Parte:

 A
Posso morrer pelo meu time

Se ele perder, que dor, imenso crime

Eu posso chorar se ele não ganhar

Mas se ele ganha não adianta
           G                        A
Não há garganta que não pare de berrar
      A
A chuteira veste o pé descalço

O tapete da realeza é verde

Olhando pra bola eu vejo o Sol
        G
Está rolando agora
                      A
É uma partida de futebol


Terceira Parte:

        A
O meio campo é o lugar dos craques

Que vão levando o time todo pro ataque

O centroavante, o mais importante
           G                          A
Que emocionante é uma partida de futebol

( A  D  E )  (4x)

Repete a Intro 2x: A  D  G  A


Quarta Parte:

         A
O meu goleiro é um homem de elástico

Os dois zagueiros têm a chave do cadeado

Os laterais fecham a defesa
           G                         A
Mas que beleza é uma partida de futebol


Repete o Refrão:

 A
Bola na trave não altera o placar

Bola na área sem ninguém pra cabecear

Bola na rede pra fazer o gol
      G
Quem não sonhou
                          A
Em ser um jogador de futebol


Repete a Terceira Parte:

        A
O meio campo é o lugar dos craques

Que vão levando o time todo pro ataque

O centroavante, o mais importante
           G                          A
Que emocionante é uma partida de futebol

( A  D  E )  (16x)

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
G = 3 2 0 0 3 3
