Skank - Presença

(intro) F#m E B

E|-------------------------------------|
B|-------------------------------------|
G|-------------------------------------|
D|---------------------4-2-4---4-2-4---|
A|--4--2--4-----2-0-2--------2---------|
E|-2--------0--0-----2--------------2--|

F#m    E             B
 No sol que vai dourar
F#m    E                  B
 Na noite mais escura iluminar
F#m          E                  B
No canto da espuma do verde do mar
F#m           E            B
Ela é feito pluma solta no ar... E também como é

 (riff) F#m E B
E|-------------------------------------|
B|-------------------------------------|
G|-------------------------------------|
D|---------------------4-2-4---4-2-4---|
A|--4--2--4-----2-0-2--------2---------|
E|-2--------0--0-----2--------------2--|


F#m     E          B
 Meus dedos vão tocar
F#m              E                     B
Seus lábios vermelhos a minha boca beijar
F#m            E                B
De olhos fechados só pra apimentar
F#m             E                   B
Desse outro mundo o segredo vou revelar (Éeee)

D                     A
Está em tudo sempre onde vou
          E           C#
Já não preciso... Procurar! Êee

(refrão)

F#m      E          B
O seu presente eu sou
                     F#m
Onde está presente eu vou
          E        B
O meu presente chegou
                      F#m
Se ela está presente eu tô

 (riff) F#m E B
E|-------------------------------------|
B|-------------------------------------|
G|-------------------------------------|
D|---------------------4-2-4---4-2-4---|
A|--4--2--4-----2-0-2--------2---------|
E|-2--------0--0-----2--------------2--|

F#m       E         B
No seu jeito de olhar
F#m           E               B
Um pensamento livre vou imaginar
F#m            E             B
No horizonte surge, não há lunar
F#m            E              B
Num suspiro profundo a te desejar

D                 A
Está comigo, eu levo onde vou
         E            C#
Já não preciso... Procurar! Êee

(refrão 2x)

F#m      E          B
O seu presente eu sou
                     F#m
Onde está presente eu vou
          E        B
O meu presente chegou
                      F#m
Se ela está presente eu tô

F#m        E          B
É só me chamar que eu vou!

D                     A
Está em tudo sempre onde vou
          E              C#
Já não preciso... Procuraaaarrr !!! Êeeeeee!!!

(refrão)
F#m      E          B
O seu presente eu sou
                     F#m
Onde está presente eu vou
          E        B
O meu presente chegou
                      F#m
Se ela está presente eu tô

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C# = X 4 6 6 6 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
