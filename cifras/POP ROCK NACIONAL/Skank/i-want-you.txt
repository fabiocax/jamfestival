Skank - I Want You

    G
The guilty undertaker sighs
    Bm
The lonesome organ grinder cries
Em                               D
The silver saxophones say I should refuse you
    C/G
The cracked bells and washed-out horns
D
Blow into my face with scorn
Em
But it's not that way
                 D
I wasn't born to lose you

G
I want you
Bm
I want you
Em
I want you

      D
So bad
G
I want you

    G
The drunken politicians leaps
Bm
Upon the street where mothers weep
Em
And the saviours who are fast asleep
      D
They wait for you
C/G
And I wait for them to interrupt
D
Me drinkin´ from my broken cup
Em
And ask for me
               D
To open up the gate for you

G
I want you
Bm
I want you
Em
I want you
      D
So bad
G
I want you

G  Bm
    Now all my fathers, they've gone down
     Em
True love, they´ve been without it
    D
But all their daughters put me down
      C/G                D
´Cause I think about it
        G
Well, I returned to the Queen of Spades
Bm
And talk with my chambermaid
Em
She knows that I'm not afraid
D
To look at her
C/G
She is good to me
D
And there's nothing she doesn't see
Em
She knows where I´d like to be
                 D
But it doesn't matter

G
I want you
Bm
I want you
Em
I want you
      D
So bad
G
I want you

         G
Now your dancing child with his Chinese suit
Bm
He spoke to me, I took his flute
Em
No, I wasn't very cute to him
D
Was I?
C/G
But I did it, though, because he lied
D
Because he took you for a ride
Em
And because time was on his side
D
And because I...

G
I want you
Bm
I want you
Em
I want you
      D
So bad
G
Honey I want you

----------------- Acordes -----------------
Bm = X 2 4 4 3 2
C/G = 3 3 2 X 1 X
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
