Skank - Baixada News

(intro) Gm7  Cm7

Gm7             Cm7
Zilda é uma mulher
               Gm7          Cm7
Que mora na Baixada Fluminense
Gm7             Cm7
Mãe de cinco filhos
                  Gm7  Cm7
Cinco bocas pra comer
     Gm7            Cm7
Seu ex-marido trabalhava
                  Gm7   Cm7
Como trocador de ônibus
   Gm7               Cm7
Trocou Zilda por uma dama
                  Gm7   Cm7
Que passou pela roleta
  F
A vida já não era fácil
        Eb
Com a ajuda dele lá

 F
Agora Zilda tá sozinha
        Eb
Com os filhos pra criar
   F
Às cinco horas ela acorda
     Eb
E prepara o café
  F
Depois com outros pescadores
        Eb
Vai pro mangue de Magé
Gm7          Cm7
Baía de Guanabara
                Gm7   Cm7
A pesca do caranguejo
Gm7         Cm7
Baía de Guanabara
                    Gm7  Cm7
Impossível, mas eu vejo
               Gm7   Cm7
A pesca do caranguejo
Gm7         Cm7
Baía de Guanabara
                    Gm7  Cm7 (Gm7 Cm7)
impossível, mas eu vejo
    Gm7      Cm7
Aos 28 anos, Zilda diz
                 Gm7   Cm7
Que é dona de si mesma
     Gm7
Não pensa muita coisa
      Cm7             Gm7   Cm7
Não espera nada de ninguém
   Gm7                 Cm7
Catando a vida pelas patas
                  Gm7  Cm7
Dando tapas no destino
    Gm7                 Cm7
Arregaçar as mangas no mangue
                  Gm7     Cm7
Paciência em cada gota de sangue
  F
A vida já não era fácil
        Eb
Com a ajuda do marido
     F
Mas ela não sabe pensar
         Eb
No que podia ter sido
   F
Às cinco horas ela acorda
     Eb
E prepara o café
  F
Depois com outros pescadores
        Eb
Vai pro mangue de Magé
Gm7          Cm7
Baía de Guanabara
                Gm7   Cm7
A pesca do caranguejo
Gm7         Cm7
Baía de Guanabara
                    Gm7  Cm7
Impossível, mas eu vejo
                Gm7   Cm7
A pesca do caranguejo
Gm7         Cm7
Baía de Guanabara
                    Gm7
Impossível, mas eu vejo ...

----------------- Acordes -----------------
Cm7 = X 3 5 3 4 3
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
Gm7 = 3 X 3 3 3 X
