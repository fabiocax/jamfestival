Pitty - Me Adora

(intro)  B E B E C#m F#

(primeira parte)
B
  Tantas decepções eu já vivi
G#m
  Aquela foi de longe a mais cruel
E
  Um silêncio profundo e declarei
F#
  Só não desonre o meu nome

B
  Você que nem me ouve até o fim
G#m
  Injustamente julga por prazer
E
  Cuidado quando for falar de mim
F#
  E não desonre o meu nome


( B B4 B B4 )
( B B4 B B4 )

(pré-refrão)
E
  Será que eu já posso enlouquecer
            B
Ou devo apenas sorrir?
E
  Não sei mais o que eu tenho que fazer
       F#
Pra você admitir...

(refrão 2x)
              B   E
Que você me adora
             B    E
Que me acha foda
                    C#m           F#
Não espere eu ir embora pra perceber

B
  Perceba que não tem como saber
G#m
  São só os seus palpites na sua mão
E
  Sou mais do que o seu olho pode ver
F#
  Então não desonre o meu nome

B
  Não importa se eu não sou o que você quer
G#m
  Não é minha culpa a sua projeção
E
  Aceito a apatia, se vier
F#
  Mas não desonre o meu nome

(pré-refrão)

(refrão)

(solo)

(B B/C# G#m7 G)x4 G

(refrão 2x)

----------------- Acordes -----------------
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
