Pitty - Dançando

[Intro]

      A                           F#m
E|-----------0---------------------------0--------0----|
B|------0h2-----------0h2-----------0h2--------------2-|
G|---2-------------2-------2-----2-------------2-------|
D|-----------------------------------------------------|
A|-0--0-----0----0--0-----0----------------------------|
E|-----------------------------2--2-----2----2--2------|

      D                           E
E|-----------0-----------------------------------------|
B|------0h2-----------0h2-----------0h2-----------2----|
G|---2-------------2-------2-----2-------2-----1-----1-|
D|-0--0-----0----0--0-----0----------------------------|
A|-----------------------------------------------------|
E|-----------------------------0--0-----0----0--0------|

A
 Eu sei que lá no fundo
F#m
   Há tanta beleza no mundo

D                  E
Eu só queria enxergar
A
 As tardes de domingo
F#m
O dia me sorrindo
D                  E
Eu só queria enxergar
F#m                      E
   Qualquer coisa pra domar o peito em fogo
F#m                 E
   Algo pra justificar uma vida morna

   A
O mundo acaba hoje eu estarei dançando
   F#m
O mundo acaba hoje eu estarei dançando
   D                                        F#m        E
O mundo acaba hoje eu estarei dançando com você  hmmmm

   A
O mundo acaba hoje eu estarei dançando
   F#m
O mundo acaba hoje eu estarei dançando
   D                                        F#m      E   F#m  D   E  A
O mundo acaba hoje eu estarei dançando com você    hmmmm        hmmmm

A
  Não esqueço aquela esquina
F#m
   A graça da menina
D                  E
Eu só queria enxergar
A
 Por isso eu me entrego
F#m
A um imediatismo cego
  D                  E
Pronta pro mundo acabar

F#m                  E
   Você acredita no depois ?
    E
Prefiro o agora
F#m                         E
   Se no fim formos só nós dois, que seja lá fora

   A
O mundo acaba hoje eu estarei dançando
   F#m
O mundo acaba hoje eu estarei dançando
   D                                        F#m        E
O mundo acaba hoje eu estarei dançando com você  hmmmm

   A
O mundo acaba hoje eu estarei dançando
   F#m
O mundo acaba hoje eu estarei dançando
   D                                        F#m         E  F#m       D  E  A
O mundo acaba hoje eu estarei dançando com você  hmmmm         hmmmm

[Pré-refrão]

      F#m                    E
E|------2-----------2-----------2-----------2-------|
B|------2-----------2-----------2-----------2-------|
G|---2-----2--2--2-----2--2--2-----2--2--2-----2--2-|
D|--------------------------------------------------|
A|--------------------------------------------------|
E|-2--2-----2--2--2-----2--0--0-----0--0--0-----0---|

[Refrão]

      A                           F#m
E|-----------0---------------------------0---------------|
B|------0h2-----------0h2-----------0h2------------0h2---|
G|---2-------------2-------2-----2-------------2--------2|
D|-------------------------------------------------------|
A|-0--0-----0----0--0-----0------------------------------|
E|-----------------------------2--2-----2----2--2------2-|

      D                           E
E|-----------0-----------------------------------------|
B|------0h2-----------0h2-----------0h2-----------2----|
G|---2-------------2-------2-----2-------2-----1-----1-|
D|-0--0-----0----0--0-----0----------------------------|
A|-----------------------------------------------------|
E|-----------------------------0--0-----0----0--0------|

[Pós-refrão]

      F#m                    D       E
E|------------------------------------------------|
B|------2-----------2-----------2----------2------|
G|---2-----2--2--2-----2--2--2----2--2--2----2--2-|
D|-------------------------0--0----0--------------|
A|------------------------------------------------|
E|-2--2-----2--2--2-----2-------------0--0----0---|

      A
E|----------------------------------------------------|
B|------0h2----------0h2----------0h2----------0h2----|
G|---2-------2----2-------2----2-------2----2-------2-|
D|----------------------------------------------------|
A|-0--0-----0---0--0-----0---0--0-----0---0--0-----0--|
E|----------------------------------------------------|

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
