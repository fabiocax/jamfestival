Pitty - Hoje Cedo

      Bbm
Hoje cedo
               Ab           Eb7/G
Quando eu acordei e não te vi
                    F#
Eu pensei em tanta coisa

      Bbm
Tive medo
                Ab
Ah, como eu chorei, eu sofri
       Eb7/G
Em segredo
                 F#
Tudo isso, hoje cedo

     Bbm
Holofotes fortes, purpurina
      Ab
O sorriso dessas mina só me lembra cocaína
Eb7/G
    Em cinco abrem-se as cortinas

Estáticas retinas brilham, garoa fina
     Bbm
Que fita
Meus poema me trouxe
Onde eles não habita
Ab
   A fama irrita, grana dita, cê desacredita
Eb7/G
      Fantoches, pique Celso Pitta mente
 F#
Mortos tipo meu pai, nem eu me sinto presente
   Bbm
É rima que "cês qué"? Toma duas, três
Ab
  Farta pra infartar cada um de vocês
      Eb7/G
Num abismo sem volta, de festa, ladainha
 F#
Minha alma afunda igual
Minha família em casa sozinha

Bbm
    Entre putas, como um cafetão
             Ab
Coisas que afetam sintonia
Como sonhei em tá aqui um dia
  Eb7/G
Crise, trampo, ideologia, pause
E é aqui, onde "nóis" entende a Amy Winehouse

      Bbm
Hoje cedo
               Ab           Eb7/G
Quando eu acordei e não te vi
                    F#
Eu pensei em tanta coisa

      Bbm
Tive medo
                Ab
Ah, como eu chorei, eu sofri
       Eb7/G
Em segredo
                 F#
Tudo isso, hoje cedo
Vagabundo, a trilha é um precipício, tenso, o melhor
Quero salvar o mundo
Pois desisti da minha família e numa luta mais difícil
                        Bbm
A frustração vai ser menor
Digno de dó, só o pó, vazio comum
Ab
  Que já é moda no século 21
Eb7/G
     Blacks com voz sagaz gravada
 F#
Contra vilões que sangram a quebrada
        Bbm
Só que raps por nóiz, por paz, mais nada
    Ab
Me pôs nas gerais, numa cela trancada
        Eb7/G
Eu lembrei do Racionais, reflexão
         F#
Aí, os próprio preto num tá nem aí com isso, não
          Bb
É um clichê romântico, triste
                   Ab
Vai perceber, vai ver, se matou e o paraíso não existe
          Eb7/G
Eu ainda sou o Emicida da Rinha
                F#
Lotei casas do sul ao norte

Mas esvaziei a minha
   Bbm
E vou por aí, Taleban
                    Ab
Vendo os boy beber dois mês de salário da minha irmã
       Eb7/G
Hennessys, avelãs, camarins, fãs, globais
   F#
Mano, onde eles tavam há dez anos atrás
     Bbm
Showbiz como a regra diz, lek
             Ab
A sociedade vende Jesus, por que não ia vender rap
Eb7/G
     O mundo vai se ocupar com seu cifrão
            F#
Dizendo que a miséria é quem carecia de atenção

      Bbm
Hoje cedo
               Ab           Eb7/G
Quando eu acordei e não te vi
                    F#
Eu pensei em tanta coisa

      Bbm
Tive medo
                Ab
Ah, como eu chorei, eu sofri
       Eb7/G
Em segredo
                 F#
Tudo isso, hoje cedo

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Bb = X 1 3 3 3 1
Bbm = X 1 3 3 2 1
Eb7/G = 3 X 1 3 2 X
F# = 2 4 4 3 2 2
