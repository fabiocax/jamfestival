Pitty - Todos Estão Mudos


(intro) Em5 D5 B5 Em5       2X
        Em5 D5 B5 A5

Em5                D5
Já não ouço mais clamores
Em5                   A5
Nem sinal das frases de outrora
Em5                     D5
Os gritos são suprimidos
Em5                     D5
O corvo diz: "nunca mais"

Em5                      D5
Não parece haver mais motivos
Em5                           A5
Ou coragem pra botar a cara pra bater
Em5                          D5
Um silêncio assim pesado
Em5                         D5
Nos esmaga cada vez mais


Em5                 D5
Não espere, levante
B5                        D5
Sempre vale a pena bradar       REFRÃO
Em5           D5
É hora
              B5   D5
Alguém tem que falar

Em5 D5 B5 Em5    Em5 D5 B5 A5
Alguém tem que falar...
Em5 D5 B5 Em5   Em5 D5 B5 A5

Em5                           D5
Há quem diga que isso é velho
Em5                          A5
Tanta gente sem fé num novo lar
Em5                        D5
Mas existe o bom combate
Em5                       D5
É não desistir sem tentar.

Em5                 D5
Não espere, levante
B5                        D5
Sempre vale a pena bradar
Em5           D5
É hora
              B5   D5
Alguém tem que falar

(solo)Em5   D5   B5   Em5  Em5   D5   B5   A5  (2x)

hu, hu, hu,...  (nessa parte apenas baixo e bateria)

 Em5                 D5
Não espere, levante
B5                        D5
Sempre vale a pena bradar           2X Refrão
Em5           D5
É hora
              B5   D5
Alguém tem que falar.

----------------- Acordes -----------------
A5 = X 0 2 2 X X
B5 = X 2 4 4 X X
D5 = X 5 7 7 X X
Em5 = 0 2 2 0 0 0
