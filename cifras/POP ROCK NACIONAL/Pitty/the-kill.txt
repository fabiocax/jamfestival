Pitty - The Kill

Intro:

    C5        D5       E5          B5         
e|-------(0)-------(0)------(0)---------(0)-----|
B|-------(O)-------(O)------(0)---------(0)-----|
G|-5-5-5-----7-7-7----9-9-9-------4-4-4---------|
D|-5-5-3-----7-7-7----9-9-9-------4-4-4---------|
A|-3-3-3-----5-5-5----7-7-7-------2-2-2---------|
E|----------------------------------------------|

As partes que aparece (0), basta tocar depois do acorde, as duas cordas mais agudas(e,B),juntas,sem abafar.
Galera essa é a versão da música The Kill,gravada com a cantora brasileira Pitty.

C5                        D5
     What if I wanted to break
                         E5
Laugh it all off in your face
              B5
What would you do?

C5                 D5
    E se eu desmoronar

                      E5
se não pudesse mais aguentar
          B5
O que você faria?

C5             D5
Come break me down and bury me, bury me
E5                 B5
I am finished with you

           2X                        2X
E|------------------------|---------------------------|
B|------------------------|---------------------------|sempre na 2x desse riff
G|-12-9-------12-9--------|---------12---------12(14)-|vc substitui a ultima nota  
D|-----10-9--9----10-9--9-|-9h10-10----9h10-10--------|12 por 14 q esta entre paranteses.
A|---------10---------10--|---------------------------|riff acompanha a parte abaixo
E|------------------------|---------------------------|

C5                     D5
    E se eu quisessem lutar
                       E5
Pelo resto da vida implorar
             B5
O que você faria?

C5
You say you wanted more
D5
 What are you waiting for?
E5                  B5
I'm not running from you

C5             D5
Come break me down and bury me, bury me
E5                 B5
I am finished with you
C5          D5
Look in my eyes You're killing me, killing me
E5               B5
All I wanted was you

A5             B5       E5
   I tried to be someone else
                        C5        D5
   But nothing seems to change I know now

E5
This  is  who  I   really   am

  A5            B5       E5                  C5         D5
Inside  I finally found myself  Fighting for a chance  I know now,
E5
This  is  who  I  really  am

    E5        E5        E5      C5         
e|---------------------------------------|
B|---------------------------------------|
G|-9--9-9---9--9-9---9--9-9--5-5-5-5-5-5-| (x3)
D|-9--9-9---9--9-9---9--9-9--5-5-5-5-5-5-|
A|-7--7-7---7--7-7---7--7-7--3-3-3-3-3-3-|
E|---------------------------------------|

      E5                F#5         G5                 
e|--------------------------------------|           
B|--------------------------------------|   
G|-7--7-7--7--7--7-7/9--9---9--9-/10-10-|   
D|-7--7-7--7--7--7-7/------------/------|  
A|-9--9-9--9--9--9-9/11-11--11-11/12-12-|  
E|--------------------------------------|  

C5             D5
Come break me down and bury me, bury me
E5                  B5
I am finished with you
C5          D5
Look in my eyes You're killing me, killing me
E5               (B5)
All I wanted was you

----------------- Acordes -----------------
