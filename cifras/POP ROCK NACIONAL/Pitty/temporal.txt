Pitty - Temporal

  B    A               B    A
Chega simples como um temporal
 B   A      B     A
Parecia que ia durar
 B       A       B        A
Tantas placas e tantos sinais
 B      A              B  A
Já não sei por onde caminhar

G
E quando olhei no espelho
                      B
Eu vi meu rosto e já não reconheci
G
E então vi minha história
                      F#
Tão clara em cada marca que tava ali

      A       Em
Se o tempo hoje vai depressa
 A         Em
Não tá em minhas mãos

 A      Em
Cada minuto me interessa
 A       Em
Me resolvendo ou não

  F           G              A
Quero uma fermata que possa fazer
F        G         A
Agora o tempo me obedecer
     F                  G
E só então eu deixo os medos e as armas

  B    A               B    A
Chega simples como um temporal
 B   A      B     A
Parecia que ia durar
 B       A       B        A
Tantas placas e tantos sinais
 B      A              B  A
Já não sei por onde caminhar

G
E quando olhei no espelho
                      B
Eu vi meu rosto e já não reconheci
G
E então vi minha história
                      F#
Tão clara em cada marca que tava ali

      A       Em           A
Se o tempo hoje vai depressa
           Em
Não tá em minhas mãos
 A      Em             A
Cada minuto me interessa
         Em
Me resolvendo ou não

  F             G            A
Quero uma fermata que possa fazer
F        G         A
Agora o tempo me obedecer
     F                  G
E só então eu deixo os medos e as armas

F            G
Eu deixo os medos e as armas
F            G                 B
Eu deixo os medos e as armas pra trás
B
As armas pra trás
B
As armas pra trás

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
