Pitty - De Você

(intro é a mesma da 1ª estrofe)

E |---------------------------------|
B |---------------------------------|
G |---------------------------------| paralelo tem ( G|--1--11-1)
D |---------------------------------|
A |------4--------------------------|
E |--2-2---2-5-5-4-5----------------|

(1ª estrofe - pal mute - abafando as cordas)

Esse vidro fechado
e a grade no portão  (G --1-1)
Suposta segurança
Mas não são proteção

E |--------------------------------|
B |--------------------------------|
G |--------------------------------|
D |--------------------------------|
A |--------6-----------------------|
E |--4-4---- 4-7-7-6-7-------------|


E quando o caos chegar
        A5         B5  C5
Nenhum muro vai te guardar

F#5
De você

E |---------------------------------|
B |---------------------------------|
G |---------------------------------|
D |---------------------------------|
A |--------4------------------------|
E |--2-2----5-5-5-4-5---------------|

Protótipo imperfeito
Tão cheio de rancor  (G --1-1)
É fácil dar defeito
É só lhe dar poder

E |--------------------------------|
B |--------------------------------|
G |--------------------------------|
D |--------------------------------|
A |--------6-----------------------|
E |--4-4----7-7-7-6-7--------------|

E quando o caos chegar
        A5      B5 C5
Nenhum muro vai te guardar

F#5
De você

(solo)
E |----------------------------------------------
B |----------------------------------------------
G |-9~~9~~9~~-7h8-7-5h6-5h7--3~~-2~~3-2--
D |----------------------------------------------
A |----------------------------------------------
E |----------------------------------------------

E |----------------------------------------------
B |-15-~~~~~~14-15~~~~-17~~~~--------------
G |----------------------------------------------
D |----------------------------------------------
A |----------------------------------------------
E |----------------------------------------------

E |---------------------------------|
B |---------------------------------|
G |---------------------------------|
D |---------------------------------|
A |--------4------------------------|
E |--2-2----5-5-5-4-5---------------|

Se tornam prisioneiros
Das posses ao redor
Olhando por entre as grades
O que a vida podia ser

E |--------------------------------|
B |--------------------------------|
G |--------------------------------|
D |--------------------------------|
A |--------6-----------------------|
E |--4-4----7-7-7-6-7--------------|

Mas quando o caos chegar
       A5       B5  C5
Nenhum muro vai te guardar

F#5
De você

F#5          F5   E5
E é com mão aberta
F#5              G5       A5
Que se tem cada vez mais
F#5           F5   E5
A usura que te move
F#5           G5        A5
Só vai te puxar para trás

( G5 A5 G5 A5 )

----------------- Acordes -----------------
A = X 0 2 2 2 0
A5 = X 0 2 2 X X
B = X 2 4 4 4 2
B5 = X 2 4 4 X X
C5 = X 3 5 5 X X
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E5 = 0 2 2 X X X
F#5 = 2 4 4 X X X
F5 = 1 3 3 X X X
G = 3 2 0 0 0 3
G5 = 3 5 5 X X X
