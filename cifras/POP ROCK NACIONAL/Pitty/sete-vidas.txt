Pitty - Sete Vidas

 C#m   E        F#    A
Só nos últimos cinco meses
C#m       E        F#    A
Eu já morri umas quatro vezes
C#m       E       F#
Ainda me restam três vidas
  A     C#m  E  F#  A
Pra gastar

C#m               Am
Era um mar vermelho
                                C#m
Me arrastando do quarto pro banheiro
Pupila congelada
 Am
Já não sabia mais de nada

   C#m
É besta sim esse quase morrer
    Am
Desconcertante perceber

        C#m
Que as coisas são e tudo floresce
   Am
A despeito de nós

 E        F#
Pálido, doente
    G         F#
Rendido, decadente
 E            F#
Viver parece mesmo
 G             F#
Coisa de insistente

      E
A postura é combativa
       F#
Ainda tô aqui viva
    G
Um pouco mais triste
     F#
Mas muito mais forte
E               F#
E agora que eu voltei
       A           F
Quero ver me aguentar!

 C#m   E        F#    A
Só nos últimos cinco meses
C#m       E        F#    A
Eu já morri umas quatro vezes
C#m       E       F#
Ainda me restam três vidas
  A     C#m  E  F#  A
Pra gastar

C#m
A caixa de sombra se abriu
 Am                          C#m
Foi um maremoto atrás do outro
              Am
Ferro na jugular
Tirando tudo do lugar

    C#m
Se coisa ruim faz a gente crescer
   Am
E todo esse clichê
        C#m
Já nem caibo mais na casa
     Am
Não caibo mais aqui

 E        F#
Pálido, doente
    G         F#
Rendido, decadente
 E            F#
Viver parece mesmo
 G             F#
Coisa de insistente

      E
A postura é combativa
       F#
Ainda tô aqui viva
     G
Um pouco mais triste
     F#
Mas muito mais forte
E                  F#
E agora que eu voltei
       A           F
Quero ver me aguentar!

 C#m   E        F#    A
Só nos últimos cinco meses
C#m       E        F#    A
Eu já morri umas quatro vezes
C#m       E       F#
Ainda me restam três vidas
  A     C#m  E  F#  A
Pra gastar

  C#m   E     F#    A
Pra gastar, pra gastar
Só três vidas pra gastar
  A     C#m
Pra gastar

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
