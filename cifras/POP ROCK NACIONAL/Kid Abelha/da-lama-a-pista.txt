Kid Abelha - Da Lama a Pista

Intro:
E|-------------------------------------------
B|-------5-------5-7-----------5-------5-7---
G|---5-7-----5-7-----7-----5-7-----5-7-------
D|-7-------7-------------7-------7-----------
A|-------------------------------------------
E|-------------------------------------------

Am                D7
hoje eu não tou grande coisa
Am            D7
você vai me abandonar
        Dm
pela cidade
     Em              Am D7
vou ter que me apaixonar

Dm           Em
ninguém ama só um
Dm             Em
amar ninguém é o fim

Dm                 Em
vivo morrendo de amor
         E
e quero férias de mim

Am         D7
da lama à pista
Am        D7
a vida é besta
Dm            Em     Am D7
e eu tive um dia de cão
Am        D7
da lama à pista
Am      D7
o q me resta
Dm        Em     Am D7
é a festa da solidão

a vida é boa, a vida é bela
manda você de presente
e de repente
inventa outra vida urgente

mesmo vestida
estou sempre nua
sou de nenhuma pessoa
a vida é bela, a vida é boa

da lama à pista
a vida é besta
e eu tive um dia de cão
da lama à pista
Am      D7
o q me resta
Bb         A      G  E
é a festa da solidão

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
