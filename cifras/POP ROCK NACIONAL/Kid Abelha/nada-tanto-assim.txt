Kid Abelha - Nada Tanto Assim

Solo:

E|-------------------------------------------------7-6-7-----------------6-7-7-|
B|---------------------------------------------7-9-------7-9---------7-9-------|
G|-----------------------------------------------------------8---8-9-----------|
D|-9-9-9-7---7---7-9-9-9-7---7---7---9-8---6-----------------------------------|
A|---------9---9-----------9---9---7-----9-------------------------------------|
E|-----------------------------------------------------------------------------|

G#m                                 E
Só tenho tempo pras manchetes no metrô
G#m
E o que acontece na novela
          E
Alguém me conta no corredor
G#m                                   E
Escolho os filmes que eu não vejo no elevador
G#m
Pelas estrelas que eu encontro
   E
Na crítica do leitor


B
Eu tenho pressa
E tanta coisa me interessa
    A            E
Mas nada tanto assim

Só me concentro em apostilas
Coisa tão normal
Leio os roteiros de viagem
Enquanto rola o comercial

Conheço quase o mundo inteiro por cartão postal
Eu sei de quase tudo um pouco e quase tudo mal

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
E = 0 2 2 1 0 0
G#m = 4 6 6 4 4 4
