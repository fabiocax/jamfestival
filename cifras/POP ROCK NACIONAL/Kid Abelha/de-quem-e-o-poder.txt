Kid Abelha - De Quem É o Poder

(George Israel - Nilo Romero - Cazuza)


Intr.: D4


(D  D4)
De quem é?  Uuuuu
De quem é?
De quem é o poder?
G
Quem manda na minha vida?
   D     C/D
De quem é?
   D     C/D
De quem é?
G                 A
Uns dizem que ele é de Deus
G                 A
Outros do guarda da esquina
G                 A
Uns dizem que é do presidente

G                 A       D
E outros que vem mais de cima
   D    C/D
De quem é?
   D    C/D
De quem é?
G                      (D  D4)
Quem inventou essa tara?
G                 A
Uns dizem que ele é do povo
G          A
E sai pra trabalhar
G                 A
Outros que é dos muito loucos
G                  A
Que não tem contas a prestar
D                    G
Me dê o poder eu te mostro
D                   G
O mais inteiro dos sonhos
D                      Em
Porque as verdades da vida
G                    A   (D  C/D)    Solo
São sempre ditas na cama

G
É do ativo ou do passivo?
    D   C/D
De quem é?
    D   C/D
De quem é?
G             A
Às vezes você me domina
G               A
Dizendo que eu sou o teu dono
G             A
Às vezes você me dá nojo

    G               A  (D  D4)
Seguindo feliz o rebanho
G                      (D  D4)
Onde vai dar tudo isso?
G               A
Prender alguém ou ser preso?
G          A
Quem é o mais infeliz?
G                 A
Eu dando ordem o dia inteiro?
    G            A
E você que nem sabe o que diz?

D                    G
Me dê o poder eu te mostro
D                   G
O mais inteiro dos sonhos
D                      Em
Porque as verdades da vida
G                    A
São sempre ditas na cama...
(D G D G D G)

----------------- Acordes -----------------
A = X 0 2 2 2 0
C/D = X X 0 0 1 0
D = X X 0 2 3 2
D4 = X X 0 2 3 3
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
