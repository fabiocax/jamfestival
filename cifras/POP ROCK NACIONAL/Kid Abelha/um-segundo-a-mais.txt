Kid Abelha - Um Segundo a Mais

George Israel / Paula Toller

Gm
O sexo que fazemos
     Dm
é profissional
      Gm
planejado, programado
         Dm       Dm7
para um grande final

Um segundo a mais
é o que deve durar
o olhar que te permite
iniciar a sedução

Um segundo a mais
é o sim, é o começo
das surpresas reservadas
combinadas com você


      Gm      F
Um segundo a mais
      Gm      F
Um segundo a mais
      Gm      F  Dm
Um segundo a mais

   Gm
Delícias previsíveis
      G#
garantias manuais
      Cm                F
um degrau acima dos prazeres normais

----------------- Acordes -----------------
Cm = X 3 5 5 4 3
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
F = 1 3 3 2 1 1
G# = 4 3 1 1 1 4
Gm = 3 5 5 3 3 3
