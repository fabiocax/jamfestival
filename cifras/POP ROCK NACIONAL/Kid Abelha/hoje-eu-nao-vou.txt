Kid Abelha - Hoje Eu Não Vou

Beni/Leoni /Paula Toller

D                G
Quando eu acordo ainda está escuro
D                G
eu digo hoje eu não vou
D                G
Mas minha mãe já vem bater na porta
F#m                G
Eu digo hoje eu não vou

Sonambulismo no café eu não entendo
o dia já começou
Quando eu percebo tô na aula
de desenho, o dia já começou

A
Aquela voz nos meus ouvidos
em geometria eu tô perdido
Bm
no quadro negro, ouço o barulho

   C             A
da unha do professor
Bm
me dá nos nervos eu só consigo
C               A
fugir pro corredor
G      F#m  Em           A
Nunca never hoje eu não vou

Eu não consigo concentrar por um segundo
Só sei que aqui eu não tou
Podia estar em qualquer lugar do mundo
Só sei que aqui eu não tou

Os meus colegas favoritos
tocam fogo na cortina
as minhas notas, todas vermelhas
na mesa do diretor
me dá nos nervos eu só consigo
fugir pro corredor

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
