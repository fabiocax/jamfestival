Kid Abelha - Lolita

George Israel / Bruno Fortunato / Paula Toller

Bb              D#            Bb
Toda menina já foi Lolita na vida
Gm                 C#              G#
Não por amor, mas por desejo de poder
Bb              D#            Bb
praticar a crueldade e se satisfazer
Gm          C#              G#
Sexo fácil, companhia e prazer

Cm                Cm7+
É mais fácil seduzir
D#               F7/9
um homem-lobo perverso
Cm           Cm7+
do que destruir
D
um coração incerto

Gm               D#
Você pensou que me ensinava

Gm              D#
Bebia na fonte da juventude
Cm                Gm
Mas era eu que bancava o jogo
Cm                  D
Mas era eu que possuía seu corpo

Preencho meu tempo
com a sua vontade
Preencho seu tempo
com a minha idade

Você pensou que se divertia
sabia como me dominar
mas era eu que sabia fingir
mas era eu que te fazia sonhar

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C# = X 4 6 6 6 4
Cm = X 3 5 5 4 3
Cm7+ = X 3 5 4 4 3
D = X X 0 2 3 2
D# = X 6 5 3 4 3
F7/9 = X X 3 2 4 3
G# = 4 3 1 1 1 4
Gm = 3 5 5 3 3 3
