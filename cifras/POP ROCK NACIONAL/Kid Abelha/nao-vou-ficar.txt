Kid Abelha - Não Vou Ficar

Introdução: (G7)

   G7                              C7
Há muito tempo eu vivi calada mas agora resolvi falar
   G7                                    C7
Chegou a hora, tem que ser agora e com você não posso
mais ficar
não vou ficar não
G7
 não, não
C7
 Não posso mais ficar não não não
G7
 não não
C7                        G7
 Não posso mais ficar não

  G7                                 C7
Toda a verdade deve ser falada e não vale nada se enganar
    G7
Não tem mais jeito e tudo está desfeito

        C7
e com você não posso mais ficar
não vou ficar não
G7
 não, não
C7
 Não posso mais ficar não não não
G7
 não não
C7                G7
 Não posso mais ficar não

Am        Bm
 Pensando bem
Am          Bm
 não vale a pena
Am        Bm
 ficar tentando em vão
  Em             A7       D7
O nosso amor não tem mais condição
Não não não não não não não

    G7
Por isso resolvi agora
              C7
lhe deixar de fora do meu coração
G7
Com você não dá mais certo
          C7
e ficar sozinha é minha solução
é solução sim
G7
 Ah, ah
C7
 Não tem mais solução não não não
G7
 Não, não
C7
 Não tem mais solução não

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C7 = X 3 2 3 1 X
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
