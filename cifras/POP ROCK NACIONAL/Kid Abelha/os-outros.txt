Kid Abelha - Os Outros

[Intro]  E7+  B7+  F#/A#  E/G#

 B          B7+
Já conheci muita gente
 E           G#m  C#m
Gostei de alguns garotos
 E             B7+
Mas depois de você
C#m        B7+
Os outros são os outros

Ninguém pode acreditar
Na gente separado
Eu tenho mil amigos, mas você foi
O meu melhor namorado

    E7+         Em7
Procuro evitar comparações
        B/D#
Entre flores e declarações
    C#m   F#      B7+
Eu tento te esquecer

   E7+           Em7
A minha vida continua
       B7+            G#
Mas é certo que eu seria sempre sua
      G#m  A7+     F#
Quem pode me entender

 E           B7+
Depois de você
   F#/A#      E/G#      B
Os outros são os outros e só

São tantas noites em restaurantes
Amores sem ciúmes
Eu sei bem mais do que antes
Sobre mãos, bocas e perfume

Eu não consigo achar normal
Meninas do seu lado
Eu sei que não merecem
Mais que um cinema
Com meu melhor namorado

----------------- Acordes -----------------
A7+ = X 0 2 1 2 0
B = X 2 4 4 4 2
B/D# = X 6 X 4 7 7
B7+ = X 2 4 3 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E7+ = X X 2 4 4 4
Em7 = 0 2 2 0 3 0
F# = 2 4 4 3 2 2
F#/A# = 6 X 4 6 7 X
G# = 4 3 1 1 1 4
G#m = 4 6 6 4 4 4
