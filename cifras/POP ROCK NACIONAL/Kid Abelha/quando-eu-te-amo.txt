Kid Abelha - Quando Eu Te Amo

George Israel / Paula Toller

[Intro:] D F#m C Bm

             A
depois q te vi
                F#m
não sei mais olhar
         D
só vejo vc
           F#m
em todo lugar
             C
depois q te vi
                 Em
não sei mais beijar
          D
só beijo vc
           E
em todo lugar


líquidos vertendo
músculos pensando
fluindo partindo
mão e contramão
densas e viscosas
doces e salgadas
gotas manchas poças
de sangue e suor

  A    G      F
quando eu te amo
    F          A G F G
me sinto aprendiz
  A    G      F
quando eu te amo
 G      A
fico feliz...

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
