Titãs - Pra Dizer Adeus

Introdução: G

   D       C      D     C
Você apareceu do nada
 G        D      C     G
E você mexeu demais comigo
 G        D      C     G
Não quero ser só mais um amigo
   D             C   D    C
Você nunca me viu sozinho
     G        D      C     G
E você nunca me ouviu chorar
  G        D      C     G    C D
Não dá pra imaginar quando

Refrão:
   G       B7       C
É cedo ou tarde demais
       G    D           C     G
Pra dizer adeus, pra dizer jamais
   G       B7       C
É cedo ou tarde demais

       G    D           C     G
Pra dizer adeus, pra dizer jamais

    D       C         D     C
Às vezes fico assim pensando
 G        D      C     G
Essa distância é tão ruim
 G        D      C     G
Por que você não vem pra mim
   D             C   D    C
Eu já fiquei tão mal sozinho
     G        D      C     G
Eu já tentei, eu quis chamar
  G        D      C     G    C D
Não dá pra imaginar quando

Refrão:
   G       B7       C
É cedo ou tarde demais
       G    D           C     G
Pra dizer adeus, pra dizer jamais
   G       B7       C
É cedo ou tarde demais
       G    D           C     G
Pra dizer adeus, pra dizer jamais

----------------- Acordes -----------------
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
