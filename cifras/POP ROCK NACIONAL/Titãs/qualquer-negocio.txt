Titãs - Qualquer Negócio

Intro: E A (4x)

E          A                    E
Está ao alcance das mãos-experimente

Como é antigo o passado recente
           A              E
Dentro de mais alguns instantes
                A             E
De novo tudo igual ao que era antes

Repete Intro 2x

B                 A                  B
Sem conservantes, cem por cento natural
       A                  B
Cada um é seu próprio animal
              A                    B
Troque a armação e leve grátis as lentes
                    A                  E
O que os olhos não vêem o coração não sente

           A              E
O sexo do homem que é mulher
     A                     E
Até mesmo o que você não quer
            A                   E
O espírito santo está na sua tevê
        A                          E
O pior cego é aquele que não quer ver

Repete intro 2x

B          A             B
Compra-se tudo, tudo se vende
         A                      B
É conversando que a gente se entende
           A                    B
Você pode ser a namorada do Brasil
               A                    E
De novo pra você o que você nunca viu

( E D ) (2X)

E           D      E               D
E antes de ir, doe aqui o seu coração
E             D                 E             D
Guarde o que tem agora para a próxima encarnação

(B B B B A A A A)
Morreram Sérgio de Britto Álvares Affonso e Charles de Souza Gavin
Desaparecido Paulo Roberto de Souza Miklos, desde sábado de manhã

INTRO 2X

E           A                   E
Dentro de apenas mais alguns segundos
       A                      E
Pra você, agora sim, um novo mundo
               A                 E
A estátua da Nossa Senhora que chora
          A                     B
Estamos abertos vinte e quatro horas
        A                 B
Para crianças, jovens e adultos
              A                B
A nossa mais nova linha de produtos
                    A             B
Os cavaleiros do zodíaco estão aqui
              A                E
O sorriso do homem que não sorri
                 A               E
Divirta-se, você é o que você come
             A                    E
Aqui é o paraíso para mulheres e homens
             A            E
E antes que mais um dia acabe
               A                  B
De novo pra você tudo como já se sabe
            A                         B
Dinheiro é bom, dinheiro é bom até assim
               A                   B
Ainda é muito bom mesmo quando é ruim
                A                        B
Se você não provou, um dia ainda vai provar
           A                    B
É fácil dizer, difícil é acreditar

(E D) (2X)

E              D                 E              D
E quem é que quer ver as coisas como realmente são?
 E            D                       E
Sempre tem alguma farmácia que fica aberta de
     D
plantão

(B B B B A A A A)
Internados Joaquim Claudio Corrêia de Mello Junior e José Fernando Gomes dos Reis
Detidos Antonio Carlos Liberalli Bellotto e Marcelo
Fromer, desde o último dia seis
E
  Libertado Arnaldo Augusto Nora Antunes Filho, poeta e compositor
Que havia sido seqüestrado ao sair de um baile funk do morro chapéu
mangueira na zona sul no Rio de Janeiro

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
