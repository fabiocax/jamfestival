Titãs - Tudo o Que Você Quiser

Esse é um solinho que o toni faz no comeco
da música com muita distorcão.


E|-----5-5----------10-10---9-9-----------
B|-----------5-5--------------------------
G|----------------------------------------
D|----------------------------------------
A|----------------------------------------
E|----------------------------------------

E|-----5-5----------10-10---12-12---------
B|-----------5-5--------------------------
G|----------------------------------------
D|----------------------------------------
A|----------------------------------------
E|----------------------------------------




Intro: (A E D Db)


A    E     D   Db
Pode escolher
A    E     D   Db
Pode escolher por mim
Pode resolver
Pode resolver por mim

A        E
O que fazer
D        Db       A
O que falar e quando ouvir
A        E
O que dizer
D           Db    A
Com quem andar e aonde ir

O que fazer
O que falar e quando ouvir
O que dizer
D            Db   D    E
Com quem andar e aonde ir

A        Db       D
Tudo que você quiser
Tudo que você disser
A        Db       D   A
Tudo que você quiser
A        Db       D
Tudo que você quiser
Tudo que você disser
A        Db       D   E
Tudo que você quiser

A    E      D    Db
Pode responder
A    E      D    Db
Pode responder por mim
Pode decidir
Pode decidir por mim

A        E
O que fazer
D        Db       A
O que falar e quando ouvir
A        E
O que dizer
D           Db    A
Com quem andar e aonde ir

O que fazer
O que falar e quando ouvir
O que dizer
D            Db   D    E
Com quem andar e aonde ir

A        Db       D
Tudo que você quiser
Tudo que você disser
A        Db       D   A
Tudo que você quiser
A        Db       D
Tudo que você quiser
Tudo que você disser
A        Db       D   E
Tudo que você quiser

(E Ab A B Db D) - 1ª Guitarra
(E7 D Db B Db A) - 2ª Guitarra
(A Ab A B Db D) - Final 2ª Guitarra

         Repete Intro: (A E D Db)

A        E
O que fazer
D        Db       A
O que falar e quando ouvir
A        E
O que dizer
D           Db    A
Com quem andar e aonde ir

O que fazer
O que falar e quando ouvir
O que dizer
D            Db   D    E
Com quem andar e aonde ir

A        Db       D
Tudo que você quiser
Tudo que você disser
A        Db       D   A
Tudo que você quiser
A        Db       D
Tudo que você quiser
Tudo que você disser
A        Db       D   E  A
Tudo que você quiser

----------------- Acordes -----------------
A = X 0 2 2 2 0
Ab = 4 3 1 1 1 4
B = X 2 4 4 4 2
D = X X 0 2 3 2
Db = X 4 6 6 6 4
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
