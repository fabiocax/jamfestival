Titãs - Daqui Prá Lá

F#           C#     B          F#
O futuro é hoje, cabe na mão,
F#           C#     B          F#
O futuro é hoje, cabe na mão,

F#        B                Bbm               B
Era um pacato cidadão, sem documento,
F#        B                Bbm               B                    (2 vezes)
Não tinha nome, profissão, não teve tempo,

D#m          D#m7+          B              C#
Mas certo dia deu-se um caso e ele embarcou num disco
D#m          D#m7+          B              C#
E foi levado pra bem longe do asterisco em que vivemos

F#        B                Bbm               B
Ele partiu e não voltou e não voltou porque não quis
F#        B                Bbm               B                                      2vezes
Quero dizer ficou por lá, já que por la se é mais feliz

D#m          D#m7+          B              C#
E um espaçograma ele enviou, Pra quem quisesse compreender

D#m          D#m7+          B              C#
Mas ninguém nunca decifrou o que ele nos mandou dizer

F#           C#     B          F#
O futuro é hoje, cabe na mão,
F#           C#     B          F#
O futuro é hoje, cabe na mão,

F#        B                Bbm               B
Era um pacato cidadão, sem documento,
F#        B                Bbm               B
Não tinha nome, profissão, não teve tempo,

D#m
VietVistaVisão
Bbm
Para Azar de quem não sabe
e Não crê
D#m
Que sempre pode a sorte escolher
Bbm
E Enterrar uma estrela no chão

D#m
VietVistaVisão
Bbm
Terramarear Atenção
D#m
Fica a morte por medida
Bbm              F#
Fica a vida por prisão

----------------- Acordes -----------------
B = X 2 4 4 4 2
Bbm = X 1 3 3 2 1
C# = X 4 6 6 6 4
D#m = X X 1 3 4 2
D#m7+ = X 6 8 7 7 6
F# = 2 4 4 3 2 2
