Titãs - Babi Índio

Intro: (A) D C (A)

(A)
 Babi índio enjoy selva coca-cola
 Babi índio enjoy selva coca-cola
 Babi índio enjoy selva coca-cola
 Babi índio enjoy selva coca-cola

 D                                 E   D E
 Pode ser que eu vá viajar nesse navio
 A
 Não sei, não sei, não sei
 D                                   A
 Pode ser, pode ser, poder ser, não sei
 A
 Cenas de terror e tensão,
 Fúria na terra, ira no céu
 B         C          D  C D
 Televisão o futuro será

(A)
 Babi índio enjoy selva coca-cola

 Babi índio enjoy selva coca-cola
 D                                   E   D E
 Acho que eu já vou embarcar nesse navio
 A
 Bagdá, província de Kales, Boca do tigre
           B      C          D  C D
 Beira do abismo, o futuro será

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
