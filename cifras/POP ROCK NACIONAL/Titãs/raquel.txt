Titãs - Raquel

D
Estou pensando em você
D7+
O tempo não passa
D7           G
Aqui neste quarto de hotel
Gm               D
Eu sinto a tua falta
D
Estou pensando em nós dois
D7+
Aqui neste quarto
D7           G
E mais que ouvir tua voz
Gm                    D    D C#m Bm
Queria ter te ao meu lado
Bm
Aqui já passa das três
E7
E o dia está lindo
Em7                             G
Mas nem dez mil dias assim vão matar

      A A#        D
A saudade que eu sinto
D
Estou pensando em você
D
O tempo não passa
D
Aqui neste quarto de hotel
D               D           D D7+ D7 G Gm D
Eu sinto a tua falta

D4 D

D
Estou pensando em nós dois
D7+
Aqui eu comigo
D7              G
Nem as quatro paredes do quarto do hotel
Gm                D   D   C#m Bm
Escutam o que eu digo
Bm
Te ligo mais tarde outra vez
E7
Te conto meu dia ah
Em7                              G
Parece tão perto a tua voz mas não tão
        A A#       D
Perto quanto eu queria
D
Estou pensando em você
D
O tempo não passa
D
Aqui neste quarto de hotel
D               D           Em D  D C#m Bm
Eu sinto a tua falta

Bm
Te ligo mais tarde outra vez
E7                        Em7 G A A# D
Te conto meu dia ah

D
Estou pensando em você
D7+
Não sei se preciso
D7              G
Mais dos teus olhos cor de mel
Gm            D       Em D
Ou do teu sorriso

----------------- Acordes -----------------
A = X 0 2 2 2 0
A# = X 1 3 3 3 1
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
D4 = X X 0 2 3 3
D7 = X X 0 2 1 2
D7+ = X X 0 2 2 2
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
