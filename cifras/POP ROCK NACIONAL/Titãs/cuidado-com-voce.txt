Titãs - Cuidado Com Você

intr.: A Em 4x

       (Use este riff para a intro e a base)
        E|------------------------------------------------
        B|------------------------------------------------
        G|------------------------------------------------
        D|------------------------------------------------
        A|------------------------------------------------
        E|-(5--5--8--5--0--0)----5--5--8--7--5------------ 
                (3X)


         (Use este riff para o refrão)
        E|------------------------------------------------
        B|------------------------------------------------
        G|------------------------------------------------
        D|------------------------------------------------
        A|------------------------------------------------
        E|-(5--5--8--5--0--0)----------------------------- 

A Em
 Estão olhando pra você

 Estao pensando em você
 A chuva é pra molhar você
 A guerra é pra matar você
 A reza é pra salvar você
 Cachorros latem pra você
 Estão pensando em você
 Cuidado com você
 Cuidado!
 Cuidado!
 Com você!  A Em

A Em
 Quem tem dinheiro te roubou
 Quem tem amante te traiu
 Se alguém partiu, te abandonou
 Se ela é puta, te pariu
 Se não tem pai o filho é seu
 Se alguém morreu a culpa é sua
 O prazer é todo seu
 Se você entra a casa é sua
 Cuidado!
 Cuidado!
 Com você!  A Em

 (C E C B)
 C                        E
 Olha para os outros e se vê
 C                        E
 Todos são Espelhos pra você
 C                   B
 Só para você a vida corre
 C                         B
 Quando você dorme o mundo morre
 C
 Até você morrer

A Em
 Até
 Até
 Você morrer!
 Cuidado!
 Cuidado!
 Com você!   A

 Cuidado!   A

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C = X 3 2 0 1 0
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
