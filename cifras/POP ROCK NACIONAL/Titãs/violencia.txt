Titãs - Violência

Intro: (A F# A B F#)

A
O movimento começou, o lixo fede nas calçadas.
     F#
Todo mundo circulando, as avenidas congestionadas.
   A
O dia terminou, a violência continua.
     F#
Todo mundo provocando todo mundo nas ruas.
     D
A violência está em todo lugar.
           B
Não é por causa do álcool,
Nem é por causa das drogas.
D
A violência é nossa vizinha,
      B
Não é só por culpa sua,
Nem é só por culpa minha.


A                         E F#
Violência gera violência.


* O restante segue a mesma sequencia acima.


Violência doméstica, violência cotidiana,
São gemidos de dor, todo mundo se engana...
Você não tem o que fazer, saia pra rua,
Pra quebrar minha cabeça ou pra que quebrem a sua.
Violência gera violência.
Com os amigos que tenho não preciso inimigos.
Aí fora ninguém fala comigo.
Será que tudo está podre, será que todos estão vazios?
Não existe razão, nem existem motivos.
Não adianta suplicar porque ninguém responde,
Não adianta implorar, todo mundo se esconde.
É difícil acreditar que somos nós os culpados,
É mais fácil culpar deus ou então o diabo.

*``O crime é venerado e posto em uso por toda terra,
De um pólo a outro se imolam vidas humanas.
No reino de Zópito os pais degolam os próprios filhos,
Seja qual for o sexo, desde que sua cara não lhes agrade.
Os coreanos incham o corpo da vítima a custa de vinagre
E depois de estar assim inchado, matam-no a pauladas.
Os irmãos Morávios mandavam matar com cócegas''

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
