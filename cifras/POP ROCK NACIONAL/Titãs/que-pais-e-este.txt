Titãs - Que País É Este

(E5 C5 D5)
Nas favelas, no senado
Sujeira pra todo lado
Ninguém respeita a Constituição
Mas todos acreditam no futuro da nação
Que país é esse
Que país é esse
Que país é esse

(RIFF) 1

E|---9---9---9---9-----7---5---5---5---5-----7----|
B|-7---7---7---7---7\5---7---7---7---7---7/9------|
G|------------------------------------------------|
D|------------------------------------------------|
A|------------------------------------------------|
E|------------------------------------------------|

(E5 C5 D5)
No Amazonas, no Araguaia iá, iá,
Na Baixada Fluminense

Mato Grosso, nas Gerais e no
Nordeste tudo em paz
Na morte eu descanso, mas o
sangue anda solto
Manchando papéis, documentos fiéis
Ao descanso do patrão
Que país é esse
Que país é esse
Que país é esse
Que país é esse

Repete: (RIFF) 1

(E5 C5 D5)
Terceiro mundo, se for
Piada no exterior
Mas o Brasil vai ficar rico
Vamos faturar um milhão
Quando vendemos todas as almas
Dos nossos índios em um leilão
Que país é esse
Que país é esse
Que país é esse

----------------- Acordes -----------------
C5 = X 3 5 5 X X
D5 = X 5 7 7 X X
E5 = 0 2 2 X X X
