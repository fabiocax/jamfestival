Titãs - Medo

(G G7)
Precisa perder o medo do sexo
Precisa perder o medo da morte
Precisa perder o medo da música
Precisa perder o medo da música

    Bb       F
O que se vê não se via
Bb          F
O que se crê não se cria

(G G7)
Precisa perder o medo da musa
Precisa perder o medo da ciência
Precisa perder o medo da perda
Da consciência

    Bb       F
O que se vê não se via
Bb          F
O que se crê não se cria


(G G7)
Precisa perder o medo de mim
Precisa perder o medo de mim
Precisa perder o medo da música
Precisa perder o medo da música

    Bb       F
O que se vê não se via
Bb          F
O que se crê não se cria

(Bb F)
Medo medo medo medo
O que se crê não se cria

(G G7)
Precisa perder o medo da musa
Precisa perder o medo da musa
Precisa perder o medo da música
Precisa perder o medo da música

(Bb F)
Medo medo medo medo
O que se crê não se cria

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
