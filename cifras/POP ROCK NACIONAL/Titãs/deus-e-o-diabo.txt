Titãs - Deus e o Diabo

Introdução: Em7

   Em
   Deus está debaixo da mesa
   O diabo está atrás do armário
   Deus está atrás da porta
   O diabo está no meio da sala
   O que há de errado com meu coração?

                      Em7
   O que há de errado?

   Em
   Deus está lendo jornal
   O diabo está dançando
   O diabo está fazendo o jantar
   Deus está escrevendo uma carta
   O que há de errado com meu coração?

                      C7+ D C7+ D
   O que há de errado?

  C7+
   Deus está sonhando
  D
   O diabo está fazendo discurso
  C7+
   Deus está lavando os pratos
  D                         Em7
   O diabo está tocando piano

  (Em7)
   Deus é o teto da casa
   O diabo é a porta dos fundos
   O diabo é o chão da cozinha
   Deus é o vão da entrada
   O que há de errado com meu coração?

                     Em7
   O que há de errado?

  C7+
   Deus está debaixo da mesa
  D
   O diabo está atrás do armário
  C7+
   Deus está atrás da porta
  D
   O diabo está no meio da sala

  (Em7)
   O que há de errado com meu coração?
                      Em7
   O que há de errado?

   Em
   Deus está lendo jornal
   O diabo está dançando
   O diabo está fazendo o jantar
   Deus está escrevendo uma carta
   O que há de errado com meu coração?
   O que há de errado?
   O que há de errado com meu coração?
   O que há de errado com meu coração?

                     Em7
   O que há de errado?

----------------- Acordes -----------------
C7+ = X 3 2 0 0 X
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
