Titãs - Não É Por Não Falar

Intro: (B5 E5) 4 VEZES

B5               E5
Não é por não falar
B5        E5
em felicidade
B5
Que eu não goste
E5
de felicidade
B5                E5
Não é que eu não goste
B5        E5
de felicidade
B5
É por não falar
E5
em felicidade
F#5    E5
é por falar
F#5      E5
infelicidade

F#5              E5          F#5  E5
Que eu não gosto de falar
F#5      E5
em felicidade

Solo (B5 E5 B5 C#5 B5)
Reptete tudo novamente

----------------- Acordes -----------------
B5 = X 2 4 4 X X
C#5 = X 4 6 6 X X
E5 = 0 2 2 X X X
F#5 = 2 4 4 X X X
