Catedral - Sem TV Eu Te Enxergo Muito Mais

     D
A minha TV estragou
      A               A7         Bm
E agora o que eu vou fazer se não sei nem falar com você?
        A
Se não sei nem mais o que dizer
      G                    Em
Como posso pensar sem TV? Já não sei mais olhar pra você
        D                 G           A
E de frente não consigo nem me ver
     D
A minha TV estragou
      A              A7         Bm
E agora o que vou comprar? Qual a roupa que eu posso usar?
        A
Como é que eu vou poder votar?
        G                      Em
Qual a onda que eu vou tirar? Como é que vou poder viver?
        D                   G                  A
Se nunca aprendi a sonhar sem olhar pra TV...



     D          D9      A
Sem TV a gente pode conversar
    G               D          Em       A
Sem TV eu te vejo, eu existo, e não existem marcas
         D            D9     Bm
Nem modelos, nem receios, ilusões e medos
      G                      Em         G                      A            D
Sem TV a nossa vida não é novela    Sem TV eu te enxergo muito mais


     D
A minha TV estragou
      A           A7         Bm
De repente vi tudo mudar, pois existe um mundo ao redor
        A
Que não conseguia enxergar
        G               Em
Sempre olhava para a TV e idealizava você
        D              G                A
Mas você é o inverso da imagem da TV.


     D          D9      A
Sem TV a gente pode conversar
    G               D      Em           A
Sem TV eu te vejo, eu existo, e não existem marcas
         D            D9     Bm
Nem modelos, nem receios, ilusões e medos
      G                      Em         G                      A            D
Sem TV a nossa vida não é novela    Sem TV eu te enxergo muito mais

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D9 = X X 0 2 3 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
