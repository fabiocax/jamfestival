Catedral - Here I miss you now

Composição: Kim, Júlio e Cezar

D
carros vêm e vão
   G            D
às 10 horas da manhã
                         G        D
qual é a direção que me leva até você?
     C   G        D
sinto falta de te ver

D
rumos desiguais
   G           D
te espero sem saber

só na multidão
     G          D
sem resposta de você
      C   G        D
sinto falta de te ver



D                G                D      G
here I miss you, here I miss you, now ow ow
D                G                Em     G
here I miss you, here I miss you, now     (2x) depois de repetir toca

                                               isso D,C,D,F,D,C,D,A9


D                     G
corro sem olhar, são 10 horas e o
D
céu já escureceu
        G            D
e eu não sei o que fazer
      C  G       D
então penso em você
      C  G       D
sinto falta de você

----------------- Acordes -----------------
A9 = X 0 2 2 0 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
