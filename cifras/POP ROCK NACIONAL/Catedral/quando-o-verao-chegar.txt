Catedral - Quando o Verão Chegar

(intro)

E|------2----0h2----2-0---0-----0h2--|----2----0h2----2-0---0-----0h2--|
B|----3---3-------3-----------3------|--3---3-------3-----------3------|
G|----------------------2---2--------|--------------------2---2--------|
D|--0--------------------------------|---------------------------------|
A|-----------------------------------|---------------------------------|
E|-----------------------------------|---------------------------------|


            D      A/C#
Quando o verão chegar
 G                 A             D A/C#
Eu quero ser mais forte do que sou....
    G         A
Sem medo no olhar
   G                 A
Nenhum motivo p'ra chorar
            D      A/C#
Quando o verão chegar
   G             A        D  A/C#
O Sol mais forte aparecerá

  G            A
E vai nos aquecer
   G               A
Viver é preciso viver

          Bm
Por isso vem,
              F#m
Não pare de sonhar
                 A
Com Deus no coração
                   G
Sonhar é mais que realidade
         Bm
Por isso vem
                   F#m
Contigo eu sou mais eu
              A
Preciso de você
                     G
P'ra sempre e sempre ao meu lado

            D      A/C#
Quando o verão chegar
    G            A             D  A/C#
Eu quero transformar tudo em amor
    G            A
Um mundo bem melhor
     G           A         D
P'ra minha filha poder brincar...

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
