Catedral - Viva o Povo Brasileiro

(intrro) Dm F Dm G

Dm F
Todas as crianças estão nas escolas
Dm G
Educação integral planejada
Dm F
Todas as ruas bem sinalizadas
Dm G
E há segurança em cada calçada
Dm F
O povo pode passear sem medo
Dm G
Nossa policia é de confiança!
Dm F
Não falta água, nem luz, nem comida
Dm G
Não há preconceitos, nem intolerância
Dm
Grande celeiro da humanidade, somos a nova Pátria do presente!
Dm
Nossos políticos, nosso orgulho, corrupção é uma palavra ausente!

Dm F Dm G
Mais de mil razões para bater no peito e cantar bem alto o hino nacional
Dm F Dm G
Viva o povo brasileiro! Viva o povo brasileiro!
Dm F
A cesta básica é tão barata
Dm G
E o salário é constitucional, que legal!
Dm F
Não há desmandos, nem apologias
Dm G
E o imposto é tão racional
Dm
Tudo é tão justo, tão democrático
Dm
Tanto respeito chega a emocionar
Dm
Há igualdade nas oportunidades
Dm
Nosso futuro é só comemorar!!

----------------- Acordes -----------------
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
