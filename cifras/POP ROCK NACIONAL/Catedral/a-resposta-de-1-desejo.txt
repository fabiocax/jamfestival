Catedral - A resposta de 1 desejo

(intro) G C G C Am C G

 G                     C9               G
Se um de nós errou sem ter como saber que tudo
             C9                Am
Que podia então viver não foi miragem nem uma
       C            G  C
Ilusão comum, bobagem

 G                    C9                  G
Se um de nós errou em ter que negar tudo que
             C                 Am
Sentia preso no olhar faltou coragem uma
    C          G  C      G
Atitude de verdade,   verdade

                               Am  C
Deixa eu te ver destranque a porta
                             G
Nosso sentimento é o que importa
                         C
Pra você eu dei minha resposta

      Am    C      G
Meu desejo, meu desejo.

                           C
Deixa de mistério e vem sem medo
                            G
Vou te revelar os meus segredos
                      C
Matar a saudade do teu beijo
      Am     C        G
Vem responde, meu desejo.

C  G  C  Am  C  G
se um de nós ...

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
G = 3 2 0 0 0 3
