Catedral - Quem Me Dera

Introdução: C  Am  Dm  G - 2x  C

                      Am                Dm
Quem me dera um mundo livre de todo mal
                    G                 C
Sem os preconceitos que só vão punir
Am           Dm                 G    C
Tudo o que ficou e ainda há de vir.

                   Am                  Dm
Quem me dera que a nossa estrada fosse
                       G                C
Sempre a mesma estrada livre pra seguir
Am             Dm                G
Quem me dera o sol pudesse nos unir.

F       G          Am                Bb
 Queria que você viesse aqui comigo
                   G
 Você nunca foi feliz
                   F
 Vem ver as flores do campo

          C
 Sinta o cheiro do jasmim
           F         G     F
 Olhe pra mim eu te amo
                  G    F
 E Ele te ama também
                        G
 Quero te mostrar a verdade
   C   Am      Dm   G      C
 Jesus,  oh, oh, oh, oh, oh.

                   Am                Dm
Quem me dera só pensar que a bondade
                    G           C
Que hoje vive é verdade que nasceu
Am        Dm                 G   C
No imenso mar, desejo seu e meu.

                   Am                   Dm
Quem me dera encontrar difícil um lugar
                  G             C
Onde todos  são iguais e o arco-íris
Am       Dm               G
Ao amanhecer revela o porque.

    C        Am
Do abrir das flores
      Dm    G
Da amizade Eterna

      C   Am
Da beleza do mar.
      Dm       G
É tão fácil de ver.

F       G          Am               Bb
 Queria que você viesse aqui comigo
                   G
 Você nunca foi feliz
                   F
 Vem ver as flores do campo
          C
 Sinta o cheiro do jasmim
           F         G    F
 Olhe pra mim eu te amo
                  G    F
 E Ele te ama também
                        G
 Quero te mostrar a verdade
   C   Am    Dm G  C
 Jesus,  o,o,o, o, o,

Final (C Am Dm G ...)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
