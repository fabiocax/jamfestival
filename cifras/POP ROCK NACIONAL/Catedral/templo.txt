Catedral - Templo

                 G
Se você olha pra mim
              Am
Se me dá atenção
C               Am
Eu me derreto suave
C            G
Neve no vulcão

G
Se você toca em mim
           Am
Alaúde emoção
C                 Am
Eu me desmancho suave
C           G
Nuvem no avião

G              D
Himalaia himeneu
                   C
Esse homem nu sou eu

D                 G
Olhos de contemplação

G              D
Inca maia pigmeu
                   C
Minha tribo me perdeu
                 D           G
Quando entrei no templo da paixão

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
