Catedral - O Sapo, O Escorpião e A Paixão

Intro: Em   C9   D   Em   C9   D   Em 2x


Parte 1:
E|----3h5-3--------55----3h5-5-3---------------|             
B|---------6-4-4h6---------------6-4-6-33-------|                
G|-----------------------------------------------------|                
D|-----------------------------------------------------|
A|-----------------------------------------------------|
E|-----------------------------------------------------|

E|----3h5-3--------55-----------------------------|
B|---------6-4-4h6---------------------------------|
G|----------------------6426426424642--------|
D|-----------------------------------------------------|
A|-----------------------------------------------------|
E|-----------------------------------------------------|


Parte 2:
E|----------------------------------------||----------------------------------|
B|----------------------------------------||-----------------------------------|
G|----------------------------------------||----------------------------------|
D|---99-7-5-99-----99-7-5-77-----||---99-7-5-99---99-77-55---|
A|---77-5-3-77-----77-5-3-55-----||---77-5-3-77---77-55-33---|
E|----------------------------------------||-----------------------------------|



Em           C9                D     Em
O sapo ouviu de um escorpião
                C9                D     Em
um pedido meigo de grande emoção.
                C9                D     Em
Me ajude não posso atravessar
         C9              D     Em
deste lado para o lado de lá.
                       C9                D     Em
Mas ao mesmo tempo ele relutou.
                 C9            D     Em
Como poderei eu em ti confiar.
                   C9                    D     Em
Como vou saber que não vais deixar
           C9           D          Em
seu veneno em meu coração.
      C9     D     Em    C9        D     Em
A paixão é assim    queremos confiar.
         C9     D       Em    C9     D       Em
Mas nós sabemos que ela pode enganar.


Em           C9                D     Em
O sapo ouviu de um escorpião
                C9                D     Em
um pedido meigo de grande emoção.
                C9                D     Em
Me ajude não posso atravessar
         C9              D     Em
deste lado para o lado de lá.
                       C9                D     Em
Mas ao mesmo tempo ele relutou.
                 C9            D     Em
Como poderei eu em ti confiar.
                   C9                    D     Em
Como vou saber que não vais deixar
           C9           D          Em
seu veneno em meu coração.
      C9     D     Em    C9        D     Em
A paixão é assim    queremos confiar.
         C9     D       Em    C9     D       Em
Mas nós sabemos que ela pode enganar.

----------------- Acordes -----------------
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
