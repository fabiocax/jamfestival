Catedral - Para Todo Mundo

Intro: (Em, G, D, A)

Em            G     D       A  Em, G, D, A
Abaixo toda a nossa armadilha, abaixo toda forma de opressão
Vamos valer a nossa rebeldia, nossa bandeira é o coração
Vamos punir toda desigualdade, lutar pela justiça social
O pão de cada dia para todos, nosso decreto institucional

     Em   G         D  A
Para todo mundo ouvir
Para todo mundo ver,
para todo mundo, para todo mundo viver

vamos unir então as nossas vozes,
mostrar o que queremos sem temer, escolas para todas as crianças
e o pensamento em Deus para você

o não as drogas e a impunidade, sim a todo exemplo de amor
o não a hipocrisia e a maldade,
a guerra, a fome, a dor, a violência e a dor

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
