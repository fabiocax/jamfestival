Catedral - Você É o Meu Amor

C
Você é o meu amor
F                  G                        Am
Minha emoção, brilho do meu olhar
F                      G                   C G Am
Minha inspiração,o meu acreditar
F
Estava escrito era você
G
Destino do meu querer

C
Você o meu amor
F                      G                             Am
Agradeço a Deus, pois pude te encontrar
F                         G                       C G Am
Eu que nem sabia mais como sonhar
F                             G                                             Am
Pendurei minha razão, mergulhei no oceano desse amor



                   Dm
Do nosso amor
G                     Am
Eu fiz nascer o sol
            Dm  G                    Am
Pra iluminar o que está por vir
              Dm  G                      Am
Quando te vi, te dei meu coração
                   Dm                G
Tão bem guardado com as chaves
                          F
De toda a minha vida


C
Você é o meu amor
F                 G                          Am
Minha emoção, brilho do meu olhar
F                                           C G Am
Minha inspiração, o meu acreditar
F                          G                           F Fm C
Eu prometo te amar por toda minha vida!




----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
