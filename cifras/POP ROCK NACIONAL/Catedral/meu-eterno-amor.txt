Catedral - Meu Eterno Amor

C
Deixa me dizer tudo que sinto dentro do meu coração e diz respeito a você
Dm
Deixa me mostrar todo esse sentimento que é capaz de mudar todo sentido de ser
G
Quero declarar o quanto eu te amo e agradecer a Deus por sentir esse lindo amor
C
Meu eterno amor...
C
Deixa me olhar bem fundo pros teus olhos e me ver no centro de uma linda paixão
Dm
Que é divina e acompanha os meus passos tão tranquilos sempre firmes numa direção
G
Quero declarar o quanto eu te amo e agradecer a Deus por sentir esse lindo amor
C
Meu eterno amor...
F
Sinto teu amor
C
E eu amo o teu olhar
F G C
Do teu lado meu amor prá semrpe eu quero estar

C
Deixa me olhar bem fundo pros teus olhos e me ver no centro de uma linda paixão
Dm
Que é divina e acompanha os meus passos tão tranquilos sempre firmes numa direção
G
Quero declarar o quanto eu te amo e agradecer a Deus por esse lindo amor
C
Meu eterno amor...
F
Eu sinto teu amor
C
E eu quero o teu querer
F G C
Eu amo tudo aquilo que me faz lembrar você

----------------- Acordes -----------------
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
