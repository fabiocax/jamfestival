Catedral - Um minuto

(Kim,César,Júlio)

intr.:(Bb F C) 2X
C                 G
Queria tanto entender o amor
Dm                          F
Mas quem sou eu, um simples sonhador
    C                      G
Que não está cansado de te procurar
Dm                       F
Pois tem certeza que vai te encontrar,
  Am       Em           Dm
E não há razão pra desistir -------------
         F         Am
Quando acreditar é mais
        Em       Dm
Não tem como impedir
               F         G
O querer de um coração

C              G
Queria só um minuto com você

Dm             F
Queria só um minuto pra dizer
C                     G
Tudo aquilo que nunca pude fazer
Dm                 F
Queria apenas um minuto pra esquecer
       Bb         F          C
(Eu só quero um minuto com você)  2X

solo.: (Bb F C) 2x

Repete ------- até o final

volta pra intr. até o final

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
