Catedral - Always On My Mind

[Intro] G

G               D
 Maybe I didn't treat you
Em        D       C           D
 Quite as good as I could have
G               D
 Maybe I didn't love you
Em        D        A7          G
 Quite as often as I could have
C                            G
 Little things I should have said and done
C       G              Am       G  Em  D
 I just never took the time
D         Em     D/F#  G        Am  G    C
 You were always on my mind
         D7           G    C   D
You were always on my mind
G               D
 Maybe I didn't hold you
Em          D            C       D
  All those lonely, lonely times

G                    D
 And I guess I never told you
Em       D                 A7      G
  I'm so happy that you're mine
C                  G
 If I made you feel second best
C             G         Am        G  Em  D
 Girl I'm so sorry I was blind
D         Em     D/F#  G     Am  G  C
 You were always on my mind
         D7           G    C  D
You were always on my mind
G    D/F#  Em   D
Tell       me
C                 G                 Am    D
Tell me that your sweet love hasn't died
G    D/F#  Em   D
Hear     me
        C                                D
Give me one more chance to keep you satisfied
     G
Satisfied

[Solo] G  D  Em  D
       A7  G  C

C                             G
  Little things I should have said and done
C       G              Am       G  Em  D
 I just never took the time
D         Em     D/F#  G    Am  G    C
 You were always on my mind
                   D7
You were always on my mind

( G  D/F#  Em  D )
( C  G  Am  D )

                      G
You were always on my mind
G               D
 Maybe I didn't treat you
Em        D         C           D
 Quite as good as I should have
G               D
 Maybe I didn't love you
Em        D        C          D
 Quite as often as I could have
G               D
 Maybe I didn't hold you
Em         D              C     D
 All those lonely, lonely times
G                    D
 And I guess I never told you
Em      D                 C    G
 I'm so happy that you're mine

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
