Catedral - Desejo, Muito Prazer

Intro: G Dm C Dm G

C                   F
Eu sou exatamente o que você queria ser
C                      F
E tantas outras que gostaria de esquecer
Am               Dm                      Em           G
Sou o inesperado, venho de um jeito tão assim, inexplicável
C                   F
Eu sou aquilo que você queria dizer
C                      F
Todas as coisas que sonhou um dia fazer
Am                   Dm                  Em        G
O que te deixou feliz e triste num amanhecer inigualável
Am            Em                         Dm
Às vezes impossível e outras vezes tão negado
        F          G          Am
Pela censura que reside em você
             Em                        Dm
Às vezes criativo e outras vezes tão carente
               F              G            C
Do medo que você recalca e ao mesmo tempo sente

     G             C
Você luta por mim, sonha por mim
        Dm                G          C
E a sua mente não para de pensar em mim
     G              C
Você chora por mim, mente por mim
        Dm                G          C
Mas sua mente não para de pensar em mim
         G                 Dm
Muito prazer, eu sou o teu desejo
        C                Dm        G          C
E fico tão feliz por finalmente você me conhecer


----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
