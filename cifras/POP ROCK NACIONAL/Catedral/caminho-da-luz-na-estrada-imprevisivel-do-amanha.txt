Catedral - Caminho da Luz Na Estrada Imprevisível do Amanhã

Intro: D A Bm A G A G D

D          A
E se fosse fácil achar
Em      D                 Bm                        A
Todas as respostas que eu procuro nesse intenso caminhar
D         A
E se fosse simples ver
Em     D         Bm                         A
A boa vontade da humanidade livre ao se entender

D      A           Bm
Te avistei no desespero
      A              G
Me joguei de corpo inteiro a esse amor
         A        G          D
Sei que ele pode curar minha dor
       A           Bm
Minha noite se fez dia
       A          G
Da tristeza a alegria se tornou

       A          G            D
Sei que nada pode mudar esse amor

           A
E se fosse um balão
Em            D        Bm                          A
Pelos céus na busca do amanhã perfeito sem ter previsão
D          A
E por esse caminhar
Em           D     Bm                           A
Tua luz me guia na estrada imprevisível do encontrar

Solo: D A Bm A G A G D

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
