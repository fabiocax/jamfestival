Catedral - Ver Estrelas e Sorrir


INTRO: G A D (2x) G A Bm G A D


G        A          D
Você já imaginou o que seria de nós
G          A         D
Se não existisse o amor
G        A             Bm(*)
De mãos dadas com a justiça e livres
G        A          D
E o coração para seguir

Você já imaginou a humanidade
Vivendo em paz com união
Venceremos todos na verdade
Pois ela habita em todos nós

D            G     A
Deixa eu te dar a mão
G       A        D  Em  A
Vamos sair, por aí,

       G     A      D
Ver estrelas e sorrir

Pensar que existem
Tantos planos na mente
Só falta agora praticar
O amor ao próximo, o amor a Deus
E Jesus Cristo em todos nós.

*baixo: F# B C# D C# B A G

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
C# = X 4 6 6 6 4
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
