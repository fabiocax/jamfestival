Catedral - Estórias que me contaram

Composição: Kim, Cezar e Júlio C.


Intro: Am, G/B, C, D (4X)

(Am, G/B , C, D)
Havia certa vez um homem
Que dizia o nome de Deus
Quando o coração lhe doía
Por uma criança que chorava
Ou um pobre que mendigava...
Ele andava até a floresta
Acendia o fogo
Entoava canções e dizia as palavras   G (oitavado) D

C         D       (Am, D) 2X
E Deus o ouvia...

C        G/B     Am            G
O tempo passou, voltou a mesma floresta
C        G/B              Am  G
Mas não carregava fogo nas mãos

C          G/B    Am             G
Só lhe restou cantar as mesmas canções
C          G/B               Am  G  ( C, D) 2X
E dizer as palavras, e Deus atendeu ainda assim
C                      G/B
Um tempo mais longo se foi
Am            G
Sem fogo nas mãos
C              G/B           Am          G
Sem força nas pernas, não alcançou a floresta
C            G
Mas do seu quarto
Am               D             C          G
Saíram as mesmas canções, e as mesmas palavras
  Am             G
E Deus lhe disse sim...
C             G
Chegou a velhice
 Am                     G
Nem floresta, nem fogo ou canções
C                G
Restaram as palavras
Am                G          Am, D) 2X
E o mesmo milagre ocorreu

Am, G/B, C, D (4X)

Am , G/B , C, D
Por fim, sem fogo ou floresta
Sem canções ou palavras
Só mesmo o infinito desejo e o silêncio
E Deus tudo entendeu (3X)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
