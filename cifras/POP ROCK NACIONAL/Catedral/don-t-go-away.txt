Catedral - Don't Go Away

(intro 2x)

E|--------------------------------|
B|--------------------------------|
G|-----5h7~--b---/9--7-5-7-5------|
D|-5h7-------------------------7--|
A|--------------------------------|
E|--------------------------------|

E|----------------|**----|
B|----------------|------|
G|----------------|-5----|
D|-------------7--|---7--|
A|-7-7--7p5-p0----|------|
E|----------------|------|

     *Abafado

(verso 1)
 Am                              G
A cold and frosty morning there's not a lot to say

          Fmaj7                    D
About the things caught in my mind
Am                        G
As the day was dawning my plain flew away
             Fmaj7                    D
With all the things caught in my mind

ponte)
(riff 1)
E|--------------|
B|--------------|
G|------5-4--5--|
D|----5---------|
A|--------------|
E|--------------|

Dm (riff 1)                        F
And I don't wanna be there when you're coming down
Dm (riff 1)                            F           G
And I don't wanna be there when you hit the ground

(refrão)
            C                  G
So don't go away, say what you say
                    Am                  G
But say that you'll stay, forever and a day
                   F                       G
In the time of my life, cause I need more time
                Am             G           F
Yes I need more time just to make things right

(solo)
E|-----------------------|
B|-5b~-------5b----------|
G|---------------5b--2b--|
D|-----------------------|
A|-----------------------|
E|-----------------------|

(verso 2)
Am                         G
Damn my situation and the games I have to play
             Fmaj7                   D
With all the things caught in my mind
Am                   G
Damn my education I can't find the words to say
          Fmaj7                    D
About the things caught in my mind

(bridge)
Dm ((riff 1)                           F
And I don't wanna be there when you're coming down
Dm ((riff 1)                          F             G
And I don't wanna be there when you hit the ground

(refrão)

                C                 G
So don't go away, say what you say
    ´                 Am                   G
But say that you'll stay, forever and a day
                   F                      G
In the time of my life cause I need more time
                Am             G           F
Yes I need more time just to make things right

Fm
Me and you what's going on

(solo 1)
    C          G           Am
E|---------------------------|
B|---------------------------|
G|-----5----5-7b-5---5--5----|
D|-5h7----7--------7------7--|
A|---------------------------|
E|---------------------------|

F                             Fm
All we seem to know is how to show the feelings that are wrong

(solo 2)
    C                 G                    Am                         G                     F
E|-------------------------------8--10-8-------8----8-10-8------8-8------8----8-10-8------8--|
B|-------------------------8--10----------8-10---10--------8-10-----8-10---10--------8-10----|
G|-----5--5-7b-7b-5---5-9--------------------------------------------------------------------|
D|-5h7--------------7------------------------------------------------------------------------|
A|-------------------------------------------------------------------------------------------|
E|-------------------------------------------------------------------------------------------|

(refrão 2x)
               C               G
So don't go away, say what you say
    ´               Am                  G
But say that you'll stay, forever and a day
                   F                     G
In the time of my life cause I need more time
                Am             G         F
Yes I need more time just to make things right

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
Fmaj7 = 1 X 2 2 1 X
G = 3 2 0 0 0 3
