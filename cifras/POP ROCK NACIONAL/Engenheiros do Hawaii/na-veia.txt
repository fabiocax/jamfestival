Engenheiros do Hawaii - Na Veia

(Gessinger)

intro: G G/Bb Am C

(Em C D)
se você perguntar por mim
vão dizer que eu ando muito estranho
vão dizer que eu ando por aí
 Em       C      Am     C    D
quando você perguntar por mim

(Em C D)
se você perguntar por mim
vão dizer as coisas mais estranhas
nenhuma resposta vai satisfazer
Em       C       A     C    D
quando você perguntar por mim

  G G/Bb
! vem !
 Am              C
 ver com os próprios olhos


 G G/Bb
! vem !
 Am        C
 ver a vida como ela é       REFRÃO  2X

(Em C D) 2X

(Em C D)
se você está mesmo a fim
de saber por onde eu ando
de saber por quê eu ando assim
 Em       C       Am     C    D
é melhor nem perguntar por mim

REFRÃO 2X

C           D       Em
sem filtro, na veia
C           D       Em
sem filtro, na veia

Solo:
e|3-----------------------|         e|3--|
B|--3--1-0-1-0-1-0-1-0---0|  3x     B|--3|  4x
G|---------------------0--|         


o dedilhado ta +/- + da pra tocar.... melhor doque nada

P.1.2.1

LEGENDA: P = Polegar
         1 = dedo 1
         2 = dedo 2

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G/Bb = X 1 0 0 3 3
