Engenheiros do Hawaii - Toda Forma de Poder

Eu presto atenção no que eles dizem
                     Dm  G  C  E
Mas eles não dizem nada
  Am
Fidel e Pinochet tiram sarro de você
             Dm  G  C  E
Que não faz nada
       Am
E eu começo a achar normal que algum boçal
                    Dm    G  C  F  Bb  E
Atire bombas na embaixada

        A                   Dm         E
Se tudo passa,  talvez você passe por aqui
     A           Dm              E
E me faça  esquecer tudo que eu vi
        A                     Dm          E
Se tudo passa,   talvez você passe por aqui
      A         Dm   E   D   E
E me faça  esquecer


Am
Toda forma de poder
                           Dm  G  C  E
É uma forma de morrer por nada
Am
Toda forma de conduta
                            Dm  G  C E
Se transforma numa luta armada
      Am
A história se repete
                                    Dm   G  C  F  Bb  E
Mas a força deixa a estória mal contada

        A                     Dm         E
Se tudo passa,  talvez você passe por aqui
     A         Dm               E
E me faça  esquecer tudo que eu vi
        A                  Dm          E
Se tudo passa,  talvez você passe por aqui
      A         Dm   E   D   E
E me faça  esquecer

   Am
O fascismo é fascinante
                               Dm  G  C  E
Deixa a gente ignorante fascinada
       Am
E tão fácil ir adiante
                                     Dm  G  C  E
E esquecer que a coisa toda está errada

    Am
Eu presto atenção no que eles dizem
                   Dm   G  C  F  Bb  E
Mas eles não dizem nada

        A                   Dm         E
Se tudo passa,  talvez você passe por aqui
     A          Dm               E
E me faça  esquecer tudo que eu vi
        A                  Dm          E
Se tudo passa,  talvez você passe por aqui
      A          Dm   E   D   E
E me faça  esquecer

(solo) A Dm E  A Dm E  A Dm E  A Dm E
        A                   Dm         E
Se tudo passa,  talvez você passe por aqui
     A          Dm               E
E me faça  esquecer tudo que eu vi
        A                     Dm          E
Se tudo passa,  talvez você passe por aqui
      A          Dm
E me faça  esquecer...

E     A    Dm    E     A    Dm    E     A
Iêêêê Ouuuu Iêêêê Iêêêê Ouuuu Iêêêê Iêêêê Ou
E|--------------------------|
B|--------------------------|
G|--9---9--------9----------| (4x)
D|--7---10-------9----------|
A|--------------------------|
E|--------------------------|

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
