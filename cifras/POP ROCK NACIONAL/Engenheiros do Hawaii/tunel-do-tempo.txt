Engenheiros do Hawaii - Túnel do Tempo

(Humberto Gessinger)

Introdução: 2x (E G#m A) - A B E

E  G#m       A
te vejo infinita
E  G#m        A
invejo quem grita
                B                     E
o fim do silêncio: canção que não acabou
E G#m           A
interna luz em fuga
E  G#m             A
lanterna sangra e suga
                 B                 E
pra ouvir melhor, melhor apagar a luz

F#m                  E
deve ser o que chamam CANTO DO CISNE
F#m       E
44 minutos do segundo tempo

F#m                  A
pra frente é que se anda
                     B
para a praça, ver a banda passar
F#m
se você for, eu vou
A                     B
se você vier, eu estou no mesmo lugar
F#m                  A
pra frente é que se anda
              B
na rua a banda continua a tocar
F#m                A
sem você, eu fico longe
                B
com você, tudo volta ao lugar

(introdução)

E  G#m      A
há vida na terra
E  G#m        A
há chances de erro
                    B           E
não há nada que possa nos proteger
E   G#m               A
acontece a qualquer hora
E   G#m              A
acontece a qualquer um
                    B        E
não há nada de errado com a gente

F#m                  E
deve ser o que chamam TELHADO DE VIDRO
F#m              E
chuva de granizo, vitrines & vitrais
F#m         A                   B
atire a primeira pedra quem nunca atirou
F#m          A                       B
espere pelo sangue que o bumerangue despertou
F#m        A                       B
atire a segunda pedra, a terceira, o milhar
F#m           A                    B
na idade das pedras que não criam limo
    C#m             B        A
os Flinstones continuam a rolar

F#m                  E
deve ser o que chamam TÚNEL DO TEMPO
F#m                E
ano 2000 era futuro há pouco tempo atrás
F#m                   A
há uma luz no fim do túnel
                    B
e não é um trem na contramão (eterna luz em fuga)
F#m                     A
há um tempo certo para tudo
            B
para tudo uma razão (ou não)
F#m                   A
há uma luz no fim do túnel
                    B
uma chama que nos chama, nos atrai (lanterna sangra e suga)
F#m         A               B
é a luz do fim do túnel do tempo
     C#m     B        A G#m F# F#m E   (F# F#m E)
fogo fátuo, falta de ar

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
