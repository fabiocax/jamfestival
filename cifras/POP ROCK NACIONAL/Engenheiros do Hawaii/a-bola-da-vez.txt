Engenheiros do Hawaii - A Bola da Vez

Introdução: 2x(A C#m)

           A   C#m
A bola da vez brilha em teu olhar
             A           C#m
Matar ou morrer é a impressão que ela dá
    D                     Bm             E
No verde do veludo a tua chance ao teu alcance
             A         C#m
É a bola da vez: o paraíso e a maçã
            A    C#m
O arqueiro zen e sua fé pagã
 D
Tudo ou nada: agora ou nunca:
    Bm            E
Um lance: a tua chance
      C#m       D          A
O jogador é um fogo a queimar
            C#m        D        A
Beleza e horror de um jogo de azar
   Bm
A gente sempre está a fim

   E                              A
A gente sempre está pela bola da vez

(repete introdução)

              A  C#m
Pela enésima vez vamos recomeçar
            A        C#m
Tudo outra vez: cada bola em seu lugar
    D                        Bm             E
No verde do veludo é tudo exposto: é tudo aposta
      C#m       D          A
O jogador é um fogo a queimar
            C#m        D        A
Beleza e horror de um jogo de azar
   Bm
A gente sempre está a fim
   E                              A
A gente sempre está pela bola da vez

solo: 4x(A F#m G#m E)

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
