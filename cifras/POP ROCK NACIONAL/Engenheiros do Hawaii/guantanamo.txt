Engenheiros do Hawaii - Guantánamo

(intro 2x)  Am G F G

Am             G          F    G
Quem foi que disse "te quero"?
Am       G           F     G
Qual era mesmo a canção?
Am       G        F        G
Quem viu a cor do dinheiro?
Am     G          F
Qual a melhor tradução?
Am       G         F       G
Quem foi ao Rio de Janeiro?
Am       G       F   GG
Qual era a intenção?
Am        G      F       G
Qual foi o dia e a hora?
Am         G    F
Quem foi embora...adeus
Dm
Me tira daqui (não adianta gritar)
G
Me ajuda a fugir  (ninguém vai escutar)

Am     G     F       Dm     C        G
Não aguento mais... eu não sei a resposta
Am     G     F       Dm     C        G
Não aguento mais... eu não sei a resposta

(solo da gaita)
E|-10/12-10-8-10-8------8-|-10/12-10-8-10-12-8--|
B|-----------------8-10---|---------------------|
G|------------------------|---------------------|
D|------------------------|---------------------|
A|------------------------|---------------------|
E|------------------------|---------------------|

Am          G       F    G
Quem sabe o que vem primeiro?
Am         G        F    G
Quem sabe o que vem depois?
Am       G         F     G
Quem cabe no mundo inteiro?
Am        G         F    G
Quem mais além de nós dois?
Am        G      F      G
Quem chama ao telefone?
Am          G     F    G
Por que não bate na porta?
Am        G      F     G
Que chama arde teu nome?
Am         G        F
Será que alguém se importa?
Dm
Me tira daqui (nao adianta gritar)
G
Me ajuda a fugir (ninguem vai escutar)
Am     G      F     Dm     C       G
Não aguento mais...eu não sei a resposta
Am     G      F     Dm     C       G
Não aguento mais...eu não sei a resposta


(solo 2x)  Am G F Dm C G


E|----5/12--12--15--10--12--8------------10--12--10--9-----------|
B|----------------------------12--14-----------------------------|
G|-------------------------------------------------------------- |
D|---------------------------------------------------------------|
A|---------------------------------------------------------------|
E|-------------------------------------------------------------- |


E|--12b--12b--12b--12b--15--10--12--8-----------8--10--8--7------|
B|--------------------------   --------10--12--------------------|
G|---------------------------------------------------------------|
D|---------------------------------------------------------------|
A|---------------------------------------------------------------|
E|---------------------------------------------------------------|


Am        G        F    G
Qual é a droga que salva?
Am       G    F        G
Qual é a dose letal?
Am       G            F     G
Quem quer saber tudo isso?
Am       G         F
Será que aguenta a pressao?
Dm       C        G       Dm     C      G
Eu não sei a resposta...eu não sei a resposta
Am      G     F      Dm      C        G
Não aguento mais...eu não sei a resposta
Am      G     F      Dm      C       G
Não aguento mais...eu não sei a resposta.


(Solo Final)

E|-10/12-10-8-10-8-----8---|
B|-----------------8-10----|
G|-------------------------|
D|-------------------------|
A|-------------------------|
E|-------------------------|

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
