Engenheiros do Hawaii - Quando o Carnaval Chegar

Versão 1
-======-

(D9 D9/C D9/B D9/Bb)
quem me vê sempre parado, distante
garante que eu não sei sambar
G                     A                D
tou me guardando pra quando o carnaval chegar

(D9 D9/C D9/B D9/Bb)
eu tô só vendo, sabendo, sentindo, escutando
e não posso falar
G                     A
tou me guardando pra quando o carnaval chegar

(D9 D9/C D9/B D9/Bb)
eu vejo as pernas de louça da moça que passa e não posso pegar
há quanto tempo desejo seu beijo molhado de maracujá
G                      A                 D
tou me guardando pra quando o carnaval chegar


(D9 D9/C D9/B D9/Bb)
e quem me ofende, humilhando, pisando, pensando
que eu vou aturar
e quem me vê apanhando da vida duvida que eu vá revidar
G                     A                   D
tou me guardando pra quando o carnaval chegar

(D9 D9/C D9/B D9/Bb)
eu vejo a barra do dia surgindo, pedindo pra gente cantar
eu tenho tanta alegria, adiada, abafada, quem me dera gritar
G                     A                   D9
tou me guardando pra quando o carnaval chegar


_______________________________________________________


Versão 2
-======-

D                   D/C
quem me vê sempre parado, distante
                G/B       Gm/Bb
garante que eu não sei sambar
G                        A                D
tou me guardando pra quando o carnaval chegar
D                  D/C
eu tô só vendo, sabendo, sentindo, escutando
        G/B     Gm/Bb
e não posso falar
G                     A
tou me guardando pra quando o carnaval chegar

(D D/C G/B Gm/Bb)
eu vejo as pernas de louça da moça que passa e não posso pegar
há quanto tempo desejo seu beijo molhado de maracujá
G                      A                 D
tou me guardando pra quando o carnaval chegar

(D D/C G/B Gm/Bb)
e quem me ofende, humilhando, pisando, pensando
que eu vou aturar
e quem me vê apanhando da vida duvida que eu vá revidar
G                     A                   D
tou me guardando pra quando o carnaval chegar

(D D/C G/B Gm/Bb)
eu vejo a barra do dia surgindo, pedindo pra gente cantar
eu tenho tanta alegria, adiada, abafada, quem me dera gritar
G                     A                   D
tou me guardando pra quando o carnaval chegar

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
D/C = X 3 X 2 3 2
D9 = X X 0 2 3 0
D9/B = X 2 0 2 3 0
D9/Bb = X 1 X 2 3 0
D9/C = X 3 0 2 3 0
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
Gm/Bb = 6 5 5 3 X X
