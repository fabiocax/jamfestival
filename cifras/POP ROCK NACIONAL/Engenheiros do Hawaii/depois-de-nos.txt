Engenheiros do Hawaii - Depois de Nós

[Intro] Gm  F  Eb  Dm  Gm  F  Eb

E|-10-8-6-8-10-8-6-8-6-5~---10-8-6-8-10-8-6~--|
B|--------------------------------------------|
G|--------------------------------------------|
D|--------------------------------------------|
A|--------------------------------------------|
E|--------------------------------------------|

Bb                      F
  Hoje os ventos do destino
Gm                Dm
  Começaram a soprar
Bb                  F
  Nosso tempo de menino
Gm                   Dm
  Foi ficando para trás

Bb                    F
  Com a força de um moinho
Gm                  Dm
  Que trabalha devagar

Bb                    F
  Vai buscar o teu caminho
Gm                  Dm
  Nunca olha para trás

Eb                     Bb
  Hoje o tempo voa nas asas de um avião
Eb            Bb
  Sobrevoa os campos da destruição
Eb                     Bb                      F
  É um mensageiro das almas dos que virão ao mundo

  Depois de nós

( Gm  F  Eb  Dm  Gm  F  Eb )

Bb                   F
  Hoje o céu está pesado
Gm                     Dm
  Vem chegando o temporal
Bb                     F
  Nuvens negras do passado
Gm                   Dm
  Delirante flor do mal

Bb               F
  Cometemos o pecado
Gm                  Dm
  De não saber perdoar
Bb                             F
  Sempre olhando para o mesmo lado
Gm                   Dm
  Feito estátuas de sal

Eb                         Bb
  Hoje o tempo escorre nos dedos da nossa mão
Eb                  Bb
  Ele não devolve o tempo perdido em vão
Eb                     Bb                      F
  É um menssageiro das almas dos que virão ao mundo

  Depois de nós

( Gm  F  Eb  Dm  Gm  F  Eb )

  Bb                    F
Meninos na beira da estrada
            Gm                   Dm
Escrevem mensagens com lápis de luz
  Bb                 F
Serão mensageiros divinos
           Gm               Dm
Com suas espadas douradas azuis

   Bb                  F
Na terra, no alto dos montes
             Gm                 Dm
Florestas do norte, cidades do sul
  Bb                F
Meninos avistam ao longe
    Gm               Dm
A estrela do menino Jesus

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
