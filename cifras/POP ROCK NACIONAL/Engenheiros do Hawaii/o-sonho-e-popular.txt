Engenheiros do Hawaii - O Sonho É Popular

(intro) E D A E

     E        D      A      E     E        D        A        E
e|-------------------------------------------------------------------
a|--2-4------2-4----2-4---2-4----2-4------2-4------2-4------2-4------
d|------2-2-----2-2----2-2---2-2----2-4-6----2-4-6----2-4-6----2-4-6-
g|-------------------------------------------------------------------

           E
A pampa é pop

O país é pobre
           D
É pobre a pampa

(o pib é pouco)
        A                B
O povo pena mas não pára
              E
(poesia é um porre)


O poder

O pudor

Várias variáveis
   D
O pão

O peão

Grana, engrenagens
   A
A pátria
           B
À flor da pele
         E
Pede passagem...pqp

O sonho é popular
                 D
Eu li isso em algum lugar
                       A
Se não me engano é ferreira gullar
   B                      E
Falando da arquitetura de um oscar

O concreto paira no ar
                      D
Mais aqui do que em chandigarh
   A     B     E
O sonho é popular

               D
Um golpe em 61
                  A
Um golpe qualquer
       B    (E)
Num lugar comum

Parte que o maltz fala de fundo

  (E)
Uma página arrancada
Um segredo mantido
  (D)
Em passagens subterrâneas
Sob a praça da matriz
  (A)
Uma stória mal contada
  (B)
Uma mentira repetida
  (E)
Até virar verdade
(uma página virada)
Uma página subterrânea
Um segredo arrancado
  (D)
Um passado mal contadas
Até virar verdade
  (A)
A verdade a ver navios
   (B)
Uma mentira repetida
      (E)
...repetida, repetida

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
