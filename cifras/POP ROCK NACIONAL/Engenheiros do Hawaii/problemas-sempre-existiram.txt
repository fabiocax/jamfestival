Engenheiros do Hawaii - Problemas... Sempre Existiram

Intro: Am

         C
Não fui eu
    G/B   Am
Não foi você
          C          G/B   Am
Nem foi a máquina de escrever
Bm              Am
Que matou a poesia
              C
Não foram os deuses
    G/B             Am
Nem foi a morte de deus
             C     G/B  Am
Não foi o jabá da academia
Bm              Am
Que matou a poesia
Bm              C#m D C#m D E E7
Que matou a poesia


Am          C
O fim de semana
  G/B       Am
O fim do planeta
               C       G/B      Am
A palavra "sarjeta" no fim do poema
Bm                      Am
Problemas... sempre existiram
                   C
Esteróides anabolizantes
          G/B
(samplers)
               Am
Dicionários de rima
           C        G/B        Am
O medo do fim (fim) no final das contas
Bm                      Am
Problemas... sempre existiram
Bm                      C#m
Problemas... sempre existiram
              D
Sempre existirão
              C#m
Sempre existiram
              D   E E7
Sempre existirão

C#m         B                A              D
A última palavra é a mãe de todo o silêncio
C#m        B                  A              D
Façamos silêncio para ouvir o último suspiro
C#m          B            A          D      (E E7)
Descanse em paz a mãe de todas as batalhas
C#m         B                A              D
A última palavra é a mãe de todo o silêncio
C#m          B        A              D
Descanse em paz, dê o último suspiro
C#m        B                  A        D    (E E7)
Façamos silêncio para ouvir o último poema

A
Por que você não soa quando toca
G
Por que você não sua quando ama
A
Ninguém derrama sangue quando perde
G
Guerras de fliperama
E
Por que você não soa quando toca
D
Por que você não sua quando ama
E
Por que você não sua quando toca
E7
Por que você não sua quando ama

C#m            B         A     D
As mentiras da arte são tantas...
C#m      B             A   D
...são plantas artificiais
C#m          B
Artifícios que usamos
      A              D
Para sermos (ou parecermos)
      (E E7)
Mais reais
C#m   B          A   D
Um pedaço do paraíso
C#m      B        A    D
Uma estação no inferno
C#m  B     A      D             (E E7)
Uma soma muito maior do que as partes:
 (E E7)
As mentiras da arte

            Am
(o último poema)

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
