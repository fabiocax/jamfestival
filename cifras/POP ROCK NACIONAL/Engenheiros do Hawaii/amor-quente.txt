Engenheiros do Hawaii - Amor Quente

G        C
preto no branco
   G                  C
amarelo, um pouco de azul
G          C
noite estrelada
G       C
peito feliz

G        C
olho no olho
   G             C
pintura a quatro mãos
G        C
tintas claras
G         C     D
o mesmo cigarro
(D D5+ D6 D7)
isso é amor
amor quente


G                 C
água de coco pra dois
G               C
porta do carro aberta
G               C
vento morno da areia
G             C     D
palavras mentirosas
(D D5+ D6 D7)
isso é amor
amor quente

G         C
cama de casal
G                    C
luz bem baixinha pra ver
G          C   G    C   D
gemidos de dor e alegria
D
sair de si por três minutos
isso é amor
A    D
amor quente

G       C     G          C
supermercado, escolher iogurte
G             C
fazer compras juntos
G             C     D
brigar por besteira
(D D5+ D6 D7)
isso é amor
amor quente

G                  C
tomar café, banho, brisa
G                      C
champanhe, tristeza, beleza
G                C
cremes, músicas, sucos, água
G               C            D
drogas, fumo, passar perfume
(D D5+ D6 D7)
isso é amor
amor quente

(A,D)
isso é amor
amor quente

----------------- Acordes -----------------
A = X 0 2 2 2 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D5+ = X 5 4 3 3 X
D6 = X 5 4 2 0 X
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
