﻿Engenheiros do Hawaii - Era Um Garoto Que Como Eu Amava Os Beatles e Os Roling Stones

Capo Casa 1

[Intro] F  C  Bb  C
        F  C  Bb  C

E|----------------------------------------------------|
B|-7-6-7-6-7-6-7-6---6-4-6-4------4-2-4-2-------------|
G|6-6-6-6-6-6-6-6-----4-4-4-4------1-1-1-1------------| (2x)
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

F              C
  Era um garoto que, como eu
Bb                       C
  Amava os Beatles e os Rolling Stones
F               C
  Girava o mundo sempre a cantar
Bb                      C
  As coisas lindas da América

F                    C
  Não era belo, mas mesmo assim

   Bb        C
Havia mil garotas a fim
F                       C
  Cantava "Help!" and "Ticket to Ride"
     Bb              C
Oh "Lady Jane" ou "Yesterday"

F              Bb           C         Bb
  Cantava "viva a liberdade" mas uma carta
    F/A     F               Bb
Sem esperar, da sua guitarra o separou
G                   C
  Fora chamado na América

[Refrão]

  F   Ab  Bb  B     C
Stop com Rol-ling Stones!
  F   Ab  Bb  B    C
Stop com Beatle's songs!

    Bb   A      Bb  F
Mandado foi ao Vietnã
   Bb  A   Bb B C
Lutar com vie-tcongs

F                       Bb
 Tata-ratata, tata-ratata
                       C
Tata-ratata, tata-ratata
                       F
Tata-ratata, tata-ratata
          C
Tata-ratata

F              C
  Era um garoto que como eu
Bb                       C
  Amava os Beatles e os Rolling Stones
F                C
  Girava o mundo, mas acabou
Bb                      C
  Fazendo a guerra do Vietnã

F                    C
  Cabelos longos não usa mais
     Bb          C
Não toca sua guitarra, e sim
F                     C
  Um instrumento que sempre dá
   Bb          C
A mesma nota (rá-tá-tá-tá)

F                Bb
  Não tem amigos, não vê garotas
C           Bb     F/A
  Só gente morta caindo ao chão
F            Bb
  Ao seu país não voltará
G                      C
  Pois está morto no Vietnã

[Refrão]

  F   Ab  Bb  B     C
Stop com Rol-ling Stones!
  F   Ab  Bb  B    C
Stop com Beatle's songs!

    Bb        A   Bb      F
No peito, um coração não há
     Bb   A Bb    B  C
Mas duas medalhas,  sim

[Solo] F

F                       Bb
 Tata-ratata, tata-ratata
                       C
Tata-ratata, tata-ratata
                       F
Tata-ratata, tata-ratata
          C
Tata-ratata

F                       Bb
 Tata-ratata, tata-ratata
                       C
Tata-ratata, tata-ratata
                       F
Tata-ratata, tata-ratata
          C
Tata-ratata

----------------- Acordes -----------------
Capotraste na 1ª casa
A*  = X 0 2 2 2 0 - (*A# na forma de A)
Ab*  = 4 3 1 1 1 4 - (*A na forma de Ab)
B*  = X 2 4 4 4 2 - (*C na forma de B)
Bb*  = X 1 3 3 3 1 - (*B na forma de Bb)
C*  = X 3 2 0 1 0 - (*C# na forma de C)
F*  = 1 3 3 2 1 1 - (*F# na forma de F)
F/A*  = 5 X 3 5 6 X - (*F#/A# na forma de F/A)
G*  = 3 2 0 0 0 3 - (*G# na forma de G)
