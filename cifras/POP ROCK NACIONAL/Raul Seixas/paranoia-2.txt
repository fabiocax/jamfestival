Raul Seixas - Paranoia 2

Intro: Am

   Am
Eu vivo procurando em tudo quanto é lugar
Nos bares nas igrejas eu tentei encontrar
    Dm
Nos becos, nas esquinas, na lama e no pó
F                      E7
  Até do bolso do meu paletó
   Am
Eu sei que essa coisa que eu tenho que achar
Talvez tão perto que a mão não possa tocar
      Dm
Quem sabe uma gilete, talvez no coração
F                   E7              Am
  Olhei até debaixo do meu colchão
     A              D
Oh!Baby, baby,eu preciso parar
 A                      D
Essa paranóia tenho que eliminar
 A                    C#7              F#m
Mas o que eu procuro você escondeu na barriga

     Bm
Não quer me entregar
    B7                      E7
Que diabo você quer mais de mim?
      Am
Que triste sorte a minha, fui me apaixonar
Por alguém que tinha um brilho estranho no olhar
  Dm
Caí na sua teia, serei tua ceia
     F
Um pacto com satã ainda
 E7           Am
quero tentar

(solo)

 A                    D
Mona, Monalisa, cê tá rindo de mim
 A                     D
Garga-gargalhando seu canino de marfim

Eu faço qualquer coisa
        C#7                F#m
Te dou tudo que tenho,oh! bruxa
    Bm               B7
Por um pedacinho de paz que
          E7
um dia eu perdi
    A             D
Oh!Baby, baby,eu preciso parar
 A                       D
Essa paranóia tenho que eliminar
 A                   C#7
Mas o que eu procuro você escondeu
    F#m
na barriga
   Bm
Não quer me entregar
    B7                      E7
Que diabo você quer mais de mim?
    A              D
Oh!Baby, baby,eu preciso parar
 A                       D
Essa paranóia tenho que eliminar
 A                      D
Mona, Monalisa, cê tá rindo de mim
 A                    D
Garga-gargalhando seu canino de marfim

Oh!Baby, baby,eu preciso parar
 A                       D
Essa paranóia tenho que eliminar
 A                     D
Mona, Monalisa, cê tá rindo de mim
 A                    D
Garga-gargalhando seu canino de marfim

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C#7 = X 4 3 4 2 X
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
