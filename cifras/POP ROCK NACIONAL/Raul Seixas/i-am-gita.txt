Raul Seixas - I Am (Gîtâ)

(A E) 3x  D A E
Falado:
"Since the beginning of time
Man has search for the great answer
It was given
Today I give it once more"

E                       F#m
Sometimes you ask me a question
    B7                 E
You ask why I talk so little
  Ab           C#m
I hardly ever speak of love
      F#7                      B7
Don't side you and smiling so bittle
C7                       B7
You think of me all the time
C7                       B7
You eat me, spew me and leave me
C7                           B7
Come forth, see through your ears

         C7                     B7
Cause today I'll challenge your sight
  A                    E
I am the star of the starlights
  A                    E
I am the child of the moon
  A                    E
Yes, I am your harred of love
  D                    E
I am too late and too soon
  A                    E
Yes, I am the fear of failure
  A                    E
I am the power of will
  A                    E
I am the bluff of the gambler
  D      A          E
I am. I move, I'm still
  A                    E
Yes, I am your sacrifice
  A                    E
The placard that spells "forbidden"
  A                    E
Blood in the eyes of the vampire
  D                    E
I am the curse unbidden
  A                    E
Yes, I am the black and the indian
  A                    E
I am the WASP and the jew
  A                    E
I am the Bible and the I-Ching
     D         A             E
The red, the white and the blue
E                    F#m
Why do you ask me a question
B7                       E
Asking is not going to show
     Ab                  C#m
That I am all things in existence
 F#7           B7
I am, I was, I go
C7                       B7
You have me with you forever
C7                       B7
Not knowing if it's bad or good
C7                       B7
But know that I am in yourself
C7                       B7
Why don't you just meet me in the woods
  A                    E
For I am the eaves of the roof
  A                    E
I am the fish and the fisher
  A                    E
"A" is the first of my name
  D                    E
Yes, I am the hope of the wisher
  A                    E
Yes, I am the housewife and the whore
  A                    E
Hunting the markets asleep
  A                    E
I am the devil at your door
       D       A        E
I am shallow, wide and deep
  A                    E
Yes, I am the law of Thelema
  A                    E
I am the fang of the shark
  A                    E
I am the eyes of the blindman
  D                    E
I am the light in the dark
  A                    E
Oh, yes, I am bitter in your tongue
  A                    E
Mother, father and the riddle
  A                    E
I am the son yet the come
              D              A           E
Yes, I'm the beginning, the end and the middle
              D              A           E
Yes, I'm the beginning, the end and the middle
              D              A           E
Yes, I'm the beginning, the end and the middle

----------------- Acordes -----------------
A = X 0 2 2 2 0
Ab = 4 3 1 1 1 4
B7 = X 2 1 2 0 2
C#m = X 4 6 6 5 4
C7 = X 3 2 3 1 X
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#7 = 2 4 2 3 2 2
F#m = 2 4 4 2 2 2
