Raul Seixas - Século XXI

(intro) E C#m F#m B7

          E
Há muitos anos você anda em círculos
        C#m
Já não lembras de onde foi que partiu
         F#m
Tantos desejos soprados pelo vento
         B7
Se espatifaram quando o vento sumiu

        E
Você vendeu sua alma ao acaso
          C#m
Que por descaso estava ali de bobeira
     F#m
E em troca recebeu os pedaços
          B7
Cacos de vida de uma vida inteira

(refrão 2x)

          E                    G#m
Se você correu, correu, correu tanto
          A              E
e não chegou a lugar nenhum
C#m      E         A               B7      E     B7
Baby, oh baby, bem vinda ao século vinte e um

SOLO - E C#m F#m B7 2x

(refrão 1x)

         E
Você cruzou todas as fronteiras
         C#m
Não sabe mais de que lado ficou
         F#m
E ainda tenta e ainda procura
        B7
Por um tempo que faz tempo passou

         E
Agora é noite na sua existência
        C#m
Cuja essência perdeu o lugar
         F#m
Talvez esteja aí pelos cantos
         B7
Mas tá escuro pra poder encontrar

(refrão 2x)

    A               B7      E
Bem vinda ao século vinte e um
    A               B7      E
Bem vinda ao século vinte e um
    A               B7      E

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
