Raul Seixas - Um Minuto Mais

Introd: G  D   G   D

G                                          D
Eu já bem sabia que você não ficaria um minuto mais
                                                    G
Eu que já fui tudo que você já quis no mundo algum tempo atrás
   Dm                                                C         Cm
Jamais esquecerei aquelas horas que passei junto ao teu lado amor
   G                         D                         G
Embora eu soubesse que mais tarde o amor esquece e só deixa a dor

Introd: G   D  G   D

 G                                                   D
Quando a gente ama mesmo, é cego e não reclama nunca o que tem
                                                     G
Pois ainda te quero a qualquer hora eu espero por você meu bem
    Dm                                            C             Cm
Se estiver sofrendo e quiser voltar correndo sabe onde me encontrar
 G                         D                         G          D
Bem feliz serei e eu juro não me importarei o tempo que ficar

 G                         D                         G          D
Bem feliz serei e eu juro não me importarei do tempo que ficar
 G                         D                         G          D      G.
Bem feliz serei e eu juro não me importarei do tempo que ficar

----------------- Acordes -----------------
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
Dm = X X 0 2 3 1
G = 3 2 0 0 0 3
