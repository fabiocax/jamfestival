Raul Seixas - Muita Estrela, Pouca Constelação

(intro 2x) A F E A

A
A festa é boa tem alguém que tá bancando
F
Que lhe elogia enquanto vai se embriagando
E
E o tal do ego vai ficando nas alturas
A
Usar brinquinho pra romper as estruturas

A
E tem um punk se queixando sem parar
F
E um wave querendo desmunhecar
E
E o tal do heavy arrotando distorção
A
E uma dark em profunda depressão

(refrão)

G                           D
Eu sei até que parece sério, mas é tudo armação
G                            E
O problema é muita estrela, prá pouca constelação

A
Tinha um junkie se tremendo pelos cantos
F
Um empresário que jurava que era santo
E
Uma tiete que queria um qualquer
A
E uma sapatão que azarava minha mulher

A
Tem uma banda que eles já vão contratar
F
Que não cria nada mas é boa em copiar
E
A crítica gostou vai ser sucesso ela não erra
A
Afinal lembra o que se fez na Inglaterra

(refrão)
E agora vem a vem a periferia

A
O fotógrafo, ele vai documentar
F
O papo do mais novo big star
E
Pra'quela revista de rock e de intriga
A
Que você lê quando tem dor de barriga

A
E o jonalista ele quer bajulação
F
Pos new old é a nova sensação
E
A burrice é tanta, tá tudo tão a vista
A
E todo mundo pousando de artista

(refrão)

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
