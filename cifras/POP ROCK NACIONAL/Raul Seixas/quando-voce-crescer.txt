Raul Seixas - Quando Você Crescer


Intr.: Bb7+ Eb7+ Bb7+ Eb7+ Bb7+

Bb7+
   O que que você quer ser
       F7        F#º
quando você crescer?
Eb7                   Ebm
   Alguma coisa importante
   Bb             F7    E7
Um cara muito brilhante
Eb       F7        Bb
  Quando você crescer
Não adianta perguntas não
F7    F#º
valem nada
Eb7                   Ebm
   É sempre a mesma jogada
     Bb
Um emprego e uma
    F7   E7
namorada

Eb7       F7        Bb
   Quando você crescer, é!
Cm                      F7          Bb7+
   E cada vez é mais difícil de vencer
      Bº                Cm
(pra quem nasceu pra perder)
                     F7
Pra quem não é importante
         Bb7+
é bem melhor,
    Bº                Cm
(sonhar do que conseguir)
        F7  F#º        Gm
Ficar invés    de partir
             Cm7
Melhor uma esposa ao invés de
        F7/5+  Bb7+
   uma amante
Uma casinha, um carro, a
   F7     F#º
   prestação
Eb7                  Ebm
   Saber de cor a lição que no,
       Bb
Que no bar não se cospe no
   F7         E7
   chão, nego
Eb7       F7        Bb
   Quando você crescer
Alguns amigos da mesma
     F7    F#º
   repartição
Eb7                   Ebm
   Durante o fim de semana
   Bb                 F7   E7
Se vai mais tarde pra cama
Eb        F7        Bb
   Quando você crescer
Cm7                  F7              Bb7+
   E no subúrbio com flores na sua janela
    B°            Cm7
(você sorri para ela)
E dando um beijo lhe
     F7         Bb
     diz, felicidade
   B°            Cm7
(é uma casa pequenina)
    F7  F#º    Gm
É amar  uma menina e não
        Cm7
     ligar pro que
    F7/5+  Bb7+
Se diz
Belo casal que paga as
   F7       F#º
   contas direito
Eb7                  Ebm
   Bem comportado no leito,
Bb               F7
mesmo que doa no peito,
     E7
sim!
Eb7       F7        Bb
   Quando você crescer
E o futebol te faz pensar
    F7    F#º  Eb7
   que no jogo    você é
            Ebm
Muito importante
       Bb
Pois o gol é o seu grande
   F7       E7
   instante
Eb7       F7        Bb
   Quando você crescer
O cafezinho, mostrar o
   F7        F#º
   filho pra vó
Eb7                    Ebm
  Sentindo o apoio dos pais
  Bb                 F7
Achando que não está
   E7      Eb7
   só, aa...
       F7        Bb  E7  Eb7
Quando você crescer, aa...
       F7        Bb  E7  Eb7
Quando você crescer, aa...
             F7      Bb
    quando você crescer,
- Tudo igual. Vai ser exatamente o mesmo

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bb7+ = X 1 3 2 3 1
B° = X 2 3 1 3 1
Bº = X 2 3 1 3 1
Cm = X 3 5 5 4 3
Cm7 = X 3 5 3 4 3
E7 = 0 2 2 1 3 0
Eb = X 6 5 3 4 3
Eb7 = X 6 5 6 4 X
Eb7+ = X X 1 3 3 3
Ebm = X X 1 3 4 2
F#º = 2 X 1 2 1 X
F7 = 1 3 1 2 1 1
F7/5+ = 1 X 1 2 2 1
Gm = 3 5 5 3 3 3
