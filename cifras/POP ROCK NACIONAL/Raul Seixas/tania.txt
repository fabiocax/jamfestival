Raul Seixas - Tânia

G      C                 G
Tânia, hoje cê faz treze anos,
C                       G
Vejo em teu rosto teus planos,
D
Sei que você quer deitar,
G    F       C             G      Em
  Vem   cá   que eu vou te ajudar,
D                    G
Esqueça as freiras e o altar,
              D
Tânia, Tânia, baby.
G       C                    G
Tânia, de pasta contra os botões
C                       G
Que empinam apesar dos sermões,
       D
Rosa no branco da blusa,
G    F    C         G Em
  Recusa, mas quer deitar,
        D              G
Esqueça as freiras e o altar.

Dm7               G
Teu corpo nú e molhado,
  Dm7               G
Virou no banho ó pecado,
Dm7                 G
Tinha que ser exorcizado,
    C    Am7+   C    Am7+
Oh, Tâ   nia,   Tâ   nia,
E                    G
Deus não é tão mal assim,
D       D
Vem meu bem confia em mim.
G      C                      G
Tânia, esquece o despeito das freiras,
C                  G
Gente que vive nas beiras
  D
E nunca se atreve ao profundo,
G        F      C         G     Em
   Fogo, negro, centro do mundo,
     D                   G
Deus está no puro e no imundo.
Dm7                  G
Teu rosto mostra o receio
    Dm7                    G
Que pulsa do arfar do teu seio,
    Dm7               G
Teu corpo seco ou molhado,
     C      Am7+
Foi feito, prá, prá
E                    G
Ser mesmo tocado por deus.
D         D        G
Vem, vem, vem, vem Tânia,
C                        G
Porque hoje cê faz treze anos, nega,
 C                         G
Teu rosto espalha os teus planos
      D
E eu sei que você quer deitar,
G     F        C       G   Em
  Por  deus, venha prá cá,
     D               G
Prá deus nos vendo abençoar,
Vamô!!!

----------------- Acordes -----------------
Am7+ = X 0 2 1 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Dm7 = X 5 7 5 6 5
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
