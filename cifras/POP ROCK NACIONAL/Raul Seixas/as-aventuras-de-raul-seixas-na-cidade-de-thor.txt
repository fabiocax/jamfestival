Raul Seixas - As Aventuras de Raul Seixas Na Cidade de Thor

E
Tá rebocado meu compadre
E
Como os donos do mundo piraram
E                  A
Eles já são carrascos e vítimas
           B7             E
Do próprio mecanismo que criaram

( E G G E A E) 3x

E
O monstro SIST é retado
E
E tá doido pra transar comigo
E                     A
E sempre que você dorme de touca
           B7         E
Ele fatura em cima do inimigo

  ( E G G E A E) 3x


E
A arapuca está armada
E
E não adianta de fora protestar
E
Quando se quer entrar
           A
Num buraco de rato
         B7          E
De rato você tem que transar

  ( E G G E A E) 3x

E
Buliram muito com o planeta
E
E o planeta como um cachorro eu vejo
E                         A
Se ele já não aguenta mais as pulgas
         B7        E
Se livra delas num sacolejo

  ( E G G E A E) 3x

E
Hoje a gente já nem sabe
E
De que lado estão certos cabeludos
E
Tipo estereotipado
E                  A
Se é da direita ou dá traseira
            B7          E
Não se sabe mais lá de que lado

  ( E G G E A E) 3x

E
Eu que sou vivo pra cachorro
E
No que eu estou longe eu tô perto
E                        A
Se eu não estiver com Deus, meu filho
             B7            E
Eu estou sempre aqui com o olho aberto

  ( E G G E A E) 3x

E
A civilização se tornou tão complicada
E
Que ficou tão frágil como um computador
E
Que se uma criança descobrir
E              A
O calcanhar de Aquiles
          B7         E
Com um só palito pára o motor

  ( E G G E A E) 3x

E
Tem gente que passa a vida inteira
E
Travando a inútil luta com os galhos
E                  A
Sem saber que é lá no tronco
           B7           E
Que está o coringa do baralho

  ( E G G E A E) 3x

E
Quando eu compus fiz Ouro de Tolo
E
Uns imbecis me chamaram de profeta do apocalipse
E                           A
Mas eles só vão entender o que eu falei
         B7     E
No esperado dia do eclipse

  ( E G G E A E) 3x

E
Acredite que eu não tenho nada a ver
E
Com a linha evolutiva da Música Popular Brasileira
E                  A
A única linha que eu conheço
             B7         E
É a linha de empinar uma bandeira

  ( E G G E A E) 3x

E
Eu já passei por todas as religiões
E
Filosofias, políticas e lutas
E                          A
Aos 11 anos de idade eu já desconfiava
   B7       E
Da verdade absoluta

  ( E G G E A E) 3x

E
Raul Seixas e Raulzito
E
Sempre foram o mesmo homem
E                      A
Mas pra aprender o jogo dos ratos
           B7          E
Transou com Deus e com o lobisomem

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
