Raul Seixas - Anarkilópolis

[Intro] E  D  E  A  E

A                                                D
 Eu estava na cidade comprando milho pras galinha
Quando um garoto chegou correndo para me avisar
  A                                                          E
Que a diligência do correio tinha deixado uma carta pra mim
                                     A
Uma carta? De quem seria essa merda?   ...é, pois é, mas
                   D
Ah... é que era da prefeitura de Anarkilópolis
                       A                     E     A
Me convidando para uma festa da sua emancipação. Ok boy

A
Whisky de montão eu vou beber E fazer tudo que eu quero fazer
D                          A
Cada um manda no seu nariz Por isso que o povo lá é feliz
  E
É isso aí! Meu filho, é isso aí... Agora


A                                  D
 Montei no meu "silver-jegue" E parti com o firme propósito
         A                                      E
De unir o útil ao agradável Pois Anarkilópolis era também
                          A
O berço da minha amada A bela Josefina Lee
       D                               A
Filha única do meu amigo Xerife James Adean
           E                           A
Enquanto o jegue seguia rinchando Eu seguia pela estrada cantando

           D           E              A
Eu não sou besta pra tirar onda de herói
        F#m              D     E           A     A7
Sou vacinado, eu sou cowboy Cowboy fora da lei
         D       E          A           F#m            D
Durango Kid só existe no gibi E quem quiser que fique aqui
              E              A   D   A
Entrar pra história é com vocês

  A
Quando eu e meu jegue chegamos em Anarkilópolis
                D
Pensei que tinha me enganado até de cidade
               A
Tinha uns que era mal encarado, armado até os dentes
E
Percebi logo a situação
                   A                             D
Os bandidos haviam dominado o lugar E mantinham todos como reféns
            A              E
James Adean não era mais o Xerife
                   A
E só se via a cara das pessoas com tristeza e medo

A
Deus me livre, quase que eu dancei Dedo no gatilho era da lei
D                                A
Sozinho e desarmado estava ali.. Pra o diabo, os que me chamaram aqui
E
Foi então..."Tá dominado cowboy"

           D           E              A
Eu não sou besta pra tirar onda de herói
        F#m              D     E           A     A7
Sou vacinado, eu sou cowboy Cowboy fora da lei
         D       E          A           F#m            D
Durango Kid só existe no gibi E quem quiser que fique aqui
              E              A   D   A
Entrar pra história é com vocês

( A  D  A  E  A )
( D  A  E  A )

           D           E              A
Eu não sou besta pra tirar onda de herói
        F#m              D     E           A     A7
Sou vacinado, eu sou cowboy Cowboy fora da lei
         D       E          A           F#m            D
Durango Kid só existe no gibi E quem quiser que fique aqui
              E              A   D   A
Entrar pra história é com vocês

           D           E              A
Eu não sou besta pra tirar onda de herói
        F#m              D     E           A     A7
Sou vacinado, eu sou cowboy Cowboy fora da lei
         D       E          A           F#m            D
Durango Kid só existe no gibi E quem quiser que fique aqui
              E              A
Entrar pra história é com vocês

Meu filho, é isso aí

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
