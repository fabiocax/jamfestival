Raul Seixas - Rock 'n' Roll

(intro 6x) riff

(riff)
E |-------------
B|-------------
G|-------------
D|-------------
A|------2--4---
E|-0--4--------

   E (riff)
Há muito tempo atrás na velha Bahia
Eu imitava Little Richard e me contorcia
as pessoas se afastavam pensando
que eu tava tendo um ataque de
             A            B
       epilepsia (de epilepsia)

     E  (riff)
No teatro Vila Velha, velho
          conceito de moral

Bosta Nova pra universitário,
gente fina, intelectual
Oxalá, oxum dendê oxossi de não sei
          A                 B
       o quê (de não sei o quê)
E                A           B
Oh, rock'n'roll, yeah, yeah, yeah,
                      E   B
       that's rock'n'roll

        E (riff)
A carruagem foi andando e uma década depois
Nego dizia que indecência era o mesmo
Feijão com arroz
Eu não podia aparecer na televisão
Pois minha banda era nome de
               A                 B
           palavrão (nome de palavrão)

     E (riff)
E lá dentro do camarim no maior abafamento
A mulherada se chegando
altos pratos suculentos
E do meu lado um hippie punk
                              A
Me chamando de traidor do movimento
                 (vê se eu aguento)
               B
(Traidor do movimento)
E                 A           B
Oh, rock'n'roll, yeah, yeah, yeah,
                     E
       that's rock'n'roll

(solo)
(baixo)
G |-------------|--------|-----------|--------|--------
D|-------------|------4-|-----------|------6-|--------
A|------2--4---|--4-7---|------2--4-|--6-9---|----2--4
E|-0--4--------|-5------|-0--4------|-7------|-0-4----

       (4x)        (2x)       (2x)
       E  (riff)
Alguns dizem que ele é chato
Outros dizem que é banal
Já o colocam em propaganda
fundo de comercial
Mas o bicho ainda entorta minha
                 A                B
      coluna cervical (coluna cervical)

     E (riff)
Já dizia o eclesiastes
Há dois mil atrás
Debaixo do sol não há nada novo
Não seja bobo meu rapaz
Mas nunca vi Beethoven fazer
                        A
Aquilo que Chuck Berry faz
                        B
           (Chuck Berry faz)
E (riff)
  Roll olver Beethoven, roll over Beethoven,
               A     B
Roll over Beethoven, tell,
                      E   B
     Tchaikovsky the news

          E (riff)
E pra terminar com esse papo
Eu só queria dizer
Que não importa o sotaque
e sim o jeito de fazer
Pois há muito percebi que
Genival Lacerda tem a ver
com Elvis e com Jerry
     A                  B
    Lee (Elvis e Jerry Lee)

     E (riff)
Por aí os sinos dobram,
isso não é tão ruim
Pois se são sinos da morte
ainda não bateram para mim
E até chegar a minha hora
                      A
eu vou com ele até o fim
                       B
       (com ele até o fim)

(riff)
 E               A           B
Oh, Rock'n'roll, yeah, yeah, yeah,
                     E
       that's rock'n'roll...   (4x)

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
