Raul Seixas - Caminhos II

(Raul Seixas / Paulo Coelho)


Dm                    Bm7/5-
  Assim como todas as portas são diferentes
Dm                         Bm7/5-
  Aparentemente todos os caminhos são diferentes
            Dm                  Bm7/5-  Dm  Bm7/5-
Mas vão dar todos no mesmo lugar
Sim
Dm                      Bm7/5-
  O caminho do fogo é a água
      Dm  Bm7/5-
Assim como
                 Dm         Bb7+
o caminho do barco é o porto
             D#m               B7+
O caminho do sangue é o chicote

Assim como

D#m                      B7+
   O caminho do reto é o torto
D#m    B7+                  D#m  B7+
   O caminho do risco é o sucesso

Assim como
              Em             Em/B
O caminho do acaso é a sorte
    Em                Em/B
O caminho da dor é o amigo
Em                 Em/B     Em
  O caminho da vida     é a morte

----------------- Acordes -----------------
B7+ = X 2 4 3 4 2
Bb7+ = X 1 3 2 3 1
Bm7/5- = X 2 3 2 3 X
D#m = X X 1 3 4 2
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
Em/B = X 2 2 0 0 0
