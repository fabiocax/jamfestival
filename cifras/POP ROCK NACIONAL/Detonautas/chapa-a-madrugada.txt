Detonautas - Chapa a Madrugada

Intro: A G Bm A   (x2)

  A
Chapa e esborracha a solidão da madrugada,
  G
o clima tá quente a loucura controlada,
  Bm
quem dita o seu limite é você e se não for,
  A
liga o alerta pra não ter terror.

  A
Chapa a solidão e esborracha o silêncio,
  G
entra pela fresta da janela o som do vento.
  Bm
Raios abusados invadem o quarto,
  A
é o sol.
A          G     Bm    A
E eu aqui sozinho escondido no lençol.


  A
Chapa e esborracha a solidão da altitude.
  G
Corro pro banheiro pra ouvir o som da água,
  Bm
dizem que acalma, dizem que liberta,
  A
finjo que é uma fonte, deixo a porta aberta.

  A
Ouço várias vozes na cabeça ao mesmo tempo,
  G
algumas vão cantando, outras me dizendo...
  Bm
Um turbilhão de coisas que eu não quero ouvir,
  A
sinto tanto medo, não sei pra onde ir.

  A
Chapa a solidão e esborracha um sentimento,
        G
sei que é angustia, pego um copo d'água,
   Bm                                  A
a garganta ta seca, a pupila dilatada.

  A
Dentro da mochila cato comprimidos,
  G
tapo meus ouvidos, as vozes continuam,
  Bm
estou ficando louco, tento respirar,
  A
sei que o controle só existe num lugar...

  A
tento concentrar e ouvir meu coração,
  G
tento relaxar e sentir a sensação...
  Bm
olho a parede e tem lá um crucifixo
  A
- Deus tá me olhando, sou um pecador,
A                                          G         Bm              A
Deve estar dizendo que eu vou para o infernoo...         será que eu vou?

 A
Gotas de suor escorrem no meu rosto,
 G
corro pra janela, escuto o som dos carros,
 Bm
olho para o céu, olho para o chão,
 A
as sombras na parede são da minha solidão.

 A
Chapa e esborracha a solidão no amanhecer,
 G
vou me suportar, vou me entender,
 Bm
estou me conhecendo entre e a luz e a escuridão,
 A
que habitam minha alma, o meu corpo e o meu peito...

  A
Preciso respirar,
      G
eu preciso respirar
Bm                       A
    eu preciso do meu leito.

 A
Deito na minha cama e começo olhar pro teto,
 G
as luzes apagadas, alguém chamou meu nome...
 Bm                     A
preciso levantar...  eu preciso levantar...

 A
Quando eu abro a porta não consigo acreditar,
 G
sou eu ali parado, de frente pra mim mesmo,
 Bm                                     A
será que eu morri? Será que é isso mesmo?

  A
Chapa a madrugada e eu acordo assustado,
   G
foi só um pesadelo, estou todo suado.
  Bm                                          A
Olho o que há em volta para ter certeza disso,

   A                     G        Bm         A
eu preciso levantar, tenho um compromisso...

  A
Puta que pariu, ainda bem que eu acordei...
  G
Eu podia ter surtado, só me assustei...
  Bm
Deixa eu ir em frente que tem gente me esperando.
  A
Depois eu volto aqui. Sei que to sonhando.

  A
Puta que pariu, ainda bem que eu acordei...
  G
Eu podia ter surtado, só me assustei...
  Bm
Deixa eu ir em frente que tem gente me esperando.
  A
Depois eu volto aqui. Sei que to sonhando.

(G   A)

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
G = 3 2 0 0 0 3
