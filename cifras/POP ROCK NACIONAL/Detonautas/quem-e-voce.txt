Detonautas - Quem É Você?

 Intro:  G  F  C  (2x)

        G                                                F                    C
Você trabalha feito um burro de carga puxando um sistema podre que é bancado com o seu suor
         G                                                  F                     C
E sexta-feira vai a igreja comungar com sua família a voz sagrada Jesus Cristo é o Senhor
       G                                         F                 C
Deixa parte do salário em retribuição a dádiva divina da palavra do pastor.
     G                                            F                  C
É melhor garantir um lugar no céu / Aqui nesse inferno tenta só sobreviver
         G                                                 F                   C
e o que salva é a cervejeira no fim de semana assistindo o jogo do seu time preferido na TV

( G F C )

         G                                                  F                   C
Segunda-feira o seu filho tá em casa porque a escola onde estuda não tem nem um professor
          G                                   F                         C
E o professor esta na rua apanhando da policia tá cobrando seu salário lá do Governador
         G                                         F                   C
Enquanto isso numa casa confortável uma família abastada reunida assiste televisão

        G                                                F                        C
E praguejando fala mal de quem esta na rua enfrentando e dando a cara pra lutar contra a situação!
         G                                              F                  C
Um fura fila que entrou na sua frente conseguiu ser atendido muito antes de você
          G                                             F                         C
E aquele cara que foi reclamar do caso chamaram de barraqueiro que não tinha o que fazer
          G                                           F                         C
A sogra dele há semanas na espera, vai pensando que já era, não consegue o leito em um hospital
        G                                          F              C           G
E na favela aquela guerra continua traficante e a policia no controle social! Mas..
        F    C
Quem é você?
          G  F   C  G
Quem é você?
        F    C
Quem é você?
          G  F   C
Quem é você?
            G                                                F                      C
Tu fuma um Beck e é chamado de financiador por um senhor que toma uísque e bate na mulher
        G                                             F                        C
E nego enche a cara no fim de semana sai de carro dirigindo mata cinco e puxa o carro e sai de ré
         G                                            F                             C
a gente gasta são 6 meses de salário dando tudo pro governo e não tem quase nada em troca
       G                                             F                C
E o governo vai tomando e gastando o seu dinheiro eles são o parafuso e você é a a porca
              G                                               F                  C
Já foram pra mais 500 anos dessa história e não mudou tanto assim desde a colonização
       G                                                 F              C
A diferença é que hoje o colonizador é aplaudido num programa de televisão
         G                                             F                       C
A gente acha como se por um milagre Deus no auge da bondade fosse um dia interceder
             G                                            F                       C        G
e enquanto esse dia não chega a gente vai aceitando e esperando alguma coisa acontecer, mas..
        F    C
Quem é você?
          G  F   C  G
Quem é você?        Me diz!
        F    C
Quem é você?
          G  F   C
Quem é você?
        G                                                   F                 C
O teu avô que trabalhou a vida inteira dia e noite, noite e dia até se aposentar
         G                                           F                   C
recebe agora uma miséria de salário fica 10 horas na fila esperando e não pode reclamar
          G                                                F                    C
Mas as crianças vão crescer e o futuro do Brasil por algum dia deverá ser bem melhor!
              G                                                          F                         C
Só que o problema é que as crianças tão crescendo com seus pais longe de casa e mais ninguém a seu redor!
           G                                        F                         C
Eu não queria te dizer mas vou ter que falar tu é esperto mas tá sendo passado pra trás
      G                                         F                C
Pode ser que quando tu percebas isso lá na frente já seja tarde demais
        G          F    C
Agora dance! Vai, dance! Mão na cabeça, mão no joelho
 G                  F   C
Fica de quatro, não pode parar
       G                           F
Agora dance, dance, dance, dance.. Dance!
   C                                      G                     F      C
Mãozinha prum lado, bundinha pro outro, se finge de morto e não pare de dançar!
        G        F       C                             G                  F    C
Agora dance! dance! dance, mão na cabeça, mão no joelho, fica de quatro, não pode parar!
       G                     F
Agora dance, dance, dance.. dance!
    C                                     G                    F     C     G
Mãozinha prum lado, bundinha pro outro, se finge de morto e não pare de dançar!
           F   C
Quem é você?
          G  F  C  G
Quem é você?
        F    C
Quem é você?
          G
Quem é você?

----------------- Acordes -----------------
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
