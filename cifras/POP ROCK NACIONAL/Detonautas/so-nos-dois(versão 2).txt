Detonautas - Só Nós Dois

                    Am/C
Agora somos só nós dois
   G                                 E
E não temos que provar pra mais ninguém
 F7+          Am/C                         G
Amor, eles não conseguem perceber como é real
                                 E      F7+
Que a gente se encante com alguém assim...
Am/C                          G                           E
Existem mil mistérios﻿ que renovam os nossos planos de seguir
                       F7+
Acreditando nesse nosso amor!
Am/C                     G                            E
E nada do que digam vai mudar o que pensamos, deixa estar
                     F7+
E agora vamos, já chegou!

(refrão)
        Am/C       G
Meu coração vai te mostrar
           E                   F
Que esse amor não precisa esperar...

                Am/C
Não precisa esperar!

----------------- Acordes -----------------
Am/C = X 3 2 2 5 X
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
F7+ = 1 X 2 2 1 X
G = 3 2 0 0 0 3
