Detonautas - Ela Não Sabe (Mas Nós Sabemos)

Riff ( E G D A )

E                         A
Ela não sabe
                      G                          E         A   G
Mas nós sabemos os seus segredos
E                         A
Seguimos juntos
              G                        E         A   G
Você e eu somos verdadeiros

E
Já faz um tempo quando a gente se encontrou
D
Eu percebi a intenção do seu sorriso
E
Por um segundo aquilo quase me enganou
D
Mas eu queria era me divertir com isso

E                                                G      A
Eu descobri o que ela me fez

E                                        G       A
Eu descobri o que ela tentou

E
Ela não sabe
                                                           A   G
Mas nós sabemos os seus desejos
E                       A           G                          E       A  G
Seremos tolos, em aceitar todos seus apelos

Refrão

E      D    A/C#              E
Ah e quando tudo chegar ao fim
D             A/C#                      E
Vamos guardar nossos segredos

Refrão

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
D = X X 0 2 3 2
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
