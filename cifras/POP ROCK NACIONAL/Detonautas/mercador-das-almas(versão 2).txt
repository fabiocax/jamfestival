Detonautas - Mercador Das Almas

Intro:
Riff 1 (8x)
Riff 1 + Riff 2 (3x)
Riff 3

Seus olhos rasos d’água
Revelam sua dor

(Riff 4)
Perde um espaço no céu
Luta pra não se entregar e viver

( F#5 + Riff 1 )
Vende o corpo, vende a alma
Vende tudo, vende bem
Que vende bem
Trabalha pelas sombras escondido
Que o lucro é garantido
E não se importa mais com a vida de ninguém

(Riff 4)

Perde um espaço no céu
Luta pra não se entregar e viver

( F#5 + Riff 1 )
Tem pra quem tem 100
Tem pra quem vem sem
Tem pra quem tem 100
Tem pra quem vem sem

(Riff 1) (4x)
(Solo)

Seus olhos rasos d’água
Revelam sua dor

(Riff 4)
Perde um espaço no céu
Luta pra não se entregar e viver

( F#5 + Riff 1 )
Vende o corpo, vende a alma
Vende tudo, vende bem
Que vende bem
Trabalha pelas sombras escondido
Que o lucro é garantido
E não se importa mais com a vida de ninguém

(Riff 4)
Perde um espaço no céu
Luta pra não se entregar e viver

(Riff 5)
Sai daqui quem não presta
Vamos acabar com essa festa
Quando o sangue esquenta é fatal
Eu já não estou mais assim tão normal

(Riff 4)
Perde um espaço no céu
Luta pra não se entregar e viver

( F#5 + Riff 1 )
Tem pra quem tem 100
Tem pra quem vem sem
Tem pra quem tem 100
Tem pra quem vem sem

Tablaturas:

Riff 1:
D|-------------------------|
A|-------------------------|
E|-0h2--0h2--0h2--0h2-0-h2-|

 Riff 2:
E|--------|
B|10~~~~--|
G|--------|
D|--------|
A|--------|
E|--------|

Riff 3:
Guitarra 1
             *Deixa soar F#5
E|-------------|
B|-------------|
G|-------------|
D|----------4--|
A|5-5-4-2-4-4--|
E|----------2--|

Guitarra 2
E|--------------|---------|-----|----------|
B|--------------|---------|-----|----------|
G|--------------|---------|-----|----------|
D|--------------|---------|-----|----------|
A|5-5-4-2-4-2-0-|-2-0-2/4-|-2-0-|-2-0-2/4--|
E|--------------|---------|-----|----------|


Riff 4:
   D7M C#m D7M E
E|---------------|
B|---------------|
G|6---6---6---9--|
D|7---6---7---9--|
A|5---4---5---7--|
E|---------------|


Solo
E|-------12-/9-----------------------9~--9-12-14------------------14~-|
B|-----------------------------------------------12-10-9-7-5--7-------|
G|-9-11b-------9-11-11--9-11~--9-11b----------------------------------|
D|--------------------------------------------------------------------|
A|--------------------------------------------------------------------|
E|--------------------------------------------------------------------|


Riff 5:
E|--------------------|
B|--------------------|
G|--------------------|
D|4-4--4-2-4-2---4-4--|
A|4-4----------4-4-4--|
E|2-2------------2-2--|

----------------- Acordes -----------------
C#m = X 4 6 6 5 4
D7M = X X 0 2 2 2
E = 0 2 2 1 0 0
F#5 = 2 4 4 X X X
