Detonautas - Dias Assim (part. Leoni)

[Intro]  Em7  D9(11)  C  D9(11)
         Em7  D9(11)  C  D9(11)

  Em7           D9(11)        C  D9(11)
Quando você se sentir tão cansado
  Em7          D9(11)       C  D9(11)
Que o próximo passo pareça demais
Em7               D9(11)      C       D9(11)
E a vida a pesar nos seus ombros como um castigo
 Bm7                C
Não pense em se entregar
  Em7           D9(11)           C    D9(11)
Quando os seus medos te acharem no escuro
  Em7               D9(11)          C  D9(11)
Trancado no quarto sem ter pra onde ir
 Em7                  D9(11)       C   D9(11)
Lembrando dos sonhos perdidos e de outras derrotas
 Bm7            C  B
Não pense em desistir

 E     D         A
Todos nós temos dias assim

 E            D      A
Correndo pro mar de noite sem fim
 E     D         A      C#m
Todos nós temos dias assim
                           C          D9(11)
Eu juro que passa, me abraça e confia em mim

( Em7  D9(11)  C  D9(11) )
( Em7  D9(11)  C  D9(11) )

  Em7           D9(11)          C  D9(11)
Quando estiver com o peito apertado
 Em7           D9(11)            C  D9(11)
Sentindo saudades de alguém que partiu
Em7          D9(11)           C  D9(11)
E a solidão nos seus olhos parir uma lágrima
 Bm7           C  B
Sentir é resistir

 E     D         A
Todos nós temos dias assim
 E            D      A
Correndo pro mar de noite sem fim
 E     D         A      C#m
Todos nós temos dias assim
                           C          D9(11)
Eu juro que passa, me abraça e confia em mim

G
Em algum amigo
C9
Em algum poema
 G              C9
Como uma tábua de salvação
 G              C9
Numa lembrança na sua força
G              C9    D9(11)
Ou pelo menos nessa canção

 E     D         A
Todos nós temos dias assim
 E            D      A
Correndo pro mar de noite sem fim
 E     D         A      C#m
Todos nós temos dias assim
                         C         D9(11)
Eu juro que passa, me abraça e confia em mim

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
D9(11) = X 5 4 0 3 0
E = 0 2 2 1 0 0
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
