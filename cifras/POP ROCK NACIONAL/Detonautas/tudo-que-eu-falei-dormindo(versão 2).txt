﻿Detonautas - Tudo Que Eu Falei Dormindo

Capo Casa 3

Intro: Em  Bm7

Em
Aumenta o som do meu stereo
Bm7
Que eu quero te levar a sério
Em
Apaga a luz e chega perto
Bm7
Pra eu te mostrar os meus segredos
Em                      Bm7
Você dormiu sem me dizer as coisas boas do seu dia
Em                    Bm7                         G7+
E eu saí sem te contar que o que importa nessa vida
            Bm7 A/B     G7+ Em            B7m  C7+
É só deixar rolar e sempre...é só deixar rolar...

(Intro 2x)

Em
E no meu corpo ainda sinto o seu perfume

Bm7
O resultado do nosso confronto
Em
E se para os outros já não faz sentido
Bm7
Eu continuo tentando insistindo
Em                       Bm7
Você dormiu sem me dizer as coisas boas do seu dia
Em                    Bm7                          G7+
E eu saí sem te contar que o que importa nessa vida
            Bm7  A        B7+             A
É só deixar rolar e sempre...é só deixar rolar

C7+    B7+
Sei lá tudo pode parecer estranho
C7+    B7+                      Em    D        C7+
Sei lá tudo pode parecer a (todo tempo de verdade) 2x

Solo: Em  Bm7 (2x)
      G7+  Bm7  A  G7+  A

C7+               Am
E tudo o que eu falei dormindo
D               C7+
Eu sempre quis dizer de dia
Am           C7+
Invento artifícios para nunca te perder
Em     D          C7+
(Eu não vou te perder...) 3x

Termina em Em

----------------- Acordes -----------------
Capotraste na 3ª casa
A*  = X 0 2 2 2 0 - (*C na forma de A)
A/B*  = X 2 2 2 2 X - (*C/D na forma de A/B)
Am*  = X 0 2 2 1 0 - (*Cm na forma de Am)
B7+*  = X 2 4 3 4 2 - (*D7+ na forma de B7+)
Bm7*  = X 2 4 2 3 2 - (*Dm7 na forma de Bm7)
C7+*  = X 3 2 0 0 X - (*D#7+ na forma de C7+)
D*  = X X 0 2 3 2 - (*F na forma de D)
Em*  = 0 2 2 0 0 0 - (*Gm na forma de Em)
G7+*  = 3 X 4 4 3 X - (*A#7+ na forma de G7+)
