Fresno - Lá pt 2

Intro 2x: G Em7 D A7/4

Bm         G
Eu nunca fui de lembrar
Bm              G
Nem tenho quadros em casa
Bm              G       A9
Pois são a fonte do problema

Bm      G
A vida nem sempre é
Bm      G
Do jeito que eu esperava
Bm              G       A9
Eu já nem sei se vale à pena

Em7                     G
Mas se eu pintar um horizonte infinito
   D
E caminhar
    A7/4
Do jeito que eu acredito

Em7             G            A9
Eu vou chegar em um lugar só meu

G
Lá pode ter um novo amor pra eu viver
Em7
Quem sabe uma nova dor pra eu sentir
D
A droga certa pra fazer te esquecer
A7/4
Vai apagar a tua marca de mim
G       Em7             D
Tudo pode estar lá

Bm      G
Quem dera poder partir
Bm              G
Sem tchau, sem mala, sem nada
Bm              G               A9
Ver bem de longe o meu planeta

E perceber

Bm              G
Que a gente é pequeno demais
Bm              G
Na imensidão das galáxias
Bm              G       A9
Voltar a bordo de um cometa

Em7                     G
Mas se eu pensar, que em tudo há algo de perfeito
D               A7/4
E assim, voar, pra onde o ar é rarefeito
Em7                 G           A9
Eu vou chegar em um lugar só meu

G
Lá pode ter um novo amor pra eu viver
Em7
Quem sabe uma nova dor pra eu sentir
D
A droga certa vai fazer te esquecer
A7/4
Vai apagar a tua marca de mim
G               Em7
Tudo pode estar lá
D
E eu aqui

----------------- Acordes -----------------
A7/4 = X 0 2 0 3 0
A9 = X 0 2 2 0 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
