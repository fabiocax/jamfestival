Fresno - Diga

C        Am         Em
Ontem eu sonhei com você
         C         Am               Em
Eu só te liguei porque eu precisava ouvir
        F
Aquelas coisas que eram tão normais
     D
Que a gente já não falava mais
           F                     G
Eu queria tanto te ouvir dizer agora

      C
Então diga
                           Am
Que não vai sair da minha vida
                           F
Diga que não passam de mentiras
                            G
Quando dizem que o amor morreu

       C
Então diga

                              Am
Que o tempo fecha todas as feridas
                           F
E que pra nós existe uma saida
                                G
Que nem por um segundo me esqueceu

C       Am         Em
E hoje acordei sem você
        C              Am                Em
E eu só percebi quando eu senti falta de mim.
              F
Pois existem coisas que eu queria mais
   D
E tantas coisas pra deixar pra trás
           F                     G
Eu queria tanto te ouvir dizer agora

      C
Então diga
                           Am
Que não vai sair da minha vida
                          F
Diga que não passam de mentiras
                            G
Quando dizem que o amor morreu

       C
Então diga
                              Am
Que o tempo fecha todas as feridas
                         F
E que pra nós existe uma saída
                           G
Que nem por um segundo me esqueceu.

       C
Então diga
                           Am
Que não vai sair da minha vida
                           F
Diga que não passam de mentiras
                            G
Quando dizem que o amor morreu

       C
Então diga
                              Am
Que o tempo fecha todas as feridas
                         F
E que pra nós existe uma saída
                           G
Que nem por um segundo me esqueceu.

       C           Am          F     Fm  C
Então diga, então diga, então diga

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
