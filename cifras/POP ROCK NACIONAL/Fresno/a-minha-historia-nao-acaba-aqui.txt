Fresno - A Minha História Não Acaba Aqui

(intro) C#5   G#5   F#5  E5  C5  C#5

A5
Vão te dizer que você não é mais o mesmo,
                                    C#5
Vão apontar o dedo na tua cara pra te acusar.
A5
Vão arrumar mil motivos pra te incriminar.
                                C#5
Por todo canto alguém esperando pra te derrubar.
A5                                       B5
Podem dizer que acabou, eu acho que nem começou.
A5                          G#5
O sol nasceu e eu ainda não dormi.

(refrão)
          C#5
Eu sei que é facil falar,
        G#5
Então me ajude a fazer
            F#5
Ontem eu estava no seu lugar,

                 E5      C5
E não há como voltar
             C#5
Existe algo em você
                      G#5
Que eu sinto falta em mim
                                F#5        E5     C5
Não foi a primeira vez, que eu enxerguei o fim.

A5
Vão te vender sem saber o que há por dentro.
            C#5
E vão achar que com alguns trocados podem te comprar.
A5
Vão encontrar mil maneiras de te rotular.
                                                  C#5
Em todo canto sempre tem alguém que quer roubar o seu lugar.
A5                                            B5
Podem dizer que acabou, eu acho que nem começou.
A5                                        G#5
O sol nasceu e eu ainda não dormi.

(refrão)

A5
Por mais que tentem me impedir,
C#5                      F#5
agora nada pode nos parar (enquanto uma voz dizer)
Não vou (enquanto eu puder correr).
A5
A minha história não acaba aqui,
C#5                           F#5
quem põe esse ponto final sou eu (enquanto eu quiser viver)
G#5
Sou eu!

( C#5  E5 E5   D#5  D5 )

(refrão)

( C#5 )

----------------- Acordes -----------------
A5 = X 0 2 2 X X
B5 = X 2 4 4 X X
C#5 = X 4 6 6 X X
C5 = X 3 5 5 X X
D#5 = X 6 8 8 X X
D5 = X 5 7 7 X X
E5 = 0 2 2 X X X
F#5 = 2 4 4 X X X
G#5 = 4 6 6 X X X
