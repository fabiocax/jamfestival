Fresno - A Faca

(um, dois, três)

(intro) G  D7  G G  D7  Am C  D7  G  D/F#  Em  C D7  G

      G       D7   G
Se você quer me matar
            D7    Am
é só continuar assim
       C    D7      Am
e afundar cada vez mais
      C       D7     G
essa faca dentro de mim

            G        D7    G
Mas cuida pra não me assustar
                 D7     Am
não me fazer gritar de dor
        C      D7  Am
ao receber no coração
          C      D7  Ebº Em
de volta todo o meu a--mor

            C     D7   Ebº Em
que no meu peito se alo--ja
          C     D7  Ebº Em  (ralentando)
que você viu e nem no-tou

        G       D7      G
Não há mais o que importa
                D7       Am
e aquela faca que eu te dei
         C      D7    Am
minha ferida arde demais
            C     D7      G
e eu sofro porque eu te amei

               G     D7     G
E eu estou tossindo todo o sangue
                D7    Am
que já turva o meu olhar
         C      D7    Am
Minha ferida arde demais
           C   D7   Ebº Em
e faz meu coração pa--rar
         C   D7     Em
e faz a poça aumentar
         C      D7 Ebº Em
e faz a dor me do-mi-nar

    G   D7     G
E acaba por aqui
              D7   Am
a vida de um perdedor
        C       D7      Am
que morreu num canto escuro
     C      D7      G
vítima do próprio amor
     Cm     D7       Em
vítima do próprio amor
     C      D7       G
vítima do próprio amor

(G  Em  C  D7  G)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
Ebº = X X 1 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
