Fresno - Planos e Promessas

C9        G/B C9        G/B
  Tudo parou,   você falou
C9         G/B       A        Am
  como se você calculara pra me magoar

C9         G/B C9        G/B
  Eu me virei    e caminhei
C9            G/B         A
  Não mais olhei pra tua cara
       Am
(descobri o que é chorar)

C9         G/B  C9          G/B
  A visão turva,  água da chuva
C9           G/B     A     Am
  não mais iria simular a lágrima

C9               G/B C9        G/B
  Quando eu cheguei,   me deparei
C9               G/B        A
  com o que eu iria te mostrar

     Am
caso eu ouvisse um "sim"

   C C/B Am                        D7
Você        não sabe o quanto eu planejei
                G        G/F#   Em
muito menos o quanto que eu chorei
    D    C9 D              G G7
ao perceber  que você se foi
   C C/B Am                   D7
Você        nem em sonhos conseguiu ver
                  G      G/F# Em
tudo que eu escrevi pra você ler
    D   C9      D               G
ou para eu recitar pra você dormir

(G Em C D)

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C/B = X 2 2 0 1 0
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G/F# = X X 4 4 3 3
G7 = 3 5 3 4 3 3
