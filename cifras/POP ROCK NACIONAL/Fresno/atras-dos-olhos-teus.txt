Fresno - Atrás Dos Olhos Teus

(C Am7 F Fm)

 C              Am7
Tudo pode estar melhor do que eu quis
 D7              F              G
mas sempre surge algo pra estragar
 C  C/B      Am7  Am7/G             F
Se tenho que ir,  não   dá pra impedir
Dm7           G  G7
se  eu já tentei

E7                      Am
Eu não sei se isso pode se chamar azar
   D7             G  G7
então, diga o que é

 C
Diga de que serve
  Am7
o mundo sem você?
Dm7
O céu sem estrelas

 F      Fm        C
Nada, inútil é viver.

(C Am7 Dm7 F Fm)

  C      C/B          Am                 C/B
Quando o teu sonho chegar (teu sonho chegar)
 C         C/B             Am                 Am/G
todo sonho bom que você tiver (bom que você tiver)
 F              E7          Am      Am7
saiba que serei eu que estarei projetando
   D7            G      G7
atrás dos olhos teus.

 C
Diga de que serve
  Am7
o mundo sem você?
Dm7
O céu sem estrelas
 F      Fm        C
Nada, inútil é viver

 C
Diga de que serve
  Am7
o mundo sem você
Dm7
O céu sem estrelas
 F      Fm        C      Am7
Nada, inútil é viver, viver
   Dm7    F  Fm
viver, viver

(C Am7 F Fm)

e|-------8-8-7-------------
B|--8--8-------10-8---10---
G|------------------9------

                   C
..atrás dos olhos teus.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am/G = 3 X 2 2 1 X
Am7 = X 0 2 0 1 0
Am7/G = 3 0 2 0 1 0
C = X 3 2 0 1 0
C/B = X 2 2 0 1 0
D7 = X X 0 2 1 2
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
