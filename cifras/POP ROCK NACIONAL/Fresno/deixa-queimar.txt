Fresno - Deixa Queimar


Dm
Eu vejo a vida desfilar
Bb
Sem que eu possa tocar
Dm
Um filme pálido e sem final
Bb
Que eu assisto sem mudar de canal

Gm
Mas tudo tem uma hora certa pra mudar
A
Sinto ferver meu sangue e o coração pulsar
Bb5
Ardendo mais que o fogo
C
Deixa, deixa, deixa queimar!

Gm
É uma canção tão forte que chega a ferir
A
Quero acordar da morte pra sair daqui
Bb5
Escuto a voz que diz
C                   Dm
Deixa, deixa queimar!

Dm
Cansei de buscar respostas
   Bb                Gm
Cansei de andar sem ter onde chegar
   Dm
Tá vendo essa chama? É nossa!
   Bb            Gm
Pra incendiar a convicção
Desses que querem nos parar

Dm
Que lado você vai escolher?
Bb
Isso é muito mais importante que vencer
Dm
Qual é o tamanho da sua fé?
Bb
Será que é o bastante pra manter você de pé?

Gm
E se você não sabe contra quem lutar
A
Só vai ter inimigos em todo lugar
Bb5
Já posso ouvir o coro
C
Deixa, deixa, deixa queimar!

Gm
Não vai ser a primeira nem segunda vez
A
Que a gente vai pra guerra
Bb5
Contra quem nos fez esquecer de sorrir
C
Deixa, deixa queimar!

Dm
Cansei de buscar respostas
   Bb                Gm
Cansei de andar sem ter onde chegar
   Dm
Tá vendo essa chama? É nossa!
   Bb            Gm
Pra incendiar a convicção
Desses que querem nos parar

Ponte: D5 - F5 - Bb5 - C5
       D5 - F5 - Bb5 - C5
       
       D5 - D5 - D5 - D5


Dm                  F
Cansei de buscar respostas
   Bb                Gm
Cansei de andar sem ter onde chegar
   Dm                  F
Tá vendo essa chama? É nossa!
   Bb            Gm
Pra incendiar a convicção
           F              D5
Desses que querem nos parar

----------------- Acordes -----------------
Gm*  = 3 5 5 3 3 3 - (*Fm na forma de Gm)
A*  = X 0 2 2 2 0 - (*G na forma de A)
Bb*  = X 1 3 3 3 1 - (*Ab na forma de Bb)
Bb5*  = X 1 3 3 X X - (*Ab5 na forma de Bb5)
C*  = X 3 2 0 1 0 - (*A# na forma de C)
C5*  = X 3 5 5 X X - (*A#5 na forma de C5)
D5*  = X 5 7 7 X X - (*C5 na forma de D5)
Dm*  = X X 0 2 3 1 - (*Cm na forma de Dm)
F*  = 1 3 3 2 1 1 - (*D# na forma de F)
F5*  = 1 3 3 X X X - (*D#5 na forma de F5)
