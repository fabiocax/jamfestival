Fresno - Não Jogue As Cartas Fora

Intro: (G Em9)

 G                    Em9
Se você um dia encontrar
 C            D             D7       G
todas aquelas cartas que um dia eu mandei
                           Em9
Não as jogue fora, sem pensar
 C            D       D7             G
Todo o tempo que eu demorei para externar
     D/F#           Em
tudo que existe em mim
            B7            C
e que eu coloco a seu dispor
     D   D7   G
Não seja má assim
   B7            C       D        D7   G
fazendo que não ver o tamanho do meu amor
Em       C        D  D7 G
    o tamanho do meu a-mor..

----------------- Acordes -----------------
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
Em9 = 0 2 4 0 0 0
G = 3 2 0 0 0 3
