﻿Fresno - Polo

Intro: C#5  F#5  A#5 G#5 F#5 2x

e|-------------------------------------|
B|-9-----6-4---6-4---9-----6-4---6-4---| 
G|---6-6-----6-----6---6-6-----6-----6-|                          
D|-------------------------------------| 
A|-------------------------------------|
E|-------------------------------------|                      

e|-------------------------------------|-------------|     
B|-9---6-4---6-4---9---6-4---6-4-------|9---7-6-4-2--|  
G|---6-----6-----6---6-----6-----6-----|--6----------|  2x 
D|-------------------------------------|-------------| 
A|-------------------------------------|-------------|
E|-------------------------------------|-------------|

C#5                        F#5
  Se eu te disser que foi difícil te esquecer,

         A#5         G#5          F#5
seria o mesmo que dizer "não sou capaz"
C#5                        F#5
  de me curar das surras que o mundo me dá,
           A#5           G#5            F#5
de prosseguir, deixar o que passou pra trás

    A#5                  G#5
Eu devo desistir, pra um dia ser feliz,
    F#5               F#5
ou devo resistir? Eu devo insistir?

    C#5                           F#5
Cantando, e mais do que isso: gritando,
                        A#5         G#5      F#5
e, às vezes, até confessando que eu não sei amar
         C#5                      F#5
Pois, sabendo, eu não estaria sofrendo
                       A#5      G#5      F#5
E ainda por cima escrevendo , em vez de falar

A#5 G#5 F#5

C#5                              F#5
Será que alguém que te fez chorar
       A#5         G#5        F#5
sem ter proferido uma palavra?
C#5
  E o que você fez?
          F#5       A#5       G#5          F#5
Tentou lutar ou compôs uma canção indo pra casa?

    A#5                  G#5
Eu devo desistir, pra um dia ser feliz,
    F#5               F#5
ou devo resistir? Eu devo insistir?

    C#5                           F#5
Cantando, e mais do que isso: gritando,
                        A#5         G#5      F#5
e, às vezes, até confessando que eu não sei amar
         C#5                      F#5                       Refrão 2X
Pois, sabendo, eu não estaria sofrendo
                       A#5      G#5      F#5
E ainda por cima escrevendo , em vez de falar

(C#5)
Não! Não! Eu não vou desistir assim! (4x)

----------------- Acordes -----------------
A#5 = X 1 3 3 X X
C#5 = X 4 6 6 X X
F#5 = 2 4 4 X X X
G#5 = 4 6 6 X X X
