Fresno - Nesse Lugar

Intro: D
       D  G  Em  G  A

E|--2-------------2/5-------------2/5-3/2-0-------------2/5--5h7p5--5h7p5/3-2-0--|
B|--3---3---3-3-3-------3---3-3-3-------------3---3-3-3--------------------------|
G|--2-----2---------------2---------------------2--------------------------------|
D|--0----------------------------------------------------------------------------|
A|-------------------------------------------------------------------------------|
E|-------------------------------------------------------------------------------|

D                                         G
 Fiquei tentando decorar o que eu ia dizer
                                                Em
Quando a gente se encontrar e quando eu ver você
                                              G            A
Mas as palavras vão faltar e eu vou ficar fingindo que não é nada demais

D                                            G
 Tem tanta coisa pra falar dizer que eu sofri
                                      Em
Estando em todo lugar sem ter você aqui

                                             G       A            G
Mas não tem como evitar todo esse tempo longe só me fez te querer mais

D
   Nesse lugar      (Nesse lugar)
G                    Em
   A gente pode enxergar
                       G      A
A mais distante das estrelas
D
   Nesse lugar (Nesse lugar)
G                           Em
   Ninguém jamais vai nos achar
                       G                A
Tudo que eu quero é ficar pra sempre aqui

( D/F#  G  A )

              Bm                      Bm(7M/11)
E o que fazer se o plano não funcionar
                       Bm7(11)
Se a caminhada é em vão
                       E
Quem é que vai me guiar

Andando na escuridão

G                  A
Não posso mais esperar

D
   Nesse lugar      (Nesse lugar)
G                    Em
   A gente pode enxergar
                          G          A
A mais distante das estrelas  (Lá do céu)
D                         G     Em
   Nesse lugar (Nesse lugar)
                       G                A
O que eu mais quero é ficar pra sempre aqui
        D
Com você

Nesse lugar




----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
Bm7(11) = 7 X 7 7 5 X
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
