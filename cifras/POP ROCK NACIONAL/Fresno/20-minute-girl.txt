Fresno - 20 Minute Girl

Intro: A

         A5
E eu me lembro como se fosse ontem,
          D5            F5  G#5
quando o coração se partiu
       A       A/G#       F#m     E
Eu fiquei te olhando por vinte minutos
      D     C#m     Bm  E7
e tu nem sequer me viu

F#m              C#m
   E eu fiquei imóvel,
F#m             C#m
   não pude reagir,
         D              A
pois na hora eu estava tão inseguro
        E                   E7
que não ia realmente conseguir

     A          A/G#    F#m         E
Mas hoje aqui estou cantando pra você

       D             C#m          Bm  E
e nem sei se isso é bom ou se é ruim
A           A/G#      F#m         E
  E de uma coisa eu quero ter certeza,
         D          E7     A
é que você vai lembrar de mim

       A5
Outro dia eu estava no meu canto
       D5          F#5   G#5
Quando vi, você me olhou
       A         A/G#            F#m        E
Não estava acreditando no que estava se passando
        D      C#m       Bm    E7
E se você estivesse brincando?

F#m                  C#m
  Dessa vez não hesitei
F#m             C#m
  Não podia deixar passar
     D                       A
Mas deixa eu te contar o que ela fez comigo
    E                E7
Um fora ela veio me dar

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/G# = 4 X 2 2 2 X
A5 = X 0 2 2 X X
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
D5 = X 5 7 7 X X
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#5 = 2 4 4 X X X
F#m = 2 4 4 2 2 2
F5 = 1 3 3 X X X
G#5 = 4 6 6 X X X
