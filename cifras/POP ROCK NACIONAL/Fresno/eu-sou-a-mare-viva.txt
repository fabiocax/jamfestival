Fresno - Eu Sou a Maré Viva

         G9
A casa cheia, o coração vazio
                 C9
Escorre do meu rosto, um lamento arredio

              Em7/9             D4
O veneno acabou, a festa esvaziou
                G5 G4       G
O tempo da inocência terminou

                  G9
Os amigos que eu fiz, e quem jamais voltou
                 C9
Ferida que eu abri, e a que jamais fechou
              Em7/9                   D4
Para passar a luz, que vence a escuridão
                   G5 G4           G
Pra eu tentar aquecer, o meu coração

                C9                        D4
Vão tentar derrubar, que é pra me ver crescer

                Eb°                     Em
E às vezes me matar, que é pra eu renascer
               C9                  D4
Como uma supernova que atravessa o ar
              Eb°                         Em
Eu sou a maré viva… se entrar vai se afogar
                 C9                   D4
Eu grito pro Universo, o meu nome e o seu
                  G9
E ele vai me escutar

                G9
Eu mandei um sinal rumo ao firmamento
              C9
Eu forneci a prova cabal desse meu desalento
             Em7/9
A sonda vai voar
         D4
Até não dar mais pra ver
                    G5 G4
Levando o que há de bom em mim
               G
Levando pra você

                    G9
E os que não estão mais aqui, todos os que se foram
                  C9
Eles vão me encontrar em outra dimensão
                 Em7/9                 D4
Onde não existe dor, não se declara guerra
                   G9
Quando estamos em paz

                C9                        D4
Vão tentar derrubar, que é pra me ver crescer
                 Eb°                    Em
E às vezes me matar, que é pra eu renascer
              C9                   D4
Como uma supernova que atravessa o ar
               Eb°                       Em
Eu sou a maré viva… se entrar vai se afogar
                 C9                   D4
Eu grito pro Universo, o meu nome e o seu
                  G9
E ele vai me escutar

----------------- Acordes -----------------
C9 = X 3 5 5 3 3
D4 = X X 0 2 3 3
Eb° = X X 1 2 1 2
Em = 0 2 2 0 0 0
Em7/9 = X 7 5 7 7 X
G = 3 2 0 0 0 3
G4 = 3 5 5 5 3 3
G5 = 3 5 5 X X X
G9 = 3 X 0 2 0 X
