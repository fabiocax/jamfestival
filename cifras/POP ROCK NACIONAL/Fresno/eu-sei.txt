Fresno - Eu Sei

Intro: Gb7+ Ebm7   Db9

Gb7+                           Ebm7
Às vezes fico com saudades de momentos,
                    Db9
Que eu ainda não vivi
Gb7+                         Ebm7
às vezes peco na vontade de sentimentos,
                    Db   Db4   Db   Db9
Que eu ainda não senti

Bbm                  Bbm7+
te vejo nas paredes dos hotéis
Bbm7                   Gm7(5-)
eu vivo interpretando papéis
Gb7+
as vezes não sei mais quem sou
Gb
me deu vontade de voltar

Db9                                         Gb
Pois eu sei, que você quer viver comigo, outra vez
                          Bbm
Que você quer viver ao lado meu
Ab                        Gb
Até a luz do sol se apagar

Gb7+
Eu exagero nas palavras
                Ebm7
Mas nos meus versos
                  Db9
Eu só encontro você
Gb7+                                         Eb
É só mais um dia de chuva, e eu vou pra Redenção
                       Db         Ab/C  Bbm7 Ab
Pois amanhã já vou estar em outro lugar
          Ebm7
Muito longe daqui
          Gb
Muito longe de ti

Db                                       Gb
Pois eu sei, que você quer viver comigo, outra vez
                          Bbm
Que você quer viver ao lado meu
Ab                   Gb
Até a luz do sol se apagar

Db                                                     Gb
Eu sei (eu sei que você), que você quer viver comigo, outra vez ,(mais uma vez)
                          Bbm
Que você quer viver ao lado meu (É só mais um dia de chuva e eu vou pra redenção)
Ab                            Gb
Até a luz do sol se apagar, ate a luz do sol se apagar.
                              Db
Enquanto houver ar pra respirar.

----------------- Acordes -----------------
Ab*  = 4 3 1 1 1 4 - (*G na forma de Ab)
Ab/C*  = X 3 X 1 4 4 - (*G/B na forma de Ab/C)
Bbm*  = X 1 3 3 2 1 - (*Am na forma de Bbm)
Bbm7*  = X 1 3 1 2 1 - (*Am7 na forma de Bbm7)
Bbm7+*  = X 1 3 2 2 1 - (*Am7+ na forma de Bbm7+)
Db*  = X 4 6 6 6 4 - (*C na forma de Db)
Db4*  = X 4 4 1 2 X - (*C4 na forma de Db4)
Db9*  = X 4 6 6 4 4 - (*C9 na forma de Db9)
Eb*  = X 6 5 3 4 3 - (*D na forma de Eb)
Ebm7*  = X X 1 3 2 2 - (*Dm7 na forma de Ebm7)
Gb*  = 2 4 4 3 2 2 - (*F na forma de Gb)
Gm7(5-)*  = 3 X 3 3 2 X - (*F#m7(5-) na forma de Gm7(5-))
Gb7+*  = 2 X 3 3 2 X - (*F7+ na forma de Gb7+)
