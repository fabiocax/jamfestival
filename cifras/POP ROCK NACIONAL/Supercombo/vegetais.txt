Supercombo - Vegetais

Intro: Dm

E|-----------------------------------------------|
B|--6-----6-----6-----6-----6-----6-----6-----6--|
G|----7-----7-----7-----7-----7-----7-----7------|
D|------7-----7-----7-----7-----7-----7-----7----|
A|-----------------------------------------------|
E|-----------------------------------------------|

Dm              F
Moça, sai da sacada
            C             Gm        Dm
Você é muito nova pra brincar de morrer
             F               C         Gm          Dm
Me diz o que há, o quê que a vida aprontou dessa vez?

(Dm  F  C  Gm)
 Aaaah

Dm             F
Venha, desce daí

               C           Gm             Dm
Deixa eu te levar pra um café, pra conversar
      F
Te ouvir
     C             Gm
E tentar te convencer

Dm                 F
Que a vida é como mãe
              C              Gm          A#       Dm
Que faz o jantar e obriga os filhos a comer os vegetais
      C             G5 A5 A#5 C5
Pois sabe que faz bem
     A#          F
E a morte é como pai
             C              Gm        A5 A#5        Dm
Que bate na mãe e rouba os filhos do prazer de brincar
         C                A#
Como se não houvesse amanhã...

----------------- Acordes -----------------
A# = X 1 3 3 3 1
A#5 = X 1 3 3 X X
A5 = X 0 2 2 X X
C = X 3 2 0 1 0
C5 = X 3 5 5 X X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G5 = 3 5 5 X X X
Gm = 3 5 5 3 3 3
