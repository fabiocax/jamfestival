Supercombo - Eu Prefiro Assim

[Intro] Am  Am  F  Am  Am  F  Am  Am  F  G  D

Am                   E
Perdi a fé em suas palavras
                        C       F        G
Independente do que for falar , Oh oh oh oooh
Am                    E
Nem sei por onde eu andava
                           C
Uma estrada sem asfalto ou cor
      F    G
E uma explosão
Am                           E
Revelou o lado bom de uma tragédia
       C                         F
Não mostrar sofrer e esquecer as rédias
      G
Quem dera ser assim

Am                E
Eu não sirvo para festas

         C
Gosto de promessas
               F
Sei que eu não sou normal
    G
Mas eu prefiro assim
Am     E
Assim, assim
Am
Estou ficando sem tempo
              E
E quero só um segundo
              C            F
Pra mostrar o que eu tenho aqui
              G
Sem mais delongas

Am
Corro em meu caminho
             E
Quase sempre sozinho
                          C
Esquecendo o quanto é bom sorrir
           F    G
Sem me preocupar
   Am                         E
Eu lamento se já me deu a sua hora
   Am                        E
Se quiser me ver, estarei lá fora
Lá fora

Am                   E
Eu não deixo a porta aberta
          C
Olho pela fresta
             F          G
Esperando um dia alguém olhar de volta pra mim
Am                E
Eu não sirvo para festas
         C
Gosto de promessas
               F
Sei que eu não sou normal
    G
Mas eu prefiro assim
Am     E
Assim, assim
           C
Eu prefiro assim
Am                  E
Embora esteja só no fim
Am              E
Agora que eu te vi
  C                   F   G
E vi você olhando pra mim.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
