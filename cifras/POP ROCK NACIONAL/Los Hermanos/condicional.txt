Los Hermanos - Condicional

[Intro] Gm  Dm  Cm  D
        Gm  Dm  Cm  D

[Primeira Parte]

  Gm           Dm
Quis nunca te perder
           Cm
Tanto que demais
           D
Via em tudo céu
           Gm
Fiz de tudo   cais
         Dm
Dei-te pra ancorar
         Cm      D
Doces deletérios

    Gm         Dm
E quis ter os pés no chão
            Cm
Tanto eu abri mão

               D
Que hoje eu entendi
              Gm
Sonho não se dá
     Dm
É botão de flor
     Cm
O sabor do fel
      D
É de cortar

[Refrão]

Cm  F        Bb    Gm
    Eu sei é um doce te amar
Cm     F        Bb
   O amargo é querer-te pra mim
Cm               F          Bb      Gm
   Do que eu preciso é lembrar, me ver
Cm                    D
Antes de te ter e de ser teu, muito bem

( Gm  Dm  Cm  D )
( Gm  Dm  Cm  D )

[Segunda Parte]

  Gm           Dm
Quis nunca te ganhar
           Cm
Tanto que forjei
          D
Asas nos teus pés
        Gm
Ondas pra levar
             Dm
Deixo desvendar
      Cm       D
Todos os mistérios

 Gm               Dm
Sei, tanto te soltei
       Cm
Que você me quis
         D
Em todo lugar
           Gm
Li em cada olhar
             Dm
Quanta intenção
       Cm    D
Eu vivia preso

[Refrão]

Cm  F        Bb    Gm
    Eu sei é um doce te amar
Cm     F        Bb
   O amargo é querer-te pra mim
Cm               F          Bb      Gm
   Do que eu preciso é lembrar, me ver
Cm                    D
Antes de te ter e de ser teu

[Ponte]

             Cm                    D
O que eu queria o que eu fazia o que mais?
          Cm                     D
E alguma coisa a gente tem que amar

                   Gm  Cm  Dm
Mas o que não sei mais

( Gm  Cm  Dm )
( Gm  Cm  Dm )
( Gm  Cm  Dm )

    Gm             Dm
Os dias que eu me vejo só
Cm                 D
  São dias que eu me encontro mais
   Gm             Dm
E mesmo assim eu sei também
Cm              D
   Existe alguém pra me libertar

( Gm  Dm )

[Final] Gm  Dm  Cm  D
        Gm  Dm  Cm  D
        Gm  Dm  Cm  D
        Gm  Dm  Cm  D  Gm

----------------- Acordes -----------------
Bb*  = X 1 3 3 3 1 - (*A na forma de Bb)
Cm*  = X 3 5 5 4 3 - (*Bm na forma de Cm)
D*  = X X 0 2 3 2 - (*C# na forma de D)
Dm*  = X X 0 2 3 1 - (*C#m na forma de Dm)
F*  = 1 3 3 2 1 1 - (*E na forma de F)
Gm*  = 3 5 5 3 3 3 - (*F#m na forma de Gm)
