Los Hermanos - Vai Embora

Em
Não sei mais o que fazer
              Am
Da minha vida sem você
                          Em
Agora que se foi eu sofro tanto,
         B
Eu sofro tanto
         Em  C A B
Sem teu amor


Em
Minha Linda não sei mais
                Am
O que fazer pra agüentar
                          Em
Agora que se foi eu sofro tanto
         B
Eu sofro tanto
         Em  C A B
Sem teu amor



Em
Minha linda teu amor
              Am
Não vale tudo que passei
                        Em
Agora que você já foi embora
           B
Eu posso então ser
           Em  C A B Em
Bem mais feliz (4x)
i

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
C = X 3 2 0 1 0
Em = 0 2 2 0 0 0
