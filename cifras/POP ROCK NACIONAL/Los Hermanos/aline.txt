Los Hermanos - Aline

(intro) F# D#m G#m C#

F#        D#m       G#m          C#
Oh minha menina és de tudo que mais belo existe
F#        D#m         G#m          C#
Ver tua beleza é esquecer tudo que há de triste
F#           D#m          G#m           C#
Tua presença Aline é tão sublime quanto o mar e o ar
    F#                D#m         G#m          C#
E estar sempre ao teu lado é ser amado e ter pra sempre o teu

D          A              D       A         G
Olhar que faz meu bem querer, sustenta meu amor
                        F#            B
Que faz com que a cada dia eu te ame mais...

F#            D#m        G#m         C#
Sei que a tua boca já beijou a outra que não a minha
F#         D#m           G#m           C#
Sei que já amou a outros quando não me conhecia
F#           D#m        G#m        C#
Mesmo assim Aline teu carinho me tomou o peito

F#         D#m            G#m      C#
Hoje sem você não mais consigo ser do mesmo jeito

D        A             D
Então dedico a ti esta canção
    A               G
tentando em notas dizer
               F#
Que eu te amo tanto
Tentando gritar ao mundo
 D     A                       D
Aline sem você confesso eu não vivo
A                    G
Sem você a vida é um castigo
                    F#
Sem você prefiro a solidão
               B
a 7 palmos do chão

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C# = X 4 6 6 6 4
D = X X 0 2 3 2
D#m = X X 1 3 4 2
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
G#m = 4 6 6 4 4 4
