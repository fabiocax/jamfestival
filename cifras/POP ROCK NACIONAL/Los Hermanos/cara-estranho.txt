Los Hermanos - Cara Estranho

[Intro]  F#m  C#7

F#m   C#7                    F#m       A#º
Olha só, que cara estranho que chegou
           Bm       E
Parece não achar lugar
                       F#m      C#7
No corpo em que Deus lhe encarnou
                 F#m       C#7
Tropeça a cada quarteirão
                   F#m      A#º
Não mede a força que já tem
                   Bm     E
Exibe à frente um coração
                F#m        C#7
Que não divide com ninguém

                    D#º   D
Tem tudo sempre às suas mãos
                    A    A#º
Mas leva a cruz um pouco além

                  Bm       E
Talhando feito um artesão
                F#m          C#7
A imagem de um rapaz de bem

F#m    C#7                   F#m     A#º
Olha ali, quem tá pedindo aprovação
                 Bm      E
Não sabe nem pra onde ir
                          F#m   C#7
Se alguém não aponta a direção

              F#m         C#7
Periga nunca se encontrar
                  F#m      A#º
Será que ele vai perceber
                 Bm       E
Que foge sempre do lugar
           F#m           C#7
Deixando o ódio se esconder

                 D#º     D
Talvez se nunca mais tentar
              A    A#º
Viver o cara da tv
                   Bm   E
Que vence a briga sem suar
              F#m       C#7
E ganha aplausos sem querer

F#m  C#7
F#m  C#7
F#m  C#7
F#m  A#º

 D
Faz parte desse jogo

 Bb                 G
Dizer ao mundo todo
           Gº             Bm      [Riff 1]
Que só conhece o seu quinhão ruim

[Riff 1]

E|----------------------------------|
D|----------------------------------|
G|----------------------------------|
B|----------------------------------|
A|--2-0-2-4-5-4-2-0-----------------|
E|----------------------------------|

Em
É simples desse jeito
  A#º
Quando se encolhe o peito
D                        G     F#m
Ee finge não haver competição
Em       A
É a solução
              Em           A           G
De quem não quer perder aquilo que já tem
           F#                Bm   F#
E fecha a mão pro que há de vir
Bm  F#
Ia, ia ia

Bm  F#
Bm  F#
Bm  F#
Bm  D#
Em  A
Em  A
Em  A
Bm  F#
Bm  F#
Bm  F#
Bm  F#
Bm

E|------------------------------------------------------|
D|-------------------------------------------10-10b12r10|
G|--9h12-9h11-9---------------9h12-9h11-----------------|
B|--------------9-12-11--9-7------------9-12------------|
A|------------------------------------------------------|
E|------------------------------------------------------|

E|------------------------------------------------------|
D|-----------------------------------10-----------------|
G|--9h12-9h11-9---------------9h12-9-------11------9----|
B|--------------9-12-11--9-7----------------------------|
A|------------------------------------------------------|
E|------------------------------------------------------|

E|------------------------------------------------------|
B|---------10-------------------------10----------------|
G|--9h12-9-------11-----9------9h12-9-------11-----9----|
D|------------------------------------------------------|
A|------------------------------------------------------|
E|------------------------------------------------------|

E|------------------------------------------------------|
B|------------------------------------------------------|
G|--9h11-9h11-9h11-9------------------------------------|
D|-------------------9^^^^------------------------------|
A|------------------------------------------------------|
E|------------------------------------------------------|

 D
Faz parte desse jogo
 Bb                 G
Dizer ao mundo todo
           Gº             Bm      [Riff 1]
Que só conhece o seu quinhão ruim

[Riff 1]

E|----------------------------------|
D|----------------------------------|
G|----------------------------------|
B|----------------------------------|
A|--2-0-2-4-5-4-2-0-----------------|
E|----------------------------------|

Em
É simples desse jeito
  A#º
Quando se encolhe o peito
D                         G   F#m
E finge não haver competição
Em       A
É a solução
              Em           A           G
De quem não quer perder aquilo que já tem
           F#                Bm   F#
Que fecha a mão pro que há de vir

----------------- Acordes -----------------
A*  = X 0 2 2 2 0 - (*G# na forma de A)
A#º*  = X 1 2 0 2 0 - (*Aº na forma de A#º)
Bb*  = X 1 3 3 3 1 - (*A na forma de Bb)
Bm*  = X 2 4 4 3 2 - (*A#m na forma de Bm)
C#7*  = X 4 3 4 2 X - (*C7 na forma de C#7)
D*  = X X 0 2 3 2 - (*C# na forma de D)
D#*  = X 6 5 3 4 3 - (*D na forma de D#)
D#º*  = X X 1 2 1 2 - (*Dº na forma de D#º)
E*  = 0 2 2 1 0 0 - (*D# na forma de E)
Em*  = 0 2 2 0 0 0 - (*D#m na forma de Em)
F#*  = 2 4 4 3 2 2 - (*F na forma de F#)
F#m*  = 2 4 4 2 2 2 - (*Fm na forma de F#m)
G*  = 3 2 0 0 0 3 - (*F# na forma de G)
Gº*  = 3 X 2 3 2 X - (*F#º na forma de Gº)
