Los Hermanos - Cher Antoine

Riff 3x: Cm  A#  G

Cm                                    Fm                 A#                Cm    (G)
   Cher Antoine, Je suis vraiment desolé mais je ne peux   pas partir avec toi.
Cm                        Fm                A#             D#    G
Du 20 au 24 je dois travailler, j'ai 4 jour, 4 jour de congé.

   Cm          A#  G
Ai ai ai ai ai ai
Cm            A# G
  Ai ai ai ai

D|-------------------------------5-3-------|
A|-3-5-6-3-5-6-2-3-5-3-2-----2-5-----6-5-3-|
E|-----------------------5-3---------------|

Cm                                     Fm                      A#                    D#    G
   Je vais à la campagne. Je voyage en train pour le montagnes, c'est un drôle de chemin!
Cm                              Fm                        A#                            D#    G
   Je vais à la plage avec des amis. Je vais faire du sport, et apres je vais fair du ski!"


   Cm          A#  G
Ai ai ai ai ai ai
Cm            A# G
  Ai ai ai ai

D|-------------------------------5-3-------|
A|-3-5-6-3-5-6-2-3-5-3-2-----2-5-----6-5-3-|
E|-----------------------5-3---------------|

D#           G              G#     Gm   Fm    A#
- Feito pra mim, bom pra você. Deixa mudar e confundir!

      D#         G
E|--b15-b15r13----11-10--------11--------|
B|----------------------12-13------12-13~|

G#    Gm   Fm   A#         D#         G            G#
- Deixa de lado o que se diz. Tem no mercado, é só pedir!...
    Gm     Fm         A#        Cm   G
- Me faz chorar... e é feito pra rir.

( Cm ) (4x)
( Cm G ) (4x)

----------------- Acordes -----------------
A# = X 1 3 3 3 1
Cm = X 3 5 5 4 3
D# = X 6 5 3 4 3
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
Gm = 3 5 5 3 3 3
