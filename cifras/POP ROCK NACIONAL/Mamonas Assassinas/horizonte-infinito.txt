Mamonas Assassinas - Horizonte Infinito

Intro: G  D  Em  Bm  C

G
Eu não vejo mais um horizonte infinito
D
Já não sinto mais as suas curvas e gemidos
Em
Eu já não posso crer
Bm                     C
No que eu quero tanto dizer
G
Me coloco frente a frente com um espelho
D
E novamente me apanho fingindo
Em
Para mim mesmo mentindo
Bm                  C
E de mim mesmo fugindo

Refrão:
        G
Quero ouvir frases malditas

                      D
Quero ouvir palavras nunca ditas a ninguém
       Em
Quero ouvir frases malditas
                      Bm               C
Quero ouvir palavras nunca ditas a ninguém

G
Mergulhando no sonho, me transporto pro mundo
D
De falsas verdades que só eu habito
Em
Vagando entre as nuvens
Bm                       C
Em caminho que eu nem acredito
G
Até tudo ficar sem sentido de novo
D
E novamente eu ficar sozinho
Em
Num canto escuro da rua
Bm                C
De volta ao velho caminho

Refrão:
        G
Quero ouvir frases malditas
                      D
Quero ouvir palavras nunca ditas a ninguém
       Em
Quero ouvir frases malditas
                      Bm               C
Quero ouvir palavras nunca ditas a ninguém

Solo:
( G  D  Em  Bm  C )
Me coloco frente a frente com um espelho
E novamente me apanho fingindo
Para mim mesmo mentindo
E de mim mesmo fugindo
Até tudo ficar sem sentido de novo
E novamente eu ficar sozinho
Num canto escuro da rua
Do lado escuro do mundo

G      D      Em     Bm  C
(Oooooo oooooo ooooo ooo o)

----------------- Acordes -----------------
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
