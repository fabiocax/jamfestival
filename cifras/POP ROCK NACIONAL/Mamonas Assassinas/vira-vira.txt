Mamonas Assassinas - Vira-Vira

Introdução:

(Riff 1 - 2x)

E|-12-12-12...--11--9--7-7-7...-7---------------------|
B|----------------------------------------------------|
G|----------------------------------------------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

E|-7-7-7...--9--11--12-12-12...-12--------------------|
B|----------------------------------------------------|
G|----------------------------------------------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|


Raios



( E5  D#5  C#5  B5 )

       P.M .       . .
E|----------------------------------------------------|
B|----------------------------------------------------|
G|-9----8--6--4----4-4--------------------------------|
D|----------------------------------------------------|
A|-7----6--4--2----2-2--------------------------------|
E|----------------------------------------------------|

( B5  C#5  D#5  E5 )

       P.M .       . .
E|----------------------------------------------------|
B|----------------------------------------------------|
G|-4----6--8--9----9-9--------------------------------|
D|----------------------------------------------------|
A|-2----4--6--7----7-7--------------------------------|
E|----------------------------------------------------|


Primeira Parte:

          E5                    B5
Fui convidado pra uma tal de suruba

Não pude ir
                    E5
Maria foi no meu lugar
                                     B5
Depois de uma semana ela voltou pra casa

Toda arregaçada
                  E5
Não podia nem sentar
                              B5
Quando vi aquilo fiquei assustado
                                E5
Maria chorando começou a me explicar
                         A5
Daí então eu fiquei aliviado
                B5
E dei graças a Deus
                         E5
Porque ela foi no meu lugar


Refrão:

              B5
Roda, roda e vira
                E5
Solta a roda e vem
                      B5
Me passaram a mão na bunda
                      E5
E ainda não comi ninguém
              B5
Roda, roda e vira
                E5
Solta a roda e vem
                 A5
Neste raio de suruba
                         B5
Já me passaram a mão na bunda

E ainda não comi ninguém!


Introdução: E5  D#5  C#5  B5
            B5  C#5  D#5  E5
            E5  D#5  C#5  B5
            B5  C#5  D#5  E5


Segunda Parte:

        E5                     B5
Oh, Manoel olha acá como eu estou
                                  E5
Tu não imaginas como eu estou sofrendo
                              B5
Uma teta minha um negão arrancou
                            E5
E a outra que sobrou está doendo
                              B5
Oh, Maria vê se larga de frescura
                                    E5
Que eu te levo no hospital pela manhã
                           A5
Tu ficaste tão bonita monoteta
                 B5
Mais vale um na mão
                   E5
Do que dois no sutiã


Refrão:

              B5
Roda, roda e vira
                E5
Solta a roda e vem
                      B5
Me passaram a mão na bunda
                      E5
E ainda não comi ninguém
              B5
Roda, roda e vira
                E5
Solta a roda e vem
                 A5
Neste raio de suruba
                         B5
Já me passaram a mão na bunda
                      E5  D#5  C#5  B5
E ainda não comi ninguém

          B5  C#5  D#5  E5
Bate o pé

E5  D#5  B5

          B5  C#5  D#5  E5
Bate o pé

(Riff 3)

E|----------------------------------------------------|
B|----------------------------------------------------|
G|-9----8--6--4---------------------------------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

E|----------------------------------------------------|
B|----------------------------------------------------|
G|-4----6--8--9---------------------------------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

E|----------------------------------------------------|
B|----------------------------------------------------|
G|-4----6--8--9\--------------------------------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

Ponte:

      E5                   B5
Oh Maria essa suruba me excita
                          E5
(Arrebita, arrebita, arrebita)
                                 B5
Então vai fazer amor com uma cabrita
                          E5
(Arrebita, arrebita, arrebita)
                                 B5
Mas Maria isto é bom que te exercita
                           E5
(Bate o pé, arrebita, arrebita)
                           A5
Manoel tu na cabeça tem titica
              B5                     E5
Larga de putaria e vá cuidar da padaria


Refrão:

              B5
Roda, roda e vira
                E5
Solta a roda e vem
                      B5
Me passaram a mão na bunda
                      E5
E ainda não comi ninguém
              B5
Roda, roda e vira
                E5
Solta a roda e vem
                 A5
Neste raio de suruba
                         B5
Já me passaram a mão na bunda
                      E5  D#5  C#5  B5
E ainda não comi ninguém

Vamos lá, todo mundo dançando raios
Todo mundo comigo...
Uou, uou, uou
Oh Maria si deu mal, vamo lá!
Ai, como dói

----------------- Acordes -----------------
A5 = X 0 P2 2 X X
B5 = X 2 4 4 X X
C#5 = X 4 6 6 X X
D#5 = X 6 8 8 X X
E5 = 0 P2 2 X X X
