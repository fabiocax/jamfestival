Mamonas Assassinas - Robocop Gay

Introdução:

E|-7/8-8-8-7/8-8-8-7/8-8-10b11r10-8-------------------|
B|-7/8-8-8-7/8-8-8-7/8-8-10b11r10-8-------------------|
G|----------------------------------8h9---------------|
D|--------------------------------------10--9h10------|
A|----------------------------------------------------|
E|----------------------------------------------------|

E|-10----10-----10------------------------------------|
B|-10b12—10b12--10b12r-8---8~~------------------------|
G|-----------------------9----------------------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

E|------------------10---------------------8----------|
B|---------8-10b12------b12r10-8-----8h10--8----------|
G|-----7h9-------------------------9------------------|
D|-8/9------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|


E|--8-8-8---------------------------------------------|
B|--8-8-8---------------------------------------------|
G|----------------------------------------------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|


Primeira Parte:

                 C
Um tanto quanto másculo
               G
Ai, com "M" maiúsculo

Vejam só os meus músculos
                   C
Que com amor cultivei

Minha pistola é de plástico

(quero chupar, pa, pa)
              G
Em formato cilíndrico

(quero chupar, pa)

Sempre me chamam de cínico

(quero chupaaar)
                     C
Mas o porquê eu não sei

(quero chupar, pa)

                   C
O meu bumbum era flácido
                        G
Mas esse assunto é tão místico

Devido a um ato cirúrgico
                    C
Hoje eu me transformei

O meu andar é erótico

(silicone, yeah, yeah, yeah)
                 G
Com movimentos atômicos

(silicone, yeah, yeah)

Sou um amante robótico

(silicone, yeaaah)
                  C
Com direito a replay

(silicone, yeah!)

(Riff 1)

E|------10--------------------------------------------|
B|-8h10-10--------------------------------------------|
G|----------------------------------------------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

(Riff 2)

E|----------------------------------------------------|
B|-----2b---------------------------------------------|
G|-5b------3br----------------------------------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

                  C
Um ser humano fantástico
               G
Com poderes titânicos

Foi um moreno simpático
                   C
Por quem me apaixonei

E hoje estou tão eufórico

(doce, doce, amor)
                  G
Com mil pedaços biônicos

(doce, doce, amor)

Ontem eu era católico

(doce, doce, amor)
                    C5
Ai, hoje eu sou um gay!

( C5  G5  A5  F5 )

E|----------------------------------------------------|
B|-8p6-5-6-6h8--6-5~~--6p5---5-5h6--5-----------------|
G|-------------------------7----------7~~-------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

E|----------------------------------------------------|
B|--------------------8b10----8b10-8b10-8b10r8--------|
G|-7b9--5~-------5~~----------------------------------|
D|-----------5h7--------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|


Refrão:

          C5
Abra sua mente
              G5
Gay também é gente
               A5
Baiano fala "oxente"
            F5
E come vatapá

               C5
Você pode ser gótico
                 G5
Ser punk ou skinhead
                   A5
Tem gay que é Mohamed
               F5
Tentando camuflar

Alá, meu bom Alá

            C5
Faça bem a barba
                G5
Arranque seu bigode
               A5
Gaúcho também pode
                F5
Não tem que disfarçar

           C5
Faça uma plástica
               G5
Ai entre na ginástica
             A5
Boneca cibernética
            F5
Um robocop gay
           C5
Um robocop    gay
            G5
Um robocop gay
                A5
Ai, eu sei, eu sei
             F5  C5
Meu robocop gay

Ai como dói!


Solo Final:

E|----------------------------------------------------|
B|-8p6-5-6-6h8--6-5~~--6p5---5-5h6--5-----------------|
G|-------------------------7----------7~~-------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

E|----------------------------------------------------|
B|--------------------8h10----8b10-8b10-8b10r8--------|
G|-7b9--5~-------5~~----------------------------------|
D|-----------5h7--------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

E|-20-------------------20-------------17-------------|
B|-------20----17-18----------18-20----------20\------|
G|----17----17-------17----17-------17----17----------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

E|-15-------------------15-------------12-------------|
B|-------15----12-13----------13-15----------15\------|
G|----12----12-------12----12-------12----12----------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

E|-17p12----------12-17p12-17p12----------12-17p12----|
B|-------13----13----------------13----13-------------|
G|----------14----------------------14----------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

E|-17p12----------12h17p12----------------------------|
B|-------13----13----------13-------------------------|
G|----------14----------------14----------------------|
D|-------------------------------14-------------------|
A|----------------------------------15-12-------------|
E|----------------------------------------------------|

  H.A  \ /
E|----------------------------------------------------|
B|----------------------------------------------------|
G|-0--------------------------------------------------|
D|---------x\-----------------------------------------|
A|---------x\-----------------------------------------|
E|---------x\-----------------------------------------|

----------------- Acordes -----------------
A5 = X 0 2 2 X X
C = X 3 2 0 1 0
C5 = X 3 5 5 X X
F5 = 1 3 3 X X X
G = 3 2 0 0 0 3
G5 = 3 5 5 X X X
