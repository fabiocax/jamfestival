Cássia Eller - Amor Destrambelhado

(C# Eb)    Abra a porta do armário
                  Tire suas roupas
                Minha cara de otário
                  Olha a sua muito louca

(C# Eb)    Já queimei todo o filme
                  E você o meu salário
                  Nosso vício é um crime
                  Pra esse amor destrambelhado (C#5 C5)

(C# Eb)    Já ta tudo acabado
                  Nossa vida foi tão louca
                  Foi um jeito aviadado
                  Negando um de boa moça

(C# Eb)    Veja bem se me entende
                  Não dá mais pra ser escravo
                  De um romance adolescente
                  Um amor destrambelhado

(C# Eb)    O telefone é meu
                  A geladeira é sua

                  O nosso amor morreu
                  E a vida continua.               (C# Eb) várias vezes

----------------- Acordes -----------------
C# = X 4 6 6 6 4
C#5 = X 4 6 6 X X
C5 = X 3 5 5 X X
E = 0 2 2 1 0 0
Eb = X 6 5 3 4 3
