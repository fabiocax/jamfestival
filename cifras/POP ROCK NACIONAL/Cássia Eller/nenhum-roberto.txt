Cássia Eller - Nenhum Roberto

Versão 1
-======-

E
Não quero nem saber aonde está você
           A
Não que eu não saiba não
E
Não precisa me dizer que tudo pode acontecer
           A
Não que eu não saiba não

G       A                  E
O mesmo erro não se repetirá você verá

E
Não sou nenhum Roberto, mas às vezes eu chego perto.
           A
Não que eu não saiba não
E
Não venha dizer pra mim dos erros do meu português ruim

           A
Não que eu não saiba não

G       A                  E
O mesmo erro não se repetirá você verá

E
Não venha dizer pra mim dos erros do meu português ruim
           A
Não que eu não saiba não
E
E que tudo é um mistério que me fez sorrir me fez tão sério
           A
Não que eu não saiba não

G       A                  E
O mesmo erro não se repetirá
G       A                  E
O mesmo erro não se repetirá
Você verá

_______________________________________________________


Versão 2
-======-

Nenhum Roberto - Cássia Eller

ACORDES.:

  G#/5-/E   C/5-/G#  G7(no5)  A7(no5)    A
e|---8----|---12---|--------|--------|---5----|
b|---0----|---0----|--------|--------|---5----|
g|---7----|---11---|---4----|---6----|---6----|
D|---6----|---10---|---3----|---5----|---7----|
A|---7----|---11---|---2----|---4----|---7----|
E|--------|--------|---3----|---5----|---5----|


Tom.: G
Intro.:  (G#/5-/E)   (C/5-G#)

    G#/5-/E
Não quero nem saber aonde está você
               A
Não que eu não saiba não, Não que eu não saiba não
    G#/5-/E
Não precisa me dizer que tudo pode acontecer
               A
Não que eu não saiba não, Não que eu não saiba não

                                    A|-----------|

O mesmo erro não se repetirá           você verá


    G#/5-/E
Não sou nenhum Roberto, mas às vezes eu chego perto.
               A
Não que eu não saiba não, não que eu não saiba não
    G#/5-/E
Não venha dizer pra mim dos erros do meu português ruim
               A
Não que eu não saiba não, não que eu não saiba não

                                    A|-----------|

O mesmo erro não se repetirá           você verá


    G#/5-/E
Não venha dizer pra mim dos erros do meu português ruim
               A
Não que eu não saiba não, não que eu não saiba não
      G#/5-/E
E que tudo é um mistério que me fez sorrir me fez tão sério
               A
Não que eu não saiba não, não que eu não saiba não


G7(no5) A7(no5)            G#/5-/E
O mesmo erro não se repetirá

G7(no5) A7(no5)            G#/5-/E
O mesmo erro não se repetirá

A|-----------|
E|--4-5-6-0--|


----------------- Acordes -----------------
