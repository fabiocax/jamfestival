Cássia Eller - Palavras Ao Vento

Intro 4x: A  Bm11/A

Primeira Parte:

A                              Bm11/A
Ando por aí querendo te encontrar
                                A
Em cada esquina paro em cada olhar

Deixo a tristeza e trago
       Bm11/A
A esperança em seu lugar

 A                             Bm11/A
Que o nosso amor pra sempre viva

Minha dádiva
A
  Quero poder jurar
             Bm11/A
Que essa paixão jamais será


Refrão:

   A
Palavras apenas
Bm11/A
       Palavras pequenas
A
  Palavras

Intro 2x: A  Bm11/A

Primeira Parte:

A                              Bm
Ando por aí querendo te encontrar
                                A
Em cada esquina paro em cada olhar

Deixo a tristeza e trago
       Bm
A esperança em seu lugar

A                              Bm
  Que o nosso amor pra sempre viva

Minha dádiva
A
  Quero poder jurar
             D            C#7
Que essa paixão jamais será

Refrão:

   F#m
Palavras apenas
   E
Palavras pequenas
A              C#7
  Palavras, momento

   F#m
Palavras, palavras
   E
Palavras, palavras
A              C#7
  Palavras ao vento

Primeira Parte:

A                              Bm
Ando por aí querendo te encontrar
                                A
Em cada esquina paro em cada olhar

Deixo a tristeza e trago
       Bm
A esperança em seu lugar

A                              Bm
  Que o nosso amor pra sempre viva

Minha dádiva
A
  Quero poder jurar
             D            C#7
Que essa paixão jamais será

Refrão Final:

   F#m
Palavras apenas
   E
Palavras pequenas
A              C#7
  Palavras, momento

   F#m
Palavras, palavras
   E
Palavras, palavras
A              C#7
  Palavras ao vento

   F#m
Palavras apenas, apenas
E
  Palavras pequenas
A          C#7  Bm  D  A
  Palavras

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
Bm11/A = X 0 4 4 3 0
C#7 = X 4 6 4 6 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
