Cássia Eller - Blues da Piedade

[Intro]  D7  Gm  Bb  C7  G

Gm
Agora eu vou cantar pros
Miseráveis
  Bb
Que vagam pelo mundo,
Derrotados
 C7
Dessas sementes mal
Plantadas
        D7
Que já nascem com caras
De abortadas
  Gm
Pras pessoas de alma bem
Pequena
   Eb
Remoendo pequenos problemas
  C7
Querendo sempre aquilo

         G
Que não têm
Pra quem vê a luz
            Eb
Mas não ilumina suas
Mini-certezas
 C7
Vive contando dinheiro
       Gm
E não muda quando é lua cheia
  C7                Gm
Pra quem não sabe amar
 C7
Fica esperando
                         Gm
Alguém que caiba no seu sonho
      C7
Como varizes que vão
Aumentando
        D7
Como insetos em volta
Da lâmpada
 Bb               Am
Vamos pedir piedade
 C7         Gm
Senhor, piedade
  Bb              Am         Gm
Pra essa gente careta e covarde
 Bb             Am
Vamos pedir piedade
 C7         Gm
Senhor, piedade
  C7
Lhes dê grandeza e um pouco
        Gm
De coragem
  Gm
Quero cantar só para as
Pessoas fracas
  Eb
Que tão no mundo e
Perderam a viagem
  C7
Quero cantar os blues
          Gm
Com o pastor e o bumbo
Na praça
Vamos pedir piedade
              Eb
Pois há um incêndio sob a
Chuva rala
 C7
Somos iguais em desgraça
       D7
Vamos cantar o blues da piedade

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C7 = X 3 2 3 1 X
D7 = X X 0 2 1 2
Eb = X 6 5 3 4 3
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
