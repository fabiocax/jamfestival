Cássia Eller - Coronel Antônio Bento

Intr.: E
E
Coroné Antônio Bento

No dia do casamento

Da sua filha Juliana

Ele não quis sanfoneiro

Foi pro Rio de Janeiro

Convidou Benê Nunes

Pra tocar, olê, lê, olá, lá

Este dia Bodocó
        F#         E
Faltou pouco pra virá

                         E
Todo mundo que mora por ali

                        A
Esse dia num pôde arresisti

Quando ouvia o toque do piano
                       E
Rebolava, saia arrequebrando

Até Zé Macaxera que era o noivo

Dançou a noite inteira sem pará

Que é costume de todos que se casa
 A               B         E
Ficá doido pra festa se acabá

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
