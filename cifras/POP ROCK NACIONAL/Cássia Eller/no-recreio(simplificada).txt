Cássia Eller - No Recreio

Intro:

E

Primeira Parte:

E
 Quer saber quando te olhei na piscina

Se apoiando com as mãos na borda

Fervendo a água que não era tão fria

E um azulejo se partiu por que a porta
                 B9
Do nosso amor estava se abrindo

E os pés que irão por esse caminho
                   E
Vão terminar no altar


Eu só queria, me casar
                      B9
Com alguem igual a você
A9                             B9
  E alguém igual não há de ter


Segunda Parte:

                         E
Então, quero mudar de lugar

Eu quero estar no lugar
                    B9
Da sala pra te receber
             A9
Na cor do esmalte
                   A9/G#
Que você vai escolher
B9                     E
  Só para as unhas pintar

Quando é que você vai sacar

Que o vão que fazem suas mão
               B9
É só porque você não está comigo


Terceira Parte:

C          G         B9
  Só é possível te amar


Repete Intro:
 E

Repete a Primeira Parte:

E
  Seus pés se espalham

Em fivela e sandália

E o chão se abre por dois sorrisos

Virão guiando o seu corpo que é praia

De um escândalo, charme macio
                      B9
Que cor terá se derreter

Que som os lábios vão morder
                    E
Vem me ensinar a falar

Vem me ensinar ter você
                     B9
Na minha boca agora mora o teu nome
     A9                             B9
É a vista que os meus olhos querem ter


Repete a Segunda Parte:

                   E
Sem precisar procurar

Nem descansar e adormecer

Não quero acreditar
                      B9
Que vou gastar desse modo a vida
A9
  Olhar pro sol
          A9/G#
Só ver janela e cortina
B9                       E
  No meu coração fiz um lar

  O meu coração é o teu lar
       E
E de que me adianta tanta mobília
      B9
Se você não esta comigo


Repete a Terceira Parte:

C         G         B9
  Só é possível te amar
C         G        B9
  Ouve os sinos, amor
C         G         B9
  Só é possível te amar
C             G          B9
  Escorre aos litros, o amor

----------------- Acordes -----------------
A9 = X 0 2 2 0 0
A9/G# = 4 0 2 2 0 0
B9 = X 2 4 4 2 2
Bm/D = X X 0 4 3 2
C = X 3 2 0 1 0
D7 = X X 0 5 3 2
E = 0 2 2 1 0 0
G = 3 2 0 0 3 3
