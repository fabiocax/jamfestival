Cássia Eller - Maluca

-= MALUCA (Luiz Capucho) =-

Intro:G
G
Num dia triste de chuva
E
Foi minha irmã quem me chamou pra ver
            D                G
Era um caminhão, era um caminhão
Em                      E
Carregado de botão de rosas
           G
Eu fiquei maluca
    D                             G
Por flor tenho loucura, eu fiquei maluca
D
Saí
Quando voltei molhada
              E
Com mais de dúzias de botão
               G         D       C
Botei botão na sala, na mesa, na TV, no sofá

    D                                  G
Na cama, no quarto, no chão, na penteadeira
        D                        C
Na cozinha, na geladeira, na varanda
                                      D
E na janela era grande o barulho da chuva
      C
Da chuva
D             C
Eu fiquei maluca
G             C
Eu fiquei maluca
D
Saí
Quando voltei molhada
              E
Com mais de dúzias de botão
               G         D       C
Botei botão na sala, na mesa, na TV, no sofá
    D                                  G
Na cama, no quarto, no chão, na penteadeira
        D                        C
Na cozinha, na geladeira, na varanda
                                      D
E na janela era grande o barulho da chuva
      C
Da chuva
D             C
Eu fiquei maluca
G             C
Eu fiquei maluca

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
