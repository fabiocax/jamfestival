Charlie Brown Jr. - Não É Sério

Intro:  Em G D A

                     Em
Eu vejo na TV o que eles falam sobre o jovem não é sério
     G
O jovem no Brasil nunca é levado a sério
                     D                                                                     A
Eu vejo na TV o que eles falam sobre o jovem não é sério

Não é sério
                     Em
Eu vejo na TV o que eles falam sobre o jovem não é sério
     G
O jovem no Brasil nunca é levado a sério
                    D                                                                      A
Eu vejo na TV o que eles falam sobre o jovem não é sério
Não é sério


                         Em
Sempre quis falar


Nunca tive chance
                        G
Tudo que eu queria

Estava fora do meu alcance
            D
Sim, já

Já faz um tempo

Mas eu gosto de lembrar
         A
Cada um, cada um

Cada lugar, um lugar
       Em
Eu sei como é difícil
                                           G
Eu sei como é difícil acreditar
                                                      D
Mas essa porra um dia vai mudar

Se não mudar, pra onde vou
                                A
Não cansado de tentar de novo

Passa a bola, eu jogo o jogo


       Em                                                                        G
Eu vejo na TV o que eles falam sobre o jovem não é sério

O jovem no Brasil nunca é levado a sério
       D                                                                          A
Eu vejo na TV o que eles falam sobre o jovem não é sério, não é sério
       Em                                                                        G
Eu vejo na TV o que eles falam sobre o jovem não é sério

O jovem no Brasil nunca é levado a sério
       D                                                                          A
Eu vejo na TV o que eles falam sobre o jovem não é sério, não é sério


         Em
A polícia diz que já causei muito distúrbio
         G
O repórter quer saber porque eu me drogo

O que é que eu uso
             D
Eu também senti a dor

E disso tudo eu fiz a rima
    A
Agora tô por conta
                                           Em
Pode crer que eu tô no clima
                     G                               D       A
Eu tô no clima.... segue a rima


             Em                                                        G
Revolução na sua mente você pode você faz

Quem sabe mesmo é quem sabe mais
             D                                                         A
Revolução na sua vida você pode você faz

Quem sabe mesmo é quem sabe mais
            Em                                                        G
Revolução na sua mente você pode você faz

Quem sabe mesmo é quem sabe mais
                              D
Também sou rimador, também sou da banca
                         A                                               Em
Aperta um do forte que fica tudo a pampa
                     G                       D                       A
Eu to no clima! Eu to no clima ! Eu to no clima

Segue a Rima!


                         Em
Sempre quis falar

Nunca tive chance
                        G
Tudo que eu queria

Estava fora do meu alcance
            D
Sim, já

Já faz um tempo

Mas eu gosto de lembrar
         A
Cada um, cada um

Cada lugar, um lugar
       Em
Eu sei como é difícil
                                           G
Eu sei como é difícil acreditar
                                                      D
Mas essa porra um dia vai mudar

Se não mudar, pra onde vou
                                A
Não cansado de tentar de novo

Passa a bola, eu jogo o jogo


       Em                                                                        G
Eu vejo na TV o que eles falam sobre o jovem não é sério

O jovem no Brasil nunca é levado a sério
       D                                                                          A
Eu vejo na TV o que eles falam sobre o jovem não é sério, não é sério
       Em                                                                        G
Eu vejo na TV o que eles falam sobre o jovem não é sério

O jovem no Brasil nunca é levado a sério
       D                                                                          A
Eu vejo na TV o que eles falam sobre o jovem não é sério, não é sério


         Em
A polícia diz que já causei muito distúrbio
         G
O repórter quer saber porque eu me drogo

O que é que eu uso
             D
Eu também senti a dor

E disso tudo eu fiz a rima
    A
Agora tô por conta
                                           Em
Pode crer que eu tô no clima
                     G                               D       A
Eu tô no clima.... segue a rima

             Em                                                     G
Revolução na sua mente você pode você faz

Quem sabe mesmo é quem sabe mais
         D                                                           A
Revolução na sua vida você pode você faz

Quem sabe mesmo é quem sabe mais
             Em                                                      G
Revolução na sua mente você pode você faz

Quem sabe mesmo é quem sabe mais
             D                                                        A
Revolução na sua mente você pode você faz
                                                               Em
Quem sabe mesmo é quem sabe mais
                     G          D    A
Eu to no clima


                               Em
O que eu consigo ver é só um terço do problema
        G
É o Sistema que tem que mudar
          D
Não se pode parar de lutar
                    A
Senão não muda
      Em
A Juventude tem que estar a fim
                       G
Tem que se unir
        D
O abuso do trabalho infantil, a ignorância
       A
Só faz destruir a esperança
        Em                                                           G
Na TV o que eles falam sobre o jovem não é sério
                    D                   A
Deixa ele viver! É o que liga

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
