Charlie Brown Jr. - Pra Não Dizer Que Não Falei Das Flores

Guitarra Base:
(Ebm C#m) x2

Voce deverá executar um pequeno dedilhado todas as vezes que os acordes F# e F aparecerem(exceto no refrão).
 dedilhado:
G|-11-11-------10-|
D|-11-----11---10-|
A|--9-----------8-|

(Ebm G#m C#m F#5 F5)
Caminhando e cantando e seguindo a canção
Somos todos iguais braços dados ou não

     (Eb5 G#5 C#5 F#5 F5) ------> dedilhado*..
     Nas escolas nas ruas, campos, construções
     Caminhando e cantando e seguindo a canção
*dedilhado:
D|---4----4------4----4----4----4---11-11-------10-|
A|6----6----6-----------4----4------11-----11---10-| dedilhado..----------------------|

E acreditam nas flores vencendo o canhão


Refrão

Guitarra Solo:
E|---------------------------------------------------------------------------------------|
B|-11-11-11-9-9-9--7-7-7-6b-----------14-14-14-14-11b--11b-9b-7b-14b-11b--11-11-9-11-9-11|
G|--------------------------11-11-11-----------------------------------------------------|

Guitarra Base:
(Ebm C#m) x2

(Ebm G#m C#m F#5 F5)
Há soldados armados, amados ou não
Quase todos perdidos de armas na mão

(Eb5 G#5 C#5 F#5 F5) ----> dedilhado..
Nos quartéis lhes ensinam uma antiga lição
De morrer pela pátria e viver sem razão

Refrão

(Ebm G#m C#m F#5 F5)
Nas escolas, nas ruas, campos, construções
Somos todos soldados, armados ou não

(Eb5 G#5 C#5 F#5 F5) ----> dedilhado..
Caminhando e cantando e seguindo a canção
Somos todos iguais braços dados ou não

(Ebm G#m C#m F#5 F5)
Os amores na mente, as flores no chão
A certeza na frente, a história na mão

(Eb5 G#5 C#5 F#5 F5) ----> dedilhado..
Caminhando e cantando e seguindo a canção
Aprendendo e ensinando uma nova lição

Refrão x2

----------------- Acordes -----------------
C#5 = X 4 6 6 X X
C#m = X 4 6 6 5 4
Eb5 = X 6 8 8 X X
Ebm = X X 1 3 4 2
F#5 = 2 4 4 X X X
F5 = 1 3 3 X X X
G#5 = 4 6 6 X X X
G#m = 4 6 6 4 4 4
