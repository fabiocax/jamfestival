Charlie Brown Jr. - Cidade Grande

[Intro]

[Riff]

E|----------------------------------------
B|----------------------------------------
G|-----------------------------0----------
D|----------0----0h2----2h3------3--------  (4x)
A|---1h3----------------------------------
E|-3------3----3------3------3------------

[Base solo]

   G5               C5
E|----------------------------------------
B|------------------------3---------------
G|-------0--------------5---5-------------
D|-----5---5----------5-------5-----------
A|---5-------5------3-----------3---------
E|-3-----------3--------------------------


   Bb5              C5
E|----------------------------------------
B|--------------------------3-------------
G|-------0--------------5-----5-----------
D|-----8---8----------5---5-----5---------
A|---8-------8------3-------------3-------
E|-6-----------6--------------------------

[Solo]

E|---------------------------------------|-------------18-20-18s15----------------------------~----|
B|---------------------------------------|-------------------------16-15----15-16-15--15s16s15~~~~-|
G|---------------------------------------|----------19-------------------17------------------------|
D|---------------------------------------|-------17------------------------------------------------|
A|-17s18s17~~--17p15----15~~--15s17------|----17---------------------------------------------------|
E|-------------------18-------------15~~-|-15------------------------------------------------------|

E|---------------------------------------|
B|-18p15----15---18-bend-----------------|
G|-------17------------------------------|
D|---------------------------------------|
A|---------------------------------------|
E|---------------------------------------|

[Riff]

As coisas foram acontecendo sem que eu pudesse perceber
O desapego do maior interessado que era eu
Perdi tudo que era meu
De repente o mundo inteiro me esqueceu

[Pré Refrão]

  G5                         C5
O groove é a lenda. Não, não quero que me entenda
    Bb5                          C5
Mas quero que respeite o que foi feito com suor
     G5                 C5
A história foi feita, a rua é minha seita
  Bb5                            C5
E isso quer dizer que hoje eu me sinto bem melhor

[Pré Refrão Solo]

E|----------------------------------------------20s22-18---15h18p15-------15---------------|
B|------------------------------------------------------------------18h15------------------|
G|----------------------------*12*12*12*12-------------------------------------------------|
D|-12p10----10--10s12-------------------------------------------------------------5~~~~~~--|
A|-------13-----------10-------------------------------------------------------------------|
E|-----------------------------------------------------------------------------------------|

[Refrão]

    G5        Bb5 C5            Bb5             C5
E|---------------------------|-----------------------------|
B|---------------------------|-----------------------------|
G|---------------------------|-----------------------------|
D|------------3--5-------5-5-|-3----3-------3--5------5-5--|
A|-5--5-------1--3--3-3--3-3-|-1----1--1-1--1--3-3-3--3-3--|
E|-3--3--3-3-----------------|-----------------------------|

    G5        Bb5 C5            Bb5             C5
E|---------------------------|-----------------------------|
B|---------------------------|-----------------------------|
G|---------------------------|-----------------------------|
D|------------3--5-------5-5-|-3----3-------3--5-5-5-5-5-5-|
A|-5--5-------1--3--3-3--3-3-|-1----1--1-1--1--3-3-3-3-3-3-|
E|-3--3--3-3-----------------|-----------------------------|

Sem pensar, sem imaginar
No que viria a ser melhor, yeah
Cidade grande
Eu só vi gesticular, mas não
Não deu pra escutar não
Vidas distantes, yeah

[Riff]

E|----------------------------------------
B|----------------------------------------
G|-----------------------------0----------
D|----------0----0h2----2h3------3--------  (4x)
A|---1h3----------------------------------
E|-3------3----3------3------3------------

[Solo base]

   G5               C5
E|----------------------------------------
B|------------------------3---------------
G|-------0--------------5---5-------------
D|-----5---5----------5-------5-----------
A|---5-------5------3-----------3---------
E|-3-----------3--------------------------

   Bb5              C5
E|----------------------------------------
B|--------------------------3-------------
G|-------0--------------5-----5-----------
D|-----8---8----------5---5-----5---------
A|---8-------8------3-------------3-------
E|-6-----------6--------------------------

[Solo]

E|---------------------------------------|-----------------------------------|
B|---------------------------------------|-----------------------------------|
G|---------------------------------------|-----------------------------------|
D|---------------------------------------|-----------------------------------|
A|-17s18s17~~--17p15----15~~--15s17------|-17p15h17p15h17p15-17s18s17~~~~~~--|
E|-------------------18-------------15~~-|-----------------------------------|

E|---------------------------------------------|
B|-------------------------------15s18---------|
G|-------------------------------------19~~~~--|
D|----------------------17---------------------|
A|-17p15----15~~--15h17------------------------|
E|-------18------------------------------------|

[Riff]

As coisas foram acontecendo sem que eu pudesse perceber
O desapego do maior interessado que era eu
Perdi tudo que era meu
De repente o mundo inteiro me esqueceu

[Pré Refrão]

  G5                         C5
O groove é a lenda. Não, não quero que me entenda
    Bb5                          C5
Mas quero que respeite o que foi feito com suor
     G5                 C5
A história foi feita, a rua é minha seita
  Bb5                            C5
E isso quer dizer que hoje eu me sinto bem melhor

[Pré Refrão Solo]

E|--------------------------------------|--------------------------------|
B|-15s16s15~~~~--15p13----13--13s15-----|-15p13h15p13h15p13--15s16s15~~--|
G|---------------------15-----------12--|--------------------------------|
D|--------------------------------------|--------------------------------|
A|--------------------------------------|--------------------------------|
E|--------------------------------------|--------------------------------|

E|----------------------------13--------|
B|-15p13----13--13s15~~~~~~-------------|
G|-------15-----------------------------|
D|--------------------------------------|
A|--------------------------------------|
E|--------------------------------------|

[Refrão]

    G5        Bb5 C5            Bb5             C5
E|---------------------------|-----------------------------|
B|---------------------------|-----------------------------|
G|---------------------------|-----------------------------|
D|------------3--5-------5-5-|-3----3-------3--5------5-5--|
A|-5--5-------1--3--3-3--3-3-|-1----1--1-1--1--3-3-3--3-3--|
E|-3--3--3-3-----------------|-----------------------------|

    G5        Bb5 C5            Bb5             C5
E|---------------------------|-----------------------------|
B|---------------------------|-----------------------------|
G|---------------------------|-----------------------------|
D|------------3--5-------5-5-|-3----3-------3--5-5-5-5-5-5-|
A|-5--5-------1--3--3-3--3-3-|-1----1--1-1--1--3-3-3-3-3-3-|
E|-3--3--3-3-----------------|-----------------------------|

Sem pensar, sem imaginar
No que viria a ser melhor, yeah
Cidade grande
Eu só vi gesticular, mas não
Não deu pra escutar não
Vidas distantes, yeah

Eu vou viver nas nuvens

[Ponte]

E|-------------------------------------------------------------------|
B|-------------------------------------------------------------------|
G|-------------------------------------------------------------------|
D|-------------------------------------------------------------------|
A|-5-----------5-------------5-------------5-------------------5-8---|
E|-3-3-3-3-3-3-3-3---3-3-3-3-3-3-3-3---3-3-3-3-3-3-3-3---5-6-8-------|


E|-------------------------------------------------------------------|
B|-------------------------------------------------------------------|
G|-------------------------------------------------------------------|
D|-------------------------------------------5-5-5--3-3--7-7---------|
A|-5-----------5-------------5---------------3-3-3--1-1--5-5---------|
E|-3-3-3-3-3-3-3-3---3-3-3-3-3-3-3-3---3-3---------------------------|

E|-----------------------------------------|
B|-----------------------------------------|
G|-----------------------------------------|
D|------------------------5-5-5--3-3--7-7--|
A|-5-----5-------5--------3-3-3--1-1--5-5--|
E|-3-3-3-3---3-3-3-3---3-------------------|

E|-----------------------------------------|
B|-----------------------------------------|
G|-----------------------------------------|
D|------------------------5-5-5--3-3--7-7--|
A|-5-----5-------5--------3-3-3--1-1--5-5--|
E|-3-3-3-3---3-3-3-3---3-------------------|

E|-------------------------------------------------------------------|
B|-------------------------------------------------------------------|
G|-------------------------------------------------------------------|
D|-------------------------------------------5-5-5--3-3--7-7---------|
A|-5-----------5-------------5---------------3-3-3--1-1--5-5---------|
E|-3-3-3-3-3-3-3-3---3-3-3-3-3-3-3-3---3-3---------------------------|

E|-------------------------------------------------------------------|
B|-------------------------------------------------------------------|
G|-------------------------------------------------------------------|
D|-------------------------------------------5-5-5--3-3--7-7---------|
A|-5-----------5-------------5---------------3-3-3--1-1--5-5---------|
E|-3-3-3-3-3-3-3-3---3-3-3-3-3-3-3-3---3-3---------------------------|

[Solo] G5  C5  Bb5  C5

E|--------------------------------------------------6h8h10s11s13~~~~-|
B|-----------------------------------6-------6h8h10------------------|
G|-------------------5---------5h7h8---5h7h8-------------------------|
D|------------5hh7h8-----5h7h8---------------------------------------|
A|-----5h6h8-----------8---------------------------------------------|
E|-6h8---------------------------------------------------------------|

E|-13p11p10h11--10s11s10--------------s13-13--15b17-15b17-15b17-15~~-|
B|-----------------------13-11---------------------------------------|
G|------------------------------10-10--------------------------------|
D|-------------------------------------------------------------------|
A|-------------------------------------------------------------------|
E|-------------------------------------------------------------------|

E|----------15-------18~~~~|-----13----20~~~~s22-|
B|-15-16-18----18-15-------|-15-----15-----------|
G|-------------------------|-14------------------|
D|-------------------------|---------------------|
A|-------------------------|---------------------|
E|-------------------------|---------------------|

E|-------------------------------------------------------|
B|-------------------------------------------------------|
G|-----------------------------------------------12~~~~--|
D|--------------------------------12h13h15p13h15---------|
A|-15p13p12-------12h13h15p13h15-------------------------|
E|----------15p13----------------------------------------|

E|-------------------------------------------------------|
B|-------------------------------------------------------|
G|-------------------------------------------------------|
D|-15p13p12-----------------12-13-15~~---15-15-15*15-----|
A|-----------15p13p12h13h15------------------------------|
E|-------------------------------------------------------|


E|-------------------------------------------------------------------|
B|-------------------------------------------------------------------|
G|-------------------------------------------------------------------|
D|-------------------------------------------5-5-5--3-3--7-7---------|
A|-5-----------5-------------5---------------3-3-3--1-1--5-5---------|
E|-3-3-3-3-3-3-3-3---3-3-3-3-3-3-3-3---3-3---------------------------|

E|-------------------------------------------------------------------|
B|-------------------------------------------------------------------|
G|-------------------------------------------------------------------|
D|-------------------------------------------5-5-5--3-3--7-7---------|
A|-5-----------5-------------5---------------3-3-3--1-1--5-5---------|
E|-3-3-3-3-3-3-3-3---3-3-3-3-3-3-3-3---3-3---------------------------|

E|-------------------------------------------------------------------|
B|-------------------------------------------------------------------|
G|-------------------------------------------------------------------|
D|-------------------------------------------5-5-5--3-3--7-7---------|
A|-5-----------5-------------5---------------3-3-3--1-1--5-5---------|
E|-3-3-3-3-3-3-3-3---3-3-3-3-3-3-3-3---3-3---------------------------|

E|-------------------------------------------------------------------|
B|-------------------------------------------------------------------|
G|-------------------------------------------------------------------|
D|-------------------------------------------5-5-5--3-3--7-7---------|
A|-5-----------5-------------5---------------3-3-3--1-1--5-5---------|
E|-3-3-3-3-3-3-3-3---3-3-3-3-3-3-3-3---3-3---------------------------|

[Final]

[Riff]

|----------------------------------------
|----------------------------------------
|-----------------------------0----------
|----------0----0h2----2h3------3--------  (3x)
|---1h3----------------------------------
|-3------3----3------3------3------------

|----------------------------------------
|----------------------------------------
|-----------------------------0----------
|----------0----0h2----2h3------3---*5---
|---1h3-----------------------------*5---
|-3------3----3------3------3------------

----------------- Acordes -----------------
A*  = X 0 2 2 2 0 - (*G# na forma de A)
B*  = X 2 4 4 4 2 - (*A# na forma de B)
Bm*  = X 2 4 4 3 2 - (*A#m na forma de Bm)
D*  = X X 0 2 3 2 - (*C# na forma de D)
E*  = 0 2 2 1 0 0 - (*D# na forma de E)
F#m*  = 2 4 4 2 2 2 - (*Fm na forma de F#m)
G*  = 3 2 0 0 0 3 - (*F# na forma de G)
