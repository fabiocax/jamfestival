Charlie Brown Jr. - Beco Sem Saída

(primeira vez dedilhado, na segunda normal)

G#                  A#                    Fm
As circunstâncias se tornaram um beco sem saída
G#                A#                  Fm
Seu orgulho te traiu e te jogou no chão
G#                 A#                   Fm
E as cicatrizes dessa história mal escrita
G#                 A#                   Fm
Se converteram no aprendizado da reconstrução

        G#           A#
Todos vivemos dias incríveis
        Fm
Que não passam de ilusão
        G#           A#
Todos vivemos dias difíceis
    Fm
Mas nada disso é em vão

   G#                           A#
Todo bem que você faz pra quem te ama

                 Fm
E quem te ama te faz
                                           G#
E isso tudo que te faz levar a vida na paz
                    A#
Só deus sabe quanto tempo
               Fm
O tempo deve levar...    ( inicio )

G#
Viver, viver e ser livre....
A#
Saber dar valor para as coisas mais simples
Fm
Só o amor constrói pontes indestrutíveis...    (2x)

G#                            A#
Arte maior é o jeito de cada um
          Fm                              G#
Vivo pra ser feliz, não vivo pra ser comum.


----------------- Acordes -----------------
A# = X 1 3 3 3 1
Fm = 1 3 3 1 1 1
G# = 4 3 1 1 1 4
