Charlie Brown Jr. - O Errado Que Deu Certo

Intro: (D#m F# C#9 B9)

(D#m F# C#9 B9)
Mudei minha história
E foi melhor pra mim
Nem sempre o que a vida me pede
É aquilo que a vida me dá
Se eu corro atrás do que é melhor pra mim
Deve ser porque meu tempo de volta
O seu dinheiro não trás

(D#m F# C#9 B9)
Com o coração na mão
Minha mente voa livre, leve, louco, bicho solto
Bem melhor, bem melhor
Faça da sua vida algo melhor
Do que já viu
E ouviu, se eu

(D#m F# C#9 B9)
Não busco a perfeição em ninguém

Não quero que busquem isso em mim
Eles querem que você se sinta mal
Pois assim eles se sentem bem
Mas tudo que é bom
Me lembra você
Vem comigo até o fim!


(D#m F# C#9 B9)
Mudei minha história
E foi melhor pra mim
Nem sempre o que a vida me pede
É aquilo que a vida me dá

(D#m F# C#9 B9)2x


(D#m F# C#9 B9)
Leve essa música
Me traz um sorriso
Leve essa música
Me traz um sorriso


É só ouvir o ritmo da música e tocar

Contribuição de Rafael Amaral

----------------- Acordes -----------------
B9 = X 2 4 4 2 2
C#9 = X 4 6 6 4 4
D#m = X X 1 3 4 2
F# = 2 4 4 3 2 2
