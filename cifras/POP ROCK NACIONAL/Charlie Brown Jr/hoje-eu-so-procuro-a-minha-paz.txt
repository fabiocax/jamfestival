Charlie Brown Jr. - Hoje Eu Só Procuro A Minha Paz

Versão 1
-======-

Afinar meio tom abaixo(Eb Ab Db Gb Bb Eb)
Obs: O riff inicial é tocado durante toda a música

Intro 4 vezes:
Só não vale correr
Não adianta correr
E|------------------------------------------
B|------------------------------------------
G|------------------------------------------
D|--2-2-2-2-2-2---4--5-5-5-5-5-5-2----------
A|--2-2-2-2-2-2---2--3-3-3-3-3-3-4-0--------
E|--0-0-0-0-0-0-0---0-----------------------

Depois da Intro
Tudo que faz querer levar a vida assim na
E|--0---0-------0---3-------2-------0---0-------0---3---5-3-2----
B|----3---3---3---3---3---3---3---3---3---3---3---3---3----------
G|----------4-----------4-------4-----------4--------------------
D|---------------------------------------------------------------
A|---------------------------------------------------------------
E|---------------------------------------------------------------


...paz
E|--/7-5-7-5-0---0-------0---3-------2-0---0---2-3-5----
B|-------------3---3---3---3---3---3-----3---3----------
G|-------------------4-----------4----------------------
D|------------------------------------------------------
A|------------------------------------------------------
E|------------------------------------------------------

Eu sei que eu já fiz muita coisa errada
Mas o fato de eu ser maluco não quer dizer
Que eu não de valor paras coisas
E nem pra você, e nem pra você

E|--7-----------7----0-------------------------------
B|--7-8-7-8-7-8-7------3-----------------------------
G|--9--------------------0--0------------------------
D|--9-------------------------4----------------------
A|--7---------------------------2--------------------
E|--------------------------------0------------------

Todas as poesias do mundo eu dedico a você
Todas as coisas que eu amo eu divido com você
(Com efeito, talvez trêmolo e delay)

E|--7-----
B|--8-----
G|--9-----

Eu sei que a vida é louca
E é difícil acreditar
O mundo é das pessoas que mandam em seus sentimentos
O mundo é das pessoas que sonham

2x
E|--5h7-0---0-5h7-0---0---2---3---2----------
B|--------5---------5---0---0---0------------
G|-------------------------------------------
D|-------------------------------------------
A|-------------------------------------------
E|-------------------------------------------

E o meu sonho é você, o meu sonho é você, essa eu fiz por você

E|----0---------0------0-----0-------------------
B|----0-------1---0--3-------0-------------------
G|------2-4-5-------------------4-5-4-2-2/4------
D|-----------------------------------------------
A|-----------------------------------------------
E|--0----------------------0---------------------

Essa eu fiz por voce, e por todos nós

E|----0----------0------0-----------------------------
B|----0--------1---0--3-------------------------------
G|-------2-4-5----------------------------------------
D|-------------------------2-2-2-2-2-2-2-2-2-2-2-2-2--
A|-------------------------2-2-2-2-2-2-2-2-2-2-2-2-2--
E|--0----------------------0-0-0-0-0-0-0-0-0-0-0-0-0--

Todos nós essa eu fiz foi por todos nós, essa eu fiz foi por todos nós

Guitarra 1(As duas guitarras repetir por oito compassos)
E|----0-------------------------0----------------------
B|----0-------------------------0----------------------
G|-----------------------------------------------------
D|--------4-5-5-5-5-5-5-5-2---------4-5-5-5-5-5-5-5----
A|------0-2-3-3-3-3-3-3-3-4-0-----0-2-3-3-3-3-3-3-3----
E|--0-------------------------0------------------------

Guitarra 2
E|--7-----7-----8----10-----10-----8----7-----7-----7----5-----5------8-----
B|----0-----0-----------0------0----------0-----0----------0-----0----------
G|------0-----0----0------0------0----0-----0-----0---0------0-----0----0---
D|--------------------------------------------------------------------------
A|--------------------------------------------------------------------------
E|-------------------------------------------------------------------------- 

Todas as pessoas que amam a liberdade, a paz
Todas pessoas as pessoas de valor pra liberdade, a paz
Pense em coisas boas sonhe com a pessoa amada

Pois o mundo e das pessoas que mandam em seus sentimentos
O mundo é das pessoas que sonham
E|--0---2---3---5---0-5h7---5h7------------
B|----0---0---0---0-------0-----0----------
G|-----------------------------------------
D|-----------------------------------------
A|-----------------------------------------
E|-----------------------------------------

Repete: O meu sonho é você...(Igual)
Repete: Essa eu fiz por você, e por todos nós...(igual)

Deixe-me furar as ondas
Deixe-me velos voar
Deixe-me tirar minha onda
Deixe-me vê-los voar
E|--7----------
B|--8----------
G|--9----------

A partir desse acorde acho que mais uns três versos
são tocados com muitos efeitos, o que deixa meio ''impossível''
tirar essas partes de ouvido.

Solo
E|------------10------------10------------0------0------0-------------
B|------------------------------12-10-12----0------0------0-----------
G|--7/9-11-12----12-11-7/9--------------------7------5------4---------
D|--------------------------------------------------------------------
A|--------------------------------------------------------------------
E|--------------------------------------------------------------------

E|------------10------------10------------0------0------0---------0--
B|------------------------------12-10-12----0------0------0----------
G|--7/9-11-12----12-11-7/9--------------------11-----12-----10-12----
D|-------------------------------------------------------------------
A|-------------------------------------------------------------------
E|-------------------------------------------------------------------

Igual ao solo
Tudo que faz  levar a vida na divina paz
Sei bem o que é bom pra mim,o que me mais me satisfaz
Tudo que me faz levar a vida na divina paz

Repete: Essa eu fiz por todos nós... (igual)

Charlie Brown
E|--7-----------7-------
B|--7-8-7-8-7-8-7-------
G|--9-------------------
D|--9-------------------
A|--7-------------------
E|----------------------

As flores são bonitas em qualquer lugar do mundo,
Muita gente tem forma mas não ten conteúdo
Eu sou alienado
Eu não vivo esse absurdo
Eu conheço o fim da linha
Eu era assim do submundo.
O medo vem fundo
Eu tenho tudo para chegar ao fim do poço de um mundo sujo.

O resto também têm muitos efeitos
A música pode continuar com o riff inicial
E terminar com esse acorde E|--12----
                           B|--12----
                           G|--12----

____________________________________________________________


Versão 2
-======-

Em C (toda a musica)

Intro 4 vezes:
Só não vale correr
Não adianta correr

Em                           C
Tudo que faz querer levar a vida assim na paz

Em                          C
Eu sei que eu já fiz muita coisa errada
Mas o fato de eu ser maluco não quer dizer
Que eu não de valor paras coisas
E nem pra você, e nem pra você

Em                             C
Todas as poesias do mundo eu dedico a você
Todas as coisas que eu amo eu divido com você
(Com efeito, talvez trêmolo e delay)

Em               C
Eu sei que a vida é louca
E é difícil acreditar
O mundo é das pessoas que mandam em seus sentimentos
O mundo é das pessoas que sonham

Em                C     Em           C      Em            C
E o meu sonho é você, o meu sonho é você, essa eu fiz por você
Em                C         Em
Essa eu fiz por você, e por todos nós
Em                          C              Em                  C
Todos nós essa eu fiz foi por todos nós, essa eu fiz foi por todos nós

Em                           C
Por todas as pessoas que amam a liberdade, a paz
Todas pessoas as pessoas de valor pra liberdade, a paz
Pense em coisas boas sonhe com a pessoa amada

Em                               C
Pois o mundo e das pessoas que mandam em seus sentimentos
O mundo é das pessoas que sonham

Em                C     Em           C      Em            C
E o meu sonho é você, o meu sonho é você, essa eu fiz por você
Em                C         Em
Essa eu fiz por você, e por todos nós
Em                          C              Em                  C
Todos nós essa eu fiz foi por todos nós, essa eu fiz foi por todos nós...

Em             C
Deixe-me furar as ondas
Deixe-me ve-los voar
Deixe-me tirar minha onda
Deixe-me vê-los voar

Em             C
Tudo que faz  levar a vida na divina paz
Sei bem o que é bom pra mim,o que me mais me satisfaz
Tudo que me faz levar a vida assim na paz

Em                      C                    Em                        C
Fiz por todos nós, essa eu fiz foi por todos nós, essa eu fiz por todos nós,essa eu fiz por todos nós...  (2x)

Em C (2x)

Charlie Brown

Em             C
As flores são bonitas em qualquer lugar do mundo,
Muita gente tem forma mas não ten conteúdo
Eu sou alienado
Eu não vivo esse absurdo
Eu conheço o fim da linha
Eu era assim do submundo.
O medo vem fundo
Eu tenho tudo para chegar ao fim do poço de um mundo sujo.

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Db = X 4 6 6 6 4
E = 0 2 2 1 0 0
Eb = X 6 5 3 4 3
Em = 0 2 2 0 0 0
Gb = 2 4 4 3 2 2
