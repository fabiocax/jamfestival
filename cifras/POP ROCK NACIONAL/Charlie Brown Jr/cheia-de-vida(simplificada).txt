Charlie Brown Jr. - Cheia de Vida

[Intro]

E|-------4----2h4p2---2--------------------------4--2--4--2------|
B|--2---------------------5---2---4----5-------2-------------5--2|
G|---------------------------------------------------------------|
D|---------------------------------------------------------------|
A|--4---------2--------------0----------------4----2------------0|
E|-------0---------------------------------------0---------------|

C#m           E            B                       A
Atenção rapaziada a dona do sorriso mais lindo vai passar
C#m                 E            B                    A
E quando ela passar eu não quero ver ninguém perdendo a linha
C#m               E               B    A
Ela vem com o seu sorriso estonteante
C#m                  E
Dona de um andar que é só dela
B                           A
E com uma personalidade única

       C#m           E  B
Você é tão cheia de vida

A                       C#m  E  B
Que ninguém resiste a você
       C#m           E  B
Você é tão cheia de vida
A                       C#m  E  B
Que ninguém resiste a você

C#m           E            B                       A
Quando você encontra uma pessoa, que te faz esquecer tudo, que te faz tão bem
C#m      E                      B  C#m  A
Até numa fase problema e você nem lembra
C#m           E            B                       A
Quando você encontra uma pessoa, que te faz esquecer tudo, que te faz tão bem
C#m      E                      B  C#m  A      B  C#m  A
Até numa fase problema e você nem lembra...nem lembra

C#m                      E
Eu quero que você fique segura
             B                       A
Fazer amor contigo na minha cama ou na sua
                 C#m                  E
E uma dose muito forte não vai ser suficiente
                  B                     A
Me sinto mais em casa quando você está presente

Então
     C#m             E                       B              A
Fica mais, fica mais um pouco, porque muito de você pra mim ainda é pouco

Então
      C#m            E         B       A     B
Fica mais fica mais um pouco, você me deixa muito louco

       C#m           E  B
Você é tão cheia de vida
A                       C#m  E  B
Que ninguém resiste a você
       C#m           E  B
Você é tão cheia de vida
A                       C#m  E  B
Que ninguém resiste a você

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
