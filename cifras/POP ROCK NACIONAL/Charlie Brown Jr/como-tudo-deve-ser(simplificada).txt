Charlie Brown Jr. - Como Tudo Deve Ser

(Introdução)

(Primeira Parte)
         A5(9)                  F#m7(11)
Um belo sonho veio então despertar minha vontade,
 C#m7
Tudo vale apena pra te reencontrar,
    E5(9)
Me livrei de tudo aquilo e consegui mudar,
 A5(9)                       F#m7(11)
Tudo que foi feito em troca de uma amizade mas,
  C#m7
Felicidade é poder estar
      E5(9)
Com quem você gosta em algum lugar,
   A5(9)               F#m7(11)          C#m7
É foda ser louco, advogado do mundo mas,
                E5(9)
Como tudo deve ser?,

   A5(9)              F#m7(11)            C#m7
É foda ser taxado de doido, vagabundo mas,

                E5(9)
Como tudo deve ser?,

(Refrão)
            F#m7(11)     A5(9)
Foi quando te encontrei,
            F#m7(11)     A5(9)
Ouvindo um som e olhando o mar,
            F#m7(11)     A5(9)
Foi quando te encontrei,
           F#m7(11)     A5(9)
Ouvindo o som do mar rolar...

(Primeira Parte com Variação na Letra)
A5(9)
      Eu não nasci ontem,
      F#m7(11)                 C#m7
Nem quando, como por onde mas,
                   E5(9)
Como tudo deve ser,
                                 A5(9)
Com as balizas do nosso sistema
         F#m7(11)                   C#m7
Sigo imprimindo meu sonho na história,
           E5(9)
Como tudo deve ser,
         A5(9)                  F#m7(11)
Um belo sonho veio então despertar minha vontade,
 C#m7
Tudo vale apena pra te reencontrar,
    E5(9)
Me livrei de tudo aquilo e consegui mudar,
 A5(9)                       F#m7(11)
Tudo que foi feito em troca de uma amizade mais...

(Refrão)
            F#m7(11)     A5(9)
Foi quando te encontrei,
            F#m7(11)     A5(9)
Ouvindo um som e olhando o mar,
            F#m7(11)     A5(9)
Foi quando te encontrei,
           F#m7(11)     A5(9)
Ouvindo o som do mar rolar...

(Introdução)

(Primeira Parte com Variação na Letra)
           A5(9)
Eu não preciso de promessas
     F#m7(11)
E acho que você também,
        C#m7                       E5(9)
Eu não tento ser perfeito e acho que você também.
A5(9)                    F#m7(11)
      Dias e noites, pensando no que fiz,
    C#m7                  E5(9)
Eu sou um vencedor, eu lutei pelo o que eu quis,
      A5(9)                    F#m7(11)
Mas quando não se pode mais mudar
               C#m7  E5(9)
Tanta coisa errada,
                    A5(9)   F#m7(11)
Vamos viver nossos sonhos,
                 C#m7  E5(9)
Temos tão pouco tempo.

(Repete o Refrão)

(Solo)

(Repete o Refrão)

F#m7(11)
         O mar...

----------------- Acordes -----------------
A5(9) = X 0 2 2 0 0
C#m7 = X 4 6 4 5 4
E5(9) = 0 2 4 4 0 0
F#m7(11) = 2 X 2 2 0 X
