Charlie Brown Jr. - Senhor do Tempo

Intro:

E|-----------------------------------------
B|-------2---------------------------------
G|-----------------------------------------
D|---3h4-------6---4--------4h6--1h3-------
A|-4---------7---7------4------------------
E|--------------------7---7----------------

E|-----------------------------------------
B|-----2-----------------------------------
G|-----------------------------------------
D|-3h4-------6---4--------4h6--------------
A|-4-------7---7------4--------------------
E|------------------7---7------------------

Primeira Parte:

   C  E6(9) B5(9)
E|----2------------------------------------
B|-2--2-----2------------------------------
G|-1--1-----4------------------------------
D|-3--2-----4------------------------------
A|-4--------2------------------------------
E|-----------------------------------------



Variação 2:

   C  E6(9) B5(9)
E|-----------------------------------------
B|-2--2-----2------------------------------
G|-1--1-----4------------------------------
D|-3--2-----4------------------------------
A|-4--------2------------------------------
E|-----------------------------------------

Variação 3:

   C         E6(9) B5(9)
E|-----------------------------------------
B|-------2-----2-----2---------------------
G|-----1-1-----1-----4---------------------
D|---3---3---2-2-----4---------------------
A|-4-----4---------2-----------------------
E|-----------------------------------------

Variação 4:

   C      E6(9)    B5(9)
E|----0---------2--------------------------
B|-2--2---------2--------2-----------------
G|-1--1-----1---1--------4-----------------
D|-3--3---2---2------4---4-----------------
A|-4--4------------2---2-------------------
E|-----------------------------------------


        C
Eu não sou o senhor do tempo, mas eu sei que vai chover
    D#6(9)                 B5(9)
Me sinto muito bem quando fico com você
    C
Eu tenho habilidade de fazer histórias tristes
   D#6(9)              B5(9)
Virarem melodia vou vivendo o dia-a-dia.
    C                                        D#6(9)
Na paz, na moral, na humilde busco só sabedoria
                 B5(9)
Aprendendo todo dia,
                 C
me espelho em você, corro junto com você,
                  D#6(9)              B5(9)
Vivo junto com você, faço tudo por você.
              C
Seguindo em frente com fé e atenção
                D#6(9)             B5(9)
Continuo na missão continuo por você e por mim
         C
Porque quando a casa cai
                                  D#6(9)
Não dá pra fraquejar, quem é guerreiro tá ligado
         B5(9)
Que guerreiro é assim.

Refrão:

                 C
E|------------------13---------------------
B|-----------------------14----------------
G|-------3----1--0-------------------------
D|---3h4---------3-------------------------
A|---------------4-------------------------
E|-2--------2------------------------------

                 C
E|------------------1----------------------
B|---------------------2-------------------
G|-------3----1--0-------------------------
D|---3h4---------3-------------------------
A|---------------4-------------------------
E|-2--------2------------------------------


         F
O tempo passa e um dia a gente aprende
         C
Hoje eu sei realmente o que faz a minha mente
         F
Eu vi o tempo passar vi pouca coisa mudar
       C
Então tomei um caminho diferente
       F
Tanta gente equivocada faz mal uso da palavra
        C
Falam, falam o tempo todo mas não tem nada a dizer
        F
Mas eu tenho santo forte é incrível a minha sorte
     C
Agradeço todo tempo por ter encontrado você.

(Primeira parte com variação na letra)
           C
O tempo é rei, e a vida é uma lição
                   D#6(9)
E um dia a gente cresce
                   B5(9)               C
E conhece nossa essência e ganha experiência
                                        D#6(9)  B5(9)
E aprende o que é raiz então cria consciência.
     C
Tem gente que reclama da vida o tempo todo
       D#6(9)             B5(9)
Mas a lei da vida é quem dita o fim do jogo
          C
Eu vi de perto o que neguinho é capaz por dinheiro
         D#6(9)               B5(9)
Eu conheci o próprio lobo na pele de um cordeiro
        C
Infelizmente a gente tem que tá ligado o tempo inteiro
              D#6(9)      B5(9)
Ligado nos pilantra e também nos bagunceiro
     C
E a gente se pergunta porque a vida é assim?
     D#6(9)               B5(9)
É difícil pra você e é difícil pra mim.

(Riff Introdução)
Eu não sou o senhor do tempo, mas eu sei que vai chover
Me sinto muito bem quando fico com você
Eu tenho habilidade de fazer histórias tristes
Virarem melodia vou vivendo o dia-a-dia
Na paz, na moral, na humilde busco só sabedoria
Aprendendo todo dia me espelho em você
Corro junto com você, vivo junto com você, faço tudo por você.

(Segunda Parte)
Vivendo nesse mundo louco hoje só na brisa
Viver pra ser melhor também é jeito de levar a vida

Refrão:

         F
O tempo passa e um dia a gente aprende
         C
Hoje eu sei realmente o que faz a minha mente
         F
Eu vi o tempo passar vi pouca coisa mudar
       C
Então tomei um caminho diferente
       F
Tanta gente equivocada faz mal uso da palavra
        C
Falam, falam o tempo todo mas não tem nada a dizer
        F
Mas eu tenho santo forte é incrível a minha sorte
     C
Agradeço todo tempo por ter encontrado você.

(Segunda Parte com variação na letra)
Vem que o bom astral vai dominar o mundo!
Eu já briguei com a vida, hoje eu vivo bem com tudo mundo aí
Na maior moral...Charlie Brown!

(Riff Introdução)
Vivendo nesse mundo louco hoje só na brisa
Viver pra ser melhor também é um jeito de levar a vida.

----------------- Acordes -----------------
B5(9) = X 2 4 4 2 2
C = X 3 2 0 1 0
D#6(9) = X X 1 0 1 1
E6(9) = X 7 6 6 7 7
F = 1 3 3 2 1 1
