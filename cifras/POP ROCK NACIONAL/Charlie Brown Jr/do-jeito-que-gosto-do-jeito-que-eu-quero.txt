Charlie Brown Jr. - Do Jeito Que Gosto, do Jeito Que Eu Quero

Intro: Riff 1 (12x)

E|--------------------------------------------------
B|--------------------------------------------------
G|--------------------------------------------------
D|---1---0------------------------------------------
A|-3--3-1--1----------------------------------------
E|--------------------------------------------------


Riff 1
 Hoje eu não estou pra ninguém
Só estou pra ela
Ela vem de branco pra fazer mais uma história
Ponto alto da loucura, enlouquece e fica nua
Vem que eu te quero também
Menina linda
Do jeito que eu te olho dá pra ver que tem história
E eu sou louco por você e o que interessa é aqui e agora
D#9  C#9 (Uma palhetada para baixo para cada acorde)
Riff 1

Vem que eu te quero também
Menina linda (vem)
E|6-4-3---------------------------------------------
B|-------4-6-1-4-6-1-4-6-1--------------------------
G|--------------------------------------------------
D|--------------------------------------------------
A|--------------------------------------------------
E|--------------------------------------------------

D#9  C#9
Vem que eu te quero também
Menina linda (vem)
E|6-4-3---------------------------------------------
B|-------4-6-1-4-6-1-4-6-1--------------------------
G|--------------------------------------------------
D|--------------------------------------------------
A|--------------------------------------------------
E|--------------------------------------------------

D#9  C#9
C5 A#5

Cm                          Bb9
A vida é uma boca a ser beijada
      Cm                     Bb9
Mas a vida só gosta de quem gosta de viver
       Cm                   Bb9
A vida não quer ser desperdiçada
    Cm                          Bb9
Aproveite o seu tempo, está tudo aí pra você
7x
E|--------------------------------------------------
B|--------------------------------------------------
G|--------------------------------------------------
D|--------------------------------------------------
A|-3-6-3-1-4-1-/3-6-3-1-4-1-------------------------
E|--------------------------------------------------

Riff1
Guarde o melhor para mim
Hoje me espera
Do jeito que eu te pego, eu só largo quando gozo
Eu sou seu dono, eu sei eu posso
Eu sou o dono da favela
D#9  C#9
Riff1
Ela nasceu para mim e eu pra ela
Num dia frio, ensolarado eu quero ela do meu lado
Nosso amor tem muita força
Já passamos muitas coisas
D#9  C#9
Riff 1
Vem que eu te quero também
D#9  C#9
Menina linda (vem)
Riff 1
Vem que eu te quero também
Menina linda (vem)
Riff1 (7 ou 8x)
Cm                          Bb9
A vida é uma boca a ser beijada
      Cm                     Bb9
Mas a vida só gosta de quem gosta de viver
       Cm                   Bb9
A vida não quer ser desperdiçada
    Cm                          Bb9
Aproveite o seu tempo, está tudo aí pra você


Cm         Bb9
Eu amo, eu vivo
   Cm
Do jeito que gosto
    Bb9
Do jeito que eu quero

Cm         Bb9
Eu amo, eu vivo
   Cm
Do jeito que gosto
    Bb9
Do jeito que eu quero

E|--------------------------------------------------
B|--------------------------------------------------
G|--------------------------------------------------
D|--------------------------------------------------
A|-3-6-3-1-4-1-/3-6-3-1-4-1--------------------------
E|-------------------------------------------------- 4x

  D#9  C#9
E|6-4-3---------------------------------------------
B|-------4-6-1-4-6-1-4-6-1--------------------------
G|--------------------------------------------------
D|--------------------------------------------------
A|--------------------------------------------------
E|--------------------------------------------------

  D#9  C#9
E|6-4-3---------------------------------------------
B|-------4-6-1-4-6-1-4-6-1--------------------------
G|--------------------------------------------------
D|--------------------------------------------------
A|--------------------------------------------------
E|--------------------------------------------------

D#9

----------------- Acordes -----------------
A#5 = X 1 3 3 X X
Bb9 = X 1 3 3 1 1
C#9 = X 4 6 6 4 4
C5 = X 3 5 5 X X
Cm = X 3 5 5 4 3
D#9 = X 6 8 8 6 6
