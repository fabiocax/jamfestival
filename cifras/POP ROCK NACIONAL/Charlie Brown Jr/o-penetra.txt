Charlie Brown Jr. - O Penetra

Intro: Solo (Toca o solo 2x)

E|----------------------------------------------------------------------|
B|----------------9-9-9-------------------------------------------------|
G|--------8-8-8-----------8-8-8----------8-8-8-----6-6-6----------------|
D|----6---------------------------6-6-6--------------------9-8-9--------|
A|----------------------------------------------------------------------|
E|----------------------------------------------------------------------|

E|----------------------------------------------------------------------|
B|---------------------7-7-7--------------------------------------------|
G|------------------------------9--8--9--8--9--8-8-------------6--8-----|
D|---9-9-9--8--6--8--------------------------------8---6~8--9-----------|
A|----------------------------------------------------------------------|
E|----------------------------------------------------------------------|

E|---------------------------------------------------------------------|
B|--------7-------------7----------------------------------------------|
G|------------9----9------9-----9--------------------------------------|
D|---8~9--------9------------9-----------------------------------------|
A|---------------------------------------------------------------------|
E|---------------------------------------------------------------------|


Parte 1
(Acordes em forma de F)   ( O Em toca só as cordas E,A e D)

                G#m                        B
   Nem vou te lembrar dos motivos que me trazem até aqui, mas
    F#                        E
   Diz que eles fizeram bem melhor mais eu nem vi, não
    G#m                      B                     F#
   Não me convidaram mas entrei  tô dentro agora eu sei
                   E
   vou falar dos poderes de outra lei
                   G#m              B
   Você é tudo sofisticado quer na hora e paga em cheque
    F#m                              E
   Olha pra mim do jeito como se eu fosse algum pivete
   G#m                         B
   Se não tenho gangue o que corre sangue
           F#m                           E
   hoje a noite vai ser louca e hoje eu quero fugir daqui

Refrão:
G#m               B                 F#m        E
 Talvez eu ja nem lembre mais das coisas como eram
G#m               B                 F#m        E
 mas talvez agora saiba bem o que é bom pra mim
G#m               B           F#m               E                     G#m
 Esqueceram de me avisa, nem olhou pra tráz mas bem que eu não me importei
              B            F#m        E
 O excesso me faz sonhar, correr atrás


(Toca o solo 2x novamente)

E|----------------------------------------------------------------------
B|----------------9-9-9-------------------------------------------------
G|--------8-8-8-----------8-8-8----------8-8-8-----6-6-6----------------
D|----6---------------------------6-6-6--------------------9-8-9--------
A|----------------------------------------------------------------------
E|----------------------------------------------------------------------

E|----------------------------------------------------------------------
B|---------------------7-7-7--------------------------------------------
G|------------------------------9--8--9--8--9--8-8-------------6--8-----
D|---9-9-9--8--6--8--------------------------------8---6~8--9-----------
A|----------------------------------------------------------------------
E|----------------------------------------------------------------------

E|----------------------------------------------------------------------
B|--------7-------------7----------------------------------------------
G|------------9----9------9-----9--------------------------------------
D|---8~9--------9------------9-----------------------------------------
A|----------------------------------------------------------------------
E|----------------------------------------------------------------------



(Repete a parte 1 com o refrão)

(G#m B F#m E)
   G#m           B        F#m    E
 3X O excesso me faz sonhar

(G#m B F#m E)
2X Bem que eu não me emportei...

Interlude:
E|--------------------------------------------------------------------------------------------------------
B|--10-10-10-10-------------------------------------------------------------------------------------------
G|--------------------5--5--5--5----12--12--12--12----10--10--10--10--------------------------------------
D|--7--7--7--7--------------------------------------------------------------------------------------------
A|--------------------3--3--3--3----10--10--10--10----8--8--8--8--8---------------------------------------
E|--------------------------------------------------------------------------------------------------------

E|--------------------------------------------------------------------------------------------------------
B|--------------------------------------------------------------------------------------------------------
G|5-55----5-55------5-55------5-555----10-1010----10-1010----10--------10-10-10-10----10-10-10
Db|7>bend--7>bend----7>bend----7>bend---12>bend----12>bend----12>bend---12>bend---------------------------
A|--------------------------------------------------------------------------------------------------------
E|--------------------------------------------------------------------------------------------------------

E|----------------------------------------------------------
B|----------------------------------------------------------
G|-------4----4-5--------4----4------7777----5~~~~~~--------
D|- 7-7----7--------7-7----7--------------------------------
A|----------------------------------------------------------
E|----------------------------------------------------------

E|--------------------------------------------------------------------------------------------------------
B|--------------------------------------------------------------------------------------------------------
G|5-55----5-55------5-55------5-555----10-1010----10-1010----10--------10-10-10-10----10-10-10------------
Db|7>bend--7>bend----7>bend----7>bend---12>bend----12>bend----12>bend---12>bend----------------------------
A|--------------------------------------------------------------------------------------------------------
E|--------------------------------------------------------------------------------------------------------

(G#m B F#m E)
2X  Se não tenho gangue o que corre sangue
      hoje a noite vai ser louca e hoje eu quero fugir daqui

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
