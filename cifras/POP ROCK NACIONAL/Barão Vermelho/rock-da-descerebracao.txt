Barão Vermelho - Rock da Descerebração

INTRODUÇÃO : (G7)
            G7                                                   C7
DESCEREBREM-SE, CELEBREM, EU TÔ AQUI ANIMAR .
            G7                                                   C7
DESESPEREM-SE, ROUBEM, QUEM SABE EU POSSA AJUDAR.
       Em                                                                   A
DEPOIS DESCULPEM-SE, ESQUEÇAM, EU VOLTO PRA LEMBRAR.
                G                                        D                                               Eb      C7 G7
QUE HABITUEM-SE, MORRAM, EU É QUE NÃO VOU ENTERRAR.
Em                                                 A                                      Em
E SE A PIOR PESSOA DA CIDADE ME AJUDAR EU VIRO OPERÁRIO-PADRÃO.
A                                                         C                                          Eb                              C7
EU, O MEU PATRÃO, QUE SE ESCONDE NOS FUNDOS GELADOS DE FELICIDADE.
    G7                                               C7
CAGÜETEM-SE, SOLITÁRIOS, ANTES DO INTERROGATÓRIO.
       Em                                              A
ENGRANDEÇAM A MENTIRA, DÊEM SENTIDO À VIDA.
  Em                                                         A
TENHAM FÉ, TENHAM MEDO, OU USEM ANESTESIAS.
 C7                                           Eb                                      G7
UNIFORMES, FANTASIAS, VEJAM, QUE LIQUIDAÇÃO.

Em                                                                      A
E SE AS SUAS CONSCIÊNCIAS TÃO BONDOSAS, DIZEM NÃO.
Em                                          C                                           G           Em
É UM BOM MOTIVO PRA GENTE COMEMORAR O ROCK, O ROCK,
C                                         D                                 G Em C                     D           F             G7
DA DESCEREBRAÇÃO, DA DESCEREBRAÇÃO,       O ROCK DA DESCEREBRAÇÃO,

    G7                                               C7
CAGÜETEM-SE, SOLITÁRIOS, ANTES DO INTERROGATÓRIO.
       Em                                              A
ENGRANDEÇAM A MENTIRA, DÊEM SENTIDO À VIDA.
  Em                                                         A
TENHAM FÉ, TENHAM MEDO, OU USEM ANESTESIAS.
 C7                                           Eb                                      G7
UNIFORMES, FANTASIAS, VEJAM, QUE LIQUIDAÇÃO.
Em                                                                      A
E SE AS SUAS CONSCIÊNCIAS TÃO BONDOSAS, DIZEM NÃO.
Em                                          C                                           G           Em
É UM BOM MOTIVO PRA GENTE COMEMORAR O ROCK, O ROCK,
C                                         D                                 G Em C D                                 G
DA DESCEREBRAÇÃO, DA DESCEREBRAÇÃO, O ROCK DA DESCEREBRAÇÃO,

----------------- Acordes -----------------
A = X 0 2 2 2 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
D = X X 0 2 3 2
Eb = X 6 5 3 4 3
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
