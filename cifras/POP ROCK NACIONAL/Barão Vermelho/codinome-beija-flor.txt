Barão Vermelho - Codinome Beija-Flor

A       A7M      A        A7M
Pra que mentir, fingir que perdoou
    D7M          E/D       F#m7
Tentar ficar amigos sem rancor
    D7M      E/D       D7M              E/D
A emoção acabou, que coincidência é o amor
   Bm7   C#m7   D7M   D/E    F7M
A nossa música nunca mais tocou

A       A7M     A        A7M
Pra que usar de tanta educação
     D7M         E/D        F#m7
Pra destilar terceiras intenções
   D7M              E/D     D7M               E/D
Desperdiçando o meu mel, devagarzinho flor em flor
Bm7      C#m7    D7M     D/E   F   F/G
Entre os meus inimigos, Beija-Flor

C7M                    Bm7 E7   F7M         G7        A7M
Eu protegi seu nome por amor,   em um codinome Beija-Flor
C7M                      Bm7 E7     F7M                G         A7M
Não responda nunca meu amor,nunca    pra qualquer um na rua Beija-Flor

F7M            G     F7M                   G
Que só eu que podia, dentro da tua orelha fria
F7M               G          A7    F7M          G
Dizer segredos de liquidificador, voce sonhava acordada
F7M                    G      F7M              G               A7M
Um jeito de não sentir dor, prendia o choro e aguava o bom do amor
  F7M             G7M              A7M
Prendia o choro e aguava o bom do amor

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
A7M = X 0 2 1 2 0
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
C7M = X 3 2 0 0 X
D/E = X X 2 2 3 2
D7M = X X 0 2 2 2
E/D = X 5 X 4 5 4
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
F#m7 = 2 X 2 2 2 X
F/G = 3 X 3 2 1 X
F7M = 1 X 2 2 1 X
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
G7M = 3 X 4 4 3 X
