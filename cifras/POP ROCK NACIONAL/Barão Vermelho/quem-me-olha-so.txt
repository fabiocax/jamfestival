Barão Vermelho - Quem Me Olha Só

 Intro: C7   F   Bb   F

 F
   Já reguei quase todas as plantas
 Bb
   Já chorei sobre todo jardim
 F
   Elas gostam da chuva que molha
 Bb
   Elas pensam
 Que o sol é ruim
 F                        A7
   Quando o sol nos meus olhos
      brilhava
 Bb
   Por amar minha flor tanto assim
 F                  A7
   Fui feliz sem saber que secava
    Bb       G7
 A rosa trazia o seu fim
 F                         Bb
   Hoje sente dó, quem me olha só

 F                       Bb
   Entre flores, folhas e capim
 F              A7
 Elas gostam da chuva que molha
 Bb                 G7
   Se alimentam do mal que há em mim
 F                A7
   Elas gostam da chuva que molha
 G7                F#7
   Se alimentam do mal que há em mim
 F                        Bb
   Hoje sente dó, quem me olha só
    F                    Bb
 Eu tenho o carinho do espinho
 F                       A7            Bb
   Hoje sente dó quem me olha sozinho
 F                        Bb
   Hoje sente dó, quem me olha só
    F           Bb
 Eu tenho os espinhos do carinho
 F                       A7            Bb
   Hoje sente dó quem me olha sozinho
 F
   Já reguei quase todas as plantas
 Bb
   Já chorei sobre todo jardim
 F
   Elas gostam da chuva que molha
 Bb
   Elas pensam
   Que o sol é ruim
 F                       A7
   Quando o sol nos meus olhos
      brilhava
 Bb
   Por amar minha flor tanto assim
 F                 A7
   Fui feliz sem saber que secava
   Bb         G7
 A rosa e trazia o seu fim
 F                        Bb
   Hoje sente dó, quem me olha só
 F                          Bb
   Eu tenho o carinho do espinho...

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bb = X 1 3 3 3 1
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
F#7 = 2 4 2 3 2 2
G7 = 3 5 3 4 3 3
