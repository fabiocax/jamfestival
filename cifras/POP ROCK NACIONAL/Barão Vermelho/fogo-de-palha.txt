Barão Vermelho - Fogo de Palha

Afinação Ré Aberto: D  A  D  F#  A  D

(Tocar acordes com Slide)

[Intro] D  F  E  D

D
Esse seu fogo
C
É fogo de palha
G
Toda essa bala
     D
Não quer dizer nada

     D
Não atinge ninguém
      C
É tão bem comportada
      G
Não arrisca nada

       D
Só pra se manter

G              E            D
Parece que não é de carne e osso
G         E                 A
Cumpre bem as regras do bem gosto

D      C                D
Aposto tudo no rei de espadas
          G              D
O que ela diz, não diz nada
D      C                    D
Aposto tudo aquilo que eu tiver

(Nessa parte os acordes são tocados com os dedos)

        G        Gm          D
Mesmo que eu não tenha quase nada
        G        Gm          D
Mesmo que eu não tenha quase nada

  | 2x                              | Com slide   |
D |------------------0------0-------|-/12-12-12\--|
A |--------------------0------0-----|-/12-12-12\--|
F#|-----0----0-----------0------0---|-/12-12-12\--|
D |---0----0-----0----------------0-|-------------|
A |--3----2-------------------------|-------------|
D |------------3---0----------------|-------------|




----------------- Acordes -----------------
A = X 0 2 2 2 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
G2 = 3 X 0 2 0 X
Gm = 3 5 5 3 3 3
