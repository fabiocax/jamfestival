Barão Vermelho - Guarda Essa Canção

Intro: G#m  C#m/Bb  B  C#m  (Eb7/9+)

G#m                                                    E7+/B
Era só um outro tempo de encantos escondidos em certas pessoas
            Eb7                     G#m           C#m/Bb  B
Como ela especiais, no seu jeito de se entregar
G#m
Tinha um nome que nadava como um cisne, encantando as pessoas
               E7+/B                          Eb7
E uma luz que amarela, hoje cisma em me despertar
G#m                   B                   E7+/B     Eb7
Eu que não esqueço das longas noites no Leblon
G#m       B              E7+/B                 Eb7
Olhos, boca, pés e tudo mais,  muito acima do chão
G#m
Guarda essa canção
                B
Como o último desejo
              E7+/B
Do fundo do coração
Eb7                    G#m  C#m/Bb  B  C#m  (Eb7/9+)
      À superfície do beijo...

G#m
Esse tempo todo que foi lindo não é pra cabar assim
                                  E7+/B                Eb7
Chega então pra dentro do meu sonho e conte de mim
G#m
Vamos beber do mesmo copo, nosso corpo celebrar
                     E7+/B                 Eb7
Um tempo que se foi e a sorte que ele nos lançou
    G#m               G#m7+
Sonhando viver entrelaçados
B               E7+/B
Copo de vinho na mão
                  Eb7
Num blues carregado
G#m               C#m/Bb     B      C#m             Eb7
Foi o fim daquele tempo e a sorte,    ela te levou irmã
G#m
Guarda essa canção
                B
Como o último desejo
               E7+/B
Do fundo do coração
Eb7                   G#m  C#m/Bb  B  C#m  (Eb7/9+)
      À superfície do beijo...
((G#m) E7  Eb7)  G#m  C#m/Bb  B  C#m  (Eb7/9+)
G#m                  B                    E7+/B     Eb7
Eu que não esqueço das longas noites no Leblon
G#m        B              E7+/B               Eb7
Olhos, boca, pés e tudo mais,  muito acima do chão
G#m
Guarda essa canção
                B
Como o último desejo
                E7+/B
Do fundo do coração
                Eb7
À superfície do beijo...
G#m
Guarda essa canção
                 B
Como o último desejo
              E7+/B
Do fundo do coração
Eb7                   G#m  C#m/Bb  B  C#m  (Eb7/9+)
      À superfície do beijo...

----------------- Acordes -----------------
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
C#m/Bb = X X 8 6 5 4
E7 = 0 2 2 1 3 0
E7+/B = X 2 2 1 4 X
Eb7 = X 6 5 6 4 X
Eb7/9+ = X 6 5 6 7 X
G#m = 4 6 6 4 4 4
G#m7+ = X X 6 8 8 7
