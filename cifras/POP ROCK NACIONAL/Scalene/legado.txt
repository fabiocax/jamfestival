Scalene - Legado

Riff:
E|----------------------
B|-------13-11-11h13p11-
G|-12-15----------------
D|---------------------- (3x)
A|----------------------
E|----------------------

E|----------------------
B|-------13-16-15-13----
G|-12-15----------------
D|----------------------
A|----------------------
E|----------------------


Gm            Bb
Hoje me encontrei
D#5         Cm
Tão longe de mim
        Gm          Bb
Tantas tropas eu armei

     D#5         Cm
Com lanças de marfim
    Gm        Bb
Um palácio de rubi
D#5            Cm
Fiz pra sustentar
        Gm           Bb
Tantos sonhos que previ
D#5          Cm
Ali pra alcançar

D#5          Cm
Tanto quero, tanto busco
Gm
Mas quando acabar
F
O que deixo aqui?

Gm Bb D#5 Cm (2X)

Gm            Bb
O homem se perdeu
D#5         Cm
Cego já não vê
        Gm       Bb
Além do que o colocar
 D#5         Cm
No mais belo altar
   Gm               Bb
A Ilusão que está completo
   D#5          Cm
Lentamente o desfaz
      Gm             Bb
Tudo em busca de algo tolo
        D#5       Cm
Que seu ego satisfaz

D#5          Cm
Tanto quero, tanto busco
Gm
Mas quando acabar
F
O que deixo?
D#5        Cm
Meu legado foi manchado
Gm           F
Por não ver além do que eu quis

Gm         Bb
Só me sentirei
D#5           Cm
Pronto pra partir
Gm          Bb
Quando me doar
D#5            Cm
Pelos outros, ser

Gm   Bb
Ser
D#5  Cm
Ser
Gm   Bb
Ser
D#5   Cm  Gm
Ser

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
D#5 = X 6 8 8 X X
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
