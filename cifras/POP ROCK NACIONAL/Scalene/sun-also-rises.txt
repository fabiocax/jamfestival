Scalene - Sun Also Rises

[Intro] Cm

Cm
It's kind of hard to wish you well
G#              A#             Cm
But its kind of hard to wish you not

Play you cards, but play them well
G#                A#                Cm
Cause I know this game of yours its poison

Cm
We have been wrong when saying we
G#               A#              Cm
Are just another ship thats been off shore
Cm
Cause Id say through all the storms
G#                     A#               Cm
We have managed to turn into something more

( D# F Cm )


D#            F
And like old times, we should not care
Cm                      D#             F
About others say or do, cause in the end
Cm
   My poison is you

( G# Cm )

    G   A#              F
How long can we keep on living like this?
Cm
How long?

( G A# F )

( G# A# )

[Solo] Cm G# A#

D#            F
And like old times, we should not care
Cm                      D#             F
About others say or do, cause in the end
Cm
   My poison is you

[Final] C#

----------------- Acordes -----------------
A# = X 1 3 3 3 1
C# = X 4 6 6 6 4
Cm = X 3 5 5 4 3
D# = X 6 5 3 4 3
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
