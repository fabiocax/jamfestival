Scalene - Marco Zero

Intro 2x: Dm Dsus4 Am Am5+ Gm

Dm              Am
Viver a realidade eu vou
Dm              Am
Do cotidiano provar
     Dm                  Am
Descobrir como é seu ar respirar
  Dm        Am
E se puder tirar algo de bom


Dm      Dsus4        Am   Am5+
Com minha sombra eu caminhei
Gm
Até me reinventar


A#            Dm    C
Há de haver alguma cor
Gm            F     C
Que afague o sonhador


( Dm Am ) - 2x

Dm                     Am
Colorir, pelas ruas eu procurei
Dm                               Am
É tentador, só preto e cinza encontrei

Dm    Dsus4     Am       Am5+
As luzes não brilhavam mais
Gm
Como me reinventar ( a sombra não se faz )

A#          Dm    C
Há de haver alguma cor
Gm          F     C
Que afague o sonhador.

( Dm Am ) - 8x

A#            Dm    C
Há de haver alguma cor
Gm            F     C
Que afague o sonhador

( Dm Dsus4 Am Am5+ )

----------------- Acordes -----------------
A# = X 1 3 3 3 1
Am = X 0 2 2 1 0
Am5+ = 5 8 7 5 6 5
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Dsus4 = X X 0 2 3 3
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
