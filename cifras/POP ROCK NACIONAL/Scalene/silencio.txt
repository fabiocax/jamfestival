Scalene - Silêncio

Intro ( Dm F ) - 2x

Dm     F
Só assim pra saber
Dm      F
Sem ninguém sem você
Dm   F
A verdade é que não
Gm
Há mais porquê
Gm
Buscar entender onde errei:

Dm   F
Cada dia é um erro.
Dm              F
Dando voltas em desacertos
Dm    F
Nos levando sempre
Gm
Em direção

Gm
Direto ao chão, sob mim

Bb  Dm    C
Faz tanto tempo
Bb  Dm    C
Mas o silêncio
Bb  Dm    C            G
Vai me levar pra um lugar melhor

Dm        F
Por mais triste que isso
Dm     F
Possa soar (Jogo no ar)
Dm    F               Gm
Meu meio de me sentir livre e feliz
                  A
É através da solidão onde te conheci

Bb  Dm    C
Faz tanto tempo
Bb  Dm    C
Mas o silêncio
Bb  Dm    C            G         G7
Vai me levar pra um lugar melhor... lugar melhor

( D5   C ) - 4x

Bb   Dm     C
Mas, eu confesso
Bb   Dm   C
Paz, desconheço
Bb   Dm  C            G
Faz, o silencio nos deixar ser

Bb  Dm    C
Faz tanto tempo
Bb  Dm    C
Mas o silêncio
Bb  Dm    C            G         G7
Vai me levar pra um lugar melhor... lugar melhor

( D5   C ) - 4x

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D5 = X 5 7 7 X X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
