The Police - Wrapped Around Your Finger

Glockenspiel intro:
   Am            Em7 Am               G  Am        Em7       Am      G
e|-----------------------------------------------------------------------|
B|-----------------------------------------------------------------------|
G|-9-10-9-10-7-9-7-9----7----7-----------------------------------------9-|
D|-------------------10---10---9-10-9----7-7--7-10-9-10-9----7-7--10-9---|
A|------------------------------------10------------------10-------------|
E|-----------------------------------------------------------------------|

Keyboard:

   Am                Am                  Am                  Am
e|-5----5\-----------5----5\-------------5-5\----------------5-5\--------|


Am                  Em7      Am      G
You consider me the young apprentice
Am                        Em7     Am    G
Caught between the Scylla and Charybdis
Am                   Em7      Am     G
Hypnotized by you if I should linger
Am                   Em7        Am     G
Staring at the ring around your finger


I have only come here seeking knowledge
Things they would not teach me of in college
I can see the destiny you sold
Turned into a shining band of gold

G                           Fmaj7
I'll be wrapped around your finger
G                           Fmaj7  *
I'll be wrapped around your finger

Mephistopheles is not your name
But I know what you're up to just the same
I will listen hard to your tuition
You will see it come to it's fruition

And I'll be wrapped around your finger
I'll be wrapped around your finger *

Am            Em7             F      G
Devil and the deep blue sea behind me
Am            Em7               F      G
Vanish in the air, you'll never find me
Am               Em7         F     G
I will turn your flesh to alabaster
Am                 Em7             F
When you find your servant is your master

And you'll be wrapped around my finger
You'll be wrapped around my finger *

[Intro riff again to fade]


Fill at * mark:
   G        Dm7  G  Am
e|----3-----5----3\-5-|
B|-3----3---6----3\-5-|
G|-0------0-5---(4)---|
D|-0------------------|
A|--------------------|
E|--------------------|

How the chorus is played:
   G (strummed)                 Fmaj7
e|-3-------3-------------------0------0---|
B|-0-------0-------------------1------1---|
G|-0-------0----------------------2-----2-|
D|-0-------0----------------------3-----3-|
A|-2-------2----------------------0-----0-|
E|-3-------3------------------------------|
   I'll be wrapped around your finger

|  /  slide up
|  \  slide down
|  h  hammer-on
|  p  pull-off
|  ~  vibrato
|  +  harmonic
|  x  Mute note
| (?) Ghost note

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Dm7 = X 5 7 5 6 5
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
Fmaj7 = 1 X 2 2 1 X
G = 3 2 0 0 0 3
