Roxette - One Wish

A       F#      C#m     E

      A    F#m     C#m E
One wish    One whish

A
If you had one wish, what would it be?
D                                       A
If you had one wish, would it be about me?
                                     E  A
Just one wish, could it be about me?
                                       D
You might make a wish for money or peace
                                  F#m
You'd make a wish for cosmic relief
                                     E      Bm E
shade by the trees and green summer leaves

       D                 A        F#m
Well I was thinking that hey-hey-hey
            C#
maybe one day

  E
a wish of you and me
     A        F#m
yeah hey-hey-hey
           C#m
maybe one day
          E
a wish of you and me

                                  Bm         A
Please don't tell me what I don't need to know
        A                          E
Please stop offer what I shouldn't know
                                  B
I don't wanna know what doesn't show
E                                   A
If you had one wish, what would it be?
                                       C#m
If you had one wish, would it be about me?
                             C#m   B
Just one wish, could it be about me?

A                          E       C#m
'Cause I was thinking that hey-hey-hey
           G#m
maybe one day
       B
a wish of you and me
     E       C#m
yeah hey-hey-hey
           G#m
maybe one day
       B                A
a wish of you and me

          C#m   E          B           A
Waving high,   then down below (down below)
           C#m
shine your light
B             A
down here below (down below)

oh one wish
      C#m
one wish

         F#m E C A
one wish
         C#m
one wish

E        C#m
Hey-hey-hey
           G#m
maybe one day
       B
a wish of you and me
     E        C#m
yeah hey-hey-hey
           G#m
maybe one day
       B                A
a wish of you and me

E C#m G#m B

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C# = X 4 6 6 6 4
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
