Roxette - Keep Me Waiting

4x

e|----------------------------------------------------------------------------------|
B|-------------------------3----------------3-----------------------3---------------|
G|---------------0-----------------0-----------0---------3-----------------0--------|
D|-----------------------------------3----------------------------------------------|
A|----------------------------------------------------------------------------------|
E|----------------------------------------------------------------------------------|

e|-------------------------------------------------------------------------------|
B|----------3---------1----------------------------------------------------------|
G|----------------------------3--------------------------------------------------|
D|-------------------------------------------------------------------------------|
A|-------------------------------------------------------------------------------|
E|-------------------------------------------------------------------------------| 

   Em
1.     I hit the coast with the faith of a preacherman,

                          D
    I saw a ghost with a face like a madman.
    I had this dream I was sleeping at the Royal Ball,
Em
    I hit the streets, hey, watch me in the monitor!
                       D
    I saw a clone who looked just like a senator.
G                           Em                      B7       D
    I checked the clock, I felt that it was time to go, go, go.


CHORUS
Em   -C-D              G       D         Em    -C-D
Then came you.      Why did you keep me waiting so long?
             G       D          Em     - C - D
Why did you keep me waiting so long?
             G       D          Em
Why did you keep me waiting so long?

INTRO RIFF times 2

verse 2
    Em
2.     I was a spy, I was captured by the enemies,
                      D
    I made a million selling secret fantasies.
    I saw my love wasted in a pale grey zone,
Em
    I was the quake shaking every monument.
                      D
    I had to fi-find out what all the money meant.
G                         Em                      B7         D
    I checked the sun, I knew that it was time to go, go go go.
CHORUS
           Em   -C-D              G       D         Em    -C-D
Then came you.      Why did you keep me waiting so long?
             G       D          Em     - C - D
Why did you keep me waiting so long?
             G       D          Em      -D-G-D   Em-D-G-D
Why did you keep me waiting so long?
CHORDS FOR CHORUS PLAYED WITH NO VOCAL

THEN RIFF 1 - 4x

e|--------------------------------------------------------------------------------------|
B|--------3---3-------3----3---------3-----3--------------------------------------------|
G|--------0---0----------------------3-----3--------3------5---5------3----5------------|
D|--------------------3----3------------------------3------5---5------3----5------------|
A|--------------------------------------------------------------------------------------|

CHORUS
           Em   -C-D              G       D         Em    -C-D
Then came you.      Why did you keep me waiting so long?
             G       D          Em     - C - D
Why did you keep me waiting so long?
             G       D          Em      -D-G-D   Em-D-G-D
Why did you keep me waiting so long?
 BY JAMES AGED 12
INTRO RIFF TIME 4

----------------- Acordes -----------------
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
