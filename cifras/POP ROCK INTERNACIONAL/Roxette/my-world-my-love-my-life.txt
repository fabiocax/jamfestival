Roxette - My World, My Love, My Life

(Words & music by Per Gessle)

 Intr: Gm D# F (4x)

 Bb            F                      Cm                       Gm  F
 Some days are covered in rain. While other days are made for glory.
 Bb              F                Cm                      Gm  F
 And you are the summer sun. The only one who knows the story.
               D#
 Do you wanna talk about it?
         Bb                 D#
 I don't know. Do you wanna say some words?
        Gm       F
 Oh, I can't let go.

                      Gm        D#       F
 I'm talking about my world, my love, my life.
              Gm     D#       F
 Every time I try to close my eyes.
          Bb                    G#                       Cm       Bb
 You just enter my mind and you lay yourself down beside me. Oh, why?


 Bb                F
 Some streets are covered with sweat.
            Cm                      Gm    F
 There are no regrets, just new connections.
 Bb                     F                Cm              Gm    F
 Oh, you are the summer son. And I'm the one who wants affection.
               D#                           Bb
 Do you wanna talk about it? Hmm... I don't know.
               D#                         Gm        F
 Do you wanna speak some words? No, oh, I can't leg go.


 I'm talking about my world, my love, my life.

 Every time I try to close my eyes.

 You just run through my head. And you lay yourself down beside me. Oh, why?

 My world, my life...

                      Am        F        G
 I'm talking about my world, my love, my life.
       Am      F               G
 Every time I try to close my eyes.
           C                     Bb                      Dm       G
 You just enter my mind. And you lay yourself down beside me. Oh why?
              C                        Bb                 Dm           G
 You just run through my head. And you lay yourself down beside me. Oh why?

 My world, my life...

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D# = X 6 5 3 4 3
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
Gm = 3 5 5 3 3 3
