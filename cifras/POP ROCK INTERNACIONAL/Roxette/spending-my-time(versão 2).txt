Roxette - Spending My Time

Capo Casa 2

           Em               Am       D4      D
What's the time, seems it's already morning
           Em          C             D4     D
I see the sky, it's so beautiful and blue
         Em         C          D                       Asus2       Am
The tv's on but the only thing showing is a picture of you

         Em     C           D4   D
Oh I get up and make myself some coffee
         Em                  C            D4   D
I try to read a bit, but the story is too thin
            Em         C                  D4   D
I thank the Lord above you're not here to see me
                A       Am
In this shape I'm in

            D       D4  D
Spending my time
             A
Watching the days go by
           Bm                    G
Feeling so small, I stare at the wall

            Em   G           A
Hoping that you think of me too
                Bm
I'm spending my time

         Em         Am                 D4     D
I try to call but I don't know what to tell you
          Em           Am           D4   D
I leave a kiss on your answering machine
           Em               Am             D4      D
Oh help me please is there someone who can make me
                     A    Am
Wake up from this dream?

            D       D4  D
Spending my time
             A
Watching the days go by
           Bm                    G
Feeling so small, I stare at the wall
             Em     G           A
Hoping that you are missing me too

            D       D4  D
Spending my time
                 A
Watching the sun go down
                      Bm
I fall asleep to the sound
                G
Of "tears of a clown"
             Em     A
A prayer gone blind
                Bm
I'm spending my time

Em               G              D            C
 My friends keep telling me hey life will go on
Em         G                  D
 Time will make sure I'll get over you ooh
Em          G
 This silly game of love
    D              A       C    B
You play, you win, only to lose

                E
I'm spending my time
               B
Watching the days go by
            C#m                   A
Feeling so small, I stare at the wall
             F#m A          B
Hoping that you think of me too
                E
I'm spending my time
             B
Watching the sun go down
                      C#m
I fall asleep to the sound
                   A
of the Tears of a clown
               F#m
A prayer gone blind...

B                C#m
 I'm spending my time...

----------------- Acordes -----------------
Capotraste na 2ª casa
A*  = X 0 2 2 2 0 - (*B na forma de A)
Am*  = X 0 2 2 1 0 - (*Bm na forma de Am)
Asus2*  = X 0 2 2 0 0 - (*Bsus2 na forma de Asus2)
B*  = X 2 4 4 4 2 - (*C# na forma de B)
Bm*  = X 2 4 4 3 2 - (*C#m na forma de Bm)
C*  = X 3 2 0 1 0 - (*D na forma de C)
C#m*  = X 4 6 6 5 4 - (*D#m na forma de C#m)
D*  = X X 0 2 3 2 - (*E na forma de D)
D4*  = X X 0 2 3 3 - (*E4 na forma de D4)
E*  = 0 2 2 1 0 0 - (*F# na forma de E)
Em*  = 0 2 2 0 0 0 - (*F#m na forma de Em)
F#m*  = 2 4 4 2 2 2 - (*G#m na forma de F#m)
G*  = 3 2 0 0 0 3 - (*A na forma de G)
