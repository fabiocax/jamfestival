Roxette - She's Got Nothing On (But The Radio)

Intro: A5 A5 A5 G5 G#5 G5
       A5 A5 A5 G5 A5 G5 A5 (2x)

       G5 G#5 A5 A7/9+ (3x) G5 G#5 A5

A5 A5 A5 G5 G#5 G5 A5 A5 A5    G5 A5 G5 A5
What she got she got to give it to somebody
A5 A5 A5 G5 G#5 G5 A5 A5 A5
What she got she got to give it to someone
G5 A5 G5             G5 A5-D5
It’s not a case of growin’ up or lots of money
G5 G#5 A5            G5 G#5 A5
It’s just the fundamental twist of the sun
G G# A  A7/9+              G G# A  A7/9+
What she got she got to let somebody find it (- "Really?”)
G G# A A7/9+              G G# A A7/9+
What she got is not for her to keep alone (- "Oh!”)
G5 A5 G5             G5 A5 D5
Nobody’s got a clue if there is such a reason (- "Yea?”)
G G# A A7/9+            G G# A A7/9+
Why she wanna play it o-on her own


Dm                Bb         F        C
She’s got nothing on but the radio…radio
Dm              Bb
She’s a passion play
             F
And like the break of day
      C             Bb      Dm
She takes my breath away…heeey…

( G G# A A7/9+ ) (4x)

A5 A5 A5 G5 G#5 G5 A5 A5 A5    G5 A5 G5 A5
What she got she got to give to some contender
A5 A5 A5 G5 G#5 G5 A5 A5 A5
What she got is just like gold dust on a shelf
G5 A5 G5            G5 A5 D5
And no one’s got a clue what’s on her brave agenda
G G# A A7/9+            G G# A A7/9+
Why she wanna keep it keep it to herself

Dm                Bb         F        C
She’s got nothing on but the radio…radio
Dm              Bb
She’s a passion play
             F
And like the break of day
      C             Bb      Dm
She takes my breath away…heeey…

( C Dm ) (2x)

            C              Dm
Who did the painting on my wall?
           C             Dm
Who left a poem down the hall?

           C             Bb
Oh I don’t understand at all, he-he-hey

Solo: G G# A A7/9+  (4x)
      G D  G G# A A7/9+  G5 G#5 A5

Dm                Bb         F        C
She’s got nothing on but the radio…radio
Dm              Bb
She’s a passion play
             F
And like the break of day
      C
She takes my breath away…

Dm                Bb         F        C
She’s got nothing on but the radio…radio
Dm              Bb
She’s a passion play
             F
And like the break of day
      C             Bb      Dm  Bb
She takes my breath away…heeey…
      Dm            Bb         Dm
She takes my breath away…ah ah ah…
    Bb               G5 G#5 A5 (4x)
She takes my breath away…Oh...Ah...Oh…

G G# A7/9+  (4x)  G D  G G# A A7/9+  G G# A A7/9+
Oh…Ah…

( G D  G G# A A7/9+   G G# A A7/9+ )

( G D A )

----------------- Acordes -----------------
A = X 0 2 2 2 0
A5 = X 0 2 2 X X
A7/9+ = X X 7 6 8 8
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D = X X 0 2 3 2
D5 = X 5 7 7 X X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
G#5 = 4 6 6 X X X
G5 = 3 5 5 X X X
