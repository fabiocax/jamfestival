Roxette - Heart Of Gold

Intr. { Em D  C}

 Em         C    D           G
 I want to live, I want to give
 Em         C            D           G
 I've been a miner for a heart of gold
 Em         C      D             G
 It seems expressions, I never give

 Refrão:
 Em                           G         D
 It keeps me searching for a heart of gold
 C                      G
 And I'm getting older
 Em                        G        D
 Keeps me searching for a heart of gold
 C                    *
 And I'm getting older

 *Dedilhado                         *Acompanhamento
 I----------------------------I

 I----------------------------I
 I----------------------------I   &       C - G
 I----------------------------I
 I-----3-----2-----0----------I
 I-----------------------3----I

 Repete Intr.

 Em              C    D                G
 I've been to hollywood, I've been to Redwood
 I've cross the ocean for a heart of gold
 I've been in my mind, it's such a fine line

 Refrão

 -Tocar como Intr. nessa parte:
 Em                           D       C
 You keep me searching for a heart of gold
 Em                              D     C
 You keep me searching and I'm growing old
 Em                           D       C
 You keep me searching for a heart of gold
 Em              C      G            D C  G
 I've been a miner for a heart of gold

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
