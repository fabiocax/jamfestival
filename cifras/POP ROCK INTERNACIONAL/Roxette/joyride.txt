Roxette - Joyride

    A      G/A    D/A
Hello you fool I love you
A                 A
  C'mon join the joyride

( A )

[Primeira Parte]

A
  Hit the road out of nowhere
A
  Had to jump in my car
A
  Be a rider in the love game
A
  Following the stars
D
  Don't need no book of wisdom
Bm                           A
  I get no money-talk at all

A
  She has a train going downtown
A
  She's got a club on the moon
A
  And she's telling all her secrets
A
  In a wonderful balloon
D
  She's the heart of the funfair
Bm                                  A
  She got me whistling her private tune

[Pré-Refrão]

       E      D            A
And it all begins where it ends
          E           D
And she's all my, my magic friend

[Refrão]

           A         G      D
She says "Hello you fool I love you
A                        D
  C'mon join the joyride
          A
Join the joyride"

[Segunda Parte]

A
  She's a flower - I can paint her
A
  She's a child of the sun
A
  We're a part of this together
A
  Could never turn around and run
D
  Don't need no fortune teller
Bm
  To know where my lucky love belongs

[Pré-Refrão]

         E      D                 A
Cause it all begins again when it ends
          E    D
And we're all magic friends

[Refrão]

              A      G      D
She says "Hello you fool I love you
A                        D
  C'mon join the joyride
          A
Join the joyride"
              A      G      D
She says "Hello you fool I love you
A                        D
  C'mon join the joyride
      A
Be a joyrider"

[Solo] D  G  D  G  A
       D  G  D  G  A
       G  D  A
       B5  C5  C#5

[Ponte]

      D5
I'll take you on a skyride

Feeling like you're spellbound
 A5
Sunshine is a lady

Rocks you like a baby

[Refrão]

              A      G      D
She says "Hello you fool I love you
A                        D
  C'mon join the joyride
          A
Join the joyride"

Hello you fool I love you
C'mon join the joyride
Join the joyride

           A      G      D
Hello, hello you fool I love you
      A                       D
C'mon, c'mon join the joyride
      A
Be a joyrider

Roxette!

[Final] D  G  D  G  A

----------------- Acordes -----------------
A = X 0 2 2 2 0
A5 = X 0 2 2 X X
B5 = X 2 4 4 X X
Bm = X 2 4 4 3 2
C#5 = X 4 6 6 X X
C5 = X 3 5 5 X X
D = X X 0 2 3 2
D/A = X 0 X 2 3 2
D5 = X 5 7 7 X X
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
G/A = 5 X 5 4 3 X
