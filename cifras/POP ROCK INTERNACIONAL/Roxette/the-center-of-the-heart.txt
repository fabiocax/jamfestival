Roxette - The Center Of The Heart

Intro: G C F

 G                               C
What am I gonna do when I get a little excited,
          F              G  C  F
 a little in pain, tell me.
  G                                C
 What am I gonna say when I find the centre of my heart
        F
 is a suburb to the brain?


  Bb              F
 You wear them so well.
   Cm
Those innocent eyes.
                 Gm    F          Bb
You're putting on a wonderful disguise.
Bb              F
I want you so bad.
   Cm
 I'm pushing my luck.

                 Gm    F      Bb
 It feels like being hit by a truck.


 Am                Em
This is no place special.
  Bm                D
 Don't know why I came.
  Am                C
 If someone has a minute.
               D
 Won't you explain?


    G                           C
 What am I gonna do when I get a little excited,
    F
a little in pain, tell me.
  G                                C
What am I gonna say when I find the centre of my heart
      F
 is a suburb to the brain?
          G     C F
Singing na na na na na na


  Bb          F
 Being with you.
              Cm
Dealing with fire.
                    Gm
Oh won't you come around (come out),
 F                    Bb
come out (today) and play.
 Bb             F              Cm
 I want you so bad. Answer my calls.
 Gm                    F         Bb
Let's spend the night within these walls.


 Am                Em
This is no place special.
 Bm                D
Nothing for the sane.
Am                    C
If someone's got a minute.
           D
Do please explain.


 What am I gonna do when i get a little excited,
a little of pain,tell me.
What am i gonna say when i find the center of my heart
 is a suburb to the brain?

Singing na na na na na na


Solo: Em  C  Am  G  D  (2x)


  A                             D
What am i gonna do when i get a little excited,
 G                     A D G
a little of pain,tell me.
 A                              D
what am i gonna do when i get a little excited,
  G
a little of pain,tell me.
 A                                 D
what am i gonna say when i find the center of my heart
    G
is a suburb to the brain?
         A                D G
Singing na na na na na na

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
