Roxette - It Just Happens

Dm Bb    F C
Ah…      Uh…
Dm Bb    F C
Ah…      Uh…  Oh, it just happens
Dm Bb    F C
Ah…      Uh…  Oh, it just happens
Dm Bb    F C
Ah…      Uh…


Dm     Gm       C      Am
It's a cabaret, taking over
        E            Am
On a quiet lazy backseat day
Dm            Gm
And it's so beautiful
C          Am              E
Like the sunshine on your balcony
Am
Just yesterday


Bb        F
Don't underestimate
Bb             F
Your heart is never late
Bb           F              C
And love will always find a way

       Dm  Gm
It just happens
F          C           Dm Gm
You don't know what's going on
F       C                 Bb            C
If it's new or if it's been there since long
        F        C
If it's right or wrong


Dm Bb       F       C
Ah…     You fall in love
Dm Bb       F       C
Ah…     You fall in love


Dm          Gm
You don't have a say
C           Am                    E
Just let it slide and close your eyes
            Am
And watch the passion play


Bb       F
Don't underestimate
Bb          F
Your heart can never wait
Bb             F            C
And love will always find a way


Dm Bb       F       C
Ah…     You fall in love… It just happens
Dm Bb       F       C
Ah…     You fall in love…Oh, It just happens
Dm Bb               F       C              Dm Bb
Ah…  oh yeah..  You fall in love… It just happens, happens oh!
    F       C
You fall in love


Bb        F
Don't underestimate
Bb          F
Your heart can never wait
Bb         F            C
Love will always find a way

             Dm Gm
It just happens
F             C             Dm  Gm
And you don't know what's going on
F       C              Bb               C
If it's new or if it's been there since long
        F         C
If it's right or wrong


Dm Bb          F       C
Ah…     You fall in love…  Oh, It just happens
Dm Bb          F       C
Ah…     You fall in love…  It just happens Oh…

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
