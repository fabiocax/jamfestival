Roxette - Spending My Time

Capo Casa 2

[Primeira Parte]

           Em              Am      D4    D
What's the time? Seems it's already morning
           Em              C      D4    D
I see the sky, it's so beautiful and blue
           Em              C      D4    D
The tv's on, but the only thing showing is a picture
A     Am
Of you

         Em    Am               D4    D
Oh I get up and make myself some coffee
         Em    C               D4    D
I try to read a bit but the story's to thin
         Em    C                 D4    D
I thank the lord above that you're not here to see
                    A   Am
Me in the shape I'm in

[Refrão]


             D4       D
Spending my time watching
     A
The days go by
             Bm
Feeling so small
                G
I stare at the wall
             Em
Hoping that you
  G          A
Think of me too
                 Bm
I'm spending my time

[Segunda Parte]

          Em
I try to call but
   Am
I don't know what
    D4  D
To tell you
           Em
I leave a kiss on
     Am            D4  D
Your answering machine
             Em
Oh help me please
          Am
Is there someone who can
 D
Make me wake up
            A9  Am
From this dream?

[Refrão]

             D4       D
Spending my time watching
     A
The days go by
             Bm
Feeling so small
                G
I stare at the wall
             Em
Hoping that you
  G          A
Think of me too
                 Bm
I'm spending my time
              A
Watching the sun go down
                      Bm
I fall asleep to the sound
                 G
Of "Tears of a Clown"
                Em  A
A prayer gone blind
                 Bm  (A)  Em
I'm spending my time

[Solo]

E|-----------9--|
B|-0-5-7-9-7----|
G|--------------|
D|--------------|
A|--------------|
E|--------------|

[Ponte]

Em      C
  My friends keep telling me
     D           A
Hey life will go on
Em           C
  Time will make sure
               D
I'll get over you
Em            C
  This silly game of love
      D         A       C    B
You play, you win only
    C7M  B
To lose

[Refrão]

           E
Spending my time
              B
Watching the days go by
            C#m
Feeling so small
                A
I stare at the wall
             F#m
Hoping that you are
 A              B
Missing of me too
                  E
I'm spending my time
              B
Watching the sun go down
                      C#m
I fall asleep to the sound
                 A
Of "Tears of a Clown"
               F#m    B
A prayer gone blind
                 C#m ( B )
I'm spending my time

[Final] A  B  E  F#m  A  B  E

----------------- Acordes -----------------
Capotraste na 2ª casa
A*  = X 0 2 2 2 0 - (*B na forma de A)
A9*  = X 0 2 2 0 0 - (*B9 na forma de A9)
Am*  = X 0 2 2 1 0 - (*Bm na forma de Am)
B*  = X 2 4 4 4 2 - (*C# na forma de B)
Bm*  = X 2 4 4 3 2 - (*C#m na forma de Bm)
C*  = X 3 2 0 1 0 - (*D na forma de C)
C#m*  = X 4 6 6 5 4 - (*D#m na forma de C#m)
C7M*  = X 3 2 0 0 X - (*D7M na forma de C7M)
D*  = X X 0 2 3 2 - (*E na forma de D)
D4*  = X X 0 2 3 3 - (*E4 na forma de D4)
E*  = 0 2 2 1 0 0 - (*F# na forma de E)
Em*  = 0 2 2 0 0 0 - (*F#m na forma de Em)
F#m*  = 2 4 4 2 2 2 - (*G#m na forma de F#m)
G*  = 3 2 0 0 0 3 - (*A na forma de G)
