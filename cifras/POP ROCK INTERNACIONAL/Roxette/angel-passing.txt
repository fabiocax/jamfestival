Roxette - Angel Passing

Capo Casa 9

Intro 4x: Am7 F9 G

Am7            F9       G           Am7
She’s an angel passing through the room
Am7                F9    G
(Would you like to know her?)
Am7            F9       G          Am7
She’s an angel passing through my room
Am7                F9    G
(Would you like to know her?)
    C           Am  G                 F
The red settin’ sun feathers down in field of heather
C     Am
Beautiful
      G             F      Fsus4  F
She’s then and now, forever
Am7              F9   G      Am7
When I ask I ask her for a dance
Am7           F9   G
(Every time I see her)
Am7                F9    G            Am7
We leave the beat, obey the laws of chance

 Am7          F9   G
(Every time I see her)
    C        Am           G            F
How sweet surprise - she wears it so it shows
          C        Am
She never hides her eyes
    G                 F
She wears it where it shows

Solo: D Em C G
      D Em C D
      Am7 F9 G Am7 ...

Am7            F9       G           Am7  F9 G
She’s an angel passing through the room
Am7            F9      G            Am7    F9 G
Like an angel passing through the room
C        Am    G               F
I wish I could save our souls alone and together
C     Am
Beautiful
      G               F   Fsus4 F
We’re then and now, forever
Am7
Forever

----------------- Acordes -----------------
Capotraste na 9ª casa
Am*  = X 0 2 2 1 0 - (*F#m na forma de Am)
Am7*  = X 0 2 0 1 0 - (*F#m7 na forma de Am7)
C*  = X 3 2 0 1 0 - (*A na forma de C)
D*  = X X 0 2 3 2 - (*B na forma de D)
Em*  = 0 2 2 0 0 0 - (*C#m na forma de Em)
F*  = 1 3 3 2 1 1 - (*D na forma de F)
F9*  = 1 3 5 2 1 1 - (*D9 na forma de F9)
Fsus4*  = 1 3 3 3 1 1 - (*Dsus4 na forma de Fsus4)
G*  = 3 2 0 0 0 3 - (*E na forma de G)
