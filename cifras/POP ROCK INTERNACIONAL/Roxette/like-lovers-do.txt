Roxette - Like Lovers Do

(Words & Music: Per Gessle)
Intro: D- E - A - 4x

D         E      A
Take the line between us
        D                E            A
And hold it close to you,
      D        E            A   D         E
I want us to get together like lovers do
D              E                A
Let me kiss away your troubles
   D            E              A
The cold and lonely nights
       D                 E              A
Just take this heart I’m giving
       D                  E
And love is on the rise

((Like lovers do))
Want to hold you thru the night
D     E         A
Like lovers do

And when I look into your eyes
Bm   E         A
Like lovers do
         D                      E
Oh I’m drowning in an ocean of emotion

  D          E  A
It feels just like forever
D       E           A
Since I made love to you
      D      E       A
September days of scarlet
D                 E
April morning dew
    D        E          A
So take the love between us
      D           E           A
And hold it close to you
  D                E         A
I want us to get together,
D                E
like lovers do

((Like lovers do))
Want to hold you thru the night
D     E         A
Like lovers do
And when I look into your eyes
Bm   E         A
Like lovers do
         D                      E
Oh I’m drowning in an ocean of emotion…

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D- = X X 0 2 3 1
E = 0 2 2 1 0 0
