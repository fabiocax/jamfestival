Roxette - I Call Your Name

Words & music by Per Gessle
Intro: Em

Riff:
E|-------------------
B|----------------8~-
G|-7-9----7-7-7h9----  (4x)
D|-7--9-7-7-7--------
A|-------------------
E|-------------------

 Em                                            D
It's a madman's situation, reminiscing in the rain,
                 C                                                 Em
And I've lost your love again, I call your name.
                   Em
And I don't want to think about it
        D
When I go to sleep at night,
               C                                              Em
Can you hear it in the wind? I call your name.


 Am D         Em        (C)
I call your name.}3x

Riff:
E|-------------------
B|----------------8~-
G|-7-9----7-7-7h9----  (2x)
D|-7--9-7-7-7--------
A|-------------------
E|-------------------

           Em
And I close the door behind me,
    D
I turn the lights all down,
                 C                                                  Em
There's no-one there beside me, I call your name.
                    Em
And I don't want to think about it
        D
But I wind up all the same,
    C                                                 Em
 Nothing'll ever change, I call your name.

Am D         Em
I call your name.}3x

solo base:Am   c   Em Am  C  Em  c Bm  Am  ...

solo:
E|-------------------------------------------------------------------------
B|--8/10-12-10-12-12/13--12-10-8-10/12------10/12\10-8-8/10-12-10-12-12/13-
G|-------------------------------------9-9~-----------9--------------------
D|-------------------------------------------------------------------------
A|-------------------------------------------------------------------------
E|-------------------------------------------------------------------------

E|------------------15--14-12-12--17b18r17-15-14-15-14-10-12~--------------
B|-12-13b15r13p12-12--12---------------------------------------------------
G|-------------------------------------------------------------------------
D|-------------------------------------------------------------------------
A|-------------------------------------------------------------------------
E|-------------------------------------------------------------------------


Am
 Lie awake under the stars,
                                            Em
 Lie awake under the stars

Riff:
E|------------------------------
B|--------------10b11r10-8-10---
G|-7-9----7-7-7-----------------
D|-7--9-7-7-7-------------------
A|------------------------------
E|------------------------------

Am
 Lie awake under the stars,
                                            Em
 Lie awake under the stars

                        C
(I call your name)

Am D
I    call
Am D  Em   Am   D   Em  (C)  Am  D  Em  Em Am Em
I call your name...

final

Riff:
E|-------------------
B|----------------8~-
G|-7-9----7-7-7h9----  (ate decair)
D|-7--9-7-7-7--------
A|-------------------
E|-------------------

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
