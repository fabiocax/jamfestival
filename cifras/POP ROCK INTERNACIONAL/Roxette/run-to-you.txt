Roxette - Run To You

[Intro] G  C  Em  D
        G  C  G  D
        G  C  Em  D  A

Bm                 A         G
Baby, sometimes I feel like dying
G                 A          Bm
Driving while I'm closing my eyes
Bm             A      G
Moving in and out of hiding
G                    A           Bm
Trying to catch some truth in my life
Em             D              C
Watching your stars and your moonlight
Em             D             C
Come tumbling down from the sky

Take it now

G                C             Em                  D          G                C                   G       D                  G
I'm gonna run to you, I'm gonna come to you, I wanna find you, In everything that I do

                 C              Em                  D          G                      C         G                D                Bm
I'm gonna run to you, I'm gonna count on you, I'm gonna follow, baby what else can I do?

Bm                            A             G                                   A            Bm
Sunday morning my town is sleeping, lying all alone in my bed.
Bm                                  A                       G                                      A                          Bm
There's not a sound, I can't help but listening, wishing I was somewhere else instead
        Em                           D                   C                   Em     D                  C
But sometimes they're to hard to handle, those voices inside my head... Listen now!

 G                C            Em                  D          G                C                   G       D                  G
 I'm gonna run to you, I'm gonna come to you, I wanna find you, In everything that I do
                  C             Em                  D          G                      C         G                D                Bm
 I'm gonna run to you, I'm gonna count on you, I'm gonna follow, baby what else can I do?

Bm                            A             G                                   A            Bm
Sunday morning my town is sleeping, lying all alone in my bed.
Bm                                  A                       G                                      A                          Bm
There's not a sound, I can't help but listening, wishing I was somewhere else instead
        Em                           D                   C                   Em     D                  C
But sometimes they're to hard to handle, those voices inside my head... Listen now!

G                C             Em                  D          G                C                   G       D        G
 I'm gonna run to you, I'm gonna come to you, I wanna find you, In everything that I do
                  C             Em                  D          G                      C         G                D                Bm
 I'm gonna run to you, I'm gonna count on you, I'm gonna follow, baby what else can I do?

                                               D                                                     Bm
 Take a walk inside my dream: a church, a lonely road, all the people
                                                   D                             A   D  F#m  E     A  D  A  E
 All the people come and go and come and go.
A                 D             F#m  E   A                  D                   A       E
I’m gonna run to you…              I’m gonna come to you….             Do it now!

 A                D            F#m               E
 I'm gonna run to you, I'm gonna come to you,
 A             D           A       E
I'm gonna follow,     Oh! Oh!
A                D             F#m               E            A                D                          A         E             A
 I'm gonna run to you, I'm gonna come to you, I wanna find you, In everything that I do
                     D           F#m              E            A                    D      A         E                A         D  F#m  E      A  D  A  E    (2x)
 I'm gonna run to you, I'm gonna count on you, I'm gonna follow, baby what else can I do?

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
