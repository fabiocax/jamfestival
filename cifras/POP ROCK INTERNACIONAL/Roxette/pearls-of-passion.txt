Roxette - Pearls Of Passion

Words & Music: Per Gessle/
B|-Side to the ‘soul deep’ - Single
Into.: D - G - A / 2x
              D             G
There was a banquet of believers
           A                D
When we walked into the room
      Bm                A
December days of x-mas
G                    A
Felt like days in June
                Bm
And there were morning light
           Bbm  A             G
And sunrise chimes echoed all around
D                A
Now’s the time for weary minds,
           G                    Bm
The moon is northern bound
                        D
        And you know I’m going back

                G
Going back today
(Back to where I came from)
                 D
And you know I’m going back
                   F#m   Bm
Going back to stay
                     A
To the alleys where pearls of passion came my way ( D- G - A )
            D                G
There’s a promise in the twilight
         A                  D
It’s so innocent and wild
   Bm                   A
Two hearts that break the silence,
        G              A
You can hear a distant cry
                Bm              Bbm
And there’s emptiness and loneliness,
       A                    G
It’s more or less the same
          D                   A
Let’s run between the raindrops,
        G                         Bm
Babe and catch the midnight train.
                        D
        And you know I’m going back
                G
Going back today
(Back to where I came from)
                 D
And you know I’m going back
                   F#m   Bm
Going back to stay
                     A
To the alleys where pearls of passion came my way ( Bm - Bbm - A - G - D - A - G - Bm )
                        E
        And you know I’m going back
                A
Going back today
(Back to where I came from)
                 E
And you know I’m going back
                   G#m  C#
Going back to stay
                     B
To the alleys where pearls of passion came my way
                        E
        And you know I’m going back
                A
Going back today
(Back to where I came from)
                 E
And you know I’m going back
                   G#m  C#
Going back to stay
                     B
To the alleys where pearls of passion came my way
E                B
Came my way, came my way…

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
Bbm = X 1 3 3 2 1
Bm = X 2 4 4 3 2
C# = X 4 6 6 6 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G#m = 4 6 6 4 4 4
