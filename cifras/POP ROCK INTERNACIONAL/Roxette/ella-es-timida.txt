Roxette - Ella Es Tímida

[Intro] F# A#m B C#

E                G#m
Es asi, la veo sonreir
               A
Con ojos distraidos
F#m                         E
Quieta en una esquina corno si
                  B
Yo no estuviera allí
E                            G#m
Creo yo que el juego es para dos
                   A
Los dos el mismo juego
F#m
Pero no se da
                  E
De pronto ya no esta
                     C#
Y no me deja ni un adiós
         F#   A#m B               C#
Ella es tímida,   se esfuma si la ves

         F#   A#m B               C#
Ella es tímida,   por eso es come es
F#        A#m
Yo no soy capaz
       B           C#
De lastimar su corazon
        F#     B
Ella es tímida
        F#     C#
Mas que tímida
E                      G#m
Fíjate, la quiero pero se
                       A
Que es dulce y vulnerabili
     F#m                      E
Tan frágil como hilos da cristal
                   B
No quiero hacerle mal
E                         G#m
Conozco sus secretos pero no
                  A
La voy a hacer sufrir
              F#m
Porque es tan débil
                   E
Como un pétalo de flor
                      C#
Y su doler me duele a mi yeah!
        F#                A#m    B               C#
Ella es tímida, (ella es tímida) se esfuma si la ves
        F#                A#m    B                C#
Ella es tímida, (más que tímida) por eso es corno es
F#        A#m
Yo no soy capaz
       B           C#
Da lastimar su corazon
        F#     B        F#    C#
Ella es tímida, mas que tímida

[Solo] D#m G# C# F#
       D#m G# C# F#
       D#m G# C# D#

        G#                Cm     C#              D#
Ella es timida, (ella es tímida) se esfuma si la ves
        G#                Cm     C#               D#
Ella es tímida, (más que tímida) por eso es corno es
G#        Cm
Yo no soy capaz
      C#           D#
De lastimar su corazon
        G#              Cm
Ella es tímida,(ella es tímida)
C#              D#
Se esfuma si la ves
        G#              Cm
Ella es tímida,(más que tímida)
C#               D#
Por eso es corno es
G#        Cm
Yo no soy capaz
       C#          D#
De lastimar su corazón
        G#    C#          G#    D#    ( G# Cm C# D# )
Ella es tímida,  más que tímida

----------------- Acordes -----------------
A = X 0 2 2 2 0
A#m = X 1 3 3 2 1
B = X 2 4 4 4 2
C# = X 4 6 6 6 4
Cm = X 3 5 5 4 3
D# = X 6 5 3 4 3
D#m = X X 1 3 4 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
G# = 4 3 1 1 1 4
G#m = 4 6 6 4 4 4
