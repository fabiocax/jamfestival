Guns N' Roses - New Rose

Afinação Meio Tom Abaixo

Tab Notation
A/B Play chord A slide up to chord B
A\B Play chord A slide down to chord B
19B/\/\ Strike note then bend up and down several times
X Muted string

Is she really going out with him
Intro: (Riff 1)
         D   B   E   A
E|------------------------------------------|
B|------------------------------------------|
G|--X-X--7--4-4--9-9------------------------| Repeat
D|--X-X--7--4-4--9-9-7-7--------------------| 5X
A|--X-X--5--2-2--7-7-7-7--------------------|
E|-------------------5-5--------------------|

Riff 2
|---------------------------------------------------|
|---------------------------------------------------|
|---------------------------------------------------| Repeat
|-10/12\10-10-10--10-10-10\9/10-10-10-10\9/10-10-10-| 2x
|--8/10\8---8--8---8--8--8\7/8---8--8--8\7/8---8--8-|
|---------------------------------------------------|


D                             D\B
I've got a feeling inside of me

B                                     B/D
Its gotta stay its gotta some way stay

D                              D\B
I don't know why I don't know why

B                             B/D
Special things she's got for me

Riff 2
I've got a new rose I've got a gun

Riff 2
Yeah I knew that I always would

Riff 2
I cant stop to mess around

Riff 2
I've got a brand new rose in town

D                                D\B
See the sun see the sun n the sky

B                                   B/D
Forget the rose and the_______ rides

D                         D\B
Don't you run away that way

B                          B/D
I'm coming back another day

Riff 2
I've got a new rose I've got a gun

Riff 2
Yeah I knew that I always would

Riff 2
I cant stop to mess around

Riff 2
I've got a brand new rose in town

|------------------------------------------|
|------------------------------------------|
|--X-X--7--4-4--9-9------------------------| Repeat
|--X-X--7--4-4--9-9-7-7--------------------| 5X
|--X-X--5--2-2--7-7-7-7--------------------|
|-------------------5-5--------------------|

|---------------------------------------------------|
|---------------------------------------------------|
|---------------------------------------------------| Repeat
|-10/12\10-10-10--10-10-10\9/10-10-10-10\9/10-10-10-| 2x
|--8/10\8---8--8---8--8--8\7/8---8--8--8\7/8---8--8-|
|---------------------------------------------------|

In this next part play chord B, right after the lyrics end play what is
tabbed below the lyric line.......

B
I never thought this would happen to me
|---------------------------------------------------|
|---------------------------------------------------|
|--------------------------------------19B/\/\/\/\--|
|---------------------------------------------------|
|---------------------------------------------------|
|---------------------------------------------------|

B
On the same_______ machine
|-----------------------------------------|
|-----------------------------------------|
|-------------------------19B/\/\/\/\21---|
|-----------------------------------------|
|-----------------------------------------|
|-----------------------------------------|

B                            Repeat 2x [---------]
I never serve somebody in the same
|-----------------------------------------------------|
|------------------------------17B(17)-15-17-15-17-15-|
|-----------------------------------------------------|
|-----------------------------------------------------|
|-----------------------------------------------------|
|-----------------------------------------------------|

B
I never never never be too late
|------------------------------------------------------|
|------------------------------17B(17)-15-17-15-17-19B-|
|------------------------------------------------------|
|------------------------------------------------------|
|------------------------------------------------------|
|------------------------------------------------------|

|------------------------------------------|
|------------------------------------------|
|--X-X--7--4-4--9-9------------------------| Repeat
|--X-X--7--4-4--9-9-7-7--------------------| 5X
|--X-X--5--2-2--7-7-7-7--------------------|
|-------------------5-5--------------------|

D                             D\B
I've got a feeling inside of me

B                                     B/D
Its gotta stay its gotta some way stay

D                              D\B
I don't know why I don't know why

B                             B/D
Special things she's got for me

Riff 2
I've got a new rose I've got a gun

Riff 2
Yeah I knew that I always would

Riff 2
I cant stop to mess around

Riff 2
I've got a brand new rose in town

|---------------------------------------------------|
|---------------------------------------------------|
|---------------------------------------------------| Repeat
|-10/12\10-10-10--10-10-10\9/10-10-10-10\9/10-10-10-| Several times
|--8/10\8---8--8---8--8--8\7/8---8--8--8\7/8---8--8-|
|---------------------------------------------------|

|--------|
|--------|
|--------|
|-9-7-7--|
|-9-7-7--|
|-7-5-5--|

So here it is.....enjoy...... any comments...


---------------------------------------------------------

New Rose
Guns N' Roses
The spaghetti incident?


Chords by:    Gonzalo Rivero


Intro:[D5 B5 E5 A5]
      [F#5 G#5 E5] x 2
C5
I got a feeling inside of me,

A5
it's kind of strange like storme sea,

C5
I don't know why, I don't Konw why,

A5
Iguess these things have got to be

[F#5 G#5 E5] I've got a new rose, I got her god,

[F#5 G#5 E5] guess I knew that I always good,

[F#5 G#5 E5] I can't stop this mess around,

[F#5 G#5 E5] I got a brand new rose in town

See the sun, see the sun, it shines
don't get too close or it'll burn your eyes,
don't you run away that way
you can come back another day

A5  B5    C#m
I never thogh this culd happen to me,

A5  B5     C#m
all we said is washing machine,

A5  B5  C#m
I don't deserve somebody insane,

A5  B5  C#m
I never, never be too late

Chords
 D5=x577xx   B5=x244xx   E5=022xxx   A5=x022xx
 C5=x355xx  F#5=244xxx  G#5=466xxx  C#m=x46654

----------------- Acordes -----------------
A = X 0 2 2 2 0
A5 = X 0 2 2 X X
B = X 2 4 4 4 2
B/D = X 5 4 4 4 X
B5 = X 2 4 4 X X
C#m = X 4 6 6 5 4
C5 = X 3 5 5 X X
D = X X 0 2 3 2
D5 = X 5 7 7 X X
E = 0 2 2 1 0 0
E5 = 0 2 2 X X X
