Guns N' Roses - Yesterdays

Afinação Meio Tom Abaixo

[Riff 1]

      D                   D/F#
Eb|----------0h2------|----------0h2-----|
Bb|------3---------3--|------3-----------|
Gb|----2---2-----2----|----2---2-------0-|
Db|--0----------------|--4-----------0---|
Ab|-------------------|------------------|
Eb|-------------------|------------------|

      C9
Eb|---------2--0------3-------|
Bb|-----3---------3-----3---3-|
Gb|-----0-----------0-----0---|
Db|-----2---------------------|
Ab|--3------------------------|
Eb|---------------------------|

[Riff 1]
Yesterday, there was so many things


I was never told

Now that I startin' to learn

I feel I'm growin' old


D                     C9
'Cause yesterday's got nothin' for me

G5
Old pictures that I'll always see

 D
Time just fades the pages

G5
In my book of memories

[Riff 1]
Prayers in my pocket

And no hand in destiny

I'll keep on movin' along

With no time to plant my feet

D                     C9
'Cause yesterday's got nothing for me

G5
Old pictures that I'll always see

 D
Some things could be better

G5
If we'd all just let them be


[Riff 2]
Uh...Yesterday's got nothin' for me

Yesterday's got nothin' for me

Got nothin' for me

[Riff 2]

Eb|--------------------------|   
Bb|--------------------------|    
Gb|--2-----------------------|    
Db|--2---4-5--x-x--5---7/9---|
Ab|--0---2-3--x-x--5---------|
Eb|----------------3---5/7---|

[Solo]

Eb|--------------------------------|     
Bb|---------7----------------------|       
Gb|--9b10-----b10r---7h9p7---------|       
Db|------------------------9-7-9~--|       
Ab|--------------------------------|
Eb|--------------------------------|

Eb|----------7------------------------|
Bb|--10b12~----10-7------------7------|
Gb|-----------------9b10r--7h9---7~~--|
Db|-----------------------------------|
Ab|-----------------------------------|
Eb|-----------------------------------|

Eb|-------------9-9--------------------------------------|
Bb|--10b12~--12-----12b14~--14-14b15-14b15-14b15-15-15r--|
Gb|------------------------------------------------------|
Db|------------------------------------------------------|
Ab|------------------------------------------------------|
Eb|------------------------------------------------------|

Eb|----------------------------12---------12--------------------|
Bb|--15b17-15-12-----------------12-15b17----15-12--------------|
Gb|--------------13------12/13---------------------14-12h13-----|
Db|-----------------14~-------------------------------------14--|
Ab|-------------------------------------------------------------|
Eb|-------------------------------------------------------------|

Eb|----------12-------15--------------------17/--|
Bb|--------12---15/17----17/15~-------17b19------|
Gb|--12/13---------------------------------------|
Db|----------------------------------------------|
Ab|----------------------------------------------|
Eb|----------------------------------------------|

[Riff 1]
Yesterday there were so many things

I was never shown

Suddenly this time I found

I'm on the streets and I'm all alone


D              C9
Yesterday's got nothin' for me

G5
Old pictures that I'll always see

D                   G5
I ain't got time to reminisce old novelties

[Riff 2]
Yesterday's got nothin' for me

Yesterday's got nothin' for me

Yesterday's got nothin' for me

[Riff 3]
Yesterday


[Riff 3]

Eb|--------------|    
Bb|--------------|
Gb|--2-----------|
Db|--2---4-5-----|
Ab|--0---2-3-----|
Eb|--------------|

----------------- Acordes -----------------
C9*  = X 3 5 5 3 3 - (*B9 na forma de C9)
D*  = X X 0 2 3 2 - (*C# na forma de D)
D/F#*  = 2 X 0 2 3 2 - (*C#/F na forma de D/F#)
G5*  = 3 5 5 X X X - (*F#5 na forma de G5)
