Guns N' Roses - November Rain

Afinação Meio Tom Abaixo

[Intro]  F7M  Dm  C
         F7M  Dm  C

F7M                     Dm
  When I look into your eyes
                      C
I can see a love restrained
F7M                   Dm
  But darling when I hold you
                           C
Don't you know I feel the same

Dm                 G
  Nothing lasts forever
                              C
And we both know hearts can change
Dm                         G
  And it's hard to hold a candle
                      C
In the cold november rain


F7M                               Dm
  We've been through this such a long, long time
                         C
Just trying to kill the pain, oh yeah

     F7M
But lovers always come, And lovers always go
    Dm                                      C
An no one's really sure Who's letting go today walking away

      F7M
If I could take the time to lay it on the line
         Dm                                      C
I could rest my head just knowing that you were mine, all mine

Dm                   G
  So if you want to love me
                       C
Then darling don't refrain
Dm                        G
  Or I'll just end up walking
                      C
In the cold november rain

Refrão:

F                            G
Do you need sometime on your own?
       F                 G
Do you need sometime all alone?
          F                       G
Everybody needs sometime on their own
                   F                 G
Don't you know you need sometime all alone
Em          F                C
I know it's hard to keep and open heart
Em        F                   Dm
When even friends seem out to harm you
Em         F            C
But if you could heal a broken heart
Em       F              G
Wouldn't time be out to charm you

[Solo]  F  Dm  C
        F  Dm  C
        Dm  G  C
        Dm  G  C

E|--------------7---------------------------|
B|--7/9--9p7--9---9--7------7h9-------------|
G|---------------------8--------------------|
D|------------------------------------------|
A|------------------------------------------|
E|------------------------------------------|

E|--------7----------7----------------------|
B|--9p7-9---9--4b5r4---4b5r4b5r4----------7-|
G|-------------------------------6p4-6/8----|
D|------------------------------------------|
A|------------------------------------------|
E|------------------------------------------|

E|-------------------7----------------------|
B|---------9--9p7--9---9--7---7h9-----------|
G|8/6 4---------------------8---------------|
D|------------------------------------------|
A|------------------------------------------|
E|------------------------------------------|

E|------------------------------------------|
B|-9P7-9-12-14-12-14b16--12h14p12----12-12--|
G|--------------------------------13--------|
D|------------------------------------------|
A|------------------------------------------|
E|------------------------------------------|

E|-12p11----12p11----12b14-12---------------|
B|-------12-------12-------------------14-16|
G|------------------------------------------|
D|------------------------------------------|
A|------------------------------------------|
E|------------------------------------------|

E|------------------------------------------|
B|-17-16h17p16-14-14-16-17b19-17-16-17-16---|
G|------------------------------------------|
D|------------------------------------------|
A|------------------------------------------|
E|------------------------------------------|

E|-18h19-18-18-18/16-16-16/14-14-14/12-12-12|
B|------------------------------------------|
G|------------------------------------------|
D|------------------------------------------|
A|------------------------------------------|
E|------------------------------------------|

E|-11--12p11p9-12--11-12-11-12p11p9----11---|
B|----------------------------------12----12|
G|------------------------------------------|
D|------------------------------------------|
A|------------------------------------------|
E|------------------------------------------|

E|-9h11----9--------------------------------|
B|------12---11-12-11-12p11p9----11----9----|
G|----------------------------11----11---11-|
D|------------------------------------------|
A|------------------------------------------|
E|------------------------------------------|

E|------------------------------------------|
B|-------------12---------12---------14b16--|
G|9/8--8h9p8-8----8h9p9-8----8h9p8-8--------|
D|------------------------------------------|
A|------------------------------------------|
E|------------------------------------------|

E|14----------------------------------------|
B|---16r14-12-------------------------------|
G|------------------------------------------|
D|------------------------------------------|
A|------------------------------------------|
E|------------------------------------------|

F                               G
Sometimes I need sometime on my own
            F                 G
Sometimes I need sometime all alone
          F                       G
Everybody needs sometime on their own
                   F                 G
Don't you know you need sometime all alone

F
And when your fears subside
Dm                C
And shadows still remains, oh yeah
F                   Dm
I know that you can love me
                            C
When there's no one left to blame
Dm                G
So never mind the darkness
                    C
We still can find a way
Dm                  G
Cause nothin' lasts forever
                   C
Even cold november rain



----------------- Acordes -----------------
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F7M = 1 X 2 2 1 X
G = 3 2 0 0 0 3
