Bryan Adams - Rock Steady

Verso 1:

F
   You need a man that'll treat you
   like the woman you are
   Those little boys are just fooling around
         C
   with your heart
Dm                        Bb
   Whatcha gonna do now that you're all alone?
Dm                Bb
   You need a rock not a rollin' stone
F
   Yeah so when I'm bored
C
   It just ain't enough
Dm                Bb
   You need a man made of stronger stuff
F         C
   Get ready


Chorus:

Bb
   Rock steady all night long
F
   Rock steady till the light of dawn
Bb
   Slow and easy tried and true
C          Bb
   Rock steady
          F
   Just me and you

Verso 2:

You need somebody who will help you
with your heavy load
Well I've been there baby
Lord give me a rock and roll
Well I can bend but I won't break
Cause you ain't got what I can take
So when you're tired of those little girls
You want a woman that'll rock your world
Get ready
Chorus:
Rock steady all night long
Rock steady till the light of dawn
Slow and easy tried and true
Rock steady
Just me and you
Instrumental: F F Bb F

Bridge:

Db
Amateurs are clearly tought
Ab
They can't give you what you really want
Db
When all the other have been untrue
C
I'll give you something you can hold on to

Chorus:

Rock steady all night long
Rock steady till the light of dawn
Slow and easy tried and true
Rock steady baby
Me and you
Repeat chorus:
Rock steady
Rock me baby, you know what I'm talking baby
Come on, rock me baby
I want you to rock it baby
Come on rock me baby
Come on rock me baby
Rock steady all night long

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Db = X 4 6 6 6 4
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
