Bryan Adams - It's Only Love

[Intro]  D5  C5  A5  Bb5  F5  F5
         D5  C5  A5  Bb5  F5  F5
         D5  C5  A5  Bb5  F5  F5
         D5  C5  A5  Bb5  F5  F5

E|------------------------------------------------------------------------------------------|
B|--3---------------------------------------------------------------------------------------|
G|--2------5---------------2------3---------------------------------------------------------|
D|--0------5---------------2------3---------3---------3---3----3--3-----------3---3----3--3-|
A|---------3---------------0------1-----3-5----5-3----3---3----3--3-----------3---3----3--3-|
E|----------------------------------------------------1---1--1-1--1-----------1---1--1-1--1-|

[Verse] (Bryan sings)
         D5            C5                      A5          Bb5
When the feelin  ' is end  ed      There ain't no  use  pretendin
         F5                      F5
Don't ya wor  ry   - it's on ly  love
          D5               C5                  A5            Bb5
When your world   has been shattered     Ain't nothin'  else mat ters
         F5                   F5
It ain't o  ver - it's on ly  love     And that's all - yea eah


( D5  C5  A5  Bb5  F5  F5 )

          D5               C5                    A5               Bb5
When your heart   has been bro ken         Hard  words  have been spo ken
         F5                         F5
It ain't ea  sy   - but it's on ly  love
            D5                 C5                      A5                Bb5
And if your life   ain't worth li  vin '    And you're rea  dy   to give   in
          F5                       F5
Just remember   - that it's on ly  love       o oo  ove

[Solo] D5  C5  A5  Bb5  F5  F5
       D5  C5  A5  Bb5  F5  F5

C5                            D5
  Don't you  live without the agg    ra vation
Bb5                         G5
  Ya gotta wanna   win   -    ya gotta wanna   win
C5                         D5
  You keep lookin' back in des    pera tion
Bb5                           C5
  O  ver   and   o  ver  and   o  ver  again

( D5   C5   A5   Bb5   F5   F5 )

  D5  C5  A5  Bb5  F5  F5
 oh    yeah            it's only love baby  alright
D5  C5  A5  Bb5  F5  F5
 ooooh baby baby       it's only love love love
                           D5          C5                       A5          Bb5
When your world   has been shattered        Ain't nothin'  else mat ters
         F5                   F5
It ain't o  ver - it's on ly  love
        D5                 C5                      A5                Bb5
If your life   ain't worth li  vin '    And you're rea  dy   to give   in
          F5                       F5
Just remember   - that it's on ly  love      yeah   that's all

( D5  C5  A5  Bb5  F5  F5 )
( D5  C5  A5  Bb5  F5  F5 )
( D5  C5  A5  Bb5 )

              F5                         F5
Yeah It ain't ea  sy   - but it's on ly  love   - and that's all

----------------- Acordes -----------------
A5 = X 0 2 2 X X
Bb5 = X 1 3 3 X X
C5 = X 3 5 5 X X
D5 = X 5 7 7 X X
F5 = 1 3 3 X X X
G5 = 3 5 5 X X X
