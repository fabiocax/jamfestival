Bryan Adams - Lonely Nights

Intro: E     B     A     B  (2x)

  B   E     A             E
 Would you risk your reputation
  B   E     A
 I don't know
  B   E    A                       E
 You just don't know what you're saying
  B   E     A
 Any  -  more

  B   E    A                   E
 You know beggars can't be choosers
  B   E     A
 And it's fair
  B   E    A             E
 When we win we may be losers
  B   E    A      B
 I don't care... no, no, no

        E       B                         A
    Oh, baby, I just can't stand another lonely night

         B                E
    So come over and save me
         B                    A         B
    Save me from another lonely night

  B   E   A                E
 I hear every word your sayin'
  B   E       A
 They're all lies
  B   E    A                  E
 But with every breath your taking
  B   E   A                B
 Your thinking of ways to say good night

            E        B                        A
    Oh now, baby, I just can't stand another lonely night
         B                E
    So come over and save me,
         B                    A         B
    Save me from another lonely night

        E       B                        A
       Baby, I just can't stand another lonely night
            B               E      B     A     B
       So come over and save me,


            E      B                         A
       Baby, I just can't stand another lonely night
            B                E       B     A     B
       So come over and save me,

             E       B                        A
    Oh now, baby, I just can't stand another lonely night
         B                E
    So come over and save me,
         B                    A           B    E
    Save me from another lonely night ...

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
E = 0 2 2 1 0 0
