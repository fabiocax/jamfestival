Bryan Adams - Everything I Do (I Do It For You)

[Intro] Db  Ab/Db  Gb/Db  Ab

Db                  Ab/Db             Gb/Db            Ab/Db
  Look into my eyes,      you will see what you mean to me
               Db                  Ab/Db
  Search your heart,   search your soul and when you
  Gb/Ab                 Db/Ab     Ab
  Find me there, you'll search no more
        Ebm                    Db/F   Ebm
  Don't tell me it's not worth trying for
            Ebm                    Db/F  Ebm
  You can't tell me it's not worth dying for
               Db/Ab              Ab            Db
  You know it's true, everything I  do, I do it for you

Db                     Ab/Db                        Gb/Db
  Ab/Db
  Look into your heart,      you will find, there's nothing there to hide
Db                         Ab/Db         Gb/Db                  Ab/Db
  Take me as I am, take my life, I would give it all, I would sacrifice
        Ebm                    Db/F   Ebm
  Don't tell me it's not worth fighting for

          Ebm                     Db/F    Ebm
  I can't help it, there's nothing I want more
                Db/Ab            Ab            Db
  You know it's true, everything I do, I do it for you

           B            E                 B                F#
   There's no love like your love, and no other could give more love

Mudança de Tom: Db

           Db              Ab                    Eb            Ab
   There's no where unless you're there, all the time, all the way

[] Gb  Db  Gb  Db

Solo:

E|-----------|---------------------------------------------------------------8------------------------|
B|-----------|-----------------------------------------------8-8---8/10---10----10/8------------------|
G|-7-9-------|--7/9-~\---/9--7/5-2/5-/7---5-5----7/9---9/7/5-------------------------7/9---7b-7b-7b-5-|
D|------7-10-|------------------------------5/7-------------------------------------------------------| 
A|-----------|----------------------------------------------------------------------------------------|
E|-----------|----------------------------------------------------------------------------------------|

                Ebm                           Ab
   Oh you can't tell me it's not worth trying for
           Ebm                            Ab
   I can't help it there's nothing I want more
                Db                 Ab
   Yeah I would fight for you, I'd lie for you, walk the
   Gb                     Gbm
   Wire for you, yeah I'd die for you
                Db/Ab             Ab         Gb  Gbm         Db
   You know it's true, everything I do, oh-o-o,      I do it for you

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Ab/Db = X 4 6 5 4 4
B = X 2 4 4 4 2
Db = X 4 6 6 6 4
Db/Ab = 4 X 3 1 2 X
Db/F = X 8 X 6 9 9
E = 0 2 2 1 0 0
Eb = X 6 5 3 4 3
Ebm = X X 1 3 4 2
F# = 2 4 4 3 2 2
Gb = 2 4 4 3 2 2
Gb/Ab = 4 X 4 3 2 X
Gb/Db = X 4 4 3 2 2
Gbm = 2 4 4 2 2 2
