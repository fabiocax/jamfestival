Bryan Adams - Everything I Do (I Do It For You)

Afinação Meio Tom Abaixo

Verse 1

D                  A            G                A
Look into my eyes, you will see what you mean to me
            D                  A                 G
Search your heart, search your soul and when you find me there,
G      D         A
you'll search no more

(A)   Em                     D/F#   Em/G
Don't tell me it's not worth trying for
D/F#      Em                     D/F#  Em/G
You can't tell me it's not worth dying for
                  D             A               Dsus2
You know it's true, everything I do, I do it for you


Verse2

D                    A                       G                     Asus4  A
Look into your heart, you will find, there's nothing there to hide

(A)          D           A             G                    D    A
Take me as I am, take my life, I would give it all, I would sacrifice

(A)   Em                     D        Em  D/E
Don't tell me it's not worth fighting for
(D/E)   Em                       D      Em
I can't help it, there's nothing I want more
(Em)              D             A               D     Dsus  D
You know it's true, everything I do, I do it for you


Bridge
D       C            F                 C                G
There's no love like your love, and no other could give more love
        D               A                     E             A
There's no where unless you're there, all the time, all the way


Solo
G       D    Dsus       D       G       D     Dsus      D

(Solo na afinação padrão E B G D A E)

e:------------|--------|----------------------|--------|----------------|-------|-9-------|
b:------------|--------|----------------------|--------|---------9-9----|-9h-11-|---11-11-|
g:-8--10------|--8h-10-|-10h-8-6h-3-3h-6--8---|------6-|-6h-8-10-----10-|-------|---------|
d:--------8-8-|--------|----------------------|-6h-8---|----------------|-------|---------|
a:------------|--------|----------------------|--------|----------------|-------|---------|
e:------------|--------|----------------------|--------|----------------|-------|---------|

e:---------------|----------------|
b:-11h-9-9h-11-9-|----------------|
g:---------------|-8h-10-8-8-8--6-|
d:---------------|----------------|
a:---------------|----------------|
e:---------------|----------------|

Last Chorus:

D            Em                            A
Oh you can't tell me it's not worth trying for
        Em                             A
I can't help it there's nothing I want more
G            D                  A                     G
Yeah I would fight for you, I'd lie for you, walk the wire for you
         Gm
Yeah I'd die for you
              D                  Asus A     G  G6               D
You know it's true, everything I do,    oh,         I do it for you


----------------- Acordes -----------------

A = X 0 2 2 2 0
Asus = X 0 2 2 2 0
Asus4 = X 0 2 2 3 0
B = X 2 4 4 4 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/E = X X 2 2 3 2
D/F# = 2 X 0 2 3 2
Dsus = X X 0 2 3 2
Dsus2 = X X 0 2 3 0
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
Em/G = 3 X 2 4 5 X
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G6 = 3 X 2 4 3 X
Gm = 3 5 5 3 3 3
