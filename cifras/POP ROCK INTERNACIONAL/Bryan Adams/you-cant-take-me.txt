Bryan Adams - You Cant Take Me

Intro:  Em  D  C  C  Bm

- Come on!

Verso 1:
Em                           D
Got to fight another fight - I gotta run another night
C
Get it out - check it out
C                   Bm    D    C    Bm
I'm on my way and I don't feel ri - ght
Em                    D
I gotta get me back - I can't be beat and that's a fact
C
It's OK - I'll find a way
C                       Bm            ( Bm  D  Em  G )
You ain't gonna take me down, no way  (oh oh oh oh)

Refrão:
Em
Don't judge a thing until you know what's inside it

C
Don't push me - I'll fight it
G                                             Bm   ( Bm  D  Em  G )
Never gonna give in - never gonna give it up, no   (oh-oh-oh-oh)
Em
If you can't catch a wave then your'e never gonna ride it
C
You can't come uninvited
G                                          Bm
Never gonna give in - never gonna give up, no
Am7            Bm     Em     (Em  Am7  Bm  Em - Em Am7 Bm)
You can't take me I'm free

Verso 2:
Em                         D
Why did it all go wrong? - I wanna know what's going on
C
And what's this holding me?
C                  Bm    D  C Bm
I'm not where I supposed to be-e
Em
I gotta fight another fight
D
I gotta fight will all my might
C
I'm getting out , so check it out
C                          Bm                 ( Bm  D  Em  G )
You're in my way, so you better watch out     (oh-oh-oh-oh)

Refrão:
Em
Don't judge a thing until you know what's inside it
C
Don't push me - I'll fight it
G                                             Bm   ( Bm  D  Em  G )
Never gonna give in - never gonna give it up, no   (oh-oh-oh-oh)
Em
If you can't catch a wave then your'e never gonna ride it
C
You can't come uninvited
G                                          Bm
Never gonna give in - never gonna give up, no
Am7            Bm     Em     ( Em  Am7  Bm  Em  Am7  Bm )
You can't take me I'm free

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
