Bryan Adams - Let Him Know

Intro:  F9
C      Em    Am
Another day goes by
        F           C
Still you wonder what happened
             Em           Am
You'd give the world for one more try
       F
But you're too shy to ask him
   Fm                Am
You don't want to ask him now
          C             F
I know your heart will pull you through
            G
If he means that much to you, yeah

Chorus:
            C
You gotta let him know (Let him know, know that you love him)
              Em
Let him know that you love him (Let him know, know that you need him)

            Bb                          F
You gotta let him know (Let him know, know that you want him now)
C        Em           Am
What can I say? It's up to you
     F            C
You gotta make up your own mind
         Em               Am
It's your life, it's up to you
      F
Cause you didn't believe me
  Fm                     Am
You still don't believe me now
         C              F
You're gonna see it's all coming true
                 G
That's why I'm leaving it up to you, yeah

Chorus

Ab Eb D                  Eb  Db Ab
The news is out I guess you heard
                        Eb  Db  Ab
You shouldn't breathe a single word
                                 Eb     Db  G
The bottom line is nothing's going to stop you now
       G
You gotta let him know....

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D = X X 0 2 3 2
Db = X 4 6 6 6 4
Eb = X 6 5 3 4 3
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F9 = 1 3 5 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
