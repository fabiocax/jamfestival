Bryan Adams - House Arrest

Fig.1
  G         D    G   Gsus   G  D      G
 ---3--3-3----2-2-3---3-3--3-3----2-2-3--|
 ---3--3-3----3-3-3---1-1--1-0----3-3-3--|
 ---0--0-0----2-2-0---0-0--0-0----2-2-0--|
 -----------0-----0---0-0--0-0--0-----0--|
 -----------------2---2-2--2-2--------2--|
 -3---------------3---3-3--3-3--------3--|
 (w/Fig.1)
 Well there's gonna be a party, gonna make some noise
 Yeah we're invitin' all the girls, invitin' all the boys
 There'll be dancin' on the tables, dancin' on the chairs
 Yeah there'll be dancin' in the parking lots and up and down the stairs
  F   G          G   C
 Close the windows,lock the doors
  F   G         G   C
 Turn it up now, pack the floors
  F    G                G   C
 Cops are on the outside, landlord's on the phone
       D
 Quick turn out all the lights

       D
 Pretend there ain't nobody home!

           G      D           Em     C
    It's a house arrest - everybody run
           G           D              Em       C
    I gotta plead guilty havin' - too much fun
                   G     D            Em        C
    This is a house arrest - up against the wall
           G            D            Em       C
    We can't start rockin' justa havin' a ball

 (w/Fig.1)
 Won't you come around to my place, don't you know it's overdue
 You gotta' bring your little lady wear your dancing shoes
  F   G         G   C
 Get a yellow taxi take a limousine
  F   G             G   C
 You gotta' get there it's gonna be a seen
      F    G            G   C
 It's too late can't wait get out while you can
        D
 It was fun while it lasted let's do it all again

           G      D           Em    C
    It's a house arrest - everybody run
           G          D               Em       C
    I gotta plead guilty havin' - too much fun
                   G     D            Em        C
    This is a house arrest - up against the wall
            G           D             Em      C
    We can't start rockin' justa havin' a ball

 B   B   A   B                  E
We'll be waking up the neighbours
 B   B   A   B         A
Bouncin' off the walls
 B   B   A   B          D
Hangin' off the ceilings.
                  D
We'll be dancing in and out of the halls...

**Solo:   G    D    Em    C    G    D    Em    C
          G    D    Em    C    G    D    Em    C

           G      D           Em    C
    It's a house arrest - everybody run
           G          D               Em       C
    I gotta plead guilty havin' - too much fun
                   G     D            Em        C
    This is a house arrest - up against the wall
            G           D             Em      C
    We can't start rockin' justa havin' a ball


----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
Gsus = 3 5 5 5 3 3
