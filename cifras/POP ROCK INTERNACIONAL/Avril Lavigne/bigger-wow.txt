Avril Lavigne - Bigger Wow

[Intro]

F#    B  D#m   Db
Na na na na na na
F#    B  D#m   Db
Na na na na na na

[Verso 1]

               F#            B
Yeah you could show up at my front door
     D#m                 Db
On a white horse I'll be ready
       F#             B
We can jump out of an airplane
       D#m                Db
We can fall like we're confetti
          F#    B                 D#m    Db
Just like kites unafraid of those heights

[Verso 2]


         F#              B
We could sail across the ocean
       D#m        Db
Winter summer any season
        F#             B
You can kiss me on the mountain top
      D#m          C#
Don't ask me for a reason
          F#    B                 D#     Db
Just like kites unafraid of those heights
  F#  B
No    cause sooner or later

[Refrão 1]

F#            B
I just want a bigger wow
D#m           Db
   I'm not complaining
       F#            B
If you throw me up I won't come down
D#m             Db          F#  B
   Just say you waiting for me
D#m              Db
   I'll meet you in the clouds
F#            B
I just want a bigger wow
D#m         Db
   Bi-bi-bi-bi-bigger (wow)
F#    B  D#m   Db
Na na na na na na
F#    B  D#m   Db
Na na na na na na
                    F#  B  D#m  Db
Bi-bi-bi-bi-bigger (wow)

[Verso 3]

       F#            B
Quit denying stop pretending
       D#m             Db
Just admit it that you love it
       F#              B
We can light up all of Vegas
       D#m          Db
And we never will unplug it oh
  F#    B                 D#m    Db
Tonight unafraid of those heights
  F#  B
No    cause sooner or later

[Refrão 2]

F#            B
I just want a bigger wow
D#m           Db
   I'm not complaining
       F#            B
If you throw me up I won't come down
D#m             Db          F#  B
   Just say you waiting for me
D#m              Db
   I'll meet you in the clouds
F#            B
I just want a bigger wow
D#m         Db
   Bi-bi-bi-bi-bigger (wow)
F#    B  D#m   Db
Na na na na na na
F#    B  D#m   Db
Na na na na na na

[Ponte]

        B
You can tell me all the reasons
       D#m            Db
Why we shouldn't be together
     B
And spin me until I fall down
         D#m               Db
Say that this will last forever

Come on now

[Refrão 3]

F#            B
I just want a bigger wow
D#m           Db
   I'm not complaining
       F#            B
If you throw me up I won't come down
D#m             Db          F#  B
   Just say you waiting for me
D#m              Db
   I'll meet you in the clouds
F#            B
I just want a bigger wow
D#m         Db
   Bi-bi-bi-bi-bigger (wow)
F#    B  D#m   Db
Na na na na na na
F#    B  D#m   Db
Na na na na na na
Bi-bi-bi-bi-bigger (wow)
F#    B  D#m   Db
Na na na na na na
F#    B  D#m   Db
Na na na na na na

Bi-bi-bi-bi-bigger (wow)

----------------- Acordes -----------------
B = X 2 4 4 4 2
C# = X 4 6 6 6 4
D# = X 6 5 3 4 3
D#m = X X 1 3 4 2
Db = X 4 6 6 6 4
F# = 2 4 4 3 2 2
