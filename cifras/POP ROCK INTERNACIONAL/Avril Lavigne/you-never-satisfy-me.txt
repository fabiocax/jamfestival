Avril Lavigne - You Never Satisfy Me

Intro: C#m-B-F#-B (x2)

C#m                               B
  Excuse me sir, do I have your attention
F#                                                B
  there's something if you don't mind I'd like to mention
C#m                            B             F#
  so open those ears, sit your butt down and shut your mouth Oh
C#m                                   B
  I'm aware that it's hard for you to do that
F#                                B
  all I ask is lay down, put your feet up
C#m                                   B      F#
  now soak in, pay attention, learn a lesson loud and clear


Refrão:
C#m                 C#m+M7
Hey, you're a dirty liar
    B                  F#
you wronged me out for something I thought was right
  C#m                C#m+M7
I wanted to be left alone
    B            F#
not shown around like your golden prize
A             G#
oh, I looked around
  A                             G#
I look what happened, you found out
                  C#m
you never satisfy me

(Violão Solo)

C#m                           B          F#
  Never trusted anyone in the first place
                                   B
you turned me up, your disgusting anyway
C#m
  hear that music, crank it up
         B         F#              B
is there something familiar in the singers voice
C#m                        B    F#
  turn it down, now listen to me
                                  B      C#m
what made you think you were my authority
                            B       F#
I'm awake, finally, Now I'm able to move on (just gotta tell the truth)

(Refrão)

A
  Now do these word stick to you, hope they do
C#m
think about it, think about it
A                                                           G#
  stick to you like superglue, keep you from the things you do Oh

(Refrão)

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
F# = 2 4 4 3 2 2
G# = 4 3 1 1 1 4
