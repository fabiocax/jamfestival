Avril Lavigne - Ironic

    F#  B   F#            G#m
an old man turned ninety-eight
           F#   B      F#            G#m
he won the lottery and died the next day
       F#    B           F#    G#m
it's a black fly in your chardonnay
       F#        B          F#         G#m
it's a death row pardon two minutes to late
          F#   B              F#    G#m
isn't it ironic... don't you think
                     F# B         F#      G#m
chorus:    it's like rain on your wedding day
                       F# B             F#      G#m
           it's a free ride when you've already paid
                         F#   B          F#          G#m
           it's the good advice that you just didn't take
           A              E             F#
           who would have thought... it figures
mr. play-it-safe was afraid to fly
he packed his suitcase and kissed his kids good-bye

he waited his whole damn life just to take that flight
and as the plane crashed down he thouht "well isn't this nice..."
             F#    B             F#     G#m
and isn't it ironic... don't you think?
(chorus)
     B                                F#
well life has a funny way of sneaking up on you
                                      G#m
when you think everything is okay and everything's going right
     B                               F#
and life has a funny way of helping you out when
                                      G#m
you think everything's gone wrong and everything blows up in your face
B   F#   (B very short)
a traffic jam when you are already late
a no-smoking sign on your cigarette break
it's like ten thousand spoons when all you need is a knife
it's like meeting the man of my dreams then meeting his beautiful wife
             F#     B            F#      G#m
and isn't it ironic... don't you think?
             F#     B             F#              G#m
a little too ironic... and yeah i really do think...
(chorus)
 B                                     F#
life has a funny way of sneaking up on you
 B                         F#                B
life has a funny, funny way  of helping you out -- helping you out

Mostrar acordes para teclado

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
