Avril Lavigne - Losing Grip

Intro 2x: G#m E F# E

G#m                 E               F#    E
Are you aware of what you make me feel baby
G#m               E             F#              E
Right now I feel invisible to you like I'm not real
G#m          E              F#                E             G#m
Didn't you feel me lock my arms around you why'd you turn away
 E                     F#  E
Here's what I have to say

              G#m                    E                          F#
I was left to cry there waiting outside there grinning with a lost stare
E
That's when I decided

G#5  D#m          E  D#m
Why should I care
       G#5                    D#m            E         C#5
Cause you weren't there when I  was scared I was so alone
G#5  D#m             E    D#m
You,  you need to listen

      G#5                  D#m                    E               C#5
I'm starting to trip I'm losing my grip and I'm in this thing alone

G#m E F# E

G#m         E                      F#           E
Am I just some chick you place beside you to take somebody's place
G#m              E             F#            E
When you turn around can you recognize my face
G#m        E                      F#           E              G#m
You used to love me you used to hug me but that wasn't the case
E                   F#   E
Everything wasn't okay

              G#m                    E                          F#
I was left to cry there waiting outside there grinning with a lost stare
E
That's when I decided

G#5  D#m          E  D#m
Why should I care
       G#5                    D#m            E         C#5
Cause you weren't there when I  was scared I was so alone
G#5  D#m             E    D#m
You,  you need to listen
      G#5                  D#m                    E               C#5
I'm starting to trip I'm losing my grip and I'm in this thing alone

C#          E       F#      E
Crying out loud I'm crying out loud
C#          E       F#      E
Crying out loud I'm crying out loud

C# E         F# E
   Open your eyes
C# E
   Open up wide

G#5 D#m           E  D#m
Why       should I care
      G#5                        D#m            E
Cause you weren't there when I was scared I was so alone

G#5  D#m          E  D#m
Why should I care
       G#5                    D#m            E         C#5
Cause you weren't there when I was scared I was so alone
G#5  D#m             E    D#m
 Why should I care
      G#5                  D#m                    E           C#5
If you don't care then I don't care we're not going anywhere
G#5  D#m          E  D#m
Why should I care
       G#5                    D#m            E         C#5
Cause you weren't there when I was scared I was so alone
G#5  D#m             E    D#m
Why should I care
      G#5                  D#m                    E           C#5   G#5
If you don't care then I don't care we're not going anywhere.

----------------- Acordes -----------------
C# = X 4 6 6 6 4
C#5 = X 4 6 6 X X
D#m = X X 1 3 4 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
G#5 = 4 6 6 X X X
G#m = 4 6 6 4 4 4
