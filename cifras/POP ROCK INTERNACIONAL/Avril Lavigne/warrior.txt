Avril Lavigne - Warrior

[Intro] C#m  A  E  C#m  A  E

[Verso 1]

C#m                              A                      E
   I'll pick my battles 'cause I know I'm gonna win the war (Win the war)
C#m                        A                       E
   I'm not rattled cause I shattered all of this before (This before)
 C#m                          A                        E
Steadier than steel cause I'm ready with my shield and sword (Shield and sword)
C#m                           A                            E
   Back on the saddle cause I gathered all my strength for more (Strength for more)

[Pré-Refrão]

    A           B
And I won't bow I won't break
C#m                         E
No I'm not afraid to do whatever it takes
     A              B
I'll never bow I'll never break


[Refrão 1]

            A         E
Cause I'm a warrior I fight for my life
       C#m     B
Like a soldier all through the night
            A       E                    C#m    B
And I won't give up I will survive I'm a warrior
         A              E
And I'm stronger that's why I'm alive
       C#m     B
I will conquer time after time
           A      E                    C#m    B
I'll never falter I will survive I'm a warrior

A           E
Woah, woah, woah, woah
C#m         B
Woah, woah, woah, woah
A           E
Woah, woah, woah, woah
C#m           B
   I'm a warrior

[Verso 2]

C#m                      A                            E
   Like a viking I'll be fighting through the day and night (Day and night)
C#m                             A                        E
   I'll be marching through the darkness til the morning light (Morning light)
C#m                            A                     E
Even when it's harder like the armor you will see me shine (See me shine)
C#m                            A                        E
   No I won't stop and I won't drop until the victory's mine (Oh)

[Pré-Refrão]

    A           B
And I won't bow I won't break
C#m                         E
No I'm not afraid to do whatever it takes
     A              B
I'll never bow I'll never break

[Refrão 2]

            A         E
Cause I'm a warrior I fight for my life
       C#m     B
Like a soldier all through the night
            A       E                    C#m    B
And I won't give up I will survive I'm a warrior
         A              E
And I'm stronger that's why I'm alive
       C#m     B
I will conquer time after time
           A      E                    C#m    B
I'll never falter I will survive I'm a warrior

[Ponte]

A            E
Oh you can't shoot me down
           C#m
You can't stop me now
        B
I got a whole damn army
A              E
Oh they try to break me down
            C#m
They try to take me out
          B
You can't cut a scar on me

[Refrão 3]

            A         E
Cause I'm a warrior I fight for my life
       C#m     B
Like a soldier all through the night
            A       E                    C#m    B
And I won't give up I will survive I'm a warrior
         A              E
And I'm stronger that's why I'm alive
       C#m     B
I will conquer time after time
           A      E                    C#m    B
I'll never falter I will survive I'm a warrior

A           E
Woah, woah, woah
C#m    B
   whoa I'm a warrior
A           E
Woah, woah, woah
C#m    B             C#m  A  E
   whoa I'm a warrior

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
