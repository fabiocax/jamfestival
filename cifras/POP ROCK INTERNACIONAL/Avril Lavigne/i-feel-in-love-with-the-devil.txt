Avril Lavigne - I Feel In Love With The Devil

[Intro] F#m  A  E  Bm
        F#m  A  E  Bm

[Verso 1]

F#m                  A
   Shotguns and roses make a deadly potion
E                      D
  Heartbreak explosions in reckless motion
F#m
  Teddy bears and "I'm sorry" letters
A
  Don't seem to make things better
E
  Don't bury me alive
D
  Sweet talking alibi

[Pré-Refrão 1]

    F#m
But I-I-I-I-I can't stop the rush

    A
And I-I-I-I-I can't give you up
   E
No I-I-I-I-I know you're no good for me
D
  You're no good for me

[Refrão 1]

                        F#m  A
I fell in love with the Devil
               E
And now I'm in trouble
Bm                        F#m
  I fell in love with the Devil
    A                   E             Bm
I'm underneath his spell (Oh-oh-oh-oh)
                   F#m  A
Someone send me an angel
             E
To lend me a halo
Bm                         F#m
   I fell in love with the Devil
       A                 E              Bm
Please save me from this hell (Oh-oh-oh-oh)

[Verso 2]

F#m
   Got me playing with fire (Playing with fire)
A
   Baby hand me the lighter (Hand me the lighter)
E
   Tastes just like danger (Tastes just like danger)
Bm
   Chaotic anger

[Pré-Refrão 2]

    F#m
But I-I-I-I-I can't stop the rush
    A
And I-I-I-I-I can't give you up
   E
No I-I-I-I-I know you're no good for me
D
  You're no good for me

[Refrão 2]

                        F#m  A
I fell in love with the Devil
               E
And now I'm in trouble
Bm                        F#m
  I fell in love with the Devil
    A                   E             Bm
I'm underneath his spell (Oh-oh-oh-oh)
                   F#m  A
Someone send me an angel
             E
To lend me a halo
Bm                         F#m
   I fell in love with the Devil
       A                 E              Bm
Please save me from this hell (Oh-oh-oh-oh)

[Ponte]

D                         F#m
  Angels and Devils always fight over me (Fight over me)
A                   E
  Take me to heaven wake me up from this dream
D                         F#m
  Even in sunlight clouds shadow over me (Shadow on me)
A                   E
  It's now or never wake me up from this dream

[Refrão 2]

                        F#m  A
I fell in love with the Devil
               E
And now I'm in trouble
Bm                        F#m
  I fell in love with the Devil
    A                   E
I'm underneath his spell
                   F#m  A
Someone send me an angel
             E
To lend me a halo
Bm                         F#m
   I fell in love with the Devil
       A                 E              Bm
Please save me from this hell (Oh-oh-oh-oh)

F#m A    E   D
Dig deep six feet (Ahh, ahh, ahh ahh, ahh, ahh, ahh)
F#m A         E       D
Dig deep it's killing me (Ahh ahh ahh ahh)

(I fell in love with the Devil)
F#m A    E   D
Dig deep six feet (Ahh ahh ahh ahh ahh, ahh, ahh)
(And now I'm in trouble)
(I fell in love with the Devil)
F#m A
Dig deep (Ahh, ahh, ahh, ahh)
     E       D E
It's killing me

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
