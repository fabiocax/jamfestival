Avril Lavigne - 4 Real

   C
If i show you
       G
Get to know you
       Am         F
If i hold you just for today
                     C
I’m not gonna wanna let go
 G
I’m not gonna wanna go home
Am                   F
Tell me you feel the same

refrão:
Am
'cause i’m for real are you for real?
C
I can’t help myself it’s the way i feel
G
When you look me in the eyes like you did last night
F
I can’t stand to hear you say goodbye

Am
But it feels so right 'cause it feels so right
C
Just to have you standing by my side
G
So don’t let me go cause you have my soul

F
And i just wanted you to know
              C
I don’t wanna look back
                  G
Cause i know that we have
              Am              F
Something the past could never change.
                    C
And i’m stuck in the moment
                G
And my heart is open
Am                        F
Tell me that you feel the same

Refrão:
Am
'cause i’m for real are you for real?
C
I can’t help myself it’s the way i feel
G
When you look me in the eyes like you did last night
F
I can’t stand to hear you say goodbye
Am
But it feels so right 'cause it feels so right
C
Just to have you standing by my side
G
So don’t let me go cause you have my soul

Ponte:
G           Dm
Hold (hold) me down (me down)
Dm          Am
Hold (hold) me now
    G                   Dm
I’m safe (i’m safe) i’m sound (sound)
     Dm     Am
When you’re around
G           Dm
Hold (hold) me down (me down)
Dm          Am
Hold (hold) me now
    G                   Dm
I’m safe (i’m safe) i’m sound (sound)
     Dm     Am
When you’re around

Refrão:
Am
'cause i’m for real are you for real?
C
I can’t help myself it’s the way i feel
G
When you look me in the eyes like you did last night
F
I can’t stand to hear you say goodbye
Am
But it feels so right 'cause it feels so right
C
Just to have you standing by my side
G
So don’t let me go cause you have my soul
F
And i just wanted you to know (i’m for real)
Am
'cause i’m for real are you for real?
C
I can’t help myself it’s the way i feel
G
When you look me in the eyes like you did last night
F
I can’t stand to hear you say goodbye
Am
But it feels so right 'cause it feels so right
C
Just to have you standing by my side
G
So don’t let me go cause you have my soul
F
And i just wanted you to know.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
