Avril Lavigne - Bitchin' Summer

Capo Casa 1

F C Am G (4x)

Uh oh oh oh
Uh Oh Oh

   F          C            Am         G
Everyone's waiting on the bell (on the bell )
   F               C               Am           G
A Couple seconds we'll be raising hell (raising hell)
   F               C      Am                G
The sun is shining down, School is finally out
   F            C               Am           G
Nothing matters so we might as well (might as well)

   F            C
Everybody's baking in the sun
  Am           G
Come and party do it while we're young
   F            C
Move your body when you hear the drums
 G
So put your hands up


                   F       C
It's gonna be a bitchin' summer
  Am                     G
We'll be living fast kicking ass together
        F       C
Like high school lovebirds
  Am                 G
Gonna have a blast make it last forever
        F          C
I'll pick you up at the liquor store
  Am                     G
Pour it up and you get one more
      F       C
It's now or never
      Am                   G
It's gonna gonna be be a bitchin' summer

   F     C   Am       G
Oh oh oh oh oh oh oh, Bitchin' summer
   F     C   Am       G
Oh oh oh oh oh oh oh, Bitchin' summer

   F          C            Am         G
Throwin' empty bottles in the fire
 F             C            Am         G
Rest has got us singing like a choir
   F               C      Am                G
We're not gonna run when the police come

   F            C               Am           G
But in the morning we are so alive

   F            C
Everybody's baking in the sun
  Am           G
Come and party do it while we're young
   F            C
Move your body when you hear the drums
 G
So put your hands up

                   F       C
It's gonna be a bitchin' summer
  Am                     G
We'll be living fast kicking ass together
        F       C
Like high school lovebirds
  Am                 G
Gonna have a blast make it last forever
        F          C
I'll pick you up at the liquor store
  Am                     G
Pour it up and you get one more
      F       C
It's now or never
      Am                   G
It's gonna gonna be be a bitchin' summer

   F     C   Am       G
Oh oh oh oh oh oh oh, Bitchin' summer
   F     C   Am       G
Oh oh oh oh oh oh oh, Bitchin' summer

   F          C            Am         G
Like we at the beach, the party don't stop
   F               C               Am           G
If we don't get harassed by the motherfuckin cops
   F               C      Am                G
A quarter taking gas, I bought a half a pack
   F               C               Am           G
My cell phone's dying but there's no turning back
   F            C               Am           G
I can feel the breeze down on my knee
   F               C               Am           G
Drummin on the dashboard, bumpin to the beat
   F               C               Am           G
Hit the windshield wiper, searching for my lighter
   F               C               Am           G
I gotta get right before this all nighter

                   F       C
It's gonna be a bitchin' summer
  Am                     G
We'll be living fast kicking ass together
        F       C
Like high school lovebirds
  Am                 G
Gonna have a blast make it last forever
        F          C
I'll pick you up at the liquor store
  Am                     G
Pour it up and you get one more
      F       C
It's now or never
      Am                   G
It's gonna gonna be be a bitchin’ summer

   F     C   Am       G
Oh oh oh oh oh oh oh, Bitchin' summer
   F     C   Am       G
Oh oh oh oh oh oh oh, Bitchin' summer

                   F       C
Pick you up at the liquor store
  Am                     G
Pour it up and you get one more
        F       C
It's now or never
  Am                     G
It's gonna gonna be be a bitchin’ summer

----------------- Acordes -----------------
Capotraste na 1ª casa
Am*  = X 0 2 2 1 0 - (*A#m na forma de Am)
C*  = X 3 2 0 1 0 - (*C# na forma de C)
F*  = 1 3 3 2 1 1 - (*F# na forma de F)
G*  = 3 2 0 0 0 3 - (*G# na forma de G)
