Avril Lavigne - My Happy Ending

( Versão Acústica )

So much for my happy ending
Riff
Bm     G      D      A
oh oh, oh oh, oh oh, oh oh...
Bm     G      D      A
oh oh, oh oh, oh oh, oh oh...

                   Bm
Let's talk this over
                       G
It's not like we're dead
                        D
Was it something I did?
                        A
Was it something You said?
                    Bm
Don't leave me hanging
                G
In a city so dead

                 D
held up so high
                            A
On such a breakable thread

           G                A
You were all the things I thought I knew
    G                   A
And I thought we could be

              G               D          A
You were everything, everything that I wanted
              G               D               A
We were meant to be, supposed to be, but we lost it
             G               D                 A    Bm
ALL of the memories, so close to me, just fade away

E5
All this time you were pretending
G
So much for my happy ending

Riff
oh oh, oh oh, oh oh...
oh oh, oh oh, oh oh...

                Bm
You've got your dumb friends
                 G
I know what they say
                   D
They tell you I'm difficult
            A
But so are they
                Bm
But they don't know me
                   G
Do they even know you?
                          D
All the things you hide from me
                      A
All the shit that you do

          G                 A
You were all the things I thought I knew
    G              A
And I thought we could be

              G              D          A
You were everything, everything that I wanted
           G                 D               A
We were meant to be, supposed to be, but we lost it
              G              D                 A   Bm
ALL OF the memories, so close to me, just fade away
E5
All this time you were pretending
G
So much for my happy ending

A                           G
It's nice to know that you were there
                             A
Thanks for acting like you cared
                                       G
And making me feel like I was the only one
A                                 Bm
It's nice to know we had it all
                                   G
Thanks for watching as I fall
                             A
And letting me know we were done

        G              D          A
He was everything, everything that I wanted

Distorção
              G              D               A
We were meant to be, supposed to be, but we lost it
              G              D                 A   Bm
ALL OF the memories, so close to me, just fade away

E5
All this time you were pretending
G
So much for my happy ending

              G              D         A
You were everything, everything that I wanted
              G              D               A
We were meant to be, supposed to be, but we lost it
              G              D                A   Bm
ALL OF the memories, so close to me, just fade away

E5
All this time you were pretending
G
So much for my happy ending

Riff
oh oh, oh oh, oh oh...
oh oh, oh oh, oh oh...       G
oh oh, oh oh, oh oh...oh..ohhhh

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E5 = 0 2 2 X X X
G = 3 2 0 0 0 3
