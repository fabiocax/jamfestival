Red Hot Chili Peppers - Even You, Brutus?

Intro: Cm  Am  Cm  Am  Ab  Bb

Verso 1:
Cm
God is good and fate is great,
              Am
when it feels so strong it's hard to wait.
               Cm
You never know how it might unfold,
                           Am
hearts to break and bodies to hold.

Time will tell but it never gets old.
F                   Ab         Bb
Like I told you I'm in for the long long.

Verso 2:
Cm                                                      Am
Had to of been one of those things that took place in outer space.
                                           Cm
A match made in heaven, not a scratch on her face.

                                   Am
Angels must have smoked some dust,

singing songs about "In God We Trust".
          F                            Ab               Bb
Something sly, but this is a must.
             Like I told you there's a time for finding out.

Pre-Refrão 1:
Cm
Ahh people try to tell me what not to do,
               Am
"You shouldn't make it with a younger girl." and
Cm
Too many chances for the foot to drop,
           Am
I tried to tell 'em that I found a pearl, well...
F                  Ab                  Bb
Like I told you be careful what you're asking for.

Refrão:
Dm         Dm
Hey, sister Brutus
      Cm               Gm    Dm
Gotta mess of a better half. Oww
Dm
Hey, sister Judas!
     Cm               Gm
Even you never had my back.

Verso 3:
Cm
She was the cutest thing that I ever did see.
  Am
A drink in her hand and I don't mean tea.
               Cm
She was with a dude, but I just didn't care.
                   Am
I had to find out, these moments are rare.
                  F                              Ab            Bb
And Stevie says, "All is fair."
                        Like I told you there is something for everyone.

Pre-Refrão 2:
Cm                                               Am
Well I guess I never told you some things that I really, really wanted to say.
Cm                                         Am
Did I do everything I could do, or did the mice just need to play?
F                       Ab                  Bb
Like I told you there's more than meets the eye.

Refrão:
Dm         Dm
Hey, sister Brutus
      Cm               Gm    Dm
Gotta mess of a better half. Oww
Dm
Hey, sister Judas!
     Cm               Gm
Even you never had my back.

( Cm Am Cm Am Ab Bb )

Pre-Refrão 3:
Cm                                              Am
I'm not trying to point fingers at you, and I'm not trying to lay any blame.
Cm                                            Am
But when it comes to the punishment, girl you know how to bring the pain.
F                   Ab        Bb
Like I told you I'd do it all again.

Refrão:
Dm         Dm
Hey, sister Brutus
      Cm               Gm    Dm
Gotta mess of a better half. Oww
Dm
Hey, sister Judas!
     Cm               Gm
Even you never had my back. Come on!

Refrão:
Dm         Dm
Hey, sister Brutus
           Cm                  Gm    Dm
There's a fury to the woman's wrath. Oww
Dm
Hey, sister Judas!
     Cm               Gm
Even you never had my back

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
