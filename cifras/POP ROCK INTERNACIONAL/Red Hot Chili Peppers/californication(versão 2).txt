Red Hot Chili Peppers - Californication

Introdução 2x: Am  F


[Primeira Parte]

  Am
Psychic spies from China
         F
Try to steal your mind's elation
 Am
Little girls from Sweden
          F
Dream of silver screen quotations
    C                  G
And if you want these kind of dreams
      F        Dm    Am  F  Am  F
It's Californication

         Am
It's the    edge of the world
            F
And all of western civilization

     Am
The sun may rise in the East
             F
At least it settles in the final location
     C                G
It's understood that Hollywood
       F        Dm
Sells Californication


Primeira Ponte: Am  F  Am  F

 Am
Pay your surgeon very well
     F
To break the spell of aging
   Am
Celebrity Skin is this your chin
   F
Or is that war your waging
Am                F
   First born unicorn
Am                 F
   Hard core soft porn


[Primeiro Refrão]

  C           G    Dm    Am
Dream of Californication
  C           G    Dm
Dream of Californication


Segunda Parte: Am  F  Am  F

 Am
Marry me girl be my fairy to the world
       F
Be my very own constellation
   Am
A teenage bride with a baby inside
         F
Getting high on information
     C                    G
And buy me a star on the Boulevard
      F        Dm    Am  F  Am  F
It's Californication

  Am
Space may be the final frontier
          F
But it's made in a Hollywood basement
 Am
Cobain can you hear the spheres
         F
Singing songs off station to station
    C               G
And Alderaan's not far away
      F        Dm
It's Californication


Segunda Ponte: Am  F  Am  F

 Am
Born and raised by those who praise
     F
Control of population
Am
Everybody's been there and
   F
I don't mean on vacation
Am                F
   First born unicorn
Am                 F
   Hard core soft porn


[Segundo Refrão]

  C           G    Dm    Am
Dream of Californication
  C           G    Dm
Dream of Californication
  C           G    Dm    Am
Dream of Californication
  C           G    Dm
Dream of Californication


Solo 2x: F#m  D  F#m  D
         Bm  D  A  E

Solo 3x: Bm  D  A  E


Terceira Parte: Am  F  Am  F

     Am
Destruction leads to a very rough road
       F
But it also breeds creation
    Am
And earthquakes are to a girl's guitar
         F
They're just another good vibration
     C                    G
And tidal waves couldn't save the world
      F        Dm
From Californication


Terceira Ponte: Am  F  Am  F

 Am
Pay your surgeon very well
     F
To break the spell of aging
 Am
Sicker than the rest

There is no test
      F
But this is what you're craving
Am                F
   First born unicorn
Am                 F
   Hard core soft porn


[Terceiro Refrão]

  C           G    Dm    Am
Dream of Californication
  C           G    Dm
Dream of Californication
  C           G    Dm    Am
Dream of Californication
  C           G    Dm
Dream of Californication

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
