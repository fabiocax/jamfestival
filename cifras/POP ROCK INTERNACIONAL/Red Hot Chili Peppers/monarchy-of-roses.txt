Red Hot Chili Peppers - Monarchy Of Roses

Primeira Parte:
E|-----------------------------|
B|-----------------------------|
G|-----------------------------|
D|-----------------------------|
A|-----------------------------|
E|-0-0-0--0-0-0--0-0-0--0-0-0--|

The crimson tide is flowing thru
Your fingers as you sleep
The promise of a clean regime
Are promises we keep

E|-----------------------------|
B|-----------------------------|
G|-----------------------------|
D|-----------------------------|
A|-----------------------------|
E|-0---3---7--6~~--------------|

Do you like it rough I ask

And are you up to task
The calicos of Pettibon
Where cultures come to clash

Refrão:
 C             Em
Several of my best friends wear
     C              Em
The colors of the crown
 C              Em
Mary wants to build it up
      D               Bm              C    Em
And Sherri wants to tear it all back down girl
     C              Em
The savior of your light
     D           Bm
The monarchy of roses
     D           Bm
The monarchy of roses
E|-------------14~-14--15---14-19-14-15~-|
B|-12~...--------------------------------|
G|---------------------------------------|
D|---------------------------------------|
A|---------------------------------------|
E|---------------------------------------|


Segunda Parte:
E|-----------------------------|
B|-----------------------------|
G|-----------------------------|
D|-----------------------------|
A|-----------------------------|
E|-0---3---7--6~~--------------|

The cross between my former queen
Her legendary stare
The holy tears of Ireland
A lovely cross to bear

Refrão:
 C             Em
Several of my best friends wear
     C              Em
The colors of the crown
 C              Em
Mary wants to build it up
      D               Bm              C    Em
And Sherri wants to tear it all back down girl
     C              Em
The savior of your light
     D           Bm
The monarchy of roses
     D           Bm
The monarchy of roses tonight
Solo:
E|----------------------------|
B|----------------------------|
G|----------------------------| (repete)
D|----------------------------|
A|----------------------------|
E|-0------0------0------0-----|

E|----12-----12----12-----12--|
B|----------------------------|
G|-13-----13----14-----14-----|  (8x)
D|----------------------------|
A|----------------------------|
E|----------------------------|


Refrão:
 C             Em
Several of my best friends wear
     C              Em
The colors of the crown
 C              Em
Mary wants to build it up
      D               Bm              C    Em
And Sherri wants to tear it all back down girl
     C              Em
The savior of your light
     D           Bm
The monarchy of roses
     D           Bm
The monarchy of roses

Final:
 G5  A5
Heeeeey
 C5              Eb5
We all want the rose you know
 G5  A5
Heeeeey
  C5            Eb5
Show us love before you go
 G5              A5
Say I will then say I want to
      C5                Eb5
The story knows that I will never taunt you
 G5  A5
Heeeeey
 C5              Eb5
We all want the rose you know
 G5  A5
Heeeeey
  C5            Eb5
Show us love before you go
 G5              A5
Say I will then say I want to
      C5                Eb5              G5
The story knows that I will never taunt you

----------------- Acordes -----------------
A5 = X 0 2 2 X X
Bm = 7 9 9 7 7 7
C = 8 10 10 9 8 8
C5 = X 3 5 5 X X
D = 10 12 12 11 10 10
Eb5 = X 6 8 8 X X
Em = X 7 9 9 8 7
G5 = 3 5 5 X X X
