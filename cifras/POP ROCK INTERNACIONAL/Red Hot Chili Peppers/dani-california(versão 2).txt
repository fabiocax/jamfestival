Red Hot Chili Peppers - Dani California

[Intro] Am  G  Dm  Am
        Am  G  Dm  Am

Am             G
Getting born in the state of Mississippi
Dm                     Am
Papa was a copper, and her mama was a hippy
Am             G
In Alabama she would swing a hammer
Dm                         Am
Price you got to pay when you break the panorama
Am          G                     Dm           Am
She never knew that there was anything more than poor
Am         G                   Dm       Am
What in the world does your company take me for?

Am               G
Black bandanna, sweet Louisiana
Dm                  Am
Robbin' on a bank in the state of Indiana
Am
She's a runner

  G
Rebel, and a stunner
Dm                      Am
On her merry way saying baby, watcha gonna?
Am           G               Dm      Am
Looking down the barrel of a hot metal forty-five
Am           G          Dm
Just another way to survive

       F    C       Dm
California, rest in peace
      F      C    Dm
Simultaneous release
     F      C      Dm
California, show your teeth
         F
She's my priestess
C        Dm
I'm your priest

Yeah, yeah, yeah

[Solo] Am  G  Dm  Am
       Am  G  Dm  Am

Am             G
She's a lover, baby, and a fighter
Dm                            Am
Should've seen it coming when I got a little brighter
Am               G
With a name like Dani California
Dm                         Am
Day was gonna come when I was gonna mourn ya
Am           G               Dm      Am
A little loaded, she was stealing another breath
Am          G       Dm
I love my baby to death

       F    C       Dm
California rest in peace
      F      C    Dm
Simultaneous release
     F      C      Dm
California show your teeth
         F           C        Dm
She's my priestess, I'm your priest
Yeah, yeah

Am   G      Dm        Am
Who knew the other side of you?
Am   G      Dm        Am
Who knew that others died to prove?
Am   G      Dm        Am
Too true to say goodbye to you
Am   G      Dm        Am
Too true to say, say, say

Am                  G
Pushed the fader, gifted animator
Dm                        Am
One for the now, and eleven for the later
Am             G
Never made it up to Minnesota
Dm                 Am
North Dakota man Wasn't gunnin' for the quota
Am             G                Dm            Am
Down in the badlands she was saving the best for last
Am         G            Dm
It only hurts when I laugh
          Bm
Gone too fast

       F    C       Dm
California rest in peace
      F      C    Dm
Simultaneous release
     F      C      Dm
California show your teeth
         F           C        Dm
She's my priestess, I'm your priest
Yeah, yeah

       F    C       Dm
California rest in peace
      F      C    Dm
Simultaneous release
     F      C      Dm
California show your teeth
         F           C        Dm
She's my priestess, I'm your priest
Yeah, yeah

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
