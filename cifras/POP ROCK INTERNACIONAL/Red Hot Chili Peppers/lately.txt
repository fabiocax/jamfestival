Red Hot Chili Peppers - Lately

Verse:

Am          C              Dm Am          (4 times)
     On the corner into my car
Am          C                 Dm Am
     I drive forever never get far
Am          C                 Dm Am
     The 45 is slowly getting loaded
Am          C                 Dm Am
     I send another song to my star
Am          C                 Dm Am

Chorus:

     California mountains in the mornin' Stitch it up, and get it all
Dm                               F                      Am
     All along the woman's got a want it Stitch it up, and get it all
Dm                               F                      Am
     We all walk your pavement, get it off
Dm                          F            Am
Take more than your....

Dm                        F
                                  Whoa whoa
Am          C                  Dm    Am           (2 times)

Verse:

    A pendulum inside of my bed
Am          C              Dm Am
    Swingin' low, I hope to get hit
Am          C                 Dm Am
    The overflow is slow, but not enough no
Am          C                  Dm Am
    Another drop will never fix it
Am          C                Dm Am

Chorus:

California mountains in the mornin' Get along, and get it all
Dm                   F                Am
All along the woman's got a want it Stitch it up, and get it all
Dm                   F                Am
We all walk your pavement, get if off
Dm                  F      Am
Take more than your friends
Dm             F      E

Bridge:

If I call you lately
F           Am Asus4
I could be your man
F           Am Asus4
I could be your greatness
F             Am Asus4
If I call you lately
F          Am Asus4
I could be your man
F          Am Asus4
                       whoa whoa
Am          C            Dm Am           (2 times)

Verse:

I shut it down the look in my eyes
Am          C               Dm Am
Knock 'em dead a' covered in flies
Am          C               Dm Am
The big sleep without a bed of roses
Am          C                Dm Am
The light of day will never tell lies
Am          C                Dm Am

Chorus:

All along the woman's got a want it Stitch it up, and get it all
Dm                  F                  Am
California mountains in the mornin' Stitch it up, and get it all
Dm                  F                  Am
We all walk your pavement
Dm                  F                  Am
Take more than your friends
Dm              F      E

Bridge:

If I call you lately
F              Am  Asus4
I could be your man
F              Am  Asus4
I could be your greatness
F              Am  Asus4
If I call you lately
F             Am  Asus4
I could be your man
F            Am  Asus4

Verse:

On the river into my dock
Am         C      Dm Am
I paddle throught the shadow and fog
Am         C               Dm Am
To ponder all of those who came before me
Am         C                 Dm Am
Another piece for building my arch
Am         C                Dm Am

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Asus4 = X 0 2 2 3 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
