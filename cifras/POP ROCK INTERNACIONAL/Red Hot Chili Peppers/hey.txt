Red Hot Chili Peppers - Hey

Introdução:

   Cm      A#6/C
E|-3--3-3---3---3---3---3---3---3-3---3---3---3---3---3---3-3---3------------------|
B|-4--4-4---3---4---3---4---3---4-4---3---4---3---4---3---4-4---4---4---3----------|
G|-5--5-5---3---5---3---5---3---5-5---3---5---3---5---3---5-5-------5---3---3---5--|
D|-5--5-5---3---5---3---5---3---5-5---3---5---3---5---3---5-5---------------5---5--|
A|-3--3-3---3---3---3---3---3---3-3---3---3---3---3---3---3-3----------------------|
E|---------------------------------------------------------------------------------|

E|-3--3-3---3---3---3---3---3---3-3---3---3---3---3---3---3-3---4--3--4--3---------|
B|-4--4-4---3---4---3---4---3---4-4---3---4---3---4---3---4-4---4--4--4--4---------|
G|-5--5-5---3---5---3---5---3---5-5---3---5---3---5---3---5-5---------------3---5--|
D|-5--5-5---3---5---3---5---3---5-5---3---5---3---5---3---5-5---------------5---5--|
A|-3--3-3---3---3---3---3---3---3-3---3---3---3---3---3---3-3----------------------|
E|---------------------------------------------------------------------------------|


Riff:
E|--------5h6-----------------------------6---6h8-------11----|
B|--6h8-6-----8--6---6h8-6---------6h8-6----6-----6-----11----|
G|8----------------8--------8-7-8-------------------8-8s11-8--|
D|------------------------------------------------------------|
A|------------------------------------------------------------|
E|------------------------------------------------------------|


E|--------5h6--------------------------------------------------------|
B|--6h8-6-----8--6---6h8-6---------6h8-6--------------------4--------|
G|8----------------8--------8-7-8---------5-3-3h5-3-----------3------|
D|--------------------------------------------------3-1-3h5-----3--1-|
A|-------------------------------------------------------------------|
E|-------------------------------------------------------------------|

Depois do 2º Riff:

      Fm       D#       C#      G#m      A#m
E|---8----|---6----|---4----|---4----|---6----|
B|---9----|---8----|---6----|---4----|---6----|
G|---10---|---8----|---6----|---5----|---7----|  2X
D|---10---|---8----|---6----|---6----|---8----|
A|---8----|---6----|---4----|---6----|---8----|
E|--------|--------|--------|---4----|---6----|


Letra:
Won't somebody come along
And teach me how to keep it alive?
To survive.

Come along and show me something
That I never knew in your eyes.
Take away the tournicate.

I used to be so full of my confidence,
I used to know just what I wanted and just where to go.
More than ever I could use a coincidence,
But now I walk alone and talk about it when I know.

Hey, oh yeah, how long?
I guess I ought to walk away,
Hey, oh yeah, so long...
What you gonna do today?

I don't wanna have to, but I will,
If that's what I'm supposed to do.
We don't wanna set up for the kill,
But that's what I'm about to do.

Let it on, I'll cut you off when you're screaming into the phone.
Hard to own.
Anyway, I wanna let you know that everything is on hold.
What you gonna do to me?

You used to be so warm and affectionate.
All the little things I used to hear my fairy say,
But now you're quick to get into your regret.
I'll take the fall and now you got to give it all away.

Hey, oh yeah, how long?
I guess I ought to walk away,
Hey, oh yeah, so long...
What you gonna do today?

I don't wanna have to, but I will,
If that's what I'm supposed to do.
We don't wanna set up for the kill,
But that's what I'm about to do.

Hey, what would you say if I stayed?
Stayed for a while, if I may.
Say it again and I'll come around,
But not for the last time...
Hey, what would you say if I changed?
I'll change everything but my name.
Play it again and I'll come around,
But not for the last time.

You used to be so warm and affectionate;
I used to know just what I wanted and just where to go.
And now you're quick to get into your regret;
And now I walk alone and talk about it when I know...

Hey, oh yeah, how long?
I guess you gotta walk away;
Hey, oh yeah, so long...
What you gonna do today?

Hey, oh yeah, how long?
I guess you gotta get away;
Hey, oh yeah, so long...
What you gonna do today?

----------------- Acordes -----------------
A#6/C = X 3 3 3 3 3
A#m = X 1 3 3 2 1
C# = X 4 6 6 6 4
Cm = X 3 5 5 4 3
D# = X 6 5 3 4 3
Fm = 1 3 3 1 1 1
G#m = 4 6 6 4 4 4
