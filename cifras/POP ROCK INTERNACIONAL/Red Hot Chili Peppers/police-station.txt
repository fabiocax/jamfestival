Red Hot Chili Peppers - Police Station

Intro:    G  C    E      A  G7M  D   D7     A  A9  C7M 
E|-3-----0---0----0------2---2---2---0------0------0--|
B|-0/3---1---0----1------0---3---1---1------0------0--|
G|-0-----0---0----2------0---2---2---1------2------0--|
D|-0-----2---2----2------0---0---0---1------2------2--|
A|-2-----3---2----0------2---x---x---0------0------3--|
E|-3-----x---0----x------3---x---x---x------0------x--|


  G              C                     Em
I saw you at the police station and it breaks my heart to say
     G                 C                Em
Your eyes had wandered off to something distant, cold and grey
Am
I guess you didn't see it coming

Someone's gotten used to slumming
G
Dreaming of the golden years
G7M
I see you had to change careers

Am
Far away, but we both know it's somewhere

G                C                 Em
I saw you on the back page of some pre press yesterday
G                     C                Em
The drip wood in your eyes had nothing short of love for pain
Am
I know you from another picture

Of someone with the most convictions
G
We used to read the funny papers
G7M
Fooled around and pulled some capers
Am
Not today, send a message to her
D                          D7
A message that I'm coming, coming to pursue her
F                 C                A9  A
Tell your country I, rest my face on your bed
F                            C                        A9  A
I've got you ten times over, I'll chase you down 'til you're dead
G              C                 Em
I saw you on a TV station and it made me wanna pray
G                 C                 Em
An empty shell of loveliness is now dusted with decay
Am
What happened to the funny papers
Am
Smiling was your money maker
G
Someone oughta situate her
G7M
Find a way to educate her
Am
All the way, time to come and find you
D                            D7
You can't hide from me girl, so never mind what I do
F                 C                 A9 A
Tell your country I, rest my face on your bed
F                              C                 A9 A
I bet my sovereign country and I, left it all for your head

( D  Am  Em  Am  C  D )

G                C                    Em           Em
I saw you in the church and there was no time to exchange
G                C              Em           Em
You were getting married and it felt so very strange
Am
I guess I didn't see it coming
Am
Now I guess it's me who's bumming
G
Dreaming of the golden years
G7M
You and I were mixing tears
Am
Not today, not for me but someone
D                          D7
I never could get used to, so now I will refuse to
F                 C                 A9 A
Tell your country I, rest my face on your bed
F                              C                 A9 A
I bet my sovereign country and I, left it all for your head
F                              C                        A9  A
I got my best foot forward and I'll chase you down 'til you're dead

( Am  Em  Am  Em  Am  Em  Am  Em  Am )
E|--------------------------------------------------------------------|
B|--------------------------------------------------------------------|
G|--------------------------------------------------------------------|
D|-7b9b7-------9b10=--9b10b9----9h12p9-9-5-7-7-5-7b8b7----------------|
A|-------7b9b7------------------------------------------9-10-12-9-----|
E|--------------------------------------------------------------------|


----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C7M = X 3 2 0 0 X
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7M = 3 X 4 4 3 X
