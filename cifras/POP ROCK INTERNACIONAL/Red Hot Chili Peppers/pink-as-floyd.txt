Red Hot Chili Peppers - Pink As Floyd

Intro: Ab Bb Gm Cm (2x)

Verse:
Ab           Bb
Say what you need to say
        Gm             Cm
Make it clear, make it great
           Ab
Cause its not too late
            Bb
And theres no mistake
         Gm
When you shine
                Cm      B
Shake it for me anyway---
       Ab
Now we crash the gate
     Bb
To investigate
     C
Your fate



Chorus:
F
Pleasure to meet you
    Cm7
Got so much more
   Eb
To offer the world
    Bb            A
See whats in store
Ab   Bb  Cm
Stay all day
F
Someone to love
          Cm7
That’s my boy chip chop
Eb
Open the doors
       Bb
And we never close shop
Ab
Stay all day


Verse:
Ab            Bb
Take what you need to take
      Gm
To forsake
               Cm
Make it for me anyway
        Ab
When we fill the void
        Bb
That is Pink As Floyd
       Gm
To destroy
                Cm          B
Records that we file away---
         Ab
And Ill clean the slate
     Bb
As I calculate
     C
Your fate


Chorus

Bridge:

   E                          Ebm (B13)
  Ab (B13)
E|-----0---------0---------|---------------------------|----------0---------|
B|----0--0------0--0-------|-----0---------0-----------|-----------4--------|
G|--1------1--1------1-----|----3--3------3--3---------|----5---5---5-------|
D|-2---------2----------2--|--4------4--4------4-------|---6-6-6-----6------|
A|-------------------------|-6---------6---------6-----|--6---6-------6-----|
E|-------------------------|---------------------------|-4------------------|

Repeat that 3 times.


Verse:
        Ab           Bb
And you shine as you redefine
     Gm                 Cm
Your time, just another time of day
       Ab
Do you feel the spark
         Bb
When you play the part
     Gm
Superstar
               Cm      B
Fake it for me anyway---
        Ab
Time to tell them all
        Bb
Without hem or haw
   Gm
No applause
                 Cm
Tell them how to generate
       Ab
How to raise the bar
       Bb
How to keep it hard
      Gm                    Cm      B
To be alive, make it for me anyway---
       Ab
If you care at all
         Bb
You will bare it all
         Gm                     Cm
You will grind, shake it for me anyway
        Ab
Born to play the part
      Bb
Dying for your art
         Gm
'cause its time
               Cm      B
Take it for me anyway---
         Ab
You will clear the air
       Bb
With a single stare
     C
Your fate

----------------- Acordes -----------------
A = X 0 2 2 2 0
Ab = 4 3 1 1 1 4
B = X 2 4 4 4 2
B13 = X 2 4 1 4 X
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
Cm7 = X 3 5 3 4 3
E = 0 2 2 1 0 0
Eb = X 6 5 3 4 3
Ebm = X X 1 3 4 2
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
