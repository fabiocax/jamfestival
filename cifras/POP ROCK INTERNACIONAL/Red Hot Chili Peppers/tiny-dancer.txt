Red Hot Chili Peppers - Tiny Dancer




Intro:F,C,Dm


 F          C             Dm  Em
  Hold me closer tiny dancer
 F           C                 Dm     Em
  Count the headlights on the highway
 F        C                 Dm    Em
  Lay me down in sheets of linen
 F           C          Dm
  You had a busy day today


 F          C             Dm  Em
  Hold me closer tiny dancer
 F           C                 Dm     Em
  Count the headlights on the highway
 F        C                 Dm    Em
  Lay me down in sheets of linen

 F           C          Dm
  You had a busy day today

----------------- Acordes -----------------
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
