Red Hot Chili Peppers - Can't Stop

E|---------------------------------------------------
B|---------------------------------------------------
G|---------------------------------------------------   (4x)
D|---------------------------------------------------
A|5--7--5--7--5--7--5--7--5--7-----------------------
E|---------------------------------------------------

E|---------------------------------------------------
B|---------------------------------------------------
G|---------------------------------------------------
D|---------------------------------------------------
A|5--7--5--7--5--7--5-----5--7-----------------------
E|---------------------7-----------------------------

E|---------------------------------------------------
B|---------------------------------------------------
G|---------------------------------------------------   (3x)
D|---------------------------------------------------
A|5--7--5--7--5--7--5--7--5--7-----------------------
E|---------------------------------------------------



Introdução: Em  D  Bm  C

Riff Principal:
  Em                        D
E|---------------------------------------------------
B|---------------------------------------------------
G|----7-9-------7-x-9---------7-9-------7-x-9--------
D|--------------------------0-----0-0-0---------0-0--
A|7-7-----7-7-7-------7-7-7-------------------0------
E|---------------------------------------------------

  Bm                      C
E|---------------------------------------------------
B|---------------------------------------------------
G|--7-9-------7-x-9---------7--9-------7-x-9---------
D|---------------------------------------------------
A|---------------------------------------------------
E|7-----7-7-7-------7-7-7-8------8-8-8-------8-8-8---

(Variação)
  Em                        D
E|---------------------------------------------------
B|---------------------------------------------------
G|----7-9-------7-x-9---------7-9-------7-x-9--------
D|--------------------------0-----0-0-0-------0-0-0--
A|7-------7-7-7-------7-7-7--------------------------
E|---------------------------------------------------

  Bm                      C
E|---------------------------------------------------
B|---------------------------------------------------
G|--7-9-------7-x-9---------7--9-------7-x-9---------
D|---------------------------------------------------
A|---------------------------------------------------
E|7-----7-7-7-------7-7-7-8------8-8-8-------8-8-8---


Em
   Can't stop addicted to the shin dig
D
  Chop top he says I'm gonna win big
Bm
   Choose not a life of imitation
 C
Distant cousin to the reservation

Em
   Defunkt the pistol that you pay for
D
  This punk the feeling that you stay for
Bm
   In time I want to be your best friend
C
Eastside love is living on the westend

Em
   Knock out but boy you better come to
D
  Don't die you know the truth is some do
Bm
   Go write your message on the pavement
 C
Burnin' so bright I wonder what the wave meant

Em
   White heat is screaming in the jungle
D
  Complete the motion if you stumble
Bm
   Go ask the dust for any answers
 C7M
Come back strong with 50 belly dancers
(Passagem para o Refrão)

  C7M
E|7------7------7------7------7------------
B|--8------8------8------8------8----------
G|----9------9------9------9------9--------
D|----10-----10-----10-----10-----10-------
A|-----------------------------------------
E|-----------------------------------------


Refrão:
     G
The world I love
  D5(9)
The tears I drop
    Bm
To be    part of
     C7M
The wave can't stop
G        D5(9)      Bm       C7M
Ever wonder if it's all for you

     G
The world I love
  D5(9)
The trains I hopped
    Bm
To be part of
     C7M
The wave can't stop
 G             D5(9)        Bm   C7M
Come and tell me when it's time to

Em
   Sweetheart is bleeding in the snowcone
D
  So smart she's leading me to ozone
Bm
   Music the great communicator
C
Use two sticks to make it in the nature

Em
   I'll get you into penetration
D
  The gender of a generation
Bm
   The birth of every other nation
 C
Worth your weight the gold of meditation

Em
   This chapter's going to be a close one
D
  Smoke rings I know your going to blow one
Bm
   All on a spaceship persevering
C
Use my hands for everything but steering

Em
   Can't stop the spirits when they need you
D
  Mop tops are happy when they feed you
Bm
   J. Butterfly is in the treetop
 C7M
Birds that blow the meaning into bebop

Refrão:
     G
The world I love
  D5(9)
The tears I drop
    Bm
To be    part of
     C7M
The wave can't stop
G        D5(9)      Bm       C7M
Ever wonder if it's all for you

     G
The world I love
  D5(9)
The trains I hopped
    Bm
To be part of
     C7M
The wave can't stop
 G             D5(9)        Bm   C7M
Come and tell me when it's time to
Interlúdio:

     Em                      D5(9)
E|---7-----7-----7-----7-----5-----5-----5-----7-----
B|---8-----8-----8-----8-----5-----5-----5-----5-----
G|x--9--x--9--x--9--x--9--x--7--x--7--x--7--x--x--x--
D|x--9--x--9--x--9--x--9--x--7--x--7--x--7--x--7--x--
A|x--7--x--7--x--7--x--7--x--5--x--5--x--5--x--5--x--
E|---------------------------------------------------


  Bm                       C
E|---------------------------------------------------
B|7-----7-----10-----8-----8------8------8------10---
G|7-----7-----7------7-----9------9------9------9----
D|9--x--9--x--9---x--9--x--10--x--10--x--10--x--10---
A|9--x--9--x--x---x--x--x--10--x--10--x--10--x--x----
E|7--x--7--x--7---x--7--x--8---x--8---x--8---x--8----


     Em                      D5(9)
E|---7-----7-----7-----8-----5-----5-----5-----7-----
B|---8-----8-----8-----x-----5-----5-----5-----5-----
G|x--9--x--9--x--9--x--9--x--7--x--7--x--7--x--x--x--
D|x--9--x--9--x--9--x--9--x--7--x--7--x--7--x--7--x--
A|x--7--x--7--x--7--x--7--x--5--x--5--x--5--x--5--x--
E|---------------------------------------------------


  Bm                      C
E|---------------------------------------------------
B|7-----7-----7-----7-----8------8------8------10----
G|7-----7-----7-----7-----9------9------9------9-----
D|9--x--9--x--9--x--9--x--10--x--10--x--10--x--10----
A|9--x--9--x--9--x--9--x--10--x--10--x--10--x--x-----
E|7--x--7--x--7--x--7--x--8---x--8---x--8---x--8-----


     Em                      D5(9)
E|---7-----7-----7-----7-----5-----5-----5-----7-----
B|---8-----8-----8-----x-----5-----5-----5-----5-----
G|x--9--x--9--x--9--x--9--x--7--x--7--x--7--x--x--x--
D|x--9--x--9--x--9--x--9--x--7--x--7--x--7--x--7--x--
A|x--7--x--7--x--7--x--7--x--5--x--5--x--5--x--5--x--
E|---------------------------------------------------


  Bm                      C
E|---------------------------------------------------
B|7-----7-----7-----7-----8------8------8------10----
G|7-----7-----7-----7-----9------9------9------9-----
D|9--x--9--x--9--x--9--x--10--x--10--x--10--x--10----
A|9--x--9--x--9--x--9--x--10--x--10--x--10--x--x-----
E|7--x--7--x--7--x--7--x--8---x--8---x--8---x--8-----


     Em                      D5(9)
E|---7-----7-----7-----8-----5-----5-----5-----7-----
B|---8-----8-----8-----x-----5-----5-----5-----5-----
G|x--9--x--9--x--9--x--9--x--7--x--7--x--7--x--x--x--
D|x--9--x--9--x--9--x--9--x--7--x--7--x--7--x--7--x--
A|x--7--x--7--x--7--x--7--x--5--x--5--x--5--x--5--x--
E|---------------------------------------------------


  Bm                         C
E|---------------------------------------------------
B|7-----7-----7-----7--------------------------------
G|7-----7-----7-----7--------------------------------
D|9--x--9--x--9--x--9--x--x--10--10-10-10-10---------
A|9--x--9--x--9--x--9--x--x--10--10-10-10-10---------
E|7--x--7--x--7--x--7--x--x--8---8--8--8--8----------


E|---------------------------------------------------
B|---------------------------------------------------
G|---------------------------------------------------
D|10-10-10-10-10-10-10-------------------------------
A|10-10-10-10-10-10-10-------------------------------
E|8--8--8--8--8--8--8--------------------------------



Em                   D
   Wait a minute I'm   passing out
              Bm            C
  Win or lose    just like you
Em
   Far more shocking
         D               Bm
  Than anything I ever knew
             C
  How about you

Em
   Ten more reasons
         D             Bm            C
  Why I need somebody new just like you
Em                          D               Bm
   Far more shocking than anything I ever knew
            C
  Right on cue

Solo: Em  D  Bm  C
E|---------------------------------------------------
B|15b17----15b17--15----15--12----12-----------------
G|------------------------------------12-------------
D|---------------------------------------------------
A|---------------------------------------------------
E|---------------------------------------------------

E|---------------------------------------------------
B|15b17~---17b19r17--15----15b17r15--12----15--------
G|----------------------------------------------14-12
D|---------------------------------------------------
A|---------------------------------------------------
E|---------------------------------------------------

E|---------------------------------------------------
B|---------------------------------------------------
G|\11\9\---------------------------------------------
D|---------------------------------------------------
A|---------------------------------------------------
E|---------------------------------------------------


Em
   Can't stop addicted to the shin dig
D
  Chop top he says I'm gonna win big
Bm
   Choose not a life of imitation
 C
Distant cousin to the reservation

Em
   Defunkt the pistol that you pay for
D
  This punk the feeling that you stay for
Bm
   In time I want to be your best friend
C
Eastside love is living on the westend

Em
   Knock out but boy you better come to
D
  Don't die you know the truth is some do
Bm
   Go write your message on the pavement
 C
Burnin' so bright I wonder what the wave meant

Em
   Kick start the golden generator
D
  Sweet talk but don't intimidate her
Bm
   Can't stop the gods from engineering
 C
Feel no need for any interfering

Em
   Your image in the dictionary
D
  This life is more than ordinary
Bm
   Can I get two maybe even three of these
 C
Come from space To teach you of the pliedes

Can't stop the spirits when they need you
This life is more than just a read through

----------------- Acordes -----------------
Bm = 7 9 9 7 7 7
C = 8 10 10 9 8 8
C7M = X X 10 9 8 7
D = X 5 7 7 7 5
D5(9) = X 5 7 7 5 5
Em = X 7 9 9 8 7
G = X 10 9 7 8 7
