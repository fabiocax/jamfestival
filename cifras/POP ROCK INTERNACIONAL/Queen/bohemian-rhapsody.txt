Queen - Bohemian Rhapsody

Capo Casa 3

[Primeira Parte]

G6
  Is this the real life?
A7
  Is this just fantasy?
 D7
Caught in a landslide
 G
No escape from reality

Em
Open your eyes
     G7                   C
Look up to the skies and see
Am
I'm just a poor boy
D7
I need no sympathy
            G#    G     F#   G
Because I'm easy come, easy go

 G#     G     F#     G
Little high, little low
C           G
Anyway the wind blows
 A#°            D7
Doesn't really matter to me
    G6
To me

[Segunda Parte]

[Dedilhado]

   G6
E|---------0----------------0-------------------------|
B|-------0-----3----------0-----3---------------------|
G|-----0----------------0-----------------------------|
D|---0-------0---0----0-------0---0-------------------|
A|----------------------------------------------------|
E|-3----------------3---------------------------------|

                     Em
E|---------0-----------------2---0--------------------|
B|-------0-----3-----------0--------------------------|
G|-----0-----------------0----------------------------|
D|---0-------0---0-----2-------2---2------------------|
A|----------------------------------------------------|
E|-3-----------------0--------------------------------|

   Am
E|---------7---5--------------------------------------|
B|-------5--------------------------------------------|
G|-----5----------------------------------------------|
D|---7-------7---7------------------------------------|
A|-0--------------------------------------------------|
E|----------------------------------------------------|

 G6         Em
Mama, just killed a man
       Am
Put a gun against his head
                             D
Pulled my trigger, now he's dead
 G6             Em
Mama, life had just begun
     Am                  Am/G#  Am/G  Am/F#  Am/F  Am/E
But now I've gone and thrown it all        away

 C   G/B      Am
Mama uh uh uh uh
        Dm           Dm/C#  Dm/C  Dm/B
Didn't mean to make you   cry
   G6             G7               C
If I'm not back again this time tomorrow
      G/B       Am
Carry on, carry on
       Fm             C
As if nothing really matters

[Riff 1]

E|------8---8---8---8---------------------------------|
B|-1/10---8---7---6-----------------------------------|
G|----------------------------------------------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

[Segunda Parte]

( G6 )

 G6           Em
Too late, my time has come
        Am
Sends shivers down my spine
                       D
Body's aching all the time
 G6
Goodbye everybody
      Em
I've got to go
       Am              Am/G#
Gotta leave you all behind
     Am/G  Am/F#         Am/F  Am/E
And face        the truth

 C   G/B      Am
Mama uh uh uh uh
(Anyway the wind blows)
Dm          Dm/C# Dm/C  Dm/B
I don't wanna    die
   G6                 G7
I sometimes wish I'd never been born at all

[Solo] C  G/B  Am  Dm
       Dm/C#  Dm/C  Dm/B  G
       C  G/B  Am  Dm
       Dm/C#  Dm/C  Dm/B  G

[Solo Sem Capo]

E|----------------------------------------------------|
B|-----------------11~---8--11b13~--------------------|
G|------10b12r10p8------------------------------------|
D|-8~-------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

E|-------------------------------15-16-18-b20~r-------|
B|-13--15-16-13--15-16-b18----18----------------------|
G|----------------------------------------------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

E|-16-15-16-15h16p15----15----------------------------|
B|-------------------18----18-16-15-16-15h16p15----15-|
G|----------------------------------------------17----|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

E|----------------------------------------------------|
B|----------------------------------------------------|
G|-17-15-15~------------------------------------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

E|-------------------15-16-18-------------------------|
B|----------15-16-18----------------------------------|
G|-15~---17-------------------------------------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

E|-----~----------15-16-18-b20~-----------------------|
B|-------15-16-18-------------------------------------|
G|-15-17----------------------------------------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

E|----------------------------------------------------|
B|-15-16-13--15-16-13--15-16-b18~---------------------|
G|----------------------------------------------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

E|------------13--------------------------------------|
B|-----------------------14--14-14-14\----------------|
G|-10-12-13~-------13~-----------------------6h7p6\2--|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

[Terceira Parte]

B/F#  F#      F#°     F#  B/F#   F#      F#°
I    see  a  little  silhouetto  of  a  man
F#     B/F#    F# B/F#   F#        F#°      F# B/F#  F#
  Scarmouch  Scarmouch  will  you do  the  fandango?
Bb                      F           A            C#7         F#
  Thunder  bolts  and  lightening  very  very  frightening  me

Galileo Galileo Galileo Galileo Galileofigaro mangnifico oh oh oh

[Quarta Parte]

G#    G        F#    G    G#  G     F#     G
I'm  just  a  poor  boy  no__body  loves  me
F       C      G    C     F     C    G   C
  He's just a poor boy, from a poor family
F                C              D7       G
  Spare him his life from this monstrosity

[Riff 2]

E|----------------------------------------------------|
B|---1---1---1---1------------------------------------|
G|-2---0----------------------------------------------|
D|---------4---3--------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

G#      G    F#    G  G#       G      F#
  Easy come  easy go will you let me go?

G     C  G  C                         G
  Bismillah  no, we will not let you go (let  him  go)
G     C  G
  Bismillah we will not let you go (let him go )
G     C  G
  Bismillah we will not let you go (let him go )

Will not let you go let me go will not let you go

[Quinta Parte]

Let  me  go oh  oh  oh  oh oh
G#m    F# B  A# A  G  C
   No no no no no no no
                            F   C  G
Oh mamamia mamamia mamamia let me go
  C    F        B7         Em       G
Beelzebu has a devil put aside for me for me for me

( C  D7 )

[Sexta Parte]

G7                                    C        G7  A#
  So you think you can stone me and spit in my eye?
G7                                  C           F
  So you think you can love me and leave me to die?
Dm     G
  Oh, baby!
Dm                      G
  Can't do this to me, baby!
Dm               G
  Just gotta get out
Dm                G           C
  Just gotta get right outta here!

( C  D7  E7  G#m  F  G )

C  G/B  Am  E7  Am    E7   Am7  E7  Am
            Oh, yeah! Oh, yeah!

( B  Em  F  C )

[Parte Final]

Am                Em
  Nothing really matters
Am          Em
Anyone can see
Am                Fm
  Nothing really matters
 F                         C
Nothing really matters to me

( F#°  G  Gm/A#  A  A7  A  D )

G    C       C°    A7  C
Any way the wind blows

----------------- Acordes -----------------
Capotraste na 3ª casa
A*  = X 0 2 2 2 0 - (*C na forma de A)
A#*  = X 1 3 3 3 1 - (*C# na forma de A#)
A#°*  = X 1 2 0 2 0 - (*C#° na forma de A#°)
A7*  = X 0 2 0 2 0 - (*C7 na forma de A7)
Am*  = X 0 2 2 1 0 - (*Cm na forma de Am)
Am/E*  = 0 X 2 2 1 0 - (*Cm/G na forma de Am/E)
Am/F*  = 1 X 2 2 1 X - (*Cm/G# na forma de Am/F)
Am/F#*  = 2 X 2 2 1 0 - (*Cm/A na forma de Am/F#)
Am/G*  = 3 X 2 2 1 X - (*Cm/A# na forma de Am/G)
Am/G#*  = X X 6 5 5 5 - (*Cm/B na forma de Am/G#)
Am7*  = X 0 2 0 1 0 - (*Cm7 na forma de Am7)
B*  = X 2 4 4 4 2 - (*D na forma de B)
B/F#*  = X X 4 4 4 2 - (*D/A na forma de B/F#)
B7*  = X 2 1 2 0 2 - (*D7 na forma de B7)
Bb*  = X 1 3 3 3 1 - (*Db na forma de Bb)
C*  = X 3 2 0 1 0 - (*D# na forma de C)
C#7*  = X 4 3 4 2 X - (*E7 na forma de C#7)
C°*  = X 3 4 2 4 2 - (*D#° na forma de C°)
D*  = X X 0 2 3 2 - (*F na forma de D)
D7*  = X X 0 2 1 2 - (*F7 na forma de D7)
Dm*  = X X 0 2 3 1 - (*Fm na forma de Dm)
Dm/B*  = X 2 X 2 3 1 - (*Fm/D na forma de Dm/B)
Dm/C*  = X 3 X 2 3 1 - (*Fm/D# na forma de Dm/C)
Dm/C#*  = X 4 3 2 3 X - (*Fm/E na forma de Dm/C#)
E7*  = 0 2 2 1 3 0 - (*G7 na forma de E7)
Em*  = 0 2 2 0 0 0 - (*Gm na forma de Em)
F*  = 1 3 3 2 1 1 - (*G# na forma de F)
F#*  = 2 4 4 3 2 2 - (*A na forma de F#)
F#°*  = 2 X 1 2 1 X - (*A° na forma de F#°)
Fm*  = 1 3 3 1 1 1 - (*G#m na forma de Fm)
G*  = 3 2 0 0 0 3 - (*A# na forma de G)
G#*  = 4 3 1 1 1 4 - (*B na forma de G#)
G#m*  = 4 6 6 4 4 4 - (*Bm na forma de G#m)
G/B*  = X 2 0 0 3 3 - (*A#/D na forma de G/B)
G6*  = 3 X 2 4 3 X - (*A#6 na forma de G6)
G7*  = 3 5 3 4 3 3 - (*A#7 na forma de G7)
Gm/A#*  = 6 5 5 3 X X - (*A#m/C# na forma de Gm/A#)
