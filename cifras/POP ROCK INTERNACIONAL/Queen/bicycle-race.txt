Queen - Bicycle Race

Eb      D5+     Bbm/C#
Bicycle bicycle bicycle
  Ab      Bbm4    D       B       Ab
I want to ride my bicycle bicycle bicycle
          Bbm4    Ab
I want to ride my bicycle
          Bbm4    Ab
I want to ride my bike
          Bbm4    Ab
I want to ride my bicycle
          Bbm4    Ab
I want to ride it where I like
        Bbm7
You say black I say white

You say bark I say bite

You say shark I say hey man
F#7               Gm7/5-
Jaws was never my scene
                 Bbm7
And I don't like Star Wars


You say Rolls I say Royce

You say God give me a choice

You say Lord I say Christ
  F#7
I don't believe in Peter Pan
Gm7/5-
Frankenstein or Superman
F           G  Am
All I wanna do is
Eb      D5+     Bbm/C#
Bicycle bicycle bicycle
  Ab      Bbm4    D       B       Ab
I want to ride my bicycle bicycle bicycle
          Bbm4    Ab
I want to ride my bicycle
          Bbm4    Ab
I want to ride my bike
          Bbm4    Ab
I want to ride my bicycle
          Bbm4
I want to ride my
Gm7               C7
Bicycle races are coming your way
       F                     Bb   Bb4 Bb
So forget all your duties oh yeah!
Gm7
Fat botgirls
           C7
They'll be riding today
         F                         Bb
So look out for those beauties oh yeah
             A7      Dm
On your marks get set go

Solo:
C F Bb C Dm C
A7 Dm Dm C Bb C

G            G6           G7            G Am Bm
Bicycle race bicycle race bicycle race
F       Am      Ab
Bicycle bicycle bicycle
          Bbm4     D
I want to ride my bicycle
B       G7
Bicycle bicycle bicycle bicycle
         C
Bicycle race

Solo:
A7 D A7 D A7
D B Em F#7

Bbm7
You say coke I say caine

You say John I say Wayne

Hot dog I say cool it man
F#7                  Gm7/5-         Bbm7
I don't wanna be the President of America

You say smile I say cheese

Cartier I say please

Income tax I say Jesus
  F#7
I don't wanna be a candidate for
Gm7/5-
Vietnam or Watergate
    F           G  Am
Cos all I wanna do is
Eb      D5+     Bbm/C#
Bicycle bicycle bicycle
  Ab      Bbm4    D       B       Ab
I want to ride my bicycle bicycle bicycle
          Bbm4    Ab
I want to ride my bicycle
          Bbm4    Ab
I want to ride my bike
          Bbm4    Ab
I want to ride my bicycle
          Bbm4    Ab
I want to ride it where I like



----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Ab = 4 3 1 1 1 4
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
Bb = X 1 3 3 3 1
Bb4 = X 1 3 3 4 1
Bbm/C# = X 4 X 3 6 6
Bbm4 = X 1 1 3 2 1
Bbm7 = X 1 3 1 2 1
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
D = X X 0 2 3 2
D5+ = X 5 4 3 3 X
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F#7 = 2 4 2 3 2 2
G = 3 2 0 0 0 3
G6 = 3 X 2 4 3 X
G7 = 3 5 3 4 3 3
Gm7 = 3 X 3 3 3 X
Gm7/5- = 3 X 3 3 2 X
