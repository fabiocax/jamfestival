Queen - You Are My Best Friend

Dm/C              C          Dm/C                 C
Ooo, you make me live.  Whatever this world can give to me

     Dm/C              C     Dm/C              C
It's you, you're all I see.  Ooo, you make me live now honey

Dm/C              C    G/B    Am
Ooo, you make me live


           D                   F    G
You're the best friend that I ever had

     C         G/B        Am
I've been with you such a long time

          D              F           G
You're my sunshine and I want you to know

        E/G#          Am       G          F    Fm6
That my feelings are true.  I really love you


          C
You're my best friend


Dm/C                    C
Ooo, you make me live

C    C/G# Am C7/Bb                F               Fm6
 I've been wandering round but I still come back to you

   G       E            Am          D7
In rain or shine you've stood by me girl

    G6             G                C
I'm happy, happy at home.  You're my best friend


Ooo, you make me live
Whenever this world is cruel to me
I got you to help me forgive
Ooo, you make me live now honey
Ooo, you make me live

You're the first one
When things turn out bad
You know I'll never be lonely
You're my only one
And I love
The things that you do
You're my best friend

Ooo, you make me live

I'm happy, happy at home
You're my best friend
You're my best friend
Ooo, you make me live
You, you're my best friend

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C/G# = 4 X 5 5 5 X
C7/Bb = X 1 2 0 1 X
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Dm/C = X 3 X 2 3 1
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
F = 1 3 3 2 1 1
Fm6 = 1 X 0 1 1 X
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G6 = 3 X 2 4 3 X
