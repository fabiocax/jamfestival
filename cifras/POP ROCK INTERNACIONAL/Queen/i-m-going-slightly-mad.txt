Queen - I'm Going Slightly Mad

Intro: : Dm : Dm/F : =>6x

Verso 1:
Dm
when the outside temperature rises
Em7º5                     Eº
and the meaning is oh so clear
Dm
one thousand and one yellow daffodils
Em7º5                             Eº
begin to dance in front of you - oh dear
F#7
are they trying to tell you something
G
you're missing that one final screw
F#7
you're simply not in the pink my dear
G
to be honest you haven't got a clue

2x

  A9       A7       
e-3-0------3-0-----|
B-----2-0------2---|
G----------------2-|

Refrão:
D          Dm      Em7º5
I'm going slightly mad
D          Dm      Em7º5
I'm going slightly mad
           F
it finally happened - happened
           Em
it finally happened - ooh oh
           F
it finally happened
              Abº  Gm6
i'm slightly mad
Dm
oh dear

Ponte:| Dm | Dm/F | =>2x

Verso 2:
Dm
i'm one card short of a full deck
Em7º5              Eº
i'm not quite the shilling
Dm
one wave short of a shipwreck
Em7º5                 Eº
i'm not my usual top billing
F#7
i'm coming down with a fever
G
i'm really out to sea
F#7
this kettle is boiling over
G
i think i'm a banana tree
oh dear

2x
  A9       A7       
e-3-0------3-0-----|
B-----2-0------2---|
G----------------2-|

Refrão:
D          Dm      Em7º5
I'm going slightly mad
D          Dm      Em7º5
I'm going slightly mad
           F
it finally happened - happened
           Em
it finally happened - ooh oh
           F
it finally happened
              Abº  Gm6
i'm slightly mad
Dm
oh dear

Solo:[2:30]
Gm7 Dm D
Dm7 Em7 D Dm Em7 F
Em F
Abº Gm7

------------------------------------3/2/3/5-5/4/5/6~~~/10-------------------
--------5/7-7-7-7b----------------------------------------------------------
-----------------------7-2/1/2---------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------

-----------------------------------------------------------------------------
---------------------------------6b-5----------------------------------------
----------------------------------------4------------------------------------
------3-3-3-3-----------------------------5----2-2---------------------------
----------------3------------------------------------------------------------
-------------------5-1-1-----------------------------------------------------

---//13-15-17-17b---13------17b-16-13-10--------13---------------------------
----------------------------13--------------------11---------13--------------
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------

F#7
i'm knitting with only one needle
G
unravelling fast it's true
F#7
i'm driving only three wheels these days
G
but my dear how about you

  A9       A7        B9       B7
e-3-0------3-0-----|-5-2------5-2-----|
B-----2-0------2---|-----4-2------4---|
G----------------2-|----------------4-|

E          Em      F#m7º5
I'm going slightly mad
E          Em      F#m7º5
I'm going slightly mad
           G
it finally happened
           F#m
it finally happened - ooh yes
           G
it finally happened
              Bbº Am
i'm slightly mad
                   A/Bb  Am  Em
just very slightly mad
Em                    Em/G   Em/B...
and there you have it

Notas:
Em7º5: xx2333
Eº: 0x2323
F#7: xx4320
A9: x05600
A7: x05650
Dmº5: xx0131
Abº: 453433
?: 355353
Bbº: x12020
A/Bb: x12220

----------------- Acordes -----------------
A/Bb = X X 8 9 10 9
A7 = X 0 2 0 2 0
A9 = X 0 2 2 0 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
B9 = X 2 4 4 2 2
D = X X 0 2 3 2
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Em = 0 2 2 0 0 0
Em/B = X 2 2 0 0 0
Em/G = 3 X 2 4 5 X
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
F#7 = 2 4 2 3 2 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
Gm7 = 3 X 3 3 3 X
