Queen - Mad The Swine

Afinação Meio Tom Abaixo

D                  D7M
Been here before a long time ago
    Am               D  Em
But this time I wear no sandals

Passagem: C Em D Em

Em        C Am    G      A
Ages past I gave all you people
          D4   D
Food and water

D                  D7M            Em
Three feet tall so very small I'm no trouble
        Em      Em/D      C           Am
I bring thunder lightning sun and the rain
    G       A             D4   D
For all the people in the land

  D              D7M
A message of love

                    Am
I bring you from up above
G   G/F# Em   A   Bm
All good children gather around
               D     D/C D/B  G   A     G A
Come join your hands and sing a - long

             D
They call me mad the swine
            Bm
I guess I'm mad the swine
             Em       A4       A
I've come to save you save you
G/A            Am
Mad the swine, Mad the swine
   G   G/F# Em A   Bm
So all you  people gather around
              D     D/C D/B    G   A     G A
Hold out your hands and praise the lord

     D                  D7M           Em
I'll walk upon the water    just as before
              Em           Em/D         C             Am
I'll help the meek and the mild and believers and the blind
    G       A                    D4   D
And all the creatures great and small

                       D7M             Am
Let me take you to the river without a fall
       G        G/F#       Em
Oh and then one day you'll realise
       G       G/F#   Em
You're all the same within his eyes
       G        G/F#    Em               A4   A
That's all I've come to say just like before

             D
They call me mad the swine
            Bm
I guess I'm mad the swine
             Em       A4       A
I've come to save you save you
G/A            Am
Mad the swine, Mad the swine
   G   G/F# Em A   Bm
So all you  people gather around
              D     D/C D/B    G   A     G A
Hold out your hands and praise the lord

Base do Solo: D Bm Em A G/A Am


   G   G/F# Em A   Bm
So all you  people gather around
              D     D/C D/B    G   A     G A
Hold out your hands and praise the lord

Don't ever fail me

D
Mad the swine
            Bm
I guess I'm mad the swine
             Em       A4       A
I've come to save you save you
G/A            Am
Mad the swine, Mad the swine
   G   G/F# Em A   Bm
So all you  people gather around
              D     D/C D/B    D/A D
Hold out your hands and praise the lord

Final: D D/C D/B D/A D

----------------- Acordes -----------------
A*  = X 0 2 2 2 0 - (*G# na forma de A)
A4*  = X 0 2 2 3 0 - (*G#4 na forma de A4)
Am*  = X 0 2 2 1 0 - (*G#m na forma de Am)
Bm*  = X 2 4 4 3 2 - (*A#m na forma de Bm)
C*  = X 3 2 0 1 0 - (*B na forma de C)
D*  = X X 0 2 3 2 - (*C# na forma de D)
D/A*  = X 0 X 2 3 2 - (*C#/G# na forma de D/A)
D/B*  = X 2 0 2 3 2 - (*C#/A# na forma de D/B)
D/C*  = X 3 X 2 3 2 - (*C#/B na forma de D/C)
D4*  = X X 0 2 3 3 - (*C#4 na forma de D4)
D7M*  = X X 0 2 2 2 - (*C#7M na forma de D7M)
Em*  = 0 2 2 0 0 0 - (*D#m na forma de Em)
Em/D*  = X X 0 4 5 3 - (*D#m/C# na forma de Em/D)
G*  = 3 2 0 0 0 3 - (*F# na forma de G)
G/A*  = 5 X 5 4 3 X - (*F#/G# na forma de G/A)
G/F#*  = X X 4 4 3 3 - (*F#/F na forma de G/F#)
