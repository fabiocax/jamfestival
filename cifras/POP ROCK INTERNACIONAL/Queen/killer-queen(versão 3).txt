Queen - Killer Queen

          Cm
She keeps Moet and Chandon,
A#
 In her pretty cabinet.
Cm
 "Let them eat cake", she says,
A#               D#
 Just like Mary Antoinette.

  D#        A#
A built in remedy,
    D#7           G#
For Kruschev and Kennedy,
    G#m           D#
And any time, an invitation,
A#
 You can't decline.

Bridge 1:

G7          Cm
 Caviar and cigarettes,

A#7              D#
 Well versed in etiquette.
      D7         Gm
Extra-ordinarily nice.

F7
 She's a...

Chorus 1:

A#       Dm
 Killer Queen,
Gm          Dm
 Gunpowder, gelatine,
Gm               A7   Dm
 Dynamite with a laser beam.
G7                       C
 Guaranteed to blow your mind;
A#
 Anytime, ooh...

A7          Dm
 Recommended at the price,
   G7          Cm
In-satiable in appetite;
C  A#
     Wanna try?


Verse 2:

     Cm
To a-void complications,
    A#
She never kept the same address.
Cm
 In conversation,
     A#                D#
She spoke just like a baroness.

D#             A#
 Met a man from China,
     D#7             G#
Went down to Geisha Minah,
       G#m         D#
Then a-gain, inci-dentally,
          A#
If you're that way inclined.

Bridge 1:

A#           G7             Cm
 Perfume came naturally from Paris,
(Naturally),
   A#7                 D#
Be-cause she couldn't care less;
     D7             Gm
Fast-idious and pre-cise.

F7
 She's a...

Chorus 2:


A#       Dm
 Killer Queen,
Gm          Dm
 Gunpowder, gelatine,
Gm               A7   Dm
 Dynamite with a laser beam.
G7                       C
 Guaranteed to blow your mind;
A#
 Anytime, ooh...

Solo:

Bridge 3:

          G7            Cm
Drop of a hat, she's as willing as,
G7            Cm
 Playful as a pussycat.
     A#           D#
Then momentarily out of action,
A#            D#
 Temporarily out if gas;
   D7         Gm    F
To absolutely drive you...

A#   F   A#  F
 Wi-ld, wi-ld;
       F
(She's out to get you!)

F7
 She's a...

Chorus 3:

A#       Dm
 Killer Queen,
Gm          Dm
 Gunpowder, gelatine,
Gm               A7   Dm
 Dynamite with a laser beam.
G7                       C
 Guaranteed to blow your mind;
A#
 Anytime, ooh...

A7          Dm
 Recommended at the price,
   G7          Cm
In-satiable in appetite;
C  A#
     Wanna try?


Outro:

F7  A#  F7  A#,
F7  A#  F7  A#  D#

----------------- Acordes -----------------
A# = X 1 3 3 3 1
A#7 = X 1 3 1 3 1
A7 = X 0 2 0 2 0
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D# = X 6 5 3 4 3
D#7 = X 6 5 6 4 X
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
G# = 4 3 1 1 1 4
G#m = 4 6 6 4 4 4
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
