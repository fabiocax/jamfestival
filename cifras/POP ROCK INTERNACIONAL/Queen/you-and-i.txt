Queen - You And I

[Intro] D  G
        G  D  Em  D

       D                 A
Music is playing in the darkness

  D                       G  B°
And a lantern goes swinging by
      D                   A
Shadows flickering my heart's jittering
  G   D  Em  D
Just you and I
  G
Not tonight come tomorrow
  D
When everything's sunny and bright
(Sunny and bright)
  G                       E
No no no come tomorrow 'cause then
                   A
We'll be waiting in the moonlight

 G            D          F     G#°
We'll go walking in the moonlight
  C   G         G#°  A
Walking in the moonlight
  D                      A
Laughter ringing in the darkness
 D                          G  B°
People drinking for days gone by
     D
Time don't mean a thing
     A
When you're by my side
     G  D  Em  D
Please stay awhile
   F#                              Bm
You know I never could forsee the future years
   F#
You know I never could see
               Bm
Where life was leading me
      D                   A  F#
But will we be together forever?
        D
What will be my love?

                        A
Can't you see that I just don't know
      G
No not tonight not tomorrow
       D
Everything's gonna be alright (sunny and bright)
       G                   E
Wait and see if tomorrow we'll be
       A
As happy as we're feeling tonight
  G          D           F     G#°
We'll go walking in the moonlight (we'll be happy)
 C      G      G#°   A
Walking in the moonlight
  D                         A
I can hear the music in the darkness
 D                         G  B°
Floating softly to where we lie
  D
No more questions now
  A
Let's enjoy tonight

 G  D  Em  D
(Just you and I) just you and I

 G  D  Em   A    D
Just you and I

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
B° = X 2 3 1 3 1
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
G#° = 4 X 3 4 3 X
