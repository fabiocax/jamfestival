Queen - Crazy Little Thing Called Love

Intro: D D4 D D4

     D
This thing - called love
  G            C      G
I just - can't handle it.
     D
This thing - called love
  G          C        G
I must - get round to it
        D       Bb           C            D
I ain't ready.  Crazy little thing called love.

     D
This thing (this thing) called love (called love)
   G                        C         G
It cries (like a baby) in a cradle at night
   D
It swings (woo woo) - it jives (woo woo)
   G                      C     G
It shakes all over like a jelly fish.

        D
I kinda like it
Bb           C            D
Crazy little thing called love.

              G
There goes my baby
    C                   G
She knows how to rock 'n roll.
              Bb
She drives me crazy
            E7                         A
She give me hot and cold fever, then she leaves me in a cool, cool sweat.

E|--------------------
B|--------------------
G|-----------------2--
D|----------222222----
A|--543---------------
E|-----543------------

          D
I gotta be cool - relax,
    G         C     G
Get hip - get on my tracks
       D
Take a back seat - hitchhike,
    G                      C    G
And take a long ride on my motorbike.
          D
Until I'm ready.
Bb           C            D
Crazy little thing called love.

Solo

           D                                  -------------------------------------
I gotta be cool - relax,                      - Esta estrofe não tem instrumental,-
    G         C     G                         - apenas PALMAS, mas se quiser      -
Get hip - get on my tracks                    - tocar, as cifras seriam estas.    -
       D                                      -------------------------------------
Take a back seat - hitchhike,
    G                      C    G
And take a long ride on my motorbike.
          D
Until I'm ready (ready Freddy)
Bb           C            D
Crazy little thing called love.

          D
This thing - called love
  G            C      G
I just - can't handle it.
     D
This thing - called love
  G          C        G
I must - get round to it
        D       Bb           C            D
I ain't ready.  Crazy little thing called love.
Bb           C            D
Crazy little thing called love. (repeat and fade)

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D = X X 0 2 3 2
D4 = X X 0 2 3 3
E7 = 0 2 2 1 3 0
G = 3 2 0 0 0 3
