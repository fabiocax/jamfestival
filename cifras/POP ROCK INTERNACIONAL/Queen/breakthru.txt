Queen - Breakthru

Vocalização:
When love breaks up
When the dawn light wakes up
A new life is born
Somehow I have to make this final breakthrough
Now

Verso 1:
F
I wake up
Bb/F
Feel just fine
F
Your face
Bb/F
Fills my mind
F
I get religion quick
Bb/F
'Cos you're looking divine
Bb                                C/Bb
Honey you're touching something you're touching me

Bb                                Dm     Eb        C
I'm under your thumb under your spell can't you see

Pré-refrão:
F
If I could only reach you
C
If I could make you smile
Bb
If I could only reach you
                                F
That would really be a breakthrough - oh yeah

Refrão:
F               Eb9
Breakthrough these barriers of pain
F             F4
Breakthrough to the sunshine from the rain
Dm
Make my feelings known towards you
                         F
Turn my heart inside and out for you now
Bb                            Dm C Bb           Bb9/C
Somehow I have to make this final breakthrough        Now!


Verso 2:
F
Your smile
Bb/F
speaks books to me
F
I break up
With each and
 Bb/F
every one of your looks at me
F
Honey you're starting something
 Bb/F
deep inside of me
Bb                                C/Bb
Honey you're sparking something this fire in me
Bb
I'm outta control
                       Dm   Eb        C
I wanna rush headlong into this ecstacy

Pré-refrão:
F
If I could only reach you
C
If I could make you smile
Bb
If I could only reach you
                                F
That would really be a breakthrough

Pré-refrão:
F
If I could only reach you
C
If I could make you smile
Bb
If I could only reach you
                                F
That would really be a breakthrough - oh yeah

F            Eb9
Breakthrough,
F            F4
breakthrough


Solo:
|------10...-----12...-----14...-----15...-----17...-----19...-|
|-13b15--...15b17--...17b19--...18b20--...20b22--...22b24--...-|
|--------------------------------------------------------------|
|--------------------------------------------------------------|
|--------------------------------------------------------------|
|--------------------------------------------------------------|

|----------------------------151718_p17p1517v---|
|-----------------18151718----------------------|
|------------1416-------------------------------|
|--------1517-----------------------------------|
|-15-1818---------------------------------------|
|-----------------------------------------------|

|---------------------------_____17-18-20-b22v-----------------|
|---------------------15171820(20)---------------22r__20p18vv--|
|---------------141517-----------------------------------------|
|---------141517-----------------------------------------------|
|-15131517-----------------------------------------------------|
|--------------------------------------------------------------|

|-----------------------------------------------|
|-22r__20p18--20b22--20b22--20b22--20b22--20vv..|
|-----------------------------------------------|
|-----------------------------------------------|
|-----------------------------------------------|
|-----------------------------------------------|

Pré-refrão:
F
If I could only reach you
C
If I could make you smile
Bb
If I could only reach you
                                F
That would really be a breakthrough

Pré-refrão:
F
If I could only reach you
C
If I could make you smile
Bb
If I could only reach you
                                F
That would really be a breakthrough

         F
Breakthroughhhhhh...

   F           C           Bb         Bb/F

 <-----      ||||1|      <-----      |||||1
 |||2||      ||2|||      ||||||      ||||||
 |34|||      |3||||      ||234|      ||234|
 ||||||      ||||||      ||||||      ||||||
 ||||||      ||||||      ||||||      ||||||
 O ooo        O ooo       O ooo        Oooo


 Bb9/C         Eb         C/Bb         Dm

 ||||<-    6ª<-----      ||||||      |||||1
 ||||||      ||||||      ||||||      |||2||
 |234||      ||234|      ||||||      ||||3|
 ||||||      ||||||      ||||||      ||||||
 ||||||      ||||||      ||||||      ||||||
  Ooooo       O ooo      O oooo        Oooo


  F4          Eb9

 ||||<-      ||||||
 ||||||    6ª|<----
 ||34||      ||||||
 ||||||      ||34||
 ||||||      ||||||
   Oooo       Ooooo

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bb/F = X 8 8 7 6 X
Bb9/C = X 3 3 3 1 1
C = X 3 2 0 1 0
C/Bb = X 1 2 0 1 X
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
Eb9 = X 6 8 8 6 6
F = 1 3 3 2 1 1
F4 = 1 3 3 3 1 1
