Queen - Tutti Frutti

Capo Casa 2


E|---0--0--0-0--0-0--0--0----|
B|---5--5--5-5--5-5--5--5----|
G|---------------------------|
D|---------------------------|
A|---------------------------|
E|---------------------------|

Bop bopa-a-lu a whop bam boo

      E
Tutti frutti, oh Rudy
      E
Tutti frutti, oh Rudy
      A
Tutti frutti, oh Rudy
      E
Tutti frutti, oh Rudy
      B          A
Tutti frutti, oh Rudy
E
A whop bop-a-lu a whop bam boo


      E
Got a girl named Sue, she knows just what to do
      A                                      E
Got a girl named Sue, she knows just what to do
E                     stop                  stop
She rocks to the east, she rocks to the west
E7
She's the girl that I know best
      A
Tutti frutti, oh Rudy
      E
Tutti frutti, oh Rudy
      B          A
Tutti frutti, oh Rudy
E
A whop bop-a-lu a whop bam boo

Got a girl named Daisy, she almost drives me crazy
Got a girl named Daisy, she almost drives me crazy
She knows how to love me, yes indeed
Boy I don't know what you're doin' to me

Tutti frutti, oh Rudy
Tutti frutti, oh Rudy
Tutti frutti, oh Rudy
A whop bop-a-lu a whop bam boo
A whop bop-a-lu a whop bam boo
( whop bop-a-lu a whop bam boo)
A whop bop-a-lu a whop bam boo
Tutti frutti, oh Rudy
Tutti frutti, oh Rudy
Tutti frutti, oh Rudy
Tutti frutti, oh Rudy
A whop bop-a-lu a whop bam boo

Got a girl named Daisy, she almost drives me crazy
Got a girl named Daisy, she almost drives me crazy
She knows how to love me, yes indeed
Boy I don't know what you're doin' to me

Tutti frutti, oh Rudy
Tutti frutti, oh Rudy
Tutti frutti, oh Rudy
A whop bop-a-lu a whop bam boo


----------------- Acordes -----------------
Capotraste na 2ª casa
A*  = X 0 2 2 2 0 - (*B na forma de A)
B*  = X 2 4 4 4 2 - (*C# na forma de B)
E*  = 0 2 2 1 0 0 - (*F# na forma de E)
E7*  = 0 2 2 1 3 0 - (*F#7 na forma de E7)
