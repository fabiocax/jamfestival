Queen - My Melancholy Blues

Cm
Another party's over
Gm
And I'm left cold sober
F#º       Fm7             Eº
My baby left me  for somebody  new
Fm
I don't want to talk about it
Abm
Want to forget about it wanna
Eb/G
Be intoxicated with that
Fm7          Bb7
Special brew    So come and

Eb        G7
get me    Let me
Ab             Abm
get in that sinking feeling
Eb              F#º          Fm7
that says my heart is on an all-time low

Bb7               Eb
So        don't expect me
G7
To behave perfectly
Ab                Abm
And wear that sunny smile
Eb/G            F#º
My guess is I'm in for a
Fm7
cloudy and overcast
F#º
Don't try and stop me cos I'm
Eb/G             Eº           Fm    Fm7
heading for that stormy weather soon

Ab
I'm causing a mild sensation
Abm
With this new occupation
Eb/G               Cm
I'm permenantly glued to
Gm                 Cm
this extraordinary mood.  So now move o-
Fm7
ver     let me take
F#º
over    with my
Fm7   F#º      Ab7    Bb7         Eb
Mel - an - chol - y      Blues

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Ab7 = 4 6 4 5 4 4
Abm = 4 6 6 4 4 4
Bb7 = X 1 3 1 3 1
Cm = X 3 5 5 4 3
Eb = X 6 5 3 4 3
Eb/G = 3 X 1 3 4 X
Eº = X X 2 3 2 3
F#º = 2 X 1 2 1 X
Fm = 1 3 3 1 1 1
Fm7 = 1 X 1 1 1 X
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
