Queen - Lost In Love

(intro)

E|---------------------------------|------------------------0---------|
B|---------------------------0-----|-------1-------1----------------1-|
G|-2-------0-------2-----0-----0---|-----2---0------------2-------0---|
D|---0---0---0---0---0-----0-----0-|---2---------2------2-------2-----|
A|-----2-------2-------2-----------|-3---------3------3-------3-------|
E|---------------------------------|----------------------------------|

E|-------0--------------------------|---------------------------------|
B|---------1-----0-------1--1/0-----|-------------------------0-------|
G|-----0-----0---------0----0---0-0-|-------2-------0-------0---0-----|
D|---2---------2-----2--------2-----|-----0---0---0---0---0-------0---|
A|-0---------------0----------------|---2-------2-------2-----------2-|
E|----------------------------------|-3-------------------------------|

  G9                                   C
I realize the best part of love is the thinnest slice
And it don't count for much
        Am
but I'm not letting go

                                   G
I believe there's still much to believe in

   G
So lift your eyes if you feel you can
C
Reach for a star and I'll show you a plan
    Am
I figured it out
                              G       (G F# G B A - bass only)
What I needed was someone to show me

Am                  Bm
You know you can't fool me
           C              G    (G F#)
I've been loving you too long
Em             Am
It started so easy
     A/C#                      C
You want to carry on (to carry on)

(refrão)
G
Lost in love and I don't know much
       C
Was I thinking aloud

Fell out of touch
         Am
But I'm back on my feet
                      G
And eager to be what you wanted

So lift your eyes if you feel you can (feel you can)
C
Reach for a star and I'll show you a plan (show you a plan)
  Am
I figured it out
                              G
What I needed was someone to show me

(G F# G B A - bass only)

Am                  Bm
You know you can't fool me
           C             G
I've been loving you too long
Em            Am
It started so easy
    A/C#                            C
You want to carry on (to carry on)

(refrão)

(G F# G B A - bass only)

Am                  Bm
You know you can't fool me
           C             G
I've been loving you too long
Em            Am
It started so easy
    A/C#                            C
You want to carry on (want to carry on)

(refrão)

(Now I'm lost, lost in love, lost in love, lost in love..)

----------------- Acordes -----------------
A/C# = X 4 X 2 5 5
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
G9 = 3 X 0 2 0 X
