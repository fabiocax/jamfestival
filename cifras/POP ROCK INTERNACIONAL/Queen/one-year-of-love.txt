Queen - One Year Of Love


D    A            Bm
Just one year of love,
    G             Em        A
Is better than a lifetime alone.
D                A              Bm
One sentimental moment in your arms,
           G             Em               A
Is like a shooting star right through my heart.
Chorus:
      Bm                    F#m
It's always a rainy day without you.
Bm                        F#m
I'm a prisoner of love inside you.
      G                 A         G         A
I'm falling apart all around you. Yeah.
 D        A        Bm
My heart cries out to your heart,
     G          Em      A
I'm lonely but you can save me.
   D            A    Bm
My hand reaches out for your hand,

     G            Em                A
I'm cold but you light the fire in me.
Chorus 2:
Bm                               F#m
My lips search for you lips, I'm hungry for your touch,
Bm                       F#m
There's so much left unspoken.
    G                 A
And all I can do is surrender.
G                    A
To the moment just surrender.
Sax Solo: D  A  Bm  G  Em  A    D  A  Bm  Em  A
Bridge:
    Bm                        F#m
And no one ever told me that love would hurt so much. (Oooh yes it hurts)
    Bm                   F#m
And pain is so close to pleasure.
    G                  A
And all I can do, is surrender,
G            A
To your love. (Just surrender to your love)

D    A            Bm
Just one year of love,
    G             Em        A
Is better than a lifetime alone.
D                A              Bm
One sentimental moment in your arms,
           G             Em               A
Is like a shooting star right through my heart.
Chorus:
      Bm                    F#m
It's always a rainy day without you.
Bm                        F#m
I'm a prisoner of love inside you.
      G                 A
I'm falling apart all around you.
    G                  A             G       A
And all I can do, is surrender.
Final Progression: D  A  Bm  G  Em  A   D  A  Bm  G  Em  A    Bm  A  Bm  A

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
