Coldplay - Strawberry Swing

Capo Casa 1

(intro) G

E|---------16-16-16/13-----------|
B|--16-/18-------------16-13-----|
G|---------------------------15--|
D|-------------------------------|   (x2) *ao começar a música, toca-se apenas uma vez essa parte
A|-------------------------------|
E|-------------------------------|

E|-------------------------------|
B|---------16-16-16/13-----------|
G|--15-/17-------------15-13-----|
D|---------------------------15--|   (x2)
A|-------------------------------|
E|-------------------------------|

              C                  G                       D
They were sitting, they were sitting in the strawberry swing
                          C       G      D
And every moment was so precious
              C                  G                       D
They were sitting, they were talking in the strawberry swing

                       C          G
And everybody was for fighting
                         D
Wouldn't wanna waste a thing
            C               G
Cold, cold water bring me 'round
        D
Now my feet won't touch the ground
           C
Cold, cold water
         G
What you say?
     Bm                        C
It's such, it's such a perfect day
                     G
It's such a perfect day
        C              G                      D
I remember we were walking up to strawberry swing
                        C        G
I can't wait until the morning
                          D
Wouldn't wanna change a thing
        C              G
People moving all the time
          D
Inside a perfect straight line
          C           G
Don't you wanna just curve away
      Bm                      C
It's such it's such a perfect day
                     Am
It's such a perfect day

 Am  Em  Am  C
           Ahhhh....

        G              G/F#
Now the sky could be blue
        G/F
I don't mind
     C                      G
Without you its a waste of time
              G/F#        G/F       C                       G
...could be blue I don't mind, without you it's a waste of time
                   G/F#           G/F     C                      G
The sky could be blue, could be gray without you I'm just miles away
                 G/F#           G/F      C                        G
The sky could be blue, I don't mind, without you it's a waste of time

(G)

----------------- Acordes -----------------
Capotraste na 1ª casa
Am*  = X 0 2 2 1 0 - (*A#m na forma de Am)
Bm*  = X 2 4 4 3 2 - (*Cm na forma de Bm)
C*  = X 3 2 0 1 0 - (*C# na forma de C)
D*  = X X 0 2 3 2 - (*D# na forma de D)
Em*  = 0 2 2 0 0 0 - (*Fm na forma de Em)
G*  = 3 2 0 0 0 3 - (*G# na forma de G)
G/F*  = 1 X X 0 0 3 - (*G#/F# na forma de G/F)
G/F#*  = X X 4 4 3 3 - (*G#/G na forma de G/F#)
