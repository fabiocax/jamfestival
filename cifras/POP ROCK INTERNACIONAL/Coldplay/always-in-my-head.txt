Coldplay - Always In My Head

Capo Casa 3

Em         G   Bm             A   Em
I think of you,   I haven't slept
        G    Bm            A/C#   A
I think I do,  but I don't forget

        Em                 G
My body moves, goes where I will
          Bm                       A
But though I try, my heart stays still

Em                          G
It never moves just won't be led
                Bm          A
And so my mouth waters to be fed
                     Em       G
And you're always in my head

                     Bm    A
And you're always in my head
                 Em      G
You're always in my head

                 Bm    A
You're always in my head

(Sem Capotraste)
E|---8-----8-----8-----8-----8-----8-----8-----8-----8-----8----|
B|6-----6-----6-----6-----6-----6-----6-----6-----6-----6-----6-|
G|--------------------------------------------------------------|
D|--------------------------------------------------------------|
A|--------------------------------------------------------------|
D|--------------------------------------------------------------|

E|---8-----10-----8-----8-----10-----8-----8-----10-----8-----8-|
B|6-----6------6-----6-----6------6-----6-----6------6-----6----|
G|--------------------------------------------------------------|
D|--------------------------------------------------------------|
A|--------------------------------------------------------------|
D|--------------------------------------------------------------|

                 Em      G
You're always in my head
          Bm            A/C#   A
Always in my, always in my

Em     G     Bm
This I guess
                      A/C#    C            Em
Is to tell you you're chosen out from the rest

----------------- Acordes -----------------
Capotraste na 3ª casa
A*  = X 0 2 2 2 0 - (*C na forma de A)
A/C#*  = X 4 X 2 5 5 - (*C/E na forma de A/C#)
Bm*  = X 2 4 4 3 2 - (*Dm na forma de Bm)
C*  = X 3 2 0 1 0 - (*D# na forma de C)
Em*  = 0 2 2 0 0 0 - (*Gm na forma de Em)
G*  = 3 2 0 0 0 3 - (*A# na forma de G)
