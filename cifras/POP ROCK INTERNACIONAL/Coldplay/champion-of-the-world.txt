Coldplay - Champion Of The World

[Intro] G  D  C
        G  D  C

E|----------------------------------------------------|
B|----------------------------------------------------|
G|-5-4---5-4---5-4---5-4---5-2---5-2---2-5---7-5------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

E|----------------------------------------------------|
B|----------------------------------------------------|
G|-5-4---5-4---5-4---5-4---5-2---5-2---2-5---7-5------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

[Primeira Parte]

    G
I tried my best to be just like

    D                C
The other boys in school
    G
I tried my best to get it right
     D             C
And died at every duel

      G
This mountainside is suicide
       D               C
This dream will never work
           G
Still the sign upon my headstone, write
     D              C
"A champion of the world"

( G  D  C )

[Segunda Parte]

         G
Yeah I tried my best to stay alight
     D          C
Fly like a firework
    G
I tried my best at taking flight
        D            C
But my rocketship reversed
     G
Oh, referee, don't stop the fight
     D                C
Everyone can see I'm hurt
           G
But I'll stand before conquistadors
          D              C
Til I'm champion of the world

[Refrão]

         C
(When I sail) And when I sail
             G
I'm sailing west
                 C
(Though I might fail) Knowing I might fail
                 G              D
But, still I am hoping for the best
         C
(In my dreams) And in my dreams
          Em
Onto my chest
                       C
She'll pin the colours and say

"I wandered the whole wide world but
       D          G
Baby, you're the best"

( G  D  C )
( G  D  C )

[Final]

         G
So I'm flying on my bicycle
        D                C
Heading upwards from the Earth
      G
I am jumping with no parachute
    D            C
Out into the universe
       G
I have E.T. on my bicycle
         D               C
Because giving up won't work
      G
I am riding on my rocketship
          D              C
And I'm champion of the world

( G  D  C )

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
