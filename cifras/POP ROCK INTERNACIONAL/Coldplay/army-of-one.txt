Coldplay - Army Of One

Intro 4x: Ab Eb Bb

                 Ab
Been around the world
Eb              Bb
The wonders to view
                 Ab
Been around the world
Eb                       Bb
Looking for someone like you
          Ab
Pyramids try
Eb       Bb
Babylon too
          Ab                    Eb                  Bb
But the beautifullest treasures lie in the deepest blue
                Ab
So I never say die
Eb            Bb
Aim never untrue
              Ab   Eb             Bb
I'm never so high as when I'm with you

                   Ab   Eb                    Bb
And there isn't a fire that I wouldn't walk through
Gm         Ab   Eb              Bb
My army of one is gonna fight for you

(repete a sequência o resto da música)

Been around the world
The universe too
I've been around flying, baby
It's nothing I wouldn't do
Tanzaku stars
Lasso the moon
And be standing there beside you right
When the storm comes through

Cause I never say die
Aim never untrue
Never so high as when I'm with you
And there isn't a fire
That I wouldn't walk through
My army of one is gonna fight for you
My army of one is gonna fall for you, yeah

Cause you reign
All cause you reign
All cause you reign
My army of one is gonna call it through
Saying my heart is my gun, army of one
Yeah my heart is my gun, army of one
It's my only weapon, army of one
Saying my heart is my gun, army of one

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Bb = X 1 3 3 3 1
Eb = X 6 5 3 4 3
Gm = 3 5 5 3 3 3
