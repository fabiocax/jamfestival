Coldplay - Adventure Of A Lifetime

[Intro] Dm G  Am

[Riff Principal]

E|--17p12h17p12-----12-------------------------|
B|---------------15-----15-13----------13------|
G|----------------------------14-12-14----14---|  (2x)
D|---------------------------------------------|
A|---------------------------------------------|
E|---------------------------------------------|

E|--15p12h15p12-----12------------------------|--15p12h15p12-----12--------------19-20--|--17~--|
B|---------------15-----15-13----------13-----|---------------15-----15-13--------------|-------|
G|----------------------------14-12-14----14--|----------------------------14-12--------|-------|
D|--------------------------------------------|-----------------------------------------|-------|
A|--------------------------------------------|-----------------------------------------|-------|
E|--------------------------------------------|-----------------------------------------|-------|

[Intro] Dm G  Am

Dm                G              Am
  Turn your magic on, Umi she'd say

                                 Dm
Everything you want is a dream away
        G              Am
We are legends, every day
That's what she told me

Dm                G                Am
  Turn your magic on, to me she'd say
                                 Dm
Everything you want is a dream away
             G                   Am
Under this pressure, under this weight
We are diamonds

           Dm           G
I feel my heart beating
           Am
I feel my heart underneath my skin
           Dm
I feel my heart beating
                                         Dm G  Am         Dm   G
Oh, you make me feel like I'm alive again         alive again
                 Am
Oh, you make me feel like I'm alive again

Dm                G                Am
  Said I can't go on, not in this way
                                     Dm
I'm a dream that died by light of day
               G                Am
Gonna hold up half the sky and say
Only I own me

           Dm           G
I feel my heart beating
           Am
I feel my heart underneath my skin
                   Dm
Oh, I can feel my heart beating
                                         Dm G  Am         Dm   G
Oh, you make me feel like I'm alive again         alive again
                 Am
Oh, you make me feel like I'm alive again

[Dm  G  Am]

Dm                G              Am
  Turn your magic on, Umi she'd say
                                 Dm
Everything you want is a dream away
             G                   Am
Under this pressure, under this weight
                           Dm
We are diamonds taking shape
We are diamonds taking shape

[Dm  G  Am]
[Dm  G  Am]

         Dm             G
If we've only got this life
        Am
this adventure oh then I
             Dm             G
And if we've only got this life
                 Am
You'll get me through oh
             Dm             G
And if we've only got this life
              Am
in this adventure oh then I
                     Dm
Wanna share it with you
      G         Am
With you, with you
Sing it, oh

    Dm               G
Woohoo (woohoo)  Woohoo (woohoo)
    Am
Woohoo (woohoo)  Woohoo (woohoo)


[Riff Final]

E|---13---13---13---13---12--|----------------------|-----------------------------------|
B|---------------------------|--15--15--15--13--15--|--/17----------13--15--/17---------|
G|---------------------------|----------------------|-------14--14---------------14-14--|
D|---------------------------|----------------------|-----------------------------------|
A|---------------------------|----------------------|-----------------------------------|
E|---------------------------|----------------------|-----------------------------------|

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Dm = X X 0 2 3 1
G = 3 2 0 0 0 3
