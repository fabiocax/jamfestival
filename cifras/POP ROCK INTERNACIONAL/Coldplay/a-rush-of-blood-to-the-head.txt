Coldplay - A Rush Of Blood To The Head

        Am                                      C/G
He said I'm gonna buy this place and burn it down
Em7                              Am7
I'm gonna put it six feet underground
            Am                                  C/G
He said "I'm gonna buy this place and watch it fall
Em7                                          Am7
Stand here beside me baby in the crumbling walls

    Am                                       C/G
Oh I'm gonna buy this place and start a fire
Em7                                              Am7
Stand here until I fill all your heart's desires
           Am                                 C/G
Because I'm gonna buy this place and see it burn
Em7                                       Am   Am7
Do back the things it did to you in return

F7M       F7M(9)
Ah ah ah, ah ah ah


F7M(9/11+)     Am                            C/G
         He said I'm gonna buy a gun and start a war
Em7                                          Am7
If you can tell me something worth fighting for
       Am                                      C/G
Oh and I'm gonna buy this place that's what I said
Em7                                     Am     Am7
Blame it upon a rush of blood to the head

F                                            D
Honey all the movements you're starting to make
                                  F
See me crumble and fall on my face
                                 D
and I know the mistakes that I made
         Bb                       F
See it all disappear without a trace.
                                 D
And they call as they beckon you on
          Bb                         Am    C/G    Em7
They said start as you mean to go on
Am7                        Am   C/G   Em7   Am7
Start as you mean to go on

Am                                             C/G
He said "I'm gonna buy this place and see it go
Em7                                           Am7
Stand here beside my baby, watch the orange glow
Am                                     C/G
Some will laugh and some just sit and cry
Em7                                        Am7
But you just sit down there and you wonder why

Am                                  C/G
So I'm gonna buy a gun and start a war
Em7                                         Am7
If you can tell me something worth fighting for
Am                                         C/G
And I'm gonna buy this place that's what I said
Em7                                    Am
Blame it upon a rush of blood to the head
               Am7
Oh to the head

F                                             D
Honey all the movements you're starting to make
                                  F
See me crumble and fall on my face
                                 D
and I know the mistakes that I made
         Bb                       F
See it all disappear without a trace
                                 D
And they call as they beckon you on
          Bb                         Am
They said start as you mean to go on
C/G            Em7   Am7            Am  C/G  Em7  Am7
  Need to go on        need to go on

                        Am                    C/G
So meet me by the bridge, oh meet me by the lane
                         Em7              Am7
When am I going to see that pretty face again
                   Am                C/G
Meet me on the road meet me where I said
              Em7                           Am
Blame it all upon a rush of blood to the head

    Am     Bm/A     Am
E|--0---------------0--|
B|--1--1-1/3-3-3-3--1--|
G|--2--2-2/4-4-4-4--2--|
D|--2--2-2/4-4-4-4--2--|
A|--0---------------0--|
E|---------------------|

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Bb = X 1 3 3 3 1
Bm/A = X 0 4 4 3 X
C/G = 3 3 2 X 1 X
D = X X 0 2 3 2
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
F7M = 1 X 2 2 1 X
F7M(9) = X 8 7 9 8 X
F7M(9/11+) = 1 0 2 0 0 0
