Coldplay - Rainy Day

Capo Casa 1

(intro 8x) D   G  

(verse 1)
                             D
Then there was rain
           G                            D
The sky wore a veil of gold and green
       G                                            D
At night it was the bright of the moon with me
  G                           D                         G
Time is just floating

(verse 2)
                          D
Then there was rain
         G                                   D
The sound foundations are crumbling
                          G                                D
Through the ground comes a bit of a-tumbling
         G                                  D
And time was just floating away
                                   G
We can watch it and stay

And we can listen

(pre chorus 2x) Eb  A 

C             G                 D
Oh rainy day, come 'round
C                            G                    D
  Sometimes i just want it to slow it down
C                     G          D
 And we're separated now, i'm down
               C                                   G
But i love it when you come over to the house
                              D
I love it when you come to my house

( D   G )  (8x)

(verse 1 & 2)
(chorus 3x)

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
