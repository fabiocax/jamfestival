Coldplay - Gold In Them Hills

C                            F
I know it doesn't seem that way
C                          F           Dm
But maybe it's the perfect day
                 G               Dm
Even though the bills are piling
                G                C
And maybe Lady Luck ain't smiling

                  F              C
But if we'd only open our eyes
                      F           Dm
We'd see the blessings in disguise
                   G                    Dm
That all the rain clouds are fountains
                    G                C
Though our troubles seem like mountains

           F             C
There's gold in them hills
           F            Am
There's gold in them hills

    G             Am
So don't lose heart
         G                    C
Give the day a chance to start

(mesmos acordes)
Every now and then life says
Where do you think you're going so fast
We're apt to think it cruel but sometimes
It's a case of cruel to be kind

And if we'd get up off our knees
Why then we'd see the forest for the trees
And we'd see the new sun rising
Over the hills on the horizon

         F                C
There's gold in them hills
         F               Am
There's gold in them hills
     G              Am
So don't lose faith
          G                     Am
Give the world a chance to say
     G                       Am
A word or two, my friend
  G                         Am
There's no telling how the day might end
    F            G       F
And we'll never know until we see
  C           F               C
That there's gold in them hills
        F                 Am
There's gold in them hills
    G             Am
So don't lose heart
         G                C
Give the day a chance to start
            G                  C
There's gold in them hills
             G                C
There's gold in them hills

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
