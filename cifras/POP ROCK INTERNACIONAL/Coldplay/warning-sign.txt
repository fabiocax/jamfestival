Coldplay - Warning Sign

G              D/F#
A warning sign,
A         E                                              G    D/F#
    I missed the good part, then I realised,
A       E                                      G      D/F#
    I started looking and the bubble burst,
A       E                                  G   D/F#
    I started looking for excuses,
A   E     G      D/F#
          Come on in,
A          E                                    G       D/F#
    I've gotta tell you what a state I'm in,
A           E                                 G       D/F#
    I've gotta tell you in my loudest tones,
A              E                                  G      D/F#
   That I started looking for a warning sign

A  E (segura 4 tempos)

|Chorus 
 |                         D        F#m        A       E
|           When the truth is,  I miss you,

 |                       D      F#m          A              E
|     Yeah the truth is,  that I miss you so

G                D/F#
A warning sign,
A                          E                        G         D/F#
It came back to haunt me, and I realised,
A                             E                     G                D/F#
That you were an island and I passed you by,
A                            E                   G       D/F#    A     E
And you were an island to discover
G           D/F#
Come on in,
A                E                          G         D/F#
I've gotta tell you what a state I'm in,
A                E                        G            D/F#
I've gotta tell you in my loudest tones,
A          E                                 G            D/F#
That I started looking for a warning sign

A  E (segura 4 tempos)

|Chorus 
 |                         D        F#m        A       E
|           When the truth is,  I miss you,
 |                       D      F#m          A              E
|     Yeah the truth is,  that I miss you so
 |                   G     F#m                   A                    E
|     And I'm tired, I should not have let you go,
 |     A                G       D     A  (preparação para o fim da música - batida mais devagar e mais suave)
|     Oooooooo,

          A      G                       D      A
So I crawl back into your open arms,
          A         G                     D        A
Yes I crawl back into your open arms,
            A       G                       D       A
And I crawl back into your open arms,
           A       G                      D        F#m   (deixa os acordes soarem uma única vez)
Yes I crawl back into your open arms.

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
