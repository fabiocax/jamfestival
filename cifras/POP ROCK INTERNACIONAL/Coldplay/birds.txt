Coldplay - Birds

Intro 2x: Am F C

 Am                       F
been standing in the corner
                C
studying the lights
Am                  F                        C
the dreaming of escape will keep you up at night
Am                            F
but someone had put the flares up
                    C
and got me in the rays
Am  F                      C
so... I guess I'd better stay
                        F
'uh uh no come on' you say
                      G
it's a fool's gold thunder
                     F
it's just a warring rain
                                   G
don't let the fears just start 'what if

                  F
I won't see you again'
                       G                  F
around here you never want to sleep all night
                     G
so start falling in love, start the riot and
         Am   F    C
come on rage with me
                Am
we don't need words
F               C
and we'll be birds
                      Am  F  C
got to make our own key
  Am              F
only got this moment
       C
you and me, guilty of nothing
           F
but geography
                G
come on and raise it
                         F
come on and raise this noise
                  G
for the million people
                   F
who got not one voice
                   G
come on it's not over if you
              F
mean it say loud
                  G
come on all for Love
                          Am    F    C
out from the underground, away with me
                Am  F  C
we don't need words
            Am   F   C
close your eyes and see
               Am
and we'll be birds
F        C
flying free
                        Am  F  C   (solo)
holding on in the mystery
          Am     F     C     G
fearless go  oh oh oh oh oh oh
              Am  F  C  G
fearless together
          Am  F    C   G
when you fly won't you
           Am   F   C  G
won't you take me too?
          Am   F   C   G
in this world so cruel
           Am  F    C
I think you're so cool

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
