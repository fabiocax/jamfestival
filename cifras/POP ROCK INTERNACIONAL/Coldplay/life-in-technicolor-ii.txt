Coldplay - Life In Technicolor II

Riff 1 - acompanha quase toda a música:
(Na 2ª vez, as notas da corda 1 são trocadas como no parênteses)

E|----------------------2(4)-2(4)-----|
B|---------2--5------------------5-2--|   2x
G|-2---4-2-----2--2-4-2---------------|
D|------------------------------------|
A|------------------------------------|
E|------------------------------------|

Riff 2 - usado no refrão:

E|---------9-----------------9-9------|
B|-10--12----10-12--10-12-10-----10---|
G|------------------------------------|
D|------------------------------------|
A|------------------------------------|
E|------------------------------------|

Intro: Riff 1 (2x)
       F#m  D   A  (8x)



          F#m           D
There's a wild wind blowing
          A
Down the corner of my street
      F#m              D               A
Every night there the headlights are glowing
          F#m       D
There's a cold war coming
        A       F#m
On the radio I heard
E4         E          A
Baby, it's a violent world

   F#m          D      C#m
Oh love, don't let me go
           F#m                D             C#m
Won't you take me where the street lights glow?
       F#m     D
I can hear it coming
       A              F#m
I can hear the siren sound
        E4         E          A
Now my feet won't touch the ground

F#m  D  A  (2x)


F#m           D
Time came a-creeping
        A
Oh and time's a loaded gun
       F#m       D      A
Every road is a ray of light
        F#m      D
It goes o___________on
A                   F#m
Time will leak and lead you on
      E4           E          A
Still it's such a beautiful night

    F#m         D      C#m
Oh love, don't let me go
           F#m                D             C#m
Won't you take me where the street lights glow?
       F#m     D
I can hear it coming
        A           F#m
Like a serenade of sound
        E4         E          A
Now my feet won't touch the ground

F#m  D  A

  F#m    D        A
O___________________oh

 F#m        D
Gravity, release me
          A             F#m
And don't ever hold me down
        E4         E          A     ( A )
Now my feet won't touch the ground

----------------- Acordes -----------------
A = X 0 2 2 2 0
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E4 = 0 2 2 2 0 0
F#m = 2 4 4 2 2 2
