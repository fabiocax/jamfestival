Coldplay - Solid Ground

Verse 1:

    Emaj7               A5
Oh, when I get back on solid ground,

          C#m7          Bsus4
I'm gonna crawl my way to you,

      Emaj7               A5
Oh, I once was lost, but now I'm found,

          F#m11          Bsus4
I'm gonna do what I should do,

    Emaj7                  A5
And when I get back on my feet again,

          Bsus4         C#m7
I'm gonna run my way to you,

         A5              Emaj7
Oh, when all is lost and come undone,


          Bsus4                A5
I'm gonna sing out, and see ya through,
    Emaj7    A5
But no, the water,

    Emaj7        B7 A5
The water flows o--ver,
Verse 2:

    Emaj7                   A5
Oh, when I get back off my feet again,

          C#m7          Bsus4
I'm gonna crown my days a turn,

       Emaj7            A5
So all I've been on my knees again,

         F#m11       Bsus4
And I'll do it all the time,

         Emaj7          A5
And when I get back on solid ground,

          C#m7          Bsus4
I'm gonna crawl my way to you,

      A5                 Emaj7
Oh, I once was lost, but now I'm found,

   Bsus4                 A5
So here's what I'm gonna do you,
        Emaj7    A5
I sing, oh, the water,

    Emaj7        B7 A5
The water flows o--ver,

        Emaj7    A5
I sing, oh, the water,

    Emaj7        B7 A5  Emaj7
The water flows o--ver you,

----------------- Acordes -----------------
A5 = X 0 2 2 X X
B7 = X 2 1 2 0 2
Bsus4 = X 2 4 4 5 2
C#m7 = X 4 6 4 5 4
Emaj7 = X X 2 4 4 4
F#m11 = 2 2 4 2 2 2
