Coldplay - Bigger Stronger


chorus:
Am                          F     C
i wanna be bigger, stronger drive a faster car

to take me anywhere in seconds,
               Dsus2       Am
to take me anywhere i wanna go
                       F    C
and drive around a faster car

(repeat once)

then
(C)
i will settle for nothing less
          Dsus2          Am         (Am)
i will settle for nothing less

     (Chorus)
                  Dm       Dm       Gm
i think i want to change my   attitude

  C                Gm    Em   A
i think i want to change my oxygen
                   Dm       Dm      Gm  C  Gm
i think i want to change my air, my   atmosphere,
 Em       A           (A)
i want to reach out

(Am, D, Am, D) x 2

(Verse)

(Chorus)
then solo bit i havent worked out yet :)

Am           D        Am        D
bigger and better, bigger and better
Am          D         Am         D
bigger and better, bigger and better
Am                              F    C
bigger, stronger drive a faster car,
                                    Dsus2          Am
at the touch of a button i can go anywhere i wanna go

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
Dsus2 = X X 0 2 3 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
