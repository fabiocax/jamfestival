Coldplay - Charlie Brown

Capo Casa 3

[Intro]  Em  C  G  Em  C  Am

E|------3-------3-----3---3---3--3-------------|
B|--1/3--3--0h1---1h0----0--0---0--------------|
G|---------------------------------------------|
D|---------------------------------------------|
A|---------------------------------------------|                       
E|---------------------------------------------|

E|---------------------------------------------|
B|-----0----0----0-----------0----0----0-------|
G|--0h2--0h2--0h2---2--0--0h2--0h2--0h2---2--0-|
D|----------------------2----------------------|
A|---------------------------------------------|
E|---------------------------------------------|


Em     C     G
I    stole a key,
            Em                 C
Took a car downtown where the lost boys meet,

                      Am
I took a car downtown and took what they offered me

Em     C        G
To    set me free,
           Em                       C
I saw the lights go down at the end of the scene,
                               Am
I saw the lights go down and they're standing in front of me

[Em  C  G  Em  C  Am]

 Em    C       G
My scarecrow dreams,
            Em                  C
When they smash my heart into smitharines,
                             Am
I be a bright red rose come bursting the concrete.
 Em     C        G
Be the cartoon heart

 C
Light a fire, light a spark
 D                           C
Light a fire, a flame in my heart

We'll run wild,
                      Em
We'll be glow-in-the-dark

[Em  C  G  Em  C  Am]
[Em  C  G  Em  C  Am]

         D
All the boys, all the girls,
          C              G
All that matters in the world

         D
All the boys, all the girls,
         C              G
All the madness that occurs

         D
All the high's, all the low's,
        Bm               C
As the room a-spinning goes
 C
We'll run riot,
We'll be glowing in the dark

[Em  C  G  Em  C  Am]
[Em  C  G  Em  C  Am]
[Em  C  G  Em  C  Am]

          Em  C  G
So we'll soar
 C
Luminous and wired
                         G
We'll be glowing in the dark

----------------- Acordes -----------------
Capotraste na 3ª casa
Am*  = X 0 2 2 1 0 - (*Cm na forma de Am)
Bm*  = X 2 4 4 3 2 - (*Dm na forma de Bm)
C*  = X 3 2 0 1 0 - (*D# na forma de C)
D*  = X X 0 2 3 2 - (*F na forma de D)
Em*  = 0 2 2 0 0 0 - (*Gm na forma de Em)
G*  = 3 2 0 0 0 3 - (*A# na forma de G)
