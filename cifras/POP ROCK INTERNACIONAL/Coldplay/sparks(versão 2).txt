Coldplay - Sparks

 Am   A#m7 Am   C7   A#m6 Am6
:--0----0----0----0----0----0---------:
:--1----1----1----1----1----1---------:
:--2----1----2----3----2----2--2p0----:
:--2----2----2----2----4----3------0--:
:--0----0----0----3----0----0---------:
:-------------------------------------:

C                Am7
Did I drive you away
C                       Am7
Well I know what you'll say
           Fmaj9           C
You'll say "Oh sing one we know"
C                 Am7
But I promise you this
C                Am7     Fmaj9
I'll always look out for you
C
That's what I'll do
CHORUS:

       Am A#m7 Am C7 A#m6 Am6
And say "Oh!"
      Am A#m7 Am C7 A#m6 Am6
I say "0h!"
:C   Am7  :x4

C           Am7
My heart is yours
C              Am7     Fmaj9
Its you that I hold on to
              C
That's what I do
C                Am7
And I know I was wrong
C                   Am7
But I won't let you down
   Fmaj9
Oh yeah I will yeah I will
      C
Yes I will

       Am A#m7 Am C7 A#m6 Am6
I said "oh!"
      Am A#m7 Am C7 A#m6 Am6
I cry "oh!"
      C       Am7
I saw sparks      x4

Singing
C     Am7   C
La ah ah ah...x4

----------------- Acordes -----------------
A#m6 = 6 X 5 6 6 X
A#m7 = X 1 3 1 2 1
Am = X 0 2 2 1 0
Am6 = 5 X 4 5 5 X
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Fmaj9 = X 8 7 9 8 X
