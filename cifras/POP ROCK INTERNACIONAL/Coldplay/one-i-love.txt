Coldplay - One I Love

Intro:

Aadd9
Amaj7 Cmaj7 Gmaj7 Dmaj7
Amaj7 Cmaj7 Gmaj7 Dmaj7

e|-0---0---0-|-0---0---0-| 
b|-5-p-7-p-5-|-5-p-7-p-5-| 
G|-7-u-9-u-7-|-7-u-9-u-7-| 
D|---l---l---|---l---l---| 
A|---l---l---|---l---l---| 
E|-----------|-----------| 

Verse 1:

Amaj7                      Cmaj7
Could you, could you come back?

Gmaj7        Dmaj7
Come back together,


Amaj7               Cmaj7
Put yourself on my back,

Gmaj7           Dmaj7
And say its forever,

Amaj7                      Cmaj7
Could you, could you come on?

Gmaj7       Dmaj7
Come on forever,

Amaj7                   Cmaj7
Shoulders, knees in my back,

Gmaj7      Dmaj7
Keep us together,

[CHORUS]

 Amaj7 Cmaj7 Gmaj7 Dmaj7


Amaj7                    Cmaj7 Gmaj7 Dmaj7 Amaj7
'Cause you're the one I love,

Amaj7             Cmaj7 Gmaj7 Dmaj7 Amaj7
You're the one I love,

Amaj7             Cmaj7 Gmaj7 Dmaj7 Amaj7
You're the one I love,

Instrumental:

e|-0---0---0-|-0---0---0-| 
b|-5-p-7-p-5-|-5-p-7-p-5-| 
G|-7-u-9-u-7-|-7-u-9-u-7-| 
D|---l---l---|---l---l---| 
A|---l---l---|---l---l---| 
E|-----------|-----------| 
Verse 2:

Amaj7                      Cmaj7
Could you, could you come in?

Gmaj7                 Dmaj7
Could you tell me forever?

Amaj7             Cmaj7
Tie youself to a mast,

Gmaj7               Dmaj7
It's now or it's never,

Amaj7              Cmaj7
Could it tear us apart?

Gmaj7       Dmaj7
Tell me forever,

Amaj7                Cmaj7
It's gonna tear us apart,

Gmaj7         Dmaj7
Keep us together,

[CHORUS]

 Amaj7 Cmaj7 Gmaj7 Dmaj7


Amaj7                    Cmaj7 Gmaj7 Dmaj7 Amaj7
'Cause you're the one I love,

Amaj7             Cmaj7 Gmaj7 Dmaj7 Amaj7
You're the one I love,

Amaj7             Cmaj7 Gmaj7 Dmaj7 Amaj7
You're the one I love,

Amaj7 Cmaj7 Gmaj7 Dmaj7

Amaj7 Cmaj7 Gmaj7 Dmaj7

Amaj7 Cmaj7 Gmaj7 Dmaj7

Instrumental:

e|-0---0---0-|-0---0---0-|-0---0---0-|-0---0---0-| 
b|-5-p-7-p-5-|-5-p-7-p-5-|-5-p-7-p-5-|-5-p-7-p-5-| 
G|-7-u-9-u-7-|-7-u-9-u-7-|-7-u-9-u-7-|-7-u-9-u-7-| 
D|---l---l---|---l---l---|---l---l---|---l---l---| 
A|---l---l---|---l---l---|---l---l---|---l---l---| 
E|-----------|-----------|-----------|-----------| 

(Chris comes in with the piano)

Note: Here just do 1 strum at a time for one chord.

Fade:

Amaj7             Cmaj7 Gmaj7 Dmaj7
You're the one I love,

Amaj7             Cmaj7 Gmaj7 Dmaj7
You're the one I love,

Amaj7             Cmaj7 Gmaj7 Dmaj7
You're the one I love,

----------------- Acordes -----------------
Aadd9 = X 0 2 2 0 0
Amaj7 = X 0 2 1 2 0
Cmaj7 = X 3 2 0 0 X
Dmaj7 = X X 0 2 2 2
Gmaj7 = 3 X 4 4 3 X
