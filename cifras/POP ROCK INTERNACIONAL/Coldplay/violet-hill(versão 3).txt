Coldplay - Violet Hill

Riff 1
   F#5 F5 E5 Eb5
E|--------------|        
B|--------------|
G|--------------|
D|--4--3--2--1--|
A|--4--3--2--1--|
E|--------------|

      C#m
Was a long and dark December
                    C#m7
From the rooftops I remember
          A   A9    F#m  (Riff 1)
There was snow, white snow

  C#m
Clearly I remember from the windows
          C#m7               A       A9
They were watching while we froze
        F#m   (Riff 1)
Down below


          A
When the future's architectured
     B                     C#m
By a carnival of idiots on show
                 B
You'd better lie low
G#m     A   G#m  E        C#m B  C#m    (C#m)
If you love me, won't you let me know?

      C#m
Was a long and dark December
                       C#m7
When the banks became cathedrals
        A  A9      F#m  (Riff 1)
And the fox   became God

 C#m
Priest clutched onto bibles
                          C#m7
Hollowed out to fit their rifles
         A    A9      F#m   (Riff 1)
And the cross was held aloft

A
Bury me in armour
         B
When I'm dead and hit the ground
              C#m           B
My nerves are poles that unfroze
    G#m     A   G#m  E        C#m  B  C#m
And if you love me, won't you let me know?

[Solo]

     C#m                    C#m7
E|---------------------------------------------| 
B|--5p4---5----------------5p4---5-------------|
G|------6-----4---4-6----------6-----4---4-----|
D|----------6---6------------------6---6---4---| 
A|---------------------------------------------|
E|---------------------------------------------|

     A        A9            F#m                    (Riff 1)
E|---------------------------------------------| 
B|--5p4---5----------------5p4---5-------------|
G|------6-----4---4-6----------6-----4---4-----|
D|----------6---6------------------6---6-------| 
A|---------------------------------------------|
E|---------------------------------------------|

     C#m                    C#m7
E|---------------------------------------------| 
B|--5p4---5----------------5p4---5-------------|
G|------6-----4---4-6----------6-----4---4-----|
D|----------6---6------------------6---6---4---| 
A|---------------------------------------------|
E|---------------------------------------------|

     A        A9            F#m                    (Riff 1)
E|---------------------------------------------| 
B|--5p4---5----------------5p4---5-------------|
G|------6-----4---4-6----------6-----4---4-----|
D|----------6---6------------------6---6-------| 
A|---------------------------------------------|
E|---------------------------------------------|

        A
I don't wanna be a soldier
        B
who the captain of some sinking ship
      C#m          B
would stow, far below
   G#m   A   G#m   E        C#m   B   C#m
So, if you love me, why'd you let me go?

C#m       E    A        E     G#m      A
I took my love down to Violet Hill
        C#m     G#m   A
There we sat    in    snow
A     B      C#m    F#m     E         G#m
All   that   time   she was silenced still
    G#m    A    G#m  E        C#m  B   C#m      A
So, if you love me, won't you let  me  know?
G#m    A    G#m  E        C#m  B  C#m
If you love me, won't you let  me know?

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
E = 0 2 2 1 0 0
E5 = 0 2 2 X X X
Eb5 = X 6 8 8 X X
F#5 = 2 4 4 X X X
F#m = 2 4 4 2 2 2
F5 = 1 3 3 X X X
G#m = 4 6 6 4 4 4
