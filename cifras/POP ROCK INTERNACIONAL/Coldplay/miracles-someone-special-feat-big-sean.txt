Coldplay - Miracles (Someone Special) (feat. Big Sean)

Capo Casa 2

[Intro] A  E  B  C#m  E  A
        A  E  B  C#m  E  A

                 A          E    B
My father said, "never give up son"
                   C#m     E  A
Just look how good Cassius become
               A       E   B
Muhammad, Mahatama and Nelson
                       C#m E A
Not scared to be strong

(Man, what if they say I'm no good)

(What if they say, "get out of here kid, you got no future")
     A                     E            B
Now you could run and just say they're right
          C#m               E         A
No, I'll never be no one in my whole life
    A                         E            B
Or you could turn and see the way they're wrong

            C#m     E       A
And get to keep on dancing all life long

                 A          E    B
My father said, "never give up son"
                   C#m     E   A
Just look what Amelia and John done
            A           E   B
Oh Rosa, Theresa their war won
                       C#m E A
Not scared to be strong
     A                     E            B
Now you could run and just say they're right
          C#m               E         A
No, I'll never be no one in my whole life
    A                         E            B
Or you could turn and see the way they're wrong
            C#m     E       A
And get to keep on dancing all life long

                E
Yeah, you could be
    C#m   B
Someone special
        E  A
You've got bright in your brains and lightning in your veins
E                       B
You'll go higher then they've ever gone
          E
In you I see
    C#m  B
Someone special
       E   A
You've got fire in your eyes an'
                            E                       B
When you realize, you'll go further then we've ever gone

[Rap]

Just turn it on
   A                E              B
(I pay my intuition, I couldn't afford tuition)

(My funds was insufficient, it felt like I'm in prison)
C#m            E                         A
(Until I realized I had to set my mind free)

(I was trusting statistics more than I trust me)
A                E          B
(Get a degree, good job, 401k)

(But I'm trying to turn Ks to Ms, what does it take?)
C#m          E                 A
(And maybe I could be the new Ali of music, probably)
                      A               E               B
(Instead of doing it just as a hobby, like these boys told me to)

(I guess you either watch the show, or your show approved)
C#m               E                 A
(Prove it to them or you prove it to yourself)

(But honestly its better if you do it for yourself)

A             E                   B
(Never complacent 'til we hit the oasis)

(One life don't waste it, feel my heart races)
C#m            E
(Success I taste it, ah)
           A
(We on the verge, of gettin' every single thing that we deserve)

               E
Yeah you could be
    C#m  B
Someone special
        E   A
You've got fire in your eyes, I see heaven inside
           E                      B
You'll go further then we've ever gone
         E
In you I see
    C#m  B
Someone special
       E   A
You've got bright in your brains, you can break through the chains
          E                       B
You'll go higher then we've ever gone
               A      E      B
Just turn it on

( C#m  E  A )

          A    E   B
In you I see
           C#m    E   A
Someone special
            A                 E        B
Don't go to war with yourself
                                       C#m     E      A
Just turn, just turn, just turn it on
      A                 E     B
And you can't go wrong

[Final] C#m  E  A

----------------- Acordes -----------------
Capotraste na 2ª casa
A*  = X 0 2 2 2 0 - (*B na forma de A)
B*  = X 2 4 4 4 2 - (*C# na forma de B)
C#m*  = X 4 6 6 5 4 - (*D#m na forma de C#m)
E*  = 0 2 2 1 0 0 - (*F# na forma de E)
