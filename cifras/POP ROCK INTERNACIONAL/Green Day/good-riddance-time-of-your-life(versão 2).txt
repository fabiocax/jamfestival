Green Day - Good Riddance (Time Of Your Life)

[Introdução 2x] G  C  D

[Primeira Parte]

G
  Another turning point
   C                 D
A fork stuck in the road
G
  Time grabs you by the wrist
   C                  D
Directs you where to go


[Pré-refrão]

Em              D            C
   So make the best of this test
                G
And don't ask why
Em             D
   It's not a question

       C                 G
But a lesson learned in time


[Refrão]

      Em             G
It's something unpredictable
    Em               G
But in the end it's right
   Em
I hope you had
     D           (G   C  D) (2x)
The time of your life


[Segunda Parte]

G
  So take the photographs
      C                   D
And still frames in your mind
G
  Hang it on a shelf
    C                    D
Of good health and good time


[Pré-refrão]

Em             D
   Tattoos of memories
     C             G
And dead skin on trial
Em                D
   For what it's worth
        C              G
It was worth all the while


[Refrão]

      Em             G
It's something unpredictable
    Em               G
But in the end it's right
   Em
I hope you had
     D           (G   C  D) (2x)
The time of your life


[Refrão Final]

      Em             G
It's something unpredictable
    Em               G
But in the end it's right
   Em
I hope you had
     D           (G   C  D) (2x)
The time of your life

      Em             G
It's something unpredictable
    Em               G
But in the end it's right
   Em
I hope you had
     D           (G   C  D) (2x)
The time of your life

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
