Green Day - Hold On

Intro 2x: G  C  G  C  G  C  D

       G            C                 G          D
As i stepped to the edge beyond the shadow of a doubt
 G       C          G  D
With my conscience beating
        G         C        G             D
Like a pulse of drum,that hammers on and on
         C          D       G
Until i reach the break of day
        G         C           G       D
As the sun beats down on the halfway house
 G      C          G  D
Has my conscience beating
   G           C        G           D
A sound in my ear, the will to persevere
      C          D       G
As i reach the break of day

                   Em         G
When you lost all hope and excuses

          D              G
And the cheapskates and loosers
           Em           G
Nothing's left to cling onto
            C   D               G   (Intro)
You got to hold on...on to yourself

    G     C       G       D
A cry of hope, a plea of peace
G       C          G  D
And my conscience beating
      G          C             G           D
It's not that i want, but it's all that i need
    C          D       G
To reach the break of day
      G         C                 G          D
So i run to the edge beyond the shadow of a doubt
 G       C           G  D
With my conscience bleeding
 G            C              G                D
Here lies a truth, the lost treassures of my youth
      C              D       G
As i hold on to the break of day

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
