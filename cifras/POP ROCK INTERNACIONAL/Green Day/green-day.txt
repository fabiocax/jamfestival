Green Day - Green Day

OBS: E' : X 7 9 9


    E                           
e|--------------0--------|      
B|----------------0------|      
G|--1--1-1-1h2------2-1--|      
D|--2--------------------|      
A|--2--------------------|      
E|--0--------------------|      


INTRO:  B Em D B / Ab Em E B - 2x / Ab Em E B A

VERSO 1:

E'          D
  A small cloud has fallen
      C#                  B
The white mist hits the ground

F#          A               E
  My lungs comfort me with joy
E'        D
  Vegging on one detail
     C#                B
The rest just crowds around
F#        A                E
  My E yes itch of burning red

REFRÃO:

Ab Em E       B
     Picture sounds
Ab Em E        B              A
     Of moving insects so surreal
Ab Em E    B
     Lay around
Ab Em E         B                  A
     Looks like I found something new

PONTE:  E ' D C# B / F# A E

VERSO 2:

E'       D
  Laying in my bed
    C#               B
I think I'm in left field
F#           A                     E
  I picture someone, I think it's you
E'                 D
  You're standing so damn close
    C#              B
My body begins to swell
F#         A        E
  Why does 1+1 make 2?

(REFRÃO)

(PONTE - 2x)


SOLO - 2x :
                                       F#      A        E  
e|-----------------------------------------------------------------0--------|
B|--0-0-0-0h2-0------------------------------------------------------0------|
G|--------------2-1--1-1h2-1-------------------2-2-2-2--1--1-1-1h2-----2-1--|
D|---------------------------4-2-4-----4-4-4-4-2-2-2-2--2-------------------|
A|-------------------------------------4-4-4-4-0-0-0-0--2-------------------|
E|-------------------------------------2-2-2-2----------0-------------------|

(VERSO 2)

(REFRÃO)

Termine Em:  E' D C# B      E4
e|------0---------0---------0---------0------|
B|----0---0-----0---0-----0---0-----0---0----|
G|--2-------2---------2---------2---------2--|
D|--2----------------------------------------|
A|--2----------------------------------------|
E|--0----------------------------------------|

----------------- Acordes -----------------
A = X 0 2 2 2 0
Ab = 4 3 1 1 1 4
B = X 2 4 4 4 2
C# = X 4 6 6 6 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
