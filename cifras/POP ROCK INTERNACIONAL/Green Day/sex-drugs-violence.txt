Green Day - Sex, Drugs & Violence

Intro: E5, A5 e B5
       E5, B5 e A5

Verso:
E5                                    B5           A5
All my life trouble follows me like a mystery girl
E5                                            B5           A5   B5
And I've been chasing round the memory like a mystery girl
E5                                           B5               A5
I've been getting lost searching my soul all around this town
E5                                         B5              A5   B5
I took a wrong turn in growing up and it's freaking me out

Pré-Refrão:
A5                      B5
Back at school it never made much sense
A5                  B5
Now I pay but I can pay attention
A5             B5                      C#5
Teaches me the hardest lessons of my life
A5              B5                                 C#5
These turned to dark, yes, but that's the way it goes

A5          B5
Sometimes I must regress these

Refrão:
E5           B5       A5
Sex, drugs & violence
E5              B5      A5   B5
English, math & science,
E5        B5      A5
Safety of numbers
E5               B5     A5   B5
Give me, give me danger

Pós-Refrão(Mike):
C#5                       A5
Well, I Don't wanna be an imbecil?
    E5                 B5
But Jesus made me that way

Verso:
E5                                 B5             A5
All my life I've been running wild like a runaway
E5                               B5     A5   B5
Where did that magic mean like a subway

Pré-Refrão

Refrão

Solo: E5, A5 e B5
      E5, B5 e A5
        2 vezes

Pré-Refrão

Refrão 3 vezes

Fim: E5

----------------- Acordes -----------------
A5 = X 0 2 2 X X
B5 = X 2 4 4 X X
C#5 = X 4 6 6 X X
E5 = 0 2 2 X X X
