Green Day - 2000 Light Years Away

Intro 4x: Bb5  G5  F5

  Bb5   F5
I sit alone in my bedroom
Bb5             F5
Staring at the walls
Bb5          F5
I've been up all damn night long
Bb5                 F5
My pulse is speeding, my love is yearning

G5                     Bb5
I hold my breath and close my eyes
   G5            F5
And dream about her
Eb5            F5              Bb5
Cause she's 2000 light years away

G5                Bb5             G5
She holds my mala kite so tight so
          F5
Never let go

Eb5          F5              Bb5
Cause she 2000 light years away

Intro (x4) Years away...

  Bb5     F5
I sit outside and watch the sunrise
Bb5                 F5
Lookout as far as I can
Bb5         F5
I can't see her, but in the distance
Bb5                 F5
I hear some laughter, we laugh together

G5                     Bb5
I hold my breath and close my eyes
   G5            F5
And dream about her
Eb5            F5              Bb5
Cause she's 2000 light years away

G5                Bb5             G5
She holds my mala kite so tight so
          F5
Never let go
Eb5          F5              Bb5
Cause she 2000 light years away

Intro (x4) Years away...

Bb5

(Bass only)

I sit alone in my bedroom
Staring at the walls
I've been up all damn night long
My pulse is speeding, my love is yearning

G5                     Bb5
I hold my breath and close my eyes
   G5            F5
And dream about her
Eb5            F5              Bb5
Cause she's 2000 light years away

G5                Bb5             G5
She holds my mala kite so tight so
          F5
Never let go
Eb5          F5              Bb5
Cause she 2000 light years away

Intro (x4) Years away...

Bb5

----------------- Acordes -----------------
Bb5 = X 1 3 3 X X
Eb5 = X 6 8 8 X X
F5 = 1 3 3 X X X
G5 = 3 5 5 X X X
