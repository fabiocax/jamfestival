Green Day - Horseshoes And Handgrenades

Intro/Verse (both guitars)

e|---------------------------------------------------------------------------|
B|---------------------------------------------------------------------------|
G|------7---9-9--------------------x6----------------------------------------|
D|---9--7---9-9-----7-7---4-4-4----------------------------------------------|
A|---9--5-/-7-7-----7-7---4-4-4----------------------------------------------|
E|---7----------0---5-5-0-2-2-2----------------------------------------------|

then

Guirar 1

e|---------------------------------------------------------------------------|
B|---------------------------------------------------------------------------|
G|---------------------------------------------------------------------------|
D|---------------------------------------------------------------------------|
A|---------------------------------------------------------------------------|
E|---77777777--55555555--44444444--33333-------------------------------------|

Guitar 2


e|-----------------------------------------------------------------------------|
B|-----------------------------------------------------------------------------|
G|---44444444--77777777--99999999--12-12-12-12-12------------------------------|
D|---xxxxxxxx--xxxxxxxx--xxxxxxxx--x--x--x--x--x-------------------------------|
A|---22222222--55555555--77777777--10-10-10-10-10------------------------------|
E|-----------------------------------------------------------------------------|

Verse (both guitars)

e|---------------------------------------------------------------------------|
B|---------------------------------------------------------------------------|
G|------7---9-9--------------------x8----------------------------------------|
D|---9--7---9-9-----7-7---4-4-4----------------------------------------------|
A|---9--5-/-7-7-----7-7---4-4-4----------------------------------------------|
E|---7----------0---5-5-0-2-2-2----------------------------------------------|

chorus (both guitars)

e|-------------------------------------9-9-9-9-9-9-9-9-9-9-9-9-9-------------|
B|---9-9-9-9-9-9-9-9-9-9-9-9-9---------9-9-9-9-9-9-9-9-9-9-9-9-9-------------|
G|---9-9-9-9-9-9-9-9-9-9-9-9-9---------8-8-8-8-8-8-8-8-8-8-8-8-8-------------|
D|---9-9-9-9-9-9-9-9-9-9-9-9-9--7-7-7--9-9-9-9-9-9-9-9-9-9-9-9-9---7-7-7-----|
A|---7-7-7-7-7-7-7-7-7-7-7-7-7--7-7-7--9-9-9-9-9-9-9-9-9-9-9-9-9---7-7-7-----|
E|------------------------------5-5-5--7-7-7-7-7-7-7-7-7-7-7-7-7---5-5-5-----|

e|---------------------------------------------------------------------------|
B|---9-9-9-9-9-9-9-9-9-9-9-9-9-9-9-9-----------------------------------------|
G|---9-9-9-9-9-9-9-9-9-9-9-9-9-9-9-9-----------------------------------------|
D|---9-9-9-9-9-9-9-9-9-9-9-9-9-9-9-9---4-4-4-4-4-4-4-4-4-4-4-4---------------|
A|---7-7-7-7-7-7-7-7-7-7-7-7-7-7-7-7---4-4-4-4-4-4-4-4-4-4-4-4---------------|
E|-------------------------------------2-2-2-2-2-2-2-2-2-2-2-2---------------|

Verse (both guitars)

e|---------------------------------------------------------------------------|
B|---------------------------------------------------------------------------|
G|------7---9-9--------------------x8----------------------------------------|
D|---9--7---9-9-----7-7---4-4-4----------------------------------------------|
A|---9--5-/-7-7-----7-7---4-4-4----------------------------------------------|
E|---7----------0---5-5-0-2-2-2----------------------------------------------|

chorus (both guitars)

e|-------------------------------------9-9-9-9-9-9-9-9-9-9-9-9-9-------------|
B|---9-9-9-9-9-9-9-9-9-9-9-9-9---------9-9-9-9-9-9-9-9-9-9-9-9-9-------------|
G|---9-9-9-9-9-9-9-9-9-9-9-9-9---------8-8-8-8-8-8-8-8-8-8-8-8-8-------------|
D|---9-9-9-9-9-9-9-9-9-9-9-9-9--7-7-7--9-9-9-9-9-9-9-9-9-9-9-9-9---7-7-7-----|
A|---7-7-7-7-7-7-7-7-7-7-7-7-7--7-7-7--9-9-9-9-9-9-9-9-9-9-9-9-9---7-7-7-----|
E|------------------------------5-5-5--7-7-7-7-7-7-7-7-7-7-7-7-7---5-5-5-----|

e|---------------------------------------------------------------------------|
B|---9-9-9-9-9-9-9-9-9-9-9-9-9-9-9-9-----------------------------------------|
G|---9-9-9-9-9-9-9-9-9-9-9-9-9-9-9-9-----------------------------------------|
D|---9-9-9-9-9-9-9-9-9-9-9-9-9-9-9-9---4-4-4-4-4-4-4-4-4-4-4-4-4-4-4-4-------|
A|---7-7-7-7-7-7-7-7-7-7-7-7-7-7-7-7---4-4-4-4-4-4-4-4-4-4-4-4-4-4-4-4-------|
E|-------------------------------------2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-2-------|

bridge

e|---------------------------------------------------------------------------|
B|---------------------------------------------------------------------------|
G|---------------------------------------------------------------------------|
D|---9-9-9--7---9-9-9--7---9-9-9--7---9-9-9--7----\--------------------------|
A|---9-9-9--7---9-9-9--7---9-9-9--7---9-9-9--7----\--------------------------|
E|---7-7-7--5---7-7-7--5---7-7-7--5---7-7-7--5----\--------------------------|

intro/verse (both guitars)

e|---------------------------------------------------------------------------|
B|---------------------------------------------------------------------------|
G|------7---9-9--------------------x4----------------------------------------|
D|---9--7---9-9-----7-7---4-4-4----------------------------------------------|
A|---9--5-/-7-7-----7-7---4-4-4----------------------------------------------|
E|---7----------0---5-5-0-2-2-2----------------------------------------------|

then

Guirar 1

e|---------------------------------------------------------------------------|
B|-----------------------------------------------x3--------------------------|
G|---------------------------------------------------------------------------|
D|---------------------------------------------------------------------------|
A|---------------------------------------------------------------------------|
E|---77777777--55555555--44444444--33333333----------------------------------|

Guitar 2

e|-----------------------------------------------------------------------------|
B|---------------------------------------------------------x3------------------|
G|---44444444--77777777--99999999--12-12-12-12-12-12-12------------------------|
D|---xxxxxxxx--xxxxxxxx--xxxxxxxx--x--x--x--x--x--x--x-------------------------|
A|---22222222--55555555--77777777--10-10-10-10-10-10-10------------------------|
E|-----------------------------------------------------------------------------|


Guirar 1

e|---------------------------------------------------------------------------|
B|---------------------------------------------------------------------------|
G|---------------------------------------------------------------------------|
D|---------------------------------5-5-5-------------------------------------|
A|---------------------------------5-5-5-------------------------------------|
E|---77777777--55555555--44444444--3-3-3-------------------------------------|

Guitar 2

e|-----------------------------------------------------------------------------|
B|-----------------------------------------------------------------------------|
G|---44444444--77777777--99999999----------------------------------------------|
D|---xxxxxxxx--xxxxxxxx--xxxxxxxx--5-5-5---------------------------------------|
A|---22222222--55555555--77777777--5-5-5---------------------------------------|
E|---------------------------------3-3-3---------------------------------------|

verse (both guitars)

e|---------------------------------------------------------------------------|
B|---------------------------------------------------------------------------|
G|------7---9-9--------------------x6----------------------------------------|
D|---9--7---9-9-----7-7---4-4-4----------------------------------------------|
A|---9--5---7-7-----7-7---4-4-4----------------------------------------------|
E|---7----------0---5-5-0-2-2-2----------------------------------------------|

Last Chorus (both guitars)

e|---------------------------------------------------------------------------|
B|---------------------------------------------------------------------------|
G|------7--9-9------------9-9-----------7--9-9-----------9-9------x2---------|
D|---9--7--9-9---7--9-9---9-9---7--9----7--9-9--7--9-9---9-9-----------------|
A|---9--5--7-7---7--9-9---7-7---7--9----5--7-7--7--9-9---7-7-----------------|
E|---7-----------5--7-7---------5--7------------5--7-7-----------------------|

e|---------------------------------------------------------------------------|
B|---------------------------------------------------------------------------|
G|---------------------------------------------------------------------------|
D|---9-9-9--7----------------------------------------------------------------|
A|---9-9-9--7----------------------------------------------------------------|
E|---7-7-7--5----------------------------------------------------------------|


| /  slide up
| \  slide down
| h  hammer-on
| p  pull-off
| ~  vibrato
| +  harmonic
| x  Mute note

----------------- Acordes -----------------
