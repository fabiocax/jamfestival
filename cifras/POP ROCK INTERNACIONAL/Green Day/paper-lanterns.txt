Green Day - Paper Lanterns

Intro 4x: B5  A5  E5  A5

B5             F#5                E5            B5
Now i rest my head from such an endless dreary time
B5                  A5              E5            B5
A time of hopes and happiness that had you on my mind
B5                       F#5              E5              B5
Those days are gone and now it seems as if i'll get some rest
B5                     A5                   E5               B5
But now and then i'll see you again and it puts my heart to test

   E5               F#5               B5
So when are all my problems going to end?
    E5             F#5            B5  G#5  E5           A5              E5
I'm understanding now that we are only friends, to this day i'm asking why

I still think about you

( B5  A5  E5  A5 )  (2x)

B5             F#5         E5             B5
As the days go on I wonder will this ever end?

B5               A5              E                    B5
I find it hard to keep control when you're with your boyfriend
B5               F#5          E5                B5
I do don't mind if all I am is just a friend to you
B5                 A5                      E5             B5
But all I want to know right now is if you think about me too

   E5               F#5              B5
So when are all my problems going to end?
    E5            F#5             B5  G#5  E5            A5             E5
I'm understanding now that we are only friends, to this day i'm asking why

I still think about you

----------------- Acordes -----------------
A5 = X 0 2 2 X X
B5 = X 2 4 4 X X
E = 0 2 2 1 0 0
E5 = 0 2 2 X X X
F#5 = 2 4 4 X X X
G#5 = 4 6 6 X X X
