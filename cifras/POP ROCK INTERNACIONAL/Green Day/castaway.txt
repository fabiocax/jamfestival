Green Day - Castaway

Intro: (B G# E) 4x

e|----x---x---x-x-----------x---x---x-x---------|
B|----x---x---x-x-----------x---x---x-x---------|
G|--4-x-4-x-4-x-x-----------x---x---x-x---------|
D|--4-x-4-x-4-x-x--6-6-6--2-x-2-x-2-x-x--4-4-4--|
A|--2-x-2-x-2-x-x--6-6-6--2-x-2-x-2-x-x--2-2-1--|
E|----x---x---x-x--4-4-4--0-x-0-x-0-x-x---------|

Verso:

e|----------------------------------|
B|----------------------------------|
G|--4-4-4-4-4-----------------------|
D|--4-4-4-4-4--4-4--2-2-2--4-4-4----|
A|--2-2-2-2-2--4-4--2-2-2--2-2-1----|
E|-------------2-2--0-0-0-----------|

Riff 1

e|----x---x---x-x------x--|
B|----x---x---x-x------x--|
G|----x---x---x-x------x--|
D|--4-x-4-x-4-x-x-4--4-x--|
A|--2-x-2-x-2-x-x-1-/2-x--|
E|----x---x---x-x------x--|


B
I'm on a sentimental journey
F#               E
Into sight and sound
B                     F#             E
Of no return and no looking back or down
B
A conscientious objector to the
F#               E
War that's in my mind
B
Leaving in the lurch and I'm
F#                  E
Taking back what's mine

E
I'm on a mission
                    Riff 1 (2x)
Into destination unknown
E
An expedition
                  F#
Onto desolation road
Where I'm a...

B           G#           E
Castaway - going at it alone
B           G#             E
Castaway - now I'm on my own
B          G#             E
Castaway - going at it alone
B           G#             E
Castaway - now I'm on my own
E               F#
Lost and found, truble bound
      B
Castaway

Intro

B
I'm riding on the night train and
F#             E
Driving stolen cars
B                F#             E
Testing my nerves out on the blvd.
B
Spontaneous combustion in the
F#             E
Corners of my mind
B
Leaving in the lurch
F#             E
And I'm taking back what's mine

E
I'm on a mission
                    Riff 1 (2x)
Into destination unknown
E
An expedition
                  F#
Onto desolation road
Where I'm a...

B           G#             E
Castaway - going at it alone
B           G#             E
Castaway - now I'm on my own
B           G#             E
Castaway - going at it alone
B           G#             E
Castaway - now I'm on my own
E               F#
Lost and found, truble bound
      B
Castaway

Intro

Solo (B, F#, E)

e|-------------------------5p2---5p2---5p2---5p2--------|
B|-----------------------------2-----2-----2-----2-5b7--|
G|--2-/-4-4--4-------4--4-------------------------------|
D|--------------4h6-------------------------------------|
A|------------------------------------------------------|
E|------------------------------------------------------|


:Só bateria e baixo
:
: B
: I'm on a sentimental jurney
: F#             E
: Into sight and sound
: B                     F#             E
: Of no return and no looking back or down
: B
: A conscientious objector to the
: F#             E
: War that's in my mind
: B
: Leaving in the lurch and I'm
: F#             E
: Taking back what's mine

E
I'm on a mission
                    Riff 1 (2x)
Into destination unknown
E
An expedition
                  F#
Onto desolation road
Where I'm a...

B           G#             E
Castaway - going at it alone
B           G#             E
Castaway - now I'm on my own
B           G#             E
Castaway - going at it alone
B          G#             E
Castaway - now I'm on my own
E               F#
Lost and found, truble bound
      B G# E
Castaway
      B G# E
Castaway
      B G# E
Castaway
      B G# E
Castaway

----------------- Acordes -----------------
B = X 2 4 4 4 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
G# = 4 3 1 1 1 4
