Green Day - Don't Wanna Fall In Love

Intro: D#

D#
Don't want to have you hanging
Around me like a leech
I think your just a problem
So stay the hell away from me
Because I don't believe in you

    A#5                           D#
And I wanna sit here all my life alone
     Bb5                 A#5
This may sound a little rough
                           D#
Don't want to fall in love

D#
Don't need security
I ain't no dog without a bone
Don't have no time for love
So stay the fuck away from me

Because I don't believe in you

    Ab5                           D#
And I wanna sit here all my life alone
     Bb5                 Ab5
This may sound a little rough
                           D#
Don't want to fall in love


Solo: D#  Ab5  Bb5
Parte 1
      D#
E|--------------------------------------------|
B|--------------------------------------------|
G|--3-3---3-3-3-5-3-3-----3-3-3-3-3-5-3---3---|
D|--------------------5-----------------5-----|
A|--------------------------------------------|
E|--------------------------------------------|

E|-------------------------------------------------------|
B|-------------------------------------------------------|
G|--3-3---3-5-3-3-----3-3-3-3-3-5-3---3-3-3-3-3-5-3---3--|
D|----------------5-----------------5---------------5----|
A|-------------------------------------------------------|
E|-------------------------------------------------------|
      Ab5
E|-------------------------------/14--13--11--10\-------|
B|------------4-7-6-4------------/13--12--10--10\-------|
G|----------5---------5-3-------------------------------|
D|---6-6-6----------------------------------------------|
A|------------------------------------------------------|
E|------------------------------------------------------|

     Bb5                 Ab5
This may sound a little fucked
                           D#
Don't want to fall in love

----------------- Acordes -----------------
A#5 = X 1 3 3 X X
Ab5 = 4 6 6 X X X
Bb5 = X 1 3 3 X X
D# = X 6 5 3 4 3
