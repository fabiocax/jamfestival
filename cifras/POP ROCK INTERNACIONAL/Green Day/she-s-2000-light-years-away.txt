Green Day - She's 2000 Light Years Away

C          G
I sit alone in my bedroom
C          G
Staring at the walls
C          G
I've been up all damn night long
C          G
My pulse is speeding, my love is yearning

Am                   C
I hold my breath and close eyes
   Am           G
And dream about her
F             G               C
Cause she's 2000 light years away
F             G               C
She holds my mala kite so tight so
F             G               C
Never let go
F             G               C
Cause she 2000 light years away


F             G               C
I sit outside and watch the sunrise
F             G               C
Lookout as far as I can
F             G               C
I can't see her, but in the distance
F             G               C
I hear some laughter, we laugh together

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
