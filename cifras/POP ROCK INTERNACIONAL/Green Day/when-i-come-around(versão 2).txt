Green Day - When I Come Around

Afinação Meio Tom Abaixo

G     D                 Em     C
   I heard you crying loud
G           D          Em     C
  all the way across town
            G                   D
You've been searching for that someone
         Em              C
and it's me out on the prowl
    G             D          Em           C
As you sit around feeling sorry for yourself

G       D                 Em    C
  Well Don't get lonely now
G      D                Em     C
  And Dry your whining eyes
         G                D
I'm just roaming for the moment
         Em                     C
Sleazin' my back yard so don't get
G                D                    Em        C
so uptight you been thinking about ditching me


Am                         C
    No time to search the world around.
Am                                C
    Cause you know where I'll be found

    When I come around

( G  D  Em  C ) (2x)

G     D              Em     C
  I heard it all before
G      D                    Em     C
  So don't knock down my door
       G            D          Em            C
I'm a loser and a user so I don't need no accuser
    G                D               Em          C
to try and slag me down because I know you're right
G     D               Em    C
  So go do what you like
G        D              Em   C
  Make sure you do it wise
         G                   D
You may find out that your selfdoubt
        Em             C
means nothing was ever there
     G                 D
You can't go forcing something
         Em       C
if it's just not right

Am                         C
    No time to search the world around.
Am                                C
    Cause you know where I'll be found

    When I come around

( G  D  Em  C ) (4x)

Am                         C
    No time to search the world around.
Am                                C
    Cause you know where I'll be found

    When I come around

( G  D  Em  C )
When I come around (3x)

----------------- Acordes -----------------
Am*  = X 0 2 2 1 0 - (*G#m na forma de Am)
C*  = X 3 2 0 1 0 - (*B na forma de C)
D*  = X X 0 2 3 2 - (*C# na forma de D)
Em*  = 0 2 2 0 0 0 - (*D#m na forma de Em)
G*  = 3 2 0 0 0 3 - (*F# na forma de G)
