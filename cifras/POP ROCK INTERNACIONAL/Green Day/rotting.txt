Green Day - Rotting

Riff 1
e|-------12------------------------------------------------------------------
B|-13-15-----15-12-----------13-----15-17--------13--------13-----15-17--17~~
G|-----------------12-----13-----12--------10-12-----10-12-----12------------
D|---------------------15----------------------------------------------------
A|---------------------------------------------------------------------------
E|---------------------------------------------------------------------------

C                 G
I'm rotting inside,
        F               C
My flesh turns to dust,
F                                  C
Whisper are you dying in my ear.
C (com Riff 1)   G
I'm so sick of death,
F               C
Tumors in my head,
F                                  C
Whisper are you dying in my ear.



G#              C
Red rose of death.
G#              C
In my fist I clutch.
G#                  C
Thorns shred my fingertips.
F                       G
And drips of toxic blood.


C                    G
Kiss me one last time,
F               C
Cloth off my sweat,
F                                  C
Whisper are you dying in my ear.
C (com Riff 1)    G
As my bones they rust,
        F                 C
With every ounce of trust,
F                                  C
Whisper are you dying in my ear.


G#              C
Red rose of death.
G#              C
In my fist I clutch.
G#                  C
Thorns shred my fingertips.
F                       G
And drips of toxic blood.


C (pm)            G (pm)
I'm rotting inside,
        F (pm)          C (pm)
My flesh turns to dust,
F(pm)                              C (pm)
Whisper are you dying in my ear.
C (com Riff 1)    G
Kiss me one last time,
F               C
Cloth off my sweat,
F                                  C
Whisper are you dying in my ear.
F                                  C
Whisper are you dying in my ear

        G          C
e|---------------
B|-----------13--
G|---12--12------
D|---------------
A|---------------
E|---------------

----------------- Acordes -----------------
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
