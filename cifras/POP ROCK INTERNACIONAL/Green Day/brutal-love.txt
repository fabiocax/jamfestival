Green Day - Brutal Love

Intro: Ab x4

 Ab
E|------------------------------
B|------------------------------
G|---------5--------------------
D|-------6---6------------------
A|-----6-------6----------------
E|---4--------------------------

 Eb
E|------------------------------ O E|--------------------------
B|------------------------------   B|--------8-----------------
G|----------12------------------   G|------8---8---------------
D|-------13---13----------------   D|----8-------8-------------
A|----13---------13-------------   A|--6-----------------------
E|--11--------------------------   E|--------------------------

E|------------------------------ O  E|-------------------------
B|------------------------------    B|--------6----------------
G|-----------10-----------------    G|------6---6--------------
D|--------11----11--------------    D|----6-------6------------
A|-----11-----------11----------    A|--4----------------------
E|--9---------------------------    E|-------------------------


              Ab
Turn out the lights
           Eb
Close your eyes
            Ab
Turn up the silence
                       Db
The heartache of your life

         Ab
Dance forever
            Eb
Under the lights
             Ab
This brutal love

            Ab
Oh how you want it
               Eb
You're begging for it
               Ab
But you can't have it
            Db
Even if you try

              Ab
It's in the clutches
        Eb
In the hands of
             Ab
This brutal love

Ab
Old toys
              Eb
This plastic card
            Ab
Loners and fools
                Db
Are tearing me apart

             Ab
Here comes trouble
        Eb
The uninvited
             Ab
This brutal love

Ab
Danger
              Eb
Not quite at home
                Ab
The eyes of temptation
                 Db
The flesh of my bones

         Ab
Hello stranger
         Eb
I'm a disaster
             Ab
This brutal love

Db
Bad luck
             Ab
Bitters and soda
             Eb
Anguish and shame
            Ab
The modern fool

Db
Bad sex
          Ab          Fm
Buy me a train-wreck
       Db
Just something for my troubled
Eb
Mind

Solo acompanhamento:
Ab Cm Db E
Ab Cm Db Eb A
Ab Cm Db E
Ab Cm Db Eb

Solo:

E|--------------------------------------------------------------------------|
B|--------------------------------------------------------------------------|
G|-1~\--10-8---8-10-8~-10-8--1~----1~-5~-0~--0-1~-3~--3-1-3~----------------|
D|--------------------------------------------------------------------8//---|   2x
A|--------------------------------------------------------------------8//---|
E|--------------------------------------------------------------------6//---|


Db
Bad luck
             Ab
Bitters and soda
             Eb
Anguish and shame
            Ab
The modern fool

     Db
Bad love
              Ab     Fm
Kiss me I'm loaded
Db
Something for my troubled
Eb     E
Mind

A    C#m
Drop out
D             F
Drop-dead hideous
A   C#m
How low
    D           E    Bb
Is this brutal love

A    C#m
Drop out
D             F
Drop-dead hideous
A   C#m
How low
    D           E    Bb
Is this brutal love

A    C#m
Drop out
D             F
Drop-dead hideous
A   C#m
How low
    D           E    Bb
Is this brutal love


A    C#m
Drop out
D             F
Drop-dead hideous
A   C#m
How low
    D           E
Is this brutal love
             A
This brutal love

----------------- Acordes -----------------
A = X 0 2 2 2 0
Ab = 4 3 1 1 1 4
Bb = X 1 3 3 3 1
C#m = X 4 6 6 5 4
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
Db = X 4 6 6 6 4
E = 0 2 2 1 0 0
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
