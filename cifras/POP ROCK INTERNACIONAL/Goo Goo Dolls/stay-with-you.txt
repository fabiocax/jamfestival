Goo Goo Dolls - Stay With You

Intro: (C   G   Am   F) 2x
        Am  G   C

Verse 1 :

  e|------3---3--|------7----------|-------7--------|
  B|----1---1----|----8---8--10----|-----8---8------|
  G|--2----------|--9--------------|---9-------7----|
  D|-------------|-----------------|----------------|
  A|-------------|-----------------|----------------|
  E|-------------|-----------------|----------------|

            Am7
  And these streets
  *       F
  Turn me inside out
  $    Am7
  Everything shines
      &         G
  But leaves me empty still


     Am7   *            F
  And I, I'll burn this lonely house down
 $       Am7
  If you run with me
 &       G
  If you run with me

Chorus : (chords played in (5) form)

  C    G         Am     F
  I'll stay with you
      C                 G          Am     F
  The walls will fall before we do
     C
  So take my hand now
        Baug
  We'll run forever
  Am             F
  I can feel the storm inside you
  Am   G         F
  I'll stay with you

Verse 2 :

      Am7    *     F
  And fooled by my own desire
  $       Am7
  I twist my fate
  &       G
  Just to feel you
      Am7  *           F
  But you, you turn me toward the light
 $           Am7
  And you're one with me
 &         G
  Will you run with me?

Chorus

Prelude :

  Em          F
  Now come in from this storm
      Em          F
  And I taste you sweet and warm
                Am
  Take what you need
                F
  Take what you need
       C       C
  From me

  F
  Now wake up this world
  F
  Wake up tonight
      F
  And run to me
  F         C    G    Am    F
  Run to me now

  C  G  Am  F

Chorus :

  C    G         Am     F
  I'll stay with you
  C                 G          Am     F
  Walls will fall before we do
     C
  So take my hand now
        Baug
  We'll run forever
  Am             F
  I can feel the storm inside you
  Am   G         F
  I'll stay with you
      Am   G         F
  And I'll stay with you

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
