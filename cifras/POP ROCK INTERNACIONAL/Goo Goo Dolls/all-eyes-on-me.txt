Goo Goo Dolls - All Eyes On Me

Tuning: EADGCe

Chords:
C - x35500
G - 355000
Am7 - 577000
F9 - 133000
B* - x24400 (actually a B4/min9, I believe)
E/C - 022000

Intro:
e|----0----0-3--0----0-|----0----0------0----------
C|-4----2----0----4----|-4----2----0--0---0-----0--
G|-2----2----2----2----|-2----2----2--------4h5----
D|---------------------|---------------------------
A|---------------------|---------------------------
E|---------------------|---------------------------
(play 2x)


Verse:
e|----0----0-3--0----0-
C|-4----2----0----4----
G|-2----2----2----2----
D|--------------------- 
A|--------------------- 
E|--------------------- 
   Daylight burns your sleepy eyes

e|----0----0------0----------
C|-4----2----0--0---0-----0--
G|-2----2----2--------4h5----
D|---------------------------
A|---------------------------
E|---------------------------
    It's hard to see you dreaming

e|----0----0-3--0----0-
C|-4----2----0----4----
G|-2----2----2----2----
D|--------------------- 
A|--------------------- 
E|--------------------- 
   You hide inside yourself

e|----0----0------0----------
C|-4----2----0--0---0-----0--
G|-2----2----2--------4h5----
D|---------------------------
A|---------------------------
E|--------------------------- 
   I wondered what you're thinkin'

e|----0----0-3--0----0-
C|-4----2----0----4----
G|-2----2----2----2----
D|--------------------- 
A|--------------------- 
E|--------------------- 
   And everything you're chasing

e|----0----0------0----------
C|-4----2----0--0---0-----0--
G|-2----2----2--------4h5----
D|---------------------------
A|---------------------------
E|--------------------------- 
   It seems to leave you empty

Chorus: (the C is accompanied by G|-----10--9---7---9--- )
C                 E/C     F9
And it won't take long to burn
All eyes on me
C           E/C                 F9
Through the nothing that you've learned
All eyes on me
C
        E/C                  F9
And the things you choose to be
G           C   Am7
All eyes on me  (on "me" start rhythm figure 1, at end)
    G              F9
But your eyes look away
     C          Am7
It's so hard to be someone
G                 F9
Strung out from today
    Am7          B*         C
And all that you knew slips away

(repeat intro)

(verse)
You drown in deeper oceans
Inventing new religions
They smile and stab my back and
I lie and have to laugh

(chorus)
And it won't take long to burn
All eyes on me
Through the nothing that you've learned
All eyes on me
And the things you choose to be
All eyes on me

But your eyes look away
It's so hard to be someone
Strung out from today
And all that you knew slips away

(repeat intro)

Bridge:
G-F9
     And you hid in your room
Am7-G
     And the light burns away
G-F9
     And you move from the truth
Am7-G
     It's all so far from you

Solo:
G-Am7-F9-C (6 times)

Bridge2:
e|-----0---0---------0-----0---
C|---0-------0-----0-----0---0-
G|-------------0---------------
D|-----------------------------
A|---------------------3-------
E|-3-----5-------1-------------
(play 3 times)

Outro:
G - Am7 - F9 - C (7 times)

================

Rhythm Figure 1:
e|-----0---------0---------0---------0-----
C|---0---0-----0---0-----0---0-----0---0---
G|---------0---------0---------0---------0-
D|-----------------------------------------
A|-3---------------------------------------
E|-----------5---------3---------1---------

e|-----0---------0---------0---------0-----
C|---0---0-----0---0-----0---0-----0---0---
G|---------0---------0---------0---------0-
D|-----------------------------------------
A|-3---------------------------------------
E|-----------5---------3---------1---------

e|-----0---------0---------0-----0-0-0-0-0-0-0-
C|---0---0-----0---0-----0---0---0-0-0-0-0-0-0-
G|---------2---------0---------0-5-5-5-5-5-5-5-
D|-------------------------------5-5-5-5-5-5-5-
A|-0---------2---------3---------3-3-3-3-3-3-3-
E|---------------------------------------------

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
B = X 2 4 4 4 2
C = X 3 2 0 1 0
E/C = X 3 2 1 0 0
F9 = 1 3 5 2 1 1
G = 3 2 0 0 0 3
