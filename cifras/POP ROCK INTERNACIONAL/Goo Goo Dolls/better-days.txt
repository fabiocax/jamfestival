Goo Goo Dolls - Better Days

        D
And you ask me what I want this year
          G
And I try to make this kind and clear
           A                                D
Just a chance that maybe we'll find better days
          D
Cuz I don't need boxes wrapped in strings
             G
And desire and love and empty things
           A                                 D
Just a chance that maybe we'll find better days

     G
So take these words
        A
And sing out loud
        G                  A
Cuz everyone is forgiven now
        Bm                   G             D
Cuz tonight's the night the world begins again


              D
And it's someplace simple where we could live
        G
And something only you can give
  A                                              D
And thats faith and trust and peace while we're alive
             D
And the one poor child that saved this world
        G
And there's 10 million more who probably could
      A                                      D
If we all just stopped and said a prayer for them

     G
So take these words
        A
And sing out loud
        G                   A
Cuz everyone is forgiven now
        Bm                   G            D
Cuz tonight's the night the world begins again

(Base:  D G A D )

           D
I wish everyone was loved tonight
       G
And somehow stop this fight
         A                                  D
Just a chance that maybe we'll find better days

     G
So take these words
        A
And sing out loud
         G               A
Cuz everyone is forgiven now
       Bm                   A             D
Cuz tonight's the night the world begins again
       Bm                   A             D
Cuz tonight's the night the world begins again

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
G = 3 2 0 0 0 3
