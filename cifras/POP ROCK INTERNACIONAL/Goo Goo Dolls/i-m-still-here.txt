Goo Goo Dolls - I´m Still Here

Capo Casa 2

G        C              G
I am a question to the world
        C             G
Not an answer to be heard
       C            Em7            C
Or a moment, that's held in your arms

     G           C                G
And what, do ya think you'd ever say?
           C     G
I won't listen anyway
            C
You don't know me
      Em       D           C          G C G C
And I'll never be what you want me to be


[verse 2]

     G             C                 G
And what, do you think you'd understand?

       C              G
I'm a boy, no, I'm a man
            C           Em7       C
You can't take me, and throw me away

     G              C                 G
And how, can you learn what's never shown?
          C                   G
Yeah you stand here on your own
             C
They don't know me,
       Em  D    C             G C G C
cause I'm not here


[chorus]

    G        C             G
And I want a moment to be real
       C                     G
Wanna touch things I don't feel
        C           Em7    C
Wanna hold on, and feel I belong
     G           C                 G
And how, can the world want me to change?
             C                  G
They're the ones that stay the same
            C            Em  D    C
hey don't know me, cause I'm not here


[verse 3]

     G             C                 G
And you, see the things they never see
         C               G
All you wanted, I could be
          C           Em7       C
Now you know me, and I'm not afraid

    G        C             G
And I, wanna tell you who I am
         C            G
Can you help me be a man?
            C
They can't break me
   Em         D         C             G C G C
As long as I know who I am


[chorus]

    G        C             G
And I want a moment to be real
       C                     G
Wanna touch things I don't feel
        C           Em7    C
Wanna hold on, and feel I belong
     G           C                 G
And how, can the world want me to change?
             C                  G
They're the ones that stay the same
            C            Em  D    C
They can't see me, But I'm still here


[Bridge]

C          D              Em
They can't tell me who to be

  (*-played after the C)
E|-----------------|
B|-----------------|
G|-----------------|
D|-----------------|
A|--3-2----3-2-----|
E|-------3------3--|

 C       D             Em
Cuz I'm not what they see
           C                           D                      Em
Yeah the world is still sleeping While I keep on dreaming, for me
        C                        D                    Em D C
And it wasn't just whispers and lies That I'll never believe


[chorus]

    G        C             G
And I want a moment to be real
       C                     G
Wanna touch things I don't feel
        C           Em7    C
Wanna hold on, and feel I belong
     G           C                 G
And how, can they say I'll never change?
             C                  G
They're the ones that stay the same
         C               Em  D    C
I'm the one now, cause I'm still here


[Outro]
        G           Em   D    G
I'm the one, cause I'm still here
  Em  D     G
I'm still here
  Em  D    G
I'm still here
  Em  D    C
I'm still here

----------------- Acordes -----------------
Capotraste na 2ª casa
C*  = X 3 2 0 1 0 - (*D na forma de C)
D*  = X X 0 2 3 2 - (*E na forma de D)
Em*  = 0 2 2 0 0 0 - (*F#m na forma de Em)
Em7*  = 0 2 2 0 3 0 - (*F#m7 na forma de Em7)
G*  = 3 2 0 0 0 3 - (*A na forma de G)
