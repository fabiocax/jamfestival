Goo Goo Dolls - Flat Top

Intro / Chorus (riff 1):

---------0-------------------0-------------------------------
---------0-------------------0-------------------------------
--4--------------2-2--2--2---2--------1---11-11-11\8-8-8\4-4-
--4--------------2-2--2--2---2--------2----9--9--9\6-6-6\2-2-    
--2--------------0-0--0--0---0--------2----------------------
--------------------------------------0----------------------

---------0-------------------0----------------0-0-0-0-0-
---------0-------------------0----------------0-0-0-0-0-
--4------0-------2-2--2--2---2----0-----------1-1-1-1-1-
--4--------------2-2--2--2---2----0---2---2---2-2-2-2-2-    
--2--------------0-0--0--0---0----0---2---2---2-2-2-2-2-
--------------------------------------0---0---0-0-0-0-0-


Verse:
 Flat top intervention, bringing home the new invention.
-0-------------0--------0-----------------0--------------------
-0-------------0--------0-----------------0--------------------
-3-------------4--------9-----------------8--------------------
-4-------------6--------9-----------------9--------------------     
-4-------------6--------7-----------------9--------------------
------------------------------------------7--------------------

          See it there in pieces on the ground.
---------0---------------0-------------0-----------------------
---------0---------------0-------------0-----------------------
---------4---------------3-------------1-----------------------
---------6---------------4-------------2-----------------------     
---------6---------------4-------------2-----------------------
---------------------------------------0-----------------------

A television war between the cynics and the saints
Flip the dial and that's whose side you're on

Ah, sleeping on the White House lawn ain't never changed a thing

-0---------------0-----------------0-----------------
-0---------------0-----------------7-----------------
-0---------------3-----------------7----------------
-6---------------3-----------------4-----------------     
-6---------------1-----------------0-----------------
-4---------------x-----------------------------------
Look at all the washed out Hippie dreams

(Riff 1)
And it's fallin' all around us
Is this some kind of joke they're trying to pull on us?
Fallin' all around us
I'll turn my head off for a while

(Verse)
The tabloid generation's lost
Choking on it's fear
Used to be that's all we had to fear

And conscience keeps us quiet while the crooked love to speak
There's knowledge wrapped in blankets on the street.

A visionary coward says that anger can be power
As long as there's a victim on TV

(Riff 1)
And it's fallin' all around us
Is this some kind of joke they're trying to pull on us?
Fallin' all around us
I'll turn my head off for a while

 And my dirty dreams all come alive on my TV
-0------0----------------0----------0---------------
-0------0----------------0----------0---------------
-0------2----------------6----------4---------------
-6------2----------------6----------4---------------    
-6------0----------------4----------2---------------
-4--------------------------------------------------
screen
And assasination plots
Show me what I haven't got
Show me what I love, and who I'm s'posed to be
Show me everything I need

Show it all to me 
------------------
------------------
------------------
--------4------2--     
--------4------2--
--------2------0--
Show it all to me

(solo)

(Riff 1)
And it's fallin' all around us
Is this some kind of joke they're trying to pull on us?
Fallin' all around us
I'll turn my head off for a while

----------------- Acordes -----------------
