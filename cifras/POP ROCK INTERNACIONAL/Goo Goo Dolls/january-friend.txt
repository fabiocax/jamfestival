Goo Goo Dolls - January Friend

Intro: (D  G  D  G   D  G   C  A)

E
Reach
            C
For higher ground
           G
About the way you look
                        D     C#
The way you scream out loud
E
Mine
              C
Just like the last time
             G
It's all the same to me
       A
She said...
       D
Let's pretend
  G          D
My January friend

 G                 D
I'm wantin' you again
  G
I wanna touch you
C             A
Every single heart that beats
   D
Pretend
  G          D
My January friend
 G                 D
I'm wantin' you again
  G
I wanna touch you
C               A
Every single heart that beats
E
Cry
               C
Don't cry out loud
            G
You've gotta bear your cross
                        D     C#
But never dream too loud and you're
E
Tied
             C
Tied to the next time
  G                A
You realize your crimes...
         D
Let's pretend
   G          D
My January friend
 G                 D
I'm wantin' you again
  G
I wanna touch you
C            A
Every single heart that beats
    D
Pretend
    G          D
My January friend
 G                 D
I'm wantin' you again
  G
I wanna touch you
C            A
Every single heart that beats
E           C                    G   D  C#
Time stood still Monday morning yeah
E                      C
Showed me what I had to see
G                   A
It's not the way I thought it should be
    E   C   G   D C#  E   C   G   A
Oh yeah..
         D          G
You're my January friend
        D          G
You're my January friend
        D          G
You're my January friend
   C                     A
And every heart that beats pretend
  D          G
My January friend
           D          G
You're my January friend
          D          G
You're my January friend
   C                     A
And every heart that beats tonight
    D
Pretend

----------------- Acordes -----------------
A = X 0 2 2 2 0
C = X 3 2 0 1 0
C# = X 4 6 6 6 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
