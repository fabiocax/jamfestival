Goo Goo Dolls - Home

Verse 1:
G#m                          E
Clouded rooms full of empty faces
G#m                           E
Deepest conversation full of life
G#m
Another night with all my friends
E
The kind you never see again
G#m                                  E
I wonder if they'll see through my disguise


Pre-Chorus:
              F#                     G#m
And I want to say that I can't hold back
                 F#                    G#m
And it might be wrong, but it's all I have


Chorus:

             B E     F#
Come take me home tonight
             B E  F#
Come take me home
                  G#m  E               F#
All I need is you now  I'm lost without you
           G#m    E              F#
A million miles   But I will find you
             G#m E  G#m E
So take me home


Verse 2:
     G#m                         E
It's 3 AM and I can't sleep without you
   G#m                                 S
I think I've found my perfect words to say
     G#m
The satellite transmits my voice
     E
But sometimes we don't have a choice
   G#m                            E
I wake you up from half a world away


Pre-Chorus:
                F#                    G#m
And I tried so hard, tried to be so strong
                 F#                   G#m
But you see the cracks, my defenses gone


Chorus:
             B E     F#
Come take me home tonight
             B E  F#
Come take me home
                  G#m  E               F#
All I need is you now  I'm lost without you
           G#m    E              F#
A million miles   But I will find you

Instrumental Interlude:

G  Am  F#  G

G  Am  B (hold out)


Chorus:
             B  E    F#
Come take me home tonight
             B  E  F#
Come take me home
             B  E    F#
Come take me home tonight
             B  E  F#
Come take me home
B                E                  F#
  Satellite transmits my voice but sometimes we don't have a choice
B                E              F#
Wake you up from half a world away
           G#m  E          F#
I need you now, lost without you
            G#m E           F#
I'm holding on til I can find you
            B
So take me home

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
G#m = 4 6 6 4 4 4
