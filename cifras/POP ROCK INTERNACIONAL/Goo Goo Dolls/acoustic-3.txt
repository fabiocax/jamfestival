Goo Goo Dolls - Acoustic # 3

Riff 1                Riff 2                              Riff 3
e -----------------|-----------------|-----------------|-----------------|
B -----------------|-----3-----3-----|-----------------|-----------------|
G -----0-----0-----|---2-----0---0---|-----0-----0-----|-----------0-----|
D ---------0---0---|-0-----2-------2-|---0-------------|-----0---0---0---|
A ---2---3-------0-|-----------------|-3-------2---2---|---2---3-------0-|
E -3---------------|-----------------|-------3-------3-|-3---------------|


Intro:   Riff 1 ( 3x )  Riff 3

Verso 1

They painted up your secrets                    Riff 3 - 2x
With the lies they told you                     Riff 3 e Riff 1
And the least they ever gave you                Riff 3 - 2x
Was the most you ever knew                      Riff 3 e Riff 1

Refrão 1

And I wonder where these dreams go              Riff 2

When the world gets in your way                 Riff 2
What's the point in all this screaming          Riff 2
No one's listening anyway                       Riff 1 - 2x

Riff 1 - 2x

Verso 2

Your voice is small and fading                  Riff 1 - 2x
And you hide in here unknown                    Riff 1 - 2x
And your mother loves your father               Riff 1 - 2x
'Cause she's got nowhere to go                  Riff 1 - 2x

Refrão 2 ( é como o refrão 1 )

And she wonders where these dreams go
'Cause the world got in her way
What's the point ever trying
Nothing changing anyway

Verso 3

They press their lips against you               Riff 1 - 2x
And you love the lies they say                  Riff 1 - 2x
And I tried so hard to reach you                Riff 1 - 2x
But your falling anyway                         Riff 1 - Riff 3

Refrão 3  ( é como o refrão 1 )

And you know I see right through you
When the world gets in your way
What's the point in all the screaming
You're not listening anyway

Riff 1 - 2x  e termine em G
