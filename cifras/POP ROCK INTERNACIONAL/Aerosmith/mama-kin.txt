Aerosmith - Mama Kin

Introdução=(E B A E)2x E


   E              B
   It ain't easy livin' like a gypsy

    A                           E
   Tell you, honey, how it feels

  E
   I been dreaming floatin' down stream

         A                        E
   And losin' just what aal is real


     E                 B
   Whole lotta lover keepin' undercover

     A                          E
   Never knowin' where you've been, oh, yeah


                        B
   You've been fading, always operatin'

     A                       E
   Keepin' touch with mama kin

            A                          E
   You've always got your tale on the way

             A
   Shooting fire from your mouth

            E         F# G
   Just like a ray gun


   G#   A                     E
   You act like a perpetual drag

        F#
   You better check it out or someday soon

                                 B       G G# A A# B
   You're gonna climb back on the wag on


    E             B
   It ain't easy livin' like you wanna

       A                          E
   And so hard to find peace of mind, yes ,it is

                         B
   The way I see it you got to say shit

      A                            E F# G G#
   But don't forget to drop me a line


   A                      E
   Bald as an egg in a dream

         A                           E      F# G
   And workin' for your daddy's just a drag

   G#    A                         E
   You still don't know what it means

        F#
   You better check it out or someday soon

                                 B      G G# A A# B
   You're gonna climb back on the wag on


   E                          B
   Keepin' touch with mama kin

   E                                 B
   Tell her where you're gone and been

   E                      B
   Livin' out your fantasy

  E                             B
   Sleepin' late and smokin' tea

  E  A                 G    D  B
   Keepin' touch with mama kin

  E  A                    G       D   B
   Tell her where you're gone and been

  E  A              G    D B
   Livin' out your fantasy

  E   A                G      D C B A G F#m A G F#m
   Sleepin' late and suckin' me


   E               B
   It ain't easy livin' like you wanna

   A                              E
   And so hard to find peace of mind, yes, it is

                         B
   The way I see it you got to say shit

         A                        E   F# G G#
   But don't forget to drop me a line


   A                     E
   Bald as an egg in a dream

         A                            E    F# G G#
   And workin' for your daddy's just a drag

         A                         E
   You still don't know what this mean

        F#
   You better check it out or someday soon

                                B        G G# A A# B
   You're gonna climb back on the wag on


   E                          B
   Keepin' touch with mama kin

   E                                 B
   Tell her where you're gone and been

   E                      B
   Livin' out your fantasy

  E                             B
   Sleepin' late and smokin' tea

  E  A                 G    D  B
   Keepin' touch with mama kin

  E  A                    G       D   B
   Tell her where you're gone and been

  E  A              G    D B
   Livin' out your fantasy

  E   A                G      D C D E
   Sleepin' late and suckin' me

----------------- Acordes -----------------
A = X 0 2 2 2 0
A# = X 1 3 3 3 1
B = X 2 4 4 4 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
