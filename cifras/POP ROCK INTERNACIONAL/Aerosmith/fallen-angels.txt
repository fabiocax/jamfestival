Aerosmith - Fallen Angels

         C                        Am
There's A candle Burning in this world tonite
    G                       F        G
Another child who vanished out of sight
         C                        Am
And a heart is broken, another prayer in vain
           G                        F       G     Am   G
There's a million tears that fill a sea of pain

F             G           C
Sometimes I stare out my window
F             G           C
My thoughts all drift into space
  F                 C               G
Sometimes I wonder if there's a better place


C                            Am
Where do fallen Angels Go? (I just don't know)
G                            F          G       Am         G
Where do fallen Angels go? They keep falling (They keep falling)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
