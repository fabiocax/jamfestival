Aerosmith - Blind Man

Intr.:   E F#m  A
         E F#m  D
         E F#m  A
         E F#m  D
         E F#m

A                        D
I took a course in Hallelujah
A                              D
I went to night school for the blues
A                                  D
I took some stuff they said would cool ya, heh hah
A                              D
But nothing seemed to light my fuse

         F#m        B           D                   E
But it's all in the past like a check that's in the mail
          F#m           B
She was a tall whiskey glass
         D                                           E
I was an old hound dog that just loved to chase his tail


             D         A      E                 B
Until I met a   blind man who taught me how to see, yeah
D          A             E
  A blind man who could change night into day
         D    A       E                        B
And if-a   I can, I'm gonna make you come with me, yeah
        D                                                      A
Cause a-here comes the sun and we'll be chasin' all the clouds away

E F#m  D    E F#m  A    E F#m  D    E F#m

A                         E    F#m  D       F#m
    I 've had some lovers like a    joy ride
B                              E    F#
Some thing are never what they seem
A                         D        F#m
My heaven's turned into a landslide
B                             A     G
Thank God I woke up from that dream

        D        A      E                B
I met a   blind man who taught me how to see, yeah
D          A            E
  A blind man who could change night into day
         D    A       E                        B
And if-a   I can, I'm gonna make you come with me, yeah
        D
Cause a-here comes the sun and we'll be chasin' all the clouds away

E                F#m        E                      D
 So please chase harder and drink all the flowers dry
          E                   F#m
We'll be savin' us a little money
        G                                           A
And if that doesn't do it, yeah, I know the reason why

                             D
Don't make no sense lightin' candles, ah
B                                   A    G
 There's too much moonlight in your eyes

        E        B      F#               C#
I met a   blind man who taught me how to see, yeah
E        B            F#
  Blind man who could change night into day
         E     B             F#                 C#
And if-a    I can, I'm gonna make you come with me, oh oh oh oh yeah
           E
Because a-here comes the sun, ain't no surprise
Ain't no doubt about it, gonna open up my eyes

F# G#m B    F# G#m  E    F# G#m C#   F# G#m E
F# G#m B    F# G#m  E    F# G#m C#   F# G#m E

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C# = X 4 6 6 6 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G#m = 4 6 6 4 4 4
