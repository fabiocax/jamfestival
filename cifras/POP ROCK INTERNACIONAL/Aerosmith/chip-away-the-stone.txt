Aerosmith - Chip Away The Stone

(base)
(E  B   C#m  A)

I saw you standing there I wondered who you were
You looked right through me and I think we can concur
That you don't know my name my claim to fame but I'm not the same
As all the other guys who play the game but look so lame

A                 B
And see what they became

E           B              A
Now its begun and I am the one
            B            E
Your only hero on the stage
           B                 A
I can't complain out in the sun
                 B               E
The lights don't shine for everyone
              B              A          B
They shine on me For all to see its history

         E                 B
Now its begun and I am the one
         A            B           E
I am the one I am the one I am the one

(E B  C#m   A)
So now the tables turned and you know who I am
The boy who made the grade and now you give a damn
But hey its much too late to make that date accept your fate
Your just another girl that I don't rate and wrapped in hate
Obsessed about her weight

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
