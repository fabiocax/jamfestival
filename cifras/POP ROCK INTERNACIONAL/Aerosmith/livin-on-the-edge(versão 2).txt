Aerosmith - Livin On The Edge

E|-----------0h2p0-------0---------|-----------0h2p0-------0---------|
B|-----3-----------3-------3-----3-|-----3-----------3-------3-----3-|
G|---------------------------0h2---|---------------------------0h2---|
D|-0-------------------------------|-0-------------------------------|
A|---------------------------------|---------------------------------|
D|---------------------------------|---------------------------------|
         D
There's something wrong with the world today, I don't know what it is
Bm
Something's wrong with our eyes
       D
We're seeing things in a different way, and God knows it ain't His
Bm
It sure ain't no surprise, yeah
                    D
We're Livin' On The Edge
Livin' On The Edge
              Bb  G
Livin' On The Edge
               D
Livin' On The Edge

         D
There's something wrong with the world today, the lightbulb's gettin' dim
Bm
There's meltdown in the sky
            D
If you can judge a wise man by the color of his skin
Bm
Then mister you're a better man than I
               D
Livin' On The Edge     (you can't help yourself from falling)
               Bb   G
Livin' On The Edge   (you can't help yourself at all)
               D
Livin' On The Edge     (you can't stop yourself from falling)
Livin' On The Edge
F#m
Tell me what you think about your situation, complication, aggravation
Bm
It's getting to you
F#
If Chicken Little tells you that the sky is fallin, even if it wasn't
                               G
Would you still come crawling back again?
           A
I bet you would, my friend
  Bb
Again and again and again and again and again
          D
There's something right with the world today,

And everybody knows it's wrong
          Bm
Well we could tell 'em no, we could let it go

But I would rather be hangin' on!
...
            D
Livin on the edge(7x)

(Final)
|---------17--------------17------|
|-15--------------15--------------|
|-----14------14------14------14--|
|---------------------------------|
|---------------------------------|
|---------------------------------|

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bb = X 1 3 3 3 1
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
