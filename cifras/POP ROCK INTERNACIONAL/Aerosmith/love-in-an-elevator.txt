Aerosmith - Love In An Elevator

{A}
Intro:
G1: (Perry)    riff 1
I-----------------------------I
I-----------------------------I
I-----------------------------I
I-2-0-2---0-------------------I
I-------2---2--1-0---0---2----I
I------------------3---3---0--I
G2: (Whitford) } 4 times
I-----------------------------I
I-----------------------------I
I-9-7-9---7-------------------I
I-------9---9--8-7-5-7-5-9----I
I--------------------------7--I
I-----------------------------I


G1:
I---------------------------------------------------------------I
I--------------------------------------------3---3---3----------I

I--------------2---2---2---------2------------------------------I
I-2------------------------------0-----4-p-5---5---5-----5p4p0--I
I-0-----4-p-5----5---5----5p4p0---------------------------------I
I---------------------------------------------------------------I
G2: riff 2
I---------------------------------------------------------------I
I--------------------------------2------------------------------I
I-2------------------------------2------------------------------I
I-2------------------------------0------------------------------I
I-0-------------------------------------------------------------I
I---------------------------------------------------------------I

{B}
Verse:
G1&2:                                       4 times
I------------------------------------------------I
I------------------------------------------------I
I----------------------------------------5-------I
I----------------------------------------5-------I
I-5--5---5----5----5-----5---------------3-------I
I-0--2---2----2----3-----3-----------------------I
Workin like a dog fo de' boss man    (Whoa) etc.
Jackie's in  the  elevator           (Whoa)

I-----------------------------3--I-----------------------------------I
I-----------------------------0--I-----------------------------------I
I---------------------7-------0-.I-----------------7----7------------I
I-7---7---7---7---7---7-------0-.I-7--7---7---7----7----7------------I
I-0---4---0---4---5---5-------2--I-0--4---0---4----5----5------------I
I-----------------------------3--I-----------------------------------I
I kinda hope we get stuck...etc   to read between the lines
So where am i gonna look          and have you home by five

{C}
Chorus
G1:
I-------------------------------------------------I
I-------------------------------------------------I
I-------------------------------------4-----------I
I--4--4--4---6--4------4--4--4--4-----4--4--4-----I
I--2--2--2---2--2------2--0--2--2-----2--2--2-----I
I-------------------------------------------------I
Love in an elevator______________________________

G2:
I-------------------------------------------------I
I--4-------------------------------4--------------I
I--4--4----------------------4-----4--4--4--4-----I
I--4--4--4-6------4----4--6--4-----4--4--4--4--4--I
I--2--2--2-2------2----2--2--2-----2--2--2--2--2--I
I-------------------------------------------------I


G1: [1st time, 3rd time]
I-------------------------------------------------I
I-------------------------------------------------I
I-------------------------------------------------I
I--4---4--4----------4---4--4-----6---4-----4-----I  to {C}
I--2---2--2----------2---2--0-----2---2-----0-----I
I-------------------------------------------------I
Livin it up         when i'm     goin      down
G2:
I-------------------------------------------------I
I--4----------------------------------------------I
I--4---4------------------------------------2-----I
I--4---4--4--6-----------4-----4----6---4---2-----I
I--2---2--2--2-----------2-----2----2---2---2-----I
I-------------------------------------------------I


G1: [2nd time]
I-------------------------------------------------I
I-------------------------------------------------I
I-4--------------2--------------------------------I
I-4---------4----2-------4-------4----3----2------ITo {B}
I-2---------2------------2-------2----1----0------I
I-------------------------------------------------I
lovin   it up 'til      I       hit the ground
G2:
I------------------------------------------0------I
I-4----------------------------------------2------I
I-4--4-------------------4-----------------2------I
I-4--4--4---6---------4--4-------4----3----2------I
I-2--2--2---2---------2--2-------2----2----0------I
I-------------------------------------------------I


G1: [4th Time]
I------------------------------------------------I
I------------------------------------------------I
I----4---4--4---4--4--4---4-----4--4---2---------I to {D}
I----4---4--4---4--4--4---4-----4--4---2---------I
I----2---2--2---2--2--2---2-----2--2---2---------I
I------------------------------------------------I
  Livin' it up  'til    I    hit the ground in the
G2:
I----------------------------------------------I
I---4------------------------------------------I
I---4--4-----2---------------4--------2--------I
I---4--4--4--2--4--4--4------4--------2--------I
I---2--2--2--2--2--2--2------2--------2--------I
I----------------------------------------------I

{D}
Bridge
G1 & G2:
I---------------------------------------------I
I---------------------------------------------I
I-4-------------4------4---4------------------Ito {C}
I-2-------------2------2---2------------------I
I---------------------------------0--2--0--2--I
I---------------------------------0--2--0--2--I
    air    in the air

G1: [2nd time]
I-----------------------------------------------------I
I-----------------------------------------------------I
I-----------------------------------------------------I
I-4----4----4-------4--4---4--------6--4--------4-----ITo {E}
I-2----2----2-------2--2---0--------2--2--------2-----I
I-----------------------------------------------------I
  lovin' it up        when i'm       goin      down

G2:
I-----------------------------------------------------I
I-4---------------------------------------------------I
I-4----4----------------------------------------------I
I-4----4----4----6----4-------4----6----4-------4-\---I
I-2----2----2----2----2-------2----2----2-------2-\---I
I-----------------------------------------------------I

{E}
GUITAR SOLO
G1 (G2 w/ riff 1&2)
I-9-----10----12----10--9------10----12~~~~--I
I-10----10----10----10--10-------------------I
I--------------------------------------------I
I--------------------------------------------I
I--------------------------------------------I
I--------------------------------------------I

I-----------------------------------------7-----I
I--7^----7--8----10---8--7-----8-----10---7-----I
I--7^----7--7-----7---7--7-----7--7--10---------I
I-----------------------------------------------I
I-----------------------------------------------I
I-----------------------------------------------I

G1 (G2 w/ rhy. fig. 1)
I-0-0---12-0--0--10-0---------------12--------------------I
I-0--------------------15^------------15--12-14-----------I
I--------------------------------------------14--12-------I
I--------------------------------------------------14--12-I
I---------------------------------------------------------I
I---------------------------------------------------------I

I----------------------------------I-----5-----------9-9------10\----I
I-----12--------12----12-12---12---I--------------------------10\----I
I-----12--------12----12-12---12---I-4/6---6/7---4/9---9-9/11------9-I
I--14------14-14---14------14-\----I---------------------------------I
I----------------------------------I---------------------------------I
I----------------------------------I---------------------------------I

I---------------12------------14----14-14--------------------I
I------------------------------------------------------------I
I-11----11-11/12--12-12-12p14-14-14-14-14--14--14\-----------I
I------------------------------------------------------------I
I------------------------------------------------------------I
I------------------------------------------------------------I

I-------------------------------I
I---14--14--14--12^12^----------I
I---14--14--14--12^12^----------I
I-14--14---------------14-12-14-I
I-------------------------------I
I-------------------------------I

I-------------------------------------------I
I-------------------------------------------I
I-------------------------------------------I
I---14--12-12-12----------------------------I
I-----------------14-14-13-13-12-12-10------I
I---------------------------------------12\-I

I-------------------------------------------------------I
I-------------------------------------0-----------------I
I----------4^p2-4^p2-4^p2-4^p2-4^p2-4p0-4p2p0-4p2p0-2/--I
I-------------------------------------------------------I
I-0--0-0-0----------------------------------------------I
I-------------------------------------------------------I

G2 (G1 w/ riff 1)  (hold bend)_______]
I-19^------19^------19^--19----19--19--19--17-I
I---------------------------------------------I
I---------------------------------------------I
I---------------------------------------------I
I---------------------------------------------I
I---------------------------------------------I

I-----------------------------------19---------I
I-19----19^-------(19)p17-17--17--------17-----I
I----------------------------------------0-----I
I----------------------------------------------I
I----------------------------------------------I
I----------------------------------------------I

G1 (g2 pick scratches):
I-12---12-12---12-12--12--------------------------------------12-12I
I-15^-----15^-----15^----15^--12-14^-(14)p1214^-(14)p12-14-12-12-12I
I------------------------------------------------------------------I
I------------------------------------------------------------------I
I------------------------------------------------------------------I
I------------------------------------------------------------------I

I--------------------------12-12-12--------15-17^------------------I
I-----------14-14------15^-12-12-12-17-17--------------------------I
I-1214^--12-14-14----12--------------------------------------------I
I-----------------14-----------------------------------------------I
I------------------------------------------------------------------I
I------------------------------------------------------------------I
~~~~
I-17^-----------------------------------12-------------------------I
I--------15^-(15)p12-------12-15^-------12---12--------------------I
I----------------------15------------------------14p\p12--9--------I
I------------------------------------------------------------------I
I------------------------------------------------------------------I
I------------------------------------------------------------------I

I------------------------------------------------------------------I
I------------------------------------------------------------------I
I-12^~~~~-12-9-12p9------7^~~~---7---------------------------------I
I--------------------12-------------7p0-7p0------------------------I
I--------------------------------------------5---------------------I
I------------------------------------------------------------------I

I------------------------------------------------------------------I
I-------------------------------------0----------------------------I
I-------------------------------------0----2-----------------------I
I------------------------0--2---0--2-------------------------------I
I---7p5-----7p\p5--------------------------------------------------I
I--------7-------------------------------------3p\p0---------------I

I------------------------------------------------------------------I
I------------------------------------------------------------------I
I------------------------------------------------------------------I
I------------------------------------------------------------------I
I------------------------------------------------------------------I
I--4p/p7-----------------------------------------------------------I

G1&2      Riff 3
I------------------------------------------------------------------I
I------------------------------------------------------------------I
I------------------------------------------------------------------I
I------------------------------------------------------------------I
I------------------------------------------------------------------I
I-0-0-0-0-4p0-0-5p0-0-6p0-7p0-9-10--0-4p0-0-5p0-0-6p0-7p0-9-10-----I

Repeat x2

G1 (g2 w/ riff 3)
I------------------------------------------------------------------I
I---------------------------------------------------9-\7-----------I
I----12^--12~~~~---12^---12~--9------12-9-----9\7-9-9-\7--7--------I
I-------------------------------12--------7/9-9\7-9------------/12-I
I------------------------------------------------------------------I
I------------------------------------------------------------------I

I-12---------------------------------------------------------------I
I----12-------15^---------------------------------15-------15^p12--I
I----12----------------15---12-14-12-15-14--15^--------------------I
I------------------------------------------------------------------I
I------------------------------------------------------------------I
I------------------------------------------------------------------I

I-15^----15-15----15---15-15----15-15--15^----12-------------I
I-14^----14-14----14---14-14----14-14--14^----12-------------I
I----------------------------12------------------------------I
I------------------------------------------------------------I
I------------------------------------------------------------I
I------------------------------------------------------------I

I------------------------------------------------------------------I
I------------------------------------------------------------------I
I------------------------------------------------------------------I
I------------------------------------------------------------------I
I------------------------------------------------------------------I
I------------------------------------------------------------------I
  going_________________    down___________________________________

I-12-----------12--12--------------12-12----I
I-12-----------12--12--------------12-12----I
I-------------------------------------------I
I----14-----14--------------14--------------I
I-------------------------------------------I
I-------------------------------------------I

I------------------------------------------------------I
I--------------------------------------------14--------I
I---------------12------12^----12^-----------14--------I
I----11/12--14------14------------14---14-14-----14----I
I------------------------------------14----------------I
I------------------------------------------------------I

I------------------------------15-12----12--------15----I
I------------12h15--15p12------------12----15/17--------I
I----12^------------------14-12-------------------------I
I---------14--------------------------------------------I
I-------------------------------------------------------I
I-------------------------------------------------------I

I--------------15--------------15----15--17^----I
I----17--17~~-----17--17-17~~~----17------------I
I-----------------------------------------------I
I-----------------------------------------------I
I-----------------------------------------------I
I-----------------------------------------------I

I----17------------17------------17--------15^---(15)-----------I
I------------20^----------20^-------------------------12--------I
I---------------------------------------------------------12----I
I---------------------------------------------------------------I
I---------------------------------------------------------------I
I---------------------------------------------------------------I

I------------------------------------------------I
I----14--------0----------9----------------------I
I-12----12-----0----------9-----7--7~~~----------I
I-------------------7/9--------------------9/----I
I------------------------------------------------I
I------------------------------------------------I

I--------------------------------14------14------12----12----I
I---------14---------------------------------12----12--12----I
I--------------12----(12)\12/14-----14-----------------------I
I----/14-----------------------------------------------------I
I------------------------------------------------------------I
I------------------------------------------------------------I

I-------12------12-----------12--12-----------12--12---------12----I
I----15-----------15^--15^---------15^--15^---------15^--15^-------I
I------------------------------------------------------------------I
I------------------------------------------------------------------I
I------------------------------------------------------------------I
I------------------------------------------------------------------I

I----12------12-----------12----------12----------12---------12----I
I------15^-------12-15^-------12-15^-----12-15^------12-15^-----12-I
I------------------------------------------------------------------I
I------------------------------------------------------------------I
I------------------------------------------------------------------I
I------------------------------------------------------------------I

I-------19----19----19----19----19----19----19----19----I
I----20~~~~20~~~~20----20----20----20----20----20-------I
I-------------------------------------------------------I
I-------------------------------------------------------I
I-------------------------------------------------------I
I-------------------------------------------------------I

{E}
Chorus
G1:
I-------------------------------------------------I
I-------------------------------------------------I
I-------------------------------------4-----------I
I--4--4--4---6--4------4--4--4--4-----4--4--4-----I
I--2--2--2---2--2------2--0--2--2-----2--2--2-----I
I-------------------------------------------------I
Love in an elevator______________________________

G2:
I-------------------------------------------------I
I--4-------------------------------4--------------I
I--4--4----------------------4-----4--4--4--4-----I
I--4--4--4-6------4----4--6--4-----4--4--4--4--4--I
I--2--2--2-2------2----2--2--2-----2--2--2--2--2--I
I-------------------------------------------------I


G1:
I-------------------------------------------------I
I-------------------------------------------------I
I-------------------------------------------------I
I--4---4--4----------4---4--4-----6---4-----4-----I
I--2---2--2----------2---2--0-----2---2-----0-----I
I-------------------------------------------------I
Livin it up         when i'm     goin      down
G2:
I-------------------------------------------------I
I--4----------------------------------------------I
I--4---4------------------------------------2-----I
I--4---4--4--6-----------4-----4----6---4---2-----I
I--2---2--2--2-----------2-----2----2---2---2-----I
I-------------------------------------------------I

G1:
I-------------------------------------------------I
I-------------------------------------------------I
I-------------------------------------4-----------I
I--4--4--4---6--4------4--4--4--4-----4--4--4-----I
I--2--2--2---2--2------2--0--2--2-----2--2--2-----I
I-------------------------------------------------I
Love in an elevator______________________________

G2:
I-------------------------------------------------I
I--4-------------------------------4--------------I
I--4--4----------------------4-----4--4--4--4-----I
I--4--4--4-6------4----4--6--4-----4--4--4--4--4--I
I--2--2--2-2------2----2--2--2-----2--2--2--2--2--I
I-------------------------------------------------I

G1:
I-------------------------------------------------I
I-------------------------------------------------I
I-4--------------2--------------------------------I
I-4---------4----2-------4-------4----3----2------I
I-2---------2------------2-------2----1----0------I
I-------------------------------------------------I
lovin   it up 'til      I       hit the ground
G2:
I------------------------------------------0------I
I-4----------------------------------------2------I
I-4--4-------------------4-----------------2------I
I-4--4--4---6---------4--4-------4----3----2------I
I-2--2--2---2---------2--2-------2----2----0------I
I-------------------------------------------------I

{F}
VERSE
G1&2 4x
I-----------------------------------------I
I-----------------------------------------I
I--------------------------------5--------I
I--------------------------------5--------I  To {B}
I-5-5---5--5--5---------5--------3--------I
I-0-2---0--2--3---------3-----------------I
Gonna_ be a penthouse_ pauper__ (Whoa) etc

{G}
Bridge 2
G1:
I------------------------------------------------------------------I
I------------------------------------------------------------------I
I------------------------------------------------------------------I
I-2----------------2----------------2-------------2----------------I
I-2----------------2----------------2-------------2----------------I
I-0----------------0----------------0-------------0----------------I
  In the air,                in the air,
G2:
I------------------------------------------------------------------I
I-4-p-5-------5----5-------4-------4-p-5-------4-------------------I
I-4-----------4----4-------4-------4-----------4-------4----4------I
I-4---6-------6----6-------4-------4---6-------4-------4----4------I
I------------------------------------------------------------------I
I------------------------------------------------------------------I

G1:
I------------------------------------------------------------------I
I------------------------------------------------------------------I
I------------------------------------------------------------------I
I-2------------2----------2-------2--------------------------------I
I-2------------2----------2-------2--------------------------------I
I-0------------0----------0-------0--------------------------------I
  Honey  now  one  more  time it aint fair
G2:
I------------------------------------------------------------------I
I-------------------------5-----4----------------------------------I
I-------------------------4-----4-2--------------------------------I
I-4----4----4---6---4-----6-----4-2--------------------------------I
I-2----2----2---4---2----------------------------------------------I
I------------------------------------------------------------------I


Love in an elevator___________   livin it up when im goin down______

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G1 = 3 2 0 0 0 3
G2 = 3 X 0 2 0 X
