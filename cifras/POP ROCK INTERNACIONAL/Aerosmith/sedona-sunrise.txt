Aerosmith - Sedona Sunrise

Intro: Riff com base em A

D
  I was a city boy
                     A
Cought up in a city way
D
  A nitty gritty boy, stayin' out all night
                          A
And always gettin blown away
       E                   E7               A
There ain't no breeze to cool the heat of love

D
  She was a city girl
                    A
With no responsibility
 D                                                                       A
 A pretty little city girl, all fired up and what's that got to do to me
       E                  E7                 A
There ain't no breeze to cool the heat of love


REFRÃO:
        F#m          G          A
It's as clear as a Sunrise in Sedona
      F#m               G               D
Just what it is that's blowin' in the wind
          F#m                       G                             A9                      B9
It's the fire in her eyes, It's the tear when she cries, It's the heat when I fall on my knees
                  C#m              D9                 E                    A
That I'm thinkin' of ....... There ain't no breeze to cool the heat of love

Base do solo: (D, A, D, A, E, E7, A)
Repete Refrão, no final, fica no riff, com a base em A (e solo de gaita)

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
B9 = X 2 4 4 2 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
