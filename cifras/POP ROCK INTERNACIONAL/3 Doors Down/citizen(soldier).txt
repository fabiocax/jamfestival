3 Doors Down - Citizen (Soldier)

          C#m                             A
Beyond the boundaries of your city's lights,
E                         B
Stand the heroes waiting for your cries.
C#m                         A
So many times you did not bring this on yourself,
E
When that moment finally comes,
B
I'll be there to help.

C#m              A                           E               B
On that day when you need your brothers and sisters to care, I'll be right here.
C#m              A                           E                         B
Citizen soldiers holding the light for the ones that we guide from the dark of despair.
C#m                       A
Standing on guard for the ones that we sheltered,
E                       B                       C#m    A  E  B
We'll always be ready because we will always be there.

C#m                          A
When there are people crying in the streets,

E                           B
When they're starving for a meal to eat,
C#m                     A
When they simply need a place to make their beds,
E
Right here underneath my wing,
B
You can rest your head.

C#m              A                           E               B
On that day when you need your brothers and sisters to care, I'll be right here.
C#m              A                           E                         B
Citizen soldiers holding the light for the ones that we guide from the dark of despair.
C#m                       A
Standing on guard for the ones that we sheltered,
E                       B                       C#m
We'll always be ready because we will always be there...
C#m
There...There...There
A
There...There...There

C#m                 A
Hope and pray that you'll never need me,
E                        B
But rest assured I will not let you down.
C#m                       A
I'll walk beside you but you may not see me,
E                            B
The strongest among you may not wear a crown.

C#m                A                          E
On that day when you need your brothers and sisters to care,
B
I'll be right here!
C#m                   E                                A
On that day when you don't have the strength for the burden you bear,
B
I'll be right here!
C#m                  A                       E                                  B
Citizen soldiers holding the light for the ones that we guide from the dark of despair.
C#m                        A
Standing on guard for the ones that we sheltered,
E                     B                         C#m
We'll always be ready because we will always be there.

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
