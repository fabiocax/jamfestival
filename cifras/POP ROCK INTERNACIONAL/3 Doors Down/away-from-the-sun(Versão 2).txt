3 Doors Down - Away from the sun

Tom : F

intro:

e|----------------------------------------- |
b|----1------1------1-----1---------------- |
G|-------2------2------2------4h5---------- |
D|----------------------------------5------ |
A|----------------------------------------- |
E|-1--------------------------------------- |

e|----------------------------------------- |
b|----1------1------1-----1---------------- |
G|-------2------2------2------4h5---7---9-- |
D|----------------------------------------- |
A|----------------------------------------- |
E|-1--------------------------------------- |

e|------------------------------------------|
b|---1---1---1---1--------------------------|
G|-----2---2---2----4h5---7-5-4---7-5-4-4h5-|
D|----------------------5-------5-----------|
A|------------------------------------------|
E|-1----------------------------------------|


(continue usando a intro)

It's down to this /
I've got to make my life make sense /
Can anyone tell what I've done /
I miss the life /
I miss the colours of the world /
Can anyone tell where I am /


F               Am                C
Cause now again I've found myself so far down /
F                   Am             C
Away from the sun that shines into darkest place /
F
I'm so far down

   F           F        Am
D|-3-----------3--------7---------------------- |
A|-3----3------3--------7---------------------- |
E|-1h3-----3---1--------5---------------------- |

   away from the sun again/

(volte c/ a intro)

   Away from the sun again /

(continue c/ a intro, preste atenção p cair certo)

I'm over this /
I'm tired of livin' in the dark /
Can anyone see me down here /
The feeling's gone /
There's nothing left to lift me up /
Back into the world I know /

(refrão)

F             Am                C
And now again I've found myself so far down /
F                  Am                      C
Away from the sun that shines into darkest place /

I'm so far down

   F           F        Am
D|-3-----------3--------7---------------------- |
A|-3----3------3--------7---------------------- |
E|-1h3-----3---1--------5---------------------- | 

    away from the sun /

F                    Am              C
That shines to light the away for me to find my way

     F         F        
D|-3-----------3-------- |
A|-3----3------3-------- |
E|-1h3-----3---1-------- | 

  back into the arms /

F               Am
That care about the ones like me /
C
I'm so far down

   F           F        Am
D|-3-----------3--------7---------------------- |
A|-3----3------3--------7---------------------- |
E|-1h3-----3---1--------5---------------------- | 

   Away from the sun again /

      (F Am C )


e|---6--6/8---6---5------------ |
b|--------------------6-------- |

(volte c/ a intro)

It's down to this /
I've got to make my life make sense /
And now I can' tell what I've done /
And now again I've found myself so far down /
Away from the sun that shines to light the away for me /

(refrão)

F               Am                C
Cause now again I've found myself so far down /
F                   Am             C
Away from the sun that shines into darkest place /
F
I'm so far down

   F           F        Am
D|-3-----------3--------7---------------------- |
A|-3----3------3--------7---------------------- |
E|-1h3-----3---1--------5---------------------- |

   away from the sun again/

F                    Am              C
That shines to light the away for me to find my way

     F         F        
D|-3-----------3-------- |
A|-3----3------3-------- |
E|-1h3-----3---1-------- | 

  back into the arms /

F               Am
That care about the ones like me /
C
I'm so far down

   F           F        Am
D|-3-----------3--------7---------------------- |
A|-3----3------3--------7---------------------- |
E|-1h3-----3---1--------5---------------------- | 

   Away from the sun again /

      ( F Am C )

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
