3 Doors Down - Presence Of The Lord

Intro 2x: C  F  Em  Dm

C      G               Am        Am  C  F
I have finally found a way to live,
F           G           C     C  F  Em  Dm
Just like I never could before.
C           G            Am        Am  C  D7
I know that I don't have much to give,
D7                 G7   Em  G  Am
But I can open any door.

Am                  F      Em  G  Am
Everybody knows the secret,
Am                  D7
Everybody knows the score.

C      G               Am        Am  G  F
I have finally found a way to live,
F      G               C    F  Em  Dm  C
In the presence of the Lord.


C      G               Am        Am  G  F
I've finally found a place to live,
F      G          C         F  Em  Dm  C
Just like I never could before.
   C      G               Am        Am G F
And I know that I don't have much to give,
F      G          C         F  Em  Dm  C
But soon I'll open any door.

C      G               Am         Am  G  F
I have finally found a place to live,
F      G               C   C  F  Em  Dm  C
In the presence of the Lord

Solo:

C      G               Am        Am  G  F
I've finally found a place to live,
F      G          C         F  Em Dm  C
Just like I never could before.
   C      G               Am        Am  G  F
And I know that I don't have much to give,
F      G          C         F  Em  Dm  C
But soon I'll open any door.

C      G               Am         Am  G  F
I have finally found a place to live,

F      G                C   C  F  Em  Dm C  F  Em  Dm  Am
In the presence of the Lord

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
