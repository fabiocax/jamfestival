3 Doors Down - Let Me Be Myself

E|---------------------------------------------------------------------------|
B|--0h1---0h1------0h1---0h1----------0h1---0h1----------0h1---0h1----3------|
G|------0-----0--------2-----2------------0-----0------------0-----0---------|
D|-------------2---------------2------------------2h3h2----------------------|
A|--3-----3--------0-----0-------5h3-----------------------------------------|
E|------------------------------------1-----1------------3-----3-------------|

I guess I just got lost
being someone else.
I tried to kill the pain,
But nothing ever helped.
I left myself behind,
somewhere along the way
hoping to come back around
to find myself someday...

F            C          G              F
Lately I'm so tired of waiting for you
                 C
to say that it's okay.
G       C
Tell me please...

                     G
Would you one time let me be myself
         F
so I can shine

with my own light.
C             G
Let me be myself.

E|---------------------------------------------------------------------------|
B|--0h1---0h1------0h1---0h1----------0h1---0h1----------0h1---0h1----3------|
G|------0-----0--------2-----2------------0-----0------------0-----0---------|
D|-------------2---------------2------------------2h3h2----------------------|
A|--3-----3--------0-----0-------5h3-----------------------------------------|
E|------------------------------------1-----1------------3-----3-------------|

Would you let me be myself?
'Cause I'lll never find my heart
behind someone else.
I'lll never see the light of day
living in this cell.
It's time to make my way
into the world I knew.
And then take back all of these times
that I gave in to you...

F          C            G               F
Lately I'm so tired of waiting for you
                 C
to say that it's okay.
G       C
Tell me please...
C                  G
Would you one time let me be myself
         F
so I can shine
with my own light.
C            G
Let me be myself.

      C
For a while,

if yo don't mind,
G
let me be myself
         F
so I can shine
with my own light.
C           G
Let me be myself.
                                                                                                    (solo)
E|--0-0-0--0--0---0-0-0--0--0---0-0--0--0--0--0---0---0---
B|--0-0-0--0--0---0-0-0--0--0---0-0--0--0--0--0---0---0---
G|--5-7-10-9--9---5-7-10-9--9---7-7--9--7--12-10--5---4---
D|--0-0-0--0--0---0-0-0--0--0---0-0--0--0--0--0---0---0---
A|--3-5-8--7--7---3-5-8--7--7---5-5--7--5--10-8---3---2---
E|--0-0-0--0--0---0-0-0--0--0---0-0--0--0--0--0---0---0---

E|--0-0-0--0--0---0-0-0--0--0---0-0--0--0--0-----
B|--0-0-0--0--0---0-0-0--0--0---0-0--0--0--0-----
G|--5-7-10-9--9---5-7-10-9--9---7-7--9--7--12----
D|--0-0-0--0--0---0-0-0--0--0---0-0--0--0--0-----
A|--3-5-8--7--7---3-5-8--7--7---5-5--7--5--10----
E|--0-0-0--0--0---0-0-0--0--0---0-0--0--0--0-----

That's all I ever wanted from this world,
is to let me be me.

               C           G                  F               C           G
E|-----------------------------3----------------------------------------3-----------
B|-----------1-0h1-------------3-----------------------------1h0--------3-----------
G|---------0-------0-------0-----------------0-2----------0-------------0-----------
D|-------2---------------0---------------3-3------------2---------------0-----------
A|-----3---------------2---------------3--------------3-----------------2-----------
E|-------------------3---------------1----------------------------------3-----------

Please, would you one time
let me be myself
so I can shine
with my own light.
Let me be myself.

C
Please, would you one time
G
let me be myself

          F
so I can shine
with my own light.
C            G
Let me be myself.
       C
For a while, if you don't mind
G
let me be myself
          F
so I can shine,
with my own light.
C           G
Let me be myself.
       C                  G
Would you one time uuh.. uuh..
              F
let me be myse..lf.
C    G     C
let me be me.


----------------- Acordes -----------------
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
