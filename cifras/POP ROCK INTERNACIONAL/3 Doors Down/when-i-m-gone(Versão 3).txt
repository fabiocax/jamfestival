3 Doors Down - When Im Gone

[Verso 1]

            Am
There's a mellow world inside of me
      C             G
That you may never see
           Am                              C     G
There's secrets in this life that I can't hide
Am
Somewhere in this darkness
           C                    G
There's a light that I can't find
Am                  C
Maybe it's too far away
G               Am
Maybe I'm just blind


[Refrão]

                  Am
Hold me when I'm here


Love when I'm wrong
                   G
Hold me when I'm scared
             F
And love me when I'm gone
             Am
Everything I am

And everything you need
             G               F
Possibly the one you wanted me to be
                    Am
I'll never let you down

Even if I could
               G
Give up everything
          F
If only for your good
                  Am
Hold me when I'm here

Love when I'm wrong
                   G
Hold me when I'm scared
                F
I won't always be there
                     Am  C  G
So love me when I'm gone
                     Am  C  G
So love me when I'm gone


[Verso 2]

           Am
When your education X-ray
         C            G
Can not see under my skin
    Am
I won't tell you a damn thing
       C                   G
That I could not tell my friends
     Am
The roaming in this darkness
     C              G
I'm alive but I'm alone
Am
Part of me is fighting this
     C             G
But part of me is gone


[Refrão]

                  Am
Hold me when I'm here

Love when I'm wrong
                   G
Hold me when I'm scared
             F
And love me when I'm gone
             Am
Everything I am

And everything you need
             G               F
Possibly the one you wanted me to be
                    Am
I'll never let you down

Even if I could
               G
Give up everything
          F
If only for your good
                  Am
Hold me when I'm here

Love when I'm wrong
                   G
Hold me when I'm scared
                F
I won't always be there
                     Am  C  G
So love me when I'm gone
                     Am  C  G
So love me when I'm gone


[Refrão Final]

                     Am  C  G
So love me when I'm gone
                     Am  C  G
So love me when I'm gone

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
