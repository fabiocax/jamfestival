3 Doors Down - Be like that

[Intro]

E|----3-------2-------0------0----|
B|-----0----3--3-----3-3----3-3---|
G|---0--0----2--2-------0--0---0--|
D|--0----0-0-----0--2-----2-------|
A|-----------------2-----3--------|
E|-3--------------0---------------|

G                            D
He spends his nights in California
Em                             C
Watching the stars on the big screen
G                          D
Then he lies awake and he wonders
Em                C
Why can't that be me?

G
Cause in his life he's filled
     D
With all these good intentions

Em
He's left a lot of things
       C                       G
He'd rather not mention right now
                          D
But just before he says goodnight
Em                   C
He looks up with a little smile at me

And he says.

G                   D
If I could be like that

I would give anything
Em                 C
Just to live one day, in those shoes
G                   D
If I could be like that
             Em
What would I do
              C
What would I do?

G                                D
She spends her days up in the north park
Em                            C
Watching the people as they pass
G
And all she wants is just
    D
A little piece of this dream,
Em                   C
Is that too much to ask?

        G                D
With a safe home, and a warm bed,
Em                   C
On a quiet little street
G                            D
All she wants is just that something to
            Em             C
Hold onto, that's all she needs

Yeah!

G                   D
If I could be like that

I would give anything
Em                 C
Just to live one day, in those shoes
G                   D
If I could be like that
             Em
What would I do
              C
What would I do?

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
