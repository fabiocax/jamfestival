3 Doors Down - Be like that

  G                D9                  Em*              C*  
e|--------3--------------0---------------0--------------0------|
B|----------3--------3-----3-----------3---3----------3---3----|
G|---0--------0--------2-----2-------0-------0------0-------0--|
D|-----0--------0-0------------0------------------2------------|
A|---------------------------------2------------3--------------|
E|-3-----------------------------0-----------------------------|


G                            D/F#
He spends his nights in California,
Em*                            C*
Watching the stars on the big screen
G                          D/F#
Then he lies awake and he wonders,
Em*             C*
Why can't that be me

G
Cause in his life he is filled
D/F#
With all these good intentions
Em*
He's left a lot of things
C*
He'd rather not mention right now
G                       D/F#
But just before he says goodnight,
Em*                   C*
He looks up with a little smile at me,

And he says,

-REFRÃO-

G                  D/F#
If I could be like that,
                   Em*
I would give anything
                 C*               G
Just to live one day, in those shoes
G                   D/F#              Em*
If I could be like that, what would I do,
             C*
What would I do


(Am* - C*- G) 2x
Am*      C*         G  Bm C
Now and dreams we run

G                              D/F#
She spends her days up in the north park,
Em*                        C*
Watching the people as they pass
G
And all she wants is just
D/F#
A little piece of this dream,
Em*                 C*
Is that too much to ask
G                D/F#
With a safe home, and a warm bed,
Em*                 C*
On a quiet little street
G                          D/F#
All she wants is just that something to
            Em*              C*
Hold on to, that's all she needs

Yeah!

G                  D/F#
If I could be like that,
                   Em*
I would give anything
                 C*               G
Just to live one day, in those shoes
G                   D/F#              Em*
If I could be like that, what would I do,
             C*
What would I do

Am*           C*        G
Yeaaaahh Yeyeeaah Ohuoou yeahhhh
                 Am*          C*
I'm falling into this, sweet dreams
   G       Bm C
We run away


G                   D/F#
If I could be like that,
                 Em*
I would give anything
                 C*                G
Just to live one day, in those shoes
G                    D/F#              Em*
If I could be like that, what would I do,
             C*
What would I do


G                  D/F#
If I could be like that,
                   Em*
I would give anything
                 C*               G
Just to live one day, in those shoes
G                   D/F#              Em*
If I could be like that, what would I do,
             C*
What would I do

(Em - C)

        Em*
Falling in
C*                  Em*    C       G
I feel I am falling in, to this again.

Am* = Alternar a nota entra Am e Am11
C* = Alternar a nota entre C e C9
Em* = Alternar a nota entre Em e Em7

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D/F# = 2 X 0 2 3 2
D9 = X X 0 2 3 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
