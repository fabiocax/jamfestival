3 Doors Down - Everything Changes

             E
If you just walked away
              A
What could I really say?
                C#m
would it matter anyway?
                        A
would it change how you feel?

         E
I am the mess you chose
                A
the closet you can not close
                 C#m
The devil in you I suppose
                      A
'cuz the wounds never heal

E
But everything changes if I could
A
turn back the years if you could

C#m                               A
learn to forgive me then I could learn to feel

              E
Sometimes the things I say
               A
In moments of disarray
                  C#m
Succumbing to the games we play
                       A
To make sure that it's real

E
But everything changes if I could
A
turn back the years if you could
C#m                                        A
learn to forgive me then I could learn to feel

(solo) C#m B A E B C#m A E B A

               E
When it's just me and you
               A
Who knows what we could do
               C#m
If we can just make it through
                         A
through this part of the day

E
But everything changes if I could
A
turn back the years if you could
C#m
learn to forgive me then I could
A
learn, learn to feel then we could
C#m
Stay here together and we could
A
Conquer the world if we could
C#m                              A
Say that forever it's more than just a word

            E
If you just walked away
              A
What could I really say?
                   C#m
It wouldn't matter anyway
                           A
It wouldn't change how you feel

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
