Elvis Presley - Fool

Intro :F#   C#/F    C#/E    B    D     F#    C#
G    D/F#    Dm/F    C    Eb    G     D

F#                                     C#/F
Fool, you didn't have to hurt her
 E                               B
Fool, you didn't have to lose her
 D                            F#
Fool, you only had to love her
      C#       C#7      F#
But now her love is gone

F#                                           C#/F
Fool, you could have made her want you
 E                                          B
Fool, you could have made her love you
 D                             F#
Fool, you only had to love her
      C#       C#7      F#
But now her love is gone


 Ebm                           Dº
Gone now, the love and laughter
 E                                  B
See yourself the morning after
 D                                      F#
Can't you see her eyes are misty
C#                       F#    D
As she said good-bye

G                               D/F#
Fool, you didn't have to hurt her
Dm/F                          C
Fool, you didn't have to lose her
 Eb                          G
Fool, you only had to love her
       D          C/D      G
But now your love is gone
G                         D/F#
Oh-oh-oh-oh-oh-oh-oh-oh
Dm/F                   C
Oh-oh-oh-oh-oh-oh-oh-oh
 Eb                          G
Fool, you only had to love her
       D          C/D      G      Eb
But now your love is gone

 G#                                       Eb/G
Fool, you could have made her want you
Ebm/F#                                      C#
Fool,     you could have made her love you
 E                           G#
Fool, you only had to love her
      Eb       Eb7
But now her love is gone
 G#                              Eb/G
Fool, you didn't have to hurt her
  F#                            C#
Fool, you didnt have to lose her
  E                           G#
Fool, you only had to love her..

----------------- Acordes -----------------
B = X 2 4 4 4 2
C = X 3 2 0 1 0
C# = X 4 6 6 6 4
C#/E = X X 2 1 2 1
C#/F = X 8 X 6 9 9
C#7 = X 4 3 4 2 X
C/D = X X 0 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Dm/F = X X 3 2 3 1
Dº = X X 0 1 0 1
E = 0 2 2 1 0 0
Eb = X 6 5 3 4 3
Eb/G = 3 X 1 3 4 X
Eb7 = X 6 5 6 4 X
Ebm = X X 1 3 4 2
Ebm/F# = 2 X 1 3 4 X
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
