Elvis Presley - From a Jack To a King

VERSO 1, 3 and 4:
:(E) :E :E :B :B :B :B :E :
VERSO 2:
:(E) :E :E :B :B :B :B :E A :E7 :
REFRÃO:
:A :A :E :E :F# :F# :B :B :
SOLO:
:B :B :E A :E7 :

VERSO 1
(E)              E
>From a jack to a king
                     B
>From loneliness to a wedding ring
I played an ace and I won a queen
                          E
And walked away with your heart

VERSO 2
(E)              E
>From a jack to a king

                             B
With no regret I stacked the cards last night
And lady luck played her hand just right
                         E    A    E7
To make me king of your heart

REFRÃO
           A                                   E
For just a little while I thought that I might lose the game
             F#                             B
Then just in time I saw the twinkle in your eye

VERSO 3
(E)              E
>From a jack to a king
                     B
>From loneliness to a wedding ring
I played an ace and I won a queen
                         E
You made me king of your heart

SOLO
            ____          ______________      ______________
      :\   :    :        :    :    :    :    :    :    :    :
      :    :    :        :    :    :    :    :    :    :    :
:----12---12---12---:---12---11---11---11---11---------------------:
:-------------------:----------------------------14---14---14------:
:-------------------:----------------------------------------------:
:-------------------:----------------------------------------------:
:-------------------:----------------------------------------------:
:-------------------:----------------------------------------------:
   ___________     ___     _____           ___      ________
  :   :   :   :   :   :   :  :  :    :    :   :    :  :  :  :
  :   :   :   :   :   :   :  :  :    :    :   :    :  :  :  :
:----------------------------------:--------------------11-14-:-12-:
:-14--12--12--12--12---------------:----------12-----12-------:----:
:---------------------13--14-13-11-:------13------14----------:----:
:---------------------------\/-----:-14-----------------------:----:
:----------------------------P-----:--------------------------:----:
:----------------------------------:--------------------------:----:

REFRÃO
           A                                   E
For just a little while I thought that I might lose the game
             F#                             B
Then just in time I saw the twinkle in your eye

VERS0 4
(E)              E
>From a jack to a king
                     B
>From loneliness to a wedding ring
I played an ace and I won a queen
                         E
You made me king of your heart

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F# = 2 4 4 3 2 2
