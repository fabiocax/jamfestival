Elvis Presley - Separate Ways

E                  Emaj7
    I see a change is come in to our lives
  E7                        A
    It's not the same as it used to be
 F#m                        B7
    And it's not too late to realise our mistake
 F#m               F#m7/B  B        A   E
    We're just not right   for each o---ther

   E                     Emaj7
    Love has slipped away and left us only friends
  E7                    A
    We almost seem like strangers
   F#m                                 E
    All that's left between us are the memories we shared
           F#m7/B                   B         A   E
    Of the times we thought we cared for each o---ther

    E                                  Emaj7
    Now there's nothing left to do but go our separate ways
  E7                                  A
    And pick up all the pieces left behind us

 F#m                  E
    And maybe someday, somewhere along the way
 F#m7/B   B7             A    E    C#7  B7  F#m7  B7
       Another love will find us

    Some day when she's older, maybe she will understand
    Why her mom and dad are not together
    The tears that she will cry when I have to say goodbye
    Will tear at my heart forever

    There's nothing left to do but go our separate ways
    And pick up all the pieces left behind us
    And maybe someday, somewhere along the way
    Another love will find us

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
C#7 = X 4 3 4 2 X
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
Emaj7 = X X 2 4 4 4
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
F#m7/B = X 2 2 2 2 2
