Elvis Presley - I'll Hold You In My Heart

Standard tuning.

               G   (tacet)      G     (tacet)
    I said I'd hold, I said I'd hold,
               G              G7                C
    I said I'd hold you in my heart 'till I can hold you in my arms
                G                 D
    Like you've never been held before
              G                 G7                C
    Well I'll think of you each day and then I'll dream the night away
              G             D          G
    Oh, 'till you are in my arms once more

        C                                  G
    The stars up in the sky, you know they know the reason why
    Em        A                         D
    I feel so blue 'cause I'm away from you
              G              G7                C
    Yeah I'll hold you in my heart 'till I can hold you in my arms
       G       D               G
    Oh darling please wait for me


                 C                                  G
    You know the stars up in the sky, you know they know the reason why
    Em        A                                   D
    I feel so blue 'cause I'm away, I'm away from you
              G              G7                C
    Yeah I'll hold you in my heart 'till I can hold you in my arms
       G       D               G
    Oh darling please wait for me

        C                                 G
    The stars up in the sky you know they know the reason why...
    Em        A                                                       D
    I feel so blue 'cause I'm away, I'm away, I'm away, I'm away from you
                       G              G7
    I said, I said I'd hold you in my heart, heart, heart 'till I can hold you in my arms
       G       D               G
    Oh darling please wait for me

        C                                  G
    The stars up in the sky, you know they know the reason why
       Em        A                                   D
    I, I feel so blue 'cause I'm away, I'm away from you
               G              G7
    Yeah, I'll hold you in my heart 'till I can hold you in my arms
       G       D               G
    Oh darling please wait for me

----------------- Acordes -----------------
A = X 0 2 2 2 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
