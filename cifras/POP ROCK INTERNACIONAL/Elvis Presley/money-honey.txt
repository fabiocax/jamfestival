Elvis Presley - Money Honey

E
You know, the landlord rang my front door bell.
E
I let it ring for a long, long spell.
E
I went to the window,
E
I peeped through the blind,
E (Tacet)
And asked him to tell me what's on his mind.

A
He said, Money, honey, a hah hah.
E
Money, honey.
       B7            A                      E
Money, honey, if you want to get along with me.

Well, I screamed and I hollered,
I was so hard-pressed.
I called the woman that I loved the best.

I finally got my baby about half past three,
She said I'd like to know what you want with me.

I said, Money, honey, a hah hah.
Money, honey.
Money, honey, If you want to get along with me.

Well, I said tell me baby, what's wrong with you?
From this day on our romance is thru
I said tell me baby face to face
How could another man, take my place

She said, Money, honey, a hah hah.
Money, honey.
Money, honey, If you want to get along with me.

Well, I've learned my lesson and now I know
The sun may shine and the winds may blow.
The women may come and the women may go,
But before I say I love you so,

I want Money, honey, a hah hah.
Money, honey.
Money, honey, If you want to get along with me.

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
