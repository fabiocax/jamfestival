Elvis Presley - Ku-u-i-po

         D            Bm
Ku-u-i-po I love you more today
D               Bm
more today than yesterday
D               G
But I love you less today
A7          A    Fm   D
Less than I will tomorrow
D                      G
See the sweet Hawaiian rose
A7                   D
See it blossom see it grow
D                       G
That's the story of our love
A    A7            D
Ever since we said hello
         D            Bm
Ku-u-i-po I love you more today
D               Bm
more today than yesterday
D               G
But I love you less today

A7          A    Fm   D
Less than I will tomorrow

As the years go passing by
We'll recall our wedding day
I will be there by your side|------>repete as cifras do começo
You will always hear me say
           D          Bm
Ku-u-i-po I love you more today
D               Bm
more today than yesterday
D               G
But I love you less today
A7          A    Fm   D
Less than I will tomorrow

D         G         A         A7       Fm    D
Ku-u-i-po Ku-u-i-po you're my Hawaiian sweetheart
D         G         A         A7       Fm    D
Ku-u-i-po Ku-u-i-po you're my Hawaiian sweetheart

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
