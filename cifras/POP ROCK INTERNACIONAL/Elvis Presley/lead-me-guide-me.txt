Elvis Presley - Lead Me, Guide Me

I am (C) weak and I (A7) need Thy (Dm) strength (Dm7) and (G7) power,
To (Dm) help me (G7) over my (C) weak(G7)est (C) hour.
Let me through the (A7) darkness Thy (Dm) face (A7) to (Dm) see, (Fm)(C)
Lead me, oh (G7) Lord lead (C) me. (F) (C)

Lead (C) me, guide me (D7) along the (G7) way,
(F) For if you (C) lead (Dm) me (C) I (E7) cannot (Am) stray. (Fm) (C)
Lord let me walk each day (Bb7) with (A7) Thee.(
Dm) Lead (C#dim) me, (Dm) oh (C) Lord (G7) lead (C) me. (F)

(C)Help me tread in the paths of righteousness.
Be my aid when Satan and sin oppress.
I am putting all my trust in Thee:
Lead me, oh Lord lead me.

Chorus

I am lost if you take your hand from me,
I am blind without Thy light to see.
Lord just always te me they servant be,
Lead me, oh Lord lead me.


Chorus

----------------- Acordes -----------------
