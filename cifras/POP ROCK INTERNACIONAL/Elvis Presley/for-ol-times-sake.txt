Elvis Presley - For Ol' Times Sake

    Capo Casa 1

      A                Amaj7
    Before you go walk out on me
    Em          A              D
    Take a look around tell me what you see
    A                    Amaj7
    Here I stand like an open book
    Em                 A                   D
    Is there something here you might have overlooked
           D             E                   E
    'Cause it would be a shame if you should leave
                  Bm                 E                    A
    And find that freedom ain't what you thought it would be

    The years we had were not all bad
    In fact I know the good outweighed the bad
    Now you say that you've grown tired
    You want to be by yourself a while
    It would be a shame if you should go
    And find that freedom was a long time ago


    D                     E                     A
    I know that you can't stand the chains that bind you
    D                 E           A
    They just keep on drivin' us apart
    D                  E             A
    You could go where I could never find you
                  E       F#m      D                  E
    But could you go far enough to get away from your heart

    Solo:  A   Amaj7   Em   A   D

       A                  Amaj7
    So one more time, for ol' times sake
    Em                A            D
    Come and lay your head upon my chest
    A                       Amaj7
    Please don't throw this moment away
    Em                A                D
    We can forget the bad and take the best
    D                 E               A       E
    If you don't have nothing left to say
           Bm                E                  A
    Let me hold you one more time for ol' times sake

    D                 E               A       E
    If you don't have nothing left to say
           Bm                E                  A
    Let me hold you one more time for ol' times sake
  Amaj7
E|--0--
B|--2--
G|--1--
D|--2--
A --0--
E|-----

----------------- Acordes -----------------
Capotraste na 1ª casa
A*  = X 0 2 2 2 0 - (*A# na forma de A)
Amaj7*  = X 0 2 1 2 0 - (*A#maj7 na forma de Amaj7)
Bm*  = X 2 4 4 3 2 - (*Cm na forma de Bm)
D*  = X X 0 2 3 2 - (*D# na forma de D)
E*  = 0 2 2 1 0 0 - (*F na forma de E)
Em*  = 0 2 2 0 0 0 - (*Fm na forma de Em)
F#m*  = 2 4 4 2 2 2 - (*Gm na forma de F#m)
