Elvis Presley - If We Never Meet Again

            F           F7           Bb
Soon we'll come to the end of life's journey
        F                  C   C7
And perhaps never meet anymore
         F         F7              Bb
Till we gather in Heaven's bright city
      F           C          F
Far away on that beautiful shore

       F           F7                Bb
If we never meet again this side of Heaven
        F                                   C     C7
As we struggle through this world and its strife
          F              F7                 Bb
There's another meeting place somewhere in Heaven
        F           C        F
By the side of the river of life

            Bb                    F
Where the charming roses bloom forever
            F                  C    C7
And where separations come no more

       F           F7                Bb
If we never meet again this side of Heaven
      F                 C        F
I'll meet you on that beautiful shore

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
