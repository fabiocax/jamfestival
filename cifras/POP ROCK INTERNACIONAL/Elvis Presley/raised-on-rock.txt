Elvis Presley - Raised On Rock

A
I remember as a child I used to hear
D                            G     A
Music that they played Lord with a feel'
A
Some call it folk, some call it soul
D                            G         A
People let me tell you it was rock and roll
A
I was raised on rock, I got rhythm in my soul
      D                                  G        E
Every day when I got home I turned on my radi-o-o-o-o-o
Listening to the music that my idols made
I knew every single record the DJ's played
A honky tonk a Hound Dog, a Johnny B. Goode
Chain Gang, Love Is Strange, Knock On Wood
I was raised on rock, I got rhythm in my soul
I was born to love the beat I was made for rock and roll
I thought it was a fad, thought that it would pass
But the younger generation knew it would last
Time's gone by, the beat goes on

But every time I hear it Lord it takes me home
I was raised on rock, I got rhythm in my soul
Every day when I got home I turned on my radio
Mother played recordings of Beethoven's Fifth
Mozart's sonatas down the classical Liszt
My papa loved to listen to his country songs
While I was in the back room rockin' on
I was raised on rock, I got rhythm in my soul
I was born to love the beat I was made for rock and roll
I was raised on rock, I got rhythm in my soul
I was born to love the beat I was made for rock and roll
I was raised on rock, I got rhythm in my soul

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
