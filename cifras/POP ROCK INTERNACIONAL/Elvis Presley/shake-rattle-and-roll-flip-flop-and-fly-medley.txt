Elvis Presley - Shake, Rattle And Roll / Flip, Flop And Fly (Medley)

Intro: Strum E chord once.

[NC] E
Well get out of that kitchen and rattle those pots and pans
     A
Get out of that kitchen and rattle those pots and
E
pans
        B7                      A             E
Well I want my breakfast 'cause I'm a hungry man
  E
I believe you're doin' me wrong and now I know
  A                                        E
I believe you're doin' me wrong and now I know
           B7                          A      E
'cause the harder I work the faster my money goes

     E
Well I said shake, rattle and roll
        E
I said shake rattle and roll

        A
I said shake, rattle and roll
        E
I said shake rattle and roll
      B7
Well you won't do right
    A                 E
To save your doggone soul

I'm like the one-eyed cat peeping in a seafood store
I'm like the one-eyed cat peeping in a seafood store
Well I can look at you till you ain't no child no more

Well I said shake, rattle and roll
I said shake rattle and roll
I said shake, rattle and roll
I said shake rattle and roll
Well you won't do right
To save your doggone soul

I'm like a Mississippi bullfrog sittin' on a hollow stump
I'm like a Mississippi bullfrog sittin' on a hollow stump
I've got so many women I don't know which way to jump

I said flip flop and fly I don't care if I die
I said flip flop and fly I don't care if I die
I won't ever leave .. Don't ever say goodbye

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
