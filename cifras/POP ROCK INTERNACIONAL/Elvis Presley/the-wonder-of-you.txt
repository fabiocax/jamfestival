Elvis Presley - The Wonder Of You

[Intro] G  Em  Am  D

G                          Em       Am
When no-one else can understand me
                        D       G
When everything I do is wrong
                          Em     Am
You give me hope and consolation
                              D
You give me strength to carry on

     G                     G7
And you're always there to lend a hand
    C   D       Em
In everything I do
           Am      D               G  Em  Am  D
That's the wonder   the wonder of you

G                               Em       Am
And when you smile the world is brigther
                            D     G
You touch my hand and I'm a king

                           Em      Am
Your love for me is worth the fortune
                        D
Your love for me is everything

                G              G7
I'II guess I'II never know the reason why
    C       D        Em
You love me as you do
           Am      D               G  Em  Am  D
That's the wonder   the wonder of you

[Solo] G  Em  Am  D
       G  Em  Am  D

                G              G7
I'II guess I'II never know the reason why
    C       D        Em
You love me as you do
           Am      D               G    C  D#  G
That's the wonder   the wonder of you

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D# = X 6 5 3 4 3
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
