Elvis Presley - What Now My Love

[Intro] C

               C
What now my love
              F  C
Now that you left me
           Dm   G                 C
How can I live    through another day

            C                F  C
Watching my dreams turn into ashes
           Dm    G             C   C7
And all my hopes  into bits of clay

             F   G               Em   Am
Once I could see,   once I could feel
           Dm
Now I'm a numb
       G      C     C7
I've become unreal


           Fm    Bb                Eb    Ab
I walk the night,     oh, without a goal
               Fm    Dm     G
Stripped of my heart,    my soul

               C
What now my love
              F  C
Now that it's o-ver
           Dm    G              C
I feel the world   closing in on me

C    C     C   C
Here comes the stars
           F   C
Tumbling around me
                Dm  G                     C  C7
And there's the sky   where the sea should be

            F    G
What now my love
                Em   Am
Now that you're gone
         Dm       G       C   C7
I'd be a fool to go on and on

             Fm   Bb               Eb  Ab
No one would care,    no one would cry
            Fm   Dm    G
If I should live    or die

            C
What now my love
             F  C
Now there is nothing
        Dm   G       C
Only my last     goodbye
        Dm   G       C
Only my last     goodbye

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
