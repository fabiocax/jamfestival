Elvis Presley - Way Down

C
Babe, you're getting closer
The lights are goin' dim
The sound of your breathin'
Has made the mood I'm in
F
All of my resistance
Is lying on the floor
C
You're sending me to places
G                 C
I've never been before
               G
Ooh, and I can feel it,
Feel it, feel it, feel it
                               C
Way down where the music plays
                           G
Way down like a tidal wave
Way down where the fires blaze
    F    C   F   C
Way down,    down, way, way on down


Ooh, my head is spinnin'
You got me in your spell,
A hundred magic fingers
On a whirling carousel
    F
The medicine within me
No doctor could prescribe
     C
Your love is doing something
     G               C
That I just can't describe
               G
Ooh, and I can feel it,
Feel it, feel it, feel it
                               C
Way down where the music plays
                           G
Way down like a tidal wave
Way down where the fires blaze
    F    C   F   C
Way down,    down, way, way on down

Hold me again,
Tight as you can
I need you so,
Baby, let's go
G                                  C
   Way down where it feels so good
                                G
Way down where I hoped it would
Way down where I never could
    F    C   F   C
Way down,    down, way, way on down

----------------- Acordes -----------------
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
