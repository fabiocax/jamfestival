Elvis Presley - Welcome To My World

Intro:  Bm E  D

A               Bm               E           A
Welcome to my world.    Won't You come on in?
F#m          Bm     E               A
Miracles I guess   Still happen now and then.
          Bm             E          A
Step into my heart;    leave your cares behind
       Bm      E                 A        D   A
Welcome to my world    built with you in mind.
 E                   A                E              A
Knock and the door will open;  Seek and you will find
E            A       F#7             B                 E A
Ask and You'll be given  The key to this world of mine.
     Bm                 E       A             F#m
I'll be waiting here    with my arms unfurled,

 F#m                 Bm           E         D A
Waiting just for you;    Welcome to my world.

(Instrumental)


 F#m               Bm                     E  D A
Waiting just for you;    Welcome to my world.

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#7 = 2 4 2 3 2 2
F#m = 2 4 4 2 2 2
