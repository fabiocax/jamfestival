Elvis Presley - I'll Be There

Capo Casa 1

D                  G                   D
I'll be there when all your dreams are broken
D          Bm           Em      A
To answer your unspoken prayer, oh....
D                                         G              D
When the little things you're doin', ooh, don't turn out right
D          Bm    Em      A       D
Don't you worry darlin' I'll be there
G                   D           E                         A
There whenever you need to know that there is someone who cares, oh yeah!
  D                 G           D
So if your new love isn't your true love
D          Bm   Em       A       D
Don't you worry darlin', I'll be there

Solo: You can play along these chords:
D   G    D
D  Bm  Em  A
D   G   D
D  Bm  Em A


There whenever you need to know that there is someone who cares, oh yeah!
So if your new love isn't your true love
Don't you worry darlin', I'll be there
Don't you worry darlin', I'll be there
Don't you worry darlin', I'll be there

----------------- Acordes -----------------
Capotraste na 1ª casa
A*  = X 0 2 2 2 0 - (*A# na forma de A)
Bm*  = X 2 4 4 3 2 - (*Cm na forma de Bm)
D*  = X X 0 2 3 2 - (*D# na forma de D)
E*  = 0 2 2 1 0 0 - (*F na forma de E)
Em*  = 0 2 2 0 0 0 - (*Fm na forma de Em)
G*  = 3 2 0 0 0 3 - (*G# na forma de G)
