Elvis Presley - Any Day Now

Intro: Am   G   Em

    G      Bm
Any day now I will hear you say
    C
Goodbye my love
                  C#dim7
You'll be on your way
        G              B
Then my wild beautiful bird
              Em      C
You will have flown
    G                  Am
Any day now I'll be all alone
        G
Oh, oh, oh

        G  Bm
Any day now, when your restless eyes
     C
Meet someone new

              C#dim7
To my sad surprise
         G                B             Em    C
Then the blue shadow will fall all over town
    G
Any day now
              Am
Love will let me down
        G
Oh, oh, oh

Em
I know I shouldn't want to keep you
       Bm            C     D
If you don't want to stay, yeah
Em                   Bm
Until you've gone forever
        Em      Bm          Em
I'll be holding on for dear life
                 C
Holding you this way
C              G
Begging you to stay

    G
Any day now
Bm
When the clock strikes go
       C
You'll call it off
                   C#dim7
Then my tears will flow
         G                B             Em   C
Then the blue shadow will fall all over town
    G
Any day now
              Am
Love will let me down
        G
Oh, oh, oh

Oh, any day now
Em
Oh, oh, oh
G              Em
Oh, any day now
                            G      Em
Don't fly away my beautiful bird
                            G     Em
Don't fly away my beautiful bird

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C#dim7 = X 4 5 3 5 3
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
