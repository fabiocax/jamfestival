Elvis Presley - I Want To Be Free

Intro: F  C  F  C
           F         C           F          C
There's no joy in my heart, only sorrow
        F        C       F          C
And I'm sad as a man can be
      F            F7             Bb     Ddim7
I sit alone in the darkness of my lonely room
         F         C         F
And this room is a prison to me
[Chorus]
             C                       F
I look at my window and what to do I see
        C                F
I see a bird way up in a tree
             F    C    Bb               Dm
I want to be free free free - ee - ee - ee
             Dm6         C7          F
I want to be free like a bird in a tree
    F                C                F            C
Oh, what good are my eyes, they can't see you
       F                C           F            C
And my arms, they can't hold you so tight

           F             F7                    Bb         Ddim7
I have two lips that are yearning, but they're no good to me
        F            C        F
Cause I know I can't kiss you tonight

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Ddim7 = X X 0 1 0 1
Dm = X X 0 2 3 1
Dm6 = X 5 X 4 6 5
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
