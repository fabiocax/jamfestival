Elvis Presley - Burning Love

 D            G          A           D
Lord almighty i feel my temperature rising
 D                G               A           D
Higher and higher it's burning through to my soul
 D              G            A         D
Girl girl girl you're gonna set me on fire
 D                  G              A           D
My brain is flaming i don't know which way to go

 Bm          A       G
Your kisses lift me higher
       Bm           A        G
Like the sweet song of the choir
 Bm           A        G
You light my morning sky
      A       D
With burning love

D           G          A           D
Ooh ooh ooh i feel my temperature rising
 D                  G       A           D
Help me i'm flaming i must be a hundred and nine

 D             G          A           D
Burnin burnin burnin and nothing can cool me
D                  G        A     D
I just might turn to smoke but i feel fine

D                    G        A            D
It's coming closer the flames are licking my body
 D                G           A            D
Won't you help me i feel like i'm slipping away
D                    G       A     D
It's hard to breath my chest is a heaving
 D              G             A        D
Lord have mercy i'm burning the whole day

D                          G
I'm a hunka hunka burning love
D                          G
I'm a hunka hunka burning love
D                          G
I'm a hunka hunka burning love
D                          G
I'm a hunka hunka burning love

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
G = 3 2 0 0 0 3
