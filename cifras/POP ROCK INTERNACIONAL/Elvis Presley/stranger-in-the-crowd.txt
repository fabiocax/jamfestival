Elvis Presley - Stranger In The Crowd

       C
I've been standing on a corner
        Dm
Since a quarter after seven
      G
I was down to my last cigarette
        C
And the clock in the window
     Dm         G
At a quarter to eleven

   C
I was watching all the people
        Dm
Passing by me going places
         G
Just the loneliest guy in the town
        C              Am
Looking for a friendly smile
        Dm                    G
But all that I could see were faces


    C                                     G
And then, just like the taste of milk and honey
  G                                    C
I found the stranger I've been looking for
       C                            G
Like a wave, my cup of love was overflowing
  G
I knew the stranger in the crowd
                           C
And I would be stranger no more

    C
The love that comes on the corner
     Dm
At a quarter to eleven
          G
I thought you were to good to be true
      C
All my life I had believed
            Dm           G
That angels only live in heaven
    C                                   G
But now, we share the taste of milk and honey
     G                           C
Each day is sweeter than the day before

    C              G
My cup runned overflowing
            G
because the stranger in the crowd
                           C
And I would be stranger no more

(solo) C   Dm   G    C

     C                              G
Deep inside, my cup of love was overflowing
  G                                    C
I found the stranger I've been looking for
       C                            G
Like a wave, my cup of love was overflowing
           G
I knew the stranger in the crowd
                           C
And I would be stranger no more
           G
I knew the stranger in the crowd
                           C
And I would be stranger no more

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
G = 3 2 0 0 0 3
