Elvis Presley - You Gave Me a Mountain

[Intro]  C  Em  F  G#  G

C                          Am
Born in the heat of the desert
Dm                         G
My mother died giving me life
C                          Am
Deprived of the love of a father
Dm                         G
Blamed for the loss of his wife
C                          Am
You know Lord I've been in a prison
Dm                        G
For something that I never done
C                         Em
It's been one hill after another
F                        G
I've climbed them all one by one

C                     F                 C
Oh this time, Lord you gave me a mountain

            F             C     Bb  F    C
A mountain I may never climb
                 F           C
It isn't Lord a hill any longer
C                 F           C   Bb  F    C
You gave me a mountain this time

C                          Am
My woman got tired of the heartaches
Dm                         G
Tired of the grief and the strife
C                          Am
So tired of working for nothing
Dm                         G
Just tired of being my wife
C                          Am
She took my one ray of sunshine
Dm                         G
She took my pride and my joy
C                          Em
She took my reason for living
F                         G
She took my small baby boy

C                     F                 C
Oh this time, Lord you gave me a mountain
            F             C     Bb  F    C
A mountain I may never climb
                 F           C
It isn't Lord a hill any longer
C                 F           C
You gave me a mountain this time
C                F                    C   Em
You gave me a mountain ........ this time
       F    G#  G   C   A# F  C
Oh a mountain this time Oh Oh Oh

----------------- Acordes -----------------
A# = X 1 3 3 3 1
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
