Elvis Presley - A Fool Such As I

C          E7
Pardon me, if I'm sentimental
F               C
When we say goodbye
          C         A7         C7   G
Don't be angry with me should I cry
                C              E7
Now when you're gone, yet I'll dream a little
F               C
Dream as years go by
C                      G            C  C/Bb
Now and then, there's a fool such as I
         F                           C
Now and then there's a fool such as I am over you
   G              D
You taught me how to love
        G7
And now you say that we are through
    C           E7
I'm a fool but I love you dear
F                C
Untill the day I die

                      G            C
Now and then there's a fool such as I

[Solo]

        F                         C
Now and then there's a fool such as I am over you
   G             D
You taught me how to love
       G7
And now you say that we are through
    C               E7
I'm a fool, but I    love you dear
F                C
Untill the day I die
                      G            C
Now and then there's a fool such as I

                      G             C
Now and then there's a fool such as I

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
C = X 3 2 0 1 0
C/Bb = X 1 2 0 1 X
C7 = X 3 2 3 1 X
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
