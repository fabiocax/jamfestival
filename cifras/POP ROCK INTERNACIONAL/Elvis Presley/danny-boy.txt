Elvis Presley - Danny Boy

         C Cmaj7  C7                   F      Fm
Oh Danny boy, the pipes, the pipes are calling
             C        Am                D7  G
From glen to glen and down the mountain side
             C Cmaj7 C7             F    Fm
The summer's gone and all the roses dying
               C Am     Dm     G      C
'Tis you, 'tis you must go and I must bide

G   Am  G/B C         F               C
But come ye back when summer's in the meadow
G  Am   G/B Am       F          C          D   G
Or when the valley's hushed and white with snow
            C C/E   F              C Em/B Am Fm
And I'll be here in sunshine or in shadow
         C             Dm    G        C
Oh Danny boy, oh Danny boy I love you so

       C Cmaj7 C7                F    Fm
But if he come and all the roses dying
         C G/B    Am Am/G         D/F# G
And I am dead, as dead I well may be

                C Cmaj7 C7                F    Fm
You'll come and find the place where I am lying
              C  Am  F   G         C
And kneel and say an Ave there for me

G  Am G/B   C            F              C
And I shall feel, though soft you tread above me
G   Am  B/G Am Am/G    F     C/E       D/F# G
And then my grave will richer, sweeter be
             C C/E    F                C Em/B Am G#
For you will bend and tell me that you love me
            C       Am      Dm      G       C
And I shall rest in peace until you come to me
C    Am    F     G         C
 (Oh Danny boy I love) you so

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am/G = 3 X 2 2 1 X
B/G = 3 X 4 4 4 X
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
C7 = X 3 2 3 1 X
Cmaj7 = X 3 2 0 0 X
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Em/B = X 2 2 0 0 0
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
G/B = X 2 0 0 3 3
