Elvis Presley - Down In The Alley

E
Janie, Janie, Janie, Janie, Jane Jane

Janie, Janie, Janie, Janie, Jane Jane

Down in the alley, just you and me
                   A
We're going bowlin' till half past three
                 E                                 B7
Just rockin' and reelin', we'll get that feelin'
             E         A             E
Down in the alley, oh baby gee

I wake you now and dig you later
'cause you're a fine sweet potato
We'll have a ball and that ain't all
Down in the alley, just you and me

The clock is striking a mournful sound
This time of evening my love comes down
That's when I'm missin' your kind of kissin'

Down in the alley, that's where I'll be

Down in the alley, we sure have fun
We just get started 'bout half past one
So if you're 'round just come on down
Down in the alley and you will see

Janie, Janie, Janie, Janie, Jane Jane
Janie, Janie, Janie, Janie, Jane Jane
Janie, Janie, Janie, Janie, Jane Jane
Janie, Janie, Janie, Janie, Jane

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
