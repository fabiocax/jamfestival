Elvis Presley - Big Boss Man

 G
             G
    Big boss man, can't you hear me when I call?
             C                             G
    Big boss man, can't you hear me when I call?
    Spoken: "Can't you hear me when I call?"
             D            C                                 G
    Well you ain't so big, you know you're just tall that's all, All right
             G
    Well you got me workin' boss man
    G
    Workin' round the clock
    G
    I wanna little drink of water
    G
    But you won't let big Al stop
             C                                      G
    Big boss man now can't you hear me when I call? All right
               D            C                                 G
    I said you ain't so big, you know you're just tall that's all


    Big boss man, why can't you hear me when I call? All right
    You know you ain't so big, I said you're just tall that's all, All right
    I'm gonna get me a boss man
    One who's gonna treat me right
    I work hard in the day time
    Rest easy at night
    Big boss man, can't you hear me when I call? Can't you hear me when I call?
    I said you ain't so big, you're just tall that's all
    I'm gonna get me a boss man
    One that's gonna treat me right
    I work hard in the evenin'
    Rest easy at night
    Big boss man, big boss man, can't you hear me when I call?
    I said you ain't so big, you're just tall that's all
    All right, big boss man
    It's all right

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
