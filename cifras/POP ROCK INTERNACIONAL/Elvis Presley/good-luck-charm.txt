Elvis Presley - Good Luck Charm

C                     D7                G
Ah, ah, ah... ah, ah, ah... ah, ah, ah, oh yeah
G                       C
Don't want a four leaf clover
G                      D
Don't want an old horse shoe
G                      C
Want your kiss 'cause I just can't miss
      D                    G
With a good luck charm like you
           D7
Come on and be my little good luck charm
            G
Uh-huh huh, you sweet delight
         D7
I want a good luck charm

A|-hanging on my arm
    A                 D            G
To have, to have, to hold, to hold tonight
G                   C
Don't want a silver dollar

G                  D
Rabbit's foot on a string
G                C
The happiness in your warm caress
D                        G
No rabbit's foot can bring
           D7
Come on and be my little good luck charm
            G
Uh-huh huh, you sweet delight
       D7
I want a good luck charm

A|-hanging on my arm
  A                   D          G
To have, to have, to hold, to hold tonight
C                     D7                G
Ah, ah, ah... ah, ah, ah... ah, ah, ah, oh yeah

----------------- Acordes -----------------
A = X 0 2 2 2 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
