Elvis Presley - Words

G
Smile an everlasting smile
                      A
A smile can bring you near to me
       D7
Don't ever let me find you gone
                           C/G    G
'Cause that could bring a tear to me
      Bb
This world has lost it's glory,
                                F
Let's start a brand new story, now my love
       G
Right now there'll be no other time
                   A       D7
And I can show you how my love
G
Talk in everlasting words
                     A
And dedicate them all to me
      D7
I will give you all my life

                        C/G      G
It's here if you should call to me
     Bb
You think that I don't even mean
                D
A single word I say
          G                          D7
It's only words and words are all I have
                    G      D7
To take your heart away

** SOLO ** G - A - D7 - C/G - G

Bb
You think that I don't even mean
                D
A single word I say
          G                          D7
It's only words and words are all I have
                    G      D7
To take your heart away
G                          D7
It's only words and words are all I have
                    G      D7
To take your heart away

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bb = X 1 3 3 3 1
C/G = 3 3 2 X 1 X
D = X X 0 2 3 2
D7 = X X 0 2 1 2
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
