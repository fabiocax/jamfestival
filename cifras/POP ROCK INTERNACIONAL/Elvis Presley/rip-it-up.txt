Elvis Presley - Rip It Up

              A        (Tacet)
    'Cause it's Saturday nite and I just got paid
    A    (Tacet)
    Fool about my money don't try to save
       A     (Tacet)
    My heart says go, go
                            A        (Tacet)
    Have a time 'cause it's Saturday nite
    And I'm feelin' fine
              D9
    I'm gonna rip it up
              A
    I'm gonna rock it up
              D9
    I'm gonna shake it up
              A
    I'm gonna ball it up
              E9           D9    (Tacet)
    I'm gonna rip it up
               A
    And ball tonite

    I got a date and I won't be late
    Pick her up in my '88'
    Shag it on down to the union hall
    When the music starts jumpin'
    I'll have a ball
    Along about 10 I'll be flying high
    Rocking on out into the sky
    I don't care if I spend my gold
    'Cause tonite I'm gonna be one happy soul

----------------- Acordes -----------------
A = X 0 2 2 2 0
D9 = X X 0 2 3 0
