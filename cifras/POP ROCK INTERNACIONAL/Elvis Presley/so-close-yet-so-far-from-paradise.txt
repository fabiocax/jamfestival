Elvis Presley - So Close, Yet So Far (From Paradise)

Riff 1:
    C                 C+                C6                C+
e||------------------------------------------------------------------------|
B||------------------------------------------------------------------------|
G||------------------------------------------------------------------------|
D||-5--5--5--5--5--5--6--6--6--6--6--6--7--7--7--7--7--7--6--6--6--6--6--6-|
A||-3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3--3-|
E||------------------------------------------------------------------------|

    (Riff 1)                  (Riff 1)
    So close, yet so far from paradise
    (Riff 1)                   C
    I hold, you in my arms, in paradise
       Em             Am
    Is mine, then you slip away
           F        G7        C         Em
    Like a child at play, and here am I
       F      G7              (Riff 1)
    So close, yet so far from paradise


    When you, are close to me, it's paradise
    We kiss, oh my love, paradise
    Is mine, then suddenly you're gone from me
    Like a floating star, I see, and here am I
       F      G7              C       C7
    So close, yet so far from paradise

    F                   E7
    I reach out for you, but each time I do
    Am                 D9
    I always find you gone
    Am                F
    I know love is new, I'll take care of you
    D7                      G9
    In my arms is where you belong

    Just, a step away, are we from paradise
    Take my hand, come away, to paradise
    My love, I want you close to me
    I need you desperately, oh here am I
    Waiting for you
    Here am I
    Praying for you
    Here am I, so close, yet so far from paradise

Chords:
   D9     G9
E|--5--  --10--
B|--5--  --10--
G|--5--  --10--
D|--4--  --9---
A|--5--  --10--
E|-----  ------

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
C = X 3 2 0 1 0
C+ = X 3 2 1 1 X
C6 = 8 X 7 9 8 X
C7 = X 3 2 3 1 X
D = X X 0 2 3 2
D7 = X X 0 2 1 2
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
G9 = 3 X 0 2 0 X
