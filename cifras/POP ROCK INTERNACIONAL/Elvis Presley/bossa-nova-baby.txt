Elvis Presley - Bossa Nova Baby

Standard tuning.
    Intro: C
    C
    I said, 'Take it easy, baby
    I worked all day and my feet feel just like lead
    You got my shirt tails
    Flyin' all over the place
    And the sweat poppin' out of my head'
              F
    She said, 'Hey, Bossa nova, baby
    Keep on a workin' child
    This ain't no time to quit'
               C
    She said, 'Go, Bossa nova, baby
    Keep on dancin'
    I'm about to have myself a fit'
    Bossa nova, Bossa nova

    C   F   G   C

            C
    I said, 'Hey little mama,

    Let's sit down
    Have a drink and dig the band'
    She said, 'Drink, drink, drink
    Oh, fiddle-de-dink
    I can dance with a drink in my hand'
             F
    She said 'Hey Bossa nova, baby
    Keep on workin' child
    This ain't no time to drink'
              C
    She said 'Go, Bossa nova, baby
    Keep on dancin'
    'Cause I ain't got time to think'
    Bossa nova, Bossa nova

    C   F   G   C

    Solo...

             C
    I said, 'Come on, baby
    It's hot in here
    And it's oh so cool outside
    If you lend me a dollar
    I can buy some gas
    And we can go for a little ride'
               F
    She said, 'Hey Bossa nova, baby
    Keep on workin' child
    I ain't got time for that'
               C
    She said, 'Go Bossa nova, baby
    Keep on dancin'
    Or I'll find myself another cat'
    Bossa nova, Bossa nova

    C   F   G   C

    Bossa nova, Bossa nova

    C   F   G   C

    Bossa nova, Bossa nova

----------------- Acordes -----------------
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
