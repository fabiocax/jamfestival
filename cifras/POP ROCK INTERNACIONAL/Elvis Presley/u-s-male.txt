Elvis Presley - U.S. Male

   Afinação Meio Tom Abaixo

   A                 Bm
    "I'm a U.S. Male 'cause I was born"
       E                   A
    "In a Mississippi town on a Sunday morn"
        A7                           D
    "Now Mississippi just happens to occupy a place"
                         E                    A
    "In the southeastern portion of this here United States"
     A7
    "Now that's a matter of fact, buddy"
         D
    "And you know it well"
          E                         A7
    "So I just call myself the U.S. Male"
            A7
    "That's M-A-L-E, son. That's me!"
    Now I said all that to say all this
    I've been watchin' the way
    You've been watchin' my miss
    For the last three weeks you been hot on her trail

    And you kinda upset this U.S. Male
    You touch her once with your greasy hands
    I'm gonna stretch your neck like a long rubber band
    She's wearin' a ring that I bought her on sale
    And that makes her the property of this U.S. Male
        D7                                    A7
    You better not mess with the U.S. Male my friend
        D7                                    A7
    The U.S. Male gets mad, he's gonna do you in
        D7                       A7
    You know what's good for yourself son
               D7            A7
    You better find somebody else son
          Bm                         E7        A7
    Don't tamper with the property of the U.S. Male
    Through the rain and the heat and the sleet and the snow
    The U.S. Male is on his toes
    Quit watchin' my woman, for that ain't wise
    You ain't pullin' no wool over this boy's eyes
    I catch you 'round my woman, champ
    I'm gonna leave your head 'bout the shape of a stamp
    Kinda flattened out, so you'll do well
    To quit playin' games with this U.S. Male
    You better not mess with the U.S. Male my friend
    The U.S. Male gets mad, he's gonna do you in
    You know what's good for yourself son
    You better find somebody else son
    Don't tamper with the property of the U.S. Male
    Sock it to me
    All right...now I'm gonna tell it like it is son
    I catch you messin' 'round with that woman of mine
    I'm gonna lay one on ya. You're talkin' to the U.S. Male
    The American U.S. Male

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
