Elvis Presley - Known Only To Him

      G    D7  G          C             G
Known only to him are the great hidden secrets
      G           Em       A7                  D7
I'll fear not the darkness when my flame shall dim
       G                   G7
I know not what the future holds
      c
But I know who holds the future
        G            D7     G       C    G
It's a secret known only to Him
        G
In this world of fear and doubt
      G7               C
On my knees I ask the question
       G                  Em      A7      D7
Why a lonely, heavy cross I  must bear
         G
Then he tells me in my prayer
     G7                 C
It's because I am trustworthy
         G           D7                G    C   G
He gives me strength far more than my share

Known only to him are the great hidden secrets
I'll fear not the darkness when my flame shall dim
I know not what the future holds
But I know who holds the future
It's a secret known only to Him

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
C = X 3 2 0 1 0
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
