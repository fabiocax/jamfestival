Elvis Presley - Hawaiian Wedding Song

Afinação Meio Tom Abaixo

Intro: A
    A
    This is the moment
    D           A
    I've waited for
    A
    I can hear my heart singing
         B             E
    Soon bells will be ringing
    A           B
    This is the moment
    E         A
    Of sweet Aloha
    A                              E
    I will love you longer than forever
    E                                 A
    Promise me that you will leave me never
    A        B
    Here and now dear,

    E
    You're my love,
      A
    I know dear

    Promise me that you will leave me never
    I will love you longer than forever
    Now that we are one
    Clouds won't hide the sun
    Blue skies of Hawaii will smile
    On this, our wedding day
    I do love you with all my heart

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
