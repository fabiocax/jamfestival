Elvis Presley - Unchained Melody


    C       Am       F
    Oh, my love, my darling
          G7                C        Am          G   G7
    I've hungered for your touch, a long lonely time
         C         Am      F         G7             C
    And time goes by, so slowly and time can do so much
         Am         Em    G
    Are you, still mine?
    G7   C         G6   Am7     C
    I   need your love, I need your love
          Dm        G7    C  C7
    God speed your love  to me

     F              G          F           Eb
    Lonely rivers flow to the sea, to the sea
     F             G            C  C7
    To the waiting arms of the sea
     F              G            F            Eb
    Lonely rivers cry, wait for me, wait for me
    F               G              C
    I'll be coming home, wait for me


            Am       F             G7                C
    Oh, my love, my darling, I've hungered for your kiss
      Am           Em   G
    Are you still mine?
    G7  C             G6  Am7      C
    I need your love, I need your love
           Dm              G7  C
    God, speed your love, to  me

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G6 = 3 X 2 4 3 X
G7 = 3 5 3 4 3 3
