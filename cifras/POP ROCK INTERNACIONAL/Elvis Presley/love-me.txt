Elvis Presley - Love Me

Intro: C

                F
Treat me like a fool
                  F7        Bb
Treat me mean and cruel But love me
                  C
Wring my faithful heart
               C7     F      Bb C
Tear it all apart But love me

            F
If you ever go Darling,
        F7    Bb
I'll be oh so lonely
                C
I'll be sad and blue
            C7        F    F7
Crying over you, dear only

                Bb
I would beg and steal

         F F7      G                     C  C7
Just to feel your heart Beatin' close to mine

                  F
Well, if you ever go
                 F7    Bb
Darling, I'll be oh so lonely
                C
I'll be sad and blue
              C7      F  F7
Crying over you, dear only

                Bb
I would beg and steal
         F F7      G                     C  C7
Just to feel your heart Beatin' close to mine

                  F
Well, if you ever go
                 F7    Bb
Darling, I'll be oh so lonely
           C
Beggin' on knees
              C7             F
All I ask is please, please love me

Bb  F
Oh yeah!

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
G = 3 2 0 0 0 3
