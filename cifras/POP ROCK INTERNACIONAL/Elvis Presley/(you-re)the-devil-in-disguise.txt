Elvis Presley - (You're The) Devil In Disguise

introd: Batida Rapida (Bb C F)  Segura em C

     F
You look like an angel, you walk like an angel,
     Bb                            C
you talk like an angel, but I got wise.
                        F                  Dm
You`re the devil in disguise, oh yes, you are,
              F       Dm
devil in disguise, hmmmm.

   F
1.   You fooled me with your kisses,
   Dm
    you cheated and you schemed.
   F                      Dm
    Heaven knows how you lied to me,
            Bb       C       F
    you`re not the way you seemed.

   F
2.   I thought I was in heaven,

   Dm
    but I was sure surprised.
   F                    Dm
    Heaven help me, I didn`t see,
         Bb     C       F
    the devil in your eyes.

             F                  Dm
devil in disguise, oh yes, you are

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
