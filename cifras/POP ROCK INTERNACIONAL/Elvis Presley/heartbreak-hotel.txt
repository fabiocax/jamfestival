Elvis Presley - Heartbreak Hotel

       E
Well, since my baby left me
I found a new place to dwell
It's down at the end of lonely street at
 E7
Heartbreak Hotel

A
  You make me so lonely baby, I get so lonely
B           B7             E
  I get so lonely I could die

        E
And although it's always crowded
You still can find some room
For broken-hearted lovers
 E7
To cry away their gloom

A
  You make me so lonely baby, I get so lonely,

B             B7           E
  I get so lonely I could die

           E
Well, the Bell hop's tears keep flowin'
And the desk clerk's dressed in black
Well they been so long on lonely street
     E7
They ain't ever gonna look back

A
  You make me so lonely baby, I get so lonely
B             B7           E
  I get so lonely I could die
              E
Well now, if your baby leaves you
And you got a tale to tell
Just take a walk down lonely street to
 E7
Heartbreak Hotel

A
  You make me so lonely baby, I get so lonely
B             B7           E
  I get so lonely I could die

        E
And although it's always crowded
You still can find some room
For broken-hearted lovers
 E7
To cry away their gloom

A
  You make me so lonely baby, I get so lonely
B           B7             E
  I get so lonely I could die

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
