Elvis Presley - Anything That's Part Of You

Intro:   D    A    D    G    D

   (tacet)                 D      Dmaj7      D7
   I memorize the note you sent
                            G
   Go all the places that we went
                                 D          A
   I seem to search the whole day through
          A7                   D      G      D
   For anything that's part of you

   (tacet)                   D      Dmaj7      D7
   I kept a ribbon from your hair
                              G
   A breath of perfume lingers there
                                D       A
   It helps to cheer me when I'm blue
                           D     G     D
   Anything that's part of you

   (tacet)                      G

   Oh, how it hurts to miss you so
          A7             D          G
   When I know you don't love me anymore
            D
   To go on needing you
                          A    A7
   Knowing you don't need me

   (tacet)                  D     Dmaj7    D7
   No reason left for me to live
                              G
   What can I take, what can I give
                               D       A
   When I'd give all of someone new
          A7                   D     G     D
   For anything that's part of you

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
