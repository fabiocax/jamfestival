Pink Floyd - The Gold It's In The...

Intro: E  D  A (2x)

E                           D            A
Come on, my friends, let's make for the hills
      E                        D            A
They say there's gold but I'm looking for thrills
         E                  D          A
You can get your hands on whatever we find
           E                          D    A
'Cause I'm only coming along for the ride

       B7
Well, you go your way, I'll go mine
G
I don't care if we get there on time
A
Everybody's searching for something, they say
F
I'll get my kicks on the way

E                      D        A
Over the mountains, across the seas

E                       D         A
Who knows what will be waiting for me?
         E                D             A
I could sail forever to strange sounding names
 E                    D         A
Faces of people and places don't change

B7
All I have to do is just close my eyes
G
To see the seagulls wheeling in those far distant skies
A
All I want to tell you, all I want to say
F
is count me in on the journey, don't expect me to stay

Solo: E   D A  (4x)
B7  G  A  F (2x)
E   D A  (4x)
B7  G  A  F
E   D A (fade out)

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
