Pink Floyd - Poles Apart

Intro: G  D  G  D

        G                                 D
Did you know... it was all going to go so wrong for you
            G                             D
And did you see it was all going to be so right for me
F#m Em  D  C#m  Bm  A
Why did we tell you then
F#m Em   D           C#m   Bm  A
You were always the golden boy then
F#m       Em          D         G             D
And that you¹d never lose that light in your eyes

    G                                D
Hey you... did you ever realize what you'd become
            G                                   D
And did you see that it wasn't only me you were running from
F#m     Em   D       C#m  Bm  A
Did you know all the time but it
F#m Em  D      C#m Bm A
Never bothered you anyway

F#m          Em            D              G             D
Leading the blind while I stared out the steel in your eyes

( C#m  D  C#m  E  C#m  D  Bm  E  D )
( Bb  Am  Bm  A  D )

              G                                D
The rain fell slow, down on all the roofs of uncertainty
             G                                          D
I thought of you and the years and all the sadness fell away from me
            G          D
And did you know...

( F#m  Em  D  C#m  Bm  A )
( F#m  Em  D  C#m  Bm  A )

F#m       Em               D         G             D
I never thought that you'd lose that light in your eyes

( G  D  G  D )

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
