Pink Floyd - Crying Song

   D          D(-5)
We smile and smile
   D          D(-5)
We smile and smile
D                       G      Em  D
Laughter echoes in your eyes
   D           D(-5)
We climb and climb
   D           D(-5)
We climb and climb
D                        G      Em  D
Foot falls softly in the pines

   D       D(-5)
We cry and cry
   D       D(-5)
We cry and cry
D                   G      Em  D
Sadness passes in a while
   D        D(-5)
We roll and roll

   D        D(-5)
We roll and roll
D                     G     Em  D
Help me roll away the stone

Riff: tocado logo após o final de cada verso

D---------------------0---
A-0-4-0-5-0-6-0-7-0-8-----

----------------- Acordes -----------------
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
