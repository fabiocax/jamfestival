Pink Floyd - Wearning The Inside Out

   Cm7                 Eb7+
From morning to night I stayed out of sight
Bb7+                     F
Didn't recognize I'd become
Cm7                Eb7+
No more than alive I'd barely survive
Bb7+          F
In a word...overrun

      Gm            F
Won't hear a sound from my mouth
       Gm            F
I've spent too long on the inside out
  Gm            F
My skin is cold to the human touch
        Gm            F
This bleeding heart's not beating much

Cm7              Eb7+
I murmured a vow of silence and now
Bb7+                   F
I don't even hear when I think aloud

Cm7                 Eb7+
Extinguished by light I turn on the night
 Bb7+                   F
Wear its darkness with an empty smile

Fm
I'm creeping back to life
   G4      G        Ab
My nervous system all awry
               Bb
I'm wearing the inside out

Cm7               Eb7+
Look at him now, he's paler somehow
Bb7+                 F
  But he's coming around
Cm7                     Eb7+
He's starting to choke, it's been so long since he spoke
Bb7+                     F
Well he can have the words right from my mouth

Gm              F
And with these words I can see
Gm                 F
Clear through the clouds that covered me
Gm                 F
Just give it time then speak my name
Gm                 F
Now we can hear ourselves again

Gm                 F
I'm holding out for the day
Gm                 F
When all these clouds have blown away
Gm                 F
I'm with you now, can speak your name
Gm                     F
Now we can hear ourselves again

final:Cm7        Eb7+
      Bb7+        F

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Bb = X 1 3 3 3 1
Bb7+ = X 1 3 2 3 1
Cm7 = X 3 5 3 4 3
Eb7+ = X X 1 3 3 3
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
G4 = 3 5 5 5 3 3
Gm = 3 5 5 3 3 3
