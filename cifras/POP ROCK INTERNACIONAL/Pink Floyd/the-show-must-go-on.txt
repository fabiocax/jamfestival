Pink Floyd - The Show Must Go On

(intro) G  C  G  C  G  C

D      C                          G     C  G
Oooh Ma Oooh Pa, must the show go on ?
D    Bm6
Oooh Pa take me home
D    Bm6
Oooh Ma let me go
Cmaj7
There must be some mistake, I didn't mean to let them
                                          G     C  G
Take away my soul, Am I too old is it too late ?
D      G  C
Oooh Ma   Oooh Pa
G
Where has the feeling gone?
D      G  C
Oooh Ma   Oooh Pa
Will I remember the songs?
D  G C                     G#/C  G
Oooh ah - The show must go on.

----------------- Acordes -----------------
Bm6 = 7 X 6 7 7 X
C = X 3 2 0 1 0
Cmaj7 = X 3 2 0 0 X
D = X X 0 2 3 2
G = 3 2 0 0 0 3
G#/C = X 3 X 1 4 4
