Pink Floyd - See Emily Play

G                D         A        Em
Emily tries but misunderstands, ah ooh
C7+
She often inclined to borrow
Am
somebody's dreams
      G
till tomorrow

(Da uma paradinha)
              E      D
There is no other day
               E        D
Let's try it another way
                   E          D
You'll lose your mind and play
     A
Free games for may
            G
See Emily play


(efeitos - uma sacada que reproduz como se fosse alguem brincando mesmo)

G                D     A       Em
Soon after dark Emily cries, ah ooh
C7+
Gazing through trees in
         Am
sorrow hardly
                 G
a sound till tomorrow

(repete a paradinha)

              E      D
There is no other day
                E       D
Let's try it another way
                  E           D
You'll lose your mind and play
       A
Free games for may
            G
See Emily play

(efeitos, mas dessa vez diferentes)

G                    D            Am       Em
Put on a gown that touches the ground, ah ooh
C7+                 Am               G
Float on a river forever and ever, Emily

              E      D
There is no other day
               E        D
Let's try it another way
                   E          D
You'll lose your mind and play
       A
Free games for may
           G  D
See Emily plaay

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
C7+ = X 3 2 0 0 X
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
