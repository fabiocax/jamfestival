Pink Floyd - One Slip

Eb                            Gm4
A restless eye across a weary room
Eb                                       Fsus4
A glazed look and I was on the road to ruin
    C                                              Eb6  Eb
The music played and played as we whirled without end
F                       Ab9
No hint, no word her honour to defend

Eb                                Gm4
I will, I will she sighed to my request
                 Eb                                             Fsus4 F Fsus4
And then she tossed her mane while my resolve was put to the test
     C                                       Eb
Then drowned in desire, our souls on fire, I lead the way to the funeral pyre
        F
And without a thought of the consequence, I gave into my decadence

C9                          Eb7M/Bb
One slip, and down the hole we fall
   Bb9                  Gm9
It seems to take no time at all

  C9                            Eb7M/Bb
A momentary lapse of reason, that binds a life for life
    Bb9                               Gm9
A small regret, you won't forget, there'll be no sleep in here tonight.

( Eb  Gm/D  Gm  Eb  Gm/D  Gm )

Eb                                       Gm4
Was it love, or was it the idea of being in love?
              Eb                                              Fsus4  F  Fsus4
Or was it the hand of fate, that seemed to fit just like a glove?
    C                                         Eb
The moment slipped by and soon the seeds were sown
    F
The year grew late and neither one wanted to remain alone
C9                          Eb7M/Bb
One slip, and down the hole we fall.
   Bb9                  Gm9
It seems to take no time at all
  C9                            Eb7M/Bb
A momentary lapse of reason, that binds life to a life
    Bb9                                   Gm9
The one regret you will never forget, there'll be no sleep in here tonight

( Eb  Gm/D  Gm  Eb  Gm/D  Gm )

----------------- Acordes -----------------
Ab9 = 4 6 8 5 4 4
Bb9 = X 1 3 3 1 1
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
Eb = X 6 5 3 4 3
Eb6 = X X 1 3 1 3
Eb7M/Bb = 6 6 5 3 3 3
F = 1 3 3 2 1 1
Fsus4 = 1 3 3 3 1 1
Gm = 3 5 5 3 3 3
Gm/D = X 5 5 3 3 3
Gm4 = 3 X 0 3 1 X
Gm9 = 3 5 7 3 3 3
