Pink Floyd - Welcome To The Machine

INTRO:
                       ^
    [Synthesizers]    Emadd9
E|-|--------------||----0-----------------------------------|
B|-|--------------||----0-----------------------------------|
G|-|--------------||----0-----------------------------------|
D|-|--------------||----4-----------------------------------|
A|-|--------------||----2-----------------------------------|
E|-|--------------||----0-----------------------------------|
                       ^                  ^
       Cmaj7                             Emadd9
E|-|-----0---------------------------|-----0----------------------------|
B|-|-----0---------------------------|-----0----------------------------|
G|-|-----0---------------------------|-----0----------------------------|
D|-|-----2---------------------------|-----4----------------------------|
A|-|-----3---------------------------|-----2----------------------------|
E|-|---------------------------------|-----0----------------------------|
                                          ^
VERSE:
        Em
E|-|-----0---------------------------|
B|-|-----0---------------------------|
G|-|-----0---------------------------|
D|-|-----2---------------------------|
A|-|-----2---------------------------|
E|-|-----0---------------------------|

                     Welcome my son,

 C                  Em
welcome to the machine.
(C)
Where have you been?
 Am                                   Em
It's all right, we know where you've been.
 C
You've been in the pipeline filling in time,
 Em
Provided with toys and scouting for boys.
 C
You bought a guitar to punish your ma,
 Em
And you didn't like school,

And you know you're nobody's fool.

      Em         Cmaj7                         Em
E|-|---0--0--0-----0--0--0--0---0--0--0--0------0---|
B|-|---0--0--0-----0--0--0--0---0--0--0--0------0---|
G|-|---0--0--0-----0--0--0--0---0--0--0--0------0---|
D|-|---2--2--2-----2--2--2--2---2--2--2--2------2---|
A|-|---2--2--2-----3--3--3--3---3--3--3--3------2---|
E|-|---0--0--0----------------------------------0---|
              So welcome    to   the    machine.

Strums:
Em  Cmaj7  Em  Cmaj7
Em  Cmaj7  Em  Cmaj7

SOLO:
     Em                         Emadd9
E|-|------0-0-0-0-----0-0-0-0-|-----0-0-0-0---0-0-0-0-|-----0-0-0-0---0-0-0-0--|
B|-|------0-0-0-0-----0-0-0-0-|-----0-0-0-0---0-0-0-0-|-----0-0-0-0---0-0-0-0--|
G|-|------0-0-0-0-----0-0-0-0-|-----0-0-0-0---0-0-0-0-|-----0-0-0-0---0-0-0-0--|
D|-|--2-----------2-----------|-2/4---------4---------|-4/5---------5----------|
A|-|--------------------------|-----------------------|------------------------|
E|-|--------------------------|-----------------------|------------------------|

E|-|--------0-0-0-0-----0-0-0---| 
B|-|--------0-0-0-0-----0-0-0---|
G|-|--------0-0-0-0-----0-0-0---|
D|-|--5/7-----------7-----------|
A|-|----------------------------|
E|-|----------------------------|

Strums:
Cmaj7  Emadd9    Cmaj7  Emadd9
Cmaj7  Emadd9    Cmaj7  Enadd9

Repeat Solo

VERSE:
      ^                                                         ^
      Em                               Cmaj7                    Em
E|-|---0----------------------------|----0-----------------------0---------|
B|-|---0----------------------------|----0-----------------------0---------|
G|-|---0----------------------------|----0-----------------------0---------|
D|-|---2----------------------------|----2-----------------------2---------|
A|-|---2----------------------------|----3-----------------------2---------|
E|-|---0----------------------------|----------------------------0---------|
            Welcome  my son,            Welcome to the machine. ^

     Cmaj7                            A                              Emadd9
E|-|---0----------------------------|-0---------------------------------0----|
B|-|---0----------------------------|-2---------------------------------0----|
G|-|---0----------------------------|-2---------------------------------0----|
D|-|---2----------------------------|-2---------------------------------4----|
A|-|---3----------------------------|-0---------------------------------2----|
E|-|--------------------------------|-----------------------------------0----|
      What did you dream?           It's all right we told you what to dream.

     Cmaj7                             Em
E|-|---0----------------------------|---0-----------------------------------|
B|-|---0----------------------------|---0-----------------------------------|
G|-|---0----------------------------|---0-----------------------------------|
D|-|---2----------------------------|---2-----------------------------------|
A|-|---3----------------------------|---2-----------------------------------|
E|-|--------------------------------|---0-----------------------------------|
      You dreamed of a big star.       He played a mean guitar.

     Cmaj7                            Emadd9
E|-|---0----------------------------|---0-----------------------------------|
B|-|---0----------------------------|---0-----------------------------------|
G|-|---0----------------------------|---0-----------------------------------|
D|-|---2----------------------------|---4-----------------------------------|
A|-|---3----------------------------|---2-----------------------------------|
E|-|--------------------------------|---0-----------------------------------|
   He always ate atthe steak bar,   He loved to drive in his Jaguar.

     Cmaj7
E|-|---0-----------------------0-----||
B|-|---0-----------------------0-----||
G|-|---0-----------------------0-----||
D|-|---2-----------------------4-----||    FADE OUT
A|-|---3-----------------------2-----||
E|-|---------------------------0-----||
   So welcome   to  the    machine.

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Cmaj7 = X 3 2 0 0 X
Em = 0 2 2 0 0 0
Emadd9 = 0 2 4 0 0 0
