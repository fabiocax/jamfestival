Pink Floyd - Terrapin

*música da carreira solo de Syd Barrett.

VERSE 1:
E                      G
I really love you, and I mean you
E                   G       A
The star above you, crystal blue
      D           E                   A
Well, oh baby, my hair's on end about you

C G

VERSE 2:
I wouldn't see you, and I love to
I fly above you, yes I do
Well, oh baby, my hair's on end about you

C G G# A

BRIDGE:
A
Floating, bumping, noses dodge a tooth

A                        C G Bb E
The fins a luminous
A
Fangs all 'round the clown is dark
A
Below the boulders hiding
       C                   G  E
All the sunlight's good for us

VERSE 3:
Cause we're the fishes and all we do
The move about is all we do
Well, oh baby, my hair's on end about you

(Solo over verse chords)

BRIDGE:
A
Floating, bumping, noses dodge a tooth
A                        C G Bb E
The fins a luminous
A
Fangs all 'round the clown is dark
A
Below the boulders hiding
       C                   G  E
All the sunlight's good for us

(Solo over verse chords)

E                      G
I really love you, and I mean you
E                   G       A
The star above you, crystal blue
      D           E                   A
Well, oh baby, my hair's on end about you

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
