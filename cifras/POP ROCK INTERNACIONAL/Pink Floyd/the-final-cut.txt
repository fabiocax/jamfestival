Pink Floyd - The Final Cut

F                                          F/C   C
Through the fish-eyed lens of tear stained eyes____
      Bbadd9                                   F
I can barely define the shape of this moment in time
    F                                  F/C   C
And far from flying high in clear blue skies___,
    Bbadd9                                           F
I'm spiralling down to the hole in the ground where I hide

Bb                   F ::
If you negotiate the minefield in the drive, ::
 ::
Bb                                   F ::
And beat the dogs and cheat the cold electronic eyes, ::
 ::
Bb         C                    Dm ::
And if you make it past the shotgun in the hall, ::
 ::
Gm7 ::
Dial the combination, open the priesthole ::
 ::

                    C                                 F ::
And if I'm in I'll tell you what's behind the wall. ::
Am                                   F
There's a kid who had a big hallucination
Am                          C
Making love to girls in magazines
   Bb                                   Dm
He wonders if you're sleeping with your new found faith
Gm7
Could anybody love him
                / C          F      F/C  C  Bbadd9  F
Or is it just a crazy dream___?
    F                     C
And if I show you my dark side
                    Bb   F
Will you still hold me tonight?
    F            C
And if I open my heart to you
                     Bb
And show you my weak side,
               F
What would you do?
Bb                                   F
Would you sell your story to Rolling Stone? ::
 ::
Bb ::
Would you take the children away ::
 ::
F ::
And leave me alone? ::
 ::
    Bb           C ::
And smile in reassurance ::
 ::
       Dm ::
As you whisper down the phone, ::
 ::
Gm7 ::
Would you send me packing, ::

 C                   F
Or would you take me home?

( Am  F  Am  C  Bb  Dm  Gm7  C  F )

Solo:

Lead guitar: lightly distorted

4/4
    H.                      Er  E   F
E|-13>(15)~~~~~~~~~~~~~~~~~----20-|17~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|
B|--------------------------------|-------------------------------|
G|--------------------------------|-------------------------------|
D|--------------------------------|-------------------------------|
A|--------------------------------|-------------------------------|
E|--------------------------------|-------------------------------|

   Q       Q       H               Q       E   E   H
E|---------13------13>(15)~~~~~~~~|13------12 p10-----------------|
B|-13-----------------------------|----------------13~~~~~~~~~~~~~|
G|--------------------------------|-------------------------------|
D|--------------------------------|-------------------------------|
A|--------------------------------|-------------------------------|
E|--------------------------------|-------------------------------|

   Q        Q      Q       Q.          Q       E   H
E|-----------------12------13-----|----12-------------------------|
B|-13-------15--------------------|------------15--15~~~~~~~~-----|
G|--------------------------------|-------------------------------|
D|--------------------------------|-------------------------------|
A|--------------------------------|-------------------------------|
E|--------------------------------|-------------------------------|

   Q         Q.       E   Q        Q       Q.          E   Q
E|--------------------------------|-------------------------------|
B|-15--------13-------------------|-------------------------------|
G|--------------------14--14------|10------12----------14--12-----|
D|--------------------------------|-------------------------------|
A|--------------------------------|-------------------------------|
E|--------------------------------|-------------------------------|

   E   E   H.                      Q       Q       H
E|--------------------------------|15>(17)(18)====>15~~~~~~~~~~~~~|
B|--------------------------------|-------------------------------|
G|-14 p12--10~~~~~~~~~~~~~~~~~\---|-------------------------------|
D|--------------------------------|-------------------------------|
A|--------------------------------|-------------------------------|
E|--------------------------------|-------------------------------|

                                    E   Q.          E   Q.
E|---------13------13>(15)~~~~~~~~|13--13-12----------------------|
B|-13-----------------------------|----------------13--10---------|
G|--------------------------------|-------------------------------|
D|--------------------------------|-------------------------------|
A|--------------------------------|-------------------------------|
E|--------------------------------|-------------------------------|

   Q       H.
E|--------------------------------|
B|--------------------------------|
G|-14 p12--10~~~~~~~~~~~~~~~~~\---|
D|--------------------------------|
A|--------------------------------|
E|--------------------------------|

Am                                 F ::
Thought I oughta bare my naked feelings, ::
 ::
Am                                C ::
Thought I oughta tear the curtain down ::
 ::
  Bb ::
I held the blade in trembling hands, ::
 ::
   Dm                   Gm7 ::
Prepared to make it but just then the phone rang, ::

                                        G      C  Bbadd9  F
I never had the nerve to make the final cut

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bbadd9 = X 1 3 3 1 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
F/C = X 3 3 2 1 1
G = 3 2 0 0 0 3
Gm7 = 3 X 3 3 3 X
