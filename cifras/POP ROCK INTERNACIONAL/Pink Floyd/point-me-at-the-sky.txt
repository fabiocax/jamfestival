Pink Floyd - Point Me At The Sky

C               G                  F                     D
Hey Jean misses Henry McLean an' I finished my beautiful flying machine
        C                       G
An' I'm ringing to say that I'm leaving
                F                       D
An' maybe you'd like to fly with me and hide with me baby
C                    G                 F                    D
Isn't it strange how little we change, isn't it sad we're insane
C                        G
Playing the game that we know ends in tears
    F                           D
The game we've been playing for thousands and thousands and thousands
F#m                          A
Jumps into his cosmic flyer, pulls his plastic collar higher
D                                      C#
Light the fuse and stand well back, he cried, this is my last goodbye
D                              G
Point me at the sky and let it fly
D                              G
Point me at the sky and let it fly
D                              G   A
Point me at the sky and let it fly....


    C                        G                   F                       D
And if you survive 'till two thousand and five I hope you're exceedingly thin
    C                         G
For if you are stout you will have to breathe out
          F                         D
While the people around you breathe in, breathe in, breathe in
F#m                            A
People pressing on my sides is something that I hate
          D                             C#
And so is sitting down to eat with only little capsules on my plate
D                              G
Point me at the sky and let it fly
D                              G
Point me at the sky and let it fly
D                              G   A
Point me at the sky and let it fly...

----------------- Acordes -----------------
A = X 0 2 2 2 0
C = X 3 2 0 1 0
C# = X 4 6 6 6 4
D = X X 0 2 3 2
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
