Pink Floyd - Don't Leave Me Now

G   G+       Fm7
       Ooooh Babe
                     Dm7 (5-)
Don't leave me now.
                              Bb11
Don't say it's the end of the road
Remember the flowers I sent
        G+
I need you Babe
   G
To put through the shredder
In front of my friends
      Fm
Ooooh Babe
                    Dm7 (5-)
Don't leave me now
                   Bb11
How could you go?
                              G
When you know how I need you.
                        G+
To beat to a pulp on a Saturday night

      Fm
Ooooh Babe
             Fm(maj7)  Dm7 (5-)
Don't leave me now
                          Bb9
How can you treat me this way?
Running away
G
Ooooooh babe
                           Dm  Am  Dm  Am  Dm  Am  Dm  G7  Am
Why are you running away?                              Oooooooh

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb11 = X 1 3 3 4 1
Bb9 = X 1 3 3 1 1
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Fm = 1 3 3 1 1 1
Fm(maj7) = X X 3 5 5 4
Fm7 = 1 X 1 1 1 X
G = 3 2 0 0 0 3
G+ = 3 X 1 0 0 X
G7 = 3 5 3 4 3 3
