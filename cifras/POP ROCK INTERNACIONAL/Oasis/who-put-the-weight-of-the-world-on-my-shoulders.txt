Oasis - Who Put The Weight Of The World On My Shoulders?

G              Bm           G       Bm x2

G              Bm           G       Bm
Who put the weight of the world on my shoulders?
   G         Bm         G        Bm
Who put the lies in the truth that you sold us?
Am                      C                                         G         Bm
Lost behind a silver screen are all the things you could have been to us.

G              Bm           G       Bm
So don't try and fuck up my head with your problems
G              Bm           G       Bm
I'm just trying to fix up my bed in the doldrums
Am                      C                                        Cm
Lost behind a silver screen are all the things you could have been in love... and life.. so...

G        Em7             Bm
Help me out, my friend
C                         G
My head just started to hurt
Em7            Bm
I don't pretend

C                                       G
I've got the all of the time in the world
D/F#             Em7
So now, She's gone
C                            G
On her own in her own universe
Em7                Bm
I still, walk on
C                             Cm
'til I hold you within my world.

G     D/F#     Em7    C
G     D/F#     Em7    C   Cm

G        Em7             Bm
Help me out, my friend
C                         G
My head just started to hurt
Em7            Bm
I don't pretend
C                                       G
I've got the all of the time in the world
D/F#             Em7
So now, She's gone
C                            G
On her own in her own universe
Em7                Bm
I still, walk on
C                             Cm
'til I hold you within my world.

G     D/F#     Em7    C
G     D/F#     Em7    C   Cm


G              Bm           G       Bm
Who put the weight of the world on my shoulders?
   G         Bm         G        Bm
Who put the lies in the truth that you sold us


----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D/F# = 2 X 0 2 3 2
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
