Oasis - The Turning

Intro:   Dm7/A   |  G/D        | Gm/D   |  D   |  x4

 Verse 1:
 Dm7/A               G/D     Gm/D                   D
       Eyes over the city,        Rise up from your soul,
 Dm7/A               G/D                 Gm/D                   D
       Hang over the streets at night,        brought on by the cold

 Verse 2:
 Dm7/A                   G/D               Gm/D                   D
        We live with the numbers, mining a dream for the same old song,
 Dm7/A                    G/D           Gm/D                   D
        What hope for the turning,   if everything you know is wrong.

 Chorus 1:
        G#   G           D                G#        G          D
 So come on,   shake your ragdoll baby,  before you change your mind,
          G#   G         D                  F                          E
 And come on,   when the rapture takes me,  Be the fallen angel by my side.

 Link:   Dm7/A   |  G/D        | Gm/D   |  D   |


 Verse 3:

 Dm7/A               G/D        Gm/D                D
       You carry the lantern,        I'll carry you home,
 Dm7/A                    G/D            Gm/D               D
       You search for the dissapeared,        I'll bury the cold

 Verse 4:

 Dm7/A                G/D             Gm/D                        D
       Yours is a messiah,  mine is a dream and it won't be very long,
 Dm7/A                 G/D            Gm/D                 D
       No hope for the journey,    if no-one ever sees the dawn.

 Chorus 2:
         G#   G           D                G#         G         D
 So come on,   shake your ragdoll baby,  before you change your mind,
           G#  G         D                       G#     G      D
 Then come on,  when the rapture takes me,  will you be by my side?,

         G#   G           D                G#        G          D
 So come on,   shake your ragdoll baby,  before you change your mind,
           G#  G         D                  F                          G
 Then come on,  when the rapture takes me,  be the fallen angel by my side.

 Solo:  | Bb   C |  D*       |  x4

 Chorus 3:

         Bb  C           D*               Bb        C          D*
 So come on,  shake your ragdoll baby,  before you change your mind,
           Bb  C         D*                      Bb     C      D*
 Then come on,  when the rapture takes me,  will you be by my side?,

           Bb   C           D*               Bb        C          D*
 Then come on,   shake your ragdoll baby,  before you change your mind,
           Bb  C         D*                 F                         E
 Then come on,  when the rapture takes me,  be the fallen angel by my side.

Outro:    Dm7/A   |  G/D        | Gm/D   |  D   |  to fade.

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D = X X 0 2 3 2
Dm7/A = X 0 0 2 1 1
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
G/D = X 5 5 4 3 X
Gm/D = X 5 5 3 3 3
