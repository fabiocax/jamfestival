Oasis - Acquiesce

Intro
-----
Tab:
x = corda muda
/ = de um slide nao especifico ate o começo do braço
^ = vibrar

e|------------------------------------------
B|---------------------------------7^^^-----
G|---------------------------------7^^^-----    
D|---5h7-7-5-7/--------5h7-7-5-7-5----------  
A|---5h7-7-5-7/--------5h7-7-5-7-5----------
E|---5-5------------------------------------

Verso 1
-------

  Am                                    D/F#  G
I don't know what it is that makes me feel alive
I don't know what awakes the things that sleep inside
I only wanna see the light that shines behind your eyes

I hope that I can say the things I wish I'd said
To sing my soul to slepp and take me back to bed
    Am                                  D/F#  G       Am  G  F#  Fmaj7  E
You want to be alone but when we could be alive instead

Refrão
------
           Fmaj7 C      G
Because we need each other
     Fmaj7  C         G
We believe in one another
           Fmaj7 C       G
And I know we're gonna uncover
                       Fmaj7    C    G
What's sleepin' in our soul
Becuase we need each other
We believe in one another
(And) I know we're gonna uncover
What's sleepin' in our soul
                       Am   G   F#   Fmaj7    E
What's sleepin' in our soul

Verse 3
-------
There are many things that I would like to know
And there are many places that I wish to go
But everything's depending on the way the wind may blow

I don't know what it is that makes me feel alive
I don't know how to wake the things that sleep inside
I only wanna see the light that shines behind your eyes

Refrão
------
Because we need each other
We believe in one another
And I know we're gonna uncover
What's sleepin' in our soul
Because we need each other
We believe in one another
And I know we're gonna uncover
What's sleepin' in our soul
What's sleepin' in our soul

What's sleepin' in our soul
What's sleepin' in our soul
'Cause we believe
'Cause we believe
Yeah, we believe
'Cause we believe
'Cause we believe
'Cause we believe
Because we need
Because we need

Termina com:

F   C/E   Ebsus2   C    G

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
D/F# = 2 X 0 2 3 2
E = 0 2 2 1 0 0
Ebsus2 = X 6 8 8 6 6
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
Fmaj7 = 1 X 2 2 1 X
G = 3 2 0 0 0 3
