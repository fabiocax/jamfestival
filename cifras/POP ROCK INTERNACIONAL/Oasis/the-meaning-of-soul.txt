Oasis - The Meaning Of Soul

Intro: G5 E5 x4

Verso:
G5 E5 G5 E5
I'm gonna show you 'bout the meaning of soul
Taking a load tonight
Get outta my way, man I dig what you say
But that aint enough, alright

G E G E
A different breeze, man I'm outta your league
Im 10 outta 10 all night

G5 E5 G5 E5
Get outta my way, man I dig what you say
But that wint enough, alright

CHORUS:
G D A E
Alllllllright, I see the love in your eyes
G D (back into verse progression)

Its alllright, and I'll be your light

Repeat Verse and chorus

G5 E5 G5 E5
I'm gonna show you 'bout the meaning of soul
Taking a load tonight


-------------------------------------------------
Qualquer dúvida em relação a música do Oasis

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E5 = 0 2 2 X X X
G = 3 2 0 0 0 3
G5 = 3 5 5 X X X
