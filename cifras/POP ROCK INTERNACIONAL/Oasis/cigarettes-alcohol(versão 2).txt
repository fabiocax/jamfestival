Oasis - Cigarettes & Alcohol

ALBUM: Familiar To Millions (CD2, track 2)

Chords used:

Bb  (x13331)
E   (022100)
F#  (244300)
A   (002220)
D   (xx0232)
B7  (021200)

(Tomorrow Never Knows section)
                                         Bb      C
e------------------------------          -11111-0-0-    
b-----------3------------------          -33333-0-1-    
g----------3—-3-------00-------          -33333-0-0-    (Gem)
d--------33--3------22----2202-          -33333-0-2-    -9b----
a----1111---------33-----3-----          -11111-0-3-    ---9s7-
e-0-0----------0-0------------- x2 then  -xxxxx-0-0- x4 ------7


(Liam shakes tambourine)

(Cigarettes and Alcohol starts)

Main riff
e-------------- -------------x-xxx-0-00-000-0-0-
b-------------- -------------x-xxx-3-33-333-0-0-
g-------------- -------------3-333-3-33-333-0-1-
d------99------ ------99---0-4-444-3-33-333-0-2-
a-242247742---- -242247742-2-4-444-0-00-000-0-2-
e-00000—-00320- -00000—-00-0-2-222------------0-

This is played by Gem to introduce the rhythm

                   E into sync with rhythm
e-------------------0
b-12,12,12,12-10,10-0
g-13,13,13,13-11,11-1
d-------------------2
a-------------------2
e-------------------0

Do the main riff twice bridge step once
Noel carries main riff on throughout whist
Gem does rhythm (below)

E
Is it my imagination?
          F#
Or have I finally found
A                         E
Something worth living for
E
I was looking for some action
          F#         A                    E
But all I found were Cigarettes and Alcohol

A                    E
You could wait for a lifetime
A                           E
To spend you’re days in the sunshine
A                        E
You might as well tell a white lie
E           D          A
Cos when it comes on top

A                    E    D   A
You’ve gotta make it happen
A                    E    D   A
You’ve gotta make it happen
A                    E    D   A
You’ve gotta make it happen
A                    E    D   A
You’ve gotta make it happen

D,B7,E

Then repeat main riff

Noel                        F#    A          E
e-------------- -------------x-xxx-0-00-000-0-0-
b-------------- -------------x-xxx-3-33-333-0-0-
g-------------- -------------3-333-3-33-333-0-1-
d------99------ ------99---0-4-444-3-33-333-0-2-
a-242247742---- -242247742-2-4-444-0-00-000-0-2-
e-00000—-00320- -00000—-00-0-2-222------------0-

Gem just plays this                    F#                      E
e---------------------------------------x-xxx--------------------
b---------------------------------------x-xxx-12,12,12,12,10,10--
g---------------------------------------3-333-13,13,13,13,11,11-1
d---------------------------------------4-444-------------------2
a-2242242242---2242242242---22422422422-4-444-------------------2
e-0000000000320000000000032000000000000-2-222--------------------
Then Gem breaks back into the rhythm


2.
Is it worth the aggravation?
To find yourself a job
When there’s nothing worth working for
It’s a crazy situation
But all I need are Cigarettes and Alcohol

You could wait for a lifetime
To spend you’re days in the sunshine
You might as well do a white lie
Cos’ when it comes on top

You’ve gotta make it happen
You’ve gotta make it happen
You’ve gotta make it happen
You’ve gotta make it happen


Noels next riff
b------------------
g-1-2-4b-1---------
d----------4b5-0h1-
Repeated several times over
Gem continues with Rhythm

E
You’ve gotta
You’ve gotta
             A
You’ve gotta make it
E
You’ve gotta
You’ve gotta
             A
You’ve gotta fake it
E
You’ve gotta
You’ve gotta
             A
You’ve gotta make it
E
You’ve gotta
You’ve gotta
             A
You’ve gotta fake it

(Then rhythm continues as before E,D,A )


Noel’s Last Riff b-------------------------
                 g-2b3-2b3-2b3-2b3-0-------
                 d-------------------4b6-2-

The rhythm continues on and ends like the first chorus did with Gem playing D,B7,E
On this live version as this happens Noel continues and breaks into the main riff of 'Whole Lotta Love’ by Led Zeppelin

(Whole Lotta’ Love)
It goes like this (I think)
(I’m now not sure as to what Gem plays besides Noel)

g---------------------
d—-0-02----------0-02-
a-2-2-----------2-2---
e-------000-000------- and so on

Then it turns to this, which isn’t how I think Led Zeppelin did it

g----0-------------0---------
d—-0—-2----------0—-2--------
a-2-2-----------2-2----------
e-------000-000------000-300-

This is played as someone I think uses an e-bow
e--------3-2
b-----------
g-2-2s1-0---

g-------3-1---------
d---2-------2-1-----
a-2---2---------3-0-

Gem plays something like this, I’m not sure about it although I do think that it belongs to a G# pentatonic. It’s live so they probably made it up as they went along. Just play around with it and it will always sound right.

To finish of somewhere along the string-g, Noel picks 3 times over on this string and progresses up the board although I can’t manage to decipher which frets.
The rest of what can be heard is the keyboard - which I can’t play very well.

I can now only leave you this quote:
“That’s rock’n’roll mister” (Noel after just performing this)

                                                   BY PETER LLOYD


:: Ultimate Guitar Archive ::
http://*

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
