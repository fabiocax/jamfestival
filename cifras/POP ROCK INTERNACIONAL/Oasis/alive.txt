Oasis - Alive

Acordes

Esus2   (x79977) ou E (022100)
E5      (022xxx)  " "   "
C#m     (x46654)
F#m     (x44200)
A       (x02220)
A5      (x022xx)
B5      (x244xx)

Riff Inicial:

   C#m
e---------------------------------
B------4---5-4-------4---5-4------
G--------6-----6-------6-----6----
D--6--------------6-------------6-
A---------------------------------
E---------------------------------


Intro
-----
Esus2   C#m     Esus2   C#m

Verso 1
-------
Esus2                                   C#m
  People will notice that the times are changin'
Esus2                               C#m
  It's just now but not for good
Esus2                                  C#m
  It's my feelings that I think you're draining
Esus2                               C#m
  So take them now if you think you should

Refrão
------
F#m         A
   You want something for nothing
    E5                        B5
The higher that you climb the longer you fall
F#m      A
   Maybe your time is coming,
E5                              B5
will you have to climb when you find the wall
         F#m                     A
Is there time to doubt, is there time to wait
     E5                       B5
Will you be left alone at the starting gate?
F#m
I'm not blind and I don't mind
      A                         E5 A5 E5   E5 A5 E5
Cause I've got time, now I'm alive

Verso 2
-------
The people have noticed that the times are changin'
But are they gonna do something now
I think I've seen you all hesitating
I think I'll go and do something now

Refrão
------
You want something for nothing, the higher that you climb the longer you
fall
Maybe your time is coming, will you have to climb when you find the wall?
Is there time to doubt? is there time to wait? Will you be left alone at
the starting gate?
I'm not blind and I don't mind,
                               E5 A5 E5           E5 A5 E5
'cause I've got time now I'm alive      Now I'm alive
             E5 A5 E5   E5 A5 E5
Yeah, I'm alive

Rafael Tosta

----------------- Acordes -----------------
A = X 0 2 2 2 0
A5 = X 0 2 2 X X
B5 = X 2 4 4 X X
C#m = X 4 6 6 5 4
E5 = 0 2 2 X X X
Esus2 = 0 2 4 1 0 0
F#m = 2 4 4 2 2 2
