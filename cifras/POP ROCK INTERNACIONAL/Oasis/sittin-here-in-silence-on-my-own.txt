Oasis - Sittin' Here In Silence (On My Own)

Intro - C - D - G - C - D - G - G7 - C - D - Bm - C - C - D - G

C          D             G (riff)
Sitting in silence on my own
C          D             G (riff) G7
I wait 'til everybody's gone
C                D
And only I can see
Bm               C
What it means to me
C                   D             G (riff)
I'm sitting here in silence on my own
C                D
And only I can see
Bm               C
What it means to me
C                   D             G (riff)
I'm sitting here in silence on my own

B|--3---3-
G|-5b7-5b7

Repete intro com solo de piano e Fade

Riff

G|---3-4-0

----------------- Acordes -----------------
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
