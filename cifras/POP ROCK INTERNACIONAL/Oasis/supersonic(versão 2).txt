Oasis - Supersonic

INTRO:
         B5         A5    B7 
e|--------0-------------------- 
B|----------0--------------0--- 
G|------2-----2-------2----2--- 
D|----4---------4-----2----1---     x4 
A|--4-------------4---0----2--- 
E|----------------------------- 

          F#m       B5  A   B7
I need to be myself
           F#m         B5  A  B7
I can't be no one else
      F#m
I'm feeling supersonic
  A            B7
Gimme gin and tonic
 F#m             A       B7
You can have it all but how much do you want it
     F#m            B5  A   B7
You make me laugh

            F#m      B5  A   B7
Gimme your autograph
       F#m                  A   B7
Can I ride with you in your B.M.W
         F#m                 A       B7
You can sail with me in my yellow submarine
              E
You need to find out
                                      F#m
Cos no one is gonna tell you what I'm on about
             E                                  C#7
You need to find a way for what you wanna say

But before tomorrow
 D             A          E      F#
Cos my friend said he'd take you home
D        A      E     F#m
He sits on corner all alone
D         A      E     F#m
He lives under a waterfall
D         A           E       F#m
Nobody can see him Nobody can ever hear him call

SOLO:

 D       A      E         F#m      
B|------------------------------------------------- 
G|----16b17-16-14--16-14----------14--------------- 
D|-----------------------16-14-16----16-14--------- 

part 2:
   D       A     E         F#m 
B|----------------17------------------------------------ 
G|----16b17-16-14----16b17r16-14--------14h16-14----14-- 
D|--------------------------------14h16----------16----- 


You need to be yourself
You can't be no one else
I know a girl called Elsa
She's into Alka Seltzer
She sniffs it through a cane
On a supersonic train
And she made me laugh
I got her autograph
She done it with a doctor
On a helicopter
She's sniffin' in her tissue
Sellin' the Big Issue

When she finds out
No one is gonna tell her what I'm on about
She needs to find a way
For what you want to say
But before tomorrow

----------------- Acordes -----------------
A = X 0 2 2 2 0
A5 = X 0 2 2 X X
B5 = X 2 4 4 X X
B7 = X 2 1 2 0 2
C#7 = X 4 3 4 2 X
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
