Oasis - Don't Look Back In Anger

Chords used:

         EADGBe
C       (x32010)
G       (3x0033)
G2      (355433)
Am      (x02210)
E       (022100)
F       (x33211)
F2      (133211)
Fm      (x33111)
Abdim   (456xxx)

Tab Key
-------
b = bend (7b8 means bend the string at the 7th fret to make the note the
string would make if played at the 8th fret with no bend)
r = release bend
h = hammer on
p = pull off
s = slide



Intro (guitar & piano parts mixed and tabbed for guitar)
-----
  C                F                  C               F
e|-----------------------------------------------------------------------------|
B|-1---1---1---1----1---1---1--------1-1---1---1---1---1--1---1----------------|
G|-0---0---0---0----2---2---2------0---0---0---0---0---2--0h2-0------0-------0-|
D|-2---2---2---2----3---3---3--2h3-----2---2---2---2---3---------0h2---0h2p0---|
A|---3---3---3---3----3---3--------------3---3---3---3-------------------------|
E|-----------------------------------------------------------------------------|

Verse 1
-------
C               G            Am
Slip inside the eye of your mind
          E                F
Don't you know you might find
G                     C      Am G  [play these 2 chords quickly]
   A better place to play
C             G            Am
You said that you'd never been
            E                   F
But all the things that you've seen
G               C     Am G
   Slowly fade away


Pre|-Chorus
----------
F                   Fm           C
  So I start a revolution from my bed
                                e|-----------------------------|
                                B|-----------------------------|
                                G|----5---5--------------------|
                                D|----5h7-5-------5------------|
                                A|------------5s7-----7s5--3---|
                                E|-----------------------------|

        F                 Fm            C
Cos you said the brains I had went to my head
                                      e|----------------------|
                                      B|----------------------|
                                      G|-----------5-5-----5--|
                                      D|-----------5-5-----5--|
                                      A|---------------5h7----|
                                      E|----------------------|
F                 Fm              C
Step outside, the summertime's in bloom
                                e|------------------8-8---|
                                B|------------------8-8---|
                                G|--------------8h9-------|
                                D|------------------------|
                                A|------------------------|
                                E|------------------------|
G
Stand up beside the fireplace
Abdim
Take that look from off your face
    Am             G2            F2              G2
Cos you ain't ever gonna burn my heart ouuuuuuuuuuuuuuuut

  G2(cont.)
e|--------------------------|
B|----------------5---------|
G|--5---5--7--7b9---7p5--7--|
D|--5h7-5-------------------|
A|--------------------------|
E|--------------------------|

[NOTE: The "Abdim" chord in the pre|-chorus could possibly also be played 4
other ways if you don't think the "Abdim" sounds right:

         G#/E   (4x2100)
         G#/E7  (420100)
         Abdim  (xx0101)
         Abdim7 (056400)

Chorus
------
C  G         Am        E              F
So Sally can wait, she knows it's too late
          G          C   Am G
as she's walking on by
    C    G        Am  E                F             G
Her soul slides away,   but don't look back in anger
             C  G  Am  E                  F    G            C    Am
I heard you say
           e|--------------------------------------------------------------|
           B|-----------------------------------8--------------------------|
           G|-----------5-7-7b9r7--5---5----7s9----b8r7--5--------5h7------|
               D|-------5h7--------------7------------------------5h7------|
           A|--------------------------------------------------------------|
            E|-------------------------------------------------------------|

Verse 2
-------
  G        C              G               Am
           Take me to the place where you go
e|-------------|
B|-------------|
G|-----5h7--5--|
D|-5h7---------|
A|-------------|
E|-------------|
      E       F         G                    F
Where nobody knows,         if it's night or day
         e|---------------------------------------|
         B|---------------------------------------|
         G|--5---5------------------5h7---5-------|
guitar #2    D|--5h7-5----------5---5h7-----------|
         A|---------5--5h7------------------------|
         E|---------------------------------------|

            Am      G     C                     G           Am
                          Please don't put your life in the hands
     e|----------------------------------------------------------------|
     B|----------------------------------------------------------------|
guitar 2 G|--7b9--7--5--7--5-------------------------------------5--5--|
     D|--------------------------------------------------5h7-----------|
     A|----------------------------------------------------------------|
     E|----------------------------------------------------------------|

guitar 3                         e|--12------12-----|
                             B|--15b17---15b17------|

                  E             F     G                       C
             of a rock and roll band,     who'll throw it all away
         e|--------------------------------------------------------|
     B|------------------------------------------------------------|
guitar 2 G|---7-5--7b9--7--5--7--5--------------------5--5-----5---|
     D|--------------------------------------5h7--------7----------|
     A|------------------------------------------------------------|
     E|------------------------------------------------------------|

                   e|-12----12-----------|
guitar 3               B|-15b17-15b17----|


Pre|-Chorus
----------
F                    Fm             C
   Gonna start a revolution from my bed
                                e|----------------8---------|
                                B|----------------8---------|
                                G|------------8h9--------5--|
                                D|-----------------------5--|
                                A|-------------------5h7----|
                                E|--------------------------|

        F                 Fm             C                          F
Cos you said the brains I had went to my head                       Step
                                e|---------------------------------------|
                                B|------------------------5--------------|
                                G|-------------------7b9-----7p5-----5---|
                                D|--------------------------------7------|
                                A|---------------------------------------|
                                E|---------------------------------------|

             Fm              C
outside, the summertime's in bloom
                                e|--------8-8---------|
                                B|--------8-8---------|
                                G|----8h9-------------|
                                D|--------------------|
                                A|--------------------|
                                E|--------------------|

G
Stand up beside the fireplace
Abdim
Take that look from of your face
    Am              G2           F2               G2
Cos you ain't never gonna burn my heart oooooouuuuuuuuut

  G2(cont.)
e|-----------------------------------------5--------------------|
B|--------------------------------------------------------------|
G|--------5---7--5--7b9r7--5-----5--7--7b9----7--5--7--5--7--5--|
D|---5h7----------------------7---------------------------------|
A|--------------------------------------------------------------|
E|--------------------------------------------------------------|

Chorus again:
-------------
So sally can wait, she knows it's too late
as she's walking on by
Her soul slides away, but don't look back in anger
I heard you say

Guitar Solo
-----------
        F           Fm        C
e|----------------------------------------------8-------------|
B|----------------------------------------------8-------------|
G|----5--7b8r7-7b8r7-7b8r7--5-----------5---8h9----5-----5----|
D|--7--------------------------5---5h7----------------7-------|
A|------------------------------------------------------------|
E|------------------------------------------------------------|

        F           Fm        C                        F
e|---------------------------------------------------------------------------|
B|---------------------------------------------------------------------------|
G|--7-5--7b8r7-7b8r7-7b8r7--5-----5---5--------------------------------------|
D|-----------------------------5--5h7-5------5--------------------------5--7-|
A|---------------------------------------5h7---7p5-3----------3--5--5s7------|
E|------------------------------------------------------1--5-----------------|

  Fm                      C                           G
e|--------------------------------------8--8--8h10--8-------8------8------8--|
B|----------------------------8--8--10-----------------8h10---8h10---8h10----|
G|-5--5--7b9--7--5-----5---s9------------------------------------------------|
D|------------------7--------------------------------------------------------|
A|---------------------------------------------------------------------------|
E|---------------------------------------------------------------------------|

                 Abdim                             Am
e|------8------8--8h10p8------8------8------8-------12----12----12----12-----|
B|-8h10---8h10-----------8h10---8h10---8h10---8h10--15b17-15b17-15b17-15b17--|
G|---------------------------------------------------------------------------|
D|---------------------------------------------------------------------------|
A|---------------------------------------------------------------------------|
E|---------------------------------------------------------------------------|

  G2           F2
e|-12----12-----------------------------------17---------------------|
B|-15b17-15b17--15b17r15--13----13--15--15b17---b17r15--15-17-15-17--|
G|---------------------------14--------------------------------------|
D|-------------------------------------------------------------------|
A|-------------------------------------------------------------------|
E|-------------------------------------------------------------------|

   G2
e|--------12-12-------12-12-------12-12-------12-12-------12-12--12------|
B|--15b17-------15b17-------15b17-------15b17-------15b17--------15b17---|
G|-----------------------------------------------------------------------|
D|-----------------------------------------------------------------------|
A|-----------------------------------------------------------------------|
E|-----------------------------------------------------------------------|

[Chorus again]
C  G          Am       E               F            G          C    Am  G
So Sally can wait, she knows it's too late as we're walking on by
    C    G        Am  E
Her soul slides away,

               F          G              C   Am     G            C
but don't look back in anger I heard you say                     So
e|----------------------------------------------------------------------|
B|--------------------8--8-8----6--6--6---5-----------------------------|
G|--------------------9--9-9----7--7--7---5----7b9---7p5---5--5h7-5--5--|
D|-------------------------------------------------------7--------------|
A|----------------------------------------------------------------------|
E|----------------------------------------------------------------------|

  G         Am      E                F             G          C
  Sally can wait, she knows it's too late as we're walking on by
e|----------------------------------------------------------------|
B|------5--5s9----------------5--5s9----------8--8--8----6-----5--|
G|-5--7--------------5--5--7------------------9--9--9----7-----5--|
D|----------------------------------------------------------------|
A|----------------------------------------------------------------|
E|----------------------------------------------------------------|

                            C    G        Am
                        Her soul slides away   [long pause]
e|----------------------------------------------|
B|--6---5----8---------------8---------6p5--5---|
G|----5---5-----5--5----------------------------|
D|----------------------------------------------|
A|----------------------------------------------|
E|----------------------------------------------|

               F
But don't look back in anger
           Fm
Don't look back in anger  [pause]
            C                G                     Am        E
I heard you say
        e|--------------------------------------------------------------|
        B|--------------------------------------------------------------|
        G|---------------5-7--7b9r7--5-----5--7--5--7b9---7--5---5h7--5-|
        D|----------5h7-----------------7-------------------------------|
        A|-----5h7------------------------------------------------------|
        E|--------------------------------------------------------------|

           F         Fm            C
                       least not today
        e|--------------------------0----------------|
        B|--------------------------1----------------|
        G|--------------------------0----------------|
        D|--7--5------------5-------2----------------|
        A|--------3-5-5s7---------7s3----------------|
        E|-------------------------------------------|

----------------- Acordes -----------------
Abdim = 4 X 3 4 3 X
Abdim7 = 4 X 3 4 3 X
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
F2 = 1 3 5 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
G#/E = 0 X 1 1 1 X
G2 = 3 X 0 2 0 X
