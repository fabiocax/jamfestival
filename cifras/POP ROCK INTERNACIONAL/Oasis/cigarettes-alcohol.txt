Oasis - Cigarettes & Alcohol

Intro
--------

E|-------------------------------------------------------------
B|-------------------------------------------------------------
G|-------------------------------------------------------------
D|--------------2-2---------------(3x)-------------------------
A|--2--4--2--4--2-2--4-2----------------4-4-4-4-4--7-7-7-7-7---
E|--0--0--0--0-------0-0--3-2-0---------2-2-2-2-2--5-5-5-5-5---

-------

Solo

E|-------------------------------------------------------------
B|------------------------------------------0-0-0-0-0----------
G|------------------------------------------1-2-1-2-1----------
D|--------------2-2-------------------------2-2-2-2-2----------
A|--2--4--2--4--2-2--4-2------------2-4-2---2-2-2-2-2----------
E|--0--0--0--0-------0-0--3-2-0-----0-0-0----------------------


Verso 1
-------
E                               F#            A                       E    Solo
Is it my imagination, or have I finally found something worth looking for,
E                                  F#              A                   E   Solo
I was looking for some action, but all I found was Cigarettes and Alcohol,

Refrão
------
A                    E         A                         E
You could wait for a lifetime, to spend your days in the sunshine,
A                      E                         D
You might as well be a white lie, 'cause when it comes on top,
A                 E       A                 E
You gotta make it happen, you gotta make it happen,   (X2)

D A E A E ... (repete Intro e Solo)

Verso 2
------                      Solo
Is it worth the agrivation,                                   Solo
to find yourself a job when there's nothing worth working for,    Solo
It's a crazy situation, but all I need are Cigarettes and Alcohol,

-----

Refrão

-----

E   A
    You gotta make it
E   A
    You gotta fake it
(X2)

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
