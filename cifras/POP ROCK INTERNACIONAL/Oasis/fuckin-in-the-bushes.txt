Oasis - Fuckin In The Bushes

*Intro*:

        Drums x2

        We put this festival on you bastards, with a lotta love.

e|-------------------|
B|-pick slide--------|
G|-----------------9-| 
D|-x/--------------9-|
A|-x/--------------7-| 
E|-x/----------------|

        We marched one year for you pigs.
        And you want to break our walls down? And you wanna destroy us?
        Well you go to hell!


*Verse*:

e|--------------------------|
B|--------------------------|
G|--------------------------|
D|--------------------------|
A|---------------/7-5-5h7-7-|
E|--0-0-0-3p0---------------|

Bass joins in after 4 times. Keep repeating this riff.
Then these chords are played after 8 times.

      A      B  C
e|----5------7--8----|
B|----5------7--8----| 
G|----6------8--9----|
D|----7------9--10---|
A|----7------9--10---|
E|----5------7---8---|

Then this is played after another 16 times :

*Bridge*:

e|---------------------|----------------------|------------------
B|---------------------|----------------------|------------------
G|---------------------|----------------------|------------------
D|--7b9p7--------------|--7b9p7---------------|-7b9p7------------
A|--------7-7-7-7-7-7--|--------7-7-7-7-7-7---|-------7-7-7-7-7-7
E|---------------------|----------------------|------------------

e|---------------------|
B|---------------------|
G|---------------------|
D|--7b9p7----7b9p7-----|
A|--------7--------7-7-|
E|---------------------|


*Chorus*:

        Kids running around naked, fucking in the bushes.

e|-------------------------------|
B|------------8p5----------------|
G|--7b9-----------7p5------------|     x 4      (Riff 1)
D|--------------------7p5--------|
A|---hold bend-------------7p5---|
E|-------------------------------|

e|---------------5--8b10--|-8-5-8-5--8p5------------------------|
B|-------8----------------|--------------8p5---------8p5--------|
G|--7b9~~~~~~9------------|-------------------7p5--------7p5----|
D|------------------------|----------------------7p5---------7--|
A|----hold bend-----------|-------------------------------------|
E|------------------------|-------------------------------------| 

        I love it!

*Then repeat the bridge, with slight variation.*

*Repeat Chorus*. (Only using riff 1)

        Room for everybody here, yes all are welcome. Yes indeed
        I love them.
        Fun. Nice. Life. Youth. Beautiful. I'm all for it.

*Repeat Riff 1 to fade.*

        Room for everybody here, yes all are welcome. Yes indeed
        I love them.
        Fun. Nice. Life. Youth. Beautiful. I'm all for it.

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C = X 3 2 0 1 0
