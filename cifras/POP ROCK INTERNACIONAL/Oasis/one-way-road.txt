Oasis - One Way Road

INTRO:
E|--------------------------------------------------
B|--------------------------------------------------
G|--------------------------------------------------
D|-------0---4-------2---2-------2---2-------0---4--
A|-----0---0-------2---2-------2---2-------0---0----
E|---0-----------2-----------2-----------0----------


Chords used (Tune bottom E down to D)

Name:         DADGBE

D:            000232
Em:           222000
Dm:           000231
G:            x20033
C:            x32010
D7:           000212
Dm(#5th):     000331



D                                            Em                                            D
I wanna' get high but I never could take the pain cos' it'll blow away my soul like a hurricane
D                                           Em                                                       D
Like a one-man band clappin' in the pourin' rain if I know where I'm going I don't know from where i came
D                 Dm G     D                           Dm  G       D           C               G
Where we gonna be in summertime? And are we gonna' see the heavens shine? Like diamonds in the sky


D7              Dm                     Dm(#5th)          D
As soon as they come the feelings they go all alone on a one way road  C   G   D7
D7              Dm                     Dm(#5th)          D
As soon as they come the feelings they go all alone on a one way road  C   G   D   C   G   D


D                                            Em                                            D
I wanna get high but i really can't take the pain cos' it'll blow away my soul like a hurricane
D                                               Em                               C          G            D
I'm like a one man band clappin' in the pouring rain if i know where i'm going i don't know from where i came
D                 Dm G     D                          Dm  G       D           C                    G
Where we gonna be in summertime? and are we gonna see the heavens shine? like diamonds in the sky


D7              Dm                     Dm(#5th)          D
As soon as they come the feelings they go all alone on a one way road  C   G   D7
D7              Dm                     Dm(#5th)          D
As soon as they come the feelings they go all alone on a one way road

C   G   D   C   G   D   C   G   D   C   G   D

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
