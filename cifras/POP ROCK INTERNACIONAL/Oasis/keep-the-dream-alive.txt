Oasis - Keep The Dream Alive

Intro: B  C#m

     B                 C#m                      B
Four Seasons.. seconds flick and flash... I`m alone

C#m

  B               C#m
A lonely scream provides the scene
        F#
It`s no home

      E                C#m
Every night I hear you scream,
        E                  A
But you don`t say what you mean
 B                 C#m               B    C#m
This was my dream, but now my dream has flown


I`m at the crossroads waiting for a sign
My life is standing still, but I`m still alive
Every night I think I know
In the morning where did he go
The answers disappear when I open my eyes

B
I`m no stranger to this place
B
Where real life and dreams collide
C#m
And even though I fall from grace
B
I will keep the dream alive

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
