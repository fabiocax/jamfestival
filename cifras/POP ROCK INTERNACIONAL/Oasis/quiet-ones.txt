Oasis - Quiet Ones

Chords used:
     D  D5 Dº Em   A   C  Bm  E7
e|---2---2---4-----0---0---0---7---0---| 
B|---3---3---3-----0---2---1---7---0---| 
G|---2---1---4-----0---2---0---7---1---| 
D|---0---0---3-----2---2---2---9---0---| 
A|---X---X---X-----2---0---3---9---2---| 
E|---X---X---X-----0---X---X---7---0---|

intro: D D5  (x2)

D        Dº    Em            A   D
So let's hear it      for the quiet ones
D         Dº   Em         A      Bm
let's all hear it     this brighter sun
       C                   Bm          E7
when stuck and you're overcome, listen up

D         Dº   Em            A   D     (repeat intro: D D-5 [x2])
Let's all hear it     for the quiet ones


D        Dº    Em            A   D
So let's hear it      for the quiet ones
D         Dº   Em          A       Bm
cant get near it       to a brighter sun
       C                  Bm          E7
when stuck and we're overcome, listen up

D         Dº   Em            A   D
Let's all hear it     for the quiet ones...

(repeat intro: D D 5 [x4] and hold the last D 5, end with D)

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D5 = X 5 7 7 X X
Dº = X X 0 1 0 1
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
