Oasis - Wonderwall

Capo Casa 2

[Intro 4x] Em7  G  D4  A7(4)

[Primeira Parte]

Em7           G
    Today is gonna be the day
             D4
That they're gonna
                  A7(4)
Throw it back to you
Em7              G
    By now you should've somehow
   D4                   A7(4)
Realized what you gotta do
Em7                  G
I don't believe that anybody
D4              A7(4)
Feels the way I do
           C9  D4  A7(4)
About you now



Em7                G
   Backbeat, the word was on the street
         D4                   A7(4)
That the fire in your heart is out
Em7                  G
   I'm sure you've heard it all before
        D4                 A7(4)
But you never really had a doubt
Em7                  G
I don't believe that anybody
D4              A7(4)
Feels the way I do
           Em7  G  D4  A7(4)
About you now


[Dedilhado Pré-Refrão]

Parte 1 de 3 (2x)
   C9      D         Em7
E|-3--3----2h3p2------3---------------------|
B|-3----3--3-----3----3---------------------|
G|-0-------2-------2--0------------0-----0--|
D|-2-------0----------2------0-------0h2----|
A|-3------------------2--0h2---0h2----------|
E|--------------------0---------------------|


Parte 2 de 2
   C9    D         G       D11/F#   Em7
E|-3-3---2h3p2-----3-3-----3-3-----3-3-----|
B|-3---3-3-----3---3---3---3---3---3---3---|
G|-0-----2-------2-0-----0-0-----0-0-----0-|
D|-2-----0---------0-------0-------0-------|
A|-3---------------2-------0-------2-------|
E|-----------------3-------2-------0-------|


Parte 3 de 3
   G        A7(4)
E|-3-3----------0-------0-------0-------0---|
B|-3---3----3-3---3-3-3---3-3-3---3-3-3---3-|
G|-0-----0--0-------0-------0-------0-------|
D|-0--------2-------2-------2-------2-------|
A|-2--------0-------0-------0-------0-------|
E|-3----------------------------------------|


    C9
And all the roads
    D                Em7
We have to walk are winding
   C9
And all the lights
      D                  Em7
That lead us there are blinding
 C9             D
There are many things that I
      G       D11/F#  Em7
Would like to say to  you
       G          A7(4)
But I don't know how


[Refrão]

         C9   Em7  G
Because maybe
        Em7
You're gonna be the one
      C9      Em7  G
That saves me
   Em7   C9  Em7  G
And after all
           Em7   C9  Em7  G  Em7  A7(4)
You're my wonderwall


[Segunda Parte]

Em7            G
    Today was gonna be the day
            D4                     A7(4)
But they'll never throw it back to you
Em7              G
    By now you should've somehow
   D4                       A7(4)
Realized what you're not to do
Em7                  G
I don't believe that anybody
D4              A7(4)
Feels the way I do
           Em7  G  D4  A7(4)
About you now


[Pré-Refrão]

    C9
And all the roads
      D                   Em7
That lead you there were winding
    C9
And all the lights
      D                  Em7
That light the way are blinding
 C9             D
There are many things that I
      G       D11/F#  Em7
Would like to say to  you
       G          A7(4)
But I don't know how


[Refrão]

       C9   Em7  G
I said maybe
        Em7
You're gonna be the one
      C9      Em7  G
That saves me
   Em7   C9  Em7  G
And after all
           Em7   C9  Em7  G  Em7
You're my wonderwall

       C9   Em7  G
I said maybe
        Em7
You're gonna be the one
      C9      Em7  G
That saves me
   Em7   C9  Em7  G
And after all
           Em7   C9  Em7  G  Em7
You're my wonderwall

       C9   Em7  G
I said maybe
        Em7
You're gonna be the one
      C9      Em7  G
That saves me
        Em7
You're gonna be the one
      C9      Em7  G
That saves me
       Em7
You're gonna be the one
      C9      Em7  G
That saves me

(C9  Em7  G  Em7) (4x)

----------------- Acordes -----------------
Capotraste na 2ª casa
A7(4)*  = X 0 2 0 3 0 - (*B7(4) na forma de A7(4))
C9*  = X 3 5 5 3 3 - (*D9 na forma de C9)
D*  = X X 0 2 3 2 - (*E na forma de D)
D11/F#*  = 2 X X 2 3 3 - (*E11/G# na forma de D11/F#)
D4*  = X X 0 2 3 3 - (*E4 na forma de D4)
Em7*  = 0 2 2 0 3 0 - (*F#m7 na forma de Em7)
G*  = 3 2 0 0 0 3 - (*A na forma de G)
