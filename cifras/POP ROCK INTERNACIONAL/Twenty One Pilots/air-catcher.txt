Twenty One Pilots - Air Catcher

Intro: Bbm  Ab  G  C  C

Eb                       (Eb)
I don't fall slow like I used to
Bbm                   (Bbm)
I fall straight down
Ab                      Gm
You've stolen my air catcher
C
That kept me safe and sound

Eb
My parachutes will get me
Bbm
Safely to ground
Ab            Gm
But now the cord's not working
C
And I see you staring me down

Fm
I won't fall in

Db
Love with falling
Ab               C
I will try to avoid
(C)
Those eyes

( Fm  Db  Ab  C )

Eb
     I think you would beat
              Bbm
The moon in a pretty contest
Ab                        Gm
     And the moon just happened to be
    C
The very first thing that I missed

Eb
    I was doing fine on my own
    Bbm
And there wasn't much I lacked
Ab                        Gm
   But you've stolen my air catcher
    C
And I don't know if I want it back

Fm
I won't fall in
Db
Love with fall in
Ab               C
I will try to avoid
(C)
Those eyes

Fm
'Cause I'm not sure
  Db
I want to give you
Ab                 C
Tools that can destroy
(C)      Fm
My heart

(Fm)                Ab
And judges don't say
(Ab)             Bb
What you want to hear
(Bb)             Db        Eb, Fm
So I'll write my fears
(Fm)           Ab
And I don't believe
(Ab)               Bb
In talking just to breathe
(Bb)               Db      Eb, Fm
And falling selfishly

Fm
I won't fall in
(Db)      (Ab)
Love with fall in
             (C)
I will try to avoid
(C)
Those eyes

(Fm)
But now I'm here
   (Db)     (Eb)
To give you words
   (Ab)             (C)
As tools that can destroy
(C)      (Fm)
My heart

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Bb = X 1 3 3 3 1
Bbm = X 1 3 3 2 1
C = X 3 2 0 1 0
Db = X 4 6 6 6 4
Eb = X 6 5 3 4 3
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
