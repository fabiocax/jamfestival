Twenty One Pilots - Tear In My Heart

Intro:
                 D F# G
An-nyŏng-ha-se-yo
                 D F# G


Verso 1:
                              D F# G
Sometimes you gotta bleed to know
                              D F# G
That you're alive and have a soul
                              A7
But it takes someone to come around
            A
To show you how


Refrão:
                     G
She's the tear in my heart


I'm alive
          A           Bm
She's the tear in my heart

I'm on fire
          A          D
She's the tear in my heart
         A
Take me higher
                    D F# G
Than I've ever been


Verso 2:
                             D F# G
The songs on the radio are okay
                               D F# G
But my taste in music is your face
                             A7
And it takes a song to come around
            A
To show you how


Refrão:
                     G
She's the tear in my heart

I'm alive
          A          Bm
She's the tear in my heart

I'm on fire
          A          D
She's the tear in my heart
        A
Take me higher
               G
Than I've ever been
          A    Bm
Than I've ever been
          A    D
Than I've ever been
          A    D
Than I've ever been


Ponte:
D                          A D           F#  G   D          A D
ohh, oh-oh, oh, ooh, oh-oh,      ooo, oo-oo, oo, ooo, oo oo
D        F#         G  D     D       D             A D
You fell asleep in my    car I drove the whole time
D          F#         G    D        D        D            A D
But that's okay I'll just avoid the holes so you sleep fine
D           F#     G
I'm driving here I sit
D   D      D        A D
Cursing my government
D          F#    G     D                             A D
For not using my taxes to fill holes with more cement
D        F#        G  D                        A D
You fell asleep in my car I drove the whole time
D          F#        G    D                               A D
But that's okay I'll just avoid the holes so you sleep fine
D           F#     G
I'm driving here I sit
D
Cursing my government
A7        A                                          D F# G
For not using my taxes to fill holes with more cement


Verso 3:
                             D     F#  G
Sometimes you gotta bleed to know, oh, oh
                             D     F#  G
That you're alive and have a soul, oh, oh
                             A7
But it takes someone to come around
            A
To show you how


Refrão:
                     G
She's the tear in my heart

I'm alive
          A          Bm
She's the tear in my heart

I'm on fire
          A          D
She's the tear in my heart
         A
Take me higher

Than I've ever been

G
My heart is my armour
          A          Bm
She's the tear in my heart

She's a carver
        A              D
She's a butcher with a smile
       A
Cut me farther
               G
Than I've ever been
          A    Bm
Than I've ever been
          A    D     A
Than I've ever been

Than I've ever been

G
My heart is my armour
          A          Bm
She's the tear in my heart

She's a carver
        A              D
She's a butcher with a smile
       A
Cut me farther
                   D
Than I've ever been

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
