Twenty One Pilots - Before You Start Your Day

Capo Casa 3

Intro 2x: G  D  Em  C

G    D   Em    D            G
Open the slits in your face
          D       Em      C
and start your day
G              D        Em      C           G
You don't have much time to make your slits
         D     Em
look just right

( G  D  Em )

    C
I'm in your mind

( G  D  Em )

    C
I'm singing


( G  D  Em )

    C
I'm in your mind

( G  D  Em )

    C
I'm singing

C     D     Em    G     C     D     G
la-da la-da la-da la-da la-da la-da da

( D  Em  D  G  D  Em  D )

G       D   Em         C         G              D      Em
Look in the mirror and ask your soul if you're alright
G       D   Em           D    G          D     Em   C
Put out the glitter that your soul hides behind

( G  D  Em )

    C
You're in my mind

( G  D  Em )

    C
I'm singing

( G  D  Em )

    C
You're in my mind

( G  D  Em )

    C       C     D     Em    G     C     D     G
I'm singing la-da la-da la-da la-da la-da la-da da

( G  D  Em  C )

D                 C G
Nowhere were they holy

D            C       G
Open up your eyes and see

D                     C    G
The clouds above will hold you

D          C          G
The clouds above will sing

( D  Em  C  G  D  Em  C )

( G  D  Em )

    C
And in your mind

( G  D  Em )

    C          D     D     Em    G     C     D     G
You're singing la-da la-da la-da la-da la-da la-da da

----------------- Acordes -----------------
Capotraste na 3ª casa
C*  = X 3 2 0 1 0 - (*D# na forma de C)
D*  = X X 0 2 3 2 - (*F na forma de D)
Em*  = 0 2 2 0 0 0 - (*Gm na forma de Em)
G*  = 3 2 0 0 0 3 - (*A# na forma de G)
