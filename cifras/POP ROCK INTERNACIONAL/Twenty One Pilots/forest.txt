Twenty One Pilots - Forest

( Bb  F  Dm  C ) 8x

Bb    F           Dm       C
I don't know why I feed on emotion
Bb                F              Dm    C
There's a stomach inside my brain
Bb          F
I don't want to be heard
Dm         C
I want to be listened to
Bb            F          Dm
Does it bother anyone else
            C                   Bb  F
That someone else has your name?
    Dm            C           Bb
Oh, does it bother anyone else
           F                Dm          C
That someone else has your name, your name

F         C
I scream, you scream

    Dm                Bb        Gm
We all scream 'cause we're terrified
   Bb                C
Of what's around the corner
F          C
We stay in place
           Dm           Bb       Gm
?Cause we don't want to lose our lives
         Bb                 C
So let's think of something better.

Bb         F       Dm         C
Down in the forest we'll sing a chorus
Bb           F     Dm   C
One that everybody knows
Bb        F        Dm      C
Hands held higher, we'll be on fire
Bb           F            Dm   C
Singing songs that nobody wrote.

Bb      F
My brain has given up
Dm             C
White flags are hoisted
Bb         F
I took some food for thought
Dm         C
It might be poisoned
Bb         F
The stomach in my brain
Dm          C
Throws up on to the page
Bb            F           Dm
Does it bother anyone else
            C             Bb    F  Dm
That someone else has your name?
              C         Bb
Does it bother anyone else
               F         Dm   C
That someone else has your name?

F         C
I scream, you scream
    Dm                Bb        Gm
We all scream 'cause we're terrified
   Bb                C
Of what's around the corner
F          C
We stay in place
           Dm           Bb       Gm
?Cause we don't want to lose our lives
         Bb                 C
So let's think of something better.


Bb         F       Dm         C
Down in the forest we'll sing a chorus
Bb           F     Dm   C
One that everybody knows
Bb        F        Dm      C
Hands held higher, we'll be on fire
Bb           F            Dm   C
Singing songs that nobody wrote.

Ponte: Dm C F Bb

       Dm
Quickly moving towards a storm
Moving forward, torn
In to pieces over reasons
Of what these storms are for
C
I don't understand why everything I adore
Takes a different form when I squint my eyes
         F
Have you ever done that
When you squint your eyes
And your eyelashes make it look a little not right
              Bb
And then when just enough light
Comes from just the right side
And you find you're not who you're suppose to be?
Dm
This is not what you're suppose to see
Please, remember me? I am suppose to be
      C
King of a kingdom or swinging on a swing
Something happened to my imagination
                 F
This situation's becoming dire
My treehouse is on fire
And for some reason I smell gas on my hands
Bb
This is not what I had planned
This is not what I had planned.


Bb         F      Dm C
Down in the forest
Bb        F        Dm C
We'll sing a chorus
Bb        F        Dm      C
Hands held higher, we'll be on fire
Bb           F            Dm   C
Singing songs that nobody wrote.

Bb         F       Dm         C
Down in the forest we'll sing a chorus
Bb           F     Dm   C
One that everybody knows
Bb        F        Dm      C
Hands held higher, we'll be on fire
Bb           F            Dm   C
Singing songs that nobody wrote.

Bb         F      Dm C
Hands held higher
Bb         F    Dm C
We'll be on fire
Bb         F      Dm C
Hands held higher
Bb         F    Dm C
We'll be on fire

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
