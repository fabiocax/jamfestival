Twenty One Pilots - House Of Gold

[Refrão]

C
She asked me, "Son, when I grow old
Will you buy me a house of gold?
And when your father turns to stone
Will you take care of me?"

    C                    F
She asked me, "Son, when I grow old
     Am           G
Will you buy me a house of gold?
    C                F
And when your father turns to stone
     C        G       C
Will you take care of me?"

[Ponte]

F      A7
I will make you

Dm       Bbm            F
Queen of everything you see
                    C
I'll put you on the map
                 F            C
I'll cure you of disease

[Primeira Parte]

      C             F
Let's say we up and left this town
     Am               G
And turned our future upside-down
      C                 F
We'll make pretend that you and me
      C     G     C
Lived ever after, happily

[Refrão]

    C                    F
She asked me, "Son, when I grow old
     Am           G
Will you buy me a house of gold?
    C                F
And when your father turns to stone
     C        G       C
Will you take care of me?"

[Ponte]

F      A7
I will make you
Dm       Bbm            F
Queen of everything you see
                    C
I'll put you on the map
                 F       C
I'll cure you of disease

[Segunda Parte]

    C                  F
And since we know that dreams are dead
    Am                  G
And life turns plans up on their head
C              F
I will plan to be a bum
   C      G       C
So I just might become someone

[Refrão]

    C                    F
She asked me, "Son, when I grow old
     Am           G
Will you buy me a house of gold?
    C                F
And when your father turns to stone
     C        G       C
Will you take care of me?"

[Ponte]

F      A7
I will make you
Dm       Bbm            F
Queen of everything you see
                    C
I'll put you on the map
                 F
I'll cure you of disease

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bbm = X 1 3 3 2 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
