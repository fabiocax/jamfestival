Twenty One Pilots - The Pantaloon

Capo Casa 2

G            C
Your grandpa died
              Em
When you were nine
          D
They said he had
         G
Lost his mind
         C
You have learned
        Em
Way too soon
           D                    G     C     Em     D
You should never trust the pantaloon

G             C
Now it's your turn
       Em
To be alone
       D
Find a wife

                     G
And build yourself a home
         C
You have learned
        Em
Way too soon
          D                 C
That your dad is now a pantaloon

Hook:
              G
You are tired
              Em
You are hurt
                   D
A moth ate through
                    C
Your favorite shirt
                    G          Em
And all your friends fertilize
                    D
The ground you walk
Bm   C   G
Lose your mind

(No music)
He's seen too many stare downs
Between the sun and the moon
In the morning air
How he used to hustle all the people
Walking through the fairgrounds
He's been around so long
He's changed his meaning of a chair now
Because a chair now,
Is like a tiny island in the sea of all the people
Who glide across the very surface
That made his bones feeble
The end can't come soon enough
But is it too soon?
Either way he can't deny
He is a pantaloon

Hook:
C
              G
You are tired
              Em
You are hurt
                   D
A moth ate through
                    C
Your favorite shirt
                    G          Em
And all your friends fertilize
                    D
The ground you walk
Bm   C   G
Lose your mind

Verse 3:
G                       C
You like to sleep alone
                          Em
It's colder than you know
                       D
Cause your skin is so
                     G
Used to colder bones
                       C
It's warmer in the morning
                    Em
Than what it is at night
                      D
Your bones are held together
                                   C
By your nightmare and your frights

Chorus:
              G
You are tired
             Em
You are hurt
                   D
A moth ate through
                    C
Your favorite shirt
                    G          Em
And all your friends fertilize
                    D
The ground you walk
Bm   C   G
Lose your mind

----------------- Acordes -----------------
Capotraste na 2ª casa
Bm*  = X 2 4 4 3 2 - (*C#m na forma de Bm)
C*  = X 3 2 0 1 0 - (*D na forma de C)
D*  = X X 0 2 3 2 - (*E na forma de D)
Em*  = 0 2 2 0 0 0 - (*F#m na forma de Em)
G*  = 3 2 0 0 0 3 - (*A na forma de G)
