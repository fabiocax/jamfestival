Twenty One Pilots - Heavydirtysoul

 Dm9
There's an infestation in my mind's imagination
I hope they choke on smoke 'cause
I'm smoking them out the basement
This is not rap, this is not hip hop
Just another attempt to make the voices stop
Rapping to prove nothing
Just writing to say something
'Cause I wasn't the only one who
Wasn't rushing to say nothing
This doesn't mean I lost my dream
It's just right now I got a really crazy mind to clean

 Dm9
Gangsters don't cry
Therefore, therefore I'm
Mr. Misty-eyed, therefore I'm

Bb            Gm
Can you save, can you save my
Dm               C
Can you save my heavydirtysoul?

Bb            Gm
Can you save, can you save my
Dm               C
Can you save my heavydirtysoul?
 Bb       Gm
For me, for me, uh
 Dm               C
Can you save my heavydirtysoul?
 Bb       Gm
For me, for me, uh
 Dm              C
Can you save my heavydirtysoul?

 Dm9
Nah, I didn't understand a thing you said
If I didn't know better
I'd guess you're all already dead
Mindless zombies walking around
With a limp and a hunch
Saying stuff like, "you only live once. "
You've got one time to figure it out
One time to twist and one time to shout
One time to think and I say we start now
Sing it with me if you know what I'm talking about

 Dm9
Gangsters don't cry
Therefore, therefore I'm
Mr. Misty-eyed, therefore I'm

Bb            Gm
Can you save, can you save my
Dm               C
Can you save my heavydirtysoul?
Bb            Gm
Can you save, can you save my
Dm               C
Can you save my heavydirtysoul?
 Bb       Gm
For me, for me, uh
 Dm               C
Can you save my heavydirtysoul?
 Bb       Gm
For me, for me, uh
 Dm              C
Can you save my heavydirtysoul?

(Bb  Gm  Dm  C)

Dm9
Death inspires me like a dog inspires a rabbit  (2x)

Bb            Gm
Can you save, can you save my
Dm               C
Can you save my heavydirtysoul?
Bb            Gm
Can you save, can you save my
Dm               C
Can you save my heavydirtysoul?
 Bb       Gm
For me, for me, uh
 Dm               C
Can you save my heavydirtysoul?
 Bb       Gm
For me, for me, uh
 Dm              C
Can you save my heavydirtysoul?

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Dm9 = X 5 7 9 6 5
Gm = 3 5 5 3 3 3
