Led Zeppelin - Ramble On

Capo Casa 7

A                           D           A               D
    Leaves are falling all around, it's time I was on my way.
A                       D           A                        D
    Thanks to you, I'm much obliged     for such a pleasant stay.
A                         D              A                    D
    But now it's time for me to go, the autumn moon lights my way,
A                              D                              A            D
    for now I smell the rain,     and with it pain, and it's headed my way.
A     D                        A
     Ah, sometimes I grow so tired,
            D
    but I know I've got one thing I got to do.

 A             D
Ramble on, and now's the time, the time is now,
    A                        D
to sing my song, I'm goin' 'round the world, I got to find my girl.
 A               D
On my way, I've been this way ten years to the day,
 A                D
ramble on, gotta find the queen of all my dreams.


A                     D                 A                    D
   Got no time for spreadin' roots, the time has come to be gone.
A                                    D
    And tho' our health we drank a thousand times,
A                          D A D A D
    it's time to ramble on.

 A             D
Ramble on, and now's the time, the time is now,
    A                        D
to sing my song, I'm goin' 'round the world, I got to find my girl.
 A               D
On my way, I've been this way ten years to the day,
 A                D
ramble on, gotta find the queen of all my dreams.
A  D                      A  D
        I ain't tellin' no lie.

A                        D                  A                D
    Mine's a tale that can't be told, my freedom I hold dear.
A                     D                       A             D
    How years ago in days of old, when magic filled the air.
A                                   D         A                 D
    T'was in the darkest depths of Mordor, I met a girl so fair,
         A                      D                                 A
    but Gollum, and the evil one   crept up and slipped away with her.
          D       A D         A                    D
    Her, her....yeah.  Ain't nothing I can do, no.     I can just keep on

      A                    D           A           D
     rambling,  I'm gonna,     sing,  sing my song,
   A                 D
    ramble on (Gotta keep-a-searchin' for my baby).
 A                 D
    ramble on (gonna work my way, round the world).
     A                           D
    ramble on(I can't stop this feelin' in my heart)
           A
    Gotta keep searchin' for my baby...

----------------- Acordes -----------------
Capotraste na 7ª casa
A*  = X 0 2 2 2 0 - (*E na forma de A)
D*  = X X 0 2 3 2 - (*A na forma de D)
