The Beatles - Dear Prudence

Introdução:
e|---10~---8~----7~----5~----------------------|
B|-8-----8-----8-----8------5-8~---5-8~---5-7~-|
G|---------------------------------------------|
D|---------------------------------------------|
A|---------------------------------------------|
E|---------------------------------------------|

Riff - estende por quase toda a musica:
e|---2~----2~-|
B|-3-----3----|
G|------------|
D|------------|
A|------------|
E|------------|

     D         D/C              Gmaj7/B              GmMaj7/Bb
Dear Prudence                                     won\'t you come out
D              D/C              Gmaj7/B            GmMaj7/Bb
to play                                           Dear
D              D/C              Gmaj7/B            GmMaj7/Bb
Prudence                                          greet the brand

D              D/C              Gmaj7/B            GmMaj7/Bb
new day                                           The

D              D/C              Gmaj7/B            GmMaj7/Bb
sun is up, the sky is blue it\'s beautiful, and so are you Dear
D              D/C              C              G
Prudence                  won\'t you            come out to
D              D/C              Gmaj7/B            GmMaj7/Bb
play

Dear Prudence  open up your eyes
Dear Prudence  see the sunny skies
The wind is low, the birds will sing
That you are part of everything
Dear Prudence  won\'t you open up you eyes

D     G/D       A/D     G/D
Look around round
Look around round round
D     F    A#    G      D
Look around_____________

Dear Prudence  let me see you smile
Dear Prudence  like a little child
The clouds will be a daisy chain
So let me see you smile again
Dear Prudence  won\'t you let me see you smile

Peace, love, and soul,
Paul Zimmerman

D              x x 0 2 3 2        G/D  x x 0 4 3 3
D/C            x 3 x 2 3 2        A/D  x x 0 6 5 5
Gmaj7/B        x 2 x 0 3 2
GmMaj7/Bb      x 1 x 0 3 2

----------------- Acordes -----------------
A# = X 1 3 3 3 1
A/D = X X 0 6 5 5
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/C = X 3 X 2 3 2
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/D = X 5 5 4 3 X
Gmaj7/B = 7 X 5 7 7 X
GmMaj7/Bb = X 1 0 0 3 2
