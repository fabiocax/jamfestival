The Beatles - Twist And Shout

[Intro 2x] D  G  A

[Primeira Parte]

                    D
Well, shake it up, baby, now
   G           A
(Shake it up, baby)
            D      G         A
Twist and shout (twist and shout)

Come on, come on, come on, come on
 D          G        A
Baby, now (come on, baby)
                       D
Come on and work it on out
  G         A
(Work it on out)

                 D     G         A
Well, work it on out (work it on out)

                      D
You know you look so good
  G       A
(Look so good)
                     D
You know you got me goin' now
  G      A
(Got me goin')
                      D
Just like I knew you would
          G       A
(Like I knew you would)

[Segunda Parte]

                    D
Well, shake it up, baby, now
   G           A
(Shake it up, baby)
            D      G         A
Twist and shout (twist and shout)

Come on, come on, come on, come on
 D          G        A
Baby, now (come on, baby)
                       D
Come on and work it on out
  G         A
(Work it on out)
                            D
You know you twist, little girl
   G            A
(Twist, little girl)
                       D
You know you twist so fine
   G       A
(Twist so fine)
                             D
Come on and twist a little closer now
   G              A
(Twist a little closer)
                             D
And let me know that you're mine
          G          A
(Let me know you're mine)

[Intro 2x] D  G  A

[Terceira Parte]

A
Ah! Ah! Ah! Ah! Ah!
 D           G           A
Baby, now (shake it up, baby)
            D      G         A
Twist and shout (twist and shout)

Come on, come on, come on, come on
 D          G        A
Baby, now (come on, baby)
                       D
Come on and work it on out
  G         A
(Work it on out)

                            D
You know you twist, little girl
   G            A
(Twist, little girl)
                       D
You know you twist so fine
   G       A
(Twist so fine)
                             D
Come on and twist a little closer now
   G              A
(Twist a little closer)
                             D
And let me know that you're mine
          G          A
(Let me know you're mine)


Well shake it, shake it, shake it
 D           G           A
Baby, now (shake it up, baby)

Well shake it, shake it, shake it
 D           G           A
Baby, now (shake it up, baby)

Well shake it, shake it, shake it
 D           G           A
Baby, now (shake it up, baby)

[Final]

A               D
Ah! Ah! Ah! Ah!

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
G = 3 2 0 0 3 3
