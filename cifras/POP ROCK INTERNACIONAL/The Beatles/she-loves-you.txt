The Beatles - She Loves You

Riff 1:

E|-------------------------|
B|-------------------------|
G|-3---3--5/6\5--3---------|
D|-3---3--5/6\5--3-----3---|
A|---5--------------5------|
E|-------------------------|

Riff 2:

E|-3--2--0--|
B|-0--0--0--|
G|-0--0--0--|
D|-5--4--2--|
A|----------|
E|----------|

    Em
She loves you, yeah, yeah, yeah
      A7
She loves you, yeah, yeah, yeah

      C                      G
She loves you, yeah, yeah, yeah! (Riff 1)

      G                     Em
You think you've lost your love
        Bm            D
Well I saw her yesterday
      G                Em
It's you she's thinking of
         Bm              D
And she told me what to say

               G
She says she loves you (Riff 2)(Faz em cima do "And you know...")
                           Em
And you know that can't be bad (Riff 2)
         Cm
Yes she loves you
                            D
And you know you should be glad (Riff 1)

     G               Em
She said you hurt her so
      Bm             D
She almost lost her mind
     G                 Em
But now she says she knows
        Bm              D
You're not the hurting kind

                 G
She says she loves you (Riff 2)
                           Em
And you know that can't be bad (Riff 2)
         Cm
Yes she loves you
                            D
And you know you should be glad

     Em
She loves you, yeah, yeah, yeah
      A7
She loves you, yeah, yeah, yeah
        Cm
With a love like that
     D                   G
You know you should be glad (Riff 1)

     G               Em
You know it's up to you
   Bm               D
I think it's only fair
  G                Em
Pride can hurt you too
   Bm         D
Apologize to her

          G
Because she loves you (Riff 2)
                           Em
And you know that can't be bad  (Riff 2)
         Cm
Yes she loves you
                            D
And you know you should be glad, oh!

Riff 3:

E|------------|
B|------------|
G|------------|
D|------------|
A|----5-------|
E|-3-----2---0|

     Em
She loves you, yeah, yeah, yeah
      A7
She loves you, yeah, yeah, yeah
        Cm
With a love like that
     D                   G   Em
You know you should be glad (Riff 3)
        Cm
With a love like that
     D7                  G  Em
You know you should be glad  (Riff 3)
        Cm
With a love like that
     D                 G      (Riff 2)
You know you should be glad
Em
Yeah, yeah, yeah  (Riff 4)
C
Yeah, yeah, yeah, yeah

Riff 4:

E|-3--2--0------3--2--0-------|
B|-2--2--2------1--1--1--1p0--|
G|-------0------------0--0----|
D|----------------------------|
A|----------------------------|
E|----------------------------|

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
