The Beatles - Paperback Writer

intro:

e|----------------------------------| 
B|----------------------------------| 
G|----------------------------------| 
D|---------5----3------------3------| 2x
A|-----3-5----------3---3h5---------| 



 Paperback writeeeeer...

      G7
 Dear Sir or Madam, will you read my book?
 It took me years to write, will you take a look
 It's based on a novel by a man named Lear
                                              C
 And I need a job so I want to be a paperback writer
           G7
 Paperback writer


 G7
 It's a dirty story of a dirty man
 And his clinging wife doesn't understand
 His son is working for the Daily Mail
                                                  C
 It's a steady job but he wants to be a paperback writer
           G7
 Paperback writer

 G7
 It's a thousand pages, give or take a few
 I'll be writing more in a week or two
 I can make it longer if you like the style
                                                     C
 I can change it 'round and I want to be a paperback writer
           G7
 Paperback writer

 G7
 If you really like it you can have the rights
 It could make a milion for you overnight
 If you must return it you can send it here
                                                 C
 But I need a break and I want to be a paperback writer
           G7
 Paperback writer

 G7
 Paperback writer...

----------------- Acordes -----------------
C = X 3 2 0 1 0
E = 0 2 2 1 0 0
G7 = 3 5 3 4 3 3
