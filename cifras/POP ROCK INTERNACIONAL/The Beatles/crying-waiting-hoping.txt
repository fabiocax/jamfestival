The Beatles - Crying, Waiting, Hoping

John Lennon / Paul McCartney

[Intro:] Bb F Bb D# Bb F

  Bb
Crying (crying),
 F
Waiting (waiting),
 Bb               D#
Hoping (hoping), you'll come back
   Bb                 F              Bb F
I just can't seem to get you off my mind

Crying (crying),
Waiting (waiting),
Hoping (hoping), you'll come back
           Bb              F                      Bb D# Bb
You're the one I love, I think about you all the time

  F             Bb
Crying (crying),

              D#     Bb
Tears keep a-falling all night long
 F              Bb
Waiting (waiting)
            D#           Bb        F
It seems so useless, I know it's wrong to keep on...

Crying (crying),
Waiting (waiting),
Hoping (hoping), you'll come back baby,
Someday soon things will change and you'll be mine

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
D# = X 6 5 3 4 3
F = 1 3 3 2 1 1
