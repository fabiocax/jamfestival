The Beatles - Mean Mr. Mustard

E
Mean Mr Mustard sleeps in the park
Shaves in the dark, trying to save paper
B7
Sleeps in a hole in the road
D
Saving up to buy some clothes
B7
Keeps a ten bob note up his nose
       E             C7 B7       E            C7 B7
Such a mean old man,      such a mean old man

E
His sister Pam works in a shop
She never stops she's a go-getter
B7
Takes him out to look at the Queen
D
Only place that he's ever been
B7
Always shouts out something obscene

      E          C7 B7           E          C7 B7
Such a dirty old man,            dirty old man

----------------- Acordes -----------------
B7 = X 2 1 2 0 2
C7 = X 3 2 3 1 X
D = X X 0 2 3 2
E = 0 2 2 1 0 0
