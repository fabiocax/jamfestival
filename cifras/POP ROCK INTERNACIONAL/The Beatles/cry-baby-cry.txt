The Beatles - Cry Baby Cry

  G      Am   F                G
Cry baby cry make your mother sigh
      Em7                A7     F
she's old enough to know better

    Em      Em7M                  Em7
The king of Marigold was in the kitchen
        Em6                 C7  G
cooking breakfast for the queen
    Em               Em7M             Em7
The queen was in the parlour playing piano
            Em6          C7
For the children of the king

 G       Am    F               G
Cry baby cry make your mother sigh
      Em7                   A7       F       G
she's old enough to know better so cry baby cry

    Em       Em7M                   Em7
The king was in the garden picking flowers

      Em6                 C7   G
For a friend who came to play
    Em        Em7M                      Em7
The queen was in the playroom painting pictures
            Em6      C7
For the children's holiday

 G       Am    F               G
Cry baby cry make your mother sigh
      Em7                   A7       F       G
she's old enough to know better so cry baby cry

     Em        Em7M             Em7
The duchess of Kircaldy always smiling
     Em6              C7  G
And arriving late for tea
     Em       Em7M                  Em7
The duke was having problems with a message
        Em6            C7
At the local bird and bee

 G       Am    F               G
Cry baby cry make your mother sigh
      Em7                   A7       F       G
she's old enough to know better so cry baby cry

    Em              Em7M              Em7
At twelve o'clock a meeting round the table
      Em6           C7   G
For a seance in the dark
     Em         Em7M              Em7
With voices out of nowhere put on specially
        Em6           C7
By the children for a lark

 G       Am    F               G
Cry baby cry make your mother sigh
      Em7                   A7       F       G
she's old enough to know better so cry baby cry, cry, cry, cry baby

 F                G
Make your mother sigh
      Em7                   A7       F       G
she's old enough to know better so cry baby cry, cry, cry, cry

 F                G
Make your mother sigh
      Em7                   A7       F      Em
she's old enough to know better so cry baby cry

 Fm
Can you take me back where are people, can you take me back
Can you take me back where are people, brother can you take me back
Can you take me back?
Oh can you take me where are people, can you take me back?

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
C7 = X 3 2 3 1 X
Em = 0 2 2 0 0 0
Em6 = X 7 X 6 8 7
Em7 = 0 2 2 0 3 0
Em7M = X X 2 4 4 3
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
