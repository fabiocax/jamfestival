The Beatles - Mother Nature's Son

D        G                 D   A/C#
Born a poor young country boy -
Bm      Bm/A     E/G#
Mother Nature’s son.
A   D/A  A   D/A  A         D/A   A         D/A     D
All day long I’m sitting singing songs for everyone.

Dm G/D D


Sit beside a mountain stream - see her waters rise.
Listen to the pretty sound of music as she flies.

D         G   D D D7 G Gm D
Tu tu tu tu...

Find me in my field of grass -
Mother Nature’s son.
Swaying daisies sing a lazy song beneath the sun.
Mother Nature’s son


Tu tu tu...

Mother Nature’s son.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
Bm = X 2 4 4 3 2
Bm/A = X 0 4 4 3 X
D = X X 0 2 3 2
D/A = X 0 X 2 3 2
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E/G# = 4 X 2 4 5 X
G = 3 2 0 0 0 3
G/D = X 5 5 4 3 X
Gm = 3 5 5 3 3 3
