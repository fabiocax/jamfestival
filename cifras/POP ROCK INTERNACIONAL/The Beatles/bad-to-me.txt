The Beatles - Bad To Me

E                    C#m
 If you ever leave me   I'll be sad and blue
G#m                           F#m              B7
 Don't you ever leave me, I'm so in love with you

E|-------------------------------------|
B|-------------------------------------|
G|---6---4-----------------------------|
D|-------------6----4----2-------------|
A|-------------------------------------|
E|-------------------------------------|

    E                      C#m
The birds in the sky would be sad and lonely
        E                C#m
If they knew that I lost my one and only
          A             B7     E   A  B7
They'd be sad if you're bad to me

    E                       C#m
The leaves in the tree would be softly sighin`

        E                          C#m
If they heard from the breeze that you left me
                  A            B7     E
Crying, They'll be sad don't be bad to me

              A
But I know you won't leave me
          B7
'Cos you told me so
    E                       C#m
And I've no intention of letting you go
A                   B7
Just as long as you let me know
    F#m             B7
You won't be bad to me

 E                     C#m
So the birds in the sky won't be sad and lonely
           E              C#m
'Cos they know that I got my one and only
        A                B7        E
They'd be glad, you're not bad to me

[Solo] E C#m E C#m A B7 E

E|-------7---7--9-7-9-7-----------7---7--9-7-9-7------------7-11-7-|
B|---7-9---9------------9-7---7-9---9------------9-7----7-9--------|
G|-9------------------------9------------------------9-------------|
D|-----------------------------------------------------------------|
A|-----------------------------------------------------------------|
E|-----------------------------------------------------------------|

               A
But I know you won't leave me
          B7
'Cos you told me so
    E                       C#m
And I have no intenttion of letting you go
 A                   B7
Just as long as you let me know
    F#m             B7
You won't be bad to me

      E                     C#m
So the birds in the sky won't be sad and lonely
           E              C#m
'Cos they know that I got my one and only
          A                B7     E  Eb D C#
They'd be glad, you're not bad to me
           F#m              B7     E  A  B7
They'd be glad, you're not bad to me
   E  A  B7   E  A  B7   E
To me,     To me      To me

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
C# = X 4 6 6 6 4
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Eb = X 6 5 3 4 3
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
