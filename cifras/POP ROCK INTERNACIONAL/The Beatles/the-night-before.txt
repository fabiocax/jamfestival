The Beatles - The Night Before

Solo:
E|-----------------------------------------------------------------------------
B|----------------------5----------------------------------------5-------------
G|-7-5--------------5-7---7-5-------------5-7-5--------------5-7---7-5---------
D|-----7--75-3---7------------53--------------7--75-3---7------------53----
A|---------------------------------5----------------------------------------5--
E|-----------------------------------------------------------------------------

John Lennon / Paul McCartney

[Intro:] D F G A

D                C  G
We said our goodbyes
     A
The night before
 D               C   G
Love was in your eyes
     A
The night before


Bm          Gm6
Now today I find
Bm                    Gm6
You have changed your mind
  D                G7              D  F G
Treat me like you did the night before

Were you telling lies?
The night before
Was I so unwise
The night before

When I held you near
You were so sincere
Treat me like you did the night before

Am                 D7             G
Last night is the night I will remember you by
Bm                E7              A7
When I think of things we did It makes me wanna cry

When I held you near
You were so sincere
Treat me like you did the night before
 F                D
Like the night before

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
Gm6 = 3 X 2 3 3 X
