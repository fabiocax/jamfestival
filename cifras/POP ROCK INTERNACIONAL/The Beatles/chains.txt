The Beatles - Chains

Bb
Chains  my baby's got me locked up in chains
                   Eb                Bb
And they ain't the kind that you can see
          F              Eb               Bb   F
Woah it's chains of love    got a hold on me


Bb
Chains  I can't break away from them chains
           Eb                  Bb
Can't run around cause I'm not free
           F              Eb              Bb         Bb7
Woah these chains of love    won't let me be, yeah,

Eb                            Bb                 Bb7
I wanna tell you pretty baby     I think you're fine
Eb                         F
I'd like to love you  But darling,

I'm imprisoned by these


Bb
Chains  my baby's got me locked up in chains
                   Eb                Bb
And they ain't the kind that you can see
          F              Eb               Bb       Bb7
Woah it's chains of love    got a hold on me, yeah,

Eb                                Bb                  Bb7
Please believe me when I tell you    You're lips are sweet
Eb                           F
I'd like to kiss them  But I can't break away from all these
Bb
Chains  my baby's got me locked up in chains   And they ain't the
                   Eb                Bb
And they ain't the kind that you can see
          F              Eb               Bb         Bb7
Woah it's chains of love    got a hold on me, yeah,

Bb
Chains, chains of love, chains of love
                          Eb  Ebm
chains of love chains of love

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bb7 = X 1 3 1 3 1
Eb = X 6 5 3 4 3
Ebm = X X 1 3 4 2
F = 1 3 3 2 1 1
