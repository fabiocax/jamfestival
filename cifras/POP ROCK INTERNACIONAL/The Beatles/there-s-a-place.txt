The Beatles - There's A Place

 There's A Place

(verse 1)
E                 A           E
There is a place, where I can go,
A           E   C#m           B7
When I feel low...when I feel blue.
A           G#m7 A                G#m7
And it's my mind...and there's no time,
A         E
when I'm alone.
           E    A              E
I think of you, and things you do,
A           E   C#m7              B7
Go round my head...the things you said,
C#m7     A         B7
Like, 'I love only you.'

(chorus)
C#m7                  F#7
In my mind there's no sorrow,

E                        G#m7
Don't you know that it's so.
C#m7                 F#7
There'll be no sad tomorrow,
E                        G#m7 C#m7 G#m7 C#m7
Don't you know that it's so?

(verse 2)
E                 A           E
There is a place, where I can go,
A           E  C#m            B7
When I feel low...when I feel blue.
A           G#m7  A
And it's my mind,
               G#m7            B7   C#m7 E
And there's no time, when I'm alone.
E
There's a place...

(chorus)

(verse 2)

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
E = 0 2 2 1 0 0
F#7 = 2 4 2 3 2 2
G#m7 = 4 X 4 4 4 X
