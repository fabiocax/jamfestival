The Beatles - Hold Me Tight

(intro) F

     C7
It feels alright now

(verse 1)
 F       Bb
Hold me tight
G7                C7
  Tell me I'm the only one
      F     Bb
And then I might
G7              C7
  Never be the lonely one

(chorus)
    F       F7
So hold me tight
   Bb       Bbm
tonight, tonight
      F
it's you,

Bbm          F      C
  you, you ,you, oo

(verse 2)
F       Bb
Hold me tight
G7              C7
  Let me go on loving you
   F        Bb
Tonight, tonight
G7               C7
  Making love to only you

(chorus)

(brigde)

        F    G#                  F
Don't know     what it means to hold you tight
Bb             Gm                    G
  Being here alone tonight with you
    C7
It feels alright now

(verse 1)
(chorus)
(bridge)
(verse 2)
(chorus)

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bbm = X 1 3 3 2 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
