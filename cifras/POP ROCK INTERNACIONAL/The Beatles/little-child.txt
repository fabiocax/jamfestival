The Beatles - Little Child

No refrão, na última frase (Baby take a chance with me)
quando é cantado pela 1ª vez os acordes são F#7 e B7 ; a partir da
2ª vez os acordes serão F#7 B7 e E7.

No solo, é tocado um solo de rock'n' roll básico (usado
principalmente naquelas músicas da década de 50,e ínício de 60)


Intro: E7

       E7
Little child, little child
                       A7         E7
Little child won't you dance with me?
B7             A7
I'm so sad and lonely
F#7                     B7
Baby take a chance with me

Little child, little child

Little child won't you dance with me?
I'm so sad and lonely
F#7         B7          E7
Baby take a chance with me

                                B7
If you want someone to make you feel so fine
           E7
Then we'll have some fun when you're mine all mine
        F#7               B7
So come on, come on; come on

Refrão (nº 2)

Solo: E7 /  /  / A7 / B7 A7 E7 /


When you're by my side you're the only one
Don't you run and hide just come on, come on
So come on, come on; come on

Refrão (nº 2)
F#7         B7          E7
Baby take a chance with me (oh yeah) (2X)

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
E7 = 0 2 2 1 3 0
F#7 = 2 4 2 3 2 2
