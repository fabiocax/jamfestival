The Beatles - You're Gonna Lose That Girl

Solo:

E|----7---7---7---7-------------------12--9b10rb9-7-7-----7---7---7---7------------------------12-
B|-8b9-8b9-8b9-8b9-8b9rb8------------------------------8b9-8b9-8b9-8b9-8b9rb8-8b9rb8--------------
G|-------------------------11b12rb11-9----------------------------------------------11b12rb11-9---
D|------------------------------------------------------------------------------------------------
A|------------------------------------------------------------------------------------------------
E|------------------------------------------------------------------------------------------------

E|--10rb9-7-7s4-|
B|--------------|
G|--------------|
D|--------------|
A|--------------|
E|--------------|

              E            C#m
You're gonna lose that girl
             F#m       B7
You're gonna lose that girl


E                     G#
If you don't take her out tonight
            F#m            B7
She's gonna change her mind
E                   G#
And I will take her out tonight
            F#m          B7  E
and I will treat her kind

              E            C#m
You're gonna lose that girl
             F#m       B7   E
You're gonna lose that girl

                        G#
If you don't treat her right my friend
             F#m           B7  E
you're gonna find her gone
                   G#
Cause I will treat her right and then
              F#m         B7
you'll be the lonely one

             E            C#m
You're gonna lose that girl
             F#m       B7
You're gonna lose that girl
             F#m D
You're gonna lose

G                       C                  G
I'll make a point of taking her away from you
G                       C               F
The way you treat her, what else can I do?

Solo: E G# F#m B7

             E            C#m
You're gonna lose that girl
             F#m       B7
You're gonna lose that girl
             F#m D
You're gonna lose

G                       C                  G
I'll make a point of taking her away from you
G                       C               F
The way you treat her, what else can I do?

E                     G#
If you don't take her out tonight
            F#m            B7
She's gonna change her mind
E                   G#m
And I will take her out tonight
            F#m          B7  E
and I will treat her kind

              E            C#m
You're gonna lose that girl
             F#m       B7
You're gonna lose that girl

             D    A    E
You're gonna lose that girl

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
G#m = 4 6 6 4 4 4
