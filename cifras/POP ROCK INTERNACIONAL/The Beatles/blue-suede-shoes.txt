The Beatles - Blue Suede Shoes

Intro: A

Well it's a one for the money, two for the show
                       A7
Three to get ready now go, cat ,go
     D                                A
But don't you step on my blue suede shoes
         E                     D                     A
You can do anything,  but lay off of my blue suede shoes
        A
Well you can knock me down, step in my face
Slander my name all over the place
And do anything that you want to do
But ah ah, honey, lay off of my shoes
     D                                A
And don't you step on my blue suede shoes
         E                    D                     A
You can do anything, but lay off of my blue suede shoes
                A
Well you can burn my house, steal my car
Drink my liquor from an old fruit jar

And do anything that you want to do
                       A7
But ah ah, honey, lay off of my shoes
     D                                A
And don't you step on my blue suede shoes
         E                   D                     A
You can do anything but lay off of my blue suede shoes
          A
It`s blue blue, blue suede shoes, blue blue, blue suede shoes
D                                       A
Blue blue, blue suede shoes, baby,     blue blue, blue suede shoes
               E                   D                     A
Well, you can do anything but lay off of my blue suede shoes

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
