The Beatles - Honey Don't

Essa vai para todos que trabalham duro no Conselho do Off Topic!!!

intro:
                                       E
e ----0---0------0---0------0---0---------------------------
B ----0---0------0---0------0---0---------------------------
G --4---4---4--3---3---3--2---2---0------------------------
D ---------------------------------------------------------
A -------------------------------------------4-7-4---------
E -------------------------------------0-4-7-------7-4-0----


      E
Well, how come you say you will when you won't?
C
  Say you do, baby when you don't
E
  Let me know, honey, how you fell
C
  Tell the truth now, is love real?
      B7

  so, aw, aw
            E
Well, honey don't
Well, honey don't
Honey don't
      A7
Honey don't
      E7
Honey don't
          B7
I say you will when you won't
A7            E
aw, aw, honey don't


        E
Well, I love you, baby, and you ought to know
C
 I like the way that you wear your clothes
    E
Everything about you is so doggone sweet
    C
You got that sand all over your feet
    B7
so, aw, aw
            E
Well, honey don't
Honey don't
Honey don't
      A7
Honey don't
      E7
Honey don't
          B7
I say you will when you won't
A7             E
aw,  aw, honey don't


antes do solo Ringo fala: "Oh, rock on George, one time for me"

(solo)

E C E C B7 E
I feel fine

E A7 E7 B7 A7 E
Ooo, oo, I say


     E
Well Sometimes I love you on a Saturday night
C
  Sunday morning you don't look right
E
 You been out painting the town
C
 Uh huh, baby you been slipping around
    B7
so, aw, aw
            E
Well, honey don't
I say, honey don't
Honey don't
      A7
Honey don't
      E7
Honey don't
          B7
I say you will when you won't
A7             E
aw,  aw, honey don't


(solo 2)

E C E C B7 E

E
      Well, honey don't
            A7
Well, honey don't
                        E7
a little, little, honey don't
           B7
I say, you will when you won't
A7            E
Aw, as, honey don't


esses riffs são tocados com uma guitarra durante a música, são facilmente identificados:

riff 1
     E                   E
D -----------------------------------------
A ---------4-7-4---------------4-5-4------
E ---0-4-7-------7-4-----0-4-7-------7-4---

riff 2
     A
D ---------4-5-4----------
A ---0-4-7-------7-4------
E -------------------------

riff 3
     B
D ---------6-7-6--------
A -----6-9-------9-6------
E ---7-----------------

----------------- Acordes -----------------
