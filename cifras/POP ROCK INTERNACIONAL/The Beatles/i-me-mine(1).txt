The Beatles - I, Me, Mine

Intro  Riff:

     Am                           D
E|----------------------------|----------5--------8----10----
B|----------------------------|5----8------------------------
G|------------------------5h7-|------------------------------
D|--------------------5h7-----|------------------------------
A|----------------------------|------------------------------
E|5---------------------------|------------------------------

     G            E7
E|10b11-------------8----10----
B|-----------------------------
G|-----------------------------
D|-----------------------------
A|-----------------------------
E|-----------------------------

Am    C                   D7
All______ through the day
         G          E7          Am
I me mine, I me mine, I me mine___

    C                     D7
All______ through the night
         G          E7          Am
I me mine, I me mine, I me mine___
            Dm
Now they're frightened of leaving it
F6
Everyone's reading it
E7/b9
Coming on strong all the time
Am   Am(maj7)       Am7  Am6
All____ through the day
       Fmaj7
I, me, mine

Refrão:

A7
I me me mine____
I me me mine____
D7
I me me mine____
A7               E7   E7
I me me mine____
Am    C              D7
All______ I can hear
         G          E7          Am
I me mine, I me mine, I me mine___
  C              D7
Even those tears
         G          E7          Am
I me mine, I me mine, I me mine___

Pré-Refrão:

         Dm
No one's frightened of playing it
F6
Everyone's saying it
E7/b9
Flowing more freely than wine
Am   Am(maj7)         Am7  Am6
All_____ through the day
       Fmaj7
I, me, mine

Refrão:

A7
I me me mine____
I me me mine____
D7
I me me mine____
A7               E7
I me me mine____
Am   C               D7
All______ I can hear
         G          E7          Am
I me mine, I me mine, I me mine___
  C              D7
Even those tears
       G        E7          Am
I me mine, I me mine, I me mine___

Pré-Refrão:

         Dm
No one's frightened of playing it
F6
Everyone's saying it
E7/b9
Flowing more freely than wine
Am   Am(maj7)         Am7   Am6
All_____ through your life
       Fmaj7
I, me, mine

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Am(maj7) = X 0 2 1 1 0
Am6 = 5 X 4 5 5 X
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
E7/b9 = X X 2 1 3 1
F6 = 1 X 0 2 1 X
Fmaj7 = 1 X 2 2 1 X
G = 3 2 0 0 0 3
