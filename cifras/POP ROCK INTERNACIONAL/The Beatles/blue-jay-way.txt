The Beatles - Blue Jay Way

George Harrison

                     C  D#dim                             C   D#dim
There's a fog upon L.A.   and my friends have lost their way,
                                                             C
"Well be over soon" they said now they've lost themselves instead
  C
Please don't be long, please don't you be very long,

Please don't be long or I may be a sleep
                     C    D#dim                       C  D#dim
Well it only goes to show    and I told them where to go,
                                                        C
Ask a p'liceman on the street there's so many there to meet
  C
Please don't be long, please don't you be very long,

Please don't be long or I may be a sleep
                       C    D#dim                    C  D#dim
Now it's past my bed I know   and I'd really like to go,
                                                       C
Soon will be the break of day sitting here in Blue Jay Way

  C
Please don't be long, please don't you be very long,

Please don't be long or I may be a sleep  (repeat)

----------------- Acordes -----------------
C = X 3 2 0 1 0
D#dim = X X 1 2 1 2
