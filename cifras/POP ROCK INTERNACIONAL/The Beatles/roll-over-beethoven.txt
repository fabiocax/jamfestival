The Beatles - Roll Over Beethoven

(solo de introdução)
E|------------8s10-10-10-8s10-10-10-8s10-10-10--13-10------------------------------  
B|-----10-12--8s10-10-10-8s10-10-10-8s10-10-10-------13-10------------------------- 
G|--11-----------------------------------------------------13-12-10-9-5s7-7-7-7-7-7
D|--------------------------------------------------------------------5s7-7-7-7-7-7
A|---------------------------------------------------------------------------------
E|---------------------------------------------------------------------------------

E|----------------------------------------------------10-10-10----10--12-13
B|----8s10-10-10-10----------(10)(varias vezes)-------10-10-10----10--12-13
G|----8s10-10-10-10-12-10----(10)------------------------------------------
D|------------------------12-----------------------------------------------
A|-------------------------------------------------------------------------
E|-------------------------------------------------------------------------


E|---10-13--10----------------
B|---10--------13--10---------
G|-------------------12--5s7--
D|-----------------------5s7--
A|----------------------------
E|----------------------------



obs: para o solo sair idêntico escute a música executando-o e colocando no tempo
correto, e ao mesmo tempo usando efeitos como hamer on e pull off.

Uma guitarra faz os acordes normais e outra  vai auternando os mesmo acordes,
mas só que em 5ª
exemplo:

D (guitarra 1)                               G (guitarra 1)

D|-7-9-10-9--7                               G|-7-9-10-9--7  
A|-5-5-5--5--5   (Guitarra 2)                D|-5-5-5--5--5   (guitarra 2)


A (guitarra 1)

G|-9-11-12-11-9
D|-7-7--7--7--7  (Guitarra 2)


D
I'm gonna write a little letter,
D
Gonna mail it to my local DJ.
    G
It's a rockin' rhythm record
                       D
I want my jockey to play.
 G                    A7                         D
Roll over Beethoven, I gotta hear it again today.

D
You know, my temperature's risin'

And the jukebox blows a fuse.
G
My heart's beatin' rhythm
                                      D
And my soul keeps on singin' the blues.
G                    A7                           D
Roll over Beethoven and tell Tchaikovsky the news.

D
I got the rockin' pneumonia,

I need a shot of rhythm and blues.
G
I think I'm rollin' arthritis
                                 D
Sittin' down by the rhythm review.
G                   A7                    D
Roll over Beethoven rockin' in two by two.

D
well, if you feel you like it
D
go get your lover, then reel and rock it.
D             G
roll it over and move on up just
                 D
a trifle further and reel and rock it, roll it over,
G                   A7                    D
roll over Beethoven rockin' in two by two.

D
Well, early in the mornin' I'm a-givin' you a warnin'
Don't you step on my blue suede shoes.
G
Hey diddle diddle, I am playin' my fiddle,
D
Ain't got nothin' to lose.
G                    A7                            D
Roll over Beethoven and tell Tchaikovsky the news.

D
You know she wiggles like a glow worm,
Dance like a spinnin' top.
G
She got a crazy partner,
                            D
Oughta see 'em reel and rock.
G                       A7                       D
Long as she got a dime the music will never stop.

D
Roll over Beethoven,
D
Roll over Beethoven,
G
Roll over Beethoven,
D
Roll over Beethoven,
G                    A7                           D
Roll over Beethoven and dig these rhythm and blues.

Logo colocarei o solo do meio.

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
