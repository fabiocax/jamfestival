The Beatles - Eleanor Rigby

Refrão:

C/G                         Em
Ah! Look at all the lonely people
C/G                         Em
Ah! Look at all the lonely people


Primeira Parte:

Em
Eleanor Rigby

Picks up the rice in a church
                     C/G
Where a wedding has been
             Em
Lives in a dream

Em
Waits at the window


Wearing the face that she keeps
                 C/G
In a jar by the door
           Em
Who is it for


Segunda Parte:

Em7             Em6
All the lonely people
       Em5+              Em
Where do they all come from
Em7             Em6
All the lonely people
       Em5+          Em
Where do they all belong?


Repete a Primeira Parte:
 Em
Father McKenzie

Writing the words of a sermon that
             C/G
No one will hear
              Em
No one comes near

 Em
Look at him working

Darning his socks in the night
                      C/G
When there's nobody there
              Em
What does he care


Repete a Segunda Parte:

Em7             Em6
All the lonely people
       Em5+              Em
Where do they all come from
Em7             Em6
All the lonely people
       Em5+          Em
Where do they all belong?

Repete o Refrão:

C/G                         Em
Ah! Look at all the lonely people
C/G                         Em
Ah! Look at all the lonely people

Repete a Primeira Parte:
Em
Eleanor Rigby

Died in the church and was buried
                C/G
Along with her name
        Em
Nobody came

 Em
Father McKenzie

Wiping the dirt from his hands
                       C/G
As he walks from the grave
            Em
No one was saved


Repete a Segunda Parte:

Em7             Em6
All the lonely people
       Em5+              Em
Where do they all come from
Em7             Em6
All the lonely people
       Em5+          Em
Where do they all belong?

----------------- Acordes -----------------
C/G = 3 3 2 X 1 X
Em = 0 2 2 0 0 0
Em5+ = 0 2 2 0 1 0
Em6 = 0 2 2 0 2 X
Em7 = 0 2 2 0 3 0
