The Beatles - Michelle

Acordes utilizados (capotraste na 5ª casa)

A|--3----3----5----3----2--|--5----8--|--3---------4----3--|
E|--3----1----5----3----1--|--4----7--|--4----4----4----3--|
C|--0----0----1----3----2--|--5----8--|--5----6----5----4--|
G|--0----2----1----3----1--|--4----7--|--5----5----6----5--|
D|--2----3----3----1-------|----------|--3----6----6----5--|
A|--3---------1------------|----------|------------4----3--|


Intro:


A|-----3----3----3----3---3--1-----2/3--2/3--2/3--2/3--|
E|-----4----4----4----4---4--1--4--2/3--2/3--2/3--2/3--|
C|---5----4----3----2---1----1-------------------------|
G|-----------------------------------------------------|
D|-----------------------------------------------------|
A|-----------------------------------------------------|



C/G         Fm7/A   Bb6                 F#° A° C° A° G/D   F#°   G/D
Michelle, ma belle, these are words that go together well, my Michelle
C/G         Fm7/A   Bb6                  F#°         G/D   F#°   G/D
Michelle, ma belle, sont des mots qui vont très bien ensemble, très bien

Cm                                  Eb7                  G#
I love you, I love you, I love you, that's all I want to say
G5             Cm
until I find a way
(Intro)                                         G/D
I will say the only words I know that you'll understand.

C/G         Fm7/A   Bb6                 F#°          G/D   F#°   G/D
Michelle, ma belle, sont des mots qui vont très bien ensemble, très bien

Cm                                  Eb7                  G#
 I need to , I need to , I need to I need to make you see
G5             Cm
oh what you mean to me
(Intro)                                    G/D
until I do I'm hoping you will know what I mean

C/G         Fm7/A   Bb6                  F#°         G/D   F#°   G/D
I love you

Solo:

        C/G             Fm7/A     Bb6                 F#°
A|----------------------------------------------------|
E|----------------------------------------------------|
C|----------------------------------------------------|
G|-------------2--3--5--6---1---0---0--1--3-----------|
D|----3--3--5---------------------------------1----3--|
A|----------------------------------------------------|

                     G/D  F#°       G/D
A|---------------------------------------------------------|
E|---------------------------------------------------------|
C|---------------------------------------------------------|
G|-----0--1---------------1--------------------------------|
D|--3--------0----2---0-------3---2-----------2--3--5--6---|
A|--------------------------------------3--5---------------|

Cm                                  Eb7                  G#
I want you, I want you, I want you. I think you know by now
G5             Cm
I'll get to you somehow.
(Intro)                                    G/D
Until I do I'm telling you so you'll understand.

C/G         Fm7/A   Bb6                  F#°         G/D   F#°   G/D
Michelle, ma belle, sont des mots qui vont très bien ensemble, très bien
(Intro)                                         G/D
I will say the only words I know that you'll understand, my Michelle


Solo (2x):

        C/G             Fm7/A     Bb6                 F#° A°
A|----------------------------------------------------|
E|----------------------------------------------------|
C|----------------------------------------------------|
G|-------------2--3--5--6---1---0---0--1--3-----------|
D|----3--3--5---------------------------------1----3--|
A|----------------------------------------------------|

             C° A°  G/D  F#°        G/D
A|---------------------------------------------------------|
E|---------------------------------------------------------|
C|---------------------------------------------------------|
G|-----0--1---------------1--------------------------------|
D|--3--------0----2---0-------3---2-----------2--3--5--3---|
A|--------------------------------------3--5---------------|

----------------- Acordes -----------------
Capotraste na 5ª casa
A°*  = 5 X 4 5 4 X - (*D° na forma de A°)
Bb6*  = X 1 3 0 3 X - (*Eb6 na forma de Bb6)
C/G*  = 3 3 2 X 1 X - (*F/C na forma de C/G)
Cm*  = X 3 5 5 4 3 - (*Fm na forma de Cm)
C°*  = X 3 4 2 4 2 - (*F° na forma de C°)
Eb7*  = X 6 5 6 4 X - (*Ab7 na forma de Eb7)
F#°*  = 2 X 1 2 1 X - (*B° na forma de F#°)
G#*  = 4 3 1 1 1 4 - (*C# na forma de G#)
G/D*  = X 5 5 4 3 X - (*C/G na forma de G/D)
G5*  = 3 5 5 X X X - (*C5 na forma de G5)
