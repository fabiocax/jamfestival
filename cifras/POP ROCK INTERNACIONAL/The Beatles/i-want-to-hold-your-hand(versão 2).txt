The Beatles - I Want To Hold Your Hand

Intro 3x: C D

Primeira Parte:
    G                  D
Oh yeah I'll tell you something
Em                      B7
  I think you'll understand
     G           D
When I say that something
Em                    B7
  I wannna hold your hand

Refrão:
C           D         G     Em
  I wanna  hold your hand
C           D         G
  I wanna  hold your hand

(repete a primeira parte)
     G            D
Oh please say to me

Em                       B7
  You'll let me be your man
      G            D
And please say to me
Em                         B7
  You'll let me hold your hand

Refrão:
C                D         G    Em
  You'll let me hold your hand
C                D         G
  You'll let me hold your hand

Segunda Parte:
Dm            G
  And when I touch you
        C       Am
I feel happy inside.
Dm             G               C
  It's such a feeling that my love
C        D    C        D    C        D
I can't hide, I can't hide, I can't hide!

(repete a primeira parte)
      G            D
Yeah you got that somethin'
Em                      B7
  I think you'll understand
     G           D
When I say that something
Em                   B7
  I wanna hold your hand

Refrão:
C            D         G     Em
  I want to hold your hand
C            D         G
  I want to hold your hand

Segunda Parte:
Dm          G
And when I touch you
       C       Am
I feel happy inside.
Dm             G               C
  It's such a feeling that my love
C        D    C        D    C        D
I can't hide, I can't hide, I can't hide!

(repete a primeira parte)
       G            D
Yeah, you say that something
Em                      B7
  I think you'll understand
     G           D
When I got that something
Em                     B7
  I want to hold your hand

Refrão Final:
C          D         G      Em
  I wanna hold your hand
C          D         B7
  I wanna hold your hand
C          D         C     G
  I wanna hold your hand

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
