The Beatles - Glad All Over

INTRO: A   E

      E
Ain't no doubt about it

This must be love
A
One little kiss from you
           B
And I feel   glad all over
          A
Oo baby, hot dang gilly
      B
It's silly
                   E
But I'm glad all over


          E
Yeah the goosepimples baby


Cos I feel so good
          A
When you call me like you do
           B
And I feel   glad all over
               A
Oo mercy, I'm rock gone puppy
         B
And I'm happy
                   E
And I'm glad all over


      A
Your touch sewed me
                      E
Like an electric wire

Never thought of make love
    A
It don't take much looking

To see what I've got
         B
But it shows, yes it shows
     E
Come on honey Bonnie

Give me one more time
A
  Everytime you do
            B
Well I feel   glad all over
          A
Oo baby, hot dang gilly
      B
It's silly
                   E
But I'm glad all over

Yeah!

GUITAR SOLO: A      E      B   A   E


A
Tried to tell you

How I'm cooking inside
E
When we're cheek to cheek
    A
My temperature is low

Fever is high
            B
I can't speak

I'm too weak

          A
Well come on honey Bonnie

Give me one more time
E
  Everytime you do
            B
Well I feel   glad all over
          A
Oo baby, hot dang gilly
      B
It's silly
                   E
But I'm glad all over
 A
Hot dang gilly
      B
It's silly
                   E
But I'm glad all over
       A
Well, hot dang gilly
      B
It's silly
                   E
But I'm glad all over

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
E = 0 2 2 1 0 0
