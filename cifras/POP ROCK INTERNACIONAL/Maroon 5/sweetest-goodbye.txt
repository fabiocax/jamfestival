Maroon 5 - Sweetest Goodbye

  (Riff 1)

  E|------------------------------------------------            
  B|------------------------------------------------
  G|----4-----4-----4-----4-----6—----6-----6-----6-      
  D|-4-----4-----4-----4-----6-----6-----6-----6----  
  A|------------------------------------------------
  E|------------------------------------------------

    -----------------------------------------------|
    -----------------------------------------------|
    ----6-----6-----6-----6-----6-----6-----6-----6|
    -4-----4-----4-----4-----4-----4-----4-----4---|
    -----------------------------------------------|
    -----------------------------------------------|

Where you are seems to be
As far as an eternity
Outstretched arms open hearts
And if it never ends then when do we start?


I'll never leave you behind
Or treat you unkind
I know you understand
And with a tear in my eye
Give me the sweetest goodbye
That I ever did receive

  |REFRÃO|

  E|-7-7-x7-x7-x7-x-x6-6-x6-x6-x6xxx-6-6-x6-x6-xx6-6-6-2-2-x2-x2-xx-x|  
  B|-7-7-x7-x7-x7-x-x6-6-x6-x6-x6xxx-7-7-x7-x7-xx6-7-6-4-4-x4-x4-xx-x|
  G|-8-8-x8-x8-x8-x-x7-7-x7-x7-x7xxx-8-8-x8-x8-xx6h8p6-4-4-x4-x4-xx-x|*(repete)
  D|-----------—-----------------------------------------------------|  
  A|-----------------------------------------------------------------|
  E|-----------------------------------------------------------------|

Pushing forward and arching back
Bring me closer to heart attack
Say goodbye and just fly away
When you comeback
I have some things to say

How does it feel to know you never have to be alone
When you get home
There must be someplace here that only you and I could go
So I can show you how I


(riff1)

Dream away everyday
Try so hard to disregard
The rhythm of the rain that drops
And coincides with the beating of my heart

I'll never leave you behind
Or treat you unkind
I know you understand
And with a tear in my eye
Give me the sweetest goodbye
That I ever did receive


(|REFRAO|)

Pushing forward and arching back
Bring me closer to heart attack
Say goodbye and just fly away
When you comeback
I have some things to say

How does it feel to know you never have to be alone
When you get home
There must be someplace here that only you and I could go
So I can show you how I feel

  (Solo)

  E|-------------------11----11~-------------14-------14-------         
  B|--------11-9~---11----11------15~--15h16----15h16-----15h16
  G|-8-8-11-----------------------—----------------------------   
  D|----------------------------------------------------------- 
  A|-----------—-----------------------------------------------
  E|-----------—-----------------------------------------------

   -14-------14-------------------------------------------------|
   ----15h16----16-15-------------------------------------------|
   -------------------15-13----13-13----------------------------|
   -------------------------16-------16-15-16-16-15-13-15-16----|
   ---—---------------------------------------------------------|
   ---—---------------------------------------------------------|

  E|-----------------------------------------------------------------   
  B|------------------9----------9-11~--------9----------------------
  G|-11h13/p11p10/h11—--11-10-11--------10h11---8h10-11-----8--------
  D|-----------—----------------------------------------8-9--------8-
  A|-----------—----------------------------------------------9-11---
  E|-----------—--------------------------------------------------—--

   -14p13-14p13----11---11-11-14--11---11-11-14--11-------|
   -------------11-11---11-11-11--11---11-11-11--11-------|
   -------------------------------------------------------|
   -------------------------------------------------------|
   -------------------------------------------------------|
   -------------------------------------------------------|
