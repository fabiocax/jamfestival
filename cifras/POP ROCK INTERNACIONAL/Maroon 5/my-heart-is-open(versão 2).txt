Maroon 5 - My Heart Is Open

Capo Casa 1

[Intro]  Am - F - C
 
Am           F
I know you're scared 
          C
And I can feel it 
Am         F              C
It's in the air, I know you feel that too 
Am        F
But take a chance on me 
    C       E   Am  F  C
You won't regret it, no 
Am      F             C
One more "no" and I'll believe you 
Am        F             C
I'll walk away and I will leave you be 
Am            F                   C       E     Am  F  C
And that's the last time you'll say no, say no to me 
            F             Am         F
It won't take me long to find another lover 
    Am     G
But I want you 
        F            Am            F   Am     G
I can't spend another minute getting over loving you 

                      Am F C
If you don't ever say yeeeaaah 
                    Am F C
Let me hear you say yeeeaaah 
                   Am  F C
Wanna hear you say yeah yeah yeah 
      Am F      C
Until my heart is open 
                  Am F C
Now you gotta say yeeeaaah 
                    Am F C
Let me hear you say yeeeaaah 
                   Am  F  C
Wanna hear you say yeah yeah yeah 
               Am
Wanna hear you say 
               F
Wanna hear you say 
                C
Wanna hear you say 
 
Am       F        C
Yes, yes, yes, yes, yes, yeah 
Am       F        C
Yes, yes, yes, yes, yes 

Am         F           C
It's just a moment going season 
Am         F               C
Don't be afraid to give your heart to me 
Am        F              C       E      Am   F C
And if you do, I know that I won't let you down, no 
     Am         F             C
Yeah, so hand it over, trust me with your love 
Am     F           C
I'll do anything you want me to 
Am           F             C       E   Am F C
Cause I can't breath until I see your face 
       F                   Am             F
Oh and I don't need time to find another lover 
   Am      G
But I want you 
        F            Am            F   Am    G
I can't spend another minute getting over loving you 
 
                      Am F C
If you don't ever say yeeeaaah 
                    Am F C
Let me hear you say yeeeaaah 
                   Am  F C
Wanna hear you say yeah yeah yeah 
      Am F      C
Until my heart is open 
                  Am F C
Now you gotta say yeeeaaah 
                    Am F C
Let me hear you say yeeeaaah 
                   Am  F  C
Wanna hear you say yeah yeah yeah 
               Am
Wanna hear you say 
               F
Wanna hear you say 
                C
Wanna hear you say 

E
Yes, yes, yes 
Am  F   C
Yes, yes, yes

----------------- Acordes -----------------
G = 3 2 0 0 0 3
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
