Maroon 5 - Memories

[Intro] B  F#  G#m  D#m
        E  B  E  F#

   B      F#     G#m    D#m 
E|------------------------------------------|
B|-----4------2-------------7---------------|
G|-----4------3------4------8---------------|
D|---4------4--------6----8-----------------|
A|-2---------------6----6-------------------|
E|--------2------4--------------------------|

   E      B      E      F# 
E|------------------------------------------|
B|------------4------0------2---------------|
G|-----1------4------1------3---------------|
D|-----2----4------2------4-----------------|
A|---2----2---------------------------------|
E|-0-------------0------2-------------------|

[Primeira Parte]


 B                   F#
Here's to the ones that we got
  G#m               D#m
Cheers to the wish you were here, but you're not
             E               B
'Cause the drinks bring back all the memories
   E                 F#
Of everything we've been through
 B                 F#
Toast to the ones here today
 G#m                D#m
Toast to the ones that we lost on the way
             E               B
'Cause the drinks bring back all the memories
         E              F#
And the memories bring back

Memories bring back you

[Segunda Parte]

           B             F#
There's a time that I remember
        G#m             D#m
When I did not know no pain
        E             B
When I believed in forever
           E                   F#
And everything would stay the same
        B                 F#
Now my heart feel like December
          G#m           D#m
When somebody say your name
          E                  B
'Cause I can't reach out to call you
        E              F#
But I know I will one day, yeah

[Refrão]

B       F#
  Everybody hurts sometimes
G#m              D#m
Everybody hurts someday, ayy-ayy
E            B
  But everything gon' be alright
 E               F#
Go and raise a glass and say, ayy

[Primeira Parte]

 B                   F#
Here's to the ones that we got
  G#m               D#m
Cheers to the wish you were here, but you're not
             E               B
'Cause the drinks bring back all the memories
   E                 F#
Of everything we've been through
 B                 F#
Toast to the ones here today
 G#m                D#m
Toast to the ones that we lost on the way
             E               B
'Cause the drinks bring back all the memories
         E              F#
And the memories bring back, memories bring back you

 B        F#
Doo-doo, doo-doo-doo-doo
 G#m              D#m
Doo-doo-doo-doo, doo-doo-doo-doo
 E                B
Doo-doo-doo-doo, doo-doo-doo
 E              F#
Memories bring back, memories bring back you

           B             F#
There's a time that I remember
        G#m           D#m
When I never felt so lost
        E               B
When I felt all of the hatred
         E            F#
Was too powerful to stop (ooh, yeah)
        B                 F#
Now my heart feel like an ember
          G#m             D#m
And it's lighting up the dark
         E                B
I'll carry these torches for ya
           E               F#
That you know I'll never drop, yeah

[Refrão]

B       F#
  Everybody hurts sometimes
G#m              D#m
Everybody hurts someday, ayy-ayy
E            B
  But everything gon' be alright
 E               F#
Go and raise a glass and say, ayy

[Primeira Parte]

 B                   F#
Here's to the ones that we got
  G#m               D#m
Cheers to the wish you were here, but you're not
             E               B
'Cause the drinks bring back all the memories
   E                 F#
Of everything we've been through
 B                 F#
Toast to the ones here today
 G#m                D#m
Toast to the ones that we lost on the way
             E               B
'Cause the drinks bring back all the memories
         E              F#
And the memories bring back, memories bring back you

 B        F#
Doo-doo, doo-doo-doo-doo
 G#m              D#m
Doo-doo-doo-doo, doo-doo-doo-doo
 E                B
Doo-doo-doo-doo, doo-doo-doo
 E              F#
Memories bring back, memories bring back you

 B        F#
Doo-doo, doo-doo-doo-doo
 G#m              D#m
Doo-doo-doo-doo, doo-doo-doo-doo
 E                B
Doo-doo-doo-doo, doo-doo-doo
 E              F#
Memories bring back, memories bring back you

( B  F#  G#m )
( D#m  E  B )

 E              F#                        B
Memories bring back, memories bring back you

----------------- Acordes -----------------
B = X 2 4 4 4 2
D#m = X X 1 3 4 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
