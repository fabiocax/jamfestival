Maroon 5 - Don't Wanna Know

[Intro] C D Em E5(9)

               C
I don't wanna know, know, know, know
                  D
Who's taking you home, home, home, home
               Em
And loving you so, so, so, so
    E5(9)
The way I used to love you, no
               C
I don't wanna know, know, know, know
                  D
Who's taking you home, home, home, home
               Em
And loving you so, so, so, so
    E5(9)
The way I used to love you, oh

I don't wanna know


 C               D
Wasted (wasted)
                              Em                  E5(9)
And the more I drink the more I think about you
                  C         D
Oh no, no, I can't take it
                   Em                     E5(9)
Baby every place I go reminds me of you


       C                       D
Do you think of me? Of what we used to be?
      Em                  E5(9)
Is it better now that I'm not around?
               C                                D
My friends are actin' strange, they don't bring up your name
        Em                 E5(9)
Are you happy now? Are you happy now?

               C
I don't wanna know, know, know, know
                  D
Who's taking you home, home, home, home
               Em
And loving you so, so, so, so
    E5(9)
The way I used to love you, no
               C
I don't wanna know, know, know, know
                  D
Who's taking you home, home, home, home
               Em
And loving you so, so, so, so
    E5(9)
The way I used to love you, oh

I don't wanna know

 C                D
And every time I go out, yeah
               Em                                              E5(9)
I hear it from this one, I hear it from that one, glad you got someone new yeah
C                    D
I see but I don't believe it
        Em                                        E5(9)
Even in my head you're still in my bed, maybe I'm just a fool

       C                       D
Do you think of me? Of what we used to be?
      Em                  E5(9)
Is it better now that I'm not around?
               C                                D
My friends are actin' strange, they don't bring up your name
        Em                 E5(9)
Are you happy now? Are you happy now?

               C
I don't wanna know, know, know, know
                  D
Who's taking you home, home, home, home
               Em
And loving you so, so, so, so
    E5(9)
The way I used to love you, no
               C
I don't wanna know, know, know, know
                  D
Who's taking you home, home, home, home
               Em
And loving you so, so, so, so
    E5(9)
The way I used to love you, oh

I don't wanna know

C
No more "please stop"
        D
No more hashtag boo'd up screenshots
         Em
No more tryin' make me jealous on your birthday
              E5(9)
You know just how I made you better on your birthday, oh
      C
Do he do you like this, do he woo you like this?
       D
Do he let it down for you, touch your poona like this?
       Em
Matter fact, never mind, we'll let the past be
      E5(9)
Maybe his right now, but your body's still me, woah

 C               D
Wasted (wasted)
                              Em                  E5(9)
And the more I drink the more I think about you
                  C         D
Oh no, no, I can't take it
                   Em                     E5(9)
Baby every place I go reminds me of you

               C
I don't wanna know, know, know, know
                  D
Who's taking you home, home, home, home
               Em
And loving you so, so, so, so
    E5(9)
The way I used to love you, no
               C
I don't wanna know, know, know, know
                  D
Who's taking you home, home, home, home
               Em
And loving you so, so, so, so
    E5(9)
The way I used to love you, oh
               C
I don't wanna know, know, know, know
                  D
Who's taking you home, home, home, home
               Em
And loving you so, so, so, so
    E5(9)
The way I used to love you, no
               C
I don't wanna know, know, know, know
                  D
Who's taking you home, home, home, home
               Em
And loving you so, so, so, so
    E5(9)
The way I used to love you, oh

I don't wanna know

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
E5(9) = 0 2 4 4 0 0
Em = 0 2 2 0 0 0
