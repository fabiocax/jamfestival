Maroon 5 - Infatuation

C#m                                      F#m
Baby, I don't want to spend my life on trial
B                       E
For something that I did not do
And maybe if you stopped and looked around some time
I wouldn't pass right by you

Maybe it's because you are so insecure
Maybe your pain don't care
Maybe it's the chase that really gets me off
I fall so when it's just not there

Am
Burn another bridge, break another heart
B
Try again, it will only fall apart

E            C#m
Infatua....tion
F#m                               B
Not seeing the rest of you is getting the best of me

E               C#m
It's such a shame that you shot me down
F#m                               B
It would have been nice to be around
E                       C#m
I'm touching your skin
F#m                               B
If it's only a fantasy, then why is it killing me?
Em  C#m         F#m            B
I.....guess this must be.......infatuation

E
I want it, I want it,
C#m
I want it, I want it,
F#m
I want it, I want it,
B
(I want it…x7)

Try to put my finger on what burns me up
It always seems to escape me
And when you have decided that you've had enough
Just tell me where I need to be

Now her face is something that I never had
To ever deal with before
She left me with the feeling that she'd had enough

And I'm the one wanting more

Burn another bridge, break another heart
Try again, it will only fa ll apart

Infatuation
Not seeing the rest of you is getting the best of me
It's such a shame that you shot me down
It would have been nice to be around
I'm touching your skin
If it's only a fancy, then why is it killing me?
And I guess this must be infatuation (I want it…x7)

G       F#m            E
I'm so attracted to you
G       F#m            E
The feeling's mutual too
G       F#m            E
And I get scared the moment you leave

Get so hot I forget to breathe, yeh

Infatuation
Not seeing the rest of you is getting the best of me
It's such a shame that you shot me down
It would have been nice to be around
I'm touching your skin
If it's only a fancy, then why is it killing me?
I guess this must be infatuation (I want it…x3)
Ooh (I want it…x6)
Ooh (I want it…x3)
Yeh… (I want it…x8)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
