Maroon 5 - No Curtain Call

Bb5                                           A5
 You say you need someone. But everybody does
                          F5
I'm no different than you

I just believe what I do
Bb5                                            A5                                 F5
You point your finger at. Everyone but yourself, and blame the ones that you love
                          Gb
Who're only try'n to help
                        Db
As it's winding down to zero
                   Ab                        F
I am yours like a hero. I'll see this through
                           Gb
There's so much me and you
                   Db
Take this enemy together
                          Ab               Eb5
Fight these demons off forever, forever, forever, forever.


(refrão)
Bbm                                   Db
5,4,3,2,1 I won't stop until it's done
                Ab               F/A
No curtain call, I will not fall
Bbm                                        Db
This may be the one we've been waiting for
                Ab                F/A
No curtain call, just take it all.

Bb5                                         A5
I have no time for fear. Or people in my ear
                             F5
Head down and running so fast.

Try not to dwell on the past.
Bb5                                                       A5                             F5
I'm fighting through this pain, and things I cannot change. Running right into the flame
                         Gb
Rather than running away
                        Db
As it's winding down to zero
                   Ab                        F
I am yours like a hero. I'll see this through
                           Gb
There's so much me and you
                   Db
Take this enemy together
                          Ab               Eb5
Fight these demons off forever, forever, forever, forever.

(refrão)
Bbm                                   Db
5,4,3,2,1 I won't stop until it's done
                Ab               F/A
No curtain call, I will not fall
Bbm                                        Db
This may be the one we've been waiting for
                Ab                F/A
No curtain call, just take it all.

Bbm                          Ab                                                    Bbm
Sweat drips down from every angle. Love your body as it gathers in a pool by your feet.
                  Gb
You turn up the heat.
                Db                   Ab
Tossin' and turnin', you cannot sleep.
            F/A
Quietly weep, your in too deep.

{refrão}

----------------- Acordes -----------------
A5 = X 0 2 2 X X
Ab = 4 3 1 1 1 4
Bb5 = X 1 3 3 X X
Bbm = X 1 3 3 2 1
Db = X 4 6 6 6 4
Eb5 = X 6 8 8 X X
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
F5 = 1 3 3 X X X
Gb = 2 4 4 3 2 2
