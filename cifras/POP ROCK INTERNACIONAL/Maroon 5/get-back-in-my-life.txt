Maroon 5 - Get Back In My Life

(intro) C#m7 F#m7 C#m7 C#m7 F#m7 C#m7

C#m7
You are relentless, I am defenseless
             F#m7            C#m7
Why did you knock me down tonight?
C#m7
You keep me senseless, I just don't get this
         F#m7                C#m7
How many times do I have to try?
C#m7
Your whisper, so clear, the world disappeared
           F#m7
Cause I'll fall into the darkness, it's impossible to express
    C#m7
How good it feels, I'm wrapped up, I'm sealed
      F#m7
So entitled, I'll never be free

Oh, I don't fight the feeling


(refrão)
C#m7                                  F#m7
Get back in my life, come knock on my door
                 E9      G#m7              C#m7
What I looking for? I think you should know
                                   F#m7
You started a fight, put me to the floor
                      E9
Please don't resist anymore
            G#m7        C#m7
I'll never leave you alone

( C#m7 F#m7 C#m7 )

C#m7
Beauty from many, you're so demanding
               F#m7                 C#m7
But I've got time, I don't mind at all
C#m7
You're picture perfect, completely worthy
          F#m7               C#m7
You got my back against my wall

Your whisper, so clear, the world disappeared
        F#m7
Cause I fall into the darkness, impossible to express
     C#m7
How good it feels, I'm wrapped up, I'm sealed
      F#m7
So entitled, I'll never be free (yeah)

Oh, I don't fight the feeling

(refrão)
C#m7                                  F#m7
Get back in my life, come knock on my door
                 E9      G#m7           C#m7
What I looking for? I think you should know
                                   F#m7
You started a fight, put me to the floor
                      E9
Please don't resist anymore
            G#m7        C#m7
I'll never leave you alone

( C#m )

And can you hear me calling?
I'm screaming, scratching, crawling
You ignore me cause I'm always
Coming back to you, coming back to you
And you know how much I miss it
And you know I can't resist it
See your lips and just come running
Running back to you, running back to you

(refrão)
C#m7                                  F#m7
Get back in my life, come knock on my door
               E9      G#m7              C#m7
What I looking for? I think you should know
                                   F#m7
You started a fight, put me to the floor
                      E9
Please don't resist anymore
            G#m7        C#m7
I'll never leave you alone

----------------- Acordes -----------------
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
E9 = 0 2 4 1 0 0
F#m7 = 2 X 2 2 2 X
G#m7 = 4 X 4 4 4 X
