Maroon 5 - Won't Go Home Without You

Capo Casa 1

  D                                   Em
I asked her to stay, but she wouldn't listen
    A                               D    D/C#
She left before I had the chance to say,  oh
    Bm                                          Em
the words that were meant, the things that were broken
    G                A                D
But now it's far too late she's gone away.

      D
Every night you cry yourself to sleep
         Bm
Thinking why does this hap-pen to me
         G                          A
Why does every moment have to be so hard?

Hard to believe it

D
It's not over tonight
                      Em
Just give me one more chance to make it right

                  G
I may not make it through the night
  A                 D
I won't go home without you

    Bm                                  Em
The taste of her breath, I'll never get over
        A                             D     D/C#
And the noises that she made keep me awake, oh
    Bm                          Em
the weight of things, re-main unspoken
      G             A               D
built up so much it crushed us everyday

Every night you cry yourself to sleep
         Bm
Thinking why does this hap-pen to me
         G                          A
Why does every moment have to be so hard?

Hard to believe it

D
It's not over tonight
                      Em
Just give me one more chance to make it right
                  G
I may not make it through the night
  A                 D
I won't go home without you

D
It's not over tonight
                      Em
Just give me one more chance to make it right
                  G
I may not make it through the night
  A                 D
I won't go home without you

C          G                  D            A
Of all the things I felt I've never really showed
F          C               G            A
Perhaps the worst is that I ever let you go,

Should not ever let you go oh oh oh oh

D
It's not over to night
                      Em
Just give me one more chance to make it right
                  G
I may not make it through the night
  A                 D
I won't go home without you

D
It's not over to night
                      Em
Just give me one more chance to make it right
                  G
I may not make it through the night
  A                 D
I won't go home without you

I won't go home without you
                    Bm
I won't go home without you
  G                 D
I won't go home without you

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/C# = X 4 0 2 3 2
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
