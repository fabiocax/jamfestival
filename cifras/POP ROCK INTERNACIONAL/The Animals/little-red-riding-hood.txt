The Animals - Little Red Riding Hood

Who's that I see walking in these woods?
Why it's Little Red Riding Hood.

Em                   G
Hey there Little Red Riding Hood.

Am
You sure are lookin' good,

C                   B7                 Em    B7
You're everything a big bad wolf could want.

Listen to me!

Em         G
Little Red Riding Hood,

Am
I don't think little big girls should.

C                           B7          Em      B7
Go walking' in these spooky old woods alone.



G
What big eyes you have,

Em
The kind of eyes that drive wolves mad.

       A7
So just to see that you don't get chased,

          D7
I think I ought to walk with you for a way.

G
What full lips you have.

Em
They're sure to lure someone bad.

  A7
So until you get to Gramma's place,

  D7
I think you ought to walk with me and be safe.


I'm gonna keep my sheep suit on,
'Til I'm sure you've been shown,
That I cam be trusted walkin'
with you alone. (Howl!)

Little Red Riding Hood,
I'd like to hold you if I could.
But you might think I'm a big
bad wolf, so I won't.

What a big heart I have,
The Better to love you with.
Little Red Riding Hood,
Even big bad wolves can be good.
I'll try to keep satisfied,
Just to walk by you're side.
Maybe you'll see things my way,
Before we get to Gramma's place.

Hey there Little Red Riding Hood.
You sure are lookin' good,
You're everything a big bad wolf could want.
Little Red Riding Hood.
You sure are lookin' good,
You're everything a big bad wolf could want.
Little Red Riding Hood.
You sure are lookin' good,
You're everything a big bad wolf could want.

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
