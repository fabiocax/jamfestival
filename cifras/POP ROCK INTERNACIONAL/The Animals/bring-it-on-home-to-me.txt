The Animals - Bring It On Home To Me

If you [G]ever  change your [D]mind about [G]leaving, [G7]leaving me [C]behind
Oh, [G]bring to me, [D]bring your sweet [C]loving
[D]Bring it on home to [G]me  [C]  oh [G]yeah   [D]

You know I [G]laughed  when you [D]left but now I [G]know I've [G7]only hurt my[C]self
Oh [G]bring it to me, [D]bring your sweet [C]loving
[D]Bring it on home to [G]me  yeah [C] yeah [G] yeah [D]

I'll give you [G]jewelry, money [D]too, and that's not [G]all, [G7]all I do for [C]you
Oh, [G]bring to me, [D]bring your sweet [C]loving
[D]Bring it on home to [G]me yeah [C] yeah  [G] yeah [D]

{c:Solo}
[G]      [D]      [G]      [C]      [G]      [D]      [C]      [D]      [G]      [C]      [G]      [D]
You know I'll [G]always be your [D]slave, till I'm [G]dead and [G7]buried in my [C]grave
Oh, [G]bring to me, [D]bring your sweet [C]loving
[D]Bring it on home to [G]me yeah [C] yeah [G] yeah [D]

If you [G]ever  change your [D]mind about [G]leaving, [G7]leaving me [C]behind
Oh, [G]bring to me, [D]bring your sweet [C]loving
[D]Bring it on home to [G]me  [C]  oh [G]yeah   [D]

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
