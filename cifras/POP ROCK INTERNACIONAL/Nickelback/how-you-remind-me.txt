Nickelback - How You Remind Me

Primeira Estrofe:

C5                     F5
   Never made it as a wise man
Bb5                         Eb5
    I couldn't cut it as a poor man stealing
C5                          F5
   Tired of living like a blind man
Bb5                              Eb5
    I'm sick of sight without a sense of feeling
C5              Eb5/Ab        F5/Bb
And this is how        you remind me

E|-3-------3-------3----------------------------------|
B|----1-------1-------1-------------------------------|
G|-----------------------3----------------------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
D|----------------------------------------------------|



Segunda Estrofe:

C5             F5        Bb5
   This is how    you remind me
        Eb5
Of what I really am
C5             F5        Bb5
   This is how    you remind me
        Eb5
Of what I really am

   Bb5
E|-1--------------------------------------------------|
B|----------------------------------------------------|
G|----3-----------------------------------------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
D|----------------------------------------------------|


Primeiro Pré-Refrão:

C5                   Eb5
   It's not like you     to say sorry
Bb5                     F5
    I was waiting on a different story
C5               Eb5
   This time I'm     mistaken
Bb5                    F5
    For handing you a heart worth breaking

C5                   Eb5
And I've been wrong,     I've been down
Bb5                       F5
    Been to the bottom of every bottle
C5             Eb5
   These five words in my head
Bb5                 F5
    Scream "are we having fun yet?"


Primeiro Refrão:

C5     F5     Bb5     Eb5
   yet    yet     yet     no no
C5     F5     Bb5     Eb5
   yet    yet     yet     no no

   C5(9)      Bb5(9)
E|-3--3-------1--1--------------------------|
B|-3----------1----1------------------------|
G|-5-----5----3------3----------------------|
D|-5----------3-----------------------------|
A|-3----------1-----------------------------|
D|------------------------------------------|

(Riff 1)
   C5
E|------------------------------------------|
B|------------------------------------------|
G|-5----------------------------------------|
D|-5----------------------------------------|
A|-3--/5--/6--------------------------------|
D|------------------------------------------|


Terceira Estrofe:

It's not like you didn't know that

I said I love you and I swear I still do
C5(9)
      And it must have been so bad
Bb5(9)
       Cause living with me must

Have damn near killed you

And this is how, you remind me

Of what I really am
                    Bb5
This is how, you remind me
        Eb5
Of what I really am

   C5(9)      Bb5(9)
E|-3--3-------1-----------------------------|
B|-3----------1-----------------------------|
G|-5-----5----3-----------------------------|
D|-5----------3-----------------------------|
A|-3----------1-----------------------------|
D|------------------------------------------|

E|-3-------3-------1------------------------|
B|----1-------1-------1---------------------|
G|------------------------------------------|
D|------------------------------------------|
A|------------------------------------------|
D|------------------------------------------|

E|-3-------3-------1------------------------|
B|----1-------1-------1---------------------|
G|------------------------------------------|
D|------------------------------------------|
A|------------------------------------------|
D|------------------------------------------|


Segundo Pré-Refrão:

C5                   Eb5
   It's not like you     to say sorry
Bb5                     F5
    I was waiting on a different story
C5               Eb5
   This time I'm     mistaken
Bb5                    F5
    For handing you a heart worth breaking

C5                   Eb5
And I've been wrong,     I've been down
Bb5                       F5
    Been to the bottom of every bottle
C5             Eb5
   These five words in my head
Bb5                 F5
    Scream "are we having fun yet?"


Segundo Refrão:

C5     F5     Bb5     Eb5
   yet    yet     yet     no no
C5     F5     Bb5     Eb5
   yet    yet     yet     no no
C5     F5     Bb5     Eb5
   yet    yet     yet     no no
C5     F5     Bb5     Eb5       C5
   yet    yet     yet     no no

   C5(9)      Bb5(9)
E|-3--3-------1--1--------------------------|
B|-3----------1----1------------------------|
G|-5-----5----3------3----------------------|  (4x)
D|-5----------3-----------------------------|
A|-3----------1-----------------------------|
D|------------------------------------------|


( C5  F5  Bb5  Eb5 )  (2x)

E|------------------------------------------|
B|-3----1-----------------------------------|
G|-5----3-----------------------------------|
D|-5----3-----------------------------------|
A|-3----1-----------------------------------|
D|------------------------------------------|


Quarta Estrofe:

C5                     F5
   Never made it as a wise man
Bb5                         Eb5
    I couldn't cut it as a poor man stealing
C5              Eb5/Ab        F5/Bb
And this is how        you remind me
C5             Eb5/Ab        F5/Bb
   This is how        you remind me

C5             F5        Bb5
   This is how    you remind me
        Eb5
Of what I really am
C5             F5        Bb5
   This is how    you remind me
        Eb5
Of what I really am

E|-3-------3-------1------1-----------------|
B|----1-------1-------1------1--------------|
G|------------------------------------------|
D|------------------------------------------|
A|------------------------------------------|
D|------------------------------------------|

E|-3-------3-------1------------------------|
B|----1-------1-------1---------------------|
G|------------------------------------------|
D|------------------------------------------|
A|------------------------------------------|
D|------------------------------------------|


Terceiro Pré-Refrão:

C5                   Eb5
   It's not like you     to say sorry
Bb5                     F5
    I was waiting on a different story
C5               Eb5
   This time I'm     mistaken
Bb5
    For handing you a heart worth breaking

C5                   Eb5
And I've been wrong,     I've been down
Bb5                       F5
    Been to the bottom of every bottle
C5             Eb5
   These five words in my head
Bb5                 F5
    Scream "are we having fun yet?"


Refrão Final:

C5     Eb5     Bb5
   Yet     yet
        F5
Are we having fun yet

C5     Eb5     Bb5
   Yet     yet
        F5
Are we having fun yet

C5     Eb5     Bb5
   Yet     yet
        F5
Are we having fun yet

   C5(9)      Bb5(9)
E|-3--3-------1--1--------------------------|
B|-3----------1----1------------------------|
G|-5-----5----3------3----------------------|  (3x)
D|-5----------3-----------------------------|
A|-3----------1-----------------------------|
D|------------------------------------------|

C5     Eb5     Bb5
   Yet     yet

   C5(9)
E|-3--3------------6-------6-----6-------6--|
B|-3--------6----6-------6-----6-------6----|
G|-5-----5--5--------5-------------5--------|
D|-5----------------------------------------|
A|-3----------------------------------------|
D|----------6-------------------------------|

----------------- Acordes -----------------
Bb5 = X 1 3 3 X X
Bb5(9) = X 1 3 3 1 1
C5 = X 3 5 5 X X
C5(9) = X 3 5 5 3 3
Eb5 = X 6 8 8 X X
F5 = 1 3 3 X X X
