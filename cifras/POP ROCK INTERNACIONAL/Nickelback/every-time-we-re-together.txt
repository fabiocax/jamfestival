Nickelback - Every Time We're Together

Am               C
   I'm back in my hometown
               G
The main street's dying down
                    F
It's like there's nothing left of it

Am               C
   No more high school grounds
                G
They tore that building down
           F
And everything we built, I miss

Am               C
   I cut off my own friends
               G
So lost inside seeing them
                 F
Feels like were back there instantly, to me


Am            C
   We all need a chance
          G
The backyard fires on
            F
And we stop telling tales of how life used to be

F                G
  The years have come and gone
                F
But these stories carry on
            G
Looking back, they're better than they used to be

     C
Cause you should'a seen the size of the guy's we were fightin'
   F
And we shouldn't be alive at the speeds we were driving
  Am               G            F
But momma always taught us never ever to tell a lie

     C
And every tenny out there's always turned into twenty
   F
Every girl we were with never cost any money
   Am                G        F
And we drank more tequila than any man alive

           Bb
And the story goes on
             F N.C.
It gets a little better, every time we're together

( C  F  Am  G  F )

Am                C
   It's funny how we change
              G
And some things stay the same
         D5
God, I miss the glory days, always

Am               C
   Daylight getting near
            G
We shed a few more tears
           D5
Lord, I love the memories we've made

F                G
  The years have come and gone
                F
But these stories carry on
            G
Looking back, they're better than they used to be

     C
Cause you should'a seen the size of the guy's we were fightin'
   F
And we shouldn't be alive at the speeds we were driving
  Am               G            F
But momma always taught us never ever to tell a lie

     C
And every tenny out there's always turned into twenty
   F
Every girl we were with never cost any money
   Am                G        F
And we drank more tequila than any man alive

           Bb
And the story goes on
             F N.C.
It gets a little better, every time we're together

( C  F  Am  G  F )

     C
Cause you should'a seen the size of the guy's we were fightin'
   F
And we shouldn't be alive at the speeds we were driving
  Am               G            F
But momma always taught us never ever to tell a lie

     C
And every tenny out there's always turned into twenty
   F
Every girl we were with never cost any money
   Am                G        F
And we drank more tequila than any man alive

           Bb
And the story goes on
             F N.C.
It gets a little better, every time we're together

( C  F  Am  G  F )

           Bb
And the story goes on
             F N.C.
It gets a little better, every time we're together

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D5 = X 5 7 7 X X
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
