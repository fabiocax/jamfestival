Nickelback - Someday

Verso 1:
Bm                       D         A
How the hell did we wind up like this
            Bm
Why weren't we able
             D             A
To see the signs that we missed
           Bm
And try to turn the tables
                   D            A
I wish you would unclench your fists
            Bm
And unpack your suitcase
                     D            A
Lately there's been too much of this
           Bm
Dont think its too late

( Em )

Pré-Refrão:

Em
Nothin's wrong, just as long as
A5
you know that someday I will

Refrão:
G       D
Someday, somehow
A5                 Bm
gonna make it allright but not right now
G         A5        Bm
I know you're wondering when
                 A5
(You're the only one who knows that)
G       D
Someday, somehow
A5                 Bm
gonna make it allright but not right now
G         A5        Bm
I know you're wondering when

Verso 2:
       Bm                 D              A5
Well I hoped that since we're here anyway
         Bm
We could end up saying
                      D          A5
Things we've always needed to say
             Bm
So we could end up stringing

Bm                D               A5
Now the story's played out like this
                  Bm
Just like a paperback novel
                  D           A5
Lets rewrite an ending that fits
               Bm
Instead of a hollywood horror

Pré-Refrão:
Em
Nothin's wrong, just as long as
A5
you know that someday I will

Refrão:
G      D
Someday, somehow
A5                     Bm
gonna make it allrght but not right now
G           A5        Bm
I know you're wondering when
              A5
(You're the only one who knows that)
G      D
Someday, somehow
A5               Bm
gonna make it allright but not right now
G            A5         Bm
I know you're wondering when

(You're the only one who knows that)
Solo:
E|----------------------------------------------------|---------------------------------------|
B|----------------------------------------------------|---------------------------------------|
G|------4/6----4-----4/6-7-6---4--------2-4-------6-4-|-4-4-4-6-6-7-7-------6-6-6-7-6---------|
D|----0------7-----0---------7--------0-------2-------|---------------7-7-7-------------------|
A|----------------------------------------------------|---------------------------------------|
E|----------------------------------------------------|---------------------------------------|

E|--------------------------------|
B|--------------------------------|
G|-4-4-4-6-6-6-7-7-----9-9-6-6-7-6|
D|-----------------7-7------------|
A|--------------------------------|
E|--------------------------------|

E|---------------------------------|
B|---------------------7-7---------|
G|-4-4-4-6-6-6-7-7---------7-7-9-7-|
D|-----------------7-7-------------|
A|---------------------------------|
E|---------------------------------|

E|--------------------|
B|--------------------|
G|-4-4/6-7---9-9/11---|
D|--------7---------9~|
A|--------------------|
E|--------------------|

Verso 3:
Bm                    D             A
How the hell did we wind up like this
        Bm
Why weren't we able
            D             A
To see the signs that we missed
           Bm
And try to turn the tables
                  D                A
Now the story's played out like this
              Bm
Just like a paperback novel
                  D            A
Lets rewrite an ending that fits
             Bm
Instead of a hollywood horror

Pré-Refrão:
E5
Nothin's wrong, just as long as
A5
you know that someday I will

Refrão:
G       D
Someday, somehow
A5                    Bm
gonna make it allright but not right now
G5            A5        Bm
I know you're wondering when
              A5
(You're the only one who knows that)
G       D
Someday, somehow
A5                    Bm
gonna make it allright but not right now
G            A5        Bm
I know you're wondering when
              A5
(You're the only one who knows that)

Outro:
G            A5        Bm
I know you're wondering when
              A5
(You're the only one who knows that)
G            A5        Bm
I know you're wondering when

----------------- Acordes -----------------
A = X 0 2 2 2 0
A5 = X 0 2 2 X X
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E5 = 0 2 2 X X X
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G5 = 3 5 5 X X X
