Nickelback - Savin' Me

(riff)

E|----------------------------------------|
B|----------------------------------------|
G|----------------------------------------|
D|---5-3--------5-3-5-----5-3---------3---|
A|--------3-6-6-----------------3-3-6---3-|
E|-4--------------------1-----3-----------|

G#                   D#
Prision gate's won't open up for me
Fm                 Cm
On these hands and knees I'm crawlin'
G#         D#           Fm
Oh, I reach for you
        G#            D#
Well I'm terrified of these four walls
      Fm              Cm
These iron bars can't hold my soul in
G#         D#      Fm
All I need is you    (Come please I'm callin')

    G#         D#       Fm
And oh I scream for you   (Hurry I'm fallin')
                                (I'm fallin')

Cm         D#        Bb          Fm
Show me what its like (to be the last one standing)
    Cm            D#         Bb                  Fm
And teach me wrong from right (and I'll show you what i can be)
G#             D#           Fm                    Cm x x  D# x x
Say it for me, say it to me, and I'll leave this life behind me
G#             D#            Fm
Say it if it's worth saving me

G#                    D#
Heaven's gates wont't open up for me
Fm                 Cm
With these broken winds I'm fallin'
    G#      D#         Fm
And all I see is you
     G#                D#
These city walls ain't got no love for me
Fm                      Cm
I'm on the ledge of the eighteenth story
    G#         D#       Fm
And oh I scream for you  (Come please I'm callin')
    G#       D#       Fm
And all I need from you   (Hurry I'm fallin')
                                (I'm fallin')

Cm         D#        Bb          Fm
Show me what its like (to be the last one standing)
    Cm            D#         Bb                  Fm
And teach me wrong from right (and I'll show you what i can be)
G#             D#           Fm                    Cm x x  D# x x
Say it for me, say it to me, and I'll leave this life behind me
G#             D#            Fm
Say it if it's worth saving me  (Hurry I'm fallin')

(solo)

E|-----------------------------------------------------------|
B|-----------------------------------------------------------|
G|--1-0--------1-0--------1-0--------------------------------|
D|------3-1-3------3-1-3------3-1-3---------1-1/5--3--2/3--1-|
A|-----------------------------------3-3-3-------------------|
E|-----------------------------------------------------------|

E|------------------------------------------------------------|
B|------------------------------------------------------------|
G|--1-0---------------------------------0-0-0-0-1-1-1-1-3-3-3-|
D|------3-1-3--------1-1/5--3--3-3-3-3------------------------|
A|-------------3-x-3------------------------------------------|
E|------------------------------------------------------------|

E|------------------------------------------------------------|
B|---9-8-6---6---9-8-6---6---9-8-6---6------------7/8-6-5/6---|
G|---------8-----------8-----------8-----5-5-5-8------------8-|
D|------------------------------------------------------------|
A|------------------------------------------------------------|
E|------------------------------------------------------------|

E|--------------------------13~-|
B|--6-8-8-8-8-9-9-9-11-9-11-----|
G|------------------------------|
D|------------------------------|
A|------------------------------|
E|------------------------------|

    G#       D#       Fm
And all I need is you   (Come please I'm callin')
    G#         D#       Fm
And oh I scream for you   (Hurry I'm fallin')
                                (I'm fallin')
                                (I'm fallin')
                                (I'm fallin')

Cm         D#        Bb          Fm
Show me what its like (to be the last one standing)
    Cm            D#         Bb                  Fm
And teach me wrong from right (and I'll show you what i can be)
G#             D#           Fm                    Cm x x  D# x x
Say it for me, say it to me, and I'll leave this life behind me
G#             D#            Fm
Say it if it's worth saving me  (Hurry I'm fallin')
G#             D#           Fm                    Cm x x  D# x x
Say it for me, say it to me, and I'll leave this life behind me
G#             D#            Fm
Say it if it's worth saving me

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
D# = X 6 5 3 4 3
Fm = 1 3 3 1 1 1
G# = 4 3 1 1 1 4
