Nickelback - Photograph

(Verso 1)

Eb                   Bb                                C#
 Look at this Photograph, every time I do it makes me laugh,
                         Ab                                   Eb
How did our eyes get so red,  and what the hell is on Joey's head?
                         Bb                                      C#
And this is where I grew up,  I think the present owner fixed it up,
                              Ab                                          Eb
I never knew we ever went without,  the second floor is high for sneaking out
                               Bb                                          C#
And this is where I went to school, most of the time had better things to do,
                                  Ab                                  Eb
Criminal record says I broke in twice, I must have done half a dozen times,
                      Bb                                 C#
I wonder if it's too late, should I go back and try to graduate,
                                     Ab                                  F#
Life's better now than it was back then, if I was them I wouldn't let me in,

                Ab
 Oh oh oh oh... oh God I..


(Refrão)

        Eb
 Every memory of looking out the back door,
             Bb
 I had the photo album spread out on my bedroom floor,
       C#
 It's hard to say it, time to say it,
  Ab
 Good-bye, good-bye.....

        Eb
 Every memory of walking out the front door,
               Bb
 I found the photo of the friend that I was looking for,
       C#
 It's hard to say it, time to say it,
  Ab
 Good-bye, good-bye.....

(Solo)

Eb  Bb  C#  Ab

(Verso 2)

Eb                   Bb                                   C#
 Remember the old arcade, blew every dollar that we ever made,
                           Ab                                        Eb
The cops seeing us hanging out, they say somebody went and mowed it down,
                          Bb                                          C#
We used to listen to the radio, and sing along with every song we'd know,
                                      Ab                                             Eb
We said someday we'd find out how it feels, to sing to more than just the steering wheel.

                        Bb                                     C#
Kim's the first girl I kissed, I was so nervous that I nearly missed,
                                   Ab                                       F#
She's had a couple of kids since then, I haven't seen her since God knows when,

               Ab
 Oh oh oh oh...oh God I..

(Refrão)

        Eb
 Every memory of looking out the back door,
             Bb
 I had the photo album spread out on my bedroom floor,
       C#
 It's hard to say it, time to say it,
  Ab
 Good-bye, good-bye.....

        Eb
 Every memory of walking out the front door,
               Bb
 I found the photo of the friend that I was looking for,
       C#
 It's hard to say it, time to say it,
  Ab
 Good-bye, good-bye.....

(Ponte)

 B  C#

Eb              Bb
   I miss that town
               C#
   I miss the faces
               Ab
   You can't erase it
                 Eb
   You can't replace it

              Bb
   I miss it now
              C#
   I can't believe it
                Ab
   So hard to stay
               Eb
   So hard to leave it

(Verso 3)

Eb                          Bb                                           C#
  If I could re-live those days, I know the one thing that would never change....

(Refrão)

        Eb
 Every memory of looking out the back door,
             Bb
 I had the photo album spread out on my bedroom floor,
       C#
 It's hard to say it, time to say it,
  Ab
 Good-bye, good-bye.....

        Eb
 Every memory of walking out the front door,
               Bb
 I found the photo of the friend that I was looking for,
       C#
 It's hard to say it, time to say it,
  Ab
 Good-bye, good-bye.....

(Verso 4)

Eb                   Bb                                C#
 Look at this Photograph, every time I do it makes me laugh,
                           Ab
 Every time I do it makes me

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
B = X 2 4 4 4 2
Bb = X 1 3 3 3 1
C# = X 4 6 6 6 4
Eb = X 6 5 3 4 3
F# = 2 4 4 3 2 2
