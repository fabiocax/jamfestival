Dire Straits - Skateaway

Intro:  D  G  C  D  G  C  G
        D  G  C  D  G  C  G

            D                          G
   I seen a girl on a one-way corridor
C             D               G  C6   G
   Stealing down a wrong way street
D                            G     C
   For all the world like an urban toreador
               D         G C
   She had the wheels on - on her feet
G       D                     C
   Well the cars do the usual dances
D                          G        C6
   Same old cruise and the kerbside crawl
G  D                        G      C
   But the rollergirl she's taking chances
D                    G     C               G
   They just love to see her take them all

         A           G
           No fears alone at night

                 Em/B                G
           She's sailing through the crowd
         A                 C
           In her ears the phones are so tight
                   G               A    D/G     A
           And the music's playing loud

            D           G
              Hallelujah
C                 D               G    C6  G
   Here she comes queen rollerball
D           C
   Enchante what can I say
   D            G    C
   I don't care      at all
G  D                 G          C
   You know she used to have to wait around
                      D           G    C6  G
   She used to be the lonely one
   D                    G      C
   But now that she can skate around the town
   D              G   C          G
   She's the only one   only one

                A            G
           No fears alone at night
                 Em/B                G
           She's sailing through the crowd
        A                  C
           In her ears the phones are so tight
                   C               D
           And the music's playing loud

        C      D     Gsus4 G     Gsus4
           She get's rock  n roll
        G    G           Bm7   G/D
           A rock 'n roll station
        Em                        Em/D  G/B
           And a rock n roll dream
        C                    G/B
           She's making movies
        Am              G/D
           On location
               G          D
           She don't know what it means

                   G                                G/B
           But the music make her wanna be the story
           D       Em
           And the story was whatever was the song what it was
        C            G/B
           Rollergirl
        Am
           Don't worry
        G/D              D7
           D.J. play the movies
        Em     Em/D  C
           All night long
                     G
           All night long

       G  C
   She tortures taxi drivers just for fun
D                           G  C6  G
   She like to read their lips
        D                G  C
   Says: "Toro, toro, taxi - see 'ya tomorrow my son"
D                          G  C6          G
   She just let a big truck    graze her hip
D                  G  C
   She got her own world in the city - yeah!
D                        G   C6       G
   you can't intrude on her, no - no, no
D                 G  C
   She got her own world in the city
D                        G  C6        G
   Cos' the city's bein' so rude to her

        A            G
           No fears alone at night
                 Em/B                G
           She's sailing through the crowd
        A                  C
           In her ears the phones are so tight
                   C               D
           And the music's playing loud

        C      D     Gsus4 G     Gsus4
           She get's rock  n roll
        G    G           Bm7   G/D
           A rock 'n roll station
        Em                        Em/D  G/B
           And a rock n roll dream
        C                    G/B
           She's making movies
        Am              G/D
           On location
               G          D
           She don't know what it means

                   G                                G/B
           But the music make her wanna be the story
           D       Em
           And the story was whatever was the song what it was
        C            G/B
           Rollergirl
        Am
           Don't worry
        G/D              D7
           D.J. play the movies
        Em     Em/D  C
           All night long
                     G
           All night long

C
   Slippin' and a-slidin'
D            G  C6            G
   Yeah, life's a roller ball
D                          G  C
   Slippin' and a-slidin'
D            G C6         G  D    G  C
   Skateaway  -  that's all

D            G  C6  G
   Skateaway
   D              G   C    D       G  C6   G
   Shala shalay, hey hey, skateaway
      D               G   C    D    G  C6   G
   Now shala shalay, skateaway
                  D             G   C
   She's singin' shala shalay, hey hey
    D    G  C6  G
   Skateaway


----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
C6 = 8 X 7 9 8 X
D = X X 0 2 3 2
D/G = 3 X X 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
Em/B = X 2 2 0 0 0
Em/D = X X 0 4 5 3
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G/D = X 5 5 4 3 X
Gsus4 = 3 5 5 5 3 3
