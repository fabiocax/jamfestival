Dire Straits - Fade To Black

Dm7                             A7
Well I wonder where U R 2 nite  probably on a rampage...
F                               G7
You have been known to take...  in getting in somebody's....
Bb                              A7      Dm7     A7
Always had the knack 2          fade to black
Dm7                                     A7
I betcha already made your pass.....    I see a darkened room somewhere...

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bb = X 1 3 3 3 1
Dm7 = X 5 7 5 6 5
F = 1 3 3 2 1 1
G7 = 3 5 3 4 3 3
