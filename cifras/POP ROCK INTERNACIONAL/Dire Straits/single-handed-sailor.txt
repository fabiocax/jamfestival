Dire Straits - Single Handed Sailor

Intro:  Dm  C : G

Dm           C       Bb        F
 Two in the morning bright uptown
Dm              C                 Bb  F
 The river run away in the night
Dm           C             Bb        F
 Little Gypsy mare she's all tied down
Dm               C                      Bb  F
 She put her in the wind and the light

(Os mesmos acordes de cima)

 Yeah, the sailing ship is just held down in chains
 From the lazy days of sailing
 She's just lying there silent in pain
 You lean on the tourist rail

Am                              Dm              Dm-C-Bb
 A mother and her baby in the college of war
                   C
 In the concrete grave

Dm                  C                 Bb         F
 You never want to fight against the river law
Gm                  Bb
 Nobody rules the waves

              C             A7           Dm
 Yeah, on a night when the lazy wind is wailing
Bb                   C
 Around the cutting saw
   Dm            C     Bb      F
 Single-handed sailor go sailing
Gm                          Bb
 Singing away in the dark

 He's up on the bridge on the self-say night
 A mariner of dried-up land
 Two in the morning, well there's one green light
 And the man on the barge is sad

 She's gonna step away below him
 Away from things he's done
 But he just shouts, "Hey man whatcha call this thing?"
 She could say, "Pride of London"

 On a night when the lazy wind is wailing
 Around the cutting saw
 Yeah, single-handed sailor go sailing
 Singing away in the dark

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
