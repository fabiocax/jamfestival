Dire Straits - Where do You Think You're Going

Intro: Am    / F
       G     / Esus4

Am
     Where do you think you're going
F
     don't you know it's dark outside
G
     Where do you think you're going
Esus4
     don't you care about my pride
Am
     Where do you think you're going
F
     I think you don't know
G
     You got no way of knowing
Esus4
     there's really no place you can go
Am   G  /  F
Dm7  C  /  F

Am
     I understand your changes
F
     long before you reach the door
G
     I know where you think you're going
Esus4
     I know what you came here for
Am
     And now I'm sick of joking
F
     you know I like to be free
G
     where do you think you're going
Esus4
     i think you better  go with me, girl
Am   G  /  F
Dm7  C  /  F
G
     You say there is no reason
             Am
     but you still find cause to doubt me
G
     If you ain't with me girl
Esus4                        Am        G
     You're gonna be without me
F      / Dm7
F
Am
     Where do you think you're going
F
     don't you know it's dark outside
G
     Where do you think you're going
Esus4
I wish I didn't care about my pride
Am
     And now I'm sick of joking
F
     You know I like you to be free
G
(So) Where do you think you're going
Esus4                              Am     G
     I think you better go with me girl
F     / Dm7     C
F     / G
(double tempo)
(repeat)
Am         /         G
F          / F
Dm7        /         C
F          /         G

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm7 = X 5 7 5 6 5
Esus4 = 0 2 2 2 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
