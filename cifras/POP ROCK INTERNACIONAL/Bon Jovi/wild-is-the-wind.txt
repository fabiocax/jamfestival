Bon Jovi - Wild Is The Wind

Intro: Em  G  D  C  G

  H.N....................................
E|--------------------------------------12-----|
B|----------------------------12--12--12-------|
G|-------------12------12--12---12--12---------|
D|------12--12---12-12---12--------------------|
A|---12---12-----------------------------------|
E|-12------------------------------------------|

E|---12~-12p10p8/7----8-8h10h12-12/17----------------|
B|-------------------------------------15p13p12/10---|
G|---------------------------------------------------|
D|---------------------------------------------------|
A|---------------------------------------------------|
E|-0---------------0---------------------------------|

E|-------------------------------------------------------|
B|-----------------8h10h12~--10p8p7h8p7~--7h8p7h8p7------|
G|-----5--7---9-------------------------------------7-9--|
D|-----7--9---10-----------------------------------------|
A|------------10-----------------------------------------|
E|--5--5--7---8------------------------------------------|

              H.N.............................
E|------------------------------------------12-----|
B|--5--/3---------------------------12---12--------|
G|--5--/2-------------------12---12---12-----------|
D|--------------------12--12---12------------------|
A|-------------12--12---12-------------------------|
E|---------0-----12--------------------------------|

Em
  I tried to make you happy
G                                D
  Lord knows I tried so hard to be
          C        G          Em
What you hoped that I would be
I gave you what you wanted
G                                  D
  God couldn't give you what you need
            F
You wanted more from me
             Am
Than I could ever be
            G
You wanted heart and soul
But you didn't know, baby

C    G              Am
Wild, wild is the wind
                C  Em   Am
That takes me away from you
               C        G         Am
Caught in the night without your love
            F     G
To see me through
C    G              Am
Wild, wild is the wind
                        C   Em
That blows through my heart

F     G        Am
 Wild is the wind,
F     G        Am
 Wild is the wind
F            G      Am
 You got to understand, baby
F                Am
 Wild is the wind

Em
 You need someone to hold you
G                                  D
 Somebody to be there night and day
            C         G         Em
Someone to kiss your fears away
I just went on pretending
G                                     D
 Too weak, too proud, too tough to say
            F
I couldn't be the one
               Am
To make your dreams come true
              G
That's why I had to run
Though I needed you, baby

C    G              Am
Wild, wild is the wind
                C  Em   Am
That takes me away from you
               C        G         Am
Caught in the night without your love
            F     G
To see me through
C    G              Am
Wild, wild is the wind
                      C    Em  Am
That blows through my heart tonight
              C    Em
That tear us apart

F     G        Am
 Wild is the wind,
F     G        Am
 Wild is the wind
F            G      Am
 You got to understand, baby
F
 Wild is the wind

Solo (F G Am) G

         F
Maybe a better man
                Am
Would live and die for you
         G
Baby, a better man would
Never say goodbye to you, baby

C    G              Am
Wild, wild is the wind
                C  Em   Am
That takes me away from you
               C        G         Am
Caught in the night without your love
            F     G
To see me through
C    G              Am
Wild, wild is the wind
                      C    Em  Am
That blows through my heart tonight
              C    Em
That tear us apart

F     G        Am
 Wild is the wind,
F     G        Am
 Wild is the wind
F            G      Am
 You got to understand, baby
F                Am
 Wild is the wind

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
