Bon Jovi - Silent Night

           A                     D
               After the smoke clears and it's down to you and I
                        A                       D
               When the sun appears and there's nothing left but goodbyes
                                   A                         D
               We'll just turn and walk away... how could we let it end like

               this
                             A                      D
               Just turn and walk away... should we seal it with a kiss?
               Bm                           A
                It's too late... now you're out and on the run
               Bm                               E
                Much too late... to  held up in love without a gun
                      A                       D
               Silent night... we hold up our candle light
                      A                            D
               Silent night.... the night that our love died
                           A                G
               No words to say... now we're both to tired to fight
                          Bm                      E
               So hold me close.... and don't let go

                             A                            D
               It was all so simple when... you wre to be queen and I'd be your

               kingt
                             A                                    D
               I guess these dreams got lost... cause baby you're still you and

               I'm still me
                           A                     D
               Now letting go... it's always the hardest part to fight
                            A                      D
               When we both know... we're just two more victms of the night
               Bm                            A
                It's too late... too late to wonder why
               Bm                           E
                Much too late... to  save a love that's died
                      A                       D
               Silent night... we hold up our candle light
                      A                            D
               Silent night.... the night that our love died
                           A                G
               No words to say... now we're both to tired to fight
                          Bm                      E
               So hold me close.... and don't let go
                         A                     D
               After the smoke clears and it's down to you and I
                        A                       D
               When the sun appears and there's nothing left but goodbyes...

               Silent night...

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
