Bon Jovi - Last Chance Train

(Intro)
D  Dsus2  D4  D

(VERSE)
D                         D(#5)
October leaves are falling, so am I
D6/9                       D*
A harvest moon is calling me and I know why
G                               A4  A
The traffic's like a symphony in blue

D                              D(#5)
In the autumn air I smell your sweet perfume
D6/9                            D*
I miss you sitting cross-legged in this empty room
G                       A4  A
Yesterdays are eating me alive

(BRIDGE)
F#m                            G
I'm your angel, baby - I've got broken wings

F#m                                           G                         A
I can't walk, I can't run, I can't drop until you're next to me, so the only thing…

(Chorus)
                  D      F#m
You gotta meet me tonight
         G
I'm on a last chance train
                   D                    F#m
Leave a flower the station, I know my destination
             G
Love bring me home again

(VERSE)
D                            D(#5)
You try to reach me but I've sailed away
D6/9                        D*
You tried to read me but my words got in the way
            G                          A4  A
The music's over but the dance has just begun

(BRIDGE)
  F#m                               G
I wish that I could just get back to Baltimore
             F#m
You know I'd scratch, I'd claw, I'd scream 'til I'm raw,
G                  A
just to fall down at your door


(Chorus)
                  D      F#m
You gotta meet me tonight
         G
I'm on a last chance train
                   D                    F#m
Leave a flower the station, I know my destination
             G
Love bring me home again

(BRIDGE)
Bm
Let the scarecrows laugh coming down the tracks
       Bm7+
At the broken hearts that don't come back
           Bm/A                              Bm/G#
'Cause the train has left the station with my pain
                   G     Gm
Let them forget my name, oh… I'm on a last chance train

(SOLO)
D  D/C  D/B  D/Bb
D  D/C  D/B  D/Bb
D  D/C  D/B  D/Bb  A

(VERSE)
D                                 D(#5)
Sitting here I smoke my nineteenth cigarette
D6/9                           D*
Now all that's left are ashes, butts and my regrets
    G                            A4  A
It's your taste that I just can't forget

(CHORUS)
D  F#m  G
                  D   F#m
You gotta meet me tonight
        G
I'm on a last chance train
        Bm
Let the scarecrows laugh coming down the tracks
       Bm7+
At the broken hearts that don't come back
        Bm/A                            Bm/G#
We'll be laughing at them, crying in the rain
               G      Gm
It's you and I again, oh..

(Chorus)
                  D      F#m
You gotta meet me tonight
         G
I'm on a last chance train
                   D                    F#m
Leave a flower the station, I know my destination
             G
Love bring me home again
              D            F#m
I'm on a last chance train
                    G
It's gonna bring me home to you
                    D                     F#m
Oh, the last chance train, gone trought my faith
                     G                A
This time I'm coming through, coming through...


 Acordes
-=======-

D      xx0232    F#m    244222
D(#5)  xx0332    G      320003
D6/9   xx0432    Gm     355333
D*     xx0532    A      x02220
Dsus2  xx0230    A4     x02230
D4     xx0233    Bm     x24432
D/C    x30232    Bm7+   x24332
D/B    x20232    Bm/A   x04432
D/Bb   x10232    Bm/G#  424432


--------------------------------------
Felipe Fontoura Melachawças

----------------- Acordes -----------------
A = X 0 2 2 2 0
A4 = X 0 2 2 3 0
Bm = X 2 4 4 3 2
Bm/A = X 0 4 4 3 X
Bm/G# = X X 6 4 3 2
Bm7+ = X 2 4 3 3 2
D = X X 0 2 3 2
D(#5) = X 5 4 3 3 X
D/B = X 2 0 2 3 2
D/Bb = X 1 X 2 3 2
D/C = X 3 X 2 3 2
D4 = X X 0 2 3 3
D6/9 = X 5 4 2 0 0
Dsus2 = X X 0 2 3 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
