Bon Jovi - Happy Now

                G
What would you say to me
               Bm
If I told you I had a dream
               Em
If I told you everything
                     C
Would you tell me to go back to sleep

                      G
Take a look in these tired eyes
               Bm
They're coming back to life
               Em
I know I can change, got hope in my veins
               C
I tell you I ain't going back to the pain

           G
Can I be happy now?
           Bm
Can I let my breath out

         Em
Let me believe I'm building a dream
               C
Don't try to drag me down

               G
I just wanna scream out loud
          Bm
Can I be happy now?
                 Em
Went down on my knees I learned how to bleed
               C
I'm turning my world around

          G
Can I be happy now
            Bm
Can I break free somehow
             Em
I just wanna live again, Love again
        C
Take my pride off off of the ground

              G
I'm ready to pick a fight
                 Bm
Crawl out of the dark to shine a light
                  Em
I ain't throwing stones, got sins of my own
                  C
[...] everybody just trying to find their way home

           G
Can I be happy now?
          Bm
Can I let my breath out
          Em
Let me believe,I'm building a dream
              C
Don't try to drag me down
              G
I just wanna scream out loud

          Bm
Can I be happy now?
                 Em
Went down on my knees,I learned how to bleed
                C
I'm turning my world around

       Am                          D
You're born and you die, and it's gone in a minute
   G                           Em
I ain't looking back, 'cause I don't wanna miss it
     Am
You better live now
       D                     Em      Bm
Cause noone's gonna get out alive - alive
Em         C
(short solo)

           G
Can I be happy now?
          Bm
Can I let my breath out
         Em
Let me believe I'm building a dream
              C
Don't try to drag me down
              G
I just wanna scream out loud
          Bm
Can I be happy now?
                 Em
Went down on my knees I learned how to bleed
                C
I'm turning my world around
           G             Bm         Em
Can I be happy now              (oohoohooh)
                 C
I'm turning my world around
           G
Can I be happy now?

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
