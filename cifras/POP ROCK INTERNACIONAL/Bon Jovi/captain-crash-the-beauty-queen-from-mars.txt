Bon Jovi - Captain Crash & The Beauty Queen From Mars

Intro:
e|---------------------------------------------|
B|---------------------------------------------|
G|---------5-----------5-----------5--------0--| 
D|-------------------------------------2h3-----| (3x)
A|--3-3-3-----3-3-3-3-----3-3-3-3--------------|
E|---------------------------------------------|

C5 F5 G5

e|------------------------------------------------------
B|------------------------------------------------------
G|---------9~---------9~----------------------------7~--
D|--10-12------10-12------10-12---9~------9-9-9-10------
A|------------------------------------------------------
E|------------------------------------------------------

 C              Em
Dressed up for a big date

      F                   G
Like Halloween day but it was Fourth of July now
C                                 F
A car crash with a suitcase and a painted face
         G
She was one of a kind

Riff 1:
e|--5-5-5-5--5-5-5-5--5-5-5-5---5~~---|
B|--5-5-5-5--5-5-5-5--5-5-5-5---5~~---|
G|--5-5-5-5--5-5-5-5--5-5-5-5---5~~---|
D|--7-7-7-7--6-6-6-6--5-5-5-5---4~~---|
A|------------------------------------|
E|------------------------------------|

            Dm (Riff 1)                F
She wears a plastic crown like Cinderella
    Dm               G
And roller skates in bed


They greyhound from his hometown
When he comes around 'cause they don't let him drive now
Mixed up as a milkshake
But make no mistake
Crash is looking at the stars

Chorus:
C
You and me
      F          G
We're invincible together
C            F         G
We can be so tragical, whatever

(Riff 1)
Am                   E             C             D
Dressed up just like ziggy but he couldn't play guitar
F                     G                 C
Captain Crash and the beauty queen from mars

Riff 2:
e|--12-------12----12-------12-----13--------13~/-8p7---8p7-------|
B|-----13-------13----13-------13-----13--------------8-----8~~---|
G|--------12-------------12--------------12-----------------------|
D|----------------------------------------------------------------|
A|----------------------------------------------------------------|
E|----------------------------------------------------------------|

Share a toothpick, trading lipstick
Watching traffic for days at the diner
Holding hands, making big plans
Playing Superman, he was wearing eyeliner
Another local legend and his longtime lucky charm

Chorus:
You and me
We're invincible together
We can be so tragical, whatever

(Riff 1)
All Dressed up just like ziggy but he couldn't play guitar
 F5                    G5                C5
Captain Crash and the beauty queen from mars

Riff 3:
e|--5-5-5-5--5-5-5-5--5-5-5-5---5-5-5-5--|
B|--5-5-5-5--5-5-5-5--5-5-5-5---5-5-5-5--|
G|--5-5-5-5--5-5-5-5--5-5-5-5---5-5-5-5--|
D|--7-7-7-7--6-6-6-6--5-5-5-5---4-4-4-4--| (2x)
A|---------------------------------------|
E|---------------------------------------|


        Am (Riff 3)       E
They're drunk on love as you can get
        G                F
Getting high on lust and cigarettes
Am               E
Living life with no regrets

   F                   G (Riff 4)
At least they're gonna try to fly

Riff 4:
e|-------------------------------------------------------|
B|-------------------------------------------------------|
G|-------------------------------------------------------|
D|-------------------------------------------------------|
A|-------------------------------------------------------|
E|-1-1-1-1-1-1-1-1-1-1-1-1--3-3-3-3-3-3-3-3-3-3-3-3-3-3--|

Solo: C Em F G
(Riff 2)

Riff 5:
e|--12-------12----12-------12-----13--------13~/-8p7---8p7------------|
B|-----13-------13----13-------13-----13--------------8-----8~~-6p5----|
G|--------12-------------12--------------12--------------------------6-|
D|---------------------------------------------------------------------|
A|---------------------------------------------------------------------|
E|---------------------------------------------------------------------|

Chorus:
You and me
We're invincible together
We can be oh so tragical, whatever
(Riff 3)
We're Sid and Nancy
Fred and Ginger
Clyde and Bonnie
Liz and Richard
Kurt and Courtney
Bacall and Bogie
Joltin' Joe and Ms. Monroe
         F5                    G5               C5
Here's captain crash and the beauty queen from mars

(Riff 4)
Capitan Crash, and the beauty queen from Mars

(Riff 2)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C5 = X 3 5 5 X X
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F5 = 1 3 3 X X X
G = 3 2 0 0 0 3
G5 = 3 5 5 X X X
