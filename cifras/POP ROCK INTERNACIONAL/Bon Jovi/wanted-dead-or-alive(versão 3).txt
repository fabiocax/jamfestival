Bon Jovi - Wanted Dead Or Alive

(intro) Dadd9    Dm  C  Dm  C  F  G  F   C  F  Dadd9

      D         Dsus4  Dadd9    D
It's all the same
         C                G
Only the names will change
 Cadd9         G         C    F    D
Everyday it seems we're wasting away
   D       Dsus4    Dadd9     D
Another place
          C          G
Where the faces are so cold
      Cadd9       G
I'd drive all night
  C       F        D
Just to get back home

(chorus)
      C      G     F              D    Dadd9       D
I'm a cowboy, on a steel horse I ride
    C       G    C    F    D
I'm wanted...    dead or alive


(solo)
         D       Dsus4    Dadd9     D
Sometimes I sleep,
      C                         G
Sometimes it's not for days
      Cadd9              G       C    F      D
And people I meet always go their separate ways
         D               Dsus4    Dadd9     D
Sometimes you tell the day
        C                         G
By the bottle that you drink
         Cadd9               G    C       F     D
And times when you're all alone all you do is think

(chorus)
(solo)
(chorus)

          D           Dsus4   Dadd9    D
I walk  these streets,
          C                G
A loaded six string on my back
  Cadd9          G          C        F        D
I play for keeps, 'cause I might not make it back
         D        Dsus4    Dadd9   D
I been everywhere,
          C              G
Still I'm standing tall
    Cadd9                 G
I've seen a million faces
       C    F         D
And I' ve rocked them all

(chorus 2x)
      C        G        F          D    Dadd9       D
I'm a cowboy, I got the night on my side
    C      G    C    F     D
I'm wanted...   dead or alive

(solo final)

----------------- Acordes -----------------
C = X 3 2 0 1 0
Cadd9 = X 3 5 5 3 3
D = X X 0 2 3 2
Dadd9 = X X 0 2 3 0
Dm = X X 0 2 3 1
Dsus4 = X X 0 2 3 3
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
