Bon Jovi - Room At The End Of The World

           E
There's a room at the end of the world
         B             Bdim
Where my secrets go to hide
          E
There's a room at the end of the world
           B              Bdim
Where I'll wait for you tonight

          E
Where the roads all go to end
           B              Adim
Where lost Valentines get sent
        E
Where a rose comes back to life
      B                Adim
Where young love never dies
Adim                  E        E
Where we never say goodbye

          E             Bdim
Don't say no, just give in

           Adim            Adim
Close your eyes, there's no sin
           E              Bdim
Don't hold back, just let go
          Adim         Adim
Take your time, take it slow
     E      Bdim      Adim
It's me and you in our room at the end of the world

(C#m E  C#m E  C#m)

          E
There's a room at the end of the world
           B            Bdim
Where your memories are safe
          E
There's a room at the end of the world
     B               Bdim
Just gotta have some faith

          E
Where all prayers go to be heard and
    B                   Adim
The truth will have its turn
       E
Take a look into these eyes
           B                     Adim
There's no place I'd rather be tonight
   Adim                 E    E
No place I'd rather be tonight

----------------- Acordes -----------------
Adim = 5 X 4 5 4 X
B = X 2 4 4 4 2
Bdim = X 2 3 1 3 1
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
