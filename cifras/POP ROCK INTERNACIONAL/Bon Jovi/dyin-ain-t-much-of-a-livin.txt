Bon Jovi - Dyin' Ain't Much Of a Livin'


(Intro)

(Em  D)  4x

(Verse)
  Am             D
A whisky bottle comforts me
     G              C
And tells me not to cry
         Am               D
While a full moon says a prayer for me
   c              E
I try to close my eyes
         Am              D
But the night's there to remind me
       G     G/F#         C
Of the guns and the early graves
     Dm                G
The ghosts appear as I fall asleep
           Am        D    G     Gm
To sing an outlaw's serenade



(CHORUS)
Dm                       G
Dyin' ain't much of a livin'
            C             Am
When you're livin' on the run
Dm                     G                   Am
Dyin' ain't much of a livin' ...  for the young
            Dm                 G
Is it too late to ask for forgiveness
        C                  Am
For the things that I have done
Dm                    G                    C     D
Dyin' ain't much of a livin' ...  for the young


(Em   D)  4x


(Verse)
Am                  D
The desert's been a friend to me
   G           C
It covers me by night
      Am                 D
And a snakebite's not my enemy
      C                E
But it taught me how to fight
         Am                  D
All this fame don't bring you freedom
          G       G/F#      C
Though it wears a thin disguise
        Am               D
When an outlaw is just a man to me
     C    D      G
And a man has to die

(CHORUS)
Dm                       G
Dyin' ain't much of a livin'
            C             Am
When you're livin' on the run
Dm                     G                   Am
Dyin' ain't much of a livin' ...  for the young
            Dm                 G
Is it too late to ask for forgiveness
        C                  Am
For the things that I have done
Dm                    G                    C
Dyin' ain't much of a livin' ...  for the young

(Solo)
Am       C            Am
Na na na na  Na na na na
         C
Na na na na

(Bridge)
G
And I hope someone will pray for me
G5
When it's my turn to die  ...  pray for me


(CHORUS)
Dm                       G
Dyin' ain't much of a livin'
            C     C/B      Am
When you're livin' on the run
Dm                     G                   Am
Dyin' ain't much of a livin' ...  for the young
            Dm                 G
Is it too late to ask for forgiveness
        C          C/B      Am
For the things that I have done
Dm                    G                    C
Dyin' ain't much of a livin' ...  for the young

         C/E          F
Na na na na  Na na na na
         G/E          G
Na na na na  Na na na na

         C/E          F
Na na na na  Na na na na
         G/E          G
Na na na na  Na na na na

-------------------------------

   C/E  C/B  G/E  G/F# 
e---0----0----3----3---
B---1----1----3----3---
G---0----0----0----0---
D---2----0----2----0---
A---3----2----0----2---
E---0----X----3----2---

----------------------
transcripted by Felipe Fontoura
MSN bonj0vi@yahoo.com.br

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C/B = X 2 2 0 1 0
C/E = 0 3 2 0 1 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/E = 0 2 0 0 3 3
G/F# = X X 4 4 3 3
G5 = 3 5 5 X X X
Gm = 3 5 5 3 3 3
