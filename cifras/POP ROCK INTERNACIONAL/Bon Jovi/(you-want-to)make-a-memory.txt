Bon Jovi - (You Want To) Make a Memory

C5
Hello again, it's you and me
C5/B
Kinda always like it used to be
E5
Sippin' wine, killing time
F5
Trying to solve life's mysteries

C5
How's your life, it's been awhile?
C5/B
God it's good to see you smile
E5
I see you reaching for your keys
F5
Looking for a reason not to leave

C5                G5
If you don't know if you should stay
       A5         F5
If you don't say what's on your mind

            C5
Baby just breathe
          E5                           F5
There's nowhere else tonight we should be
                 C5
You wanna make a memory?

C5
I dug up this old photograph
C5/B
Look at all that hair we had
      E5
It's bitter sweet to hear you laugh
      F5
Your phone is ringin I don't wanna ask

C5             G5
If you go now I'll understand
        A5       F5
If you stay hey I've gotta plan

                 C5        C5/B
You wanna make a memory?
                  A5           F5
You wanna steal a piece of time?
                 C5         C5/B
You can sing the melody to me
                  A5         F5
And I can write a couple lines

              C5     C5/B  A5  F5
You wanna make a memory

  Solo: C5  G5  A5  F5
E|--------------------------------------------|
B|--------------------------------------------|
G|-------------------5-7-7b-7b-7b-5-----------|
D|----------------1/5-------------------------|
A|-3-3-5/7-5-3-3h5----------------------------|
E|--------------------------------------------|

       C5           G5
If you don't know if you should stay
       A5         F5
If you don't say what's on your mind
      C5
Baby just breathe
        E5                            F5
There's nowhere else tonight we should be, we should be.


                 C        C/B
You wanna make a memory?
                  Am            F
You wanna steal a piece of time?
                 C           C/B
You can sing the melody to me
                  Am          F
And I can write a couple lines

                 C      C/B  Am  F
You wanna make a memory

              C      C/B  Am  F
You wanna make a memory

              C      C/B  Am  F
You wanna make a memory
E|--------------------------------------------|
B|--------------------------------------------|
G|-5-5-7/9/7/5/7-7/5/4/5-5-5------------------|
D|-x-x-x/x/x/x/x-x/x/x/x-x-x-5-5/7------------|
A|-3-3-5/7/5/3/5-5/3/2/3-3-3-x-x/x------------|
E|---------------------------3-3/5------------|

----------------- Acordes -----------------
A5 = X 0 2 2 X X
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C/B = X 2 2 0 1 0
C5 = X 3 5 5 X X
E5 = 0 2 2 X X X
F = 1 3 3 2 1 1
F5 = 1 3 3 X X X
G5 = 3 5 5 X X X
