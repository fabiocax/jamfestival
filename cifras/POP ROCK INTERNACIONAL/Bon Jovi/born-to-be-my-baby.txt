Bon Jovi - Born To Be My Baby

 F#m               D
Na na na na na na na na na na
 E             A        E
Na na na na na na na na na na
 F#m               D           Esus  E
Na na na na na na na na na na na


[Riff 1]

E|--------------------------------------|
B|--------------------------------------|
G|--------------------------------------|
D|-----------------------------7-7------|
A|----9-9---7-9~---7-9~----7-9-----9~---|
E|--------9------9------9---------------|

[Riff 2]

E|--------------------------------------|
B|--------------------------------------|
G|--------------------------------------|
D|-----------------------------5-7------|
A|----7-7---5-7~---5-7~----5-7-----7~---|
E|--------7------7------7---------------|


 F#m
Rainy night and we worked all day
    D
We both got jobs cause there's bills to play
 E
We got something they can't take away
Our love, our lives

  F#m
Close the door, leave the cold outside
   D/F#
I don't need nothing when i'm by your side

 E
We got something that'll never die

D/A   A     D     F#m7
Our dreams, our pride

          F                 G    Am
My heart beats like a drum (all night)
           F                          G    Am
Flesh to flesh, one to one (baby it's all right)
          F                          Am
And i'll never let go cause there's something i know deep
   E
Inside

          Am            F
You were born to be my baby
             G               C
Baby, i was made to be your man
 G/B    Am              F
We got something to believe in
            G                    C
Even if we don't know where we stand
G/B   F                 C
Only god would know the reasons
       G/B    Am               Gsus
But i bet he must have had a plan
    G      Am           F
Cause you were born to be my baby
             G7sus       G7      Am
And baby, i was made to be your man

 F#m
Light a candle, blow the world away
 D
Table for two on a tv tray
E
It ain't fancy, baby that's ok

D/A  A    D    F#m7
Our time, our way

 F#m
So hold me close better hand on tight
 D/F#
Buckle up, baby, it's a bumpy ride

 E
We're two kids hitching down the road of life

D/A  A     D     F#m7
Our world, our flight

        F                 G    Am
If we stand side by side (all night)
            F                            G    Am
There's a chance we'll get by (baby it's all right)
           F                             Am                   E
And i'll know that you'll be live in my heart till the day i die

              F                 G    Am
And my heart beats like a drum (all night)
           F                          G    Am
Flesh to flesh, one to one (baby it's all right)
          F                          Am                      E
And i'll never let go cause there's something i know deep inside

 Am                D
Na na na na na na na na na na
 G              C  G/B   Am
Na na na na na na na na na na

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/A = X 0 X 2 3 2
D/F# = 2 X 0 2 3 2
E = 0 2 2 1 0 0
Esus = 0 2 2 1 0 0
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G7 = 3 5 3 4 3 3
G7sus = 3 5 3 4 3 3
Gsus = 3 2 0 0 0 3
