Bon Jovi - Out Of Bounds

(Power Chords)

(INTRO) 4x
B-A-B  G  A

(VERSE)
B
A loaded gun needs no alibis
G                           D        A
Out on the street where the truth's a lie
B
Wasted blood that I left behind
G                     D          A
Broken hearts leave a young man blind
G                        A
I need protection on the underground
G                                 A
I'm going down, down, down, down, down

(Chorus)

B-A-B
Out of bounds
        G          A
You can cross that line
           D
Out of bounds
   G      A
Is out of time
B-A-B
Out of bounds
G            A
Is where I'll be
D                     G           A
I can't hold on but I can't break free
          G                       A
I'm going down, down, down, down, down
        B-A-B  G     A
Out of bounds, Oohhh Ohhh
        B-A-B  G  A
Out of bounds

(VERSE)
B
You're asking why but who's to say
 G                             D    A
The things you've done haven't gone away
B
Nowhere to run, no one to blame
G                      D         A
I'm just a puppet in a bad man's game
G                        A
I need protection on the underground
          G                      A
I'm going down, down, down, down, down

(Chorus)
B-A-B
Out of bounds
        G          A
You can cross that line
           D
Out of bounds
   G      A
Is out of time
B-A-B
Out of bounds
G            A
Is where I'll be
D                     G           A
I can't hold on but I can't break free
          G                       A
I'm going down, down, down, down, down
        B-A-B  G     A
Out of bounds, Oohhh Ohhh
        B-A-B  G  A
Out of bounds

(SOLO) 4x
B  G  A

(BRIDGE)
G                       A
So say a prayer for the innocent
G                       A
It's the innocent who believe
G                           A
You can't forgive what you can't forget
G                   A
When that innocent man was me

(Chorus)
B-A-B
Out of bounds
        G          A
You can cross that line
           D
Out of bounds
   G      A
Is out of time
B-A-B
Out of bounds
G            A
Is where I'll be
D                     G           A
I can't hold on but I can't break free
          G                       A
I'm going down, down, down, down, down
        B-A-B  G     A
Out of bounds, Oohhh Ohhh
        B-A-B  G  A
Out of bounds

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
D = X X 0 2 3 2
G = 3 2 0 0 0 3
