Bon Jovi - It's My Life

Intro:

E|------2---3--------------------|
B|----3--------------------------|
G|-4-----------------------------|
D|-------------------------------|
A|-------------------------------|
E|-------------------------------|

Tom: D
Intro:


Bm
 This aint a song for the broken hearted,
Bm
 A silent prayer for faith departed,
Bm
 I aint gunna be just a face in the crowd,
             Em
 Your gunna hear my voice when i shout it all loud.

        Bm
 it's my life,
        G          D
 It's now or never,
             A         Bm
 I aint gunna live forever,
                G             A       A   A#
 I just wanna live while i'm alive,
         Bm                         G          D
 Its my life, My heart is like the open highway,
                       A         Bm
 Like frankie said i did it my way,
               G               A     A   A#
 I just wanna live while i'm alive,
 F#m    A     Bm
 Its   My   Life.

Intro

Bm
 This is for ones who stood their ground
Bm
For Tommy and Gina, never backed down
Bm
Tomorrow's getting harder make no mistake
   Em
Luck ain't even lucky got to make your own breaks

        Bm
 it's my life,
        G          D
 It's now or never,
             A         Bm
 I aint gunna live forever,
                G             D         A  A#
 I just wanna live while i'm alive,

         Bm                         G          D
 Its my life, My heart is like the open highway,
                       A         Bm
 Like frankie said i did it my way,
               G               A
 I just wanna live while i'm alive,
 A     A#     Bm
 Its   My   Life.

Solo  (G   A) 2x

         A                                     D
You better stand tall when they're calling you out
      Bm                     D          A
Don't bend don't break baby don't back down


        Bm
 it's my life,
        G          D
 It's now or never,
             A         Bm
 I aint gunna live forever,
                G             A       A  A#
 I just wanna live while i'm alive,
         Bm                         G          D
 Its my life, My heart is like the open highway,
                       A         Bm
 Like frankie said i did it my way,
               G               A
 I just wanna live while i'm alive,
 A     A#     Bm
 Its   My   Life. (2x)

----------------- Acordes -----------------
A = X 0 2 2 2 0
A# = X 1 3 3 3 1
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
