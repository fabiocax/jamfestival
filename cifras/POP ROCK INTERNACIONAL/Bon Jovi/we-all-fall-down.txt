Bon Jovi - We All Fall Down

A
Don't be afraid to lose your way
C#m
Take that step don't hesitate
                D
Your time is now, oho

A
Don't be afraid to scrape your knees
C#m
On these streets of shattered dreams
                      D
Stand up and be proud, oho
                A
I'm telling you now

Refrão:
                A
Climb every mountain, How high doesn't matter
           F#m           E
Don't ever be afraid to fall

                   A
Walk out on that wire, Climb every ladder
           F#m              E
Keep your back against the wall
                F#m              A
And don't be scared to hit the ground
           D   E    A
'Cause we all fall down


----------------- Acordes -----------------
A = X 0 2 2 2 0
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
