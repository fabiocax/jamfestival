Bon Jovi - We Rule The Night

(INTRO)
Dm  Bb  C  Dm  Bb  C
Dm  Bb  C  Dm  Bb  C

(VERSE)
Dm                                        Bb
We walked through the sunshine, we walked thru the rain
     Gm                         C         Dm  Bb  C
As endless as time the thoughts will remain
Dm
We'll take what we want
                  Bb
You take what you need
  Gm            C             A
We are the targets, you better believe

((BRIDGE))
Bb   C
Ouuuooooo
Dm
No one can save you, there's nothing to say

Bb    C
Ouuuooooo
A
Deception's the name of the game

((Chorus))
Dm
We rule the night
         C
Only the strong will survive
                            Bb   C
There's no wrong, there's no right
           Dm    Bb C
We rule the night
            Dm
We rule the night
            C
We'll never give up the fight
                   Bb     C
Where the wrong is alright
           Dm    Bb  C
We rule the night

(VERSE)
Dm                        Bb
All is for one and one is for all
   Gm               C                   Dm  Bb C
If we take the fight then you take the fall
Dm                                    Bb
You run from the shadows but they call my name
    Gm                C                        A
You search for the truth but your search is in vain

((BRIDGE))
Bb   C
Ouuuooooo
Dm
No one can save you, there's nothing to say
Bb    C
Ouuuooooo
A
Deception's the name of the game

((Chorus))
Dm
We rule the night
         C
Only the strong will survive
                            Bb   C
There's no wrong, there's no right
           Dm    Bb C
We rule the night
            Dm
We rule the night
            C
We'll never give up the fight
                   Bb     C
Where the wrong is alright
           Dm    Bb  C
We rule the night
            Dm   Bb  C
We rule the night

(SOLO)
Dm  Bb  C  Dm  Bb  C
Dm  Bb  C  Dm  Bb  C

((BRIDGE))
Bb   C
Ouuuooooo
Dm
When you look for someone on the blame
Bb    C
Ouuuooooo
A
I know you remember my name

((Chorus))
Dm
We rule the night
         C
Only the strong will survive
                            Bb   C
There's no wrong, there's no right
           Dm    Bb C
We rule the night
            Dm
We rule the night
            C
We'll never give up the fight
                   Bb     C
Where the wrong is alright
           Dm    Bb  C
We rule the night
            Dm   Bb  C
We rule the night
            Dm   Bb  C
We rule the night
              Dm  Bb  C
We rule the night.....


--------------------------
Transcripted by Felipe Fontoura Melachawças

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Gm = 3 5 5 3 3 3
