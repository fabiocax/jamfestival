Bon Jovi - When We Were Beautiful

A
The world is cracked
A  G
The sky is torn
A  F#
I'm hanging in
A
You're holding on

A
I can't pretend
A  G
That nothings changed
A  F#
Living in the shadows
A
Of the love we made

A                       G
Back…When we were beautiful
                D
Before the world got small

                A
Before we knew it all
A                       G
Back… When we were innocent
                D
I wonder where it went
                A
Let's go back and find it

A                     D
Sha la la Sha la la hey Sha la la
G                     D
Sha la la Sha la la hey Sha la la

Some dreams live
Some will die
But the you and me
Is still alive

Now am I blessed?
Or am I cursed?
Cause the way we are
Aint the way we were

Back…When we were beautiful
Before the world got small
Before we knew it all
Back… When we were innocent
I wonder where it went
Let's go back and find it

(solo) A G D A

The world is cracked
The sky is torn
So much less
Meant so much more

Back…When we were beautiful
Before the world got small
Before we knew it all
Back… When we were innocent
I wonder where it went
Let's go back and find it

Back…When we were beautiful
Back…When we were beautiful
Back…When we were beautiful
Back…When we were beautiful

Sha la la Sha la la hey Sha la la
Sha la la Sha la la hey Sha la la

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
