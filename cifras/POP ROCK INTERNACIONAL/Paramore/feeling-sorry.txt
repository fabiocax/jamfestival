Paramore - Feeling Sorry

(intro) E G#m C#m B E F#m C#m B

E
  we still live in
                   G#m
the same town well dont we
C#m                             B
   but i dont see you around anymore
E
  i go to all the same places
    F#m    C#m
not even a trace of you
                          B
your days are numbered at 24

A                B
  and im getting bored of
                   C#m
waiting around for you
                 G#m            A
were not getting any of you and i

           B                  C#m
won't look back coz theres no use
                 B/Eb
its time to move forward

E   A   E   B
  i feel no sympathy
E     A   C#m  B
  you live   inside a cave
E            A
  you barely get by
          E
while the rest of
       B
us are trying
        E           A
there's no need to apologize
     C#m
i've got no time for
B
feeling sorry

( E G#m C#m B E F#m C#m B )

E
  well i try not to think
              G#m
of what might happen
C#m
   when your reality
                B
it finally cuts through
E
  well as for me i got
            F#m    C#m
out and i'm on the road
the worst part is that this
B
this could be you

A                 B
  and you know it too
                        C#m
you can't run from your shame
                   G#m
you're not getting any of and
A                  B
time keeps passing by
                C#m
but you waited away
                 B/Eb
its time to roll onward

(repete refrão)

    A            B
and all the best lies
         C#m  B/Eb
that are told with
E  F#m  A
fingers tied
              B
so cross them tight
         C#m B/Eb E  F#m A
wont you pro-mise me to--night
if it's the last thing
                 B
you do you'll get out

E   A   E   B
  i feel no sympathy
E     A   C#m  B
  you live   inside a cave
E            A
  you barely get by
          E
while the rest of
       B
us are trying
        E           A
there's no need to apologize
     C#m
i've got no time
B
got no time
got no time

E   A   E   B
  i feel no sympathy
E     A   C#m  B
  you live   inside a cave
E            A
  you barely get by
          E
while the rest of
       B
us are trying
        E           A
there's no need to apologize
     C#m    B
i've got no time

E  G#m C#m
          i've got no time for
B
feeling sorry---y
E  F#m C#m
          i've got no time
    A
for feeling sorry

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
B/Eb = X 6 X 4 7 7
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
