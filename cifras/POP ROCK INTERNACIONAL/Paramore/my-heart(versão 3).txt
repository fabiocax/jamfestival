Paramore - My Heart-The Final Riot!

Intro:
E|-7-----------7------------------------------------|
B|-----9---9-------7---7----7h9--9---9---9---9------|
G|---9---9---9----9--9---9---------9---9---9---9----|
D|--------------------------------------------------|
A|--------------------------------------------------|
E|--------------------------------------------------|

E|----------------7---------------7----7--7---7-7---|
B|-9/12---9---9--------7---7------7-----------7-7---|
G|------9---9---9----9---9------9----9---9--9--9----|
D|-----------------------------------x--------------|
A|--------------------------------------------------|
E|--------------------------------------------------|

Intro 2x: E A9 C#m Abm A

Verso:
C#m    B          A9            C#m   B    A9
  I am finding out that maybe I was wrong
C#m           B          A9                C#m   B   A9
  that I have fallen down and I can't do this alone

C#m     B  A9              C#m Abm    A9
  stay with me this is what I need, please.

Refrão:
E           A9             C#m    Abm     A
 Sing us a song and we'll sing it back to you
E                 A9                      C#m         Abm A
 We could sing our own but what would it be without you?

Verso 2:
C#m    B     A9         C#m      B   A9
 I am nothing now and it's been so long
C#m         B         A9                      C#m B A9
 Since I've heard the sound, the sound of my only hope
C#m   B A9            C#m Abm A9
 This time I will be listening.

Refrão:
E           A9              C#m   Abm    A
 Sing us a song and we'll sing it back to you
E                A9                      C#m         Abm A
 We could sing our own but what would it be without you?

C#m                  E                 A9
 This heart, it beats, beats for only you
C#m                  E                 A9
 This heart, it beats, beats for only you
C#m                  E                 A9
 This heart, it beats, beats for only you
 My heart is yours
C#m                  E                 A9
 This heart, it beats, beats for only you
 My heart is yours
(My heart, it beats for you)
C#m                  E                  A9
 This heart, it beats, beats for only you (It beats, beats for only you)

 My heart is yours (My heart is yours)
C#m                  E                  A9
 This heart, it beats, beats for only you (Please don't go now, please don't fade away)

 My heart, my heart is yours (Please don't go now, please don't fade away)

E no final : C#m E A9

(Please don't go now, please don't fade away) My heart is yours

(Please don't go now, please don't fade away) My heart is yours

(Please don't go, please don't fade away)

(Please don't go now, please don't fade away) My heart is...

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
Abm = 4 6 6 4 4 4
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
