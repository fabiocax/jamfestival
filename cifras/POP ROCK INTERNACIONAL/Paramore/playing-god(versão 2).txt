Paramore - Playing God

Capo Casa 4

Intro 2x: Am  G  C

Verso:
Am                                                  G
Can't make my own decisions or make any with precision
C
Well, maybe you should tie me up, so I don't go where you don't want me
Am                                                        G
You say that I've been changing that I'm not just simply aging
C
Yeah, how could that be logical? Just keep on cramming ideas down my throat
F   G
Wooh Oh Oh Ohhhh

Refrão:
C                                     Am
You don't have to believe me, but the way I, way I see it
F                            G
Next time you point a finger, I might have to bend it back
Em                        F
Or break it, break it off

Whoa
F                            G
Next time you point a finger, I'll point you to the mirror

( Am  G  C  )

Verso:
Am
If God's the game that your playing, then we must get more acquainted
C
Because it has to be so lonely, to be the only one who's holy
Am
It's just my humble opinion, but it's one that I believe in
C
You don't deserve a point of view, if the only thing you see is you
F   G
Woah

Refrão:
C                                     Am
You don't have to believe me, but the way I, way I see it
F                            G
Next time you point a finger, I might have to bend it back
Em                       F
Or break it, break it off
F                            G
Next time you point a finger, I'll point you to the mirror

Ponte 2x:  F  G  Am  Em

F                          G
This is the last second chance (I'll point you to the mirror)
Am                    Em
I'm half as good as it gets (I'll point you to the mirror)
F                         G
I'm on both sides of the fence (I'll point you to the mirror)
Am                      G
Without a hint of regret, I'll hold you to it

Refrão:
C                                     Am
I know you don't believe me, but the way I, way I see it
F                            G
Next time you point a finger, I might have to bend it back
E                        F
Or break it, break it off
F                            G
Next time you point a finger, I'll point you to the mirror

C                                     Am
I know you won't believe me, but the way I, way I see it
F                            G
Next time you point a finger, I might have to bend it back
E                        F
Or break it, break it off
F                            G
Next time you point a finger, I'll point you to the mirror

----------------- Acordes -----------------
Capotraste na 4ª casa
Am*  = X 0 2 2 1 0 - (*C#m na forma de Am)
C*  = X 3 2 0 1 0 - (*E na forma de C)
E*  = 0 2 2 1 0 0 - (*G# na forma de E)
Em*  = 0 2 2 0 0 0 - (*G#m na forma de Em)
F*  = 1 3 3 2 1 1 - (*A na forma de F)
G*  = 3 2 0 0 0 3 - (*B na forma de G)
