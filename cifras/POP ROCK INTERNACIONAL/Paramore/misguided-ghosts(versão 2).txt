Paramore - Misguided Ghosts

intro:
E|----0-------3---|----------------------------------|
B|0h1---1-1p0---0-|-----0---0h1-0-----0----0h1--3--1-|
G|----------------|2x-0---0-------0------------------|
D|----------------------------------2----2-----------|
A|---------------------------------------------------|
E|---------------------------------------------------| 2x


E|----0-------3---|----------------------------------|
B|0h1---1-1p0---0-|-----0---0h1-0-----0----0h1--3----|
G|----------------|2x-0---0-------0----------------0-|
D|----------------------------------2----2-----------|
A|---------------------------------------------------|
E|---------------------------------------------------| 2x


E|-12----12-12----12-10----10-10----10-8----8-8----8-10----10-10----10|
B|----13-------13-------12-------12------10-----10------12-------12---|
G|--------------------------------------------------------------------|
D|--------------------------------------------------------------------|
A|--------------------------------------------------------------------|
E|--------------------------------------------------------------------| 2x



E|-------------------------|   |
B|-5-3-0-1---1-1---1-1---1-|   |
G|---------0-----0-----0---|   |
D|-------------------------|   |
A|-7-5-2-3-----------------|   |
E|-------------------------|   |

                               |
E|-------------------------|   |
B|-5-3-0-------------------|   |
G|-------2---2-2---2-2---2-|   |
D|---------3-----3-----3---|   |
A|-7-5-2-------------------|   |
E|-------1-----------------|   |2x


E|----0-------3---|----------------------------------|
B|0h1---1-1p0---0-|-----0---0h1-0-----0----0h1--3----|
G|----------------|2x-0---0-------0----------------0-|
D|----------------------------------2----2-----------|
A|---------------------------------------------------|
E|---------------------------------------------------| 3x


E|-12----12-12----12-10----10-10----10-8----8-8----8-10----10-10----10|
B|----13-------13-------12-------12------10-----10------12-------12---|
G|--------------------------------------------------------------------|
D|--------------------------------------------------------------------|
A|--------------------------------------------------------------------|
E|--------------------------------------------------------------------| 2x


E|-------------------------|
B|-5-3-0-1---1-1---1-1---1-|   |  sequencia, se quiser vc pode fazer isso:
G|---------0-----0-----0---|   |  E|-------------------|
D|-------------------------|   |  B|-5-3-0-1---1-1~3~1-|
A|-7-5-2-3-----------------|   |  G|---------0---------|
E|-------------------------|   |  D|-------------------|

                               |  A|-7-5-2-3-----------|
E|-------------------------|   |  E|-------------------|
B|-5-3-0-------------------|   |
G|-------2---2-2---2-2---2-|   |
D|---------3-----3-----3---|   |
A|-7-5-2-------------------|   |
E|-------1-----------------|   | 5x


E|----0-------3---|----------------------------------|
B|0h1---1-1p0---0-|-----0---0h1-0-----0----0h1--3----|
G|----------------|2x-0---0-------0----------------0-|
D|----------------------------------2----2-----------|
A|---------------------------------------------------|
E|---------------------------------------------------| 2x


E|-----|
B|-----|
G|-----|
D|-----|
A|-5-~~|
E|-3-~~|

----------------- Acordes -----------------
A = X 0 2 2 2 0
