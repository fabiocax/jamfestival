Paramore - Stuck On You

Intro:

Guitarra 1 :F# Ebm G#m B

Guitarra 2:
e|-4-----4-----7---|-12------11-----------|
B|---7-----7-------|----9-------9----9----|
G|-----6-----6---6-|------11------11---11-|
D|-----------------|----------------------| (x3)
A|-----------------|----------------------|
D|-----------------|----------------------|
          x3

Guitarra 1: entra nos 0:13seg
e|-----------------------------------------|
B|-----------------------------------------|
G|-----------------------------------------|
D|-4-----7-----1-----6-----9---------------| (x2)
A|-4(x8)-7(x8)-1(x8)-6(x8)-9(x8)--|
D|-4-----7-----1-----6-----9---------------|


Verso 1: Mesmos acordes da intro

F#                    A
  I heard you driving in my car
          Ebm
Then in a frozen bar
      G#m                       F#
And I claimed I didn't care for you
                             A
But your verse got trapped inside my head
         Ebm
Over and over again
    G#m                B
You played yourself to death in me

(Repete a segunda parte e a Intro)

Verso 2:

F#                       A
  I thought I'd drop you easily
             Ebm
But that was not to be
    G#m             B      F#
You burrowed like a summer tic
         A                    Ebm
So you invade my sleep and confuse my dreams
     G#m           B
Turn my nights to sleepless itch

Refrão:
F#         A                    Ebm
  Stuck on you 'till the end of time
    G#m          B          F#
I'm too tired to fight your rhyme
         A                    Ebm
Stuck on you 'till the end of time
G#m           B
you've got me paralyzed

(Repete a segunda parte do refrão)

Verso 3:
F#               A
  Holding on the telephone
            Ebm
I hear your midrange moan
       G#m          B       F#
You're everywhere inside my room
          A                    Ebm
Even when I'm alone I hear your mellow drone
       G#m          B
You're everywhere inside of me

Refrão:

F#         A                    Ebm
  Stuck on you 'till the end of time
    G#m          B          F#
I'm too tired to fight your rhyme
         A                    Ebm
Stuck on you 'till the end of time
G#m        B
You got me trapped inside

Ponte:
  F#          A       Ebm          G#m
I can't escape your incessant whine
B        F#             A  Ebm         G#m
When you beam it out all across the sky
B    F#          A
No I can't escape
(stuck on you 'till the end of time)
       Ebm        G#m
your insipid rhyme
(I'm too tired to fight your rhyme)
B        F#           A
When you shoot it deep
(stuck on you 'till the end of time)
           Ebm       G#m - B
Straight into my mind

Acaba em  F#

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
Ebm = X X 1 3 4 2
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
