Paramore - My Heart

Tuning: Standard E,A,D,G,B,e

Intro: E -- A9 ---- C#m -- G#m -- A9 (x2)

Verse 1:
C#m    B          A9          C#m   B    A9
  I am finding out that maybe I was wrong
C#m           B          A9              C#m   B   A9
  that I have fallen down and I can't do this alone
C#m    G#m  A9         C#m          G#m    A9
  stay with me this is what I need, please.


Chorus:
E           A9            C#m      G#m        A9
  sing us a song and we'll sing it back to you
E                A9                     C#m        G#m A9
  we could sing alone but what would it be without you?


Verse 2:

C#m    B          A9        C#m     B   A9
  I am nothing now and it's been so long
C#m          B            A9               C#m  B   A9
  since I've heard a sound the sound of my only home
C#m    G#m A9         C#m  G#m A
  this time I will be listening

(Repeat Chorus)

Interlude: C#m -- E -- A9 ---- B

Bridge:
C#m                  E                  A9 -- B
  this heart it beats beats for only you
C#m                  E                  A9 -- B
  this heart it beats beats for only you

C#m                  E                  A9                B
  this heart it beats beats for only you my heart is yours
C#m                  E                  A9
  this heart it beats beats for only you.
                   B
  my heart is yours. (my heart beats for you...)

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
G#m = 4 6 6 4 4 4
