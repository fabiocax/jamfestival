John Lennon - Remember

Intro: 6/8 

  C F C [A:--A:--] [A:--C F C] [A:--A:--] [A:--A:--]
        [D:--D:--] [D/C:--D/C:--]

4/8 [Em---] [Em---] [Em---] [Em   -    -    -   ]
   Remember.......................when you  were
    [Em7+---] [Em7+---] [Em7+---] [Em7+   -   -   - ]
     young....................................How the
    [Em7 ---] [Em7 ---] [Em7 - - -] [G7 -  -  -  ] [C:---]
     hero..............................was never hung
3/8 [C F C] 4/8 [A - - - ] [A - - -] [A - - - ]
                     Always............

5/8 [A - - - -]  4/8 [D - - -] [D - - -]
             got    a-way..........
    [D/C:---] [D/C:---]


  Em                       Em7+
Remember.......... how the man.............used to
Em7                G7        C     C F C
leave you............. empty handed
A          C F C  A           D              D/C
Always,........always...... let you down

       Em               Em7+
If you ever change your mind
      Em7        G7        C     C F C
About leaving ....it all behind
  A       C F C    A        A7    D          D/C (pause)
Remember,      remember    to - day    hey, hey

CHORUS (in a regular 4/4 feel)
    G     F    C      G7
and don¹t feel sor -  ry
C            G7
the way it¹s gone  and don¹t you
C       G7     Am              [G       G/F#]
wor  -  ry    'bout what you¹ve done

  Em                   Em7+
Remember when you were small
    Em7   G7         C      C F C
How people seemed so tall
A          A7            D        D/C
Always         had their way  hey hey
         Em                 Em7+
Do you remember your ma and pa
     Em7     G7            C         C F C
Just wishing     for movie stardom
A      C F C   A                      D    D/C
Always,       always playing a part

       Em           Em7+
If you ever feel so sad
         Em7        G7                C     C F C
And the whole world    is driving you mad
  A      C F C  A        D          D/C (pause)
Remember,   remember today    hey hey

CHORUS
         Em         Em7+          Em7   G7     C / /
No, no remember   Remember    the fifth   of November

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/C = X 3 X 2 3 2
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
Em7+ = X X 2 4 4 3
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/F# = X X 4 4 3 3
G7 = 3 5 3 4 3 3
