John Lennon - Look At Me

A  A/G# F#m
 Look  at me
B
Who am I supposed to be, who am I supposed to be?
A  A/G# F#m
 Look  at me
B                                               E7
What am I supposed to be, what am I supposed to be?
D D/C#   Bm   C#m    F#m   C#m    F#m
Look at me, oh my love, oh my love

A  A/G# F#m
Here I am
B
What am I supposed to do, what am I supposed to do?
A  A/G# F#m
Here I am
B                                               E7
What can I do for you, what can I do for you?
D D/C#   Bm
Here I am

 C#m    F#m   C#m    F#m
Oh my love, oh my love
 F#m      F#m/F B  F#m      F#m/F      B     B7
Look at me,       oh please look at me, my love
D D/C#   Bm   C#m    F#m
Here I am, oh my love

A  A/G# F#m
Who am I
B
Nobody knows but me, nobody knows but me
A  A/G# F#m
Who am I
B                                 E7
Nobody else can see, just you and me
D D/C#   Bm
Who are we?
C#m    F#m   C#m    F#m  C#m    F#m
Oh my love, oh my love, oh...

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/G# = 4 X 2 2 2 X
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
D/C# = X 4 0 2 3 2
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
F#m/F = X X 3 2 2 2
