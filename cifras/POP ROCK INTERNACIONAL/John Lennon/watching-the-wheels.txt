John Lennon - Watching The Wheels

Intro: C  F

C
  People say I`m crazy
F               C
 doing what I`m doing

Well, they give me all kinds of warnings
F                  C
  To save me from ruin
 F                 Dm
When I say that I`m o.k., well, they
G
look me kind of strange
F                   Dm
\"Surely, you`re not happy now
       G
You no longer play the game\"
C
  People say I`m lazy
F            C
 dreaming my life away


Well, they give all kinds of advies
F              C
 Designed to enlighten me
       F                  Dm
When I tell them that I`m doing fine
         G
Watching shadows on the wall
F                    Dm
 \"Don`t you miss the big time, boy
           G
You`re no longer on the ball?\"
F         G
 I'm just sitting here watching
    C                   Am     F
the wheels go round and round
         Am                  Dm7 Dm7/C
I really love to watch them roll
G          G/F            Am             Am/G
 No longer riding on the merry-go-round
F       Ab6            C   F C F
 I just had to let it go
C
 People asking questions
F           C
 lost in confusion

 Well, I tell them there's no problems
F        C
 Only solutions
           F
Well, they shake their heads
    Dm               G
And look at me as if I've lost my mind
  F                   Dm
I tell them there's no hury
         G
I'm just sitting here doing time
F          G
  I'm just sitting here watching
    C                   Am
The wheels go round and round
F          Am
  I really love to watch
      Dm7 Dm7/C
them roll
G           G/F            Am            Am/G
  No longer riding on the merry-go-round
F        Ab6            C
  I just had to let it go
F        Ab6            C
  I just had to let it go...

----------------- Acordes -----------------
Ab6 = 4 X 3 5 4 X
Am = X 0 2 2 1 0
Am/G = 3 X 2 2 1 X
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Dm7/C = X 3 0 2 1 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/F = 1 X X 0 0 3
