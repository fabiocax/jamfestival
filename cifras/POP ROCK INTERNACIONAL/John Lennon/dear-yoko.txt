John Lennon - Dear Yoko

Capo Casa 7

Intro:  D, A*, G, *A, G, A*

  D                     A*
 Even after all these years,
    G                    A*
 I miss you when you're here.
    G               A*  D-Dsus2**-D
 I wish you were here, my dear Yoko.

 (play the same chords as in first verse)

 Even if it's just a day,
 I miss you when you're away.
 I wish you were here today , dear Yoko.

 Chorus:
 C  G   D-Dsus2**-D           C                       G                 D-Dsus2**-D
 Oh,   Yoko,          I'm never,never,never,never,ever gonna let you go.

 Even if it's just one night,
 I miss you and it don't feel right.

 I wish you were here tonight, dear Yoko.

 Even if it's just one hour,
 I wilt just like a faded flower.
 Ain't nothing in the world like our love, dear Yoko.

 {Chorus}

 Even when I'm miles at sea
 and nowhere is the place to be,
 your spirit's watching over me, dear Yoko.

 Even after all this time,
 I miss you like the sun don't shine.
 Without you, I'm a one track mind , dear Yoko.

 Even when I watch T.V.,
 there's a hole where you're supposed to be.
 There's nobody lying next to me , dear Yoko.
RELATED

----------------- Acordes -----------------
Capotraste na 7ª casa
A*  = X 0 2 2 2 0 - (*E na forma de A)
C*  = X 3 2 0 1 0 - (*G na forma de C)
D*  = X X 0 2 3 2 - (*A na forma de D)
G*  = 3 2 0 0 0 3 - (*D na forma de G)
