John Lennon - Scared


Intro : (2 howlings no chords), Em Am Em7 x2]
I'm [Em]scared, I'm [Am]scared, I'm [Em7]scared.
I'm [Em]scared[Am], so [Em7]scared.
I'm [Em]scared, I'm [Am]scared, I'm [E]scared
As the [Am7]years roll away and the [Bm7]price that I paid
as the [C7]straws slip away[B7].
[B7 Bb7 A7] You don't have to suf[F7]fer, it is what it is[D7].
No bell, book or [F7]candle
can get you out of [B7]this, [B7/#5]oh, [B7]no.
I'm [Em]scarred, I'm [Am]scarred, I'm [Em7]scarred.
I'm [Em]scarred[Am Em7]
I'm [Em]scarred, I'm [Am]scarred, I'm [Em]scarred
Ev'ry [Am7]day of my life, I just [Bm7]manage
I just [C7]wanna stay alive[B7].
[B7 Bb7 A7] You don't have to [F7]worry in heaven or [D7]hell.
Just dance to the [F7]music,
You do it so [B7]well, [B7/#5]well, [B7]well.
[E7] Hatred and jealousy [D9] gonna be the death of me,
[C9] I guess I knew it right from the [E7]start.
Sing out about love and peace, [D9] don't wanna see the red raw meat,

[C9] the green eyed goddamn straight [E7]from your heart.
[Instrumental : Em Am Em7 x2]
I'm [Em]tired, I'm [Am]tired, I'm [Em]tired of [Am7]being so alone.
No [Bm7]place to call my own, like a [C7]rolling stone[B7].
(steady men...)
Ah! [Em Am Em] [Am Am7] [Em7 Am Em7] [Am Em7] Repeat and fade.

----------------- Acordes -----------------
