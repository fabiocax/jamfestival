John Lennon - Child Of Nature

G        Em           D   D6
  On the road to Rishikesh
D7                       Em   Em5
  I was dreaming more or less,
Em6                        D    Dsus4 D
  And the dream I had was true
Em          Em7            C
  Yes, the dream I had was true

G            Dm         C   Dsus4
  I'm just a child of nature
G               Dm      Bb
  I don't need much to set me free
G           D          Em  Em7
  I'm just child of nature
C                      G
  I'm one of nature's children

G          Em              D   D6
  Sunlight shining in your eyes
D7                     Em   Em5
  As I face the desert skies

Em6                         D    Dsus4 D
  And my thoughts return to home
Em         Em7               C
  Yes, my thoughts return to home

G            Dm         C   Dsus4
  I'm just a child of nature
G              Dm      Bb
  I don't need much to set me  free
G           D          Em  Em7
  I'm just child of nature
C                      G
  I'm one of nature's children

G      Em                 D     D6
  Underneath the mountain ranges
D7                          Em      Em5
  Where the wind that never changes
Em6                       D    Dsus4 D
  Touch the windows of my soul
Em           Em7           C
  Touch the windows of my soul

G            Dm         C   Dsus4
  I'm just a child of nature
G               Dm      Bb
  I don't need much to set me free
G           D          Em  Em7
  I'm just child of nature
C                      G
  I'm one of nature's children

D6      xx0222
Em5     022010
Em6     022020

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D = X X 0 2 3 2
D6 = X 5 4 2 0 X
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Dsus4 = X X 0 2 3 3
Em = 0 2 2 0 0 0
Em5 = 0 2 2 0 0 0
Em6 = X 7 X 6 8 7
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
