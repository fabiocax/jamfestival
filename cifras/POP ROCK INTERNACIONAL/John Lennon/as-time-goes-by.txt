John Lennon - As Time Goes By

 C Em F G
    Dm            G       Dm              G7
You must remember this, a kiss is still a kiss,
  C              Am  Em Am
a sigh is just a sigh.
    D           D7       G       G7        C  Em F G
The fundamental things apply, as time goes by.

    Dm              G         Dm                 G7
And when two lovers woo, they still say, 'I love you,'
   C            Am  Em Am
on that you can rely.
   D               D7     G          G7        C  F C C7
No matter what the future brings, as time goes by.

CHORUS:
F                            A7
Moonlight and love songs are never out of date...
Dm                      F
hearts full of passion, jealousy and hate.
Am                  D                 D7
Woman needs man and man must have his mate,

G             Dm      G G7
that, no one, can de..ny.

           Dm                 G
Well, it's still the same old story,
  Dm                 G7       C             Am  Em Am
A fight for love and glory, a case of do or die.
    D                 D7      G          Dm        C  G7 C C7
The world will always welcome lovers, as time goes by.
            D                 D7      G
Oh yes, the world will always welcome lovers,
   Dm        C  F C
as time goes by.

CHORUS:


----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
