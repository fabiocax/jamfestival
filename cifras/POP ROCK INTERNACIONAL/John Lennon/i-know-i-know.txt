John Lennon - I Know (I Know)

Intro:
E|--------------------------------------------------
B|--------------------------------------------------
G|------2-------2---2-------2---2--------2---2------
D|----2-----2h4---4-----4h5---4------5p4---4---4---- 2x
A|--0----0-----------0------------0----------------
E|--------------------------------------------------

E|-------------------------
B|-------------------------
G|------2-------2---2------
D|----2-----2h4---4---4----
A|--0-----0----------------
E|-------------------------

E|-------------------------
B|-------------------------
G|-------------------------
D|-------2--------2---2----
A|-----2------2h4---4---4--
E|---0------0--------------


E|---------------------------------------------------
B|--------2---------------------2------------2-------
G|----------2--------6--------2-----2----2-------2---
D|-----2-----------6---6----4-----4----2---2---2-----
A|---0-----------4-----------------------------------
E|---------------------------------------------------

E|------------------------------
B|---------3-------------5------
G|------2----2--------4-----4---
D|---0-------------2------------
A|------------------------------
E|------------------------------

E|-------------------------------
B|-------------------------------
G|------2-------2---2------------
D|----2-----2h4---4-----4h5~~----
A|--0-----0-----------0----------
E|-------------------------------


A
The years have passed so quickly
                    E
One thing I've understood
A    C#m  F#m   A/C#
I am only learning
D           E          A
To tell the trees from wood..


E|-----------------------------------------
B|--2----5-------------------12------------
G|--2------4b-2~~-------1-------11b---9~---
D|--2-------------------2------------------
A|--0-------------------2------------------
E|----------------------0------------------

E|-----------------------------------
B|--14-14-14---12-12-12---11-11-11---
G|-----------------------------------
D|--14-14-14---12-12-12---10-10-10---
A|-----------------------------------
E|-----------------------------------


A                    E
I know what's coming down
           D                 A
And I know where it's coming from
      A            Bm7         E
And I know and I'm sorry (yes I am )
      D           E         A
But I never could read your mind.


    A                 E
And I know just how you feel
      D                     A
And I know now what I have done
      A          Bm7            E
And I know and I'm guilty (yes I am )
      D           E        A
But I never could speak my mind.


A
I know what I was missing
                    E
but now my eyes can see
A       C#m     F#m  A/C#
I put myself in your place
D      E       A
as you did for me


D    E                     A  C#m  F#m  A/C#
Today I love you more than yesterday
      D                    A C#m F#m
Right now I love you more right now

    A                 E
I know what's coming down
      D                       A
I can feel where it's coming from
       A  A7M               Bm7        E
And I know it's getting better all the time
      D             E       A
As we share in each other's minds.


D     E                    A  C#m F#m A/C#
Today I love you more than yesterday
      D E                 A  C#m  F#m
Right now I love you more right now
D   E           A  C#m  F#m
Ooh hoo no more crying      X4

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A7M = X 0 2 1 2 0
Bm7 = X 2 4 2 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
