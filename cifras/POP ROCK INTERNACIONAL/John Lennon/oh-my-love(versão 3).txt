John Lennon - Oh, my love

D                    C#m7
Oh, my love, for the first time in my life
Bm7                   A
My  eyes  are  wide   open
D                     C#m7
Oh, my lover, for the first time in my life
Bm7  E7     A
My eyes can see

F#m                 C#7
I see the wind, oh, I see the trees,
D                         A      C#7
Everything is clear in my heart
F#m                   C#7
I see the clouds, oh, I see the sky
D                          A
Everything is clear in our world

D                     C#m7               F#7
Oh, my lover, for the first time in my life
D                      A
My   mind   is  wide  open

D                     C#m7
Oh, my lover, for the first time in my life
Bm7    E7   A
My mind can feel

F#m                C#7
I feel sorrow, oh, I feel dreams
D                         A      C#7
Everything is clear in my heart
F#m              C#7
I feel life, oh, I feel love
D                          A
Everything is clear in our world

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm7 = X 2 4 2 3 2
C#7 = X 4 3 4 2 X
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
F#7 = 2 4 2 3 2 2
F#m = 2 4 4 2 2 2
