John Lennon - God Save Oz

Chords used:
EADGBE
Bm:    224432
D:     x00232
A7:    x02020

D
Oh, God save Oz one and all
Oh, God save Oz from defeat
                           Bm
Oh, God save Oz climb the wall
D        A7               D
Oh, God save Oz on the street

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
