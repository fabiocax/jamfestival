John Lennon - The Luck Of The Irish

G G  intro

G               C              G
If you had the    luck of the    Irish
Bm                  C                D
You'd be sorry and    wish you were    dead
G                    C              G
You should have the    luck of the    Irish
           Bm               D          G
And you'd     wish you was    English    instead!

A thousand years of torture and hunger
Drove the people away from their land
A land full of beauty and wonder
Was raped by the British brigands! Goddamn! Goddamn!

If you could keep voices like flowers
There'd be shamrock all over the world
If you could drink dreams like Irish streams
Then the world would be high as the mountain of morn


In the 'Pool they told us the story
How the English divided the land
Of the pain, the death and the glory
And the poets of auld Eireland

If we could make chains with the morning dew
The world would be like Galway Bay
Let's walk over rainbows like leprechauns
The world would be one big Blarney stone

Why the hell are the English there anyway?
As they kill with God on their side
Blame it all on the kids the IRA
As the bastards commit genocide! Aye! Aye! Genocide!

If you had the luck of the Irish
You'd be sorry and wish you was dead
You should have the luck of the Irish
And you'd wish you was English instead!
Yes you'd wish you was English instead!

----------------- Acordes -----------------
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
