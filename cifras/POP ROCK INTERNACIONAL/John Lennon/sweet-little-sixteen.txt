John Lennon - Sweet Little Sixteen

Intro: G  C  G  D  C

                           G
They're really rockin' in Boston
                C
In Pittsburgh, PA
                      G
Deep in the heart of Texas
                   C
On down to Frisco bay
              F
All over St. Louis
                   C
And down in New Orleans
                    G
All the cats wanna dance with
                 C
Sweet little sixteen

Sweet, sweet, sweet little sixteen
She's got the grown-up blues

Tight dresses and lipstick
She's sportin' high heel boots
Oh, but tomorrow morning
She's gotta have to change her trend
Become sweet sixteen
And back in class again

           F
Oh mummy, mummy
                     C
Please, please may I go
                               G
You know it's such a sight to see them
                      C
Some other steal the show
Oh daddy, daddy, daddy, daddy
You know I beg of you
              G
Scream it to momma
                     C
It's all right with you

Because they're rockin in New York
In Philadelphia, PA
Deep in the heart of Texas
Down at the Rainbow, LA
All over St. Louis
And down in New Orleans
All them mothers wanted to
Be sweet little sixteen

Sweet, sweet, sweet sixteen
She's so delicate
She's just gotta have it(fade)

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
