John Lennon - One Day At a Time

Bm       Bm/A  G
You are my weakness
G/F#      C         C/B  C
You are my strength
           D                D/C  D/B
Nothing I have in the world
D/A           G
Makes better sense
               F#                  G
'Cause I'm the fish and you're the sea

When we're together
Or when we're apart
There's never a space in between
The beat of our hearts
'Cause I'm the apple and you're the tree

F#            B       E  A  B
One day at a time is all we do
Ab           C#m      F#  Ab   C#m  Dbm/C  C#m/B  A
One day at a time is good for you


You are my woman
I am your man
Nothing else matters at all
Now I understand
That I'm the door and you're the key

And every morning
I wake in your smile
Feeling your breath on my face
And the love in your eyes
'Cause you're the honey and I'm the bee

One day at a time is all we do
One day at a time is good for us two (you too)

Saxophone solo over verse chords, end with:

G               F#                  G
'Cause I'm the fish and you're the sea
'Cause I'm the apple and you're the tree
'Cause I'm the door and you're the key
'Cause you're the honey and I'm the bee

----------------- Acordes -----------------
A = X 0 2 2 2 0
Ab = 4 3 1 1 1 4
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
Bm/A = X 0 4 4 3 X
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
C#m/B = 7 X 6 6 5 X
C/B = X 2 2 0 1 0
D = X X 0 2 3 2
D/A = X 0 X 2 3 2
D/B = X 2 0 2 3 2
D/C = X 3 X 2 3 2
Dbm/C = X 3 2 1 2 0
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
G/F# = X X 4 4 3 3
