The Cranberries - Cordell

Intro :  Am F7+ G C Am   F7+ G

Am                F7+
Though once you ruled my mind,
       G                         C     Am
I thought you`d always be there.
         F7+                          G
And I`ll always hold on to your face.
Am            F7+
But everything changes in time,
           G                 C     Am
And the answers are not always fair.
      F7+                    G
And I hope you`ve gone to a better place.

    Am
Cordell,
         F7+
Time will tell,
        G                 C    Am
They say that you`ve passed away,

      F7+                    G
And I hope you`ve gone to a better place.
         Am
Time will tell,
         F7+
Time will tell,

     G                   C      Am
They say that you`ve passed away,
      F7+                             G
And I know that you`ve gone to a better place.
                Am F7+ G C Am F7+ G
Cordell (x7)...

Am            F7+
Your lover and baby will cry,
        G                    C      Am
But your presense will always remain,
          F7+                G
Is this how it was meant to be?
Am                F7+
You meant something more to me,
        G                C    Am
That what many people will see,
        F7+           G
And to hell with the industry.

   Am
Cordell,
         F7+
Time will tell,
     G                     C     Am
They say that you`ve passed away,
      F7+                          G
And I hope you`ve gone to a better place.

          Am
Time will tell,
         F7+
Time will tell,
         G              C    Am
We all will depart and decay,
         F7+                   G
And we all will return to a better place.
             Am F7+ G C Am F7+ G
Cordell (x7)...
Am F7+ G C Am F7+ G   Am
Na, na, na...

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
F7+ = 1 X 2 2 1 X
G = 3 2 0 0 0 3
