The Cranberries - Animal Instinct

[Intro]  Em  Am7  Cadd9  D
         Em  Am7  Cadd9  D

Em          Am7                       Cadd9
  Suddenly something has happened to me
            D                Em
  As I was having my cup of tea
           Am7            Cadd9
  Suddenly I was feeling depressed
        D                      Em
  I was utterly and totally stressed
           Am7              Cadd9  D  Em
  Do you know you made me cry  ouoou
           Am7             Cadd9  D
  Do you know you made me die
            Em                Am7
  And the thing that gets to me
             Cadd9        D
  Is you'll never really see
            Em                 Am7
  And the thing that freaks me out

             Cadd9      D
  Is I'll always be in doubt
G          D                    Cadd9
  It is a lovely thing that we have
           D
  It is a lovely thing that we
G          D                Cadd9
  It is a lovely thing, the animal
      D
  The animal instinct
Em       Am7     Cadd9       D
   Uhuhu    uhuhu    uhuhuhuuuu

[Solo]

E|----------------------------------------------------------------|
B|----------------------------------------------------------------|
G|---4/5--42--4---5-/-7---5---5-5-----4/5--42--4---5---0---2--2---|
D|----------------------------------------------------------------|
A|----------------------------------------------------------------|
E|----------------------------------------------------------------|

Em            Am7                 Cadd9
  So take my hands and come with me
            D
  We will change reality

Em            Am7                Cadd9
  So take my hands and we will pray
              D
  They won't take you away
Em           Am7            Cadd9  D  Em
  They will never make me cry, nouou
             Am7           Cadd9  D
  They will never make me die

            Em                Am7
  And the thing that gets to me
             Cadd9        D
  Is you'll never really see
            Em                 Am7
  And the thing that freaks me out
          Cadd9         D
  Is I'll always be in doubt
      Em          Am7         Cadd9               D
  The animal, the animal, the animal instinct in me
           Em          Am7         Cadd9               D
  It's the animal, the animal, the animal instinct in me
           Em               Am7              Cadd9               D
  It's the animal, it's the animal, it's the animal instinct in me
           Em               Am7              Cadd9               D
  It's the animal, it's the animal, it's the animal instinct in me
      Em          Am7         Cadd9               D
  The animal, the animal, the animal instinct in me
           Em               Am7              Cadd9               D
  It's the animal, it's the animal, it's the animal instinct in me

[Solo]

E|-------------------------------------------------------------------|
B|-------------------------------------------------------------------|
G|---4/5--42--4---5-/-7---5---5-5-----4/5--42--4---5---0---2--2------|
D|-------------------------------------------------------------------|
A|-------------------------------------------------------------------|
E|-------------------------------------------------------------------|

E|-------------------------------------------------------------------|
B|-------------------------------------------------------------------|
G|---0-0-0-2---2-2---4-4-4-2---2-2---4-4-4-2---2-2---0-0-0-2---2-2---|
D|-------------------------------------------------------------------|
A|-------------------------------------------------------------------|
E|-------------------------------------------------------------------|

E|-------------------------------------------------------------------|
B|-------------------------------------------------------------------|
G|---/-4/5--42--4---5-/-7-/-9-9-9--9-9-9-9-9--7--7-7--5-5-5-4--4-4---|
D|-------------------------------------------------------------------|
A|-------------------------------------------------------------------|
E|-------------------------------------------------------------------|

E|-------------------------------------------------------------------|
B|-------------------------------------------------------------------|
G|---0-0-0-2---2-2--0-0-0-2---2-2-/-4-4-4-2---2-2---0-0-0-2---2-2--0-|
D|-------------------------------------------------------------------|
A|-------------------------------------------------------------------|
E|-------------------------------------------------------------------|

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Cadd9 = X 3 5 5 3 3
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
