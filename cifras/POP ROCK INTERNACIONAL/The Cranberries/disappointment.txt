The Cranberries - Disappointment

Intro: B5

Versos:
G D C B5

Refrão:
 C Em

Riff 1
E|-10-10-10-10-8-7---|
B|-----------------8-|

Riff 2
E|-7-7-7-7-|-5-5-5-5-|-8-8-8-8-|-7-7-7-7-|

Words:
Disappointment
You shouldn't have done
You couldn't have done
You wouldn't have done
The things you did then

And we could've been happy
What a piteous thing
A hideous thing
Was dangered by the rest
But it won't get any harder
And I hope you find your way again
And it won't get any higher
But it all boils down to what you did
Then

In the night we fight, I fled, you're right
It was exactly then, it was exactly then
I decided, decided, decided, decided
And drew you out
In the night we fight, I fled, you're right
It was exactly then, it was exactly then
I decided, decided, decided, decided

But it won't be any harder
And I hope you find your way again
And it won't get any higher
But it all boils down to what you did
Then

Disappointment

----------------- Acordes -----------------
B5 = X 2 4 4 X X
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
