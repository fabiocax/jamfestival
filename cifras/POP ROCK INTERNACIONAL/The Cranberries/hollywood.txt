The Cranberries - Hollywood

Intro  Em   C   Am
Intro :
E|-----------0--------------0---------------0---------------0----------
B|-------0------0-------1------1--------1-------1--------1--------1----
G|----0-------------0--------------2---------------2-------------------
D|---------------------------------------------------------------------

Em         C             Am
I've got a picture in my head
In my head
Em          C             Am
It's me and you we are in bed
We are in bed
Em            C                Am
You've always beenthere when I called
When I called
Em            C                  Am
You've always been there most of all, all, all, all

  (Refrão)


Em      C        Am
This is not Hollywood
Like I understood
Em C        Am
Is not Hollywood
Like, like, like
Em        C
Run away, run away,
Am
Is there anybody there?
Em        C
Run away, run away,
Am
Is there anybody there?
Em        C         Am
Run away, run away, get away, away....

Em         C             Am
I've got a picture in my room
In my room
Em       C               Am
I will return there I presume
It will be soon
Em           C        Am
The greatest irony of all
Shoot the wall
Em          C             Am
It's not so glamourous at all, all, all

Refrão

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Em = 0 2 2 0 0 0
