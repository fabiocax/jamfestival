The Cranberries - Dreams

Introdução: E A B

solo (2X)
E|--------------------------------------------------------------- 
B|--0----0--0-0---0---0-0---0--0-0---0--0-0---0--0-0----0--0-0--- 
G|--------1---------1--------2--------2--------2---------2------- 
D|--------------------------------------------------------------- 
A|------------------------0--------0--------2--------2----------- 
E|-----0--------0----------------------------------------------0- 

E         A
All my life
                   B
Is changing every day
                   E
In every possible way
           A
In all my dreams
                         B
It's never quite as it seems
                    E
Never quite as it seems      riff = solo intr. 1X

                              A
I know I've felt like this before
                              B
But now I'm feeling it even more
                      E
Because it came from you
                    A
Then I open up and see
                           B
The person falling here is me
                    E
A different way to be

(G,C)2X

E         A
I warn more
                  B
Impossible to ignore
                 E
Impossible to ignore
              A
They'll come true
                   B
Impossible not to do
                   E
Impossible not to do       riff = solo intr. 1X
                    A
Now I tell you openly
                                B
You have my heart so don't hurt me
                          E
You're what I couldn't find
                  A
Totally amazing mind
                          B
So understanding and so kind
                      E
You're everything to me
         A
All my life
                   B
Is changing every day
                   E
In every possible way
             A
And oh my dreams
                         B
It's never quite as it seems
                         E
Cause you're a dream to me
          E
Dream to me

Introduão: E A B

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C = X 3 2 0 1 0
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
