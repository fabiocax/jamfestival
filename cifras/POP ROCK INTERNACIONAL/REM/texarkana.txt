R.E.M. - Texarkana

(intro)   Em   G   Bm   Bm   Em   A   G   G

Em          G        Bm
20,000 miles to an oasis.
Em          A            G
20,000 years will I burn.
Em         G        Bm  Em                A             G
20,000 chances I wasted waiting for the moment to turn.
Cadd9               D            Bm
I would give my life to find it, I would give it all.
Cadd9                 Am   Em   D Dsus2   Em   D Dsus2
Catch me if I fall.

Em                        G              Bm
Walking through the woods,  I have faced    it.
Em               A              G
Looking for something to learn.
Em              G               Bm     Em               A            G
30,000 thoughts   have replaced    it, never in my time   to return.
Cadd9               D            Bm
I would give my life to find it, I would give it all.

Cadd9                 Am   Em   D Dsus2   Em   D Dsus2
Catch me if I fall.

(bridge) Asus2 Asus2 G5  G5 Asus2 Asus2 G5   (dedilhando)

           D   D   G   G                 D   D   E   E
All alone,             waiting to fall.

Em           G           Bm
40,000 stars   in the evening.
Em                A               G
Look at them fall   from the sky.
Em        G          Bm
40,000 reasons for living.
Em           A              G
40,000 tears   in your eye.
Cadd9               D            Bm
I would give my life to find it, I would give it all.
Cadd9                 Am   Em   D Dsus2   Em   D Dsus2
Catch me if I fall.
Cadd9                 Am   Em   D Dsus2   Em   D Dsus2
Catch me if I fall.
Cadd9                 Am   Em   D Dsus2   Em   D Dsus2
Catch me if I fall.
Em            D
Catch me if I fall.

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
Cadd9 = X 3 5 5 3 3
D = X X 0 2 3 2
Dsus2 = X X 0 2 3 0
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
