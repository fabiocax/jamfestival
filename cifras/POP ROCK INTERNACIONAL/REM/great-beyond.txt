R.E.M. - Great Beyond

Versão 1:

VERSE
    Am7                     Dsus2
E||--0------0------0--0--|--0------0----0--0--0--|--0------0------0--0--|
B||--1------1------1--0--|--3------3----3--3--0--|--1------1------1--0--|
G||--0------0------0--0--|--2------2----2--2--0--|--0------0------0--0--|
D||--2------2------2--0--|--0------0----0--0--0--|--2------2------2--0--|
A||--0------0------0--0--|-----------------------|--0------0------0--0--|
E||----------------------|-----------------------|----------------------|

--0------0------0--0--||
--3------3------3--0--||
--2------2------2--0--|| guitar 1
--0------0------0--0--||
----------------------||
----------------------||

-----0----------------||
---------3------------||
--------------2-------||
----------------------||  guitar 2 (riff A)  the guitar is absent from most
----------------------||   of the verse, but this riff is played
----------------------||


I've watched the stars fall silent from your eyes
Oh the sights that I have seen  (riff A)
I can't believe that I believed
I wish that you could see (riff A)
that there's a new planet in the solar system
there is nothing up my sleeve (riff A)

CHORUS

C     C/B        Am7
 I'm pushing an elephant up the stairs
 I?m tossing up punchlines that were never there
Over my shoulder a piano falls
G(strum)
crashing to the grouououond

And all this talk of time
Talk is fine
And I don?t want to stay around
Why can?t we pantomime, just close our eyes
And sleep sweet dreams
Being here with wings on our feet

I?m pushing an elephant up the stairs
I?m tossing up punchlines that were never there
Over my shoulder a piano falls
Crashing to the ground

I?m breaking through
I?m bending spoons
I?m keeping flowers in full bloom
I?m looking for answers from the great beyond

I want the hummingbirds, the dancing bears
Sweetest dreams of you
Look into the stars
Look into the moon

I?m pushing an elephant up the stairs
I?m tossing up punchlines that were never there
Over my shoulder a piano falls
Crashing to the ground

I?m breaking through
I?m bending spoons
I?m keeping flowers in full bloom
I?m looking for answers from the great beyond

=================================

Versão 2:

intro:

A9    Dsus2
A9    Dsus2
A9    Dsus2
A9    Dsus2


A9                  Dsus2
 I've watched the sun spot silent
A9             Dsus2
from your eyes
A9                   Dsus2
all the sights that I have seen
A9    Dsus2

A9         Dsus2
 I can't believe that
    A9                 Dsus2
I believed I wish that you could see
A9                          Dsus2
Desert need no cloud in the summer zest
A9               Dsus2
there is nothing up my sleeve

C    Cmaj9(B)   Am
 I'm pushing an elephant up the stairs,
C    Cmaj9(B)    Am
 I'm tossing out punchlines that were never there
C        Cmaj9(B)     Am
 over my shoulder a piano falls,
G
chrashing to the ground.

A9            Dsus2
 and all this talk of time
A9           Dsus2
talk is fine
    A9              Dsus2
but I don't want to stay around
A9     Dsus2

A9            Dsus2
 why can't we pat some on
     A9                  Dsus2
just close your eyes and sleep sweet dreams
A9     Dsus2
in the place on offing
A9     Dsus2

C    Cmaj9(B)   Am
 I'm pushing an elephant up the stairs,
C    Cmaj9(B)    Am
 I'm tossing out punchlines that were never there
C        Cmaj9(B)     Am
 over my shoulder a piano falls,
G
chrashing to the ground.


Am   C        G            D
 I'm breaking through, I'm bending
  Am        C       G
spoons, I'm keeping flowers in full
  Am       C           G                D       C
bloom, I'm looking for answers from the great beyond

A9     Dsus2
A9     Dsus2
A9     Dsus2
A9     Dsus2


           A9              Dsus2
I want the hummingbirds to dance in their
A9                 Dsus2
sweetest dreams of you
A9            Dsus2
look into the stars
A9             Dsus2
look into your eyes

C    Cmaj9(B)   Am
 I'm pushing an elephant up the stairs,
C    Cmaj9(B)    Am
 I'm tossing out punchlines that were never there
C        Cmaj9(B)     Am
 over my shoulder a piano falls,
G
chrashing to the ground.


2x   Am   C        G            D
      I'm breaking through, I'm bending
        Am        C       G
      spoons, I'm keeping flowers in full
        Am       C           G                D       C
      bloom, I'm looking for answers from the great beyond


Am   C        G            D
 I'm breaking through, I'm bending
  Am        C       G
spoons, I'm keeping flowers in full
  Am       C           G                D
bloom, I'm looking for answers from the great...
   C
...answers from the great


Am   C        G            D
 I'm breaking through, I'm bending
  Am        C       G
spoons, I'm keeping flowers in full
  Am       C           G                D
bloom, I'm looking for answers from the great...
   C
...answers from the great   ...answers


Am  Cmaj9(B)  C  Cmaj9(B)   G   D
Am  Cmaj9(B)  C  Cmaj9(B)   G
Am  Cmaj9(B)  C  Cmaj9(B)   G   D   C


Am  Cmaj9(B)  C  Cmaj9(B)   G   D
Am  Cmaj9(B)  C  Cmaj9(B)   G
Am  Cmaj9(B)  C  Cmaj9(B)   G   D   C
C  Cmaj9(B)  A9

the chords:

A9:        x02000
Dsus2:     xx0230
C:         x32010
Cmaj9(B):  x20010
Am:        x02210
G:         320003

=================

Versão 3:


| Am | D | Am | D |      | Am | D | Am | D |

| Am |                  | D |                    | Am |            | D |
I've watched the stars fall silent from your eyes,
| Am |                   | D |              | Am | D |
Oh, the sights that I have seen,
| Am |               | D |               | Am |                          | D |
           I can't believe that I believed I wished that you could see,
| Am |                                | D |         | Am |                 | D |
Desert in the mind of the silencist, there is nothing up my sleeve

| C           Bm |         | Am |
      I'm pushing an elephant up the stairs,
| C           Bm |         | Am |
I'm tossing up punch-lines that were never there,
| C              Bm |         | Am |
    Over my shoulder a piano falls,
| G |                         | G |
Crashing to the ground,

| Am |                | D |             | Am |      | D |
      And all this talk of time, talk is fine,
| Am |             | D |              | Am | D |
But I want to stay around
| Am |                 | D |                    | Am |                       | D |
   Why can't we pantomime, just close our eyes and sleep sweet dreams,
| Am |              | D |                   | Am | D |
Me and you, please are nothing

| C           Bm |         | Am |
      I'm pushing an elephant up the stairs,
| C           Bm |         | Am |
I'm tossing up punch-lines that were never there,
| C              Bm |         | Am |
    Over my shoulder a piano falls,
| G |                         | G |
Crashing to the ground,

| Am      C |            | G                D |           | Am
     I'm breaking through, I'm bending spoons,
         C |        | G        D |         | Am
I'm keeping flowers in full bloom,
         C |             | G                        D |          | Am | Am |


| Am | D | Am | D |      | Am | D | Am | D |

                    | Am |                  | D |               | Am ||                       | D |
I want the hummingbirds to dance in their sweetest dreams of you,
 | Am |               | D |   | Am|             | D  |
Look into the stars, look into the moon,

| C           Bm |         | Am |
      I'm pushing an elephant up the stairs,
| C           Bm |         | Am |
I'm tossing up punch-lines that were never there,
| C              Bm |         | Am |
    Over my shoulder a piano falls,
| G |                         | G |
Crashing to the ground,

| Am      C |            | G                D |           | Am
     I'm breaking through, I'm bending spoons,
         C |        | G        D |         | Am
I'm keeping flowers in full bloom,
         C |             | G                        D |          | Am | Am |

| Am      C |            | G                D |           | Am
     I'm breaking through, I'm bending spoons,
         C |        | G        D |         | Am
I'm keeping flowers in full bloom,
         C |             | G                        D |          | Am | Am |

| Am      C |            | G                D |           | Am
     I'm breaking through, I'm bending spoons,
         C |        | G        D |         | Am
I'm keeping flowers in full bloom,
         C |             | G                        D |          | Am | Am |
I'm looking for answers from the Great, answers from the great

| Am      C |            | G                D |           | Am
     I'm breaking through, I'm bending spoons,
         C |        | G        D |         | Am
I'm keeping flowers in full bloom,
         C |             | G                        D |          | Am | Am |
I'm looking for answers from the Great, answerrrrrsssss,

| Am  C | G  D | Am  C | G  D |     | Am  C | G  D | Am | Am |

| Am  C | G  D | Am  C | G  D |     | Am  C | G  D | Am | Am |

Am

=====================================

Versão 4:

I transcribed it from when they did on the David Letterman Show.
Please use this transciption in conjuntion with the previous one on the archive.

--x-x-x-x-|-0-0-0-0-|-x-x-x-x-|-0-0-------------------------------------------|
--5-5-5-5-|-3-3-3-3-|-5-5-5-5-|-3--3------------------------------------------|
--5-5-5-5-|-2-2-2-2-|-5-5-5-5-|-2---2-----------------------------------------|
--5-5-5-5-|-0-0-0-0-|-5-5-5-5-|-0----0----------------------------------------|
--x-x-x-x-|---------|-x-x-x-x-|-----------------------------------------------|
--5-5-5-5-|---------|-5-5-5-5-|-----------------------------------------------|
 Am7        D5        Am7       D5(Riff A)
This is the Intro and Verse Progression.

-0-0-0-0-|-0-0-0-0-|-0-0-0-0-|-0-0-0-0-|-0-0-0-0-|-0-0-0-0-|-3-3-3-3-3-3-3-3--|
-1-1-1-1-|-1-1-1-1-|-1-1-1-1-|-1-1-1-1-|-1-1-1-1-|-1-1-1-1-|-3-3-3-3-3-3-3-3--|
-0-0-0-0-|-2-2-2-2-|-0-0-0-0-|-2-2-2-2-|-0-0-0-0-|-2-2-2-2-|-0-0-0-0-0-0-0-0--|
-2-2-0-0-|-2-2-2-2-|-2-2-0-0-|-2-2-2-2-|-2-2-0-0-|-2-2-2-2-|-0-0-0-0-0-0-0-0--|
-3-3-2-2-|-0-0-0-0-|-3-3-2-2-|-0-0-0-0-|-3-3-2-2-|-0-0-0-0-|-2-2-2-2-2-2-2-2--|
---------|---------|---------|---------|---------|---------|-3-3-3-3-3-3-3-3--|
 C   C/B   Am        C   C/B   Am        C   C/B   Am        G
This is the Pre-Chorus Progression
"I'm pushing elephants up the stairs.............."

-0-0-0-0-|-3-3-2-2-|-0-0-0-0-|-3-3-0-0-|-0-0-0-0-|-3-3-2-2-|-0-0-0-0-0-0-0-0--|
-1-1-1-1-|-3-3-3-3-|-1-1-1-1-|-3-3-1-1-|-1-1-1-1-|-3-3-3-3-|-1-1-1-1-1-1-1-1--|
-0-0-0-0-|-0-0-2-2-|-0-0-0-0-|-0-0-0-0-|-0-0-0-0-|-0-0-2-2-|-0-0-0-0-0-0-0-0--|
-2-2-0-0-|-0-0-0-0-|-2-2-0-0-|-0-0-2-2-|-2-2-0-0-|-0-0-0-0-|-2-2-2-2-2-2-2-2--|
-3-3-2-2-|-2-2-----|-3-3-2-2-|-2-2-0-0-|-3-3-2-2-|-2-2-----|-3-3-3-3-3-3-3-3--|
---------|-3-3-----|---------|-3-3-----|---------|-3-3-----|------------------|
 C   C/B   G   D     C   C/B   G   Am7   C   C/B   G   D     C
This is the Chorus Progression
"Im breakin' through..........."

-0-0-0-0-|-0-x-x--------------------------------------------------------------|
-1-1-1-1-|-1-5-5--------------------------------------------------------------|
-0-0-0-0-|-0-5-5--------------------------------------------------------------|
-2-2-2-2-|-0-5-5--------------------------------------------------------------|
-3-3-3-3-|-2-x-x--------------------------------------------------------------|
---------|---5-5--------------------------------------------------------------|
 C        C/B  Am7
This is the outro progression, try to accent the C/B chord, as this is what he does.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C/B = X 2 2 0 1 0
D = X X 0 2 3 2
Dsus2 = X X 0 2 3 0
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
