R.E.M. - Crush with Eyeliner

A G    D
I know you
A G           D
I know you've seen her
A       G    D
She's a sad tomato
A           G            D
She's three miles of bad road
A       G        D
Walking down the street
A      G          D
Will I never meet her?
A       G          D
She's a real woman child
A     G            D
Oh my kiss, grip, turpentine

B

E D  G   A
I am smitten

E       D    G
I'm the real thing (I'm the real thing)
E      D       G    A
Let me see her come around
G
My crush with eyeliner


A G  D
I am fine
A   G     D
I'm infatuated
A        G          D
It's all too much pressure
A         G          D
She's all that I can take
A             G        D
What position should I wear?
A   G   D
Cop an attitude? (Faker)
A       G     D
How can I convince her? (Faker)
A        G         D
That I'm inventive too, yeah


B

E D  G   A
I am smitten
E       D    G
I'm the real thing (I'm the real thing)
E  D   G      A
We all invent ourselves
G
and you know me


BRIDGE:  B,A (2x)  D,high E,G

(Solo over bridge):
E|------7--------------               -----------------------
B|--5bhr--5--3--3bhr-----   (2x) then -------7-7-7------9-9-9
G|-------------------5-2-0            -sl/7--7-7-7sl/9--9-9-9
                                      -------7-7--------9-9--(G)


A             G    D
Yeah, she's a sad tomato
A           G            D
She's three miles of bad road
A         G     D
She's her own invention (She's her own invention)
A           G        D
That gets me in the throes
A          G      D
What can I make myself be?
A    G   D
Life is strange (Life is strange)
A          G      D
What can I make myself be (Faker)
A  G        D
To make her mine?


B

E D  G    A
I am smitten
E    D   G
I'll do anything (I'll do anything)
E D           G      A
A kiss, grip, turpentine,
G
my crush with eyeliner


E D  G    A
I am smitten
E   D    G
You know me (Yeah, you know me)
E D             G      A
I could be your Frankenstein
G
my crush with eyeliner


E D  G   A
I am smitten
E       D    G
I'm the real thing (I'm the real thing)
E     D       G     A
Won't you be my valentine?
G
My crush with eyeliner


E D G A   E D G E   A G A G

Final E

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
