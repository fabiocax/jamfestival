R.E.M. - Drive

  Dm
E|---------1-----------1--------------------------1--------1-----------1------------1----|
B|-------------3-----------3--------------------------3--------3-----------------------3-|
G|-----2--------------------------------------2----------------------------------2-------|
D|-0---------------0----------0----3p2h3--0---------------0----------0-------0-----------|
A|-----------------------------------------------------------------------0-3-------------|
E|---------------------------------------------------------------------------------------|

Dm
Smack, crack, bushwhacked
G                              Dm (intro riff)
Tie another one to the racks, baby

Dm
Hey kids, Rock and Roll
G                             Dm  (intro riff)
Nobody tells you where to go, baby

F
What if I ride, what if you walk

Am
What if you rock around the clock
G           G
Tick, tock, tick, tock
Em
What if you did, what if you walk
Em                       G     Dm  (intro riff)
What if you tried to get off, baby

Dm
Hey kids, where are you
G                            Dm        Dm
Nobody tells you what to do, baby

Dm
Hey kids, shake a leg
G                                Dm  (intro riff)
maybe you're crazy in the head, baby

F
Maybe you did, maybe you walked
Am
Baby you rocked around the clock
G           G
Tick, tock, tick, tock
Em
Maybe I ride, maybe you walk
Em                   G     Dm  (intro riff)
Maybe I drive to get off, baby

Nesta parte entra o solo da guitarra elétrica.

C
Hey kids, shake a leg
                                 Dm  (intro riff)
maybe you're crazy in the head, baby

C
Ollie Ollie Ollie Ollie Ollie
C                           Dm  (intro riff)
Ollie Ollie in come free, baby

C
Hey kids, where are you
                              Dm  (intro riff)
Nobody tells you what to do, baby

Aqui acaba o solo da guitarra elétrica.

Dm
Smack, crack, shackalack
G                              Dm  (intro riff)
Tie another one to your back, baby

Dm
Hey kids, Rock and Roll
G                              Dm  (intro riff)
Nobody tells you where to go, baby

F
Maybe you did, maybe you walked
Am
Maybe you rock around the clock
G           G
Tick, tock, tick, tock
Em
Maybe I ride, maybe you walk
Em                   G     Dm  (intro riff)
Maybe I drive to get off, baby

Em
Hey kids, where are you
Em                       G    Dm  (intro riff)
Nobody tells you what to do, baby

Em
Hey kids, Rock and Roll
Em                         G    Dm              (intro riff)
Nobody tells you where to go, baby, baby, baby

Este é o solo que se toca com guitarra elétrica.

E|-5-3-------------------5-3------------------|
B|-----6-5-3-----6---3-------6--3-----6---3---|
G|--------------------------------------------|
D|--------------------------------------------|
A|--------------------------------------------|
E|--------------------------------------------|

E|-5-3-------------------5-3------------------|
B|-----6-3-------------------6--3---5/6---3---|
G|---------5---5/7---5------------------------|
D|--------------------------------------------|
A|--------------------------------------------|
E|--------------------------------------------|

E|-5-3----------------------------------------|
B|-----6-3------------------------------------|
G|---------5---5/7---0---2-0------------------|
D|---------------------------3-2-0------------|
A|--------------------------------------------|
E|--------------------------------------------|

OBS.: Sempre que a música muda de Dm para G, há um riff:

E|-----3--|
B|-----0--|
G|-----0--|
D|-0-3-0--|
A|-----2--|
E|-----3--|

e quando é de G para Dm:

E|-----1--|
B|-----3--|
G|-----2--|
D|-----0--|
A|-0-3----|
E|--------|

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
