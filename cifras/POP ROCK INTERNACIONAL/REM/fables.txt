R.E.M. - Fables

G-4-----4-slide>-6-----6-h-7-----6-6-----6-----6-----------------
D-2-----2-slide>-4--------------------------------7-----7-----7--
A-0-----0--------0-----------------------------------------------
E----------------------------------------------------------------

        call  for?
E---0-----0----------0-----0-------  Chords:
B-----0-----0---0------0-----0---0-  E   A                   E
G----------------------------------      What do you have to say?
D-7-----7-----7----7-----7-----7---
A-0-----0-----0----0-----0-----0---
E----------------------------------

            Keep you hat on your head
E-0-0---0--------0-0--------------0---------0-------5-----5------------
B-0---0-0--------0---0--------------5---------5-------5-----5---5------
G-2-----2-slide>-4-----4-slide>-6-----6-h-7-----6-6-----6-----6--------
D-2-----2-slide>-4--------------------------------7-----7-----7--------
A-0-----0--------0-----------------------------------------------------
E----------------------------------------------------------------------


                                                       E
                                   Home is a long way away
E---0-----0----------0-----0---------0-----0---------------------------
B-----0-----0---0------0-----0---0-----0-----0-------------------------
G----------------------------------------------------------------------
D-7-----7-----7----7-----7-----7---7-----7-----------------------------
A-0-----0-----0----0-----0-----0---0-----0-----------------------------
E----------------------------------------------------------------------

Verse III:                         |  Verse IV:
At the end of the day,             |  When you greet a stranger,
I'll forget your name.             |  Look at his shoes.
I'd like it here if I couldn't     |  Keep your memories in your shoes,
Even see you from a long way away. |  Put your travel behind.

CHORUS (as above):

Break:
Em
At the end of the day,
When there are no friends.

Verse V: (wierd but it works)         Verse VI:
And there are no lovers.           |  Familiar face, a boring place,
Who are you going to call for?     |  I'll forget your name.
What do you have to change.        |  I'd like it here if I couldn't
(Mike Mills in the background)     |  Even see you from a long way away.

CHORUS (as above) END


WENDELL GEE

 C                    Dm
 That's when Wendell Gee takes a tug
   G         G7         Em               G
 Upon the string that held the line of trees
 C                    Dm
 Behind the house he lived in
           G             G7
 He was reared to give respect
       Em                 G        F
 But somewhere down the line he chose
                     C Em F
 To whistle as the wind  blows
  F              C      G                 C
 Whistle as the wind   blows through the leaves

  {same as before}
 He had a dream one night
 That the tree had lost its middle
 So he built a trunk of chicken wire
 To try to hold it up but the wire the wire turned to lizard skin
 And when he climbed it sagged
  F                  C        Em     F
 There wasn't even time to say
  F                  C        G      F
 Goodbye to Wendell Gee
                     C       Em      F
 So whistle as the wind blows
                   C    G                    C  (second time through, play F)
 Whistle as the wind blows through the leaves

                   C      Em      F
 If the wind were colors
                        C     G   F
 And if the air could speak
                       C      Em  F
 Then whistle as the wind blows
                 C     G     C
 Whistle as the wind blows

----------------- Acordes -----------------
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
