R.E.M. - Toys in the Attic (Aerosmith)

Tom :A

A      A E     D     D  A    G     D     A   B
Light voices scream nothing seems real's a dream
E                             A        E
Leaving the things that you learned behind
E                            A      E
Leaving the things that you love behind
E                            A           E
All of the things that you learned were real
E                       A
Nothing except for the years
A   E    D       D A    G     D     A   B
Voices scream, nothing seems real's a dream
E     E     E  D      A
Toys toys toys in the attic

A      A E     D     D  A    G     D     A   B
Light voices scream nothing seems real's a dream
E                            A      E
Leaving the things that you love behind

E                             A        E
Leaving the things that you learned behind
E                            A           E
All of the things that you learned were real
E                       A
Nothing except for the years
A   E    D       D A    G     D     A   B
Voices scream, nothing seems real's a dream
E     E     E  D      A
Toys toys toys in the attic

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
