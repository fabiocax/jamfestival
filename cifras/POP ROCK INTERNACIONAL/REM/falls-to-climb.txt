R.E.M. - Falls to Climb


G             D
I'll take the position
           C           Em  G5
assume the missionare part
G               D
you work by committee,
        C                  Em    G5
you had me pegged from the start.
G              D
I'll be pounce pony,
        C
phony maroney,
       Em        G5
pony before the cart.
G              D
I'll be pounce pony.
         C
this ceremony
      Em       G5
only fills my heart.

D            G     C
who cast the final stone?
D             G        C
who threw the crushing blow?
D              G        C
someone has to take the fall
Am      G
why not me?
G           D
a punch toy volunteer
  C               Em   G5
a weakling on its knee.
G          D
is all you want to hear
    C               Em   G5
and all you want to see.
G                   D
romantically, you'd martyr me
    C                 Em    G5
and miss this story's point
G                     D
it is my strength, my destiny
     C                Em    G5
this is the role that I have chosen.
D            G     C
who cast the final stone?
D             G        C
who threw the crushing blow?
D              G        C
someone has to take the fall
Am      G
why not me?
Am      G
why not me?
F  Am  C   G
F  Am  C   G
F  Am  C   G
Am     G

G                      D
gentlemen mark your opponents
     C             Em   G5
fire into your own ranks.
G                D
pick the weakest as strategec
C
move. square off. to
Em         G5
meet your enemy.
G            D           C
for each and every gathering
            Em       G5
a scapegoat falls to climb.
G          D            C
as I step forward, silently.
        Em    G5
deliberately mine

D            G     C
who cast the final stone?
D             G        C
who threw the crushing blow?
               G        C
someone has to take the fall
Am      G   Am      G
why not me? why not me.
    Am                G
had consequence chose differently
    Am            G
had fate its ugly head
   Am              G
my actions make me beautiful
    Am          G
and dignify the flesh
Am   G
me.
Am   G
I am free.
Am   G
I am free
Am   G
Am   G
Am   G
Am   G

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G5 = 3 5 5 X X X
