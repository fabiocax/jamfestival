R.E.M. - Every Day Is Yours To Win

Intro: Em  A  D  A  Em  A  D  A

         Em           A
With the walk and the talk
        D             A/F#
And the tick tock clock
         Em           A
With the rock and the roll
        D              A/F#
And the bridge and the toll
         Em                A
With the brillance and the light
        D             A/F#
And the stink and the fight.
        Em     A         D     A/F#
And the road ahead of you

  Em            A
I cannot tell a lie,
         D          A/F#
It´s not all cherry pie

         Em        A              D         A/F#
But it´s all there waiting for you, yeah you

Em        A  D        A/F#
  hey yeah,   hey yeah,
Em        A       D   A/F#
  hey yeah, i know.

         Em           A
With the warp and the wooze
        D     A/F#
And the subterfuge
        Em       A              D     A/F#
Does it all look bitter and blue?

         Em             A
Well i´m nothing but confused
     D               A/F#
With nothing left to lose
           Em        A          D          A/F#
And if you buy that, i´ve got a bridge, for you

Em         A            D          A/F#
   hey yeah,(hey yeah),   hey yeah, (hey yeah)
Em         A       D     A/F#
   hey yeah, i know.
Em         A            D         A/F#
   hey yeah, (hey yeah), hey yeah, (hey yeah)
Em         A       D    A/F#
   hey yeah, i know.

ponte:
C   F           C
  ahh  aahaa  aaha
C   F      F#
  ahh  aahaa
C   F           C
  ahh  aahaa  aaha
C   F      F#
  ahh  aahaa

Em           A
Every day is new again
D            A/F#
Every day is yours to win
    Em         A              D   A/F#
And that´s how heroes are made
  Em        A
I wanted to win
     D           A/F#
So i said it again;
Em         A              D   A/F#
That´s how heroes are made

Em         A            D          A/F#
   hey yeah,(hey yeah),   hey yeah, (hey yeah)
Em         A       D     A/F#
   hey yeah, i know.
Em         A            D         A/F#
   hey yeah, (hey yeah), hey yeah, (hey yeah)
Em         A       D    A/F#
   hey yeah, i know.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/F# = 2 0 2 2 2 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
