Pearl Jam - Wishlist

   C
E|-----------------------------------------------------------------|
B|-----------------------------------------------------------------|
G|-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-|
D|-2-2-0-0-2-2-0-0-2-2-0-0-2-2-0-0-3-3-3-3-3-3-3-3-2-2-0-0-2-2-0-0-|
A|-3-3-3-3-3-3-3-3-3-3-3-3-3-3-3-3-3-3-3-3-3-3-3-3-3-3-3-3-3-3-3-3-|
E|-----------------------------------------------------------------|

   C                              F
I wish I  was a neutron bomb for once I could go off
   C                           F
I wish I  was a sacrifice but somehow still lived on

   C                         F
I wish I  was a sentimental ornament you hung on
       C                              F
The Christmas tree, I wish I was the star that went on top
   F             C            F               C
I wish I was the evidence, I wish I was the grounds

     F               C                 G                  F
For fifteen million hands upraised and open towards the sky

   C                         F
I wish I was a sailor with someone who waited for me
   C                           F
I wish I was as fortunate, as fortunate as me
   F            C            F                 C
I wish I was a messenger and all the news was good
   F              C                G                  F
I wish I was the full moon shining off your Camaro's hood

( G  F  C )
( G  F  C )
( G  F  C )

   C                      F
I wish I was an alien at home behind the sun
   C                           F
I wish I was the souvenir you kept your house key on
   F              C                F
I wish I was the pedal brake that you depended on
   F              C                 G             F
I wish I was the verb to trust and never let you down

----------------- Acordes -----------------
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
