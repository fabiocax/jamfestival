Pearl Jam - Daughter

[Intro]

E|----------------|
B|-3--3--3--3-3-3-|
G|-0--4--2--4-4-4-|
D|-0--0--0--0-0-0-|
A|----------------|
E|-3--3-----3-3-3-|

E|-----------------|
B|-3--3--3--3--3-3-|
G|-0--4--2--0--0-0-|
D|-0--0--0--0--0-0-|
A|-----------------|
E|-3--3-----3--3-3-|

E|---------------------------------------|
B|-3--3--3--3--3-3-----3--3-3--3-3--3-3--|
G|-0--4--2--0--0-0-----2--2-2--2-2--2-2--|
D|-0--0--0--0--0-0-----0--0-0--0-0--0-0--|
A|---------------------------------------|
E|-3--3-----3--3-3-----3--3-3--3-3--3-3--|


G       G4      G      G4   G       G4    G      G4
Alone, listless. Breakfast table in an otherwise empty room
G      G4  G    G4     G         G4 G      G4
Young girl, violence. Center of her own attention
  G     G4             G  G4    G      G4
The mother reads aloud, child tries to understand it
  G       G4
Tries to make her proud
G       G4            Em7  Em6 Em7    Em6
  The shades go down. It's in her head
               Em7     Em6        Em7        Em6
Painted room. Can't deny there's something wrong

 G     G4                    G   G4
Don't call me daughter. Not fit to
G      G4           G      G4
  The picture kept will remind me
 G     G4                    G   G4
Don't call me daughter. Not fit to
G      G4           G      G4
  The picture kept will remind me
 G     G4
Don't call me

( Em7  Em6  Em7  Em6  Em7  Em6  Em7  Em6  G  G4 )

Em7   Em6       Em7       Em6       Em7  Em6
 She holds the hand that holds her down
  Em7     Em6       G   G4  G  G4 G G4 G G4
She will rise above. Ooh    Oh

( G  G4  G  G4  G  G4  G  G4 )

G
  Don't call me daughter. Not fit to
G
  The picture kept will remind me
G
  Don't call me daughter. Not fit to be
G
  The picture kept will remind me
 G     G4                    G   G4
Don't call me daughter. Not fit to
G      G4           G      G4
  The picture kept will remind me
 G     G4                    G   G4
Don't call me daughter. Not fit to be
G      G4           G      G4
  The picture kept will remind me
       G
Don't call me

The shades go down
The shades go down
The shades go Go Go

----------------- Acordes -----------------
Em6 = X 7 X 6 8 7
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
G4 = 3 5 5 5 3 3
