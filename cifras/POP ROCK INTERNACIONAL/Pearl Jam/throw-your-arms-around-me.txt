Pearl Jam - Throw Your Arms Around Me

Intro- E A E B

Verse 1

        E                     A   E                              B
I will come to you at daytime,    I will wake you from your sleep,
        E                     A   E                              B
I will kiss you in four places,    As I go running down your street

PreChorus

           E                                 A                 E                  B
And I will squeeze the life right out of you, I will make you laugh and make you cry,
           E              A        E                           A                          B
And we may never forget it, As I make you call my name, as you shout it, to the blue summer sky

Chorus

           E               A      E                     B
And we may never meet again, so shed your skin lets get started,
             E     A          B
And you will throw-- your arms around me! (2X)

E A E B

Verse 2

        E                   A E                       B
I will come to at night-time, I will climb in to your bed,

       E                                       A    E                                B
I will kiss you in a hundred and ninety-five places, as I go swimming round in your head

PreChorus

Chorus

              E    A                   E    A                  E     A             B
And you will thro----w, And you will thro----w, and you will thro----w, your arms.......around me!

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
E = 0 2 2 1 0 0
