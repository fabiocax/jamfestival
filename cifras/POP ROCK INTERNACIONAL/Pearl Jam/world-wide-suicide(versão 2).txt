Pearl Jam - World Wide Suicide

Intro (Bb G G# Bb G G# Bb G G# C# D#)

Bb                                                  G#
I felt the earth on Monday. It moved beneath my feet.

In the form of a morning paper. Laid out for me to see.
Bb                                                      G#
Saw his face in a corner picture. I recognized the name.

Could not stop staring at the. Face I'd never see again.

Bb G G#
It's a shame to awake in a world of pain
                                 C# D#
What does it mean when a war has taken over
Bb G G#
It's the same everyday in a hell manmade
                              C# D#
What can be saved, and who will be left to hold her?

Bb C# G#
The whole world...World over.

D#
It's a worldwide suicide.

------------------2ª parte Toca igual 1ª-----------------------

Medals on a wooden mantle. Next to a handsome face.
That the president took for granted.
Writing checks that others pay.

And in all the madness. Thought becomes numb and naive.
So much to talk about. Nothing for to say.

It's the same everyday and the wave won't break
Tell you to pray, while the devils on their shoulder

Laying claim to the take that our soldiers save
Does not equate, and the truth's already out there

The whole world,... World over.
It's a worldwide suicide.

The whole world,... World over.
It's a worldwide suicide.

Solo(C Bb)

C
Looking in the eyes of the fallen
You got to know there's another, another, another, another
Another way

Bb G G#
It's a shame to awake in a world of pain
                    C# D#
What does it mean when a war has taken over

Bb G G#
It's the same everyday and the wave won't break
                   C# D#
Tell you to pray, while the devils on their shoulder

Bb C# G#
The whole world,... World over.
D#
It's a worldwide suicide.

The whole world,... World over.
It's a worldwide suicide.

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C# = X 4 6 6 6 4
D# = X 6 5 3 4 3
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
