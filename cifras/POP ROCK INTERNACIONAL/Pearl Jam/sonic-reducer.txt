Pearl Jam - Sonic Reducer

the main chord progression is pretty easy C#-G#-A-F# (power chords of
course) this is during the lines

   I don't need anyone, don't need ...

The chords between the verse and the chorus are:

E           F#           A           B
I've got my time machine, got my 'lectronic dream

At least that's what they sound to me. Bye


then for the chorus i play      g-4/6-6-------  (I don't have my guitar
                                D-4/6-6-6-7---   with me so i can't remember
                                A-2/4-4-6-7---   the exact strums but those
                                E-------4-5---   are the chords)
   Sonic reducer, ain't ...

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
