Pearl Jam - I Wont Back Down

(intro)

       Em  D  G
E |-----0-0-22-3--|
B |-----0-0-33-3--|
G |-----0-0-22-0--|
D |-----2-2-00-0--|
A |-----2-2-XX-2--|
E |-3-2-0-0-XX-3--|

      Em      D    G          Em   D   G
Well I won't back down, Well I won't back down
          Em      D          G
You can stand me up at the gates of hell
    Em       D       G       Em  D     G
But I'll stand my ground and I won't back down

   Em     D      G          Em         D     G
No I'll stand my ground, I won't be turned around
      Em           D      G
In a world that keeps on draggin' me down

        Em    D    G      Em   D    G
I will stand my ground and I won't back down

C G6 D G     C      G6   D   G
Hey baby, there ain't no easy way out
C G6 D       Em  D   G        Em  D   G
hey I'll, stand my ground and I won't back down

Well I know what's right, I got just one life
In a world that keeps on pushin' me around
I'll stand my ground and I won't back down

Hey baby, there ain't no easy way out
Hey I will stand my ground and I won't back down
And I won't back down
No I wont't back down


----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G6 = 3 X 2 4 3 X
