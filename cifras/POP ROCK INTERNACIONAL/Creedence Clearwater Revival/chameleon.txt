Creedence Clearwater Revival - Chameleon

Intro B A D

A            G             D                B D
You took me runnin' up a wrong way street.
A        G                      D               B D
When we got there, you said, "Can't you read ?"
A           G                 D          B D
I must be blind, but now and then I see
A         G                 D
Another number where mine used to be.

CHORUS:
D                             G
You keep on changin' your face like a Chameleon.
D                             G
You keep on changin' your face like a Chameleon.

A            G                 D            B D
I say what's up, and then you say it's down.
A           G             D
I see triangles, and you say it's round, round, round.

A        G                D             B D
Saw an empty glass, you said it's full.
A           G                 D
Lord, it's so hot, then you come on cool, cool.

Chorus 4's

solo over verses

repeat chorus few times and fade out

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
D = X X 0 2 3 2
G = 3 2 0 0 0 3
