Creedence Clearwater Revival - Cotton Fields

Chords Used:

    A  Asus2    D       Dsus2     E  E7

E|--0--0--------2--2-------0---------0--0--|
B|--2--0--------3--3-------3---------0--4--|
G|--2--2--------2--2-------2---------1--1--|
D|--2--2--------0--4-------0---------2--2--|
A|--0--0--------x--x-------x---------2--2--|
E|--x--x--------x--x-------x---------x--x--|


Intro:

E|-----------------------------0-----0------------------------|
B|------------------0------5b-----2-----2-----5--5---3--3--2--|
G|------2--2--2--2----2-----------------------4--4---4--2--2--|
D|--2-4----------------------------------------------------2--|
A|------------------------------------------------------------|
E|------------------------------------------------------------|


When I was a little bitty baby
              D              A
My mama would rock me in the cradle,
Asus2 A                        E    E7   E
In them old cotton fields back home;

       A      Asus2 A  Asus2 A
It was down in Louisiana,
Asus2 A      D               A
Just about a mile from Texarkana,
Asus2 A     E                  A       A
In them old cotton fields back home.

              D               Dsus2 D
Oh, when them cotton bolls get rotten
         A         Asus2 A
You cant pick very much cotton,
Asus2 A                        E    E7   E
In them old cotton fields back home.

       A       Asus2 A Asus2 A
It was down in Louisiana,
             D               A
Just about a mile from Texarkana,
Asus2 A     E                  A       A (Strum Once)
In them old cotton fields back home.


When I was a little bitty baby
              D              A
My mama would rock me in the cradle,
Asus2 A  Asus2 A               E    E7   E
In them old cotton fields back home;

       A       Asus2 A Asus2 A
It was down in Louisiana,
Asus2 A      D               A
Just about a mile from Texarkana,
Asus2 A     E                  A       A
In them old cotton fields back home.

              D               Dsus2 D
Oh, when them cotton bolls get rotten
         A              Asus2 A
You cant pick very much cotton,
Asus2 A   Asus2 A              E    E7   E
In them old cotton fields back home.

       A         Asus2 A Asus2 A
It was down in Louisiana,
Asus2 A      D               A
Just about a mile from Texarkana,
Asus2 A     E                  A       A
In them old cotton fields back home.

Solo


When I was a little bitty baby
              D              A
My mama would rock me in the cradle,
Asus2 A Asus2 A                E    E7   E
In them old cotton fields back home;

       A         Asus2 A Asus2 A
It was down in Louisiana,
Asus2 A      D               A
Just about a mile from Texarkana,
Asus2 A     E                  A       A
In them old cotton fields back home.
A           E                  A       A
In them old cotton fields back home.

----------------- Acordes -----------------
A = X 0 2 2 2 0
Asus2 = X 0 2 2 0 0
D = X X 0 2 3 2
Dsus2 = X X 0 2 3 0
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
