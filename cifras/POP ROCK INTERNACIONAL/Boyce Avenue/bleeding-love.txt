Boyce Avenue - Bleeding Love

C9
Closed off from love
I didn't need the pain
Am7
Once or twice was enough
And it was all in vain
F
Time starts to pass       G
Before you knoe it you're frozen
C9
But something happened
For the very first time with you
Am7
My heart melts into the ground
Found something true
F
And Everyone's looking round
                   G
Thinking I'm going crazy
C9
But I don't care what they say

I'm in love with you
Am7
They try to pull me away
But they don't know the truth
F
My heart's crippled by the vein
That I keep on closing
G
You cut me open and I
C9
Keep bleeding
Keep, keep bleeding love
Am7
I keep bleeding
I keep, keep bleeding love
F
Keep bleeding
Keep, keep bleeding love
G
You cut me open
C9
Trying hard not to hear
But they talk so loud
Am7
Their piercing sounds fill my ears
Try to fill me with doubt
F
Yet I know that the goal
                   G
Is to keep me from falling
C9
But nothing's greater
Than the rush that comes with your embrace
Am7
And in this world of loneliness
I see your face
F
Yet everyone around me       G
Thinks that I'm going crazy, maybe, maybe
C9
But I don't care what they say
I'm in love with you
Am7
They try to pull me away
But they don't know the truth
F
My heart's crippled by the vein
That I keep on closing
G
You cut me open and I
C9
Keep bleeding
Keep, keep bleeding love
Am7
I keep bleeding
I keep, keep bleeding love
F
Keep bleeding
Keep, keep bleeding love
G                  Am7         F
You cut me open and I
                    Am7
You cut me open and I
                G  G4
I keep bleeding love
                 Am7
You cut me open and
I Keep bleeding
 F                      C9
Keep, keep bleeding love

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
C9 = X 3 5 5 3 3
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G4 = 3 5 5 5 3 3
