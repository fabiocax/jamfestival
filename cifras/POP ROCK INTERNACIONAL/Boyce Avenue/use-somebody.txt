Boyce Avenue - Use Somebody

                        C
I've been roaming around
                  Em7            F
Always looking down at all I see
           C                Em7               F
Painted faces, build the places I can't reach

                        Am7         Em7  F
You know that I could use somebody
                        Am7         Em7  F
You know that I could use somebody

                C               Em7                F
Someone like you, and all you know, and how you speak
              C            Em7             F
Countless lovers under cover of the street

                       Am7         Em7  F
You know that I could use somebody
                       Am7         Em7  F
You know that I could use somebody

                 C Em7 F
Someone like you
                 C Em7 F
Someone like you

                C                    Em7                F
Off in the night, while you live it up, I'm off to sleep
           C               Em7           F
Waging wars to shake the poet and the beat

                   Am7           Em7  F
I hope it's gonna make you notice
                   Am7           Em7  F
I hope it's gonna make you notice

                C Em7 F
Someone like me
                C Em7 F
Someone like me
                C        Em7 F
Someone like me, somebody

     Em7   Am7
I'm ready now
     F
I'm ready now
                Am7
I'm ready now
     Em7     F
I'm ready now

                        C
I've been roaming around
                  Em7            F
Always looking down at all I see
             C               Em7           F
Painted faces, build the places I can't reach

                       Am7         Em7 F
You know that I could use somebody
                       Am7         Em7 F
You know that I could use somebody

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
