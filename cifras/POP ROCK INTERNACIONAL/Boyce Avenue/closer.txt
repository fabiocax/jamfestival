Boyce Avenue - Closer

Capo Casa 2

[Intro/Riff] - 2x


    C9       D5      Em7     D5


E|----------------------------------------|
B|--3--------------0-0------------(3)-(3)-|
G|----0-0-2--2-0-0-----0-0-2-2--0---------|
D|-----------0---------------0------------|
A|--3-------------------------------------|
E|-------------------0--------------------|


[Verso/Tab] - Tocado por todos os versos


        C9         D5     Em7        D5
E|-------X-----------------X------------|
B|--3--X-X--X--3-3----3--X-X--X--3-3----|
G|--0--X-X--X--2-2h0--0--X-X--X--2-2h0--|
D|--2--X-X--X--0-0----2--X-X--X--0-0----|
A|--3--X----X---------2--X----X---------|
E|-------------------(0)----------------|



[Verso I]


C9         D9            Em7
 Hey, I was doing just fine before I met you

 D9          C9
I drink too much and that's an issue

     D9     Em7   D9
But I'm OK

C9        D9         Em7                           D9
 Hey, you tell your friends it was nice to meet them

       C9          D9         Em7    D9
But I hope I never see them again


[Pré-Refrão]

C9                      D9
 I know it breaks your heart

Em7                      D9
 Moved to the city in a broke-down car

    C9                D9
And   four years, no calls

Em7                             D9
 Now you're looking pretty in a hotel bar

    C9              D9
And I, I, I, I, I can't stop

   Em7              D9
No, I, I, I, I, I can't stop


[Refão]

    C9             D9
So, baby, pull me closer

       Em7               D9
In the back seat of your Rover

       C9               D9
That I know you can't afford

          Em7              D9
Bite that tattoo on your shoulder

          C9                   D9
Pull the sheets right off the corner

        Em7                 D9
Of that mattress that you stole

           C9               D9
From your roommate back in Boulder

          Em7          D9
We ain't ever getting older

[Riff]


    C9       D5      Em7     D5


E|----------------------------------------|
B|--3--------------0-0------------(3)-(3)-|
G|----0-0-2--2-0-0-----0-0-2-2--0---------|
D|-----------0---------------0------------|
A|--3-------------------------------------|
E|-------------------0--------------------|


[Verso II]

C9      D9      Em7               D9
You     look as good as the day I met you

     C9              D9
I forget just why I left you,

         Em7   D9
I was insane

C9     D9             Em7
Stay   and play that Blink-182 song

D9         C9               D9
  That we beat to death in Tucson,

Em7
 OK


[Pré-Refrão]

C9                      D9
 I know it breaks your heart

Em7                      D9
 Moved to the city in a broke-down car

    C9                D9
And   four years, no calls

Em7                             D9
 Now you're looking pretty in a hotel bar

    C9              D9
And I, I, I, I, I can't stop

   Em7              D9
No, I, I, I, I, I can't stop


[Refão]

    C9             D9
So, baby, pull me closer

       Em7               D9
In the back seat of your Rover

       C9               D9
That I know you can't afford

          Em7              D9
Bite that tattoo on your shoulder

          C9                   D9
Pull the sheets right off the corner

        Em7                 D9
Of that mattress that you stole

           C9               D9
From your roommate back in Boulder

          Em7          D9
We ain't ever getting older

----------------- Acordes -----------------
Capotraste na 2ª casa
C9 = X 3 5 5 3 3
D5 = X 5 7 7 X X
D9 = X X 0 2 3 0
Em7 = 0 2 2 0 3 0
