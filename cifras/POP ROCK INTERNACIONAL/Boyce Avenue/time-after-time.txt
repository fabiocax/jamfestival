Boyce Avenue - Time After Time (feat. Megan Davies / Jaclyn Davies)

Capo Casa 1

[Intro] C  G
        C  G
        C  G
        C  G

 C  G         C   G         C     G         C      G
Lying in my bed I hear the clock tick and think of you
 C     G      C    G      C    G      C     G
Caught up in circles confusion is nothing new
  C  D      Bm  C       D         Bm
Flashback warm night, almost left behind
  C  D       Bm  C
Suitcase of memories, time after

 C   G         C       G
Sometimes you pictured me
     C   G      C     G
I'm walking too far ahead
 C    G      C    G         C    G       C      G
You're calling to me I can't hear what you've said


      C   D    Bm  C      D      Bm
Then you said: Go slow, I fall behind
C     D      Bm     C
 The second hand unwinds

            D                        Em7           C    D      G
If you're lost you can look and you will find me, time after time
        D                          Em7         C    D      G
If you fall I will catch you I'll be waiting, time after time

(C  G)
(C  G)


 C  G        C    G         C   G         C       G
After my picture fades and darkness has turned to grey
 C  G            C  G         C  G         C    G
Watching through windows you're wondering if I'm ok
 C  D     Bm  C        D    Bm
Secrets stolen from deep inside
 C   D          Bm      C
The drum beats out of time

            D                        Em7           C    D      G
If you're lost you can look and you will find me, time after time
        D                          Em7         C    D      G
If you fall I will catch you I'll be waiting, time after time

(C  G)
(C  G)

  C   D   Bm  C      D      Bm
You said: Go slow, I fall behind
C     D      Bm     C
 The second hand unwinds

            D                        Em7           C    D      G
If you're lost you can look and you will find me, time after time
        D                          Em7         C    D      G
If you fall I will catch you I'll be waiting, time after time
            D                        Em7           C    D      G
If you're lost you can look and you will find me, time after time
        D                     Em7              C    D      G
If you fall I will catch you I'll be waiting, time after time

 C    D      G
Time after time
 C    D      Em7
Time after time
 C    D      G
Time after time

----------------- Acordes -----------------
Capotraste na 1ª casa
Bm*  = X 2 4 4 3 2 - (*Cm na forma de Bm)
C*  = X 3 2 0 1 0 - (*C# na forma de C)
D*  = X X 0 2 3 2 - (*D# na forma de D)
Em7*  = 0 2 2 0 3 0 - (*Fm7 na forma de Em7)
G*  = 3 2 0 0 0 3 - (*G# na forma de G)
