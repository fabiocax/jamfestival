Boyce Avenue - Too Good At Goodbyes

Afinação: D G C F A D

Em                       Em7
You must think that I'm stupid
 D                       Am
You must think that I'm a fool
 Em                           Em7
You must think that I'm new to this
 D                        Am
But I have seen this all before

     Em
I'm never gonna let you close to me
Em7
Even though you mean the most to me
D                              Am
'Cause every time I open up, it hurts
 Em
So I'm never gonna get too close to you
Em7
Even when I mean the most to you

D                                 Am
In case you go and leave me in the dirt

                Em                       G5(9)
Every time you hurt me, the less that I cry
                    D                               Am
And every time you leave me, the quicker these tears dry
                        Em                    G5(9)
And every time you walk out, the less I love you
                         D                       Am
Baby, we don't stand a chance, it's sad but it's true

                        Em                            G5(9)
I'm way to good at goodbyes (I'm way to good at goodbyes)
                        D                          Am
I'm way to good at goodbyes (I'm way to good at goodbyes)

Em                          Em7
I know you're thinking I'm heartless
D                         Am
I know you're thinking I'm cold
Em                     Em7
I'm just protecting my innocence
D                     Am
I'm just protecting my soul

     Em
I'm never gonna let you close to me
Em7
Even though you mean the most to me
D                              Am
'Cause every time I open up, it hurts
 Em
So I'm never gonna get too close to you
Em7
Even when I mean the most to you
D                                 Am
In case you go and leave me in the dirt

                Em                        G5(9)
Every time you hurt me, the less that I cry
                    D                                Am
And every time you leave me, the quicker these tears dry
                        Em                    G5(9)
And every time you walk out, the less I love you
                         D                       Am
Baby, we don't stand a chance, it's sad but it's true

                        Em                            Em7
I'm way to good at goodbyes (I'm way to good at goodbyes)
                        D                          Am
I'm way to good at goodbyes (I'm way to good at goodbyes)
                            Em                             Em7
No way that you'll see me cry (No way that you'll see me cry)
                         D                          Am
I'm way too good at goodbyes (I'm way to good at goodbyes)
Em                                            Em7
  No, no, no, no, no (I'm way to good at goodbyes)
 D                                                  Am
  No, no, no, no,no, no, no (I'm way to good at goodbyes)
Em                             Em7
  (No way that you'll see me cry)
 D                        Am
  (I'm way to good at goodbyes)

                Em                        Em7
Every time you hurt me, the less that I cry
                    D                                Am
And every time you leave me, the quicker these tears dry
                        Em                    Em7
And every time you walk out, the less I love you
                         D                       Am
Baby, we don't stand a chance, it's sad but it's true

I'm way too good at goodbyes

----------------- Acordes -----------------
Am = X 0 2 2 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
G5(9) = 3 X 0 2 3 3
