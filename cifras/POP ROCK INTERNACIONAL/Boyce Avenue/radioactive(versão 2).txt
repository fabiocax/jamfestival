Boyce Avenue - Radioactive

[Intro]  G#m  B  F#  C#
         G#m  B  F#  C#
         G#m  Bbm  G#m

Oh Oh
             B                F#
I'm waking up to ash and dust
                C#
I wipe my brow and sweat my rust
G#m            B             F#   Bbm  G#m
I'm breathing in the chemicals
               B              F#
I'm breaking in and shaping up
                C#
Then checking out on the prison bus
G#m       B               F#
This is it, the apocalypse

       C#
Oh oh oh
            G#m
I'm waking up

             B
I feel it in my bones
F#                     C#
Enough to make my system blow

G#m                  B
Welcome to the new age

To the new age
F#                    C#
Welcome to the new age

G#m       B
Oh oh oh oh oh oh oh
   F#
I'm radioactive
C#
Radioactive

G#m       B
Oh oh oh oh oh oh oh oh
   F#
I'm radioactive
C#
Radioactive

G#m           B              F#
I raise my flag and dye my clothes
            C#                G#m
It's a revolution, I suppose
                B              F#
We're painted red to fit right in
        Bbm  G#m
Oh oh oh

               B               F#
I'm breaking in and shaping up
               C#                     G#m
Then checking out on the prison bus
         B               F#
This is it, the apocalypse
         C#
Oh oh oh

             G#m
I'm waking up
              B
I feel it in my bones
     F#              C#
Enough to make my system blow
G#m                     B
Welcome to the new age
                F#
To the new age
                     C#
Welcome to the new age

G#m        B
Oh oh oh oh oh oh oh oh
    F#
I'm radioactive
C#
Radioactive

G#m        B
Oh oh oh oh oh oh oh oh
    F#
I'm radioactive
C#
Radioactive

G#m           B
All systems go
      F#         C#
The sun hasn't died
  G#m           B
Deep in my bones
  F#             C#
Straight from inside

             G#m
I'm waking up
              B
I feel it in my bones
     F#             C#
Enough to make my system blow

G#m                    B
Welcome to the new age

To the new age
F#                  C#
Welcome to the new age

----------------- Acordes -----------------
B = X 2 4 4 4 2
Bbm = X 1 3 3 2 1
C# = X 4 6 6 6 4
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
