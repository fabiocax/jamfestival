Boyce Avenue - Back For Good (Take That Cover)

Capo Casa 1

    D    Em7    G    D/E    A   Asus4   Bm   Gmaj9  D/F#
E|-2-----3-----3-----2-----0-----0-----2-----2-----2----|
B|-3-----3-----3-----3-----2-----3-----3-----0-----3----|
G|-2-----0-----0-----2-----2-----2-----4-----0-----2----|
D|-0-----2-----0-----2-----2-----2-----4-----0-----0----|
A|-x-----2-----x-----x-----0-----0-----2-----x-----0----|
E|-x-----0-----3-----x-----x-----x-----x-----3-----2----|


Intro 2x: D  Em7  G  D  A  D

VERSO 1

 D       Em7           G  D  A            D
   I guess now it's time for me to give up
 Em7                G    D A
   I feel it's time
          D                Em7
   Got a picture of you beside me
          G                  D
   Got a lipstick mark still on

                A    D
   Your coffee cup
 Em7     G  D A
   Oh yeah
          D             Em7
   Got a fist of pure emotion
          G                  D A
   Got a head of shattered dreams
          Bm              Bm
   Gotta leave it, gotta leave it
 A        G         D A
   All behind now

REFRÃO
            D                Em7
        Whatever I said, whatever I did I
                G
        Didn't mean it
                D    A             D
        I just want you back for good
                     Em7             G                      D A
        Want you back,  want you back,  want you back for good
            D                   Em7
        Whatever I'm wrong just tell me
                          G
        The song and I'll sing it
                   D     A            D
        You'll be right and understood
                      Em7            G                      D A
        Want you back, want you back, I want you back for good

VERSO 2

  D      Em7         G    D
   Unaware but underlined
         A              D
   I figured out the story
 Em7           G  D A
   It wasn't good
               D            Em7 G D
   But in the corner of my mind
           A       D    Em7
   I celebrated glory
             G           D A
   But that was not to be
            D           Em7
   In the twist of separation
         G                 D A
   You excelled at being free
              Bm
   Can't you find
             Bm      A       G D A
   A little room inside for me

REFRÃO
            D                Em7
        Whatever I said, whatever I did I
                G
        Didn't mean it
                D    A             D
        I just want you back for good
                     Em7             G                      D A
        Want you back,  want you back,  want you back for good
            D                   Em7
        Whatever I'm wrong just tell me
                          G
        The song and I'll sing it
                   D     A            D
        You'll be right and understood
                      Em7            G                      D A
        Want you back, want you back, I want you back for good

PONTE

  Gmaj9               D/F#   Gmaj9           D/F#
   And we'll be together, this time is forever
  Gmaj9                     D/F#
   We'll be fighting and forever we will be
           Bm         A
   So complete in our love
            Gmaj9       D/F#    A  A7 A A4 (slice da terceira casa da segunda corda até quinta casa [conferir no video])
   We will never be uncovered again

REFRÃO (2x)
            D                Em7
        Whatever I said, whatever I did I
                G
        Didn't mean it
                D    A             D
        I just want you back for good
                     Em7             G                      D A
        Want you back,  want you back,  want you back for good
            D                   Em7
        Whatever I'm wrong just tell me
                          G
        The song and I'll sing it
                   D     A            D
        You'll be right and understood
                      Em7            G                      D A
        Want you back, want you back, I want you back for good


Intro: D  Em7  G  D  A

 D       Em7           G        D        A           D
   I guess now it's time that you came back for good

----------------- Acordes -----------------
Capotraste na 1ª casa
A*  = X 0 2 2 2 0 - (*A# na forma de A)
Asus4*  = X 0 2 2 3 0 - (*A#sus4 na forma de Asus4)
Bm*  = X 2 4 4 3 2 - (*Cm na forma de Bm)
D*  = X X 0 2 3 2 - (*D# na forma de D)
D/E*  = X X 2 2 3 2 - (*D#/F na forma de D/E)
D/F#*  = 2 X 0 2 3 2 - (*D#/G na forma de D/F#)
Em7*  = 0 2 2 0 3 0 - (*Fm7 na forma de Em7)
G*  = 3 2 0 0 0 3 - (*G# na forma de G)
Gmaj9*  = 3 2 4 2 X X - (*G#maj9 na forma de Gmaj9)
