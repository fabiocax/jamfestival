Boyce Avenue - No Air

[Verse 1]:
D
If I should die before I wake
C
It's cause you took my breath away
Em                            G
Losing you is like living in a world with no air, oh

[Verse 2]: same as vs1

I'm here alone, didn't wanna leave
My heart won't move, it's incomplete
Wish there was a way that I could make you understand

[Refrain1]
    D              C
But how do you expect me
                          Em
To live alone with just me
                            G
'Cause my world revolves around you


It's so hard for me to breathe

[CHORUS]
D                               C
Tell me how I'm supposed to breathe with no air
                                 Em
can't live, can't breathe with no air
                                   G
That's how I feel whenever you ain't there
there's no air, no air

D                       C
Got me out here in the water so deep
                                   Em
Tell me how you gonna be without me
                            G
If you ain't here I just can't breathe

There's no air, no air


[Verse 3] same as vs 1 &2
I walked, I ran, I jumped, I flew
Right off the ground, to flow to you
There's no gravity to hold me down, for real


[Refrain1]
    D              C
But how do you expect me
                          Em
To live alone with just me
                            G
'Cause my world revolves around you

It's so hard for me to breathe

[CHORUS]
D                               C
Tell me how I'm supposed to breathe with no air
                                 Em
can't live, can't breathe with no air
                                   G
That's how I feel whenever you ain't there
there's no air, no air

D                       C
Got me out here in the water so deep
                                   Em
Tell me how you gonna be without me
                            G
If you ain't here I just can't breathe

There's no air, no air

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
