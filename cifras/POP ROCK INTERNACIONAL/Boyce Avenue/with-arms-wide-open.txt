Boyce Avenue - With Arms Wide Open

     C                C/B
Well I just heard the news today
    Am                              G
It seems my life is going to change
   C              C/B
I closed my eyes, begin to pray
      Am                          G
Then tears of joy stream down my face

Chorus 1:
                F
With arms wide open
          C
Under the sunlight
         C/B
Welcome to this place
              Am
I'll show you everything
               C     C/B
With arms wide open
               Am    G
With arms wide open


Verse 2:
     C               C/B
Well I don't know if I'm ready
   Am                       G
To be the man I have to be
     C                            C/B
I'll take a breath, I'll take her by my side
   Am                          G
We stand in awe, we've created life

Chorus 2:
     Am        F
With arms wide open
          C
Under the sunlight
        C/B
Welcome to this place
              Am G F
I'll show you everything
     G         F
With arms wide open
         C
Now everything has changed
              C/B
I'll show you love
              Am G F
I'll show you everything
     G         C     G
With arms wide open

Solo:  000hhhhh hoooo

=
E|---8-8-8-8-8-8-8-8-8---x-x-x-x-x-x-x-x---x-x-x-x-x--------------------------------------------|
A|---x-x-x-x-x-x-x-x-x---x-x-x-x-x-x-x-x---x-x-x-x-x--------------------------------------------|
D|---x-x-x-x-x-x-x-x-x---0-0-0-0-0-0-0-0---0-0-0-0-0---------0-0-0-0-0-0-0-0 -------------------|
G|---(b9)(b9)(b9)(b9)(b9)(b9)7-7-7---7-7-7-7-7-(b9)(b9)(b9)--5-5-5-5-5-0/2-7/0-5-5-5-5-5-5-7-7--|
C|---0-0-0-0-0-0-0-0-0---0-0-0-0-0-0-0-0---0-0-0-0-0---------0-0-0-0-0-0-0-0--------------------|
C|---0-0-0-0-0-0-0-0-0---0-0-0-0-0-0-0-0---0-0-0-0-0---------0-0-0-0-0-0-0-0--------------------|


Verse 3:
              C                     C/B
I'll show you everything ...oh yeah
              Am
I'll show you everything
              Am
I'll show you love
         C
If I had just one wish
     C/B
Only one demand
            Am
I hope he's not like me
          Am G  F    C
I hope he understands
Am   G  C
That he can take this life
            C/B
And hold it by the hand
           Am
And he can greet the world
               G
With arms wide open...

Chorus 3:
      Am  G    F
With arms wide open
          C
Under the sunlight
        C/B
Welcome to this place
              Am G  F
I'll show you everything
     Am        F
With arms wide open
        C
Now everything has changed
              C/B
I'll show you love
              Am G F
I'll show you everything
               C      C/B
With arms wide open
               Am
With arms wide open
     G         C
With arms wide open

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C/B = X 2 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
