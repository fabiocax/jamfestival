Boyce Avenue - Roar

F
I used to bite my tongue and hold my breath
                   Bb
Scared to rock the boat and make a mess
Dm                      Bb
So I sit quietly, agree politely
F
I guess that I forgot I had a choice
                      Bb
I let you push me past the breaking point
Dm
I stood for nothing, so I fell for
Bb
everything
    F
You held me down but I got up (hey!)

Already brushing off the dust
    Dm
You hear my voice, you hear that sound
Bb
Like thunder gonna’ shake the crowd

    F
You held me down but I got up (hey!)

Get ready cos’ I’ve had enough
Dm                           Bb9
I see it all, I see it now

Refrão:
                     F
I got the eye of the ti--ger, a fighter,
                    Bb
dancing through the fi--re
        Dm
Cos’ I am a champion, and you’re gonna’
Bb       F
hear me roar
        F              Bb         Dm
Louder, louder than a lion, cos’ I am a champion

                  Bb       F
And you’re gonna’ hear me roar (oh oh oh
         Dm
oh oh oh oh oh
              Bb       F
You’re gonna’ hear me roar

F
Now I’m floating like a butterfly,

                Bb
'stinging like a bee, I earned my stripe
Dm                          Bb9
I went from zero, to my own hero
    F
You held me down but I got up (hey!)

Already brushing off the dust
    Dm
You hear my voice, you hear that sound
Bb9
Like thunder gonna' shake the crowd
    F
You held me down but I got up (hey!)

Get ready cos’ I’ve had enough
  Dm                        Bb9
I see it all, I see it now

Refrão:
                     F
I got the eye of the ti--ger, a fighter,
                    Bb
dancing through the fi--re
        Dm
Cos’ I am a champion, and you’re gonna’
Bb       F
hear me roar
        F              Bb         Dm
Louder, louder than a lion, cos’ I am a champion

                  Bb       F
And you’re gonna’ hear me roar (oh oh oh
         Dm
oh oh oh oh oh
              Bb       F
You’re gonna’ hear me roar

Ponte: F  Bb  Dm C/G

F Dm   Bb9
Roar, roar, roar, roar, roaaaar!

Refrão:
                     F
I got the eye of the ti--ger, a fighter,
                    Bb
dancing through the fi--re
        Dm
Cos’ I am a champion, and you’re gonna’
Bb       F
hear me roar
        F              Bb         Dm
Louder, louder than a lion, cos’ I am a champion

                  Bb       F
And you’re gonna’ hear me roar (oh oh oh
         Dm
oh oh oh oh oh
              Bb       F
You’re gonna’ hear me roar

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bb9 = X 1 3 3 1 1
C/G = 3 3 2 X 1 X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
