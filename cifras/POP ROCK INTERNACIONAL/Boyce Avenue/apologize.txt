Boyce Avenue - Apologize

Capo Casa 3

[Intro] Am  F  C9  G

     Am
I'm holdin'on a rope
    F7/9                  C9    G (G#º)
Got me ten feet off the ground
     Am
I'm hearing what you say
       F7/9              C9   G
But I just can't make a sound (Oh yea..)
      Am
They tell me that you mean it
 F7/9                   C9
Then you go and cut me down
     G
But wait
     Am
You tell me that your sorry
 F7/9                   C9
Didn't think I'd turn around
     G
And say


          Am               F7/9
That it's too late to apologize
      C9      G (G#º)
It's too late
          Am               F7/9
Said it's too late to apologize
      C9      G
It's too late

( Am  F  C9  G )

   Am                          F7                     C9  G
I take another chance, take a fall, take a shot from you
   Am                            F7/9
I need you like a heart needs a beat
               C9   G  (G#º)
It's not from you (Yeaaaa)
   Am                    F7/9                   C9
I loved you with a fire red and I was turnin' blue
        G
And to say
  Am
Sorry like the angel
 F7                      C9
Heaven let me think was you
     G
But I'm afraid

It's too late to apologize
It's too late

Said

----------------- Acordes -----------------
Capotraste na 3ª casa
Am*  = X 0 2 2 1 0 - (*Cm na forma de Am)
C9*  = X 3 5 5 3 3 - (*D#9 na forma de C9)
F*  = 1 3 3 2 1 1 - (*G# na forma de F)
F7*  = 1 3 1 2 1 1 - (*G#7 na forma de F7)
F7/9*  = X X 3 2 4 3 - (*G#7/9 na forma de F7/9)
G*  = 3 2 0 0 0 3 - (*A# na forma de G)
G#º*  = 4 X 3 4 3 X - (*Bº na forma de G#º)
