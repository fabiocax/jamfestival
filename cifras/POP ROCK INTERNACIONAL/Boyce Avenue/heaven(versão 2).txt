Boyce Avenue - Heaven

[Intro] B

B     G#m                  F#
Oh  thinkin' about all our younger years
          C#m         G#m
There was only you and me
        A                  F#
We were young and wild and free
B     G#m                  F#
Now nothin' can take you away from me
            C#               G#m
We've been down that road before
            A     E                F#
But that's over now, You keep me comin' back for more

E           F#        G#m
Baby you're all that I want
            B                E
When you're lying here in my arms
E              F#        G#m            F#
I'm finding it hard to believe we're in heaven

E           F#          G#m
And love is all that I need
      B                     E
And I found it here in your heart
E            F#       G#m          F#
It isn't too hard to see we're in heaven

( B )

B   G#m                         F#
Oh, once in your life you find someone
         C#m               G#m
Who will turn your world around
          A                     F#
Bring you up when you're feeling down
B     G#m                           F#
Yeah, nothing could change what you mean to me
           C#m                G#m
Oh there's lots that I could say
         A            E            F#
But just hold me now, 'cause our love will light the way

E           F#         G#m
Baby you're all that I want
            B                E
When you're lying here in my arms
E              F#         G#m           F#
I'm finding it hard to believe we're in heaven
E           F#         G#m
And love is all that I need
      B                     E
And I found it here in your heart
E            F#      G#m          F#
It isn't too hard to see we're in heaven

C#m                      E
I've been waiting for so long
    E            G#m
For something to arrive
    G#m          F#
For love to come along
C#m                       E
Now our dreams are coming true
            E                  B
Through the good times and the bad
F#
Yeah, I'll be standing there by you

[Solo] C#m  G#m  B  E  G#m  F#

E           F#         G#m
Baby you're all that I want
            B                E
When you're lying here in my arms
E              F#         G#m           F#
I'm finding it hard to believe we're in heaven
E           F#         G#m
And love is all that I need
      B                     E
And I found it here in your heart
E            F#      G#m          F#
It isn't too hard to see we're in heaven re in heaven

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C# = X 4 6 6 6 4
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
