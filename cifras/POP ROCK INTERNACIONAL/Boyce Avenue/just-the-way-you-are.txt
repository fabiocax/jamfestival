Boyce Avenue - Just The Way You Are

Parte 1:
   C#
Oh her eyes, her eyes;
                                            Bbm7
Make the stars look like they're not shining.
Her hair, her hair;
                                 F#
Falls perfectly without her trying.
She's so beautiful;
                   C#
And I tell her every day.

Parte 2:
    C#
Yeah; I know, I know,
When I compliment her
                     Bbm7
She won't believe me.
And it's so, it's so
                                          F#
Sad to think that she don't see what I see.

But every time she asks me "do I look okay?",
  C#
I say:

Refrão:
                C#
When I see your face,
              Bbm7
There's not a thing that I would change.
              F#
Cause you're amazing,
                 C#
Just the way you are.
             C#
And when you smile,
                Bbm7
The whole world stops and stares for a while.
                   F#
Cause girl you're amazing,
                 C#
Just the way you are.

Parte 3:
      C#
Yeah, Her lips, her lips;
       C#                                  Bbm7
I could kiss them all day if she'd let me.
Her laugh, her laugh;
Bbm7                                F#
She hates but I think it's so sexy.
                   F#
She's so beautiful
                C#
And I tell her every day.

Parte 4:
       C#
Oh you know, you know, you know
I'd never ask you to change.
   Bbm7
If perfect's what you're searching for
Then just stay the same.
  F#
So, don't even bother asking
If you look okay;
           C#
You know I say:

Refrão 2:
                C#
When I see your face,
              Bbm7
There's not a thing that I would change.
              F#
Cause you're amazing,
                 C#
Just the way you are.
             C#
And when you smile,
                Bbm7
The whole world stops and stares for a while.
                  F#
Cause girl you're amazing,
                 C#
Just the way you are.
                C#
The way you are.
                Bbm7
The way you are.
            F#
Girl you're amazing,
                 C#
Just the way you are.
            C#
The way you are.
            Bbm7
The way you are.
                F#
Cuz girl you're amazing
                 C#
Just the way you are

Refrão 3:
                C#
When I see your face,
              Bbm7
There's not a thing that I would change.
              F#
Cause you're amazing,
                 C#
Just the way you are.
             C#
And when you smile,
                Bbm7
The whole world stops and stares for a while.
                   F#
Cause girl you're amazing,
                 C#
Just the way you are.

----------------- Acordes -----------------
Bbm7 = X 1 3 1 2 1
C# = X 4 6 6 6 4
F# = 2 4 4 3 2 2
