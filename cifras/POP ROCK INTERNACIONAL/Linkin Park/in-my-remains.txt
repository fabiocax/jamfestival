Linkin Park - In My Remains

Intro 3x: F  G  Am  B5  C  Am  G

Am
Separate
Dm                                   G
Sifting through the wreckage i can't concentrate
Am                      G       Am
Searching for a message in the fear and pain
Dm                            G               Am
Broken down and waiting for a chance to feel alive

       F  G  Am   B5
Now in my remains
    C               Am  G
Are promises that never came
         F G     Am   B5
Set this silence free
   C                   Am  G
To wash away the worst of me

Am
Come apart

Dm                             G
Falling in the cracks of every broken heart
Am                           G       Am
Digging through the wreckage of your disregard
Dm                             G               Am
Sinking down and waiting for a chance to feel alive

       F  G  Am   B5
Now in my remains
    C               Am  G
Are promises that never came
         F G     Am   B5
Set this silence free
   C                   Am  G
To wash away the worst of me

        F   G Am     B5 C          Am G
Like an army  falling     one by one by one
        F   G Am     B5 C          Am G
Like an army  falling     one by one by one
        F   G Am     B5 C          Am G
Like an army  falling     one by one by one
        F   G Am     B5 C          Am G
Like an army  falling     one by one by one

       F  G  Am   B5
Now in my remains (One by one, one by one)
    C               Am  G
Are promises that never came (One by one by one)
         F G     Am   B5
Set this silence free (One by one, one by one)
   C                   Am  G
To wash away the worst of me (One by one by one)

        F   G Am     B5 C          Am G
Like an army  falling     one by one by one
        F   G Am     B5 C          Am G
Like an army  falling     one by one by one

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B5 = X 2 4 4 X X
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
