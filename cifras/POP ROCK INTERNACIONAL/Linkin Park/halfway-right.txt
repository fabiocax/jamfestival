Linkin Park - Halfway Right

Intro:
     A                           D              F#m
I scream at myself when there's nobody else to fight
         A                          D                      F#m
I don't lose, I don't win, if I'm wrong, then I'm halfway right

Primeira Parte:
A                          Bm
Used to get high with the dead end kids
  F#m
Abandoned houses where the shadows live
   A                        Bm
I never been higher than I was that night
   F#m
I woke up driving my car
   A                        Bm
I couldn't see then what I see right now
     F#m
The road dissolving like an empty vow
 A                           Bm
Couldn't remember where I'd been that night

    F#m
I knew I took it too far

Pré-Refrão:
D                A
All you said to do was slow down
E                    F#m
I remember, now I remember
D                A
All you said to do was slow down
 E
But I was already gone

Refrão:
     A                           Bm             F#m
I scream at myself when there's nobody else to fight
         A                          Bm                     F#m
I don't lose, I don't win, if I'm wrong, then I'm halfway right
    A                       Bm                 F#m
I know what I want, but it feels like I'm paralyzed
         A                          Bm                     F#m
I don't lose, I don't win, if I'm wrong, then I'm halfway right (halfway right)

Segunda Parte:
 A                          Bm
Told me "Kid, you're going way to fast
     F#m
You burn too bright, you know you'll never last"
        A                         Bm
It was bullshit then, I guess it makes sense now
   F#m
I woke up driving my car
 A                      Bm
Said I'd lose you if I lost control
F#m
I just laughed because what do they know?
 A                  Bm
Here I am, standing all alone
   F#m
Because I took it too far

Pré-Refrão:
D                A
All you said to do was slow down
E                    F#m
I remember, now I remember
D                A
All you said to do was slow down
 E
But I was already gone

Refrão:
     A                           Bm             F#m
I scream at myself when there's nobody else to fight
         A                          Bm                     F#m
I don't lose, I don't win, if I'm wrong, then I'm halfway right
    A                       Bm                 F#m
I know what I want, but it feels like I'm paralyzed
         A                          Bm                     F#m
I don't lose, I don't win, if I'm wrong, then I'm halfway right (halfway right)

Ponte:
 D           A
Na na na na na na na na
 E                  F#m
Nana nana, na nana nana
 D           A
Na na na na na na na na
 E                 F#m
But I was already gone

Refrão Final:
     A                           Bm             F#m
I scream at myself when there's nobody else to fight
         A                          Bm                     F#m
I don't lose, I don't win, if I'm wrong, then I'm halfway right
 D           A
Na na na na na na na na
 E                  F#m
Nana nana, na nana nana
 D           A
Na na na na na na na na
 E                 F#m
But I was already gone
     A                           Bm             F#m
I scream at myself when there's nobody else to fight
         A                          Bm                     F#m
I don't lose, I don't win, if I'm wrong, then I'm halfway right

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
