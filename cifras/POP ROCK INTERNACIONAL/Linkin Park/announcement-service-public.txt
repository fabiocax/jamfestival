Linkin Park - Announcement Service Public

Intro - Guitar 1
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|

|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|

(Synth enters)
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|


|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|

|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|

|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|

(Backwards lyrics enter)
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|

|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|

(Drums/bass enter)
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|

|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|

|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|

|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|

(Synth re-enters)
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|

|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|

|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|

|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|Guitar 1
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------12~---------------------------------------------|
|---------------------(fade in w/feedback)--------x-----------------------------------------------|
|-------------------------------------------------10~---------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|Guitars 2 and 3
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|---------------------(fade in w/feedback)--------5*~---------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|Guitar 4

Heavy Part
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|Guitar 1
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-|
|-x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--|
|-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-|
|-------------------------------------------------------------------------------------------------|
|Guitars 2 and 3
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-10----------------------------------------------------------------------10----------------------|
|-10----------------------------------------------------------------------10----------------------|
|-10----------------------------------------------------------------------10----------------------|
|Guitar 4

|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|Guitar 1
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-|
|-x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--|
|-8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--|
|-------------------------------------------------------------------------------------------------|
|Guitars 2 and 3
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-8-----------------------------------------------8-----------------------8-----------------------|
|-8-----------------------------------------------8-----------------------8-----------------------|
|-8-----------------------------------------------8-----------------------8-----------------------|
|Guitar 4

|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|Guitar 1
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-|
|-x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--|
|-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-|
|-------------------------------------------------------------------------------------------------|
|Guitars 2 and 3
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-6-----------------------------------------------------------------------6-----------------------|
|-6-----------------------------------------------------------------------6-----------------------|
|-6-----------------------------------------------------------------------6-----------------------|
|Guitar 4

|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|Guitar 1
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-|
|-x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--|
|-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-|
|-------------------------------------------------------------------------------------------------|
|Guitars 2 and 3
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-5-----------------------------------------------8-----------------------8-----------------------|
|-5-----------------------------------------------8-----------------------8-----------------------|
|-5-----------------------------------------------8-----------------------8-----------------------|
|Guitar 4

|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|Guitar 1
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-|
|-x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--|
|-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-|
|-------------------------------------------------------------------------------------------------|
|Guitars 2 and 3
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-10----------------------------------------------------------------------10----------------------|
|-10----------------------------------------------------------------------10----------------------|
|-10----------------------------------------------------------------------10----------------------|
|Guitar 4

|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|Guitar 1
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-|
|-x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--|
|-8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--|
|-------------------------------------------------------------------------------------------------|
|Guitars 2 and 3
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-8-----------------------------------------------8-----------------------8-----------------------|
|-8-----------------------------------------------8-----------------------8-----------------------|
|-8-----------------------------------------------8-----------------------8-----------------------|
|Guitar 4

|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|Guitar 1
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-|
|-x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--|
|-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-|
|-------------------------------------------------------------------------------------------------|
|Guitars 2 and 3
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-6-----------------------------------------------------------------------6-----------------------|
|-6-----------------------------------------------------------------------6-----------------------|
|-6-----------------------------------------------------------------------6-----------------------|
|Guitar 4

|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|Guitar 1
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-|
|-x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--|
|-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-|
|-------------------------------------------------------------------------------------------------|
|Guitars 2 and 3
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-5-----------------------------------------------8-----------------------8-----------------------|
|-5-----------------------------------------------8-----------------------8-----------------------|
|-5-----------------------------------------------8-----------------------8-----------------------|
|Guitar 4

|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|Guitar 1
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-|
|-x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--|
|-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-|
|-------------------------------------------------------------------------------------------------|
|Guitars 2 and 3
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-10----------------------------------------------------------------------10----------------------|
|-10----------------------------------------------------------------------10----------------------|
|-10----------------------------------------------------------------------10----------------------|
|Guitar 4

|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|Guitar 1
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-|
|-x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--|
|-8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--8--|
|-------------------------------------------------------------------------------------------------|
|Guitars 2 and 3
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-8-----------------------------------------------8-----------------------8-----------------------|
|-8-----------------------------------------------8-----------------------8-----------------------|
|-8-----------------------------------------------8-----------------------8-----------------------|
|Guitar 4

|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|Guitar 1
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-|
|-x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--|
|-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-|
|-------------------------------------------------------------------------------------------------|
|Guitars 2 and 3
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-6-----------------------------------------------------------------------6-----------------------|
|-6-----------------------------------------------------------------------6-----------------------|
|-6-----------------------------------------------------------------------6-----------------------|
|Guitar 4

|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|Guitar 1
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-12-|
|-x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--x--|
|-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-10-|
|-------------------------------------------------------------------------------------------------|
|Guitars 2 and 3
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-5-----------------------------------------------8-----------------------8-----------------------|
|-5-----------------------------------------------8-----------------------8-----------------------|
|-5-----------------------------------------------8-----------------------8-----------------------|
|Guitar 4

Outro
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|Guitar 1
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-12~---------------------------------------------------------------------------------------------|
|-x-----------------------------------------------------------------------------------------------|
|-10~---------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|Guitars 2 and 3
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-10~---------------------------------------------------------------------------------------------|
|-10~---------------------------------------------------------------------------------------------|
|-10~---------------------------------------------------------------------------------------------|
|Guitar 4

|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|Guitar 1
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-\10~--------------------------------------------------------------------------------------------|
|--x----------------------------------------------------------------------------------------------|
|-\8~---------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|Guitars 2 and 3
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-\8~---------------------------------------------------------------------------------------------|
|-\8~----------------------------------------------------------------------------------(fade out)-|
|-\8~---------------------------------------------------------------------------------------------|
|Guitar 4

|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|Guitar 1
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-/12~--------------------------------------------------------------------------------------------|
|--x-----------------------------------------------------------------------------------(feedback)-|
|-/10~--------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|Guitars 2 and 3

|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|----------12-------------------------12-------------------12-------------------------12----------|
|-10----------------10----------10----------10----10----------------10----------10----------10----|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|Guitar 1
|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-(continue feeback)------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|-------------------------------------------------------------------------------------------------|
|Guitars 2 and 3

----------------- Acordes -----------------
