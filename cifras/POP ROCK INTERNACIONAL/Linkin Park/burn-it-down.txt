Linkin Park - Burn It Down

[Intro] Dm  F  Am  G

E|-----8-8---------10--8-----8--7------------|
B|----------8-10-----------10--------8-------|
G|-7-9---------------------------------------|
D|-------------------------------------------|
A|-------------------------------------------|
E|-------------------------------------------|


           Dm       F
The cycle repeated
       Am                    G
As explosions broke in the sky
             Dm   F
All that I needed
        Am                    G
Was the one thing I couldn't find
               Dm
And you were there at the turn
 F                  Am  G
Waiting to let me know


                  Dm   F
We're building it up
                  Am  G
To break it back down
                    Dm  F
We're building this up
            Am
To burn it down
We can't wait
 G                  Dm   F  Am  G
To burn it to the ground

            Dm     F
The colors conflicted
         Am                      G
As the flames climbed into the clouds
             Dm   F
I wanted to fix this
     Am                            G
But couldn't stop from tearing it down

               Dm
And you were there at the turn
 F                      Am  G
Caught in the burning glow
            Dm
And I was there at the turn
 F                   Am    G
Waiting to let you know

                  Dm   F
We're building it up
                  Am   G
To break it back down
                    Dm  F
We're building this up
            Am
To burn it down
We can't wait
 G                  Dm   F  Am  G
To burn it to the ground

             Dm
You told me yes

You held me high
       F
And I believed when you told that lie
               Am
I played that soldier

You played king
               G
And struck me down when I kissed that ring
     Dm
You lost that right to hold that crown
            F
I built you up but you let me down
     Am
So when you fall
I'll take my turn
              G
And fan the flames as your blazes burn

               Dm
And you were there at the turn
 F                  Am   G
Waiting to let me know

                  Dm  F
We're building it up
                  Am  G
To break it back down
                    Dm  F
We're building this up
            Am
To burn it down
We can't wait
 G                  Dm   F  Am  G
To burn it to the ground

          Dm
When you fall
I'll take my turn
              F
And fan the flames as your blazes burn
          Am
We can't wait
 G                  Dm   F  Am  G
To burn it to the ground

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
