Linkin Park - Loatr / Sotd / Iridescent (Medley)

Am
I dreamed I was missing.
C
You were so scared,
F
But no one would listen
Dm
'Cause no one else care.
Am
After my dreaming,
C
I woke with this fear.
F
What am I leaving,
Dm
When I'm done here?
F
So if you're asking me,
Dm
I want you to know:


[Chorus]
Am
When my time comes
C
Forget the wrong that I've done,
F
Help me leave behind some
Dm
Reasons to be missed.
Am
Don't resent me,
C
When you're feeling empty
F
Keep me in your memory,
Dm
Leave out all the rest

Leave out all the rest...

(Am G/B Fm) 2x

Am            G/B                   F
I close both locks below the window.
Am            G/B                  F
I close both blinds and turn away.
Am             G/B                   F
Sometimes solutions aren't so simple.
Am          G/B                F
Sometimes goodbye's the only way.
        C  G5           F5
And the sun will set for you,
     C  G5           F5
The sun will set for you.
C                              G
And the shadow of the day,
Am                                  F
Will embrace the world in grey,
        C  G5           F5
And the sun will set for you.

            C                     Am         G
Do you feel cold and lost in desperation
    G          C                     Am         G
You build up hope of failures, all you've known
   G      C                     Am         G
Remember all the sadness and frustration
            F           G
And let it go, oh oh oh oh

        C          Am   G
Let it go oh oh oh, oh
        C          Am   G
Let it go oh oh oh, oh
        C          Am   G
Let it go oh oh oh, oh
        F             G
Let it go oh oh oh

            C                  F    Am
Do you feel cold and lost in desperation
    G        C                 F         Am
You build up hope the failures all you've known
   G     C               F     Am       G
Remember all the sadness and frustration

               F        G         Am       G/B F
And let it go, oh oh oh oh, let it go

(Am G/B F)2X
(Am)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
F5 = 1 3 3 X X X
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G5 = 3 5 5 X X X
