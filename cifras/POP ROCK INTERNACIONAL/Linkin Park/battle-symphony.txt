Linkin Park - Battle Symphony

[Intro]  Bb  Eb  Gm  F

         Gm          Bb        Gm       Bb
I got a long way to go and a long memory
        Gm              Bb               Gm          Bb
I been searching for an answer, always just out of reach
  Gm           Bb     Gm       Bb
Blood on the floor, sirens repeat
        Gm                Bb          Gm         Bb
I been searching for the courage to face my enemies

       Cm             F
When they turn down the lights

           Gm     Eb        Bb                  F
I hear my battle symphony, all the world in front of me
      Gm      Eb           Bb       F
If my armor breaks, I'll fuse it back together
 Gm     Eb          Bb                    F
Battle symphony, please just don't give up on me
       Gm        Eb     Bb   F
And my eyes are wide awake


       Bb     Eb     Gm  F
For my battle symphony
       Bb     Eb     Gm  F
For my battle symphony

              Gm         Bb          Gm           Bb
They say that I don't belong, say that I should retreat
          Gm               Bb          Gm         Bb
That I'm marching to the rhythm of a lonesome defeat
         Gm            Bb              Gm            Bb
But the sound of your voice puts the pain in the reverse
       Gm           Bb                Gm        Bb
No surrender, no illusions, and for better or worse
           Cm            F
When they turn down the lights

           Gm     Eb        Bb                  F
I hear my battle symphony, all the world in front of me
      Gm      Eb           Bb       F
If my armor breaks, I'll fuse it back together
 Gm     Eb          Bb                    F
Battle symphony, please just don't give up on me
       Gm        Eb     Bb   F
And my eyes are wide awake

      Eb                 Cm
If I fall, get knocked down
      Bb                  F
Pick myself up off the ground
      Eb                 Cm
If I fall, get knocked down
      Bb                  F
Pick myself up off the ground

           Cm            F
When they turn down the lights

           Gm     Eb        Bb                  F
I hear my battle symphony, all the world in front of me
      Gm      Eb           Bb       F
If my armor breaks, I'll fuse it back together
 Gm     Eb          Bb                    F
Battle symphony, please just don't give up on me
       Gm        Eb     Bb    F
And my eyes are wide awake

       Bb     Eb     Gm  F
For my battle symphony
       Bb     Eb     Gm  F
For my battle symphony

( Bb  Eb  Gm F )

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
