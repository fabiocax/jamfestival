Linkin Park - Hole

I would have sailed away
If I'd known that nothing would change
staring out my window sill
In my wasted prison cell

D             E
And I know what you want
      D              E
And I know what you fear
       D             E
As I cradle your loving
     D               E
And watch you disappear

Refrão ------------- D     A         F
And I feel your heartbeat
D        A      F
Pounding in my head
D        A      F
I like to control you
D          A           F
Cause I can't control myself

D      D A E D A F
Myself--------------------

D
Rain,
A       F
Come my way
D
Mold my head
  A           F
Like a ball of clay
D             A          F
Softly wither into my grave
D              A       F
Never to see, the sun again

(Refrão)

D               A         E
All alone in a crowd by myself
D               A            E
So sorry,wish I could find a way
D             A        E
Back into your hole again
D               A      E
But I've become your enemy

(Refrão)

Hole

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
