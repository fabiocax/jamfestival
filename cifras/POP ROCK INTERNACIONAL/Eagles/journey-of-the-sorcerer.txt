Eagles - Journey Of The Sorcerer

This is the main part and not the sort of chorus, HitchHiker's Guide bit.

E---------------------------------------------|
B---6-4-3-1----6-4-3-1----6-4-3-1-----6-4-8-6-|
G-5----------3----------2----------1----------|
D---------------------------------------------|
A---------------------------------------------|
E---------------------------------------------|

The HitchHiker's Guide movie-version has a variation on the 6-4-3-1 bit which goes like this--

E----------------|
B---6-4--1H3-4P1-|
G-5--------------|
D----------------|
A----------------|
E----------------|

And then a variation on the 5-3-2-1 which goes like this --

E--------------------|
B-----4----6-4-3-1---|
G-3H5--3H5-----------|
D--------------------|
A--------------------|
E--------------------|


The strumming bit is muted in parts but is basically Am, Am7, D7, F6, played with the
on the 3rd fret (Cm, Cm7, etc...)