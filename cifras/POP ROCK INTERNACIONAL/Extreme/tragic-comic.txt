Extreme - Tragic Comic

Introdução do Baixo

G|---------|------------------|--------------------------|
D|---------|o-----------2/5---|-------------------------o| (Não continue tocando à
A|-7\--0-0-|o---2/5-3---------|---------2-0---0-3---2---o|  menos que seja necessário)
E|---------|--3-------------0-|-0-0-4-----------------3--|


Introdução do violão

Riff 1

      G7    C       E7        Am           G7    C       E7        Am
Eb|--1-1-x-0---0-x-0-|---0-x-0---0-0-0--|-1-1-x-0---0-x-0-|---0-x-0---0-0-0-|
Bb|--0-0-x-1---1-x-3-|---3-x-1---1-1-0--|-0-0-x-1---1-x-3-|---3-x-1---1-1-1-|  Batida: B= Baixo    
Gb|o-0-0-x-0---0-x-1-|---1-x-2---2-2-0-o|-0-0-x-0---0-x-1-|---1-x-2---2-2-2-|          X= Cordas abafadas
Db|o-0-0-x-2---2-x-0-|---0-x-2---2-2-0-o|-0-0-x-2---2-x-0-|---0-x-2---2-2-2-|          C= Cima
Ab|--2-2-x-3---3-x-2-|---2-x-0---0-0-0--|-2-2-x-3---3-x-2-|---2-x-0---0-0-0-|  B C X C C X C C X C C X
Eb|--3-3-x---------0-|---0-x------------|-3-3-x---------0-|---0-x-----------|


     F         G7
Eb|-1-1-1-1-x-1-|---1-1-1-1-1-1-|
Bb|-1-1-1-1-x-0-|---0-0-0-0-0-0-|
Gb|-2-2-2-2-x-0-|---0-0-0-0-0-0-|
Db|-3-3-3-3-x-0-|---0-0-0-0-0-0-| 
Ab|-3-3-3-3-x-2-|---2-2-2-2-2-2-|
Eb|-1-1-1-1-x-3-|---3-3-3-3-3-3-|

     Am          G7 D               A
Eb|-0-0-0-0-0-0-1--2-|---2-2-2-2-x---|
Bb|-1-1-1-1-1-1-0--3-|---3-3-3-3-x---|
Gb|-2-2-2-2-2-2-0--2-|---2-2-2-2-x-2-|
Db|-2-2-2-2-2-2-0--0-|---0-0-0-0-x-2-|
Ab|-0-0-0-0-0-0-2----|-------------0-|
Eb|-------------3----|---------------|


Riff 2
         A A            A A A         A A             A A A         A A           A A A
Eb|---------------------------|-----------------------------|---------------------------|
Bb|---------------------------|-----------------------------|---------------------------|
Gb|-----2-2-----------2-2-2---|-----2-2-------4/6\--2-2-2---|-----2-2-----------2-2-2---|
Db|-----2-2-----5-----2-2-2---|-----2-2-----5-----7-2-2-2---|-----2-2-----5-----2-2-2---|
Ab|-----0-0-5/7---7\0-0-0-0---|-----0-0-5/7---------0-0-0---|-----0-0-5/7---7\0-0-0-0---|
Eb|---------------------------|-----------------------------|---------------------------|
                          A A A A
Eb|--------------------------------|
Bb|--------------------------------|
Gb|----------------------2-2-2-2---|
Db|----------------------2-2-2-2---| 
Ab|-----2--------2-0-5/7-0-0-0-0---|
Eb|--------------------------------|
    Começa a música... ''Flow....ers,

Riff 2 Durante toda a música, com exceção do refrão.

Flowers, i sent,
Were found dead on their arrival
The words, i said,
Inserted foot in my mouthful
So when, we dance,
My lead it ain't so graceful
(isn't so, not ain't so)

(Riff 1 / Refrão)

G7     C    E7   Am
I'm a hapless romantic
G7   C   E7      Am
St-t-tuttering p-poet
G7        C      E7    Am
Just call me a tragic comic
        F   G                  Am   G7 D
Cause i'm, in, in love with you

(Segue com Riff 2)

And when, we dine,
I forget to push in your seat
I wear, the wine,
Spilling hearts all over my sleeve
A stitch, in time,
Proposing down on my knees
(splitting between the seams)

(Riff 1 / Refrão)

G7   C    E7     Am
I'm a hapless romantic
G7   C   E7      Am
St-t-tuttering p-poet
G7        C      E7    Am
Just call me a tragic comic
        F   G                  Am   G7 D
Cause i'm, in, in love with you

(Segue com Riff 2)

Nobody, can know the,
Trouble i've, seen
Nobody, can know the,
Trouble i, get into,
When i'm with you

----------------- Acordes -----------------
A*  = X 0 2 2 2 0 - (*G# na forma de A)
Am*  = X 0 2 2 1 0 - (*G#m na forma de Am)
C*  = X 3 2 0 1 0 - (*B na forma de C)
D*  = X X 0 2 3 2 - (*C# na forma de D)
E7*  = 0 2 2 1 3 0 - (*D#7 na forma de E7)
F*  = 1 3 3 2 1 1 - (*E na forma de F)
G*  = 3 2 0 0 0 3 - (*F# na forma de G)
G7*  = 3 5 3 4 3 3 - (*F#7 na forma de G7)
