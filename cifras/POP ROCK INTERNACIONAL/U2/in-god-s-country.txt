U2 - In God's Country

(live)

INTRO

E|-17-17-17-17-17-15-15-15-15-15-15-15-15-15-15---
B|-15-15-15-15-15-13-13-13-13-13-12-12-12-12-12---      Repita várias vezes
G|-14-14-14-14-14-12-12-12-12-12-12-12-12-12-12--- 


SOLO

D|-----------0--- 
A|-0-0-3-2-0-----


         (INTRO)

        D          Am
        Desert Sky
                        D          Am
        Dream beneath a desert sky
                           D             Am
        The rivers run but soon run dry

                          D      Am
        We need new dreams tonight

        Desert rose
        Dreamed I saw a desert rose
        Dress torn in ribbons and in bows
        Like a siren she calls to me

        Em           G      D    Em        G    D
        Sleep comes like a drug... In God's Country
        Em       G           D    Em        G    D
        Sad eyes crooked crosses... In God's Country

        (SOLO)2X
        (INTRO) várias vezes
        (Mesmos acordes do primeiro verso)
        Set me alight
        We'll punch a hole right through the night
        Everyday the dreamers die
        To see what's on the other side

        She is Liberty
        And she comes to rescue me
        Hope faith, her vanity
        The greatest gift is gold


        Sleep comes like a drug... In God's Country
        Sad eyes crooked crosses... In God's Country

       (Solo) 2X
       (Intro) várias vezes
       (D, Am)
        Naked flame
        She stands with a naked flame
        I stand with the sons of Cain
        Burned by the fire of love
                      D             Am
        Burned by the fire of love
                      D             Am
                      love

----------------- Acordes -----------------
Am = X 0 2 2 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
