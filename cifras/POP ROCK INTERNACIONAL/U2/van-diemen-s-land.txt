U2 - Van Diemen's Land

Intro
G D G C G

        G     C
Hold me now,
        G  C G
hold me now
G    D/F#  Em C            D D4 D9 D
Till this hour has gone around
         Bm                 Em
And I´m gone on the rising tide
C          G  D              G  C G
  for to face  Van Diemen´s Land

               G   C           G  C G
It´s a bitter pill  I swallow here
G   D/F# Em  C           D    D4 D9 D
To be a rent  from one so dear
               Bm                  Em
we fought for justice and not for gain
C                G   D          G  C G
  But the magistrate  sent me away


                G   C                   G  C G
Now Kings will rule  and the poor will toil
G         D/F#  Em    C                  D  D4 D9 D
And tear their  hands  as they tear the soil
                Bm                 Em
But a day will come in the downing age
C                 G  D                G  C G
  When an honest man  sees an honest wage


Hold me now, hold me now
Till this hour has gone around
And I´m gone on the rising tide
For to face Van Diemen´s land

----------------- Acordes -----------------
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D4 = X X 0 2 3 3
D9 = X X 0 2 3 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
