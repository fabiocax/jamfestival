U2 - When Love Comes To Town

Yeah, yeah, yeah, yeah, yeah
Yeah, yeah, yeah, yeah, yeah.

Verse1(Bono):
(E)                              A E
I was a sailor, I was lost in sea
(E)                                           A E
I was under the waves, before loves resqued me

(E)                                   (A E)
I was a fighter I could turn on threat
    E(stop)
Now I stand accused of things I've said.

Chorus(B.B.):
(A)
Love comes to town,
                          A
I'm gonna jump that train
      E
When love comes to town,
                           A E
I'm gonna catch that flame.

Maybe I was wrong to ever let you down,
        E(stop)
But I did what I did before love came to town.

This is the B.B.'s fill, do it twice:
(* means bend it up 1/4 of a step)

--------------------E
---5*--5*----3-3----B
------------------4-G
--------------------D
--------------------A
--------------------E

Verse 2(Bono):

Used to make love under the red sunset,
I was making promises I that were soon to forget.
She was pale as the lace of her wedding gown,
But I left her standing before love came to town.


Verse 3(Bono):

Ran into a juke joint when I heard a guitar scream,
The notes were turning blue as days are a dream.
As the music played I saw my life turn around
That was the day before love came to town.

Chorus(B.B.):

The Edge solo (he picks open E chord)
Play the figure 4 times (# this note plays only the first time):

0#-|-----------0-|----------E
---|--0----------|----------B
---|----1--------|----------G
---|-----2-------|----------D
---|-------2-----|----------A
---|---------0---|----------E  

Chorus(B.B.) over The Edge's solo part (the figure above):

B.B.'s solo, sorry I do not remember it, but it is basically played
around the fill he does between the first chorus and the second verse.

Verse 4(B.B.), there is no guitar part over this one:

I was there when they crusified my lord
I held the scabbard when the soldier drew his sword
I threw the dice as they pierced his side,
But I've seen love conquer the great divide.

Chorus(B.B.):

The second solo by B.B. in the beginning of which Bono sings his famous:
"Yeah, Yeah, Yeah.........."
And it fades as B.B. solos.

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
E = 0 2 2 1 0 0
