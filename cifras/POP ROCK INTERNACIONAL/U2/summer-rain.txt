U2 - Summer Rain

E          Dsus4 D            (x5)

           E            Dsus4 D
When you stop seeing beauty
      E            Dsus4 D
You start growing old
     C                D
The lines on your face

     C                D
Are a map to your soul


           E            Dsus4 D
When you stop taking chances
         E              Dsus4 D
You'll stay where you sit
           E          Dsus4 D
You won't live any longer
           E        Dsus4 D
But it'll feel like it
    E        Dsus4 D
Ah Ah
E        Dsus4 D


C    G       D
  I lost myself in the summer rain
C    G       D    Dsus4 D
  I lost myself
C    G       D                 G  Gsus4  Em7  D7sus4
  I lost myself in the summer rain
  C            Am
Oooooh ooh   Aaaaaah
Asus2  Am      G  Gsus4  Em7  D7sus4
In the summer rain
    C                Am  Asus2 Am
Mhm Mhhhhhhm Mhhhm


    E       Dsus4 D
Tequila and Orange
   E        Dsus4 D
Jamaica and rum
E         Dsus4 D
At the Morella
      E           Dsus4 D
Honey on my tongue


E           Dsus4 D   E         Dsus4 D
In a small boat on a generous sea
     C       D     C    D
You let me be your enemy
E      Dsus4 D
Tiny hand
         E           Dsus4 D
With a grip on the world
E               Dsus4 D
Holding our breath now
E            Dsus4 D
Diving for pearls


E          Dsus4 D            (x2)


C    G       D
  I lost myself in the summer rain
C    G       D    Dsus4 D
  I lost myself
C    G       D                 G  Gsus4  Em7  D7sus4
  I lost myself in the summer rain
C
    Ahh Ah Ah
Am     Asus2  Am
Ahh Ah Ah


G               Gsus4
Just as you find me
Em7             D7sus4
Always I will be
   C
A little bit too free
 Am     Asus2  Am
With myself


G               Gsus4
Just as you find me
Em7             D7sus4
Always I will be
   C
A little bit too free
 Am     Asus2  Am
With myself


E          Dsus4 D            (x4)


C    G       D
  I lost myself in the summer rain
C    G       D    Dsus4 D
  I lost myself
C              G     D
  Now there's no one else
              G  Gsus4  Em7  D7sus4
In the summer rain


 C
Raining down
 Am       Asus2  Am
Raining down
G  Gsus4  Em7  D7sus4
Rain


 C
Raining now
 Am      Asus2 Am
Raining now


G               Gsus4
Just as you find me
Em7             D7sus4
Always I will be
   C
A little bit too free
 Am     Asus2  Am
With myself


G               Gsus4
Just as you find me
Em7             D7sus4
Always I will be
   C
A little bit too free
 Am     Asus2  Am
With myself


E          Dsus4 D            (x4)


      E              Dsus4 D
It's not why you're running
       E           Dsus4 D
It's where you're going
      E                 Dsus4 D
It's not what you're dreaming
           E            Dsus4 D
But what you're gonna do


      E                Dsus4
It's not where you're born
D      E           Dsus4 D
It's where you belong
      E          Dsus4 D
It's not how weak
               E           Dsus4 D        E     [fading out]
But what will make you strong

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Asus2 = X 0 2 2 0 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7sus4 = X X 0 2 1 3
Dsus4 = X X 0 2 3 3
E = 0 2 2 1 0 0
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
Gsus4 = 3 5 5 5 3 3
