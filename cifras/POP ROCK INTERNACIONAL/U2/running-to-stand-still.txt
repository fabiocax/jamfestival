U2 - Running to Stand Still

D                                  G
  And so she woke up, woke up from where she was lying still, said I
D                                     G
got to do something about where we're going.
D                      G
Step on a steam train,   step out of the driving rain, maybe
D                                G
  run from the darkness in the night, singing
A            C     G
ah, ah la la la de day
A          C     G           D         G
  ah la la la de day, ah la la de day

Sweet the sin, but bitter the taste in my mouth.
I see seven towers, but I only see one way out.
You got to cry without weeping, talk without speaking,
scream without raising your voice, you know I took the poison,
from the poison stream, then I floated out of here, singing
ah, ah la la la de day
ah la la la de day, ah la la de day


 G      D     G
oooh, oooh, oooh

D
  She runs through the street with her eyes painted red
G
  under black belly of cloud in the rain
D
  in through the doorway she brings me
G                                                   D
white gold and pearls, stolen from the sea, she is raging
                       G
she is raging, and the storm blows up in her eyes, she will
A                C    G
    suffer the needle chill
A                    C     G    D         G  D  G  D
    she's running to stand      still

----------------- Acordes -----------------
A = X 0 2 2 2 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
