U2 - The Ground Beneath Her Feet

Am     Em      F7m        G
All my life, I worshipped her.
Am         Em     F7m          G
Her golden voice, her beauty's beat.
        Am      Em
How she made us feel,
        F7m     G
how she made me real,
        F7m                Em
and the ground beneath her feet.
        F7m                G
and the ground beneath her feet.

    Am    Em       F7m     G
And now I can't be sure of anything,
Am Em         F7m        G
     black is white, and cold is heat;
Am Em         F7m         G         Am      Em
     for what I worshipped stole my love away,
           F7m                Em
it was the ground beneath her feet.

        F7m                G
and the ground beneath her feet.

She was my ground, my favorite sound,
my country road, my city street,
my sky above, my only love,
and the ground beneath my feet.

Am Em                F7m      G
Go lightly down your darkened way,
Am Em      F           G
go lightly underground,
Am      Em             F7m    G
I'll be down there in another day,
        F                 G
I won't rest until you're found.

Let me love you true, let me rescue you,
let me lead you to where two roads meet.
O come back above, where there's only love,
and the ground's beneath your feet.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
