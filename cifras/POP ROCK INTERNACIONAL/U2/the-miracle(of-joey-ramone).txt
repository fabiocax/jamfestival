U2 - The Miracle (of Joey Ramone)

INTRO: A C G F E
      VOCALIZAÇÃO

(A G F)

A                           C
I was chasing down the days of fear
G                              F         E
Chasing down a dream before it disappeared
A                           C
I was aching to be somewhere near
G                    F         E
Your voice was all I heard
A                           C
I was shaking from a storm in me
G                              F         E
Haunted by the spectres that we had to see
A                           C
Yeah I wanted to be the melody
G                          F
Above the noise, above the hurt


      A
I was young
    C
Not dumb
G                  F      E
Just wishing to be blinded
   A
By you
     C
Brand new
D
And we were pilgrims on our way

G                D               Bm             A D5 C#5 A5 G5
I woke up at the moment when the miracle occurred
G                      D                     A    A4   A   A4 A
Heard a song that made some sense out of the world
G            D          Bm           A       D5 C#5 A5 G5
Everything I ever lost, now has been returned
G                      D             A    A4   A   A4 A
In the most beautiful sound I'd ever heard

VOCALIZAÇÃO
A  C  G F E

A                           C
We got language so we can't communicate
G                 F         E
Religion so I can love and hate
A             C              G         F         E
Music so I can exaggerate my pain, and give it a name

      A
I was young
    C
Not dumb
G                  F      E
Just wishing to be blinded
   A
By you
      C
Brand new
D
And we were pilgrims on our way


G                D               Bm             A D5 C#5 A5 G5
I woke up at the moment when the miracle occurred
G                      D                     A    A4   A   A4 A
Heard a song that made some sense out of the world
G            D          Bm           A       D5 C#5 A5 G5
Everything I ever lost, now has been returned
G                     D              A    A4   A   A4 A
In the most beautiful sound I'd ever heard


       A5    C5 B5 A5
We can hear you
       A5    C5 B5 A5
We can hear you
       A5    C5 B5  G5   F5   E
We can hear you


(A G F)VOCALIZAÇÃO

G                D               Bm             A
I woke up at the moment when the miracle occurred
G             D              A    A4   A   A4 A
I get so many things I don't deserve
G            D        Bm              A
All the stolen voices will someday be returned
G                  D              A
The most beautiful sound I'd ever heard


A  C  G F E VOCALIZAÇÃO

A      C   G               F    E
Hhho      Your voices will be heard
A      C   G               F    E     A
Hhho      Your voices will be heard

----------------- Acordes -----------------
A = X 0 2 2 2 0
A4 = X 0 2 2 3 0
A5 = X 0 2 2 X X
B5 = X 2 4 4 X X
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C#5 = X 4 6 6 X X
C5 = X 3 5 5 X X
D = X X 0 2 3 2
D5 = X 5 7 7 X X
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
F5 = 1 3 3 X X X
G = 3 2 0 0 0 3
G5 = 3 5 5 X X X
