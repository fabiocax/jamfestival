U2 - All I Want is You

Capo Casa 1

Intro: (G G9 G) (C C9 C) 2x


G          C       G            C
You say you want a diamond ring of gold
G          C        G              C
You say you want your story to remain untold

       Em            C
All the promises we made
      Em                   C
From the cradle to the grave
     G       C    G      C   G     C
When all I want is you

G               C      G               C
You say you'll give me a highway with no one on it
G            C              G                  C
Treasure just to look upon it all the riches in the night
G            C         G              C
You say you'll give me eyes on a moon of blindness
G                  C          G              C
A river in a time of dryness a harbour in the tempest

       Em             C
All the promises we make
      Em                   C
From the cradle to the grave
G         C       G  C      G       C      (G - C - G - C)
When all I want is you

G            C      G                C
You say you want you love to work out right
G                        C
To last with me through the night
G            C    G            C
You say you want a diamond ring of gold
G                     C        G                C
Your story to remain untold your love not to grow cold

       Em              C
All the promises we break
      Em                  C
From the cradle to the grave
G          C       G  C  G  C        G    C     G    C
When all I want is you
(G     C)
All I want is you

G- C >>> até o fim...

----------------- Acordes -----------------
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G9 = 3 X 0 2 0 X
