U2 - Cedarwood Road

INTRO:

riff1 (4x)
E|------------------------------------------------|
B|-10-8-7-8-7-5--7-7-7--5-5-----------------------|
G|------------------------------------------------|
D|------------------------------------------------|
A|------------------------------------------------|
E|------------------------------------------------|

riff2 (2X)
                                         D5    E5
E|------------------------------------------------|
B|------------------------------------------------|
G|----------------------------------7-7---9-9-----|
D|----------------------------------7-7---9-9-----|
A|----------------------------------5-5---7-7-----|
E|-0-0-0-5-3--0-0-0-5-3--0-0-0-5-3----------------|

riff3 (2X)
                                            D5   E5
E|------------------------------------------------|
B|------------------------------------------------|
G|-------------------------------------7-7---9-9--|
D|-------------------------------------7-7---9-9--|
A|-------------------------------------5-5---7-7--|
E|-0-0-0-5-3b--0-0-0-5-3b--0-0-0-5-3b-------------|


riff4 
E|---------------------------|
B|---------------------------|
G|---------------------------|
D|---------------------------|
A|-7-5-----------------------|
E|------7-5-3-2-0------------|

Em
I was running down the road
Am                   G
The fear was all i knew
C
I was looking for a soul that's real
Am               B
Then i ran into you
Em
And that cherry blossom tree
Am                   G
Was a gateway to the sun
C
And friendship, once it's won
Am             B
It's won, it's one

RIFF1 (6X)
C5     E5      B5          C5          C5
Northside just across the river to the southside
E5                C5  E5
That's a long way here
C5      E5     B5         C5
All the green and all the gold
C5       E5            B5       E5
The hurt you hide, the joy you hold
C5          E5               B5         C5
The foolish pride that gets you out the door
C5                      B5           E5
Up on cedarwood road, on cedarwood road

riff2 (2X)
riff3 (2X)
riff4

Em
Sleepwalking down the road
Am                       G
Not waking from these dreams
C
'cause it's never dead it's still my head
Am                       B
It was a warzone in my teens
Em
I'm still standing on that street
Am               G
Still need an enemy
C
The worst ones i can't see
Am           B
You can, you can


RIFF1 (6X)
C5      E5     B5         C5           C5
Northside just across the river to the southside
E5                C5  E5
That's a long way here
C5      E5     B5         C5
All the green and all the gold
C5           E5        B5        E5
The hurt you hide, the joy you hold
C5          E5               B5          C5
The foolish pride that gets you out the door
C5                        B5          E5
Up on cedarwood road, on cedarwood road

riff2 (2X)
riff3 (2X)


SOLO: (DICA MONTAR ACORDE)
E|---------------------------------------------------------
B|---------------------------------------------------------
G|-9-9-9-9-9-9-9-9-9---11--12---14---16--16/17/16/17/16----
D|-X-X-X-X-X-X-X-X-X---X---X----X----X---X-/X-/X-/X-/X-----
A|-7-7-7-7-7-7-7-7-7---9---10---12---14--14/15/14/15/14----
E|---------------------------------------------------------

E|--------------------------------------------------------------------------------------------
B|--------------------------------------------------------------------------------------------
G|-9-9-9-9-9-9----7-7-7---4-4-4---12--9---7-7-7---9-9-9---11--12---14---16--16/17/16/17/16----
D|-X-X-X-X-X-X----X-X-X---X-X-X---X---X---X-X-X---X-X-X---X---X----X----X---X-/X-/X-/X-/X-----
A|-7-7-7-7-7-7----5-5-5---2-2-2---10--7---5-5-5---7-7-7---9---10---12---14--14/15/14/15/14----
E|--------------------------------------------------------------------------------------------


C5      E5             B5    C5
If the door is open it isn't theft
C5                  E5            B5   E5
You can't return to where you've never left
C5               E5               B5            C5
Blossoms falling from a tree they cover you and cover me
C5                E5
Symbols clashing, bibles smashing
B5                   E5
Paint the world you need to see
C5        E5               B5                         C5
Sometimes fear is the only place that we can call our home
B5            E5
Cedarwood road

riff2 (2X)
riff3 (2X)
riff4

A heart that is broken

Is a heart that is open

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
B5 = X 2 4 4 X X
C = X 3 2 0 1 0
C5 = X 3 5 5 X X
D5 = X 5 7 7 X X
E5 = 0 2 2 X X X
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
