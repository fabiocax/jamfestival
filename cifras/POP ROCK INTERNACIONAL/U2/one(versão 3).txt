U2 - One

Intro:. Gm   C9   D#   F

Gm               C9
Is it getting better
D#               F
Or do you feel the same
Gm                C9
Will it make it easier on you, Now..
D#                 F
You got someone to blame.  You say...
A#        Gm
One love, one life
D#             A#
When it's one need in the night
             Gm
It's one love we get to share it
D#                  A#
It leaves you baby if you don't care for it

Gm   C9   D#   F


Gm               C9
Did I disappoint you
D#                      F
Or leave a bad taste in your mouth
Gm                    C9
You act like you never had love
D#                        F
And you want me to go without. Well it's...
A#        Gm
too late tonight
D#                         A#
To drag the past out into the light
            Gm
We're one, but we're not the same
D#                     A#
We get to carry each other, carry each other

Base: Gm  C9  D#  F (2x)

Gm                        C9
Have you come here for forgiveness?
D#                         F
Have you come to raise the dead?
Gm                        C9
Have you come here to play Jesus?
D#                     F
To the lepers in your head?

A#                 Gm
Did I ask too much, more than a lot
D#                            A#
You gave me nothing, now it's all I got
                  Gm
We're one, but we're not the same
D#                         A#
We hurt each other then we do it again

You say
A#                Gm
Love is a temple, Love a higher law
A#                Gm
Love is a temple, Love the higher law
     A#                    F
You ask me to enter, but then you make me crawl
                         D#
And I can't be holding on to what you got
                     A#
When all you got is hurt

A#         Gm
One love, one blood
D#                   A#
One life, you got to do what you should
        Gm
One life with each other
D#       A#
Sisters, brothers

A#                Gm
One life, but we're not the same. We get to...
D#               A#
carry each other, carry each other

Gm      D#     A#
One……….
Gm      D#     A#
One…….

----------------- Acordes -----------------
A# = X 1 3 3 3 1
C9 = X 3 5 5 3 3
D# = X 6 5 3 4 3
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
