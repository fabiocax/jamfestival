U2 - Kite (Live)

Afinação Meio Tom Abaixo

Intro: E A (repeat)

lead gtr (w. slide):
------
-14--
-14--
------
------
------
VERSE:
E A9 E A9
Something is about to give
E A9 E A9
I can feel it coming I think I know what it is
E A9 E A9
I'm not afraid to die I'm not afraid to live
E A9 E A9
And when I'm flat on my back I hope to feel like I did
(enter 2 slide guitar tracks):
-------------------------
--5~--4-5-4-5-4~---------
-------------------------
-------------------------
-------------------------
-------------------------

(repeat)
A
And hardness It sets in
C#m A
You need some protection the thinner the skin
E A C#m A
I want you to know that you don't need me anymore
E A C#m
I want you to know You don't need anyone
A
Or anything at all
CHORUS:
E5
Who's to say where the wind will take you
B5
Who's to say what it is will break you
A5
I don't know which way the wind will blow
E5
Who's to know when the time has come around
B5 A5
Don't want to see you cry
E5
I know that this is not goodbye

(E5 A5)4xs

(E5 A5)
In summer I can taste the salt in the sea
There's a kite blowing out of control on a breeze
I wonder what's gonna happen to you
You wonder what has happened to me

Solo:(A C#m)Slide solo

      (E5 B5 A5)
I'm a man..., I'm not a child
A man...who sees...the shadow behind your eyes

CHORUS:
E5
Who's to say where the wind will take you
B5
Who's to say what it is will break you
A5
I don't know which way the wind will blow
E5
Who's to know when the time has come around
B5 A5
Don't want to see you cry
E5
I know that this is not goodbye

(E5 A5)
Goodbye...goodbye...goodbye...goodbye

(E5 A5)
Did I waste it?
Not so much I couldn't taste it
Life should be fragrant
Roof top to the basement
The last of the rock stars
When hip hop drove the big cars
In the time when new media
Was the big idea
That was the big idea
Comunication,comunication

Termina com E5

Obs:Depois que termina o Bono canta sozinho com o violão

(E5 A5)
Did I waste it?
Not so much I couldn't taste it
Life should be fragrant
Roof top to the basement
The last of the rock stars
When hip hop drove the big cars
In the time when new media
Was the big idea
That was the big idea

Lead guitar chords on above chorus:
E5 B5 A5
------0---7--------------
--9---7---5---------(0)--
--9---8---6-(8)-(6)------
--9---9---7--------------
--7-------0--------------
--0----------------------
(play with variations of these)

slide solo in second verse:
E|-7/9--9---1211--117--75--5-
B|---------------------------

E|-7/9--9---1211--117--74--4-
B|---------------------------

E|-2/4--42--0--0--
B|-----------------

----------------- Acordes -----------------
A*  = X 0 2 2 2 0 - (*G# na forma de A)
A5*  = X 0 2 2 X X - (*G#5 na forma de A5)
A9*  = X 0 2 2 0 0 - (*G#9 na forma de A9)
B5*  = X 2 4 4 X X - (*A#5 na forma de B5)
C#m*  = X 4 6 6 5 4 - (*Cm na forma de C#m)
E*  = 0 2 2 1 0 0 - (*D# na forma de E)
E5*  = 0 2 2 X X X - (*D#5 na forma de E5)
