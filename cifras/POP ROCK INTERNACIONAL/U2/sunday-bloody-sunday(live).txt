U2 - Sunday Bloody Sunday (Live at Red Rocks 1983)

Intro:  A#m  C#  F#


E|---------------------------|          
B|----6----6-----4----4------|          
G|---6-6--6-6---6-6--6-6-----|    
D|--8----6-----4----4--------|          
A|---------------------------|          
E|---------------------------|          


A#m         C#               F#
 I can't believe the news today
A#m         C#               F#
 I can't close my eyes and make it go away

C#          D#m
 How long, how long must we sing this song?
C#              D#m
 How long, how long?


  A#5  C#5  F#5                   A#5 C#5 F#5
Tonight we can be as one, tonight

A#5       C#5             F#5
 Broken bottles under children's feet
A#m           C#             F#
 And body strewn across a dead-end street
A#m            C#               F#
 But I won't heed the battle call
A#m           C#                F#
 It puts my back up, puts my back up against the wall

A#m   C#          F#
 Sunday, bloody Sunday (2x)

(Intro)

Sunday, bloody Sunday (Sunday, bloody Sunday)

(RIFF B) - Que não coloquei (ajudem ae, pf)

( A#5  C#5  F#5 ) (4x)

A#5        C#5             F#5
 And the battles just begun
A#m             C#                 F#
 There's many lost but tell me: who has won?
A#m             C#              F#
 The trenches dug within our hearts
A#m              C#                  F#
 And mother's children, brothers, sisters torn apart

A#m   C#          F#
 Sunday, bloody Sunday  (2x)

C#         D#m
 How long, how long must we sing this song?
C#              D#m
 How long, how long?

  A#5  C#5  F#5                   A#5 C#5 F#5
Tonight we can be as one, tonight

                A#m  C#  F#
Wipe your tears away    (5x)

A#m   C#           F#
 Sunday, bloody Sunday (4x)

Sunday, bloody Sunday (Sunday, bloody Sunday)

(RIFF B)

( A#5  C#5  F#5 ) (4x)

A#5          C#5            F#5
 And it is true we are immune
A#5          C#5            F#5
 When fact is fiction and TV reality
A#5          C#5            F#5
 And today the millions cry
A#5          C#5            F#5
 We eat and drink while they die
A#5          C#5            F#5
 The real battle just begun
A#5          C#5            F#5
 To claim the victory Jesus won
A#5        C#5         F#5
On a Sunday, bloody Sunday
A#5   C#5        F#5
 Sunday, bloody Sunday

----------------- Acordes -----------------
A#5 = X 1 3 3 X X
A#m = X 1 3 3 2 1
C# = X 4 6 6 6 4
C#5 = X 4 6 6 X X
D#m = X X 1 3 4 2
F# = 2 4 4 3 2 2
F#5 = 2 4 4 X X X
