U2 - With Or Without You

G       D                Em
See the stone set in your eyes,
       C                  G
See the thorn twist in your side
  D       Em  C
I wait for you

 G         D              Em
Sleight of hand and twist of fate
            C                G
On a bed of nails she makes me wait
       D            Em   C
And I wait....without you


REFRAIN
           G         D
  With or without you
           Em        C
  With or without you


 G         D                 Em
Through the storm we reach the shore
           C              G
You give it all but I want more
       D       Em      C
And I'm waiting for you


REFRAIN
          G          D
  With or without you
          Em         C
  With or without you  ah ha
         G    D           Em         C
  I can't live... with or without you

        G            D
And you give yourself away
        Em            C
And you give yourself away
        G
And you give
        D
And you give
        Em           C
And you give yourself away


G            D
My hands are tied
Em        C               G
My body bruised she got me with
       D
Nothing left to win
    Em              C
and nothing left to loose

        G           D
And you give yourself away
       Em            C
And you give yourself away
        G
And you give
        D
And you give
        Em          C
And you give yourself away


REFRAIN
          G          D
  With or without you
          Em         C
  With or without you  ah ha
         G    D           Em         C
  I can't live... with or without you

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
