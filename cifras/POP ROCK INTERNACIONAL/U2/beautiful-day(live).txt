U2 - Beautiful Day (Live)

Intro: A Bm D G D A

A Bm D G D A

Riff1 Intro/verse 
B|------<7>-------------------------------------
G|----------<7>-----------------<7>-------------
D|--------------<7>---------<7>-----------------


A Bm D G D A
A Bm D G D A
The heart is a bloom__ Shoots_ up_ through the stony ground_
A Bm D G D A
There's no room__ No_ space_ to rent in this town__

Riff2 Verse lead guitar
e|----------------------
B|-2---3---(3)----------
G|-----------------4----
D|----------------------
A|----------------------
E|----------------------


A Bm D G D A (&Riff2)
You're out of luck_ And the reason that you had to care_
A Bm D G D A
The traffic is stuck_ And you're not mo_ving anywhere_
A Bm D G D A
You thought you'd found__ a friend_ To take you out_ of_ this_ place_
(aaaaawwwaaaayyyy)
A Bm D G D A
Someone you__ could_ lend__ a hand In return__ for__ grace__
(aaaaawwwaaaayyyy)
[Chorus1] (distortion)

A5 B5 D5 G5 D5 A5
It's a beautiful day___ Sky__ falls__, you feel_ like
A5 B5 D5 G5 D5 A5
It's a beautiful day Don't let_ it__ get_ away__

A Bm D G D A (&Riff2)
You're on the road__ But you've got__ no_ destination_
A Bm D G D A
You're in the mud__ In the maze of her_ imagina_tion_
A Bm D G D A
You love this town_ Even if that_ doesn't ring true_
A Bm D G D A
You've been all_____ over_ And it's been__ all__ over_ you__

[Chorus2] (distortion)
A5 B5 D5 G5 D5 A5
It's a beautiful day Don't let_ it__ get_ away__
A5 B5 D5 G5 D5 A5
It's a beautiful day Oooo____oooo_ooohh

[Bridge1]Riff5
F#m G D A
Touch___ me__ Take me to that other_ place
F#m G D A
Teach___ me__ I know I'm not a hopeless case


Riff3 Bridge lead
e|-9-----9-10----10--10--10----1010-9-10-9--9--9--9---
B|----12----------------------------------12-12-12-12-
G|-------------12--12--12--12-------------------------
D|----------------------------------------------------
A|----------------------------------------------------
E|----------------------------------------------------

A Bm D G D A
Em D
See the world__ in green and blue__ See China right in front of you__
See the canyons broken by cloud__ See the tuna fleets clearing the sea out
Em G D
See the Bedouin fires at night__ See the oil fields at first light and
Em G D
See the bird with a leaf in her mouth after the flood all the colors came out
[nc]
(ddddddaaaaaaaaaaaaaaayyyyyyyyyyy) (ddddddaaaaaaaaaaaaaaayyyyyyyyyyy)
aaaaaaaaa aaaaaaaaaaaaaah take___ me____ out____

[Chorus3] (distortion)
A5 B5 D5 G5 D5 A5
It was a beautiful_ day__ Don't let_ it__ get_ away__
A5 B5 D5 G5 D5 A5
Beautiful day____aa__aa__aaay__ aaa_aaaahaaaa__aaaahh

[Bridge2]riff5
F#m G D A
Touch___ me__ Take me to that other_ place
F#m G D A Rea_ea_ea_eaach__ me__ I know I'm not a hopeless case

[Outro]
A Bm D G
What_ you_ don't_ have_ you_ don't__ need it now__
D A
What_ you_ don't_ know_ you can feel__ it somehow___
A Bm D G
What_ you_ don't_ have_ you_ don't__ need it now__
D A
Don't_ need_ it_ now___
[Chorus4] (distortion)
A5 B5 D5 G5 D5 A5 (&Riff3)
Was a beautiful_ day_________
A5 B5 D5 G5 D5 A5 (&Riff3)
Goal!!! Goal!!! Beautiful Goal Goal!!! Beautiful Goal!!!

Riff4
Riff4 End
E|----------------------------------------------3---
B|--------------------------------------------3-----
G|---6--7--11--4---11---6-----6--7--11--4---4-------
D|---7--9--12--5---12---7-----7--9--12--5-5---------
A|--------------------------------------------------
E|---5--7--10--3---10---5-----5--7--10--3-----------

Beautiful...Goal...

---------------------------------------------
riff5

B|-----14----14----14----12---14----15---15---15-----
G|-----14----14----14----14---14----14---12---12-----


B|-----15---15---15---15---15----15---14---15--------
G|-----12---12---12---14---14----14---14---14--------


B|-----14----12---14---12---14---12----14----12---14-
G|-----14----14---14---14---14---14----14----14---14-

----------------- Acordes -----------------
A = X 0 2 2 2 0
A5 = X 0 2 2 X X
B5 = X 2 4 4 X X
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D5 = X 5 7 7 X X
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G5 = 3 5 5 X X X
