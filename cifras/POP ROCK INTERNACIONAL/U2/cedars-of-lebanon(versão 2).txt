U2 - Cedars Of Lebanon

INTRO: D#m A#m

D#m                A#m
Yesterday I spent asleep,
D#m                               A#m
woke up in my clothes in a dirty heap
D#m                             A#m
Spent the night trying to make a deadline
D#m                               A#m
Squeezing complicated lives into a simple headline
D#m                         A#m
I have your face in an old polaroid
D#m                     A#m
Tidying the children's clothes and toys
D#m                                A#m
You're smiling back at me, I took the photo from the fridge
D#m               A#m
Can't remember what then we did

D#m                             A#m
I haven't been with a woman it feels, like, for years
D#m                             A#m
Thought of you the whole time, your salty tears
D#m                    A#m
This shitty world sometimes produces a rose
D#m                      A#m
The scent of it lingers but then it just goes

D#m         C#       B7M     D#m C# A#m
Return the call to home

D#m                    A#m
The worst of us are a long, drawn out confession
D#m                          A#m
The best of us are geniuses of compression
D#m                                 A#m
You say you're not gonna leave the truth alone
D#m                            A#m
I'm here 'cause I don't wanna go home

D#m                              A#m
Child drinking dirty water from the riverbank
D#m                         A#m
Soldier brings oranges, he got out from a tank
D#m                          A#m
Waiting on the waiter, he's taking a while to come
D#m                       A#m
Watching the sun go down in Lebanon

D#m         C#       B7M     D#m C# A#m
Return the call to home

solo:D#m A#m

D#m                          A#m
Now I got a head like a lit cigarette
D#m                         A#m
Unholy clouds reflecting a minaret
D#m                       A#m
You're so high above me, higher than everyone
D#m                          A#m
Where are you in the Cedars of Lebanon?

SOLO:
E|-----------------------------|
B|-----------------------------|
G|-----6-8--------------6-4-6--|
D|---8-----8~-4-6-8-6-4--------|
A|-6---------------------------|
E|-----------------------------|


D#m                             A#m
Choose your enemies carefully 'cause they will define you
D#m                      A#m
Make them interesting, 'cause in some ways they will mind you
D#m                                 A#m
They're not there in the beginning but when your story ends
D#m                                   A#m
Gonna last with you longer than your friends

----------------- Acordes -----------------
A#m*  = X 1 3 3 2 1 - (*Am na forma de A#m)
B7M*  = X 2 4 3 4 2 - (*A#7M na forma de B7M)
C#*  = X 4 6 6 6 4 - (*C na forma de C#)
D#m*  = X X 1 3 4 2 - (*Dm na forma de D#m)
