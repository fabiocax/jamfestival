U2 - Sometimes You Can't Make it on Your Own

(Intro)
A:- A:- G:- G:- F#m - D:- A:- A

    A           A           G/A         G/A         F#/A        Dsus2

e|-------------------------------------------------------------------------
B|-----2-2-3-2-----2-2-3-2-----2-2-3-2-----2-2-3-2-----2-2-3-2-----2-2-3-2-
G|-----2-2-2-2-----2-2-2-2-----2-2-2-2-----2-2-2-2-----2-2-2-2-----2-2-2-2-
D|--------------------------------------------------------------0----------
A|--0-----------0----------------------------------------------------------
E|--------------------------3-----------3-----------2----------------------


A     G
Tough, you think you've got the stuff
F#m                   D
You're telling me and anyone
       A
You're hard enough

    A
You don't have to put up a fight

    G                             F#m
You don't have to always be right
                        D
Let me take some of the punches
    A
For you tonight


B|---10-9----7-9----10------7---7----------10-9---7-9--10-------------------
G|---------------------9------9---9-----------------------9-----------------

D            A
Listen to me now
                  F#m
I need to let you know
                          D
You don't have to go it alone

Refrão

B|--12h14-12h14-12h14-12h14-12h14--

         F#m                    E
And it's you when I look in the mirror
         D
And it's you when I don't pick up the phone
F#m                 E               D
Sometimes you can't make it on your own


e|---12-9--------12-9--------12-9-----------12-9-----------------------
B|---10-10-------10-10-------10-10-------12-10-10----------------------
G|---------11-9--------11-9---------11-9----------11-9-----------------
   A             G
We fight all the time
        F#m         D
You and I... that's alright
A
We're the same soul

e|---12-9----------------------12-9-----------12-9--------------------------
B|---10-10-------10--9--7--5---10-10-------12-10-10-------------------------
G|---------11-9-----------------------11-9----------11-9--------------------
        A               G
I don't need... I don't need to hear you say
      ]F#m              D
That if we weren't so alike
             A
You'd like me a whole lot more


B|---10-9----7-9----10------7---7----------10-9---7-9--10-------------------
G|---------------------9------9---9-----------------------9-----------------

D            A
Listen to me now
                  F#m
I need to let you know
                          D
You don't have to go it alone


Refrão

B|--12h14-12h14-12h14-12h14-12h14--

         F#m                    E
And it's you when I look in the mirror
         D
And it's you when I don't pick up the phone
F#m                 E               D
Sometimes you can't make it on your own

Solo:
G|--10/9/7/5/2/7---10/9/7/5-0-2---10/9/7/5/2/7-/9-/10-/12-/14-/16-/18-

(Just inside)
(F - C - Dm)
I know that we don't talk
(F - C - Am)
I'm sick of it all
(F - C - Dm)                 A
Can, you, hear, me, when, I, sing

G|-18b-16-18bp16h18--18--16--18--16-----18--16--18--16----------------
D|-----------------------------------19----------------19-------------

       F#m
You're the reason I sing
D                 E      A
You're the reason why the opera is in me

e|--17-16---14---16-17------14-----14-----
B|---------------------17------17-----17--
G|----------------------------------------  2x

     D
Well hey now
                 A
Still got to let you know
                             F#m
A house still doesn't make a home
                      D
Don't leave me here alone

         F#m                    E
And it's you when I look in the mirror
         D
And it's you that makes it hard to let go
F#m                 E               D
Sometimes you can't make it on your own
F#m                 E
Sometimes you can't make it
            D
Best you can do is to fake it
F#m                 E               D
Sometimes you can't make it on your own

A:- A:- A:- A

e|---12-12-9--------12-12-9--------12-12-9--------12-12-9-------12-12-9---------------
B|---10-10-10-------10-10-10-------10-10-10-------10-10-10------10-10-10--------------

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
Dsus2 = X X 0 2 3 0
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
F#/A = X 0 4 3 2 X
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G/A = 5 X 5 4 3 X
