U2 - With Or Without You

[Intro]  D A Bm G
         D A Bm G

D           A               Bm
  See the stone set in your eyes
            G                  D
  See the thorn twist in your side
     A        Bm  G
  I wait for you

 D            A                 Bm
  Sleight of hand and twist of fate
               G                  D
  On a bed of nails she makes me wait
         A          Bm      G
  And I wait....without you

              D     A
  With or without you
              Bm    G
  With or without you


D               A                  Bm
  Through the storm we reach the shore
              G               D
  You give it all but I want more
           A           Bm     G
  And I'm waiting for you

              D     A
  With or without you
              Bm    G
  With or without you
           D    A             Bm    G
  I can't live... with or without you

( D A Bm G )

           D              A
  And you give yourself away
           Bm             G
  And you give yourself away
           D
  And you give
           A
  And you give
           Bm             G
  And you give yourself away

D               A
  My hands are tied
Bm          G                 D
  My body bruised she got me with
              A
  Nothing to win
       Bm              G
  and nothing left to loose

           D            A
  And you give yourself away
           Bm           G
  And you give yourself away
           D
  And you give
           A
  And you give
           Bm           G
  And you give yourself away

              D     A
  With or without you
              Bm    G
  With or without you
           D    A             Bm    G
  I can't live... with or without you
           D     A
  With or without you
              Bm    G
  With or without you

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
G = 3 2 0 0 0 3
