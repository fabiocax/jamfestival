Adão Negro - Amassa o barro

INTRO : G D Em D (2X)


G D Em D
Procurei o fruto da canção
G D Em D
Procurei o sangue e o suor
G D Em D
Mas não há sinal na construção
G D Em D
Onde quem colhe nunca semeou

C
Colocar o trabalho
D C
Em minha mão (em minha mão)
D
Quero ver seu rosto e a sua voz


G D Em D

Amassarei o barro com amor (se for pra construir)
G D Em D
Amassarei o barro com amor

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
