Adão Negro - Chuva Ou Sol

(intro) D A Em A

D
Ela fica bem com chuva ou sol
Em
descomplicada ela sempre se sai bem
D
simplicidade seguranca e know how
    Em
e na balada rouba a cena na moral

D
Sofisticação e elegância natural
Em
sensibilidade ela nunca sai do tom
D
vai bem na beca ou na praia do luau
Em
menina leve me carregue sangue bom
F#m                  Em
Oh na na Que brilho próprio estrelar!

F#m             G       A
Oh na na Sabe vencer na boa..

D
Olha ela lá
(ê! ê!)
Em             A
Toda minha jah
(ê! ê!)
D
Eu vou ser seu par
(ê! ê!)
Em              A
Eu vou me jogar
(ê! ê!) (2x)

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
