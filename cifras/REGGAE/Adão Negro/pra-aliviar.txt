Adão Negro - Pra Aliviar

INTRO: G F C G (2X)


G F
Que vida dura que paisagem bela
C G
Vivo sempre na janela a me perguntar
F
Se toco reggae ou se peço chuva
C G
Oh meu Deus ajuda meu povo de lá
F
A muito tempo que eu tô na estrada
C G
Só sinto a saudade a me apertar
F
Saudades tenho de Dona Maria,
C G
Bastiana, Ana, saudades Naná (mas só o reggae)



F C G
Mas só o reggae faz relaxar
F C G
Mas só o reggae pra aliviar
(Marley Gonzaga pra aliviar)


G F
É tudo rápido tudo passageiro
C
A onda é dinheiro
G
A moda é estar
F
É tanto medo é tanto absurdo
C
Vivo em dois mundos

G
Pode acreditar
F
Eu vejo tudo mas não vejo nada
C G
Eu não me acostumo então vou cantar

F
De um lado a paisagem bela
C G
Do outro a favela outro a lamentar (mas só o reggae)

G
Só o reggae

----------------- Acordes -----------------
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
