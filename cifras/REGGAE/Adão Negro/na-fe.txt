Adão Negro - Na Fé

Riff Inacial:
Parte 1
E|----------7-------|-----------------|
B|---7-9-10--10-9-7-|-----------------|
G|-9----------------|---6-7-8-9-7-6-9-|
D|------------------|-9---------------|
A|------------------|-----------------|
E|------------------|-----------------|

Parte 2
E|----------7-------|-------------------|
B|---7-9-10--10-9-7-|-------------------|
G|-9----------------|---6-7-8-9-7-6~7---|
D|------------------|-9---------------7-|
A|------------------|-------------------|
E|------------------|-------------------|

A                                   Bm
Vou levando a vida assim, mas sem desanimar
             A               Bm
(Na Fé my brother! Na Fé my brother!)

A                                        Bm
Eu vou jogando o jogo assim, mas sei que vou ganhar
            A               Bm
(Na Fé my brother! Na Fé my brother!)
A                                Bm
Saio do que é ruim pra sorte me ajudar
             A              Bm
(Na Fé my brother! Na Fé my brother!)
A                                       Bm
a fé no bem que tenho em mim, que nunca vai faltar
             A                Bm
(Na Fé my brother! Na Fé my brother!)
 D                       E
Sou filho de Deus, meu escudo minha redenção.
                C#m                               Bm
é quem me dá o toque pra essa vida não me dar rasteira não
D                   E
O mundo é meu bate forte o meu coração.
                 C#m
fazer da vida um sonho não é facil
                     D
e é melhor não dar bobeira não
        E
e a caminha não tem fim.

Riff Final:
Parte 1
E|-14-12------------|------------------------------|
B|-------10-14-12---|---12-15-15-15-15-15-14-12-14-|
G|----------------9-|-9----------------------------|
D|------------------|------------------------------|
A|------------------|------------------------------|
E|------------------|------------------------------|

Parte 2
E|-14-12------------|------------------------------|
B|-------10-14-12---|---12-15-15-15-15-15-14-12-10-|
G|----------------9-|-9----------------------------|
D|------------------|------------------------------|
A|------------------|------------------------------|
E|------------------|------------------------------|

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
