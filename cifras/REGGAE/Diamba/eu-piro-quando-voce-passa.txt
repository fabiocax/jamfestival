Diamba - Eu Piro Quando Você Passa

A          Bm
La laia laiala
    E                 A
Eu piro quando você passa
             Bm
Seu olhar seduz
              A
Coração dispara
               Bm
Faz mais de um mês
                          A
Sonho contigo toda madrugada
                   Bm
Se eu tivesse o mundo
                  A
Meu bem pra ti dava
                   Bm
Mas só tenho um amor
                       A
Casa,comida e roupa lavada
                     Bm
Quando ouvi essa canção

                C#m
Canto ela pra você
                      Bm
Que é só seu meu coração
                      A
Ja cansei de te esconder
                     Bm
Quando ouvi essa canção
                C#m
Canto ela pra você
                               Bm
Canção que eu fiz só pra lhe dizer
         E                 A
Que eu piro quando você passa

          Bm
La laia laiala
    E                 A
Eu piro quando você passa

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
