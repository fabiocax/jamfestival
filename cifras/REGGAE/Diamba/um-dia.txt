Diamba - Um Dia


Am                               Dm
A quanto eu vou me dar meu bem
Am                       Dm
O prazer de olhar ou rever
Am                          Dm
O quanto vou ter amor
Am             Dm
O quanto vou sofrer
Am       Dm
Uma ilusão
Am            Dm
Uma aparição
Am
Um dia
Am                  Dm
Agora você já não
                                Am Dm
Tem tempo pra sonhar
 Am     Dm Am Dm
No que vai viver

Am        Dm
Agora você
                           Am Dm
Vai ter que falar
Am    Dm    Am Em
Agora vai me dizer tudo
Am
Tudo o que eu quero
Dm
Ouvir você cantar
Am
Tudo que eu quero
               Dm
Ver você falar
Am Em F7+ Bb7
Pra me conquistar
Am Em F7+
Pra me consolar
Am              Dm
Um dia você vai
               Am
Querer me ter
               Dm
E vai ter
Am              Dm
Um dia você vai
                        Am Dm
Querer saber
                        Am Dm
Um dia
                        Am Dm
Um dia

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb7 = X 1 3 1 3 1
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F7+ = 1 X 2 2 1 X
