Diamba - Miscigenação

Dm                               Gm
Cachaça, mandinga, caruru, pacto
É a companhia de quem é zulu
Dm
Nunvem de fumaça
                  Gm
Mágica pra quem viu
                               Dm
O índio e a sua contribuição
                                        Gm
E ainda tem gente falando em disse me disse
                                       Dm
Que essa vertente é a culpa miscigenação

Cadê o sangue bom?
Cadê o sangue bom?
Gm
Corria dessas costas após tantas chibatadas
Dm
Cadê o sangue bom?
Cadê o sangue bom?

Gm
A começar com os índios e suas flechadas

Meu Deus, quem foi que criou?

Dm Gm
Quem foi que criou verdade abominável
Que o índio é preguiçoso e o negro miserável
Meu Deus, quem foi que criou?
E não importa a sua cor
Meu Deus, quem foi que criou?
E não importa a sua raça

----------------- Acordes -----------------
Dm = X X 0 2 3 1
Gm = 3 5 5 3 3 3
