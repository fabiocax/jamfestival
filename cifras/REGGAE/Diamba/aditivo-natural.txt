Diamba - Aditivo Natural

Intro 4x: G D

G                        D
Eu vim só pra me divertir
G                        D
E o reggae ta rolando ae
C                  D
O aditivo é natural
C                        D
Água de coco e por do sol, ééé

G                       D
A princesinha a deslizar
G                       D
De sandalinha de arrastar
C                       D
Ela arrastou meu coração
C                       D
Me chamando pra ter noção do que é bom

G                       D
A beira mar dançar um reggae com você

G                       D
Pra celebrar a vida que é oii meu Deus do céu
G                       D                       G
É o que me dar, te dar total condição pra festejar
              D
Que esse é o verão da nossa vida

2X


Solo: G D

G                        D
Eu vim só pra me divertir
G                        D
E o reggae ta rolando ae
C                  D
O aditivo é natural
C                        D
Água de coco e por do sol, ééé

G                       D
A princesinha a deslizar
G                       D
De sandalinha de arrastar
C                       D
Ela arrastou meu coração
C                       D
Me chamando pra ter noção do que é bom

G                       D
A beira mar dançar um reggae com você
G                       D
Pra celebrar a vida que é oii meu Deus do céu
G                       D                       G
É o que me dar, te dar total condição pra festejar
              D
Que esse é o verão da nossa vida

G                       D
A beira mar dançar um reggae com você
G                       D
Pra celebrar a vida que é oii meu Deus do céu
G                       D                       G
É o que me dar, te dar total condição pra festejar
              D
Que esse é o verão da nossa vida.

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
