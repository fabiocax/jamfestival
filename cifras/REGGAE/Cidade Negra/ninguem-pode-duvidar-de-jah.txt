Cidade Negra - Ninguém Pode Duvidar de Jah

Intro 2x:   E  G#m  C#m  B  A    B

 E                          G#m
Se eu fosse um milionário  te levaria pro espaço
C#m
Então lá eu cuidaria
        A      B
do teu coração
 E
Se eu tivesse uma outra chance
         G#m
O pedido que eu faria
     C#m                A             B
Que JAH unisse a mim  a você à harmonia
 G#m                     A
Ninguém pode duvidar
 G#m                     A
Ninguém pode duvidar de JAH
 F#m                                B
Eu não vou fugir  desaparecer  tenho algo a te dizer


Refrão:
 G          D             Em   Bm            C
Se o amor então te procurar  abra suas portas
             Am      D
Para deixá-lo entrar
 G          D             Em
Se o amor então te procurar
 Bm          C         Am   D
Esteja livre pra receber

Segunda parte:
 E
Eu e o meu tapete voador
 G#m
Te levando até à lua
 C#m                      B
Sem limites do espaço Sideral

 E
Uma ilha no Atlântico
 G#m
Com um oceano  uma lagoa azul
 C#m                             B
E pensar que é tão simples ser feliz

 G#m                     A
Ninguém pode duvidar
 G#m                     A
Ninguém pode duvidar de JAH
 F#m                             B
Eu não vou fugir  desaparecer  tenho algo a te dizer

Refrão:
 G           D            Em   Bm            C
Se o amor então te procurar  abra suas portas
             Am      D
Para deixá-lo entrar
 G           D            Em
Se o amor então te procurar
 Bm         C           Am   D
Esteja livre pra receber

Base final:  G  D  Em  Bm  C    D   2X

Repete refrão:
Diminuindo volume
 G           D            Em   Bm            C
Se o amor então te procurar  abra suas portas
             Am      D
Para deixá-lo entrar
 G           D            Em
Se o amor então te procurar
 Bm         C           Am   D
Esteja livre pra receber

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G#m = 4 6 6 4 4 4
