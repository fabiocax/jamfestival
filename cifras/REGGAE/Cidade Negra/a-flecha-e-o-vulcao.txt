Cidade Negra - A Flecha E O Vulcão

Intro: G D7 G D7 G D7 G D7 G D7 G D7 G D7 G D7 G

                     D7   G
Se me chamar eu vou agora
                       D7     G
Se me chamar eu vou correndo
                         D    G
Se é pra voar no mundo afora
G              D7
Eu vou, Eu vou

G                     D7  G
Se me chamar eu vou agora
                       D7    G
Se me chamar eu vou correndo
                         D   G  D7  G
Se é pra voar no mundo afora

             D7
Vou que nem Flecha
                G
Vou que nem vulcão

         D7                     G
Vou furacão que foi lançado ao mar
         D7
Algum talento
        G
Tua Bondade
        D7                 G
O divindade vem mostrar o céu


 A             Bm            D
Quero ir com você pra Negril
               A
E floripa nao pode esperar
             Bm             D
Vou fazer filhote no brasil
    A   E7
Lavar a alma
        A     G D7 G D7 G D7 ...
Lavar a alma

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
G = 3 2 0 0 0 3
