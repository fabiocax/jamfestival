Cidade Negra - Além das Ondas

(Da Gama - Bernado Vilhena)

Metais -> Bb Cm Dm Cm

Além, Além, Além das ondas
Além, Além
Além, Além, Além das ondas
Além, Além, Além

Bb Cm Bb Cm
Bb Cm Dm Cm

Além das ondas eu vou viajar
No mar bravio impossível parar
Só Deus sabe o quanto eu planejei
Estou tranqüilo como eu quero ser
O mundo é bom, também me faz viver

Bb Cm Dm Cm

Nas ondas, mar alto  2x

Tem uma pedra no meio do oceano

METAIS Bb Cm Dm Cm

Bb Cm Dm Cm

Além, Além, Além das ondas
Além, Além
Além, Além, Além das ondas
Além, Além, Além

Bb Cm Bb Cm
Bb Cm Dm Cm

Além das ondas eu vou viajar
No mar bravio não consigo parar
Só Deus sabe o quanto eu planejei
Estou tranqüilo como eu quero ser
O mundo é bom, também me faz viver
(pára a bateria rapidamente)

Bb Cm Dm Cm

Nas ondas, mar alto  2x
Tem uma pedra no meio do oceano

Dm/F Cm

Eu sei que céu igual a esse não existe,
Não, não existe
Na verdade eu sei, eu sei que a gente sempre exige novas sensações

SOLO: Bb Cm Bb Cm
        Bb Cm Dm Cm
SOLO:
E||-----------------------------------------------------------------------------------------||
A||--------------------------------------------------4-3------3/4-6------6/8-6-4-3-4-3------||
D||-------------------------------------------3--2/3-----3-3--------3-3----------------5-3--||
G||---------------------------------------3/5-----------------------------------------------||
B||-1-------5-3---6-5-6-8--6p5h6-5-3-1------------------------------------------------------||
E||-----3h6---------------------------------------------------------------------------------||


sem bateria 1 x: Bb Cm Dm Cm

Nas ondas, mar alto  2x
Tem uma pedra no meio do oceano

Metais ao fim: Bb Cm Dm Cm

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
Dm = X X 0 2 3 1
Dm/F = X X 3 2 3 1
