Cidade Negra - A Cor do Sol

Base: Em C Am B7

Introdução
e|-----------------------------------------------------------|
B|--------------------------------------2--------------------|
G|------------2-4--------------2-4--------5-4-2--------------|
D|------2-4-5------5-----2-4-5------5-----------5-4-2-1--/9--|
A|----2----------------2-------------------------------------|
E|-0---------------------------------------------------------|

e|------------7----------------7------10-8-7-----------------|
B|-----7-8-10----8------7-8-10----8----------10-8-7------/12-|
G|---9----------------9-----------------------------9-8------|
D|-9----------------9----------------------------------------|
A|-----------------------------------------------------------|
E|-----------------------------------------------------------|


 Em
  Amor
    C

  Estamos atravessando
      Am
  O milênio
                       B7        Em
  O novo tempo está chegando, ê yeah

           C
  A cor do sol está brilhando
          Am
  E anuncia
                 B7
  Um novo dia de folia
    Em
  A inocência, o respeito
    C
  A alegria de estar sério
    Am
  O bom humor e sentimento
  B7
  Nós vamos levar
 Em
  Deixar pra trás maus pensamentos
  C
  Desequilíbrios, as amarguras
     Am
  Viver em paz todo o momento
     B7
  Dentro de casa, no meio da rua
 Em
  Se você acha impossível
      C
  Ter tudo isso e um amor tranquilo
      Am
  Que tal tentar só um pouquinho?
    B7
  Experimente ver no que dá

  Em
  E vê se dá!
  Experimente
  Pra ver se dá
  C
  E vê se dá!
  Experimente
  Pra ver se dá
  Am
  Viva sempre
       B7
  Aproveite o momento


  Em
  E vê se dá!
  Experimente
  Pra ver se dá
  C
  E vê se dá!
  Experimente
  Pra ver se dá
  Am
  Viva sempre
       B7
  Aproveite o momento


  Introdução


  Em
  1001 lendas do novo milênio
Am
  de um novo milenio...
 C
  de um novo milenio...
B7
  de um novo milenio...


  Em
  1001 lendas do novo milênio
Am
  de um novo milenio...
 C
  de um novo milenio...
B7
  de um novo milenio...


 Solo: (2x)

e|-----------------------------------------------------------------------------------|
B|-12-10-12-10h12p10--8-10-8h10p8--7-8-7h8p7--5-7-5h7p5--3-5-3h5p3--1h3p1--0-1-0h1p0-|
G|-----------------------------------------------------------------------------------|
D|-----------------------------------------------------------------------------------|
A|-----------------------------------------------------------------------------------|
E|-----------------------------------------------------------------------------------|

e|-----------------------------------------------------------------------------------|
B|-----------------------------------------------------------------------------------|
G|-0--2-2-0-2--0------2-2-0-2--0------2--4-4-2--4-4-2--4-4-2-4-----------------------|
D|---------------2-2-------------2-2-------------------------------------------------|
A|-----------------------------------------------------------------------------------|
E|-----------------------------------------------------------------------------------|

----------------- Acordes -----------------
