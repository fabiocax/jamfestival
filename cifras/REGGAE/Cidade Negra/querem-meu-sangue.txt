Cidade Negra - Querem Meu Sangue

[Intro] C  G  F  G

          G
Dizem que guardam um bom
lugar pra mim no céu
F
logo que eu for pro beleléu
         G
A minha vida só eu sei como guiar
        F
Pois ninguém vai me ouvir se eu chamar
      Em7
Mas enquanto o sol puder arder
            Am
Não vou querer meus olhos escurecer

              G
Pois se eles querem meu sangue
  F                     C
Verão o meu sangue só no fim
           G
E se eles querem meu corpo

    F                        C
Só se eu estiver morto, só assim

         G
Meus inimigos tentam sempre me ver mal
          F
Mas minha força é como o fogo do Sol
             G
Pois quando pensam que eu já estou vencido
          F
É que meu ódio não conhece perigo
       Em7
Mas enquanto o sol quiser brilhar
          Am
Eu vou querer a minha chance de olhar

         G
Eu vou lutar pra ter as coisas que eu desejo
           F
Não sei do medo, amor pra mim não tem preço
            G
Serei mais livre quando não for mais que osso
         F
Do que vivendo com a corda no pescoço
            Em7
Enquanto o sol no céu estiver
         Am
Só vou fechar meus olhos quando quiser

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
