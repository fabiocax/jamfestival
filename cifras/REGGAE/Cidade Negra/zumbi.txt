Cidade Negra - Zumbi

Tom - Bb
Introdução - (Bb Eb)


  Bb
Angola Congo Benguela
                Eb
Monjolo Capinda Nina
         F
Quiloa Rebolo
Eb                    Bb
   Aqui onde estão os homens
Eb       F         Bb
   Há um grande leilão
Eb           F
   Dizem que há
               Bb
Uma princesa à venda
Eb           F
   Que veio junto com seus
     Bb
     súditos

Eb
   Acorrentados num carro
        Bb
     de boi
    Eb
Eu quero ver
    Eb
Eu quero ver
    Eb       Bb Eb F
Eu quero ver
  Bb
Angola Benguela
                Eb
Monjolo Capinda Nina
         F
Quiloa Rebolo
Eb                    Bb
   Aqui onde estão os homens
Eb            F         Bb
   De um lado cana de açucar
Eb          F        Bb
   Do outro lado cafezal
Eb              F        Bb
   Ao centro senhores sentados
Eb            F
   Vendo a colheita do
             Bb
     algodão branco
Eb         F
   Sendo colhido por mãos
     Bb
     negras
   Eb
Eu quero ver
   Eb
Eu quero ver
   Eb
Eu quero ver
   Eb
Eu quero ver
Eb           F      Bb
   Quando Zumbi chegar
Eb           F      Bb
   O que vai acontecer
                   Eb
Zumbi é senhor das guerras
    F           Bb
É senhor das demandas
                Eb
   Quando Zumbi chega
        F
     é Zumbi
        Bb
É quem manda
   Eb
Eu quero ver
   Eb
Eu quero ver
   Eb
Eu quero ver
   Eb
Eu quero ver
  Eb   Cm    Eb    Bb
Angola Con...go Benguela
    Cm  Eb
Monjo...lo
  Bb    Cm   Eb
Capinda Nina
   Bb     Cm    Eb
Quiloa Rebolo
    ( Bb Eb )
Eu quero
Angola
Benguela
Capinda
Quiloa

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
