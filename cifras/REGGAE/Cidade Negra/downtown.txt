Cidade Negra - Downtown

Tom - A

Introdução - A Bm E

   A
Eu fui...
                  E
Pro outro lado de lá
   A
Eu fui...
             E
Brasil, Jamaica
         A
Sou brasileiro, sou muito feliz
         Bm
Ouvi seu nome muito além
              E
        de Paris
  A
Saímos pra tocar, fomos pro
        lado de lá

 Bm                     E
América, Europa, de que tanto
        ouvi falar
  A
Ouvi muitas histórias, "Mundo
        novo é aqui"
Bm
Terra das maravilhas,
              E
        Disneylândia, Mickey Mouse
A
Tocar com Ziggy Marley em
        Miami foi legal
Bm
Todas as potências do reggae
        mundial, então
   A
Eu fui...
                  E
Pro outro lado de lá
   A
Eu fui...
             E
Brasil, Jamaica
        A
E Jimmy Cliff na baixada
        uma hora nos falou
Bm
Que na Jamaica em Montego Bay
A
  Numa favela ele viu nascer um rei
Bm
Sua mensagem que muito
        nos marcou
        A
"Pois o reggae quando bate você
        Nunca sente dor"
 Bm                     E
Assim dizia o rei que o mundo encantou
 A
Assim dizia o rei que ao mundo provou
Bm              E
Que a liberdade e a sapiência
A
  A sapiência...
Bm                      E
São peças chaves pra maior inteligência
   A
Eu fui...
                   E
Pro outro lado de lá
   A
Eu fui...
             E
Brasil, Jamaica
A
Um grande sonho a se realizar
Bm                    E
Reggae, Brasil, estou louco pra tocar
        A
Shabba Ranks, Pato Banton, Burning spear
        Bm
Wailing souls, Big Youth,
            E
        Cocotea
      A
Sinto muito mas eu tenho que
                  Bm          E
        partir... muito... partir...
   A
Na baixada uma hora nos falou
Bm
Que na Jamaica em Montego Bay
A
  Numa favela ele viu nascer um rei
Bm
Sua mensagem que muito
        nos marcou
        A
"Pois o reggae quando bate você
        Nunca sente dor"
 Bm                     E
Assim dizia o rei que o mundo encantou
 A
Assim dizia o rei que ao mundo provou
Bm              E
Que a liberdade e a sapiência
A
  A sapiência...
Bm                      E
São peças chaves pra maior inteligência
   A
Eu fui...
                  E
Pro outro lado de lá
   A
Eu fui...
             E
Brasil, Jamaica
A
Um grande sonho a se realizar
Bm                    E
Reggae, Brasil, estou louco pra tocar
        A
Shabba Ranks, Pato Banton, Burning spear
        Bm
Wailing souls, Big Youth,
            E
        Cocotea
      A
Sinto muito mas eu tenho que
        partir...
   A
Eu fui...
                  E
Pro outro lado de lá
   A
Eu fui...
             E
Brasil, Jamaica

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
E = 0 2 2 1 0 0
