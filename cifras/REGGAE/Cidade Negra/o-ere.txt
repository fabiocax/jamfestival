Cidade Negra - O Erê

Intro:

Primeiro Violão

x----4s5--7s9--7--5------------------------x {(2)x}
x-------------------5s7--7-7---7s9--9-9----x
x------------------------------------------x
x------------------------------------------x
x------------------------------------------x
x------------------------------------------x

Segundo Violão


x---------12--10--9---9s10--10s12----------x {(2)x}
x---9s10-----------------------------------x
x------------------------------------------x
x------------------------------------------x
x------------------------------------------x
x------------------------------------------x


A         F#m   D
Pra entender o Ere
E            A     F#m
Tem que tá moleque,
D    E
o  Erê, Erê
A         F#m          D
Tem que conquistar alguém
    E             A
E a consciência leve,
F#m   D  E
Aaa...

A                 F#m     D   E
Há semanas em que tudo vem
A                 F#m    D   C#m
Há semanas que é seca pura  ooo
A                F#m
Ha selvagens que são do bem
D      E
ê hum  ê hum
A              F#m       D   C#m
A sequência do filme muda

Bm7
Milhões de anos luz podem curar
C#m7
O que alguns segundos na vida podem representar
Bm7
O Erê a criança sincera convicção
C#m7
Fazendo a vida com o que o sol nos traz

      A
Você sabe
      F#m              D
Um sentimento não trai
          E
Um bom sentimento não trai
      A
Você sabe
      F#m              D
Um sentimento não trai
          E
Um bom sentimento não trai

A   F#m   D  E
  U u u...u
A   F#m    D   E
  U u u...u u u u u
A   F#m   D  E
  U u u...u
A   F#m    D   E
  U u u...u u u u u

A         F#m   D
Pra entender o Ere
E            A     F#m
Tem que tá moleque,
D    E
o  Erê, Erê
A         F#m          D
Tem que conquistar alguém
    E             A   F#m   D  E
E a consciência leve

A                   F#m       D  E
Para e pense no que já se viu
A                   F#m      D      C#m
Pense e sinta o que já se fez,  biribirum
A                 F#m      D  E
O mundo visto de uma janela
A               F#m      D   C#m
Pelos olhos de uma criança

Bm7
Milhões de anos luz podem curar
C#m7
O que alguns segundos na vida podem representar
Bm7
O Erê a criança sincera convicção
C#m7
Fazendo a vida com o que o sol nos traz

      A
Você sabe
      F#m              D
Um sentimento não trai
          E
Um bom sentimento não trai
      A
Você sabe
      F#m              D
Um sentimento não trai
          E
Um bom sentimento não trai

( A  F#m  D  E )
Você sabe
( A  F#m  D  E )
Você sabe
( A  F#m  D  E )
Você sabe
( A  F#m  D  E )
Você sabe
( A  F#m  D  E )
Você sabe

[Solo]

E|-4/5-7/9-5/7-4/5--------------------|
B|-----------------5/7-7-7--7/9-9-9---|
G|------------------------------------|
D|------------------------------------|
A|------------------------------------|
E|------------------------------------|

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm7 = X 2 4 2 3 2
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
