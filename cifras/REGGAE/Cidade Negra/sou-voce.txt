Cidade Negra - Sou Você

Intro: D7+(9)  Bm  G7+  Em  D7+(9)  G/A

    D7+(9)                  Bm
    Mar sob o céu, cidade da luz, mundo meu canção que eu
      G7+         Em                 D7+(9)  G/A
    compus, mudou tudo que agora é você
    D7+(9)                     Bm
    A minha voz que era da amplidão, do universo da
        G7+           Em           Bm
    multidão, hoje canta só por você
            C#m7/5-    C7+        F7+       Bb7+
    Minha mulher, meu amor, meu lugar, antes de você
       Gm7     Bb7+     D7+(9)
    chegar era tudo saudade.
    C#m7/5-    C7+     F7+        Bb7+           Gm7
    Meu canto mudo no ar, faz do seu nome hoje o ceu da
      A7
    cidade.
    D7+(9)                   Bm
    Lua no mar , estrelas no chão
                              G7+
    A seus pés, entre as suas mãos

         Em             Bm   G/A
    Tudo quer alcançar você
    D7+(9)                   Bm
    Levanta o sol do meu coração
                               G7+
    Já não vivo , nem morro em vão
        Em                    Bm
    sou mais eu por que sou você
           C#m7/5-    C7+        F7+       Bb7+
    Minha mulher, meu amor, meu lugar, antes de você
       Gm7     Bb7+     D7+(9)
    chegar era tudo saudade.
    C#m7/5-    C7+     F7+        Bb7+           Gm7
    Meu canto mudo no ar, faz do seu nome hoje o ceu da
      A7
    cidade.
    D7+(9)                   Bm
    Lua no mar , estrelas no chão
                              G7+
    A seus pés, entre as suas mãos
         Em             Bm   G/A
    Tudo quer alcançar você
    D7+(9)                   Bm
    Levanta o sol do meu coração
                               G7+
    Já não vivo , nem morro em vão
        Em                    Bm
    sou mais eu por que sou você

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bb7+ = X 1 3 2 3 1
Bm = X 2 4 4 3 2
C#m7/5- = X 4 5 4 5 X
C7+ = X 3 2 0 0 X
D7+(9) = X 5 4 6 5 X
Em = 0 2 2 0 0 0
F7+ = 1 X 2 2 1 X
G/A = 5 X 5 4 3 X
G7+ = 3 X 4 4 3 X
Gm7 = 3 X 3 3 3 X
