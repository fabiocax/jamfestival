Natiruts - O Fundo do Mar

Intro: ( G#m  F# ) Na,Na,Na,Na,Na,Na,Na...(4X)

   Refrão: ( G#m   C#m   F#7   G#m   D#7 )
             O fundo do mar com você foi demais
             Só não sei como vou cuidar......

    Verso : ( G#m   C#m   F#7   G#m   D#7 )
    Vou procurar amuletos de fé que ajudem
    A deixar num asteróide distante todo nosso medo do amor
    Mas se eu não conseguir não tem problema
    Existem mil lugares secretos no seu pensamento
    Talvez na pedra do sol ou na estrela cadente
    Esteja a força para que os homens
    curem a fraqueza de não saber
    Que se o instante da felicidade não chega
    Falta o amor da tecnologia com a natureza...

    Repete Refrão 2X O Fundo do Mar.....
    Repete Verso 1X Vou Procurar.....
    Repete Intro Na,Na,Na,Na,Na,Na,Na.....


OBS.:
    No Intro são 4 Tempos cada nota na batida Reggae
    No Verso e no Refrão (G#m C#m F#m) são 4 Tempos
    cada nota , e G#m e D#7 são 2 Tempos
    Toque Com Pestana essas notas como toda Musica de Reggae
    Notas: D#7=668686 as outras notas vocêis sabem...
    Esta versão dessa musica esta certinha...
    Corrigi ela nos mínimos detalhes...
    Viva O Reggae Nacional...

----------------- Acordes -----------------
C#m = X 4 6 6 5 4
D#7 = X 6 5 6 4 X
F# = 2 4 4 3 2 2
F#7 = 2 4 2 3 2 2
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
