﻿Natiruts - Bob Falou

E|---------------------------------------------------------------------------|
B|---3---h---5---h---8---10---10---8------3---h---5---h---8------5h3---------| 
G|-----------------------------------------------------------------------5h3-| 
D|---------------------------------------------------------------------------|
A|---------------------------------------------------------------------------|
E|---------------------------------------------------------------------------|

(Refrão 2X)
G         C
Bob cantou : Take it easy
G         C
Bob falou : caia no reggae, não faz mal

Bm          Em              C
Hoje o sol mostrou toda beleza
Bm                       C
Clareando o dia levantando o astral
Bm               Em          C
De tanta luz, surgiu uma idéia então

Bm                 C
Celebrar o reggae e a vida

REFRÃO

Bm                 Em         C
A lua nasceu com brilho de princesa
Bm                 Em         C
Transmitindo a luz que afasta todo mal
Bm                 Em           C
De tanta luz levou ao pensamento bom
 Bm                  C
Celebrar o reggae e a vida

REFRÃO
 Bm                 Em        C
O mestre falou : respeite a natureza
 Bm               Em               C
E devolva à ela, o bem que ela nos traz
Bm                    Em       C
Pense em você e nos outros que ainda virão
Bm                    C
E darão o reggae e a vida

REFRÃO

INTRO

----------------- Acordes -----------------
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
