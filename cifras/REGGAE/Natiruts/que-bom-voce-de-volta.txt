Natiruts - Que Bom Você de Volta

Dm7(9)                 Fm7               Gm7
Que bom você de volta, senti a sua falta aqui
         Dm7(9)                      G7+              G
Na fantasia dos meus pensamentos, na riqueza dos meus sentimentos
   C7+                  A7
E tudo o que não posso alcançar
  Dm7(9)         G6     C7+                  A7
E quando você me tocar, certamente irá perceber
Dm7                             G#7+       G7 C7+
Muito mais forte do que imagina quanto eu amo você

Gm7                         Dm7(9)
Teus olhos, teu jeito e teu sorriso
Gm7                        Dm7(9)
Teu beijo e tudo o que eu preciso
                Bb7+              A7     Dm7(9)
E tudo o que eu ver que nos faça viver o amor

Gm7                         Dm7(9)
Teus olhos, teu jeito e teu sorriso
Gm7                        Dm7(9)
Teu beijo e tudo o que eu preciso

                Bb7+              A7
E tudo o que eu ver que nos faça viver o amor

Dm7(9)                 Fm7               Gm7
Que bom você de volta, senti a sua falta aqui
         Dm7(9)                      G7+              G
Na fantasia dos meus pensamentos, na riqueza dos meus sentimentos
   C7+                  A7
E tudo o que não posso alcançar
  Dm7(9)         G6     C7+                  A7
E quando você me tocar, certamente irá perceber
Dm7                             G#7+       G7 C7+
Muito mais forte do que imagina quanto eu amo você

Gm7                         Dm7(9)
Teus olhos, teu jeito e teu sorriso
Gm7                        Dm7(9)
Teu beijo e tudo o que eu preciso
                Bb7+              A7     Dm7(9)
E tudo o que eu ver que nos faça viver o amor

Gm7                         Dm7(9)
Teus olhos, teu jeito e teu sorriso
Gm7                        Dm7(9)
Teu beijo e tudo o que eu preciso
                Bb7+              A7
E tudo o que eu ver que nos faça viver o amor
Dm7(9)
Que bom você de volta

[Solo] Gm7  Am7  Dm7(9)

Gm7                         Dm7(9)
Teus olhos, teu jeito e teu sorriso
Gm7                        Dm7(9)
Teu beijo e tudo o que eu preciso
                Bb7+              A7     Dm7(9)
E tudo o que eu ver que nos faça viver o amor

Gm7                         Dm7(9)
Teus olhos, teu jeito e teu sorriso
Gm7                        Dm7(9)
Teu beijo e tudo o que eu preciso
                Bb7+              A7
E tudo o que eu ver que nos faça viver
Dm7(9)                 Fm7               Gm7
Que bom você de volta, senti a sua falta aqui
Dm7(9)                 Fm7               Gm7
Que bom você de volta, senti a sua falta aqui
Dm7(9)                 Fm7               Gm7
Que bom você de volta, senti a sua falta aqui

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am7 = X 0 2 0 1 0
Bb7+ = X 1 3 2 3 1
C7+ = X 3 2 0 0 X
Dm7 = X 5 7 5 6 5
Dm7(9) = X 5 3 5 5 X
Fm7 = 1 X 1 1 1 X
G = 3 2 0 0 0 3
G#7+ = 4 X 5 5 4 X
G6 = 3 X 2 4 3 X
G7 = 3 5 3 4 3 3
G7+ = 3 X 4 4 3 X
Gm7 = 3 X 3 3 3 X
