Natiruts - Palmares 1999

Intro - (D7M  C#m7  Bm7  C#m7  D7M  C#m7  E11)  2x

F#m                  C#m7
  A cultura e o folclore são meus
Bm7                    E
  Mas os livros foi você quem escreveu
F#m                     C#m7
  Quem garante que palmares se entregou
Bm7                    E
  Quem garante que Zumbi você matou
F#m                  C#m7
  Perseguidos sem direitos nem escolas
Bm7                  E
  Como podiam registrar as suas glórias
F#m                     C#m7
  Nossa memória foi contada por vocês
Bm7                 E
  E é julgada verdadeira como a própria lei
F#m                    C#m7
  Por isso temos registrados em toda história
Bm7           E
  Uma mísera parte de nossas vitórias

F#m                   C#m7
  É por isso que não temos sopa na colher
Bm7                   E                        F#m             D7M - C#m7 - Bm7     E
  E sim anjinhos pra dizer que o lado mal é o candomblé
F#m                  D7M - C#m7
  A energia vem do coração
Bm7                   E
  E a alma não se entrega não
F#m                  D7M - C#m7
  A energia vem do coração
Bm7                  E
  E a alma não se entrega não

F#m       C#m7  Bm7             E
  A influência dos homens bons deixou a todos ver
F#m            C#m7
  Que omissão total ou não
Bm7                      E
  Deixa os seus valores longe de você
F#m                C#m7
  Então despreza a flor zulu
Bm7                   E
  Sonha em ser pop na zona sul
F#m               C#m7
  Por favor não entenda assim
Bm7                         E
  Procure o seu valor ou será o seu fim
D7M                   C#m7                      Bm7
  Por isso corre pelo mundo sem jamais se encontrar
D7M                    C#m7                     Bm7
  Procura as vias do passado no espelho mas não vê
D7M                 C#m7
  E apesar de ter criado o toque do agogô
Bm7                  E                         F#m  D7M  Bm7  E
  Fica de fora dos cordões do carnaval de salvador

F#m                 D7M - C#m7
  A energia vem do coração
Bm7                   E
  E a alma não se entrega não
F#m                D7M - C#m7
  A energia vem do coração
Bm7                   E
  E a alma não se entrega não


-----Tablatura para Baixo----


Pré-Intro:
G|---------------------------------------------------------
D|---------------------------------------------------------
A|-5-5--4-4--2---2-4-5-5-4-4-2---5-5-4-4-2--2-4-5-5-4-4----
E|------------------------------------------------------0--

Intro (junto com o solo de guitarra com wah-wah):
G|---------------------------------------------------------
D|--------------------------------------2-4----------------
A|-----------------0-0-0-0---2-0-2--2-0--------------------
E|-2-0-2--2-0-2-0---------------------------0-0-0-0--------

G|---------------------------------------------------------
D|--------------------------------------2-4----------------
A|-----------------0-0-0-0---2-0-2--2-0--------------------
E|-2-0-2--2-0-2-0---------------------------0-0-1----------

Verso (parte principal da música):
G|----------2-------------------4--------------------------
D|--------2-------2---4-----2-4----------------------------
A|----2-4-------4---4-----2--------------------------------
E|--2-------------------------------0-0-1------------------

  quem escreveu"

Pré-refrão:
  "E sim anjinhos pra dizer que o lado mau é o candom..."
G|---------------------------------------------------------
D|---------------------------------------------------------
A|-------------5-5-5-4-4-4-2-2---2---2---------------------
E|-2-2-0-2-0-2-----------------2---2---0--1----------------


Refrão ("A energia vem do coração"):
G|----------2---------------------4------------------------
D|--------2-------------------2-4--------------------------
A|----2-4------5-5-5-4-4-4--2------------------------------
E|--2--------------------------------0-0-1-----------------


G|----------2-------------------4--------------------------
D|--------2-------2---4-----2-4----------------------------
A|----2-4-------4---4-----2--------------------------------
E|--2-------------------------------0-0-1------------------


Outro 1:
G|---------------------------------------------------------
D|---------------------------------------------------------
A|-5-5--5-----5-5--4-4--4-----4-4--2--2---0-2-0-2-0-2-2-4--
E|--------5-5-------------4-4-----------2------------------


Outro 2:
G|---------------------------------------------------------
D|---------------------------------------------------------
A|-5-5--5-----5-5--4-4--4-----4-4--2-2--2---2-2------------
E|--------5-5-------------4-4-------------2------0---------

  dos cordões do carnaval de Salvador"

Finalização:
G|----------2-------------------4--------------------------
D|--------2-----------------2-4----------------------------
A|----2-4-----------------2--------------------------------
E|--2------------------------------------------------------


--

  [Pré-Intro]
  [Intro]
  [Verso]
A cultura e o folclore são meus
Mas os livros foi você quem escreveu
Quem garante que Palmares se entregou
Quem garante que Zumbi você matou
Perseguidos sem direitos nem escolas
Como podiam registrar as suas glórias
Nossa memória foi contada por você
E é julgada verdadeira como a própria lei
Por isso temos registrados em toda história
Uma mísera parte de nossas vitórias
É por isso que não temos sopa na colher
E sim anjinhos pra dizer que o lado mau é o candomblé
  [Pré-Refrão]

  [Refrão]
A energia vem do coração    |
E a alma não se entrega não | 2x
A energia vem do coração    |
E a alma não se entrega não |

  [Intro]

  [Verso]
A influência dos homens bons deixou a todos ver
Que a omissão total ou não
Deixa os seus valores longe de você
Então despreza a flor zulu
Sonha em ser pop na zona sul
Por favor não entenda assim
Procure o seu valor ou será o seu fim
  [Outro 1]
Por isso corres pelo mundo sem jamais se encontrar
Procura as vias do passado no espelho mas não vê
  [Outro 2]
Que apesar de ter criado o toque do agogô
Fica de fora dos cordões do carnaval de Salvador
  [Pré-Refrão]

  [Refrão]
A energia vem do coração    |
E a alma não se entrega não | 2x
A energia vem do coração    |
E a alma não se entrega não |

  [Intro]
  [Finalização]


E11 = Pestana na sétima casa

Tablatura feita por: LuGu

----------------- Acordes -----------------
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
D7M = X X 0 2 2 2
E = 0 2 2 1 0 0
E11 = 0 2 2 2 0 0
F#m = 2 4 4 2 2 2
