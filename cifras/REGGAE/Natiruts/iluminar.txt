﻿Natiruts - Iluminar

Introdução: Am7 Dm Am7 Dm.

Am7                                 Dm
A qualquer hora o mundo pode confundir sua cabeça...
É preciso ter amor para enfrentar o que aconteça...
Se é pressão que impede de ter a cabeça boa...
É preciso ter coragem e aprender a voar...
Am7                               Dm          Am7        Dm
Porque os donos da cidade não tem pena de ninguém
Am7                          Dm        Am7       Dm
Os donos da cidade eles so querem aparecer, he he
Am7                          Dm
Penso cuida do seu coração
Am7                           Dm
Não se engane tanta ilusão
Am7                                 Dm
Temos tantas coisas pra viver

Em                       E7
Cantar a riqueza do nosso amor
Am7         Dm
Iluminar
Am7         Dm
Iluminar, é tempo de
Am7         Dm
Iluminar, que é pra ver
Em               E7
a riqueza do nosso amor
Am7          Dm
Iluminar, ande e vem me
Am7          Dm
Iluminar, deixa eu te
Am7         Dm
Iluminar, que é pra ver
Em               E7
a beleza do nosso amor

Baixo

E|---------------
A|5-5--------5-5-   Riff#1
D|----7-7-3-3----

E|5-5--------5-5-
A|----7-7-3-3----   Riff#2
D|---------------

E|5-
A|--  Riff#3
D|--

          Riff#1     Riff#2
Oh meu bem, meu amor
                Riff#1          Riff#2
Nao vou desistir, se nada mudou
                    Riff#1           Riff#3
Se nem um amor tiver eu vou dizer não

                                         Am7    Dm
"Cê" tava com doença que da em burgues à toa
Am7                   Dm                 Am7     Dm
Cegando o sol da America do Sul
Am7                                       Dm      Am7   Dm
Com crise de indentidade escrava da sorte
Am7                    Dm            Am7
Tipo São Paulo quando quer ser Nova York, he he


Am7                          Dm
Pense e cuida do seu coração
Am7                           Dm
Nao se engane tanta ilusão
Am7                                 Dm
Temos tantas coisas pra viver
Em7                   E7
Cantar a riqueza do nosso amor
Am7         Dm
Iluminar
Am7         Dm
Iluminar, é tempo de
Am7         Dm
Iluminar, que é pra ver
Em            E7
a beleza do nosso amor
Am7          Dm
Iluminar, ande e vem me
Am7          Dm
Iluminar, deixa eu te
Am7          Dm
Iluminar, que é pra ver
Em              E7
a beleza do nosso amor

Am7          Dm
          Riff#1    Riff#2
Oh meu bem, meu amor
Am7          Dm
               Riff#1           Riff#2
Nao fique assim, nem tudo acabou
Am7          Dm
                    Riff#1                  Riff#2
Se nem um amor tiver, eu vou dizer que não é
Am7          Dm
                       Riff#1                Riff#2
Eu vou é cantar pro sol, e vou desejar com fé
Am7          Dm
                      Riff#1                Riff#2
E vou enfeitar de flor, o mundo que eu puder
Am7          Dm
Uuuuuuuuu,uuuuuuuuu,uuuuu,uuuu,uuuu,uuuuuuu...

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
