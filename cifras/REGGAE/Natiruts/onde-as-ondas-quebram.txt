Natiruts - Onde As Ondas Quebram

G     D      C
Nada quer se encaixar
     G        D     Em7
E o céu nunca está azul
G       D      C
Não consigo encontrar
G                 D   Em7
Caminhos não tenho nenhum
Am7                Bm7
Me perdi dos seus olhos
Am7                    G
A saudade é o que vou levar
      Am7             Bm7
Vou partir pra muito longe
     Am7                    D
Meu mundo é onde eu quero estar

G        D        C
Saiba que eu te quero
        G             D    C
A hora certa é a que você vier

Am7                    Bm7
Não ligue pro que vão dizer
Am7              Bm7
O tempo está a correr
     Am7            Bm7             C   D
E a vida não espera quem não quer viver

G        D      C
Onde as ondas quebram existe
    G              D      C
Um céu e um sonho pra nos dois
Am7             Bm7
O tempo está a correr
Am7                   Bm7
Não liga pro que vão dizer
     Am7           Bm7                C   D
Se o amor é um desejo, que se quer viver

G        D       C
Onde as ondas quebram existe
    G              D       C
Um céu e um sonho pra nos dois
Am7                   Bm7
Escuta o que eu vou dizer
Am7             Bm7
O certo é eu e você
C            D                 G
Te amo e te espero aqui no meu lugar

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
