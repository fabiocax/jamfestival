Natiruts - Proteja-se e Lute

 Db            Gb
Proteja-se  e lute!
 E             Gb
Proteja-se e lute!
   Db             Bb7
Eu quero poder andar
    Ebm
Ser feliz e estar contigo
    Db              Bb7
E ser livre pra pensar
  Ebm                 Ab
Decidir que a mente fique
  Gb       Fm    Ebm
Ou vá, ou vá, ou vá ...
  Gb       Fm    Ebm    Ab
Ou vá, ou vá, ou vá ...
Gb                      Fm
Não deixe que o ódio escravize
               Ebm
As nossas crianças
Gb                           Fm
Que a hipocrisia não tenha valor

                Ebm
Assim estaremos vivos
Gb                              Fm
Pare e reflita no que ouvir, sentir
       Ebm
Nunca aceite sem pensar
Gb                             Fm
Todo pensamento bom tem seu valor
               Ebm      Ab7
O valor de ser livre

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Ab7 = 4 6 4 5 4 4
Bb7 = X 1 3 1 3 1
Db = X 4 6 6 6 4
E = 0 2 2 1 0 0
Ebm = X X 1 3 4 2
Fm = 1 3 3 1 1 1
Gb = 2 4 4 3 2 2
