Natiruts - Deriram

[Intro] F#m7  C#m7  F#m7

F#m7           C#m7
Deriram, deriram, deriram
D7m             C#m7
Pra que todo esse medo
D7m                C#m7
Pra que dar tanta importância para a solidão
D7m            C#m7
Acredite em seus desejos
Bm7           E            Fº
Feche os olhos e escute o coração

F#m7           C#m7
Deriram, deriram, deriram
D7m           C#m7
Todo mundo tem defeitos
D7m               C#m7
A felicidade existe na imperfeição
D7m            C#m7
Acredite em seus desejos

Bm7           E           Fº            F#m7
Feche os olhos e escute o coração, baby

  C#m7     D7m
Me sinto leve
  Bm7         E       F#m7     E   F#m7
A força que me rege é a luz do sol
C#m7       D7m
     Ouvindo reggae
Bm7       C#m7         F#m7     E   F#m7
Com o pensamento à frente, o sentimento bom

F#m7           C#m7
Deriram, deriram, deriram
D7m           C#m7
Todo mundo tem defeitos
D7m               C#m7
A felicidade existe na imperfeição
D7m             C#m7
Faça as coisas do seu jeito
Bm7           E               Fº     F#m7
Feche os olhos e escute o coração, baby

 C#m7   D7m
Me sinto leve
  Bm7         E       F#m7       E F#m7
A força que me rege é a luz do sol
C#m7       D7m
     Ouvindo reggae
Bm7       C#m7        F#m7     E    F#m7
Com o pensamento à frente, o sentimento bom

F#m7 C#m7
Reggae
F#m7 C#m7
Reggae
F#m7 C#m7
Reggae

----------------- Acordes -----------------
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
E = 0 2 2 1 0 0
F#m7 = 2 X 2 2 2 X
Fº = X X 3 4 3 4
