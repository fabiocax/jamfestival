Edu Ribeiro e Cativeiro - Sendo Assim

Intro: Em  Bm

       Em                Bm
SÓ O AMOR PODE ATRAVESSAR O MAR
          Em               Bm
SOBRE VALES E SERTÕES EO TEU CORPO ALCANÇAR
          Em                 Bm
PASSA PELO VENDAVAL, FORTE FEITO UM FURACÃO
       Em             Bm
IGNORA O TEMPORAL E CHEGA AO TEU CORAÇÃO

Em    Bm      2X

      Em           Bm
PODE PERDURAR SEMPRE QUE PERCISO FOR
          Em            Bm
GRANDES ROCHAS AVISTAR PARA ENTÃO SE SOBREPOR
         Em            Bm
SE ACASO TE ALCANÇAR,  SÓ ESPERE... NADA MAIS
            Em                     Bm
SE O MEU BEIJO ENCONTRA O SEU, ESTAREMOS IMORTAIS


C7+                   Bm            Em
POIS QUANDO ESSE AMOR TE ENCONTRA, NÃO HÁ ONDE IR
             Bm
BASTA FICAR  E DEIXAR SENTIR

  G                 Bm
E SENDO ASSIM OS CORAÇÕES SERÃO REAIS                   2X

Em      Bm

----------------- Acordes -----------------
Bm = X 2 4 4 3 2
C7+ = X 3 2 0 0 X
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
