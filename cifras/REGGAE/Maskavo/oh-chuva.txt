Maskavo - Oh Chuva

Bbm                   Fm
Você que tem medo de chuva
Bbm                Fm
você não é nem de papel
        F#                C#
muito menos feito de açúcar
    F#                F
ou algo parecido com mel
Bbm                        Fm
Experimente tomar banho de chuva
Bbm                   Fm
e conhecer a energia do céu
      F#                C#
a energia dessa água sagrada
F#                          F
Que nos abençoa da cabeça aos pés
Bbm
Chuva
    F#        F        Bbm
eu peço que caia devagar
     F#          F        Bbm
só molhe esse povo de alegria, alegria

F#        Ab        Bbm
para nunca mais chorar
Repete tudo denovo.

Bbm                     F#        F        Bbm

Chuva
    F#        F        Bbm
eu peço que caia devagar
     F#          F        Bbm
só molhe esse povo de alegria, alegria
F#        F        Bbm
para nunca mais chorar
Bbm

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Bbm = X 1 3 3 2 1
C# = X 4 6 6 6 4
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
Fm = 1 3 3 1 1 1
