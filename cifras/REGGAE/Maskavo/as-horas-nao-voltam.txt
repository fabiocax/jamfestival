Maskavo - As Horas Não Voltam

Intro: A  C#m  F#m  D

     A               C#m
Não quero mais           pensar em nada
F#m                          D
        só ver a tarde deslizando
   A            C#m
O sol é meu         melhor amigo
F#m                        D
       deixando a alma elevada
   A             C#m
Final do dia         você chega
F#m                          D
       é quando a brisa se anima
   A         C#m
Você será       sempre a primeira
F#m                        D
       iliminando a minha vida

   A          F#M         Bm
As horas não voltam mais

                  D
O teu abraço é o meu lugar
     A          F#m         Bm
Pra ter toda a paz
                 D
A paz de todo o dia

      A          C#m
Não quero mais         pensar em nada
F#m                              D
        deixar um pouco dos problemas
  A       C#m
Ficar um tempo do seu lado
F#m                      D
       imaginando novos planos
    A     C#m
Sentir na pele o seu gosto
F#m                       D
       fotografias na lembrança
   A     C#m
A noite vem tomar o posto
F#m                        D
       o seu abraço é segurança

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
F#m = 2 4 4 2 2 2
