Maskavo - Dade Dade

Intro: (G C G C) 2x

G              Bm             C
    Não é novidade eu amar você
          Cm               G
Possibilidade de poder te ter
             Bm               C
Eu tentei de tudo pra te esquecer
        Cm
Mas prevaleceu o amor
G                    C  G C G C
    O meu amor por você

Bm           C                       Em
    A velha cama e  o vazio do meu quarto
   Am           Em
Enfrentam o desabafo
   Am               Em          Am
De mil canções que fiz pra você
  Bm        C               Em
Decido censurar meus sentimentos

    Am              Em
Vou deletando os momentos
     Am              Em            Am
Do amor que eu fiz questão de esquecer
   Bm           C          Bm
Tirei o nosso disco da vitrola
   C                  Bm
Passei pra frente essa bola
         C    Am           D
Mas o futuro é  Deus quem vai saber

   Bm         C            Em
A vida segue sem você por perto
    Am              Em
Em meus caminhos incertos
   Am              Em        Am
A outros braços me entreguei
  Bm        C         Em
O telefone toca de repente
     Am                 Em
Lembrei do frio que se sente
  Am          Em           Am
Quando se pressente alguém
  Bm        C           Bm
E era meu amor com novidades
   C                 Bm
Dizendo estar com saudades
     C          Am           D
Que não me perderia pra ninguém

      Bm        C           Em
Se um dia te tirei do pensamento
    Am           Em
Pra tudo isso lamento
   Am           Em       Am
Motivos já nem mesmo sei
    Bm           C          Em
As fotos estão de volta na parede
Am             Em
Retomando o enfeite
    Am            Em        Am Bm
Seu rosto lindo me entretém
    Bm          Cm         Bm
Um lápis, um papel e uma viola
  C                 Bm      C          Am          D
E essa canção me consola de seu amor voltei a ser refém

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
