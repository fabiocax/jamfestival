Maskavo - Difícil pra mim

D                       Em
Não se despeça de mim assim,
meu bem
F#m    G                   D
Pois, é difícil demais pra mim,
     A
pra você também
Em                       D
Você quis pegar aquele avião
                             Em
Queria que fosse o fim, mas não
                        D
Eu não quero que seja assim
Em                         D
Você quis levar o meu coração
                            Em
Deixá-lo a ver navios, pois sim
                    D           A
É difícil demais pra mim

D                       Em
Não se despeça de mim assim,

meu bem
F#m     G                  D
Pois, é difícil demais pra mim,
     A
pra você também
Em                         D
Você sabe bem aquilo que quer
                            Em
Pois já se tornou mulher, eu não
                         D
Eu não quero que seja assim

Em                          D
Você faz prevalecer a razão
                       Em
Esquece do coração, enfim
                    D        A
É difícil demais pra mim

D                        Em
Não se despeça de mim assim,
meu bem
F#m     G                  D
Pois é difícil demais pra mim,
     A
pra você também.


----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
