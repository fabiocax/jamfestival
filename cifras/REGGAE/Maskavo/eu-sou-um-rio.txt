Maskavo - Eu sou um rio

(Quim/Prata/Marceleza/Bruno)

Introd.: Dm C
Refrão.:
           Dm
Eu sou um rio
                 C
Um rio que vai bater no mar
Dm
  Nascido em fonte de água pura
C
 Eu brotei de encontro ao mundo
Dm
 Abençoado pela lua
C
 Iluminado pelo sol
Dm
 Vida então presenteada
C
 Minha missão a ser cumprida
Dm
 Sem saber o que viria

C                Bb
 Comecei a caminhar

Refrão.:

Dm
 Conheci a luz do dia
C
 Encontrei os meus limites
Dm
 Margeando toda vida
C
 Em fraterna cooperação
Dm
 Vejo as pedras no caminho
C
 Mas aos poucos eu desvio
Dm
 Procurando um leito amigo
C                   Bb
 Onde eu possa navegar

Refrão.:

Dm
 Com aquele que me guia
C
 Vou seguindo meu destino
Dm
 Não há nada senão
C
 Para me fazer parar
Dm
 O trajeto me ensina
C
 Em sutis simbologias
Dm
 A riqueza dessa vida
C                  Bb
 A grandeza desse mar

Refrão

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
