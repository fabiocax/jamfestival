Maskavo - Quero Te Deixar Em Paz

Intro: G, Em, 2x  C, D.

 G             Em
  Quero te deixar em paz

 G              Em             C     D
  Sinto nao poder jamais desfazer o que fiz

 G               Em
  Mas nao sei se sou capaz

 G              Em          C       D
  De me afastar de ti so voce mer faz feliz

 C
  Aceite

 G                                C
  A verdade bate alto a sua porta

 C
  A sorte


 G                                     C
  Ja e muito tarde pra qualquer aposta

 C
  O vento

Bm       Am        D
  Soprando a meu favor


Repete os acordes

Um doce perfume nasce do teu beijo
que te tao verdadeiro marcou o seu jeito
um abraco forte cura a solidao
trazendo certeza de paz no coracao
E dias tao vagos me trazem memoria
De quanto e longa a  nossa historia
Fiquei em pedacos quando foi embora
Voltei a min mesmo depois que voltou
Bem sei eu erro
Bem sei que sou capaz
Do amor espero felicidade e paz

Colaborador Bruno de J. Marchao




----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
