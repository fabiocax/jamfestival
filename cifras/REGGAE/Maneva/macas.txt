Maneva - Maçãs

[Intro] A9  A  A9  A9  A  A9
        F#m
        F  G  A  A9  A9  A  A9

A9                  A             A9
Do teu sorriso eu fico com as maçãs
F#m
Seu ombro amigo me mostra um novo amanhã
F                  G                A A9 A A9
Será que vai dar tempo de te ver amor
F                  G                A A9 A A9
Será que vai dar tempo de fazer amor

A9                  A        A9
Nós dois presos no mesmo odeon
F#m
Sem roteiro, sem censura ou direção
F                  G                A A9 A A9
São só duas almas puras sem vergonha amor
F                  G                A A9 A A9
São só duas almas nuas vestidas de amor


A9
Não saia antes desta cena terminar
B
A minha manhã está na faísca do olhar
F                  G              A A9 A A9
Que você dá pra mim querendo mais amor
F                  G               A A9 A A9
Que você dá pra mim pedindo mais amor

A9                  A             A9
Do teu sorriso eu fico com as maçãs
F#m
Seu ombro amigo me mostra um novo amanhã
F                  G                A A9 A A9
Será que vai dar tempo de te ver amor
F                  G                A A9 A A9
Será que vai dar tempo de fazer amor

A9                  A        A9
Nós dois presos no mesmo odeon
F#m
Sem roteiro, sem censura ou direção
F                  G                A A9 A A9
São só duas almas puras sem vergonha amor
F                  G                A A9 A A9
São só duas almas nuas vestidas de amor

A9
Não saia antes desta cena terminar
B
A minha manhã está na faísca do olhar
F                  G              A A9 A A9
Que você dá pra mim querendo mais amor
F                  G               A A9 A A9
Que você dá pra mim pedindo mais amor

F                  G              A A9 A A9
Que você dá pra mim querendo mais amor
F                  G            A9
Que você dá pra mim pedindo mais

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
B = X 2 4 4 4 2
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
