Maneva - Serei

Am Dm G
Serei
Am
Uma flor se for preciso ser
 Dm G  Am
Colher, o orvalho da tua manhã
Dm                      G
Serei mártir, se for necessário
             Am
De ideais precários se preciso for
Dm                        G
Deixarei que me rasguem a pele
            F               E
E com ela selem o cavalo do amor
  Am                            E/G#
Eu reviro as minhas entranhas, pra entender porque tuas danças
      Em/G                    G  E/G#
Nunca deixam de habitar o meu moral
 Am                             E/G#
Sei que tudo tem começo, meio e fim esse é o preço
        Em/G                             G E/G#
Quando estou entre seus rins é o fim que trás

 Am                          E/G#
Satisfação da alma fêmea, sensação da mais suprema
    Em/G                  G E/G#
Que explode no meu corpo dilatado
 Am                          E/G#
Nossas partes siamesas, já cansadas da proeza
   Em/G                          E
De buscar o tal prazer que é tão fugaz

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
Em/G = 3 X 2 4 5 X
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
