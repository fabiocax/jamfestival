Maneva - Lenha

B7M                                A#m7
Pra esse amor tão louco, irresistível
                                B7M
Me lembro da coragem que eu criei
                                 A#m7
Pra mim, não eram sonhos invisíveis
                         G#m7
Incrível como pude ver além
                               A#m7
Ser louco nessa vida não é fácil
                             G#m7
Viver pra ser feliz, fazer amor
                                C#
Na terra, o nosso tempo é escasso
                               B7M
Não tenho tempo pra não ser amor

B7M    A#m7
Oh uo uo

            B7M
Vai, vem cantar pra mim

          A#m7
Meu mundo é todo seu
          B7M
Estamos a sós, enfim
         A#m7
Vem clarear meu breu
         B7M
Vem ser nascente em mim
           A#m7
Esperança de um novo eu
        B7M
Seremos sempre assim
A#m7          F#
Do fogo o desejo
      G#m7
A lenha

----------------- Acordes -----------------
A#m7 = X 1 3 1 2 1
B7M = X 2 4 3 4 2
C# = X 4 6 6 6 4
F# = 2 4 4 3 2 2
G#m7 = 4 X 4 4 4 X
