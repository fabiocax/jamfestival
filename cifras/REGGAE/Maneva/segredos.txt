Maneva - Segredos

[Intro] Bm  G  Bm  G
        Bm  G  D

[Solo]

E|----------------------------------------------------------|
B|-0--2--3/8----0--2--3/7----0--2--3/8--7-------------------|
G|----------------------------------------------------------|
D|----------------------------------------------------------|
A|----------------------------------------------------------|
E|----------------------------------------------------------|

Bm     E7           A
   Adoro sempre ter, segredos com você
Bm        E7        A
  Te olhar e só você e eu, nos entender
Bm        E7         A
  O mundo é nosso lar, basta eu te encontrar
Bm        E7            A
  E quem olhar já vai saber, que eu sou seu


Bm G          Bm G            Bm G
     Nada de dor, vida sem disputas
     Bm G                 D
É só somar, minha vida na tua
          D7
Juntar os meus com os seus

G                                      Gm
Te olhando de longe já imaginando meu bem mais querido
D                             D7
Você de vestido, dando vida ao amor
G                                      Gm
Te olhando de perto meu peito parece o de um passarinho
D
Ó flor sem espinho
D7
Ferrão do torpor

Em
Viciado em teus beijos
A           Bb°
Cego de desejo
  Bm        G
Escravo do amor

[Final] Bm  G
        Bm  G  D

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bb° = X 1 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
