Maneva - Me Olhe Bem

[Intro] Gm7  Bb  F  Dm  C

[Primeira Parte]

 Bb                                    F/A
Todos bons conselhos nunca vem da solidão
            Dm           C            Bb
Já prefiro ter paz ao invés de ter razão
                                      F/A
O teu batom vermelho no meio da multidão
              Dm        C            Bb
Fez o sol vermelhar, farol na escuridão
                                      F/A
O sal do teu suor é minha doce distração
             Dm              C           Bb
Faz o tempo parar, pra decifrar teu coração

[Pré-Refrão]

           Gm7  C7/9    Dm
Não quero nunca    não te ver

           Gm7  C7/9  Dm
Não quero nunca   te perder
          Gm7  C7/9      Dm
Te quero nua,    pra te ter

[Refrão]

 Am C  Em Dm                         Am7
Me  olhe bem, veja as marcas do meu rosto
                  Bb               C   Dm
Nunca estarei disposto a perder você
 Am  C   Em Dm                            Am7
Re__pa__re bem em mim, eu não sou menino novo
                    Bb                  C
Não sou de fazer esboço pra uma obra assim

[Primeira Parte]

 Bb                                    F/A
Todos bons conselhos nunca vem da solidão
            Dm           C            Bb
Já prefiro ter paz ao invés de ter razão
                                      F/A
O teu batom vermelho no meio da multidão
              Dm        C            Bb
Fez o sol vermelhar, farol na escuridão
                                      F/A
O sal do teu suor é minha doce distração
             Dm              C           Bb
Faz o tempo parar, pra decifrar teu coração

[Pré-Refrão]

           Gm7  C7/9    Dm
Não quero nunca    não te ver
           Gm7  C7/9  Dm
Não quero nunca   te perder
          Gm7  C7/9      Dm
Te quero nua,    pra te ter

[Refrão]

 Am C  Em Dm                         Am7
Me  olhe bem, veja as marcas do meu rosto
                  Bb               C   Dm
Nunca estarei disposto a perder você
 Am  C   Em Dm                            Am7
Re__pa__re bem em mim, eu não sou menino novo
                    Bb                  C  Dm
Não sou de fazer esboço pra uma obra assim

 Am C  Em Dm                         Am7
Me  olhe bem, veja as marcas do meu rosto
                  Bb               C   Dm
Nunca estarei disposto a perder você
 Am  C   Em Dm                            Am7
Re__pa__re bem em mim, eu não sou menino novo
                    Bb                  C  Dm
Não sou de fazer esboço pra uma obra assim

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C7/9 = X 3 2 3 3 X
Dm = X X 0 2 3 1
Gm7 = 3 X 3 3 3 X
