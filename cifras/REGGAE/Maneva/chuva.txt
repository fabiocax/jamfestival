Maneva - Chuva

Intro: C Am Em G

Verso: C Am Em G

Que a chuva lave nossos lares de toda dor
De toda mágoa e rancor
Que o vento traga novos ares pro nosso amor
Traga emoção e calor

Vejo como era tão frágil, vejo
Vejo que não era fácil, lembro
Que me escondia, me suprimia
Que não vivia o que era eu
Lembro do passado era tão doloroso
E eu acreditava fui tão fervoroso
Me escondia nas melodias que eu criava pra você

Que eu sempre possa te guiar
Sem te julgar, poder te mostrar
Que não existe vida sem paixão
Que a vida esteja sempre em paz

Se desprenda de traumas, limpe sua alma
Entre em contato com você

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
