Maneva - Caminho Certo

(intro) E7+ F#/E D#m7 G#m7

E7+ F#/E D#m7 G#m7
Caminhei, certo que eu iria te encontrar,
E7+ F#/E D#m7 G#m7
 Avistei,  O caminho certo pra gente trilhar,
E7+ F#/E D#m7 G#m7 A7+ G#7
 Seu olhar,  Iluminou meus passos, Vou chegar,

C#m7 C#m7 B9 B5 A7+ A6 A7+  G#7
 Não vou mais,  Dizer que não te quero,
C#m7  C#m7 B9 B5 A7+ A6  A7+ G#7
 Não vou mais negar
C#m7 C#m7 B9 B5 A7+ A6 A7+ G#7
 Não vou mais,  Dizer que não te quero,
C#m7  C#m7 B9 B5 A7+ A6 G#7
  Não vou mais negar,

E7+ F#/E D#m7 G#m7
Caminhei, Certo que eu iria te encontrar,
E7+ F#/E D#m7 G#m7
Avistei, O caminho certo pra gente trilhar,

E7+ F#/E D#m7 G#m7 A7+ G#7
Seu olhar, Iluminou meus passos, Vou chegar,

C#m7 C#m7 B9 B5 A7+ A6 A7+ G#7
Não vou mais Dizer que não te quero,
C#m7 C#m7 B9 B5 A7+ A6 A7+ G#7
Não vou mais negar,
C#m7 C#m7 B9 B5 A7+ A6 A7+ G#7
Não vou mais, Dizer que não te quero,
C#m7 C#m7 B9 B5 A7+ A6 G#7
Não vou mais negar
C#m7 C#m7 B9 B5 A7+ A6 G#7  (2x)
 Meu sentimento por você.

C#m7 C#m7 B9 B5 A7+ A6 A7+ G#7
Nessa estrada vou a caminhar,
C#m7 C#m7 B9 B5 A7+ A6 A7+ G#7
Tenho certeza que minha paz eu encontrei,
C#m7 C#m7 B9 B5 A7+ A6 A7+ G#7
E ao seu lado tenho forças pra lutar,
C#m7 C#m7 B9 B5 A7+ A6 G#7
Por um futuro que ninguém há de abalar.

C#m7 C#m7 B9 B5 A7+ A6 A7+  G#7
Não vou mais chorar, Tudo que passamos juntos nem o tempo nem ninguém pode apagar
C#m7 C#m7 B9 B5 A7+ A6 A7+ G#7
Não vou mais chorar Vou deixar que Deus nos guie nesse caminhar.
C#m7 C#m7 B9 B5 A7+ A6 A7+  G#7
Não vou mais chorar, Tudo que passamos juntos nem o tempo nem ninguém pode apagar
C#m7 C#m7 B9 B5 A7+ A6 G#7
Não vou mais chorar Vou deixar que Deus nos guie nesse caminhar.

----------------- Acordes -----------------
A6 = 5 X 4 6 5 X
A7+ = X 0 2 1 2 0
B5 = X 2 4 4 X X
B9 = X 2 4 4 2 2
C#m7 = X 4 6 4 5 4
D#m7 = X X 1 3 2 2
E7+ = X X 2 4 4 4
F#/E = X X 2 3 2 2
G#7 = 4 6 4 5 4 4
G#m7 = 4 X 4 4 4 X
