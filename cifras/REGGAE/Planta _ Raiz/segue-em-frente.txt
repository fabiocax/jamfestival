Planta e Raiz - Segue Em Frente

  Em          Gb6       G     G
Dentro de mim há uma voz que me fala
Em        D     Em   D
Segue em frente

     Em       Gb6
O seu caminho é você quem faz
  G               G
Não temos tempo para olhar pra trás
 Em                   D
E o que virá, o que virá
            Em       D
Será bem vindo
     Em                Gb6
A vida é boa pra quem faz o bem
 G            G
Vai na fé que a hora vem
 Em                  D
E o que virá, o que virá
            Em       D
Será bem vindo


  Em
Viva o agora
  Gb6
Ame agora
  G
Perdoe agora
  G
Reze agora
    Em          D     Em  D
E respira

( Em  Gb6  G  Em  D  Em  D )

----------------- Acordes -----------------
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
Gb6 = 2 X 1 3 2 X
