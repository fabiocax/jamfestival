Planta e Raiz - Ideia Certa

(intro) Eb Dm

(Eb Dm)
E se falando em grana, brother, você diz não ser nada
Só que trocando essa idéia contigo eu vejo que você tá numa errada

(2X)
Gm                                                             F
Apesar da força de vontade do amor em seu peito
(F F#m) Gm
    O sistema suga o sangue da galera
                             F      (F F#m)
E ainda não lhe deixa agir direito

(Cm Gm)
Mas eu tenho um conselho, que talves irá lhe ajudar
Pense apenas no futuro do seu filho
E veja o bem que isso irá proporcionar
A vida é bem mais bela amigo
E foi feita pra viver
             Cm              Dm
Portanto viva a sua, e deixe os outros viverem


(Eb Dm)
A gente têm que se unir
Ao invés de querermos matar uns aos outros
Que a gente vai conseguindo
O nosso objetivo pouco a pouco

(2x)
Gm                                          F
Que é viver num Brasil, sem ter a nóia de deixar sair nossos trilhos
(F F#m) Gm
       Que vá pra ponte que partiu
             F
Os que não querem ver o país nos trilhos

(solo) Gm F Eb Dm
       Gm F Eb Dm

Mas eu tenho um conselho...

----------------- Acordes -----------------
Cm = X 3 5 5 4 3
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
Gm = 3 5 5 3 3 3
