Planta e Raiz - Te Ver

Introdução (Refrão):
e|--2--------------------4-------------------------|
B|--3--------------------5-------------------------|
G|--3-------4-6-4--------6---------4-6-8-6-5-------|
D|--3---4-6-------6------6----4-6------------6-----|
A|--2--------------------4-------------------------|
E|--2--------------------4-------------------------|

B
Te ver e não te querer
C#m
É improvável é impossível
B
Te ter e ter que esquecer
C#m
É insuportável é dor incrível

B
É como mergulhar num rio e
C#m
não se molhar

B
É como não morrer de frio no
C#m
gelo polar
B
É ter o estômago vazio e
C#m
não almoçar
B
É ver o céu se abrir no estio e
C#m
não se animar

..........REFRÃO..........

B
É como esperar o prato e
C#m
não salivar
B
Sentir apertar o sapato e
C#m
não descalçar
B
É como ser feliz de fato sem
C#m
alguém para amar
B
É como procurar no mato
C#m
estrela do mar

..........REFRÃO..........

B
É como não sentir calor
C#m
em Cuiabá
B
Ou como no Arpoador
C#m
não ver o mar
B
É como não morrer de raiva
C#m
com a política
B
Não se ligar que a tarde vai
C#m
vadia e mística
B
É como ver televisão e
C#m
não dormir
B
Ver um bichano pelo chão e
C#m
não sorrir
B
É como não provar o nectar do
C#m
verdadeiro amor
B
Quando o coração detecta a
C#m
mais fina flor

Final:
e|-----------------------------------------------------------------------------------|
B|-----------------------------------------------------------------------------------|
G|------------------------------11---------------------------------------------------|
D|9--------9-11h9------11---11b----11-9h11h9------9----------9-11h9------11----11/13-|
A|--9-9h11--------11-9-----------------------11-9-----9-9h11--------11-9----11-------|
E|-----------------------------------------------------------------------------------|

----------------- Acordes -----------------
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
