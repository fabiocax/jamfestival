Planta e Raiz - Goleiro Matador

 G
 Ô Tricolor
 G
 Ah Tricolor
Bb               Eb                       F
  O goleiro matador, bate a falta, faz o gol
                    Bb
 Faz a festa da torcida
              F    Eb               Bb
 Que sempre unida,    grita "É campeão"

 G
 Ô Tricolor
 G
 Ah Tricolor
Bb               Eb
 A nação do Morumbi
                       F
 não se cansa de aplaudir
                  Bb
  A jogada mais bonita

           F
 O mundo grita:
Eb                 Bb
 "São Paulo é campeão!!"

[Refrao]
C
 Vai lá, vai lá, vai lá, vai lá, vai lá de coração!
C
 Vamo São Paulo, vamo São Paulo, vamo ser campeão!
C
 Vai lá, vai lá, vai lá, vai lá, vai lá de coração!
C
 Vamo São Paulo, vamo São Paulo, vamo ser campeão!


[Repete]

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
