Planta e Raiz - Qual É a Cara do Ladrão

(intro 2x) Am Dm  Am Dm Em

E|---------------------------|----------------------------|
B|--------5--------------8---|--------5------------8-10-8-|
G|------5----7-5-7-5-7-9---9-|------5----7-5-7--9---------| (2x)
D|--5-7----------------------|--5-7-----------------------|
A|---------------------------|----------------------------|
E|---------------------------|----------------------------|

Am                 Dm
Qual é a cara do ladrão
Am                       G
Quem é que vai saber
Am                  Dm
Será o moleque de calção
Am                               G
Ou o engravatado no poder

Am            Dm
Eu prefiro confiar
Am                    G
Só no amor de Deus

Am                           Dm
Pois o que o dinheiro não me dá (oo iaia)
Am          G
Ele ja me deu

C                   G
Não venha dizer que não
C                 G
Essa é a minha certeza
C                G
Eu não caio na ilusão
F                          G
Agradeço a comida sobre a mesa

C                G
Onde esta seu coração
C                  G
Esta também sua riqueza
C             G
Não caia na ilusão
F                                      G        E
Agradeça a comida sobre a mesa

(refrão 2x)
Am                  Dm
A quem não saiba dividir o pão
F                G
Cresce o olho no que é do irmão
Am                       Dm
Pobre de quem ta vivendo da corrupção
F                       G
Não importa se esse mora na favela ou na mansão

(intro)
(repete tudo)
(intro 3x)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
