Planta e Raiz - Filho do Leão

E
Vivendo na Babilon
O filho do Leão
C#m
Ele se sente forte
                G#m
Ele está curado
Ele sabe que é filho do Rei
E vive a vida pra que o amor brote
E
Ele é humilde mas não abaixa a cabeça
C#m                               G#m
E veste a farda de soldado da paz
Ele é humilde mas não abaixa a cabeça
E veste a farda de soldado da paz
E
Como um fora da lei foi criado
Nasceu com o espírito alado
C#m
Sempre foi um mano adiantado
Nunca conseguiu ficar parado

G#m
Designado para ser um guerreiro
Mudar esse mundo que hoje tá cabreiro
E
Fez os bagulhos direito
Andou no caminho estreito
C#m
Aprendeu que na vida é desse jeito
Tem que tirar as coisa ruim do peito
G#m
Alucinado pra cumprir a missão
Levar adiante o bem pra essa geração
E
O filho do leão
Não flerta com a dor
C#m
E em seu novo mundo
Não há violência
G#m
Fez de si uma nova canção
Um canal só de notícia boa
E
Ele é maluco mas não perde a cabeça
C#m
Tá no caminho da evolução
G#m
Ele é maluco mas não perde a cabeça
Tá no caminho da evolução
E
Uooohh Uooohh Ahh
C#m
Uooohh Uooohh Ahh
G#m
Filho do Leão
Mais um filho do leão
E
Uooohh Uooohh Ahh
C#m
Uooohh Uooohh Ahh
G#m
Filho do Leão
Mais um filho do leão
E
Seu jeito de agir
na positividade
C#m
Em meio a essa guerra
Faz a diferença
G#m
Um exemplo de superação
Uma história pra mudar a sua
E
No travesseiro quando deita a cabeça
C#m
Ele tem sonhos de um mundo melhor
G#m
No travesseiro quando deita a cabeça
Ele tem sonhos de um mundo melhor
E
Uooohh Uooohh Ahh
C#m
Uooohh Uooohh Ahh
G#m
Filho do Leão
Mais um filho do leão
E
Uooohh Uooohh Ahh
C#m
Uooohh Uooohh Ahh
G#m
Filho do Leão
Mais um filho do leão

----------------- Acordes -----------------
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
G#m = 4 6 6 4 4 4
