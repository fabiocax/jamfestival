Planta e Raiz - O Que Você Disse

Gm  Cm ( a Música Toda ) (4 Tempos Cada Nota)

O que você disse machucou meu coração
O que você disse machucou meu coração
O que você disse machucou meu coração
O que você disse machucou meu coração

Dizem que não mais eu sei o que rola por de trás da cortina
Você não consegue me enganar
Eu já tomei dessa vacina
Eu quero ter um coração mole como manteiga
Pra quando precisar chorar
Na alegria ou na tristeza
Vou te falar

(Refrão)

Dizem que não mais eu sei o que rola por de trás da cortina
Você já não pode me enganar
Porque eu tomei essa vacina
Eu quero ter um coração mole como manteiga

Pra quando precisar chorar
Na alegria ou na tristeza
Vou te dizer

(Refrão)

Palavras são flechas envenenadas
Podem ferir ou até matar
Feridas abertas
Outras fechadas
Uma cicatriz sempre vai ficar
Por isso vou dizer

(Refrão)

----------------- Acordes -----------------
Cm = X 3 5 5 4 3
Gm = 3 5 5 3 3 3
