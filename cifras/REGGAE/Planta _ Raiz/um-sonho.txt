Planta e Raiz - Um sonho

Intro: F#m Bm C#m

F#m                            Bm
   Um dia eu sonhei com o rei
             C#m               F#m
Fui previlegiado pelo Marley
                              Bm
Um dia eu sonhei com o rei
             C#m               F#m
Fui previlegiado pelo Marley

F#m                              Bm        C#m
Na minha vida muita coisa aconteceu
F#m                              Bm        C#m
Descobrir o reggae foi um presente de Deus
          F#m
Ainda bem !!!

( nesta parte a guitarra permanece em silêncio)
                                             Bm   C#m
Que eu tenho a certeza que Jesus olha por nós

F#m                                     Bm    C#m
E que nessa ideologia nunca estaremos sós


REFRÃO


(F#m Bm C#m)
E nesse sonho tava presente a galera
Bob Marley e a sonzera lá no Ibirapuera

Ainda bem!!!

Que o reggae e a nossa força nunca mais vai acabar

Porque ele é a melhor maneira da gente poder lutar


REFRÃO

----------------- Acordes -----------------
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
F#m = 2 4 4 2 2 2
