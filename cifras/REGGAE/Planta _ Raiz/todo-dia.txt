Planta e Raiz - Todo Dia

intro: Bm Em (2x)


Bm          Em
Todo dia é assim

                  Bm
Tenho que despertar pra mais um desafio

Em                                  A
Seguindo a minha vida na labuta eu trilho


  Em                         Bm
Sei que a vida é sofrida

 Em                              Bm
Mas sigo reto no caminho bom

 Em        A           Bm
Vamos virar o jogo


 Em        A                Bm
Se duvidar da minha fé

 Em             A                 Bm
Eu vou chegar se Deus quiser


Refrão (2x):

                  Em
É o amor que me leva

   A           Bm
Pra outra dimensão

             Em
Me faz acreditar

  A                      Bm
Fazer com minhas próprias mãos


                  Em
É o amor que me leva

 A           Bm
Pra outra dimensão

                   Em
Me dá forças pra lutar

  A                Bm
Levar a minha vibração

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
Em = 0 2 2 0 0 0
