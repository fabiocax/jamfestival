Planta e Raiz - Gueto do Universo

( PLANTA & RAIZ e CHORÃO )


Introd.: F#m, Bm, F#m, Bm, F#m, Bm, F#m, E (2x)

  F#m   Bm                 F#m    Bm     F#m            Bm         C#
A terra    é o gueto do Universo,    mas tudo que há de belo está aqui
D           Bm       C#
Venha, você pra conferir...
   F#m            E     Bm                  C#m7               F#m
Desligue a televisão, o que você quer encontrar pode estar lá fora
      D           Db
Você tem que respirar...
    F#m                 E      Bm                C#m7                   F#m
Não dá pra viver de ilusão, o mundo há de se acabar, mas ninguém sabe a hora
     D           Db
Você tem que encarar...
  Bm           E           F#m          E
O homem passa fome, mesmo tendo o que comer
   Bm               E         D          Db         D         Db
Enquanto o que não come tem a sede de viver, tem a sede de viver.


A                E             F#m                      D
Força pra lutar, fé para vencer... Quantos dragões nós iremos combater?
A                E             F#m                      D
Força pra lutar, fé para vencer... Na mão de Deus tudo pode acontecer
A                E             F#m D
Força pra lutar, fé para vencer...
A                E             F#m                      D
Força pra lutar, fé para vencer... Na mão de Deus tudo pode acontecer

[solo] F#m, Bm, F#m, Bm, F#m, Bm, F#m, E


A terra é o gueto do Universo...

Força pra lutar, fé para vencer... Quantos dragões nós iremos combater?
Força pra lutar, fé para vencer... Na mão de Deus tudo pode acontecer

(F#m, Bm, F#m, Bm, F#m, Bm, F#m, E)
Na minha vida tudo acontece,
mas quanto mais a gente rala, mais a gente cresce.
Hoje estou feliz porque eu sonhei com você,
e amanhã posso chorar por não poder te ver,
mas o seu sorriso vale mais que um diamante,
se você vier comigo aí nós vamos adiante.
Com a cabeça erguida, mantendo a fé em Deus
e o seu dia mais feliz vai ser o mesmo que o meu.
A vida me ensinou a nunca desistir,
nem ganhar nem perder, mas procurar evoluir.
Podem me tirar tudo o que tenho,
só não podem me tirar as coisas boas que eu já fiz pra quem eu amo
Sou feliz e canto o universo é uma canção e eu vou que vou.
Histórias, nossas histórias, dias de luta, dias de glória. (2x)


Força pra lutar, fé para vencer... Quantos dragões nós iremos combater?
Força pra lutar, fé para vencer... Na mão de Deus tudo pode acontecer

F#m, Bm, F#m, Bm, F#m

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C# = X 4 6 6 6 4
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
Db = X 4 6 6 6 4
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
