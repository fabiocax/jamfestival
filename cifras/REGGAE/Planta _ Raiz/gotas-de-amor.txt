Planta e Raiz - Gotas de Amor

Intro:
                F         G        Em        Am
E|-----7-8-7-5------8-7-8----7-5-7-----12-10-12---------------|
B|-5-8--------8-6---------8--------5--------------------------|
G|------------------------------------------------------------|
D|------------------------------------------------------------|
A|------------------------------------------------------------|
E|------------------------------------------------------------|

                F         G        Em        Am
E|-----7-8-7-5------8-7-8----7-5-7-----12-10-12---------------|
B|-5-8--------8-6---------8--------5--------------------------|
G|------------------------------------------------------------|
D|------------------------------------------------------------|
A|------------------------------------------------------------|
E|------------------------------------------------------------|

                F            G          E
E|-----7-8-7-5------8-7-8------5-7-5--------------------------|
B|-5-8--------8-6----------5-8------8-6-5---------------------|
G|------------------------------------------------------------|
D|------------------------------------------------------------|
A|------------------------------------------------------------|
E|------------------------------------------------------------|


 Am       G
Começar
     Em               Am
refletir com os pés na água
  G           Em
Libera toda mágoa
Am        G
Despertar
    Em             Am
Na areia depois da festa
       G               Em
Logo a lua é o que nos resta

       Am                Dm
Aquela tarde parecia nos avisar
        Em             Am
O que à noite poderia acontecer
                        Dm
Fechei os olhos então rezei com fé
   Em              Am
Em Deus é quem vai fazer

        F            G
Derrame o dom sobre nós
    F     G     Am
Abençoe a nossa voz
       F                 G
O céu mudou ta chovendo amor
   Am
Gotas de amor

         F            G
Derrame o dom sobre nós
    F     G     Am
Abençoe a nossa voz
       F                   G
O céu mudou ta chovendo amor
   Am
Gotas de amor
         F                G
O céu mudou ta chovendo amor
   Am
Gotas de amor

              G               Am
Deixa que o amor caia sobre nós
            F          G    Am
E deixa que o amor caia sobre nós

              G                 Am
Deixa que o amor caia sobre nós
               F          G    Am
E deixa que o amor caia sobre nós

(Intro)

      F              G
Derrame o dom sobre nós
    F     G     Am
Abençoe a nossa voz
       F                 G
O céu mudou ta chovendo amor
   Am
Gotas de amor

         F            G
Derrame o dom sobre nós
    F     G     Am
Abençoe a nossa voz
       F                   G
O céu mudou ta chovendo amor
   Am
Gotas de amor
         F                G
O céu mudou ta chovendo amor
   Am
Gotas de amor

            G               Am
Deixa que o amor caia sobre nós
            F          G    Am
E deixa que o amor caia sobre nós

              G                 Am
Deixa que o amor caia sobre nós
               F          G    Am
E deixa que o amor caia sobre nós

(Intro)

( C )

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
