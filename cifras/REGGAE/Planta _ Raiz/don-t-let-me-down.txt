Planta e Raiz - Don't let me down

(intro)

E|---------------------------------------|
B|---------------------------------------|
G|-------------9---9----9----------------|
D|-------------9---9h11-9----------------|
A|--------9h11----------------9--12--11--|
E|-0-------------------------------------|

( F#m  E )
Don't let me down, Don't let me down
Don't let me down, Don't let me down
( F#m  E )
Nobody ever loved me like she does
Oooo she does... yes, she does
And if somebody loved me like she do me
Oh, she do me
Yes she does
( F#m  E )
Don't let me down, Don't let me down
Don't let me down, Don't let me down

( F#m  E )
Oh, I'm in love for the first time
Don't you know it's gonna last?
It's a love that lasts forever
It's a love that has no past
 ( F#m  E )
Don't let me down, Don't let me down
Don't let me down, Don't let me down...
 ( F#m  E )
And from the first time that she really done me
Oooo she done me she done me good
I gues nobody ever really done me
Ooo she done me...she done me good
( F#m  E )
Don't let me down, Don't let me down
Don't let me down, Don't let me down...

----------------- Acordes -----------------
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
