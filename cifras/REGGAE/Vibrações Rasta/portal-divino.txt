Vibrações Rasta - Portal Divino

C                       G                   F    C  G
Quando você canta o amor, você abre o portal divino
C                            G            F
Aí você entra em contato direto com o ser
    C   G
Supremo
C
Se o homem é consciente
            G
Por quê agir de forma irracional?
F                           C              G
Pra que usar sua mente, seu poder com o mal?
 C                    G
Ah, eu so queria saber
                F
Me diz o porquê
                      C G
Pois eu não sei dizer
  C                       G
Ah, eu só queria entender
                   F
quem vai me dizer?

    C     G
Quero saber
  C         G
Sim, ele virá
               F                C G
JAH JAH voltará pra nos libertar
  C           G
Sim, ele virá
                F                C                    G
JAH JAH voltará pra nos libertar da escravidão mental

C
Cantar o sossego
G                                F
e principalmente viver o que cantar
                   C            G
Esse é o meu dever, minha missão
C
Cantar o amor,
G                                  F
Mas principalmente viver o que cantar
                   C              G
Esse é o meu dever, pois ele virá
  C          G
Sim, ele virá
                F
JAH JAH voltará
                 C                      G
pra nos libertar de todo ou qualquer mal
  C          G
sim, ele virá
                F
JAH JAH voltará
                 C                   G
Pra nos libertar da escravidão mental.

----------------- Acordes -----------------
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
