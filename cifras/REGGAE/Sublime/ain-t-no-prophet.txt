Sublime - Ain't No Prophet

D           C                   G                D
  I Aint no Prophet, dont wanna cure no damn disease.
D                    C         G           D
  Would ya marry me anyways if I was on my knees.

(ritmo acelera-se com os mesmos acordes, quando ele invade o seguinte verso)

D                   C              G           D
  So I thought to myself, Im gonna marry into wealth.
D                                C          G             D
  Im gonna take everything she's got oh and keep it for myself.
C           G        C             G
She said to me, I dont like what I see
C         G    D
I said to her, I dont even know your name.

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
