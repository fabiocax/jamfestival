Sublime - Doin' Time

**Normal Tuning**

Intro (Gm Bb C C) 4x


*-*Chorus

Gm    Bb   C               Gm   Bb               C                  Gm
Summertime and the livin's easy Bradley's on the microphone w/ ras M.G.
        Bb            C                     Gm     Bb
all the people in the dance will agree that we are well qualified to
C               Cm               D
represent the LBC G me and louie run to the party dance to the riddim it
Gm         Bb  C
gets harder

*-*Verse 1 (Basically the whole song is the same rhythm pattern)

Me and my girl got this relationship I love her so bad but she
treats me like... on lock down like a penitentiary she spreads her lovin'
all over and when she gets on there's none left for me


*-*Chorus(Same chords)

*-*Verse 2
Oh take this veil from off my eyes a burning sun will sun day rise so what
am I gonna be doin' for a wife saying I'm going play it myself show
them how we come off the shelf

*-*Chorus

*-*Verse 3
Evil I've come to tell you that she's evil most definitely evil omery
scandalous and evil most definitely the tension is gettin' hotter
I'd like to hold her head underwater

 Break (Bascially the same but seems to skip the Bb)

*-*Chorus

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
