Sublime - Boss D.J.

            G                 C                    G            C
There is a steel train coming through, and I would take it if I could
       G               C           G              C
And I would not lie to you because sunday morning soon will come
        G                   C
And the things will be much easier to say
 G                         C                      G
Upon the microphone like a boss D.J., but I won't walk up upon the sea
        C             G               C
Like it was dry land, boss D.J. ain't nothing but a man
   G                                   C
No trouble no fuss, I know why

[Refrão]

G      C       G                 C
It's sooo nice, I wanna hear the same song twice
G      C       G                 C
It's sooo nice, I wanna hear the same song twice

            G                                            C
'Cause I'm spinnin', I'm rollin', I'm tumblin, it's just stones and sticks

       G                              C
'Cause on the microphone is where I'm gonna get my fix
             G                            C
Just let the lovin' take a hold, cause it will if you let it
    G                       C
I'm funky not a junky but I know where to get it
   G                                   C
No trouble no fuss, I know why

[Refrão]

G      C       G                 C
It's sooo nice, I wanna hear the same song twice
G      C       G                 C
It's sooo nice, I wanna hear the same song twice

G             A#           G            A#
Oooweee girl, ooowee girl, ooowee girl, ooowee girl
          C                       D      C                       D
And there really ain't no time to waste, really ain't no time to hate,
             C       D      C       D
Ain't got no time to waste, time to hate,
G                                C       D
Really ain't no time to make the time go way, ay ay

          G                   C              G
So mister D.J. don't stop the music, I wanna know
        C                    G     A#
Are you feeling the same way too?
                     G         A#        G             A#
I wanna rock it with you girl, ooo girl, ooo girl, ooo hoo hoo

[Solo] C  D

       C                   D
E|---8-------8-8----8----10---------10-10----10--|
B|---8------8--8----8----10-------10---10----10--|
G|---9-----9---9----9----11-----11-----11----11--| (3x)
D|--10--10-------10------12--12-----------12-----|
A|--10--10-------10------12--12-----------12-----|
E|---8---8--------8------10--10-----------10-----|

[Refrão]

      G       C       G                 C
'Cause it's sooo nice, I wanna hear the same song twice
G      C       G                 C
It's sooo nice, I wanna hear the same song twice
C              D                  C
Now-a-days the songs on the radio all, all drive me crazy

----------------- Acordes -----------------
