Armandinho - Analua

[Intro] A9  Bm7  D

E|--------------------------------|
B|--------------------------------|
G|--------------------------------| (3x)
D|------7-------7-----------7~~---|
A|--7-9-----7-9---9-7---7-9-------|
E|--------------------------------|

E|--------------------------------|
B|--------------------------------|
G|-------8/9----------------------|
D|--7/11-----9-7------------------|
A|--------------------------------|
E|--------------------------------|

 A9
Porque você não sai
         Bm7
De trás da nuvem
                   D9/B
Pro mar fica mais lindo

            A7M
Só falta você!
                              Bm7
E a minh'alma fica mais tranqüila
          D9/B
E a vida harmoniza
               A7M  A7
Quando vejo você
Bm7             E7            C#m7  F#m7
E hoje eu caminhei a praia inteira
                Bm7
Com os pés na areia
 E      E7       A7M  A7
Coração em alto mar
         D7M
Lembrar você me faz
           C#m7
Pensar besteira
                   Bm7
Vida é brisa passageira
 E      E7    A7M  A7
Não deixa passar  ôo
           D7M
Lembrar você me faz
           C#m7
Pensar besteira
                  Bm7
Praia brava lua cheia
 E   E7   A7M
Cadê Ana Lua?

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7M = X 0 2 1 2 0
A9 = X 0 2 2 0 0
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
D7M = X X 0 2 2 2
D9/B = X 2 0 2 3 0
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m7 = 2 X 2 2 2 X
