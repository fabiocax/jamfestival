Armandinho - Tchau

(intro) C#m7 D#m7 E

(primeira parte)
G#m                   D#m7
Esse teu tchau, pirou minha cabeça,
G#m                   D#m7
Tentei pisar no chão não tinha nada,
E                D#m7  E
Pintei de verde o azul, de
         D#m7
amarelo o lilás,

E                    D#m7
Quando essa onda se quebra ela não,
           G#m
Dá em nada ... (2x)

(refrão)
        C#m7            D#m7
Por favor, me diz quero só saber,
   C#m7       D#m7
Me diz, se eu te fiz tão

        C#m7
infeliz,
    D#m7             G#m
Por botar meu pé na estrada. (2x)

(segunda parte)
G#m                   D#m7
Esse teu tchau, pirou minha cabeça,
G#m                   D#m7
Por isso que eu voltei pra essa balada,
E                D#m7  E
Mais outra amor não fez jus,
         D#m7
Sempre fiquei de cara...
E                    D#m7
Quando te amo e te beijo,

           G#m
Não quero mais nada!

(refrão)
        C#m7            D#m7
Por favor, me diz quero só saber,
   C#m7       D#m7
Me diz, se eu te fiz tão
        C#m7
infeliz,
    D#m7             G#m
Por botar meu pé na estrada. (2x)

----------------- Acordes -----------------
C#m7 = X 4 6 4 5 4
D#m7 = X X 1 3 2 2
E = 0 2 2 1 0 0
G#m = 4 6 6 4 4 4
