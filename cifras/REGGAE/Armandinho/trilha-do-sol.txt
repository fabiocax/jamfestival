Armandinho - Trilha do Sol

Intro: Am G

Am       G                 Am
Eu continuo com os pés na areia
      G               Am
Continuo com minhas idéias
      G                 Am     G
Procurando fazer o meu melhor
Am        G              Am
Mas sigo sempre no meu caminho
     G                Am
E apesar de sentir sozinho
       G            Am      G
Nunca vou me sentir menor

F            G             F
Tenho uma espada e um sentimento
        G                F
Se não vivo o melhor momento
        G                  F      G
Vou na raça, no sangue, suor


C         G           Am
 Sigo a trilha do sol
        Em7           F7+
Tomo um banho de chuva
     Em7             Dm7    G
Acredito na força da lua
C         G           Am
 Sigo a trilha do sol
        Em7           F7+
Tomo um banho de chuva
     Em7             Dm7    G
A verdade é você toda nua

Am       G           Am
Eu continuo na praia brava
     G                   Am
Cada tubo a minha'lma se lava
        G                  Am   G
Meu 'cumpade' a gritar no canal
Am         G               Am
Mas não esqueço de onde eu venho
      G            Am
Dos amigos que eu tenho
          G                 Am   G
Porto a porto uma luz no farol

F            G             F
Tenho uma espada e um sentimento
        G                  F
Se não vivo o melhor momento
        G                  F      G
Vou na raça, no sangue, suor

C         G           Am
 Sigo a trilha do sol
        Em7           F7+
Tomo um banho de chuva
     Em7             Dm7    G
Acredito na força da lua
C         G           Am
 Sigo a trilha do sol
        Em7           F7+
Tomo um banho de chuva
     Em7             Dm7    G
A verdade é você toda nua

(Am G) 8 x

F            G             F
Tenho uma espada e um sentimento
        G                  F
Se não vivo o melhor momento
        G                  F      G
Vou na raça, no sangue, suor

C         G           Am
 Sigo a trilha do sol
        Em7           F7+
Tomo um banho de chuva
     Em7             Dm7    G
Acredito na força da lua
C         G           Am
 Sigo a trilha do sol
        Em7           F7+
Tomo um banho de chuva
     Em7             Dm7    G
A verdade é você toda nua

Am       G            Am
A verdade é você toda nua          /
      G        Am                  /
A verdade é você                   /
      G               Am           /   2x
A verdade é você toda nua          /
      G        Am                  /
A verdade é você                   /

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm7 = X 5 7 5 6 5
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
F7+ = 1 X 2 2 1 X
G = 3 2 0 0 0 3
