Armandinho - Paz e Amor Na Quebrada

(intro) D   C   C   D

G                   Em        C
  Paz e amor na quebrada io, io
                            D
  Paz e amor pra rapaziada, paz e amor pra rapaziada
G                           Em       C
  Pra quem não tem medo de nada io, io
                       D
  E segue na batalha, e segue na batalha (2x)

Bb                      F
  Quero ter um mar de amigo
C                    G
  Posso ser um sonhador
Bb                F         G
  Pois ainda acredito, no amor

(C D)

G                   Em        C
  Paz e amor na quebrada io, io

                            D
  Paz e amor pra rapaziada, paz e amor pra rapaziada
G                           Em       C
  Pra quem não tem medo de nada io, io
                       D
  E segue na batalha, e segue na batalha

 Bb                      F
  Quero Ensinar pro meu filho
 C                      G
  Que não se guarda rancor,
 Bb               F         G
  Pois ainda acredito, no amor

(C D)

(solo 2x)  G Em C D

 Bb                    F
  Quero ver toda essa gente
 C                         G
  crendo em Deus Nosso Senhor
 Bb               F         G
  Ele também acredita, no amor.

(C D G)


----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
