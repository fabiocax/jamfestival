Armandinho - Anjo do Céu

[Intro]  B  F#  E  B

E|--2----------------|2----------------|
B|----0-4--0-2h4-2---|--0-4--0-2h4-0---|
G|-------------------|-----------------|
D|-------------------|-----------------|
A|-------------------|-----------------|
E|-------------------|-----------------|

B
Um anjo do céu
Que trouxe pra mim
            C#m
É a mais bonita
           E   F#
A jóia perfeita
              B
Que é pra eu cuidar
Que é pra eu amar
            C#m
Gota cristalina

             E   F#
Tem toda inocência

 B          F#
Vem oh meu bem
            E
Não chore, não
                B
Vou cantar pra você

B  C#m  E  B

B
Um anjo do céu
Que me escolheu
             C#m
Serei o seu porto
             E   F#
Guardião da pureza
              B
Que é pra eu cuidar
Que é pra eu amar
            C#m
Gota cristalina
              E    F#
Tem, toda inocência

----------------- Acordes -----------------
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
