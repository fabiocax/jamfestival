Armandinho - Dengo Seu

                       E                  E7+
Faz tempo que eu não sei, como é bom ficar
                    A Am          E
Dormindo abraçadinho, com você ia, ia.
                              E7
E hoje acordei, e nem fui surfar
                 A Am             E
Me deu uma saudade, nem é bom falar

Refrão:

C               C#m
Esquece o tempo que passou,
        F#         A        Am           E  E7
se sorriu ou se chorou, se amou ou se sofreu,
C               C#m            F#         A
E pra gente ficar bem, deixa eu ser o seu nenem,
      Am             E     B
pra ganhar um dengo seu.

                      E                  E7+
Faz tempo que eu não sei, como é bom ficar

                 A   Am            E         B
Dormindo de conchinha, com você ia, ia.
               E                     E7+
E hoje me acordei, lembrei daquilo tudo
               A  Am               E
Me deu uma saudade que nem é bom falar.

Refrão:

C               C#m
Esquece o tempo que passou,
        F#          A        Am           E  E7+
se sorriu ou se chorou, se amou ou se sofreu,
C               C#m            F#         A
E pra gente ficar bem, deixa eu ser o seu nenem,
      Am             E     B
pra ganhar um dengo seu.

                      E                  E7+
Faz tempo que eu não sei, como é bom ficar
                     A  Am            E
Na varanda tomando vinho, com você ia, ia.
                E                          E7+
E hoje me acordei, você não tava aqui do lado
               A  Am                       E
Me deu uma saudade que é... como eu vou falar.

Refrão:

C               C#m
Esquece o tempo que passou,
        F#          A        Am           E  E7+
se sorriu ou se chorou, se amou ou se sofreu,
C               C#m            F#         A
E pra gente ficar bem, deixa eu ser o seu nenem,
      Am             E
pra ganhar um dengo seu.

C               C#m            F#         A
E pra gente ficar bem, deixa eu ser o seu nenem,
      Am             E
pra ganhar um dengo seu.

( A4  E  A4  E )
Só Teu.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A4 = X 0 2 2 3 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
E7+ = X X 2 4 4 4
F# = 2 4 4 3 2 2
