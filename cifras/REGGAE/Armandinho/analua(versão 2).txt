Armandinho - Analua

Into: A7+ D7+

           D7+                         C#m7
Lembrar você me faz... Pensar besteira,
                    Bm7    E7       A7+ A7
Praia brava lua cheia... Cadê Analua
   Bm7          E7                   C#m7 F#m
E hoje eu caminhei a praia inteira...
                  Bm7    E7               A7+
Com os pés na areia, coração em alto mar
            D7+                         C#m7
Lembrar você me faz... Pensar besteira,
                     Bm7     E7             A7+
Vida é brisa passageira... Não deixar passar
           D7+                         C#m7
Lembrar você me faz... Pensar besteira,
                   Bm7     E7       A7+ A7
Praia brava lua cheia... Cadê Analua

A7+ Bm7 (2x)


    A7+                             Bm7
Porque você não sai de traz da nuvem?
                         E     E7        A7+
Pro mar ficar mais lindo... Só falta você
 A7+                             Bm7
A minha alma fica mais tranqüila,
                  E     E7            A7+ A7
E a vida harmoniza... Quando vejo você

   Bm7          E7                   C#m7 F#m
E hoje eu caminhei a praia inteira...
                  Bm7    E7               A7+
Com os pés na areia, coração em alto mar
            D7+                         C#m7
Lembrar você me faz... Pensar besteira,
                     Bm7     E7             A7+
Vida é brisa passageira... Não deixar passar
           D7+                        C#m7
Lembrar você me faz... Pensar besteira,
                   Bm7     E7       A7+ Bm7
Praia brava lua cheia... Cadê Analua

    A7+                             Bm7
Porque você não sai de traz da nuvem?
                         E     E7        A7+
Pro mar ficar mais lindo... Só falta você
 A7+                             Bm7
A minha alma fica mais tranqüila,

                  E     E7            A7+ A7
E a vida harmoniza... Quando vejo você

   Bm7          E7                   C#m7 F#m
E hoje eu caminhei a praia inteira...
                  Bm7    E7               A7+
Com os pés na areia, coração em alto mar
            D7+                        C#m7
Lembrar você me faz... Pensar besteira,
                      Bm7     E7             A7+
Vida é brisa passageira...  Não deixar passar
           D7+                         C#m7
Lembrar você me faz... Pensar besteira,
                   Bm7     E7       A7+ Bm7
Praia brava lua cheia... Cadê Analua

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
D7+ = X X 0 2 2 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
