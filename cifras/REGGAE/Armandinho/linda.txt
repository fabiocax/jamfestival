Armandinho - Linda

G           C
Linda lá na areia

D                  C             G
Menina na beira do mar vira sereia
G           C
Linda lá na areia

D                  C             G
Menina que entra na roda é capoeira

G
To pegando saideira

                C
Eu já fiz minha cabeça
D
Hoje tá quebrando a vala
C
E por incrível que pareça


G
O vento viro o céu se abriu
  C
E o mar ajeitou
D
Vou voltar no meio dia
C
Tá na areia meu amor

G
To pegando saideira
                C
Eu já fiz minha cabeça
D
Hoje tá quebrando a vala
C
E por incrível que pareça

G
O vento viro o céu se abriu
  C
E o mar ajeitou
D
Vou voltar no meio dia
C
Tá na areia meu amor

G           C
Linda lá na areia
D                  C             G
Menina na beira do mar vira sereia
G           C
Linda lá na areia
D                      C
Menina que na beira do mar
G
Vira sereia

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
