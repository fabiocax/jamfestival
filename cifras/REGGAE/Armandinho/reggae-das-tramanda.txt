Armandinho - Reggae Das Tramanda

[Intro]  E  B  A  E

E|---------------------------------------------------------------------------------|
B|---------------------------------------------------------------------------------|
G|--9--9-------9-----9--9---------9--9--slide-11-11---11--9---9--9------9--9---9---|
D|--9--9--9h11-9-----9--9--9h11---11-11-------13-13---13--11--9--11-----11-9---9---|
A|--------9h11-------------9h11----------------------------------------------11----|
E|---------------------------------------------------------------------------------|

            E                  B
E eu hoje acordei querendo ver o mar
                 A           B              E
Mas eu moro bem no meio de uma selva de pedra
E                             B
O por do sol no rio é que me faz sonhar
               A         B            E
Quem nunca imaginou pega onda no guaíba
E                                 B
E as mina tão no sol, e a galera ta no mar
                A          B          E
Eu quero uma sereia de tanga na areia

E                             B
O por do sol no rio é que me faz sonhar
               A         B            E
Quem nunca imaginou pega onda no guaíba

    A                    B
Nessa cidade quando bate uma fissura
       A                        B
Pra fugir dessa violência essa loucura
      A                       B                    E
Eu pego a freeway e vo da um banho la no pier de tramanda

     C#m            G#m
Pela estrada eu vou ouvindo um som
    C#m             G#m
No ragga muffyn eu vo fazendo a mão
 A             Am        E
Vou ficar na praia todo dia
 A             Am        E
Vou ficar na praia todo dia

E  F#  A  E

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
