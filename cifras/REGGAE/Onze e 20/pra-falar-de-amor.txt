Onze:20 - Pra Falar de Amor

[Intro] B  F#  D#m  C#

    B              F#
E|------------------------------------|
B|-4------4------/11--------11--------|
G|---4------4--------11--------11-----|
D|-----4------4---------11--------11--|
A|------------------------------------|
E|------------------------------------|

    D#m            C#
E|------------------------------------|
B|-7------7-------6------6------------|
G|---8------8-------6------6----------|
D|-----8------8-------6------6--------|
A|------------------------------------|
E|------------------------------------|

[Primeira Parte]

                    B           F#
Passando pra dizer como eu te amo

                      D#m      C#
Só pra dizer como eu te quero bem
                          B
Não sei viver mais sem o seu carinho
F#                     D#m    C#
  Eu fico triste se você não vem
               B
Pra falar de amor
                F#                   D#m
De como é importante ainda estar aqui
             C#                 B
Minha felicidade é ver você sorrir
                   F#                       D#m
Ai ai meu Deus do céu o que é que eu vou fazer
       C#
Sem você

[Pré-Refrão]

          B      F#
Brigar à toa só dói
         D#m       C#
E uma palavra destrói
        B           F#            D#m   C#
Tudo aquilo que sonhamos pra nós dois

[Refrão]

                 B                     F#
Eu vou falar besteira baixinho no ouvido
                   B                    F#
Te dar mais mil razões pra não me escutar
                B                  F#
Já já são "Onze20" e eu já to saindo
                   B                  F#
Mas não vou ser feliz se eu não te levar

                 B                     F#
Eu vou falar besteira baixinho no ouvido
                   B                    F#
Te dar mais mil razões pra não me escutar
                B                  F#
Já já são "Onze20" e eu já to saindo
                   B                  F#
Mas não vou ser feliz se eu não te levar

 B        F#   B        F#
Ai... ai ai... ai... ai ai
 B        F#   B        F#
Ai... ai ai... ai... ai ai

[Primeira Parte]

                    B           F#
Passando pra dizer como eu te amo
                      D#m      C#
Só pra dizer como eu te quero bem
                          B
Não sei viver mais sem o seu carinho
F#                     D#m    C#
  Eu fico triste se você não vem
               B
Pra falar de amor
                F#                   D#m
De como é importante ainda estar aqui
             C#                 B
Minha felicidade é ver você sorrir
                   F#                       D#m
Ai ai meu Deus do céu o que é que eu vou fazer
       C#
Sem você

[Pré-Refrão]

          B      F#
Brigar à toa só dói
         D#m       C#
E uma palavra destrói
        B           F#            D#m   C#
Tudo aquilo que sonhamos pra nós dois

[Refrão]

                 B                     F#
Eu vou falar besteira baixinho no ouvido
                   B                    F#
Te dar mais mil razões pra não me escutar
                B                  F#
Já já são "Onze20" e eu já to saindo
                   B                  F#
Mas não vou ser feliz se eu não te levar

                 B                     F#
Eu vou falar besteira baixinho no ouvido
                   B                    F#
Te dar mais mil razões pra não me escutar
                B                  F#
Já já são "Onze20" e eu já to saindo
                   B                  F#
Mas não vou ser feliz se eu não te levar

 B        F#   B        F#
Ai... ai ai... ai... ai ai
 B        F#   B        F#
Ai... ai ai... ai... ai ai
 B        F#   B        F#
Ai... ai ai... ai... ai ai
 B        F#   B        F#
Ai... ai ai... ai... ai ai

( B  F#  B  F#  B  F# )

                B                  F#
Já já são "Onze20" e eu já to saindo
                   B                  F#
Mas não vou ser feliz se eu não te levar

[Final] B

----------------- Acordes -----------------
B = X 2 4 4 4 2
C# = X 4 6 6 6 4
D#m = X X 1 3 4 2
F# = 2 4 4 3 2 2
