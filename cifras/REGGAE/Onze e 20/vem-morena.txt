﻿Onze:20 - Vem Morena

Afinação Meio Tom Abaixo

[Intro] Fm  G7  C7+  G7M  D#°  G  Cm6

Parte 1 de 2

       Fm           G7       C7+          G7M
E|----7------------3-x-3----3------------x-x-x----|
B|----8-7h8p7------3-x-3----5-3h5p3------3-x-3----|
G|----9-------9-7--4-x-4----4---------4--4-x-4----|
D|----9------------3-x-3----5-------5----4-x-4----|
A|--7-7------------5-x-5--3-3------------x-x-x----|
E|-----------------3-x-3-----------------3-x-3----|

Parte 2 de 2

       D#°          G        Cm6
E|----x------------3-x-3----x---------------------|
B|----7------------3-x-3----8---------------------|
G|----5-5---5-5/7--4-x-4----8-5-4-----------------|
D|----7---7--------5-x-5----7-----7-5-------------|
A|--6-6------------5-x-5----x---------------------|
E|-----------------3-x-3--8-8---------------------|


G                 G7
  A chuva já não cai
                          C7+
Faz tempo aqui no meu sertão
           Cm6                      G
Agora ta pior ter que viver na solidão
                G7
A seca que maltrata
                   C7+
Cada parte desse chão
                   Cm6
Resseca, quebra e mata
                 G
Agora o meu coração

                G7
A chuva já não cai
                          C7+
Faz tempo aqui no meu sertão
          Cm6                       G
Agora ta pior ter que viver na solidão
                G7
A seca que maltrata
                   C7+
Cada parte desse chão
                   Cm6
Resseca, quebra e mata
                      G
Então porque você não
           C7+
Chega pra cá
                G
Sozinho não da não
          C7+
Vem cá morena
                       G
Eu vou ganhar teu coração
           C7+
Chega pra cá
                G
Sozinho não da não
          C7+
Vem cá morena
                       A7  Cm6
Eu vou te dar meu coração
                     G   G7
Eu vou te dar meu coração
                  A7   Cm6
Vou te dar meu coração
              G
Te dar meu coração

Parte 1 de 2

       Fm           G7       C7+          G7M
E|----7------------3-x-3----3------------x-x-x----|
B|----8-7h8p7------3-x-3----5-3h5p3------3-x-3----|
G|----9-------9-7--4-x-4----4---------4--4-x-4----|
D|----9------------3-x-3----5-------5----4-x-4----|
A|--7-7------------5-x-5--3-3------------x-x-x----|
E|-----------------3-x-3-----------------3-x-3----|

Parte 2 de 2

       D#°          G        Cm6
E|----x------------3-x-3----x---------------------|
B|----7------------3-x-3----8---------------------|
G|----5-5---5-5/7--4-x-4----8-5-4-----------------|
D|----7---7--------5-x-5----7-----7-5-------------|
A|--6-6------------5-x-5----x---------------------|
E|-----------------3-x-3--8-8---------------------|

G                G7
  Agora em São Paulo
                  C7+
Eu só penso em você
             Cm6
Eu tava na pior
                      G
E esqueci de te esquecer
                G7
Aí pro meu sertão
                         C7+
Eu já não posso mais voltar
                 Cm6
Vou te pedir de novo
                     G
Então porque você não
           C7+
Chega pra cá
                G
Sozinho não da não
          C7+
Vem cá morena
                       G
Eu vou ganhar teu coração
           C7+
Chega pra cá
                G
Sozinho não da não
          C7+
Vem cá morena
                       A7  Cm6
Eu vou te dar meu coração
                       G  G7
Eu vou te dar meu coração
                  A7   Cm6
Vou te dar meu coração
              G    F6(9) Em7  Cm9
Te dar meu coração

G           F6(9)
  Oh chuva cai
             C9
Molha esse chão
          Cm9       G
E faz brotar esse amor
          F6(9)
Oh chuva cai
             C9
Molha esse chão
        Cm9
E faz brotar
G           F
  Oh chuva cai
             Em7
Molha esse chão
        Cm9            G
E faz brotar esse amor
          F6(9)
Oh chuva cai
             Em7
Molha esse chão
          Cm9
E faz brotar
Faz brotar
                  G   F  Em7  Cm9
Faz brotar esse amor

( G  F6(9)  Em7  Cm9  G )

----------------- Acordes -----------------
A7*  = X 0 2 0 2 0 - (*G#7 na forma de A7)
C7+*  = X 3 2 0 0 X - (*B7+ na forma de C7+)
C9*  = X 3 5 5 3 3 - (*B9 na forma de C9)
Cm6*  = X 3 X 2 4 3 - (*Bm6 na forma de Cm6)
Cm9*  = X 3 1 0 3 X - (*Bm9 na forma de Cm9)
D#°*  = X X 1 2 1 2 - (*D° na forma de D#°)
Em7*  = 0 2 2 0 3 0 - (*D#m7 na forma de Em7)
F*  = 1 3 3 2 1 1 - (*E na forma de F)
F6(9)*  = 1 0 0 0 X X - (*E6(9) na forma de F6(9))
Fm*  = 1 3 3 1 1 1 - (*Em na forma de Fm)
G*  = 3 2 0 0 0 3 - (*F# na forma de G)
G7*  = 3 5 3 4 3 3 - (*F#7 na forma de G7)
G7M*  = 3 X 4 4 3 X - (*F#7M na forma de G7M)
