﻿Onze:20 - Sei Que É Você

Capo Casa 1

[Intro]  Am  F  C  G
         Am  F  C  G  F

Am                      G          F
 Sabe aquela noite que dançamos a toa
        Fm
Nem senti o tempo passar
Am                    G
  Fico imaginando tantos planos
        F
Em seus olhos eu posso enxergar
     Fm
E fugir não vai adiantar

        Dm
Sei que é você
       F             C
E não vem dizer que não, não
     G
Que não, não
      Dm                           G
Eu te amo e ao meu lado é o seu lugar


                  C
Me de uma chance posso te mostrar
                    G
Que o meu amor não vai te machucar
                     Bb                  F
E tudo que eu mais quero nessa vida é você
                C
O que sentimos não dá pra esconder
                  G
E nunca mais vai ter fim
     Bb9
Acredita em mim
     F
Acredita em nós de novo

( Am  F  C  G )
( Am  F  C  G )

        Dm
Sei que é você
       F             C
E não vem dizer que não, não
     G
Que não, não
      Dm                           G
Eu te amo e ao meu lado é o seu lugar

                  C
Me de uma chance posso te mostrar
                    G
Que o meu amor não vai te machucar
                     Bb                  F
E tudo que eu mais quero nessa vida é você
                C
O que sentimos não dá pra esconder
                  G
E nunca mais vai ter fim
     Bb9
Acredita em mim
     F              C  G
Acredita em nós de no-vo
     Bb9
Acredita em mim
     F
Acredita em nós
                C
O que sentimos não dá pra esconder
                  G
E nunca mais vai ter fim
     Bb9
Acredita em mim
     F              Am
Acredita em nós de novo

----------------- Acordes -----------------
Capotraste na 1ª casa
Am*  = X 0 2 2 1 0 - (*A#m na forma de Am)
Bb*  = X 1 3 3 3 1 - (*B na forma de Bb)
Bb9*  = X 1 3 3 1 1 - (*B9 na forma de Bb9)
C*  = X 3 2 0 1 0 - (*C# na forma de C)
Dm*  = X X 0 2 3 1 - (*D#m na forma de Dm)
F*  = 1 3 3 2 1 1 - (*F# na forma de F)
Fm*  = 1 3 3 1 1 1 - (*F#m na forma de Fm)
G*  = 3 2 0 0 0 3 - (*G# na forma de G)
