﻿Onze:20 - Meu Lugar

Capo Casa 1

Primeira Parte:

                          Bm
Você já sabe que em qualquer lugar
                     A9
Eu tô pensando em você
                            Bm
Por mais distante que eu pareça estar
                     A9
Eu tô pensando em você
                     Bm
Há tanta coisa fora do lugar
                            A9
Que eu tento arrumar por você
                     Bm
Só pra poder te mostrar

Que ao seu lado é o meu lugar
                       A9
E que não há mais ninguém



Refrão:

                           D9
Pensa em mim que eu tô chegando agora
E                      A9
  Não precisa mais chorar
                          D9
É que a saudade eu já deixei lá fora
            E               A9
Espera, eu não demoro a voltar
                           D9
Pensa em mim que eu tô chegando agora
E                      A9  E/G#
  Não precisa mais chorar
F#m                         Bm
  É que a saudade eu já deixei lá fora
            E               A9
Espera, eu não demoro a voltar


Solo: Bm A9


Repete a Primeira Parte:

                          Bm
Você já sabe que em qualquer lugar
                     A9
Eu tô pensando em você
                            Bm
Por mais distante que eu pareça estar
                     A9
Eu tô pensando em você
                     Bm
Agora eu tô tentando arrumar
                         A9
Um jeito de ficar com você
                     Bm
E pra nunca mais partir

Eu só quero é ser feliz
            A9
Com você e mais ninguém


Repete o Refrão:

                           D9
Pensa em mim que eu tô chegando agora
E                      A9  E/G#
  Não precisa mais chorar
F#m                         Bm
  É que a saudade eu já deixei lá fora
            E               A9
Espera, eu não demoro a voltar


Segunda Parte:

D9
  Eu não quero ser só mais um
        A9
Na sua vida
D9
  Voltando pra casa eu já sei
      A9    F#m
Onde vou ficar
D9                                A9
  Dormir e acordar com você todo dia
       G             D9       A9
E pra não ter mais que te deixar
       G          D9    E      F#
E pra nunca mais te deixar.... ah


Refrão Final:

                           E
Pensa em mim que eu tô chegando agora
F#                     B9  F#/A# G#m
  Não precisa mais chorar
                          C#m
É que a saudade eu já deixei lá fora
            F#              E  Em  B9
Espera, eu não demoro a voltar

----------------- Acordes -----------------
Capotraste na 1ª casa
A9*  = X 0 2 2 0 0 - (*A#9 na forma de A9)
B9*  = X 2 4 4 2 2 - (*C9 na forma de B9)
Bm*  = X 2 4 4 3 2 - (*Cm na forma de Bm)
C#m*  = X 4 6 6 5 4 - (*Dm na forma de C#m)
D9*  = X X 0 2 3 0 - (*D#9 na forma de D9)
E*  = 0 2 2 1 0 0 - (*F na forma de E)
E/G#*  = 4 X 2 4 5 X - (*F/A na forma de E/G#)
Em*  = 0 2 2 0 0 0 - (*Fm na forma de Em)
F#*  = 2 4 4 3 2 2 - (*G na forma de F#)
F#/A#*  = 6 X 4 6 7 X - (*G/B na forma de F#/A#)
F#m*  = 2 4 4 2 2 2 - (*Gm na forma de F#m)
G*  = 3 2 0 0 3 3 - (*G# na forma de G)
G#m*  = 4 6 6 4 4 4 - (*Am na forma de G#m)
