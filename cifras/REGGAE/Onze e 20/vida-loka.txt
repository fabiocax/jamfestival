Onze:20 - Vida Loka

Eu vou vivendo ao vivo essa vida loka
Fazendo um bom uso da palavra fé
Não quero mais saber desse seu bate boca
Eu vim pra te mostrar que ainda tô de pé

      G#m                F#
Eles dizem por aí que a fome é o que mata
      C#m7                      D#m7
Matam pra sobreviver antes que sorte mesmo faça
      G#m                        F#
O seu erro é pensar que o povo é só mais um primata
     C#m7                    D#m7
Que tem que se calar pra não falar e obedecer

          G#m
Eu vou mandar dizer
         F#                      C#m7
Pro meu povo que a gente não vai mais sofrer
         E7M
Que o caminho agora é outro
         G#m
Eu vou viver pra crer

 F#                              C#m7
Que o meu santo é forte e vai me proteger
        E7M                    (G#m / % / % / % )
Eu vou sair desse sufoco

        G#m
O rei mandou a gente se ajudar
              F#
Fazer um só caminho em um só coração
             C#m7
Ter a sabedoria pra poder lutar
              D#m7
E a sua mente aberta em forma de oração

            G#m
As suas atitudes vão fazer valer
            F#
As suas chances de poder ver
      C#m7
Classe a, classe b

Todos juntos em um c
        D#m7
Vem que tá, neguinho

É só querer

          G#m
Eu vou mandar dizer
         F#                      C#m7
Pro meu povo que a gente não vai mais sofrer
         E7M
Que o caminho agora é outro
         G#m
Eu vou viver pra crer
 F#                              C#m7
Que o meu santo é forte e vai me proteger
        E7M
Eu vou sair desse sufoco

( G#m7  D#m7  E7M  D#m7 ) 4x

          G#m
Eu vou mandar dizer
         F#                      C#m7
Pro meu povo que a gente não vai mais sofrer
         E7M
Que o caminho agora é outro
         G#m
Eu vou viver pra crer
 F#                              C#m7
Que o meu santo é forte e vai me proteger
        E7M
Eu vou sair desse sufoco

   G#m        F#       C#m7
Eu vou... Eu vou... Eu vou
        E7M
Eu vou sair desse sufoco


   G#m        F#       C#m7
Eu vou... Eu vou... Eu vou
        E7M
Eu vou sair desse sufoco

----------------- Acordes -----------------
C#m7 = X 4 6 4 5 4
D#m7 = X X 1 3 2 2
E7M = X X 2 4 4 4
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
G#m7 = 4 X 4 4 4 X
