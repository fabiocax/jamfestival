Mato Seco - Levante a Cabeça (Jah Não Vai Te Deixar)

G               C
Eu ouvi você chorar
                      G
Isso me faz chorar também
                   C
Ainda é tempo pra sonhar
                     G
Enxugue suas lágrimas

            C              G
Bate a poeira, vai adiante
                C
Você não pode parar
                G
Você não pode parar
              C
Não pense em parar

D             G
Levante a cabeça
               D
Nunca se esqueça

  C              G
Jah não vai te deixar

G                C
Eu ouvi você chorar
                     G
Isso me faz chorar também
                     C
Ainda é tempo pra sonhar
                   G
Enxugue suas lágrimas

G               C             G
Quebre as algemas, vai adiante
               C
Você precisa voar
               G
Você precisa voar
                     C
O céu não é o limite
         C
Você pode voar

D             G
Levante a cabeça
                D
Nunca se esqueça
  C                 G
Jah não vai te deixar

D             G
Levante a cabeça
                D
Nunca se esqueça
   C                G
Jah não vai te deixar
                   D
Jah não vai te deixar
                   G
Jah não vai te deixar
                   D
Jah não vai te deixar
                   G
Jah não vai te deixar

  D
Levante a cabeça
  C                G
Jah não vai te deixar!

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
