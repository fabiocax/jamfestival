Mato Seco - Farinha Pouca

Intro 2x: Bm  Am  Bm  Em  F#m

Bm
(ritmo e pausa)
Em                             A                                 Bm
OOOOOOOOOOO  OOOOOOO OOOOôOOOOOOO
Em                             A                                Bm
OOOOOOOOOOO  OOOOOOO OOOOôOOOOOOO
Bm                      Em
OOOOOOOOO Nanananananana oo
Bm                      Em
OOOOOOOOO Nanananananana oo
Bm                      Em
OOOOOOOOO Nananananananáaa
Bm                                                        Em
Há dias que penso que a casa vai cair
Am                                                             Bm
Tem horas que dá vontade de largar tudo e sumir
Em                                                           Bm
Mas logo me pego com fé em deus. Vejo que não vale a pena
               G                    F#m                             Bm
Que não é fugindo que se resolve os problemas

Bm                                                             G
Por que não podemos viver em paz?  Espalhar o amor, crescer em União?
A                                                       Bm
Por que é tão difícil estender as mãos?
Em                                                              Bm
Grandes são os olhos da indiferença , até de nossos governantes
              G                                  F#m                    Bm
Orgulho e vaidade que nos fazem ser tão distantes
Em                                                      D
Farinha pouca, vamos dividir o pirão
                                                        G
Afinal de contas somos todos irmãos
                 F#m                  Bm
E barriga cheia não reclama
Em                                                  Bm
Farinha pouca, vamos dividir o pão
                                                         G
Afinal de contas somos todos irmãos
                 A                 Bm
E barriga cheia não reclama
Bm                                                    Em
Ralar, ralar, ralar, ralar pra ter o pão
Bm                                                      Em
Ralar, ralar, ralar, ralar pra ser um bom cidadão
Em                                                                       Bm
A fome quando bate parece que vai me derrubar
                G          F#m                            Bm
A luta é dura, eu sei, mas a vitória é certa
Em                                                                      Bm
A fome quando bate parece que vai me derrubar
             G               F#m                          Bm
A luta é árdua, eu sei, mas a vitória é certa
Em                                                    D
Farinha pouca, vamos dividir o pirão
                                                        G
Afinal de contas somos todos irmãos
                  F#m                 Bm
E barriga cheia não reclama
Em                                                   Bm
Farinha pouca, vamos dividir o pão
                                                           G
Afinal de contas somos todos irmãos
                F#m                   Bm
E barriga cheia não reclama
                                    Em                                Bm
Nós temos que nos juntar Temos que juntos ser
                                  Em                                 Bm
Temos que nos juntar Temos que juntos ser
 Em                   Bm               Em                          Bm
mais fortes  Mais fortes, mais fortes, mais fortes
Em                              A                    Bm
OOOOOOOOOOO  OOOOOOO OOOO OOOOOOO
Em                              A                   Bm
OOOOOOOOOOO  OOOOOOO OOOO OOOOOOO
Em                             A                    Bm
OOOOOOOOOOO  OOOOOOO OOOO OOOOOOO
Em                             A                    B
OOOOOOOOOOO  OOOOOOO OOOO OOOOOOO

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
