Bob Marley - Real Situation

Intro: G C  G C

G                       C
Check out the real situation
G                   C
Nation war against nation
G                   D  Em             C
Where did it all begin, when will it end?
        G                 D                    G      C
Well it seem like total destruction the only solution
                   G       C                 G   C
And there ain't no use, no one can stop them now
         G     C                  G   C
Ain't no use nobody can stop them now

Give them an inch they take a yard
Give them yard they take a mile
Once a man and twice a child, and everything is just for a while
It seem like total destruction the only solution

And there ain't no use, no one can stop them now

Ain't no use, nobody can stop them now
Ain't no use, no one can stop them now
Ain't no use, no one can stop them now

Check in the real situation (check it out, check it out)
Nation fight against nation
Where did this all begin, where will it end?
Well it seem like total destruction the only solution
Oh, no use, can't stop them
Ain't no use, can't stop them
Ain't no use, no one can't stop them now
Can't stop them now, no one can stop them now
Ain't no use, no one can't stop them now
Everybody's stuck, ain't no use, ain't no use
Ain't no use, you even try
Ain't no use, got to say bye bye
Ain't no use, ain't no
Ain't no use, no one can stop them now
No one can stop them now...

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
