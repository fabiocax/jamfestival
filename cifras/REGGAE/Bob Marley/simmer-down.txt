Bob Marley - Simmer Down

 A# D# F

Simmer down, you lickin' too hot, so
Simmer down, soon you'll get dropped, so
Simmer down, can you hear what I say
Simmer down, that why won't you, why won't you, why won't you simmer down
Simmer down
Long time people dem used to say
What sweet nanny goat a go run him belly, so
Simmer down, oh control your temper
Simmer down, for the battle will be hotter
Simmer down, can you hear what I say
Simmer down, oh that I'm a leaving you today
Simmer down

Simmer down, oh you hear what I say
Simmer down, a that I'm leaving you today
Simmer down, can you hear what I say
Simmer down

Chicken Mary, hawk de near and when him de near, you must beware, so

Simmer down, oh control your temper
Simmer down, for the battle will be hotter
Simmer down, and you won't get no supper
Simmer down, and you know you bound to suffer
Simmer down, simmer, simmer, simmer right down
Simmer down, like you never did before
Simmer down, oh, oh, oh
Simmer down, can you hear what I say
Simmer down, you lickin' too hot so
Simmer down, and you know soon you'll get dropped, so
Simmer down, why won't you simmer, simmer down
Simmer down, simmer down, simmer down
Simmer down, simmer, simmer down
Simmer down, oh simmer, simmer down
Simmer down, like you never did before...

e o baixo segue assim
G|---------3--------2-----------0--------------2-|
D|---0--3-----1--------------0-----0-----1--3----|
A|-1-------------3-----3--1-----------3----------|
E|-----------------------------------------------|

G|-3--------------------------------------------3-|
D|----3--0--------1--3--0-----3--0--------1--3----|
A|----------1--3-----------1--------1--3----------|
E|------------------------------------------------|

----------------- Acordes -----------------
A# = X 1 3 3 3 1
D# = X 6 5 3 4 3
F = 1 3 3 2 1 1
