Bob Marley - The Heathen

intro: Em C G

Em C G

Rise up fallen fighters
Rise and take your stance again.
'Cause he who fight and run away
Live to fight another day.

Jah put the Heathen back
There upon the wall
Jah Jah put the Heathen back
There 'pon the wall

As a man sow, shall he reap
And I know that talk is cheap.
So the hotter the battle
Is the sweeter the victory.

Rise up freedom fighters
Rise and take your stance again.

'Cause he who fight and run away
Live to fight another day.

Jah put the Heathen back
There 'pon the wall
Jah Jah put the Heathen back
There 'pon the wall

----------------- Acordes -----------------
C = X 3 2 0 1 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
