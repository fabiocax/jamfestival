Bob Marley - Who The Cap Fit

(Bb                  Cm)
Man to man is so unjust, children
You don't know who to trust
Your worst enemy could be your best friend
And your best friend your worst enemy
(Eb                         F)
Some will eat and drink with you
Then behind them su-su 'pon you
Only your friend know your secrets
So only he could reveal it
(Gm                                  Cm)
And who the cap fit, let them wear it (2x)
Said I throw me corn, me no call no fowl
I saying, "cok-cok-cok, cluck-cluck-cluck"
(Bb                    Cm)
Some will hate you, pretend they love you now
Then behind they try to eliminate you
But who Jah bless, no one curse
Thank God we're past the worse
(Eb                   F)
Hypocrites and parasites

Will come up and take a bite
And if your night should turn to day
A lot of people would run away
(Gm                                  Cm)
And who the cap fit let them wear it (2x)
And then a gonna trhow me corn
And then a gonna call no fowl
And then a gonna "Cok-cok-cok, cluck-cluck-cluck"
(Eb                   F)
Some will eat and drink with you
And then behind them su-su 'pon you
And if your night should turn to day
A lot of people will run away
(Gm                                  Cm)
And who the cap fit, let them wear it (2x)
I throw me corn, me no call no fowl
I saying "Cok-cok-cok, cluck-cluck-cluck"

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
