Bob Marley - Redemption Song

[Intro]

E|---------------------------------------------------------|
B|---------------------------------------------------------|
G|---------------------------------------------------------|
D|-------------1/2--0------------------0-------------------|
A|---0-2----2/3-------2-------0--2-------2/3/2-0-----------|
E|-3------3-----------------3---------------------3--------|

   G                   Em7
Old pirates, yes, they rob I
     C          G/B      Am
Sold I to the merchant ships
 G                     Em
Minutes after they took I
C        G/B      Am
From the bottomless pit
        G       Em7
But my hand was made strong
C      G/B           Am
By the hand of the Almighty

    G                  Em      C           D
We forward in this generation...triumphantly

[Refrão]

                   G       C
Won't you help to sing
 D               G
These songs of freedom?
        C     D       Em
'Cause all I ever have
C  D          G      C     D
Redemption songs

      G                             Em7
Emancipate yourselves from mental slavery
           C           G/B       Am
None but ourselves can free our minds
         G                Em
Have no fear for atomic energy
       C                G/B       Am
'Cause none of them can stop the time
        G                      Em7
How long shall they kill our prophets
           C    G/B       Am
While we stand aside and look?
      G               Em
Some say it's just a part of it
       C                  D
We've got to fulfill the book

[Refrão]

                   G       C
Won't you help to sing
 D               G
These songs of freedom?
        C     D       Em
'Cause all I ever have
C  D          G  C  D  G  C  D
Redemption songs

      G                             Em7
Emancipate yourselves from mental slavery
           C           G/B       Am
None but ourselves can free our minds
         G                Em
Have no fear for atomic energy
       C                G/B       Am
'Cause none of them can stop the time
        G                      Em7
How long shall they kill our prophets
           C    G/B       Am
While we stand aside and look?
      G                  Em
Oh,some say it's just a part of it
       C                  D
We've got to fulfill the book

[Refrão]

                   G       C
Won't you help to sing
 D               G
Another song of freedom?
        C     D       Em
'Cause all I ever have
C  D          G       C
Redemption songs
 D               G        C
These songs of freedom
 D               G
These songs of freedom
 C         Em
All I ever have

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
