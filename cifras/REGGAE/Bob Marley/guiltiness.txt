Bob Marley - Guiltiness


Versos: Dm : Dm : Am : Am
Ponte: C : Am : F : G

Guiltiness (talkin' 'bout guiltiness)
Pressed on their conscience. Oh yeah.
And they live their lives (they live their lives)
On false pretence everyday - each and everyday. Yeah.

These are the big fish
Who always try to eat down the small fish, just the small fish.
I tell you what: they would do anything
To materialize their every wish. Oh yeah-eah-eah-eah. 

Say: Woe to the downpressors:
They'll eat the bread of sorrow!
Woe to the downpressors:
They'll eat the bread of sad
Woe to the downpressors:
They'll eat the bread of sorrow!
Oh, yeah-eah! Oh, yeah-eah-eah-eah!


Guiltiness
Pressed on their conscience. Oh yeah. Oh yeah.
These are the big fish
Who always try to eat down the small fish,
just the small fish.

I tell you what: they would do anything
To materialize their every wish. Oh, yeah-eah-eah-eah-eah-eah. 

But: Woe to the downpressors:
They'll eat the bread of sorrow!
Woe to the downpressors:
They'll eat the bread of sad
Woe to the downpressors:
They'll eat the bread of sad
Oh, yeah-eah! Oh yeah-e-e-e-e-e!

Guiltiness. Oh yeah. Ah!
They'll eat the bread of sorrow everyday

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
