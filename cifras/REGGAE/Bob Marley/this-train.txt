Bob Marley - This Train

G          C
This train - mm - mm - mm
G             C              G
This train is bound to glory - this train
C                      G                C                     G
Talkin' 'bout ma: this train is bound to glory - this train:
G                Em
This train is bound to glory,
C
This train carry no un'oly;
G            C                            G         C
This train - I'm a-talkin' 'bout: this train.
G               C
When I was just a kid,
       G                             C
My mommy used to sing this song.
          Em             D
Now I've grown to be a man,
             C     D             G
Well, it still lingers deep down in my soul;
             C      D            G
I said, it still lingers deep down in my soul.

C                   G          C                    G    C    G
I'm talkin' 'bout: this train is bound to glory - this train;
   G             C                   G
This train is bound to glory - this train;
G                C
This train is bound to glory,
D
O dis 'ere train carry no un'oly.
G               D               G       D
This train - I'm a-talkin' 'bout: this train.

G     C     G

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
