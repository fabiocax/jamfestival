Bob Marley - We And Dem

Intro: Bm
Am
We no know how we and dem a-go work this out, oy!
We no know how we and dem a-go work it out.
                  Bm      Em
But someone will 'ave to pay
C                  D
For the innocent blood
       Bm            Em
That they shed every day,
     C                 D
Oh, children, mark my word;
              Bm    Em          C
It's what the Bible say, yeah! yeah!
Am
Oh, we no know how we and dem a-go work this out;
We no know how we and dem a-go work it out.
              Bm      Em
But in the beginning Jah created everythin' (oo-oo-oo-oo-ooh),

Givin' man dominion over all things.

  Bm            Em
But now it's too late,
 C                 D
You see, men has lost their faith (oo-oo-oo-oo-ooh), hey-ey-hey!

Eating up all the flesh from off the earth, eh!
Am
We no know how we and dem a-go work this out (we no know);
We no know how we and dem a-go work this out (no, we don't know);

Bm      Em
Dub-da-dub-da-dub!

Dub-a-dippidi-doop-de-doo!
Nim-a-nummany num-na-noo!
Num-a-nummana-num-da-nay!
Dippy-dippy-dippy: dip-de-dub-de-day!
Dip-a-dubbada-dub-da-day!

But I say: we no know how we and dem a-go work it out:
them a flesh and bone!
We no know how we and dem a-go work it out.

But we now have no friends
In-a high society, yea-a-eah!
We no have no friends;
Oh, mark my identity;
We no have no friends. Oh-oh! Yeah!

We no know how we and dem a-go work this out (no, we no know);
We no know how we and dem a-go work this out (we no know);
We no know how we and dem a-go work it out (no, we don't know);
(We no know how we and dem a-go work this out.) Rasta!
(We no know how we and dem a-go work this out.) Ilyabom!

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
