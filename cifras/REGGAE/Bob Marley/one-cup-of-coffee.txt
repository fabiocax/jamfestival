Bob Marley - One Cup Of Coffee

Intro:  D  A  D  G  D  A  D

 D                                                                 A
One cup of coffee, then I'll go;Though I just dropped by to let you know
         D                              G
That I'm leaving you tomorrow;I'll cause you no more sorrow:
D          A                 D
One cup of coffee, then I'll go.
D                                                                                          A
I brought the money like the lawyer said to do,But it won't replace the eartache I caused you;
D                                       G
It won't take the place of lovin' you, I know,
D             A                 D
So one cup of coffee, then I'll go.

Solo: D  A  D  G  D  A  D

G                               D
Tell the kids I came last night And kissed them while they slept;
G                             D                                   A
Make my coffee sweet and warm Just the way you used to lie in my arms.

D                                              D                                             A
I brought the money like the lawyer said to do,But it won't replace the eartache I caused you;
D                                        G
It won't take the place of lovin' you, I know,
   D          A                 D
So one cup of coffee, then I'll go.
   D          A                 D
one cup of coffee, then I'll go.
   D          A                 D
one cup of coffee, then I'll go.


----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
