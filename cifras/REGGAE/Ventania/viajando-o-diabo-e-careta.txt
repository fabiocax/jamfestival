Ventania - Viajando (O Diabo É Careta)

(intro 2x)  Am  F

Am              F
Lai larai larai larai...(2x)

Am      F
Viajando...
Am      F
Viajando...
Am                   F
No brilho dessa morena amanheci no verão
Am                   F
Cada vez mais descacetado da cabeça
Am                   F
Maldita flor da trombeta me pirou de vez
Am                   F
E me levou para o inferno sem minha lucidez

Am                 F
Fui perguntá pro Diabo se ele fumava um
Am                 F
Fiz da canoa uma seda eu não me toquei

Am                       F
Foi quando ele me respondeu:
F                  F
Muito obrigado amigo, mas eu sou careta !

Am                  F
Ai Meu Deus o Diabo é careta!
Am                   F
Te denuncio pra galera, seu capeta
Am                  F
Toma esse chá de cogumelo com trombeta
Am                   F
Se não arranco esse seu rabo seu careta
Am                   F
Se não arranco esse seu rabo..... seu careta
Am              F
Lai larai larai larai...

Am      F
Viajando...
Am      F
Viajando...
Am                   F
No brilho dessa morena amanheci no verão
Am                   F
Cada vez mais descacetado da cabeça
Am                   F
Maldita flor da trombeta me pirou de vez
Am                   F
E me levou para o inferno sem minha lucidez

Am      F
Viajando...
Am      F
Viajando...

----------------- Acordes -----------------
Am = X 0 2 2 1 0
F = 1 3 3 2 1 1
