Chimarruts - Sentimento Verdadeiro

E7M - F# - G#m7

Nem todo dia é como hoje
Por isso faça tudo o que quizer fazer
Pode amanhã o dia nascer chovendo
Ou o sol atrás da nuvem se esconder
Diga eu te amo e muito obrigado
À todo ser que faz a vida florescer
Cante pelas ruas não tranque o sentimento
Que é verdadeiro e vem de dentro de você

C#m7 -          D#m7 -            G#m7 - Bb - G#m7 - Bb - G#m7
Deixe os seus sonhos voarem com o vento

C#m7 -          D#m7 -            G#m7 - Bb - G#m7 - Bb - G#m7
Humildade é o segredo para um bom momento

C#m7 -          D#m7 -            G#m7 - Bb - G#m7 - Bb - G#m7
Igualdade e liberdade pro seu pensamento

C#m7 -          D#m7 -            G#m7 - Bb - G#m7 - Bb - G#m7
Muita paz e humildade pro seu sentimento


----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C#m7 = X 4 6 4 5 4
D#m7 = X X 1 3 2 2
E7M = X X 2 4 4 4
F# = 2 4 4 3 2 2
G#m7 = 4 X 4 4 4 X
