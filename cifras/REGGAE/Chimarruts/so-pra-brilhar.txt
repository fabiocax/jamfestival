Chimarruts - Só Pra Brilhar

Intro 2x: Em  A

 Em
Meu amor
   A
O peito aperta quando chega o fim
 Em                   A
De uma história que vai deixar saudade
 Em
Mas meu amor
      A
Pra você sou eu quem causa esse fim
 Em               A
E o porque, a razão da tua ansiedade

 Em
Você me disse

O que eu não disse
                           A
Você ouvi o que ninguém falou

       Em
Mas estava tudo as claras
                           A
Só você mesmo não me enxergou

 Em
Você me disse

O que eu não disse
                              A
Você pensou o que ninguém pensou
       Em
Mas não é que deu em nada
                           A
Veja o que o destino aprontou

 Em
Tudo tem que ser

A gente tem que crer
   A
Errar também nos fez crescer
 Em
Não basta só querer

A gente tem que ser
 A
O amor está em todo lugar

 Em
Tudo é pra aprender

A gente tem que crer
  A
Errar também nos fez crescer
 Em
Não basta só querer

A gente tem que ser
  A
Um só, um sol, só pra brilhar

 Em                                  A
Só pra brilhar brilhar brilhar, só pra brilhar (4x)

----------------- Acordes -----------------
A = X 0 2 2 2 0
Em = 0 2 2 0 0 0
