Chimarruts - Do Lado de Cá

[Intro] C  G  Am  F
        C  G  Am  F

              C                      G
Se a vida as vezes dá uns dias de segundos cinzas
           Am              F
E o tempo tic taca devagar
         Dm                   Am
Ponha o teu melhor vestido, brilha teu sorriso
      F              C
Vem pra cá, vem pra cá

                                     G
Se a vida muitas vezes só chuvisca, só garôa
        Am               F
E tudo não parece funcionar
         Dm                  Am
Deixe esse problema a toa, pra ficar na boa
      C
Vem pra cá


[Refrão]

            Am              F
Do lado de cá, a vista é bonita
          C            G
A maré é boa de provar
            Am                 F
Do lado de cá, eu vivo tranquila
                  C          E7
E o meu corpo dança sem parar
            F                  C                  G
Do lado de cá, tem música, amigos e alguém para amar
            C  G  Am  F
Do lado de cá
            C  G  Am  F
Do lado de cá

              C                      G
Se a vida as vezes dá uns dias de segundos cinzas
           Am              F
E o tempo tic taca devagar
         Dm                   Am
Ponha o teu melhor vestido, brilha teu sorriso
      F              C
Vem pra cá, vem pra cá

                                     G
Se a vida muitas vezes só chuvisca, só garôa
        Am               F
E tudo não parece funcionar
         Dm                  Am
Deixe esse problema a toa, pra ficar na boa
      C
Vem pra cá

[Refrão]

            Am              F
Do lado de cá, a vista é bonita
          C            G
A maré é boa de provar
            Am                 F
Do lado de cá, eu vivo tranquila
                  C          E7
E o meu corpo dança sem parar
            F                  C                  G
Do lado de cá, tem música, amigos e alguém para amar
            Am
Do lado de cá

[Segunda Parte]

             Am7M            Am7
A vida é agora, vê se não demora
  Dm          E7
Pra recomeçar
Am              Am7M        Am7
 É so ter vontade de felicidade
  Dm      G
Pra pular

[Refrão]

            Am              F
Do lado de cá, a vista é bonita
          C            G
A maré é boa de provar
            Am                 F
Do lado de cá, eu vivo tranquila
                  C          E7
E o meu corpo dança sem parar
            F                  C                  G
Do lado de cá, tem música, amigos e alguém para amar
            C  G  Am  F
Do lado de cá
            C  G  Am  F
Do lado de cá

            C  G  Am  F
Do lado de cá
            C  G  Am  F
Do lado de cá

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Am7M = X 0 2 1 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
