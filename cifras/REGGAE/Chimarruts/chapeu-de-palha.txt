Chimarruts - Chapéu de palha

(intro 2x) Am7 D7/9 Bm7 Em Am7 D7/9 G G/C G

G                     Am7
Eu tava de chapéu de palha
     D7/9         Bm7   Em
Na praia tomando Sol
                     Am7         D7/9         G  C  G
Se vier meu bem não vaia , não caia no meu anzol (2x)


                  Am7                    D7/9               Bm7   Em
E outro dia de repente tudo estava tão quente eu queria refrescar
                       Am7               D7/9                G
E todo mundo foi pra praia ela de mini saia ficou a olhar o mar
                      Am7                 D7/9               Bm7   Em
E de repente a gente sente, tem algo diferente pairando pelo ar...
                  Am7                D7/9                  G  C  G
É um olhar desafinado, fugindo pelo lado, querendo me acertar...


(refrão 2x)


                 Am7                      D7/9                  Bm7   Em
Em Ipanema Lami beach, eu vou sem ter convite, não precisa nem pagar
                     Am7                  D7/9               G
Leve um VT, perna e galo depois vai no embalo, não pode vacilar
                     Am7                          D7/9                Bm7   Em
Pois na areia e na piscina a gente nunca perde a rima, nunca para de cantar
                         Am7                      D7/9                      G  C  G
Salto mortal, chicote e ponta, barrigaço além da conta quanta gente já foi lá...


(refrão 2x)

                  Am7                    D7/9               Bm7   Em
E outro dia de repente tudo estava tão quente eu queria refrescar
                       Am7               D7/9                G
E todo mundo foi pra praia ela de mini saia ficou a olhar o mar
                      Am7                 D7/9               Bm7   Em
E de repente a gente sente, tem algo diferente pairando pelo ar...
                  Am7                D7/9               G  C  G
É um olhar desafinado, fugindo pelo lado, querendo me acertar...

(refrão 2x)

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
D7/9 = X 5 4 5 5 X
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G/C = X 3 5 4 3 3
