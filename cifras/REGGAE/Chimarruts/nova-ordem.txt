Chimarruts - Nova Ordem

Solo na introdução e no meio da musica
E|-12/14-14-14----12--10------10-8---9-------12/14-14-14----12--10----10-8--9-------|
B|-13/15-15-15--13--10------7--------10b10---13/15-15-15--13--10----7-------10------|
G|----------------------------------------------------------------------------------|
D|----------------------------------------------------------------------------------|
A|----------------------------------------------------------------------------------|
E|----------------------------------------------------------------------------------|

D                       G
Se liga na idéia abra a mente
                 A
deixa o reggae rolar
G
(por aí)
D
quem canta se liberta
       G                   A
quem escuta chega perto de jah
G
(evoluir)
D                    G                      A
há uma nova ordem na terra vinda de outro lugar

G
(é só sentir)
D                       G                   A
que a vida conspira a favor para quem sabe amar
G
(e amar, e amar, e amar)
D
Caia, levante
   G
releve a vibração
   A
a nova ordem agora
     G
é ajudar o seu irmão
     D
humildade é saber viver
    G
pra frente e ser feliz
A
quem luta no from da vida
        G
que é difícil persistir
         D
por isso tente entender
      G
que a vida é muito mais
    A
não quero ver você
    G
dis-distante da paz
D           G
siga, liga, vê-vê se se liga
A
saiba que o amor
             G
é o que mais vale nessa vida
           D        G
sei que o amor traz paz
           A     G
sei que o amor é mais
           D        G
sei que o amor traz paz
           A     G
sei que o amor é mais

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
