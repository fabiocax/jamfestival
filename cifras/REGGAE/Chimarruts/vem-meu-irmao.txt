Chimarruts - Vem meu irmão

Int. A D (A D C#m Bm 2X)(A D C#m Bm A G)

(A D)
De tarde eu quero, quero ver o sol...(iéieieié)

Me ajude eu nao quero mais ser tao igual

De tarde o meu sonho vai ser tão real (eu sei)

Todo mundo cantando isso nao tem igual (Ohh)

Ref.
(A D C#m Bm 2X)(A D C#m Bm A G)
Vem, vem meu irmão

Vamos cantar

Com o coração...

Vem, deixa pra traz


Tudo de mal...

E vem meu irmão...

Bm7                              C#m7
Nessa tarde gostosa que é de verão
                                 Bm7
Tratamento com ervas traz a sensação
                                  C#m7
Que o mundo é perfeito pois somos irmãos
             Bm7
Nao só de cor...
             C#m7
Nao,não só de cor...
        Bm7       C#m7 (DEIXA SOAR)
Mas de amor...de amor...

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
G = 3 2 0 0 0 3
