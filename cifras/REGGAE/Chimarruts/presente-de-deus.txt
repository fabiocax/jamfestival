Chimarruts - Presente de Deus

 Dm
Paz e amor

É o que precisamos
    Bb
Persista,
      A
Ponha isso nos seus planos
Dm
 Uma porção de amor
Bb             A
 Uma pitada de alegria
     Dm
Pois isso acaba com a dor
    Bb                  A
E sacia a fome do seu dia-a-dia
Dm
 Misture seus sentimentos, vitórias
Bb
 Mas não se esqueçam
        A
Que virtudes não são glórias

Dm
 Se você é mais bondoso que o irmão
Bb
Tente, se esforce
       A
Mostre outra direção
Dm
 Mas se você não for
           Bb
Também vá lá
                A
E mostre que você
                 Dm  (Riff)
Está querendo mudar
      Bb          A
Já mudou, pra melhor

     Dm
Eu vou pegar

Carona numa nuvem
   Bb                       A
E vou voar com ela por tudo isso daqui
  Dm
E lá de cima eu vou mostrar pro homem
      Bb                A
Que não é tão difícil de viver feliz
Dm
 Um presente de Deus
Bb          A
 Sete cores belas
Dm
 Trazem alegria pro céu
Bb              A
 E harmonizam a Terra
       Dm     Bb  A  (Riff 2x)
Arco íris e eu
       Dm     Bb  A
Arco íris e eu
Riff:
     Dm
E|---------------------------------|
B|---------------------------------|
G|---------------------------------|
D|---------7-7-------------7-7-----|
A|-5-5-8-8-----8-8-5-5-8-8-----8-8-|
E|---------------------------------|

     Bb              A
E|---------------------------------|
B|---------------------------------|
G|---------------------------------|
D|---------------------------------|
A|-----5-5-8-8-5-5-----4-4-7-7-4-4-|
E|-6-6-------------5-5-------------|


----------------- Acordes -----------------
A = X 0 2 2 2 0
Bb = X 1 3 3 3 1
Dm = X X 0 2 3 1
