Ponto De Equilibrio - Ame sua missão

Intro: A#m / G#

   A#m                      G#
Tudo o que sofri, eu sei
A cruz que carreguei
Jah deu pra mim, sabendo que eu podia agüentar

  Fm                  Ebm
A Força Maior está presente
E me presenteou com a vida!

       A#m                    G#
Quem bate em minha porta é Jah
Então pode entrar, pode entrar

Quem bate em minha porta é Jah
Então pode entrar, pode entrar
     A#m                                 G#

A resistência contida em nossas canções
A força contida em nossas canções

A luz contida em nossas canções
Informação contida em nossas canções
A criatividade contida em nossas canções
A cura contida em nossas canções
A vida contida em nossas canções
Vem do Reino de Jah, de Jah irmão, de Jah, de Jah irmão!

   A#m                       G#
Tudo o que sofri, eu sei
A cruz que carreguei
Jah deu pra mim, sabendo que eu podia agüentar

    Fm                            Ebm
A Força Maior está presente
E me presenteou com a vida!

  A#m                          G#
Quem bate em minha porta é Jah
Então pode entrar, pode entrar

Quem bate em minha porta é Jah
Então pode entrar, pode entrar

Lululaiê!
Se a missão foi posta em suas mãos,
Não desista agora, não desista não
Pois o caminho a seguir
É o caminho da salvação (salvação, salvação)
De mais um irmão
Mais um irmão é sempre mais um irmão.
Mais um irmão é sempre mais um irmão!
Aceite seu irmão, ame seu irmão, todos somos irmãos
Ame seu irmão, ame seu irmão, ame seu irmão, ame!

Mas o Leão de Judá rugiu pra lhe dizer, que o amor
Que o amor está no viver
Precisamos amar, precisamos amar!
Ame mais seu irmão!

----------------- Acordes -----------------
A#m = X 1 3 3 2 1
Ebm = X X 1 3 4 2
Fm = 1 3 3 1 1 1
G# = 4 3 1 1 1 4
