Ponto De Equilibrio - Ditadura da televisão

Intro: C / Em / C / Em / C / D / A / C / D / A

C       Bm     Em             C           Bm    Em
  Na infância você chora, te colocam em frente da TV
C             Bm     Em                   C     Bm  Em
  Trocando as suas raízes por um modo artificial de se viver.
C              Bm        Em        C           Bm           Em
  Ninguém questiona mais nada, os homens do "poder” agora contam sua piada
C           Bm        Em      C           Bm          Em
  Onde só eles acham graça, abandonando o povo na desgraça
    C          Bm    Em             C
  Vidrados na tv, perdendo tempo em vão
      D      (C   D) X3
  Em vão, em vão

C     D     A           C           D       A
 Ditadura da televisão, ditando as regras, contaminando a nação! X2

C       Bm         Em                C   Bm     Em
 O interesse dos "grandes” é imposto de forma sutil
C                Bm       Em      C        Bm      Em
  Fazendo o pensamento do povo se resumir a algo imbecil:

C             Bm      Em
 Fofocas, ofensas, pornografias
    C      Bm     Em
 E pornografias, ofensas, fofocas
 C      Bm            Em
 Futilidades ao longo da programação
C             D         (C       D) X3
 Da programação, da programação

C     D     A           C           D       A
 Ditadura da televisão, ditando as regras, contaminando a nação! X2

   C      Bm      Em
 Numa manhã de Sol, ao ver a luz
   C       Bm             Em
 Você percebe que o seu papel é resistir, não é?
C          Bm                Em
 Mas o sistema é quem constrói as arapucas
     C           Bm        Em
 E você está prestes a cair
C        Bm          Em     C       Bm   Em
 Da infância a velhice, modo artificial de se viver
C       Bm           Em           C       Bm   Em
 Alienação, ainda vivemos aquela velha escravidão.
C                     D      (C            D) X3
 aquela velha escravidão,  a velha escravidão

C     D     A           C           D       A
 Ditadura da televisão, ditando as regras, contaminando a nação! X2

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
