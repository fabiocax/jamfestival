Ponto De Equilibrio - Eu

F            Em
Eu paro agora pra falar
Que eu andava em confusão
   F                Em
Oi! Com o meu próprio Eu
Vivendo desilusão
   F                Em
Olhei pra dentro de mim
E vi o meu coração
Não querendo mais bater
Pedindo uma solução

Foi aí que
 F  Em     Dm      Em
Venceu a glória de Jah
  F           Em
A força da criação
Venceu a glória de Jah
Mais uma libertação
   F             Em
Meu Pai é meu rei, sempre me guiará

O Senhor é meu pastor, nada me faltará

Pra onde Ele me guia eu vou, Eu vou, Eu vou!

----------------- Acordes -----------------
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
