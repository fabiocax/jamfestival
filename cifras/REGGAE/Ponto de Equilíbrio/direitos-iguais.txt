Ponto De Equilibrio - Direitos Iguais

REFRÃO:
   Cm              Bb             Cm              Bb
Direitos iguais e justiça para o povo tupi guarani
   G#              Gm             G#            Gm
E todas as etnias remanescentes daqui
   Cm              Bb             Cm              Bb
Direitos iguais e justiça para o povo tupi guarani
   G#              Gm             G#            Gm
E todas as etnias remanescentes daqui

1 VERSO:
Cm                     Bb
1500, O homem branco em pindorama chegou
Cm                     Bb
Muita riqueza natural foi o que encontrou
   G#              Gm             G#            Gm
Um clima quente, um belo dia e um povo que vivia em harmonia
Cm                  Bb             Cm                       Bb
 Ticuna, caiagangue, guarani-kwoa, juruna, caetes, xavantes e tupinambá
   G#              Gm             G#            Gm
Do oiapoque ao chuí, no brasil, testemunhos do maior crime que se viu



1x(Cm Bb Cm Bb)

   G#              Gm             G#            Gm
Muito ódio, muita maldade, a coroa mandou pra cá a escória da humanidade

1x(Cm Bb Cm Bb)

   G#              Gm             G#            Gm
Muito sangue, muita matança, esvaindo com toda esperança
Cm                       Bb             Cm                       Bb
Com sentimento de justiça o índio ficou, se levantando bem mais forte contra o opressor
    G#              Gm             G#            Gm             1x(Cm Bb Cm Bb) 1x(G# Gm G# Gm)
Agora isso é o que importa na sua vida, usando a lança pra curar sua ferida


(REFRÃO)

2 VERSO:
Cm                     Bb
Eu te pergunto se essa luta adiantou,
Cm                     Bb
Vendo que índio ainda não tem o seu valor
   G#              Gm             G#            Gm
O cangaceiro do fazendeiro foi o primeiro que atirou
Cm                     Bb                   Cm                     Bb
Mais foi tanta injustiça que o índio sofreu, que até hoje ele não tem o que é seu
   G#              Gm             G#            Gm
Ele chora e pede ajuda, mas até hoje inda não venceu

Cm                     Bb

500 Anos se passaram e nada mudou
Cm                     Bb
Agora querem exterminar de vez com o que restou
        G#                  Gm           G#            Gm               1x(Cm Bb Cm Bb) 1x(G# Gm G# Gm)
Não dão valor a uma cultura mais antiga, mas vão pagar cada centavo na saída

(REFRÃO)

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
G# = 4 3 1 1 1 4
Gm = 3 5 5 3 3 3
