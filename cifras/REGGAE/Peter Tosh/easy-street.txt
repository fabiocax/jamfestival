Peter Tosh - Easy Street


Intro: Gm  F  Gm  F  Eb  F

Gm                     F
i said i can't believe it
Gm                   F
now that things have changed
  Eb
i only want to see the way you feel for me
                       F
always remains the same
Gm               F
i know we had to struggle
Gm                F
but it was not in vain
   Eb
on the bad years and the cryin of tears
                                F
still i never heard you complain

refrão:

        Gm                              F
oh baby now that we livin a top an easy street
                 Gm
you make my life complete
Eb                  F
so keep on givin me sweet love (2x)

we never had much money
never had a dime to throw
even though we suffered
you were still my lover
boy you never leaved me alone
now the sun is shinin
everything is gonna be nice
how can i repay you for everyday
we have to make a sacrifice oh baby

(refrão)

rap:
all the might and tribulation have not been in vain
and you look what has restarted from all them years of pain
the consequences of our actions we must face lord
they made me something that can put you in your place
ignore all of the temptation livin within you
violation of your spirit imagination
got to set yourself free
because continue to rely upon each other
so we can remain upon the easy street oh oh oh

(refrão)2x

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
