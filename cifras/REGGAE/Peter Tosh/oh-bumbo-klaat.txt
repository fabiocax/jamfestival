Peter Tosh - Oh Bumbo Klaat

[Intro]  C#  E  C#  E  C#  E  C#  E
         C#  E  C#  E  F#
         C#  E  C#  E  F#

        C#                     F#
Oh bumbo klaat, oh ras klaat
        C#                     F#
Oh bumbo klaat, oh ras klaat

                          C#
I said I came upon this land
          B                         F#
To guide and teach my fellow man
                                   C#
But one thing I can't overstand
          B                                 F#
Is why them don't love his brother man

        C#                     F#
Oh bumbo klaat, oh ras klaat
        C#                     F#
Oh bumbo klaat, oh ras klaat


                                  C#
Sometimes I sit and a'look around
          B                 F#
And listen to the daily sound
                                    C#
But when I check, there's so much lies
               B                               F#
And that's the reason why the children cry
        C#                     F#
Oh bumbo klaat, oh ras klaat
        C#                     F#
Oh bumbo klaat, oh ras klaat

                                     C#
It's been so long   we need a change
                B                            F#
So the shitstem we got to rearrange
                                             C#
And if there's obstacles in the road
                B                   F#
e got to throw them overboard

        C#                     F#
Oh bumbo klaat, oh ras klaat
        C#                     F#
Oh bumbo klaat, oh ras klaat

                                       C#
One night, an evil spirit held me down
      B                           F#
I could not make one single sound
                                C#
Jah told me, 'Son, use the word
          B                           F#
And now I'm as free as a bird

        C#                     F#
Oh bumbo klaat, oh ras klaat
        C#                     F#
Oh bumbo klaat, oh ras klaat

        C#                     F#
Oh bumbo klaat, oh ras klaat
        C#                     F#
Oh bumbo klaat, oh ras klaat

        C#                     F#
Oh bumbo klaat, oh ras klaat
        C#                     F#
Oh bumbo klaat, oh ras klaat

        C#                     F#
Oh bumbo klaat, oh ras klaat
        C#                     F#
Oh bumbo klaat, oh ras klaat

----------------- Acordes -----------------
B = X 2 4 4 4 2
C# = X 4 6 6 6 4
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
