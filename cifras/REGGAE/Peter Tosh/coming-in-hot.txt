Peter Tosh - Coming In Hot

Intro:

F G

Verse:

G F G G G F G

Lyrics:

Coming In Hot
Firin' some shot
Coming in red hot
It's a musical shot
Chorus

I just clean up me nuzzle
I just load me barrel
And I cock me hammer
Cause I'm gonna pull my trigger


Chorus

One minute after dozing
I thought that I was a blazing
My blood it was a boiling
It was amazing

Chorus

Next day it went higher
Whole a me catch a fire
I couldn't take it no longer
I haffe chokin' on some cold ice water

Chorus

I got up the other day
My heat it never went away
103 on the hour
I had to head for the shower

Chorus

----------------- Acordes -----------------
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
