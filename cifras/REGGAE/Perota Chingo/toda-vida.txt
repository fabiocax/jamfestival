Perota Chingo - Toda Vida

A     C#   F#m  E     D
Toda vida vuelve a caer

C#m      Cm  Bm
Vuelvo a caer
           F#m
Como aprendiz
E     D
Como dios
        C#m7                     D
Como se ve en el fondo si me aquieto
           F#m      E    D
Fondo lo opuesto en el amor
           Em                  D
Muero y habito entera este misterio

( Em )

       Em
Oíd mortales el grito sagrado
                          A
Libertad, libertad, libertad

                         D
Oíd el ruido de rotas cadenas

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C# = X 4 6 6 6 4
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
