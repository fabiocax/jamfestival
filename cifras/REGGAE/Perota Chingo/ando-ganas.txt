Perota Chingo - Ando Ganas

D                        G
Ando ganas de encontrarte
                         D
Ando lejos que es tarde aca
                         G
Ando ganas de encontrarte
                   D
Ando lejos mal no me va

G
Lloran, lloran mis penas de amor
D
Lloran, lloran mis penas por vos,
G                                      D
Lloran, lloran mis penas de amor de a mares

G
A mi si que me gustan tus piernas mecerse
               D
Como si fueran olas


Em               G
Por lo que se ve
                     D
Me gusto tu mar y tu canoa

D                        G
Ando ganas de encontrarte
                     D
De una buena vez por todas,
                         G
El invierno largo se fue
                     D
Y ya cambiaron las modas

G
Lloran, lloran mis penas de amor
D
Lloran, lloran mis penas por vos,
G                                      D
Lloran, lloran mis penas de amor de a mares

G
A mi si que me gustan tus piernas mecerse
               D
Como si fueran olas

Em                  G
Por lo que se ve
                      D
Me gustÃ³ tu mar y tu canoa

G
Te amare, solo una cosa te digo
Te amare, yo necesito un abrigo
D
Te amare, en esta tierra hace frio
Te amare, si no estas al lado mio
G
Te amare, y como vos no hay ninguna
Te amare, no brilla tanto la luna
D
Te amare, ando ganas de encontrarte
Quiero llevarte pa amarte

----------------- Acordes -----------------
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
