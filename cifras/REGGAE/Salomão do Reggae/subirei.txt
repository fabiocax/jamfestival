Salomão do Reggae - Subirei

G                         D
Como a corsa suspira pelas águas
Em                               C
Assim minh'alma suspira por Ti 2x

G                  D
Eu subirei no mais alto monte
Em                           C
Pra te encontrar, pra te encontrar 2x

G                        D
E se não for o bastante, Senhor
Em
Eu voarei como a águia
C
Só pra te adorar

G           D
Subirei, Subirei
Em C
    Para estar contigo, meu Senhor

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
