Tribo de Jah - Na Pilha do Verão

       G
Quando chega o sol
    C        G     C
E a luz da nova estacao,
 G      C
É tempo de se re-energizar,

Repaginar o coração.
   G         C
Quando o sol bate à porta
     G           C
E reacende os sonhos como nova energia,
      G           C
É hora de sair da toca
    G             C
E estar em boa sintonia.
       G                 C                G      C
Deixe o sol brilhar, deixe o som fluir mais uma vez;
        G                C             G      C
A trilha do verao, o som da estação tá na SP3.
   G
Na trilha da montanha,


No caminho do mar,
   C
A SP3 te acompanha

Até onde o som pegar.
   G
Na pilha do verão,

No clima da rapês,
      C
A energia que liga

Tá na SP3.

Bm                  Em
Há tantas novas paisagens por descobrir,
 Bm              Em
Novas e outras viagens, pra se curtir;
Bm               Em
De repente, um olhar, toca no coração;
Am         Bm
Se voce se deixa envolver...
    Am
Numa noite de verão
     Bm        D
Tudo pode acontecer.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
