Tribo de Jah - Uma onda que passou

 Versão 1
-======-

Intro:
D, F#, Bm, A

           D                        F#
Eu não pratico bruxaria, não tenho bola de cristal
          Bm                   A
Nem toda grana me faria trocar o bem pelo mal
         D                           F#
Se eu soubesse eu tremia, fria,  na sua aparição
      Bm                        A
Só assim eu não teria entrado nesse turbilhão

Chorus:
G          A         D    C#m Bm A   G
 Foi uma fria que rolou, agora eu sei
G    A            D      C#m Bm A   G
 Uma onda que passou e eu não dropei
 G    A           D    C#m Bm A   G
 Amor foi só o que eu quiiis

G          A
 Mais puro e sublime

  D                          F#
Amor não é delírio, fogo de uma fullgás paixão
       Bm                A
O pesadelo e o medo de uma desilusão
     D                    F#
Eu quis dizer quis, quis te dizer
        Bm                   A
Quis botar pra fora, mas na hora eu me calei

Chorus:

G          A           D  C#m Bm A   G
 Foi um delírio que rolou, agora eu sei
G     A           D       C#m Bm A   G
 Uma onda que passou e eu não dropei
  G   A      D          C#m Bm A   G
 Amor foi só o que eu quiiis
G          A           D
 Mais puro e sublime amor
Solo
Chorus

G       A             D    C#m Bm A   G
 Tudo que eu posso dizer, tentei falar
G       A           D   C#m Bm A   G
 Tudo que agora eu sei, como explicar
       A           D    C#m Bm A   G
 Você ainda vai saber e vai sofrer
G        A           D   G  A
 Quando seu tempo chegar

Repete primeira parte


_______________________________________________


Versão 2
-======-

Intro:

        E                            Ab
e--------------------     e----------------
b--------------------     b---4------------
g-----9-------9-9----     g-----5----------
d---9---9---------9--     d-------6--------
a-7-------7-7-------7     a-----------6-2-4
e--------------------     e-4-------4------

        Dbm                       B
e--------------------     e---2------------
b-----------5--------     b-----4----------
g-----6-------6------     g-------4--------
d---6---6-------6----     d----------------
a-4-------4----------     a-2-------2------
e--------------------     e----------------

E, Ab, Dbm, B

Verse 1:
             E                        Ab
Eu não pratico bruxaria, não tenho bola de cristal
     Dbm                            B
Nem toda grana me faria trocar o bem pelo mal
     E                                 Ab
Se eu soubesse eu tremia, fria,  na sua aparição
            Dbm                      B
 Só assim eu não teria entrado nesse turbilhão

Chorus:

A        B             E   Dbm
Foi uma fria que rolou, agora eu sei
A        B            E   Dbm      A
Uma onda que passou e eu não dropei
       B            E    Dbm
Amor foi só o que eu quiiis
A          B
Mais puro e sublime

Verse 2:

    E
Amor não é delírio, fogo de uma fullgás paixão
O pesadelo e o medo de uma desilusão
Eu quis dizer quis, quis te dizer
Quis botar pra fora
Mas na hora eu me calei

Chorus:

A        B             E   Dbm
Foi um delírio que rolou, agora eu sei
A        B            E   Dbm      A
Uma onda que passou e eu não dropei
       B            E    Dbm
Amor foi só o que eu quiiis
A          B
Mais puro e sublime amor
Solo

Chorus
 E
Tudo que eu posso dizer, tentei falar
Tudo que agora eu sei, como explicar
Você ainda vai saber e vai sofrer
Quando seu tempo chegar

Repete primeira parte


Fica mais parecida com a Tribo se for tocada em G


Refrão Riff:

  A       B       E       Dbm
e-5-------7-----------------------
b---5-------7-----9---------------
g-----6-------8-----9---------6---
d-------7-------9-------9-6-----6-
a---------------------7-----4-----
e---------------------------------

----------------- Acordes -----------------
A = X 0 2 2 2 0
Ab = 4 3 1 1 1 4
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
Dbm = X 4 6 6 5 4
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
