Tribo de Jah - Irie dub feeling

(Fauzi Beydoun)

 Am , Gm  a musica inteira


 Am           Gm
Reggae music flows
            Am
The music flows
           Gm
Deep in my soul

Like a river flows

The river flows
Warm in my blood
Just want to catch the vibe
Just want to feel alright
The rhythm seems so nice
It's just a strength in my life
I say ride on

Ride on, ride on I say ride on
Ride on, ride on
It means love, love and devotion
Irie, irie feeling and emotion
Love, love and devotion
Irie, irie feeling and emotion
Love, love and devotion
Irie, irie feeling and emotion
Love, love and devotion
Irie, irie feeling and emotion
Love, it means love
Love, it means love Love!
I'm gonna tell you now
What reggae music really means to me, you know?
As Jah love shines equally
Reggae music means unity
Redemption to Jah people
Redemption and liberty
Reggae means praises and thanks to the Father
Faith in Jah, respect to one another
I say, ride on, ride on Ride on, ride on
Ride on, ride on
Ride on, ride on
It means love, love and devotion
Irie, irie feeling and emotionLove, love and devotion
Irie, irie feeling and emotionLove, love and devotion
Irie, irie feeling and emotionLove, love and devotion
Irie, irie feeling and emotionLove, it means loveLove, it means love
O reggae flui como um rio
Fundo em minha alma Correndo nas veias, denso e macio
Uma corrente quente e calma
Eu só quero sentir o somSentir a vibração
O ritmo é tão bom
Na batida do coração
Eu vou dizer pra vocês
O que o reggae realmente significa para mim
Assim como a luz do sol brilha com a igualdade
O reggae quer dizer unidade
Redenção para o povo de Jah
Redenção e liberdade
Quer dizer: graças e louvores ao Mais Alto
Fé e o respeito de uns aos outros
Amor e devoção
Sentimento irie, muita emoção
Amor e devoção
Sentimento irie, muita emoção

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Gm = 3 5 5 3 3 3
