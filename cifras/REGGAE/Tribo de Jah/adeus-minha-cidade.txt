Tribo de Jah - Adeus Minha Cidade

(intro 2x) D  Em  F#m  Em

D    Em      F#m       Em
Eu sai pra estrada um dia
             D  Em  F#m  Em
Frio desolador
      D       Em
Na mochila quase nada
F#m         Em          D  Em  F#m  Em
Uma Botina e um cobertor
D  Em         F#m    Em
Adeus minha cidade, adeus
               D  Em  F#m  Em
Assis minha região
D         Em      F#m
Iepê e o Paranapanema
 Em               D  Em  F#m  Em
Pairagens do meu coração

 Bm             F#m
Eu guardo na memória

 Bm                   F#m
Imagens de alegria e dor
 Em                A
Páginas da minha história
 G
Os tombos todos do destino
               A
Sonhos tolos desatinos de menino
         D
Do interior

(intro)

 Bm           F#m
Eu Volto na estrada
 Bm                 F#m
Revivendo as recordações
 Em              A
Os olhos razos da água
 G
coração de novo vaga
                 A
Nas planicies vastas
               D
Pastos plantações

(intro)

D    Em      F#m       Em
Eu sai pra estrada um dia
             D  Em  F#m  Em
Frio desolador
      D       Em
Na mochila quase nada
F#m         Em          D  Em  F#m  Em
Uma Botina e um cobertor
D  Em         F#m    Em
Adeus minha cidade, adeus
               D  Em  F#m  Em
Assis minha região
D         Em      F#m
Iepê e o Paranapanema
 Em               D  Em  F#m  Em
Pairagens do meu coração

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
