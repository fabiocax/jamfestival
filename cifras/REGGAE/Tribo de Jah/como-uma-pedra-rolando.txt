Tribo de Jah - Como Uma Pedra Rolando

Intro: B E B E B E B E F#


      B                   C#m                    D#m
Você sempre se vestiu tão bem, cuidou-se como ninguém,
                  E           F#
Como quem sempre teve e tem tudo;
Sempre teve a mão toda proteção;
Alguém para lhe atender e lhe obedecer mudo;

        E              F#
Só não esperava que a solidão
        E                               F#
Lhe invadisse a casa e a alma sem sua permissão,
       E   D#m   C#m            B
Agora já         não falas alto,
  E         D#m        C#m       B
Parece então      que caiu do salto,
        C#m
Mas não tem noção do que é não saber
        E                               F#
Se vai ter ou não outra refeição ou onde dormir,

                    E
(Não tem noção...)


Refrão:

                B   E  F#
Do que é se sentir
              B   E  F#
Sozinho na estrada,
                B   E  F#
Só você e mais nada,
                B   E  F#
Um completo estranho,
             B    E  F#
Sem teto, vagando,
           B           E  F#
Como uma pedra rolando.

Você estudou nas melhores escolas,
Se formou e se sentiu tão orgulhosa e inútil;
Foi educada para não se envolver
Com qualquer cara que aparecer ou parecer hostil;
Ninguém lhe ensinou a viver na rua,
Ter como cobertor só o brilho da lua,
Você caiu do topo e lhe restou tão pouco,
Eu vim do fundo do poço e já roí o osso,
Eu fiz disso uma ciência,
Tente se despir das aparências
Pra então intuir o que é se sentir...

O que é se sentir
Sozinho na estrada,
Só você e mais nada,
Um completo estranho,
Sem teto, vagando,
como uma pedra rolando.

Você não tem noção...
Do que é se sentir,
Do que é se sentir
Jogado no mundo,
Sem casa e sem rumo,
Um retirante errante,
Sem destino, distante,
Um completo estranho,
Como uma pedra rolando,
Sozinho na estrada,
Só você e mais nada,
Sem teto, vagando,
Como uma pedra rolando,
Como uma pedra rolando,
Como uma pedra rolando...

----------------- Acordes -----------------
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
D#m = X X 1 3 4 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
