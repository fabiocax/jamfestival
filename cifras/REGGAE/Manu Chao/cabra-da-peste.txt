Manu Chao - Cabra da Peste

               E
Yo no soy brasileiro
                   D
Yo soy filho do Nordeste
                E
Yo soy cabra da peste
                D
Yo gostei do Ceara

             E
Yo soy estrangeiro
                 D
Vagabundo e cachaceiro
                 E
Soy galego e atrevido
               D
Perdido no Ceara

               E
Ai vai minha menina
                 D
Ai vai minha mulher

             E
Ai vai meu cavalo
             D
Vem por um camino

               E
Sube mi pueblo sube
               D
O beijo da mulher
               E
O beijo da mulher
               D
O beijo da mulher
               E
O beijo da mulher
                      D
é como o beijo do dragão

----------------- Acordes -----------------
D = X X 0 2 3 2
E = 0 2 2 1 0 0
