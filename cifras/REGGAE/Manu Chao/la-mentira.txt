Manu Chao - La Mentira

Fm: x 8 10 10 9 8

Pour la rythmique ca suit toujours le meme schéma:
    Fm                     Fm                    D#            Fm 
e||---8--8--8----8--8--8-----8--8--8----8--8--8---6-6-6--6-6-6---8--8--8---8--8--8-|| 
B||---9--9--9----9--9--9-----9--9--9----9--9--9---8-8-8--8-8-8---9--9--9---9--9--9-|| 
G||--10-10-10---10-10-10----10-10-10---10-10-10---8-8-8--8-8-8--10-10-10--10-10-10-|| 
D||--------------------------------------------------------------------------------|| 
A||-8----------------------8---------------------6-------------8-------------------|| 
E||-----------8-----------------------8-----------------6----------------8---------|| 


u = up (gratter les cordes de bas en haut,vers le haut)
d = down


Intro: Fm Fm D# Fm  (x4)
======

   Fm
Mentira lo que dice

Fm             D#
Mentira lo que da
               Fm
Mentira lo que hace
        Fm         Fm
Mentira lo que va
      D#    Fm
la Mentira

Mentira la mentira
Mentira la verdad
Mentira lo que cuece
Bajo la oscuridad
Mentira la mentira mentira

Mentira el amor
Mentira el sabor
Mentira la que manda
Mentira comanda
mentira mentira la mentira

Mentira la tristeza
Cuando empieza
Mentira no se va
mentira la mentira ....


Mentira no se borra
Mentira no se olvida
Mentira la mentira
Mentira cuando llega
Mentira nunca se ba
Mentira la mentira
Mentira la verdad


Todo ed mentira en este mundo
Todo es mentira la verdad
Todo es mentira yo me digo
Todo es mentira

¿ Por que sera ?

----------------- Acordes -----------------
D# = X 6 5 3 4 3
Fm = 1 3 3 1 1 1
