Manu Chao - Por El Suelo

Am                 Em
Por el suelo hay una compadrita
       Dm                Am
Que ya nadie se para a mirar
                         Em
Por el suelo hay una mamacita
        Dm              Am
Que se muere de no respetar
                      Em
Patchamama te veo tan triste
      Dm                 Am
Patchamama me pongo a llorar...

    Am              Em
Esperando la ultima ola
     Dm                 Am
Cuidate no te vayas a mojar
                     Em
Escuchando la ultima rola
    Dm                  Am
Mamacita te invito a bailar...


       Am              Em
Por el suelo camina mi pueblo
       Dm              Am
Por el suelo hay un agujero
       Am              Em
Por el suelo camina la raza
    Dm                Am
Mamacita te vamos a matar...

    Am              Em
Esperando la ultima ola
      Dm               Am
Patchamama me muero de pena
    Am               Em
Escuchando la ultima rola
    Dm                  Am
Mamacita te invito a bailar...

       Am              Em
Por el suelo camina mi pueblo
       Dm                Am
Por el suelo moliendo condena
       Am                Em
Por el suelo el infierno quema
       Dm                Am
Por el suelo la raza va ciega...

    Am              Em
Esperando la ultima ola
      Dm               Am
Patchamama me muero de pena
    Am               Em
Escuchando la ultima rola
    Dm                  Am
Mamacita te invito a bailar...

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
