Manu Chao - Me Gustas Tú

[Intro] Bm  A  Em

[Solo]

      Bm    A      Em
E|----2---0----0--7--0--------|
B|-0-------------------0-3-2--|
G|------2---------------------|     (2x)
D|----------------------------|
A|----------------------------|
E|----------------------------|

   Bm           A           Em
E|---2-----2----------------7-----7----------------|
B|---3-----3----2-----2-----8-----8----------------|
G|---4-----4----2-----2-----9-----9----------------|     
D|---4--4--4----2--2--------9--9--9----------------|
A|-2----------0----------7-----------7----5-4-2----|
E|-------------------------------------7-----------|

Bm                         A
Me gustan los aviones, me gustas tú

   Em
Me gusta viajar, me gustas tú
Bm                      A
Me gusta la mañana, me gustas tú
   Em
Me gusta el viento, me gustas tú
Me gusta soñar, me gustas tú
Me gusta la mar, me gustas tú

Que voy a hacer, je ne sais pas
Que voy a hacer, je ne sais plus
Que voy a hacer, je suis perdu
Que horas son, mi corazón

Me gusta la moto, me gustas tú
Me gusta correr, me gustas tú
Me gusta la lluvia, me gustas tú
Me gusta volver, me gustas tú
Me gusta marijuana, me gustas tú
Me gusta colombiana, me gustas tú
Me gusta la montaña, me gustas tú
Me gusta la noche, me gustas tú

Que voy a hacer, je ne sais pas
Que voy a hacer, je ne sais plus
Que voy a hacer, je suis perdu
Que horas son, mi corazón

Doce un minuto

Me gusta la cena, me gustas tú
Me gusta la vecina, me gustas tú
Radio relojio
Me gusta su cocina, me gustas tú
Una de la mañana
Me gusta camelar, me gustas tú
Me gusta la guitarra, me gustas tú
Me gusta el reggae, me gustas tú

Que voy a hacer, je ne sais pas
Que voy a hacer, je ne sais plus
Que voy a hacer, je suis perdu
Que horas son, mi corazón

Me gusta la canela, me gustas tú
Me gusta el fuego, me gustas tú
Me gusta menear, me gustas tú
Me gusta la Coruña, me gustas tú
Me gusta Malasaña, me gustas tú
Me gusta la castaña, me gustas tú
Me gusta Guatemala, me gustas tú

Que voy a hacer, je ne sais pas
Que voy a hacer, je ne sais plus
Que voy a hacer, je suis perdu
Que horas son, mi corazón

Cuatro de la mañana
A la bin, a la ban a la bin bon ba
A la bin, a la ban a la bin bon ba
Obladi Obladá Obladidada
A la bin, a la ban a la bin bon ban

Radio relojio
Cinco de la mañana
No todo lo que es oro brilla
Remedio chino es infalible

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
Em = 0 2 2 0 0 0
