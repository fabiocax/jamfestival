Dread Mar I - Tu Sin Mi

Intro 2x: Am  G  Dm

Am
Es terrible percibir que te vas
              G
Y no sabes el dolor que has dejado

Justo en mí. Te has llevado la
Dm
Ilusión de que un día tú serás
               C            G
Solamente para mi ohhh para mí.
         Am
Muchas cosas han pasado, mucho
                            G
Tiempo, fue la duda y el rencor que

Despertamos al ver que no nos
       Dm
Queríamos, no ya no, ya no nos
          C      G
Queríamos ohh no


Am
Y ahora estas tu sin mí y que
              G
Hago con mi amor el que era para                (2x)
                      Dm
Ti y con toda la ilusión de que un
                               C
Día tú fueras solamente para mí
           G
Ohh para mi...

        Am
No comprendo puedo ver q el amor
                  G
Que un día yo te di no ha llenado
                                 Dm
Tu interior y es por eso que te vas

Alejándote de mi y sin mirar hacia
C                 Gm
Atrás, hacia atrás.
     Am
Pero yo corazón entendí en
                G
El tiempo que paso q no

Nos servía ya la locura de este
 Dm
Amor que un día así se fue y nunca
         C          G
Mas volvió, no volvió

(Refrão 2x)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
