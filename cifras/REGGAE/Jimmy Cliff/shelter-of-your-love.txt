Jimmy Cliff - Shelter Of Your Love

 G  D  C  C

How I survived, Oh, I dont know.
Still I'm glad to say, I'm still here today.
I tried to pretend, but it was just a show.
Now I, I just cant hide. I had to swallow my pride.
And I had to:

C  D  G  G  C  D  G  G  C  D  Em  Em  C  D  G  G

Run for the shelter of your love. (x4)

You know I'm a man, I dont like to brag.
But it's been cold and empty out here, it's been a drag.
A man dont fight and run away. He will live to fight another day.
Well it's these words I sing that keeps me holding on today.
I know that I can:


C  D  G  G  C  D  G  G  C  D  Em  Em  C  D  G  G


Run for the shelter of your love. (x4)

The shelter of your love. (Longing for the shelter of your love.)
The shelter of your love. (Talking 'bout the shelter of your love.)
The shelter of your love.

I know that I can:
Run for the shelter of your love.
Run for the shelter
Run for the shelter of your love.
Run for the shelter of your love.
Shelter of your love
The Shelter of your love

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
