Jimmy Cliff - I Can See Clearly Now

D          G               D
I can see clearly now the rain is gone
D          G            A
I can see all obstacles in my way
D             G                D
Gone are the dark clouds that had me blind

                 C                 G                         D
It's gonna be a bright, (bright) bright, (bright) sunshine day
                 C                 G                         D
It's gonna be a bright, (bright) bright, (bright) sunshine day

 D         G               D
Yes I can make it now the pain is gone,
D           G                 A
All of the bad feelings have disappeared.
D            G                    D
Here is the rainbow I have been praying for.

                 C                 G                         D
It's gonna be a bright, (bright) bright, (bright) sunshine day

                 C                 G                         D
It's gonna be a bright, (bright) bright, (bright) sunshine day

F                                     C
Look all around, there's nothing but blue skies
F                                         A         C#m G C#m G C Bm A
Look straight ahead, there's nothing but blue skiiiiiiiiiiiiiiiiiiiiiiiies .

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
