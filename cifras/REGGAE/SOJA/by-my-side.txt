SOJA - By My Side

Gm           F        A#                    Cm
I act like I left her, but I really beg her to stay.
And the more that I press her is the more she slips
away.
Act like I don't need her but...Yo, you're
necessary...
Like ten days from now I won't see your face when I
sleep.

Gm               F      A#              Cm
Act like I can hear, but Jah knows I'm deaf.
Act like I can see but now, yo, from me sight has
left.
Act like I don't know but, I know these things...
Act like I don't feel it but I'm gonna feel it, every
morning now on

Chorus (one measure or four reggae skanks per chord):
Gm           A#
Each morning, on each night
Cm                 F
when I need you by my side

on each morning and on each night
when I need you

Like I don't care but each and every time
your face comes across it never ever leaves my mind.
Oh, like I don't know but...I know your thing
like I don't feel it, but I'm gonna feel it every
evening now on -

Each morning, on each night
when I need you by my side
on each morning and on each night
when I need you

N.C. (maybe an A# to Cm if anything)
I act like I left her, but the truth is I beg her, beg
her to stay...
Gm                  F                        A#                  Cm
And the more that I press her, means all the more that she slips away.
Act like I don't need her, but I'm here to tell you
that you're necessary.
Like ten days from now I won't see your face when I'm
on -

Each morning, on each night
when I need you by my side
on each morning and on each night
when I need you by my side.

- On each morning and on each night when I need you so
desperately.

----------------- Acordes -----------------
A# = X 1 3 3 3 1
Cm = X 3 5 5 4 3
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
