SOJA - Jah is Listening Now

Intro 2x: Bb Dm Bb Dm Bb Dm C

E|--------------------------------------|
B|--------------------------------------|
G|---------3-3-2-2--------------2-------|
D|-2h3-3-3----------5-5-3-2-3-5---5-3~--|
A|--------------------------------------|
E|--------------------------------------|

E|--------------------------------------|
B|--------------------------------------|
G|------3-3-2-2--------------2----------|
D|-3-3-----------5-5-3-2-3-5---5-3~-----|
A|--------------------------------------|
E|--------------------------------------|

E|----------------------------------------------------------|
B|----------------------------------------------------------|
G|-----3-3-2-2-------------2------------------------2-------|
D|-3-3---------5-5-3-2-3-5---5-3h5-55-3h5--55-3---5---3-2h3-|
A|----------------------------------------------------------|
E|----------------------------------------------------------|


Bb          Dm             Bb             Dm
I know you feel like...you're...you're all alone
Bb          Dm              C
And all you can depend on now
Is your flesh and your bones
Bb             Dm
So look around now
Bb                Dm
And let it talk to you...yeah
Bb             Dm                 C
And when those colors run together
Remember, that  jah  is listening now.

Bb        Dm                    C
When you wake, he's listening now
          Dm                     Bb
When you sleep, he's listening now
              Dm                      C
When you're smiling, he's listening now
          Dm                    Bb
When you weep, he's listening now
         Dm                    C
If you don't, he's listening now
             Dm                   Bb
And if you need, he's listening now
        Dm                      C
When you talk, he's listening now
             Dm
When you dont speak...in

Bb          Dm
I know you think that you know
Bb          Dm
Real and fake
Bb        Dm            C
Well sometimes reality is
Truly just what we made

Bb          Dm              Bb               Dm
I know you feel like you're in total control
Bb              Dm                  C
But when things spin out of your hands
You should know, you should know, you should know

Bb      Dm          C
That we all fall down
       Dm        Bb
And we will one day
     Dm        C
We all land here
      Dm       Bb
We all drift away
     Dm       C          Dm
We all stand on this earth
        Bb
The same ground
Bb     Dm       C*
We're all so lost,but i've found
 jah  is listening now

Bb        Dm                    C
When you wake, he's listening now
          Dm                     Bb
When you sleep, he's listening now
              Dm                      C
When you're smiling, he's listening now
          Dm                    Bb
When you weep, he's listening now
           Dm                    C
When you don't, he's listening now
          Dm                     Bb
When you need, he's listening now
          Dm                    C
When you talk, he's listening now
               Dm                     Bb
When you don't speak, he's listening now
Dm                  C
   He's listening now
Dm                  Bb
   He's listening now
Dm                  C         Dm
   He's listening now, now,now

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
