SOJA - You And Me

[Intro] C  Em   D

E|----------------------------------------------------|
B|----------------------------------------------------|
G|----16\14-12~-9/12----------------------------------|
D|-14----------------12-------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

E|----------------------------------------------------|
B|----------------------------------------------------|
G|----16\14-12-11h12-12/14----------------------------|
D|-14-------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

[Primeira Parte]

C              Em             D
It's you and me, it's always been, yeah
C              Em           D
And how I feel about you, there's no end, end

 C            Em            D
But you make me chase you around, around
C             Em             D
And then you need me again when you fall down, down

[Pré-Refrão]

C              Em
And when this dance is done
 D               Em      D    C
You and me are still the only one
                Em
Well since our time begun
    D                  Em         D    C
And I'm still with you even when your gone
              Em
When this dance is done
 D               Em      D    C
You and me are still the only one
                Em
Well since our time begun
    D                  Em         D
And I'm still with you even when your

[Refrão]

 C            Em            D
So I start it, and you stop it
               Em          D               C
So I want it knowing that you're walking away
              Em            D
So I start it, and you stop it
               Em          D               C
So I want it knowing that you're walking away
              Em            D
So I start it, and you stop it
               Em          D               C
So I want it knowing that you're walking away
C             Em
I start it, and you stop it
 D
Ya-e ya-e yah-a

[Solo] C  Em  D
       C  Em  D

E|----------------------------------------------------|
B|----------------------------------------------------|
G|----16\14-12~-9/12----------------------------------|
D|-14----------------12-------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

E|----------------------------------------------------|
B|----------------------------------------------------|
G|----16\14-12-11h12-12/14----------------------------|
D|-14-------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

E|----------------------------------------------------|
B|----15-13-12-13-12-10-------------------------------|
G|----------------------11h12-12/14-------------------|
D|-14-------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

E|----------------------------------------------------|
B|----------------------------------------------------|
G|----16\14-12-9/12-----------------------------------|
D|-14---------------12--------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

[Segunda Parte]

C              Em           D
It's you and me, I'm back again, yeah
 C               Em         D
Don't introduce me to all, your new friends, friends
  C              Em            D
Why do you make me chase you around?
  C               Em           D
Why do your words make me not make a sound, sound?

[Pré-Refrão]

C              Em
And when this dance is done
 D               Em       D
You and me are still the only one
 C               Em
Yeah since time begun
  D                    Em              D
And I'm still with you even when your gone
C              Em
And when this dance is done
 D               Em       D
You and me are still the only one
 C                 Em
Yeah since time begun
D                  Em     D
I'm still with you even when your

[Refrão]

 C            Em            D
So I start it, and you stop it
               Em          D               C
So I want it knowing that you're walking away
              Em            D
So I start it, and you stop it
               Em          D               C
So I want it knowing that you're walking away
              Em            D
So I start it, and you stop it
               Em          D               C
So I want it knowing that you're walking away
C             Em
I start it, and you stop it
 D
Ya-e ya-e yah-a

[Final] C  Em  D
        C  Em  D

E|----------------------------------------------------|
B|----------------------------------------------------|
G|----16\14-12~-9/12----------------------------------|
D|-14----------------12-------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

E|----------------------------------------------------|
B|----------------------------------------------------|
G|----16\14-12-11h12-12/14----------------------------|
D|-14-------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

E|----------------------------------------------------|
B|----15-13-12-13-12-10-------------------------------|
G|----------------------11h12-12/14-------------------|
D|-14-------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

E|----------------------------------------------------|
B|----------------------------------------------------|
G|----16\14-12-9/12-----------------------------------|
D|-14---------------12--------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
