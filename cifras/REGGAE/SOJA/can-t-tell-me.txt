SOJA - Can't Tell Me

(intro 2x) G  Am  Bm  Am  G

G
I like women and I love getting high,
          Am
I like a house and a car just fine but
        Bm               Am
I love Jah and creation too,
                    G
and I don't really care if it's all right with you.
G
I love my family even the bald heads,
                Am
and I love my friends even with no dreads,
                  Bm                         Am
'cause we all get by and to ourselves stay true -
                        Gm
and I don't really care if it's Rasta to you.

(refrão 2x)
  Em
You can't tell me who I am

             Am
'cause I'm working on that too.
 Bm                 Am
What's right for me just ain't right for you.

(repete o verso)

Unless you're ready for your own judgment,
Don't do nothing before you judge yourself.
You see me around and everything that I do...
Guess what dog - I've seen you too.
But I don't judge how you live your life
'cause I know we're all trying to get by
You see me around and everything that I do
Guess what dog? Jah seen you, too.

(refrão)

You can't tell me who I am
'cause I'm working on that too.
What's right for me just ain't right for you.

(verso)

Soy un hijo de Jehova
Que mas puedo yo decir
Velo por mf caminar
Y ando recto por ahi
Si al pasar me has de juzgar
DIOS se encargara de ti
Y soy humano como tu
Me caf me levante
Y de mi error logre aprender
En la vida hay gue entender
Que el juzgar esta de mas
Anda entiendelo ahora tu

(refrão)

You can't tell me who I am
'cause I'm working on that too.
What's right for me just ain't right for you.

(solo)

(verso)

Y'all remind me of a place
we all call the Middle East,
where people fight over their laws
that they swear God handed down.
But while they hold to their side fast
thinking that they've got the only righteous path,
I've seen that they all missed the bottom line -
True love is the only thing that will forever last.

You can't tell me who I am
'cause I'm working on that too.
What's right for me just ain't right for you.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
