Edson Gomes - Ovelha Negra

Intro: G D C Em  (2x)


E|--7-7-7-7-7-7-10---8-8-8-8-7/8-8/7---7-7-7-7-7-7-10---12-10-8-8-8-8----------|
B|-----------------------------------------------------------------------------|
G|---------------------------------------------------------------------9-------|
D|-----------------------------------------------------------------------------|
A|-----------------------------------------------------------------------------|
E|-----------------------------------------------------------------------------|


Em            C                      D             Em
Há dias na vida que a gente pensa que não vai conseguir
Em            C                       D   Em
Há dias na vida que a gente pensa em desistir
Em            C                       D       Em
Há dias na vida que a gente pensa em sumir
Em                     C                 D             Em
E a turma la em casa já está apavorada com a minha decisão
Em                          C
É que não ligo mais nada

                          D           Em
Nem mesmo a namorada, que me deu o coração
G                  D       C       Em
Eu gostaria de ficar com você um pouco mais(2x)
G       D                 C            Em
Vou por ai, vou mesmo assim, vou por ai
G       D                 C            Em
Vou por ai, vou mesmo assim, vou caminhar
G         D             C           Em
Eu também sou a ovelha negra da família(5x)

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
