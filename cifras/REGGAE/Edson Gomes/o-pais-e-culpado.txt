Edson Gomes - O Pais É Culpado

Intro:C  Am  F  G

C                       Am                     C                    Am
Não existe nenhum lugar para ir só Jesus pode nos salvar
F                                  Am    F                                Am
Somos senhores das favelas somos senhores da pobreza
F                            Am                F                              G
Falta alimento em nossas mesas conclusão o País é culpado.
C                           Am              C                                Am
Quando o Mestre então voltar,Quando o Mestre nos resgatar(enquanto não vem)
F                                    Am
Somos Senhores das calçadas(enquanto não vem).
F                                      Am
Somos Senhores nas sinaleiras(enquanto não vem).
F                          Am                 F                                G
Superlotamos as penitenciarias conclusão o País é culpado.
C                                      Em                Dm                        G
Somos sobreviventes do tempo somos filhos da santa esperança
C                               Em               Dm                           G
Somos passivos resistentes mergulhados em toda essa lama.
C                   Am
A razão do nosso viver meu Deus.

C                              Am
E teu filho que vem nos Salvar (enquanto não vem).
F                        Am
Somos os analfabetos(enquanto não vem).
F                         Am
Orgulhosos indiscretos(enquanto não vem).
F                          Am     F                                 G
Grandessíssimos idiotas conclusão o País é culpado.
C                 G                        C
O País e culpado o País é culpado sim.
C                 G                        C
O País e culpado o País é culpado sim.....

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
