Edson Gomes - Filho da Terra

Intro: D Bm G A

D       Bm      G       A                               D Bm
Naturalmente, eu transo a mente, o corpo, o espírito
G       A                       D Bm G A
Eu transo a mente, o corpo o espírito!
D       Bm      G       A                               D Bm
Venha comigo, transar a mente, o corpo, o espírito
G       A                       D Bm G A
Transar a mente, o corpo, o espírito!

D       Bm              G A
Natural, filho da terra
D       Bm              G A
Natural, filho da terra!

D       Bm              G A
Mas voce resiste, porquê?
D       Bm              G A
Alguem te agride! Porquê?


        D       Bm      G       A
Alguem torce, a trilha, agora é hora,
          D     Bm      G       A
é hora de ir embora, agora é hora,
        D         Bm    G       A
é hora de ir, a trilha agora é hora,
        D       Bm      G       A
é hora de ir, embora, agora é hora,
        D       Bm G A
é hora de ir!

Solo: D Bm G A

        D       Bm      G A
Mas você resiste, porquê?
        D       Bm      G A
Alguem te agride, porquê?

        D       Bm      G       A
Alguem torce, a trilha agora é hora,
        D       Bm      G       A
é hora de ir embora, agora é hora,
        D       Bm      G       A
é hora de ir, a trilha agora, é hora,
        D       Bm      G       A
é hora de ir embora, agora é hora,
        D       Bm G A
é hora de ir!

           D    Bm
Não existe cor!
G               A
Só voce sabe que não existe cor!
           D    Bm
Não existe cor!
G               A
Só voce sabe que não existe cor!

Solo: D Bm G A

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
G = 3 2 0 0 0 3
