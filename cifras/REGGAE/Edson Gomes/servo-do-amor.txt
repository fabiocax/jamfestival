Edson Gomes - Servo Do Amor

D                     G             A            Bm           A
Eu sou filho do escravo que veio da embarcação da tortura do mar
D                     G             A              Bm                   A
Eu sou filho do escravo, vivo na embarcação da tortura, direto do canavial
G                  A                  Bm
Foi difícil amigo, foi difícil amigo, foi tão difícil
G                  A                  Bm                A
Foi difícil amigo, foi difícil amigo, foi tão difícil. Foi

D              G
Eu nasci aqui, eu estou aqui
A
Eu aqui, eu aqui

G         A             G              A                 Bm
Nada sou. Não sou nada. Não sou doutor e ainda que fosse, nada seria
A              G                     A
Longe de casa, longe da minha praia. Longe de casa
G         A             G                A                Bm           A
Nada sou. Não sou nada. Não sou doutor e ainda que fosse, nada seria. Eu nada seria
    D             A           G
Eu sou livre, sou, sou livre. Em Cristo sou livre

              Bm                A
Livre das drogas, livre dessas horas
              G                   Bm                        A
Livre da inveja, livre da hipocrisia. Sou livre da demagogia
       D           A           G
Eu sou livre, sou, sou livre. Em Cristo sou livre
              Bm               A
Livre do engano de todos esse anos
              G                 Bm
Sou livre medo, sou servo do amor
              A
Conheço o segredo

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
G = 3 2 0 0 0 3
