Edson Gomes - Edson Em Oricuri

Intro 2x: G#m  B

Refrão 2x:
B                           G#m
O campo de batalha cheira a morte (cheira a morte, cheira a morte)
B                                  G#m
No campo de batalha a morte é mais forte

C#m             G#m
Alguem vencerá, alguém vencerá,
C#m               G#m
alguem morrerá, enfim

Refrão:
F#m                G#m
Vais matar a quem nunca viu,
F#m                  G#m
vais matar a quem não te fez nada (nada)
F#m              G#m
vais morrer por nada (por nada)
F#m          G#m
derramar teu sangue

F#m         G#m
em favor de quê, de quê (nada)

C#m       G#m
E a família te espera
C#m             G#m
toda a família desespera
C#m             G#m
lágrimas nos olhos, tristeza na face
C#m                     G#m
o peito apertado, pois sabe (pois sabe)
C#m                 G#m
que alguém vencerá, alguém vencerá
C#m               G#m
alguem morrerá, enfim

Solo 2x: B  G#m

C#m             G#m
Alguem vencerá, alguém vencerá,
C#m               G#m
alguem morrerá, enfim.

( B  G#m )
War inna Babylon

----------------- Acordes -----------------
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
