Edson Gomes - Árvore

Intro 2x.: Am C G Am G

E|------------------------------------------------------------------------|
B|--------------------------------8-----------8---------------------------|
G|--------9------------9-----7-9--------------------------9--9-9-7------7-|
D|-7-9-10---7---7-9-10---7-----------9p10-9-7-----7-9-10-----------10-9---|
A|------------------------------------------------------------------------|
E|------------------------------------------------------------------------|

   Am
E|-12/10--8/10-------|
B|--------------10=--|
G|--9/7---5/7--------|
D|---------------7=--|
A|-------------------|
E|-------------------|

Am          G                 Am
     Vem me regar mãe, vem me regar
            G                 Am
     Vem me regar mãe, vem me regar


C                    Am
Todo santo dia, pois todo o dia é santo
C                    Am
E eu sou uma árvore bonita
C                    Am
Que precisa ter os seus cuidados

          G           Am
Me regar mãe...Vem me regar
              G          Am
Vem me regar mãe, vem me regar

  C                    Am
E ando sobre a terra e vivo sob o sol
  C                 Am
E as, e as minhas raízes
  C                      Am
Eu balanço, eu balanço, eu balanço

 Ponte: Dm G Dm G Dm G
E|------------------------------------------------|
B|------------------------------------------------|
G|-10-10-10-10-10-9-7-9----10-10-10-10-10-9-7-9-7-|
D|----------------------9-------------------------|
A|------------------------------------------------|
E|------------------------------------------------|

 C                    Am
E ando sobre a terra e vivo sob o sol
  C                 Am
E as, e as minhas raízes
  C                      Am
Eu balanço, eu balanço, eu balanço

          G           Am
Me regar mãe...Vem me regar
              G          Am
Vem me regar mãe, vem me regar

Ponte: Dm G Dm G Dm G Dm G C

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
G = 3 2 0 0 0 3
