Edson Gomes - Fim do Mundo

 D                                    G
Nasci no fim de mundo vivo no fim do mundo
  D                                      G
Aqui nesse fim de mundo vivo como um condenado
                    F#m
Pois nada sobrou pra min
D                                    G
Quando as casas caem sinto-me triste de mais
              D                              G
Pois no meio dos escombros bem que eu poderia
           F#m                       G                     F#m
Eu poderia estar minha família os meus amigos
                  G                 A
A minha família estava lá.
D                                       G
Todo ano isso ocorre é e sempre o mesmo corre-corre
D                                       G
Todo ano a hipocrisia faz parte dessa agonia
F#m               G                     F#m                   G                                     A
Povos oportunistas vejam as vitimas de toda inoperância da brutal ganância.
D                                  G
Quando a chuva cai e um sacrifício a mais

   D                                         G                                         F#m
Agente já não vivi em paz e quando essa chuva cai piora tudo aqui
                G                                F#m                     G                    A
E agente fica assim pedindo clemência correndo risco tudo e perigo
                G                    F#m                            G                              A
Correndo risco a morte pulsa mais queremos ajuda mais não tem ajuda
                   G                              F#m                          G                                    A
Não temos culpa de sermos tão pobre assim e calamidade publica queremos ajuda.
D                                    G                                           D
Moro no fim do mundo vivo nesse fim de mundo rastejo aqui no fim do mundo
    G                                                        F#m                G                     F#m
E sinto um desgosto profundo E muito mais minha família os meus amigos
                    G                             A
Agora estão soterrados estavam lá.
D                     G  F#m               G                              F#m                                                A
Chuva lá vem ela chuva lá vem ela e vem sem dó lá vem lá vem lá vem lá vem lá vem.

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
