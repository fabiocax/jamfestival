Edson Gomes - Fogo Na Babilônia

Intro: (Dm Am) 4x
       (G  Am) 4x
Em                 Am
A Babilônia anda aflita
Em                      Am
Anda muito mais do que aflita
Dm                 Am
Agora eu toco fogo de vez
   Dm                    Am
Eu sou o incendiário do sistema
   Dm                    Am
Eu sou o incendiário do sistema

G
Olha, minha querida,
          Am
Não tenha medo
G
Olha, minha querida,
          Am
Não tenha medo


Dm           Am
Aquele mesmo Senhor
Dm               Am
Que tantas vezes falei
Dm              Am
Ele sempre estará aqui
  Dm              Am
Ele sempre estará comigo

G
Olha, minha querida,
          Am
Não tenha medo
G
Olha, minha querida,
          Am
Não tenha medo

Dm               Am
Se por acaso o sistema (sistema, sistema)
Dm               Am
Quiser tirar minha vida
Dm               Am
Talvez até possa conseguir
Dm               Am
Porém, minha alma, nunca (never)
Dm               Am
Porém, minha alma, nunca (never)
Dm               Am
Eu sei, eu sei, eu sei
Dm               Am
Eu sei, eu sei, eu sei

F                 C              F
Toda a brutalidade virá (brutality)
              G       F
A brutalidade virá (Sei)
                    C            F
Toda a brutalidade virá (brutality)
              G      Am
A brutalidade virá, ô!

         C
Eu toco fogo!
         Am
Eu toco fogo!
         C
Eu toco fogo!
         Am
Eu ponho fogo na Babilônia!

         C
Eu toco fogo!
         Am
Eu toco fogo!
         C
Eu toco fogo!
         Am
Eu ponho fogo na Babilônia!


Dm               Am
Se por acaso o sistema (sistema, sistema)
Dm               Am
Quiser tirar minha vida
Dm               Am
Talvez até possa conseguir
Dm               Am
Porém, minha alma, nunca (never)
Dm               Am
Porém, minha alma, nunca (never)
Dm               Am
Eu sei, eu sei, eu sei
Dm               Am
Eu sei, eu sei, eu sei

F                 C              F
Toda a brutalidade virá (brutality)
              G       F
A brutalidade virá (Sei)
                    C            F
Toda a brutalidade virá (brutality)
              G      Am
A brutalidade virá, ô!

         C
Eu toco fogo!
         Am
Eu toco fogo!
         C
Eu toco fogo!
         Am
Eu ponho fogo na Babilônia!

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
