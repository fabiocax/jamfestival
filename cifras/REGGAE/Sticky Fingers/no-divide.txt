Sticky Fingers - No Divide

[Intro] F#m  E  A  Bm
        F#m  E  A  Bm

D      E       Bm
But I know it's okay
D          E         Bm
I left your house, embrace
D                F#m
Please me on the win
D            F#m
Under your side I swim
D      C#m               E
'Cause I know there's no divide

[Solo] F#m  E  A  Bm
       F#m  E  A  Bm

[Pré-Refrão]

D      E       Bm
Open up what's on your mind

D          E         Bm
I am possessed as you are alive
D            F#m
Don't lead me on a whim
D            F#m
Without your heart I'm dim
D          C#m             E
'Cause I know there's no divide

[Refrão]

A    D            F#m
     When we're reckless
D                                     F#m
Our hands are only free when they're roped in a tide
D        F#m           E
Hold my silence, peace of mind
D           F#m
When we're reckless
D                               F#m
Our hands are running free when they're roped in a tide
D       F#m
Hold me tightly
     E
No divide

[Solo] F#m  E  A  Bm
       F#m  E  A  D

D        A
Break my head
           F#m
I'm not so easy, my friend
          A          Bm
I'm not your burden, deep down inside
F#m          D
And sometimes you, you push me to the edge
F#m          E            A
But you're the only one who tries
D     E        Bm             D
But I know it's okay
       E                  Bm
I never meant to lead you astray
D       F#m
Let's tee it up again
D          F#m
I could pull you on my heart strings
D           F#m    E
Because I know there's no divide

[Refrão]

A    D
     When we're reckless
D
Our hands are only free when they're roped in a tide
D        F#m           E
Hold my silence, peace of mind
D             F#m
When we're reckless
D                                 F#m
Our hands are running free when they're roped in a tide
D       F#m
Hold me tightly
     E
No divide

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
