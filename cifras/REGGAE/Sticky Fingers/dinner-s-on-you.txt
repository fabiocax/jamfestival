Sticky Fingers - Dinner´s On You

[Intro]  F  C  G

F                        C
  How does it feel to be blamed
      G                F
For things you didn't do

Don't be ashamed
 C             G               F
You can't control what isn't true

Yes they do
      C
They like what they hear
            G
Makes the world seem so clear
          F
Yes they do

And they knew
C       G         F
I feel sorry for you

C       G         F
I feel sorry for you

( F  C  G )

F
Flying high
 C                            G
Should have put more effort into your disguise
      F
Cause what you saw your mothers cries
         C
When the tears don't come along
          G
Cause she ain't got no eyes
     F       C  G
No more, no more
    F         C
No more, it's true
G                F
I feel sorry for you
C       G        F
I feel sorry for you
C       G        F
I feel sorry for you

( F  C  G )

F
  You're looking for some answers
          C               G
Down the street, over the hill
         F
Into the sun

All you found was heat, sweat
 C              G                 F
Stress, smoke, drugs, big ugly thugs

Life is so sweet
        C
It's a treat
                   G
And I'd share with you
          F
Who else knew
 C          G
Just how I feel
F
Before the strength they could muster
         C                 Dm
There's a man as tall as a roof
  F
A brand new generation of hustler
       C                  Dm
Their hearts they didn't move
       F
I can see they were coming in numbers
     C                   Dm
But there would just be a few
      F
Their coming to ease up their hunger
    C             G           F
It looks like the dinner's on you
C       G         F
I feel sorry for you
C       G         F
I feel sorry for you

( F  C  G )

----------------- Acordes -----------------
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
