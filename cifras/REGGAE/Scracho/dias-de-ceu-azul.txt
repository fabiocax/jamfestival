Scracho - Dias de Céu Azul

(intro) D5 C#5 B5 G5
        D5 C#5 B5 A5 G5

D5         C#5
Nessa caminhada
       B5                     G5       D5
Já passamos por bem que possa perceber
                C#5
Juntos nessa estrada
      B5                           G5
Seu caminho contra o meu foi sem querer
B5                   G5
Tudo que na vida aprendeu,
                D5
Sorriso de quem já perdeu
  A5                      D5
Que sabe que só temos a ganhar

(refrão)
           A5
Dias de céu azul

        B5                      G5     D5
Quando nada é impossivel conquistar
         A5
Dias de céu azul
     B5                       G5     D5
No final quero você como meu par
          A5
Dias de céu azul
     B5      G5
Eu vou aproveitar

(repete tudo)

Tudo faz sentido
Quando menos você esperar você vai perceber
Fique aqui comigo
Vamos ver nosso momento acontecer
Tudo que na vida aprendeu
Sorriso de quem já perdeu
Que sabe que só temos a ganhar

Dias de céu azul
Quando nada é impossivel conquistar
Dias de céu azul
No final quero vc como meu par
Dias de céu azul
Eu vou aproveitar


----------------- Acordes -----------------
A5 = X 0 2 2 X X
B5 = X 2 4 4 X X
C#5 = X 4 6 6 X X
D5 = X 5 7 7 X X
G5 = 3 5 5 X X X
