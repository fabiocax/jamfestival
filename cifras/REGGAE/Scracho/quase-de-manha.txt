Scracho - Quase de Manhã

(intro)

E |--------------------------------------------------------------------------------------------------|
B |--------------------------------------------------------------------------------------------------|
G |--------------------------------------------------------------------------------------------------|
D |--------------------------------------------------------------------------------------------------|
A |--------7-5-4--------4-5-2--------4-2-0-2-0~---------7-5-4---------4-5-2--------4-2-0-2-0-0-2-0---|
E |-5-5-5--------5-5-5--------5-5-5---------------5-5-5---------5-5-5---------5-5-5------------------|

        A                E
É quase de manhã e ainda não dormi
            F#m             D
Fiquei lembrando seu olhar
            A                       E
Se a gente não der certo eu não to nem aí
       F#m              D
Ainda vou poder sonhar

       F#m
Com você
        D            A                 E
Não importa, se ninguém souber o que aconteceu.

        F#m            D       A                E
Tanto faz, o que importa é saber que o seu sorriso é meu

A        E                   F#m
Faz um tempo quero te encontrar
          D             A
Falta coragem pra chegar
         E            F#m
Mas hoje eu já decidi
            D
Eu quero ter você pra mim

E |----------------------------------|
B |----------------------------------|
G |-------------6--------------------|
D |-------7/9/7--9---7/9/7-7---------|
A |------7---------------------------|
E |-----5----------5-----------------|

A                     E
Me dói te ver assim por ele
         F#m         D
Que não soube te amar
A                      E
E eu só queria te dizer
       F#m             D
Tenho tanto pra te dar

       F#m          D
E eu sei, não importa
       A                   E
Se ninguém souber o que aconteceu
       F#m            D
Tanto faz, o que importa
    A                E
É saber que o seu sorriso é meu

(refrão)
A       E                    F#m
Faz um tempo quero te encontrar
          D             A
Falta coragem pra chegar
         E                    F#m
Mas hoje eu já decidi
         D
Eu quero ter você pra mim

(solo) F#m D A E
(refrão 2x)

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
