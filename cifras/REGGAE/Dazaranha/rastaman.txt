Dazaranha - Rastaman

G#m                          C#m
Uma vela iluminava toda casa
C#m                          G#m
assar o peixe, faltava brasa
                     C#m
chaminé avisava pro lado de fora que rasta rezava com
G#m
Lenha seca, graveto primeiro
C#m
fogo na palha, mão de feiticeiro

Refrão (2x):
G#m               C#m
Rastaman, oh, oh, oh, oh

aquilo que tu falou
G#m               C#m
Rastaman, oh, oh, oh, oh

(Solo de violino, base do refrão 1x)


G#m
Disse que em nome da alegria
B                    C#
a tristeza não devia alcançar o coração
G#m
Disse que em nome da alegria
B                    C#
a tristeza não devia alcançar o coração

G#m
Diz que são nas páginas
              B
do dia a dia
                    G#m
é que se aprende a ler
               F#
os segredos da vida
G#m                     D#m
Dormir com saúde acordar
                            C#m
com disposição de bem com o mundo

beber água
B           C#m
    com prazer de beber

Refrão:
G#m
Oh, oh, oh, oh
C#m
aquilo que tu falou
G#m               C#m
rastaman, oh, oh, oh, oh

G#m
Rastaman, oh, oh, oh, oh
C#m
aquilo que tu falou
G#m               C#m
Rastaman, oh, oh, oh, oh

(Solo de violino, base do refrão 1x)

G#m
Disse que em nome da alegria
B                    C#
a tristeza não devia alcançar o coração
G#m
Diz que são nas páginas
              B
do dia a dia
                    G#m
é que se aprende a ler
               F#
os segredos da vida
G#m                     D#m
Dormir com saúde acordar
                            C#m
com disposição de bem com o mundo

beber água
B           C#m
    com prazer de beber

Refrão:
G#m
Oh, oh, oh, oh
C#m
aquilo que tu falou
G#m               C#m
rastaman, oh, oh, oh, oh

G#m
Rastaman, oh, oh, oh, oh
C#m
aquilo que tu falou
G#m               C#m
Rastaman, oh, oh, oh, oh

FINAL (repete a intro):

G#m                          C#m
uma vela iluminava toda casa
C#m                          G#m
assar o peixe, faltava brasa
                     C#m
chaminé avisava pro lado de fora que rasta rezava com
G#m
Lenha seca, graveto primeiro
C#m
fogo na palha, mão de feiticeiro
G#m
lenha seca, graveto primeiro
C#m  C#m  C#m
fogo!

----------------- Acordes -----------------
B = X 2 4 4 4 2
C# = X 4 6 6 6 4
C#m = X 4 6 6 5 4
D#m = X X 1 3 4 2
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
