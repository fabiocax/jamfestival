Dazaranha - Durma Bem

G
Durma Bem, menina, Durma Bem, Durma Bem, mamãe falou.
              Am               C                  G
Para ver nos sonhos todas as belezas que mamãe contou

Cresça bem, Cresça bem, menina, mamãe falou.
              Am                C                 G
Para ter nos sonhos todas as belezas que mamãe contou

Bm                Em               A
Não quer mais mamadeira, não, não, não.
                  C
Não quer mais mamadeira não, mamadeira não.
Bm                Em               A
Não quer mais mamadeira, não, não, não.
                  C                     G  (G F#)
Não quer mais mamadeira não, mamadeira não.

F                    C
Sexta-feira vai ter festa mãe,
              G                    (G F#)
João me convidou, eu vou, ou ououou.

F                    C
Na outra semana acampamento mãe
              G                    (G F#)
João me convidou, eu vou, ou ououou.
F                    C
Mês que vem vou pra amsterdam
              G                    (G F#)
João me convidou, eu vou, ou ououou.
F                    C         D
Necessitamos de mais grana, mãe.

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
