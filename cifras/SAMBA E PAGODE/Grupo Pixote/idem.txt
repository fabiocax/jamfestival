Grupo Pixote - Idem

(intro)   C7+   C7/11   F7+   G7/9   C9   C7/11   F7+   G7/9/11

C7+                            F7+       G7/9
   Você tem malícia no olhar, demorou... eu e você
C7+                        F7+    E7/9-   Am7
   Eu vejo um clima no ar de amor... de prazer
              D7/9       Am7              D7/9
É louca pra me namorar...   e diz que não dá pra me ver
   F7+    Em7       Dm7         Bm5-/7 E7/9-
Mas não dá...     não dá pra entender
Am7           D7/9         Am7                  D7/9
   Se eu sou um cara legal...   me deixa te amar de uma vez
   F7+    Em7       Dm7     G7/9/11
Meu amor...     pra valer
C7+      Am7
 Ídem...   ídem...
    Dm7          G7/9 G7       Em7
É o sentimento que eu tenho por você
          Ebº
 Ídem...   ídem...
   Dm7          G7/9
Eu fui me apaixonando sem querer

C7+      Am7
 Ídem...   ídem...
    Dm7          G7/9 G7       Em7
É o sentimento que eu tenho por você
           Ebº
 Ídem...   ídem...
 G7/9/11           G7
Eu fui me apaixonando sem querer

(repete tudo)

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Bm5-/7 = X 2 3 2 3 X
C7+ = X 3 2 0 0 X
C7/11 = X 3 3 3 1 X
C9 = X 3 5 5 3 3
D7/9 = X 5 4 5 5 X
Dm7 = X 5 7 5 6 5
E7/9- = X X 2 1 3 1
Ebº = X X 1 2 1 2
Em7 = 0 2 2 0 3 0
F7+ = 1 X 2 2 1 X
G7 = 3 5 3 4 3 3
G7/9 = 3 X 3 2 0 X
G7/9/11 = 3 X 3 2 1 X
