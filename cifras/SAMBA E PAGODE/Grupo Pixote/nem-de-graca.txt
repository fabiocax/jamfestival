Grupo Pixote - Nem de Graça

[Intro] Eb  F  Dm  Gm  Eb  F

            Eb                       F
É que não sobrou espaço pra outro alguém
                              Dm                   Gm
Minha Saudade só cabe no teu abraço, no de mais ninguém
      Eb                       F
Tenho dó de quem me conhecer agora
                             Dm
Que todo amor eu tô jogando fora
                                         Gm
E qualquer um que bate aqui nesse meu coração
                   Eb
Não passa nem da porta

F         Bb        Bb7      Eb
Se essa boca não beijasse tão bem
                               F
Se esse abraço não fosse tão massa
                                 Dm
Se que saber se eu quero outro alguém

                   Gm
Nem de graça, nem de graça

          Cm
Leva mal não
    Eb              F
Só tem espaço pra você no coração

[Solo] Eb  F

            Eb    F
É que não sobrou espaço

[Refrão]

F         Bb        Bb7      Eb
Se essa boca não beijasse tão bem
                               F
Se esse abraço não fosse tão massa
                                 Dm
Se que saber se eu quero outro alguém
                   Gm
Nem de graça, nem de graça

          Cm
Leva mal não
    Eb              F
Só tem espaço pra você no coração

[Final] Eb  F

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bb7 = X 1 3 1 3 1
Cm = X 3 5 5 4 3
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
