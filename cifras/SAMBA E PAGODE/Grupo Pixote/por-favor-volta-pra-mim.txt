Grupo Pixote - Por Favor Volta Pra Mim

(intro) A  D

A                       E
Minha vida fica tão sem graça
F#m                  Bm    D  E7
Quando você não está aqui
A                         E
Fico triste e o tempo não passa
F#m                 Bm    D  E7
Quando você não está aqui
A                     E
Eu preciso de você de dia
Bm                    D  E7
Eu preciso de você de noite
A                        E
Eu preciso de você o ano todo
Bm                          D   E7
Eu preciso de você por toda vida

A     E
Meu Amor

        F#m
Melhor amigo
     Bm  D
Por favor
     A             E7
Volta pra mim
A         E    F#m
Vem fazer amor comigo
    Bm          D            A        E7
Sem você é um sofrer sem fim (Bis)

(aumenta 1 tom)

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
