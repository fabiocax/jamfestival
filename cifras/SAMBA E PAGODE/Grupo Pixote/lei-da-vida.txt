Grupo Pixote - Lei da Vida

C
Faltou fidelidade eu demorei mas despertei
Em7
E já passou da conta toda vez que eu tentei
F                 Em
Priorizar o nosso sentimento
Dm                    G
Mas na realidade você nunca se importou
C
Peguei histórias comprometedoras no seu celular
Em7
E achei que porque não teve tempo

Nem maldade pra apagar
F                      Em
Agora não adianta você vir se declarar
Dm                            G
Dizendo que me ama e não quer mais se separar

C
Perdeu a parte colorida do seu dia

Em7
É meu o beijo que te deixa sem ar
F                   Em
Sou eu o cara que você se casaria
Dm                   G
Doeu e agora não tem como perdoar
C
Ninguém consegue ser feliz e amar sozinho
Em7
Porém vou dar meu jeito e seguir o meu caminho
F                               Em
Não vem contar histórias pra me desestruturar
Dm                      G
Na lei da vida se errou tem que pagar

----------------- Acordes -----------------
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
