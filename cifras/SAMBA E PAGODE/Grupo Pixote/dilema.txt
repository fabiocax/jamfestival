Grupo Pixote - Dilema

Intro: Bb9  Gm7/11  Eb7M  Ebm6

Bb9
Você vive deixando pra lá nossos problemas
Gm7/11
Nós chegamos ao ponto em que nada vai bem
Eb7M                    Ebm6
Se desculpar, justificar não é o que deve fazer
Bb9
Tanta coisa que eu aturei por tantos anos
Gm7/11
Sempre estive ao teu lado pra te entender
Eb7M                    Ebm6
Só que você nem quer saber
            Eb/F
Que estamos perto do fim

Segunda Parte:

Eb7M                            Bb/D
Sinto que a cada dia vai se desgastando

Gm7                     Cm7
O gosto dos seus beijos se amargando
      Eb/F                   Bb7M
Que estamos convivendo por obrigação
     Bb/G
Você diz que não
Eb7M                          Bb/D
Vejo você chorando ao sair do banho
    Gm7                     Cm7
Me surpreendo ao te ouvir dizendo
      Eb/F                   Bb7M
Que quer tentar recomeçar do zero
        Bb/G
Tudo outra vez.

Refrão:

Eb7M
Então vamos enfrentar os riscos, sem dilema
Bb/D                 Gm7
Todos os momentos vão valer a pena
Cm7                     Dm7
Pelo o menos podemos tentar
                     Eb7M
E fazer com que possa durar
                         Fm7  Bb/G
E que acertamos essa decisão.


Eb7M
Então vamos enfrentar os riscos, sem dilema
Bb/D                 Gm7
Todos os momentos vão valer a pena
Cm7                     Dm7
Eu só quero que seja feliz
                     Eb7M
Vou fazer tudo que eu não fiz
     Eb/F               Bb9
Pra salvar a nossa relação.

Repete Intro

Segunda Parte

Refrão

        Eb/F      Bb7M      Ab/C A/C# Bb/D
...Pra salvar a nossa relação.

Refrão

Final: Bb9  Gm7/11  Ebm6  Bb9

----------------- Acordes -----------------
A/C# = X 4 X 2 5 5
Ab/C = X 3 X 1 4 4
Bb/D = X 5 X 3 6 6
Bb/G = 3 X 3 3 3 X
Bb7M = X 1 3 2 3 1
Bb9 = X 1 3 3 1 1
Cm7 = X 3 5 3 4 3
Dm7 = X 5 7 5 6 5
Eb/F = X 8 8 8 8 X
Eb7M = X X 1 3 3 3
Ebm6 = X X 1 3 1 2
Fm7 = 1 X 1 1 1 X
Gm7 = 3 X 3 3 3 X
Gm7/11 = 3 X 3 3 1 X
