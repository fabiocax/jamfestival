Grupo Pixote - A Vitória

C                                   Am
Quantas vezes no caminho a gente pensou
                                F7+
Oh meu Deus como é difícil vencer
                                      Ab       C G7
Mesmo assim nenhum momento alguém desanimou
C                               Am
Sempre queremos dar de nós o melhor
                             F7+
Todos juntos como se fosse um só
     C                     Dm   Gm   C7
Que bom que a nossa vez chegou

F7+           Em7           Dm                 C7
Tudo posso naquEle que me Fortalece, basta só confiar
F7+                 Em7
Saber esperar no Senhor
    Dm
A vitória vem
G4/7       G7       C
Do céu, do Redentor


C7        F7+          Dm              Em
Obrigado Senhor, Por realizar nossos sonhos
    Am               Dm           G4/7  G7   C
E guardar os nossos passos pelos palcos da vida
C7        F7+            Dm           Em
Obrigado Senhor, por nos dar saúde, o dom e a voz
    Am         Dm
Por abençoar nossos versos

Sei que Seu poder está em nós

1a vez ( G4/7   G7     C)

2a vez ( F   G   Ab)

Final (F  G )

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
F7+ = 1 X 2 2 1 X
G = 3 2 0 0 0 3
G4/7 = 3 5 3 5 3 X
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
