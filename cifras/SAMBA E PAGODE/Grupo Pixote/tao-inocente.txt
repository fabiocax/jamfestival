Grupo Pixote - Tão Inocente

G7+                        Bm7
  De repente a gente se perdeu
                             C7+
Eu não sei como é que aconteceu
            Am7
Pego de surpresa
          D7/9/11
Fui tão inocente

G7+                        Bm7
  Essa paixão antiga eu não pensei
                            C7+
Que fosse me iludir agora eu sei
      Am7            D7/9/11          F#m5-/7  B7
Foi pura maldade o que ela fez com a gente

Em7                      Bm7
Ela decidiu entrar na relação
                           C7+
Tentou levar de vez meu coração
       Am7             D7/9/11       F#m5-/7  B7
Mas o amor que sinto por você me dispertou


Em7                        Bm7
Agora a gente pode se encontrar
                          C7+
Se você disser que vai voltar
     Am7                  D7/9/11
Pode ter certeza que tudo mudou

   G7+             Bm7
Ninguém te ama como eu
     Dm  G7    C7+
Não dá pra suportar
   Am7          C7+
Essa barra de viver
       D7/9           Bm7
Sem teu corpo junto ao meu
       E7           Am7
Eu não quero te perder
         D7/9/11   G7+       D7/9/11
Diz que nunca me esqueceu

( D7/13  D7 )

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
Bm7 = X 2 4 2 3 2
C7+ = X 3 2 0 0 X
D7 = X X 0 2 1 2
D7/13 = X 5 X 5 7 7
D7/9 = X 5 4 5 5 X
D7/9/11 = X 5 5 5 5 5
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
Em7 = 0 2 2 0 3 0
F#m5-/7 = 2 X 2 2 1 X
G7 = 3 5 3 4 3 3
G7+ = 3 X 4 4 3 X
