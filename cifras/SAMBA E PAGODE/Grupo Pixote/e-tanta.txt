Grupo Pixote - É Tanta

(intro) Gm7 C7 Bm7(b5) E7(b9) Am7 E7(b9)

   Am7                        Dm7                        G7                 C7M
É tanta... emoção que eu sinto toda vez que eu minto tentando esconder...
                     D#°                  Bm7(b5)               E7(b9)
Ela já não me ama... ela já não me vê como me viu um dia...
   Am7                    Dm7                       G7               C7M
A moça... resolveu ir embora... meu Deus e agora o que faço eu...
                          D#°                  Bm7(b5) E7(b9) Am7 (Am7 G#m7)
Falo que não foi nada... digo que não doeu pra tentar ocultar...
        G7/sus4        G7               C7M
        A vontade que sinto de saber, amor...
        Bm7(b5)  E7(b9)         Am7               A7
        O teu novo endereço e te escrever dizendo assim , amor...
        Dm7 Dm/C   Bm7(b5) E7(b9) Am7 (Am7 G#m7)
        Volta logo que a vontade de te ver é tanta...
        G7/sus4        G7               C7M
        A vontade que sinto de saber, amor...
        Bm7(b5)  E7(b9)         Am7               A7
        O teu novo endereço e te escrever dizendo assim , amor...
        Dm7 Dm/C   Bm7(b5) E7(b9) Am7  E7(b9)
        Volta logo que a vontade de te ver é tanta...

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am7 = X 0 2 0 1 0
Bm7(b5) = X 2 3 2 3 X
C7 = X 3 2 3 1 X
C7M = X 3 2 0 0 X
D#° = X X 1 2 1 2
Dm/C = X 3 X 2 3 1
Dm7 = X 5 7 5 6 5
E7(b9) = X X 2 1 3 1
G#m7 = 4 X 4 4 4 X
G7 = 3 5 3 4 3 3
G7/sus4 = 3 5 3 5 3 X
Gm7 = 3 X 3 3 3 X
