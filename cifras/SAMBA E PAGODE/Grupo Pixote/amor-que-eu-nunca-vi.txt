Grupo Pixote - Amor Que Eu Nunca Vi

C7+           Am7
 ..Eu nao vou..esquecer
F7+      F6        G7
..Meu amor..de voce...
C7+         Am7
..Foi demais..me perder
F7+     F6       G7 C7
..Encontrando voce..........


F7+                           C7+     C7   F7+
nunca vi um amor tao lindo assim...nunca vi...
                              C7+    C7     F7+
que paixao gostosa saindo de mim...nunca vi...
          Em           Am7
quanto desejo..quanta vontade
           Em7       Am
em nossos beijos..intimidade
           Dm   G7             C7+ G7
maravilhoso...amor que eu nunca vi



   C7+                   C7     F7+
sera que a gente vai se ver de novo
                  Em7
fazer amor maravilhoso
                       Dm    G7
que essa noite a gente fez
   C7+                C7      F7+
sera que voce vai sentir saudade
                       Em7
sabendo que a minha vontade
                   Dm   G7
e fazer tudo outra vez

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
C7 = X 3 2 3 1 X
C7+ = X 3 2 0 0 X
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F6 = 1 X 0 2 1 X
F7+ = 1 X 2 2 1 X
G7 = 3 5 3 4 3 3
