Zeca Pagodinho - Chico não vai na Curimba

Eb
Introdução:  Eb Fm Bb7 Eb Ab Bb7 Eb

                   Eb6/9
Chico não vai na curimba
Bb7                Eb
Chico não quer curimbar
                Bb7
Bebeu água de muringa
                  Eb
Dormiu no pé do gongá
                      Bb7
Hoje não faz mais mandinga
             Eb
Não que saracotear
C7                   Fm
Chico não ascende vela
             Bb7
Nem manda flores pro seu
  Eb
Orixá

C7                Fm
Ele é de banda cheirô
       Bb7         Eb
Ele é de banda cheirá
C7                Fm
Ele é de banda cheirô
       Bb7        Eb
Ele é de banda cheirá
(Chico não vai)
 C7                   Fm
* Chico não vai na curimba
Bb7                Eb
Chico não quer curimbar
                Bb7
Bebeu água de muringa
                  Eb
Dormiu no pé do gongá
                      Bb7
Hoje não faz mais mandinga
             Eb
Não que saracotear
C7                   Fm
Chico não ascende vela
             Bb7
Nem manda flores pro seu
  Eb
Orixá
Ele é de banda cherô...(Refrão)
2ª Vez  Eb Ab Fm7 Gm7
        Eb Ab Fm7 Eb
C7                  Fm
Não banho de arruda
Bb7               Eb
Nem banho de abô
C7              Fm
E nem sabe me dizer
                    Bb7
Se é de Keto, de Angola,
            Eb
De Jeje ou Nagô
C7                  Fm
Dizem pelos sete cantos
                Bb7         Eb
Que ele era um grande babaloxirá
Ele é de banda cherô...(Refrão)
C7                        Fm
Seu pai de santo no descarrego
Bb7                           Eb
um carrego até cair no chão
C7                  Fm
Chico arrebentou a guia
           Bb7              Eb
Nosso compadre não quer proteção
C7              Fm
Dispensou a rezadeira
          Bb7              Eb
Figa de Guiné e o velho patuá
(Refrão) Volta a *
(Refrão)
C7                  Fm
O santo invade o pagode
            Bb7             Eb
Aplica sacode em qualquer lugar
(Refrão)
C7                      Fm
Pastor, Padre, Pai de santo
            Bb7       Eb
Ninguém quer lhe abençoar
(Refrão)
C7                  Fm
E só mesmo o santo papa
      Bb7       Eb
para lhe exorcisar
(Refrão)

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Bb7 = X 1 3 1 3 1
C7 = X 3 2 3 1 X
Eb = X 6 5 3 4 3
Eb6/9 = X X 1 0 1 1
Fm = 1 3 3 1 1 1
Fm7 = 1 X 1 1 1 X
Gm7 = 3 X 3 3 3 X
