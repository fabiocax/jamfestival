Zeca Pagodinho - A Grande Família

       Bb7            Eb Ebm  F7             Bb   Bb A Ab G7
Esta família é muito unida, e também muito ouriçada,
                    C7/9 F7/9/11        F7        Bb
Brigam por qualquer razão, mas acabam querendo perdão.
        Cm7         F7/9            Bb
Pirraça pai, pirraça mãe, pirraça filha
                   Cm7          F7/9         Bb
Eu também sou da família, também quero pirraçar,
       Cm7        F7/9         Bb
Catuca pai, catuca mãe, catuca filha,
                  Em7/5b         A7         Dm7
Eu também sou da família, também quero catucar.
       Eb   E°      Bb
Catuca pai, mãe, filha,
     E°           Cm7          F7/9/11      Bb
Eu também sou da família também quero catucar.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Ab = 4 3 1 1 1 4
Bb = X 1 3 3 3 1
Bb7 = X 1 3 1 3 1
C7/9 = X 3 2 3 3 X
Cm7 = X 3 5 3 4 3
Dm7 = X 5 7 5 6 5
Eb = X 6 5 3 4 3
Ebm = X X 1 3 4 2
Em7/5b = X X 2 3 3 3
E° = X X 2 3 2 3
F7 = 1 3 1 2 1 1
F7/9 = X X 3 2 4 3
F7/9/11 = X X 3 3 4 3
G7 = 3 5 3 4 3 3
