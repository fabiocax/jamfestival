Zeca Pagodinho - Tá ruim mas tá bom

Introdução: C7 ^ F / C7 / Dm / Am7 / Bb / G / Bb / F /

 Dm      Gm7           C7         F
...Tô devendo, à dona Maria da quitanda
 Dm          Gm7         C7        F
Tá ruim pra mim, chego até passar de banda
          D7           Gm7
Pra dona Maria não me ver
              C7        F
Quando ela me vê, se zanga
          D7           Gm7
Pra dona Maria não me ver
              C7        F
Quando ela me vê, se zanga
(Eu tô devendo!)
Abº                      Gm7
...Quando chego mais a frente
           C7/13         C7          F
Bato de frente, com seu Manuel do botequim
                  D7          Gm7
Que me cobra uma pinga e um torresmo

            C7               F
Que tá no prego a mais de um mês
            D7               Gm7
Sem contar também....eu tô devendo
      C7         F
O aluguel do Português
Abº            C7                     F
...Sem um qualquer....é duro de se virar
      D7              G7/9
Eu envergo mais não quebro
  C7          F
Amanhã vai melhorar
Abº           C7                            F
...Eu vou a luta....e aturo os lamentos da Joana
            Abº         Gm7
Que não faz feira há semanas
             C7       F
E suplica ao menino Jesus
                  Dm        Gm7
Diz que o homem do gás não perdoa
             C7/13    Eb6
A light vai cortar a luz
           D7        Gm7
Mas eu tô legal numa boa
               C7/13       F
Lá vou eu carregando essa cruz
               Dm         Gm7
Tá ruim mas tá bom eu tenho fé          }
           C7      F
Que a vida vai melhorar                 }
                D7       Gm7                bis
Oi, segura as pontas seu Zé             }
              C7/13   F
Eu devo mais quero pagar                }
                                 C7            F
Dezessete e cinqüenta de leite e pão.....na padaria      }b
                                  C7               F
Vinte pratas que Jorge bicheiro emprestou.lá na tendinha }i
                           C7           F
O carnê da televisão que pifou....na garantia            }s
                          C7                 F
Uma vela a minha mulher acendeu....de sete dias..        }

----------------- Acordes -----------------
Abº = 4 X 3 4 3 X
Am7 = X 0 2 0 1 0
Bb = X 1 3 3 3 1
C7 = X 3 2 3 1 X
C7/13 = X 3 X 3 5 5
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Eb6 = X X 1 3 1 3
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7/9 = 3 X 3 2 0 X
Gm7 = 3 X 3 3 3 X
