Zeca Pagodinho - Hoje é dia de festa

Introdução: D / B7 / Em7 / % / A7 / D / A7 /

        D                     B7                  Em7    G5+
Hoje é dia de festa em todos os terreiros ouviu, Maria
      A                     A7                 D   A7
Hoje dia de todos os Santos, bom dia pra te curiar
   D                   B7                     Em7
O coro ta comendo essa hora também na casa de sinha
    A7                                       D      A7
Até cobra deve estar fumando minha gente por lá   (auê, auê)
    D           B7         Em  F#m  G
Auê, auê, auê, auê, auê, auá
         A7                                         D     A7
Hoje eu quero saber como está minha banda no pé do congá (auê, auê)
  A7   D                 B7              Em7          G5+
Olho ruim é pior do que praga de mãe e madrinha
      A              A7                         D    A7
Olho ruim também seca, qualquer pimenteira da vida
           D                        D5+          G
Macacos me mordam, se a gente não tá cheio de magia
          A7                         D
E se eu estiver  a Sinhá vai tirar Maria

A7          D                B7         Em7
Até no bom tempo não se pode confiar, Maria
           A7                               D
Eu já vi chover, com tremendo sol quente, Maria
                A7                              D
Seguro morreu de velho, e desconfiada ficou, Maria
         A7                                    D
Coma com Pedro, mas sempre de olho no Pedro, Maria
A7             D                   B7          Em7  G5+
Na Bahia minha gente, os tambores tocam noite dia
           A                          A7                D
Porque os baianos, não dormem com os olhos dos outros Maria

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
D = X X 0 2 3 2
D5+ = X 5 4 3 3 X
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G5+ = 3 X 1 0 0 X
