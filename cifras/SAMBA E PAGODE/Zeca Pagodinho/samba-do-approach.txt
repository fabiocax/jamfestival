Zeca Pagodinho - Samba do Approach

[Intro]  F  Bb  F  Bb  F  C  Bb  F  C  Bb  F


                      F  E7  Eb7  D7                       Gm
Venha provar meu brunch         saiba que eu tenho approach
            C7                     F             C7
Na hora do lunch, eu ando de ferryboat, venha provar!
                  F         D7                    Gm
Venha provar meu brunch... saiba que eu tenho approach
            C7                    F
Na hora do lunch, eu ando de ferryboat

F  Eb7  D7                 Gm   C7                 F
       Eu tenho savoir-faire  meu temperamento é light
                Cm          F7             Bb
Minha casa é hi-tech, toda hora rola um insight
                    Bbm6   Eb7                Am7
Já fui fã do Jethro Tull,  hoje me amarro no Slash
       D7          G7            C7           F
Minha vida agora é cool, meu passado já foi trash


F7                 Bb      G7                    Cm
  Venha provar meu brunch  saiba que eu tenho approach
            F7                    Bb            F7
Na hora do lunch, eu ando de ferryboat, beautifull!
                   Bb     G7                    Cm
Venha provar meu brunch  saiba que eu tenho approach
            F7                    Bb
Na hora do lunch, eu ando de ferryboat

G7             Cm        F7                  Bb
Fica ligado no link  que eu vou confessar my love
                 Fm           Bb7           Eb
Depois do décimo drink, só um bom e velho engov
                    Ebm6  Ab7                Dm
Eu tirei meu green card   e  fui para Miami Beach
      G7            C7          F7          Bb   C7
Posso não ser pop star, mas já sou um noveu riche

                      F  E7  Eb7  D7                       Gm
Venha provar meu brunch         saiba que eu tenho approach
            C7                     F             C7
Na hora do lunch, eu ando de ferryboat, venha provar!
                  F         D7                    Gm
Venha provar meu brunch... saiba que eu tenho approach
            C7                    F
Na hora do lunch, eu ando de ferryboat

F  Eb7  D7             Gm     C7                F
      Eu tenho sex-appeal   saca só meu background
                  Cm    F7            Bb
Veloz como Damon Hill, tenaz com Fittipaldi
                      Bbm6   Eb7                Am7
Não dispenso um happy end, quero jogar no dream team
   D7           G7          C7             F   F7
De dia um macho man   e de noite drag queen

F7                 Bb      G7                    Cm
  Venha provar meu brunch  saiba que eu tenho approach
            F7                    Bb            F7
Na hora do lunch, eu ando de ferryboat, beautifull!
                   Bb     G7                    Cm
Venha provar meu brunch  saiba que eu tenho approach
            F7                    Bb
Na hora do lunch, eu ando de ferryboat

----------------- Acordes -----------------
Ab7 = 4 6 4 5 4 4
Am7 = X 0 2 0 1 0
Bb = X 1 3 3 3 1
Bb7 = X 1 3 1 3 1
Bbm6 = 6 X 5 6 6 X
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
Eb = X 6 5 3 4 3
Eb7 = X 6 5 6 4 X
Ebm6 = X X 1 3 1 2
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
Fm = 1 3 3 1 1 1
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
