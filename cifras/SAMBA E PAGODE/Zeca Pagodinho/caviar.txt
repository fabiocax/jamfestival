Zeca Pagodinho - Caviar

Intr: G  D7  G  D7  G

Solo (transposição do cavaquinho para a guitarra)

E|------------7--10--8----------7-8-7----------------------8--12--10--8--7--8--10--
B|-----8--10------------10--10---------10--8--7-----7--10--------------------------
G|--7--------------------------------------------7---------------------------------
D|---------------------------------------------------------------------------------
A|---------------------------------------------------------------------------------
E|---------------------------------------------------------------------------------

E|------------7--10--8----------7-8-7----------------------------7-----------------
B|-----8--10------------10--10---------10--8--7--------7--8--10-----8--7-----------
G|--7--------------------------------------------7--9---------------------9--7-----
D|---------------------------------------------------------------------------------
A|---------------------------------------------------------------------------------
E|---------------------------------------------------------------------------------

Você sabe o que é caviar?
               D7                G
Nunca vi nem comi eu só ouço falar   2x


      Refrão
  G
Caviar é comida de rico
          E                Am
Curioso fico só sei que se come
                             D
Na mesa de poucos fartura adoidado
                  D7                 G
Mas se olha pro lado depara com a fome

Sou mais ovo frito, farofa e torresmo
               G7                    C
Pois na minha casa é o que mais se consome
                D7         G
Por isso, se alguém me perguntar

                D7                 G
...O que é caviar? só conheço de nome

Refrão:
 G
Geralmente quem come esse prato
               E
Tem bala na agulha
               Am
Não é qualquer um
                              D
Quem sou eu pra tirar essa chinfra
             D7              G
Se vivo na vala pescando muçum

Mesmo assim não reclamo da vida
             G7              C
Apesar de sofrida, consigo levar
            D7           G
Um dia eu acerto numa loteria
          D7                 G
E dessa iguaria até posso provar

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
