Zeca Pagodinho - Cadê o Meu Amor

D7+      C#m7     A7+   Em7  A7
cade meu amor tao bonito
D         E7/9    A7+      Em7  A7
do inicio da paixao
 D          E7/9  A7+     Em7  A7
cade mina felicidade
 F#m        Bm    E    E7
saudade nao abre mao

A7+             D      A7+
errei quando me apaixonei
          D       A7+
quando lhe entreguei
           D    E7/9
todo o meu coraçao

A7+            D      A7+
cantei , tristeza se acabou
       D        A7+
quando voce chegou
        E7/9    A7+     Em7   A7
mas foi so ilusao


D7+      C#m7       A7+    Em7   A7
cade meu amor tao bonito
D         E7/9   A7+     Em7   A7
do inicio da paixao
 D           E7/9   A7+   Em7   A7
cade minha felicidade
 F#m        Bm    E     E7
saudade nao abre mao

D7+           C#m7      A7+   Em7  A7
qem fez desse amor agasalho
D         E7/9    A7+    Em7    A7
orvalho foi temporal
 D           E7/9   A7+     Em7   A7
num sonho de primavera
 F#m        Bm    E     E7
garoa fez um vendaval

 D         E7/9         A7+
amargo essa dor no meu canto
           D           A7+
sabor tao ruim , desencanto
 D           C#m7    Em7    A7
como doi desilusao
 D       E7/9          A7+
pena esse amor,eu quis tanto
       D              A7+
emtroca recebo esse pranto
                D/E  E7/9   A7+
pra banhar minha so...li...dao


----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
Bm = X 2 4 4 3 2
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
D/E = X X 2 2 3 2
D7+ = X X 0 2 2 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
E7/9 = X X 2 1 3 2
Em7 = 0 2 2 0 3 0
F#m = 2 4 4 2 2 2
