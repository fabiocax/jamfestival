Zeca Pagodinho - Cidade do Pé Junto

Introdução: F

         C7
(Meu coração)
      F                 D7    Gm
Meu coração era uma rosa   em botão
Hoje é saudade
      C7                       F     C7
Felicidade ao me ver nem tem assunto
           F            D7        Gm
É que a saudade que eu sinto de você
Meu grande amor
         C7                   F
Vai me levar pra cidade do pé junto
         C7
Longe de ti
                  F
A minh'alma veste luto
         C7                      F
Eu me pergunto como pude me entregar
         C7                   F
Eu que sorria, hoje vivo a chorar

D7
No meu pomar
Gm             C7           F
O amor que plantei não deu fruto
              C7
É que a saudade... A

----------------- Acordes -----------------
A = X 0 2 2 2 0
C7 = X 3 2 3 1 X
D7 = X X 0 2 1 2
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
