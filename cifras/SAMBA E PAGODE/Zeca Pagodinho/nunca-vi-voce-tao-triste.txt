Zeca Pagodinho - Nunca vi você tão triste

Introdução:  Gm Gm5+ Gm C7 Am5-/7 D7/9- Gm D7 Gm A7 D7 Gm D7

   Gm7                        Cm
Eu nunca ví você tão triste assim
                   Am5-/7
Falando coisas, Reclamando
D7/9-          Gm7        A7-D7/9-
Contra todos e até de mim
Gm7               Dm7
Sempre ví você cantando alegremente
  Cm            F7             Bb7M D7
Regando as flores do nosso jardim
                   Gm      Gm7
Mas não sei o que aconteceu
                    Cm
O que foi que te entristeceu
             F7/4 F7
Quando amanheceu
                Bb7M
Não te ví sorridente
Gm               A7                   Am5-/7

Uma tristeza aparente se espalhava no ár
Am5-/7          D7                   Gm
Será que fiz alguma coisa que pra te magoar
  Am5-/7
Se fiz amor
   Gm7
Não tive a intenção
    A7
Peço perdão, Oh! Flor
 Am5-/7            D7
Se magoei teu coração
   Am5-/7
Por que não tentar
   Gm7
Sentar, resolver
        A7
Sem ninguém opinar
  Am5-/7     D7
Só eu e você
 Am5-/7      D7/9-         Gm7  Bb6
O nosso amor jamais será vencido
 Am5-/7      D7/9-           Gm Gm7M
Se em partes iguais for dividido
   G7/4          G7    Cm6       Am5+ Am6
Porque você não diz o que te magoou
   Gm         A7----D7     Gm Gm7M

   G7/4          G7    Cm6       Am5+ Am6
Porque você não diz o que te magoou
   Gm         A7----D7     Gm 


----------------- Acordes -----------------
