Zeca Pagodinho - Pra São Jorge

Solo violão: 26 26  26 26 26  26 24 23 24   23 24  24 23 35 33 23
             43 43  43 43 43  43 45 33 23   35 23  35 35 35 33 33

Intr: Bb % Eb F Bb % Eb F Bb

%          Eb         F       Bb %
Vou ascender velas para São Jorge
          Eb      F    Bb
A ele eu quero agradecer
          Eb         F       Bb  Bb/Ab
e vou plantar comigo-ninguém-pode

para que o mal não possa então vencer     2x

      Eb                Bb       F     Bb Bb/D
Olho grande em mim não pega não pega não
     Eb               Bb    F   Bb
Não pega em quem tem fé no coração

      Eb                Bb/D       F   Bb
Olho grande em mim não pega não pega não

     Eb               Gm    F   Bb
Não pega em quem tem fé no coração

 Eb             Bb        Eb       Bb
Ogum com sua espada sua capa encarnada
 Cm         F       Bb
Me dá sempre proteção
 Cm                  Dm       Eb            Dm/F
Quem vai pela boa estrada no fim dessa caminhada
   F               Bb
Encontra em Deus perdão                           2X

Final
Solo violão

Bb              Bb              Bb                  Bb
Lá la iá lá iá...........  Vamos saudar São Jorge Cavaleiro!

G7M(b5)
   Ogum meu pai segura nós!

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bb/Ab = 4 X 3 3 3 X
Bb/D = X 5 X 3 6 6
Cm = X 3 5 5 4 3
Dm = X X 0 2 3 1
Dm/F = X X 3 2 3 1
Eb = X 6 5 3 4 3
Eb/G = 3 X 1 3 4 X
F = 1 3 3 2 1 1
G7M(b5) = 3 X 4 4 2 X
Gm = 3 5 5 3 3 3
