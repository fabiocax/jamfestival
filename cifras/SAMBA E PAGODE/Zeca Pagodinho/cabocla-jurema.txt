Zeca Pagodinho - Cabocla Jurema

Introdução: Eb % % % % % % % % % %?

                   Fm
  MEU DOCE DE CAMBUCÁ
Bb7                         Eb    Fm    Eb
  MINHA FLOR CHEIROSA DE ALFAZEMA
  Cm               Fm
  TEM PENA DESSE CABOCLO
                       Bb7
  O QUE EU TE PEÇO É TÃO POUCO
                     Eb    Bb7    Eb
  MINHA LINDA CABOCLA JUREMA

     Cm              Fm
  TEM PENA DE UM SOFREDOR
Bb7                    Eb    Fm    Eb
  QUE O MAU DESTINO CONDENOU
  Cm                 Fm
  ME LIBERTA DESTA ALGEMA
                 Bb7
  ME TIRA DESSE DILEMA

                      Eb    Ab7M    Eb
  MINHA LINDA CABOCLA JUREMA

   Cm               Fm
  VOU PEGAR MINHA VIOLA (OÔ, OÔ)
Bb7                 Eb    Fm    Eb
  MEU FAÇÃO E MEU BORNAL (OÔ, OÔ)
  Cm               Fm
  NUMA CASA DE CABOCLO
                   Bb7
  SE UM SOZINHO É SUFOCO
                  Eb    Fm    Eb
  DOIS É QUE É O IDEAL

   Cm              Fm
  VOU PEGAR MINHA CANOA (OÔ, OÔ)
Bb7                  Eb    Fm    Eb
  VOU ME EMBORA PRA GOIÁS (OÔ, OÔ)
  Cm             Fm
  NUMA CASA DE CABOCLO
                        Bb7
  SE DOIS É BOM E UM É POUCO
                      Eb
  TRÊS CERTAMENTE É DEMAIS

----------------- Acordes -----------------
Ab7M = 4 X 5 5 4 X
Bb7 = X 1 3 1 3 1
Cm = X 3 5 5 4 3
Eb = X 6 5 3 4 3
Fm = 1 3 3 1 1 1
