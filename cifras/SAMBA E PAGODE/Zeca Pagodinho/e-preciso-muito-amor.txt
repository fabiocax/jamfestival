Zeca Pagodinho - É Preciso Muito Amor

Intro: A7+ A#m7/5- E  C7 F B7
         (E B/D# C#m7 B7)
E                             F#
É preciso muito amor
F#m           B7                E
Para suportar essa mulher
D                     E                A
Tudo que ela vê numa vitrine ela quer
F#
Tudo que ela quer
                    B7
Tenho que dar sem reclamar

                  A/C#  Cº C#m7
Porque senão      ela chora
A                          E
E diz que vai embora
         B7                  E    E7/9
Oh, diz que vai embora!
                  A/C#  Cº C#m7
Porque senão      ela chora

A                          E
E diz que vai embora
         B7                  E    B7
Oh, diz que vai embora!

E                             F#
É preciso muito amor
F#m           B7                E
Para suportar essa mulher
D                     E                A
Tudo que ela vê numa vitrine ela quer
F#
Tudo que ela quer
                    B7
Tenho que dar sem reclamar

                  A/C#  Cº C#m7
Porque senão      ela chora
A                          E
E diz que vai embora
         B7                  E    E7/9
Oh, diz que vai embora!
                  A/C#  Cº C#m7
Porque senão      ela chora
A                          E
E diz que vai embora
        B7                  E
Oh, diz que vai embora!


 Ab                               C#m
Pra satisfazer essa mulher
 B7                                  E
Eu faço das tripas coração
       Ab                          C#m
Pra ela sempre digo "sim"
Bbº                             B7
Pra ela nunca digo "não"


                  A/C#  Cº C#m7
Porque senão      ela chora
A                          E
E diz que vai embora
         B7                  E    E7/9
Oh, diz que vai embora!
                  A/C#  Cº C#m7
Porque senão      ela chora
A                          E
E diz que vai embora
        B7                  E      B7
Oh, diz que vai embora!

----------------- Acordes -----------------
A = X 0 2 2 2 0
A#m7/5- = X 1 2 1 2 X
A/C# = X 4 X 2 5 5
A7+ = X 0 2 1 2 0
Ab = 4 3 1 1 1 4
B/D# = X 6 X 4 7 7
B7 = X 2 1 2 0 2
Bbº = X 1 2 0 2 0
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
C7 = X 3 2 3 1 X
Cº = X 3 4 2 4 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7/9 = X X 2 1 3 2
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
