Zeca Pagodinho - Judia de Mim

Introdução:   Am D7 G B7 C D7 G Gº G

            G   F#7 E7   Am
Judia de mim,         judia
               D7           G
Se não sou merecedor desse amor
         G Am A#m Bm               Bbº
Se eu choro      será que voce não notou
      Am
É a você que eu adoro
  D7                                 G7+ E7 Am D7 G
Carrego esse meu sentimento sem ressentimentos
E7  Am            D7       Bm        Bbº
É a cana quando é boa se conhece pelo nó
    Am          D7       G          E7
Assovio entre os dentes, uma cantiga dolente
      Am        D7           G     F7     E7
Entre cacos e cavacos sobrei eu, duro nos cascos
       Am           D7        G    E7
Bem curtido pelo cheiro do sovacos
        Am             D7      Bm                 Bbº
Quem dança qualquer dança, a bonança não sabe o que é

       Am        D7      G                E7
Desconhece a esperança no falso amor leva fé
        Am         D7         G      F      E7
Quem de paz se alimenta se contenta com migalhas
         Am         D7              G
Não se aflige e corrige as próprias falhas

----------------- Acordes -----------------
A#m = X 1 3 3 2 1
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
Bbº = X 1 2 0 2 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
F#7 = 2 4 2 3 2 2
F7 = 1 3 1 2 1 1
G = 3 2 0 0 0 3
G7+ = 3 X 4 4 3 X
Gº = 3 X 2 3 2 X
