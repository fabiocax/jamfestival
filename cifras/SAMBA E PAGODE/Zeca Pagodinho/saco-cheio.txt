Zeca Pagodinho - Saco Cheio

Grupo: Almir Guineto/Zeca Pagodinho

Introdução: F, D7, Gm7, C7, F,C7

F                  C7             F Am
Os habitantes da Terra estão abusando
   Cm7              F7             Bb
Do nosso supremo divino sobrecarregando
C7                        F             G#º
Fazendo mil besteiras e o mal sem ter motivo
   G7/9            C7                        F    C7
E só se lembram de Deus quando estão em perigo
F                        Gm             Am    G#º
Deus lhe pague, Deus lhe crie, Deus lhe abençoe
  G7/9         C7           F      F6
Deus, é nosso pai é nosso guia
C7                              F            D7
Tudo que se faz na Terra, se coloca Deus no meio
Gm7             C7             F    F6
Deus já deve estar de saco cheio.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C7 = X 3 2 3 1 X
Cm7 = X 3 5 3 4 3
D7 = X X 0 2 1 2
F = 1 3 3 2 1 1
F6 = 1 X 0 2 1 X
F7 = 1 3 1 2 1 1
G#º = 4 X 3 4 3 X
G7/9 = 3 X 3 2 0 X
Gm = 3 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
