Zeca Pagodinho - Se Ela Não Gosta de Mim

[Intro]  Eb  Ebm  Bb  G7  Cm  F  Bb  F

Bb
Se ela não gosta de mim
A7
Se ela não me adora
D
Juro que eu vou jogar
Fm                Bb
O meu cavaquinho fora
Eb                   Ebm
Se ela não gosta de mim
Bb                   G7
Assim como eu gosto dela
Cm                    F          Bb    F
Juro que ela não sai mais na Portela

Bb
Se ela não gosta de mim
A7
Se ela já não me adora

D
Juro que eu vou jogar
Fm                Bb
O meu cavaquinho fora
Eb                   Ebm
Se ela não gosta de mim
Bb                   G7
Assim como eu gosto dela
Cm                    F          Bb
Juro que ela não sai mais na Portela

Bb                   Eb
Se ela não gosta de mim
F                      Bb
Eu perco a luz do meu céu
                  Bb7
Sou capaz de proibi-la
                              Eb
E ela não desfila na Vila Isabel
                     Ebm
Se ela não gosta de mim
           F        Bb
Só por castigo este ano
      G7             Cm
Juro por Deus, meu amigo
                   F           Bb
Que ela não sai no Império Serrano
            F
Se ela não gosta

Bb
Se ela não gosta de mim
A7
Se ela já não me adora
D
Juro que eu vou jogar
Fm                Bb
O meu cavaquinho fora
Eb                   Ebm
Se ela não gosta de mim
Bb                   G7
Assim como eu gosto dela
Cm                    F          Bb
Juro que ela não sai mais na portela

Bb                   Eb
Se ela não gosta de mim
F                Bb
Agora vai ser assim
                          Bb7
Não vou mais pra minha escola
                      Eb
E vou cantar no botequim
                     Ebm
Se ela não gosta de mim
    F               Bb
Eu posso até ficar louco
    G7              Cm
Em vez de beber cerveja
       F           Bb
Vou beber água de coco
            F
Se ela não gosta

Bb
Se ela não gosta de mim
A7
Se ela já não me adora
D
Juro que eu vou jogar
Fm                Bb
O meu cavaquinho fora
Eb                   Ebm
Se ela não gosta de mim
Bb                   G7
Assim como eu gosto dela
Cm                    F          Bb
Juro que ela não sai mais na portela

Bb                   Eb
Se ela não gosta de mim
   F                    Bb
E não quer mais meu amor
                        Bb7
Não sai mais na Grande Rio, Mangueira
                           Eb
Salgueiro e nem na Beija-Flor
                Ebm         F          Bb
E até Padre Miguel disse aqui ela não sai
    G7            Cm
Em São Paulo proibiu
            F             Bb
De ir no Camisa e no Vai-Vai
            F
Se ela não gosta

Bb
Se ela não gosta de mim
A7
Se ela já não me adora
D
Juro que eu vou jogar
Fm                Bb
O meu cavaquinho fora
Eb                   Ebm
Se ela não gosta de mim
Bb                   G7
Assim como eu gosto dela
Cm                    F          Bb  Bb7
Juro que ela não sai mais na Porteeela

Eb                   Ebm
Se ela não gosta de mim
Bb                   G7
Assim como eu gosto dela
Cm                    F          Bb  Bb7
Juro que ela não sai mais na Portela

E|----------------------------|
B|-6--------------------------|
G|------7---------------------|
D|--------8----8--------------|
A|-----------8----------------|
E|----------------------------|

----------------- Acordes -----------------
A# = X 1 3 3 3 1
A#7 = X 1 3 1 3 1
A7 = X 0 2 0 2 0
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D# = X 6 5 3 4 3
D#m = X X 1 3 4 2
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G7 = 3 5 3 4 3 3
