Zeca Pagodinho - Shopping Móvel

Introdução:  Db | % | Abm | % | Gb | % | Ebm | Ab7 | Db | % | Gbm6 | Ab7 |...

           Db               Ab7             Db
Tem sempre tudo no trem que sai lá da central
 Abm7           Db7                Gb9        Gb7+
Baralho, sorvete de côco, corda pro seu varal
      Gb6         Gbm6         Fm7           Eb7/9
Tem canivete, benjamim, tem cotonete, amendoim
        Ab7/9                   Ebm7   Ab7
Sonho de valsa... e biscoito integral
          Db               Ab7             Db
Tem sempre tudo no trem que sai lá da central
    Abm7            Db7           Gb9      Gb7+
Chiclete, picolé do China e guaraná natural
      Gb6       Gbm6          Fm7          Eb7/9
Tem agulheiro, paliteiro, desodorante, brigadeiro
         Ab7/9                         Abm7   Db7
E um bom calmante quando a gente passa mal
      Gm5-/7           Gm6                Fm7            Bb7
E quem quiser pode comprar... o shopping móvel é isso aí
   Ebm7                 Ab7      Abm7    Db7
É promoção desde a Central a Japeri

      Gm5-/7           Gm6             Fm7            Bb7
E quem quiser pode comprar... um bom pedaço de cuscuz
   Ebm7                 Ab7          Db    Bb7
E mastigar desde a Central a Santa Cruz
   Ebm                 Ab7              Db
CD pirata da Frank Sinatra a Zeca Pagodinho
     Abm7                Db7               Gb9    Gb7+
E até aquele veneno pra rato chamado chumbinho
     Gb6      Gbm6         Fm7         Bb7
Bala de côco, pirulito, suco de frutas no palito
         Ebm7        Ab7/9             Bbm7  Am7   Abm7  Db7
Cuscuz, cocada... pasteizinhos de palmito
     Gm5-/7         Gbm6        Fm7            Bb7
Despertador, rádio de pilha... ventilador e sapatilha
   Ebm7        Ab7             Abm7    Db7
Até peruca é possível se encontrar
    Gm5-/7        Gbm6            Fm7            Bb7
O pagamento é no cartão, vale-transporte ou refeição

Qualquer pessoa jamais fica sem comprar

----------------- Acordes -----------------
Ab7 = 4 6 4 5 4 4
Ab7/9 = 4 X 4 3 1 X
Abm = 4 6 6 4 4 4
Abm7 = 4 X 4 4 4 X
Am7 = X 0 2 0 1 0
Bb7 = X 1 3 1 3 1
Bbm7 = X 1 3 1 2 1
Db = X 4 6 6 6 4
Db7 = X 4 3 4 2 X
Eb7/9 = X 6 5 6 6 X
Ebm = X X 1 3 4 2
Ebm7 = X X 1 3 2 2
Fm7 = 1 X 1 1 1 X
Gb = 2 4 4 3 2 2
Gb6 = 2 X 1 3 2 X
Gb7+ = 2 X 3 3 2 X
Gb9 = 2 4 6 3 2 2
Gbm6 = 2 X 1 2 2 X
Gm5-/7 = 3 X 3 3 2 X
Gm6 = 3 X 2 3 3 X
