Zeca Pagodinho - Perfeita harmonia

Introdução:  / Em7 / F#m7 / B7 / Em7 ^ F#m7 ^ G ^ A7 / D / A7/13 /
   D                B7              Em7     E7
É divino o meu despertar com os pássaros
Em7                A7         D ^ B7 ^ Em7 ^ A7
Entoando um canto sutil de amor
  D                 B7           Em7
O padeiro grita olha o pão que alegria
B7   Em7                            A7      D
Contemplando o sol no horizonte novo dia
A7   D                    B7       Em7
.....O carinho de pai e de mãe é verdade
  A7                               Am7        D7/9
É o mesmo de ter prazer para eternidade
    G7+                  G6
Ver criança brincando feliz
 F#m7                  B7
Exalando um clima de festa
Em7                        A7
Oh! Meu Deus como é bom conviver
              Am7   D7/9
Numa paz como esta

   G7+                 Gm6
À tardinha a brisa no jardim
  F#m7                  B7
Embalando as flores tão belas
 Em7                   A7
Exalando um doce perfume
         Am7     D7/9
É a primavera
   G7+                   Gm6
Às seis horas sagrada oração
 F#m7                  B7
Doce lar em repleta harmonia
 Em7                   A7
Agradecendo a Deus toda paz
               Am7  D7/9
E o pão de cada dia
   G#m5-/7              Gm6
Às seis horas sagrada oração
 F#m7                 B7
Doce lar em repleta harmonia
Em7      F#m7            G
Agradecendo a Deus toda paz
    A7           D    A7
E o pão de cada dia..

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7/13 = X 0 X 0 2 2
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
D = X X 0 2 3 2
D7/9 = X 5 4 5 5 X
E7 = 0 2 2 1 3 0
Em7 = 0 2 2 0 3 0
F#m7 = 2 X 2 2 2 X
G = 3 2 0 0 0 3
G#m5-/7 = 4 X 4 4 3 X
G6 = 3 X 2 4 3 X
G7+ = 3 X 4 4 3 X
Gm6 = 3 X 2 3 3 X
