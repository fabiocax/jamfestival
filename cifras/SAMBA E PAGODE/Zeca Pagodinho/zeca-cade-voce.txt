Zeca Pagodinho - Zeca, cadê você?

Zeca Pagodinho - P.E. Jorge Aragão, seu Jorge, Marcelo D2 e Baixinho

Introd.: E  E7  A  Am  Gm  C#7  F#m  B7  E  B7


    E             C#7        F#m
Ô  Zeca tu tá morando ondé? ( eu não sei onde é que é)
    B7                     E B7
Ô  Zeca tu tá morando ondé?

   E         C#7          F#m                     B7                                    E B7
Parceiro me diz, onde é que é, (é lá pro lado be Bagé) tá todo mundo te procurando, qual é
   E                C#7     F#m                      B7                         E
Andei de carro, carroça e trem, perguntando onde é Xerém, pra te ver pra,  te abraçar
    E7           A        D#7             G#m       C#7          F#m              B7    E B7
Pra beber e papear, te contar como eu estou, mas baixinho me travou dizendo que ocê mudou
  E            C#7          F#m                   B7                       E  B7
Baixinho, onde é que o Zeca tá me diz, pagodinho rapá, tá difícil de encontrar

Ô  Zeca... (refrão)


    E                C#7     F#m                   B7                       E   B7
Ô  Jorge tu sabes que eu trabalho, às vezes me atrapalho e fico embaixo dos panos
   E                C#7   F#m                  B7                  E   B7
De tanto pagar por esse preço, perdi meu endereço e agora eu sou cigano

Refrão...... E  E7

A           D#7      G#m                C#7  F#m              B7      E E7
Trabalho, também trabalho já fui bicho do baralho, mas o samba me adotou
A             D#7   G#m                C#7      F#m            B7     E     B7
Por isso fui pra Xerém pois melhor que ocê não tem, versa que nem doutor, falou
  E      C#7          F#m                    B7                                   E B7
E aí, tu tá morando ondé (vô mora lá no chalé), como que eu chego lá de trem ou a pé

Refão

  E                   C#7    F#m                   B7                   E   B7
Estou morando em qualquer lugar, tô aqui, ali e acolá, levo minha vida assim
    E       C#7           F#m              B7                     E   B7
Pra lhe dizer com sinceridade, falando a verdade vou morar no botequim
(mas tu não bebe parceiro!)

Refrão
E        B7        E              C#7         F#m
Mas a dondé que tú mora que me convida e sai fora
   B7                    E B7
Ô  Zeca tu tá morando ondé

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
C#7 = X 4 3 4 2 X
D#7 = X 6 5 6 4 X
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
Gm = 3 5 5 3 3 3
