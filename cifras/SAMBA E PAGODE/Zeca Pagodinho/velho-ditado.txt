Zeca Pagodinho - Velho Ditado

Introdução:  D

     Bm                 Em7
Eu mais meu chinelo de dedo
         A7              D
Do que cromo alemão apertado
        Bm          Em7
Sou daqueles melhor só
        A7         D
Do que mal acompanhado
       Bm                   Em7
Vou rolando no mundo igual bola          Refrão
             A7                   D
Só porque a gente não nasceu quadrado
                Bm               Em7
Eu fico com um olho no peixe fritando
            A7               D
E fico com outro no gato do lado
                Bm               Em7
Eu fico com um olho no peixe fritando
             A7              D
E fico com outro no gato do lado

       Bm                 Em7
Olho grande não entra na china
               A7              D
Quem anda com porco só farelo come
              Bm                Em7
Mulher com mulher sempre dá jacaré
            A7                 D
E Homem com Homem vai dar Lobisomem
             Fº           Em7
Em terra de cego caolho é rei
           A7                D
Um dia da caça outro do caçador
               Bm                     Em7
Faça o que eu digo não faça o que eu faço
           A7                   D   A7
Aluno não sabe mais que o professor
       D         Bm       Am
Quem canta seus males espanta
                  D7                   G
Vou seguindo em frente poia atrás vem gente
            Gm6               D
Pimenta no olho do outro é refresco
          Em7         A7        D
E cavalo dado não se olha os dentes

Refrão

              Bm                Em7
Relógio que atrasa pra nada adianta
               A7                D
Quem semeia o vento colhe tempestade
               Bm                Em7
Deitou com cachorro com pulga levanta
               A7              D
Quem hoje é velinho já foi mocidade
           Fº               Em7
Laranja madura é que varada
              A7                   D
Não quer ser mamãe vai ficar pra titia
             Bm                  Em7
Trombada de porco nãao mata mosquito
              A7              D   A7
Pra mim cada louco com sua mania
     D     Bm       Am
Devagar se vai ao longe
                 D7               G
Sei com quantos paus se faz uma canoa
      Gm6              D
Eu também admiro Istambul
                 Em7        A7        D
Mas eu fico em Xerém por que é terra boa

Refrão

           Bm             Em7
A justiça tarda mas não falha
             A7                D
Não venha chorar o leite derramado
                Bm               Em7
Eu fico com um olho no peixe fritando]
            A7                D      ]Estribilho 2x
E fico com outro no gato do lado     ]
            Bm               Em7
Amigos , amigos negócios a parte
               A7                   D
Não ponha a colher onde não for chamado

Estribilho

               Bm                 Em7
Não perca o compasso acerte seu passo
            A7                D
Aprenda um pouco com velho ditado

Estribilho

           Bm             Em7
Queda de velho não sobe poeira
             A7             D
Jibóia não corre mas pega viado

Estribilho

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em7 = 0 2 2 0 3 0
Fº = X X 3 4 3 4
G = 3 2 0 0 0 3
Gm6 = 3 X 2 3 3 X
