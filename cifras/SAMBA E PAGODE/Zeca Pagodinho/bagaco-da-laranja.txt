Zeca Pagodinho - Bagaço da Laranja

REFRÃO

G           D
Fui no pagode
             G
Acabou a comida
             E
Acabou a bebida
            Am
Acabou a canja
            D
Sobrou pra mim
    D            G
O bagaço da laranja
            Am
Sobrou pra mim
     D           G
O bagaço da laranja (BIS)

E                   Am
Me disseram que no céu

             D       G
A mulher do anjo é anja
               E
Eu falei pra você
            Am
Sobrou pra mim
     D          G
O bagaço da laranja
            Am
Sobrou pra mim
     D          G
O bagaço da laranja

E                    Am
Vou engomar meu vestido
          D           G
Todo enfeitado de franja
               E
Eu falei pra você
            Am
Sobrou pra mim
     D          G
O bagaço da laranja
            Am
Sobrou pra mim
     D          G
O bagaço da laranja

REFRÃO

G          D
Fui no pagode
             G
Acabou a comida
             E
Acabou a bebida
            Am
Acabou a canja
            D
Sobrou pra mim
                G
O bagaço da laranja
           Am
Sobrou pra mim
     D           G
O bagaço da laranja

E                   Am
Eu te dou muito dinheiro
        D        G
E tudo você esbanja
                 E
Eu já disse à você
            Am
Sobrou pra mim
    D            G
O bagaço da laranja
            Am
Sobrou pra mim
     D          G
O bagaço da laranja

E                Am
Olha lá seu Coronel
                 D            G
O soldado que é peixe, se enganja
                   Am
E o que sobrô pra mim
     D          G
O bagaço da laranja
           Am
Sobrô pra mim
     D           G
O bagaço da laranja

E                 Am
Toma cuidado Pretinha
         D            G
Que a polícia já te manja
                E
Eu já disse à você
            Am
Sobrou pra mim
     D          G
O bagaço da laranja
           Am
Sobrou pra mim
    D            G
O bagaço da laranja

E                      Am
Não lhe dou mais um tostão
        D          G
Vê se você se arranja
               E
Eu falei prá você
            Am
Sobrou pra mim
    D           G
O bagaço da laranja
            Am
Sobrou pra mim
     D          G
O bagaço da laranja

E                 Am
Só caroço de azeitona
     D              G
Que veio na minha canja
                E
Eu já disse à você
           Am
Sobrou pra mim
    D            G
O bagaço da laranja
            Am
Sobrou pra mim
     D           G
O bagaço da laranja

E                    Am
Vou vender minha fazenda
       D              Am
Vou vender a minha granja
                E
Eu falei pra você
           Am
Sobrou pra mim
     D           G
O bagaço da laranja
           Am
Sobrou pra mim
    D            G
O bagaço da laranja

E                    Am
Você sempre foi solteira
      D            G
Um marido não arranja
                 E
Eu já disse à você
            Am
Sobrou pra mim
     D           G
O bagaço da laranja
           Am
Sobrou pra mim
     D          G
O bagaço da laranja

REFRÃO

G           D
Fui no pagode
             G
Acabou a comida
             E
Acabou a bebida
            Am
Acabou a canja
            D
Sobrou pra mim
                 G
O bagaço da laranja
           Am
Sobrou pra mim
     D          G
O bagaço da laranja (BIS)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
