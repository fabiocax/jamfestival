Zeca Pagodinho - Fiquei Amarrado na sua Blusinha

Introdução:  D A7 D A7 D A7 D


Bm     Em                A7                D
 Ôh pretinha, fiquei amarrado na sua blusinha
Bm     Em                A7                D
 Ôh pretinha, fiquei amarrado na sua blusinha
Bm     Em                A7                D
 Ôh pretinha, fiquei amarrado na sua blusinha
Bm     Em                A7                D
 Ôh pretinha, fiquei amarrado na sua blusinha
              A7                D
Mas essa sua blusa é uma coisa louca
         Bm                  Em
Tá todo mundo prestando atenção
                                 A7
E esses moranguinhos dão água na boca
                                   D
Será que esse bordado foi feito à mão
          A7                   D
Se você topar, proponho um casamento

                 D7              G
Dou-lhe um apartamento no Novo Leblon
                          D
Todo mobilhado com empregadinha
      Bm       Em       A7     D
Tudo isso por causa da sua blusinha, pretinha
Bm     Em                A7                D
 Ôh pretinha, fiquei amarrado na sua blusinha
Bm     Em                A7                D
 Ôh pretinha, fiquei amarrado na sua blusinha
Bm     Em                A7                D
 Ôh pretinha, fiquei amarrado na sua blusinha
Bm     Em                A7                D
 Ôh pretinha, fiquei amarrado na sua blusinha
          A7               D
Mas se você já tiver compromisso
            Bm                    Em
Não quero transgredir o nono mandamento
                                      A7
Ainda mais se o seu nego, for aquele nego
                                    D
Que quando tá de fogo encara um regimento
                 A7                D
E ele já tá me olhando esse tempo todo
                  D7             G
E já bebeu uma garrafa de cana purinha
                                D     Bm
Ou será que ele também está amarrado
      Em      A7     D
No bordado da sua blusinha, pretinha
Bm     Em                A7                D
 Ôh pretinha, fiquei amarrado na sua blusinha
Bm     Em                A7                D
 Ôh pretinha, fiquei amarrado na sua blusinha
Bm     Em                A7                D
 Ôh pretinha, fiquei amarrado na sua blusinha
Bm     Em                A7                D
 Ôh pretinha, fiquei amarrado na sua blusinha

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
