Zeca Pagodinho - Deixa Clarear

Introdução:  G Em Am D7 G Em Am D7

G       Em       Am           D      G    Em     Am
Vamos naquela tendinha que a dinha jurou que tem
    D       G    Em   Am    D      G     Em  Am
Caninha da boa tem     sardinha fritinha tem
  D            G      Em        Am
Prá gente que gosta e sempre se enrosca
    D       G     Em  Am    D       G   Em  Am
Naquela birosca tem    um rabo de saia tem
    D       G      Em  Am    D       G
Vitrola de ficha tem    prá gente dançar
 Em Am     D      G       Em Am       D      G
Ioiô   então vamos lá    Ioiô     então vamos lá
 D              G    D         G
Quem quer madrugar, farrear, zoar
              B7                     Em
Conhece um caminho, encontra um jeitinho, ô
 D               G             D    G
Tem sempre um lugar quem quer forrozar
       D      G
Pernoitar, virar

             Em          F#7      Bm
Não vê que a noitada virou madrugada, ô
Am      D    G
Deixa clarear
Em   Am      D          G            Em   Am      D         G
Ioiô      então vamos lá            Ioiô     então vamos lá
G     Em      Am            D        G     Em  Am
Vamos naquela barraca que logo na entrada tem
  D       G     Em Am     D        D     Em  Am
Aquela parada tem,     angú com rabada tem
 D             G       Em    Am       D       G    Em  Am
Prá dar repeteco, procurar o neco, naquele buteco tem
    D       G    Em  Am     D        G     Em  Am
Um bom carteado tem,    sinuca e porrinha tem
      D      G
Prá gente jogar
 Em  Am       D       G              Em  Am     D       G
Ioiô       então vamos lá           Ioiô     então vamos lá

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
F#7 = 2 4 2 3 2 2
G = 3 2 0 0 0 3
