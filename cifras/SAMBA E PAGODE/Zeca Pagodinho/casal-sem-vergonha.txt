Zeca Pagodinho - Casal sem Vergonha

Intro: Gm  C7  F  D7  Gm  C7  F

A minha vida
D7        Gm              C7
A minha vida é úm mar de rosa
             F
Em tua companhia
    D7                 Gm
Brigamos mil vezes ao dia
             C7
Mas depois as brigas
               F
Retornam harmonia
    A7            Dm
As vezes ela é dengosa
   E7                    Am D7
Às vezes é bicho de peçonha
       Gm
Sem vergonha
          C7           F      D7
Somos um casal sem-vergonha

       Gm
Sem-vergonha
           C7          F
Somos um casal sem-vergonha
         D7
A minha vida (1ª vez retorna)
       D7         Gm
Nós brigamos por ciúme
           C7
Costume, queixume
           F
Ou coisas banais
F7                   Cm
Não quero que ela fume
                   F7
Ela quer que o perfume
               Bb
Que eu use não cheire demais
                     Bbm
Brigamos quando sou bravo
                            Am
Brigamos até quando banco pamonha
               D7
Eu já disse porque meu bem
        Gm
Sem-vergonha
          C7           F     D7
Somos um casal sem-vergonha

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bbm = X 1 3 3 2 1
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
Gm = 3 5 5 3 3 3
