Benito Di Paula - Se Eu Pudesse

G7+     D/F#       Em       Bm7
HÁ SE EU PUDESSE ESTAR COM VOCE
  Em      C9      Am7/5-       D7/9
EU DARIA FESTA PRA TODO MUNDO VER

                             G7+                    D7/9
                            EU SERIA LIVRE COMO UM MENINO
                             G7+                    D7/9
                            EU SERIA EXEMPLO DO AMOR DIVINO
                           Em                    Am7/5-     D7
                            EU SERIA O SONHO DO SEU AMANHECER
                          D7/9    D7/9-                    G7+  D7/9
                            SE EU PUDESSE ESTAR AGORA COM VOCÊ

----------------- Acordes -----------------
Am7/5- = 5 X 5 5 4 X
Bm7 = X 2 4 2 3 2
C9 = X 3 5 5 3 3
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
D7/9 = X 5 4 5 5 X
D7/9- = X 5 4 5 4 X
Em = 0 2 2 0 0 0
G7+ = 3 X 4 4 3 X
