Benito Di Paula - Viva o Sol

   D            A7
É LÚZ QUE NOS RESTA
  D         A7
É DIA DE GRAÇA
   Em           G
UM REI QUE NÃO FALA
   Em           G
TAMBÉM NÃO RECLAMA
   D              Em
É O BRILHO É O CAMINHO
         G/A     A7
QUE A VIDA NOS CHAMA

  D          A7
PRO NOSSO PASSADO
  D            A7
AS FÉRIAS DA ESCOLA
   Em       G
PRO MUNDO FUTURO
  Em        G
É NOSSA ENERGIA

   D       Em        G/A    A7
DE VIVA AO SOL TODOS OS DIA

      D          D7
TODA MANHÃ PELA MANHÃ
        G              Em
ABRA A JANELA FAÇA SUA LEI
     G/A                 A7
DE VIVA AO SOL (DE VIVA AO SOL)
   D                        A7
ELE É NOSSO REI (ELE É NOSSO REI)

Cifra de Marcito Alves

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G/A = 5 X 5 4 3 X
