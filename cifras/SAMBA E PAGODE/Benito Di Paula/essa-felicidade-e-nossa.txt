Benito Di Paula - Essa Felicidade É Nossa

[Intro]  CM7  Dm7  Em7  G

   C
Você é uma flor
          E7       Am
É uma estrela pra mim
                 C7      F
Você sabe que eu amo demais
                 A        Dm
E que não sei viver sem você
     G                   G/F
Me guarde bem dentro do peito
    Cm                Cm/Bb     Ab
Querida, pra gente mostrar toda vida
                  D           G
Nosso amor é bem mais que paixão

   C                    E7
E toda vez que eu te abraço
          Am                       C7
É um reencontro e no encontro do amor

      F
Eu revelo
              A        Dm
Esse meu coração pra você
   G               G/F          Cm
O nosso amor nunca perde, o compasso
                 Cm/Bb            Ab
É um samba, é um verso, em meus braços
       D             G  G7
Somos demais, pra viver

     C                             E7
Pra sermos felizes não falta mais nada
                        F
Pois Deus ilumina a avenida
                       G
E a vida pra gente passar

   Fm                             Cm
O nosso desfile é um presente de flores
                     Fm
O céu estrelado de amores
              G           Cm
E a vida pra nós, a desfilar

Cm       Ab            Bb         Eb
Graças a Deus, essa felicidade é nossa!
         Fm            G          Cm
Graças a Deus, essa felicidade é nossa!
Cm       Ab            Bb         Eb
Graças a Deus, essa felicidade é nossa!
         Fm            G          Cm
Graças a Deus, essa felicidade é nossa!

( CM7  Dm7  Em7  G  CM7 )

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
Cm/Bb = X 1 1 0 1 X
D = X X 0 2 3 2
D# = X 6 5 3 4 3
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
G/F = 1 X X 0 0 3
G7 = 3 5 3 4 3 3
