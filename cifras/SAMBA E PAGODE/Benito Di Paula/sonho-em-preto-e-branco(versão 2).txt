Benito Di Paula - Sonho Em Preto e Branco

Am                  Dm           E7                  Am
       Veja meu bem teu coração quis me deixar assim
                   Dm              G7              C
Não sei por que, mas eu sofri quase morri
                                  E7                      Am
Não sei por que teu coração tomou tal decisão
                 F                  Dm              E7
Sem compaixão meu coração quase parou
                 Dm                E7               Am
O que era meu o que era teu tinha um tom de azul
             Dm                 G7            C
Que desbotou perdeu a cor e tudo se perdeu
         E7                                              Am
Amanheceu meu coração banhado em pranto
                 F                      E7                      A      E7
Sonhei chorando com teu corpo em preto e branco

                A                     E7             F#m
Mas vai passar um dia eu sei que vai mudar
                   D                F#            Bm
É sempre assim a gente tem que suportar

                    C#7                                F#m   G#
Se a corda quebra sempre sofre o lado fraco
            C#                    G#           C#      E7
Meu coração vai ter que ter que suportar
              A             E7                     F#m
Um passarinho vai dizer para o teu coração
                  D                F#     Bm
Que se o destino mudar tal situação
              C#                                           F#m
Não vou vingar eu vou te amar do mesmo jeito
           Bm           E7                      A
Pode voltar pode morar no mesmo peito
Dm                 Am
      Veja meu bem

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C# = X 4 6 6 6 4
C#7 = X 4 3 4 2 X
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
G# = 4 3 1 1 1 4
G7 = 3 5 3 4 3 3
