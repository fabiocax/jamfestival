Benito Di Paula - Pagode da Pesada

G       E7            Am                 D7                  G
Guaracy me convidou pra um pagode em sua casa
              E7              Am            D7           G
Vai ter muita gente boa no pagode do arrasa
     E7                   Am          D7            G
Frederico na cozinha pro rango sair legal
            E7              Am                D7         G
Sílvio Caldas, convidado muito muito especial
          E7                         Am              D7              G
O Martinho e o João Nogueira vão chegar de madrugada
          E7                        Am              D7         G
Clara Nunes vem de branco clareando e clareada
             E7                    Am              D7           G
Vai ter som de cavaquinho samba de Beth Carvalho
            E7                  Am    D7                     G
Juca Chaves traz Iara prometeu que vai sambar
          E7                  Am            D7                G
Sargentelli é o convidado de honra do Guaracy
        E7                    Am            D7               G
Vai trazer suas mulatas pra moçada se ouriçar
       E7                      Am
Vou dizer pro Sargentelli

                       D7                            G
Deixa essa mulata chegar mais pra cá

       E7            Am
Quando o samba esquentar
   D7                            G
Deixa todo mundo que quiser sambar
       E7             Am
Quando o samba esquentar
   D7                            G
Deixa todo mundo que quiser sambar

----------------- Acordes -----------------
Am = X 0 2 2 1 0
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
G = 3 2 0 0 0 3
