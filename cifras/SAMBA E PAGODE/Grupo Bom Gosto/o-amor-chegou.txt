Grupo Bom Gosto - O Amor Chegou

Intro: G7M  Am7


Solo: D7/9


 G7M
Quando,
            D/F#
olho nos olhos seus
            G7M
sinto meu chão sumir
       D/F#
fico imaginando
         C
fico te namorando
      G7M     G6
apaixonado em ti
           D/F#
desde a primeira vez
           C
desde a primeira vez

G7M
Sonho
               D/F#
a vida ao teu lado é um sonho
          G7M
adoro acordar risonho
          D/F#
você coloriu meu mundo
             C
você é um amor profundo
            G7M     G6
desejo sagrado em mim
             D/F#
desde a primeira vez
             C
desde a primeira vez
        G
que te vi
              D/F#
sinto que o amor chegou


Refrão:
     C7M              D7         G   Dm7  G7/11
eu sei que o amor em mim...
     C7M            D7/9
Eu sei que o amor chegou
     D#º             Em7
eu sei que o amor chegou
     C7M                  D7/9       G   Dm7  G7/11
eu sei que o amor chegou em mim
     C7M             D7/9
eu sei que o amor chegou
     D#º              Em7
eu sei que o amor chegou
     C7M              D7/9
eu sei que o amor chegou em mim


( G  Am7 )


( C D7 G )


----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
C7M = X 3 2 0 0 X
D#º = X X 1 2 1 2
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
D7/9 = X 5 4 5 5 X
Dm7 = X 5 7 5 6 5
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
G6 = 3 X 2 4 3 X
G7/11 = 3 5 3 5 3 X
G7M = 3 X 4 4 3 X
