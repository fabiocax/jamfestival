Grupo Bom Gosto - Porta Voz da Alegria

Solo: 32-14-12-10-22  32-10-22-20-32  42-20-32-31 42-20  42-20  32-31

Introdução: A7+ A6  A7+ A6 Bm7  Bm7/5- E7

A7+                       A6
Gosto de levar a vida a cantar
A7+                       A6
Ver os amigos é sempre um prazer
D7+                             Bm7/5-                   E7
Tudo que eu tinha pra realizar, hoje só tenho que agradecer
A7+                       A6     A7+                       A6
Uma família repleta de amor e a mulher que eu sempre sonhei
D7+                           Bm7/5-                E7
Só dá forças pra ser o que sou: um guerreiro do samba
C#m7/5-               F#7/4  F#7                 G#m7(9-)
Eu sou partideiro, porta voz da alegria da rapaziada
Bm7      E7        C#m7/5-       F#7/4              F#7
Carioca da Gema e bom brasileiro, e a vida que eu levo
                Bm7
não troco por nada
    Bm7/5-        C#m7     Em7        D7+ A7/9
Eu vou na paz do Senhor, na luz do meu caminhar


D7+                                Bm7/5-            E7
Vou na certeza da fé, confiando no instinto do meu coração
C#m7                              Em7               D7+ A7/9
Vou na razão de cumprir o destino marcado na palma da mão
D7+                  C#m7  Bm7               A7/9 (1ªx)  E7 (2ªx)
Eu vou onde Deus me levar, essa é a minha missão

A7+    A6          A7+    A6    D7+
Lá lálaiá laiá...  Lá lálaiá laiá...
        Bm7/5-          E7
Lá lálaiá laia..... Lá lálaiá lá

----------------- Acordes -----------------
A6 = 5 X 4 6 5 X
A7+ = X 0 2 1 2 0
A7/9 = 5 X 5 4 2 X
Bm7 = X 2 4 2 3 2
Bm7/5- = X 2 3 2 3 X
C#m7 = X 4 6 4 5 4
C#m7/5- = X 4 5 4 5 X
D7+ = X X 0 2 2 2
E7 = 0 2 2 1 3 0
Em7 = 0 2 2 0 3 0
F#7 = 2 4 2 3 2 2
F#7/4 = 2 4 2 4 2 X
G#m7(9-) = 4 2 4 2 4 2
