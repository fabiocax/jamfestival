Grupo Bom Gosto - Brigar Não Tá Com Nada

A
Ô lele, lele ô, lele
B7
Lele ô, lele, le ô, lele
D
Lele ô, lele, lele ô, lele
A                      E7
Brigar não tá com nada (2x)

A
Quando a gente se aproxima
                     B7
Pinta um clima de paixão
                     D
não é em qualquer esquina
E7                      A
Que eu dou meu coração

A
Mexe com a adrenalina
                 B7
Não consigo disfarçar

                 D
Nesse amor você ensina
E7                  A
Com seu jeito de amar

E
Você não assume

Mas trás o ciúme estampado no olhar
D
já é de custume

Faço vista grossa pra gente ficar
A
Brigar não tá com nada
B7
Te quero minha amada
D
E vamos nessa estrada

A
Jogo do prazer, deixa eu te ganhar

Pode se perder, que eu vou te encontrar
B7
Eu vou me render, quero confessar

Colado em você, não quero desgrudar
D
Pode anoitecer, pode clarear
                            A
Fico com você custe o que custar

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
