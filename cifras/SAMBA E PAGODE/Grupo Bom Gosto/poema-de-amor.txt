Grupo Bom Gosto - Poema de Amor


(intro) G#m7    F#m7 B7 E79+ D#m7  A#m7/13

 G#m7       C#79       G#m7            C#79
Quando o dia amanheceu, meu amor, você e eu
  Gb7+              C#          A#m7/13    D#7
Depois de tanto se entregar, se entregar...
G#m7          C#79   G#m7           C#79
Noite intensa de prazer, trocando jura de amor
Gb7+       C#         A#m7     D#7
Coração acelerou, o tempo até parou
G#m7                 G#m7+                G#m7
O nosso amor é tão bonito que nos leva ao infinito
       C#79
Um céu de estrelas
Gb9             C#      A#m7/13        D#7
Ascende a chama da paixão, meu peito inflama
G#m7                  G#m7+
E quando provo do teu beijo
              G#m7              C#79
Um oceano de desejo é a felicidade

Gb7+       Você é tudo que almejo, que mais quero pra mim
G#m79                F#m79
Não consigo imaginar  ( reviver o nosso amor)
          B9
Minha vida sem você     ( num poema que cantei...)
E79+            D#m7
Nada vai nos separar     ( te amar é bom demais)
A#m7/13          D#7
Nem pensar em te perder    tudo que eu sempre quis...)
G#m79                   F#m7
Nos meus sonhos de amor
            B9
Tudo que eu sempre quis
E79+          D#m7
É você, minha paixão
A#m7/13          D#7
Deixa eu te fazer feliz.

----------------- Acordes -----------------
A#m7 = X 1 3 1 2 1
A#m7/13 = X 1 X 1 2 3
B7 = X 2 1 2 0 2
B9 = X 2 4 4 2 2
C# = X 4 6 6 6 4
C#79 = X 4 3 4 4 X
D#7 = X 6 5 6 4 X
D#m7 = X X 1 3 2 2
E79+ = X 6 5 6 7 X
F#m7 = 2 X 2 2 2 X
F#m79 = X X 4 2 5 4
G#m7 = 4 X 4 4 4 X
G#m7+ = X X 6 8 8 7
G#m79 = X X 6 4 7 6
Gb7+ = 2 X 3 3 2 X
Gb9 = 2 4 6 3 2 2
