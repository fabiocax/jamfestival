Grupo Bom Gosto - Castigo

  C7        F           E7     Am
    Estou sofrendo sem ela, sofrendo sem ela
    C7        F           E7      Am G#m Gm
    Estou sofrendo sem ela, estou
    C7        F           E7     Am
    Estou sofrendo sem ela, sofrendo sem ela
    C7        F           E7     Am   G7
    Estou sofrendo sem ela, estou
C
Ela me disse que me deixa de castigo
              F7+
Se eu der mancada
C
Que está cansada de falar a mesma coisa
            F7+
E eu não ligar
C
E que não vai fazer amor comigo
                      F7+
Se eu chegar de madrugada
C
Que pega as minhas coisas e joga na rua

                  F7+
Se eu não me acertar
      Bm7/5-
Então eu cheguei tarde
              E7                 Am
E tive que pular o portão lá de casa
          Bm7/5-                     E7
E ainda encontro o lençol e o travesseiro
             Am
Em cima do sofá
           Bm7/5-              E7            Am
Pra piorar o meu estômago anuncia uma larica braba
           Dm7        E7                       Am G#dim Gm
E lá vou eu pilotar o fogão pra preparar o meu jantar

    C7        F           G7     C
    Estou sofrendo sem ela, sofrendo sem ela
    C7        F           G7      Am G#m Gm
    Estou sofrendo sem ela, estou
    C7        F           G7     C
    Estou sofrendo sem ela, sofrendo sem ela
    C7        F           G7
    Estou sofrendo sem ela, estou

C
Ela me disse que me deixa de castigo
              F7+
Se eu der mancada
C
Que está cansada de falar a mesma coisa
            F7+
E eu não ligar
C
E que não vai fazer amor comigo
                      F7+
Se eu chegar de madrugada
C
Que pega as minhas coisas e joga na rua
                  F7+
Se eu não me acertar
          Bm7/5-
E como se já não bastasse
    E7                Am
Ela dispensou a empregada
        Bm7/5-                      E7
E disse que eu tenho que limpar, lavar, passar
              Am
E também cozinhar
        Bm7/5-              E7
E se eu não fizer o que ela manda
                      Am
Ela é quem vai dar mancada
        Bm7/5-               E7               Am G#m Gm
E jura que me abandona antes desse castigo acabar

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm7/5- = X 2 3 2 3 X
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
F7+ = 1 X 2 2 1 X
G#dim = 4 X 3 4 3 X
G#m = 4 6 6 4 4 4
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
