Grupo Bom Gosto - Um Só Sentimento

Intro 3x: E7M/9  B7/4/9

B7/4/9
       Se existisse outro alguém

Bem mais bonita que você
                   E7M/9
Eu juro, não te trocaria

B7/4/9
       Meu peito não tem mais lugar

Meus olhos não conseguem ver
                        E7M/9
Um outro amor pra minha vida

C#m/A          A6               B7/4/9
       E.. Por onde quer que eu vá
                     G#m7/4
Meu coração tá com você
                G#º
Eu tô lutando pra provar

     C#7              C#m/A
Que nada pode nos vencer
  A6                  B7/4/9
E deixa quem quiser falar
                          E7M/9
Que o nosso amor não vai morrer

 E7M/9                     E/D
         E por nada nesse mundo
                   C#m/A
Arriscava nossa história
A6                   Am7
Toda vez que eu vou embora
          B7/4/9      E7M/9
Conto as horas pra te ver

  E7M/9                   E/D
         Somos um só sentimento
                       C#m/A
Que vai transcender o tempo
A6               Am7
E na paz desse momento
        B7/4/9    E7M/9
Vou dizer: amo você.

----------------- Acordes -----------------
A6 = 5 X 4 6 5 X
Am7 = X 0 2 0 1 0
B7/4/9 = X 2 2 2 2 2
C#7 = X 4 3 4 2 X
C#m/A = X 0 2 1 2 0
E/D = X 5 X 4 5 4
E7M/9 = X 7 6 8 7 X
G#m7/4 = 4 X 4 4 2 X
G#º = 4 X 3 4 3 X
