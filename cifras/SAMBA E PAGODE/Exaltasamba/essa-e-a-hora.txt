Exaltasamba - Essa É a Hora

F          Bb C                             C7
Já foi o dia... preciso me arrumar
F          Bb C                             C7
Meia noite...boa viola a tocar
F          Bb C                             C7
Já é noite ... Vou me encontrar com iáiá
F          Bb C                             C7
Despedida...ela se põe a chamar

F         Dm            Gm               C7
       Mas eu vou pro samba rodar
F                                           Dm
Vou pra roda de samba cantar
   Gm
Que já deu minha hora
           C7                                         F
Preciso ir embora e não posso ficar

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
