Exaltasamba - Uma Carta Pra Deus

Am7/9
O fim do sofrimento
G
De tantos tormentos
F
Dores e lamentos
Bm7/5-                 E7
Fim da luta pela paz.

Am7/9
O fim de tantas guerras
G
De sangue na terra
F
Nossa atmosfera
Bm7/5-            E7
Vai parar de respirar.

   Am7/9
Falta de carinho
Am7/G
Com a natureza

F7+/A             Dm
O nosso planeta
      F      Bm7/5-   E7/9-
Pode até não aguentar.

Am7/9
Vida após a vida
Am7/G
Não há despedida
F7+/A
Eu tou esperando
  Dm                  G
Pois eu sei que vai voltar.

C9                         G
Dói de ver e nada poder fazer
       F                G7/4
Só tem uma solução a sua volta
G7             C9
Volta logo por favor Senhor
                G
Eu vejo a extinção do amor
    F
E vejo tortura e dor
       Bm7/5-        E7/9-
À minha volta, volta. (2x)

----------------- Acordes -----------------
Am7/9 = X X 7 5 8 7
Am7/G = 3 0 2 0 1 0
Bm7/5- = X 2 3 2 3 X
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
E7/9- = X X 2 1 3 1
F = 1 3 3 2 1 1
F7+/A = 5 X 3 5 5 X
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
G7/4 = 3 5 3 5 3 X
