Exaltasamba - Toque de Carinho

C
Introdução:  F7+ Fm6 Em7 Eb Dm7 G7 Gm7 C7 F7+ Fm6 Em7 Eb Dm7 G7 C6/9 G7/5+

  C6/9        G7/5+      C6/9    Gm7
Levitar nas asas da primavera
          F#7/11+      F7+
Sentir um toque de carinho
Fm6                     Em7    Eb
    O gosto bom da tua boca
          Dm7  G7/5+           C7+    F7/9
Que coisa louca,      me dá prazer
          Bb7        C6/9    Gm7
A mais sincera melodia
           F#7/11+      F7+
Eu canto em versos pra você
Fm6              Em7    Eb             Dm    G7
   Fonte de inspiração,     da minha canção
         C7+   Gm7   C7
És o meu viver
  F7+           Fm6
Deixa, amor me deixa

         C7+   Bb7             A7
Eu quero ser        seu bem querer
F#7/11+    F7+               Fm6   G7/5+
Então deixa,      amor me deixa
     C7+  Gm7   F#7/11+
Amar você

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bb7 = X 1 3 1 3 1
C = X 3 2 0 1 0
C6/9 = X 3 2 2 3 3
C7 = X 3 2 3 1 X
C7+ = X 3 2 0 0 X
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Eb = X 6 5 3 4 3
Em7 = 0 2 2 0 3 0
F#7/11+ = 2 X 2 3 1 X
F7+ = 1 X 2 2 1 X
F7/9 = X X 3 2 4 3
Fm6 = 1 X 0 1 1 X
G7 = 3 5 3 4 3 3
G7/5+ = 3 X 3 4 4 3
Gm7 = 3 X 3 3 3 X
