Exaltasamba - Continuo Pronto

Intro]  Eb7M(9)  Bb7M

Eb7M(9)
Eu vou te levar pra qualquer lugar
Bb7M
Sem atalho, sem destino
Eb7M(9)
Longe da multidão ouvir o coração
Bb7M
Um tempo nosso, nada mais

Ab7M                          Bb9
O caminho mais longo é melhor, vamos bem devagar
          Gm7           Ab7M
Olha esse céu, ouça esse som
          Cm7                   Bb9
Sinta esse vento e respira esse ar
        Aº
Nossa rotina não vai negar
       Ab7M             Bb9
Toda magia de se encontrar

     Cm7        Bb9         Ab7M   (Gm7)
De se ver, de se ter, de se amar

  Ab7M                   Bb9
E lembra de quando tudo era assim
                Eb7M(9)           Bb/D
A gente rindo à beça, não existia pressa
         Eb7/C#
Uma vida sem hora marcada
   Ab7M                          Bb9
Eu lembro sim de quando se apaixonou por mim
             Eb7M(9)              Bb/D         B7M(9) C#7M(9)
Havia tantos sonhos e eu continuo pronto pra realizar

Eb7M(9)
Longe da multidão ouvir o coração
Bb7M
Um tempo nosso, nada mais

Ab7M                          Bb9
O caminho mais longo é melhor, vamos bem devagar
          Gm7           Ab7M
Olha esse céu, ouça esse som
          Cm7                   Bb9
Sinta esse vento e respira esse ar
        Aº
Nossa rotina não vai negar
       Ab7M             Bb9
Toda magia de se encontrar
     Cm7        Bb9         Ab7M   (Gm7)
De se ver, de se ter, de se amar

  Ab7M                   Bb9
E lembra de quando tudo era assim
                Eb7M(9)           Bb/D
A gente rindo à beça, não existia pressa
         Eb7/C#
Uma vida sem hora marcada
   Ab7M                          Bb9
Eu lembro sim de quando se apaixonou por mim
             Eb7M(9)              Bb/D         B7M(9) C#7M(9)
Havia tantos sonhos e eu continuo pronto pra realizar

  Ab7M                   Bb9
E lembra de quando tudo era assim
                Eb7M(9)           Bb/D
A gente rindo à beça, não existia pressa
         Eb7/C#
Uma vida sem hora marcada
   Ab7M                          Bb9
Eu lembro sim de quando se apaixonou por mim
             Eb7M(9)              Bb/D         B7M(9) C#7M(9) Eb7M(9)
Havia tantos sonhos e eu continuo pronto pra realizar

Eb7M(9)
Eu vou te levar

----------------- Acordes -----------------
Ab7M = 4 X 5 5 4 X
B7M(9) = X 2 1 3 2 X
Bb/D = X 5 X 3 6 6
Bb7M = X 1 3 2 3 1
Bb9 = X 1 3 3 1 1
C#7M(9) = X 4 3 5 4 X
Cm7 = X 3 5 3 4 3
Eb7/C# = X 4 5 3 4 X
Eb7M(9) = X 6 5 7 6 X
Gm7 = 3 X 3 3 3 X
