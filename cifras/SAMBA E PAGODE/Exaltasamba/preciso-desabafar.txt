Exaltasamba - Preciso Desabafar

A7+           C#m7
Preciso tanto conversar
D7+               C#m7      D7+ C#m7 Bm7 E7
Falar de tudo que aconteceu comigo
A7+       C#m7
Diz então pode falar
D7+                  C#m7      D7+      C#m7
O que aconteceu que te deixou tão deprimido
Bm7               E7
É sempre bom desabafar
A7+  C#m7
Lembra
    D7+                 E7            A7+
Daquela mina que um dia eu te apresentei
         C#m7
(Lembro sim) O que é que tem?
   D7+                        E7           A7+
Ficamos juntos um bom tempo e eu me apaixonei
     C#m7
Tudo bem
   G#m7/5-               ( 4ºcorda-> G#m7/5- ) C#7/9- F#m7
É tão normal um homem se apaixonar

           C#m7
Ela foi embora
     Bm7            E7  Fº    F#m7
Sem mais nem menos decretou o fim
              C#m7
Mas dizem que ela chora
     Bm7       E7    Fº      F#m7
Se algum amigo perguntar por mim
          C#m7
Não fica assim, não chora
   Bm7                E7  Fº  F#m
Aposto que ela um dia vai voltar
                 C#m7
Meu Deus mas que demora
     Bm7             E7
Mais um pouquinho eu vou pirar

A7+        A7/9/11^A7/9 D7+        E7/9/11
Eu já nem sei quem eu   sou ou me chamo
A7+       A7/9/11^A7/9 D7+         E7/9/11
Ai que saudade    daaaquela que eu amo
A7+          A7/9/11^A7/9 D7+      E7/9/11
Bem que eu notei   você  meio estranho
A7+       A7/9/11^A7/9 D7+       E7/9/11
Tão cabisbaixo,   sooozinho num canto

FINAL
A7+       A7/9/11^A7/9  D7+       E    F#7+
Tão cabisbaixo,   soooozinho num canto uuu

----------------- Acordes -----------------
A7+ = X 0 2 1 2 0
A7/9 = 5 X 5 4 2 X
A7/9/11 = 5 X 5 4 3 X
Bm7 = X 2 4 2 3 2
C#7/9- = X 4 3 4 3 X
C#m7 = X 4 6 4 5 4
D7+ = X X 0 2 2 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
E7/9/11 = X X 2 2 3 2
F#7+ = 2 X 3 3 2 X
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
Fº = X X 3 4 3 4
G#m7/5- = 4 X 4 4 3 X
