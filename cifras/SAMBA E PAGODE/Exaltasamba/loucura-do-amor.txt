Exaltasamba - Loucura do Amor

Introdução: D7+ / % / D7+ / % / A7/9  ^ A7/9- / D7+ / % / D7+ / % / A7 /

   D7+        D5+         Bm7
Foi tanto amor num só beijo,
          F#m7                  D7+
Que a gente se quer se lembrou de deitar,
      %          Bm7
Foi tanto gozo e desejo,
             F#m            F#m7   1º(C7/9) 2º(C7/9^F#7/13)
Foi assim nosso jeito maluco de amar,
 F#m          F#m7           F#m7/5-       B7/9-
E então, num prazer bem gostoso a gente se deu,
     Em                A7                  D7+
O meu mundo de amor foi você, e seu mundo foi eu,
Am7 ^ D7   G7+   G6
Nada foi ruim,......
    Em7/5-                A7                     D7+
Quando a gente se deu tudo foi só paixão do começo ao fim,
Am7 ^ D7    Não existiu dor,......
   Bm            G#º            Am7
Só o azul do céu se abrindo pra vim colorir,

  D7/13     %
Esse amor, louco,
REFRÃO.
 G7+         G6          F#m7
Louco,... de fazer enlouquecer,
     B7           Em          A7            D7+
Foi gostoso te encontrar, me perder nesse prazer,
         %
Tão louco,... louco,
Am ^ D7   G7+     G6        F#m7
Tão louco,... de fazer enlouquecer,
    B7            Em           A7           D7+
Foi gostoso te encontrar, me perder nesse prazer,

Volta a introdução e executa-se tudo novamente.

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7/9 = 5 X 5 4 2 X
A7/9- = 5 X 5 3 2 X
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
B7/9- = X 2 1 2 1 X
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
C7/9 = X 3 2 3 3 X
D5+ = X 5 4 3 3 X
D7 = X X 0 2 1 2
D7+ = X X 0 2 2 2
D7/13 = X 5 X 5 7 7
Em = 0 2 2 0 0 0
Em7/5- = X X 2 3 3 3
F#7/13 = 2 X 2 3 4 X
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
F#m7/5- = 2 X 2 2 1 X
G#º = 4 X 3 4 3 X
G6 = 3 X 2 4 3 X
G7+ = 3 X 4 4 3 X
