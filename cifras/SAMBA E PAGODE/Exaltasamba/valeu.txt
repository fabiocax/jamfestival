Exaltasamba - Valeu

Intro:
(cavaquinho)
D|-----------------------|--------------------------|
B|-----------------------|-------------5--3---------|
G|-5---------5-5-5-5-----|------5------------5-4-5--|
D|---5--5-5-----------5--|--5-5---5-5---------------|

E|-----------------------|--------------------------|
B|-----------------------|-------------5--3---------|
G|-5---------5-5-5-5-----|------5------------5-4-5--|
D|---5--5-5-----------5--|--5-5---5-5---------------|
A|-----------------------|--------------------------|
E|-----------------------|--------------------------|

          G7/4
       Ficou mais difícil de dizer que não me quer

      Quando eu te peguei olhando pra mim
C7+/9                         Am7/9
      Com cara de desejo         cara de quem quer beijo
G7/4
      Minha mente viajando só pensando em nós


      Imagine quando estivermos a sós
C7+/9                          Am7/9
      Entre quatro paredes       sacia a minha sede
      Dm7
      Não provoca não
                    Em7
      Não me chame a atenção
                   Fm7                   F/G
      Eu não tenho a intenção de te magoar
      Dm7
      Não, não brinque assim
                Em7
      Ao chegar perto de mim
              Fm7                                         F/G
     Sabe qual vai ser o fim se eu começo a te beijar

  (C7+/9)                                        F#7/5-
     Valeu essa noite eu vou ser seu
             F7+                      F/G
     Aproveita que eu quero me entregar
   C7+/9                      Bb
     Valeu só não vou te prometer
                   F7+                F/G
     Que vai me ver quando o dia chegar

----------------- Acordes -----------------
Am7/9 = X X 7 5 8 7
Bb = X 1 3 3 3 1
C7+/9 = X 3 2 4 3 X
Dm7 = X 5 7 5 6 5
Em7 = 0 2 2 0 3 0
F#7/5- = 2 X 2 3 1 X
F/G = 3 X 3 2 1 X
F7+ = 1 X 2 2 1 X
Fm7 = 1 X 1 1 1 X
G7/4 = 3 5 3 5 3 X
