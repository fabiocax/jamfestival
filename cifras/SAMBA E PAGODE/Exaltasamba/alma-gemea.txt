Exaltasamba - Alma Gêmea

(intro) F7+ G7 Em D#° D7/9 D7/9 G7

C6/9
Eu percebi no teu olhar
Dm
Um anjo bom de me guiar
Gm                   C7/9                   F7+  E7
Do céu que não vai desabar no mel do meu prazer, há...
Fm        Bb7        Em               D#°      D7/9
Um nó que não vai desatar, a luz que veio pra ficar no coração
D7/9-               G7
Não sabe o que é sofrer, há...
C6/6                     Dm
Você também já percebeu que o seu grande amor sou eu
Gm                 C7/9           F7+          E7
E a solidão já se perdeu, vou te fazer feliz, vem.
Am            D7         G7+           G6
Se o destino quis assim, bom pra você, melhor pra mim
F7+           G7      Gm     C7/9
È impossível duvidar ainda, linda.
F#m5-/7         Fm     Em               D#°
Pede pra Deus abençoar e o nosso amor eternizar

Dm             G7       C
Abrir meu coração bem vinda

C          G/B       G/Bb         REFRÃO
Vem meu pedacinho de maçã
      A7           Dm
Me perfumar toda manhã
          G7               C         G7
Com esse cheirinho bom de fêmea, de fêmea
C             G/B     G/Bb
Vem preciso tanto de você
A7                       Dm
Eu quero ser teu bem querer
        G7        C
Você é minha alma gêmea.   (Duas vezes e depois volta à introdução)

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bb7 = X 1 3 1 3 1
C = X 3 2 0 1 0
C6/9 = X 3 2 2 3 3
C7/9 = X 3 2 3 3 X
D#° = X X 1 2 1 2
D7 = X X 0 2 1 2
D7/9 = X 5 4 5 5 X
D7/9- = X 5 4 5 4 X
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F#m5-/7 = 2 X 2 2 1 X
F7+ = 1 X 2 2 1 X
Fm = 1 3 3 1 1 1
G/B = X 2 0 0 3 3
G/Bb = X 1 0 0 3 3
G6 = 3 X 2 4 3 X
G7 = 3 5 3 4 3 3
G7+ = 3 X 4 4 3 X
Gm = 3 5 5 3 3 3
