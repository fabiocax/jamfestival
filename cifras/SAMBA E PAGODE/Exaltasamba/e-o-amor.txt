Exaltasamba - É o Amor

Introdução: A7+ E/G#

 A7+
Eu Chorei,
      D7+                     E/G#
todo mundo ta lutando, minha vida transformando...
A7+
Eu Chorei,
       D7+                       G7+
o meu pranto ta inundando, toda água do oceano...
 C7+          Bm7
Eu Amei, Só a Lua companheira pro meu pranto decifrar...
C7+     Bm7                             E/G#
Eu Amei, Coração bate em silencio pede a Deus pra melhorar...
Am7        Bm7           E/G#
Tanto amor, Tanto Amor.
2ª PARTE
A7+           D7+                          E/G#
Deixe Estar...o tempo é um mestre pro teu choro enxugar
A7+           D7+                                   G7+
Deixe estar, deva  C7+            Bm7
Já amei como você ta amando, todo mundo amou um dia.

 C7+              Bm7                 E/G#
Já chorei como você está chorando, já vivi essa agonia.
Am7            Bm7        E/G#       Dm7
É o amor , É o amor....
ESTRIBILHO
Am7                            Dm7
Eu vivo contando as estrelas do céu,
            G7+                    C7+
Tentando esquecer esse amor me fez réu
   F7+           Bm7                    E/G#
Eu to tão perdido eu ando sozinho, Procuro um carinho pra me completar.
Am7                            Dm7
O amor quando chega não dá pra evitar.
            G7+               C7+
Você Ta na faze aprendendo a amar
             F7+             Bm7
Só tenha um pouco mais de paciência
             E/G#
o amor nem ciência consegue explicar...

Refrão

A7+                            D7+
É o amor... Me desculpe se eu chorar...
        G7+                      C7+
É o amor... Não se preocupa vai passar...
          F7+               Bm7
É o amor... Eu sou muito menino...
                   E/G#
Mais calma que a pressa é amiga do azar.     F
A7+   D7+
É o amor         G7+          C7+
É o amor... É tão normal confie em mim...
          F7+               Bm7
É o amor... Mais to tão preocupado
             E/G#              A7+
Eu to do seu lado não é tão ruim

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7+ = X 0 2 1 2 0
Am7 = X 0 2 0 1 0
Bm7 = X 2 4 2 3 2
C7+ = X 3 2 0 0 X
D7+ = X X 0 2 2 2
Dm7 = X 5 7 5 6 5
E/G# = 4 X 2 4 5 X
F = 1 3 3 2 1 1
F7+ = 1 X 2 2 1 X
G7+ = 3 X 4 4 3 X
