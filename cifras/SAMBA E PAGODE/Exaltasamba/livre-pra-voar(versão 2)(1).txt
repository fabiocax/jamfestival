Exaltasamba - Livre Pra Voar

Intro: C7+/9  C7/9  F7+  F/G

C7+/9                       C7/9
Le le, le le le le le le le le
F7+                       F/G
Le le le le le le le le le

C7+/9
Le  le  le  le  le
C7/9                      F7+
Le le, le le le le le le le le
     F/G
Le le le le le le le le le


C7+/9              C7/9
Quando a gente se encontrar
               F7+
Tudo vai ser tão perfeito
               F/G
Eu quero te curtir demais

C7+/9                 C7/9
Eu vou aliviar esse aperto no meu peito
F7+                 F/G
Que vontade não da mais
         C7+/9
Se no telefone é bom
         F7+
Imagine aqui bem perto
                    F/G
Eu sentindo o teu calor
           C7+/9                  F7+
Sem medo de ser feliz, tô com o coração aberto
Bm5-/7       E7/9-
Com você eu tô esperto
Am              Am/G
Já me acostumei com o seu jeitinho
                  F#m7/5-                   E7/9-
De falar no telefone besteirinhas pra me provocar
Am                               Am/G
Quando eu te pegar "ce" tá perdida
                        F#m7/5-         Bm5-/7  E7/9-
Vai se arrepender De um dia ter me tirado do meu lugar
Am                             Am/G
Peço, por favor, não se apaixone,
                  F#m7/5-                       E7/9-
Pois não sou aquele homem que um dia o seu pai sonhou
Am                   Am/G
Eu só tenho cara de santinho
                   F#m7/5-
Sempre faço com jeitinho,
                Gsus
Coitada de quem acreditou!

C6/9  C7                     F7+
Eu,   prometo te dar carinho
Gsus               Em7             Am7
Mas gosto de ser sozinho, livre pra voar
Am6                           Gsus                       C6/9
Quem sabe um outro dia a gente possa se encontrar de novo...

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am/G = 3 X 2 2 1 X
Am6 = 5 X 4 5 5 X
Am7 = X 0 2 0 1 0
Bm5-/7 = X 2 3 2 3 X
C6/9 = X 3 2 2 3 3
C7 = X 3 2 3 1 X
C7+/9 = X 3 2 4 3 X
C7/9 = X 3 2 3 3 X
E7/9- = X X 2 1 3 1
Em7 = 0 2 2 0 3 0
F#m7/5- = 2 X 2 2 1 X
F/G = 3 X 3 2 1 X
F7+ = 1 X 2 2 1 X
Gsus = 3 2 0 0 0 3
