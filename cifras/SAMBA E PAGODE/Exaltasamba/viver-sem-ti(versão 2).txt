Exaltasamba - Viver Sem Ti

Solo:
20  20  21  23  23  12  21  30  20
20  20  21  23  23  12  21  23  20
30  30  32  34  32  30  42  30  34
30  30  32  34  32  30  42  40  42  40

 G           C       Cm     G
Eu não vou saber viver sem ti...
               C   Cm          Bm
O mundo não tem razão pra mim
                C       Cm      Bm
Não me fale adeus,não vou ouvir
             C         Cm
Não vou aceitar o fim...

G           C         Cm    G
Não me complica,não faz assim
                 C     Cm       Bm
Também não vai ser ,fácil pra mim
            C      Cm        Bm
Ensaiei tanto pra vir aqui

              C    Cm
Infelizmente é o fim...

C             D           G
Me diz aonde foi que eu errei?
                     C
Eu já sei,tem outro alguém
                 D
Tentando atrapalhar
        Em
Nosso amor...

C          D               G
Calma,senta,não tem ninguém!
                  A
É pior tente entender
                D    Bm
Eu não amo mais você

Refrão:
C
Para por favor não vá!
Am
Eu não quero te assustar
       G          D/F#
Mas não dá pra continuar
     Em     Bm
sem você!

C
Eu vou rezar por você
Am
Por que sei que vai doer
       G          D/F#
Mais não dá pra continuar
Em          Bm
Com você!

C
Eu vou chorar (fazer o que?)
Am
Vou lutar

Lutar pra que?
   G           C           G
Acabou... não acabou não... acabou sim!!!

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
