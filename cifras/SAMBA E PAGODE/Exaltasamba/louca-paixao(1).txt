Exaltasamba - Louca Paixão

Intro: G7+ C D7/9 G7+ C C Bm Am G7+

G7+                  C
Vem a noite e eu tão só
   D7/9         G7+
Doidinho pra te ver
                 C
No meu quarto na pior
  D7/9         G7+
Querendo só você

                C
Nem a lua nem o sol
  D7/9        G7+
Acendem um coração
                   C
É tão triste adormecer
   D7/9          G7+
E acordar na solidão

                     C    D7/9     Bm
Passa o dia, o tempo passa (ôh, ôh, ôh)

       Em            Am   D7/9   G7+
E a saudade me maltrata ( êh, laia )
                   C            Bm
Tô carente, tô sozinho (ôh, ôh, ôh)
       Em           Am
Sinto falta do amorzinho
      D7/9         G7+   B7
Que você sabe dar

             Em
Ficar sem te ver
               Bm
Só vai machucar
          Am
Louca Paixão
D7/9            G7+
Deixa eu te amar
          Am
Louca Paixão
D7/9            G7+
Deixa eu te amar

   Em     Am
Ôh ôh, ôh ôh
D7/9            G7+
Deixa eu te amar
   Em     Am
Ôh ôh, ôh ôh
D7/9           Em  C  G7+

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D7/9 = X 5 4 5 5 X
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G7+ = 3 X 4 4 3 X
