Exaltasamba - De Bobeira

Introdução:  A7+  F7+  Bm7/5-  F#7+  E7
 Am7/9          Bm7/5-           E7/9
Te amar........é tudo que eu sonhei..........
Am7/9             Gm           C7/9
Mas sei lá.........eu não imaginei .............
F7+    F6           Gm               C7
Que o amor..........fosse me encarcerar..........
F7+            E7/9
E me fazer sofrer........
Am7/9             Bm7/5-          E7/9
Qual será..........o preço que paguei...........
Am7/9                  Gm                  C7/9
Pois num mar............. de sonhos mergulhei.............
F7+        F6             Dm                     G7
E o medo em mim .......... foi de chegar à conclusão............
C6/9         C7/9
Que o amor é assim
F7+               E7/9
Me entreguei à ilusão
A7+   A6       D               E7/9
De bobeira você........deu mancada..........

A7+   A6              Dm         G7
Apostei........ toda a nossa jornada..........
C6/9  C7/9     F7+   G7
E parei com a rapaziada.........
C6/9   Am       D                   E7/9
Pra ficar com você......... minha amada .........

----------------- Acordes -----------------
A = X 0 2 2 2 0
A6 = 5 X 4 6 5 X
A7+ = X 0 2 1 2 0
Am = X 0 2 2 1 0
Am7/9 = X X 7 5 8 7
Bm7/5- = X 2 3 2 3 X
C6/9 = X 3 2 2 3 3
C7 = X 3 2 3 1 X
C7/9 = X 3 2 3 3 X
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
E7/9 = X X 2 1 3 2
F#7+ = 2 X 3 3 2 X
F6 = 1 X 0 2 1 X
F7+ = 1 X 2 2 1 X
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
