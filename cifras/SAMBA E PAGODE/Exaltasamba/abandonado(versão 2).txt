Exaltasamba - Abandonado

o pagode é!!...

F#m7
Lá lá ia lá ia lá ia lá ia lá ai lá ai...
    A7+            F#m7          (2x)
Lá ia lá ia lá lá ia lá ia

 A7+                          Dm
Abandonado, é assim que eu me sinto longe de você.
   A7+                       C#m7        F#7
Despreparado, meu coração dá pulo perto de você.
Bm7                                                 E7
 E quanto mais o tempo passa, mais aumenta essa vontade.

E o que posso fazer
C#m7                                               C#m7                F#7
  Se quando beijo outra boca lembro sua voz tão roca me pedindo pra fazer.
  Bm7       C#m7     F#m7     E7
Carinho gostoso, amor venenoso.

   A7+                         Dm
To preocupado, será que não consigo mais te esquecer ?

   A7+                   C#m7            F#7
Desesperado, procuro uma forma de não te querer.
Bm7                                                 E7
Mas quando a gente se encontra, o amor sempre apronta.

Não consigo conter.
C#m7                                                  C#m7                   F#7
  Por mais que eu diga que não quero, toda noite te espero com vontade de fazer.
  Bm7       C#m7     F#m7     E7
Carinho gostoso, amor venenoso.

F#m7
Faz amor comigo sem ter hora pra acabar
                                              A7+
Mesmo que for só por essa noite, eu não quero nem saber.
       A7+
Quero amar você.
F#m7
Faz amor comigo até o dia clariar.
                                                A7+
To ligado, sei que vou sofrer, mas eu não quero nem saber quero amar você.

(volta ao início)

F#m7
Lá lá ia lá ia lá ia lá ia lá ai lá ai...
    A7+            F#m7         (2x)
Lá ia lá ia lá lá ia lá ia

 A7+                          Dm                     A7+
Abandonado, é assim que eu me sinto longe de você....

----------------- Acordes -----------------
A7+ = X 0 2 1 2 0
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
F#7 = 2 4 2 3 2 2
F#m7 = 2 X 2 2 2 X
