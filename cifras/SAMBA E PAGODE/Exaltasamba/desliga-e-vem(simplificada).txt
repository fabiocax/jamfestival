Exaltasamba - Desliga e Vem

Intro: G Bm7 C Am7 D

G
  Por favor me diz quem é você
Bm7
  Que me liga toda noite
C
  Pelo menos diz seu nome
Am7               D
  Quero te conhecer

G
  Só você meu bem me liga
Bm7
  Eu vivo tão abandonado
C
  Quero alguém em minha vida
Am7             D  D#º
Fica do meu lado

         Em
Diz quem é você

     Bm7             D7     D#º
Quem sabe a gente se entende
     Em
Será um prazer
     Bm7      C    D
Eu vivo tão carente

    G                      Bm7
Meu bem , não brinca assim comigo
              Dm              G7
Tô precisando tanto de uma namorada
    C                 D
Solidão parece o meu castigo
            G                D
Fala de uma vez que está apaixonada

     G               Bm7
Toda noite fica me excitando
                   Dm         G7
Meu bem , é uma tortura tanta sedução
       C                      D
Que loucura acho que estou te amando
                G                D     D#
Desliga e vem depressa pro meu coração

    G#                     Cm7
Meu bem , não brinca assim comigo
              D#m             G#7
Tô precisando tanto de uma namorada
    C#               D#
Solidão parece o meu castigo
            G#               D#
Fala de uma vez que está apaixonada

     G#              Cm7
Toda noite fica me excitando
                   D#m        G#7
Meu bem , é uma tortura tanta sedução
       C#                     D#
Que loucura acho que estou te amando
                G#               D#
Desliga e vem depressa pro meu coração

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
C# = X 4 6 6 6 4
Cm7 = X 3 5 3 4 3
D = X X 0 2 3 2
D# = X 6 5 3 4 3
D#m = X X 1 3 4 2
D#º = X X 1 2 1 2
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
G#7 = 4 6 4 5 4 4
G7 = 3 5 3 4 3 3
