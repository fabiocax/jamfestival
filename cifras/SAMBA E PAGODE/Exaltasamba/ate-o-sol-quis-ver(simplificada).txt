Exaltasamba - Até o Sol Quis Ver

Intro] E  B  A  Am  E  B

E      A    B        E  C#m
Meu amor é tão ruim te ouvir dizer
                 A F#m              A             B  A
Que tem que ir embora, depois de uma imensidão de prazer
E        F#m       B     E   C#m
Mas porque não fica um pouco mais aqui?
                A F#m                A               B  E
Já tá raiando o dia, mas não tem hora pra gente se envolver

G        C             B  Am
Quero de novo sentir seu gosto
        D   D          G
No seu ouvido falar besteiras
    C           B
Te levar além do céu
G        C                     B  Am
Me dá tua mão, vem sentir meu corpo
           D     D     G
Olha o que esse teu beijo me faz

     C          B
Fica só um pouco mais

B F#m B  E
 Até    o     sol quis ver
B      C#m
De onde vem tanta luz
G#m        A      F#m      D
Nosso amor tem poder, fogo que nos conduz
B   B E
Olha só como é bom
B C#m
Amanhecer assim
G#m        A      F#m  D
Nosso amor tem o dom de superar o fim

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G#m = 4 6 6 4 4 4
