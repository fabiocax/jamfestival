Exaltasamba - Aquela Canção

  Gm G#m Am A#m Bm Cm C#m Dm D#m Em Fm F#m Gm

Introd.: G7+ Am7 G7+ Am7 Bm7 Am7 G7+

Am7 Bm7 Cm7
Cm7/G           G7+     G6
Sempre que ouvir aquela canção
Cm7/G                  G7+       G6
Sei que vou chorar mas sem querer
Am7 Bm7 Cm7
Cm7/G           G7+    G6
Ela toca em mim bem no coração
Am7 Bm7 Cm7
Cm7/G                 G7+    G6
Traz tanta saudade de você
F#m5-/7    B7/9-         Em7/9
Lembro bem do dia em que você partiu
C#m5-/7  F#7/5+         Bm7         E7/9-
Nun...ca mais eu pude encontrar o seu olhar
A7/13     Bm7            Cm7     D7/9/11
Só sobrou saudade em seu lugar

Am7 Bm7 Cm7
Cm7/G              G7+     G6
Faz um ano ou mais pra mim tanto faz
Am7 Bm7 Cm7
Cm7/G             Bm5-/7    G7
Um minuto é muito sem você
C7+/9       D/C           Bm5-/7       E7/9-
Hoje eu não consigo mais, nem sorrir e nem sonhar
F#m5-/7    F7/11+       Em7/9 Ebm7/9 Dm7/9 G7
Sinto um vazio no meu peito
C#m5-/7  D/C           Bm5-/7       E7/9-
Se ouço aquela canção, aquela que fala de nós
A7/13       A7         Am7      D7/9/11
Sei que vou chorar mas não tem jeito

           G7+       G6    Dm7/F       C#7/11+
           Espero um dia poder te encontrar
           C7+/9           Cm7         D7/9/11
           E no seu ouvido baixinho cantar
           G7+       G6     Dm7/F      C#7/11+
           Aquela canção que faz relembrar
          C7+/9         F7/9           G7+
           O quanto foi bom ... te amar

----------------- Acordes -----------------
A#m = X 1 3 3 2 1
A7 = X 0 2 0 2 0
A7/13 = X 0 X 0 2 2
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
B7/9- = X 2 1 2 1 X
Bm = X 2 4 4 3 2
Bm5-/7 = X 2 3 2 3 X
Bm7 = X 2 4 2 3 2
C#7/11+ = X 4 5 4 6 X
C#m = X 4 6 6 5 4
C#m5-/7 = X 4 5 4 5 X
C7+/9 = X 3 2 4 3 X
Cm = X 3 5 5 4 3
Cm7 = X 3 5 3 4 3
Cm7/G = 3 1 1 3 1 X
D#m = X X 1 3 4 2
D/C = X 3 X 2 3 2
D7/9/11 = X 5 5 5 5 5
Dm = X X 0 2 3 1
Dm7/9 = X 5 3 5 5 X
Dm7/F = 1 X X 2 1 1
E7/9- = X X 2 1 3 1
Ebm7/9 = X 6 4 6 6 X
Em = 0 2 2 0 0 0
Em7/9 = X 7 5 7 7 X
F#7/5+ = 2 X 2 3 3 2
F#m = 2 4 4 2 2 2
F#m5-/7 = 2 X 2 2 1 X
F7/11+ = 1 X 1 2 0 X
F7/9 = X X 3 2 4 3
Fm = 1 3 3 1 1 1
G#m = 4 6 6 4 4 4
G6 = 3 X 2 4 3 X
G7 = 3 5 3 4 3 3
G7+ = 3 X 4 4 3 X
Gm = 3 5 5 3 3 3
