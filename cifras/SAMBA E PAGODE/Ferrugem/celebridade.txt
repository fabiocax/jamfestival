Ferrugem - Celebridade

F7+                                                   E9
Me sinto poderoso como rei tomado pelo amor de uma rainha.
F7+                                                           E9
O dono do castelo conquistei, a bela que pra sempre vai sei minha.
Bb7+           Bb6      Bb7+                    Bb6            Am7
Num sonho encantado eu viajei, nas minas de um tesouro incalculavel.
Bb7+          Bb6         Bb7+              Bb6           Am7/4
Repleto de sucego e muito amor, e uma felicidade incontrolavel.
F7+                                                         E9
Quem vive esse universo de prazer o sonho passa a ser realidade.
F9                                                         E9
e a gente pode ser o que quiser, até brincar de ser celebridade.
Bb7+      Bb6                Bb7+                  Bb6        Am7
Inverta sempre um novo amanhecer pra não cair no tédio da rotina.
Bb7+             Bb6       Bb7+              Bb6            Am7
Assim que se da gosto de viver, viver esta ilusão que nos fascina.
Gm7  C7/9            D9                  Fm
  É assim que eu sei amar, quem ama não pode exitar,
               Em7                        Gm7
É assim que eu sei sonhar, me deixa a paixão me levar,
   C7/9     D9                       Fm
É assim não vou mudar, e fale quem quiser falar,

              Am7/4        Gm7
É assim que eu sei amar, é assim, é...

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Am7/4 = 5 X 5 5 3 X
Bb6 = X 1 3 0 3 X
Bb7+ = X 1 3 2 3 1
C7/9 = X 3 2 3 3 X
D9 = X X 0 2 3 0
E9 = 0 2 4 1 0 0
Em7 = 0 2 2 0 3 0
F7+ = 1 X 2 2 1 X
F9 = 1 3 5 2 1 1
Fm = 1 3 3 1 1 1
Gm7 = 3 X 3 3 3 X
