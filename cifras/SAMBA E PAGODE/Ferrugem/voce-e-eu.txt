Ferrugem - Você e Eu

[Intro]  A9/E  E9  A9/E

 A9/E                              E9      A9/E       E9
Não era pra ser assim, você longe de mim, você longe de mim
A9/E                                         E9     A9/E            E9
E agora que eu consegui, tudo o que eu prometi, tudo o que eu prometi

 Am7/9
Vejo o pior de tudo acontecer, sem mais nem menos
              E9    A9/E     E9  Em7/9 Dm7/9
Deus tirou você de mim, ai que triste fim
Am7/11                             Am/G
Eu sei que um dia eu vou te encontrar
         F#m7/11   B7/4(9)   B7/4(9)  E
E para sempre eu vou te abraçar

 A9                       F#m7/11                   B7/4(9)
Nem o tempo pode apagar, tudo aquilo que a gente viveu
 E9         A9         E9    D/E
Você e eu, você e eu, você e eu
 F#m7/11                 B7/4(9)
Deixa quem quiser falar, o meu coração ainda é seu

 E9         A9         E9    D/E
Você e eu, você e eu, você e eu

 A9/E                              E9      A9/E       E9
Não era pra ser assim, você longe de mim, você longe de mim
A9/E                                         E9     A9/E            E9
E agora que eu consegui, tudo o que eu prometi, tudo o que eu prometi

 Am7/9
Vejo o pior de tudo acontecer, sem mais nem menos
              E9    A9/E     E9  Em7/9 Dm7/9
Deus tirou você de mim, ai que triste fim
Am7/11                             Am/G
Eu sei que um dia eu vou te encontrar
         F#m7/11   B7/4(9)   B7/4(9)  E
E para sempre eu vou te abraçar

 A9                       F#m7/11                   B7/4(9)
Nem o tempo pode apagar, tudo aquilo que a gente viveu
 E9         A9         E9    D/E
Você e eu, você e eu, você e eu
 F#m7/11                 B7/4(9)
Deixa quem quiser falar, o meu coração ainda é seu
 E9         A9         E9    D/E
Você e eu, você e eu, você e eu

----------------- Acordes -----------------
A9 = X 0 2 2 0 0
A9/E = 0 X 2 2 0 0
Am/G = 3 X 2 2 1 X
Am7/11 = 5 X 5 5 3 X
Am7/9 = X X 7 5 8 7
B7/4(9) = X 2 2 2 2 2
D/E = X X 2 2 3 2
Dm7/9 = X 5 3 5 5 X
E = 0 2 2 1 0 0
E9 = 0 2 4 1 0 0
Em7/9 = X 7 5 7 7 X
F#m7/11 = 2 X 2 2 0 X
