Ferrugem - Ensaboado

[Intro] Dm  G7  Em7  Gm  A7

Dm
É muito mais que mereço

Acordar com teu sorriso
            G7
Iluminando meu viver toda manhã
                  Em7    Gm  A7
Como o brilho do sol do sol
                 Dm
Sentir tua boca molhada
                                       G7
Dando um beijo saboroso carregado de açúcar
                        Em7
Tudo que eu sonhei pra mim pra
 Gm     A7
Mim pra mim
G#7M                    Fm
Estrela maior que nos guia
C                       C SUS  C

Faz nosso amor fortalecer
G#7M                   Fm
E a parte melhor do meu dia
C                     G A7
É tomar banho com você

Dm
E agente todo ensaboado

Feito dois apaixonados
                 G7
Se beijando namorando no chuveiro

E agente todo ensaboado
 Em7
No calor vai e vem
                               Gm     A7
A mente voa e a mão desliza o corpo inteiro
Dm
E agente todo ensaboa

A água cai e vai levando mil bolinhas
      G#7M
De espuma
           G7         C
É o nosso banho de prazer
G  Gm  A7
De prazer

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em7 = 0 2 2 0 3 0
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
G#7M = 4 X 5 5 4 X
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
