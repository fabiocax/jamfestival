Ferrugem - Conversei com Deus

Intro:  Em F#m E7

Am       D7                         G
Hoje eu conversei com Deus e ele apareceu pra mim
G#  C#                 F# F E
E disse pra cuidar dos meus.
                    F#
E quando tocou meu coração,
E                        F#
Me levou com ele pra um rolé.
E               F#
 Homem de pouca fé,
               E
Pega na minha mão,
               F#
Ande sobre a maré
                E
Mas não duvide não.
                   F#
É muito amor pra dar,
                  E
Tanta luz pra acender

            F#  E
Semear e regar,
              F#
Esperar e colher.
G#            D#m
A paz vai morar
               G#7
Com quem perceber
C#m       D#7  G#      D#7
Que estender a mão é viver.
   G#             D#m
E quem se curvar
                G#7
Depois que entender
C#m               D#7          G#7  D#7
Que todos são iguais, vai vencer.
                   F#                    G#                      G
Eu conversei com Deus, eu conversei com Deus, eu conversei com Deus

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C# = X 4 6 6 6 4
C#m = X 4 6 6 5 4
D#7 = X 6 5 6 4 X
D#m = X X 1 3 4 2
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
G#7 = 4 6 4 5 4 4
