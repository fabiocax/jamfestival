Alexandre Pires - Leva

[Intro] F7M  Bb/C  Gm  F7M  Bb/C

       F7M                           Gm7   C7/9
  Foi bom eu ficar com você o ano inteiro
         Gm7                    C7/9              F7M   Bb/C
  Pode crer foi legal te encontrar foi amor verdadeiro
     F7M                                     Gm7  C7/9
  É bom acordar com você quando amanhece o dia
         Gm7             C7/9             F7M  Cm7  F7
  Dá vontade de te agradar te trazer alegria
       Bb7M                 B°        E      Am   D7
  Tão bom encontrar com você sem ter hora marcada
         Gm7             C7/9                Cm7  F7
  Pra falar de amor baixinho quando é madrugada
       Bb7M               B°    E        Am   D7
  Tão bom é poder despertar em você fantasias
        Gm7                      C7/9                F7M  Bb/C
  Te envolver, te acender, te ligar, te fazer companhia

[Refrão]


   F7M
  Leva
         Gm7          Am7
  O meu som contigo, leva
        Bb7M   F/A  Gm
  E me faz a tua festa
         Bb/C        F7M  Bb/C
  Quero ver você feliz

   F7M
  Leva
         Gm7          Am7
  O meu som contigo, leva
        Bb7M   F/A  Gm
  E me faz a tua festa
         Bb/C        F7M  Bb/C
  Quero ver você feliz

     F7M                                      Gm7   C7/9
  É bom quando estou com você numa turma de amigos
        Gm7                C7/9                     F7M  Bb/C
  E depois da canção você fica escutando o que eu digo
      F7M                                   Gm7  C7/9
  No carro, na rua, no bar estou sempre contigo
        Gm7               C7/9            F7M  Cm7  F7
  Toda vez que você precisar você tem um amigo
      Bb7M                B°      E       Am   D7
  Estou pro que der e vier conte sempre comigo
           Gm7               C7/9                  Cm7   F7
  Pela estrada buscando emoções despertando os sentidos
        Bb7M              B°       E           Am    D7
  Com você, primavera, verão, no outono ou no inverno
        Gm7               C7/9              F7M   Bb/C
  Nosso caso de amor tem sabor de um sonho eterno

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Bb/C = X 3 X 3 3 1
Bb7M = X 1 3 2 3 1
B° = X 2 3 1 3 1
C7/9 = X 3 2 3 3 X
Cm7 = X 3 5 3 4 3
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
F/A = 5 X 3 5 6 X
F7 = 1 3 1 2 1 1
F7M = 1 X 2 2 1 X
Gm = 3 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
