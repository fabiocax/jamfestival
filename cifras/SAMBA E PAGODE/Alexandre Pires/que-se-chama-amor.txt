Alexandre Pires - Que Se Chama Amor

A                     C#m               D
Como é que uma coisa assim machuca tanto
         E                        A
E toma conta de todo o meu ser?
                C#m                       D
É uma saudade imensa que partiu meu coração;
                E                       A
É a dor mais funda que a pessoa pode ter.
                    C#m                 D
É um vírus que se pega com mil fantasias,
             E             A
Um simples toque de olhar.
               C#m                  D
Me sinto tão carente, conseqüência desta dor
E                                         A
Que não tem dia e nem tem hora pra acabar.
            C#m              D
Aí eu me afogo num copo de cerveja
           E                   A
E que nela esteja minha solução.
                  C#m                  D
Então, eu chego em casa todo dia embriagado:

E                                            A
Vou enfrentar o quarto e dormir com a solidão.

           C#m
Meu Deus, não!
D                          E
Eu não posso enfrentar essa dor
            A       C#m     D
Que se chama amor.
        E          A    C#m     D
Tomou conta do meu ser
E                A
Dia-a-dia! Pouco a pouco
     C#m           D
Já estou ficando louco
E                 A   A       C#m     D    E
Só por causa de você!...

----------------- Acordes -----------------
A = X 0 2 2 2 0
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
