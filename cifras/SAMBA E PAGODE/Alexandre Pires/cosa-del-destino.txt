Alexandre Pires - Cosa Del Destino

                    G
Eso es cosa del destino
                    Bm
ya sufrí mas estoy vivo
                    C                       Am
Voy buscando mi camino y el dolor sigue conmigo
                    F          D
Quiero aprender a vivir... sin ti
                    G
Yo te ame y tú lo sabes,
                    Bm
no merezco tus maldades
                    C                        Am
Sufrimiento que invade, hoy te digo mis verdades
                    F          D
Con tus infidelidades... se apagó
   C            D  G        F
el sueño que había para dos
   C              D      Em          A
Los planes que el tiempo se llevo... se llevo


D                  F#m             G
Porque será que insistes en llamar
A                                         D
Me torturas, ignoras y adoras hacerme llorar
                   F#m           G
Porque no me olvidas de una vez?
A                                             D
a pesar del dolor, del engaño existe un tal vez
                   F#m             G
Porque será que insistes en llamar
A                                          D
Me torturas, ignoras y adoras hacerme llorar
                 F#m            G
Porque no me olvidas de una vez?
A                                          B
A pesar del dolor del engaño existe un tal vez
C             D
Talvez... Talvez

                 G
Yo te ame y tú lo sabes
                  Bm
no merezco tus maldades
                   C                         Am
Sufrimiento que invade, hoy te digo mis verdades
                   F         D
Con tus infidelidades se apago
    C           D   G        F
El sueño que había para dos
    C             D      Em          A
Los planes que el tiempo se llevo... se llevo

D                  F#m             G
Porque será que insistes en llamar
A                                         D
Me torturas, ignoras y adoras hacerme llorar
                   F#m           G
Porque no me olvidas de una vez?
A                      B                     D#
a pesar del dolor, del engaño existe un tal vez
                   Gm          G#
Porque será que insistes en llamar
   B                                    D#
Me torturas, ignoras y adoras hacerme llorar
             Gm              G#
Porque no me olvidas de una vez?
  B                                     C
A pesar del dolor del engaño existe un tal vez
C#        D#
Talvez, talvez

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C# = X 4 6 6 6 4
D = X X 0 2 3 2
D# = X 6 5 3 4 3
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
Gm = 3 5 5 3 3 3
