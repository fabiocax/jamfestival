Alexandre Pires - Eu Vou Pra Cima

Intro: C#

C#
Quando toca o cavaquinho
Ela chega de mansinho
Vem sambando miudinho
               G#7
Meu Deus que molejo
Atiça o meu desejo
              C#       G#7
Atiça o meu desejo
C#
Eu sou mais um pagodeiro
De olho no seu pandeiro
Tão bacana, tão maneiro
            G#7
te ver requebrando
(Nossa Senhora, hein?)
E a geral te filmando
                C#      A#m
E a geral te filmando

 D#m
Para tudo, tudo para
                  C#
Quando ela se apresenta
      A#m                 D#m         G#7
E a temperatura do pagode esquenta
 C#       A#m
Esquenta
D#m
Tô parado, alucinado,
G#7              C#
no balanço dessa mina
      A#m                  D#m
E o pagode já perdeu até a rima
G#7        C#     G#7
Eu vou pra cima

Refrão: 4x

C#
Ela desce, ela sobe
A#m        D#m
pra me provocar
        G#7             D#m
É nesse corpo desenhado que eu vou
G#7        C#
Lalalaialaia

(Repete tudo)

----------------- Acordes -----------------
A#m = X 1 3 3 2 1
C# = X 4 6 6 6 4
D#m = X X 1 3 4 2
G#7 = 4 6 4 5 4 4
