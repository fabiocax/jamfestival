Alexandre Pires - Sissi

Intro: G  C  D7  G  D7  G  C  D7

   G     Bm                     C                    D7               G
Gatinha, quanto mais o tempo passa você perde a graça e vai  ficar sozinha
  Bm                  C                  D7               G
Você já ganhou da massa o troféu de a menina mais chatinha.
  Bm                 C                      D                G
Quem avisa amigo é... se quer ser respeitada então desce daí
 Bm                C                    D7
Desmancha essa cara e vai se divertir!

  G         Bm                    C                 D7                G
Gatinha, quanto mais o tempo passa você perde a graça e vai ficar sozinha
 Bm                   C                 D7               G
Você já ganhou da massa o troféu de a menina mais chatinha.
  Bm                C                      D7                 G  B7  Em
Quem avisa amigo é... se quer ser respeitada então desce daí

                              Bm                             Em
Quando tira foto... todo mundo ri, faz beicinho de Angelina Jolie
                         Bm     F#m  Fm  Em
Botox na cara, sílica no peito

                             Bm
Quando abre a boca só fala bobagem
                              Am                   D7                 G
Diz que acabou de chegar de viagem... de Paris, Espanha, Portugal, Berlim

( G  D7 )

" E o nome dela? "
          D7       G   Bm
E o nome dela é Sissi, (Sissi)
           C           D7
'Si' sentindo, 'si' achando
                G      Bm
(E o nome dela é Sissi)
      C            B7
Si adorando, si amando
         Em                          Bm
Ela se acha a última coca-cola do deserto
                                Am
A bunda mais linda do pagode esperto
            D7                   G
Ela tá precisando de um malandro certo

          Bm             C                    D7               G
Gatinha, quanto mais o tempo passa você perde a graça e vai  ficar sozinha
 Bm                   C                  D7               G
Você já ganhou da massa o troféu de a menina mais chatinha.
 Bm                  C                      D7                G
Quem avisa amigo é... se quer ser respeitada então desce daí
 Bm                C                    D7
Desmancha essa cara e vai se divertir!

  G       Bm                      C                 D7                G
Gatinha, quanto mais o tempo passa você perde a graça e vai ficar sozinha
 Bm                   C                 D7               G
Você já ganhou da massa o troféu de a menina mais chatinha.
  Bm                C                      D7                 G  B7  Em
Quem avisa amigo é... se quer ser respeitada então desce daí

                              Bm                             Em
Quando tira foto... todo mundo ri, faz beicinho de Angelina Jolie
                         Bm      F#m  Fm Em
Botox na cara, sílica no peito
                             Bm
Quando abre a boca só fala bobagem
                              Am                   D7                 G
Diz que acabou de chegar de viagem... de Paris, Espanha, Portugal, Berlim

( G  D7 )

" E o nome dela? "
          D7       G   Bm
E o nome dela é Sissi, (Sissi)
           C           D7
'Si' sentindo, 'si' achando
                G      Bm
(E o nome dela é Sissi)
      C            B7
Si adorando, si amando
         Em                          Bm
Ela se acha a última coca-cola do deserto
                                Am
A bunda mais linda do pagode esperto
            D7                   G
Ela tá precisando de um malandro certo

                   D7
FINAL :  FALA GATINHA
                      C  G
    " E o nome dela é Sissi

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
