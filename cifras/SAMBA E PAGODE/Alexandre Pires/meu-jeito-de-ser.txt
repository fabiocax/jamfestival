Alexandre Pires - Meu Jeito de Ser

G7+       Bm
Era só dizer pra mim,
 Em            Bm
que não sentia mais
C7+         Bm
Que tudo se acabou
A7         D7
como um vento forte que passou
G7+        Bm
Eu te amei e hoje eu sofro
Em           Bm
Mas eu sei o meu lugar
C7+          Bm
Me perdoa coração
A7                      D7
Por tão fácil assim me entregar
G7+             Bm
Você levou o meu amor
C7+     Cm         G7+
E de você nada restou
        Bm
Eu posso te dizer

C7+         Cm
Que nesse mundo...

(refrão)
         G7+
O meu jeito de ser
Bm
Era você
C7+
Era te amar
    D7
Não era sofrer

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
C7+ = X 3 2 0 0 X
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G7+ = 3 X 4 4 3 X
