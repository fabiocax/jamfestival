Alexandre Pires - Apelo

C7+                 F7+     G              C7+
Difícil ficar sem você, às vezes só faço chorar
                    F7+       G              Em  Am
Ouvir seu lamento daqui, saudades eu sinto daí
     F                G                  Em  Am
Pra que terminarmos assim, essa mágoa é ruim
    F                   G       C7+
Talvez nossa escolha esteja errada

                        F7+
Reflita melhor pra nós dois
     G              C7+
Dois anos e meio depois
   Am7             F      G
Daquela saída que deu em nada
    F                      G
Eu sei que ainda gosta de mim
     Em                  Am7
Teus olhos me contam que sim
  Bb7+           G7/4
Eternamente apaixonada


C7+            C7/9                    F
É pura tolice, certo dia você disse que eu sou
   G              C7+
o dono do seu coração
                  Am                      F
Pra que viver o passado, tão errado vamos acender
   G                 C7+
a chama da nossa paixão

C7+            C7/9                    F
É pura tolice, certo dia você disse que eu sou
   G              C7+
o dono do seu coração
                  Am                      F
Pra que viver o passado, tão errado vamos acender
   G                 Bb7+  F7+
a chama da nossa paixão

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Bb7+ = X 1 3 2 3 1
C7+ = X 3 2 0 0 X
C7/9 = X 3 2 3 3 X
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F7+ = 1 X 2 2 1 X
G = 3 2 0 0 0 3
G7/4 = 3 5 3 5 3 X
