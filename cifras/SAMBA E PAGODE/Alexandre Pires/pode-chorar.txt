Alexandre Pires - Pode Chorar

Intro:

           C            G       Am            F         Dm           Am          G    G! G!
E|---3---|-333-5-3-------33---|--333-5-3-----5-5-5---|--555-7-5-3---------------3~---3--3---|
B|-------|---------6-5-6------|----------6-5-------66|6-----------6-5---6-5-3--------3--3---|
G|-----55|5-----------------55|5---------------------|------------------------5-------------| 
D|-------|--------------------|----------------------|--------------------------------------|

Passagem para entrar a voz:

E|-------------8-10---8!--|
B|--8-----8-10------8-----|
G|----7-9-----------------|
D|------------------------|

C                    G          Am
Quase que acabo com a minha vida
                     F
Você me pôs num beco sem saída
    C   Am          G
Por que fez isso comigo?

C                   G      Am
Me dediquei somente a você
                     F
Tudo que eu podia eu tentei fazer
     C    Am        G   C7
Mas não, não adiantou.

                       F
Você não sabe o que é amar
                       Am
Você não sabe o que é amor
                      F                Dm
Acha que é só mesmo ficar, ficar, ficar
             G    C7
E se rolar rolou.
                      F
Não se maltrata o coração
                      Am7
De quem não merece sofrer.
                      F               Dm
Não vou ficar na solidão de mão em mão
             G7
Assim como você.
   Refrão:
        C
Pode chorar,
                       G
Mas eu não volto pra você.
        Am
Pode chorar,
                      F
Você não vai me convencer.
        Dm
Pode chorar,
                           Am            G
Você se lembra o quanto eu chorei por você.

D|-333-5-3-------|
B|---------5-3-1-|
G|---------------|
D|---------------|

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
