Alexandre Pires - El Problema Eres Tu

    F
Que cose de hombre soy yo
               Bb
Que insiste en dar alas a la ilusion
             Gm
Y que se entrega a los tragos
Bb                   C
Como si fuera la mas perfecta solucion


    F
Que tipo de hombre soy yo
                 Bb
que se pierde en noches

vacias en busca de amor
Gm                 F
ya no me acuerdo de aquella
               Bb     F       C
que el viernes pasado conmigo se quedo



Gm                                 C
ya estoy cansado de escribir poemas
                                   Bb
escuchar baladas que me dejan penas
            Bb              Dm   Am  A
necesito resolver este problema


       Dm
y el problema eres tu
                                    C
que me aprisionaste dentro de un castigo
                               Bb
me usaste tanto y no quedas conmigo
                               A
me dices ahora que no quieres verme

                   Dm
y el problema eres tu
                                 C
puedes leer lo que esta en el papel
                              Bb
te prometo ser un hombre muy fiel
                      C         Dm
la solucion de mi problema eres tu


C Bb A


    F
Que tipo de hombre soy yo
                 Bb
que se pierde en noches
          C        D
vacias en busca de amor
Gm                 F
ya no me acuerdo de aquella
               Bb     F       C
que el viernes pasado conmigo se quedo


Gm                                 C
ya estoy cansado de escribir poemas
                                   Bb
escuchar baladas que me dejan penas
            Gm              Dm   Am  A
necesito resolver este problema


       Dm
y el problema eres tu
                                    C
que me aprisionaste dentro de un castigo
                               Bb
me usaste tanto y no quedas conmigo
                               A
me dices ahora que no quieres verme

                   Dm
y el problema eres tu
                                 C
puedes leer lo que esta en el papel
                              Bb
te prometo ser un hombre muy fiel
                      C         Dm
la solucion de mi problema eres tu

Bb

C
me usaste tanto y no quedas conmigo

Bb                               Dm  A
me dices ahora que no quieres verme


                   Dm
y el problema eres tu
                                 C
puedes leer lo que esta en el papel
                              Bb
te prometo ser un hombre muy fiel
                      C         Dm   C
la solucion de mi problema eres tu
Bb                    C         Dm   C
la solucion de mi problema eres tu
Bb                    A         Dm
la solucion de mi problema eres tu

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
