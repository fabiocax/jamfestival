Thiaguinho - Deixa Eu Te Fazer Feliz

Intro: G

           C9             Em7
Perdi meu chão, quando te vi passar
           Am7/11  Dsus4  G/F  G/B
Roubei um beijo,  perdi  o ar .
          C9           Em7
Num só segundo tudo mudou eu sei,
    F7+  Dsus4            Gsus4  G/F
Sem medo por você me apaixonei .
            C9                G/B
Perdi meu chão, perto do céu cheguei
        Am7/11  Dsus4       G/F  Gsus4
Meu coração é   todo   seu   é   lei .
        C9           Em7
Essa canção é pra você lembrar
F7+  Dsus4             G
Quando pelo rádio escutar .
Em7                Bm7
Eu nunca amei como eu te amo,
Em7/C                  Bm7
Basta em meus olhos você notar,

F#m7(b5) F#dim          Em7
Peça pra Deus   pra te mostrar,
    Am7     Dsus4       G
Cuidado pra você não se assustar
Em7                  Bm7
Pois sou capaz de mudar os meus planos
Em7/C           Bm7
Por uma vida só com você
Am7          D/C
Por nossa história
Bm7(b5)    E7
Por nossos sonhos
Am7       Dsus4   G
Por um final feliz

C7+
Um olhar, um beijo e a tristeza vai ter fim
Em7
Deixa eu te fazer feliz
C7+
Só meu beijo é capaz de te deixar assim
Em7
Deixa eu te fazer feliz
C7+
Longe um do outro nossa vida é tão ruim
Em7
Deixa eu te fazer feliz
Em7/C  G/C
Deixa eu te fazer feliz
D/G  G
Deixa eu te fazer feliz

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Am7/11 = 5 X 5 5 3 X
Bm7 = X 2 4 2 3 2
Bm7(b5) = X 2 3 2 3 X
C7+ = X 3 2 0 0 X
C9 = X 3 5 5 3 3
D/C = X 3 X 2 3 2
D/G = 3 X X 2 3 2
Dsus4 = X X 0 2 3 3
E7 = 0 2 2 1 3 0
Em7 = 0 2 2 0 3 0
F#dim = 2 X 1 2 1 X
F#m7(b5) = 2 X 2 2 1 X
F7+ = 1 X 2 2 1 X
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G/C = X 3 5 4 3 3
G/F = 1 X X 0 0 3
Gsus4 = 3 5 5 5 3 3
