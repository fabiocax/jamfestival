Thiaguinho - Tudo que eu não tive com você

 Intro...  D9 % C9 G/B  (2x)

 D9                             C9         G/B
 eu fazendo tudo entreguei meu mundo pra você
                            C9   G/B
 você querendo o que? só viver!
  D9                             C9         G/B
 construir família ser sua companhia até morrer
                   D9       C9   G/B
 você querendo o que? só viver!
           Bb7+     C7+       D6/9 ^ E6/9 ^ D6/9 ^ E6/9
 eu já decidi não mais me trair
             Bb7+    C7+       Em  A/G
 é melhor o fim vou cuidar de mim
 D9                              C9        G/B
 quando eu ficar bem nem adianta vir aparecer
                     D9         C9   G/B
 já não tem mais porque eu te ver
 D9                              C9            G/B
 só olho pra frente fazendo questão de te esquecer
                    D9          C9  G/B
 já não tem mais porque eu te ver

               Bb7+   C7+       D6/9 ^ E6/9 ^ D6/9 ^ E6/9
 quando eu te quis você nem ligou
              Bb7+   C7+      Em  A/G
 hoje eu to afim de um novo amor

 Refrão
                            D9
 que me de tudo que não me deu
                            Bm7
 que em primeiro lugar seja eu
                               G7+
 que se preocupe em me ver sorrir
                                C9
 mostre o caminho que eu vou seguir
                              D9
 depois do amor sempre conversar
                               Bm7
 que esteja pronta pra me agradar
                             G7+
 olhos nos olhos corpo a tremer
                             C9
 tudo que eu não tive com você
                             D9           C9   G/B
 tudo que eu não tive com você, ô lalaialaia
                             D9           C9   G/B
 tudo que eu não tive com você, ô lalaialaia

----------------- Acordes -----------------
A/G = 3 X 2 2 2 X
Bb7+ = X 1 3 2 3 1
Bm7 = X 2 4 2 3 2
C7+ = X 3 2 0 0 X
C9 = X 3 5 5 3 3
D6/9 = X 5 4 2 0 0
D9 = X X 0 2 3 0
E6/9 = X 7 6 6 7 7
Em = 0 2 2 0 0 0
G/B = X 2 0 0 3 3
G7+ = 3 X 4 4 3 X
