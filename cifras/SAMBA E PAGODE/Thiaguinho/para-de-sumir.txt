Thiaguinho - Para de Sumir

( C  Bm7  Am7  Am ) 3x
( Em7  G ) (2x)

C,Bm7           Am7        Am
Eu sempre tive medo de me declarar
C,Bm7           Am7        Am
E o meu coração sempre quis se esconder
C,Bm7           Am7        Am     Em7
Queria poder fazer esse tempo voltar
       G
Tenho milhões de coisas pra te dizer

   C,Bm7           Am7          Am
No meu quarto eu sinto teu perfume inesquecível
C,Bm7           Am7        Am
Eu já fico imaginando a gente... Isso é terrível!

C,  Bm7           Am7        Am
Era só eu te olhar de um jeitinho mais quente
     Em                 G
E a noite de prazer se tornava diferente


C,Bm7           Am7        Am
Tento dormir,Mas olho pro lado e vem a solidão
C,Bm7           Am7        Am
Nosso cantinho,Ta sentindo falta do nosso tesão
C,Bm7           Am7        Am          Em7 A7(9)
Cadê você? Tô desesperado vem aqui me ver

           G
Para de sumir!
            A7
Teu melhor lugar é do meu lado
         G
Fica aqui!
             A7(4)
Sinto tua falta

            G
Para de sumir!
             A7
Sem você do lado
                Em7 A7
Não tem nem porque


Lá lá laiá laiá laiá...

( C  Bm7  Am7  Am ) 3x
( Em7  G ) (2x)

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7(4) = X 0 2 0 3 0
A7(9) = 5 X 5 4 2 X
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
