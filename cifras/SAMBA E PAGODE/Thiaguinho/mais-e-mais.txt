Thiaguinho - Mais e Mais

Intro  2x: Am7  F7M  C9  G

Am7       F7M -            C9
   Eu tentando te evitar
   G                 Am7
E você não sai daqui
       F7M           C9
Eu tentando esquecer
             G            Am7
Tudo que ao seu lado vivi

       F7M              C9
Eu tentando não sofrer
      G            Am7
Você não tá nem ai
       F7M          C9              G              Am7
Eu tentando entender por qual razão eu te perdi

           F7M                C9
Para dá um tempo pra eu pensar
         G             Am7
E assimilar o que rolou

             F7M           C9      G   Am7
Para dá um tempo por favor
         F7M             C9
Nossas amizades são as mesmas
       G            Am7
Sempre no mesmo lugar
           F7M                 C9      G
Para dá um tempo pra eu pensar

                  Am7                F7M
Sei que ainda me quer vejo no seu olhar
             C9                    G
Mais a situação eu não posso aceitar
            Am7                            F7/9-
Eu querendo mais e mais do que você quer dar pra mim
         C9                          G
Cada dia mais e mais eu sinto chegamos ao fim
                   Am                 F7M
Sei dos seus problemas você sabe os meus
            C9                  G
To vivendo contra a vontade de Deus
            Am7                            F7M
Eu querendo mais e mais do que guardaram amor pra mim
               C9                        G
Se é pra ter metade de você prefiro o nosso fim

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
C9 = X 3 5 5 3 3
F7M = 1 X 2 2 1 X
G = 3 2 0 0 0 3
