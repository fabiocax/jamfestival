Thiaguinho - Pé Na Areia

[Intro] A  B  A  E
        A  B  A  E

E7M/9
Vamos amor
                       E7/4/9
Vamos curtir, bora pra beira do mar
                    A7M         A6
Vamos pra onde tá fazendo mais calor
                     Am7
E ninguém pode nos achar

E7M/9
Bora viver
                   E7/4/9
Você e eu agora, eu e você
                     A7M       A6
Vamos pra onde tudo pode acontecer
           D7/9
Inclusive nada


F#m7                                    G#m7        F#m
Nada pode ser melhor do que a gente junto, nós dois

                                        G#m             F#m7
Mil idéias e uma história de amor, e o assunto é nós dois
                                     G#m7
Dois amantes namorando na beira da praia, iá iá iá
F#m7/9                             G#7M   G#
Nada pode ser melhor pra gente se amar

       F#m7/9                          E7M
Pé na areia, a caipirinha, água de côco, a cervejinha
       F#m7/9                        G7M
Pé na areia, água de côco, beira do mar
       F#m7/9                        E7M
Pé na areia, a caipirinha, água de côco, a cervejinha
       F#m7/9                        G7M
Pé na areia, água de côco, beira do mar


----------------- Acordes -----------------
A = X 0 2 2 2 0
A6 = 5 X 4 6 5 X
A7M = X 0 2 1 2 0
Am7 = X 0 2 0 1 0
B = X 2 4 4 4 2
D7/9 = X 5 4 5 5 X
E = 0 2 2 1 0 0
E7/4/9 = X X 2 2 3 2
E7M = X X 2 4 4 4
E7M/9 = X 7 6 8 7 X
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
F#m7/9 = X X 4 2 5 4
G# = 4 3 1 1 1 4
G#7M = 4 X 5 5 4 X
G#m = 4 6 6 4 4 4
G#m7 = 4 X 4 4 4 X
G7M = 3 X 4 4 3 X
