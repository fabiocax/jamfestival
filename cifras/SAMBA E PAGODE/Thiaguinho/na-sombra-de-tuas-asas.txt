Thiaguinho - Na Sombra de Tuas Asas

Intro:  C  Bb  F   Dm

C          C/F   C        C/F       Am
tu és fortaleza, és o meu tudo
         G                    F               F9
o que presciso, eu encontro em ti,  fonte de tudo
C           C/F  C             C/F            Am
levanta o caido, restaura o abatido
            G              F
encontramos paz, em tua presença
     Am
pois quando nos prostramos
G
e te adoramos
   F     E/C    Dm
sentimos-nos seguros
  C               Am       F     E/C    Dm    G
na sombra de tuas asas, eu posso descansar
     C                Bb                 Dm          G
pois quando vem as tribulações,  tu és o meu refugio
  C             Am      F  E/C    Dm  G
em meio a tempestade eu posso confiar

C                   Bb
pois quando vem as muitas aguas
        Dm        G             C
tu és o meu refugio, confio em ti

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C/F = X 8 10 9 8 8
Dm = X X 0 2 3 1
E/C = X 3 2 1 0 0
F = 1 3 3 2 1 1
F9 = 1 3 5 2 1 1
G = 3 2 0 0 0 3
