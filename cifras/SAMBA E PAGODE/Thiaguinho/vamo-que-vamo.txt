Thiaguinho - VamoQVamo

Intro: C Gsus

C7M         C            F7+
No lê lê lê, no lê lê lê, no lê lê lê
Gsus Gsus
uh, uh

C7M         C            F7+
No lê lê lê, no lê lê lê, no lê lê lê
G    G
uh, uh

C7M      C7M      C7M  C
Vamo que vamo que vamo vamo
        C             F7+           G
Que hoje é dia de curtir com os parça
C7M        C                    F7+        G
Tem o dia certo da semana que ela não embaça
C7M                    C
Vamo que vamo que vamo vamo
C                   F7+   G
Que a feijuca já ta na medida

C7M        C                       F7+           G        E7(#9)
To ficando louco que esse clima ainda é tardezinha

   Am7       D7      Am7       D7
Se tem batucada, conversa fiada
               F7+                 Em7
Chama que eu já vou, cola que eu já to
       Bm7(b5)           E7(#9)
Até acabar
     Am7        D7     Am7       G7(13)
Sou da madrugada sem hora marcada
           F7+                F#dim
Vou ficar aqui, traz mais uma aí
               G              C7M
Que eu sou inimigo do fim
C7M                     F7+
Não tem jeito eu sou assim
   G                C7M
Amo minha rapaziada
      C                F7+
Junta o gelo e o "pagodin"
      G                 C7+
Não da outra é só risada
               Ab7+
Já mandaram te dizer
   Bb7M           C7+
Só ta faltando você
Am7            Ab7+ Bb7M      C7+
Nossa vibe você não vai esquecer.

----------------- Acordes -----------------
Ab7+ = 4 X 5 5 4 X
Am7 = X 0 2 0 1 0
Bb7M = X 1 3 2 3 1
Bm7(b5) = X 2 3 2 3 X
C = X 3 2 0 1 0
C7+ = X 3 2 0 0 X
C7M = X 3 2 0 0 X
D7 = X X 0 2 1 2
E7(#9) = X 7 6 7 8 X
Em7 = 0 2 2 0 3 0
F#dim = 2 X 1 2 1 X
F7+ = 1 X 2 2 1 X
G = 3 2 0 0 0 3
G7(13) = 3 X 3 4 5 X
Gsus = 3 2 0 0 0 3
