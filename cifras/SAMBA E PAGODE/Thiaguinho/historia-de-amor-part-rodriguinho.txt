Thiaguinho - Historia de Amor (part. Rodriguinho)

D6         E7/9            C#m7   F#m7
...E foi assim...que tudo aconteceu
D6                E7/9
...Num simples olhar
              C#m7              F#m7
Num olhar discreto, querendo se dar
       D6             E7/9
Jà foi me chamando de amor
             C#m7              F#m7
Dizendo pra todos que agora eu sou
D6           E7/9             C#m7     F#m7
...Seu namorado...eu sou seu novo namorado
D6              E7/9        C#m7       F#m7
...Com muito prazer...apaixonado por você
D6          E7/9             C#m7      F#m7
..Seu namorado...eu sou seu novo namorado
D6              E7/9       C#m7         F#m7
..Com muito prazer...apaixonado por você

Base para a parte falada: D6  E7  C#m7  F#m7


Sempre fomos o encaixe perfeito
Enquanto eu sempre fui do seu jeito
De repente minha vida mudou
E foi nessa hora que o amor acabou
Já não sentia mais sua falta
Não tinha medo da solidão
Fui embora não pensava em volta
E assim acabei com os eu coração
Eu sei que dei mancada contigo
Nessa hora não fui seu amigo
Você se apaixonou loucamente
Por mim não queria saber do perigo
Fui embora e te deixei chorando
Sou culpado, não to me esquivando
Eu mereço sofrer eu sei disso
Mas não to querendo nenhum compromisso


D6            E7/9              C#m7       F#m7
...Mais foi assim...que tambèm tudo terminou
D6              E7           C#m7         F#m7
È...eu nem percebi...e você nem sequer notou
        D6               E7/9
Que a gente quase não se via
        C#m7               F#m7
Parecia atè que não se conhecia
      D6               E7/9
O que era lindo ficou triste
          C#m7         F#m7
E hoje apenas amizade existe

D6             E7/9          C#m7           F#m7
Ôh!...Tudo acabado...o nosso amor està acabado
D6                     E7/9
...Foi bom enquanto durou
               C#m7        F#m7
foi bom bom enquanto existia amor

----------------- Acordes -----------------
C#m7 = X 4 6 4 5 4
D6 = X 5 4 2 0 X
E7 = 0 2 2 1 3 0
E7/9 = X X 2 1 3 2
F#m7 = 2 X 2 2 2 X
