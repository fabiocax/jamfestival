Dilsinho - Péssimo Negócio

[Intro] Gm7  F/A  Bb
        Gm7  F/A  Eb

[Primeira Parte]

Gm7                  F/A                 Bb     Eb
   Precisei de mil frases certas pra te conquistar
     Cm7       Bb/D     F
E de uma só errada pra te perder
Gm7                F/A           Bb   Eb
   Eu levei tanto tempo pra te apaixonar
   Cm7        Bb/D  F
Em um minuto só perdi você

[Pré-Refrão]

Bb             F/A           Gm7
   Conhece alguém que jogou fora um diamante?
    Cm7     Bb/D          F
Loucura, né?   Mas eu joguei
Bb             F/A            Gm7
   Quem já trocou um grande amor por um instante?

    Cm7     Bb/D           F
Burrice, né?   Mas eu troquei

[Refrão]

             Bb
Coração, me fala
                 F/A            Gm7
Como é que você faz um negócio desses?
                             Cm7
Trocar um pra sempre por às vezes
         Bb/D            F/A  Eb/G
Uma aventura por uma paixão
 F                    Bb
Péssimo negócio, coração

                 F/A            Gm7
Como é que você faz um negócio desses?
                             Cm7
Trocar um pra sempre por às vezes
       Bb/D                     F
Ela falou que não quer mais conversa
                Gm7  F/A  Bb
Agora dorme com essa
                Gm7  F/A  Eb
Agora dorme com essa

[Pré-Refrão]

Bb             F/A           Gm7
   Conhece alguém que jogou fora um diamante?
    Cm7     Bb/D          F
Loucura, né?   Mas eu joguei
Bb             F/A            Gm7
   Quem já trocou um grande amor por um instante?
    Cm7     Bb/D           F
Burrice, né?   Mas eu troquei

[Refrão]

             Bb
Coração, me fala
                 F/A            Gm7
Como é que você faz um negócio desses?
                             Cm7
Trocar um pra sempre por às vezes
         Bb/D            F/A  Eb/G
Uma aventura por uma paixão
 F                    Bb
Péssimo negócio, coração

                 F/A            Gm7
Como é que você faz um negócio desses?
                             Cm7
Trocar um pra sempre por às vezes
       Bb/D                     F   F#
Ela falou que não quer mais conversa

             B
Coração, me fala
                 F#/A#          G#m7
Como é que você faz um negócio desses?
                             C#m7
Trocar um pra sempre por às vezes
         B/D#            F#/A#  E/G#
Uma aventura por uma paixão
 F#                   B
Péssimo negócio, coração

                 F#/A#          G#m7
Como é que você faz um negócio desses?
                             C#m7
Trocar um pra sempre por às vezes
       B/D#                     F#
Ela falou que não quer mais conversa
                G#m
Agora dorme com essa

----------------- Acordes -----------------
B = X 2 4 4 4 2
B/D# = X 6 X 4 7 7
Bb = X 1 3 3 3 1
Bb/D = X 5 X 3 6 6
C#m7 = X 4 6 4 5 4
Cm7 = X 3 5 3 4 3
E/G# = 4 X 2 4 5 X
Eb = X 6 5 3 4 3
Eb/G = 3 X 1 3 4 X
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
F#/A# = 6 X 4 6 7 X
F/A = 5 X 3 5 6 X
G#m = 4 6 6 4 4 4
G#m7 = 4 X 4 4 4 X
Gm7 = 3 X 3 3 3 X
