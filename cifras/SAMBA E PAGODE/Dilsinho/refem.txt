Dilsinho - Refém

[Intro] Cm7  Ab9  Eb  Bb
        Cm7  Ab9  Eb  Bb

   Cm7       Ab9       Eb        Bb
E|------------------6-----------------------|
B|-------4--------4----------4--------3-----|
G|-----3--------5----------3--------3---3---|
D|---5-------------------5---------3--------|
A|-3-------------------6---------1----------|
E|-----------4------------------------------|

   Cm7       Ab9       Eb        Bb
E|------------------6-----------------------|
B|-------4--------4----------4--------3-----|
G|-----3--------5----------3--------3---3---|
D|---5-------------------5---------3--------|
A|-3-------------------6---------1----------|
E|-----------4------------------------------|

[Primeira Parte]


       Eb9        Ab               Eb
Aconteceu, minha vida estava no lugar
      Ab9             Eb                    Bb4  Bb
Tudo parecia se encaixar, foi quando eu te vi
Bº     Cm7          Ab9                Eb
Escureceu, tudo que era verdadeiro em mim
        Ab9                   Eb                   Bb4 Bb
Num instante foi chegando ao fim, foi quando eu te vi

[Riff 1]

E|------------------------------------------|
B|--4/6--4-3-----3--3/4---------------------|
G|------------5-----------------------------|
D|------------------------------------------|
A|------------------------------------------|
E|------------------------------------------|

[Segunda Parte]

         Ab9               Ab/C               Bb/D               Cm7  Bb6
E as loucuras dentro do cinema, aquela linda cena que a gente viveu
        Ab9                                    Bb               Cm7
E num quarto de motel barato, o espelho enfumaçado e um recado seu
        Ab9               Ab/C               Bb/D             Cm7  Bb6
Uma paixão de contos literários, você é Julieta e eu o seu Romeu
             Db7M                    Bb/D
Um amor proibido, em sigilo que faz bem

[Refrão]

      Eb/G Ab9                  Ab6/9             Eb9
Você me arranha, e no final de tudo sou eu quem apanha
            Eb4           Eb       Ab9                     Bb4                 Cm7
Inventando sempre uma desculpa estranha, pra disfarçar as marcas desse nosso amor
               Bb/D   Eb/G  Ab9                    Ab6/9               Eb9
Para por favor, to falando sério, pra que se apressar em revelar o mistério
                Bb4        Bb     Ab9                 Ab6/9                 Cm7/11
Se existe sentimento não é adultério, você sempre soube que eu já tinha alguém

[Riff 2]

E|------------------------------------------|
B|--4-3-1-----------------------------------|
G|---------3-1-0----------------------------|
D|----------------3-------------------------|
A|------------------------------------------|
E|------------------------------------------|

                                    Ab9
Não venha com chantagem me fazer refém
Db7M
   Não venha com chantagem me fazer refém

( Cm7  Ab9  Eb  Bb )

[Primeira Parte]

       Eb9        Ab               Eb
Aconteceu, minha vida estava no lugar
      Ab9             Eb                    Bb4  Bb
Tudo parecia se encaixar, foi quando eu te vi
Bº     Cm7          Ab9                Eb
Escureceu, tudo que era verdadeiro em mim
        Ab9                   Eb                   Bb4 Bb
Num instante foi chegando ao fim, foi quando eu te vi

[Riff 1]

E|------------------------------------------|
B|--4/6--4-3-----3--3/4---------------------|
G|------------5-----------------------------|
D|------------------------------------------|
A|------------------------------------------|
E|------------------------------------------|

[Segunda Parte]

         Ab9               Ab/C               Bb/D               Cm7  Bb6
E as loucuras dentro do cinema, aquela linda cena que a gente viveu
        Ab9                                    Bb               Cm7
E num quarto de motel barato, o espelho enfumaçado e um recado seu
        Ab9               Ab/C               Bb/D             Cm7  Bb6
Uma paixão de contos literários, você é Julieta e eu o seu Romeu
             Db7M                    Bb/D
Um amor proibido, em sigilo que faz bem

[Refrão]

      Eb/G Ab9                  Ab6/9             Eb9
Você me arranha, e no final de tudo sou eu quem apanha
            Eb4           Eb       Ab9                     Bb4                 Cm7
Inventando sempre uma desculpa estranha, pra disfarçar as marcas desse nosso amor
               Bb/D   Eb/G  Ab9                    Ab6/9               Eb9
Para por favor, to falando sério, pra que se apressar em revelar o mistério
                Bb4        Bb     Ab9                 Ab6/9                 Cm7/11
Se existe sentimento não é adultério, você sempre soube que eu já tinha alguém
                   Bb/D
Não venha com chantagem

      Eb/G Ab9                  Ab6/9             Eb9
Você me arranha, e no final de tudo sou eu quem apanha
            Eb4           Eb       Ab9                     Bb4                 Cm7
Inventando sempre uma desculpa estranha, pra disfarçar as marcas desse nosso amor
               Bb/D   Eb/G  Ab9                    Ab6/9               Eb9
Para por favor, to falando sério, pra que se apressar em revelar o mistério
                Bb4        Bb     Ab9                 Ab6/9                 Cm7/11
Se existe sentimento não é adultério, você sempre soube que eu já tinha alguém

[Riff 2]

E|------------------------------------------|
B|--4-3-1-----------------------------------|
G|---------3-1-0----------------------------|
D|----------------3-------------------------|
A|------------------------------------------|
E|------------------------------------------|

                                    Ab9
Não venha com chantagem me fazer refém
Db7M
   Não venha com chantagem me fazer refém

( Cm7  Ab9  Eb  Bb )
( Cm7 )

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Ab/C = X 3 X 1 4 4
Ab6/9 = 4 3 3 3 X X
Ab9 = 4 6 8 5 4 4
Bb = X 1 3 3 3 1
Bb/D = X 5 X 3 6 6
Bb4 = X 1 3 3 4 1
Bb6 = X 1 3 0 3 X
Cm7 = X 3 5 3 4 3
Cm7/11 = X 3 3 3 4 3
Db7M = X 4 6 5 6 4
Eb = X 6 5 3 4 3
Eb/G = 3 X 1 3 4 X
Eb4 = X 6 8 8 9 6
Eb9 = X 6 8 8 6 6
