Dilsinho - A Vingança

Intro Am7 44 32 23 14 Gm7 42 45 21 23 Am7 D7

Bb7+                         C7/4/9
É com a mesma moeda que se paga
                C/Bb
E foi só pegar você com a mão na massa
Am7                                    D7/4/9
Que a vingança foi formando no meu pensamento
         D7
Fiquei tenso

Bb7+                         C7/4/9
Fui malandro, derramei aquele choro
              C/Bb
Descarado, soluçado, mentiroso
Am7                             D7/4/9
Acabou e o pior é que eu nem lamento
             D7
Sem perder tempo

       Bb7+
Eu não vou deixar a deprê me pegar

     C7/4/9                C/Bb
Gostei de você, respeitei, mas sei lá
      Am7                                          Am7 G#m7 Gm7 F7+ E7 Eb7 D7
Basta uma cena dessa pra gente perder todo o respeito, não tem jeito
    Bb7+
Tá achando que eu sofri, fiquei mal
      C7/4/9             C/Bb
Mas eu tô aqui, tudo bem, na moral
         Am4
Eu não tenho a intenção de te reconquistar
       D7/4/9                   D7/4
Mas eu tenho uma surpresa pra te dar

              Bb7+                                      C7/4/9
Hoje eu tô na pista, e qualquer proposta indecente me conquista
                                  F7+               C/E
E não vai ser com uma mina desconhecida, vai ser sacanagem
            Cm6/Eb                     D7/4  D7
Vai mudar a imagem que os outros têm de mim
     Bb7+                               C7/4/9
E na hora, se chegar alguma amiga sua eu pego
                              F7+
Se for inimiga eu também não nego
              C/E           Eb6/9
Vai sentir na pele como é ruim
                    D7/4/9
Vai saber bem assim que eu aceitei o fim

Bb7+  Bb6             C7/4/9                       F7+ C/E Eb6/9 D7/9b
Lelele lele lelele lelele   lelele lele lelele lelele

----------------- Acordes -----------------
Am4 = X 0 0 2 1 0
Am7 = X 0 2 0 1 0
Bb6 = X 1 3 0 3 X
Bb7+ = X 1 3 2 3 1
C/Bb = X 1 2 0 1 X
C/E = 0 3 2 0 1 0
C7/4/9 = X 3 3 3 3 3
Cm6/Eb = X X 1 2 1 3
D7 = X X 0 2 1 2
D7/4 = X X 0 2 1 3
D7/4/9 = X 5 5 5 5 5
D7/9b = X 5 4 5 4 X
E7 = 0 2 2 1 3 0
Eb6/9 = X X 1 0 1 1
Eb7 = X 6 5 6 4 X
F7+ = 1 X 2 2 1 X
G#m7 = 4 X 4 4 4 X
Gm7 = 3 X 3 3 3 X
