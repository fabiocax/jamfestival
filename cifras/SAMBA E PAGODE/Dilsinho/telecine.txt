Dilsinho - Telecine

[Intro] D5  A  Bm7  G9
        D5  A  Bm7  G9

[Primeira Parte]

 D5
Faz assim
                           A6
Toda vez que você for lembrar mim
                            Bm7
Escreve num caderno pra não esquecer
    G9
Eu faço isso quando lembro de você

       D9
Faz assim
                                    A
Se a música da rádio te lembrar de nós
                                   Bm7
Se ainda tem o meu perfume nos lençóis
                             G9
Arrisca, me liga, quem sabe tô do mesmo jeito que você?


[Pré-Refrão]

 Bm7   G9                 D9
Pensa bem, ainda existe chance
               A9
A gente nem ficou distante assim
Bm7        G9              A
    Pensa bem e volta pra mim

[Refrão]

D9                   A9              Bm7
   E a gente bota o filme lá no Telecine
                   G9
Meu Passado Me Condena do Fábio Porchat
D9                    A9                  Bm7
   Se não gostar da ideia, eu boto O Exorcista
                  G9             A9   D9
E uso isso de desculpa só pra te abraçar

                 A9                    Bm7
O filme pouco importa eu já comprei pipoca
                       G9            A9  Em7
Com leite condensado pra comer com guaraná
                       G9
E se nada disso funcionar
                   A9
Eu assobio uma canção de amor pra te ninar

(  D5  A  Bm7  G9 )
(  D5  A  Bm7  G9 )

[Primeira Parte]

 D5
Faz assim
                           A6
Toda vez que você for lembrar mim
                            Bm7
Escreve num caderno pra não esquecer
    G9
Eu faço isso quando lembro de você

       D9
Faz assim
                                    A
Se a música da rádio te lembrar de nós
                                   Bm7
Se ainda tem o meu perfume nos lençóis
                             G9
Arrisca, me liga, quem sabe tô do mesmo jeito que você?

[Pré-Refrão]

 Bm7   G9                 D9
Pensa bem, ainda existe chance
               A9
A gente nem ficou distante assim
Bm7        G9              A
    Pensa bem e volta pra mim

[Refrão]

D9                   A9              Bm7
   E a gente bota o filme lá no Telecine
                   G9
Meu Passado Me Condena do Fábio Porchat
D9                    A9                  Bm7
   Se não gostar da ideia, eu boto O Exorcista
                  G9             A9   D9
E uso isso de desculpa só pra te abraçar

                 A9                    Bm7
O filme pouco importa eu já comprei pipoca
                       G9            A9  Em7
Com leite condensado pra comer com guaraná
                       G9
E se nada disso funcionar
                   A9
Eu assobio uma canção de amor pra te ninar

D9                   A9              Bm7
   E a gente bota o filme lá no Telecine
                   G9
Meu Passado Me Condena do Fábio Porchat
D9                    A9                  Bm7
   Se não gostar da ideia, eu boto O Exorcista
                  G9             A9   D9
E uso isso de desculpa só pra te abraçar

                 A9                    Bm7
O filme pouco importa eu já comprei pipoca
                       G9            A9  Em7
Com leite condensado pra comer com guaraná
                       G9
E se nada disso funcionar
                   A9
Eu assobio uma canção de amor pra te ninar

(  D5  A  Bm7  G9 )
(  D5  A  Bm7  G9  D )

----------------- Acordes -----------------
A = X 0 2 2 2 0
A6 = 5 X 4 6 5 X
A9 = X 0 2 2 0 0
Bm7 = X 2 4 2 3 2
D = X X 0 2 3 2
D5 = X 5 7 7 X X
D9 = X X 0 2 3 0
Em7 = 0 2 2 0 3 0
G9 = 3 X 0 2 0 X
