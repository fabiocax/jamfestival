Dilsinho - Ioiô (part. Ivete Sangalo)

[Intro] F  Dm  Bb9  C

[Primeira Parte]

 F                                 Dm
Vai, pode fingir que não é minha agora
                                Bb9
Eu já conheço o final dessa história
                            Gm
Daqui a pouco a ficha vai cair
                             C
Saudade vai bater e aí você volta

         F
Tá se achando
                                Dm
Pensando que eu tô na palma da mão
                               Bb9
Se você quer brincar de vai e volta
                                   C
Arruma um ioiô e desenrola do meu coração


   A                                         Bb
Será que você não percebe o mal que isso me faz?
                                    Gm
Perder tempo brigando é andar pra trás
                                   C
A vida é muito curta pra ficar chorando

[Refrão]

        F
Quer saber?
                   Dm
Já tô desapegando de você
                      Bb9
Já tô me acostumando sem você
                  C
Reaprendendo a viver
        F
Quer saber?
                        Dm
Eu tô morrendo de saudade de você
                         Bb9
Esquece o seu orgulho e vem me ver
        C
Vem me ver

        Bb
Quer saber? Quer saber?
                      F       Bb
Prefiro dar um tempo com você

[Segunda Parte]

 F
Faz isso não
                                   Dm
Não fala desse jeito, não é bem assim
                           Bb9
Até entendo que esteja magoado
                         C
Mas saiba, também não é fácil pra mim

    F                                            Dm
Tentei ficar despreocupado e fingir que eu tava bem
                                                Bb9
Eu estava errado de pensar que um dia outro alguém
                 C
Me daria o seu amor
                Bb9
Pra mim não acabou
        C
Não acabou

[Refrão]

        F
Quer saber?
                           Dm
Eu tô morrendo de saudade de você
                         Bb9
Esquece o seu orgulho e vem me ver
        C
Vem me ver
        F
Quer saber?
                      Dm
Talvez a gente possa conversar
                       Bb9
Quem sabe dessa vez recomeçar
       C
Pra valer

        F
Quer saber?
                           Dm
Eu tô morrendo de saudade de você
                         Bb9
Esquece o seu orgulho e vem me ver
        C
Vem me ver
        F
Quer saber?
                      Dm
Talvez a gente possa conversar
                       Bb9
Quem sabe dessa vez recomeçar
       C  Bb  C
Pra valer

        Bb
Quer saber? Quer saber?
                      F
Eu não consigo viver sem você
        Bb
Quer saber?
                      F
Eu não consigo viver sem você
Dm  C  Bb                      F
         Eu não consigo viver sem você

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bb = X 1 3 3 3 1
Bb9 = X 1 3 3 1 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
