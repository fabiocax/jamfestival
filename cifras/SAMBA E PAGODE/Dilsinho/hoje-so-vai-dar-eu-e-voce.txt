Dilsinho - Hoje Só Vai Dar Eu e Você

F7M       Dm7             Bb7M
Você já sabe do que eu gosto
Bbm6            F7M
Não preciso te dizer
                   Dm7          Bb7M
Desse jeito doce carinhoso bem gostoso
Bbm6    C#°    Dm7
Só você sabe fazer
                 Am7      Bb7M
A gente fica a vontade fecha a porta
Bbm6             F7M
Deixa tudo acontecer
                Dm7
Se tranca na intimidade
   Bb7M
Não tem jeito
Bbm6
Hoje só vai dar eu e você
  F7M       Dm7
É madrugada tá ficando tarde
Bb7M               Bbm6
Deixa eu chamo um táxi pra você

  F7M            Dm7
Se quiser dormir aqui comigo
Bb7M              Bbm6         C#°     Dm7
Não tem perigo te prometo que não vou morder

               Am7
Banheira com hidromassagem
 Bb7M               Bbm6
Tudo nosso com uma pitada de prazer
 F7M       Dm7
Na cama é uma viagem
Bb7M
Não tem jeito, não
Bbm6
Hoje só vai dar eu e você

(REFRÃO)
  F7M       Dm7  Bb7M
Então me ama, me beija
       Bbm6
É só deixar acontecer
  F7M       Dm7           Bb7M
Entre quatro paredes vale tudo
  Bbm6
Hoje só vai dar eu e você (2x)

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Bb7M = X 1 3 2 3 1
Bbm6 = 6 X 5 6 6 X
C#° = X 4 5 3 5 3
Dm7 = X 5 7 5 6 5
F7M = 1 X 2 2 1 X
