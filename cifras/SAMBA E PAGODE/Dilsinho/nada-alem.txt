Dilsinho - Nada Além

E|----------------------------------------------------|
B|-5--5--5----4-4--4-4---3--3--3--3--2--2--2----------|
G|--4--4------4--4--4-----2--2--2----2---2--2---------|
D|---2--2-----4---4--4-----0--0--0---2----2--2--------|
A|----------------------------------------------------|
E|----------------------------------------------------|

E                B9
Eu to bem mais maduro
               D9
Acredita, eu juro
                   A9
Você não vai me reconhecer
E9                      B9
Vai ver passei daquela fase
               D9
Só falo a verdade
                A 25-27-14-14/15/14
Errei mais aprendi a perder

E9
Os amores de antes

B9
Me fizeram ver
     D7+
Sua foto na estante
    A
Não deixei ninguém mexer
  E9
O que era importante
  B
Você levou
                 D9  A/G
E me deixou sem nada

E|-12--12-|
B|---12---|

E
Nada além
                    B9
Nada além do teu sorriso
  D9               A
Nada além me faz viver (yeah yeah)
E                      B9
Nada além do que eu preciso
D9      A9
Nada além

E|-12--12-|
B|---12---|


----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
B = X 2 4 4 4 2
B9 = X 2 4 4 2 2
D7+ = X X 0 2 2 2
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
E9 = 0 2 4 1 0 0
