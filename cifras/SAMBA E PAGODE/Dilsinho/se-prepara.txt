Dilsinho - Se Prepara

A            D/F#           A
Se você chama isso de amor
               D/F#               A    D#7
Com o tempo vai aprender, está errada
                 A    D/F#
De amor não tem nada
A                  D/F#             A
Quem sou eu pra bancar o professor?
                 D/F#             A     D/F#
Deixo a vida ensinar você, se prepara
Bm                  D/F#          E
O troco é certo e vai se arrepender
D/F#  A E         F#m7          D/F#    A
  joga, joga fora toda nossa história
E           F#m7         D/F#  A
Só que se prepara que ela volta
E                  D4/G
Cada vez que machucar e não vai demorar
A    D4/G     D/F#                  E
Chega, não dá, tem que me acostumar
Não aguentei

A     D4/G      D/F#
Já foi vai passar
                    E           E
Vou encontrar alguém pra me amar
Como eu te amei, como eu te amei

( A  D4/G  D/F#  E )

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D#7 = X 6 5 6 4 X
D/F# = 2 X 0 2 3 2
D4/G = 3 X X 2 3 3
E = 0 2 2 1 0 0
F#m7 = 2 X 2 2 2 X
