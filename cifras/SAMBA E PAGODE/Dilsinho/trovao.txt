Dilsinho - Trovão

[Intro]  D9  Bm7  A9  G9

D9
Louco pra te amar
Bm7             G9
Chego pro jantar
       Gm6
E a mesa pra um
D9
Ver você chorar
Bm7            G9
Roupas no sofá
         Gm6
Sem motivo algum
           G9           A7sus4        A/G
Quando eu perguntei: pra onde é que eu ia?
   F#m7       F#m7/11
Desacreditei
         D/C
Foi tão fria
         Bm7
Nem me ouvia

A9    G9     A7sus4
Só dizia vai
                   D   D/C Am7 D7/4/9
E que já não dava mais
     G9
Como não!
Se é separação
               A7sus4                     A/G   F#m7
De onde é o trovão, que anuncia quando a chuva vem
                       Am7               D7/4/9
Hoje eu saí sem me despedir só porque eu achei que estava tudo bem
G7+
  Não me leve a mal
          A7sus4                     A/G   F#m7
Brigas de casal, como as nossas todo mundo tem
                                  Am7
Se é pra te esquecer como eu vou viver
              D7/4/9           G7+
Se eu só tenho você e mais ninguém
                       Em7
Não tenho nada além de amor pra te dar
    Em7/9                      D9 C9 Bb9 A7sus4
Mas trouxe flores se quiser ficar

Final: G7+    A7sus4
          Mas trouxe flores se quiser ficar

----------------- Acordes -----------------
A/G = 3 X 2 2 2 X
A7sus4 = X 0 2 0 3 0
A9 = X 0 2 2 0 0
Am7 = X 0 2 0 1 0
Bb9 = X 1 3 3 1 1
Bm7 = X 2 4 2 3 2
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
D/C = X 3 X 2 3 2
D7/4/9 = X 5 5 5 5 5
D9 = X X 0 2 3 0
Em7 = 0 2 2 0 3 0
Em7/9 = X 7 5 7 7 X
F#m7 = 2 X 2 2 2 X
F#m7/11 = 2 X 2 2 0 X
G7+ = 3 X 4 4 3 X
G9 = 3 X 0 2 0 X
Gm6 = 3 X 2 3 3 X
