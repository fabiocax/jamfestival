Noel Rosa - Palpite Infeliz

         C7M          Cº        C7M
Quem é você, que não sabe o que diz?
            Em7        A7(b9)   Dm7(9)
Meu Deus do céu, que palpite infeliz!
Dm7(9)/C Bm7(b5)  E7(b9)    Am7M(9)
Salve! Estácio, Salgueiro, Mangueira,
        Am7(9)   D7(13)
Oswaldo Cruz e Matriz
  D7(b13)    G7(9)        Dm7 G7(b9)
Que sempre souberam muito bem
      C7M C7 E7(4)  E7 A7(13) A7(b13)
Que a Vila não quer abafar ninguém
          Dm7(b5)      G7(9) G7(b9) C7M
Só quer mostrar que faz sam...ba também!

        G7                        C7M
Fazer poemas lá na Vila é um brinquedo
           C7          C7(13)     F
Ao som do samba, dança até o arvoredo
        Bb7(4)          Bb7
Eu já chamei você pra ver,

        E7(13) E7(b13) A7(9)(11) A7(b9)
Você não viu porque não quis
         Dm7(9)     G7(9)        C6 Bb7M Ab7M G7(9)(11)
Quem é você, que não sabe o que diz?

A Vila é uma cidade independente
Que tira samba, mas não quer tirar patente
Pra que ligar a quem não sabe
Aonde tem o seu nariz?
Quem é você, que não sabe o que diz?

----------------- Acordes -----------------
A7(13) = X 0 X 0 2 2
A7(9)(11) = 5 X 5 4 3 X
A7(b13) = X 0 X 0 2 1
A7(b9) = 5 X 5 3 2 X
Ab7M = 4 X 5 5 4 X
Am7(9) = X X 7 5 8 7
Am7M(9) = 5 3 6 4 X X
Bb7 = X 1 3 1 3 1
Bb7(4) = X 1 3 1 4 1
Bb7M = X 1 3 2 3 1
Bm7(b5) = X 2 3 2 3 X
C6 = 8 X 7 9 8 X
C7 = X 3 2 3 1 X
C7(13) = X 3 X 3 5 5
C7M = X 3 2 0 0 X
Cº = X 3 4 2 4 2
D7(13) = X 5 X 5 7 7
D7(b13) = X 5 X 5 7 6
Dm7 = X 5 7 5 6 5
Dm7(9) = X 5 3 5 5 X
Dm7(b5) = X X 0 1 1 1
E7 = 0 2 2 1 3 0
E7(13) = 0 X 0 1 2 0
E7(4) = 0 2 0 2 0 X
E7(b13) = 0 X 0 1 1 0
E7(b9) = X X 2 1 3 1
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
G7 = 3 5 3 4 3 3
G7(9) = 3 X 3 2 0 X
G7(9)(11) = 3 X 3 2 1 X
G7(b9) = 3 X 3 1 0 X
