Noel Rosa - Tipo zero

Intr.:(F G7 C A7 Dm G7 C C7 F G7 C A7 D7 G7)

C           C/E      B/D#     C/E
  Você é um tipo que não  tem tipo
         C         C#°   Dm
Com todo tipo você se  parece
           E7                      Am
E sendo um tipo que assimila tanto tipo
         D7                        G7
Passou a ser um tipo que ninguém esquece
(Tipo Zero não tem tipo)

     D7/F#  G7  C/E      B/D#     C/E
Você é      um  tipo que não  tem tipo
         C         C#°   Dm
Com todo tipo você se  parece
           E7                      Am
E sendo um tipo que assimila tanto tipo
         D7                        Fm6/Ab  G7
Passou a ser um tipo que ninguém esquece


         F          G7   C   C7
Quando você penetra no salão
        F7              E7
E se mistura com a multidão
         A7               Dm
Esse seu tipo é logo observado
      B7              Em
E admirado todo mundo fica
        F           F#°    C/G
E o seu tipo não se classifica
       A7             D7           G7   C
E você passa a ser um tipo desclassifi..cado


----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
B/D# = X 6 X 4 7 7
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
C#° = X 4 5 3 5 3
C/E = 0 3 2 0 1 0
C/G = 3 3 2 X 1 X
C7 = X 3 2 3 1 X
D7 = X X 0 2 1 2
D7/F# = X X 4 5 3 5
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F#° = 2 X 1 2 1 X
F7 = 1 3 1 2 1 1
Fm6/Ab = 4 5 3 5 3 X
G7 = 3 5 3 4 3 3
