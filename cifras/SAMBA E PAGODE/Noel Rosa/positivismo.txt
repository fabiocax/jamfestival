Noel Rosa - Positivismo

D           A7        D      D7
A verdade meu amor mora num poço
G           D7               G
É Pilatos, lá na Bíblia, quem nos diz
Gm                D      C
E também faleceu por ter pescoço
B      E7
O (infeliz) autor
A7         D      G    D
da guilhotina de Paris
 D7
Vai orgulhosa, querida
G
Mas aceita esta lição
Gm                    D B7
No câmbio incerto da vida
E7         A7      D    G    D
A libra sempre é o coração
  D                A7                 D      D7
O amor vem por princípio, a ordem por base.
G                D7          G
Bis       O progresso é que deve vir por fim

Gm                        D   C  B
Desprezastes esta lei de Augusto Conté
E7          A7         D      G    D
E fostes ser feliz longe de mim
 D7
Vai coração que não vibra
G
Com teu juro exorbitante
Gm                      D B7
Transformar mais outra libra
E7    A7     D    G    D
Em dívida flutuante
   D                A7       D           D7
A intriga nasce num café pequeno
G            D7                      G
Bis       Que se toma para ver quem vai pagar
Gm                             D C B
Para não sentir mais o teu veneno
E7             A7          D        G    D
Foi que eu já resolvi me envenenar

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
