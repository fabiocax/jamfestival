Noel Rosa - Mulata Fuzarqueira

(Introdução do piano com violões seguindo no acompanhamento)

      Dm             Am                      E7                  Am
E|-----------------------|---------------------|-----------------------|
B|-1-3---1-3-1-----------|--1--------1-----0---|--0--------------------|
G|-------------2---------|------2------2-------|----2-1--------2-------|
D|---------------3-2-----|--------2------2-----|-------------2---------|
A|-----------------------|---------------------|-----------------------|
E|-----------------------|---------------------|-----------------------|

(1ª Passagem: fim da introdução)

      Am            E7
E|-----------------------|
B|-----------------------|
G|---0-------------------|
D|-2---3-2----0----------|
A|----------3---3-2------|
E|-----------------------|

Am           E7            Am  E7
Mulata fuzarqueira, artigo raro

     Am              G
Que samba de dar rasteira
             F7               E7
E passa as noite inteira em claro
     Am        C/G       F#7
Não quer mais saber de preparar as gordura
 B7                   E7
Nem usar mais das costura

(2ª Passagem)

          E7
E|----------------------|
B|----------------------|
G|----------------------|
D|----------------------|
A|---0-1-2--------------|
E|-4--------------------|

   Dm               Am
O bom exemplo já te dei
                  E7
Mudei a minha conduta
                 Am
Mas agora me aprumei

(Repetir 1ª Baixaria)

Am          F#7          B7
Mulata fuzarqueira da gamboa
                     F° E7
Só anda com tipo à toa
                     Am
Embarca em qualquer canoa (2x)

(3ª Passagem)

               E7
E|----------------------|
B|----------------------|
G|----------------------|
D|----------------------|
A|-------0-1-2----------|
E|-0-2-4----------------|

Am            E7             Am
Mulata, vou contar as minhas mágoa
                 G
Meu amor não tem R
       F7           E7
Mas é amor debaixo d'água
     Am        C/G            F#7
Não gosto de te ver sempre a fazer certos papel
 B7                  E7
A se passar pros coronel

 Dm                   Am
Nasceste com uma boa sina
                      E7
Se hoje andas bem no luxo
                  Am
É passando a beiçolina

(Repetir 1ª Passagem)

Am             E7          Am
Mulata, tu tem que te preparar
                G
Pra receber o azar
           F7          E7
Que algum dia há de chegar
  Am          C/G         F#7
Aceita o meu braço e vem entrar nas comida
B7                 E7
Pra começar outra vida

 Dm                    Am
Comigo tu podes viver bem
                     E7
Pois aonde um passa fome
                     Am
Dois podem passar também

(4ª Passagem)

               Am
E|----------------------|
B|----------------------|
G|----------------------|
D|----------------------|
A|-------0-2-4----------|
E|-0-2-4----------------|

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
C/G = 3 3 2 X 1 X
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
F#7 = 2 4 2 3 2 2
F7 = 1 3 1 2 1 1
F° = X X 3 4 3 4
G = 3 2 0 0 0 3
