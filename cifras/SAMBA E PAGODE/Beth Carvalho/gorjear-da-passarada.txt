Beth Carvalho - Gorjear da Passarada

Eb       Bb7        Eb
O dia vem raiando
%      Eb          C7           Fm      C7
Trazendo o gorjear da passarada
Fm    Fm7+    Fm7     Fm6
E eu sentado, meditando
   Bb7           %        Eb      Bb7
Com a alma amargurada
Eb      Bb7   Eb    %
E o rouxinol,
Bbm           Eb7                Ab        %
Com o seu canto a me aconselhar
Abm          Db7              Gm
Pra que tanto sofrimento
C7         Fm          Bb7           Bbm    Eb7
Por alguém que não lhe soube amar
Ab           Abm                Gm-Db7
Pra que tanto sofrimento
C7         Fm          Bb7           Eb
Por alguém que não lhe soube amar


Gm-Gbm    Fm         Bb7      Eb        C7
O colibri com o seu jeito mavioso
Fm               Bb7
Sugando o mel das flores
Gm              C7
De mim se aproximou
Fm
E disse:
Bb7    Gm                 C7
É fácil registrar no seu olhar
Fm     Bb7                        Eb          Bb7
Que aquela ingrata não lhe soube amar.

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Abm = 4 6 6 4 4 4
Bb7 = X 1 3 1 3 1
Bbm = X 1 3 3 2 1
C7 = X 3 2 3 1 X
Db7 = X 4 3 4 2 X
Eb = X 6 5 3 4 3
Eb7 = X 6 5 6 4 X
Fm = 1 3 3 1 1 1
Fm6 = 1 X 0 1 1 X
Fm7 = 1 X 1 1 1 X
Fm7+ = 1 X 1 1 1 X
Gm = 3 5 5 3 3 3
