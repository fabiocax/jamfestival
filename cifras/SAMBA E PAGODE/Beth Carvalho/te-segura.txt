Beth Carvalho - Te Segura

  Ab   Ab7        Db    Eb7  Ab
Te segura, te segura, te segura
              F7         Bbm
Que a vida tá dura, te segura
Eb7  Ab
te segura


F7                Bbm
Fala pro bom português
                Eb7
Que no final do mês
              Ab
Eu acerto a fatura
                  Ebm
Compra feijão e farinha
           Ab7
E uma partezinha
          Db
Só de rapadura


Pega no bolso da calça
                Eb7
Dois contos pra salsa
            Ab
Legume e verdura
     F7                  Bbm
E do que te sobrar desse troco
                Eb7
Tu me guarda um pouco
              Ab
Que a vida tá dura


  Ab   Ab7        Db    Eb7  Ab
Te segura, te segura, te segura
              F7         Bbm
Que a vida tá dura, te segura
Eb7  Ab
te segura


F7                   Bbm
Quando por menos não seja
              Eb7
Melhor que cerveja
             Ab
É tomar cana pura
                  Ebm
Não posso beber gelado
              Ab7
E o meu resfriado
              Db
cachaça é que cura

É como diz o ditado
             Eb7
Livro encadernado
                Ab
Por dentro é brochura
       F7               Bbm
Compra mesmo Cardoso Gouveia
           Eb7
Manera tenteia
              Ab
Que a vida tá dura


  Ab   Ab7        Db    Eb7  Ab
Te segura, te segura, te segura
              F7         Bbm
Que a vida tá dura, te segura
Eb7  Ab
te segura


F7              Bbm
Mente pra tua coroa
               Eb7
Que eu tô numa boa
            Ab
Mas fiz uma jura
                  Ebm
Quero subir pra colina
             Ab7
Por causa do clima
            Db
E da temperatura

Diz que eu não me dava bem
                 Eb7
Com o barulho do trem
                Ab
Parando em Cascadura
        F7                  Bbm
Mas não diz que na purga da mora
              Eb7
Fui posto pra fora
              Ab
Que a vida tá dura


  Ab   Ab7        Db    Eb7  Ab
Te segura, te segura, te segura
              F7         Bbm
Que a vida tá dura, te segura
Eb7  Ab
te segura

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Ab7 = 4 6 4 5 4 4
Bbm = X 1 3 3 2 1
Db = X 4 6 6 6 4
Eb7 = X 6 5 6 4 X
Ebm = X X 1 3 4 2
F7 = 1 3 1 2 1 1
