Beth Carvalho - Coração Poeta

Intro:C C#º G7+ E7 Eb7 D7 G7+ D7

G7+                      Bm5-/7    E7
Deus me deu uma coração poe........ta
Bm5-/7       E7             Am     E7
E a alma inquieta de um cantor
Am                         Eb7  D7
Pra que eu vigiasse a madruga....da
 G7+          A#º         Am       D7
E acordasse o Sol e o beija-flor

   G7+           C
Cantar... me  faz
   B7           Em     G7
Viver bem mais
    C      C#º        G7+             E7
Soltar a voz que nem um passarinho
Eb7               D7      G7+    G#º
Que ninguém prenderá jamais

      Am      D7    Bm     A#º
Se sou feliz    ou infeliz

       Am7              D7
São lindas minhas penas
           G7+
Vale a pena ser quem sou
           F#m5-/7  B7       Em         F#7/9
Se eu  tenho   o  céu   aqui   no   chão
     B7+   C#m7   Bm      D7/9
Se tenho mel no   co  ra  ção

G7+                     Bm5-/7    E7
Deus me deu uma coração poe....ta....

----------------- Acordes -----------------
A#º = X 1 2 0 2 0
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
B7+ = X 2 4 3 4 2
Bm = X 2 4 4 3 2
Bm5-/7 = X 2 3 2 3 X
C = X 3 2 0 1 0
C#m7 = X 4 6 4 5 4
C#º = X 4 5 3 5 3
D7 = X X 0 2 1 2
D7/9 = X 5 4 5 5 X
E7 = 0 2 2 1 3 0
Eb7 = X 6 5 6 4 X
Em = 0 2 2 0 0 0
F#7/9 = X X 4 3 5 4
F#m5-/7 = 2 X 2 2 1 X
G#º = 4 X 3 4 3 X
G7 = 3 5 3 4 3 3
G7+ = 3 X 4 4 3 X
