Beth Carvalho - Nas Veias do Brasil

A                    F#7              Bm                               E7
O   o   o   o   O   o   o   o   O   o   o   o                   o   o   o   o

 A                   E7                       A          E7              Am
O   o   o   o   O   o   o   o    Brasil,   os negros,  os   negros

                                                       E7            Am                          Dm
Trazidos lá do além   mar Vieram para espalhar suas coisas trasncendentais

                              F              E7  Dm                 F    E7                       A7
Repeito ao céu, a terra e ao mar, ao índio veio juntar, o amor a liberdade


Dm                          Am                        Dm            E7              A7

A força de um baoba  tanta luz no pensar veio de lá a criatividade

Dm                           Am                         Dm          E7               Am        E7

A força de um baobá tanta luz no pensar veio de lá a criatividade


A                                                                                     A/C#           F#7      Bm
Tantos o preto velho já curou, e a mãe preta amamentou tem alma negra o povo

                                         E7                                                    A            A7
Os sonhos tirados do fogão a magia da canção o carnaval é fo...go

     D                                                       C#m           F#7
O samba corre nas veias dessa pátria mãe gentil

                         Bm                                     E7                                       A     A7
É preciso a atitude pra de assumir a negritude pra ser muito mais Brasil

     D                Bm             E7               C#m             F#7
O samba corre nas veias dessa pátria mãe gentil

                          Bm                               E7                           A
È preciso a atitude de assumir a negritude pra ser muito mais Brasil

E7                   A                         Bm                 E7                  A                  E7
O   o   o   o    O   o   o   o         O   o   o   o      O   o   o   o    O   o   o   o    O   o   o   o

       A
Brasil................

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
F#7 = 2 4 2 3 2 2
