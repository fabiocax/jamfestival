Beth Carvalho - Clara

Obs: Essa música é uma homenagem à CLARA NUNES.

(De: Francis Hime & Geraldo Carneiro)

Intr.: Em7 / Fº / Bm7/F# / G6/9 / C#7(b13) C#7 F#7(b9) F#7/4(b9) B7/4(9) / B7(b9) /
       Em7 / Fº / Bm7/F# / G6/9 / C#7(b13) C#7 F#7(b9) F#7/4(b9)

Bm7                        B7/4(b9)  Bm7                      B7/4(9) B7(b9)
Luz: uma luz que se acendeu          Fogo: era o fogo de Xangô
     Em7(9)          A7(9)         D7M(9)       D/C            Bm6/D
Estre______la que um dia  despencou       Das esferas lá do céu
    C#7           C#m7(b5) C#m7(b5/9) F#7(b13) F#7
E caiu na minha vi____________________da
Bm7                     B7/4(b9)  Bm7                    B7/4(9) B7(b9)
Não: era uma constelação          Era a paixão de clarear
     Em7(9)     A7(9)        D7M(9)       D/C       Bm6/D
A cla______ra magia  de viver       Diz o mar ylêaiê
       C#7             C#m7(b5) C#m7(b5/9) F#7(b13) F#7
Tantas coisas diz o mar

     C#7(13) C#7(b13) F#7/4(9) F#7(b9)    Bm7                        B7/4(9) B7(b9)
É Cla_________________ra,             sere___ia Que navega noutro mar

     Em7(9)                      A7/4(9)  A7(9)       Gm6/D   D7M
Vague______ia Pela noite, pelo ar         Me   incende_____ia
         G7M           G#m7(b5)  C#7(b9)      G7 F#7
Porque o fogo de cantar          Não    se apa___ga
     C#7(b13) C#7 F#7/4(b9) F#7(b9)      Bm7                       B7/4(9) B7(b9)
É Cla_____________ra,              prince___sa No país da escuridão
   Em7(9)                        A7/4(9)  A7(9)     Gm6/D   D7M
Ace______sa Feito luz de incendia_________da   certe_____za
        G7M          G#m7(b5)  C#7(b9)       G7  F#7  F#7/4     B7/4(9) B7(b9)
Que a mania de sonhar          Não,   não vai    se  a_____cabar

     Em7  Fº        Bm7/F#  G6/9      C#7(b13) C#7 F#7(b9) F#7/4(b9)     B7/4(9) B7(b9)
E Cla_____ra A estre________la   Rebri_____________lha              no ar
       Em7  Fº       Bm7/F#  G6/9        C#7(b13) C#7 F#7(b9) F#7/4(b9)     B7/4(9) B7(b9)
A estre_____la De Cla________ra   Não vai             se      a________pagar
     Em7  Fº        Bm7/F#  G6/9      C#7(b13) C#7 F#7(b9) F#7/4(b9)     B7/4(9) B7(b9)
E Cla_____ra A estre________la   Rebri_____________lha              no ar
       Em7  Fº       Bm7/F#  G6/9        C#7(b13) C#7 F#7(b9) F#7/4(b9)     Bm7
A estre_____la De Cla________ra   Não vai             se      a________pagar

ACORDES:

Em7       - 0X000X     Bm6/D      - XX0102
Fº        - 1X010X     C#m7(b5)   - X4545X
Bm7/F#    - 2X020X     C#m7(b5/9) - X4544X
G6/9      - 3X0200     F#7(b13)   - 2X233X
C#7(b13)  - X4X465     F#7        - 2X232X
C#7       - X4X464     C#7(13)    - X4X466
F#7(b9)   - XX4353     C#7(b13)   - X4X465
F#7/4(b9) - XX4453     F#7/4(9)   - XX4454
B7/4(9)   - X2222X     A7/4(9)    - X0543X
B7(b9)    - X2121X     Gm6/D      - XX0353
Bm7       - X2X232     D7M        - XX0222
B7/4(b9)  - X2221X     G7M        - 3X443X
Em7(9)    - 0XX032     G#m7(b5)   - 4X443X
A7(9)     - X0542X     C#7(b9)    - X4343X
D7M(9)    - X5465X     G7         - 3X343X
D/C       - X3X232     F#7/4      - 2X242X

----------------- Acordes -----------------
A7(9) = 5 X 5 4 2 X
A7/4(9) = 5 X 5 4 3 X
B7(b9) = X 2 1 2 1 X
B7/4(9) = X 2 2 2 2 2
B7/4(b9) = 4 X 4 3 1 1
Bm6/D = X X 0 1 0 2
Bm7 = X 2 4 2 3 2
Bm7/F# = X X 4 4 3 5
C#7 = X 4 3 4 2 X
C#7(13) = X 4 X 4 6 6
C#7(b13) = X 4 X 4 6 5
C#7(b9) = X 4 3 4 3 X
C#m7(b5) = X 4 5 4 5 X
C#m7(b5/9) = P9 10 13 9 X X
D/C = X 3 X 2 3 2
D7M = X X 0 2 2 2
D7M(9) = X 5 4 6 5 X
Em7 = 0 2 2 0 3 0
Em7(9) = X 7 5 7 7 X
F#7 = 2 4 2 3 2 2
F#7(b13) = 2 X 2 3 3 2
F#7(b9) = X X 4 3 5 3
F#7/4 = 2 4 2 4 2 X
F#7/4(9) = 2 X 2 1 0 X
F#7/4(b9) = 2 X 2 0 0 X
Fº = X X 3 4 3 4
G#m7(b5) = 4 X 4 4 3 X
G6/9 = X X 5 4 5 5
G7 = 3 5 3 4 3 3
G7M = 3 X 4 4 3 X
Gm6/D = X 5 5 3 5 X
