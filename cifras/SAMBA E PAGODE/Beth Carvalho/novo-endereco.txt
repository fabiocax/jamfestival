Beth Carvalho - Novo Endereço

Introdução: A7+ / Am6 / G#m7 / C#7 / F#m7 / B7 / E / B7 /
    E           B7     E
Se por acaso alguém perguntar por mim    }
C#7                  F#m7   C#7
Diga que mudei de endereço               }    REFRÃO
    F#m7             C#7    F#m7
To morando na rua da felicidade          }
    B7                 E
Que faz esquina com recomeço             }
         B7
Se por acaso!
    C#7          F#m7      B7            E
Já morei em rua errada, enfrentei muito tropeço
                  Bm7
Mas encontrei uma estrada
       E7/9            A7+
Que me trouxe um novo apreço
                             Am6           E
Hoje vivo mais feliz e desse mal já não padeço
       C#7         F#7/9     B7            E    B7
Um endereço é felicidade, esquina com recomeço

REFRÃO
    C#7        F#m7           B7            E
Mas superei a fase, já faz tempo, até me esqueço
                 Bm7
Foram meses de infortúnio
      E7/9      A7+
Desse mal quase faleço
                              Am6           E
Minha vida é só ternura, viver mal eu não mereço
      C#7           F#7/9     B7           E   B7
Meu endereço é felicidade, esquina com recomeço
REFRÃO

----------------- Acordes -----------------
A7+ = X 0 2 1 2 0
Am6 = 5 X 4 5 5 X
B7 = X 2 1 2 0 2
Bm7 = X 2 4 2 3 2
C#7 = X 4 3 4 2 X
E = 0 2 2 1 0 0
E7/9 = X X 2 1 3 2
F#7/9 = X X 4 3 5 4
F#m7 = 2 X 2 2 2 X
G#m7 = 4 X 4 4 4 X
