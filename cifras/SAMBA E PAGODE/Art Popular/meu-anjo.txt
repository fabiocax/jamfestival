Art Popular - Meu Anjo

Intro: Em  D  C  Am7  B7

Solo: 27(t)  28  39  28  17  17  15  15  28  27  28  39  27  28  200  17  18  17  15  17  28  39  27  28  200  17  18  100  102  102  100  100  100  18  18  18(t)  17  17  16  15

          Em
Meu anjo vá buscar
            D
Aquela que fugiu de mim
          C       Am7
A noite acabou assim
                  B7
Um lobo caçando paixão

          Em
Meu anjo vá buscar
             D
Meu mal meu bem a tentação
                   C     Am7
Nos quatro cantos do salão
                   B7
Convida ela pra dançar


  Em                E7                Am7
Volta e lembra da nossa história a revolta
                             D7
Suas broncas tão cheias de glória
                       G
de tudo eu sinto sua falta
  B7        Em
De tudo, em tudo
                   Am7
Ouvir o que você falava
         B7    ( 63  50  52  53  52  63  60 )
Volta, volta

 Em        E7             Am7     D7
Volta pra mim a música é nossa
                              G
Volta pra mim romântica ou valsa
    B7                Em
A nossa letra, nossa história
                            Am
Volta pra mim, sambar agarradinho
         B7
Volta, volta
   Em
Meu anjo

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
