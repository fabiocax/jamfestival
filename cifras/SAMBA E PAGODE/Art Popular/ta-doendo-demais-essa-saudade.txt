Art Popular - Tá doendo demais essa saudade

______________________Refrão_________________________________
 G         D/F#          Em7
Ta doendo demais essa saudade
C9              D7            G            F# ^ G
To com medo do mel dessa paixão
G               D/F#            Em7
Mas quem não vai buscar felicidade
C9                         D7
Não ensina o caminho ao coração
____________________________________Refrão_______________

G                                     D7
Foi num samba de roda que eu vi teu olhar
Descendo a ladeira
G                              D7
Fazendo zueira pra me  conquistar
De qualquer maneira
G                           D7
O amor capoeira não pode jogar
Que leva rasteira
G                               D7
Feitiço é paixão não é facil curar

             G       G7
Nem com resadeira
C9
Eu cai feito caça na sua armadilha nas garras do amor
G
Foi com quem eu brincava gostava da fama de ser caçador
D7
fui beber do veneno gostoso em teus olhos pra me apaixonar
  G                                              G7
Hoje eu morro de medo que um dia o brinquedo me faça chorar
C9
A saudade é malvada mas eu não dou mole pra ela ficar
G
Coração ta sofrendo o amor ta doendo mas vou te buscar
D7
Não me tira da vida paixão tão bonita não pode acabar
Nem de brincadeira
G                             D7
Essa tal saudade vem me machucar
Não vou dar bobeira
G                                D7
Ta doida ta doida pra me ver chorar
Feito cachoeira
G                                D7
Mas hoje é domingo eu preciso sambar
Só segunda feira
G                             D7
A paixão é pimenta de bom paladar
Quando é verdadeira
 volta no Refrão

----------------- Acordes -----------------
C9 = X 3 5 5 3 3
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
Em7 = 0 2 2 0 3 0
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
