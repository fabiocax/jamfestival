Art Popular - Nova Era

G                     D7/F#
Dia de sol a saudade aperta
            Ebº     Em7
Desejo, a imagem desperta
                                Am7   D7/9
Um olhar, um sorriso, o vento levou ( levou )

G                        D7/F#
Fico sonhando com a nova era
            Ebº        Em7
Tentando te ver nas estrelas
                            Am7     D7  Ebº
Caçando a hora o momento de ter...  êêêêê...

Em7                                 Bm7
Ando de um lado pro outro e ligo a TV
           C     Am7         D7/9
Onde está você, preciso lhe dizer
    Ebº
Te amo


Em7                               Bm7
Toda essa nossa paixão é difícil esquecer
            C     Am7       D7/9
Onde está você, preciso lhe dizer
   D7(4/9)
Que tudo é diferente
 G      C           Am7   D7/9
Com a gente, com a gente
G      C           Am7   D7/9
Com a gente, com a gente

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
D7 = X X 0 2 1 2
D7(4/9) = X 5 5 5 5 5
D7/9 = X 5 4 5 5 X
D7/F# = X X 4 5 3 5
Ebº = X X 1 2 1 2
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
