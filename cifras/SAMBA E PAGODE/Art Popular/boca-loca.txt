Art Popular - Boca Loca

Am
Oh lábios de mel
            Dm
Da linda menina
            F7
Quando eu te vejo
             E7  Am    E7
O desejo me alucina

          Am
Quero te catar
                             Dm
Quero me enrolar neste teu calor
             F7
Quero te apertar
                E7
Quero me explicar
           Am   E7  Am
dentro do amor
        E7       Am
Seu jeito tem beleza

                Dm
Tem um sorriso meigo
             F7
De uma boca loca
        E7     Am   E7 Am
Que eu beijo , beijo....
        E7   Am
Contigo numa boa
               Dm
Sigo em alto astral
                         F7
Carregado de amor, de alegria
    E7                       Am   E7  Am
Sou folia, sou rei, sou carnaval.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
F7 = 1 3 1 2 1 1
