Art Popular - Sai da Frente

De: Leandro Lehart


Am                                 G
Amor, eu não consigo viver mais sozinho, amor
              Am
Preciso de você
                                G
Do seu amor e de um pouco de carinho, amor
                Am
Tô querendo te ver

                                  E7
Preciso de sambar, preciso de sambar
                         Am
Samba assim com Art Popular
                                  E7
Preciso de sambar, preciso de sambar, Iaiá

Ah, iaiá


  Am                                      G
Lá, na ladeira,  samba assim com Art Popular
                               Am
Na ladeira,  é diferente balançar
                                      G
Na ladeira,  ginga com o swing desse bar
                                  Am
Na ladeira,   é diferente o balançar

                             G       F
Eu não consigo viver mais sozinho, amor
                            E7       Am
Eu não consigo viver mais sozinho amor
     G       F
Ô ô ô ô,ô ô ô ô,  sai da frente
           E7                                             Am
Que eu preciso de sambar, de sambar, e de sambar e de sambar

----------------- Acordes -----------------
Am = X 0 2 2 1 0
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
