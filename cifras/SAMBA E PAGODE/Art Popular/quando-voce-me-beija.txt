Art Popular - Quando Você Me Beija

Intro: E A7+ C#m7 D7 F#m G B7

            E                          F#m
Um beijo durante aquele amor e meu coração estava lá
      B7                      E              B7
Na verdade esse passado nunca vai me libertar
      E                           F#m
Muita gente não se envolve só por medo de sofrer
       B7                       E
Mas no caso desse acaso só quem vive pra entender

     C#m                       F#m          B7
Entender o que é o amor eu não sei te explicar
                E            F#m    B7       E
Quando você me beija, você me beija, você me beija

  F#m             B7        E
À noite a nossa estrela faz você brilhar
   F#m      B7        E
Em todas avenidas vou te encontrar
          G#7/4         G#7    C#7/11   C#m
Como a história de um pobre e a princesa

                 A                B7
Mas eu quero que seja, eu quero que seja

       E                       C#m
Nossos sonhos de amor, nosso gesto de amor
        B4(7/9)                          E
Que me balança e me desperta sempre, sempre mais
         B4(7/9)    Cº           C#m
Quando você me beija, você me beija
              B7                 E
Quando você me beija, você me beija

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7+ = X 0 2 1 2 0
B4(7/9) = 7 X 7 6 5 X
B7 = X 2 1 2 0 2
C#7/11 = X 4 6 4 7 4
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
Cº = X 3 4 2 4 2
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G#7 = 4 6 4 5 4 4
G#7/4 = 4 6 4 6 4 X
