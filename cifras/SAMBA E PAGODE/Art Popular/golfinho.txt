Art Popular - Golfinho

Intro:  E7+(9) A7+

  E7+(9)
Vem me dar um beijo
                         G#m
Tô doidinho pra te namorar
    A7+                    F#m
Remo de canoa ou de barquinho
              Bsus B7(13) B7(-5)
É só você deixar
    E7+(9)
E passo a passo de mãos dadas
                       G#m
Vamos juntos a beira do mar
                   A7+
Que a nossa ilha está deserta
        F#m                  Bsus
E nos espera pra gente se amar
          Am
E no balanço
                 D79
Balancinho desse mar

          G7+
Te amo demais
   A#°
Só falta você me olhar
       Bsus
Pra gente se emocionar
      A7+
Me leva
   A#°             E7+(9)
Vou com você aonde for
 G#°                F#m
Quero chamar de meu amor
    Bsus              Bm
E a vida inteira navegar
                      E79
Eu pra você, você pra mim
    A7+
Me leva
   A#°                   E7+(9)   B C#m B A7+
Destino me fez te encon..............trar
   Bsus                Bm
E a vida inteira navegar
                      E79
Eu pra você, você pra mim
(Bsus, Bm, E79)
Um barquinho, um golfinho, amorzinho
E a vida inteira navegar

----------------- Acordes -----------------
A#° = X 1 2 0 2 0
A7+ = X 0 2 1 2 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
B7(-5) = X 2 3 2 4 X
B7(13) = X 2 X 2 4 4
Bm = X 2 4 4 3 2
Bsus = X 2 4 4 4 2
C#m = X 4 6 6 5 4
D79 = X 5 4 5 5 X
E7+(9) = X 7 6 8 7 X
E79 = X X 2 1 3 2
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
G#° = 4 X 3 4 3 X
G7+ = 3 X 4 4 3 X
