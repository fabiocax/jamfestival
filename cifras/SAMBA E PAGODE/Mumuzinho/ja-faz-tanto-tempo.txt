Mumuzinho - Já Faz Tanto Tempo

Introdução:

Dm7(9)                          Gm7(11)
Já faz tanto tempo e ainda esta linda como eu te conheci,
Dm7(9)                   Gm7(11)                    A7(#5)
É até dificil acreditar, sabe que eu jamais te esqueci,
           F7+(9)                        Bb7+(9)
Você pra mim, é mais do que desejo é emoção
                             F7+(9)
Primeiro amor que vem do coração,
                              G/F        C/Bb
Eu tenho com você milhões de sonhos, sem fim
            F7+(9)       Gm           Am            Bb7+(9)
Teu jeito assim, é o ponto que desperta o meu prazer,
                            F7+(9)
É dez da juventude o meu querer,
Gm             Am             G/F    C/Bb
Agora que eu te tenho não vou te perder

Refrão:


          F7+(9)                                  Bb7+(9)
Da pra sentir, que rola um sentimento no teu beijo,
                                Dm7(9)
Que quer se entregar mais ta com medo,
 C  Dm        Bb    A7(#5)          F7+(9)
E agora, só quer ficar, me faz feliz,
                            Bb7+(9)
Assume que no fundo o teu desejo,                   (Bb C7)
                               Dm7(9)     Gm Am    G
Só quer ficar comigo o tempo inteiro e agora me namorar ..

----------------- Acordes -----------------
A7(#5) = X 0 X 0 2 1
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bb7+(9) = X 1 0 2 1 X
C = X 3 2 0 1 0
C/Bb = X 1 2 0 1 X
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
Dm7(9) = X 5 3 5 5 X
F7+(9) = X 8 7 9 8 X
G = 3 2 0 0 0 3
G/F = 1 X X 0 0 3
Gm = 3 5 5 3 3 3
Gm7(11) = 3 X 3 3 1 X
