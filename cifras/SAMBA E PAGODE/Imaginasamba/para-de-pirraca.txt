Imaginasamba - Pára de Pirraça

Introdução: D7M /  F#m7/9  /  Bm7/b5  /  Em7 A7

Solo: 12 23 32  -  23 22 32  -  12 23 32  -  44 30 32

D7M        F#m7/9
  Pra ter felicidade
G7M           A7
Preciso de você ao lado meu
D7M             F#m7/9
  Esqueça essa amizade
G7M                C#m7/b5 ____ F#7(#5)
Por que o amor que sinto é mais forte que eu
Bm        Bm7M
  Já não consigo negar
G7M      F#m7/9
Reflete no meu olhar
    Em7    F#m7/9         G7M        A7
O amor verdadeiro que eu tenho pra te dar
D7M          F#m7/9
 Confesso fiz de tudo
G7M           A7
Pra achar uma nova paixão mas não deu

D7M             F#m7/9
 No fundo é um absurdo
G7M         C#m7/b5 __ F#7(#5)
Apagar o destino que nos escolheu
Bm         Bm7M
  E tá traçado pra mim
G7M           F#m7/9
 Como uma História sem fim
      Em7         F#m7/9      G7M
E por mais que eu tente não consigo
 A7         Am7/9      D7
(Te esquecer , te esquecer)
G7M              G6           F#m7/9 Am7/9 __ D7
Então porque não para com esse jogo
      G7M     F#7(#5)          Bm - Bbm - Am - D7
Se estamos separados um do outro
       G7M   G6     A#7          A7
É tão ruim ......... tanto pra você quanto pra mim

D7M    D7    G7M     G6        F#m7/9
 Para de pirraça vem ficar comigo
       Bm7  Em7          A7         D7M
Eu não vejo graça de ser só seu amigo
     D7          G7M        G6          F#m7/9 Bm7
Esse lance já passou tá na hora de crescer
    Em7 _ F#m7 _ G7M ____ A7 ____ D7M
Se toca e nota que eu quero você

----------------- Acordes -----------------
A#7 = X 1 3 1 3 1
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Am7/9 = X X 7 5 8 7
Bbm = X 1 3 3 2 1
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
Bm7/b5 = X 2 3 2 3 X
Bm7M = X 2 4 3 3 2
C#m7/b5 = X 4 5 4 5 X
D = X X 0 2 3 2
D7 = X X 0 2 1 2
D7M = X X 0 2 2 2
Em7 = 0 2 2 0 3 0
F#7(#5) = 2 X 2 3 3 2
F#m7 = 2 X 2 2 2 X
F#m7/9 = X X 4 2 5 4
G6 = 3 X 2 4 3 X
G7M = 3 X 4 4 3 X
