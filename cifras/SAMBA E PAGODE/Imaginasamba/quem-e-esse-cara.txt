Imaginasamba - Quem É Esse Cara

E
Introdução: A7M ^ E/G# ^ F#m7 / F#m7 / C7M ^Bm7 ^Am7 / Am7 / B7/4 / %


        Eadd9     E(#5)
Quem é esse cara,
          A7M              A7M^B/E           F#m7(9)    B7/4              Eadd9    / B7/4
Que vive ameaçando nós dois        vê se não reponde depois, tem que ser agora
       Eadd9      E(#5)
Quem é esse cara
         A7M      A7M^B/E            F#m7(9)      B7/4            Eadd9   / E4/D
Que insiste em te procurar         diz pra ele não te ligar,   isso me apavora
    A7M    A6   B4/A                 F#m/B      E7M(9)      C#m7(11)
Quer saber        confio no amor da gente mas assim já é demais
D7M               A/B           Eadd9         D/E
Alguém que chega de repente pra tirar a nossa paz
A7M    A6   B4/A                 F#m/B                 E7M(9)      C#m7(11)
É melhor         você me dizer a verdade pra que eu possa me acalmar
C7M        A/B            Eadd9
Diz que ninguém vai nos separar
  B4 ^ A/B     Eadd9               B/D#          C#m7
Mas o nosso amor, ainda está correndo risco e você sabe

         G#m7                  A7M
Você precisa muito dizer a verdade
         E/G#               F#m7
Se o seu coração ainda é só meu
                A/B ^ B/A   G#m7^C#m7
Ou é de algum novato que apareceu
F#m7(11) ^A/B   Eadd9         B/D#                C#m7
       Diga  por   favor e tira essa angústia do meu coração
           G#m7                A7M
Não quero me acostumar com a solidão                                    VOLTA AO INÍCIO
   G#m7  C#m7^D7M                    A7M/B          (A7M^E/G#^F#m7/B / F#m7/B)
Olha pra mim       e       diz, será que eu não te faço mais feliz.

FINAL
             G#m7  C#m7^D7M                   A7M/B
Olha pra mim       e       diz, será que eu não te faço mais feliz
A7M ^ E/G# / C7M ^ F#m7/B / Eadd9   / F#m7/B / Eadd9
                                                       "Quem é esse cara..."

----------------- Acordes -----------------
A/B = X 2 2 2 2 X
A6 = 5 X 4 6 5 X
A7M = X 0 2 1 2 0
A7M/B = 7 X 6 6 5 5
Am7 = X 0 2 0 1 0
B/A = X 0 4 4 4 X
B/D# = X 6 X 4 7 7
B/E = X 7 9 8 7 7
B4 = X 2 4 4 5 2
B4/A = 5 X X 4 5 7
B7/4 = X 2 4 2 5 2
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
C#m7(11) = X 4 4 4 5 4
C7M = X 3 2 0 0 X
D/E = X X 2 2 3 2
D7M = X X 0 2 2 2
E = 0 2 2 1 0 0
E(#5) = 0 X 2 1 1 0
E/G# = 4 X 2 4 5 X
E4/D = X 5 2 2 0 0
E7M(9) = X 7 6 8 7 X
Eadd9 = 0 2 4 1 0 0
F#m/B = X 2 4 2 2 2
F#m7 = 2 X 2 2 2 X
F#m7(11) = 2 X 2 2 0 X
F#m7(9) = X X 4 2 5 4
F#m7/B = X 2 2 2 2 2
G#m7 = 4 X 4 4 4 X
