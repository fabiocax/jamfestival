Imaginasamba - Jogo do Amor

Introdução:  E

 E           A7+         E              A7+       G7
Nada que eu possa fazer pra esse jogo do amor reverter
C#m    G#m         A7+         G#m  F#m         G#m   Am
eu e você somos um nessa tal relação e o meu coração quer
E7     D7   G G6          G7/4    G7       C      F#m
pagar pra ver se você acertar essa tal decisão pro nosso
B7   E             Bm       E7        A7+             G#7
coração pra que evitar se existe um lugar pra nos dois não
             C#m        F#m G#m A7+ B7
é sonho real tão lindo
Refrão{
E         F#m          G#m   E7            A7+      G#m
Não sei se vou suportar eu quero te encontrar e pro mundo
      F#m    B7
dizer eu te amo
E        F#m      G#m    E7         A7+
E quando amanhecer você vai responder eu
B7
também te amo

----------------- Acordes -----------------
A7+ = X 0 2 1 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G#7 = 4 6 4 5 4 4
G#m = 4 6 6 4 4 4
G6 = 3 X 2 4 3 X
G7 = 3 5 3 4 3 3
G7/4 = 3 5 3 5 3 X
