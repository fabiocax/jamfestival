Imaginasamba - Deixa Em Off

Intro: E7+/9  E7+/9  G#m7/9  F#4

     B7sus
Você sabe
               B7sus/G
Que é proibido, sabe
                      B7sus/E
Tenho um compromisso, sabe
B/Eb           C#m7         F#4
Eu não posso ir mais além
B7+
Você sabe
                  G#m7/9
Que ficar comigo, sabe
      Ebm/F#     E7+/9
Sempre escondido, sabe
B/Eb         C#m7/11         F#4
É desejo meu também
    Ebm7              G#m7
Tá confundindo o sentimento
E6/9                          F#4
Quer cobrar demais do meu amor

     Ebm7           G#m7
Eu tenho um relacionamento
 E7+/9                       F#4
Machucado pelo desamor
      Ebm7                   G#m7
Mas ela é tudo que eu preciso
 Ebm7            G#m7
E você sempre soube disso
E7M(9)                  C#m7/11          F#4
Desde quando o nosso caso começou
         E7+/9
Deixa eu te amar do meu jeito
                F#4(6) ( melhor na 9ºcasa)
O nosso lance é perfeito
        B7+         F#/Bb
Meu bem, se alguém nos ver
    G#m7                 F#m7  B7/9
Bye-Bye eu e você
              E7+/9
Deixa eu ficar do seu lado
          F#4(6)
Puro sabor do pecado
  B7+      F#/Bb               G#m7         F#4
Ninguém pode saber, assim que deve ser
      E7+/9         F#7(13)       B7+
Deixa em off ou então vai me perder

----------------- Acordes -----------------
B/Eb = X 6 X 4 7 7
B7+ = X 2 4 3 4 2
B7/9 = X 2 1 2 2 X
B7sus = X 2 1 2 0 2
B7sus/E = 0 2 1 2 0 X
C#m7 = X 4 6 4 5 4
C#m7/11 = X 4 4 4 5 4
E6/9 = X 7 6 6 7 7
E7+/9 = X 7 6 8 7 X
E7M(9) = X 7 6 8 7 X
Ebm/F# = 2 X 1 3 4 X
Ebm7 = X X 1 3 2 2
F#/Bb = 6 X 4 6 7 X
F#4 = 2 4 4 4 2 2
F#4(6) = 14 13 11 11 12 11
F#7(13) = 2 X 2 3 4 X
F#m7 = 2 X 2 2 2 X
G#m7 = 4 X 4 4 4 X
G#m7/9 = X X 6 4 7 6
