Imaginasamba - Contramão

Intro: F#m C#m Bm A A7+ A7 A7+ F#m

                               C#m
Com tuas amigas você se transforma
                                  Bm
Nunca fecha a cara, nunca me estressa
                              A
E não interessa se são altas horas
A7+         A7         A7+       F#m
Não mede horário nem fica com pressa
                              C#m
Com os meus amigos você fica muda
                                Bm
Não puxa conversa, não dá um sorriso
                                     A
Se faz de excluída, não vê nem me escuta
           A7
E não é de hoje que eu te aviso

D
Para, não tenho nada a esconder

C#m
Sabe, que eu só saio com você
Bm                  D                F#m        C#m               A7
Só que no seu mundo eu fico perdido, mas me esforço pra me entrosar
D
Para de querer me proibir
C#m
Sabe, gosto de te ver sorrir
Bm
Então dá um tempo
D                            C#m
Para com esse papo de regular
       F#7
Já cansei de provar

Bm
Que fazendo cena, arrumar problema
D
Se a gente briga vê que não valeu a pena
A                        A7+
Sempre se arrepende mas faz
              A7
Tá ficando demais
            F#m   F#7
Você tá na contramão
Bm                                   (C#m D)
Nunca estou certo, mas sempre conserto

Meus amigos falam que é pra eu ficar esperto
A                    A7+
Nós nunca vivemos em paz
             A7
Tá ficando demais
            F#7/4 F#7
Você tá na contramão
Bm                E7          C#m  A7
Será que estamos juntos em vão

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
F#7 = 2 4 2 3 2 2
F#7/4 = 2 4 2 4 2 X
F#m = 2 4 4 2 2 2
