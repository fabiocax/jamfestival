Imaginasamba - Ela é Pagodeira

Am7/11                               D7
Essa menina mexe com minha cabeça.
Am7/11                               D7
Chegou do nada e já mudou minha natureza.
F7+        F6                                          Bm7/5-            E7
Eu sempre fui o cara, como é que eu fui me apaixonar?
Am7/11                               D7
O compromisso nunca foi minha rotina,
Am7/11                               D7
Era uma mina diferente em cada esquina.
F7+        F6                                          Bm7/5-      C7/13
E todos me diziam que um dia eu ia pagar.
F7+                                           Em7          Gm7/11
E o pior é que tô achando tão bom, dei bobeira.
F7+                                              Em7          Gm7/11
Abalou a estrutura do meu coração, de primeira.
Fm7
Essa paixão tá me tirando até o sono,
Ab7+
Ninguém consegue perceber que eu sou teu dono.
G7/4                     G7/13                        C6/9      C7/13
E agora vou dizer: bem feito para eu aprender.


F7+                        G7                                 C6/9
Ela é pagodeira, vai pro samba a semana inteira,
Ebº                                  Dm        G7                         C6/9  C7/13
Vive de gandaia e de zoeira, não sei se aguento essa mulher.
F7+                        G7                                  C6/9
É da madrugada, e nunca dispensa uma gelada.
Ebº                                  Dm        G7                                      C6/9
Tô perdido com essa namorada, mas eu faço tudo que ela quer.

----------------- Acordes -----------------
Ab7+ = 4 X 5 5 4 X
Am7/11 = 5 X 5 5 3 X
C6/9 = X 3 2 2 3 3
C7/13 = X 3 X 3 5 5
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
Ebº = X X 1 2 1 2
Em7 = 0 2 2 0 3 0
F6 = 1 X 0 2 1 X
F7+ = 1 X 2 2 1 X
Fm7 = 1 X 1 1 1 X
G7 = 3 5 3 4 3 3
G7/13 = 3 X 3 4 5 X
G7/4 = 3 5 3 5 3 X
Gm7/11 = 3 X 3 3 1 X
