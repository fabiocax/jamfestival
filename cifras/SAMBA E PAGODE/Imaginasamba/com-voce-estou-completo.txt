Imaginasamba - Com Você Estou Completo

Solo introdução cavaquinho
43 45 33 33
43 45 35
43 45 33 24 23

Introdução:
Eb Dm (2x)

Solo passagem: 43 45 33 33 43 45 33 35

Eb
Me culpar é fácil
Dm
Me fazer de vilão
Cm                    Bb Eb Bb 43 45 33 33 43 45 33 35
É tão simples julgar

Eb
Quem errou não fui eu
Dm
Não rolou traição

Cm             Bb  Eb Gm
Tente acreditar

              Cm
Como é que eu vou trocar
                                         Dm
A mulher que me liga só pra saber se eu cheguei bem
                                 Eb
A pessoa que sabe me dar prazer como ninguém
   Ebm                   Bb
E me conhece mais do que eu mesmo
      G     F      Eb
Onde eu vou encontrar
                                          Dm
Outro alguém que escute os meus problemas sem fingir
                                      Eb
E que quando estou triste consegue me fazer sorrir
 F7/9              Bb
E tem o dom de me amar

                Eb
Com você do meu lado

Eu não quero ter fama
               Dm
Não preciso de grana
                   Gm
Muito menos de outra mulher
Cm                  F7/9                  Bb  Bb9
Tudo que sonhei nessa vida pode crer você é

                Eb
Com você to completo
                            Dm
Abro mão da gelada com a rapaziada
                    Gm
Passo o domingo a te amar
      Cm             F7/9          Bb   Bb9 (Na 2a vez: Bb Eb Gm)
Não vou precisar te perder pra valorizar

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bb9 = X 1 3 3 1 1
Cm = X 3 5 5 4 3
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
Ebm = X X 1 3 4 2
F = 1 3 3 2 1 1
F7/9 = X X 3 2 4 3
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
