Jair Rodrigues - O Conde

Introdução: Gm7/9 Am7(b5) D7(b9) Gm7/9 (2x)
     Gm7/9                      A7/9
Encontrei hoje cedo no meu barracão
                        Am7(b5)
Minha roupa de conde no chão
     D7(b9)         Gm7/9    Am7/9 D7(b9)
Fantasia de plumas azuis a rolar
   Gm7/9               A7/9      Dm7/9
E achei em pedaços bem junto à janela
    G7/9               C7/9
Meu pinho quebrado por ela
      F7/9               Bb7M    Am7(b5) D7(b9)
Tal e qual sucedeu na canção popular
           Gm7/9                     A7/9
Bem que eu quis atrás dela sair e brigar
                              Am7(b5)
Mas depois me lembrei que é melhor
    D7(b9)          G7(b9)
Ela ir de uma vez e eu ficar
          Cm7/9
E além do mais

               F7/9
Sambista até morrer eu sou
Bb7M                  Eb7M/9
E onde a minha escola for eu vou
Am7(b5)              D7(b9)     Gm7/9
Amor a gente perde a gente tem amor que vem
    G7M       G7/9             Cm7/9
Como é que eu posso por ela trocar
     Am7(b5)    D7(b9)   G7M G7/9
A emoção de ver Vilma dançar
      C7M/9    Cm7/9    G7M
Com o seu estandarte na mão
    C7M/9      C#m7(b5) F#7/9     Bm7/9
E ouvir todo o povo Meu povo aplaudir
              E7(b9)   Am7/9            D7/9   G7M  G7(b9)
Minha escola a evolu---ir / Minha ala comigo passar
                  Cm7/9     F7/9     Bb7M
Bem melhor do que ela / É sair na Portela
     Eb7M/9     Am7(b5)   D7(b9)   Gm7/9
E um samba de enredo no asfalto cantar
Introdução
Ai como é...
Introdução
Gm7/9 Am7(b5) D7(b9) Gm7/9

----------------- Acordes -----------------
A7/9 = 5 X 5 4 2 X
Am7(b5) = 5 X 5 5 4 X
Am7/9 = X X 7 5 8 7
Bb7M = X 1 3 2 3 1
Bm7/9 = X 2 0 2 2 X
C#m7(b5) = X 4 5 4 5 X
C7/9 = X 3 2 3 3 X
C7M/9 = X 3 2 4 3 X
Cm7/9 = X 3 1 3 3 X
D7(b9) = X 5 4 5 4 X
D7/9 = X 5 4 5 5 X
Dm7/9 = X 5 3 5 5 X
E7(b9) = X X 2 1 3 1
Eb7M/9 = X 6 5 7 6 X
F#7/9 = X X 4 3 5 4
F7/9 = X X 3 2 4 3
G7(b9) = 3 X 3 1 0 X
G7/9 = 3 X 3 2 0 X
G7M = 3 X 4 4 3 X
Gm7/9 = X X 5 3 6 5
