Adoniran Barbosa - Morro da Casa Verde

 Em     E7         Am
Silêncio é madrugada
      B7                                         Em                   C     B7
No Morro da Casa Verde a raça dorme em paz
     Em                E7              Am
E lá embaixo meu colegas de maloca
B7                                                          Em           E7
Quando começam a sambar não param mais.... Silêncio

 E7        Am                          Em
      Valdir vai buscar o tambor
 E7     Am                   Em
      Laércio traz o agogô
            C                    B7            Em
Que o samba na casa verde enfezou...
            C                    B7           Em      E7
Que o samba na casa verde enfezou... Silêncio!

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
