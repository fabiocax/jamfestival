Adoniran Barbosa - Joga a Chave

Dm               Gm    A7
JOGA A CHAVE MEU BEM
             Dm
AQUI FORA TÁ RUIM DEMAIS
                    Gm            A7
CHEGUEI TARDE, PERTURBEI TEU SONO,
                 Dm
AMANHÃ EU NÃO PERTURBO MAIS

                 C
FAÇO UM FURO NA PORTA
                    Bb
AMARRO UM CORDÃO NO TRINCO
                      Bb7 A7   Gm
PRA ABRIR PRO LADO DE FO..RA
                      Dm
NÃO PERTURBO MAIS TEU SONO
                   Bb
CHEGO À MEIA-NOITE SIM
     A              Dm
OU ENTÃO A QUALQUER HORA

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bb = X 1 3 3 3 1
Bb7 = X 1 3 1 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Gm = 3 5 5 3 3 3
