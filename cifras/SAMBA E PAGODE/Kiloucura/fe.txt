Kiloucura - Fé

A        A7           D           E7/9      A
É Inabalavel minha fé,  no ser que fez a luz que é,
   A7          D                   E7/9           A
O sol e o som do Oboé é nesse "Deus" que eu tenho Fé
Em7   A7
Fé      Fé e
D              E79
Montanhas ja removi
                    C#m
Com a força do meu clamor
                  G7
As Bençãos que recebi
         F#7      Bm7              E7/9
Me fazem cantar o              Amor por isso ja decidi
                      A7+        A6
que por onde quer que eu vou
     Em7
Eu vou, eu vou, eu vou
      A7  D
eu vou,   eu vou
                    E7/9
Levando a fé companheira

    C#m                G7        F#7
Eu vou, ultrapassando barrei.....ras
Bm7
Eu sei, diz o livro da lei
  E7/9                A7+      A13
Com fé eu jamais temerei a Dor
     Em7
Eu vou, eu vou, eu vou
      A7   D
eu vou,      eu vou
                         E7/9
Eu vou, meu coração é profundo
     C#m                G7          F#7
Eu sou, filho do dono do mun.......do
Bm7
Cantei quando andei pelos breus
      E7/9                      A7+
Por que não a poder maior que "Deus"

A             A7                         D
Fé, é.........meu farol na........escuridão
              E7/9             A
É a voz bonita da...........razão
                        A7             D
Se os olhos turvão na............missão
  E7/9          A7+        A6
Enxergo com o coração
Em7      A7     D                        E7/9
Co.......ra........ção, dos milagres que eu ja vi
               A      A7+
O primeiro foi nascer
            A7/4              A7                  D
Por isso eu aprendi e nunca vou.............esquecer
                   E7/9
Mesmo que eu me va daqui
               A7+       A6
A fé nunca vai morrer
     Em7
Eu vou, eu vou, eu vou
      A7                    D
eu vou,.......eu vou

----------------- Acordes -----------------
A = X 0 2 2 2 0
A13 = 5 X 4 6 5 X
A6 = 5 X 4 6 5 X
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
A7/4 = X 0 2 0 3 0
Bm7 = X 2 4 2 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E7/9 = X X 2 1 3 2
E79 = X X 2 1 3 2
Em7 = 0 2 2 0 3 0
F#7 = 2 4 2 3 2 2
G7 = 3 5 3 4 3 3
