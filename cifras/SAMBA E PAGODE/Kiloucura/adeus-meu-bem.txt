Kiloucura - Adeus Meu Bem

[Intro]  G7M  Bm7  Em7  Am7  D7

            G7M              G6              Bm7
O que você queria que eu fizesse, você me pediu pra dar um tempo
          Em7               Am7/9           D7/9
Esperava que você viesse reviver o nosso sentimento
             G7M           G6                    Bm7
Quando me cansei de te esperar, quando achei alguém pra minha vida
        Em7                      Am7/9       D7/9
Você cisma de voltar com essa paixão mal resolvida
   Bm7       Bm7M        Bm7            Bm7M
Agora é minha vez de te pedir me dá um tempo
   Am7/9                   D7/4  D7
Não faz assim perdoa eu tô feliz?
  Bm7     Bm7M             Bm7  Bm7M
Sofri demais te esperando à toa
   Am7/9         D7/9          G7M    Am7(9)  D7/9
Desculpe estou amando outra pessoa
       G7M                    G6
Que direito você tem de vir me censurar
       Bm7                Em7
Quanto tempo fiquei sem ninguém

  Am7/9                  D7/9        G7M  Am7(9)  D79
Agora que encontrei alguém adeus meu bem

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Am7(9) = X X 7 5 8 7
Am7/9 = X X 7 5 8 7
Bm7 = X 2 4 2 3 2
Bm7M = X 2 4 3 3 2
D7 = X X 0 2 1 2
D7/4 = X X 0 2 1 3
D7/9 = X 5 4 5 5 X
D79 = X 5 4 5 5 X
Em7 = 0 2 2 0 3 0
G6 = 3 X 2 4 3 X
G7M = 3 X 4 4 3 X
