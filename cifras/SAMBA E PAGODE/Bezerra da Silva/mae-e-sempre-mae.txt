Bezerra da Silva - Mãe É Sempre Mãe

Intro : ( C F Gm C F Am Dm C Bb C F C )

E|--8p10-10-8-8-------------------------8--|
B|--------------10-10-8-6---6-------6-8----|
G|------------------------7---7---7--------|
D|-----------------------------------------|
A|-----------------------------------------|
E|-----------------------------------------|

E|--8p10-10-8-8----------------------------|
B|--------------10-10-8-6---6---8-----6-6--|
G|------------------------7---7---7-7------|
D|-----------------------------------------|
A|-----------------------------------------|
E|-----------------------------------------|

             F    Gm7              Am7
Verdadeiro Amor     Que Se Tem Na Vida
 Bb  Am7  Dm7 C       Bb    C      F    C
Só Existe Um, é o Da Nossa Mãe Querida
             F    Gm7              Am7
Verdadeiro Amor     Que Se Tem Na Vida

 Bb  Am7  Dm7 C       Bb    C      F
Só Existe Um, é o Da Nossa Mãe Querida

      F             Gm  C        F         Gm  C
Mãe é Um Grande Tesouro   Cheio De Sublimação
       F         Gm         Gm7         C
É o Segundo Nazareno Na História Do Perdão
       F            Gm  C         F        Gm  C
Nossa Mãe é Sempre Mãe    Na Alegria e Na Dor
    F          Gm         Gm7          C
Ela Ama o Seu Filho Seja Lá ele o Que For
             F    Gm7              Am7
Verdadeiro Amor     Que Se Tem Na Vida
 Bb  Am   Dm7 C       Bb    C      F    C
Só Existe Um, é o Da Nossa Mãe Querida
             F    Gm7              Am7
Verdadeiro Amor     Que Se Tem Na Vida
 Bb  Am7  Dm7 C      Bb     C      F
Só Existe Um, é o Da Nossa Mãe Querida
        F           Gm     C       F           Gm  C
Se Seu Filho For Ministro,   Diplomata Ou Capitão
     F            Gm         Gm7       C
Sua Mãe Sente Prazer Deste Grande Cidadão
        F            Gm    C          F         Gm  C
Mas Se For Um Delinquente,   Que Tem Má Repultação
     F                Gm          Gm7       C
Sua Mãe Lhe Abraça e Beija Com o Mesmo Coração
                  F   Gm7              Am7
É... Verdadeiro Amor    Que Se Tem Na Vida
 Bb  Am7  Dm7 C       Bb    C       F    C
Só Existe Um, é o Da Nossa Mãe Querida
                  F   Gm7              Am7
É... Verdadeiro Amor    Que Se Tem Na Vida
 Bb  Am7  Dm7 C       Bb    C       F    C
Só Existe Um, é o Da Nossa Mãe Querida
 Bb  Am7  Dm7 C      Bb    C       F    C
Só Existe Um, é o Da Nossa Mãe Querida

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
