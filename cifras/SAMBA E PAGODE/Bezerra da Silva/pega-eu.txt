Bezerra da Silva - Pega Eu

G      E7            Am
  O ladrão foi lá em casa
          D7        G
Quase morreu do coração
G      E7            Am
  O ladrão foi lá em casa
          D7        G
Quase morreu do coração
                E7              Am
Já pensou se o gatuno tem um enfarte, malandro
   D7              G
E morre no meu barracão
        E7            Am
Eu não tenho nada de luxo
     D7                 G
Que possa agradar um ladrão
           E7       Am            D7                G
É só uma cadeira quebrada, um jornal que é o meu colchão
                E7      Am
Eu tenho uma panela de barro
       D7             G
E dois tijolos como fogão

    E7            Am
O ladrão ficou maluco
               D7                     G
De ver tanta miséria em cima de um cristão
             E7         Am
Que saiu gritando pela rua
     D7              G
Pega eu que eu sou ladrão
     E7
Pega eu,
     Am
Pega eu
     D7              G
Pega eu que eu sou ladrão...
     E7
Pega eu,
     Am
Pega eu
     D7              G
Pega eu que eu sou ladrão...

        E7          Am
Não assalto mais um pobre
         D7         G
Nem arrombo um barracão
     E7
Pega eu,
     Am
Pega eu
     D7              G
Pega eu que eu sou ladrão...

         E7                Am
Lelé da cuca ele está no pinel
           D7            G
Falando sozinho de bobeação
        E7       Am
Dando soco nas paredes
      D7           G
E gritando esse refrão
               E7
Por favor pega eu
     Am
Pega eu
     D7              G
Pega eu que eu sou ladrão...
     E7
Pega eu,
     Am
Pega eu
     D7              G
Pega eu que eu sou ladrão...
        E7           Am
Não assalto mais um pobre
        D7          G
Nem arrombo um barracão

----------------- Acordes -----------------
Am = X 0 2 2 1 0
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
G = 3 2 0 0 0 3
