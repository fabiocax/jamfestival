Bezerra da Silva - Filho do Dono

C  G7                           C
Eu não sou dono do mundo
                    F                    C
Mas filho do dono do mundo eu sou
                   A7                      Dm
Em nome do pai e do espírito santo
                 G7                     C   (na 1° vez)

(Na 2° vez) Cm
Me sinto feliz louvando meu senhor
                       Dm7(5b)               G7                        Cm
Muito tempo eu vivi neste mundo perdido dentro do pecado
                           Dm7(5b)              G7                      Cm
Só que meu pai me tocou e perdoou tudo que eu fiz de errado
                       Dm7(5b)     G7                     Cm
Deus é pai não padrasto, é amor e justiça também
Cm                                        Dm7(5b)
Só que a justiça dos homens é cega
                      G7                          Cm  C7
Mas a do meu deus enxerga muito bem
                      Fm                     Bb7                    Eb
Se entregue a jesus meu irmão, se você quer ser feliz

    Ab                        Dm7(5b)
E também vá prestando atenção
                     G7                         Gm7(5b)  C7
Nas palavras sagradas que a bíblia diz
                      Fm                     Bb7                    Eb
Se entregue a jesus meu irmão, se você quer ser feliz
    Ab                        Dm7(5b)
E também vá prestando atenção
                    G7                           C
Nas palavras sagradas que a bíblia diz

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Ab = 4 3 1 1 1 4
Bb7 = X 1 3 1 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
Dm = X X 0 2 3 1
Dm7(5b) = X X 0 1 1 1
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G7 = 3 5 3 4 3 3
Gm7(5b) = 3 X 3 3 2 X
