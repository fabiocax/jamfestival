Raça Negra - E Agora Eu

Intro: E   F#m   G#m    (F#m  B)

E|---0--2--4--4---0---4----2---0----7--7--5--4--5----7--5--4--5----9--7--7--7---5--5--5/7---7--7--7-----0----2----|
B|----------------------------------------------------------------------------------------------------------------------------2----4---|
G|----------------------------------------------------------------------------------------------------------------------------2----4---|
D|----------------------------------------------------------------------------------------------------------------------------2----4---|
A|----------------------------------------------------------------------------------------------------------------------------0----2---|
E|----------------------------------------------------------------------------------------------------------------------------x----2---|


E                                                   F#m
Sei, quanto é difícil encontrar alguém
                                         G#m
Que divida seu espaço, me dê um abraço
F#m                         B
E fique com você na dor

E                                                   F#m
Sei quanto é difícil encontrar alguém
                                                G#m
Que não deixe desgastar, que saiba preservar

   A                      B
O tempo com amor

C#m                                            G#m
      Pensei que viveria tudo isso com você
                                          A
Mas o destino não quis saber
                                  E
E fez a gente se perder

C#m                                     G#m
Sei que tenho que seguir em frente
                                         A
A solidão maltratando a gente
                    B
Isso dói demais

REFRÃO

      E
E agora eu (e agora eu)
    C#m
Preciso descobrir
  A
Achar um outro alguém
                      B
Um novo caminho a seguir
      E
E agora eu (e agora eu)
    C#m
Preciso ser feliz
   A
Achar um outro alguém
                   B
Para o meu coração se abrir
E
De novo

REPETE TUDO

FINAL: Repete refrão + Intro + Esse solo:

E|---12---11---9---4---7-----5~7~5---4----|
B|-------------------------------------------------|

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
