Raça Negra - Chega

Intro 2x: Cm  Dm

       Cm                      Dm
    A gente já não fala mais de amor
       Cm
    A gente já não liga mais
        Dm
    Não te chamo mais de meu bem
       Cm                                      Dm
    Parecemos dois estranhos vivendo num quarto de tensão
       Cm                   Dm
    Não existe afinidade nem amor no coração
              Cm                    Dm
    Por isso chega vou procurar felicidade
     Cm                 Dm
    Chega quero um amor de verdade
     Cm                    Dm
    Chega vou procurar felicidade
     Cm                 Dm
    Chega quero um amor de verdade


       Cm                      Dm
    A gente não se trata com carinho
        Cm
    Não Adianta tentar mais
        Dm
    Eu preciso de alguém
       Cm
    A Verdade é que amor acabou
         Dm
    Já não dá para suportar
        Cm
    Eu não sei o que é alegria
            Dm
    Estou cansado de chorar
              Cm                    Dm
    Por isso chega vou procurar felicidade
     Cm                 Dm
    Chega quero um amor de verdade
     Cm                    Dm
    Chega vou procurar felicidade
     Cm                 Dm
    Chega quero um amor de verdade

     ( Cm  Dm ) 2x

        Cm                      Dm
    A gente já não fala mais de amor
       Cm
    A gente já não liga mais
        Dm
    Não te chamo mais de meu bem
       Cm                                      Dm
    Parecemos dois estranhos vivendo num quarto de tensão
       Cm                   Dm
    Não existe afinidade nem amor no coração
              Cm                    Dm
    Por isso chega vou procurar felicidade
     Cm                 Dm
    Chega quero um amor de verdade
     Cm                    Dm
    Chega vou procurar felicidade
     Cm                 Dm
    Chega quero um amor de verdade

----------------- Acordes -----------------
Cm = X 3 5 5 4 3
Dm = X X 0 2 3 1
