Raça Negra - Pedindo a Sua Volta

[Intro] C  Dm7

Dm7                     C7M
 Eu já rodei o mundo, agora estou de volta

Dm7                    C7M
 Querendo seu perdão, rondando a sua porta
Dm7                    C7M
 O medo seu não me deixa tão aflito
Dm7                    C7M
 Não dá mais pra conter no peito esse grito

[Ponte]

C7M
 Eu pensei que sem você
Dm7
 Fosse fácil de viver
C7M
 Mas não era bem assim
Dm7
 Sem o seu amor em mim


[Refrão]

C7M
 Quero o seu beijo, o seu abraço
Dm7
 O seu sorriso, tudo de novo
C7M
 Quero o seu colo, o seu desejo
Dm7
 Quero o seu corpo, tudo de novo

Dm7
 Estou arrependido, assumo a minha
C7M
 Culpa
Dm7
 Estou sofrendo tanto e quero a sua
C7M
 Ajuda
Dm7                            C7M
 Distante de você, só cometi enganos
Dm7
 Estou aqui de volta pra dizer que te
C7M
 Amo

[Ponte]

C7M
 Eu pensei que sem você
Dm7
 Fosse fácil de viver
C7M
 Mas não era bem assim
Dm7
 Sem o seu amor em mim

[Refrão]

C7M
 Quero o seu beijo, o seu abraço
Dm7
 O seu sorriso, tudo de novo
C7M
 Quero o seu colo, o seu desejo
Dm7
 Quero o seu corpo, tudo de novo

----------------- Acordes -----------------
C = X 3 2 0 1 0
C7M = X 3 2 0 0 X
Dm7 = X 5 7 5 6 5
