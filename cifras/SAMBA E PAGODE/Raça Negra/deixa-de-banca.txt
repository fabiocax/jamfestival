Raça Negra - Deixa de Banca

Intro: B7  B7  B7  B7  C7  B7  Em  B7

       Em             Am
Parabadá   pará   paradá
          C7
Deixa de banca comigo
 B7                       Em
Sei que você gosta só de mim    bis
          Am
Não adianta sair e curtir por aí
           Em
Entrou na minha sei que nunca vai sair
      Am
Não adianta dizer que você não me quer
             B7
Pois não dá pé

REFRÃO
         Em
Que papelão   onda por fora
         Am
A turma toda   não te deu bola

          C7              B7
Eu sou o bom   e a turma toda
         Em
Já me namora
              Em
Nem vem de espelho   a vida é cega
           Am
E nesse embalo    ninguém me pega
       C7            B7
Tô esperando a decisão
           Em
Do sim ou não

VOLTA NO REFRÃO
Em / Am / C7 / B7 / Em / B7  bis

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
C7 = X 3 2 3 1 X
Em = 0 2 2 0 0 0
