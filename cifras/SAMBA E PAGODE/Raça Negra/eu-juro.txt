Raça Negra - Eu Juro

(intro) A Bm A Bm A

A                                                 Bm
A tanta incerteza no meu coração que eu já nem sei mais,
A                                                       Bm
Ta tudo tão errado tudo tão escuro tudo tão dolorozo pra mim
                       D                  E                      A
Isso faz com que eu desista desse tal sentimento que se  chama amor
A                                              Bm     A
parece que na minha vida  nao vai aparecer ninguém  alguem que me faça  rir
                                  D
alguem que me faça olhar a vida  diferente, diferente
                         Bm               D
isso faz  com que  eu desista desse tal sentimento
              A
que se chama amor,amor or or
        D
você amor tem obrigaçao de me fazer feliz
                                         F#m
 porque senão nunca mais eu acredito em ti
        Bm   D                  A
há eu juro! nao procuro  mais você !

                D                                  A
É tudo culpa sua por estar assim
              A                     F#m
se me der felicidade eu acredito em ti
       Bm    D                  A
há eu juro! que nao vou mais sofrer

(intro) A Bm A Bm A

       D
Você amor tem obrigaçao de me fazer feliz
          A                          F#m
porque se-nao nunca mais acredito em ti
       Bm   D                   A
há eu juro!que nao vou mais sofrer!
      D
voçe amou sem obrigaçao de me fazer feliz
          A                              F#m
porque senão nunca mais  eu acredito em ti
        Bm D                  A
hà eu juro!nao procuro mais você!

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
