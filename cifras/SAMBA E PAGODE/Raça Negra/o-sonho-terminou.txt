Raça Negra - O Sonho Terminou

 A
Chega de sofrer
Bm
Chega de chorar
C#m                      Bm   E
Já tentei de tudo mas não dá

A
Você desfez de mim
Bm
Não quis o meu amor
C#m                          Bm    E
O que sinto por você não tem valor

     A                  Bm
Acabou, eu vou me libertar
                 C#m                   Bm   E
Cansei de me entregar, sem nada a receber
     A                 Bm
Acabou, o sonho terminou
          C#m                    Bm  E
Você me magoou, preciso te esquecer


A
Chega de você
Bm
Chega de sonhar
C#m                     Bm  E
O seu coração não sabe amar
A
Foi só desilusão
Bm
Machucou meu coração
C#m                        Bm  E
Mas um novo amor vou encontrar

  A                  Bm
Acabou, eu vou me libertar
                 C#m                   Bm   E
Cansei de me entregar, sem nada a receber
     A                 Bm
Acabou, o sonho terminou
          C#m                    Bm  E
Você me magoou, preciso te esquecer

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
