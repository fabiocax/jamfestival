Raça Negra - Estou Bem

[Intro] Em  C  Em  C  D7
        Em  C  Em  C  D7

Em
Se você quer saber
      C
Como eu estou vivendo
Em
Quando você se foi
      C
Me deixou aqui sofrendo
      D
Estou bem
         C
Estou feliz
             D
Encontrei alguém
         C
Que me quis
Em
Não estou entendendo

      C
O que você quer comigo
Em
Com você minha vida
 C
Foi sofrimento e castigo
       D            C
Eu não quero mais saber
       D         C
Nem lembrar de você

Em            C
Estou feliz agora
Am              D  B7
Todo mundo pode ver
Em                     C
Como estou mais feliz agora
Am                  D  B7
  Estou longe de você

( Em  C  Em  C  D7 )
( Em  C  Em  C  D7 )

Em
Se você quer saber
      C
Como eu estou vivendo
Em
Quando você se foi
      C
Me deixou aqui sofrendo
      D
Estou bem
         C
Estou feliz
             D
Encontrei alguém
         C
Que me quis
Em
Não estou entendendo
      C
O que você quer comigo
Em
Com você minha vida
 C
Foi sofrimento e castigo
       D            C
Eu não quero mais saber
       D         C
Nem lembrar de você

Em            C
Estou feliz agora
Am              D  B7
Todo mundo pode ver
Em                     C
Como estou mais feliz agora
Am                  D  B7
  Estou longe de você

Em            C
Estou feliz agora
Am              D  B7
Todo mundo pode ver
Em                     C
Como estou mais feliz agora
Am                  D  B7
  Estou longe de você

Em            C
Estou feliz agora
Am              D  B7
Todo mundo pode ver
Em                     C
Como estou mais feliz agora
Am                  D  B7
  Estou longe de você

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
