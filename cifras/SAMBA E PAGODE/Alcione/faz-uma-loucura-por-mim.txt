Alcione - Faz uma loucura por mim

Introdução: Dm7/9 : Gm7/9 : Dm7/9 : Gm7/9 : A7 : Dm7/9

Dm7/9
.....Faz uma loucura por mim
Gm7/9
.....Sai gritando por aí, bebendo e chora
Dm7/9                                 Gm7
um porre, picha um muro que me adora
Dm7/9
.....Faz uma loucura por mim
Gm7/9
.....Fica até de madrugada perde a hora
Dm7/9                   Gm7/9
Sai comigo pra gandaia noite afora
C7/9/11                             C7/9
**...Só assim eu acredito nessa história
F7+
....Que você sentiou saudade de me ter
D7/11                         D7
.....Põe na prática besteira da memória
Gm7
....Pensa menos, faz de tudo, manda ver

G#º
....vem pra dentro tenta ser da mesma escória
Em5-/7                              A7
.....Como já fiz mil loucuras por você
     Dm7/9
Nós dois....se é pra recomeçar que seja até o fim
     Gm7/9
Nós dois....se não é pra ficar, não gasteo teu latim
    C7/9/11                                 C7/9
Nós dois....só posso te aceita ao ver que você faz
       F7+                 Em5-/7    A7
Uma loucura por mim, uma loucura por mim
   Dm7/9
Depois....que você me provar que vai fazer assim
   Gm7/9
Depois....você pode provaro que quiser de mim
   G#º
Depois....Já posso provar que você foi capaz
           A7         A7/11 ^ A7
De uma loucura por mim

INTRODUÇÃO - VOLTA NO **

FINAL
Dm7/9
.....Faz uma loucura por mim
Gm7/9                                     Dm7/9
.....se tem outra em tua vida.....manda embora..

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7/11 = X 0 2 0 3 0
C7/9 = X 3 2 3 3 X
C7/9/11 = X 3 3 3 3 3
D7 = X X 0 2 1 2
D7/11 = X X 0 2 1 3
Dm7/9 = X 5 3 5 5 X
Em5-/7 = X X 2 3 3 3
F7+ = 1 X 2 2 1 X
G#º = 4 X 3 4 3 X
Gm7 = 3 X 3 3 3 X
Gm7/9 = X X 5 3 6 5
