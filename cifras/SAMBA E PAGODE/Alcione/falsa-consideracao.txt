Alcione - Falsa Consideração

  G                                 Bm
Agora eu sei que o amor que você prometeu
                              Dm    E7
Não foi Igual ao que você me deu
                   Am
Era mentira o que você Jurou
     C           D                         Bm
Mas não faz mal    eu aprendi que não se deve Crer
Em                      Am
   Em tudo aquilo que alguém nos diz
C7                  D           D#  D
   Num Momento de prazer ou de amor
      G                                Bm
Mas tudo bem Eu sei que um dia vai e outro vem
                    Dm            G7
Você ainda há de Encontrar alguém
                      C
Pra lhe fazer o que você me fez
                         Cm             Bm
E ai....... Na hora do sufoco sei você vai me procurar
Em                                     Am
   Com a mesma conversa que um dia me fez   apaixonar

D                                G
   Por alguém de uma falsa consideração
   C  Cm                                        Bm
E ai.......Você vai perceber que eu estou numa boa
Em                                       Am
    Que durante algum tempo fiquei sem ninguém
D                                      G       D7
  Mas há males na vida que vem para o bem (e agora)

  G                                 Bm
Agora eu sei que o amor que você prometeu
                              Dm    E7
Não foi Igual ao que você me deu
                   Am
Era mentira o que você Jurou
     C           D                         Bm
Mas não faz mal    eu aprendi que não se deve Crer
Em                      Am
   Em tudo aquilo que alguém nos diz
C7                  D           D#  D
   Num Momento de prazer ou de amor
      G                                Bm
Mas tudo bem Eu sei que um dia vai e outro vem
                    Dm            G7
Você ainda há de Encontrar alguém
                      C
Pra lhe fazer o que você me fez
                         Cm             Bm
E ai....... Na hora do sufoco sei você vai me procurar
Em                                     Am
   Com a mesma conversa que um dia me fez   apaixonar
D                                G
   Por alguém de uma falsa consideração
   C  Cm                                        Bm
E ai.......Você vai perceber que eu estou numa boa
Em                                       Am
    Que durante algum tempo fiquei sem ninguém
D                                      G  G7
  Mas há males na vida que vem para o bem

  C       Bm   Em            Am    D                         G
Laiá.....laiá......Lalaiá lalaiá       lalaiá lalaiá lalaiá laiá

   C  Cm                                        Bm
E ai.......Você vai perceber que eu estou numa boa
Em                                       Am
    Que durante algum tempo fiquei sem ninguém
D                                      G   Em
  Mas há males na vida que vem para o bem
Am                  D7                  G  Em
   Mas há males na vida que vem para o bem
Am                  D7                  C  Cm  G
   Mas há males na vida que vem para o bem

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D# = X 6 5 3 4 3
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
