Alcione - Deus Me Livre de Você

 Bb7+ | C/Bb | Am7/5- | D7/4 D7
Gm7 Am7 | Bb C/Bb | F7+ | C7/4

D7/9 | Dm7 |
Deixa deixa deixa
Cm7/9 | F7/9
Meu coração sossegado
Bb7+     Am7 Gm7               F
Essa visão do pecado
Em7                          A7
É tentação de dar dor

D7/9                            Dm7 | D7/9                           G7
Nunca vi tanta beleza,  em uma só criatura
C7/4                          C7
É tentação, é perdição, esqueço até quem
F7+                                                  Dm7
Sou,aonde fui, onde não vou
                   B7                         E7                                                  A7  D7/4
Não me olhe assim, me deixe ir, deus me livre de você



Gm7                                          C7
Não é a toa que eu me acabo de desejo,
                 F7+                  Bb7+
O teu jeito esta tirando o meu sossego,
E7                                   A7
Impossível não querer sentir seus beijos,
D7/4
Me encontrar nos seus abraços

Gm7                                            C7
O teu cheiro vem no vento da paixão,
Am7                     D7/4
Ta difícil segurar tanta loucura,
Gm7                                              Bb7+
Deixa, deixa, deixa, que eu siga em paz,
Am7                      D7/4
Isso é tentação de mais
                  Gm7                          E7 A7
Não me olhe assim, me deixe ir, deus me livre de você

Solo de sax
 Bb7+ | C/Bb | Am7/5- | D7/4 D7
Gm7 Am7 | Bb C/Bb | F7+ | D7/4

Gm7                                          C7
Não é a toa que eu me acabo de desejo,
F7+                               Bb7+
O teu jeito esta tirando o meu sossego,
E7                                       A7
Impossível não querer sentir seus beijos,
D7/4                                                          D7
Me encontrar nos seus abraços

Gm7                                            C7
O teu cheiro vem no vento da paixão,
Am7                     D7/4
Ta difícil segurar tanta loucura,
Gm7                                            Bb7+
Deixa, deixa, deixa, que eu siga em paz,
Am7                      D7/4
Isso é tentação de mais
                  Gm7                          E7 A7
Não me olhe assim, me deixe ir, deus me livre de você

Dm7

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
Bb = X 1 3 3 3 1
Bb7+ = X 1 3 2 3 1
C/Bb = X 1 2 0 1 X
C7 = X 3 2 3 1 X
C7/4 = X 3 3 3 1 X
Cm7/9 = X 3 1 3 3 X
D7 = X X 0 2 1 2
D7/4 = X X 0 2 1 3
D7/9 = X 5 4 5 5 X
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
F7+ = 1 X 2 2 1 X
F7/9 = X X 3 2 4 3
G7 = 3 5 3 4 3 3
Gm7 = 3 X 3 3 3 X
