Alcione - O Surdo

Cm                                Fm
Meu surdo, parece absurdo
                          Bb
Mas você me escuta
                                                  Eb    G7
Bem mais que os amigos lá do bar
          Cm                                           Fm
Não deixa que a dor mais lhe machuque
                           Bb
Pois pelo seu batuque
                                                                 Eb      G7
Eu dou fim ao meu pranto e começo a cantar
          Cm                                     Fm
Meu surdo, bato forte no seu couro
                                  Bb
Só escuto este teu choro
                                              Eb        G7
Que os aplausos vêm pra consolar
           Cm                                Fm
 Meu surdo, velho amigo e companheiro
                               Bb                                                Eb     G7
Da avenida e de terreiro,de rodas de samba e de solidão

        Cm                                          Fm
Não deixe que eu vencido de cansaço
                                    Bb
Me descuide desse abraço
                                                                      Eb       C7
E desfaça o compasso do passo do meu coração
   Fm         G7             Cm
Amigo, que ironia desta vida
                             Fm               G7            Cm
Você chora na avenida pro meu povo se alegrar

                G7
Eu bato forte em você
                                           Cm
E aqui dentro do peito uma dor
Me destrói
                           G#
Mas você me entende
                       G7                 Cm
E diz que pancada de amor não dói
               G7
Eu bato forte em você
                                            Cm
E aqui dentro do peito uma dor
Me destrói
                           G#
Mas você me entende
                     G7           Cm
E diz que pancada de amor não dói

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
Eb = X 6 5 3 4 3
Fm = 1 3 3 1 1 1
G# = 4 3 1 1 1 4
G7 = 3 5 3 4 3 3
