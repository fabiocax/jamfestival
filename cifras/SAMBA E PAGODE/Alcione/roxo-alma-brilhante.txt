Alcione - Roxo, Alma Brilhante

                     Bm7   E7/9
É gente, é matéria, é pó
                    A7+   Bb°
É raça, é força e irmão
                      Bm7
Alma brilhante como o sol
   E7/9                A7+   G7+ ^ G#7+ ^ A7+
De todo mundo o seu coração
                  Bm7   E7/9
É gente, é cor, é amor
                    A7+   Bb°
É Porto Alegre o seu chão
                      Bm7
É roxo, é gosto, é coragem
 E7/9             A7+
Imperador a sua paixão
  F#m7 ^ Fm7 ^ Em7        A7/13             D7+
Sorriso e felicidade......do início ao cair da tarde
                      B7/11  B7            E7
Sem dúvidas, sem comentários.....conta de todo cidade
                       Em7          A7/13                D7+
Dava a vida, falar para o povo.....e tocar uma linda canção

                              B7 ^ C#m
Tanto para o velho ou para o novo
   D°       ^        B7       E7 ^ F#m7    G° ^ E7/G#
Ele sempre estendeu a mão
    Bm7               E7/9           A7+   Bb°
É o roxo, é o roxo, é o roxo, meu irmão
   Bm7                 E7/9          A7+   Bb°
É o roxo, é o roxo, é o roxo, nosso irmão
  Bm7                   E7/9         A7+  Bb°
É o roxo, é o roxo, é o roxo, meu irmão
   Bm7                   E7/9           A7+
É o roxo, é o roxo, é o roxo, nosso irmão..

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7+ = X 0 2 1 2 0
A7/13 = X 0 X 0 2 2
B7 = X 2 1 2 0 2
B7/11 = X 2 4 2 5 2
Bb° = X 1 2 0 2 0
Bm7 = X 2 4 2 3 2
C#m = X 4 6 6 5 4
D7+ = X X 0 2 2 2
D° = X X 0 1 0 1
E7 = 0 2 2 1 3 0
E7/9 = X X 2 1 3 2
E7/G# = 4 X 2 4 3 X
Em7 = 0 2 2 0 3 0
F#m7 = 2 X 2 2 2 X
Fm7 = 1 X 1 1 1 X
G#7+ = 4 X 5 5 4 X
G7+ = 3 X 4 4 3 X
G° = 3 X 2 3 2 X
