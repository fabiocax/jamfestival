Alcione - Mangueira Estação Primeira

Intro: Em  Am6  Em  C7  B7

Em   G
Ô Mangueira
C         B7         Em
Mangueira estação primeira
C            B7   Em B7
A estação da primavera
Em   G
Ô Mangueira
    C              B7      Em
Que enfeita a passarela inteira
   C              B7       Em B7
Oh vem que a multidão te espera

    Am6        B7
Não tens igual
                  Em               G
Teu cenário é realmente uma beleza
                  Am6              B7
Na bandeira tu pintaste a natureza

                 Em             E7
Que comove o coração de quem te ama
   Am6      B7
No carnaval
                     Em                 G
Com as cores verde e rosa da Mangueira
               Am6              B7
A avenida vira mata brasileira
                   Em       Am6 B7 Em
E mostra a raíz do samba

          Am6                B7
Lá vem Mangueira, meu senhor
                       Em
É um jardim em pleno asfalto
Em
É rosa aberta
        D7            G                   C#º
Verde e rosa, verso e prosa em teu louvor
                       Am6        B7
Eu farei porque és a escola dos poetas
   Am6       B7
Mangueira é
               Em             G
A rainha do desfile principal
                   Am6              B7
É por isso que ela reina na colina
                    Em    C7 B7 Em
Sobre o barro imperial

           Am6    B7
Salve a Mangueira
                   Em  E7
Que mantém-se original
Am       B7           Em
E até no nome é a primeira
      C7    B7    Em  E7
Acima do bem e do mal
Am           B7          Em
Representa a última guerreira
        C7     B7   Em
Na tradição do carnaval

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am6 = 5 X 4 5 5 X
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
C#º = X 4 5 3 5 3
C7 = X 3 2 3 1 X
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
