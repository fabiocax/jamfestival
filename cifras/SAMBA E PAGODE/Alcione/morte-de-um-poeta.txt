Alcione - Morte de Um Poeta


  Fm
Silêncio
           F7      Bbm
Morreu um poeta no morro
            C7        Fm
Num velho barraco sem forro
              C#7      C7
Tem cheiro do choro no ar
     Fm               F7      A#m
Mas choro que tem bandolim e viola
          C7          Fm
Pois ele falou lá na escola
                 C#7   C7
Que o samba não pode parar
   G#                         A#m
Por isso meu povo no seu desalento
                       C7
Começa a cantar samba lento
                     Fm
Que é jeito da gente rezar

   A#m        D#7  G#
E dizer que a dor doeu
        C7        Fm
Que o poeta adormeceu
        A#m  D#7  G#
Como um pássaro cantor
        C7          Fm
Quando vem no entardecer
      C#7    C7   Fm
Acho que nem é morrer

  Fm
Silêncio
            F7      A#m
Mais um cavaquinho vadio
           C7        Fm
Ficou sem acordes, vazio
             C#7        C7
Deixado num canto de um bar
   G#                         A#m
Mas dizem poeta que morre é semente

                      C7
De samba que vem de repente
                     Fm          F7
E nasce se a gente cantar
     A#m     D#7   G7
E dizer que a dor doeu
       C7         Fm
Que o poeta adormeceu
         A#m  D#7  G#
Como um pássaro cantor
        C7          Fm
Quando vem no entardecer
      C#7    C7  Fm
Acho que nem é morrer

----------------- Acordes -----------------
A#m = X 1 3 3 2 1
Bbm = X 1 3 3 2 1
C#7 = X 4 3 4 2 X
C7 = X 3 2 3 1 X
D#7 = X 6 5 6 4 X
F7 = 1 3 1 2 1 1
Fm = 1 3 3 1 1 1
G# = 4 3 1 1 1 4
G7 = 3 5 3 4 3 3
