Alcione - Figa de Guiné

( Reginaldo Bessa e Nei Lopes )

Obs.: Música gravada por Alcione em 1972 (Polygram) em seu disco de
      estréia. Regravada pela mesma cantora em 1982 em seu LP "Dez
      anos depois", na mesma gravadora.


Dm                  Dm7                Gm
Quem me vinga da mandinga é a figa de guiné
                  Bbdim                Dm11
mas o de fé do meu axé não vou dizer quem é
     Dm            Gm
Sou da fé, cabeça feita
 Bb9   A7
Axê, axé
  Bbdim          Dm
No peji do candomblé
A5+     Dm
Axê ,, axé
         Dm          Gm
quem não pode com a mandinga

D#7   A7
Axê, axé
    Gm        Dm
não batuca o opanijé
Dm                Dm7                Gm
Quem me vinga da mandinga é a figa de guiné
                  Bbdim                Dm11
mas o de fé do meu axé não vou dizer quem é
       Dm          Gm
Onda forte não derruba
 Bb9   A7
axê, axé
     Bbdim        Dm
nem machuca quem tem fé
A5+    Dm
axê, axé
       Dm           Gm
Vou nas águas do meu santo
D#7    A7
axê, axé
      Gm        Dm
na enchente da maré
Dm                 Dm7                 Gm
Quem me vinga da mandinga é a figa de guiné
                 Bbdim                  Dm11
mas o de fé do meu axé não vou dizer quem é
     Dm         Gm
Da Bahia me mandaram
Bb9    A7
axê, axé
      Bbdim      Dm
minha figa de guiné
D#7   A7
axê, axé
      Dm            Gm
com as bençãos  de Caymmi
D#7    A7
axê, axé
Gm             Dm
Jorge Amado e Caribé

----------------- Acordes -----------------
A5+ = X 0 3 2 2 0
A7 = X 0 2 0 2 0
Bb9 = X 1 3 3 1 1
Bbdim = X 1 2 0 2 0
D#7 = X 6 5 6 4 X
Dm = X X 0 2 3 1
Dm11 = X 5 5 7 6 5
Dm7 = X 5 7 5 6 5
Gm = 3 5 5 3 3 3
