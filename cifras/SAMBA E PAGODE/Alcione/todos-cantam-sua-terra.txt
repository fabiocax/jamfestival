Alcione - Todos Cantam Sua Terra

[Intro]  A7  G7  A7  G7  A7  B7  E7  A7  D7  A7

A7                      B7           A7                               B7                A7
Todo mundo canta sua terra eu também vou cantar a minha
                                     G7                                         A7
Modéstia à parte seu moço minha terra é uma belezinha
               G7                A7                         G7   A7
 A praia  de  olho d'água    lençóis e    a......racagi
                           D7     F#7  B7              E7             A7
Praias bonitas assim                 eu juro que nunca  vi
               D7             A7                   E7                A7
Minha terra tem beleza que em versos não sei dizer
     D7                           A7                  E7               A7
Mesmo porque não tem graça só se vendo pode crer
   D7                             E7                A7
Acho bonito até o jornaleiro a gritar
    G7    A7    G7                    A7
Imparcial diário        olha o globo
                  B7             E7                  A7
Jornal do povo descobriu outro roubo
D7                                                   E7  A7
E os meninos que vendem derrê sol a cantar

  D7         A7      D7  A7    D7
Derrê   sol  derrê    ê ê     sol
D7           A7     D7     A7     D7
Derrê   sol   derrê    ê ê    sol

      E7                   D7       E7        D7
E fruta lá tem: juçara abricó e buriti
          E7                            D7                 E7         D7
Tem tanja mangaba e manga e a gostosa sapoti
           E7                D7     E7  D7
E o caboclo da maioba vendendo bacuri
                      E7            D7
Tinha tanta coisa pra falar
                              E7                  D7
Quando estava fazendo esse baião
                              E7            D7
Que quase me esqueço de dizer
                                    E7        D7
Que essa terra é tão linda é o maranhão
E7           D7        E7       A7         B7  E7  A7
Hô maranhão  hô maranhão

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#7 = 2 4 2 3 2 2
G7 = 3 5 3 4 3 3
