Alcione - Gostoso Veneno

   Gm                    F
Este amor me envenena
 Eb                                            D7
Mas todo amor  sempre vale a pena
  Gm
Desfalecer de prazer
  A7
Morrer de dor
        Cm                   D7   Gm   D7
Tanto faz eu quero é mais amor

 Gm                      F
Este amor me envenena
 Eb                                            D7
Mas todo amor  sempre vale a pena
  Gm
Desfalecer de prazer
  A7
Morrer de dor
         Cm                 D7     Gm
Tanto faz eu quero é mais amor


F
Água da fonte
                   Bb
Bebida na palma da mão
 D7
Rosa se abrindo
                Gm
Se despetalando no chão
Cm             F    Bb     Eb
Quem não viu e nem provou
G#     D7             G7
Não viveu, e nunca amou
Cm                       F
       Se a vida é curta
                         Gm
E o mundo é pequeno
                                        G#
Vou vivendo  morrendo de amor
                 D7       Gm
Aaaai!  Gostoso veneno

Cm                       F
       Se a vida é curta
                         Gm
E o mundo é pequeno
                                        G#
Vou vivendo  morrendo de amor
                 D7       Gm
Aaaai!  Gostoso veneno

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
G# = 4 3 1 1 1 4
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
