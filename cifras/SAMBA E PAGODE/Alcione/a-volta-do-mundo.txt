Alcione - A Volta do Mundo

Introdução: Fm Gm5-/7 C7 F7 Bbm Eb7 Ab Db7 Gb C7 Fm

 Bbm            Fm
Eta mundo que volta
              Bbm
É a volta do mundo
         Eb7+    Ab7+
É que dói lá no fundo
Dm5-/7 G7        Cm
Eta mundo que volta
              Bbm
É a volta do mundo
        C7     Fm
É questão de segundos
   C7        Fm
A volta do vento
            Cm5-/7
Mudança do tempo
   F7        Bbm
Revolta do mar
             Gm5-/7   C7     Fm
A volta do moço que envelheceu

            Ab                 Db7
A volta da moça que um dia se deu
                               C7
A volta do jogo que muda o placar
         Fm       Bbm
Olha a volta revolta
   C7/9-      Fm  C7
Na porta do bar
  Fm             Bbm
Olha a volta do sol
     C7        Fm
Da chuva e da lua
             Bbm
A volta da noite
        Eb7      Ab7+
Olha a volta do dia
               Gm5-/7
Oh, volta do mundo
      C7        Fm
Que traz a vingança
             C7
A dor, a esperança
            F7
O amor, a verdade
               Bbm
Oh, volta do mundo
     C7        Fm
Só por um segundo
              Db7
Traz ao meu mundo
    C7    Fm
A felicidade

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Ab7+ = 4 X 5 5 4 X
Bbm = X 1 3 3 2 1
C7 = X 3 2 3 1 X
C7/9- = X 3 2 3 2 X
Cm = X 3 5 5 4 3
Cm5-/7 = X 3 4 3 4 X
Db7 = X 4 3 4 2 X
Dm5-/7 = X X 0 1 1 1
Eb7 = X 6 5 6 4 X
Eb7+ = X X 1 3 3 3
F7 = 1 3 1 2 1 1
Fm = 1 3 3 1 1 1
G7 = 3 5 3 4 3 3
Gb = 2 4 4 3 2 2
Gm5-/7 = 3 X 3 3 2 X
