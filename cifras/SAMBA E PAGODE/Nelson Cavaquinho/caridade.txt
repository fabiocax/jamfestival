Nelson Cavaquinho - Caridade


Introdução:
  D7M
Não sei negar esmola
           B7
A quem implora a caridade
     Em7        B7          Em7         A7
Me compadeço sempre de quem tem necessidade
   D7M         F#m7  Fm7 Em7
Embora algum dia eu receba ingratidão
      A7                    D7M        A7
Não deixarei de socorrer a quem pedir um pão
    D7M                    B7
Eu nunca soube evitar de praticar o bem
    Em7      A7             D  Bm7
Porque eu posso precisar também
Em7        A7          D7M            D6   D#º
Sei que a maior herança que eu tenho na vida
Em7     Fº      F#m7/5-      B7
É meu coração, amigo dos aflitos
Em7      Gm6          F#m7          B7
Sei que não perco nada em pensar assim

         Em7           A7         D   A7
Porque amanhã não sei o que será de mim

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
Bm7 = X 2 4 2 3 2
D = X X 0 2 3 2
D#º = X X 1 2 1 2
D6 = X 5 4 2 0 X
D7M = X X 0 2 2 2
Em7 = 0 2 2 0 3 0
F#m7 = 2 X 2 2 2 X
F#m7/5- = 2 X 2 2 1 X
Fm7 = 1 X 1 1 1 X
Fº = X X 3 4 3 4
Gm6 = 3 X 2 3 3 X
