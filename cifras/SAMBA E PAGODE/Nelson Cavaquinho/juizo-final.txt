Nelson Cavaquinho - Juízo Final

   Gm    Ab                        D7
O sol.......há de brilhar mais uma vez
   Cm   Eb7                      D7
A luz.......há de chegar aos corações
   Gm   Ab                   D7
Do mal.....será queimada a semente
   Cm   D7                 Gm    G7
O amor.....será eterno novamente

Cm          D7        Eb7               D7
É o Juízo Final, a história do bem e do mal
Cm                  D7        Eb     Eb7   D7
Quero ter olhos pra ver, a maldade desaparecer

[Solo] Gm  Ab  D7  Cm  Eb7  D7
       Gm  Ab7  D7  Cm  D7  Gm  G7

Cm          D7        Eb7               D7
É o Juízo Final, a história do bem e do mal
Cm                  D7        Eb     Eb7   D7
Quero ter olhos pra ver, a maldade desaparecer


   Gm    Ab                        D7
O sol.......há de brilhar mais uma vez
   Cm   Eb7                      D7
A luz.......há de chegar aos corações
   Gm   Ab                  D7
Do mal.....será queimada a semente
   Cm   D7/F#                Gm
O amor.......será eterno novamente
   Cm   D7/F#                Gm
O amor.......será eterno novamente
   Cm   D7/F#                Gm
O amor.......será eterno novamente

----------------- Acordes -----------------
Ab7 = 4 6 4 5 4 4
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
Eb7 = X 6 5 6 4 X
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
