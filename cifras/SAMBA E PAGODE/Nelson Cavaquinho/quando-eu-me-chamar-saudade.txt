Nelson Cavaquinho - Quando eu me chamar Saudade

Introdução: Gm     A7     Dm     Dm7/C     E7/B     A7     Eb7     D7

            Gm     A7     Dm     Dm7/C     E7/B     A7     Dm     A7

Dm
Sei que amanhã quando eu morrer
                        D7
Os meus amigos vão dizer
             Gm
Que eu tinha bom coração
                    A7
Alguns até hão de chorar
               Gm
querer me homenagear
       A7            Dm
Fazendo de ouro, um violão
D7
Mas depois que o tempo passar

Sei que ninguém vai se lembrar
             Gm
Que eu fui embora

                          Dm
Por isso é que eu penso assim
             Dm7/C         E7/B
Se alguém quiser fazer por mim
    A7    Dm
Que faça agora
   C7
Me dê as flores em vida
    F7
O carinho, a mão amiga
     Bb7         A7
Para aliviar meus ais
  Gm          A7           Dm
Depois que eu me chamar saudade
       Dm7/C      E7/B
Não preciso de vaidade
      A7       Dm
Quero preces e nada mais

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bb7 = X 1 3 1 3 1
C7 = X 3 2 3 1 X
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Dm7/C = X 3 0 2 1 1
E7/B = X 2 2 1 3 X
Eb7 = X 6 5 6 4 X
F7 = 1 3 1 2 1 1
Gm = 3 5 5 3 3 3
