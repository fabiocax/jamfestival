Nelson Cavaquinho - Devias Ser Condenada

Introdução: F7+ Fm C D#° Dm G7/13 C6/9 G7/13

C6/9           D#°         Dm
Devia ser condenada ou crucificada
                G7
Pois juraste falso
Am                   D#°
Beijaste a cruz do Senhor
                Dm6      Gm   C7
E disseste que me tinha amor
F              E7/G#
Quando eu ouço as badaladas
   Am
Do sino daquela igrejinha
F#m7/5b           B7
Julgo-me ainda feliz
                 Em  C7/11 C7
E que és toda minha
F               Fm
E quando vejo a torre bem alta
C                 D#°
Daquela linda catedral

Dm              G7
Fujo da tua amizade
      C6/9  G7/13
Infernal
Am Am/G         F#° E7
Eu vivo tão magoado
    Am Am/G             F#° E7
Não sei viver mais ao teu lado
  Em7/5b          A7/13b    Dm
Só peço à Deus que me dê coragem
            Dm7
Eu preciso esquecer
F#m7/5b            B7               E7
A tua grande mentira, que me faz sofrer
           G7/13 G7
Me faz sofrer

----------------- Acordes -----------------
A7/13b = X 0 X 0 2 1
Am = X 0 2 2 1 0
Am/G = 3 X 2 2 1 X
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
C6/9 = X 3 2 2 3 3
C7 = X 3 2 3 1 X
C7/11 = X 3 3 3 1 X
D#° = X X 1 2 1 2
Dm = X X 0 2 3 1
Dm6 = X 5 X 4 6 5
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
E7/G# = 4 X 2 4 3 X
Em = 0 2 2 0 0 0
Em7/5b = X X 2 3 3 3
F = 1 3 3 2 1 1
F#m7/5b = 2 X 2 2 1 X
F#° = 2 X 1 2 1 X
F7+ = 1 X 2 2 1 X
Fm = 1 3 3 1 1 1
G7 = 3 5 3 4 3 3
G7/13 = 3 X 3 4 5 X
Gm = 3 5 5 3 3 3
