Nelson Cavaquinho - Depois da Vida

G7            C                        E7
Passei a mocidade esperando dar-te um beijo
                  Am       F           E7
Sei que agora é tarde, mas matei o meu desejo
               Dm         G7          C
É pena que os lábios gelados como os teus
              Dm             Dm/C               G7
Nao sintam o calor que eu conservei nos lábios meus

F                        E7
No teu funeral estás tão fria assim
F                       E7
Ai de mim, e dos beijos meus
Dm                      G7
Eu te esperei, minha querida
Dm                        G7
Mas só te beijei depois da vida alan babolin

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Dm/C = X 3 X 2 3 1
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
G7 = 3 5 3 4 3 3
