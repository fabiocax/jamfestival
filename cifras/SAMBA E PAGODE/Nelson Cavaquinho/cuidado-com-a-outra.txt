Nelson Cavaquinho - Cuidado Com a Outra

C   G7           Em
Vou     abrir a porta
             A7      Dm    A7
Mais uma vez pode entrar
Dm  Dm7M         Dm7
É       dia das mães
        Fm G7 C               G7
Eu resolvi te perdoar  (vou abrir)

C            G7         C
Deus me ensinou a praticar o bem
            A7      Dm
Deus me deu essa bondade
E7                       Am
Vou abrir a porta pra você entrar
          Dm
Mas não demore
            G7
Que a outra pode te encontrar

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Dm7M = X X 0 2 2 1
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
Fm = 1 3 3 1 1 1
G7 = 3 5 3 4 3 3
