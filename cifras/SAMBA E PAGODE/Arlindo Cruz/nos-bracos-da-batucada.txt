Arlindo Cruz - Nos Braços da Batucada

[Intro]  C6/9  D/C

Bm7(b5) E7/5+          Am7
Noites a fio sem te vê
             Em7  A7
Meu desafio foi viver
     Dm   Dm/C
Saudade
Bm7(b5) E7           Am7
Agora chega de sofrer
                Em7  A7
Eu nada fiz pra merecer
        Dm7        Em7
Maldade
F7+            Em7        Dm7
Foi quando um amigo lembrou
Gm7                    Bb/C  F7+
Que o samba não pode parar, não.
                          E7/5+   Am7/9
Não é como um caso de amor que acaba
F7+                 Em7  Dm7
Se eu perco no jogo do amor

              Bb/C      F7+
Meu samba é meu protetor sim
                   E7/5+    Am7/9   Dm7 G7
Mentira do trauma da dor, da magoa

Refrão
C6/9
Cansei de esperar por você
Am7
Achei o prazer de sonhar
Dm7                    Dm/C
Não tenho mais tempo a perder
F/G                   G7
Não tenho mais medo de nada
                  Bb/C
Que bom ver o dia nascer
F#m7/5-                   Fm6
Que bom ver o sol despertar
C/E  Ebº          Dm7     G7       C6/9      G7
Nos braços da batucada

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am7 = X 0 2 0 1 0
Am7/9 = X X 7 5 8 7
Bb/C = X 3 X 3 3 1
Bm7(b5) = X 2 3 2 3 X
C/E = 0 3 2 0 1 0
C6/9 = X 3 2 2 3 3
D/C = X 3 X 2 3 2
Dm = X X 0 2 3 1
Dm/C = X 3 X 2 3 1
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
E7/5+ = 0 X 0 1 1 0
Ebº = X X 1 2 1 2
Em7 = 0 2 2 0 3 0
F#m7/5- = 2 X 2 2 1 X
F/G = 3 X 3 2 1 X
F7+ = 1 X 2 2 1 X
Fm6 = 1 X 0 1 1 X
G7 = 3 5 3 4 3 3
Gm7 = 3 X 3 3 3 X
