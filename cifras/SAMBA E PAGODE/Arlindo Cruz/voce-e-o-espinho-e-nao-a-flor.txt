Arlindo Cruz - Você É o Espinho e Não a Flor

[Intro]  F  Dm  Gm  C7  F  Dm  C  Bb
         F  Dm  Gm  C7  F  Dm  C  Bb  F

   C7  F                    Bb
Mexia, mexia com a minha emoção
 F                  D7         Gm  D7
Sabendo que o meu coração era seu
Gm                Eb
Parei com essa ilusão
C             C7       F  C7
Se você quer saber já doeu
     F              Bb
Não vou olhar para trás
 F                     D7    Gm  D7
Jurei nunca mais vou querer você
  Gm  F7
Porque?
   Bb           C
Já decidi vou seguir meu caminho
Am             Dm              Gm
Pra ficar mal parado eu fico sozinho

            C7              Cm  F7
Esperando alguém que mereça amor
   Bb             C
Se no passado fui apaixonado
Am        Dm                   Gm
Já fui enganado hoje ue sou ligado
           C7             F
Você é o espinho e não a flor
Dm    Gm           C7             F
Éeeeee, você é o espinho e não a flor
Dm
A flor
Gm           C7            F
 Você é o espinho e não a flor
Dm    Gm           C7             F
Éeeeee, você é o espinho e não a flor
      Dm
Você é o que?
Gm           C7            F
 Você é o espinho e não a flor
 C                                        F
Por você, não quis mais saber de nenhum romance
 C                                     F            Dm
Eu pensei o seu jeito menina engana bastante meu bem
 Gm                       C7              Cm  D7
Pude ver, você gosta de farra e não me dá valor
 Gm                   C7             F   Dm
Pode crer, você é o espinho e não a flor

Dm    Gm           C7             F
Éeeeee, você é o espinho e não a flor
Dm
A flor
Gm           C7            F
 Você é o espinho e não a flor
Dm    Gm           C7             F
Éeeeee, você é o espinho e não a flor
      Dm
Você é o que?,
Gm           C7            F
 você é o espinho e não a flor

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
Gm = 3 5 5 3 3 3
