Arlindo Cruz - Hoje Tem Samba

(Tom) A
(Introdução) A  E  E7  A  E  F#m7  E7

   E7                    Bm
.....A minha vida anda triste
E                   A
Tô brigado com meu bem
  F#m            Bm
Mas tenho um palpite
E7                 Em  A7
Que vai ficar tudo bem
                  D
Hoje é Domingo de sol
            C#m     F#m
É dia de namorar, oi
      Bm
Tem samba
             E7
Então abre a roda
               A
Pro meu bem sambar

     E7                   A     F#m
Abre a roda que hoje tem samba
       Bm        E7      A      F#m   Bm
Abre a roda que hoje tem samba, amor
E7     A     F#m  Bm
Tem samba de amor
 E7         A  E7
.....Vem sambar
                           D#°
Entra na roda de samba, mulher
Dm7                   C#m7
...Faz um denguinho gostoso pra mim
F#m7          G#m7
Eu prometo te dar meu axé
                 F#m
A nossa vida será um jardim
                  D#°
Só minha rosa encantada
     Dm7           C#m7  F#m7  Bm
Tem o dom de perfumar...o samba
            E7                    A
Então abre a roda pro meu bem sambar
 E7                D#°
...Entra na roda de samba, meu bem
 Dm7                   C#m7
.....Que logo mais eu te dou cafuné
F#m7                      G#°
Mostra o molejo da Vila Vintém
                          F#m
Faz o gingado na ponta do pé
                    D#°
Lança um olhar de pecado
Dm7           C#m F#m     Bm7
Hoje eu vou me acabar no samba
            E7                    A
Então abre a roda pro meu bem sambar

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
D#° = X X 1 2 1 2
Dm7 = X 5 7 5 6 5
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
E7/4/9 = X X 2 2 3 2
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
G#m7 = 4 X 4 4 4 X
G#° = 4 X 3 4 3 X
