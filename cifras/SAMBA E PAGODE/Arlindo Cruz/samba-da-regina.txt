Arlindo Cruz - Samba da Regina

Basicamente segue a sequência  F  D7  Gm  C7

Gm   F
Alô regina!
             D7               Gm  C7
É tão gente fina que sabe chegar
                F           (Daqui pra frente repete)
Em qualquer esquina
Lá na cobertura, na laje ela está
É quem domina.
Porque tem a sina de ser popular... alô
Alôôôô rainha
Se vai ter churrasco, feijão, vatapá
Vai pra cozinha.
Tem coisa gostosa de todo lugar
Traz a farinha!
O camarão seco, o jambu eo fubá
   F
E faaaaaaz verão
            D7  ... (Aí repete a sequência)
E hoje é domingo

Dia que o povão... agita!
Se liga, se encontra, faz conexão, twita
Ou pra se dar bem,
                        F
Ou pra botar alguém na fita.
             Gm    C7               F  D7
Bateria arrebenta, todo mundo comenta,
             Gm                  C7       F   D7
Que feito pimenta, o programa domingo esquenta. (2x)
Gm              C7                 F   D7
   Regina de janeiro, fevereiro e março... (2x)
Alô, alô...

----------------- Acordes -----------------
