Arlindo Cruz - Boca Sem Dente / Bagaço da Laranja

 F

D  D7        G#m5-/7      Gm6
....Laiá, laiá........laiá
     F#m7       Fº
Laiá, laiá....laiá
       Em7          A7      Am  D7
Laiá, laiá, laiá, lalaiá, laiá
              G#m5-/7      Gm6
....Laiá, laiá........laiá
      F#m7      Fº
Laiá, laiá....laiá
       Em7          A7      D
Laiá, laiá, laiá, lalaiá, laiá
                A7
Aquela boca sem dente!
  D               A7            D
**Aquela boca sem dente que eu beijava
   B7         Em     C7  B7
Já está de dentadura
Em            Em/D         A7
Aquela roupa velha que você usava

Em7  A7         D             A7
Hoje é pano de chão....mas eu mandei!
Bm    C#7              F#m7  B7
Mandei.....reformar o barraco
            Em   A7       Am       B7
Comprei geladeira...e televisão e você!
  E7/9      A7          F#m5-/7      B7
E você...me paga com ingratidão...e você!
  E7/9      A7              D                    A7
E você...me paga com ingratidão...aquela boca sem dente!

(Volta no **)

F#m7  Fm7  Em7                A7
...........Mas o que mais me revolta
               D
É que não reconhece o que fiz por você
Am            D7                G7+                G6
Obra da fatalidade eu ser desprezado sem saber porque
        G#m5-/7    Gm6            F#m7   Fº
Você zombou de mim.....só fez me aborrer
       Em7                  A7    D      D7
Sinceramente eu hei de lhe ver sofrer, sofrer
        G#m5-/7    Gm6            F#m7   Fº
Você zombou de mim.....só fez me aborrer
       Em7                  A7    D   D    F
Sinceramente eu hei de lhe ver sofrer
          C7              F
Fui num pagode acabou a comida          }
           D7               Gm7
Acabou a bebida, acabou a canja         }
            C7                 F           (Refrão) (Bis)
Sobrou pra mim...o bagaço da laranja    }
D7         Gm7       C7        F
Sobrou pra mim...o bagaço da laranja    }
       D7          Gm7
Você sempre foi solteiro, Sombrinha
     C7          F                 D7
Uma nega não arranja eu falei pra você
            C7                 F
Sobrou pra mim...o bagaço da laranja
D7         Gm7       C7        F
Sobrou pra mim...o bagaço da laranja
D7              Gm7
cuidade cumpadre Arlindo
        C7           F                  D7
Que a polícia já te manja e falei pra você
            C7                 F
Sobrou pra mim...o bagaço da laranja
D7         Gm7       C7        F
Sobrou pra mim...o bagaço da laranja
      D7           Gm7
Me disseram que no céu
             C7       F           D7
A mulher do anjo é a anja...vejam só
            C7                 F
Sobrou pra mim...o bagaço da laranja
D7         Gm7       C7        F
Sobrou pra mim...o bagaço da laranja
D7                  Gm7
Vem no suingue o cumpadre
        C7         F                    D7
Que de banjo você manja...eu falei pra vocÊ
            C7                 F
Sobrou pra mim...o bagaço da laranja
D7         Gm7       C7        F
Sobrou pra mim...o bagaço da laranja

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C#7 = X 4 3 4 2 X
C7 = X 3 2 3 1 X
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E7/9 = X X 2 1 3 2
Em = 0 2 2 0 0 0
Em/D = X X 0 4 5 3
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
F#m5-/7 = 2 X 2 2 1 X
F#m7 = 2 X 2 2 2 X
Fm7 = 1 X 1 1 1 X
Fº = X X 3 4 3 4
G#m5-/7 = 4 X 4 4 3 X
G6 = 3 X 2 4 3 X
G7+ = 3 X 4 4 3 X
Gm6 = 3 X 2 3 3 X
Gm7 = 3 X 3 3 3 X
