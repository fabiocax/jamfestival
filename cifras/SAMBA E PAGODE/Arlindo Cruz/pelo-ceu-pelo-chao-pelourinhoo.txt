Arlindo Cruz - Pelo Céu, Pelo Châo, Pelourinhoo

Introdução: E B7 E
        C#7            F#m7
Quer sambar a noite inteira
                  B7
Vem sambar no Pelourinho
                  E
Samba não é brincadeira
       B7          E
Pra sambar no Pelourinho
     C#7          F#m7
Se você vem da Ribeira
                   B7
Pra sambar no Pelourinho
               E
Escorrega na ladeira
        B7          E
Cai não cai no Pelourinho

     C#7         F#m7
Rola samba de primeira
                 B7
Samba bom é no Pelô

                  E
Não é de marcar bobeira
       B7           E
Quem faz samba no Pelô
     C#7          F#m7          B7
Partideiro abre a roda, iaiá, ioiô
                      E            B7           E
Quando o samba é no Pelô (quando o samba é no Pelô)
     C#7            F#m           B7
Partideiro entra na roda, iaiá, ioiô
                      E
Quando o samba é no Pelô

C#7              F#m7
  O Pelô é o que a gente quer
B7              E
Homem, mulher, sem distinção
C#7              F#m7
Samba no pé e na palma da mão
B7                 E         C#7
Trazendo axé pelo céu, pelo chão

           F#m7
O Pelô é a força da fé
B7              E
Mostra o que é, religião
      C#7                   F#m7
É uma reza, é uma prece e é uma oração
B7            E
Feita com devoção
    C#7                 F#m
E depois escolher samba sim, samba não
B7                E
Viva a primeira opção

E7                A
  Todo mundo que vem pra rezar
A#°            E             C#7
Se abençoar e até saravá sua banda
F#m            B7            E
Sabe que pelouriar é cair no samba

----------------- Acordes -----------------
A = X 0 2 2 2 0
A#° = X 1 2 0 2 0
B7 = X 2 1 2 0 2
C#7 = X 4 3 4 2 X
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
