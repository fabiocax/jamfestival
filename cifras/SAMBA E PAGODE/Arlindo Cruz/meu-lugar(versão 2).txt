Arlindo Cruz - Meu Lugar

Am                  D                             Dm        G7
O meu lugar É caminho de Ogum e Iansã Lá tem samba até de manhã
              C        Bm E7
Uma ginga em cada andar
Am                    D                         Dm           G7
O meu lugar É cercado de luta e suor Esperança num mundo melhor
                Gm      C7
E cerveja pra comemorar
Fm                           C7M        Am
O meu lugar Tem seus mitos e Seres de Luz
                 D                          Bm      E7
É bem perto de Osvaldo Cruz, Cascadura, Vaz Lobo e Irajá
Am                    D                         Dm        G7
O meu lugar É sorriso é paz e prazer O seu nome é doce dizer
       C   Ebº       Dm G7       C   Ebº    Dm Bm E7
Madureira, lá lá laiá,  Madureira, lá lá laiá
Am                     Em
Ah meu que lugar A saudade me faz relembrar
             Bm            E7              Am  Bm E7
Os amores que eu tive por lá É difícil esquecer
Am                        Em
Doce lugar Que é eterno no meu coração

                B7                              E
E aos poetas trás inspiração pra cantar e escrever

A                             C#7
Ai meu lugar Quem não viu Tia Eulália dançar
                    D               E7       A          Em A7
Vó Maria o terreiro benzer e ainda tem jongo à luz do luar
Dm         G7                  C            Am          Dm              G7
Ai meu que lugar tem mil coisas pra gente dizer O difícil é saber terminar
       C   Ebº      Dm   G7    C  Ebº     Dm  G7      C
Madureira, lá lá laiá, Madureira, lá lá laiá, Madureira
          Em                  Dm            G7      C
Em cada esquina um pagode num bar     Em Madurei...ra
          Em                    Dm           G7    Gm
Império e Portela também são de lá    Em Madurei...ra
        C7              F7+              G7                   Em
E no Mercadão você pode comprar Por uma pechincha você vai levar
                                 Dm           G7   Gm
Um dengo, um sonho pra quem quer sonhar Em Madureira
         C7                 F7+                G7                  Em
E quem se habilita até pode chegar Tem jogo de lona, caipira e bilhar
                        Dm          G7    Gm
Buraco, sueca pro tempo passar Em Madureira
      C7                F7+                G7             Em7
E uma fezinha até posso fazer No grupo dezena centena e milhar
                        Dm           G7   Gm
E nos 7 lados eu vou te cercar em Madureira
        C7        F7+            G7          C         C7        F7+            G7   C   B7   Dm    G7    C
E lalalaia  lalaia lalaia e lalalaia lalaia... E lalalaiala laia la la ia...Em madureira lá laiá... em    madureira. (Obs:tocar uma vez esses dois últimos acordes)

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C#7 = X 4 3 4 2 X
C7 = X 3 2 3 1 X
C7M = X 3 2 0 0 X
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
Ebº = X X 1 2 1 2
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F7+ = 1 X 2 2 1 X
Fm = 1 3 3 1 1 1
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
