Arlindo Cruz - O Amor

[Intro] Am7  Bm7  Am7  Bm7

Am7                     Bm7
Agora vou seguir o meu caminho
Am7                Bm7
Como Deus quiser, como a vida quer
Bm7/5-                       E7/9-       E7/5+
Por mais que eu te ame eu não volto atrás
Am7                               C/D
Vou sofrer muito mais, se ficar por aqui
Am7                              Bm7
Eu sei que o amor é bom, mas tem espinho
Am7                     Bm7
Quem não sabe amar, é melhor nem tentar
Bm7/5-                      E7/9-   E7
Destino diz pra mim, guarde o carinho
Am7                            C7+        C/D
Deixa o tempo correr, que eu mando pra você
Bm7                    E7/9-
O amor entenda o meu jeito
Am7                       Cm7
O amor que tenha meus defeitos

Bm7                    E7/9-
O amor de todos o perfeito
A/C#                    Cm7
O amor que dure no meu peito
Bm7                  E7/9-
O amor tenha o meu jeito
Am7                           Cm7
O amor que entenda meus defeitos
Bm7                        E7/9-
O amor mas tem que ser direito
A/C#                    Cm7
O amor se não eu não aceito

----------------- Acordes -----------------
A/C# = X 4 X 2 5 5
Am7 = X 0 2 0 1 0
Bm7 = X 2 4 2 3 2
Bm7/5- = X 2 3 2 3 X
C/D = X X 0 0 1 0
C7+ = X 3 2 0 0 X
Cm7 = X 3 5 3 4 3
E7 = 0 2 2 1 3 0
E7/5+ = 0 X 0 1 1 0
E7/9- = X X 2 1 3 1
