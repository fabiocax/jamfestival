Arlindo Cruz - Vou Te Deixar No Sereno

C7 F C7
Introdução: F
   F                  C7         F  D7     Gm     C7  F
Esta noite eu vou lhe deixar no sereno no sereno no sereno
  D7        Gm             C7        F
Tenho mil razões para te deixar no veneno
 D7                            Gm
Minha preta , o que queres de mim
                   C7                F
Se não for muita coisa , poderei te dar
 Cm                 F7
Banho de agua de cheiro
                              Bb
Embaixo do chuveiro pra te perfumar
Te quero  que te quero quero
           Bbm
Dançando bolero e um samba-canção
              D7
O que eu nao quero é ouvir lero lero
             G7        C7       F
Porque não tolero esta malcriação

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bbm = X 1 3 3 2 1
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
