Arlindo Cruz - Depois do Temporal / Ladeira do Chapelão/ Nega o Morro

Intro: A7+ Am6 A7+ Am6 E7

   A                      Bm  E7            A  F#7
Depois do temporal a lua cheia, passeou no céu
   Bm           E7      A        E7
No céu, a lua cheia passeou, passeou
   A                      Bm  E7            A  F#7
Depois do temporal a lua cheia, passeou no céu
   Bm           E7      A        E7
No céu, a lua cheia passeou, passeou  2X

                 A     F#7               Bm
E São Jorge Guerreiro, com sua lança de prata
                    E7                         A  E7
Nos livra da vida ingrata, e de tudo quanto é mau
                A           A7          D
Nos livra da enchente, da inveja dessa gente
E                   A            E7           A
E quem não anda contente, tenha seu lugar ao sol
          F#7     Bm       E   A   E7
Depois de que ? depois do temporal


  A       F#7           Bm        E7                     A  E7
Parece que ela não conhecia, a ladeira do morro do chapelão
  A       F#7           Bm        E7                     A          E7
Parece que ela não conhecia, a ladeira do morro do chapelão  ( se ralou  )
     A                  Bm
Se ralou vai arder, se ralou vai arder
     E7                     A  (E7)
Se ralou vai arder seu coração  2X

   E7            A                     Bm
Dizem que dona Maria uma senhora de respeito
                 E7                    A
Escorregou na ladeira e ficou daquele jeito
                     A7                       D
Um malandro que eu conheço, de um morro considerado
       E7           A        E7            A
Quando vai lá na ladeira dá tiro pra todo lado
                 E7
E ainda fica invocado
  A                  Bm
Se ralou vai arder, se ralou vai arder
     E7                     A  (E7)
Se ralou vai arder seu coração  2X

 A                   Bm      E7                     A
Olha o seu Pedro no morro, o quê que eles esta procurando
  F#7              Bm           E7        A
A nega pescoço de ganso, com a cara de cachorro
       F#7       Bm           E7             A
Quem souber por aqui, a onde mora o Paulo Brasão
       F#7         Bm                  E7            A
Ele vendeu a minha nega por cinco mil réis, em um leilão
             F#7
Quem quer comprar...

            Bm         E7          A
Quem quer comprar, uma nega lá no morro
F#7          Bm         E7          A
Quem quer comprar, uma nega lá no morro
     F#7               Bm        E7         A
Ela tem um pescoço de ganso, uma cara de cachorro
      F#7          Bm          E7            A
Quanto é que você paga, eu te dou cinco mil réis
     F#7              Bm                    E7             A
Mas cinco mil réis é pouco cumpadre, tenho outro que paga dez
         F#7             Bm
Dez mil réis também não chega
             E7           A                 F#7           Bm
Me da vinte mango...leva nega...me da vinte mango...leva nega...
             E7           A
Me da vinte mango...leva nega...

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
Am6 = 5 X 4 5 5 X
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#7 = 2 4 2 3 2 2
