Arlindo Cruz - Fora de Ocasião

(Tom) Em
(Introdução) Dm G7 C7+ C6 Am7 B7

Em7                         B7/D#
Acho que a saudade já te encontrou
        Dm           G7         C7+     C6
Te disse como estou, mas deixa  como está
Am7         A#°               Em7
Quando a solidão desperta o desamor
         F#m5-         B7  Em7              B7
Se quer falar de amor, não dá, ( ô mas eu acho )
              E             G#7
Depois do temporal o sol que vai brilhar
           Bm7 E7          A7+
Pode não apagar o que aconteceu
Am7         D7          G7+     Em7
Tanta escuridão pode tornar  em vão
             F#7            C7     B7
A luz que acendeu fora de ocasião
E                  G#7
Teu navio quis abandonar meu cais

      Bm7        E7        A7+   F#m7  Gm7  G#m7  Am7
Não adianta mais, correr atrás assim
              D7       G#m7    C#7
Pois o nosso amor já chegou ao fim
F#7             B7       E             Bm7   E7
E não venha procurar por mim, diz outra vez
A7+          Am7   G#m7       C#7
Pois o nosso amor já chegou ao fim
      F#7         B7    Em7               B7
E não venha procurar por mim  ( mas eu acho )

----------------- Acordes -----------------
A#° = X 1 2 0 2 0
A7+ = X 0 2 1 2 0
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
B7/D# = X X 1 2 0 2
Bm7 = X 2 4 2 3 2
C#7 = X 4 3 4 2 X
C6 = 8 X 7 9 8 X
C7 = X 3 2 3 1 X
C7+ = X 3 2 0 0 X
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F#7 = 2 4 2 3 2 2
F#m5- = 2 X 4 2 1 X
F#m7 = 2 X 2 2 2 X
G#7 = 4 6 4 5 4 4
G#m7 = 4 X 4 4 4 X
G7 = 3 5 3 4 3 3
G7+ = 3 X 4 4 3 X
Gm7 = 3 X 3 3 3 X
