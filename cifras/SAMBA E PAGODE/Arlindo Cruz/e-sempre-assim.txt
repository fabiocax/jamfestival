Arlindo Cruz - É Sempre Assim

Introdução: G7+ Bm5-/7 E7 A7 D7 G7+ E7

Am                   D7
É sempre assim, tem dias que você me trata bem
 G7M      G6
Saudade de mim
G7M                  E7
Buscando aquilo que você não tem,
Am                   Cm                     D7
É sempre assim, tem noites que eu não sei o que é
dormir
 G7M      G6
Saudade de mim
       Dm            G7
Tem noites que você está aqui
C7M        C6        C#°              F#7
É sempre assim, num segundo vira as costas, vai embora
    Bm
Saudade de mim,
    Bm5-/7            E7
Me deseja me procura toda hora

Am          E7        Am
O que fazer ah! Se eu tenho você
D7  G7M       G6   Dm G7
E parece que não
C7M        C6   C#°                  F#7
É sempre assim, me despreza, me entristece, o teu desprezo
    Bm
Saudade de mim
      Bm5-/7                    E7      Am     E7
Que loucura me dá tudo que eu mereço, na indecisão
  Am         D7    G7M
Balança o meu coração
D7  G7M             D          Bm5-/7
  É hora de toda tristeza se acabar
  E7       Am     D7   (G  G#°       Am7 D7)*
Teu coração é o meu lugar.....meu lugar
  G7M              D         Bm5-/7
Queria com toda certeza te entregar
   E7    Am      D7        (G  G#°    Am7  D7)*
Meu coração, te amar, te amar......é hora

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Bm = X 2 4 4 3 2
Bm5-/7 = X 2 3 2 3 X
C#° = X 4 5 3 5 3
C6 = 8 X 7 9 8 X
C7M = X 3 2 0 0 X
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
F#7 = 2 4 2 3 2 2
G = 3 2 0 0 0 3
G#° = 4 X 3 4 3 X
G6 = 3 X 2 4 3 X
G7 = 3 5 3 4 3 3
G7+ = 3 X 4 4 3 X
G7M = 3 X 4 4 3 X
