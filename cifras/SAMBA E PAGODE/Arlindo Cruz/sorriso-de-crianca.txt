Arlindo Cruz - Sorriso de criança

         G6(9)    C7M            Bm7
  SORRIA MAIS CRIANÇA, PRA NÃO SOFRER
E7(13b)   Am7(9)  D7       B7(13) ^ B7(13b) ^ E7(9#)
  EU VI VOCÊ CRIANÇA NO ALVORECER
            Am7       D7           B7(13)  E7
  O SOL SE ABRIU AO ENCANTO DE UMA FLOR
       A7(9)        D7(13)      G7M
  REALIZANDO O SONHO DE UM SONHADOR

Em7         Am7  D7        G7M
  EU ME EMBALEI, EU ME EMBALEI
Em7          Am7(9)       D7(13)
  NOS BRAÇOS TEUS CRIANÇA, EU ME EMBALEI

----------------- Acordes -----------------
A7(9) = 5 X 5 4 2 X
Am7 = X 0 2 0 1 0
Am7(9) = X X 7 5 8 7
B7(13) = X 2 X 2 4 4
Bm7 = X 2 4 2 3 2
C7M = X 3 2 0 0 X
D7 = X X 0 2 1 2
D7(13) = X 5 X 5 7 7
E7 = 0 2 2 1 3 0
E7(9#) = X 6 5 6 7 X
Em7 = 0 2 2 0 3 0
G6(9) = X X 5 4 5 5
G7M = 3 X 4 4 3 X
