Arlindo Cruz - Entrevista

Introdução: D

D                             Bm
Sofri uma entrevista esta manhã
     Em                 A7
Quem é....o João Nogueira
                     D                       A7
nada mais que um operário, compositor de primeira
D                B7                   Em
Se eu sou um operário e me orgulho de ser
                    A7
Eu componho meus pagodes....na hora do meu lazer
D                B7                 Em
Esse gênio que João...bate bem na viola
                       A7
Que tal Nelson Cavaquinho e Paulinho da Viola
D              B7                      Em
Agradeço o louvor, não sou prosa nem cabola
                     A7
Sou igulai Babau, Maroni e o saudoso Cartola
D                      B7                         Em
Essas linhas que escrevi, pra te exaltar grande bamba

                    A7
Para que cante do país e do teu clube do samba
D                  B7                        Em
Olha aí mestre Aniceto, se estou perto do senhor
                     A7
Cada vez fico mais certo, que a cor negra é um amor
 D                 B7                      Em
Eu não sou mistiicado, o meu canto é essa coisa
                  A7
Que orgulha meu país terra de Nosso Senhor..

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
