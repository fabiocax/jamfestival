Os Travessos - Gatinha

Introdução: A7+  A6  A7+  A6 C° F#m7 Bm7 E7

A7+          C#m7             Em7/9
Vem gatinha........vem comigo....
           A7    A7  D7+        Dm/E
Que eu tenho muito amor pra dar.............
A7+          C#m7            Em7/9
Hoje eu sou.........seu amigo....
        A7       A7    D7+            Dm/E
Mas não é só nisso que eu quero ficar............
A7+                   E
Adoro quando eu te ligo.......
        E
Você me fala oi gatinho!......
A7                 A7     D7+  Dm/E
Isso pra mim ja é ganhar o dia..............
A7+              E           E
Mas você só quer ficar comigo.......
         E
Ainda por cima escondido.......
A7                     A7   D7+   Dm
É bom mas não é isso que eu queria.....


Parte 2:

D7+                C#m7      F#m7
Não sei que tanto medo você tem
               C#m7        Bm7
Gatinha eu só quero o seu bem
    E7/9         A7+  C#m7  Bm7  E7/9  A7  D7+
E quero te mostrar.......................Que
                C#m7        F#m7
Viver bem não é olhar pra traz
               C#m7             F#m7
E aquela noite foi simples demais
           C#m7            Bm7          E7/9
Mas é tão pouco pra me contentar vou te falar

Refrão:

   A7+                 E/G#           F#m7
Agradeço.....Por aquelas horas te tanto prazer
               C#m7        D7+
Sobre a luz da lua só eu e você
    C#m7       Bm7   A       G     E/G#
...Tão escondidinhos pra ninguem nos ver.....
     A7+              E/G#            F#m7
Agredeço.....Ja sabendo que eu devo me calar
                 C#m7           D7+       C#m7   Bm7     A
Mas confesso que a vontade é de grita......a......a......a
G              E/G#        A7+
Que estou apaixonado por você!

----------------- Acordes -----------------
A = X 0 2 2 2 0
A6 = 5 X 4 6 5 X
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
C° = X 3 4 2 4 2
D7+ = X X 0 2 2 2
Dm = X X 0 2 3 1
Dm/E = X X 2 2 3 1
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E7 = 0 2 2 1 3 0
E7/9 = X X 2 1 3 2
Em7/9 = X 7 5 7 7 X
F#m7 = 2 X 2 2 2 X
G = 3 2 0 0 0 3
