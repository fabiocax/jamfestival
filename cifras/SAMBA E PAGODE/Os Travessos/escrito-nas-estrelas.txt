Os Travessos - Escrito Nas Estrelas

Intro: E G#m7 A C#7 F#m B7

                   E7+    B/D#
esta escrito nas estrelas
                C#m7    Bm-E7/9
é lindo o nosso amor
                 A7+    B/A
uma paixão tão verdadeira
                G#m  C#7
é lindo o nosso amor
                F#m   A/B
um sentimento infinito
   B/A     G#m - C#7
no meu interior
                   F#m  G#m - Am
e não há nada mais bonito
    B7               E %% B7 %%
que o nosso amor
 *                     E7+    B/D#
Eu vejo a paz no teu sorriso
                 C#m7    Bm - E7/9
é lindo o nosso amor

                  A7+    B/A
você é tudo que preciso
                G#m  C#7
é lindo o nosso amor
                      F#m   A/B
vou com você a vida inteira
   B/A   G#m - C#7
viver, sonhar
                    F#m  G#m - Am
está escrito nas estrelas
  B7                 E7+   Bm - E7/9
eu vou te amar
   A7+
Que noite linda
    B/A
sonho acordado
  G#m7
seja bem vinda
  C#7
fica ao meu lado
 F#m             B7
sinceramente estou
        E7+   (Bm-E7/9) VOLTA NO REFRÃO
apaixonado
introdução pra voltar no *

E G#m7 A F#m B7

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/B = X 2 2 2 2 X
A7+ = X 0 2 1 2 0
Am = X 0 2 2 1 0
B/A = X 0 4 4 4 X
B/D# = X 6 X 4 7 7
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C#7 = X 4 3 4 2 X
C#m7 = X 4 6 4 5 4
E = 0 2 2 1 0 0
E7+ = X X 2 4 4 4
E7/9 = X X 2 1 3 2
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
G#m7 = 4 X 4 4 4 X
