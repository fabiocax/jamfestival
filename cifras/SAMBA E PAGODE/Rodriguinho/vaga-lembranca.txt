Rodriguinho - Vaga Lembrança

(intro) Fmaj7(9)  Bb7sus  A7sus  Emaj7

F7+
Eu não sei por que voce ligou
Em                              Am7  Em7
Se era pra esquecer tudo que eu sentia
F7+                     Dm
Eu não sei por que voce ligou
                 Em                   Gm7  C7/13
Brincou não deu valor quando eu mais queria

F7+
Ta virando palhaçada
G/B
Eu não quero mas ser seu brinquedo não
Em7^^Am7                       Em7  Am7
Tudo ou nada esse é o preço da sua indecisão
F7+
Que mancada
Dm                       Em                       C7/13      C7+
É melhor voce parar, eu não quero te xingar mas cansei

F7+
Voce é uma vaga...
                                Em7  Am7
Simplesmente uma vaga lembrança, filha de uma pura ilusão
       Em7
Que não me engana mais
F7+                        Dm
Vaga... simplesmente uma vaga lembrança
 Em7                        Am7
E não adianta mais correr atras   (2x)

( Fmaj7/9  Bb7/5b  Am7/9/11 )

----------------- Acordes -----------------
A7sus = X 0 2 0 2 0
Am7 = X 0 2 0 1 0
Am7/9/11 = 5 3 5 4 3 X
Bb7/5b = X 1 2 1 2 X
Bb7sus = X 1 3 1 3 1
C7+ = X 3 2 0 0 X
C7/13 = X 3 X 3 5 5
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
Emaj7 = X X 2 4 4 4
F7+ = 1 X 2 2 1 X
Fmaj7(9) = X 8 7 9 8 X
Fmaj7/9 = X 8 7 9 8 X
G/B = X 2 0 0 3 3
Gm7 = 3 X 3 3 3 X
