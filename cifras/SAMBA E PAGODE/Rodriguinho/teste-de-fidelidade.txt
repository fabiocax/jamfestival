Rodriguinho - Teste de Fidelidade

          D7+                  C#m           D7+
Já não sei como te olhar depois do que aconteceu
                     A7+
Sei que me deixei levar
D7+                  C#m                         D7+
Sei que deve estar pensando que o culpado fui eu, quem
                     A7+
Mandou vir me testar...
 D7+                C#m               F#m      Em A7
Mandando sua amiga aqui, só porque não a conhecia, pedindo
    D7+          C#m              F#m7            Em A7
Pra ela me seguir e te contar tudo o que eu fazia
   D7+                   C#m             F#m      Em
Mas o destino   te traiu,   ela se apaixonou por mim
G#m7/5-            C#7              A/D
Sei que não esperava que seria assim...
           D7+
E hoje ela me faz feliz
C#m          F#m                   Em     A7
Descobriu que eu sou o homem certo pra viver
  D7+   C#m
Infeliz   de você

   F#m                  Em A7
E agora venho aqui te agradecer
   D7+  C#m
Tô feliz   sem você
F#m         C#m
 Sempre quis conhecer
F7+                           Bm
Tanto procurou que encontrou
  E7
Meu verdadeiro amor...

 E
eh isso ai galera

----------------- Acordes -----------------
A/D = X X 0 6 5 5
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
Bm = X 2 4 4 3 2
C#7 = X 4 3 4 2 X
C#m = X 4 6 6 5 4
D7+ = X X 0 2 2 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
F7+ = 1 X 2 2 1 X
G#m7/5- = 4 X 4 4 3 X
