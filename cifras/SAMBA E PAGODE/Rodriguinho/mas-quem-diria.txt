Rodriguinho - Mas Quem Diria

Introdução: A7+ % D/E)

A7+   C#m7         F#m7
preciso confessar meu bem
       C#m7         D7+        E/G#           Bm7     Esus
meu coração tá sem ninguém...precisando de você...
A7+   C#m7            F#m7
eu já tentei deixar pra lá
       E/A            D7+
não consegui quero voltar...
    E/G#                                Bm7                     Esus  ^^  F/G
você me quer também,não penso em mais ninguém,então por que não vem...
C7+                                      Em7
eu vou fazer do jeito que você sempre quis
  F7+            Em7                 Dm7        F/G
conheço os meus defeitos,mais posso te fazer feliz
C7+                                     Em7
mais quem diria um dia eu ter que te emplorar...
   F7+                        Em7
em ter os teus carinhos só mais uma vez
   F7+                      Em7^Em7^Em7^Em7
beijar a tua boca como eu beijei

Dm7            %
te tocar,me entregar,te amar sem parar...
        Am7                       Dm7          Gsus         C7+          Bm7    D/E
coisas que não faço com ninguém,coisas que você me faz tão bem...   ( com ninguém ... )

 (voltar no início )

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7+ = X 0 2 1 2 0
Am7 = X 0 2 0 1 0
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
C7+ = X 3 2 0 0 X
D/E = X X 2 2 3 2
D7+ = X X 0 2 2 2
Dm7 = X 5 7 5 6 5
E/A = X 0 2 1 0 0
E/G# = 4 X 2 4 5 X
Em7 = 0 2 2 0 3 0
Esus = 0 2 2 1 0 0
F#m7 = 2 X 2 2 2 X
F/G = 3 X 3 2 1 X
F7+ = 1 X 2 2 1 X
Gsus = 3 2 0 0 0 3
