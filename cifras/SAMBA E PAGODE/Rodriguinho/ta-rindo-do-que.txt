Rodriguinho - Ta Rindo do Que

Intro: G7+  Cm  G7+  Cm  G7+  Cm

G7+                     Cm
Quero te contar que encontrei sua garota
G7+                     Cm
Numa atitude que eu nem consigo descrever
G7+                     Cm
Ela estava acompanhada com um cara que eu nunca vi
G7+              Dm          G7
E o Thiago pode contar pra você!
C7+                   A                  Am             Bm
É Dézinho , é complicado mas como somos amigos minha obrigação é te contar
           C7+
O que você vai fazer?
           A
O que vai acontecer?
             Am      D7
Eu não quero nem sonhar
G7+                     Cm                        G7+                  Cm
Se foi ontem tenho idéia sei que ela estava com a família reunida numa festa, num jantar
G7+                    Cm                   G7+
Eu confio nela não acredito que por uma aventura

                 Dm       G7
Nessa altura ela iria me trocar
C7+                       A                          Am
Eu sei que vocês são meus amigos que só querem o meu bem
                      Bm
Por isso que eu vou escutar
                C7+
Sei que não são de inventar
                A
Não querem me ver chorar
                Am      Cm F7
Ver ninguém me enganar
Bb7+
Tudo bem negrão
                         Am5-/7        D7
Eu vi sua garota com outro rapaz (com outro rapaz)
        G7+
De mãos dadas negrão
                        F#m5-/7 B7
A cena que eu vi pra mim ja é demais
        Em
Ele era alto?
        A
Ele era sim
        Em
Moleton vermelho?
        A
Eu acho que sim
        Em
Cabeça raspada?
        A
Ele tinha sim
          Am           Bm           D7
É o mesmo cara que ela apresentou pra mim (hahaha)
G7+                   D/F#
Você tá rindo de que? não é mentira não!
Em                 Em/D
Eu falei pra você, não conta pra ele não
C7+                      G/B
Eu tô rindo porque eu conheço esse cara aí
Am                        D7
É o irmão mais velho dela que ela gosta de sair
G7+               D/F#
Como eu ia saber que ele era irmão?
Em                    Em/D
Eu não quis nem saber desculpa ai então
C7+                     G/B
Tudo bem vocês são meus amigos isso eu já sei
Am                    D7                        G7+
E confio muito na garota que eu me apaixonei

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Am5-/7 = 5 X 5 5 4 X
B7 = X 2 1 2 0 2
Bb7+ = X 1 3 2 3 1
Bm = X 2 4 4 3 2
C7+ = X 3 2 0 0 X
Cm = X 3 5 5 4 3
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
Em/D = X X 0 4 5 3
F#m5-/7 = 2 X 2 2 1 X
F7 = 1 3 1 2 1 1
G/B = X 2 0 0 3 3
G7 = 3 5 3 4 3 3
G7+ = 3 X 4 4 3 X
