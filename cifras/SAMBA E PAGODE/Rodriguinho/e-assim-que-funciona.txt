Rodriguinho - É Assim Que Funciona

   C                       Am
Eu já perdi coisas nessa vida
                              F
E aprendi a ser de uma mulher só
                               Fm7
Com a traição o perdão vai embora
                G
E voce nunca, vai ser melhor
C                             Am
A omissão é uma questão de tempo
                                    F
A verdade vem, depois não tem mais jeito
                                Fm7
E já não tem, mais pra onde correr
             G
E niguem pra te socorrer
C
Se ela é ciumenta, muito chato isso é normal
Am                                                F                     Fm7
Se ela foi escolhida, é porque é especial e não adianta, outra querer falar
                       G
Sua historia não vai mudar

C
Se ela fala muito, é porque quer o seu bem
Am
Se ela desconfia, é que alguma coisa tem
F                                              Fm7     G
E não adianta, de amor querer trocar só vai mudar de lugar

Refrão 2x:
C
É assim que funciona

É assim que funciona a vida
Am
De quem vive a vida, dividida com outra vida
F
É assim que funciona

É assim que funciona o mundo
Fm7                                      G
Não me iludo, no teu mundo eu quero ser tudo

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
Fm7 = 1 X 1 1 1 X
G = 3 2 0 0 0 3
