Monarco - Jardim da Solidão

       Cm         D7
Me enganei redondamente
      G7     Cm
Com você, oh flor
                   Fm
Pois pensava francamente
        Bb7         Eb
Que era puro o seu amor
     E°            Fm
Só depois que descobri
        G7        Cm
Que era tudo falsidade
                    Ab
Prefiro viver tão sozinho
             G7                Cm
Porque seu carinho não é de verdade

       Fm          Bb7
Haviam flores enfeitando
         Eb
O meu jardim

     Cm          Dm7(b5)
Gorjeando a passarada
    G7        Cm
Era tudo para mim
                   Fm
Mas vieram os dissabores
G7               Cm
Fruto de uma traição
                   Ab
Murcharam todas as flores

      Fm         G7
No jardim da solidão
     Cm        Bb7
Hoje vivo implorando
                  Eb
De joelhos ao criador
Cm              Ab
Para esquecer a mágoa que sinto
   G7                  Cm
No peito por um falso amor

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Bb7 = X 1 3 1 3 1
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
Dm7(b5) = X X 0 1 1 1
Eb = X 6 5 3 4 3
E° = X X 2 3 2 3
Fm = 1 3 3 1 1 1
G7 = 3 5 3 4 3 3
