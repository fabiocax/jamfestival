Almir Guineto - Lama Nas Ruas

Tone: Am
Intro:Am  Bm5-/7  E7  Am  E7/4  E7  Am  Gm7  C7  F  E7  E7/4

Am   D7/9              Am   E7                 A7/4
 Deixa.....desaguar tempestade......inundar a cidade
A7/5+   Dm
 .....Porque arde um sol dentro  de nós
Bm5-/7   E7                    Bm5-/7   E7
 Queixas......sabe bem que não temos
          Bm5-/7   E7
 E seremos serenos
          Am                      E7/5+
 Sentiremos prazer no tom da nossa voz
 Am  D7/9              Am
 Veja no olhar de quem ama
               A7/4  A7
 Não reflete um drama não
                      Dm
 É a expressão é sincera, sim
Bm5-/7                E7
 Vim pra mostrar que o amor

                 Am ^^^^^^ Gm7        C7
 Quando é puro e desperta e alerta o mortal
  F7+                  Bm5-/7 ^ E7
 Ai é que o bem vence o mal
               A7/4
 Deixa a chuva cair
                     A7/5+
 Que o bom tempo há de vir
   Dm             E7
 Quando o amor decidir
        Am7 ^^ Gm7               C7
 Mudar o visual trazendo a paz no sol
     F7+                        Bm5-/7    E7
 Que importa se o tempo lá fora vai mal
        A   C#m7   D/F# ^ E/G# ^ A ^ E7
 Que importa
  A    A7+              C#m5-/7   F#7/4   F#7
 Se há tantas lamas nas ruas
    Bm7  Bm7+                     Bm7 ^ Bbm7 ^^ Em7 ^
 E o céu.........deserto e sem brilho.........de  luar
^ A7/4  D             E7
 .....Se o clarão da luz
  C#m5-/7        F#7
 Do deu olhar vem me guiar
  Bm7           E7
 Conduz meus passos
      A         F#7        Bm7   E7
 Por onde quer que eu vá, se há
    A                E7  Am  E7
 Por onde quer que eu vá..    <-- na 2ª vez e volta ao início

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
A7/4 = X 0 2 0 3 0
A7/5+ = X 0 X 0 2 1
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Bbm7 = X 1 3 1 2 1
Bm5-/7 = X 2 3 2 3 X
Bm7 = X 2 4 2 3 2
Bm7+ = X 2 4 3 3 2
C#m5-/7 = X 4 5 4 5 X
C#m7 = X 4 6 4 5 4
C7 = X 3 2 3 1 X
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D7/9 = X 5 4 5 5 X
Dm = X X 0 2 3 1
E/G# = 4 X 2 4 5 X
E7 = 0 2 2 1 3 0
E7/4 = 0 2 0 2 0 X
E7/5+ = 0 X 0 1 1 0
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
F#7 = 2 4 2 3 2 2
F#7/4 = 2 4 2 4 2 X
F7+ = 1 X 2 2 1 X
Gm7 = 3 X 3 3 3 X
