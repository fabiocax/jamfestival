Almir Guineto - É pois é

Introdução: C Cm G E7 Am D7 G7+ D7

   G7+ G6  F#m5-/7      B7      Em          Dm      G7
    É    pois é, nem o galo cantou, e três vezes você me
   C    Cm         G            E7        Am7/9
traiu, iludiu, e fugiu e ainda levou, os sonhos que guardei
   Am7    D7           D7     G7+   D7
de amor levou ( 2x  guardei levou )
   G7+  G6             F#m5-/7    B7    Em
    Hora     vem agora pedindo pra voltar, chegou fora
    Dm   G7     C                    Cm
de hora, pra chorar,  me falou do passado, que penou
     G   E7             Am7/9
um bocado, me contou a maior lição, com o suor de  uma
Am7    D7
solidão
   G7+   G6                F#m5-/7  B7   Em
     Hora    aqui mora quem já lhe esqueceu, a verdade
       Dm       G7      C                        Cm
é que agora eu sou mais eu, e o que mais hoje eu ligo
             G    E7                 Am7/9
é viver mem comigo, e por isso eu lhe digo não

              Am7    D7
Va curtir uma solidão....
  G7+  G6
   É pois é....

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Am7/9 = X X 7 5 8 7
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F#m5-/7 = 2 X 2 2 1 X
G = 3 2 0 0 0 3
G6 = 3 X 2 4 3 X
G7 = 3 5 3 4 3 3
G7+ = 3 X 4 4 3 X
