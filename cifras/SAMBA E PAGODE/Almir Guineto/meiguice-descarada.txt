Almir Guineto - Meiguice Descarada

[Intro] Am  Dm  E7
        Am  Bm5-  E7  Am

Oh ! quan........ta!
Am                  G7
Quanta ilusão alimentei
F7                 Bm5-   E7
Sua meiguiçe é descara........da
Am                      G7
Desempenha bem o seu papel
  F7                Bm5-   E7
Mesmo com a ribalta apaga......da
   Am           Dm     Bm5-
Vai, segue seu sonho
   E7          Em5-     A7
Seu destino terá o luar
  Dm     G7              C
Vai.....e não perca um segundo
   F7M           Bm5-
Já que o nosso mundo
  E7              Em5-   A7
Não se pode juntar

  Dm      G7               C
Vai.....e não perca um segundo
   F7M           Bm5-   E7
Já que o nosso mundo
        A7M
Não se pode juntar
   Bm  E7
E mas
  A7M       Bm     C#m
Mas, por que tentar
C°       Bm     E7
E condenar a ilusão
A7M            F#7
(Você traz a promessa)
Bm              E7            A7M
você traz a promessa no olhar
               F#7            Bm
E tem sempre o dom de enganar
                  E7               A7M
E vem sempre com a chuva pra molhar
F#7      Bm
Molhar o quê?
  E7                                A7M  F#7
O amor que eu dou tudo pra ter de você
Bm
Molhar o quê?
  E7
O amor que eu dou tudo pra ter de você
Am          E7
    Oh! Quan........ta!

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7M = X 0 2 1 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
Bm5- = X 2 3 4 3 2
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
C° = X 3 4 2 4 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
Em5- = 0 1 2 0 0 0
F#7 = 2 4 2 3 2 2
F7 = 1 3 1 2 1 1
F7M = 1 X 2 2 1 X
G7 = 3 5 3 4 3 3
