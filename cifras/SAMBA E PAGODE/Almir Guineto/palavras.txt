Almir Guineto - Palavras

Intro: Em / Em/D / C#m5-/7 / C / Bm / Bb° / Am7 / D7/9 / G7+ / B7 /

    Em    %   F#m5-/7
Oh, Deus !
                B7       Em
Palavras não se dá.......em resposta, dei
Am7         D7/9       G7+
.....Vi voltar atrás.......palavra de rei
F#m5-/7             B7            Em
.....Palavras que deu pena.......lavo as mãos
C#°                  E°    F#m5-/7           B7
.....Repetem a mesma cena......palavra de omissão
Em        %   F#m5-/7
.....Bem sei
         B7                  Em
Dizem palavras que não se escreve
          Am7  D7/9        G7+
Que vai ao vento.....numa brisa leve
F#m5-/7              B7             Em
.....Blasfemam com palavras......na hora da aflição
C#m5-/7          F#7              F#m5-/7     B7
.....Palavras se propagam.....em busca do perdão

     E                     G#m7
Independência ou morte.....palavras tão triunfantes
Bm           C#7               F#m7
.....Composta por vogais......unindo consoantes
Am7                      G#m7          C#7
.....Nos idiomas, as palavras......envolvem todo o universo
F#m7               B7                   E             B7
.....Imortalizam os poetas.....nas vozes dos seus versos
            E                   G#m7
Quem tem o dom da palavra.....é o senhor da razão
Bm                     C#7     F#m7
.....Faz com palavras magias.....de consagração
Am7                G#m7         C#7
.....Nas veias da poesia.....viajando na canção
   F#m7             B7              Em      B7
É com palavras que encontram explicação..

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
Bb° = X 1 2 0 2 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C#7 = X 4 3 4 2 X
C#m5-/7 = X 4 5 4 5 X
C#° = X 4 5 3 5 3
D7/9 = X 5 4 5 5 X
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
Em/D = X X 0 4 5 3
E° = X X 2 3 2 3
F#7 = 2 4 2 3 2 2
F#m5-/7 = 2 X 2 2 1 X
F#m7 = 2 X 2 2 2 X
G#m7 = 4 X 4 4 4 X
G7+ = 3 X 4 4 3 X
