Almir Guineto - Dalila, cadê Guará


 B7           Em7
DALILA CADÊ GUARÁ ?
A7                D7M
JÁ SE MANDOU PRA RIBEIRA.
  B7         Em7
DALILA CADÊ GUARÁ ?
A7                 D7M
JÁ SE MANDOU PRA RIBEIRA.
 B7               Em7
Saiu na manhã de quinta,
A7                D7M
só volta segunda feira.
   B7          Em7
é metido a boa pinta,
A7                 D7M
diz que é nó na madeira.
      B7               Em7
Corta o beque, faz a finta,
A7                D7M
também toca de primeira.

  B7               Em7
Ele já passou dos trinta,
A7                  D7M
mais não é de brincadeira .  Dalila !!!

Refrão

  B7               Em7
Entra na primeira dança
A7               D7M
só para na derradeira,
  B7            Em7
Papa fina fala mansa,
A7               D7M
não conhece a canseira.
   B7             Em7
Se der mole ele avança,
A7              D7M
Não é de marcar bobeira.
   B7             Em7
O malandro nem balança,
A7                      D7M
quando bebe a saideira. Dalila !

Refrão

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
D7M = X X 0 2 2 2
Em7 = 0 2 2 0 3 0
