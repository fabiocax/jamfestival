Almir Guineto - Mãos


Am    E7
Mãos, se rendem
Bm5-/7  E7       Am
Pra  outras que tudo levam
Bm5-/7  A7  Dm
  Quase em extinção
G7       C
Mãos honestas, amorosas
B7
Em nossas pobres mãos
Bm5-/7 E7
Que   batem as cordas
A7   D7
Pago pra ver
Bm5-/7  E7  Am
Quei...mar em brasa
Bm5-/7     E7
As mãos de bacharéis
Am        Am/G
Que não condenam o mal

Bm5-/7    E7
Que inocentam reús
A7
Em troca do vil metal
Dm      E7
As mãos de bacharéis
Am         F7
Que não condenam o mal
Bm5-/7    E7
Que inocentam reús
        A  F#7  Bm E7
Em troca do vil metal
A
Mãos de infiéis
C#m
Revés que não contentam
E      A7        D
Movendo a diretriz tão fraudulenta
D#º
Sem réu e sem juiz
C#m      F#7
Mãos não se acorrentam
B7           Bm     E7
Justiça põe as mãos na consciência
A
Ato que fez Pilatos
 C#m
Lavando suas mãos
Em        A7
É o mesmo que injustiça
    D
feita com as próprias mãos
D#º      Dm
As mãos que fracassaram
C#m       F#7
Na torre de Babel
B7     E
Porque desafiaram
Am      E7
As mãos do céu

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Am/G = 3 X 2 2 1 X
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
Bm5-/7 = X 2 3 2 3 X
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
D#º = X X 1 2 1 2
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F#7 = 2 4 2 3 2 2
F7 = 1 3 1 2 1 1
G7 = 3 5 3 4 3 3
