Almir Guineto - Nos Laços

Intro: G7+ D7

    G7+            G6
Nos Laços da Insensatez
F#m5-/7           B7
Tentando me encontrar
 Em
Por mais que eu me esforcei
D#m Dm         G7 G7/9
Voltei a fracassar
Em       Cm          Bm
O meu sentimento de culpa
   E7/9            A7/9
Me fazendo mil consultas
D7             Dm G7 G7/9
Tentando me ajudar
Em      Cm       Bm
Nessa dificil solução
        E7/9           A7/9
Pra convencer meu coração
           D7   G      D7

A não te detestar (nos laços)
 Gm7/9                                 Cm
Disse-me sobre receitas de um amor de verdade
 F                     F#7/9
Não se aprende no colégio
F#7                Bb7+   Bb6
Nem tão pouco em faculdade
 D7/13
Nem livros de medicina

Há remédio com efeito
    Gm7/9
  Eb                                 D7/4 D7
Só os poderes divinos é que pode dar jeito
   G7+     G6           Bm
Coisas de amor é uma magia
          E7/9            A7/9         D7   G  D7/4 D7
Peça pra Deus pra ela voltar pra minha companhia
   G7+     G6           Bm
Coisas de amor é uma magia
          E7/9            A7/9              D7    G
Peça peça pra Deus pra ela voltar em prece Ave Maria

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7/9 = 5 X 5 4 2 X
B7 = X 2 1 2 0 2
Bb6 = X 1 3 0 3 X
Bb7+ = X 1 3 2 3 1
Bm = X 2 4 4 3 2
Cm = X 3 5 5 4 3
D#m = X X 1 3 4 2
D7 = X X 0 2 1 2
D7/13 = X 5 X 5 7 7
D7/4 = X X 0 2 1 3
Dm = X X 0 2 3 1
E7/9 = X X 2 1 3 2
Eb = X 6 5 3 4 3
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F#7 = 2 4 2 3 2 2
F#7/9 = X X 4 3 5 4
F#m5-/7 = 2 X 2 2 1 X
G = 3 2 0 0 0 3
G6 = 3 X 2 4 3 X
G7 = 3 5 3 4 3 3
G7+ = 3 X 4 4 3 X
G7/9 = 3 X 3 2 0 X
Gm7/9 = X X 5 3 6 5
