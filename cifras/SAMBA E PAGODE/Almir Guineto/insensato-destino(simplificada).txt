Almir Guineto - Insensato Destino

G             F#m7             B7
Óh insensato destino pra que
Em                 Dm       G7
Tanta desilusão no meu viver
C         D7         Bm
Eu quero apenas ser feliz
             E7
Ao menos uma vez
       A7         Am       D7   C
E conseguir o acalanto da paixão
         D7         Bm
Fui desprezado e magoado
        E7        Am  D7        Dm G7
Por alguem que abordou meu coração
C        D7         Bm
Fui desprezado e magoado
        E7        Am  D7         G  D7
Por alguem que abordou meu coração

Gm      Gm7           Cm
Destino porque fazes assim

              F7
Tenha pena de mim
                Bb7+
Veja bem não mereço sofrer
Gm              Am7
Quero apenas um dia poder
D7                Gm
Viver num mar de felicidade
Gm7               A7  D7   Gm
Com alguém que me ame de verdade
Gm7               A7  D7   Gm
Com alguém que me ame de verdade
Gm7               A7  D7   Gm
Com alguém que me ame de verdade

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
Bb7+ = X 1 3 2 3 1
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F#m7 = 2 X 2 2 2 X
F7 = 1 3 1 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
