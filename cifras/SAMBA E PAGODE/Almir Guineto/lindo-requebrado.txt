Almir Guineto - Lindo Requebrado

Introdução: A  Am  G#m  C#7  F#m  B7  E

  C#m            F#m           B7          E   C#m
Quando entra no samba com seu lindo requebrado
        F#m         B7          E
Sinto ciúme que me deixa embaraçado
  C#m            F#m           B7          E   C#m
Quando entra no samba com seu lindo requebrado
        F#m         B7          E
Sinto ciúme que me deixa embaraçado
     B7
Meu bem !
     E               G#m
Meu bem....não por que
        A     B7             E  C#m
Te amo tanto.....de enlouquecer
           F#m  B7           E   C#m
Quando te vejo....toda assanhada
        F#m         B7        E   B7
Se rebolando, na roda da batucada
   E                G#m
O peito....chega arder

         A    B7            E  C#m
É tanto medo.....de te perder
           F#m     B7        E   C#m
Por mil malandros.....é desejada
          F#m             B7          E   B7
Mas teu amor, fez do meu peito sua morada
                  E         C#m   F#m
O que mais tenho medo.....é a saudade
                B7            E    B7
Com você encontrei.....felicidade
                 E7              A
Sem você minha vida.....não é nada
 Am       E                 B7      E  B7
Não tem lua.....sol....nem madrugada
                 E7              A
Sem você minha vida.....não é nada
 Am       E                 B7     E
Não tem lua.....sol....nem madrugada
        G
Bora laiá!
B7      E
Ê....menina
       C#m       F#m         B7   E  Gº
Vou te esconder, pra ninguém te oiá
B7      E
Ê....menina
           C#m      F#m          B7    E
Vou mandar te bezer, pra ninguém te secar..

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
C#7 = X 4 3 4 2 X
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G#m = 4 6 6 4 4 4
Gº = 3 X 2 3 2 X
