Dudu Nobre - O Amor Não Pode Esperar

Introdução: F / A7 / Dm / Cm ^ F7 / Bb / Dm / Gm7 / C7 / F /

      F                  Gm7      Am7       C7
Eu procurei um amor tão longe e você tão perto
 F      Dm        Gm7  D7    Gm7                       C7
Passei batido e perdi......vaguei feito um astro no universo
                   Gm7
Sem achar um rumo certo
    C7                  F         C7
meu amor estava sempre aqui...eu nem vi
 F         D7         Gm7         C7               F     Gm7 ^ C7
Ainda bem que eu despertei......abri os olhos e encontrei
 F                D7          Gm7
Aquele amor do passado...foi grande que só
  C7                 F
Mas tá pintando um amor maior, bem maior
    C7          Cm5-/7     D7
Vem cá pro meu amor ficar melhor
    Gm7         C7         F
Vem cá pro meu amor ficar melhor
             C7     F               A7     Dm
Somente nos braços seus amor, quero me encontrar

           D7      Gm7                  C7    F
A dor já me disse adeus, se foi pra não mais voltar
         C7      F           A7    Dm
Embarca nessa viagem amor, vamos navegar
           D7       Gm7               C7      F
Paixão já pediu passagem, o amor não pode esperar..

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am7 = X 0 2 0 1 0
Bb = X 1 3 3 3 1
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
Cm5-/7 = X 3 4 3 4 X
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
Gm7 = 3 X 3 3 3 X
