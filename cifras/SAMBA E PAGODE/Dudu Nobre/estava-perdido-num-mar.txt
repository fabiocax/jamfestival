Dudu Nobre - Estava perdido num mar

Intro.: G7+ C G/B G7/13 C F D/F# G Am7 D7

G7+                 C/E     G7
Quero um pedaço do seu coração
          C         G
Vou me render a paixão
           Am7 D7  G  G7
Mas sei que posso sofrer
    C               D7     G      G7
Cansei de tanta aventura quero você
      C          D7      G       C/E
Me encanta sua ternura quero você
     G              C/E
Não posso viver sozinho
    G7            C
Sem rumo, sem disalinho
        G                  Am7 D7     G7
Seu perfume foi feito pra me enfeitiçar
       C                  D7    G          G7
Sou capaz de qualquer loucura, pra te beijar
  Am7            Bm7
Viajo na sua candura

  C6                A/C#
Pureza amor beleza pura
 Am7         D7/13       G
No seu cais quero me ancorar

          C/E       G7             :
Estava perdido num mar             :
            F#m5-/7    B7/9 Em     :
A deriva, você me encontrou        :
               Em/G      Am7       :
Sou marujo não vou marear          :
                   D7        G     :
Nem sofrer um naufrágio no amor    :
                C/E    G7          :
Várias estrelas a me guiar         : 2X
                   F#m5-/7 B7/9 Em :
Eu navegava em qualquer direção    :
               Em/G      Am7       :
Você veio pra me iluminar          :
                D7      G          :
Te entreguei o meu coração         :

                   D7
Quero um pedaço do seu coração...

----------------- Acordes -----------------
A/C# = X 4 X 2 5 5
Am7 = X 0 2 0 1 0
B7/9 = X 2 1 2 2 X
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
C6 = 8 X 7 9 8 X
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
D7/13 = X 5 X 5 7 7
Em = 0 2 2 0 0 0
Em/G = 3 X 2 4 5 X
F = 1 3 3 2 1 1
F#m5-/7 = 2 X 2 2 1 X
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G7 = 3 5 3 4 3 3
G7+ = 3 X 4 4 3 X
G7/13 = 3 X 3 4 5 X
