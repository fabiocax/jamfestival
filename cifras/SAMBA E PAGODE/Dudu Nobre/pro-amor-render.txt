Dudu Nobre - Pro Amor Render

C, F7, Em, A7, D7, G, C.

    G             Am            D7          G
EU FACO TUDO QUE TEM QUE FAZER PRO AMOR RENDER
                    Am
MEXO GOSTOSO O QUE TEM DE MEXER
D7            G   G7          C    D7           Bm7/5b
PRO AMOR RENDER, PRO AMOR RENDER, PRO AMOR RENDER
    E7           Am
EU FACO TUDO QUE TEM DE FAZER
D7            G   G7          C    D7           Bm7/5b
PRO AMOR RENDER, PRO AMOR RENDER, PRO AMOR RENDER
             E7                 Am
OLHA QUE EU MEXO GOSTOSO O QUE TEM DE MEXER
D7            G                     Am
PRO AMOR RENDER EU FACO TUDO O QUE TEM DE FAZER
D7            G                     Am
PRO AMOR RENDER MEXO GOSTOSO O QUE TEM DE MEXER
D7            G   G7          C    D7           Bm7/5b
PRO AMOR RENDER, PRO AMOR RENDER, PRO AMOR RENDER
    E7            Am
EU FACO TUDO QUE TEM DE FAZER

D7           G    G7          C    D7           Bm7/5b
PRO AMOR RENDER, PRO AMOR RENDER, PRO AMOR RENDER
             E7                 Am
OLHA QUE EU MEXO GOSTOSO O QUE TEM DE MEXER
D7           G                      Am
PRO AMOR RENDER BATO O TAMBOR QUE TIVER QUE BATER
D7                G
ACENDO TUDO O QUE TEM DE ACENDER
Em          Am              D7              G
RALO O QUE FOR DE RALAR MISTURO O QUE E DE MISTURAR
     G7             C
SEU BEIJO ME FAZ ENLOUQUECER
         Cm               G
E UM TEMPERO GOSTOSO PRO MEU VIVER
    A7                      Am
NO SEU CORPO LINDO VOU ME AQUECER
    D7                     G
PERGUNTO AO MEU PAI O QUE VOU FAZER
        G7                   C
NOSSO AMOR NAO TEM HORA NAO TEM LUGAR
       Cm           G
CRISTALINO FEITO AS ONDAS DO MAR
A7                   D7
ESSA PAIXAO VEIO ME EMBALAR
                      G
VOU COMER DESSE DOCE ME LAMBUZAR
G             Am       D7              G
O QUE E DE BEBER EU BEBO O QUE E DE BANHAR ME BANHE
Em              Am
POR FAVOR, VOCE NAO ME ASSANHE
D7             G           Em        Am
FICO LIGADO E NEM PERCEBO E GOSTOSO SER SEU NEGO
D7           G        Em           Am
SER DONO DO SEU AMOR ALIVIAR A TRISTEZA E A DOR
D7                G
COM SEU CARINHO, COM SEU CHAMEGO.

solo.G  Em  Am  D7  G

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bm7/5b = X 2 3 2 3 X
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F7 = 1 3 1 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
