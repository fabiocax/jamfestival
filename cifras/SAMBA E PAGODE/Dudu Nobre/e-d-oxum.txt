Dudu Nobre - É D'Oxum

Introdução: D / G / Em / A7 / D6/9 / % /

D6/9                        Em
Nessa cidade todo mundo é D'Oxum
 A7                      D6/9  A7/4  A7
Homem, menino, menina, mulher
D6/9                     Em
Toda essa gente irradia magia
  Em               G
Presente na água doce           }
  Em                G
Presente na água salgada        }   bis
Em        A7    D6/9
Em toda cidade brilha           }
D6/9                            Em
....Seja tenente ou filho de pescador
 A7                        D  A7/4 ^ A7
....Ou importante desembargador
D6/9                             Em
Se der presente é tudo uma coisa só
   Em                 G
A força que mora na água        }

 Em                  G
Não faz distinção de cor        }  bis
Em       A7          D6/9
E toda cidade é D'Oxum          }
     Em    A7  D6/9       Em   A7
É D'Oxum...é D'Oxum...é D'Oxum
          D
Eu vou navegar...eu vou navegar
                       Em
Nas ondas do mar...eu vou navegar
 A7          D6/9
....Eu vou navegar
     D6/9      Em   A7
É D'Oxum...é D'Oxum
           A7   D6/9
É D'Oxum...é D'Oxum..

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7/4 = X 0 2 0 3 0
D = X X 0 2 3 2
D6/9 = X 5 4 2 0 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
