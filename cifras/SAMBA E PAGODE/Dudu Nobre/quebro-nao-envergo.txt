Dudu Nobre - Quebro, Não Envergo

[Intro] Dm  Dm5+  Dm6  A7  Dm

A4          A4/Bb    A4/B                    A4/Bb
Choro, me devoro,      não imploro por você
A4                     A4/Bb   A4/B               D9(7)/C   D7
Se enfraqueço, não mereço,        viver à sua mercê
Gm             Gm5+    Gm6                         Gm5+
Quebro, não envergo,        nem me entrego a ninguém
Gm            Gm/F    Gm6/E     Gm/D     C#º     A7       Dm7(9)
Sei que o meu medo,   meu segredo,         é viver     sem ter
       Cm7(9)           F7(13)   Bb7M         A7(13)     Dm7(9)
Alguém        , que possa        me ensinar,     a  -  mar

C7                F7M       E7                    Am
Te desejar é meu vício,     dor pra me fazer chorar
Eb7                Ab     D7                 Gm
Ópio pra me entorpecer, bebida pra me embriagar
A7                Dm            D7
É paixão pra me fazer sofrer, enlouquecer
      Gm         A7                     Dm
Vou te esquecer, pois viver assim não dá


C7                      F           E7                 Am
Sinto que a qualquer momento, vou me abraçar com a solidão
   Eb7                 Ab        D7                Gm
Distante da mais bela atriz, à deriva minha embarcação
A7                  Dm
Uma luz me salva por um triz
D7                     Gm
Estrelas brilham, volto a navegar
     A7               Dm
E a lua vem testemunhar

----------------- Acordes -----------------
A4 = X 0 2 2 3 0
A4/B = X 2 2 2 2 X
A4/Bb = X X 8 9 10 9
A7 = X 0 2 0 2 0
A7(13) = X 0 X 0 2 2
Ab = 4 3 1 1 1 4
Am = X 0 2 2 1 0
Bb7M = X 1 3 2 3 1
C#º = X 4 5 3 5 3
C7 = X 3 2 3 1 X
Cm7(9) = X 3 1 3 3 X
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Dm5+ = X X 0 3 3 1
Dm6 = X 5 X 4 6 5
Dm7(9) = X 5 3 5 5 X
E7 = 0 2 2 1 3 0
Eb7 = X 6 5 6 4 X
F = 1 3 3 2 1 1
F7(13) = 1 X 1 2 3 X
F7M = 1 X 2 2 1 X
Gm = 3 5 5 3 3 3
Gm/D = X 5 5 3 3 3
Gm/F = X X 3 3 3 3
Gm5+ = 3 6 5 3 4 3
Gm6 = 3 X 2 3 3 X
