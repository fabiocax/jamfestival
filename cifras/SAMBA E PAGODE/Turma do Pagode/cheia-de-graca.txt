Turma do Pagode - Cheia de Graça

INTRO: F Gm C7 F

C6/9                     F
Ela é tão cheia de graça
                        C6/9
Causa um frisson la na massa
                           F
Tem gente que ser perde  embassa
                          C6/9
Para tudo quando a mina passa

C6/9
Baila que nem bailarina corpinho de menina
                               F
Chortinho de renda gosta de provoca
                                                 2x
C6/9
No pagode chega da um sacode
                                 F
Quem não pode fica doido pra chegar


C6/9
Ah me ganhou pra valer
                      F                 2x
Ah baila que eu quero ver

----------------- Acordes -----------------
C6/9 = X 3 2 2 3 3
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
