Turma do Pagode - Pensando Em Você

Intro:  G  Cm

G                 D            Em
Estava satisfeito só em ser teu amigo
            D                   C      G#°
Mas o que será que aconteceu comigo?
Am7                   D7
Aonde foi que eu errei
G                     D                  Em
Às vezes me pergunto se eu não entendi errado
                D                C       G#°
Grande amizade bom estar apaixonado
Am7                         D7
Se for só isso logo vai passar
C
Mas quando toca o telefone será você?
Cm
O que eu estiver fazendo eu paro de fazer
G
E se fica muito tempo sem me ligar
G#°

Arranjo uma desculpa pra te procurar
Am7        G            C9         D
Que tolo, mas eu não consigo evitar

Refrão:
              C9       D      Bm
Porque eu só vivo pensando em você
E7        Am             D7                 G   G7
É sem querer, você não sai da minha cabeça mais
        C9         D      Bm
E eu só vivo acordado a sonhar
E7       Am                                        D
Imaginar nós dois às vezes penso ser um sonho impossível
              D7      ( G7/4 )
Ou uma ilusão terrível
   C              D        Bm
Será? Eu já pedi tanto  em oração
E7                     Am
Que as portas do seu coração
     D7             G          G7
Se abrissem pra eu te conquistar
         C               D    Bm
Mas que seja feita a vontade de Deus
E7            Am
Se Ele quiser então

Não importa quando onde ou como
C        Cm       G
Eu vou ter teu coração

Solo: G  Cm

G                 D            Em
Faço tudo pra chamar tua atenção
                         D            C
De vez em quando eu meto os pés pelas mãos
                 G#°
Engulo a seco o ciúme
                    Am7
Quando outro apaixonado quer
               D7
Tirar de mim sua atenção
G              D         Em
Coração apaixonado é bobo
                          D
Um sorriso teu e eu me derreto todo
C               G#°
O seu charme o seu olhar
Am7                     D7
Sua fala mansa me faz delirar

C
Mas quanta coisa aconteceu e foi dita
Cm
Qualquer mínimo detalhe era pista
G
Coisas que ficaram para trás
G#°
Coisas que você nem lembra mais
C
Mas eu guardo tudo aquilo no meu peito
Cm
Tanto tempo estudando seu jeito
G
Tanto tempo esperando uma chance
G#°
Sonhei tanto com esse romance
Am7        G#°           C9          D
Que tolo, mas eu não consigo evitar

(Refrão)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
G7/4 = 3 5 3 5 3 X
