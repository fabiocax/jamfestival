Turma do Pagode - Mais Que Urgentemente

Intro 2x: Gm7/9  Bb7/4  Bb7  Eb6/9  Eb6  Am7/5-  D7

Gm7/9                                  Bb7/4
Tudo que eu disse da boca pra fora
Bb7         Eb6/9  Eb6        Am7/5-    D7
Esquece, ignora, não vale a pena sofrer
Gm7/9                                   Bb7/4        Bb7          Eb6/9
De cabeça quente não pensei direito, desculpe o mal jeito
Eb6          Am7/5-       D7
A gente não vai se perder
Gm7/9                               F7/5-
Eu errei, foi quando decidi terminar
                         Cm7              Cm/Bb       Am7/5-       D7
A solidão a me torturar, achava que podia suportar
Gm7/9                              F7/5-
Ilusão, foi enganar o meu coração
                                 Cm7              Cm/Bb              Am7/5-        D7
Achando que eu tinha razão, e quando dei por mim perdi meu chão
Gm7/9                                  Fm7/9                   Bb7/13
Senti meu mundo desabando, perdi a luz do meu sorriso
Eb6/9                       Eb6               Am7/5-             D7
Mas vi o quanto eu te amo, você é a dona do meu paraíso

Gm7/9                                          Bb7/4                    Bb7
Eu quero teu amor pra sempre, não quero mais sofrer a toa
Eb6/9                              Eb6                            Am7/5-           D7
Presciso mais que urgentemente ter você na minha vida, me perdoa

----------------- Acordes -----------------
Am7/5- = 5 X 5 5 4 X
Bb7 = X 1 3 1 3 1
Bb7/13 = 6 X 6 7 8 X
Bb7/4 = X 1 3 1 4 1
Cm/Bb = X 1 1 0 1 X
Cm7 = X 3 5 3 4 3
D7 = X X 0 2 1 2
Eb6 = X X 1 3 1 3
Eb6/9 = X X 1 0 1 1
F7/5- = 1 X 1 2 0 X
Fm7/9 = X X 3 1 4 3
Gm7/9 = X X 5 3 6 5
