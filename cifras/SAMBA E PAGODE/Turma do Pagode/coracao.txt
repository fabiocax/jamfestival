Turma do Pagode - Coração

Bm7/9                                           Em7                  G/A
Passa por mim e me dá um sorriso excitante dizendo que quer me beijar...
G7+     F#m7     C#m7/5-          F#7/9+
Bei...ja,       beija,          beija
Bm7/9                                              Em7                G/A
Dá um calor sobe um fogo no corpo quando eu paro e penso que posso te amar
G7+     F#m7      Em           F#7/9+
Bei...ja,       beija,          beija
G7+          A7    Bbº               Bm7    E7/9             Am7         D7/9
Você tá saben....do   que eu tenho alguém mais ainda me quer.
G7+          A7    Bbº   Bm7   E7/9        C#m7/5-      Gm7 F#m7 Fm7 Em7
Fica me tiran....do o juí.....zo   ainda vai me ganhar ...
                  Bm7
Coração tá pedindo uma chance pra se apaixonar,
Em7               Bm7
Coração vê se não entra nessa vai me bagunçar.
G7+  G6         D7+/9                      D7+
Eu tentei me esquivar mais o amor me pegou de surpresa
G7+   F#m7                 Em7             F#7                          G7+
Vi   você  com esse jeito de se apaixonar sem querer

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am7 = X 0 2 0 1 0
Bbº = X 1 2 0 2 0
Bm7 = X 2 4 2 3 2
Bm7/9 = X 2 0 2 2 X
C#m7/5- = X 4 5 4 5 X
D7+ = X X 0 2 2 2
D7+/9 = X 5 4 6 5 X
D7/9 = X 5 4 5 5 X
E7/9 = X X 2 1 3 2
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F#7 = 2 4 2 3 2 2
F#7/9+ = X X 4 3 5 5
F#m7 = 2 X 2 2 2 X
Fm7 = 1 X 1 1 1 X
G/A = 5 X 5 4 3 X
G6 = 3 X 2 4 3 X
G7+ = 3 X 4 4 3 X
Gm7 = 3 X 3 3 3 X
