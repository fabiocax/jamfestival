Turma do Pagode - Sobrenatural Eu Nunca Amei Assim

[Intro] D  B  G  D

A7+      A7       D7+
   Vi a alegria voltar
        E7         C#m7
Quando recebi um telegrama
      F#m7         Bm7
Você dizendo pra te esperar
        D/E
Que não demora e que ainda me ama, me ama

( D7+  C#m7  F#m7  Bm7  Bm7  C#m7  D7+ )

         F#m7            E
Fui ao cinema pra me distrair
                       D7+
Fiquei surpreso quando vi
                  E
Aquela história parecia a de nós dois
Bm7  C#m7  D7+                 C#m7
         Eu percebi que era um sinal

F#m7      Bm7
Aviso sobrenatural
                C#m7
Te persegui, não quis deixar para depois
     F#m7            C#m7
Mas que surpresa, você foi viajar
                          Bm7
Não sei pra onde nem se vai demorar
                  D7+      G#m7/5b  G7+
E logo agora essa vontade louca de te ver
        F#m7               C#m7
E mesmo longe eu não deixei de te amar
                              D7+
E as diferenças vamos deixar pra lá
                   Ebº   Bm7
Eu tô aqui agora e cadê você?
             E7   A7  D7+
Eu não tô podendo fi..car
           E7  E/D       C#m7
Nem mais um minuto sem te ver
            F#m7  Bm7
Eu não pude acreditar
         E7             E
Quando você foi sem me dizer
      A7/4    D7+
Vi a alegria voltar
         E7       E/D    C#m7
Quando recebi um telegrama
             F#m7    Bm7
Você dizendo pra te esperar
                   E7          A
Que não demora e que ainda me ama
           A9     A7  D7+
Eu não tô podendo fi..car
            E7  E/D       C#m7
Nem mais um minuto sem te ver
           F#m7   Bm7
Eu não pude acreditar
        E7             E
Quando você foi sem me dizer
      A7/4     D7+
Vi a alegria voltar
        E7  E/D      C#m7
Quando recebi um telegrama
              F#m7   Bm7
Você dizendo pra te esperar
          E7                   F#m7
Que não demora e que ainda me ama, me ama

( C#m7  D7+  C#m7  Bm7  C#m7  D7+  E  Cm7  Dm  Eb )

Gm7       Gm7+      D7
Melhor a gente se entender
                    Eb7+
E o que tiver que acontecer
           Dm7         Cm7  F7/9  D7/9-
Que dessa vez seja pra sempre
Gm7       Gm7+          D7
Pra que brincar de se esconder
                    Eb7+
Se o amor tocou eu e você
             Dm7        Cm7  F7/9  D7/9-
De um jeito assim, tão diferente?
Eb7+                    Dm7  Gm7  Bb7/4
Por que você não fica comigo
Eb7+                    Dm7  D7
E deixa o meu amor te levar?
Eb7+                  Dm7     Gm7  Bb7/13
Solidão a dois é um castigo, é um castigo
Gm7          Eb7+      Cm7  C#7+
Sem essa de querer complicar
Eb7+              F7         Dm7          Gm7  Bb7/4
Nosso amor é tão lindo e eu só penso em você
Eb7+              F7             Dm7      Gm7  Bb7/4
Mas o que eu tô sentindo você finge que não vê
Eb7+              F7           Dm7   Gm7  Gm7+
Nosso amor é tão lindo e você longe de mim
         Cm  Dm  Eb  Eb  F7/9        Cm  Dm  Eb
Eu nunca amei assim,        eu nunca amei assim
Gm7  Gm7+            D7
Vem cá, me deixa te tocar
                    Eb7+
Dormir à luz do seu olhar
      Dm7            Cm7    F7/9  D7/9-
E te falar meus sentimentos
Gm7  Gm7+           D7
Deixa o ciúme pra depois
                          Eb7+
O amor tem planos pra nós dois
      Dm7           Cm7     F7/9  Bb7/4
Você mudou meus pensamentos
Eb7+                    Dm7  Gm7  Bb7/4
Por que você não fica comigo
Eb7+                    Dm7  D7
E deixa o meu amor te levar?
Eb7+                  Dm7     Gm7  Bb7/13
Solidão a dois é um castigo
Gm7          Eb7+      Cm7  C#7+
Sem essa de querer complicar
Eb7+              F7           Dm7   Gm7  Bb7/4
Nosso amor é tão lindo e eu só penso em você
Eb7+              F7             Dm7      Gm7  Bb7/4
Mas o que eu tô sentindo você finge que não vê
Eb7+              F7           Dm7   Gm7  Gm7+
Nosso amor é tão lindo e você longe de mim
      Cm  Dm  Eb  Eb  F7/9        Cm  Dm  Eb
Eu nunca amei assim,     eu nunca amei assim
Eb7+              F7           Dm7   Gm7  Bb7/4
Nosso amor é tão lindo e eu só penso em você
Eb7+              F7             Dm7      Gm7  Bb7/4
Mas o que eu tô sentindo você finge que não vê
Eb7+              F7           Dm7   Gm7  Gm7+
Nosso amor é tão lindo e você longe de mim
    Cm  Dm  Eb  Eb  F7/9        Cm  Dm  Eb
Eu nunca amei assim,     eu nunca amei assim

[Final] Gm7  Gm7/11


----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
A7/4 = X 0 2 0 3 0
A9 = X 0 2 2 0 0
B = X 2 4 4 4 2
Bb7/13 = 6 X 6 7 8 X
Bb7/4 = X 1 3 1 4 1
Bm7 = X 2 4 2 3 2
C#7+ = X 4 6 5 6 4
C#m7 = X 4 6 4 5 4
Cm = X 3 5 5 4 3
Cm7 = X 3 5 3 4 3
D = X X 0 2 3 2
D/E = X X 2 2 3 2
D7 = X X 0 2 1 2
D7+ = X X 0 2 2 2
D7/9- = X 5 4 5 4 X
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
E = 0 2 2 1 0 0
E/D = X 5 X 4 5 4
E7 = 0 2 2 1 3 0
Eb = X 6 5 3 4 3
Eb7+ = X X 1 3 3 3
Ebº = X X 1 2 1 2
F#m7 = 2 X 2 2 2 X
F7 = 1 3 1 2 1 1
F7/9 = X X 3 2 4 3
G = 3 2 0 0 0 3
G#m7/5b = 4 X 4 4 3 X
G7+ = 3 X 4 4 3 X
Gm7 = 3 X 3 3 3 X
Gm7+ = 3 5 4 3 3 3
Gm7/11 = 3 X 3 3 1 X
