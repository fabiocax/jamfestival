Turma do Pagode - Horas Iguais

Solo: 12-13-10 10-12-21

F/G                  G/A
Ie,ie,ie, ie,ie,ie, ie,ie,ie, ie,ie,ie
F/G                 C9
Ie,ie,ie, ie,ie,ie  ie    12-13-10  10-12-21   (2X)

F/G                                   G/A
Se você me perguntar,o que eu fico pensando

É fácil responder a hora passa
     F/G                                       C6/9
E eu sempre lembro de você e o dia é sempre assim.

F/G                                   G/A
Toda hora no relógio eu vejo sempre igual,
                                       F/G
Não sei o que acontece, não sei se é normal
                                    C6/9  Am7
Lembrar do teu sorriso é fácil pra mim.


Dm7                   G7                Em
O tempo me deixa tão perto do teu coração
Am7                       Dm
E mais uma vez no meu celular
F/G                     C6/9  Am7  12-21 30-31-32
E me deu vontade de ligar

Dm7                 G7                Em
E quem me segue pode ver, que eu tô fim.
Am7                    Ab7+
10:10 eu tô pensando em tí
F/G                      C9   C7(9)/Bb
Tá mais que na hora de te ama

F7+
A hora chegou depende de mim,
                 C6/9                 C7(9)/Bb
O horário não mente, tô mais que afim,
F7+
Eu vou na pressão, você gosta assim
             C6/9                C7(9)/Bb
Não posso mudar, faz parte de mim  (2x)


F/G                  G/A
Ie,ie,ie, ie,ie,ie, ie,ie,ie, ie,ie,ie
F/G                 C6/9
Ie,ie,ie, ie,ie,ie  ie    12-13-10  10-12-21   (2X)

----------------- Acordes -----------------
Ab7+ = 4 X 5 5 4 X
Am7 = X 0 2 0 1 0
C6/9 = X 3 2 2 3 3
C9 = X 3 5 5 3 3
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Em = 0 2 2 0 0 0
F/G = 3 X 3 2 1 X
F7+ = 1 X 2 2 1 X
G/A = 5 X 5 4 3 X
G7 = 3 5 3 4 3 3
