Turma do Pagode - Surreal

 Gm      Dm
É você, só você
  Gm       A7   Dm
A razão do meu querer
   Bb
Coisas de nós dois
   Gm         Dm
Faz sonhar, reviver
   Gm    A7        Dm
Coisas que eu nem sei dizer
    Bb          A7
Coisas de nós dois

Dm           C              Gm          A7
E nada mais importa se lá fora, nada é real
 Dm           C             Gm              A7
Pois quando amor é puro e verdadeiro é natural

               Dm                C
Teu jeito de amar, faz enlouquecer
                 Gm                 A7
Me faz perder o ar só de lembrar você

               Dm              C
Me fez enxergar, um mundo feliz
                 Gm
O amor assim é surreal
          A7
Surreal, surreal

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Gm = 3 5 5 3 3 3
