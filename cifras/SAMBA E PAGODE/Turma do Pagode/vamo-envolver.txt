Turma do Pagode - Vamo Envolver

  G                                      G7
Deixa de fofoca e vem pra cá
                     C                                    Am   D7
Se tá com medinho era melhor nem entrar lá laia
    G                                      G7
Deixa de fofoca e vem me ver
                       C                                          Am D7
Não to nem ligando pro que os outros vão dizer lê lê
                      Em                              Bm
Lá no meu pagode não sai do meu lado
                      Em                             G7
Não quer camarote, quer ficar no palco
                   C                          G
Na balada finge não me ver, não da pra entender
Am                                 C                                   D               Am D7
Se tá com vergonha já fala que eu não tenho tempo a perder
G                            G7                                  C
Você não me quer agora, porque eles vão dizer
                               D                         D7              G
Ou você me leva embora ou me escondo com você
                Bm                      C     D
Vamo envolver, vamo envolver lê lê lê lê lê lê lê lê

G                       G7                                 C
Você fica de cantinho disfarçando seu querer
                             D                D7                 G
Todo mundo ta ligado que tá louca pra me ter
                Bm                        C              D             D7
Vamo envolver, vamo envolver lê lê lê lê lê lê lê lê

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
