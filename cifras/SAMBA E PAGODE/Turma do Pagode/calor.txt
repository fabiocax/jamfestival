Turma do Pagode - Calor

2X
Am           F7+         Am    C79       F7+      E7/9
Lelele..... lelele...... lelele..............iererere
 Am                                                           F7+
O brilho da verdade nos teus olhos pude ver
 Bm7/5-   E7/9
mais uma vez
 Am                                                           F7+  E7/9
E essa vontade incessante tá difícil controlar, Eeee
 Am                                                                               F7+
E quanto mais o tempo passa mais aumenta o nosso gás
 Bm7/5-   E7/9
Eu e você
 Am                                                                     Abm7     Gm7    C7
Ultrapassamos os limites quando o corpo pede ma ........i...........s
     F7+                                                        G7
Calor, que leva a gente lá pros braços do infinito
  C7+                                                                      A7
Calor, que nesse instante deixa o mundo mais bonito
      Dm                                                            G7
Amor, quando estou perto de você nem acredito

                       C7+                               Gm                                         C7
Meu coração é seu lugar, vem me aquecer com seu calor
     F7+                                                        G7
Calor, que leva a gente lá pros braços do infinito
  C7+                                                                      A7
Calor, que nesse instante deixa o mundo mais bonito
  Dm                                                            G7
Amor, quando estou perto de você nem acredito
                       C7+              F7+ E7/9- Am        C7/9  F7+ E7
Como é bom te amar ......................bom te amar

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Abm7 = 4 X 4 4 4 X
Am = X 0 2 2 1 0
Bm7/5- = X 2 3 2 3 X
C7 = X 3 2 3 1 X
C7+ = X 3 2 0 0 X
C7/9 = X 3 2 3 3 X
C79 = X 3 2 3 3 X
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
E7/9 = X X 2 1 3 2
E7/9- = X X 2 1 3 1
F7+ = 1 X 2 2 1 X
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
