Turma do Pagode - Pesquisa No Google

[Intro] C  G  Am  F  G

C
Eu já tô prevendo meu arrependimento
G
E pra evitar qualquer sofrimento
Am
Já tô concordando antecipado
F                         Fm
Tentei me afastar, mas tô errado
C
E eu te peço um tempo e você me dá mais espaço
G
Mal dobro a esquina e já tô no seu abraço
Am
Prometo não falar seu nome mas cê vira assunto
F                                                G
O mundo conspira pra gente tá junto
            C
Pesquiso no Google: Saudade só dá você
G
No Google pesquiso: Amor só dá você

Am
Tento fugir do teu beijo e minha boca reclama
F                         Fm
No GPS ponho: Casa ele me leva pra sua cama


----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
