Turma do Pagode - Vai com Deus

  Bb                                Eb                           Bb
Vai com Deus que com Deus tudo pode da pé
                         F                        Bb
Eu garanto a você que a maré vai subir
                                      Eb
Vai com Deus que com Deus
                   Bb                        F7
se você se entregar vai rolar, vai rolar
Eb
Vai rolar um efeito maneiro
                                Bb
Vai pintar uma coisa legal
                                  Gm7
Você pode chegar em primeiro
                                   F
Até mesmo chegar no final
                                F
Mas não vai encontrar bandoleiro
                   F7                Bb        Fm7    Bb7
Que possa embaçar meu astral
Eb
Vai rolar uma coisa de festa

                                    Bb
Vai pintar um desejo de paz
                                     Gm7                       F
Quando você tiver de boa ou com trabalho demais
                     F>E>Eb                                          F7
Você vai se pegar o tempo inteiro querendo cantar
Bb                                Eb                       Bb
Vai com Deus que com Deus tudo pode da pé
                     F                            Bb
Eu garanto a você que a maré vai subir
                                     Eb
Vai com Deus que com Deus
                   Bb                         F7
se você se entregar vai rolar, vai rolar
Eb
Reza, pede, leva, pegue,
Cm7           F7               Bb
Pega leve, vai levando a vida
              Bb7
Deixa a bola rolar
Eb
Nessa ida, nessa briga
Cm7                          F7
A gente nunca pode vacilar
F7   (breque)
Lalaia lalaia lalaia

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bb7 = X 1 3 1 3 1
Cm7 = X 3 5 3 4 3
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
Fm7 = 1 X 1 1 1 X
Gm7 = 3 X 3 3 3 X
