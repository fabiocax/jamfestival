Turma do Pagode - Segunda a Sexta

Solo:30 32 33 25 25 23 23 34 34 35 35 32 32 34 35 10 15 17 15 13 10 35 33 45

Am7/5-                     Dm7         Gm7    G7/5+
Quer freqüentar todo pagode de segunda a sexta
        Am7/5-                      Dm7       Gm7    G7/5+
Não vou deixar você viver pra lá e pra cá
          Am7/5-                     Dm7           Gm7
Ou ta comigo ou leva a vida   de        solteira
                     Eb7+                        Gm7
Se ta me entendendo tu não faz cara feia
                    F7/4             F7        Bb7+
Se ta me querendo eu não to de bobeira
     Bb7           Am7/5-       Dm7  -   Gm7
Eu conheço a noite já cancei de zoar
  F#m7  Fm7  Bb7     Eb7+      F7             Bb7+
Vou       te       dizer    você vai ter que escolher

Por que
Fm7          Bb7                   Eb7+
Sabe que comigo não vai da pra ser
F7                   Bb7
Acho bom fica no seu lugar não ve

Fm7             Bb7          Cm7     Dm7       G7
Que já ta na hora de mudar você     não vê

----------------- Acordes -----------------
Am7/5- = 5 X 5 5 4 X
Bb7 = X 1 3 1 3 1
Bb7+ = X 1 3 2 3 1
Cm7 = X 3 5 3 4 3
Dm7 = X 5 7 5 6 5
Eb7+ = X X 1 3 3 3
F#m7 = 2 X 2 2 2 X
F7 = 1 3 1 2 1 1
F7/4 = 1 3 1 3 1 X
Fm7 = 1 X 1 1 1 X
G7 = 3 5 3 4 3 3
G7/5+ = 3 X 3 4 4 3
Gm7 = 3 X 3 3 3 X
