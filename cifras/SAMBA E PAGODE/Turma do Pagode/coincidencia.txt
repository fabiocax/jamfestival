Turma do Pagode - Coincidencia

E9                   G#m7          A7+
 Você me disse que queria me encontrar
                          C°
 Que coincidência, eu precisava conversar
E9                 G#m7        A7+
 Tava no carro e tocou meu celular
                      C°
 Não atendi, faltou coragem pra falar

F#m7
 Você insistia com mensagem e eu fugindo
G#m7                   C#m7
 Na minha cabeça tava tudo decidido
A7+                    B7/4
 Era difícil te contar, deu medo
 E ^ G#m7 ^ E ^ A7+                     B7/4
            Não teve jeito, eu tive que atender
E                     E5+               A7+
 Pra dizer que não dá mais, que tudo acabou
             C°
 Espero que você entenda

C#m              G#m7              A7+
 Que, aqui no coração, só cabe um amor
             C ^ D       E9
 Espero que você entenda

----------------- Acordes -----------------
A7+ = X 0 2 1 2 0
B7/4 = X 2 4 2 5 2
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
C° = X 3 4 2 4 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E5+ = 0 X 2 1 1 0
E9 = 0 2 4 1 0 0
F#m7 = 2 X 2 2 2 X
G#m7 = 4 X 4 4 4 X
