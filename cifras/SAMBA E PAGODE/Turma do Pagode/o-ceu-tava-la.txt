Turma do Pagode - O Céu Tava Lá

Intro: A7+  B/A  A7+  B/A  A7+  B/A  A7+  Bm7/9  E7(b9)

      A7+ B/A
De repente o céu estava lá
A7+ B/A
Tinha uma energia no ar
         G#m
Eu não sei se fiz por merecer
Bm7           E7/9
Sei que eu não queria te perder
         A7+ B/A
Sem dizer adeus me sorriu
          A7+ B/A
Se virou pro mar foi surtiu
        G#m
Anotou na areia o celular
        Bm7   E7/9
Já sabia que eu ia ligar
          Am7/9
Não deixei o sol me dizer
           D7/9
Que uma onda ia chegar

      G7+
Pra tirar de mim sem perceber
        Bm7/5- E7(b9)
Tudo que eu tinha de você
    Am7/9
Esperei a lua chegar
     D7/9
E deixei a noite passar
        G7+
Só então eu pude adormecer
    Bm7/5-       E7/9
Viajei no sonho pra te ver

Refrão:
       A7+        B/A
Gostei de ter você de vez na minha vida
       A7+
Quero acordar
B/A
Deixar a timidez adormecida
     G7+
Quero te amar

Depois voltar ao mar
         A/G
E ao mesmo céu com fé agradecer
   F7+
Por ter deixado nosso amor rolar
    E7/9
O amor é tudo que se pode ter.


----------------- Acordes -----------------
A/G = 3 X 2 2 2 X
A7+ = X 0 2 1 2 0
Am7/9 = X X 7 5 8 7
B/A = X 0 4 4 4 X
Bm7 = X 2 4 2 3 2
Bm7/5- = X 2 3 2 3 X
Bm7/9 = X 2 0 2 2 X
D7/9 = X 5 4 5 5 X
E7(b9) = X X 2 1 3 1
E7/9 = X X 2 1 3 2
F7+ = 1 X 2 2 1 X
G#m = 4 6 6 4 4 4
G7+ = 3 X 4 4 3 X
