Netinho de Paula - Bom Dia

[Intro] Bb  C  Am  Dm  Bb  C7  F7M  Gm  C7

    F7M                    Am7
A melhor coisa do mundo é amanhecer contigo
    Cm7         F7/9
E enquanto você dorme
      Bb           Bb6
Sussurrar no teu ouvido
       Bb        C
Te acordar devagarinho
    Am           D7
Bolinar no teu umbigo
     G7                     Gm   C7
Como ontem teu desejo fez comigo

     F7M                     Am7
E te ver espreguiçando com o rosto tão risonho
     Cm7         F7/9            Bb         Bb6
Como sempre convidando pra eu entrar no teu sonho
    Bb            C
E dizer que eu te amo

        Am          Dm
Se entregar com euforia
      Gm                C7        F
É a melhor maneira de dizer: "bom dia"

C7              F7M                             Am7
Começar um novo dia com essa alegria antes do café
                   Cm7
Antes de ir pro trabalho
            F7/9                 Bb
De sair pra rua, pro que Deus quiser
Bb6               Bb
Se esfrega no meu peito
            Bbm            Am
Deixa a sua marca no meu coração

    Dm               Gm
Pra eu ficar o dia inteiro
              C7                 F
Só curtindo o cheiro da nossa paixão

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Bb = X 1 3 3 3 1
Bb6 = X 1 3 0 3 X
Bbm = X 1 3 3 2 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Cm7 = X 3 5 3 4 3
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
F7/9 = X X 3 2 4 3
F7M = 1 X 2 2 1 X
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
