Sem Compromisso - O Novo Milênio

Introdução: E9 / A / E9 / A / E
   E                            E5+
Espero que nesse ano que está vindo
                 E6                      E7
Que o amor seja um hino, pra vida bem melhor
   A                      A5+
Eu quero ver casais apaixonados
     A6                                 A7
Passeando lado a lado, sem temer nada pior
      D                       G7
Não quero ver crianças pelas ruas
        A                                F#7/11     F#7  %
Andando sós e quase nuas, sem saber o que é amar
    D                      E7                A        D/E
Eu quero alegria todo o dia, pra que a gente possa sonhar
 E7       A            C#m7
Vamos curtir o novo milênio
       D7+                        D/E
Com a esperança de um mundo bem melhor
 E7      A           C#m7
Vamos pedir, sinceridade

      D7+                          D/E
Pra felicidade ser inteira, ser maior
     F#m7   C#m7   D7+    D/E          F#m7   C#m7  B7/11  B7
Ser maior.....ó...ó......um mundo....melhor... ó........ó
     E                        E5+
Que todos nossos sonhos na verdade
          E6                                   E7
Se tornem plena realidade, como o fruto vem da flor
    A                           A5+
Eu quero ver o mundo mais vivilizado
                   A6                                   A7
Sem preconceito de todo lado, sem distinção de raça ou cor
    D                          G7
Não quero ver nas filas os velhinhos
          A                                    F#7/11  F#7  %
Tão descrentes, tão sozinhos.....numa vida só de dor
   D                    E7             A         D/E
Eu quero ver menino tomar bênção e cuidar do seu avô
Refrão

----------------- Acordes -----------------
A = X 0 2 2 2 0
A5+ = X 0 3 2 2 0
A6 = 5 X 4 6 5 X
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
B7/11 = X 2 4 2 5 2
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
D/E = X X 2 2 3 2
D7+ = X X 0 2 2 2
E = 0 2 2 1 0 0
E5+ = 0 X 2 1 1 0
E6 = X X 2 4 2 4
E7 = 0 2 2 1 3 0
E9 = 0 2 4 1 0 0
F#7 = 2 4 2 3 2 2
F#7/11 = 2 4 2 4 2 X
F#m7 = 2 X 2 2 2 X
G7 = 3 5 3 4 3 3
