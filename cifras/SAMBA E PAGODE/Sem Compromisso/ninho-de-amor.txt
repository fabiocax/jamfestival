Sem Compromisso - Ninho de Amor

[Intro]  F Fm Em A7 D7/9 Fm G7 C6 Dm G7/9

  C               Gm7       C7    F
Sonhar é viver, viver com você um ninho de amor
 Fm         Bb7       Eb                   Dm   G7/9
Amor sem vaidades, paixão sem motim tão dengosa
    C                   Gm7        C7
No céu teu calor, o que eu mais sonhei
  F             Fm        Bb7
Você consagrou, muito desejei
        Eb                    Dm   G7/9
É meu sonhar, pois sonhar é viver
C     G                    Am
Vem tocando fundo o meu prazer
          A7/5+      Dm G7  C           G7/9
Eu não me canso de dizer te amo, te amo

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7/5+ = X 0 X 0 2 1
Am = X 0 2 2 1 0
Bb7 = X 1 3 1 3 1
C = X 3 2 0 1 0
C6 = 8 X 7 9 8 X
C7 = X 3 2 3 1 X
D7/9 = X 5 4 5 5 X
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
G7/9 = 3 X 3 2 0 X
Gm7 = 3 X 3 3 3 X
