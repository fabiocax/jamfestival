Sem Compromisso - Meu Caso

[Intro]  G7+  F#7  Bm  B7  Em  A7  D  Asus4

   D7+         C#7           Em
Morrer de amor não é tão mal assim
B7          Em           F#7           Bm   E7  A7
Se às vezes pensa em mim eu posso suportar, ah, ah
              D       B7                     Em
A dor de uma saudade quando a tristeza for embora
F#7               Bm          C#7
Vou reciclar meus sentimentos
                 F#m          Am  D7
Esquecer os bons momentos de amor
  G7+       F#7                Bm
É tão voraz quando estou apaixonado
C#7                   Em
Estar presente lado a lado
A7                       Am    D7
Vendo a expressão do seu amanhecer

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Asus4 = X 0 2 2 3 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C#7 = X 4 3 4 2 X
D = X X 0 2 3 2
D7 = X X 0 2 1 2
D7+ = X X 0 2 2 2
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F#7 = 2 4 2 3 2 2
F#m = 2 4 4 2 2 2
G7+ = 3 X 4 4 3 X
