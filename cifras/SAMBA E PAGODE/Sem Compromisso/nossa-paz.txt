Sem Compromisso - Nossa Paz

[Intro]  Dm  Am  Bm7/5-  E7  Am  Bm7/5-  E7

[Solo]  F  F  F  D  E  F
        E  E  E  C  D  E
        D  D  D  B  D
        F  E  D  B  D  C  A

Am            F7+  F#°
Se voltar pra mim
                 B7/5+      B7
Posso até considerar o amor
                     Bm7/5-
Esquecendo tudo que falou
E7                    Am   Gm
Nos momentos de desilusão
           A               Dm
Eu me entreguei de corpo e alma
G7(13)            C7+
Dei de tudo que podia dar
C°7                  Dm          E7
Nos meus sonhos você quis entrar

                Am       Em7/5-
Na fuga o meu silêncio
A7           Dm   G7(13)
Dizendo para mim
Dm         E7         Am     F#°7
Que fui traído por um beijo
                 Dm
Imaginando mil desejos
E7           Am
Eu acordei sozinho
A7              Dm                   G7
Me diga quem tu és, que digo quem eu sou
             C7+   E7                  Am
Eu tento refazer o que você mesmo estragou
A7           Dm                   E7            Am   A7
Preciso de você, meu bem, o que fazer pra recomeçar, ah ah ah
              Dm                  G7
Preciso de alguém, vem pra cá meu bem
               C7+   E7             Am
Eu quero te beijar, amar, amar, te amar
A7            Dm                 E7                 Am   Bm7/5- E7
Fazer muito carinho, vem meu amorzinho, vamos recomeçar

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
B7/5+ = X 2 X 2 4 3
Bm7/5- = X 2 3 2 3 X
C = X 3 2 0 1 0
C7+ = X 3 2 0 0 X
C°7 = X 3 4 2 4 2
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
Em7/5- = X X 2 3 3 3
F = 1 3 3 2 1 1
F#° = 2 X 1 2 1 X
F#°7 = 2 X 1 2 1 X
F7+ = 1 X 2 2 1 X
G7 = 3 5 3 4 3 3
G7(13) = 3 X 3 4 5 X
Gm = 3 5 5 3 3 3
