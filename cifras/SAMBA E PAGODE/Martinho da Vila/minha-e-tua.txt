Martinho da Vila - Minha e Tua

Intro:  Cm    C7    Fm    Dm7(5-)    G7    Cm

G7                Cm
Deus abençoa porque
C7              Fm
Somos o sol e a lua
                 Dm7(5-)
E quando há um eclipse
      G7             Cm
Minha vida é minha e tua
G7                   Cm
Num simples toque de olhar
C7                 Fm
Faz se sentir toda nua
               Dm7(5-)
E pra escandalizar
     G7            Cm
É só minha linda e pura
     G7
Vida minha
Cm      C7
Minha

Fm
Tua
Ddim
Minha
    G7             Cm
Tua vida é minha e tua
G7                 Cm
Ela é a terra virgem
C7               Fm
Eu semente de paixão
                    Dm7(5-)
Nossas lágrimas são chuva
       G7           Cm
Nossos corpos plantação
G7         Cm
É uma afrodisia
C7            Fm
A me fazer germinar
                  Eb7+
Desbravando o seu corpo
        Ab7+       Dm7(5-)
Sinto o tato das carícias
       G7          Cm
Que só eu posso provar

----------------- Acordes -----------------
Ab7+ = 4 X 5 5 4 X
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
Ddim = X X 0 1 0 1
Dm7(5-) = X X 0 1 1 1
Eb7+ = X X 1 3 3 3
Fm = 1 3 3 1 1 1
G7 = 3 5 3 4 3 3
