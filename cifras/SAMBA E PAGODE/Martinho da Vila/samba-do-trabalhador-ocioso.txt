Martinho da Vila - Samba do Trabalhador Ocioso

(intro)  A

            F#7                Bm    E7   A
Na segunda-feira não vou trabalhar
          F#7                    Bm   E7   A
Na terça não vou pra poder descansar
            F#7            Bm    E7      A
Na quarta preciso me recuperar, ê ê.....ê a
            F#7                  Bm    E7      A
Na quinta eu acordo meio dia, não dá, ê ê.....ê a
        F#7             Bm    E7       A
Na sexta viajo pra veranear, ê ê.....ê a
     F#7                       Bm    E7       A
No sábado vou pra Mangueira sambar, ê ê.....ê a
              F#7                     Bm    E7     A             (bis)
Domingo é descanso, eu não vou mesmo lá, ê ê.....ê a
              F#7               Bm       E7      A
Mas todo fim de mês, eu chego devagar, ê ê.....ê a
             F#7                   Bm    E7      A
Porque é pagamento, eu não posso faltar, ê ê.....ê a
         F#7           Bm          E7                  A
E, quando chega fim de ano......vou minhas férias buscar

      F#7           Bm             E7            A
E quero o décimo terceiro.......pro Natal incrementar

              F#7                Bm    E7     A     F#7       Bm   E7      A
Mas na segunda-feira não vou trabalhar, ê ê.....ê a, ê ê.....ê a, ê ê.....ê a
              F#7                   Bm
Eu não sei porque eu tenho que trabalhar

              E7                 A
Se tem gente ganhando de papo pro ar
             F#7                   Bm
Eu não, eu não vou.....eu não trabalhar
                E7                        A
Eu só vou, eu só vou......se salário aumentar
  F#7    Bm     E7/9     A
Ê ê.....ê a......ê ê.....ê a         (bis)

         F#7                 Bm    E7       A
A minha formação não é de marajá, ê ê.....ê a
             F#7                   Bm     E7       A
Minha mãe me ensinou foi colher e plantar, ê ê.....ê a
            F#7                  Bm     E7       A
Eu não, eu não vou.....eu não trabalhar, ê ê.....ê a
              F#7                       Bm     E7       A
Eu só vou, eu só vou......se salário aumentar, ê ê.....ê a

  F#7    Bm      E7     A
Ê ê.....ê a......ê ê.....ê a
To cansado !..

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
E7 = 0 2 2 1 3 0
E7/9 = X X 2 1 3 2
F#7 = 2 4 2 3 2 2
