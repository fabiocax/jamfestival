Martinho da Vila - Devagar, Devagarinho

D
É devagar, é devagar, é devagar, é devagar
     Em
Devagarinho
                 A7
É devagar, é devagar, é devagar, é devagar

Devagarinho

D
Devagarinho

É que a gente chega lá

Se você não acredita
                Em
Você pode tropeçar

E tropeçando

O seu dedo se arrebenta

                     D
Com certeza não se agüenta
   A7      D
E vai me xingar

D
É devagar, é devagar, é devagar, é devagar
     Em
Devagarinho
                 A7
É devagar, é devagar, é devagar, é devagar

Devagarinho

D
Eu conheci um cara
                       Em
Que queria o mundo abarcar

Mas de repente

Deu com a cara no asfalto
                    D
Se virou, olhou pro alto
        A7       D
Com vontade de chorar

D
É devagar, é devagar, é devagar, é devagar
     Em
Devagarinho
                 A7
É devagar, é devagar, é devagar, é devagar

Devagarinho

D
Sempre me deram a fama
                 Em
De ser muito devagar

E desse jeito

Vou driblando os espinhos
                      D
Vou seguindo o meu caminho
       A7       D
Sei aonde vou chegar

D
É devagar, é devagar, é devagar, é devagar
     Em
Devagarinho
                 A7
É devagar, é devagar, é devagar, é devagar

Devagarinho

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
