Martinho da Vila - Um Grande Amor

Am         Dm                E7      Am
Um grande amor sempre tem melanco...lia
                  Dm                E7     Am   A7
Tem tristeza e alegria num mundo de ilu....são

            Dm       E7               Am
Num grande amor.....tem de tudo um bocadinho
                   Dm     E7                 Am   A7
Tem ternura, tem carinho tem castigo e tem perdão
            Dm       E7               Am
Num grande amor.....tem de tudo um bocadinho
                    Dm    E7                 Am
Tem ternura, tem carinho tem castigo e tem perdão

              Dm             Am
Se o amor se esvai  saudade vem
         Dm    G7         C     A7
Um novo amor.......virá também

             Dm              E7
Se alguém brincou, sorriu, cantou

        Am
Feliz ficou, feliz ficou
          Bb              E7              Am
Se amor demais, perdeu a paz     chorou chorou
             Dm              E7
Se alguém brincou, sorriu, cantou
        Am
Feliz ficou, feliz ficou
          Bb             E7               Am    E7
Se amor demais, perdeu a paz     chorou chorou
          Am   E7           Am
Chorou chorou      chorou chorou

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
G7 = 3 5 3 4 3 3
