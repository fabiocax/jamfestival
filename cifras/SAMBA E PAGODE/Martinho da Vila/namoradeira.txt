Martinho da Vila - Namoradeira

  A7              D       A7             D
Quando entrei na roda a baiana olhou pra mim
  A7              D       A7             D
Quando entrei na roda a baiana olhou pra mim
Em  A7  D Bm
Ôh baiana
Em  A7  D
Ôh baiana
                       A7          D
Namoradeira que é que você está querendo
                       A7          D     A7  D
Namoradeira que é que você está querendo
                   A7
Com os olhos de manteiga
                  D     A7  D
ôh baiana se derretendo
                G
Você é muito formosa
         A7           D
Mas não sambo do seu lado
       B7        Em
Porque é muito fogosa

       A7        D
E já tem três namorados
     D7         G
O primeiro é polícia
                 D
O segundo é traficante
     A7              Em  A7           D  B7
O terceiro é valentão, é mau, é valentão
          Em  A7           D  B7
É valentão, é mau, é valentão
         A7          Em
Vou-me embora desse samba
            A7         D
Que eu não quero confusão
   D7           G
A barra da sua saia
A7   D
Ôh baiana
   D7            G
É que nem mamão papaia
A7   D
Ôh baiana
     D7             G
Seu cheiro é de manacá
A7   D
Ôh baiana
     D7               G
Seu beijo é que nem ingá
A7   D
Ôh baiana
    A7    D7+      D6
Mas eu só olho de banda
           D7+       D6
Não sambo na sua beira
          D             E7/9
Não quero zanga ôh baiana
    A7           D
Por mulher namoradeira

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D6 = X 5 4 2 0 X
D7 = X X 0 2 1 2
D7+ = X X 0 2 2 2
E7/9 = X X 2 1 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
