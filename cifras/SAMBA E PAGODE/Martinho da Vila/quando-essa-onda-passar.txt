Martinho da Vila - Quando essa onda passar

INTRO: G Am D7 G D7

   :           G                                                                      Am
   :       Quando essa onda passar      Vou te levar nas favelas
R  :                                    D7                                          G
e  :       Para que vejas do alto       Como a cidade é bela
f  :                                     G7                                                 C
r  :       Vamor a Boca do Mato Meu saudoso Pretos Forros
a  : b :     C                          D7                                               G
o  : i :  Quando essa onda passar       Vou te levar bem nos morros
   : s :
               G                                                                                    Am
        Não sei onde vamos primeiro             Quando essa onda passar
                                                 D7                                                 Am
        Formiga, Borel ou Salgueiro             Quando essa onda passar
                                                                                                       G
        Sei que vou lá na Mangueira             Pegar o Mané do Cavaco
                                                       D7                                                G
        E levar pra uma roda de Samba   No meu Morro dos Macacos

·       Refrão

G                                                                                           Am
É bom zuelar nas umbandas la no Vidigal Candomblés no Turano
                                             D7                                                   G
Um fanque, um forró, um calango         No Andaraí, Tuiuti ou Rocinha
                                                                                          Am
        Ver os fogos de fim de ano              Da porta de uma tendinha
   b :                                                  D7                                        G
   i :          E depois vamos dançar um jongo  Num terreiro da Serrinha
   s :

·       Refrão 2X

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
