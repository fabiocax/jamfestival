Martinho da Vila - Coração de Malandro

Intro : C

C                Am
Coração de malandro
              Dm
Não bate, balança
              G7
Pra lá e pra cá
                                 C
Se bater é bobeira e malandro dança

E malandro não pode dançar
                 Am
Coração de malandro
               Dm
Não bate, balança
              G7
Pra lá e pra cá
                               C
Se bater é bobeira e malandro dança


E malandro não pode dançar
Am      Dm
 Ê menina
           G7
A hora é essa
  C
Ê menina

Quem mandou você fazer promessa, menina!
Am      Dm
Ê menina
           G7
A hora é essa
      C
Ê menina   /
                            G7
Vamos embora deixa de conversa
C           D7         G7
Eu estava saindo do samba
              C      Am
Na hora que você chegou
             C            Dm
Pedindo qu'eu não fosse embora
           G7          C
Pra gente brincar de amor
             Am         Dm
Fiquei até romper da aurora
           G7       C
E agora o samba acabou
          Am         Dm
Comigo ajoelha, tem reza
          G7         C
Pra que você se ajoelhou

Oi menina!
Am     Dm
Ê menina
           G7
A hora é essa
  C
Ê menina

Quem mandou você fazer promessa, menina!
Am     Dm
Ê menina
          G7
A hora é essa
   C
Ê menina
                        G7
Vamos embora deixa de conversa
C              Am
Coração de malandro
               Dm
Não bate, balança
              G7
Pra lá e pra cá
                               C
Se bater é bobeira e malandro dança

E malandro não pode dançar
               Am
Coração de malandro
             Dm
Não bate, balança
             G7
Pra lá e pra cá
                                C
Se bater é bobeira e malandro dança

E malandro não pode dançar
Am    Dm
Ê menina
           G7
A hora é essa
Am    Dm
Ê menina

Quem mandou você fazer promessa, menina!
Am     Dm
Ê menina
            G7
A hora é essa
   C
Ê menina
                            G7
Vamos embora deixa de conversa
C           D7         G7
Eu estava saindo do samba
              C     Am
Na hora que você chegou
            C            Dm
Pedindo que não fosse embora
             G7        C
Pra gente brincar de amor
            Am          Dm
Fiquei ate romper da aurora
           G7      C
E agora o samba acabou

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
G7 = 3 5 3 4 3 3
