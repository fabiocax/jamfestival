Martinho da Vila - Brasil Mulato

Bm        F#7            Bm
Pretinha, procure um branco
                           B7             Em
Porque é hora de completa integração
            Em7M        Em7
Branquinha, namore um preto
        C#m7/5-   F#7            Bm   F#7
Faça com ele a sua miscigenação
Bm         F#7        Bm
Neguinho, vá pra escola
Ame esta terra
               B7
Esqueça a guerra
               Em
E abrace o samba
          C#m7/5-   F#7            Bm
Que será lindo o meu Brasil de amanhã
           C#m7/5-       F#7            Bm
Mulato forte, pulso firme e mente sã
           A7                 G7             F#7
Que será lindo o meu Brasil de amanhã

        C#m7/5-       F#7            Bm
Mulato forte, pulso firme e mente sã
      B7     Em                       A7             D7M
Quero ver madame na escola de samba sambando
G7M             C#m7/5-
Quero ver fraternidade
       F#7            Bm
Todo mundo se ajudando
                             C#m7/5-
Não quero ninguém parado
      F#7            Bm
Todo mundo trabalhando
                        A7         G7   F#7
Que ninguém vá a macumba fazer feitiçaria
                  C#m7/5-       F#7            Bm
Vá rezando minha gente a oração de todo dia
Em                  F#7       Bm
Mentalidade vai mudar de fato
          C#m7/5-       F#7    Bm
O meu Brasil então será mulato
C#m7/5-          F#7  Bm
Mulato,          mulato
C#m7/5-          F#7  Bm
Mulato,          mulato

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C#m7/5- = X 4 5 4 5 X
D7M = X X 0 2 2 2
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
Em7M = X X 2 4 4 3
F#7 = 2 4 2 3 2 2
G7 = 3 5 3 4 3 3
G7M = 3 X 4 4 3 X
