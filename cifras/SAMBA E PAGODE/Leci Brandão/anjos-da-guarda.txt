Leci Brandão - Anjos da Guarda

Intro: D / G / D / A / }2x

D
Professores
   F#m7     G                   A7
Protetores das crianças do meu país
 D            F#m7
Eu queria, gostaria
  G                 A     F#m7
De um discurso bem mais feliz
 G             A    D
Porque tudo é educação
 G           A        D
É matéria de todo o tempo
D                   F#m7
Ensinem a quem sabe tudo    }
 G              A     D       bis
A entregar o conhecimento   }
          A
Na sala de aula             ]
                     D
É que se forma um cidadão   ]

           A
Na sala de aula             ]
                D
Que se muda uma nação       ]   bis
            A
Na sala de aula             ]
                   D
Não há idade, nem cor       ]
           A
Por isso aceite e respeite  ]
   G     D
O meu professor             ]
                 A
Batam palmas pra ele   }
                D
Batam palmas pra ele   }
                A          bis
Batam palmas pra ele   }
           D
Que ele merece..       }

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
F#m7 = 2 X 2 2 2 X
G = 3 2 0 0 0 3
