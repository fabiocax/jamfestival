Leci Brandão - Café Com Pão / Eu Só Quero Te Namorar

Introdução: Em
  E7        Am   D        Aº     Em
Acorda meu amor....é hora de levantar            }
     Em/D           Am          B7          Em
Tem café com pão torrado...só falta você provar  }
  E7        Am   D        Aº     Em                 Refrão
Acorda meu amor....é hora de levantar            }
     Em/D           Am          B7          Em
Tem café com pão torrado...só falta você provar  }
   Em/D              F#m5-/7      B7        Em
Interrompo esse teu sono...nessa hora todo dia
                  Bm5-/7     E7           Am
Pra trabalho sem abono...enfrentar com valentia
                     F#m5-/7      B7          Em   Ebm
O teu ronco é tão gostoso...teu momento de descanso
      Dm   G7      C7+          Am       B7     Em
O teu jeito preguiçoso...despertando o corpo manso
Refrão
      E7         Am
Eu só quero te namorar                   }
              D7                   G
Deixa eu te abraçar, deixa eu te beijar  }

     Em/D                C7
Não sei o que você vai pensar            }  Refrão 2
         B7        Em
Mas só quero te namorar                  }
      E7         Am
Eu só quero te namorar                   }
              D7                   G
Deixa eu te abraçar, deixa eu te beijar  }
     Em/D                C7
Não sei o que você vai pensar            }
         B7        Em
Mas só quero te namorar                  }
E7                  Am             D7                G   B7  Em
Marcar encontro na praça e fazer pirraça quando se zangar
                   Am               D7           G
mandar um cartão florido fazendo pedido pra desculpar
   E7              Am                  B7               Em
Sentar no sofá da sala com um saco de bala pra gente chupar
    Em/D                C7                 B7               Em
Não pense que isso é loucura, mas minha ternura pra te conquistar
Refrão 2
    E7          Am                 D7                    G   B7  Em
No dia dos namorados juntar uns trocados para o nosso jantar
                 Am                  D7              G
Mostrar minha simpatia quando pra família se apresentar
   E7        Am           B7             Em
A bela caligrafia na fotografia pra te dedicar
     Em/D              C7                  B7                 Em
Não pense que isso é antigo, mas um jeito amigo pra gente se amar..
Refrão 2

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Aº = 5 X 4 5 4 X
B7 = X 2 1 2 0 2
Bm5-/7 = X 2 3 2 3 X
C7 = X 3 2 3 1 X
C7+ = X 3 2 0 0 X
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
Ebm = X X 1 3 4 2
Em = 0 2 2 0 0 0
Em/D = X X 0 4 5 3
F#m5-/7 = 2 X 2 2 1 X
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
