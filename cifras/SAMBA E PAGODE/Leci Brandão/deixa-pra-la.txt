Leci Brandão - Deixa Pra Lá

Intro 2x: Fm7  F7  C#7  C7

Verso 1:
                 F7                       Bbm7
Se teu amor falou que não vai mais voltar
C7           Fm7
Deixa pra lá
        F7                    Bbm7
Se você ficou em último lugar
C7           Fm7
Deixa pra lá
           F7                    Bbm7
Se tudo começou na hora de acabar
C7           Fm7
Deixa pra lá
            F7                      Bbm7
Se você não passou nesse vestibular
C7           G#7M
Deixa pra lá

Refrão:

                Am7(5b)      Bbm7
Deixa, que essa vida um dia muda
D#7(9)                  G#7M
você tem que se assumir
               B7(5b)      Bbm7
E se o próprio amigo o acusa
     F#/G#  F/G  Fm7
Você tem    que  resistir

Verso:
Se não tem viola pra te acompanhar
Deixa pra lá
Se nesse ano a escola não vai desfilar
Deixa pra lá
Se você pediu tanto e ninguém quis lhe dar
Deixa pra lá
Se também fez um canto pra ninguém cantar
Deixa pra lá

Refrão:
Deixa que essa fase é passageira
Amanhã será melhor
E você vai ver que a cidade inteira
Seu samba sabe de cor

Verso:
Se você quer seresta e já não tem luar
Deixa pra lá
Se você foi à festa e não pode dançar
Deixa pra lá
Se a sua companheira já não quer lhe dar
Deixa pra lá
Aquele amor antigo que só faz vibrar
Deixa pra lá

Refrão:
Deixa, não perturbe a sua vida,
Carnaval já vem aí
Vou brincar com o povo na avenida
Descobrindo o que não vi

Verso:
Se você tem idéia e não pode falar
Deixa pra lá
Se cantou pra platéia e ninguém quis ligar
Deixa pra lá
Se você foi à feira e não pode comprar
Deixa pra lá
Porque o dinheiro é curto pra poder gastar
Deixa pra lá

Refrão:
Deixa que essa vida um dia muda...

----------------- Acordes -----------------
Am7(5b) = 5 X 5 5 4 X
Bbm7 = X 1 3 1 2 1
C#7 = X 4 3 4 2 X
C7 = X 3 2 3 1 X
D#7(9) = X 6 5 6 6 X
F#/G# = 4 X 4 3 2 X
F/G = 3 X 3 2 1 X
F7 = 1 3 1 2 1 1
Fm7 = 1 X 1 1 1 X
G#7M = 4 X 5 5 4 X
