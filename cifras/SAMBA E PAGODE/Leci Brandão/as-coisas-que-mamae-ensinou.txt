Leci Brandão - As Coisas Que Mamãe Ensinou

Introdução: F
                  F          Bb7      F
Dizem que mãe da gente é um caso diferente
                                 D7          Gm   F7
Muito mais que comovente que não dá pra comparar
          Bb        Eb7         Am
O que eu sei, é que tudo que eu sou
        Dm          Gm               C7              F
Simplesmente é resultado das coisas que minha mãe ensinou
           Dm               Gm
Das coisas...que mamãe ensinou   }
           C7               F
Das coisas...que mamãe ensinou   }
           Dm               Gm      bis
Das coisas...que mamãe ensinou   }
           C7               F
Das coisas...que mamãe ensinou   }
         Dm            Gm           C7          F
Carne assada que se preze...tem que ter aquela cor
                    Cm
Se não quer manda embora

              F7             Bb
Mas não faça hora com o seu amor
                   Eb7                      Am
Um bom dia um boa tarde, com licença, por favor
       Dm        Gm               C7            F
Tudo isso é resultado das coisas que mamãe me ensinou..
REFRÃO

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bb7 = X 1 3 1 3 1
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Eb7 = X 6 5 6 4 X
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
Gm = 3 5 5 3 3 3
