Jorge Aragão - História do Brasil

   E
Eu vi
   C#7            F#m F#m7+F#m7+
Não vou esquecer jamais
       B7                          E
De alguém que fez dos desiguais
Ebm75+
Um povo unido em mutirão
          C#m7
Numa só direção
 F#
Pra não ser vencido
         F#m75-      B75 B7
Pelas garras da opressão
 Em
Eu vi
                        B/E b
Das planícies e serras
                      E7/D
Dos confins desta terra
                  A/C#
Elevar-se um anseio

      D     B/Eb              Em
Tão forte mas calou como veio
                       F#E
Quando a sombra da morte
                F#m75-
Encobriu todos nós
  B7
Juro que...
    Em
Eu vi
                       B/Eb
Quase tudo deu certo
                       E/D
Quem não viu chegou perto
                     A/C#
Mas nos legou um sonho
D   B/E b
Risinho
Em
Hoje, vejo meu povo
              F#/E
Merecendo de novo
F#m75- B7 EmE7
Ser feliz
A7      D7G7+C7
Agora é lutar
          F#m75-B7 Bm75- E
Por tudo que ele quis
A7      D7 G7+C7
É hora de mudar
           Bm75-B7 Em E7
Conheço o meu país
A7      D7 G7+C7
Agora é lutar
          F#m75-B7 Bm75- E
Por tudo que ele quis
A7      D7 G7+C7
É hora de mudar
           Bm75-B7 Em E7
Conheço o meu país

----------------- Acordes -----------------
A/C# = X 4 X 2 5 5
A7 = X 0 2 0 2 0
B/E = X 7 9 8 7 7
B/Eb = X 6 X 4 7 7
B7 = X 2 1 2 0 2
C#7 = X 4 3 4 2 X
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
E/D = X 5 X 4 5 4
E7 = 0 2 2 1 3 0
E7/D = X 5 X 4 5 4
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
F#/E = X X 2 3 2 2
F#m = 2 4 4 2 2 2
