Jorge Aragão - Iguais nunca mais

Introdução: Cm

Cm
Olhar de dois samurais
         Fm
Fiéis pagando pra ver
             Bb7
Manchete em todos jornais
           Eb7+        Eb6
É o fim do amor pode crer
       Dm5-/7
São temporais vendavais
   G7/9-      Cm
Que a gente tem que esquecer
              Ab7
Que é pra escrever nos murais
         G7
Iguais a nós nunca mais
         Cm
Duvido até de onde vim
           Fm
Quando me lembro de nós

             Bb7
Tão longe perto de mim
          Eb7+      Eb6
Assim, assim meio a sós
       Fm        Bb7
Continuamos a fim
           Eb7+  Eb6
Trocentos anos após
         Dm5-/7    G7
Gritando pra quem for
       Cm       C7
Até perder a voz
         Fm         Bb7
O nosso amor é assim
         Eb7+        Eb6
Leal a todas as crenças
             Dm5-/7
Se bem que a gente
             G7          Cm   C7
Anda meio abusando das ofensas
                           Cm   G7/9-
(Anda meio abusando das ofensas) 2x


----------------- Acordes -----------------
Ab7 = 4 6 4 5 4 4
Bb7 = X 1 3 1 3 1
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
Dm5-/7 = X X 0 1 1 1
Eb6 = X X 1 3 1 3
Eb7+ = X X 1 3 3 3
Fm = 1 3 3 1 1 1
G7 = 3 5 3 4 3 3
G7/9- = 3 X 3 1 0 X
