Jorge Aragão - Ponta de Dor

F
Introdução:  Gm7   C7/13  F7+   Bb7  Em7/5- A7 Dm Gm7 C7

    F             Em                   A7   Dm
  Suas lágrimas  são o bastante pra mim dizer
                          Am                 F7   Bb
Que se houvesse perdão pra mim não poderia ser
                   Eb7/9                     Am
Dessa vez foi demais     e eu fui incapaz do amor
             Dm     Db7
Que você me deixou, faça o que preciso for.
C7 Fm                  Fm7+
      Só não vá revelar     o quanto fui ruim,
Fm7                     Fm6                Bbm
   Não queria se igualar, você não é assim,
Cada um da o que tem.
Eb7/9        Ab7+                   Db7
Você me deu amor, devolvi com desdem, uma ponta de dor,
Gm5-/7                     C7   F
       Mas não fiz por maldade.
VocÊ pode esperar

C7       C#º          Dm7       A7
 Essa felicidade um dia virá,
                          D7
Ou quem sabe um amor quem venha ficar, pra vida
G7
inteira.
        C7                           F
Mas nem sempre o que espera dá pra chegar.
Suas lágrimas são...

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Ab7+ = 4 X 5 5 4 X
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bb7 = X 1 3 1 3 1
Bbm = X 1 3 3 2 1
C#º = X 4 5 3 5 3
C7 = X 3 2 3 1 X
C7/13 = X 3 X 3 5 5
D7 = X X 0 2 1 2
Db7 = X 4 3 4 2 X
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Eb7/9 = X 6 5 6 6 X
Em = 0 2 2 0 0 0
Em7/5- = X X 2 3 3 3
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
F7+ = 1 X 2 2 1 X
Fm = 1 3 3 1 1 1
Fm6 = 1 X 0 1 1 X
Fm7 = 1 X 1 1 1 X
Fm7+ = 1 X 1 1 1 X
G7 = 3 5 3 4 3 3
Gm5-/7 = 3 X 3 3 2 X
Gm7 = 3 X 3 3 3 X
