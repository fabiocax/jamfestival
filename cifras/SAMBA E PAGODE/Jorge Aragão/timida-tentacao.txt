Jorge Aragão - Tímida Tentação


G
Um olhar
              F#m7/5-   B7
Um sorriso no ar
            Em
Uma tímida  tentação
                F#7            Bm
Nós brindamos a paz pela sedução
               Dm   E7  Am       D7
E brincamos de amor num bar devagar
Dm         E7
Sem nos tocar
    Am       D7             G  D7
Seu nome assumindo seu lugar
G
Se você quer saber
     D6    Dm6
Tá doendo
              G7    C   Am Bbm Bm Cm
A saudade eu até entendo


                    Gm
Nada houve afinal
E nem foi tão ruim
Cm            D7           G G7
Ligue um dia então pra mim
Cm            D7        G
Ligue um dia então pra mim

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
Bbm = X 1 3 3 2 1
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D6 = X 5 4 2 0 X
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Dm6 = X 5 X 4 6 5
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F#7 = 2 4 2 3 2 2
F#m7/5- = 2 X 2 2 1 X
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
