Jorge Aragão - Rede Velha

Introdução:  [[: C#m 75 / BmC#m 75 / BmC#m 75 ? BmC ]]
   B                F#Bb            A13 G#7  C#m7
Vamos lá que um desse é ruim de achar, então
              G713              F#
Já que tá na dança ? bora ? aproveitar, que é bom
              F#/Bb             A13  G#7   C#m
Outro xote arrumadinho assim pra namorar, sei não
                 G713                 F#7
Essa boca é pouca prá quem quer beijar
Bm
Tá chovendo cabra dde tocaia no rastro dessa
F#7
morena
Bm                                                     F#7
Só pra ter desfrute desse acho, estão doido, dá dó de pena
   B7
Tá vendo ela faz gosto percebendo um tantinho do
      Em
Meu amor
    A7
O rumo que eu mais quero nessa vida

           D
Onde ela for
                               C#7
Pecado não queinmar no fogo dela
     C7                          Bm
Deixando  o povo se rasgar de dor
   D7                            C#7
Deitar no fundo de uma rede velha
   C7                     Bm
E me resfatelar no seu amor

----------------- Acordes -----------------
A13 = 5 X 4 6 5 X
A7 = X 0 2 0 2 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C#7 = X 4 3 4 2 X
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
C7 = X 3 2 3 1 X
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
F#/Bb = 6 X 4 6 7 X
F#7 = 2 4 2 3 2 2
G#7 = 4 6 4 5 4 4
