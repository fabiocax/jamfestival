Jorge Aragão - Espelho d'água

[Intro] A  G  F#  F  G  A

A
Hum
      A7M                  B/A
Seus olhos são espelhos d'agua
Bm/A         E7(9)
Brilhando você
            A7M   E 7/4/9
Pra qualquer um
A
Hum
     A7M              B/A
Por onde esse amor andava
Bm/A          E7(9)
Que não quis você
           F#m7(9)  Em7  A7(9)
De jeito algum
D7M
Huuum
      E/D             C#m7(b5)
Que vontade de ter você

         A#/G        D#m7(b5)
Que vontade de perguntar
Dm6          A    Em7   A7
Se ainda é cedo
D7M
Huuuum
     E/D             C#m7(b5)
Que vontade de merecer
        A#/G          D#m7(b5)
Um cantinho do seu olhar
Dm6         A
Mas tenho medo


----------------- Acordes -----------------
A = X 0 2 2 2 0
A#/G = 3 X 3 3 3 X
A7 = X 0 2 0 2 0
A7(9) = 5 X 5 4 2 X
A7M = X 0 2 1 2 0
B/A = X 0 4 4 4 X
Bm/A = X 0 4 4 3 X
C#m7(b5) = X 4 5 4 5 X
D#m7(b5) = X X 1 2 2 2
D7M = X X 0 2 2 2
Dm6 = X 5 X 4 6 5
E = 0 2 2 1 0 0
E/D = X 5 X 4 5 4
E7(9) = X X 2 1 3 2
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
F#m7(9) = X X 4 2 5 4
G = 3 2 0 0 0 3
