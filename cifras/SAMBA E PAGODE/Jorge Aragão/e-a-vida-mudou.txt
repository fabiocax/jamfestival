Jorge Aragão - E a Vida Mudou

[Intro] Em  D  Em  B7
 Em  Am  Em  B7

Em                  F#7
Quando você quiser,fidelidade eu dou
Am                 B7          Em  B7
Não quero ter amor de outra mulher
Em                 F#7
Fingindo solidão,comprava o meu prazer
Am                       Em
Foi só te conhecer, (pois) é
B7           Em   B7            Em
Minha vida mudou.Minha vida mudou

D7                            G
Parei com a bebida,com a noitada
                   B7
Me afastei da batucada
              Em
Minha vida mudou
D7                         G
Só sei que te quero,do meu lado

                  B7
Pra amar e ser amado
              Em  B7          Em
Minha vida mudou.Minha vida mudou
B7                               Em
Me achei tão perdido,sofri um bocado
                B7             Bm7/5- E7
Mas não faz sentido,lembrar do passado
               A7              D7  G
Cantei um bom samba,matei a saudade
C7+       F#m7/5-  B7 Bm7/5-   E7
Falando de encantos e felicidade
A7                     D7             Em
Revi teu sorriso,e vi que você aceitou
B7          Em     B7           Em
Minha vida mudou,minha vida mudou

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
Bm7/5- = X 2 3 2 3 X
C7+ = X 3 2 0 0 X
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F#7 = 2 4 2 3 2 2
F#m7/5- = 2 X 2 2 1 X
G = 3 2 0 0 0 3
