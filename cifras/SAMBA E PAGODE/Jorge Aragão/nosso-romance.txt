Jorge Aragão - Nosso Romance

[Intro] F7+  B7/5+  Em  A7/9-  C/D  D7(9/11+)  F/G  G7/9-

C6/9
Todo o romance é parecido
Bm7/5-            E7/9-
Eu nunca vi um diferente
Am7+              Am7
Começa mais ou menos frio
Gm7                C7
Depois se torna muito quente
F7+              G/F
E sem a gente perceber
Em7                 A7/9-
Vai nos deixando novamente
D7/9                        Fm6  G7
Na condição de viver perigosamente

Cm
Que eu tenho a luz pra iluminar
G/B
E me entregar que eu posso ainda

Gm7/5-           C7
Amando vou acreditar
Fm7
Que toda a doce era bem-vinda
Ab/Bb               Bb7/9-
Não há prisão nem fortaleza
Eb7+                       Ab7+
Quando ele invade a nossa vida
D/C
O amor é assim minha princesa
Dm7/5-               G7
É eterno até a despedida

C6/9
Quem ama
                                              D/C
Traz do lado esquerdo do peito esse amor em chama
                                             Dm7
Onde há fogo de palha não queima nem molha a cama
F/G                                     G7  Gm7
É o melhor sentimento que Deus nos presenteou
C7/13
Presenteou

F7+  F6
Meu amor
F#m7/5-                 B7/5+         Em7/5-
Já que nosso romance não corre nenhum perigo
                    A7/5+              A7  Dm7/5-
Aproveito essa chance deixo o coração contigo
G7/5+                         G7  Gm7
Pode usar e abusar assim que precisar do amor
C7/13
Do nosso amor

F7+  F6
Meu amor
F#m7/5-                 B7/5+         Em7/5-
Já que nosso romance não corre nenhum perigo
                    A7/5+              A7  Dm7/5-
Aproveito essa chance deixo o coração contigo
G7/5+                         G7
Pode usar e abusar assim que precisar

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7/5+ = X 0 X 0 2 1
A7/9- = 5 X 5 3 2 X
Ab/Bb = X 1 1 1 1 X
Ab7+ = 4 X 5 5 4 X
Am7 = X 0 2 0 1 0
Am7+ = X 0 2 1 1 0
B7/5+ = X 2 X 2 4 3
Bb7/9- = 6 X 6 4 3 X
Bm7/5- = X 2 3 2 3 X
C/D = X X 0 0 1 0
C6/9 = X 3 2 2 3 3
C7 = X 3 2 3 1 X
C7/13 = X 3 X 3 5 5
Cm = X 3 5 5 4 3
D/C = X 3 X 2 3 2
D7(9/11+) = X X 0 1 1 0
D7/9 = X 5 4 5 5 X
Dm7 = X 5 7 5 6 5
Dm7/5- = X X 0 1 1 1
E7/9- = X X 2 1 3 1
Eb7+ = X X 1 3 3 3
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
Em7/5- = X X 2 3 3 3
F#m7/5- = 2 X 2 2 1 X
F/G = 3 X 3 2 1 X
F6 = 1 X 0 2 1 X
F7+ = 1 X 2 2 1 X
Fm6 = 1 X 0 1 1 X
Fm7 = 1 X 1 1 1 X
G/B = X 2 0 0 3 3
G/F = 1 X X 0 0 3
G7 = 3 5 3 4 3 3
G7/5+ = 3 X 3 4 4 3
G7/9- = 3 X 3 1 0 X
Gm7 = 3 X 3 3 3 X
Gm7/5- = 3 X 3 3 2 X
