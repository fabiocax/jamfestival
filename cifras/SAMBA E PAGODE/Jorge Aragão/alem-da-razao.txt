Jorge Aragão - Além da Razão

Eb
Introdução:  Eb Bb7 Gm Cm Fm Bb7 Eb Bb7

 Eb          D7       Ab6                     Gm7  C7
Por te amar eu pintei...um azul pro céu se admirar
Fm       G#m    B7                       Bb7
Até o mar adocei...e das pedras leite eu fiz brotar
Eb          D7       Ab6                      Gm7   C7
De um vulgar fiz um rei...e do nada o império pra te dar
Fm        G#m     B7                         Bb7
E a cantar eu direi o que eu acho então o que é amar.
       Eb                Bb
É uma ponte... lá para o longe
       Eb7                   Ab
Do horizonte...jardim sem espinhos
G#m           C#7 Gm               C7
Vinho que vai bem...em qualquer cancão
F7/9              Fm           Bb7/9
Roupa de vestir...qualquer estação
       Eb               Bb
É uma dança...paz de criança

            Eb7                 Ab
Que só se alcança...se houver carinho
G#m      C#7 Gm              C7
É estar além...da simples razão
F7/9                Fm         Bb7/9
Basta não mentir...pro seu coração

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Ab6 = 4 X 3 5 4 X
B7 = X 2 1 2 0 2
Bb = X 1 3 3 3 1
Bb7 = X 1 3 1 3 1
Bb7/9 = X 1 0 1 1 X
C#7 = X 4 3 4 2 X
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
Eb = X 6 5 3 4 3
Eb7 = X 6 5 6 4 X
F7/9 = X X 3 2 4 3
Fm = 1 3 3 1 1 1
G#m = 4 6 6 4 4 4
Gm = 3 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
