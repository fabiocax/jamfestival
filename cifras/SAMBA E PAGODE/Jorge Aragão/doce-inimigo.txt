Jorge Aragão - Doce Inimigo

G7 C  C7+                    Eb°   Em7
 Amor minha trilha minha estrada, amor
              Em5-/7 A7  Dm
Sinfonia de calçada     amor
Fm                    C  Bb7  A7
 Sem paixão não vale nada
                     Ab7
É uma história abandonada
 G7                  C
Mais um tema pra compor
C  C7+                   Eb°   Em7
Amor é perdão por quase nada amor
Fm7                   Bb7        Eb7+
 Doce inimigo sabe quando o coração
             Ab7             Db7
Corre perigo seu poder de sedução
        G7     Cm      G7
Sempre tem razão,   porque
Cm
Quando você foge da saudade
pode crer que isso é amor

Ab7
Quando foge da saudade
pode crer que isso é amor
Db7
Dessa vez agora a gente chora
                      Fm G7  Cm G7
Pode crer que isso é amor,  amor
Db7
E se alguém cantar é amor, amor
                      Fm G7  Cm G7
Pode crer que isso é amor,  amor

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Ab7 = 4 6 4 5 4 4
Bb7 = X 1 3 1 3 1
C = X 3 2 0 1 0
C7+ = X 3 2 0 0 X
Cm = X 3 5 5 4 3
Db7 = X 4 3 4 2 X
Dm = X X 0 2 3 1
Eb7+ = X X 1 3 3 3
Eb° = X X 1 2 1 2
Em5-/7 = X X 2 3 3 3
Em7 = 0 2 2 0 3 0
Fm = 1 3 3 1 1 1
Fm7 = 1 X 1 1 1 X
G7 = 3 5 3 4 3 3
