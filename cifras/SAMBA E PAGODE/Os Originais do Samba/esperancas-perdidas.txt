Os Originais do Samba - Esperanças Perdidas

Intr.: Dm   D7   Gm   C7  F  A7

  Dm               D7                    Gm
Quantas belezas deixadas nos cantos da vida
C7                                          F   A7
Que ninguém quer e nem mesmo procura encontrar
Dm                 D7                       Gm
 E quantos sonhos se tornam esperanças perdidas
 Em7/5-               A7                    Dm   A7
 Que alguém deixou morrer sem nem mesmo tentar

 Dm               D7                 Gm
Minha beleza encontro no samba que faço
C7                                    F     A7
Minhas tristezas se tornam um alegre cantar
Dm               D7                    Gm
 É que carrego o samba bem dentro do peito
  Em7/5-            A7              Dm
 Sem a cadência do samba não posso ficar

            A7                  Dm
Não posso ficar... eu juro que não

            D7                Gm
Não posso ficar... eu tenho razão
                           Em7/5-
Já fui batizado na roda de bamba
             A7                  Dm    A7
O samba é a corda... eu sou a caçamba

  Dm               D7                    Gm
Quantas noites de tristeza ele me consola
C7                              F   A7
Tenho como testemunha a minha viola
Dm                D7                      Gm
Ai... se me faltar o samba não sei o que será
 Em7/5-               A7            Dm
 Sem a cadência do samba não posso ficar

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
C7 = X 3 2 3 1 X
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Em7/5- = X X 2 3 3 3
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
