Os Originais do Samba - Do lado direito da rua direita

Introdução: A7+  A6  (2x)  Bm7  E9  (2x)


A7+ A6      A7+  A6         Bm7  E9  Bm7  E9
Do.....lado direito, da rua Direita
  Bm7  E9     Bm7      E9   A7+  A6  A7+  A6
Olhando as vitrines coloridas eu  a  vi
    A7+      A6         A7+    A6          Bm7   E9    Bm7   E9
Mas quando quis me aproximar de ti não tive tempo
    Bm7  E9    Bm7        E9       A7+   A6  A7+  A6
No movimento imenso da rua eu lhe perdi
Bm7           E9             C#m7             F#7
......E cada menina que passava......para seu rosto eu olhava
  Bm7     E9     Bm7       E9      A7+       Bm7         E9               A7+
E me enganava pensando que fosse você....e na rua Direita eu voltarei pra lhe
ver

----------------- Acordes -----------------
A = X 0 2 2 2 0
A6 = 5 X 4 6 5 X
A7+ = X 0 2 1 2 0
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
E9 = 0 2 4 1 0 0
F#7 = 2 4 2 3 2 2
