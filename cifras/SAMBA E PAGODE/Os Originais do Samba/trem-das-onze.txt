Os Originais do Samba - Trem das Onze

[Intro] Fm  F#°  Cm/G  G#  Am7(b5)  G/B   Gm7(b5)  C7
        Fm  G7  Cm  D#/A#  D7  G7  Cm    G7

            Cm
Não posso ficar
            Cm5+         Cm6    Cm5+
Nem mais um minuto com você
         Cm
Sinto muito amor
G#6           G7
Mas não pode ser
Fm  A#7    D#    G7/D
Moro em Jaçanã
Cm     D#/A#       G#
Se eu perder esse trem
         Fm            G7
Que sai agora às onze horas
G#6      G7     Cm
Só amanhã de manhã
         Gm7(b5)     C7
Mas além disso mulher

            Fm
Tem outra coisa
      G#7
Minha mãe não dorme
      D7          G7
Enquanto eu não chegar
Fm   F#°   Cm/G   G#
Sou filho único
Am7(b5)      G/B      Cm    C7
Tenho minha casa pra olhar
Fm   F#°    Cm/G  G#
Sou filho único
Am7(b5)       G/B     Cm
Tenho minha casa pra olhar
                 G7
Aqui não posso ficar

C7  Fm   F#°            Cm    D#/A#       G#6
  Faz, faz, faz faz faz faz, faz carinho dumdum
               G7               Cm    C7
Faz carinho dumdum, faz carim dumdum
 Fm    F#°              Cm   D#/A#
Quais,quais,quais,quais,quais,quais
           G#6                 G7               Cm
Quaiscalingudum, Quaiscalingudum  Quaiscalingudum

----------------- Acordes -----------------
A#7 = X 1 3 1 3 1
Am7(b5) = 5 X 5 5 4 X
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
Cm/G = X X 5 5 4 3
Cm5+ = X 3 1 1 1 4
Cm6 = X 3 X 2 4 3
D# = X 6 5 3 4 3
D#/A# = X X 8 8 8 6
D7 = X X 0 2 1 2
F#° = 2 X 1 2 1 X
Fm = 1 3 3 1 1 1
G# = 4 3 1 1 1 4
G#6 = 4 X 3 5 4 X
G#7 = 4 6 4 5 4 4
G/B = X 2 0 0 3 3
G7 = 3 5 3 4 3 3
G7/D = X X 0 0 0 1
Gm7(b5) = 3 X 3 3 2 X
