Os Originais do Samba - A Volta do Ponteiro

C                C7         F
Se ela não quer, deixa pra lá
          Fm                    G7           C
Procure outro que eu sou muito moço pra chorar
C                C7         F
Se ela não quer, deixa pra lá
          Fm                    G7           C
Procure outro que eu sou muito moço pra chorar

C7         F             G7            C
Já fiz recanto, fiz encantos, fiz poesia
                     C7
Pra ver se ela percebia
                  Fm
Mas de nada adiantou
                                  Em
Bati cabeça, passei noites no sereno
                     F
E ela não fez por menos
                      G7
Mesmo assim me desprezou


C                C7         F
Se ela não quer, deixa pra lá
          Fm                    G7           C
Procure outro que eu sou muito moço pra chorar
C                C7         F
Se ela não quer, deixa pra lá
          Fm                    G7           C
Procure outro que eu sou muito moço pra chorar

C7        F            G7            C
Ela não sabe, que na volta do ponteiro
                             C7
Pra quem brinca o tempo inteiro
                    Fm
Chega a hora de sofrer
                                     Em
E há nesse mundo, mais mistério e magia
                F
Que toda filosofia
                     G7
Possa ao menos perceber

C                C7         F
Se ela não quer, deixa pra lá
          Fm                    G7           C
Procure outro que eu sou muito moço pra chorar
C                C7         F
Se ela não quer, deixa pra lá
          Fm                    G7           C
Procure outro que eu sou muito moço pra chorar

----------------- Acordes -----------------
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G7 = 3 5 3 4 3 3
