Os Originais do Samba - A subida do morro

Introdução: Dm  G7  3 vezes

     Dm                  G7
Quem é de mile ano, sabe bem quem é quem
   Dm                      G7
O samba de verdade não morreu disso eu sei
   Dm                        G7
Aqui em grande estilo, em memória eu cito
    Dm           G7              Dm
Mussum, Bidi, Rubão, Chiquinho, aí
   Dm                     G7
A fina flor do Miudinho está no céu
       Dm                    G7
Descansando em paz, após cumprir o seu papel
     Dm                   G7
Não vai passar batido, o samba é tradição
         Dm                     G7
Quem não é cabelo algum, então......preste atenção
   Dm                  G7
Bigode no pandeiro, Leley no tamborim
    Dm                       G7
Os verdadeiros bambambãs do morro tão aqui

    Dm                 G7
Originais do samba, canta pra gente
 Dm                G7
.....Na subida do morro é diferente
               G7         Dm
(Na subida do morro é diferente)
       G7       Dm  G7  Dm            G7          Dm
O movimento é geral..........no sobe desce, sobe gente
       G7         Dm              G7        Dm
O movimento é diferente, o cumprimento é diferente
G7  Eb            C#º            Dm
.........Alô como vai, como é que é     }
G7  Eb            C#º            Dm       bis
.........Alô como vai, como é que é     }
 G7 Dm                G7            Dm  G7
.......Como em toda jogada e jurisdição
 F               Eb                 Dm  G7  Dm
...Tem sempre o cara que quer ser o bom
        G7     Dm      C        Dm   G7
Mas nossa rapaziada é bem destacada
 F             Eb              Dm  G7  Dm
...Não dá confiança pra esse bobão
         G7       Dm              G7
Esse é o tipo atrasado.......por fora das grandes
   Dm              G7           Dm
Jogadas......desatualizado geral
G7 Dm         G7           Dm
......E ainda diz que é valente                 }
         G7        Dm        G7            Dm     bis
Que bate faz e acontece, no meio de tanta gente }
Dm  Eb                C#º                 Dm
.......Ele está antecipando o seu próprio fim
G7  Eb                C#º                 Dm  G7  Dm
.......Ele está antecipando o seu próprio fim
  G7               Dm   G7 Dm
Existe um ditado anti...go
   G7         Dm     G7  Dm
Do tempo do cativeiro
        C7       Dm
É um ditado explicado
        G7                Dm
Que finaliza com esses dizeres
Eb          C#º       Dm
...Valente morre mais cedo               }
Eb             C#º                Dm  G7   3x
...Valente antecipa o seu próprio fim    }
              G7              Eb      C#º  Dm
Originais do Samba dois mil, mil grau
            Dm          Eb  C#º
Se liga na rapaziada nova
   Dm               G7
Bigode no pandeiro......original
  Eb              C#º
Skooby no cavaco.....mil grau
 Dm                   Dm
Lindo no tantã......repica, abala
   Eb          C#º                     Dm
Junnho, banjo......Sócrates na guitarra
                 G7
No tamborim meu mano Marvin Say
   Eb                  C#º
O samba de raiz não morreu, disso eu sei
 Dm                   Dm
Originais do Samba, valeu pelo convite
  Eb            C#º              Dm
Apelidado Xis..........na humilde..

----------------- Acordes -----------------
C = X 3 2 0 1 0
C#º = X 4 5 3 5 3
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
G7 = 3 5 3 4 3 3
