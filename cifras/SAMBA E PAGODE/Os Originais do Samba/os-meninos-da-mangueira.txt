Os Originais do Samba - Os Meninos da Mangueira

C
Um menino da mangueira,recebeu pelo natal
       Am          Dm   A7            Dm
Um pandeiro e uma cuica   ,que lhe deu papai noel
                       G7             C
Um mulato sarara,primo-irmão de Dona Zica

E o menino da mangueira,foi correndo organizar
    C7       F      Am               Dm    C
Uma linda bateria,carnaval ja vem chegando
       Dm      Am             Dm      G7    C
E tem gente batucando,sao os meninos da mangueira
         G7         C                G7        C  C7
Carlos Cachaca,o menestrel mestre Cartola,o bacharel
     F        E7      Am                    D7
Seu delegado um dancarino,faz coisas que aprendeu
       G7   C7
Com Marcelino

--------
          F                 Ebdim            C
E a velha guarda se une aos meninos la na passarela

       A7           Dm       G7         C   C7
Abram alas,que vem ela,a Mangueira toda bela

 ( BIS )
---------

   G7              C             G7            C  C7
O padeirinho,cade Xango.o preto rico,chama o Sinho
        F    E     Am              D7
E Dona Neuma....maravilhosa, é a primeira mulher
         G7  C7
Da verde-rosa

----------
    F                      Fm                   C
E onde e que se junta o passado,o futuro e o presente
        A7        Dm           G7             C  C7
Onde o samba e permanente,na Mangueira minha gente

 ( BIS )
-----------

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
Ebdim = X X 1 2 1 2
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G7 = 3 5 3 4 3 3
