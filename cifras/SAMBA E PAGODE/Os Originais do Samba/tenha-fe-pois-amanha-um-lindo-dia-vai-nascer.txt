Os Originais do Samba - Tenha Fé Pois Amanhã Um Lindo Dia Vai Nascer

(intro 16x) Gm

            Gm
Nunca tenha medo do seu inimigo
Quando não é você que começa a brigar,
Também nunca ande de cabeça baixa e bem danado...
         Cm              D7/9     Gm
Pois nem tudo que cai do céu é sagrado.  (2x)

               Gm
Mané que enche barco deixa a praia descoberta
Uma onda vai, outra onda vem,
          Cm               D7/9          Gm
Não fique triste que seu amor pode ficar bem. (2x)

      Cm               Bb              Am         Gm
Aí então você vai me dizer se tenho razão, sim ou não
       Cm               Bb                 Am            Gm
Pois é por experiência própria que eu perdoei o meu coração.

           Gm
Infeliz no jogo, feliz no amor,

Saúde e felicidade, pra dar e vender
Se a reza é forte, você vai ver
        Cm          D7/9       Gm
Que amanhã um lindo dia vai nascer (2x)

      Cm               Bb             Am          Gm
Aí então você vai me dizer se tenho razão, sim ou não
       Cm               Bb               Am             Gm
Pois é por experiência própria que eu perdoei o meu coração.

           Gm
Infeliz no jogo, feliz no amor,
Saúde e felicidade, pra dar e vender
Se a reza é forte, você vai ver
        Cm          D7/9       Gm
Que amanhã um lindo dia vai nascer (2x)

Pa PA PA PA PA, PA PA PA PA...

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
D7/9 = X 5 4 5 5 X
Gm = 3 5 5 3 3 3
