Os Originais do Samba - Aniversário do Tarzan

D       A7              D
Quando eu entrei na mata,
     A7         D
Bebi agua na cascata,
B7              Em      B7      Em
Era uma linda manhã, que manhã,
A7                     D   B7
Os macacos pulavam em festa,
Em      A7           Am6/F# B7
Era aniversário do Tarzan.
Em      A7           D
Era aniversário do Tarzan.
  A7                      D
O Príncipe Lothar e o Mandrake,
     D7                    G
Chegaram com o Fantasma Voador,
     A7       D  B7
Tudo era alegria.
   E7
O Zorro chegou beijando
              A7
O sargento Garcia

  G Em  Fm  F#m  Gm                            F#m
A Mônica ....... com aquele jeitinho que só ela tem,
B7                             Em
Bateu no Cascão e no Anjinho também,
A7                            D         A7
E fez o Cebolinha cantar parabéns, parabéns
     D          A7                D
"Palabéns pla você, nesta data quelida" (voz de Cebolinha)
"Parabéns pra você, nesta data querida" (voz do Grupo)

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
