Clara Nunes - Amor Quando É Amor

[Intro] Am  Em  F  B7  Em

 Em                               Am
Amor quando é amor é todo bem da vida
           B7
É sentir prazer é também sofrer
                 Em
O bem de querer bem
                  D
Eu te amo assim querido
              C     Am
E assim és recebido
               B7    E7
Em meu pobre coração
 Am                   Em
Olha amor não quero acabar assim
 F#7                       B7 E7
Olha amor você nasceu pra mim
 Am
Fica amor
           Em
Escuta o lamento meu

 F        B7               Em
Esse amor    ainda é teu e meu

( Em  Am  B7  Em )

 Am
Fica amor
           Em
Escuta o lamento meu
 F        B7               Em
Esse amor    ainda é teu e meu

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F#7 = 2 4 2 3 2 2
