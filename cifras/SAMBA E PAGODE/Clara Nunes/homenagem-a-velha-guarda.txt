Clara Nunes - Homenagem à Velha Guarda

Intro: A G F E7 E° E7

A    A5+       Bm   E7
Um chorinho me traz
              A
Muitas recordações
Dm     G  C      C5+  Dm7
Quando o som dos regionais
G            C   Gm7
Invadia os salões
  C7 F Dm              Em7 Am7
E era sempre um clima de festas
             Dm7 G7
Se fazia serestas
                E4 E7
Parando nos portões
                    A
Quando havia os balcões
Db°           Bm7
Sob a luz da Lua
    E7        A   A5+    Bm  E7
E a chama dos lampiões à gás

               A
Clareando os serões
Dm  G   C    C5+     Dm  G7
Sempre com gentis casais
               C   Gm7
Como os anfitriões
C7 F Dm                Em7 Am7
E era uma gente tão honesta
               Dm7   G7
Em casinhas modestas
                  E4  E7
Com seus caramanchões
                A  G#m5-/7
Reunindo os chorões
F#m                Bm
Era uma flauta de prata
            C#7        D7        C#7  F#7
A chorar serenatas, modinhas, canções
                                      F#7 Bm7
Pandeiro, um cavaquinho e dois violões
                                      F#m7
Um bandolim bonito e um violão sete cordas
         G#7     C#7     F#m7 G#m5-/7 C#7
Fazendo desenhos nos bordões
F#m7          Bm7
Um clarinete suave
                  C#7         D7          C#7 F#7
E um trombone no grave a arrastar os corações
                             F#7 Bm
Piano era o do tempo do Odeon
De vez em quando um sax-tenor
       F#m7              G#7        C#7 F#m7 B7 E4 E7
E a abertura do fole imortal do acordeom
 A      A5+    Bm7  E7
Mas já são pra nós
            A
Meras evocações
Dm  G7 C  C5+     Dm   G
Tudo já ficou pra trás
                 C   Gm7
Passou nos carrilhões
C7 F Dm               Em7  Am7
Quase ninguém se manifesta
                  Dm   G7
Pouca coisa hoje resta
                      E4 E7
Lembrando os tempos bons
            A   A Bm5-/7 E7 Am Bm5-/7 E7 Am
Dessas reuniões

----------------- Acordes -----------------
A = X 0 2 2 2 0
A5+ = X 0 3 2 2 0
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
Bm5-/7 = X 2 3 2 3 X
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
C#7 = X 4 3 4 2 X
C5+ = X 3 2 1 1 X
C7 = X 3 2 3 1 X
D7 = X X 0 2 1 2
Db° = X 4 5 3 5 3
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
E4 = 0 2 2 2 0 0
E7 = 0 2 2 1 3 0
Em7 = 0 2 2 0 3 0
E° = X X 2 3 2 3
F = 1 3 3 2 1 1
F#7 = 2 4 2 3 2 2
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
G = 3 2 0 0 0 3
G#7 = 4 6 4 5 4 4
G#m5-/7 = 4 X 4 4 3 X
G7 = 3 5 3 4 3 3
Gm7 = 3 X 3 3 3 X
