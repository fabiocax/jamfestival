Clara Nunes - Coração Leviano

Intro: Bm7 E7 A F#7 B7/9 E7 A E7

A          Cº           A
Trama em segredo seus planos
            F#7   Bm7  F#7
Parte sem dizer adeus
      Bm                 E7
Nem lembra dos meus desenganos
 B7/9      E7
Fere quem tudo perdeu
Bm7    E7      A
Ah, coração leviano
     F#7        B7/9 E7 A  F#7
Não sabe o que fez do meu
Bm     E       A
Esse pobre navegante
F#            Bm    D7
Meu coração amante
                C#7
Enfrentou a tempestade
  F#m       E7           A7+
No mar da paixão e da loucura

F#7                B7/9
Fruto da minha aventura
                 E7
Em busca da felicidade
Bm      E7        A    D7
Ah, coração teu engano
                C#7
Foi esperar por um bem
  Bm7     E7     A
De um coração leviano
      F#7  B7/9 E7   A
Que nunca será de ninguém..

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7+ = X 0 2 1 2 0
B7/9 = X 2 1 2 2 X
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
C#7 = X 4 3 4 2 X
Cº = X 3 4 2 4 2
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F# = 2 4 4 3 2 2
F#7 = 2 4 2 3 2 2
F#m = 2 4 4 2 2 2
