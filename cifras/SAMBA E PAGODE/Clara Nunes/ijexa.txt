Clara Nunes - Ijexa

C#º  Cm7 / Bm7 Bbº / Am7 D7 / G

           G           D7
Filhos de Gandhi,badauê
     C                     G
Ylê ayiê,malê debalê,otum obá
              Em
Tem um mistério
                 Am7
Que bate no coração
                 D7
Força de uma canção
                       G
Que tem o dom de encantar
  D/F#
Seu brilho parece
               G
Um sol derramado
              D7
Um céu prateado
                 G
Um mar de estrelas

B7
Revela a leveza
                 Em
De um povo sofrido
              E7
De rara beleza
                A7
Que vive cantando
                D7
Profunda grandeza
 D/F#
A sua riqueza
                G
Vem lá do passado
               D7
De lá do congado
               G
Eu tenho certeza
               C
Filhas de Gandhi
            G
Ê povo grande
      D7               G
Ojuladê,katendê,babá obá
              C
Netos de Gandhi
            G
Povo de Zambi
            D7
Traz pra você
                  G
Um novo som: Ijexá


MODULAR MEIO TOM NA REPETIÇÃO

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
Bbº = X 1 2 0 2 0
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
C#º = X 4 5 3 5 3
Cm7 = X 3 5 3 4 3
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
