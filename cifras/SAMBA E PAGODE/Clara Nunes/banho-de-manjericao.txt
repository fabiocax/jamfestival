Clara Nunes - Banho de Manjericão

 C          Dm   G7         C
Eu vou me banhar    de manjericão
        Dm                G7                 C
Vou sacudir a poeira do corpo batendo com a mão
        Gm   C7               F
E vou voltar    lá pro meu congado
Em                 Dm
   Pra pedir pro santo
              G7
Pra rezar quebranto
              C
Cortar mau-olhado
           Dm                    G7                C     Am
E eu vou bater na madeira três vezes com o dedo cruzado
       Dm                 G7          Em   A7
Vou pendurar uma figa no aço do meu cordão
            Gm                    C7
Em casa um galho de arruda é que corta
           F                 Em
Um copo d'água no canto da porta
       Dm               G7        C
Vela acesa, e uma pimenteira no portão.


 C          Dm   G7         C
Eu vou me banhar    de manjericão
        Dm                G7                 C
Vou sacudir a poeira do corpo batendo com a mão
        Gm   C7               F
E vou voltar    lá pro meu congado
Em                 Dm
   Pra pedir pro santo
              G7
Pra rezar quebranto
              C
Cortar mau-olhado

             Dm               G7              C      Am
É com vovó Maria que tem simpatia pra corpo fechado
              Dm                 G7                     Em    A7
É com pai Benedito que benze os aflitos com um toque de mão
        Gm              C7
E pai Antônio cura desengano
          F             Em
E tem a reza de São Cipriano
           Dm                  G7           C
E têm as ervas que abrem os caminhos pro cristão.


 C          Dm   G7         C
Eu vou me banhar    de manjericão
        Dm                G7                 C
Vou sacudir a poeira do corpo batendo com a mão
        Gm   C7               F
E vou voltar    lá pro meu congado
Em                 Dm
   Pra pedir pro santo
              G7
Pra rezar quebranto
              C
Cortar mau-olhado

 C          Dm   G7         C
Eu vou me banhar    de manjericão
        Dm                G7                 C
Vou sacudir a poeira do corpo batendo com a mão
        Gm   C7               F
E vou voltar    lá pro meu congado
Em                 Dm
   Pra pedir pro santo
              G7
Pra rezar quebranto
              C
Cortar mau-olhado

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
