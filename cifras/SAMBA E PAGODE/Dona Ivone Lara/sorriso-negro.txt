Dona Ivone Lara - Sorriso Negro

Introdução: F

 Gm7        C7          F   Dm
Negro é a raiz da liberdade,
 Gm7        C7          F   C7 (Pausa)
Negro é a raiz da liberdade


Refrão:
(C7)             F    C7          F    C7
     Um sorriso negro, um abraço negro
 Am5-/7   D7  Gm7
Traz....felicidade
 D7         Gm7     D7         Gm7  D7
Negro sem emprego, fica sem sossego
  Gm        C7          F    C7
Negro é a raiz da liberdade
            F   C7           F    C7
Um sorriso negro, um abraço negro
 Am5-/7   D7  Gm7
Traz....felicidade

 D7         Gm7     D7         Gm7  D7
Negro sem emprego, fica sem sossego
  Gm        C7          F
Negro é a raiz da liberdade

Verso:
     D7               Gm7
Negro é uma cor de respeito
  C7          F
Negro é inspiração
                    Cm
Negro é silêncio, é luto
       F7        Bb7+
negro é...a solidão
 Bb6                 Bbm6
Negro que já foi escravo
   C7                F
Negro é a voz da verdade
  D7             G7/13   G5+/7
Negro é destino é amor
  C7              F                  C7
Negro também é saudade.. um sorriso negro

----------------- Acordes -----------------
Am5-/7 = 5 X 5 5 4 X
Bb6 = X 1 3 0 3 X
Bb7+ = X 1 3 2 3 1
Bbm6 = 6 X 5 6 6 X
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
G5+/7 = 3 X 3 4 4 3
G7/13 = 3 X 3 4 5 X
Gm = 3 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
