Dona Ivone Lara - Nas Escritas da Vida

F F7          Bb Eb7/9 Am  D7           Gm    C7           Cm F7
    Lalaialalaiá, lá,  iá, Ê larauê larauêra, Ê Laraiá laraiá
              Bb Eb7/9 Am D7            Gm    C7            F C7
    Lalaialalaiá, lá,  iá, Ê larauê larauêra, Ê Laraiá laraiá

(35 34 33 35 43)


F6/9             A7
Vai, usa a tua razão
                Dm D7
Cega o teu coração
               Gm7
E procura entender
             C7
Me feri a valer
                F         D7
É tão triste viver sem sorrir
                G7
À procura de um ninho
                  C7        C7/13b
Pra amenizar as agruras do caminho

F                    A7
Vai, eu não vou resistir
                Dm D7
Eu não vou insistir
               Gm7
Não vou me exceder
               C7
E não vou debater
              F      D7
Só preciso dizer a você
                G7    C7                   F C7
Nas escritas da vida, eu não nasci pra sofrer

F
 Não me acostumei
   A/G
 A não ser mais ouvido, eu juro
Dm             Bb7
 Não me acostumei
   A7                        Bb
 A não ter seu carinho no depois
      C7          F
 Não posso acreditar
D7               G7
 Se a paixão acabou
     C7
 É melhor terminar

F
 Não me acostumei
  A/G
 A não ouvir bom dia, eu juro
Dm            Bb7
 Não me acostumei
   A7                 Bb
 A tanta covardia no amor
   C7          F
 É justo conjugar
Dm               G7  C7  F
 E a primeira pessoa, amar

----------------- Acordes -----------------
A/G = 3 X 2 2 2 X
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bb7 = X 1 3 1 3 1
C7 = X 3 2 3 1 X
C7/13b = X 3 X 3 5 4
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Eb7/9 = X 6 5 6 6 X
F = 1 3 3 2 1 1
F6/9 = 1 0 0 0 X X
F7 = 1 3 1 2 1 1
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
