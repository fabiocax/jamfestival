Dona Ivone Lara - Força da Imaginação

Ab           Bbm    Eb7/9    Ab           Bbm
Força da imaginação, vai lá além dos pés e do chão
Eb7/9   Ab                       Bbm  Eb7/9
Chega lá, o que a mão ainda não toca coração um dia alcança
Ab             Bbm   Eb7/9  Ab            Bbm   Eb7/9
Força da imaginação vai lá  força da imaginação vai lá
Ab                              Bbm        Eb7/9
Quando um poeta compõe mais um samba ele funda outra cidade
Ab              F7            Bbm                 D/C  Eb7/9
Lamentando sua dor ele faz felicidade força da imaginação
              Ab                    Bbm    Eb7/9
Na forma da melodia não escurece a razão, ilumina o dia-a-dia
Ab             Bbm    Eb7/9   Ab               Bbm   Eb7/9    Ab
Força da imaginação, vai lá  além dos pés e do chão chega lá, o que a mão
          Bbm   Eb7/9            Ab                 Bbm    Eb7/9
Ainda não toca coração um dia alcança força da imaginação vai lá
Ab               Bbm Eb7/9   Ab                             Bbm       Eb7/9
Força da imaginação vai lá , quando uma escola traz de lá do morro o que

                Ab                F7                   Bbm             D/C  Eb7/9
Asfalto nem é sonho atravessa o coração um entusiasmo medonho força da imaginação

                    Ab                         Bbm          Eb7/9
Se espalhando pela avenida não pra alegrar a fraqueza mas pra dar mais vida à vida

(3x)
Força da imaginação, além dos pés e do chão chega lá, o que a mão ainda não toca
Coração um dia alcança força da imaginação vai lá força da imaginação vai lá

----------------- Acordes -----------------
