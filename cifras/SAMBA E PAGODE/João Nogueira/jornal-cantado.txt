João Nogueira - Jornal Cantado

 Gm7          C9             Dm7/A  Ab7(11)
O amor anda escasso de ternura
G7            A13-    Dm7     Ab7(11)
A saúde pior não há de estar
Gm7          C9      Dm7/A  Ab7(11)
Sua mesa tá rica de fartura
G7         A13-            Dm7     C7
A manchete do dia diz que está
F6                              G7
Dois presuntos pintaram em Cascadura
C7                        F6
Desovaram mais doze no Tanguá
F6                         G7
Quatro doces na face da candura
C7            F6       A13-
Oito balas na boca na babá
Dm7     G7       Dm7     Ab7(11)
Escorria melado a tanajura
G7        A13-          Dm7
Pela baba da moça do Tauá
G7      C7            F6
Eh, malandro, sai prá lá

                        G7
Quem falou que a coisa é pura
         C7        F6
Não sabia o que falar
F6                     G7
Cuidado que a cana é dura
     A13-      Dm7      Ab7(11)
E a sujeira vai pintar
Gm7     C9          Dm7/A     Ab7(11)
Inventaram a borracha da cultura
G7       A13-            Dm7    Ab7(11)
E apagaram a memória nacional
Gm7        C9              Dm7/A      Ab7(11)
Deram um rabo-de-arraia na estrutura
G7       A13-            Dm7     C7
Dança rock no pais do carnaval
F6                        G7
Olha só o que deu dessa mistura
C7                       F6     C7
As mulheres de terno de tergal
F6                          G7
Os meninos na de corte e costura
C7            F6      A13-
As meninas jogando futebol
Dm7            G7             Dm7     Ab7(11)
Ontem foi a explosão dessa loucura
G7              A13-            Dm7
Um rapaz deu a luz no Vidigal
G7           C7        F6
Eh, malandro, sai prá lá
                         G7
Quem falou que a coisa é pura
     C7             F6
Não sabia o que falar
F6                    G7
Cuidado que a cana é dura
    A13-      Dm7      Ab7(11)
E a sujeira vai pintar
Gm7        C9              Dm7/A  Ab7(11)
Extraíram a antiga dentadura
G7         A13-            Dm7     Ab7(11)
É a mais nova noticia do jornal
Gm7         C9          Dm7/A   Ab7(11)
Que comia voraz a rapadura
G7          A13-           Dm7
E meu povo assistia da geral

----------------- Acordes -----------------
