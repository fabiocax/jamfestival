João Nogueira - Do Jeito Que o Rei Mandou

    Bm
Sorria!
F#m7        B7(b9)            Em7(9)  Em/D  C#m7(b5)
Meu bloco vem, vem descendo a cidade
     F#7                 Bm    Bm/A  G7
Vai haver Carnaval de verdade
                  F#7
O samba não se acabou

    Bm
Sorria!
F#m7       B7(b9)              Em7(9)  Em/D  C#m7(b5)
Que o samba mata a tristeza da gente
       F#7              Bm      C#m7(b5)
Quero ver o meu povo contente
            F#7       Bm
Do jeito que o rei mandou

                 Bm
Bate lata, bate surdo
             Em
Agogô e tamborim


Bate fundo no meu peito
                    Bm
Um amor que não tem fim

E pra não cair da escada
      F#m7(b5) B7    Em
Bate o prego, meu senhor
       F#7          Bm   Bm/A
Bate o pé, mas bate tudo
   C#m7(11)    F#7     Bm   F#7
Do jeito que o rei mandou

    Bm
Sorria!
F#m7        B7(b9)            Em7(9)  Em/D  C#m7(b5)
Meu bloco vem bem, descendo a cidade
     F#7                 Bm    Bm/A  G7
Vai haver Carnaval de verdade
                  F#7
O samba não se acabou

    Bm
Sorria!
F#m7       B7(b9)              Em7(9)  Em/D  C#m7(b5)
Que o samba mata a tristeza da gente
       F#7              Bm      C#m7(b5)
Quero ver o meu povo contente
            F#7       Bm
Do jeito que o rei mandou
    Bm/A      C#m7(11) F#7  B7(#9)
Do jeito que o rei   mandou
               E7(9)   Bm
Do jeito que o rei mandou

----------------- Acordes -----------------
B7 = X 2 1 2 0 2
B7(#9) = X 2 1 2 3 X
B7(b9) = X 2 1 2 1 X
Bm = X 2 4 4 3 2
Bm/A = X 0 4 4 3 X
C#m7(11) = X 4 4 4 5 4
C#m7(b5) = X 4 5 4 5 X
E7(9) = X X 2 1 3 2
Em = 0 2 2 0 0 0
Em/D = X X 0 4 5 3
Em7(9) = X 7 5 7 7 X
F#7 = 2 4 2 3 2 2
F#m7 = 2 X 2 2 2 X
F#m7(b5) = 2 X 2 2 1 X
G7 = 3 5 3 4 3 3
