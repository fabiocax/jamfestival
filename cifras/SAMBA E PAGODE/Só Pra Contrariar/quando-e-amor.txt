Só Pra Contrariar - Quando é Amor

[Intro] D  F#m7  G7+  G/A
        D  F#m7  G7+  G/A

E|--------------------------------------7-7---------------------------|
B|--------7--8/10----------7--8/10---------10-10-8-8-7-7--------------|
G|----7-9--9-----------6-9--9----------------------------9-7----------|
D|--7-----------------7---------------------------------------7/9-7---|
A|--------------------------------------------------------------------|
E|--------------------------------------------------------------------|

E|--------------------------------------7-7---------------------------|
B|--------7--8/10----------7--8/10---------10-10-8-8-7-7--------------|
G|----7-9--9-----------6-9--9----------------------------9-7--7/9-7---|
D|--7-----------------7-----------------------------------------------|
A|--------------------------------------------------------------------|
E|--------------------------------------------------------------------|

  D
A gente sabe o que é amor
                  F#7
Quando um simples beijo

Queima o coração
      G7+
Um sorriso, um olhar
   Em7                 A7/4 A7/C#
Um toque, um aperto de mão

  D
A gente sente, é pra valer
              F#7
O corpo treme todo
                 F#7/4 F#7
A voz não quer sair
    G7+
Não dá pra disfarçar
   Em7                    G/A C#m7/5- F#7
Os olhos não conseguem mentir

Bm7
A gente deita pra dormir
               F#m7
Mas o sono não vem
Não quer saber de nada,
                 G7+
Não quer ver ninguém
             G/A
O tempo é inimigo
          D7+ C#m7/5- F#7
Corre devagar

   Bm7
No rádio, uma canção tão bela
                 A
Uma história de amor
                                 G7+
Eu quero estar contigo seja como for
              G/A                D
Sem essa de juízo não dá pra esperar

[Refrão]

          F#m7
Vem, meu amor
          G7+
Não faz assim
              G/A
Você é tudo aquilo
               D
Que sonhei pra mim
          F#m7
Vem, meu amor
            G7+
Não diz que não
            G/A
Você já é a dona
            D
Do meu coração

[Solo] D  F#m7  G7+  G/A

E|--------------------------------------7-7---------------------------|
B|--------7--8/10----------7--8/10---------10-10-8-8-7-7--------------|
G|----7-9--9-----------6-9--9----------------------------9-7----------|
D|--7-----------------7---------------------------------------7/9-7---|
A|--------------------------------------------------------------------|
E|--------------------------------------------------------------------|

E|--------------------------------------7-7---------------------------|
B|--------7--8/10----------7--8/10---------10-10-8-8-7-7--------------|
G|----7-9--9-----------6-9--9----------------------------9-7--7/9-7---|
D|--7-----------------7-----------------------------------------------|
A|--------------------------------------------------------------------|
E|--------------------------------------------------------------------|

  D
A gente sente, é pra valer
              F#7
O corpo treme todo
                 F#7/4 F#7
A voz não quer sair
    G7+
Não dá pra disfarçar
   Em7                    G/A C#m7/5- F#7
Os olhos não conseguem mentir

Bm7
A gente deita pra dormir
               F#m7
Mas o sono não vem
Não quer saber de nada
                 G7+
Não quer ver ninguém
             G/A
O tempo é inimigo
          D7+ C#m7/5- F#7
Corre devagar

   Bm7
No rádio, uma canção tão bela
                 A
Uma história de amor
                                 G7+
Eu quero estar contigo seja como for
              G/A                D
Sem essa de juízo não dá pra esperar

----------------- Acordes -----------------
A/C# = X 4 X 2 5 5
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
Em7 = 0 2 2 0 3 0
F#m7 = 2 X 2 2 2 X
G/A = 5 X 5 4 3 X
G7+ = 3 X 4 4 3 X
