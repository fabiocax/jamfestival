Só Pra Contrariar - É Gostoso te Amar

Introdução:  F C Bb F C7
Solo 17- 1_0- 1_2- 1_0- 1_0- 15- 21- 21- 23 -21- 33-43- 13- 15- 13- 15  2X

F
Ai,amor
                C
Esse desejo maroto
                 Bb  A7
Tá me deixando louco ...
              Dm Dbm       Cm F7
Querendo te amar ....Lalaiá... ...
  Bb      C7              Am  D7
Nosso amor ... ficou perigoso ...
                 G7/13 G5+/7
Mas ele é tão gostoso  ....
                     Gm
Que eu não consigo parar
     C7
Ai,amor!
 F
Ai, amor ...

             C
Seu fogo ardente
             Bb  A7
Me deixou doente ...
              Dm Dbm        Cm F7
Querendo te amar,  ....Lalaiá ....
 Bb     C7                   Am  D7
Vem amar ... me dê mais um pouco ...
                        G7  C7
Deixa eu sentir o seu corpo ...
                F
Deixa eu te molhar
             C7
Porque é gostoso!
 F     Dm      G7
É gostoso te amar
 Gm    C7                  F Dm
É gostoso e não dá pra parar ....
       Gm  C7
É gostoso! ...
F      Dm      G7
É gostoso te amar ...
 Gm     C7                F
É gostoso e não dá pra parar
      C7
Ai, amor!

INTRODUÇÃO

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
Dbm = X 4 6 6 5 4
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
G5+/7 = 3 X 3 4 4 3
G7 = 3 5 3 4 3 3
G7/13 = 3 X 3 4 5 X
Gm = 3 5 5 3 3 3
