Só Pra Contrariar - Um amor puro

de: Djavan
Gravação: Só pra contrariar


D7M                                C#7(4)  C#7
    O que há dentro do meu coração
Bm7                          A7M
    Eu tenho guardado pra te dar
D7M
    E todas as horas que o tempo
G#m7(b5)             C#7(b9)
Tem      pra me conceder
    F#m7        B7(9)
São tuas  até morrer

D7M                          C#7(4)  C#7
    E a tua história, eu não sei
Bm7                          A7M
    Mas me diga só o que for bom
D7M
    Um amor tão puro que ainda nem

G#m7(b5)             C#7(b9)
Sabe     a força que tem

  F#m7              B7(9)
É teu  e de mais ninguém


            D7M
Te adoro em tudo, tudo, tudo
               Bm7
Quero mais que tudo, tudo, tudo
            E7(4/9)
Te amar sem        limites
                    A7M
Viver uma grande história

            D7M
Te adoro em tudo, tudo, tudo
               Bm7
Quero mais que tudo, tudo, tudo
            E7(4/9)
Te amar sem        limites
                    A7M   50 64
Viver uma grande história


F#m7
     Aqui ou noutro lugar
C#m7
     Que pode ser feio ou bonito
Bm7
    Se nós estivermos juntos
D7M           E7(4/9)  F°
    Haverá um céu     azul

        D7M
Um amor puro
    Bm7                  E7(4/9)
Não sabe a força que tem
         D7M
Meu amor eu  juro
    Bm7
Ser teu e de mais ninguém
        D7M
Um amor puro...


   D7M       C#7(4)      C#7        Bm7       A7M      G#m7(b5)

 ||||||  4ª |<----  4ª |<----     ||||||     |||1||     ||||||
 |||<--     ||||||     ||||||     |<----     ||2|3|     ||||||
 ||||||     ||3|||     ||3|4|     ||||2|     ||||||     ||||1|
 ||3|||     ||||4|     ||||||     ||3|||     ||||||     2|34||
 |4||||     ||||||     ||||||     ||||||     ||||||     ||||||
  O ooo      Oooo       Oooo       O ooo      O ooo     O ooo


 C#7(b9)     F#m7       B7(9)     E7(4/9)     C#m7        F°

 ||||||     ||||||     ||1|||     ||||||  4ª |<----     ||||||
 ||||||     1|234|     |2|34|     |<----     ||||2|     ||||||
 ||1|2|     ||||||     ||||||     ||||2|     ||3|||     ||1|2|
 |3|4||     ||||||     ||||||     ||||||     ||||||     |||3|4
 ||||||     ||||||     ||||||     ||||||     ||||||     ||||||
  Oooo      O ooo       Oooo      O  ooo      O ooo       Oooo

----------------- Acordes -----------------
A7M = X 0 2 1 2 0
B7(9) = X 2 1 2 2 X
Bm7 = X 2 4 2 3 2
C#7 = X 4 3 4 2 X
C#7(4) = X 4 6 4 7 4
C#7(b9) = X 4 3 4 3 X
C#m7 = X 4 6 4 5 4
D7M = X X 0 2 2 2
E7(4/9) = X X 2 2 3 2
F#m7 = 2 X 2 2 2 X
F° = X X 3 4 3 4
G#m7(b5) = 4 X 4 4 3 X
