Só Pra Contrariar - De Corpo Sem Alma

[Intro]  Eb  F  Gm7  Eb  D7  Em  Am

   Em                       B7
Às vezes a solidão é bem melhor que se aventurar
  Am              B7                Em        Am
O que se faz sem amor nem paixão só pode machucar
        Em                                B
Se envolver de corpo, sem alma, naquele momento satisfazer
     Am                      B   B7      Em            D
Os desejos que não brotam do coração e então se arrepender

   E7    Am                  D7           G7+
Porque depois que a transa rolar, vem o vazio
            C7+               Am
E me faz lembrar que o que eu fiz
            B7                C#m  D
Quando era amor eu era mais feliz
    E7      Am7            D7
E assim não sei onde vou parar
         G7+             C7+
Meu sentimento pode congelar

        Am                B7                 E  A7+  Bsus4
Até que pinte um grande amor que possa me mudar

      E               B     C#m7
Tô querendo novamente me encontrar
      C#m                E7  A7+
Tô querendo novamente me apaixonar
     A7+                  B            E       Bsus4
Sem amor eu sei que nunca vou poder chegar a lugar nenhum
     E                       B
A verdade é que eu nao quero viver de ilusao
      C#m7                 E7
Eu preciso, eu quero vida, nova emoção
     A7+                B            E
Sentimento e tempestade faz o Sol brilhar
     Bsus4        E
Entender o que é amar

[Solo]  C#m  G#m  Am  D7  G7+  C7+  Am  B7

----------------- Acordes -----------------
A7+ = X 0 2 1 2 0
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
Bsus4 = X 2 4 4 5 2
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
C7+ = X 3 2 0 0 X
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
Eb = X 6 5 3 4 3
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G#m = 4 6 6 4 4 4
G7+ = 3 X 4 4 3 X
Gm7 = 3 X 3 3 3 X
