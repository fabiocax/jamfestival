Só Pra Contrariar - Sai da Minha Aba

(intro 2x) G G7 C Cm

            G7 C Cm G
(vocalização) Laiá
   G7 C Cm
   Laiá...

(1ª parte - 2x)

G                G7                 C
Sai pra lá, se manca, vê se me esquece
             D7               G
Não aguento mais já tô com stress
         C                G
Se dou a mão, quer logo o pé
 D7          G
Isso me aborrece
            C                  G
Sai pra lá bicão, sai pra lá mané
 D7           G
Vê se desaparece


(2ª parte)
                     G7
É toda a hora meu cumpadre
               C
Quebro o galho aí
                    D7
É um qualquer emprestado
               G
Sempre a me pedir
                    G7
Se tô bebendo uma cerveja
               C
Bebe no meu copo
               D7
Se acendo um cigarro
                G
Quer fumar pra mim
              G7
Chego no pagode
              C
Quer entrar comigo
               Cm
Se ganho uma garota
              G
Tenta me atrasar
            E7
Fica me marcando
              Am
Pra não ter perigo
                 D7
De perder minha carona
               G
Quando eu me mandar
                           F# - G
Da minha aba, da minha aba
Da minha aba...

 G7
Sai da minha aba
        C
Sai pra lá
     D#                    G
Sem essa de não poder me ver
 G7                                (refrão)
Sai da minha aba
         C
Sai pra lá
 D7              G
Não aturo mais você.

(intro)
          G7  C  Cm  G
(vocalização)   Laiá.....
G7 C  Cm
  Laiá....

(repete 1ª parte 1x)
(repete 2ª parte)

G7   C
La...iá.....
D7               G
Não aturo mais você
G7    C
La... iá....
D7                      G
Sem essa de não poder me ver
G7    C
La... iá....
D7               G
Não aturo mais você
G7    C
La... iá....
D7               G
Não aturo mais você
             D7   G
Sai da minha a....ba!

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D# = X 6 5 3 4 3
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
