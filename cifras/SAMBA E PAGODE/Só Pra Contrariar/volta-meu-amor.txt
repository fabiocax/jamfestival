Só Pra Contrariar - Volta Meu Amor

Intro: G D Am D G D7

        G
Eu não quero mais viver assim
      D                                   Dm   G  Dm G
Sem você a minha vida está tão ruim, está vazia
        Dm                 G
Desde o dia em que você partiu
      Dm                  G          Dm  G  Gm A7
Minha paz, minha ilusão sumiu, estou triste
      Dm                 G
Eu te peço com toda a paixão
      Dm            G
Tenha dó do meu coração
         Dm              G               Gm     A7
Que hoje sofre calado, sozinho, querendo perdão
      Dm                 G
Eu te peço com toda a paixão
      Dm            G
Tenha dó do meu coração
         Dm              G               Am     D7
Que hoje sofre calado, sozinho, querendo perdão

      G
Então volta, meu amor
   D
Me perdoa, por favor
  Am                 D
Todo o mal que eu te fiz
        G               D7
Só você pode me fazer feliz
      G
Então volta, meu amor
   D
Me perdoa, por favor
   Am           D
Eu não vivo sem você
     G                  D
Me devolva a razão de viver

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
