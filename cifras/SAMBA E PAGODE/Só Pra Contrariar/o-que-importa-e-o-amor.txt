Só Pra Contrariar - O Que Importa É O Amor

                 Bb                   Gm
Eu quero te aprender na escola do seu coração
               Cm                F7
Saber sentir viver, aluno da sua emoção
              Bb                Gm
Preciso desfrutar do seu melhor prazer
                Cm                 F7
Seu brilho clarear o que eu não posso ver

              Gm               Dm
Nós dois numa paixão e um sentimento surgir
        Fm      Bb        Eb
Pra transar um amor de verdade
        Gm                   Dm
Sei que vou me envolver pela última vez
       Bb             F7
E a primeira na felicidade

        Bb                   Gm
O que importa é o amor, seja lá como for
      Cm               F7
Ter você todo dia, pra sempre assim

      Bb                 Gm
Minha vida vai ter outro gosto, o prazer
      Cm                  F7
De querer você perto, bem perto de mim

solo do meio: C# F F# Bbm Ebm C# F# Eb

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bbm = X 1 3 3 2 1
C# = X 4 6 6 6 4
Cm = X 3 5 5 4 3
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
Ebm = X X 1 3 4 2
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
F7 = 1 3 1 2 1 1
Fm = 1 3 3 1 1 1
Gm = 3 5 5 3 3 3
