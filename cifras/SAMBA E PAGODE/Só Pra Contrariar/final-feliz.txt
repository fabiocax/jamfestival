Só Pra Contrariar - Final Feliz

Música tocada por: Só pra contrariar e Caetano Veloso (Acústico)


Intro: (C#7+  G#/C  Bbm7  Bbm7/G#  Ebm7/9  Gbm6) 2x

C#7+         G#/C
Chega de fingir
Bbm7                   Bbm7/G#
Eu não tenho nada a esconder
F#7+              Gbm6
Agora é pra valer, haja o que houver
C#7+      G#/C
Eu Não tô nem aí
   Bbm7                    Bbm7/G#
Eu não tô nem aqui pro que dizem
  F#7+                 Gbm6
Eu quero é ser feliz, e viver pra ti
C#7+               G#/C  Bbm7   Bbm7/G#
Pode me abraçar sem me.....do
F#7+             C#/F   E7M   Bb7/9   Bb7
Pode encostar sua mão na minha

   Ebm7/9
Meu amor,
       Ab7/13   Absus Fm7/9 Bb7/9   Bb7/9   Bb7
Deixa o tempo se arrastar sem fim
   Ebm7/9
Meu amor,
     Ab7/13  Absus   Fm7/9    Bb7/b13   Bb7       No final aumenta-se 1/2 vai pra
Não há mal nenhum gostar....... assim             seguinte sequência :
      Ebm7/9
Oh, meu bem,                                      Em7/9 , A7/13 , Asus , F#m7/9 , B7/13
 Ab7/13  Absus   Fm7/9  Bb7/9   Bb7/b9   Bb7      B7.
Acredite no   final    feliz.
    Ebm7/9     Gbm6                               Em7/9 , A7/13 , Asus , F#m7/9 , B7/9
Meu amor, meu amor...                             B7/b9 , B7 , Em7/9 , Gm6.

----------------- Acordes -----------------
A7/13 = X 0 X 0 2 2
Ab7/13 = 4 X 4 5 6 X
Absus = 4 3 1 1 1 4
Asus = X 0 2 2 2 0
B7 = X 2 1 2 0 2
B7/13 = X 2 X 2 4 4
B7/9 = X 2 1 2 2 X
B7/b9 = X 2 1 2 1 X
Bb7 = X 1 3 1 3 1
Bb7/9 = X 1 0 1 1 X
Bb7/b13 = X 1 X 1 3 2
Bb7/b9 = 6 X 6 4 3 X
Bbm7 = X 1 3 1 2 1
Bbm7/G# = 4 1 3 1 2 1
C# = X 4 6 6 6 4
C#/F = X 8 X 6 9 9
C#7+ = X 4 6 5 6 4
E7M = X X 2 4 4 4
Ebm7/9 = X 6 4 6 6 X
Em7/9 = X 7 5 7 7 X
F#7+ = 2 X 3 3 2 X
F#m7/9 = X X 4 2 5 4
Fm7/9 = X X 3 1 4 3
G#/C = X 3 X 1 4 4
Gbm6 = 2 X 1 2 2 X
Gm6 = 3 X 2 3 3 X
