Só Pra Contrariar - Quem Dera

Introdução G7+ G#7+ A7+ A#7+ B7+ C7+ C#7+ D7+ D#7+ E7+ F7+ F#7+ G7+

C7+        G            Am7
A noite madrugou, se fez manhã
      B7           Em7    Eb°
Eu acordado ainda estou
          Dm   A7/9-
Pensando nela
  Dm            Dm7+
Viver sem esse amor
    Dm7               Dm6
Que é o verão do meu calor
   G7/9/11         G7/9     Gm7
Eu não....conseguirei....sem ela
C7/13       F7+
.....Quem dera.......(que dera !)
         Bb7
Se ela voltasse para mim
  Em7
Agora........(agora !)
         Am7             Eb°       Dm
Meu coração que além de triste, chora

   Em7      F7+
(Chora.....chora !)
     G7          Gm7
Voltaria a....sorrir
  C7/13             F7+
Talvez....ela não saiba........(o quê, o quê !)
  Bb7                     Em7
Que o meu amor ainda a espera
    Bb7         A7            Dm
Vivo sonhando acordado a imaginar
(O quê, o quê !)
    G7                             C   Bb7 ^ A7
Ela batendo em minha porta pra ficar
         Dm                               G7     C     A7
E nunca mais........(e nunca mais !).....par.....tir
         D7/9                            G7       C
E nunca mais........(e nunca mais !).....par.....tir
        G7
Cai a noite !..

----------------- Acordes -----------------
A#7+ = X 1 3 2 3 1
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
A7/9- = 5 X 5 3 2 X
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
B7+ = X 2 4 3 4 2
Bb7 = X 1 3 1 3 1
C = X 3 2 0 1 0
C#7+ = X 4 6 5 6 4
C7+ = X 3 2 0 0 X
C7/13 = X 3 X 3 5 5
D#7+ = X X 1 3 3 3
D7+ = X X 0 2 2 2
D7/9 = X 5 4 5 5 X
Dm = X X 0 2 3 1
Dm6 = X 5 X 4 6 5
Dm7 = X 5 7 5 6 5
Dm7+ = X X 0 2 2 1
E7+ = X X 2 4 4 4
Eb° = X X 1 2 1 2
Em7 = 0 2 2 0 3 0
F#7+ = 2 X 3 3 2 X
F7+ = 1 X 2 2 1 X
G = 3 2 0 0 0 3
G#7+ = 4 X 5 5 4 X
G7 = 3 5 3 4 3 3
G7+ = 3 X 4 4 3 X
G7/9 = 3 X 3 2 0 X
G7/9/11 = 3 X 3 2 1 X
Gm7 = 3 X 3 3 3 X
