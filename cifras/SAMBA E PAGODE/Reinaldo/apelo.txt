Reinaldo - Apelo

[Intro] Am  Am7+  Am7  Am6  F7+  Em7  Bm5-/7  E7

 Am         E/G#         E  Em7  F7+
 O....meu er...ro...foi
       Bm5-/7    E7   Am  Em7
Me entregar....demais
    F7+   G7  Dm  Dm7+  Dm7  G7  C7+     Am
Mas ago....ra........fim da nos...sa........dor
             Em7       Bm5-/7                   F7+
Do amor que nem se deu.........e nem fizemos jus
          Em7         Bm5-/7   G7/4
A luz do fogo que não acendeu
 C7+          Dm         G      F#º   G   Gm7
Má...goa.....na...da.....pra....dizer
        C7       F7+           Dm         Em7
Deixo a vida me levar.....tenho muito que viver
       C7        F7+          G7        C7+
Tinha tano pra te dar.....como pude te perder
     Am          Dm                          Em7
E o medo assim se faz.....quando a gente sem saber
        Am          Dm           G7/4     C7+
Sem querer se dar demais.....pra depois....sofrer

      Gm7            C7
Não...não...faz assim

           F7+           G7       C7+  Am
Não pode fazer o.....meu mundo....desabar
          Dm           G7       C7+  C7
Atende o apelo.....me escuta....e volta
          F7+            G7       C7+  Am
Você é minha luz.....meu tudo....e mais
             Dm          G7  G#7
Sem você a chama.....do amor apaga

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am6 = 5 X 4 5 5 X
Am7 = X 0 2 0 1 0
Am7+ = X 0 2 1 1 0
Bm5-/7 = X 2 3 2 3 X
C7 = X 3 2 3 1 X
C7+ = X 3 2 0 0 X
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Dm7+ = X X 0 2 2 1
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E7 = 0 2 2 1 3 0
Em7 = 0 2 2 0 3 0
F7+ = 1 X 2 2 1 X
G = 3 2 0 0 0 3
G#7 = 4 6 4 5 4 4
G7 = 3 5 3 4 3 3
G7/4 = 3 5 3 5 3 X
Gm7 = 3 X 3 3 3 X
