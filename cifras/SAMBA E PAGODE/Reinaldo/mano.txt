Reinaldo - Mano

 C#m             F#7            C#m              F#7             A
Mano, aonde você anda com essa cabeça, melhor parar antes que algo lhe aconteça
F#m             G#7             C#m             F#m             G#7
Estatisticamente eu sei no que vai dar, é por isso eu vim lhe aconselhar...
C#m             F#7             C#m             F#7             A
Mano, larga esse fuzil e vem falar comigo, eu não queria que levasse vida de bandido
F#m             G#7             C#m             Bm              E7
Pra quê tanta pistola, granada e ak, tô com vontade de chorar...

A               B7                                              G#m
Meu mano, minha mãe mandou um recado pra você, está morrendo de saudade
C#m             E7              A               F#m             G#7             C#m
E pediu pra lembrar que aos 10 anos de idade, você prometeu que nunca iria nos magoar...
Bm              E7 Bm           E7      A               F#m             G#7
O morro respeita sua condição, mas aqui quem fala é a voz do coração...

C#m                     Bm              E7              A
Você pode estar do lado errado, discriminado, mas é meu mano!
        F#m             G#7
Pode ser bandido e esquecer do teu passado, mas é meu mano!
C#m                     Bm                              A
Pode ser taxado de malandro, inconsequente, mas é meu mano!

                        F#m             G#7             C#m     G#7
Sangue do meu sangue vacilou, me trouxe a dor, mas é meu mano ...

Final: C#m F#7 C#m F#7 C#m

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#7 = 2 4 2 3 2 2
F#m = 2 4 4 2 2 2
G#7 = 4 6 4 5 4 4
G#m = 4 6 6 4 4 4
