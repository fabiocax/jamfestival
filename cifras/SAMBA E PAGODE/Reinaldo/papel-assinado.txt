Reinaldo - Papel Assinado

[Intro] Dm  Bm7(b5)  E7(b9)  Am
        Am/G  B7  E7  Gm7  A7
        Dm  Bm7(b5)  E7(b9)  Am
        Am/G  B7  E7  Am

Bm7(b5)              E7        Am                  Dm
A vida é doce mas também é curta, por favor me escuta
G7                C7+
Vamos ficar combinados
        E7                         Am
Já brigamos demais com esse papo furado
                 Bm7(b5)                 E7     Am
Esqueça o preto-no-branco isso e coisa do passado
Bm7(b5)            E7    Am
Somos felizes por viver assim
Em7(b5)           A7   Dm
Eu pra você e você para mim
Bm7(b5)             Bb7/9  Am
Esqueça amor essa burocracia
Bm7(b5)                       E7  Em7(b5)  A7
Pra nossa alegria não chegar ao fim

Dm      Bm7(b5)        E7  Am  Am/G
Esqueça amor essa burocracia
Bm7(b5)                       E7
Pra nossa alegria não chegar ao fim

A7
Eu falei

[Refrão]

Dm      E7(b9)  Am  Am/G
Nada de papel assinado
Bm7(b5)                            Bb7  A7
Se não der certo, cada um pro seu la...do
Dm        E7(b9)  Am  Am/G
Nada de papel assinado
Bm7(b5)                            E7   Am
Se não der certo, cada um pro seu la...do
E7                                  Am
Já vimos vários casais fazerem tudo certinho
                                 Bm7(b5)
Casar na igreja, madrinha e padrinho
E7                          Am
Várias testemunhas e depois separar
E7                                       Am
Nós que já nos conhecemos, pra que tanta pressa
Bm7(b5)                                E7(b9)
Vamos devagar, pois ha tempo abeça não vejo razão

Am                    A7
Pra se desesperar eu falei
Dm      E7(b9)  Am  Am/G
Nada de papel assinado
Bm7(b5)                            Bb7  A7
Se não der certo, cada um pro seu la...do
Dm        E7(b9)  Am  Am/G
Nada de papel assinado
Bm7(b5)                            E7   Am
Se não der certo, cada um pro seu la...do

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Am/G = 3 X 2 2 1 X
B7 = X 2 1 2 0 2
Bb7 = X 1 3 1 3 1
Bb7/9 = X 1 0 1 1 X
Bm7(b5) = X 2 3 2 3 X
C7+ = X 3 2 0 0 X
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
E7(b9) = X X 2 1 3 1
Em7(b5) = X X 2 3 3 3
G7 = 3 5 3 4 3 3
Gm7 = 3 X 3 3 3 X
