Reinaldo - Iniguálavel Paixão


E6/9                         G#m7
És inigualável na arte de amar
                               Bm7/11
Sou tão feliz que até posso afirmar
        E7             A7+
Jamais vivi um amor assim fez renascer
          Am7              B7             G#m7
De dentro de mim um sentimento que eu sepultei
     C#7          F#7/9           F#m7
Por desamor sofri confesso que chorei
   B7             E6/9                      G#m7
Foi bom  surgir você pra reabrir o meu coração e me
                  Bm7               E7            A7+
Induzir de novo a paixão e outra vez me fazer sorrir
                      Am7                 B7
A solidão se afastou enfim tudo que eu quero é ir mais
G#m7       C#7        F#7/9        B7           E            B7
Além pois encontrei o amor com a vida estou de bem (que a lua!)
        E6/9                       G#7
Que a lua venha nos iluminar e o sol para nos aquecer pois quando a

C#m                       Cm7^Bm7    E7        A7+   A#º  E  C#7   F#7/9
Brisa da manha chegar irá fortalecer ainda mais a união vou sonhar
 B7  E       B7
 Então  (QUE A LUA)!

----------------- Acordes -----------------
A#º = X 1 2 0 2 0
A7+ = X 0 2 1 2 0
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
Bm7 = X 2 4 2 3 2
Bm7/11 = 7 X 7 7 5 X
C#7 = X 4 3 4 2 X
C#m = X 4 6 6 5 4
Cm7 = X 3 5 3 4 3
E = 0 2 2 1 0 0
E6/9 = X 7 6 6 7 7
E7 = 0 2 2 1 3 0
F#7/9 = X X 4 3 5 4
F#m7 = 2 X 2 2 2 X
G#7 = 4 6 4 5 4 4
G#m7 = 4 X 4 4 4 X
