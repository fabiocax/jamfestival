Reinaldo - Eu e a Dor

Introdução: Dm7 / Ab° / Gm7 / C7/13 / F / Ab° / Gm7 / C7/13 /
    F                    Bb7      F      Gm7 ^ C7
Tem dor que é pra gente carregar sozinho            }
   F            Dm             Gm7    C7/11 ^ C7
E não dividir o pranto com ninguém                  }
    F          Bb                  F    D7/9-       BIS / REFRÃO
Por isso me deixa só......nesse cantinho                }
   G7/9           C7/13               F       (C7)
Só eu sei pra minha dor.....o que convém            }
F7      Bb7                             F      Dm
......O pranto que rola tão forte no meu rosto
 Gm7              C7             F    F7/4 ^ F7
Você...não vai entender, nem enxugar
Bb7                 C7            Am7    Dm7
.....Só eu sei de onde vem esse desgosto
G7/9                           C7
.....Até onde essa dor vai me levar
           F       Gm7                    Am7  C7
Conheço esse mar.....deixa meu barco, eu aprumo }
             F ^ Dm    Gm7 ^^^^^^ C7            F   C7              bis
Eu chego até lá, ô........ô.......eu chego até lá       }

REFRÃO
F7      Bb7                             F       Dm
......Antigas lições se fizeram pro meu mundo
  Gm7               C7              F   F7/4 ^ F7
Rochedo capaz de agüentar qualquer tremor
Bb7               C7           Am7   Dm7
.....Cada guerra vencida nessa vida
G7/9                                C7
......Lembra o carro de boi, bom gemedor
          F  Gm7             Am7  C7
O gosto do mel, apuro pela cabaça                       }
                F ^^ Dm    Gm7 ^^^^^ C7        F   C7         bis
Mamãe me ensinou, ô.......ô.......mamãe me ensinou..    }

----------------- Acordes -----------------
Ab° = 4 X 3 4 3 X
Am7 = X 0 2 0 1 0
Bb = X 1 3 3 3 1
Bb7 = X 1 3 1 3 1
C7 = X 3 2 3 1 X
C7/11 = X 3 3 3 1 X
C7/13 = X 3 X 3 5 5
D7/9- = X 5 4 5 4 X
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
F7/4 = 1 3 1 3 1 X
G7/9 = 3 X 3 2 0 X
Gm7 = 3 X 3 3 3 X
