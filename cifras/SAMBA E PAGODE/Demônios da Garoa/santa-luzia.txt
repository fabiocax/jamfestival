Demônios da Garoa - Santa Luzia

Am
Os teus lindos olhos são dois pingos d'água
                                     E
Meus olhos se embriagam com os olhos seus
Eu fico encantado com essa maravilha
                                   Am
Que são os seus olhos essa obra de Deus

Am
As flores que eu te levo escolho com os olhos
               A7              Dm
Meu peito bate forte só de te olhar
                                   Am
É lindo ver teus olhos olhando nos meus
                                    E
Meus olhos vivem namorando os olhos teus
                             Am
Santa Luzia abençoe o nosso olhar

Am       Dm              Am
San___ta Luzia, San___ta Luzia

             E             Am
Mãe dos meus olhos estrela guia
Am       Dm              Am
San___ta Luzia, San___ta Luzia
       Dm                E          Am
Te agradeço pelos nossos olhos todo dia

Am
Santa Luzia eu te peço proteção
                                      E
Que sobre os meus olhos ponha as suas mãos
Não deixe que eu esqueça esse teu olhar
                                       Am
Pois foi com esses olhos que aprendi a amar

Am
Santa Luzia é bonito a gente ver
                 A7           Dm
Os olhos de quem ama nos amanhecer
                                 Am
Cuida de quem tem os olhos da bondade
                                    E
Mas se alguém olhar com olhos de maldade
                                Am
Pões o teu amor nos olhos desse ser

Am       Dm              Am
San___ta Luzia, San___ta Luzia
             E             Am
Mãe dos meus olhos estrela guia
Am       Dm              Am
San___ta Luzia, San___ta Luzia
       Dm                E          Am
Te agradeço pelos nossos olhos todo dia

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
