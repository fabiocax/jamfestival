Demônios da Garoa - Olha o Gato

(intro) D

D    Bm7        Em7 A7
Bam        pam-padabam
D    Bm7        Em7 A7
Bam-       pam-padabam
D    D7    G
La-laia-la-ia-la

E7         A
La-laia-la-ia-la

D    Bm7        Em7 A7
Bam        pam-padabam
D    Bm7        Em7 A7
Bam-       pam-padabam
D    D7    G
La-laia-la-ia-la

( D Bm7 Em7 A7 D )


                  Em7       A7   D
Pega um gato com jeito que ele azunha.
     Bm7            Em7     A7  D      Bm7
Logo vi que era um diabo desse gato
       Bm7         Em7       A7        D
Que roubava as galinhas, comia os pintinhos,
     Bm7         Em7       A7        D
Mas hoje ele paga tim-tim por tim-tim.

Pega um gato com jeito que ele azunha.
Logo vi que era um diabo desse gato
Que roubava as galinhas, matava os pintinhos,
Mas hoje ele paga tim-tim por tim-tim.

                Em7        A7 D
Vou pegar esse gato que é ladrão,
             Em7        A7 D
Amarrar esse gato com cordão
             Em7        A7 D
E matar esse gato... agora não
                   Em7       A7      D
Que fazer com esse gato? Eu não sei, não.

               Em7      A7 D
Vou lavar esse gato com sabão,
    Bm7      Em7       A7 D
Sapecar esse gato com tição,
        D7       G      D     Bm7   Em7
Botar dentro do saco e jogar
      Em7     D
La-la-ia-la-la-ia

      Em7     D   A7  D B7  E 
La-la-ia-la-la-ia

               F#m7     B7    E
Vou pegar esse gato que é ladrão,
             F#m7        B7 E
Amarrar esse gato com cordão
             F#m7        B7 E
E matar esse gato... agora não
                   F#m7       B7      E
Que fazer com esse gato? Eu não sei, não.

    C#m7        F#m7     B7 E
Vou lavar esse gato com sabão,
    C#m7      F#m7     B7 E
Sapecar esse gato com tição,
        E7       A        E     C#m7   F#m7
Botar dentro do saco e jogar
       B7     E
Lá no meio do mar

       C#m7   F#m7   B7    E
Deixa o bicho miar Miau, miau...
      C#m7   F#m7    B7    E A7    D
Deixa o bruto miar Miau, miau...

Pega um gato com jeito que ele azunha.
Logo vi que era um diabo desse gato
Que roubava as galinhas, matava os pintinhos,
                                 D     Eb   E
Mas hoje ele paga tim-tim por tim-tim.

 F#m7     B7    E
Vou pegar esse gato que é ladrão,
             F#m7        B7 E
Amarrar esse gato com cordão
             F#m7        B7 E
E matar esse gato... agora não
                   F#m7       B7      E
Que fazer com esse gato? Eu não sei, não.

    C#m7        F#m7     B7 E
Vou lavar esse gato com sabão,
    C#m7      F#m7     B7 E
Sapecar esse gato com tição,
        E7       A        E     C#m7   F#m7
Botar dentro do saco e jogar
       B7     E
Lá no meio do mar

       C#m7   F#m7   B7    E
Deixa o bicho miar Miau, miau...
       C#m7   F#m7    B7  E  D# E
Deixa o bruto miar Miau, mia-a-u...

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
D# = X 6 5 3 4 3
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
Eb = X 6 5 3 4 3
Em7 = 0 2 2 0 3 0
F#m7 = 2 X 2 2 2 X
G = 3 2 0 0 0 3
