Demônios da Garoa - Menina Moça

Intro: F#m   B7 Em A7 Dm G Am E7

 Am                   E7
Me encosta no teu peito,
                                Am
 Feito uma criança, faça-me sonhar,
 F                      C
 Já não sou mais um menino,
                G                 C
 Não brinco de rodas e quero te amar,
  Am                         E7
 Já guardei os meus brinquedos,
                                 Am
 Nem contos do fadas, não ouço mais,
   F                  C
 Não sou moço de engenho,
              G               C
 Mas fui procurado nos canaviais.(bis)Na segunda vez termina em (Am)

  Am              E7
 Já acreditei na Eva, conquistando Adão,

               Am
 Dando-lhe a maçã,
  G                  C
 Num paraizo de pedras,
            E7             Am
 Eu tomava sol todas as manhãs,

 AM                 E7
E num berço cortinado,
                          Am
 Te imaginava sendo uma mulher,
 F                  C
 Mas como menina moça,
               G              C
 Me ame do jeito que você quiser.     (bis)Na segunda vez termina em (Am)

 Am                   E7
Tens a fita nos cabelos,
                    Am
 Muito amor no coração
 G                   C
 Vive sempre na janela,
        E7            Am
 Pra chamar minha atenção,
                     E7
 Me encosta no teu colo,
                     Am
 Mas não me faça dormir,
  F                    C
 Quero um beijo demorado,
        G          C         Bis(Am)
 Pra você me descobrir....

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
