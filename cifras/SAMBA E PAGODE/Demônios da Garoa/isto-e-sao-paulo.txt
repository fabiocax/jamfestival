Demônios da Garoa - Isto É São Paulo

(intro) Cmaj7   A7   D7   Fmaj7   Fm6   G7

C               A7             Dm7
Mil, quinhentos e cinqüenta e quatro,
Dm7      D#dim7                 Cmaj7/E
Quando de um colégio, deu-se a fundação,
   C6     D#dim7        Dm7
Nasceu a capital de um estado,
        Dm7
Desta nação.
Fmaj7      Fm7        G7
Que seria líder das demais,
 Cmaj7    A7             Dm7
Hoje a cidade que mais cresce neste mundo,
Bm7      E7          Am7  C7
É cidade dos arranha-céus,
     Fmaj7      F#dim7
Maior centro cultural,
C        A7
E industrial,
   Dm7        G7   Cmaj7
No trabalho, vem e vai,


           Cmaj7
Isto é São Paulo,
F      Cmaj7
Meu Brasil,
           F Dm7
Isto é São Paulo,
G    Cmaj7
Meu Brasil.
Cmaj7
Isto é São Paulo,
F      Cmaj7
Meu Brasil,
           F Dm7
Isto é São Paulo,
G    Cmaj7   G7
Meu Brasil.

Cmaj7       A7            Dm7
O trabalho sempre foi seu lema,
            F#dim7      C
Seu ordeiro povo ser trabalhador,
     C6           D#dim7     Dm7          Dm7
Mostrou em tantas obras sua força, o seu valor,
 Dm7          Fm6       G7
Obras que só fazem orgulhar,
 Cmaj7           A7
Túnel Nove de Julho,
   Dm7          Dm7
Elevado Costa e Silva,
Bm7    E7         Am7      C7
Ibirapuéra, Parque Anhembí,
Fmaj7           Fm7
Corrida São Silvestre,
   Cmaj7       A7
Estádio Morumbí,
        Dm7    G7   Cmaj7
E as estradas, pra rodar,

Cmaj7
Isto é São Paulo,
F      Cmaj7
Meu Brasil,
           F Dm7
Isto é São Paulo,
G    Cmaj7
Meu Brasil.
Cmaj7
Isto é São Paulo,
F      Cmaj7
Meu Brasil,
           F Dm7
Isto é São Paulo,
G    Cmaj7   G7
Meu Brasil.

O seu samba hoje consciente,
Tem entrada franca, no Municipal,
E da aquele recital,
Para quem quiser apreciar,
Suas noites hoje, se aquecem com pandeiro,
Timba, cavaquinho e violão,
Depois o Carnaval, no Anhangabaú,
É samba em festa a desfilar.

Cmaj7
Isto é São Paulo,
F      Cmaj7
Meu Brasil,
           F Dm7
Isto é São Paulo,
G    Cmaj7
Meu Brasil.
Cmaj7
Isto é São Paulo,
F      Cmaj7
Meu Brasil,
           F Dm7
Isto é São Paulo,
G    Cmaj7
Meu Brasil.

Cmaj7
Isto é São Paulo,
F      Cmaj7
Meu Brasil,
           F Dm7
Isto é São Paulo,
G    Cmaj7
Meu Brasil.
Cmaj7
Isto é São Paulo,
F      Cmaj7
Meu Brasil,
           F Dm7
Isto é São Paulo,
G    Cmaj7
Meu Brasil.

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am7 = X 0 2 0 1 0
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
C6 = 8 X 7 9 8 X
C7 = X 3 2 3 1 X
Cmaj7 = X 3 2 0 0 X
Cmaj7/E = 0 3 2 0 0 0
D#dim7 = X X 1 2 1 2
D7 = X X 0 2 1 2
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
F#dim7 = 2 X 1 2 1 X
Fm6 = 1 X 0 1 1 X
Fm7 = 1 X 1 1 1 X
Fmaj7 = 1 X 2 2 1 X
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
