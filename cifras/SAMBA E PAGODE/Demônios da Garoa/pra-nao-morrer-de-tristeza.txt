Demônios da Garoa - Pra Não Morrer de Tristeza

Am         Dm            Am
Mulher, deixaste tua moradia
      Dm        Am      A7           E
Pra viver de boemia e beber nos cabarés
   Dm      E                Am
E eu, pra não morrer de tristeza
                   E
Me sento na mesma mesa
E7                  Am
Mesmo sabendo quem és
   G                     C
E hoje nós vivemos de bebida
                     E
Sem consolo e sem guarida
                A7
Oh, mundo enganador
         Dm            E
Quem era eu, quem eras tu
            Am
Quem somos agora
                   F
Companheiros de outrora

   E          Am
Inimigos do amor

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
