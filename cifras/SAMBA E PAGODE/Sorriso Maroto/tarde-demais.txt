Sorriso Maroto - Tarde Demais

Bm      Bm7M    Bm7          Bm6  Em7     Em/D
  Tarde demais pra você se tocar, que sou eu
C#m7(b5)         F#7(#5)
Que sou eu teu lugar
Bm      Bm7M    Bm7          Bm6  Em7    Em/D
Por tantas vezes tentei te mostrar, que era eu
C#m7(b5)         F#7(4) F#7
E você me esnobando
G7M     Em7     G/A  F#7(#5)           Bm
Hoje eu consigo dormir, eu parei de chorar
    F#/Bb       Bm7      Em7     G/A
Demorou pra passar, mas voltei a sorrir
           D7M     G7M  C#m7(b5)    F#7(4)
Encontrei meu lugar e ganhei meu valor

[Refrão]

Bm7(11)                    Am7(9)               D7(13)      G7M
Tarde pra pensar em reatar, não dá pra voltar no que passou
        Em7  C#m7(b5)  F#7(#5)   Bm
É muito tarde pra falar de amor

          F#/Bb           Am7(9)               D7(13)      G7M
Tarde pra falar foi sem querer, tarde pra dizer amo você
        Em7  C#m7(b5)  F#7(#5)    Bm
É muito tarde pra se arrepender

----------------- Acordes -----------------
Am7(9) = X X 7 5 8 7
Bm = X 2 4 4 3 2
Bm6 = 7 X 6 7 7 X
Bm7 = X 2 4 2 3 2
Bm7(11) = 7 X 7 7 5 X
Bm7M = X 2 4 3 3 2
C#m7(b5) = X 4 5 4 5 X
D7(13) = X 5 X 5 7 7
D7M = X X 0 2 2 2
Em/D = X X 0 4 5 3
Em7 = 0 2 2 0 3 0
F#/Bb = 6 X 4 6 7 X
F#7 = 2 4 2 3 2 2
F#7(#5) = 2 X 2 3 3 2
F#7(4) = 2 4 2 4 2 X
G/A = 5 X 5 4 3 X
G7M = 3 X 4 4 3 X
