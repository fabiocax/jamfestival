Sorriso Maroto - Pra Mim Não É

Intro: G#/F# G#m7 D#m7 F7M E7M

E7M                E6/9                   G#/F#
Do meu jeito louco diferente pra falar de amor
         G#m7 D#m7 F7M E7M
Eu to aqui . . .
E7M           E6/9                         G#/F#
Serenata, violão na mão peito aberto eu vou
             G#m7  D#m7 F7M E7M
Tente me ouvir . . .
E7M              E9                         F#/G#
Céu a lua e as estrelas testemunham ao meu favor
            G#m7 D#m7  F7M   E7M
Como eu sofri . . .
E7M             E9                            F#/G#
Pode parecer bobagem, sendo otário ou coisa assim
       G#m7 D#m7 F7M E7M
To nem ai . . .
E7M
Se pra você o nosso amor não ta com nada
B7M                   F7/11+
Se pra você todo esse sonho é palhaçada

E7M
Se pra você tudo o que fiz foi dá mancada
   B7M                                     ( 62 52 54 54 )
Pra mim não é, Pra mim não é, Pra mim não é !
E7M
Se pra você não tem saída no fim da estrada
B7M                         F7/11+
Se pra você ontem foi tudo,hoje sou nada
E7M
Se pra você sou passatempo ou madrugada
B7M                   A/B
Pra mim não é, eu vou provar por que não é

Refrão:
E7M                   E/F#
Por que te amo e não sei viver sem teu calor
B7M                     B7/4          A/B
Por que te amo basta olhar pra ver como eu estou
E7M                    E/F#
Por que te amo com saudades fiz essa canção
F#/G#              F7/11+
Se não me ama fala logo ou quebro o violão

E7M                   E/F#
Por que te amo e não sei viver sem teu calor
B7M                    B7/4           A/B
Por que te amo basta olhar pra ver como eu estou
E7M                    E/F#
Por que te amo com saudades fiz essa canção
F#/G#
Se não me ama fala logo ou quebro o violão

E7M                 E6/9
Pra mim não é, pra mim não é, pra mim não é
F#/G#            G#m7       D#m7 F7M E7M
Pra mim não é, seja o que deus quiser
E7M                 E6/9
Pra mim não é, pra mim não é, pra mim não é
F#/G#                  G#m7  D#m7 F7M E7M
Pra mim não é, pode falar o que quiser

----------------- Acordes -----------------
A/B = X 2 2 2 2 X
B7/4 = X 2 4 2 5 2
B7M = X 2 4 3 4 2
D#m7 = X X 1 3 2 2
E/F# = X X 4 4 5 4
E6/9 = X 7 6 6 7 7
E7M = X X 2 4 4 4
E9 = 0 2 4 1 0 0
F#/G# = 4 X 4 3 2 X
F7/11+ = 1 X 1 2 0 X
F7M = 1 X 2 2 1 X
G#/F# = 2 X 1 1 1 X
G#m7 = 4 X 4 4 4 X
