Sorriso Maroto - Eu Não

Intro:  F7+  F/G  Em7  Am7 F7+ F/G

C7+
Eu não quero mais te ver,
Am7/9
Tento evitar você,
Dm7                    F/G
É tanta emoção, sei lá porque
      Bb9                       D/F#
Me perco em teu olhar, sem perceber
     Dm7                 F/G   G7/9-
E digo que te amo, sem querer

C7+
Tenho que me acostumar
Am7/9
É melhor não complicar
Dm7                      F/G
Eu não consigo mesmo te deixar,
      Bb9                          D/F#
Melhor fugir, pra não ter que encarar

     Dm7                      F/G   Gm7/9 - C7
A verdade é que não da, pra segurar

    F7+
Eu não, vou brincar comigo
    Em7                 Gm7  C7
Eu não, vou perder meu tempo
   F7+                F/G        C7+    Gm7 - C7
Eu não, vou fingir que tá tudo bem..
    F7+
Eu não, não posso ficar nessa,
    Em7                  Gm7  C7
Eu não, o coração tem pressa
   F7+        F/G        C7+    F/G
Se não for você não é ninguém.

Repete a música toda e o refrão 2x.

Final: C7+  Fm7  C7+  Fm7  C7+

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Am7/9 = X X 7 5 8 7
Bb9 = X 1 3 3 1 1
C7 = X 3 2 3 1 X
C7+ = X 3 2 0 0 X
D/F# = 2 X 0 2 3 2
Dm7 = X 5 7 5 6 5
Em7 = 0 2 2 0 3 0
F/G = 3 X 3 2 1 X
F7+ = 1 X 2 2 1 X
Fm7 = 1 X 1 1 1 X
Gm7 = 3 X 3 3 3 X
Gm7/9 = X X 5 3 6 5
