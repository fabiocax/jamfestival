Sorriso Maroto - Preciso Viver

Introdução: Bm7 Bbm7 Am7 D7/9/11

 G7M      G6  C7M  D7  G7M    G6     Am7    F7
Chega de infelicidade, tô de volta a liberdade
Bb            Eb
Vou ficar sozinho novamente,
Bb           Eb
Antes só que infeliz com a gente
Am7        Bm7   Am7      D7/9
Chega de sofrer, preciso viver
G7M    G6   C7M   D7   G7M     G6
Tô sentindo no momento a paz que eu
     Am7      F7
não tinha há tempos
Bb             Eb
Mas não vou ficar me lamentando
Bb             Eb
Pois a vida é bela e eu vou levando
Am7          Bm7   Am7         D7/9
Tenho tanto a dar, hei de encontrar
G7M      G6  C7M          D7/9
Uma namorada linda e verdadeira

G7M            G6    Am7          F7
Pra ser minha amada, minha companheira
Bb         Eb    Bb            Eb
Homem vencedor, tem um grande amor
Am7         Bm7 Am7           D7/9
Que lhe apoiou, que lhe deu valor

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Bb = X 1 3 3 3 1
Bbm7 = X 1 3 1 2 1
Bm7 = X 2 4 2 3 2
C7M = X 3 2 0 0 X
D7 = X X 0 2 1 2
D7/9 = X 5 4 5 5 X
Eb = X 6 5 3 4 3
F7 = 1 3 1 2 1 1
G = 3 2 0 0 0 3
G6 = 3 X 2 4 3 X
G7M = 3 X 4 4 3 X
