Sorriso Maroto - Por Que?

D/F#                                A/C#
Porque só aparece assim na hora errada?
                              G7+
E nessa hora não sei fazer nada
      Em7                  A7
Se te aceito eu complico toda minha vida
G/B  D/F#
    Por que
                             A/C#
Tinha que ser assim justo comigo?
                    G7+
Seu coração virou o meu destino
                   Em7
O meu destino é me perder
                 A7
Será que tem saída?
          Bm
Por que?
                              A/C#
Tudo passa na vida e pude aprender
                           F#m
Só não passa o amor que eu sinto por você

   A/C#    A7   D/F#   G7+
É forte de enfra...que...cer
                      Em7    A7
Se com ela eu desejo você
     D     D7
Por quê?

G7+
Por quê?
                            Em7
Me deixou e mal disse o porquê
                           F#m
Fiz meu porto seguro em você
                             Am7         D7
Me envolvi com alguém só pra te esquecer
G7+
Por quê?
                            Em7
Se eu já não quero mais te querer
                     F#m
Nada meu quer me obedecer
               Am7          D7
E agora me dê opção de escolher
             Em             A/C#
Ou lutar pra te esquecer,
           Em                  A7              D  A/G        Am7      D7
Ou voltar pra um louco amor.         Você..ê... ....O que fazer???

----------------- Acordes -----------------
A/C# = X 4 X 2 5 5
A/G = 3 X 2 2 2 X
A7 = X 0 2 0 2 0
Am7 = X 0 2 0 1 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F#m = 2 4 4 2 2 2
G/B = X 2 0 0 3 3
G7+ = 3 X 4 4 3 X
