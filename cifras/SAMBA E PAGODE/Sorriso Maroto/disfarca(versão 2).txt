Sorriso Maroto - Disfarça

Introdução: Bb7M  A7M  Bb7M  A7M

A7M
Deixa
                 F#m7
Que eu me viro, deixa
             Cm
Se é dessa maneira
            G#m7
Que você prefere
              A7M
Diz que me esquece
             F#m7
E que não merece
           Cm
Já fiz uma prece
G#m7          G#7M
Pra você voltar

F#/A#
    Toda vez que eu olho pra você

Bb7M
    Sinto o meu corpo estremecer
A7M
    Teu olhar perdido em meu olhar
Bm7
    E você tenta me convencer
G#7M
    Pra que eu viva sem você
Bb7M         A7M
    Só que mal...

   A7M
Disfarça
            F#/A#
Você mal disfarça
        G#7M
Aquele jeito de me olhar
C#7/9
Quando quer ficar
   A7M
Disfarça
            F#/A#
Você mal disfarça
                  G#7M
Não sei pra que viver assim
   C#7/9
Sofrendo, é tão ruim
    A7M
Disfarça
           F#/A#
Toda essa mágoa

(Volta pra mim)

----------------- Acordes -----------------
A7M = X 0 2 1 2 0
Bb7M = X 1 3 2 3 1
Bm7 = X 2 4 2 3 2
C#7/9 = X 4 3 4 4 X
Cm = X 3 5 5 4 3
F#/A# = 6 X 4 6 7 X
F#m7 = 2 X 2 2 2 X
G#7M = 4 X 5 5 4 X
G#m7 = 4 X 4 4 4 X
