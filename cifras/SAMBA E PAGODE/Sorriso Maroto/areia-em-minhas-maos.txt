Sorriso Maroto - Areia Em Minhas Mãos

Intro: F  C  G  C  G  F  C

C                       G
Eu achei que o mundo só girava ao meu redor
Am                       F
Eu pensei que tudo o que escolhia era melhor
C                            G
Eu jogava os dados, dava as cartas, guerra e paz
Am                        F
Quando acordei sozinho percebi que tanto faz
F        C          G
Sem ter amor tudo é em vão
F        C          G
Sem ter amor tudo é em vão
C                       G
Quando percebi que meu espelho era ilusão
Am                      F
Todo o meu poder virou areia em minhas mãos
C                       G
No combate de amor não tem como vencer
Am                            F
Pra viver amor sincero, pra ganhar quem se render

F        C          G
Sem ter amor tudo é em vão
F        C           G        F  G
Sem ter amor tudo é em vão
C                    Am
Oh oh oh oh oh oh oh oh oh oh oh oh
F        C        G
Sem ter amor, dói no coração

C                       G
Quando percebi que meu espelho era ilusão
Am                      F
Todo o meu poder virou areia em minhas mãos
C                       G
No combate de amor não tem como vencer
Am                            F
Pra viver amor sincero, pra ganhar quem se render
F        C          G
Sem ter amor tudo é em vão
G
Dói no coração
F        C          G       F  G
Sem ter amor tudo é em vão
C                    Am
Oh oh oh oh oh oh oh oh oh oh oh oh
F         C        G
Sem ter amor, dói no coração uh oh

Solo: C Am

F         C         G
Sem ter amor tudo é em vão uh
C                    Am
Oh oh oh oh oh oh oh oh oh oh oh oh
F        C         G            F C G
Sem ter amor, dói no coração
F        C
Sem ter amor

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
