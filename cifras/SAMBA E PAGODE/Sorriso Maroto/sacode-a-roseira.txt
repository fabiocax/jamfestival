Sorriso Maroto - Sacode a Roseira

Introdução:

 F  D7    Gm           C7           F   D7
Ve.e.e.em, sacode a roseira sem parar
Gm           C7            F    D7
Balança a cadeira, vem pra cá
Gm              C7           F
Com o Sorriso Maroto vem sambar
            D7
Vem que tem, vem meu bem
Gm        C7
Vem neném, vem que tem
F  D7    Gm           C7           F   D7
Ve.e.e.em, sacode a roseira sem parar
Gm           C7            F    D7
Balança a cadeira, vem pra cá
Gm              C7
Com o Sorriso Maroto vem sambar
F          C7              F
Oi! bota a mão no lê, lê, lê
       C7             F
Cadê você no lá, lá, lá

    D7           Gm
O batuque deu sinal
      C7
Tudo agora é alegria
F       D7         Gm
Vem pra roda, pessoal
       C7           F
Tem pagode noite e dia,
      D7          Gm
Cara triste pega mal
       C7
Ser feliz é o que faz bem
F     D7         Gm
Todo mundo tá legal
        C7
Porque só você não vem

----------------- Acordes -----------------
C7 = X 3 2 3 1 X
D7 = X X 0 2 1 2
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
