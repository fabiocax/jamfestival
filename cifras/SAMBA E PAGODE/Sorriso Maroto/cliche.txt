Sorriso Maroto - Clichê

[Intro]

E|-----------5-------5--|------------5-------5--|
B|------8------8---8---8|--------8-----8---8---8|
G|----5---5------5------|------5---5-----5------|
D|----------------------|-----------------------|
A|--8-------------------|-8-7-------------------|
E|----------------------|-----------------------|

E|-----------5-------5--|------------5-------5--|
B|------8------8---8---8|--------8-----8---8---8|
G|----5---5------5------|------5---5-----5------|
D|----------------------|-----------------------|
A|--8-------------------|-8-7-------------------|
E|----------------------|-----------------------|

Am7/5+                     Am7
sem você passei a ver o que nunca enxerguei
Am7/5+                     Am7
sem você me dei um tempo e me repensei
Am7/5+                          Am7
eu me vi naquelas folhas de outra estação

Am7/5+                            Am7
que sem vida são varridas secas pelo chão
Am7/5+                  Am7
Acreidto hoje em coisas que me ensinou
Am7/5+                Am7
Acredito que dias melhores tão por vir
Am7/5+                Am7
E amores de verdade surgem num olhar
Am7/5+                 Am7          Am7/5+  Am7
Acredito que agente possa ser feliz

Dm7
Desculpe chegar sem avisar
Em7
Mais eu não consigo esquecer
 Fm7                C7+       G
Voce dizendo que já não me ama
F/A
Ensaiei mil coisas pra falar
    Em7
Pra mim são dificeis de dizer
Fm7                           F C7+  G
Em sonhos de amor sua voz me chama
        F/A           Gsus
Sei que parece clichê preciso de você

F7+                        Gsus
Meu amor eu tive que perder pra acordar
 Gsus9
Eu sei que foi dificil de lhe dar
C7+               G/B           Am6
Sempre fechado sem falar de amor de sol de flor
F7+                 Gsus
Vim contar que ando amargando a solidão
Gsus9                 C C             G/B G/B
Você me fez sentir um coração pulsar doer
        A6 A6        ( B|-6--5-3-| )
Mesmo se não voltar queria te dizer
F7+                G7+ Bb7+ A7+ Gsus
  Que aprendi a ser melhor...       com voce

E|-----------5-------5--|------------5-------5--|
B|------8------8---8---8|--------8-----8---8---8|
G|----5---5------5------|------5---5-----5------|
D|----------------------|-----------------------|
A|--8-------------------|-8-7-------------------|
E|----------------------|-----------------------|

E|-----------5-------5--|------------5-------5--|
B|------8------8---8---8|--------8-----8---8---8|
G|----5---5------5------|------5---5-----5------|
D|----------------------|-----------------------|
A|--8-------------------|-8-7-------------------|
E|----------------------|-----------------------|

Dm7
Desculpe chegar sem avisar...   ate o fim
F7+                        Gsus
Meu amor eu tive que perder pra acordar... até o fim

----------------- Acordes -----------------
A7+ = X 0 2 1 2 0
Am6 = 5 X 4 5 5 X
Am7 = X 0 2 0 1 0
Am7/5+ = X 0 3 0 1 0
Bb7+ = X 1 3 2 3 1
C = X 3 2 0 1 0
C7+ = X 3 2 0 0 X
Dm7 = X 5 7 5 6 5
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
F7+ = 1 X 2 2 1 X
Fm7 = 1 X 1 1 1 X
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G7+ = 3 X 4 4 3 X
Gsus = 3 2 0 0 0 3
Gsus9 = 3 X 0 2 0 X
