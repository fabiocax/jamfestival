Sorriso Maroto - Se Eu Te Pego, Te Envergo

Intro: C7+/G  B7+/F#  Bb7+  Bb7sus4 Bb7

Eb7+
Não é fogo de palha
                      Dm7
Você vai jogar a toalha
                    Gm7
Pedindo pra eu parar
 Eb7+
Você não tem noção do quanto eu te desejo
              Dm7                      Gm7
Sinto gosto do seu beijo só de te olhar

F7+
Chego fico arrepiado
                        Em7
Quando para do meu lado
                     Am7
Dá vontade de te agarrar
Dm7(9)                 Em7
se ta achado q e historia

                     F7+
tenta a sorte pra tu ve
                 Em7
tu não vai se arrepende

eu garanto
            F7+ (3X)
Já faz um tempão
         G7sus4
Que eu to querendo você

Dm7(9)                    Em7
Deixa eu te enlouquecer
               F7+
Preciso te sentir
                Em7 (Pausa)
Você vai delirar

To falando sério não ri
Dm7                  Em7
Se você pagar pra ver
                 F7+
Vai se surpreender
                Em7
Não vai se conformar
                            G7sus4
De ter perdido todo esse tempo
          C7(9)          C7(9)
Faço uma aposta com você
F7+                         Em7
Garota se eu te pego, te envergo
                                           Gm7 C7(9)
Você vai pirar, você vai gamar, vai se apaixonar
F7+              F6(9)
Pode me chamar de convencido
                     Em7
Não esquento eu não ligo
                       Am7
Vem logo me experimentar    (2x)
Dm7(9)
Que eu vou te pegar
            Em7            F7+
Daquele jeito, daquele jeito
             G7sus4
Acho bom vir preparada meu bem

Dm7(9)
Que eu vou te pega
          Em7              F7+
Daquele jeito, daquele jeito
            G7M             C7(9)
Chega de conversa e vem que tem

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
B7+/F# = X X 4 4 4 6
Bb7 = X 1 3 1 3 1
Bb7+ = X 1 3 2 3 1
Bb7sus4 = X 1 3 1 4 1
C7(9) = X 3 2 3 3 X
C7+/G = 3 X 2 4 1 X
Dm7 = X 5 7 5 6 5
Dm7(9) = X 5 3 5 5 X
Eb7+ = X X 1 3 3 3
Em7 = 0 2 2 0 3 0
F6(9) = 1 0 0 0 X X
F7+ = 1 X 2 2 1 X
G7M = 3 X 4 4 3 X
G7sus4 = 3 5 3 5 3 X
Gm7 = 3 X 3 3 3 X
