Sorriso Maroto - Na Maldade

Intro: B5  Db5   Eb5   Db5 (2x)

Brinca comigo não. Eu tô na tua mão
No foco, na mira, na tua intenção
Que tal me acompanhar, ir pra algum lugar
Discreto, que eu faço você se apaixonar

 Ataque: Ebm  Ebm  Ebm  Ebm

B                         Db
Pra me ganhar tem que me deixar louca
Ebm
Me prende agora porque eu ando solta
B               Db
Eu sou difícil, mas não fico à toa
Ebm Ebm
Me mostra que é capaz de beijar a minha boca

B                   Db
Vem que hoje eu tô cheia de vontade
Ebm
É agora, não demora

Hora da verdade então
B                Db
Vem que eu tô te querendo na maldade
Ebm
Então rebola, vem
Então rebola e...

B
Vem, vem, vem...
Db
Vem, vem, vem
Ebm
Relaxa que agora eu vou mostrar como é que faz
B
Vem, vem, vem...
Db
Vem, vem, vem
Ebm Ebm
Não para que teu beijo me enlouquece, é bom demais

B                  Db
Brinca comigo não. Eu tô na tua mão
Ebm
No foco, na mira, na tua intenção
B                      Db
Que tal me acompanhar, ir pra algum lugar
Ebm Ebm
Discreto, que eu faço você se apaixonar

B                         Db
Pra me ganhar tem que me deixar louca
Ebm
Me prende agora porque eu ando solta
B               Db
Eu sou difícil, mas não fico à toa
Ebm Ebm
Me mostra que é capaz de beijar a minha boca

B                   Db
Vem que hoje eu tô cheia de vontade
Ebm
É agora, não demora
Hora da verdade então
B                Db
Vem que eu tô te querendo na maldade
Ebm
Então rebola, vem
Então rebola e...  ( 2x )

B
Vem, vem, vem...
Db
Vem, vem, vem
Ebm
Relaxa que agora eu vou mostrar como é que faz
B
Vem, vem, vem...
Db
Vem, vem, vem
Ebm Ebm
Não para que teu beijo me enlouquece, é bom demais

B
Vem, vem, vem...
Db
Vem, vem, vem
Ebm Ebm
Relaxa que agora eu vou mostrar como é que faz
B
Vem, vem, vem...
Db
Vem, vem, vem
      Ebm Ebm
Não para que teu beijo me enlouquece, é bom demais

Final:
B                  Db
Brinca comigo não. Eu tô na tua mão
Bm                Bbm                       Db
No foco, na mira, na tua intenção

----------------- Acordes -----------------
B = X 2 4 4 4 2
B5 = X 2 4 4 X X
Bbm = X 1 3 3 2 1
Bm = X 2 4 4 3 2
Db = X 4 6 6 6 4
Db5 = X 4 6 6 X X
Eb5 = X 6 8 8 X X
Ebm = X X 1 3 4 2
