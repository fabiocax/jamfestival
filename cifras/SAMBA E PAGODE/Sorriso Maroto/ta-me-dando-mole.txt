Sorriso Maroto - Tá Me Dando Mole

A7+                            F#m7/11
Quando estamos perto e junto de nossos amigos
        Bm7                   E7/4
Joga charme e sorriso pra me provocar
A7+                          F#m7/11
Se eu quero amarelo, diz que azul é mais bonito
           Bm7                       E7/4
Eu falo em sol, você em frio, só pra implicar
D/F#                               C#m7
  Já estão botando pilha que isso é amor
Bm7                                D/E
  Todo mundo por aqui também já reparou
    E7            A7+ ^ A7+ ^A7+ ^ A7+       D/E ^ E7
Que tá me dando mole

A7+            C#m7           D7+
Vem fala a verdade, fica a vontade
             Dm6
Me beija sem medo. Se entrega então
A7+         C#m7          D7+
Vem fica comigo até mais tarde

           Dm6                 A7+
Tô te querendo e você quer também
             C#m7          D7+
esse é o destino dessa amizade
            Dm6
Se a gente pode, não foge então
C#m7         F#m7/11       Bm7
Vem eu tô sozinho e você sabe
    E7/9          A7+
Que tá me dando mole

C#m7   Bm7     E7/9         A7+
ee     ee  que ta me dando mole ee.......2X

----------------- Acordes -----------------
A7+ = X 0 2 1 2 0
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
D/E = X X 2 2 3 2
D/F# = 2 X 0 2 3 2
D7+ = X X 0 2 2 2
Dm6 = X 5 X 4 6 5
E7 = 0 2 2 1 3 0
E7/4 = 0 2 0 2 0 X
E7/9 = X X 2 1 3 2
F#m7/11 = 2 X 2 2 0 X
