Sorriso Maroto - Marque Um Lugar

Intro:C/C7/F7+/G7

C       C7     F7+    G7
Preciso ter você perto de mim
C       C7          F7+     G7         (refrão)
Bem mais que o seu olhar ouvir um sim

C               D
Marque um lugar pra gente se encontar
Dm                      C       G7
Preciso tanto conversar com você
C                     D
Seu jeito de me olhar revela a timidez
Dm                  C     E7/9
Não pude acreditar que fez

Am       Am5+        Am7       Am5+
Um sinal que cativou meu coração
Am           Am5+       Gm7      C7/9
Não sei bem se foi real ou ilusão
F7+             E7/9
Desde então não parei

Am       Gm7# Gm7 C7/9
De pensar em você
F7+           G7
Essa noite eu quero te ter
(volta ao refrão)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am5+ = 5 8 7 5 6 5
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
C7/9 = X 3 2 3 3 X
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E7/9 = X X 2 1 3 2
F7+ = 1 X 2 2 1 X
G7 = 3 5 3 4 3 3
Gm7 = 3 X 3 3 3 X
