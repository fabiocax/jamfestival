Sorriso Maroto - Safadiar

Intro 2x: G D

G                D
De noite, de dia, na boa, na fria
G
Carteira vazia
D                    G
Ah hoje eu vou pagodear
                   D
Na serra, na praia, de baixo da saia
G
Tomara que caia
D                    G
Ah hoje eu vou safadiar

    C         G          D          G
Eu vou me acabar, trabalhei vou gastar
      C           G         D
Pra que se estressar,vou beber
             G
Eu vou safadiar


Refrão:
G                 D              G
Lêlê lêlê lêlê lêlê eu vou safadiar
                  D              G
Lêlê lêlê lêlê lêlê eu vou pagodear
G                 D              G
Lêlê lêlê lêlê lêlê eu vou safadiar
                  D              G
Lêlê lêlê lêlê lêlê eu vou pagodear

G                  D
É namoro, é amante, é enrolo, é ficante
G
Se é peguete que se dane!
D
Ah hoje eu vou pagodear
G                   D
De refri ou de água, de cerveja, de cachaça
G
Na manguaça na resaca
D
Ah hoje eu vou safadiar

    C         G          D          G
Eu vou me acabar, trabalhei vou gastar
      C           G         D
Pra que se estressar,vou beber
             G
Eu vou safadiar

Refrão:
G                 D              G
Lêlê lêlê lêlê lêlê eu vou safadiar
                  D              G
Lêlê lêlê lêlê lêlê eu vou pagodear
G                 D              G
Lêlê lêlê lêlê lêlê eu vou safadiar
                  D              G
Lêlê lêlê lêlê lêlê eu vou pagodear

G                        D
Sou do funk, sou do rock, do axé e do pop
G
Sertanejo ou pagode
D
Ah hoje eu vou pagodear
G                   D
Carioca ou paulista, a baiana é artista
G
A gente é conquista
D
Ah hoje eu vou safadiar

    C         G          D          G
Eu vou me acabar, trabalhei vou gastar
      C           G         D
Pra que se estressar,vou beber
             G
Eu vou safadiar

Refrão:
G                 D              G
Lêlê lêlê lêlê lêlê eu vou safadiar
                  D              G
Lêlê lêlê lêlê lêlê eu vou pagodear
G                 D              G
Lêlê lêlê lêlê lêlê eu vou safadiar
                  D              G
Lêlê lêlê lêlê lêlê eu vou pagodear

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
