Sorriso Maroto - Promessas

             Bb7+
Eu já te conheço É fácil de notar
Gm7
Fica transparente Ver no seu olhar
Cm7                        F7/9
O que está passando na sua cabeça...
Bb7+
Estão fazendo intrigas pra nos separar
Gm7
Quem não me conhece não pode falar
Cm7            Cm7+
Isso é inveja, tente raparar
              Fm7               Bb7
Nunca dei motivos pra desconfiar
Eb7+           Ebm6        Bb7+
Se eu não estivesse com você
C#°
Estaria sem ninguém
   Cm      Dm    Ebm6     F7  Bb       Fm7  Bb7/13
E faço promessas pra te provar, te provar
Eb7+
Prometo ser fiel a você

F7/9
Te amar com o fogo da paixão
F/G
Não quero mais ninguém Pode crer
G7/13-
E deixo tudo em suas mãos
  Cm
Avise a quem queira saber
     Ebm6             F7       Bb
Que esse amor é pra durar pra sempre...

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bb7 = X 1 3 1 3 1
Bb7+ = X 1 3 2 3 1
Bb7/13 = 6 X 6 7 8 X
C#° = X 4 5 3 5 3
Cm = X 3 5 5 4 3
Cm7 = X 3 5 3 4 3
Cm7+ = X 3 5 4 4 3
Dm = X X 0 2 3 1
Eb7+ = X X 1 3 3 3
Ebm6 = X X 1 3 1 2
F/G = 3 X 3 2 1 X
F7 = 1 3 1 2 1 1
F7/9 = X X 3 2 4 3
Fm7 = 1 X 1 1 1 X
G7/13- = 3 X 3 4 4 3
Gm7 = 3 X 3 3 3 X
