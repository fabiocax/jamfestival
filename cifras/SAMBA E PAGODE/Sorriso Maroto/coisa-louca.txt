Sorriso Maroto - Coisa Louca

Eb7M                               Eb/F
Não, eu não vou ficar na ilusão
                                   Dm7
Que é todo meu seu coração
                              F/G
Te vi outro dia com alguém
Bb7(9)                                Eb7M
Não me diga que não tem ninguém
                            Eb/F
É difícil, mas vou aceitar
                                         Dm7
Mas vou por alguém em seu lugar
                                        F/G
Não foi bem assim que eu planejei, mas me liguei

                Eb7M
Que hoje em dia o mundo tá assim
                    Eb/F
O que é bom pra você não é pra mim
       Dm7                             F/G           G7
Vou tentar me adaptar a esse jeito novo de amar

Cm7            Dm7               F
E quem sabe assim vou ser feliz

Bb                                               E7(#11) Eb7M
Vou beijar na boca vou passear com outra e dai?
       Eb/F
Foi você quem fez assim
Bb                      E7(#11)                  Eb7M
Ai que coisa louca nunca imaginei agir assim
   Eb/F                   Bb  E7(#11)
Vi você e aprendi

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bb7(9) = X 1 0 1 1 X
Cm7 = X 3 5 3 4 3
Dm7 = X 5 7 5 6 5
E7(#11) = X X 2 3 3 4
Eb/F = X 8 8 8 8 X
Eb7M = X X 1 3 3 3
F = 1 3 3 2 1 1
F/G = 3 X 3 2 1 X
G7 = 3 5 3 4 3 3
