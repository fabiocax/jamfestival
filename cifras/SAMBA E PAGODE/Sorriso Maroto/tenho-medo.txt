Sorriso Maroto - Tenho Medo

[Intro]  G  C9  G9  C9  D7/4

 G7+                             C9          D4
Será, que é melhor eu agora ser sincero e falar
    G7+         Em7                 C7+       G/B      Am7
Ou será, que é melhor guardar esse segredo e não arriscar
 C7+/9                     D4        G
Porque se você se afastar de mim eu vou sofrer
 F#m7/4+     B7       Em7
Perder a sua amizade não vai ser legal
       C7+                 G/B
Tenho medo de estar confundindo
                  Am7/9   Am7/G   F7+  D4
Mais o que estou sentindo é especial

             G7+
Você conhece os meus problemas
     D/F#     Em7         Dm7  G7/9
Meu relacionamento complicado
      C7+                          G/B
Sabe tudo o que penso e o que eu quero

             F7+                  Am7  D7
Que Deus me perdoe se isso for errado
                     G7+
Se estou com ela eu te desejo
    D/F#         Em7        Dm7  G7/9
Eu sonho em cometer esse pecado
        C7+              G/B
Se eu criei coragem pra falar
                 A7/9  Am7  Dsus
Pode crer que o meu coração está
       C9  D4
Apaixonado

----------------- Acordes -----------------
A7/9 = 5 X 5 4 2 X
Am7 = X 0 2 0 1 0
Am7/9 = X X 7 5 8 7
Am7/G = 3 0 2 0 1 0
B7 = X 2 1 2 0 2
C7+ = X 3 2 0 0 X
C7+/9 = X 3 2 4 3 X
C9 = X 3 5 5 3 3
D/F# = 2 X 0 2 3 2
D4 = X X 0 2 3 3
D7 = X X 0 2 1 2
D7/4 = X X 0 2 1 3
Dm7 = X 5 7 5 6 5
Dsus = X X 0 2 3 2
Em7 = 0 2 2 0 3 0
F#m7/4+ = 2 X 2 2 1 X
F7+ = 1 X 2 2 1 X
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G7+ = 3 X 4 4 3 X
G7/9 = 3 X 3 2 0 X
G9 = 3 X 0 2 0 X
