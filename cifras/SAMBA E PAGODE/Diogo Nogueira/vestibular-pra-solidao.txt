Diogo Nogueira - Vestibular Pra Solidão

                F#7(5+)       Bm9
Gosto mais de você do que de mim
F#m7(5-)     B7(9)                  Em7      Em7/D
Tá pra nascer alguém que goste de sofrer assim
  C#m7(5-)     F#7(5+)       Bm9                  D7/9
A loucura em mim prevalece, é tortura que me abastece
      G7+                          C#m7(5-)          F#7
A ilusão que me faz companhia, é a mesma que um dia não existia

     C#m7(5-)   F#7          Bm9
Gosto mais de você do que de mim
F#m7(5-)  B7(9+)               Em7      Em7/D
Quero voar, dar asas a minha doce ilusão
 C#m7(5-)     F#7(5+)       Bm9                  D7/9
A bebida que me fortalece, é a mesma que me entristece
G7+               C#m7(5-)        F#7
Uma dose a mais, perfeita solução

   B7+      G#7(5+)       C#7(13)    C#7(5+)
Eu vivo a mercê de uma grande paixão
    C#m          F#7      F#m7   B7
Prestei vestibular pra solidão


              Em       Fº
Eu prefiro cantar essa dor                                              Introdução
                Bm7         Bm7/A                               G7+ | F#7(5+) | Bm7 | D13
Assumir a tristeza evidente                                     G7+ | F#7(5+) | Bm9
                C#m7(5-)         F#7
Quem ainda não sofreu de amor
             F#m7(5-)       B7
É dificil quebrar a corrente

              Em       Fº
Eu prefiro cantar essa dor
                      Bm7      Bm7/A
Assumir a tristeza evidente
                C#m7(5-)      F#7
Quem ainda não sofreu de amor...

----------------- Acordes -----------------
B7 = X 2 1 2 0 2
B7(9) = X 2 1 2 2 X
B7(9+) = X 2 1 2 3 X
B7+ = X 2 4 3 4 2
Bm7 = X 2 4 2 3 2
Bm7/A = 5 2 4 2 3 2
Bm9 = X 2 4 6 3 2
C#7(13) = X 4 X 4 6 6
C#7(5+) = X 4 X 4 6 5
C#m = X 4 6 6 5 4
C#m7(5-) = X 4 5 4 5 X
D13 = X 5 4 2 0 X
D7/9 = X 5 4 5 5 X
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
Em7/D = X X 0 0 3 0
F#7 = 2 4 2 3 2 2
F#7(5+) = 2 X 2 3 3 2
F#m7 = 2 X 2 2 2 X
F#m7(5-) = 2 X 2 2 1 X
Fº = X X 3 4 3 4
G#7(5+) = 4 X 4 5 5 4
G7+ = 3 X 4 4 3 X
