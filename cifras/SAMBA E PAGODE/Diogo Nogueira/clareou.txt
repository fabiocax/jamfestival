Diogo Nogueira - Clareou

G7M                Dm7
A vida é pra quem sabe viver
C7M(9)               G7M
Procure aprender a arte
                   Dm7
Pra quando apanhar não se abater
C7M(9)               G7M
Ganhar e perder faz parte

G7M
Levante a cabeça, amigo, a vida não é tão ruim
Dm7
Um dia a gente perde

Mas nem sempre o jogo é assim
C7M(9)
Pra tudo tem um jeito
                                 G7M
E se não teve jeito, ainda não chegou ao fim

G7M
Mantenha a fé na crença, se a ciência não curar

Dm7
Pois se não tem remédio, então remediado está
C7M(9)
Já é um vencedor
                                  G7M
Quem sabe a dor de uma derrota enfrentar
C7M(9)
E a quem Deus prometeu, nunca faltou
                          G7M    G7
Na hora certa o bom Deus dará

C7M(9)
Deus é maior
                               G7M
Maior é Deus e quem tá com Ele nunca está só

O que seria do mundo sem Ele

Dm7
Chega de chorar
F7M                           C7M(9)
Você já sofreu demais, agora chega
                             G7M
Chega de achar que tudo se acabou
                      Dm7
Pode a dor uma noite durar
F7M                          C7M(9)
Mas um novo dia sempre vai raiar
                             G7M
E quando menos esperar, clareou

 G7M       F7M    C7M(9)    G7M
(Clareou ôôôô ôôôô ôôôu)

----------------- Acordes -----------------
C7M(9) = X 3 2 4 3 X
Dm7 = X 5 7 5 6 5
F7M = 1 X 2 2 1 X
G7 = 3 5 3 4 3 3
G7M = 3 X 4 4 3 X
