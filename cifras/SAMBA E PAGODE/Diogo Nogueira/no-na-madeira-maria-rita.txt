Diogo Nogueira - Nó Na Madeira, Maria Rita

    B7      Em
Eu sou é madeira
             A7               D
Em samba de roda já dei muito nó
             B7             Em
Em roda de samba sou considerado
            A7              D7       B7
De chinelo novo brinquei carnaval, carnaval
         Em
Sou é madeira
                A7               D7
Meu peito é do povo, do samba e da gente
            B7               Em
E dou meu recado de coração quente
                A7                  D7      B7
Não ligo à tristeza, não furo, eu sou gente
        Em
Sou é madeira
                A7                 D7
Trabalho é besteira, o negócio é sambar
              B7                Em
Que samba é ciência e com consciência

            A7                  Am    D7
Só ter paciência que eu chego até lá

Refrão 2x:
             G
Sou nó na madeira
           Gm                 D
Lenha na fogueira que já vai pegar
              B7                  Em
Se é fogo que fica ninguém mais apaga
              A7                 D        D7
É a paga da praga que eu vou te rogar (devagar)

( D  B7 )

     Em      A7        D
Por onde andará Maria Rita ?
              B7                 Em
Que andava gingando com laço de fita
          A7                D
Por quem tenho grande admiração
            Em                A7         D
É que eu preciso saber onde anda essa pequena
             B7               Em
Dos olhos redondos, da pele morena
              A7           D    B7
Que é para acalmar meu coração
  Em                A7                   D
Agora, eu chego no samba, meu peito se agita
                 B7             Em
Por que sente a falta de Maria Rita
            A7                D   B7
Cabrocha bonita, corpo escultural
     Em                A7                   D
Por isso, eu preciso saber qual é o seu paradeiro
                 B7                Em
Bem antes que chegue o mês de fevereiro
             A7                  D
Sem ela, pra mim, não vai ter carnaval

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
