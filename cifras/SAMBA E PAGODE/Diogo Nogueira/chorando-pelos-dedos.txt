Diogo Nogueira - Chorando Pelos Dedos

G     C7M/F# B7 E7              A7      D7
Olhe o homem   como ele está chorando
                G     A7        Am              D7
Chorando pelos dedos lágrimas do som de um bandolim
G     C7M/F# B7 E7                 A4 A7
Chore homem     para espantar seus medos
C7+ Cm7 G    D/F#  E7        A7   D7  G   C9/D
Que lá  vem         um tempo bom de   ir
G    F#7            G6         G7(13) G7(13-)
Ir a caminhar sob o luar de violões
C7+            E7              A7      C/E         D7
Ir a cantar, a dedilhar, pra festejar os tempos bons
       G           D/F#    Em9               C
Poder viver, sorrir amar, curtir carpir  pra prosseguir
Cm7         Bm7       D7
Mesmo assim chorar ...
G     C7/F# B7 E7                 A4 A7
Chore homem    para espantar seus medos
C7+ Cm7 G   D/F# E7        A7   D7 G
Que lá  vem      um tempo bom de ir

----------------- Acordes -----------------
A4 = X 0 2 2 3 0
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
C7+ = X 3 2 0 0 X
C9/D = X 5 5 5 3 3
Cm7 = X 3 5 3 4 3
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
Em9 = 0 2 4 0 0 0
F#7 = 2 4 2 3 2 2
G = 3 2 0 0 0 3
G6 = 3 X 2 4 3 X
G7(13) = 3 X 3 4 5 X
G7(13-) = 3 X 3 4 4 3
