Diogo Nogueira - A Vitória Demora Mas Vem

Into:  C# A#7 D#m G#7 C# A#7 D#m G#7 C# G#7

      C#
E lá vou eu
F#                C#  F#
saindo para o batente
C#             A#7      D#m  A#7
antes mesmo do galo cantar
D#m                  G#7
sempre correndo na frente,
                       C#       F#
espero a minha estrela brilhar...
     C#                    G#7    C# Fm C#
não sou de atrasar nem tão pouco puxar
     A#7      D#m  A#7 D#m
o tapete de niguém,
     F#                   Fm
nessa vida nada é por um acaso,
     A#7            A#m6
pra quem nasceu predestinado
 D#m            G#7    C#
a vitória demora mais vem...


             A#7      G#7                C#6/9
a vitória demora mais vem... a vitória demora mais vem
             A#7      G#7                    C#
a vitória demora mais vem... a vitória demora mais vem

   F#                C#
guerreiro não foge da luta
   F#                C#6/9
eu tô sempre na disputa,
 C#             Dº     D#m  A#7
em prol de uma manhã melhor
 D#m
pego firme na labuta,
                   G#7
malandro é quem escuta bom conselho,
D#m       G#7    C#    D#m G#7 C#
pra não ficar na pior...

    F#                C#
eu vou compondo minha história
  F#                C#
guardo em minha memória,
   G#m        C#7    F#   F# A#7 D#m
quem sempre me fez o bem
              F#m           Fm
aprendi pra ensinar o ensinamento
              A#7         A#m6
que tudo na vida tem seu tempo, tem
 D#m            G#7    C#
a vitória demora mais vem...

----------------- Acordes -----------------
A#7 = X 1 3 1 3 1
A#m6 = 6 X 5 6 6 X
C# = X 4 6 6 6 4
C#6/9 = X 4 3 3 4 4
C#7 = X 4 3 4 2 X
D#m = X X 1 3 4 2
Dº = X X 0 1 0 1
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
Fm = 1 3 3 1 1 1
G#7 = 4 6 4 5 4 4
G#m = 4 6 6 4 4 4
