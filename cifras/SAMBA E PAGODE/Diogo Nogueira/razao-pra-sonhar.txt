Diogo Nogueira - Razão Pra Sonhar

Em7/9 A7
Um dia eu quis juntar o sol e a lua
Em7/9                         A7
Quem sabe assim o mundo irá parar de vez
Am7           D7/9         G7+
Pra gente se amar e ser feliz
               D/F#        Em7
De repente se tornar uma raiz
               Em7/9      C7+        B7
Foi Deus quem quis assim       Você dentro de mim
Em7/9        A7
É tão dificil contornar a tempestade
Em7/9                        A7
Ver além do horizonte o nó que se desfez
Am7             D7/9        G7+
E quando o céu fechado clarear
        D/F#         Em7
Vamos embarcar na ilusão
               Em7/9       C7+   B7
Que a vida só é feita de razão       pra sonhar


Em7/9    A7      C7+             B7
Um jangadeiro avisou,  o mar do amor é agitado
Em7/9     A7      C7+        B7
E no balanço da maré, eu naveguei para o seu lado
Em7/9    A7             C7+            B7
Manhã de sol, um lindo amor,  na cheia do meu coração
Em7/9       A7       C7+           B7
O sentimento transbordou, e fez do amor uma canção

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
C7+ = X 3 2 0 0 X
D/F# = 2 X 0 2 3 2
D7/9 = X 5 4 5 5 X
Em7 = 0 2 2 0 3 0
Em7/9 = X 7 5 7 7 X
G7+ = 3 X 4 4 3 X
