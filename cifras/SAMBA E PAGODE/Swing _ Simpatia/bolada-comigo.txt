Swing & Simpatia - Bolada Comigo

Solo 1: (toque as 2 cordas juntas: 25/17 (6x) 23/14 (6x) 25/16 (12x)

Solo 2: (este solo é feito junto com o 1º): 42 46 34 23 22 32 42 25 25 25 25 27 27 27 25 25
                                            42 46 34 23 22 32 42 25 23 22 20 25 23 22 20 20
                                            20 22 23 24 25

A7+           D                                E      D   E7
Ta bolada comigo porque eu não quero ser seu exclusivo
A7+              D                        E      D   E7
Ta bolada comigo e chama o meu coração de bandido  (2x)
       A7+                                      E7
Eu te falei que não queria assumir um compromisso
                      Fº                 F#m7
Mais de mil vezes te mandei parar com isso
               D                     E       D
E agora vem querer cobrar uma paixão
    A7+                                    E7
Eu te avisei só to ficando eu não sou seu namorado
         Fº                      F#m7
Quem te falou que estou apaixonado
        D                A7+         E7
Você pensou que eu estava em suas mãos

     A7+
Lêlêlê
                          E7          D
Toda se querendo pra ganhar meu coração
 A7+
Lêlêlê
                                E7   D
Você faz de tudo pra chamar minha atenção
 A7+
Lêlêlê
                                E7   D
Eu até confesso você faz um bem pra mim
 A7+
Lêlêlê
                             E7   D
Mas um bambolê no dedo eu não to afim.

----------------- Acordes -----------------
A7+ = X 0 2 1 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m7 = 2 X 2 2 2 X
Fº = X X 3 4 3 4
