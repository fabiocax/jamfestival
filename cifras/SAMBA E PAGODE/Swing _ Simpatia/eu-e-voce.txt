Swing & Simpatia - Eu e você

Swing & Simpatia - Eu E Você

  Am7
Derê, derê, derê, derê, derê, derê, derê, derê
  Dm7                                   E7
Derê, derê, derê, derê, derê, derê, derererê
  Am7
Derê, derê, derê, derê, derê, derê, derê, derê
  Dm7                                   E7
Derê, derê, derê, derê, derê, derê, derererê

  Am7
Quero novamente aquela chama
  G7                F6/9
Que me fez sonhar......enlouquecer
    Bm5-/7    E7
Me fez delirar
  Am7
Quero toda noite teu corpo
  G7                F6/9
Juntinho ao meu......gritar pra Deus

    Bm5-/7   E7        Am7
Como é bom te a......mar

  G7                     Am7
Eternizar um beijo.......(ô  ô)
  G7                    Am7
Todo nosso desejo......(ô  ô)
  G7                     Am7
Eternizar um beijo.......(ô  ô)
  G7               E7/4 E7 Am7
Todo nosso desejo, e amar

                  Bm5-/7      E7      Am7
E aí......só vai dar..........eu e você
                  Bm5-/7      E7      Am7
Toda noite alongar.........nosso prazer
                  Bm5-/7      E7     Am7
E aí......só vai dar..........eu e você
                  Bm5-/7      E7      Am7
Toda noite alongar.........nosso prazer

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Bm5-/7 = X 2 3 2 3 X
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
E7/4 = 0 2 0 2 0 X
F6/9 = 1 0 0 0 X X
G7 = 3 5 3 4 3 3
