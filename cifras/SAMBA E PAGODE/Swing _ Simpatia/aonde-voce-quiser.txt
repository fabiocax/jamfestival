Swing & Simpatia - Aonde você Quizer

Intro: A#m D#m


A#m                       D#m
Quando eu te vi fechei os olhos
F#m                   C#7+
Tive a certeza que você
A#m                  D#m
Ia ser aquela que um dia
F#m                  C#7+
Ia me fazer muito feliz

A#m                        D#m
Tudo o que eu queria nessa hora
F#m                      C#7+
Era que você me desse a mão
A#m                     D#m
Me beijasse bem devagarinho
F#m                     C#7+
E que fosse a minha mulher


F#          Fm           A#m
Nada nessa vida é por acaso
F#          Fm           A#m
Nada nessa vida é por querer
F#          Fm           A#m
Tudo nessa vida tem seu preço
        D#m7
E se você quiser
           F#     G#
Tem que pagar pra ver

        Fm
Aonde você quiser
   A#m
Eu vou
       D#m
Aonde você quiser
       F#  G#
Eu vou ficar  (2X)

Repete Intro

----------------- Acordes -----------------
A#m = X 1 3 3 2 1
C#7+ = X 4 6 5 6 4
D#m = X X 1 3 4 2
D#m7 = X X 1 3 2 2
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
Fm = 1 3 3 1 1 1
G# = 4 3 1 1 1 4
