Revelação - Esqueci de Te Esquecer

[Intro] Am7/9  Dm  Em  F7M  Am7/9  Dm7  Em7  Dm

Am7/9                    Em7
  Quando acabou nosso romance o meu mundo desabou
Am7/9        Em                    D
A solidão me abrigou sofri sem você
              G                              D
Sei que não quis dizer adeus eu vi no seu olhar
                F/G                B°        E7/9/11
Está infeliz e tenta disfarçar só faz se magoar
Am7/9                Em7
  Força meu bem não deixe a chama da paixão se apagar
Am7/9               Em                  D
  Pro nosso bem esqueça tudo que passou
               G                        D
Eu sei que não é o fim seu corpo me chama
              F/G           B°          E7/9/11
Escute a voz do seu coração a voz da razão

           A7+     Em           A7      D     E7/9/11
Não sei viver sem teu amor sem teu abraço só você

        A7+       Em      A7       D           G7/4 G7
Me dá prazer te quero do meu lado custe o que custar
        C6/9      C7              F7M
Eu amo você sinceramente eu esqueci de te esquecer
       C6/9        F7M               Bm7/5-          E7/9/11
Chega de sofrer orgulho não nos leva a nada pode crer
           A7+     Em           A7      D     E7/9/11
Não sei viver sem teu amor sem teu abraço só você
        A7+       Em      A7       D           G7/4 G7
Me dá prazer te quero do meu lado custe o que custar
        C6/9      C7              F7M
Eu amo você sinceramente eu esqueci de te esquecer
       C6/9        F7M               Bm7/5-          E7/9/11
Chega de sofrer orgulho não nos leva a nada pode crer

( Am7/9  Dm7  Em7  Dm )

Am7/9                    Em7
  Quando acabou nosso romance o meu mundo desabou
Am7/9        Em                    D
A solidão me abrigou sofri sem você
              G                              D
Sei que não quis dizer adeus eu vi no seu olhar
                F/G                B°        E7/9/11
Está infeliz e tenta disfarçar só faz se magoar
Am7/9                Em7
  Força meu bem não deixe a chama da paixão se apagar
Am7/9               Em                  D
  Pro nosso bem esqueça tudo que passou
               G                        D
Eu sei que não é o fim seu corpo me chama
              F/G           B°          E7/9/11
Escute a voz do seu coração a voz da razão

           A7+     Em           A7      D     E7/9/11
Não sei viver sem teu amor sem teu abraço só você
        A7+       Em      A7       D           G7/4 G7
Me dá prazer te quero do meu lado custe o que custar
        C6/9      C7              F7M
Eu amo você sinceramente eu esqueci de te esquecer
       C6/9        F7M               Bm7/5-          E7/9/11
Chega de sofrer orgulho não nos leva a nada pode crer
           A7+     Em           A7      D     E7/9/11
Não sei viver sem teu amor sem teu abraço só você
        A7+       Em      A7       D           G7/4 G7
Me dá prazer te quero do meu lado custe o que custar
        C6/9      C7              F7M
Eu amo você sinceramente eu esqueci de te esquecer
       C6/9        F7M               Bm7/5-          E7/9/11
Chega de sofrer orgulho não nos leva a nada pode crer

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
Am7/9 = X X 7 5 8 7
Bm7/5- = X 2 3 2 3 X
B° = X 2 3 1 3 1
C6/9 = X 3 2 2 3 3
C7 = X 3 2 3 1 X
D = X X 0 2 3 2
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
E7/9/11 = X X 2 2 3 2
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F/G = 3 X 3 2 1 X
F7M = 1 X 2 2 1 X
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
G7/4 = 3 5 3 5 3 X
