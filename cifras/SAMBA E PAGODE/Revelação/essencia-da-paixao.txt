Revelação - Essência da Paixão

[Intro] Cm  F7(9)  Cm  G7

[Primeira Parte]

Cm                     F7(9)   Fm
   Te quero demais minha flor
     Bb7              Eb   G7
Não deixe esse amor acabar
Cm                    F7(9)  Fm
   Te juro ninguém me amou
      Bb7             Eb  Eb5+  Bbm
Do jeito que sabe me amar

[Segunda Parte]

 Eb7             Ab7M   Ab6
Ainda sinto teu perfume
                 Gm7   C7
Por essência da paixão
                 Fm7           Gm7      Ab7M
É razão do meu ciúme a saudade    me invade

   Bb7            Eb  Eb5+  Bbm7
Aumentando a solidão
     Eb7           Ab7M    Ab6
Só quero viver ao teu lado
                 Gm7     C7
Sentindo a mesma emoção
                    Fm7         Gm7    Ab7M
Te amando e sendo amado provando   pecado
      Bb7         Eb   G7
Renascendo de paixão

[Refrão]

          Cm
Vem meu amor
                          F7(9)
Que vontade de te dar um beijo
                    Ab7M
Me perder no seu desejo
    Bb7        Eb    G7
No seu corpo acordar
          Cm
Vem meu amor
                  F7(9)
Tô carente tão sozinho
                 Ab7M
Precisando de carinho
 Bb7           Eb   G7
Vem vamos recomeçar

          Cm
Vem meu amor
                          F7(9)
Que vontade de te dar um beijo
                    Ab7M
Me perder no seu desejo
    Bb7        Eb    G7
No seu corpo acordar
          Cm
Vem meu amor
                  F7(9)
Tô carente tão sozinho
                 Ab7M
Precisando de carinho
 Bb7           Eb   Eb5+  Bbm
Vem vamos recomeçar

[Segunda Parte]

 Eb7             Ab7M   Ab6
Ainda sinto teu perfume
                 Gm7   C7
Por essência da paixão
                 Fm7           Gm7      Ab7M
É razão do meu ciúme a saudade    me invade
   Bb7            Eb  Eb5+  Bbm7
Aumentando a solidão
     Eb7           Ab7M    Ab6
Só quero viver ao teu lado
                 Gm7     C7
Sentindo a mesma emoção
                    Fm7         Gm7    Ab7M
Te amando e sendo amado provando   pecado
      Bb7         Eb   G7
Renascendo de paixão

[Refrão]

          Cm
Vem meu amor
                          F7(9)
Que vontade de te dar um beijo
                    Ab7M
Me perder no seu desejo
    Bb7        Eb    G7
No seu corpo acordar
          Cm
Vem meu amor
                  F7(9)
Tô carente tão sozinho
                 Ab7M
Precisando de carinho
 Bb7           Eb   G7
Vem vamos recomeçar

          Cm
Vem meu amor
                          F7(9)
Que vontade de te dar um beijo
                    Ab7M
Me perder no seu desejo
    Bb7        Eb    G7
No seu corpo acordar
          Cm
Vem meu amor
                  F7(9)
Tô carente tão sozinho
                 Ab7M
Precisando de carinho
 Bb7           Eb   G7
Vem vamos recomeçar

          Cm
Vem meu amor
                          F7(9)
Que vontade de te dar um beijo
                    Ab7M
Me perder no seu desejo
    Bb7        Eb    G7
No seu corpo acordar
          Cm
Vem meu amor
                  F7(9)
Tô carente tão sozinho
                 Ab7M
Precisando de carinho
 Bb7           Eb   G7  Cm
Vem vamos recomeçar

----------------- Acordes -----------------
Ab6 = 4 X 3 5 4 X
Ab7M = 4 X 5 5 4 X
Bb7 = X 1 3 1 3 1
Bbm = X 1 3 3 2 1
Bbm7 = X 1 3 1 2 1
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
Eb = X 6 5 3 4 3
Eb5+ = X 6 5 4 4 X
Eb7 = X 6 5 6 4 X
F7(9) = X X 3 2 4 3
Fm = 1 3 3 1 1 1
Fm7 = 1 X 1 1 1 X
G7 = 3 5 3 4 3 3
Gm7 = 3 X 3 3 3 X
