Revelação - Nunca Mais

(intro) F7+     Dm7     Gm7      C7/9

 F         Bb           F               C
Nunca mais quero ver você triste assim,
            Bb           F    C7
Quando a gente briga é ruim.
F       Bb           F              C
É melhor a gente tentar se entender
                      Bb           F
Que é para o nosso amor não se perder.(2x)

A7                         Dm
Magoei sem motivo o seu coração,
E7                   Am7
Fui além da maldade peço perdão.
G7                         C
Estou aqui na esperança de recomeçar,
G7                          Gm7      C7/9
Cheio de amor, louco pra te beijar te abraçar.

Bb                        F
Quando a saudade bate no peito é assim,

      C              Bb        F         F7
Dói demais, tira a paz deixa a gente incapaz de viver.
Bb                             F
Errar é humano e ninguém é perfeito meu bem,
            C                Bb     C      F F7
E não tem jeito eu não posso viver sem você.(2x)

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am7 = X 0 2 0 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
C7/9 = X 3 2 3 3 X
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
F7+ = 1 X 2 2 1 X
G7 = 3 5 3 4 3 3
Gm7 = 3 X 3 3 3 X
