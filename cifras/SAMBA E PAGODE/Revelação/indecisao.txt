Revelação - Indecisão

(intro)  G  Dm G7 C  Am Em  D7

(solo) 34 23 15 17 13  12 10 15 17 12  10 21 12 10 12
           10 20 21 20 32 G Dm  12 13 12

Am                         Em
Tô apaixonado e não sei se te mereço
Am                             Em
Eu sei que na vida quase tudo tem um preço
G                              D
Eu nunca pensei um dia me ver dividido
F                                D      E7
Fui surpreendido e dominado por esta paixão
Am                                              Em
Sei que estou errado e não consigo encontrar um jeito
Am                                      Em
Se eu fico do teu lado  ou continuo a dividir meu peito
G                                  D
Essa indecisão, confesso esta me enlouquecendo
F                              D     D7
Sinto um vazio aqui dentro do meu coração

G              Dm      G7          C
Isso não se faz, não  agüento mais
      A7         D     D7
Esta indecisão só me faz chorar
G                Dm  G7       C
perdi minha paz ao te conhecer
      A7       D    D7
Eu vou decidir ela ou você

(solo)  15  17 19 15   15 17 19 17
(volta ao início)

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
