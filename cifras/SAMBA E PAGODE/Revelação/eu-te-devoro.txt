Revelação - Eu Te Devoro

D7M(9)                               Bm7(9/11)
      Teus  sinais me confundem da cabeça     aos pés
                       G7M   G7M/B  A7(4/9)
mas por dentro eu te devoro

D7M(9)                           Bm7(9/11)
      Teu olhar não me diz exato quem      tu és
                    G7M   G7M/B       A7(4/9)
mesmo assim eu te devoro, te    devoraria

  D7M(9)                             Bm7(9/11)
A qualquer preço porque te ignoro ou te       conheço
                       G7M      G7M/B  A7(4/9)
quando chove ou quando faz frio

D7M(9)                          Bm7(9/11)
      Noutro plano te devoraria tal       Caetano
              G7M
a Leonardo di Caprio

C7(9)     A7(4/9)
É    um milagre


B6(9)                               G7M
Tudo que Deus criou pensando em     você
      B6(9)                       A/G
Fez a Via-Láctea, fez os dinossauros

       B6(9)                    G7M
Sem pensar em nada fez a minha vida
F#m7        G7M
E    te deu

       B6(9)                    G7M
Sem contar os dias que me faz morrer
      B6(9)                    A/G
Sem saber de ti jogado à solidão

       B6(9)                            G7M
Mas se quer saber se eu quero outra vida...
      F#m7    G7M
Não, não

         ( Repete tudo )

D7M(9)                         Bm7(9/11)
       Eu quero mesmo é viver, pra       esperar, esperar
    G7M
devorar você

----------------- Acordes -----------------
A/G = 3 X 2 2 2 X
A7(4/9) = 5 X 5 4 3 X
B6(9) = X 2 1 1 2 2
Bm7(9/11) = X 2 0 2 2 0
C7(9) = X 3 2 3 3 X
D7M(9) = X 5 4 6 5 X
F#m7 = 2 X 2 2 2 X
G7M = 3 X 4 4 3 X
G7M/B = 7 X 5 7 7 X
