Revelação - Cavalheiro Sonhador

G7+
Assim Assim

Assim
                Gm6                  F#m
Se você cuidar de mim, vou cuidando de você
                   F°               Em
Faz da minha a sua vida e da sua meu viver
    F#m            G7+  A7              D6(9) Am7 D7
Ninguém pode ser feliz se não fizer por merecer

A7/4                                Bm
Te encontrei... E vi que a vida não perdeu o chão
                    G7+
Você me fez voltar ao natural
                  Bm
Um cavalheiro sonhador
A7/4                         Bm
Me entreguei... Cai na teia do teu coração
                       G7+
Um super homem preso em suas mãos

                Bm
Eterno escravo do amor

A7/4
Batemos de frente, um clima envolvente
                           A7/4 A#7/4 B7/4
Na mesma corrente fugindo da dor
B7/4
Passado, presente, futuro ausente
                           B7/4 A#7/4 A7/4
Plantando a semente que deus semeou
A7/4
E daqui pra frente o fogo da gente
                         Bm      ( D6(9) Am7 D7 )
Vai queimando lenha a todo vapor

Assim
                Gm6                  F#m
Se você cuidar de mim, vou cuidando de você
                   F°               Em
Faz da minha a sua vida e da sua meu viver
    F#m            G7+  A7              D6(9) Am7 D7
Ninguém pode ser feliz se não fizer por merecer

----------------- Acordes -----------------
A#7/4 = X 1 3 1 4 1
A7 = X 0 2 0 2 0
A7/4 = X 0 2 0 3 0
Am7 = X 0 2 0 1 0
B7/4 = X 2 4 2 5 2
Bm = X 2 4 4 3 2
D6(9) = X 5 4 2 0 0
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
F° = X X 3 4 3 4
G7+ = 3 X 4 4 3 X
Gm6 = 3 X 2 3 3 X
