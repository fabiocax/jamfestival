Revelação - Tá Maluca

(intro)
Gm G7 Cm6 D7

Gm             G7                     Cm6
Tá maluca, tá pirada, tá querendo me pirar
        D7
Tá procurando sarna pra coçar
Gm                  G7                 Cm6
Eu faço tudo do teu jeito pra poder te agradar
     D7
Mas 100% eu não vou te dar

G7+         D                      Bm7/5-
10% é dos amigos, do meu samba, meu lazer
        G7
É muito fácil, muito simples de entender
C                                                    D7
Tem trabalho, casa, carro, que eu também tenho que cuidar
D7
Pensa que é mole administrar?


G7+              D                      Bm7/5-
Leva filho, pega filho, faz a feira, vai pagar
        G7
Tira do prego tudo que tem tirar
C                                           D7
Eu fico rindo quando fala "ainda vou largar você"
D7                                  Gm
É bem mais fácil do contrário acontecer

G7                 Cm6 D7
Tá ligada na pegada oh oh
D7                 Gm
Eu não deixo a desejar
G7                     Cm6
Vai procurar, não vai achar
                     D7                           Gm
Um cara que te dê um terço do que eu tenho pra te dar

----------------- Acordes -----------------
C = X 3 2 0 1 0
Cm6 = X 3 X 2 4 3
D = X X 0 2 3 2
D7 = X X 0 2 1 2
G7 = 3 5 3 4 3 3
G7+ = 3 X 4 4 3 X
Gm = 3 5 5 3 3 3
