Revelação - Cabelo Pixaim

                  F#m7     F#7
Quero teu amor criou.....la....(crioula!)
 G                  Bm5-/7  E7
...Por favor não seja to....la
 Am                      F                  Am
...Pra que serve então amar....(lá lauê lalaiá!)
                       F
Se você não sabe o que é se dar
       D7                 G
Pra alguém da cor....pois é!
                      C7                     F
Olha, eu sou da pele pre....ta....(graças a Deus!)
      F7           Bb       D7        G
Bem pior pra se aturar....(pra se aturar!)
                    C7   F
Mas se me der na vene...ta
       F7          Bb                Am  D7
Quero ver alguém amar....mais do que eu
 G             Bb7             A7
...É do cabelo....do cabelo pixaim
              Ab7              G
Eu quero você....ligada só em mim

 G             Bb7             A7
...É do cabelo....do cabelo pixaim
              Ab7              G
Eu quero você....ligada só em mim

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Ab7 = 4 6 4 5 4 4
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bb7 = X 1 3 1 3 1
Bm5-/7 = X 2 3 2 3 X
C7 = X 3 2 3 1 X
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
F#7 = 2 4 2 3 2 2
F#m7 = 2 X 2 2 2 X
F7 = 1 3 1 2 1 1
G = 3 2 0 0 0 3
