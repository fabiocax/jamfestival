Revelação - Aquela Jura

D          Am
Deixa estar
B7            Em
O nosso caso está desfeito
Gm       C7  F#m      B7
Não posso mais chorar
E7                  Em
Sei que ainda te amo
                A7
Tenho que me conformar
D         Am  B7
Vou tentar
                Em
Esquecer tudo o que vivemos
Gm           C7
Que passamos
F#m        B7
Foi demais
    E7
Mas tudo é passageiro
Em            A7
Tenho que superar

Dm                A7      Gm
Vou lutar pra te esquecer
C7               Dm
Apesar de amar você
E7          Em
Dói demais
              A7
Ter que te perder
Dm          A7       Gm
Vou sofrer sem você
C7                 Dm
Sinto que o meu coração
E7    Em  A7   Am  D7
Chora o meu sofrer
        G
Aquela jura
          A7
Tanta ternura
       Am         B7
Tudo em vão, acabou
            Em
Não tem mais jeito
F#m         G  A7  D  Bm
É o fim do nosso amor
Am      D7
Nosso amor
        G
Aquela jura
          A7
Tanta ternura
       Am         B7
Tudo em vão, acabou
            Em
Não tem mais jeito
F#m         Gm  A7  Dm  A7
É o fim do nosso amor

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C7 = X 3 2 3 1 X
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
