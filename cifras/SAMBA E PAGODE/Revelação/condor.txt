Revelação - Condor

G7+         Bm7     Dm
Deixa eu....larga eu
     G7            C7M
Meu amor, eu não agüento mais
    Cm6
Me deixe em paz
 Bm7          E7
Te peço por favor
 Am7              D7            G7M  (D7)
Chega de tanto sofrer, de tanta dor
F#m7(b5)  B7(#5)    Em7/9
Lá.......lá......iá

     F#m7(b5)    B5+7   Em7/9
Vou sair.......por.....aí
       F#m7(b5)   B5+7    Em7/9
Vou fazer........um.....auê
    F#m7(b5)   B7(#5)    Em7  Ebm  Dm   Db7/9
Coração.......tá....durão
 C7+     Cm6                  Bm7          E7
Che....ga.....sentimento paranóico é demais

                   A7/9                 Am7/9
Vou buscar a minha paz, liberdade pra viver
     D7            Dm              G7
Faço tudo pra esquecer, um grande amor !
C7+      Cm6   Bm7      E7
   Leleleô........leleleô
     Am7               D7             Dm   G7
Eu quero voar bem mais alto que um condor
C7+      Cm6   Bm7      E7
   Leleleô........leleleô
     Am7               D7             G7M    D7/9-  D7
Eu quero voar bem mais alto que um condor

----------------- Acordes -----------------
A7/9 = 5 X 5 4 2 X
Am7 = X 0 2 0 1 0
Am7/9 = X X 7 5 8 7
B5+7 = X 2 X 2 4 3
B7(#5) = X 2 X 2 4 3
Bm7 = X 2 4 2 3 2
C7+ = X 3 2 0 0 X
C7M = X 3 2 0 0 X
Cm6 = X 3 X 2 4 3
D7 = X X 0 2 1 2
D7/9- = X 5 4 5 4 X
Db7/9 = X 4 3 4 4 X
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
Ebm = X X 1 3 4 2
Em7 = 0 2 2 0 3 0
Em7/9 = X 7 5 7 7 X
F#m7(b5) = 2 X 2 2 1 X
G7 = 3 5 3 4 3 3
G7+ = 3 X 4 4 3 X
G7M = 3 X 4 4 3 X
