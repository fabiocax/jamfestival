Revelação - Ô Queiroz

Intro: Em C B7 Em G7 C Bm Am Bm Em D7 B7 Em

                            G7              C6/9
Tem homem que não abre mão de uma mulher gordinha
                        Eb°               Em
Tem homem de perder a linha com a mulher sarada
                       G7          C6/9
Tem homem que só acha graça nas novinhas
                      Eb°                 Em
E outros que só se engraçam com as casadas

                      G7               C6/9
Tem homem que se apaixona por mulher carente
                       Eb°               Em
E outros que ficam carentes sem as piriguetes
                  G7          C6/9
E tem maluco só tentando dá o pente
                     Eb°             Em
E tem parceiro de trazer mulher pra gente

         G7          C6/9
Ô, Queiroz! Ô, Queiroz!

Eb°                  Bm7/5-
Traz essa mulher pra nós!
        Em           C6/9
Ô, Queiroz! Ô, Queiroz!
 Eb°                   Em
Traz essa mulher pra nós!

         G7          C6/9
Ô, Queiroz! Ô, Queiroz!
Eb°                  Bm7/5-
Traz essa mulher pra nós!
        Em           C6/9
Ô, Queiroz! Ô, Queiroz!
 Ebº                   Em
Traz essa mulher pra nós!

                G7                C6/9
Ela tem os atributos que a gente gosta
               Eb°                  Bm7/5-
Ela tem os apetrechos que a gente quer
                Em          C6/9
Mas você sempre foi sangue bom
        Eb°              Bm7/5-
Nunca deixa o parceiro a pé

              Em
Tem branca, morena
             C6/9
Tem loira, mulata
               Eb°
Você  sempre  porta
           Bm7/5-
Uma mina bonita
          Em     C6/9
Além de pagar a conta
          Eb°    Em
Tú é de botar na fita

         G7          C6/9
Ô, Queiroz! Ô, Queiroz!
Eb°                  Bm7/5-
Traz essa mulher pra nós!
        Em           C6/9
Ô, Queiroz! Ô, Queiroz!
 Eb°                   Em
Traz essa mulher pra nós!

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C6/9 = X 3 2 2 3 3
D7 = X X 0 2 1 2
Eb° = X X 1 2 1 2
Ebº = X X 1 2 1 2
Em = 0 2 2 0 0 0
G7 = 3 5 3 4 3 3
