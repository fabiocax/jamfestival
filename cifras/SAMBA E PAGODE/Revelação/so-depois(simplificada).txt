Revelação - Só Depois

Intro: Gm  Dm  Gm  D

Gm     Dm               Gm
Só depois que tudo terminou
      Dm    Bb        Eb
Entendi que não era amor
             Ab             Am
E tudo que fizemos um só momento
   D        D      Gm
Deixou de existir
      Dm               Gm
Entendi que o fim foi o melhor pra mim
    Dm    Bb      Eb
Só assim pude enxergar
                 Ab
O que estava tão perto
          Am         Dm      G
Eu feito um tolo nem percebi

Cm            Bbm              Eb     Ab      Ab
Que na vida o hoje tem que aproveitar por que

      Dm           G            Cm
Eu não sei se o amanha há de chegar
                        Bbm
Se me desse ao menos um sinal
     Eb                    Ab                 Ab
Um piscar de olhos, um sorriso, eu posso garantir
Dm                    G
Que nosso caso ia ser fatal

Cm ? Bm - Bbm              Eb     Ab      Ab
Que na vida o hoje tem que aproveitar por que
      Dm           G            Cm
Eu não sei se o amanha há de chegar
                        Bbm
Se me desse ao menos um sinal
     Eb                    Ab                 Ab
Um piscar de olhos, um sorriso, eu posso garantir
Dm                    G       (Cm   D7 e repete a música)

Que nosso caso ia ser fatal

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bbm = X 1 3 3 2 1
Bm = X 2 4 4 3 2
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
