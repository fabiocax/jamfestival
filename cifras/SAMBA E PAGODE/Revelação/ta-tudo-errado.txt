Revelação - Tá Tudo Errado

Introdução:  Dm7   G7   C#m7/5- D/C  Bm7  Bb7/13   Am7   D7   G7+

 G7+         Cm7                    G7+
Tá tudo errado, você pra lá e eu aqui
         Cm7     F7/9             Bb7+
Foi combinado, a gente não podia desistir
                Fm7        Bb7             Eb6/9
Minhas noites são vazias, nada mais me dá prazer
   Ab7+         Am7/5-            D7          G7+
Meu amor, como eu queria estar junto de você
               Dm7/9
Meu céu perdeu o luar
    G7/13          C7+
O sol já não tem mais calor
                   Cm7       F7           Bm7
Tento, não sei me livrar da maldade dessa dor
   E7            C#m7/5-
Que só quer me atormentar
   C7+             Bm7
Maltratando o meu viver
  E7/5+            A7/9            D7   G7+
Só você pra amenizar o meu sofrer

Dm7  G7 C#m7/5-  D/C     Bm7
Teu olhar, luz do alvorecer
Bbº          Am7        D7      Dm7
Onde eu vou te encontrar, me perder
G7    C#m7/5-  C7+  Bm7       Bbº
Um lugar muito além do prazer
           Am7       D7/9     Dm7
Onde eu quero acordar com você
Dm7  G7 C#m7/5-  D/C     Bm7
Teu olhar, luz do alvorecer
Bbº          Am7        D7      Dm7
Onde eu vou te encontrar, me perder
G7    C#m7/5-  C7+  Bm7       Bbº
Um lugar muito além do prazer
           Am7       D7/9     G7+
Onde eu quero acordar com você.

  G7+         Cm7           G7+
Tá tudo errado, você pra lá e eu aqui

----------------- Acordes -----------------
A7/9 = 5 X 5 4 2 X
Ab7+ = 4 X 5 5 4 X
Am7 = X 0 2 0 1 0
Am7/5- = 5 X 5 5 4 X
Bb7 = X 1 3 1 3 1
Bb7+ = X 1 3 2 3 1
Bb7/13 = 6 X 6 7 8 X
Bbº = X 1 2 0 2 0
Bm7 = X 2 4 2 3 2
C#m7/5- = X 4 5 4 5 X
C7+ = X 3 2 0 0 X
Cm7 = X 3 5 3 4 3
D/C = X 3 X 2 3 2
D7 = X X 0 2 1 2
D7/9 = X 5 4 5 5 X
Dm7 = X 5 7 5 6 5
Dm7/9 = X 5 3 5 5 X
E7 = 0 2 2 1 3 0
E7/5+ = 0 X 0 1 1 0
Eb6/9 = X X 1 0 1 1
F7 = 1 3 1 2 1 1
F7/9 = X X 3 2 4 3
Fm7 = 1 X 1 1 1 X
G7 = 3 5 3 4 3 3
G7+ = 3 X 4 4 3 X
G7/13 = 3 X 3 4 5 X
