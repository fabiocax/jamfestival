Revelação - Ela Só Vai Se For de Camarote

Intro:
         Bb                   A7            Dm            F7
13 13 10 13 13 10 10 13 13 10 21 - 32 21 10 13 12 11 10 - 17 13 15 10
         Bb                   A7            Dm
13 13 10 13 13 10 10 13 13 10 21 - 32 21 13 23


F7   Bb  A7  Dm  Bb  F7  Bb                  A7                 Dm
 Ei, ei, ei, ei, ei, ei, ei, ela é gata de elite só curte área VIP(2x)

Dm          G7                  Dm
Rainha da balada, adora uma batucada
                F7/9                Bb7+ ^ Bb6
É sempre bem chegada em qualquer pagode
                Em7/5b              A7                    Dm
Bem em cima da média, um tremendo filé, e só chega quem pode
                G7                 Dm               F7/9                Bb7+ ^ Bb6
Tá sempre perfumada, de roupa importada, vagabundo azarando e ela nem ai
                 Em7/5b                A7                   Dm           F7
Se liga ai segurança trás a pulseirinha que ela quer subir.  (Ela só vai)


       Bb7+                     A7                Dm F7/9
Ela só vai, ela só vai, ela só vai se for de camarote
   Bb7+                     A7                Dm F7/9
Ela só vai, ela só vai, ela só vai se for de camarote
   Bb7+                     A7                Dm F7/9
Ela só vai, ela só vai, ela só vai se for de camarote
   Bb7+                     A7
Ela só vai, ela só vai, ela só vai se for de camarote

  Dm        F7/9         Bb7+                 A7                Dm
Ela rouba a cena, ela é turbinada, ela é saliente, toda marombada,
          F7/9           Bb7+                  A7                   Dm
ela é atraente toda desenhada, ela é gata de elite e só curte área vip.

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bb = X 1 3 3 3 1
Bb6 = X 1 3 0 3 X
Bb7+ = X 1 3 2 3 1
Dm = X X 0 2 3 1
Em7/5b = X X 2 3 3 3
F7 = 1 3 1 2 1 1
F7/9 = X X 3 2 4 3
G7 = 3 5 3 4 3 3
