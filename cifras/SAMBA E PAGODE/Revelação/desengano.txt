Revelação - Desengano

Introo: Gm  Cm  D7  Cm  D7/11 D7  Gm

Solo: 10 21 33 21 10  10 15 17 18 17 15 10 13 11
      10 11 21  32 11 10 21 33 32 33 21 10 21 33 32 30 40 12

  Gm                   Fm7    Bb7    Eb7+
Tudo certo eu vou te es....que....cer
      Ab7+          Am5-/7  D7     Gm
Com o tempo vou me re.....fa.....zer
                       Fm7    Bb7     Eb7+
Outro amor pra mim vai re....nas.....cer
      Ab7+       Am5-/7   D7     Gm
Vou amar igual amei.....vo.....cê
     Gm7        Cm      F7       Bb7+
A inveja aplaudiu, a saudade apertou
          Eb7+       Am5-/7
Quando o meu prato caiu
D7              Gm
Sorrindo da minha dor
     Gm7           Cm
Coração não desistiu

      F7           Bb7+
Se feriu mais agüentou
     Eb7+         Am5-/7
Sabe que você mentiu
    D7           G     F#   G
Ao jurar que me amo....o....ou
  D/F#     Em7   E7                   C
Muito me magoei, só por ser um aprendiz
   E7        Am               D7        G
Eu vou recomeçar, provar que posso ser feliz
   D/F#            Em7   E7         C
Muito me magoei, só por ser um aprendiz
   C5+        Am               D7        Gm
Eu vou recomeçar, provar que posso ser feliz
            Gm7                 Ab    D7    Gm
O desengano....marcou demais o meu....vi....ver
             Gm7                Am5-/7   D7    Gm     (2x)
Ainda te amo....mas eu não posso mais....so....frer

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Ab7+ = 4 X 5 5 4 X
Am = X 0 2 2 1 0
Am5-/7 = 5 X 5 5 4 X
Bb7 = X 1 3 1 3 1
Bb7+ = X 1 3 2 3 1
C = X 3 2 0 1 0
C5+ = X 3 2 1 1 X
Cm = X 3 5 5 4 3
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
D7/11 = X X 0 2 1 3
E7 = 0 2 2 1 3 0
Eb7+ = X X 1 3 3 3
Em7 = 0 2 2 0 3 0
F# = 2 4 4 3 2 2
F7 = 1 3 1 2 1 1
Fm7 = 1 X 1 1 1 X
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
