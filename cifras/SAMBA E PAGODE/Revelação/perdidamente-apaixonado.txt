Revelação - Perdidamente Apaixonado

G
Introducao:..

G7+    G7/4 - G7            C7+
Passo o dia inteiro a me declarar
Am7        D7                  G7+        C
Te admirando da janela do meu quarto
G7+      G7/4 - G7             C7+
Fico diferente, radiante, ao te ver passar
Am7            D7                     G7+     G7/4 - G7
Quando não te vejo, logo pego seu retrato
C                 Cm7
Não escondo meu desejo
                     Bm
Quero provar o seu beijo
        Bm7+      Bm7
Vem realizar meu sonho
                 Bm6
Meu amor é verdadeiro
 Am7        Am7+
Faço o que você quiser

 C         D7          G7+  -  D/F#  -  Am7  - D7
Desde que venha ficar comigo
G7+      D/F#       Bm7/5b  E7/4 - E7
Eu preciso tanto ter você aqui comigo
Am7         Am7+                C
Não suporto mais viver sem dividir
    D7     G7+
Meu mundo
        D/F#         Bm7/5b
Tô perdidamente apaixonado
           E7/4 - E7
Quero seu carinho
Am7        Am7+              C
Quero te envolver de corpo e alma
      D7        G7+
E te levar pro ninho

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Am7+ = X 0 2 1 1 0
Bm = X 2 4 4 3 2
Bm6 = 7 X 6 7 7 X
Bm7 = X 2 4 2 3 2
Bm7+ = X 2 4 3 3 2
Bm7/5b = X 2 3 2 3 X
C = X 3 2 0 1 0
C7+ = X 3 2 0 0 X
Cm7 = X 3 5 3 4 3
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
E7/4 = 0 2 0 2 0 X
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
G7+ = 3 X 4 4 3 X
G7/4 = 3 5 3 5 3 X
