Revelação - Vida

G7M  E°           Am        D7
Ilusão pensar que o meu prazer
            Bm                   Am6/7   B7(9)
Pudesse consolar a minha solidão
      Em7           D     Bm7(b5)     E7           Am        D7
Só então Eu pude perceber Nem todo o amor é feito de desejo
                           Bm    E7         Am   D7       Bm   D
Um coração precisa muito mais Pra se satisfazer precisa ter a paz
           Bm7(b5)   E7          Am    D7       Bm      E7
A paz de um bem-querer Um colo pra deitar No amor desfalecer
       A7                 Am   D7
Pra nunca mais chorar Toda a vez que anoitecer
Am       D7               Bm     E7    A7    D7
Ando me perdendo em madrugada Quase sem pensar em nada
     Dm      G7(9)
Me perdendo em ilusões
C7M    D7            Bm      G7(9)    C7M    D7   Bm7(b5)   G7(9)
Vivo, e a vida segue o seu caminho Levo e assim eu me deixo levar
C#m7(b5)  D7    Bm          E7     A7
Nada nessa vida faz sentido Vou vivendo aqui sozinho
D7        Dm7    G7(9)
Esperando ela voltar

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Am6/7 = X 0 X 0 1 2
B7(9) = X 2 1 2 2 X
Bm = X 2 4 4 3 2
Bm7(b5) = X 2 3 2 3 X
C#m7(b5) = X 4 5 4 5 X
C7M = X 3 2 0 0 X
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
Em7 = 0 2 2 0 3 0
E° = X X 2 3 2 3
G7(9) = 3 X 3 2 0 X
G7M = 3 X 4 4 3 X
