Revelação - Sinto Muito Em Lhe Dizer

[Intro] Bb7+  C/Bb  Bb7+  C7  F7+  F6  Am7/5-  D7/9+  D7/9-  Gm7

            Bb/C    C7
Sinto muito em lhe dizer
                   Am7
Que sem amor a vida passa
           Abº
Sem a gente perceber
                       Gm7
A cor do céu fica sem graça
Bb/C              C7
E a chuva pode dissolver
                       Am7/11 ( Am7/5-  D7/9+ D7/9- )
Os seus momentos de alegria  (ôh)

Gm7                          Bb/C  C7
Quando se tem amor pra se viver
                  Am7
O mundo se enche de luz
                   Abº
E tudo pode acontecer

                   Gm7
Se a paixão que te conduz
Bb/C                  C7
Pelos caminhos do prazer
                Cm7
Você não deve desviar
              F/A   F7
E nunca se arrepender

             Bm7/5-
Você precisa de amor
             C/Bb
Com esse amor tenha razão
                   Am7/5-
Pra dar um fim na incerteza
D7/9+
E aliviar teu coração
D7/9-                 Gm7
Que ele te mostra com clareza
Am7                Bb7+
No calor da emoção
C7             F7+  D7
Que nada se perdeu

Gm7                  C7
Eu quero o teu amor, por favor
Am7                    D7
Não me faça sofrer por você
Gm7
Já tentei te esquecer
                      C7/9
Mas não deu pra evitar
           F7+
Continuo sozinho
D7             Gm7            C7
A luz do teu olhar junto ao meu
Am7                  D7
Me fez acreditar que eu sou seu
Gm7
Eu quero é te provar
C7
Que ninguém vai tirar
F7+                    ( F6  Am7/5-  D7/9+  D7/9- Gm7 )
Você do meu caminho

          Bb/C    C7
Sinto muito em lhe dizer
                   Bm7/5-  Bbm6  F7+
Que sem amor a vida passa

----------------- Acordes -----------------
Abº = 4 X 3 4 3 X
Am7 = X 0 2 0 1 0
Am7/11 = 5 X 5 5 3 X
Am7/5- = 5 X 5 5 4 X
Bb/C = X 3 X 3 3 1
Bb7+ = X 1 3 2 3 1
Bbm6 = 6 X 5 6 6 X
Bm7/5- = X 2 3 2 3 X
C/Bb = X 1 2 0 1 X
C7 = X 3 2 3 1 X
C7/9 = X 3 2 3 3 X
Cm7 = X 3 5 3 4 3
D7 = X X 0 2 1 2
D7/9+ = X 5 4 5 6 X
D7/9- = X 5 4 5 4 X
F/A = 5 X 3 5 6 X
F6 = 1 X 0 2 1 X
F7 = 1 3 1 2 1 1
F7+ = 1 X 2 2 1 X
Gm7 = 3 X 3 3 3 X
