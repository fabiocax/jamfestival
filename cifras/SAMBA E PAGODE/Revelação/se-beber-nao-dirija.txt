Revelação - Se Beber Não Dirija

F         C7                F
Sai do trabalho a fim de beber
         *D7                 G7
Era sexta-feira passei na cachanga
                               Gm
Deixei o possante pra não me perder
                C7                 F
Briguei com a comadre, banquei a sazanga
                                 Cm
Quem sabe é a gente mas hoje sou eu
             F7                Bb
Dei linha na pipa e cai na pista
                           F
Plantei na calçada e sinalizei
           C7                F    D7
Saiu pra beber tem que ter taxista
                Gm                 C7            Gm
Por favor não insista, saiu pra beber tem que ter
                                  C7
Pra fazer concreto tem que ter cimento
                                  F
Pra participar tem que entrar na lista

                                Bm7/5-
Só pode ser freira se for no convento
            E7                    Am
Só manja de arte se o dom é de artista
             F7                   Bb
Pra subir no pódio vai ter que lutar
           C7                     F
Só ergue a taça se é justa a conquista
            D7              Gm
Não pega no copo se for pilotar
           C7                 F     D7
Saiu pra beber tem que ter taxista
                 Gm              C7                 Gm
Por favor não insista,saiu pra beber tem que ter...
                                C7
Não queira noivado se não for casar
                                   F
Não é mestre-sala quem nao foi passista
                           Bm7/5-
Se ajoelhar você tem que rezar
            E7                    Am
Só compra a prazo quem não pode a vista
            F7                   Bb
Se andar na chuva tem que se molhar
                C7                  F
Não é por estar junto que é salada mista
            D7                Gm
Volante e bebida não vá misturar
           C7                 F    D7
Saiu pra beber tem que ter taxista
                 Gm              C7
Por favor nao insista saiu pra beber tem que ter
   F
Taxista

2° PARTE: sai do trabalho afim de
         ^  ^  ^
     F E Eb D
beber


VOLTAR  AO *


FINAL
             D7               Gm
Não pegue no copo se for pilotar
           C7             F
Saiu pra beber tem ter taxista
               D7                Gm
Deixar ele esperando na porta do bar
           C7             F
Saiu pra beber tem ter taxista
          D7                    Gm
E saiba moleque que eu sou brasileiro
           C7             F
Saiu pra beber tem ter taxista
             D7                 Gm
Porque sou nascido no Rio de Janeiro
           C7             F
Saiu pra beber tem ter taxista
               D7           Gm
Não gosto de andar na corda bamba
           C7             F
Saiu pra beber tem ter taxista
                      D7             Gm
As vezes quem bebe cachaça vacila no samba
           C7             F
Saiu pra beber tem ter taxista
           D7               Gm
E a galera bate na palma da mão
           C7             F
Saiu pra beber tem ter taxista
            D7            Gm
E curte o pagode do Revelação
           C7             F
Saiu pra beber tem ter taxista...

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
