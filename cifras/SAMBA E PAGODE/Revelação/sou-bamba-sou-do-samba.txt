Revelação - Sou Bamba, Sou do Samba

C
Já curti rock, samba reggae
       C
Funk, charme
        F
Num swing arretado
         F
O meu nome já deixei
         C
Olodum, frevo, forró, folia de reis
         Bb7
Até salsa e merengue
         Bb7
Te confesso, que dancei
         Eb
Bolero, valsa, capoeira, caipira
         Db
Candomblé, bumba-meu-boi, até cai no baião
         Eb
Mas te confesso que o samba e o pagode
         Dm
São de minha preferência

         G7
E moram no meu coração
         C
Já inventaram forrogode, rapgode
          C
Charmegode, funkgode
          F
E o samba não morreu
          C
Até quem não é bamba
           C
Gosta de um repique, de um pandeiro
            Bb7
De um cavaco, de um tantã e violão
            Eb
Fica maneiro quando chega fevereiro
            Db
Afinal sou brasileiro

Vou pra avenida sambar
    Eb
La la labamba
                 Dm
Sinto muito, mas o samba corre aqui em minhas veias
        G7
Ele não largo não
 C                F7+
Sou bamba, sou do samba
 Em                Dm
Não tô de brincadeira
 C                F7+
Sou bamba, sou do samba
Em              Dm G7
Ele é minha bandeira

Solo Cavaco Inicio meio e fim da música
32 32 22 22 32 32 22,32 30,32 32 21 21 32 32 21 20,30 12
12 15 12 17 15 12 24 22 34,32 34 22 23 25 25 25 25.
C C7 F G7


----------------- Acordes -----------------
Bb7 = X 1 3 1 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Db = X 4 6 6 6 4
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F7+ = 1 X 2 2 1 X
G7 = 3 5 3 4 3 3
