Revelação - Preciso Te Amar

[Intro] C  Em7  Am  Dm  G7

C          Em7
Eu estou apaixonado
Am                  Em
  Deixa eu ser seu namorado
F7+            D#º
  Quero ficar do teu lado
       F7+
Pra ter seu abraço, seu beijo
   G7/4  G7
Pra mim
C                 Em7
 O teu olhar não me engana
Am              Em
  Eu sei que você me ama
F7+            D#º
  Teu coração diz que você me quer
Am
 Mas o medo não deixa
Gm              C7
  Você se entregar

F7+
  Vem pra mim
 E7
Não faz assim
Am7           Gm7       C7
  Dê logo um fim nessa tortura
F7+            G7       C6/9
  Nosso romance tem que eternizar
 Gm7                 C7
Você tem que me aceitar
F7+
  Eu quero a paz
E7
  Do teu sorriso
Am7          Gm7       C7
   Pra enfeitar meu paraíso
F7+            G7/4
   Dê uma chance
 G7                C
Não custa nada tentar

 C7         F7+
Meu bem querer
  G7         C
Preciso te amar
C7      F7+        E7/9
O seu amor me faz sonhar
   F7+    G7    C
Se eu te perder
 C7     F7+     C
Não sei onde encontrar
C7        F7+
Um novo amor
     G7
Pro seu lugar
 C7         F7+
Meu bem querer
  G7         C
Preciso te amar
C7      F7+        E7/9
O seu amor me faz sonhar
   F7+    G7    C
Se eu te perder
 C7     F7+     C
Não sei onde encontrar
C7        F7+
Um novo amor
     G7
Pro seu lugar

C                 Em7
 O teu olhar não me engana
Am              Em
  Eu sei que você me ama
F7+            D#º
  Teu coração diz que você me quer
Am
 Mas o medo não deixa
Gm              C7
  Você se entregar
F7+
  Vem pra mim
 E7
Não faz assim
Am7           Gm7       C7
  Dê logo um fim nessa tortura
F7+            G7       C6/9
  Nosso romance tem que eternizar
 Gm7                 C7
Você tem que me aceitar
F7+
  Eu quero a paz
E7
  Do teu sorriso
Am7          Gm7       C7
   Pra enfeitar meu paraíso
F7+            G7/4
   Dê uma chance
 G7                C
Não custa nada tentar

 C7         F7+
Meu bem querer
  G7         C
Preciso te amar
C7      F7+        E7/9
O seu amor me faz sonhar
   F7+    G7    C
Se eu te perder
 C7     F7+     C
Não sei onde encontrar
C7        F7+
Um novo amor
     G7
Pro seu lugar
 C7         F7+
Meu bem querer
  G7         C
Preciso te amar
C7      F7+        E7/9
O seu amor me faz sonhar
   F7+    G7    C
Se eu te perder
 C7     F7+     C
Não sei onde encontrar
C7        F7+
Um novo amor
     G7
Pro seu lugar

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
C6/9 = X 3 2 2 3 3
C7 = X 3 2 3 1 X
D#º = X X 1 2 1 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
E7/9 = X X 2 1 3 2
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F7+ = 1 X 2 2 1 X
G7 = 3 5 3 4 3 3
G7/4 = 3 5 3 5 3 X
Gm = 3 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
