Paulinho da Viola - Jurar Com Lágrimas

Versão: ((Acústico MTV))

D     F#7       B7
Jurar com lágrimas
  E7
Que me ama
        Em7      A7
Não adianta nada
Em7             A7      D/F#    B7
Eu não vou acreditar
Em7             A7      D     A7
É melhor nos separar
(2 v.)

Em7             A7         D
Não pode haver felicidade
              A7
Se não há sinceridade
              D/F#      D6/9
Dentro do nosso lar
Em7             F#7             Bm7
Se aquele amor não morreu

                E7
Não precisa me enganar
Em7             A7        A7/4/9b
Que seu coração é meu

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7/4/9b = 5 X 5 3 3 X
B7 = X 2 1 2 0 2
Bm7 = X 2 4 2 3 2
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D6/9 = X 5 4 2 0 0
E7 = 0 2 2 1 3 0
Em7 = 0 2 2 0 3 0
F#7 = 2 4 2 3 2 2
