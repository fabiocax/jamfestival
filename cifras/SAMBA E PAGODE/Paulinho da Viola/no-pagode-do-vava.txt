Paulinho da Viola - No Pagode do Vavá

Refrão

   C       A7             Dm
Domingo, lá na casa do Vavá
                  G7/13               G7        C   G7/5+
teve um tremendo pagode que você não pode imaginar
    C               Am          Dm
provei do famoso feijão da Vicentina
                G7/13                G7        C       G7/5+
só quem é da Portela que sabe que a coisa é divina (provei)
    C               Am          Dm
provei do famoso feijão da Vicentina
                G7/13                G7        C
só quem é da Portela que sabe que a coisa é divina
       A7             Dm       G7         C
tinha gente de todo lugar no pagode do Vavá

 A7             Dm               G7                 C
nego tirava o sapato, ficava à vontade, comia com a mão
              Gm               C7                F
uma batida gostosa que tinha o nome de Doce Ilusão

                 Fm             Bb7             Em
vi muita nega bonita fazer partideiro ficar esquecido
A7             Dm
mas apesar do ciúme
            G7                 C       G7/5+
nenhuma mulher ficou sem o marido (domingo)

Refrão

A7             Dm               G7                 C
um assobio de bala cortou o espaço e ninguém mais ficou
              Gm               C7                F
muito malandro corria quando Élton Medeiros chegou
                           Fm
minha gente não fique apressada
             Bb7                Em
que não há motivos pra ter correria
       A7                  Dm
foi um nego que fez treze pontos
          G7/13              C     G7/5+
e ficou maluco de tanta alegria (domingo)

Refrão

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bb7 = X 1 3 1 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G7 = 3 5 3 4 3 3
G7/13 = 3 X 3 4 5 X
G7/5+ = 3 X 3 4 4 3
Gm = 3 5 5 3 3 3
