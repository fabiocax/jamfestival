Paulinho da Viola - Ainda Mais

Versão: Acústico MTV

G7M  Bm/F#  G7/F
Foi
         G7/D     C7M
como tudo na vida
                         Bm7/5-
Que o tempo desfaz
               E7/9-     Am7  E7/B  Am/C
Quando menos se quer
         D7/F#  G7M         B7
Uma desilu---são assim
                        Em7/9    C7M
Faz a gente perder a fé
           C#°     A7(13)  A7(9)/E
E ninguém é feliz, viu
            Eb7(9)         Am7   D7(9)
Se o amor não lhe quer
            G7M
Mas  enfim


Como posso fingir e pensar em você

Como um caso qualquer

Se entre nós tudo terminou
                    Em7/9       G7
Eu ainda não sei, mulher
              G7/D       C7M  C7M/B   Am7
E por mim não irei re-nun----ciar
              D7/F#                G7M
Antes de ver o que eu não vi
      G7M/F#          Dm6/F
Em      seu         olhar
                    E7(5+)  E7/5+/D    A7/C# 
Antes que a derra-----deira         chama que ficou 

Não queira mais queimar
C7M  C7M/B   Am7
Vai

              D7/F#                 G7M
 Que toda verdade de um amor
   G7M/F#  Dm6/F
O  tempo  traz
                           E7(5+)  E7/5+/D    A7/C#
Quem sabe um     dia        você          volta para mim
       Am7    D7(9)  G6
E amando ainda mais

----------------- Acordes -----------------
A7(13) = X 0 X 0 2 2
A7/C# = X 4 5 2 5 X
Am/C = X 3 2 2 5 X
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
Bm/F# = X X 4 4 3 2
Bm7/5- = X 2 3 2 3 X
C#° = X 4 5 3 5 3
C7M = X 3 2 0 0 X
C7M/B = X 2 2 0 0 0
D7(9) = X 5 4 5 5 X
D7/F# = X X 4 5 3 5
Dm6/F = 1 X 0 2 0 X
E = 0 2 2 1 0 0
E7(5+) = 0 X 0 1 1 0
E7/9- = X X 2 1 3 1
E7/B = X 2 2 1 3 X
Eb7(9) = X 6 5 6 6 X
Em7/9 = X 7 5 7 7 X
G6 = 3 X 2 4 3 X
G7 = 3 5 3 4 3 3
G7/D = X X 0 0 0 1
G7/F = 1 X X 0 0 3
G7M = 3 X 4 4 3 X
G7M/F# = 2 X 4 4 3 X
