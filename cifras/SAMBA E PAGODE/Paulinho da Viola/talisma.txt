Paulinho da Viola - Talismã


E7M                                    A7
Eu não preciso de um talismã
        E7M
Nem penso em teu amanhã
E                         C#7     F#m7
Vou remando com a maré
                               G#7
Eu não preciso de patuá

       C#m7
Nem peço ao meu orixá
                  F#7
Não vou na igreja, não sei rezar
      B7
Mas tenho fé
C#7          F#7
Pois agora quem eu quis
       B7              E
Também me quer


              F#m7
Por muito tempo
           B7              E               E7M
Eu batalhei o seu amor, porém
              Bm7
você me desarmava
    E7               A7M            A6
E só me dava o seu desdém

                 Am6                           G#m7
Quando me olhava parecia nem me ver
                C#7
Eu era ninguém
                F#7/13          F#7(b13)
Mas hoje em dia eu posso dizer
       F#m7                   B7(b9)
"Meu amuleto é meu bem"



----------------- Acordes -----------------
A6 = 5 X 4 6 5 X
A7 = X 0 2 0 2 0
A7M = X 0 2 1 2 0
Am6 = 5 X 4 5 5 X
B7 = X 2 1 2 0 2
B7(b9) = X 2 1 2 1 X
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
C#7 = X 4 3 4 2 X
C#m7 = X 4 6 4 5 4
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
E7M = X X 2 4 4 4
F = 1 3 3 2 1 1
F#7 = 2 4 2 3 2 2
F#7(b13) = 2 X 2 3 3 2
F#7/13 = 2 X 2 3 4 X
F#m7 = 2 X 2 2 2 X
G#7 = 4 6 4 5 4 4
G#m7 = 4 X 4 4 4 X
