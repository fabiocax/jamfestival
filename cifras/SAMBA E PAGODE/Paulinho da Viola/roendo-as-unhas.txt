Paulinho da Viola - Roendo As Unhas

    Bbm6         Ab6     Db7             G°
Meu samba não se importa que eu esteja numa
    Bbm6         Ab6     Db7         G°
De andar roendo as unhas pela madrugada
    Bbm6         Ab6     Db7         G°
De sentar no meio fio não querendo nada
    Bbm6         Ab6     Db7             G°
De cheirar pelas esquinas minha flor nenhuma

    Bbm6         Ab6     Db7              G°
Meu samba não se importa se eu não faço rima
   Bbm6       Ab6     Db7      G°
Se pego na viola e ela desafi -na
    Bbm6         Ab6     Db7             G°
Meu samba não se importa se eu não tenho amor
    Bbm6         Ab6     Db7         G°
Se dou meu coração assim sem disciplina

     Bbm6         Ab6     Db7         G°
Meu samba não se importa se desapareço
    Bbm6         Ab6        Db7         G°
Se desapareço, se desapareço, se desapareço

    Bbm6         Ab6     Db7      G°
Se digo uma mentira sem me arrepender
    Bbm6         Ab6     Db7      G°
Quando entro numa boa ele vem comigo
    Bbm6        Ab6    Db7        G°
E fica desse jeito se eu entristecer

----------------- Acordes -----------------
Ab6 = 4 X 3 5 4 X
Bbm6 = 6 X 5 6 6 X
Db7 = X 4 3 4 2 X
G° = 3 X 2 3 2 X
