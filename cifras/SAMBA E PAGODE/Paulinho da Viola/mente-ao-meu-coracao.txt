Paulinho da Viola - Mente ao meu coração

[Intro:] G Em Am D#º G7M G#º Am7 D7(9)

G7M              Bbº Am7
Mente ao meu coração
                  D#º G/D
Que cansado de sofrer
                C#º Am7
Só deseja adormecer
  G/B           Am7 Am7/G F#m7(b5)
Na palma da tua mão
                  B7 Em
Conta ao meu coração
                   A7(13)
Estória das crianças
             A7(b13)
Para que ele reviva
             Am7(b5) D7
As velhas esperanças

Am7               D7 G7M
Mente ao meu coração

                    Cº
Mentiras cor-de-rosa
Que as mentiras de amor
         Bm7(b5)     E7
Não deixam cicatrizes
Am7 Cm7 Bm7                E7 Am7
E tu és a mentira mais gostosa
                D7          G7M
De todas as mentiras que tu dizes

----------------- Acordes -----------------
A7(13) = X 0 X 0 2 2
A7(b13) = X 0 X 0 2 1
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Am7(b5) = 5 X 5 5 4 X
Am7/G = 3 0 2 0 1 0
B7 = X 2 1 2 0 2
Bbº = X 1 2 0 2 0
Bm7 = X 2 4 2 3 2
Bm7(b5) = X 2 3 2 3 X
C#º = X 4 5 3 5 3
Cm7 = X 3 5 3 4 3
Cº = X 3 4 2 4 2
D#º = X X 1 2 1 2
D7 = X X 0 2 1 2
D7(9) = X 5 4 5 5 X
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F#m7(b5) = 2 X 2 2 1 X
G = 3 2 0 0 0 3
G#º = 4 X 3 4 3 X
G/B = X 2 0 0 3 3
G/D = X 5 5 4 3 X
G7M = 3 X 4 4 3 X
