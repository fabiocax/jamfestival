Paulinho da Viola - Orgulho

[Intro] Am13(9)

Am      B5-(7)   Am/C
Você passa dissipada

A7/C#      Dm     E7/D
Na fumaça de seu orgulho

F6/9            Bbº
E os dias móveis carregam
B7/F#           E7  F7 G7
O móvel laqueado

A7                      D7(9)   G13/D
Não se usam mais os pés dourados
G7                      C7M
Nem as promessas de um amor
B7      E7      Am/C  G7
Ornamentado e vazio

Em      A7/C#           Dm     Bº        Am     Dm C7
Um velho caminhão de mudanças some na fumaça

F7M     Bº  G#º C7M          Bº    G#º    Gm6
Para onde você passa? Para onde as coisas passam?
Dm/F            E7          A7
Quando o orgulho esmaga as asas
Gm6/Bb  A7      Dm      G7      C7M     E7/G#   E7
O tempo é um pássaro de natureza vaga

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7/C# = X 4 5 2 5 X
Am = X 0 2 2 1 0
Am/C = X 3 2 2 5 X
B5-(7) = X 2 3 2 4 X
B7 = X 2 1 2 0 2
B7/F# = X X 4 4 4 5
Bbº = X 1 2 0 2 0
Bº = X 2 3 1 3 1
C7 = X 3 2 3 1 X
C7M = X 3 2 0 0 X
D7(9) = X 5 4 5 5 X
Dm = X X 0 2 3 1
Dm/F = X X 3 2 3 1
E7 = 0 2 2 1 3 0
E7/D = X 5 X 4 5 4
E7/G# = 4 X 2 4 3 X
Em = 0 2 2 0 0 0
F6/9 = 1 0 0 0 X X
F7 = 1 3 1 2 1 1
F7M = 1 X 2 2 1 X
G#º = 4 X 3 4 3 X
G7 = 3 5 3 4 3 3
Gm6/Bb = X 1 2 0 3 0
