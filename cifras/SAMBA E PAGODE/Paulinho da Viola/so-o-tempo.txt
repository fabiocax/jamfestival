Paulinho da Viola - Só o Tempo

A7(b13)   D7M D7M
Largo a paixão
Cm6               B7(b13)   Bm6
Nas horas em que me atrevo
A7      A/G       Bm7/F#  D/F#
E abro mão de dese------jos
G7(13)                    C#7(b5)
Botando meus pés no chão
F#7(b13)         Bm7M
É só eu estar feliz
G#m7(5b)        Am6
Acende uma ilusão
D/C                               Db/B
Quando percebe em meu rosto
C#7/G#                F#m7
As dores que não me fez
A7           D7M D7M
Ah, meu pobre coração
Cm6                  Bm6   A7                         D7M
O amor é um segredo. E sempre chega em silêncio
Am7      D7/A        G5 G5M G6
Como a luz no amanhecer.

G7M                     G#m7(5b)
Por isso eu deixo em aberto
G7(b5)       F#7(13) C7(9) A/B                   B7(b13) Bm6
Meu saldo de sentimentos.       Sabendo que só o tempo
A7(13)   A7(b13) D7M D7M
Ensina a gente a viver.
Cm6         B7(b13)  Bm6   A7(13)   A7(b13) D/F# D/F E7(9) Eb7(9) D6(9)
Sabendo que só o tempo. Ensina a gente a viver...

----------------- Acordes -----------------
A/B = X 2 2 2 2 X
A/G = 3 X 2 2 2 X
A7 = X 0 2 0 2 0
A7(13) = X 0 X 0 2 2
A7(b13) = X 0 X 0 2 1
Am6 = 5 X 4 5 5 X
Am7 = X 0 2 0 1 0
B7(b13) = X 2 X 2 4 3
Bm6 = 7 X 6 7 7 X
Bm7/F# = X X 4 4 3 5
Bm7M = X 2 4 3 3 2
C#7(b5) = X 4 5 4 6 X
C#7/G# = 4 X 3 4 2 X
C7(9) = X 3 2 3 3 X
Cm6 = X 3 X 2 4 3
D/C = X 3 X 2 3 2
D/F = X X 3 2 3 2
D/F# = 2 X 0 2 3 2
D6(9) = X 5 4 2 0 0
D7/A = 5 X 4 5 3 X
D7M = X X 0 2 2 2
Db/B = X 2 3 1 2 X
E = 0 2 2 1 0 0
E7(9) = X X 2 1 3 2
Eb7(9) = X 6 5 6 6 X
F#7(13) = 2 X 2 3 4 X
F#7(b13) = 2 X 2 3 3 2
F#m7 = 2 X 2 2 2 X
G#m7(5b) = 4 X 4 4 3 X
G5 = 3 5 5 X X X
G5M = 3 X 1 0 0 X
G6 = 3 X 2 4 3 X
G7(13) = 3 X 3 4 5 X
G7(b5) = 3 X 3 4 2 X
G7M = 3 X 4 4 3 X
