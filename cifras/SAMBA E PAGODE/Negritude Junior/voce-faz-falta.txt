Negritude Junior - Voce Faz Falta

Intro: G Em7 C7M D7 G D7

G
Falta alguma coisa no ar
                                         Am
Faz tempo que não vejo brilhar a lua de prata
C                      D7
Falta alguma coisa em mim
             G      D7
Você me faz falta
G
Ta faltando estela no céu
              Am
Aquela que você deu pra mim ta sempre apagada
C                       D7
Onde estão os raios de sol
             G     G7
Você me faz falta

         C
O mundo tem um coração de metal

                Bm               Em7
Se a gente não tem um amor de verdade
        Am                    C
Nessas coisas todo mundo é igual
           G                G7
Metade da gente está em alguém
       C
Eu às vezes me pego a pensar
                 Bm               Em7
Que tudo isso é parte da minha saudade
      Am              D7
Nada posso fazer se você
          G      Cm D7
É minha metade
G
Eu ainda amo você (nã vá embora)
Em7
Preciso muito te ver (te quero agora)
C
Dentro do meu coração
                         D7
Ficou esse silêncio falando de amor
G
Não sei sonhar sem você (você faz falta)
 Em7
Não sei viver sem você (você faz falta)
 C
Nada acabou entre nós
              D7
Queria te encontrar, te olhar, te dizer
         G
Fica comigo

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C7M = X 3 2 0 0 X
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
