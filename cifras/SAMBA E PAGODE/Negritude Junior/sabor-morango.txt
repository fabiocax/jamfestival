Negritude Junior - Sabor Morango

G#6 A6                 A6
      Morena da cor de jambo
G#6 A6   Co             Bm7   Go Bm7
      Queria mais um beijinho
                 Bm
Sua boca sabor morango
   F#5+         Bm7
Me deixou apaixonado
  E7              A7+  E5+ A6
Fiquei logo amarradinho

   Bm7           A6    G#6 A6
Morena da cor de jambo
   Co             Bm7   Go Bm7
Queria mais um beijinho
                 Bm
Sua boca sabor morango
   F#5+         Bm7
Me deixou apaixonado
  E7              A7+  A6
Fiquei logo amarradinho


Em7  A7            D#m7 Dm6
   É tudo que eu sonhei
               C#m7 F#7/4
Menina da zona sul
    F#7             B7/4
Vem pra completar o meu visual
    B7            Bm7
Que no sapatinho abalou geral
   E7           A6
No fundo do coração

(Ela é tudo que eu sonhei)

                  Dm
Eu te dei até meu tel
    G7/9            Am
Pra quando quiser ligar
                Dm F7/9 E7/9     A7+
Eu espero por você,          morena
  A7             D#m7/5-
E quando você surgir
     Dm          C#m7
Deus vai nos abençoar
      F#m7        B7/9 E7 A   A
Pra você morar na minha   vida

----------------- Acordes -----------------
A = X 0 2 2 2 0
A6 = 5 X 4 6 5 X
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
B7/4 = X 2 4 2 5 2
B7/9 = X 2 1 2 2 X
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
D#m7 = X X 1 3 2 2
Dm = X X 0 2 3 1
Dm6 = X 5 X 4 6 5
E5+ = 0 X 2 1 1 0
E7 = 0 2 2 1 3 0
E7/9 = X X 2 1 3 2
Em7 = 0 2 2 0 3 0
F#5+ = 2 5 4 3 2 X
F#7 = 2 4 2 3 2 2
F#7/4 = 2 4 2 4 2 X
F#m7 = 2 X 2 2 2 X
F7/9 = X X 3 2 4 3
G#6 = 4 X 3 5 4 X
G7/9 = 3 X 3 2 0 X
