Negritude Junior - Olhos Vermelhos

[Intro] Dm  Cm  Dm  Eb  Db  F

   Dm
As coisas já não andam bem pra mim
   Cm
Perder você assim não é legal
    Dm                         Cm   Dm
E a tua ausência dói demais no peito
   Eb                          Dm
Pensei que fosse fácil te esquecer
  Eb                       Dm
Jurei que nunca mais ia te ver
         Cm          Eº            Eb  F
Só não sabia que te amava tanto assim

       Bb        Gm
Quando olho no espelho
        Cm           F
Os meus olhos tão vermelhos
Dm          Gm   Cm         F
De tanto chorar, de tanto chorar


     Eb                        Dm
Na ausência dessa amor vou amargando a solidão
    Eb                Eº            Eb       F
Não tem remédio, não tem jeito, não tem solução
      Eb                           Dm
Vou rezar, fazer promessa pra esquecer essa paixão
      Eb             Eº       Eb          F
E não vou deixar ninguém machucar meu coração

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
Db = X 4 6 6 6 4
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
Eº = X X 2 3 2 3
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
