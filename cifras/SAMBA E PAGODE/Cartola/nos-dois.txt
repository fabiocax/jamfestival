Cartola - Nós Dois

[Intro] F#m7  B7/9  G#m7  C#7/9  F#m7  B7/9  E  B7

E                                        Am5-     B7
 Está chegando o momento de irmos pro altar, nós dois
E                                   G#m7    C#7/9
 Mas antes da cerimônia devemos pensar e depois

 F#m7                B7/9
Terminam nossas aventuras
  G#m7             C#7/9
Chega de tanta procura
 F#                                    B7
Nenhum de nos deve ter mais alguma ilusão

E                                      Am5-       B7
 Devemos trocar idéias e mudarmos de idéias, nos dois
E                                  G#m7    C#7/9
 E se assim procedermos seremos felizes depois

 F#m7              B7/9
Nada mais nos interessa

 G#m7        C#7/9
Sejamos indiferentes
F#m7               B7/9         E        B7
    Só nos dois, apenas dois, eternamente

----------------- Acordes -----------------
B7 = X 2 1 2 0 2
B7/9 = X 2 1 2 2 X
C#7/9 = X 4 3 4 4 X
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
F#m7 = 2 X 2 2 2 X
G#m7 = 4 X 4 4 4 X
