Cartola - Como é que eu posso

C                                         G7/13             C
Como é que eu posso, cozinhar sem banha,
            G7/13        C        G7/13          Dm
Sem cebola e alho, sem vinagre e cheiro,
                          Dm                 G7     Dm
Como é que eu posso, ter bom paladar,
            G7       Dm       G7                   C
Sem você deixar, a grana pros temperos.

                          C               G7/13    C
Pois fique sabendo, que o feijão bichado,
                      C7                                 F
E o arroz quebrado, que alguém lhe vendeu,
    Dm            G7               C            A7       Dm
Já despejei tudinho no terreiro, veja bem o dinheiro,
           G7       C
Que você perdeu.

C                                         G7/13             C
Ou você acaba com essa economia,
            G7/13        C        G7/13          Dm
Ou então acaba-se nossa amizade,

                          Dm                 G7     Dm
Já reclamo isso quase todo dia,
            G7       Dm       G7                   C
Você me responde com simplicidade.

                          C               G7/13    C
É que a cebola minha filha, está soberba,
                      C7                                 F
O alho e o vinagre cada vez subindo mais,
    Dm            G7               C            A7       Dm
Peça emprestado cada dia a uma vizinha,
           G7       C
Ou continua fazendo, sempre como você faz.

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G7 = 3 5 3 4 3 3
G7/13 = 3 X 3 4 5 X
