Cartola - Camarim

( E9  F9/E  B7 )

E9
No camarim
              G#m7/5-   C#7
As rosas vão murchan    do
F#m                F#m/E
E o contra regra dá
            B7
O último sinal
E/G#            E/G
As luzes da platéia
F#m               F#m/E
Vão se amortecendo
C#m              F#
E a orquestra ataca
    C         B7
O acorde inicial

E9
No camarim

               G#m7/5-   C#7
Nem sempre é eufori       a
F#m                 F#m/E
Artista de mim mesmo
                 B7
Nem posso fracassar
E/G#              E/G
Releio os bilhetes
F#m              F#m/E
Pregados no espelho
C#m              B7
Me pedem que jamais
     E          G#7
Eu deixe de cantar

C#m           C#m/B
Caminho lentamente
D#m7/5-           G#7
E entro em contra-luz
 G#m7/5-         C#m7/9
E as garganta acende
F#m            F#m/E
Um verso sedutor
D#m7         G#7
O corpo se agita
C#m            C#m/B
E chove pelos olhos
D#7              A7
E um aplauso explode
G#7            F#m
Em cada refletor

C#m              C#m/B
Pisando esta ribalta
D#m7/5-         G#7
Cantando pra vocês
G#m7/5-         C#m7/9
De nada sinto falta
F#m              F#m/E
Sou eu mais uma vez
D#m7/5-          G#7
As rosas vão murchar
C#m            C#m/B
Mas outras nascerão
A#7    A7           G#
Cigar  ras sempre cantam
        C#m
Seja ou não verão
A#7    A7           G#
Cigar  ras sempre cantam
         C         E9
Seja ou não verão

----------------- Acordes -----------------
A#7 = X 1 3 1 3 1
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
C#7 = X 4 3 4 2 X
C#m = X 4 6 6 5 4
C#m/B = 7 X 6 6 5 X
C#m7/9 = X 4 2 4 4 X
D#7 = X 6 5 6 4 X
D#m7 = X X 1 3 2 2
D#m7/5- = X X 1 2 2 2
E = 0 2 2 1 0 0
E/G = 3 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E9 = 0 2 4 1 0 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
F#m/E = X X 2 2 2 2
F9/E = 0 3 5 2 X X
G# = 4 3 1 1 1 4
G#7 = 4 6 4 5 4 4
G#m7/5- = 4 X 4 4 3 X
