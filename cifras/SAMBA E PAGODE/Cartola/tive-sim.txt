Cartola - Tive sim

[Intro] E7  Am7  D7(9)  G7M/B  E7(#9)  Am7  D7(9)  G7M  E7(9)

 A7
Tive,   sim
Cm6/Eb        D/C         G7M/B
Outro grande amor antes do teu
      Am7
Tive, sim
G7M/B         Bb°              Am7
O que ela sonhava eram os meus sonhos e assim
D7(9)   D/C     G7M/B
Íamos vivendo em paz
Gb7        Gb7/Bb                       G7M
Nosso lar, em nosso lar sempre houve alegria
C7M      E7/B
Eu vivia tão contente
 A7                   Cm6/Eb            D7
Como contente ao teu lado              estou
A7/C#
Tive, sim
Cm6/Eb             D7        G7M/B     G7
Mas comparar com o teu amor seria o       fim

   C7M
Eu vou calar
A7          D7(b9)        G7M
Pois não pretendo amor te magoar
   E7(9)         A7
Ai ai         ai ai
           D7(b9)            G6(9)
Pois não pretendo amor te magoar

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7/C# = X 4 5 2 5 X
Am7 = X 0 2 0 1 0
Am7(9) = X X 7 5 8 7
Bbº = X 1 2 0 2 0
C7M = X 3 2 0 0 X
Cm(maj7) = X 3 5 4 4 3
D/C = X 3 X 2 3 2
D7(4/9) = X 5 5 5 5 5
D7(b9) = X 5 4 5 4 X
E7 = 0 2 2 1 3 0
E7(9) = X X 2 1 3 2
F6 = 1 X 0 2 1 X
G6(9) = X X 5 4 5 5
G7(b9) = 3 X 3 1 0 X
G7M = 3 X 4 4 3 X
G7M/B = 7 X 5 7 7 X
Gb7 = 2 4 2 3 2 2
