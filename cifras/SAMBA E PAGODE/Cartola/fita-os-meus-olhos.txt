Cartola - Fita Os Meus Olhos

Introdução:  G   G#º  D7M  B7  E7(9)  A7  A7(#5)

D7M           B7          E7(9)
Fita os meus olhos vê como eles falam
     Bbº(b13) A7(b13)  D7M      Em7  A7
Vê como reparam o seu proceder
D7M E7(9) A     F#7       Bm7     E7(9)
Não é preciso dizer deve compreender
        A         A7          D7M
E até mesmo notar só no meu olhar
Am7    B7         Em7         Em7/D   C#m7(b5)
Não abuses por eu te confessar
      F#7     Bm7                Am7  D7(9)
Que nascestes só para eu te amar
G      G#º         D/A      B7
Gosto tanto, tanto de você
E7(9)                   A7         A7(#5)
Que os meus olhos falam o que não vê


D7M          B7            E7(9)
Fita os meus olhos vê como eles falam

    Bbº(b13) A7(b13)   D7M     Em7  A7
Vê como reparam o seu proceder
D7M E7(9) A     F#7       Bm7    E7(9)
Não é preciso dizer deve compreender
       A          A7          D7M
E até mesmo notar só no meu olhar
Am7   B7        Em7      Em7/D   C#m7(b5)
Ainda há de chegar o dia
       F#7         Bm7          Am7  D7(9)
Que eu hei de ter grande alegria
G        G#º        D/A     B7
Quando você souber compreender
E7(9)               A7        A7(#5)
Num olhar o que eu quero dizer

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
A7(#5) = X 0 X 0 2 1
A7(b13) = X 0 X 0 2 1
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
Bbº(b13) = X 1 X 0 2 2
Bm7 = X 2 4 2 3 2
C#m7(b5) = X 4 5 4 5 X
D/A = X 0 X 2 3 2
D7(9) = X 5 4 5 5 X
D7M = X X 0 2 2 2
E7(9) = X X 2 1 3 2
Em7 = 0 2 2 0 3 0
Em7/D = X X 0 0 3 0
F#7 = 2 4 2 3 2 2
G = 3 2 0 0 0 3
G#º = 4 X 3 4 3 X
