Cartola - Amor Proibido

Intro: D

Bm7       E7/9  A7       D7M/9
Sabes que vou         partir
F°            Em7        A7/
Com os olhos rasos d'água
        D7M/9     Bm7
E o coração ferido
           C#m7(b5)  F#7      Bm7
Quando lembrar           de ti
         E7/9
Me lembrarei também
               Em7   Bm7
Deste amor proibido

         E7/9
Fácil demais
A7    D7M/9  F°
Fui    presa
         Em7   A7
Servi de pasto

       F#m7(b5)  B7
Em tua mesa
           Em7/9      F#7
Mas fique certa que jamais
Bm7          Em7/9
Terás o meu amor
            E7/9  A7   D7M/9
Porque não tens          pudor

Am7               D7/9
Faço tudo para evitar o mal
G7M                G6
Sou pelo mal perseguido
Cm7                   F7
Só o que faltava era esta
Bb7M                 A7/13
Fui trair meu grande amigo
          E7/9  A7/13    F#m7(b5) B7
Mas vou limpar          a mente
          Em7  A7
Sei que errei
         D7M/9   Bm7
Errei inocente

Em7/9             E7/9 A7/13     D7M/9
Só porque não tens           pudor

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
