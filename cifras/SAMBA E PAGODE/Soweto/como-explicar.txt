Soweto - Como explicar

E7/9+
Como explicar
                    Am6
Te amo e não precisa entender
E7/9+
Basta sonhar
                     Gm6
E deixar este sonho acontecer
G7+        G6
Meu coração
                     F#m7/5-         B5+7
Bate mais forte quando a gente faz amor
E7/9+               Am6
Como explicar, Como explicar,
E7/9+                Am6
Como explicar, Como explicar
F#m7
E quando chove o sol se esconde Sinto frio
G#m7
Acamisola o seu corpo, um arrepio
Am6

E esta data em sua agenda
      E7/9+
Esta no mês de abril
F#m7
Fotografada, resgistrada e coisa tal
G#m7
Aquele dia, um dia mais que especial
Am6                             E7/9+  E7/9
Te beijei um santuário de uma catedral

A7+                        B/A
É, o nosso amor é como o vento
                     G#m7
Não tem limites para voar
                 Dbm7
O mais puro sentimento
E7/9       A7+
Como explicar
              E/G#           F#m7
Te amo tanto que nem sei de mim
   E7/9+       Bm7   E7/9
Como explicar...




----------------- Acordes -----------------
A7+ = X 0 2 1 2 0
Am6 = 5 X 4 5 5 X
B/A = X 0 4 4 4 X
B5+7 = X 2 X 2 4 3
Bm7 = X 2 4 2 3 2
Dbm7 = X 4 6 4 5 4
E/G# = 4 X 2 4 5 X
E7/9 = X X 2 1 3 2
E7/9+ = X 6 5 6 7 X
F#m7 = 2 X 2 2 2 X
F#m7/5- = 2 X 2 2 1 X
G = 3 2 0 0 0 3
G#m7 = 4 X 4 4 4 X
G6 = 3 X 2 4 3 X
G7+ = 3 X 4 4 3 X
Gm6 = 3 X 2 3 3 X
