Soweto - Não Foi à Toa

Intro: Am7     D7/9    Bm5-/7   E7/9-   A7/9   D7/13

 G7+
Amor ,
        D/F#
Mais uma vez eu me atrasei
  F/G             G7/5+
Eu sei , perdi a hora
    C7+            Cm6
No caminho me lembrei
         Dm7           G7/9-
Que essa noite é importante
 C7+           F7/9
Hoje faz um mês
G7+                    G5+                 C7+    D7/9 A7/13   C/D    D7/9-
Que os nossos olhos se olharam pela primeira vez
G7+
Amor ,
           D/F#
Receba as flores que eu comprei
 F/G              G7/5+
Na cor que você gosta

      C7+          Cm6
Foi difícil , mas achei
     Dm7       G7/9-
Procurei pela cidade ,
C7+         Cm6
Cheio de paixão
G7+                       G5+           C7+ F#º Em Ebm Dm G7
Se eu cheguei tarde o culpado é o meu coração
C7+ C6            D7/9            Bm5-/7
Eu , Já me justifiquei , não foi à toa
                 E7/9-           A7/13  A7/5+
Me abraça , meu amor vai , me perdoa
     Am7   D7/13      G7+
Te amo               demais
            Dm         G7/9
Não chego tarde nunca mais
C7+ C6           D7/9
Vem , vamos comemorar
          Bm5-/7
A noite é nossa
             E7/9-    E7/5+
Abre o seu coração
             A7/13
E tranque a porta
    Am7   D7/13        G7+
Te amo               demais

----------------- Acordes -----------------
A7/13 = X 0 X 0 2 2
A7/5+ = X 0 X 0 2 1
A7/9 = 5 X 5 4 2 X
Am7 = X 0 2 0 1 0
Bm5-/7 = X 2 3 2 3 X
C/D = X X 0 0 1 0
C6 = 8 X 7 9 8 X
C7+ = X 3 2 0 0 X
Cm6 = X 3 X 2 4 3
D/F# = 2 X 0 2 3 2
D7/13 = X 5 X 5 7 7
D7/9 = X 5 4 5 5 X
D7/9- = X 5 4 5 4 X
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
E7/5+ = 0 X 0 1 1 0
E7/9- = X X 2 1 3 1
Ebm = X X 1 3 4 2
Em = 0 2 2 0 0 0
F#º = 2 X 1 2 1 X
F/G = 3 X 3 2 1 X
F7/9 = X X 3 2 4 3
G = 3 2 0 0 0 3
G5+ = 3 X 1 0 0 X
G7 = 3 5 3 4 3 3
G7+ = 3 X 4 4 3 X
G7/5+ = 3 X 3 4 4 3
G7/9 = 3 X 3 2 0 X
G7/9- = 3 X 3 1 0 X
