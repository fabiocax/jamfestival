Soweto - Tempo de Aprender

Intro: A   A7


D                   F#m
  Dei tanto amor pra você,
G    F#m    Em       F#m G
  Mas você não entendeu
         F#m   Em         F#m G
  Nem me olhou, nem me viu.
         F#m   Em         A7
  Nem me tocou, nem sentiu
            D     A7
  O amor nascer.

D                   F#m
E foi o fim pra nós dois
G    F#m    Em       F#m G
Nada restou pra depois
         F#m   Em         F#m G
Foi bom enquanto durou
         F#m   Em         A7
Esse romance, esse amor

            D     A
Pena que acabou

          Bm     F#m        G   F#m
Mas vou viver de novo uma paixão
        Bm    F#m7   G      A7         D
E dessa vez vou acertar    bem no coração

           F#m      G
  E nunca mais vou errar
       A7       D
  Como errei com você
             F#m     G
  Quero o prazer de amar,
      A7        D    (A7)
  É tempo de aprender

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
G = 3 2 0 0 0 3
