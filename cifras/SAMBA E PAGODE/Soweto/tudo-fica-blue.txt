Soweto - Tudo Fica Blue

(intro) E G#m7 C#m7  F#m7 Am6 Am6

E    E7+       E6         E7/9   A7+      A6
Tudo fica blue,   quando o sol se põe pra lua
   F#m7       A/B              E
abençoar mais uma vez o nosso amor
     E7+       E6          E7/9    A7+  A6
Tudo fica blue,   a nossa paixão flutua,
   F#m7          B/A
no universo onde mora o nosso amor
E7+            F#m7                  G#m7
Nosso amor é natural não existe nada igual
                  A7+   A              D#m7/5-
E uma guerra de prazer, faz a gente enlouquecer
G#7          C#m7                Cm7 Bm7
E o fogo da paixão, acendendo o coração
E7/9             A7+        B/A
E o que nos faz viver, é o nosso amor
                G#m7        G°
Sol de cada amanhecer, é o nosso amor
              F#m7   G#m7       F#m7   Am6   E7+
Que me leva ate você      é o no...ss      a..mor

   Bm7  E7/9
Nosso amor
              A7+       B/A
O desejo mais fiel, é o nosso amor
                    G#m7       G°
Gostoso com sabor de mel, é o nosso amor
                  F#m7   G#m7      F#m7     A/B     E7+/9  F#m7   Am6  A B
Que nos leva ate o céu,      é o no...sso  a..mor

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/B = X 2 2 2 2 X
A6 = 5 X 4 6 5 X
A7+ = X 0 2 1 2 0
Am6 = 5 X 4 5 5 X
B = X 2 4 4 4 2
B/A = X 0 4 4 4 X
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
Cm7 = X 3 5 3 4 3
D#m7/5- = X X 1 2 2 2
E = 0 2 2 1 0 0
E6 = X X 2 4 2 4
E7+ = X X 2 4 4 4
E7+/9 = X 7 6 8 7 X
E7/9 = X X 2 1 3 2
F#m7 = 2 X 2 2 2 X
G#7 = 4 6 4 5 4 4
G#m7 = 4 X 4 4 4 X
G° = 3 X 2 3 2 X
