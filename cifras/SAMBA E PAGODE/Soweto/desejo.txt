Soweto - Desejo

(intro) Cm7/9 Fm7 Eb7+ Dm7/5- G7/5+ C7+ D/C Dm7/5- G7/5+

(solo)  10 11 10 11 10 11 10 11 21 10 21 10 21 10 21 10 33 21 33 21 33 21 33
21 31 30 43 30 43 12 13 12 13 12 13 12
13 10 12 10 12 10 12 10 12 21 10 21 10 21 33 31 31

C6/9                      Ebº    C6/9             B7/5+
A gente foi se amando     eu me apaixonei
                Em7       Bm                   Em7                 G7   C7+
Quando percebi eu já era seu   quando descobri me fortaleceu
Cm7/9                        Fm                   Fm7+
Agora é tanto amor que eu tenho pra te dar
Fm7            Ab/Bb           Eb7+
Você não pode ter ninguém mais pode amar
Ab7+        Ab6 Dm7/5-           G7/5+
É destino meu vem me procurar
Cm7/9      G7/5+                 Cm7/9  Dm7/5- G7/5+
Não quero morrer de saudade
     C7+ F7+                  C7+
É vida  que a vida me deu
          D/C                         Dm7/5-     G7/5+
Tudo me conduz aos caminhos teus

Cm7/9     G7/5+           Cm7/9  Dm7/5- G7/5+
Me ama me mata a vontade
C7+       F7+                          C7+
É chama que a chama acendeu
         D/C                Dm7/5- G7/5+
Você é a luz que reacendeu

----------------- Acordes -----------------
Ab/Bb = X 1 1 1 1 X
Ab6 = 4 X 3 5 4 X
Ab7+ = 4 X 5 5 4 X
B7/5+ = X 2 X 2 4 3
Bm = X 2 4 4 3 2
C6/9 = X 3 2 2 3 3
C7+ = X 3 2 0 0 X
Cm7/9 = X 3 1 3 3 X
D/C = X 3 X 2 3 2
Dm7/5- = X X 0 1 1 1
Eb7+ = X X 1 3 3 3
Ebº = X X 1 2 1 2
Em7 = 0 2 2 0 3 0
F7+ = 1 X 2 2 1 X
Fm = 1 3 3 1 1 1
Fm7 = 1 X 1 1 1 X
Fm7+ = 1 X 1 1 1 X
G7 = 3 5 3 4 3 3
G7/5+ = 3 X 3 4 4 3
