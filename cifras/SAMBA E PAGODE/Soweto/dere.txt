Soweto - Derê

 E7      Am      G7    C
Derê   derêrere  dererererê
   E7      Am      G7    C
Derê   derêrere  dererererê
   E7      Am      G7    C
Derê   derêrere  dererererê

Cm        Cm/Bb
 Quero você
                Fm       G7
Pra sentir tamanha emoção
             Cm          Cm7
Só o amor poderá dar o fim
                Fm            G7
Na tristeza do meu coração,  ah

Cm               Cm7               Fm           G7
 Vem pra ficar   que  a porta está aberta meu bem
            Cm        Cm/Bb             Fm          G7
É desjo demais é paixão, jamais vou esquecer de vocˆ


C              E7             Am
 Eu sei, te desejo te quero te amo,
      Gm C7             F            G7              F
E assumo que é pra valer, quero a paz em meu interior
       G7                 C         G7
Tenho amor para lhe oferecer, eu sei

   E7      Am      G7    C
Derê   derêrere  dererererê
   E7      Am      G7    C
Derê   derêrere  dererererê
   E7      Am      G7    C
Derê   derêrere  dererererê

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
Cm/Bb = X 1 1 0 1 X
Cm7 = X 3 5 3 4 3
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
