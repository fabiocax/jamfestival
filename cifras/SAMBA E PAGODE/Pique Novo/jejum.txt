Pique Novo - Jejum

(intro)

F#M7                  B F#M7
Tem alguma coisa diferente
D#m
Esse teu silêncio é de Matar
 B                  A#m                EM7+    C#4/7
Eu tô só e o Pior é que vejo voce  Me Evitar
F#M7                           B F#M7
Diz pra mim o que Houve com a gente
D#m                          B
Um amor que era de Invejar o que deu se perdeu
A#m        C#4/7    BbM7
E voce nao me fala por quer
  D#m
Voce não conversa voce tá Dispersa
A#m
ta sempre com pressa To sofrendo a Beça
B               A#m            C#4/7
e não é direito viver desse Jeito
      BbM7
(vou sair desse Jejum...)

 D#m
quando bate a fome sabe como é homem
A#m               D#4/7        C#7           F#M7+  C#7
Olha bem que te avisei tanto, tanto que eu cansei

(refrão)
F#M7
Voce tá de jogo mas quer me beijar
             D#m           D#7
voce tá de fogo deixa eu apagar
         G#m7          A#m7
Mata o teu desejo, Meu desejo
EM7+       C#7
deixa a gente se amar
F#M7
ta fazendo guerra com teu coração
               D#m         D#7
Solta logo a fera da sua Paixão
       G#m7          A#m7
mata teu desejo, Meu desejo
EM7+            C#7
Estamos sofrendo em Vão
 B     A#m7  G#m7    C#7   F#M7
Solta logo a fera da sua Paixão

----------------- Acordes -----------------
A#m = X 1 3 3 2 1
A#m7 = X 1 3 1 2 1
B = X 2 4 4 4 2
C#4/7 = X 4 6 4 7 4
C#7 = X 4 3 4 2 X
D#4/7 = X X 1 3 2 4
D#7 = X 6 5 6 4 X
D#m = X X 1 3 4 2
G#m7 = 4 X 4 4 4 X
