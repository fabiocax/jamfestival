Pique Novo - Tô carente meu bem

A7+
Introdução:  A7M   C#m   D7M   Dm6


A7M              A7                D/F#            Dm/F
O que está acontecendo com o nosso amor ....nosso amor
A7M             A7               D/F#        Dm/F
Teu beijo já não tem mais aquele sabor
A7M                         F#m7
Chega pra cá ....vem me aquecer
C#m                F#m7
Está tão frio sem você ...
A7M                F#m7
Se eu errei me dediquei ...
C#m                F#m7
Encaro o mundo pra aprender
           D7M                        C#m
Que eu sou louco por você,  eu sou vidrado em você
    Bm                          Dm6
Voce é meu bem querer ,eu faço tudo por voce
F#m7        C#m                      D7M                         Dm6
Vem .......vem.......     Que eu to carente estou sofrendo meu bem

F#m7        C#m                       D7M                        Dm6    E7
Vem .......vem.......     Que eu to carente estou sofrendo meu bem
A7M             A7/5+
Nosso romance vingou .......
D7M            Dm6
Na mesa tinha vinho e flor........
A7M           A7/5+         D7M     Dm6 - E7
E a gente se olhou ,se acariciou
A7M            A7/5+                        D7M
Não deixe tudo acabar ...Teu perfume paira no ar ....
                  Dm6           A7M       Dm6 - E7
No céu  a terra e o mar....É voce ....é voce......
A7M                 A7/5+                      D7M
A valsa começou a tocar, voce me chamou pra dançar
                    Dm6              A7M      Dm6      D/E  Dm-Dm   Dm-Dm
Meu corpo em seu corpo a se encontrar.....a se entregar......
F#m7       C#m
Vem  .....  vem......................

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
A7/5+ = X 0 X 0 2 1
A7M = X 0 2 1 2 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D/E = X X 2 2 3 2
D/F# = 2 X 0 2 3 2
D7M = X X 0 2 2 2
Dm = X X 0 2 3 1
Dm/F = X X 3 2 3 1
Dm6 = X 5 X 4 6 5
E7 = 0 2 2 1 3 0
F#m7 = 2 X 2 2 2 X
