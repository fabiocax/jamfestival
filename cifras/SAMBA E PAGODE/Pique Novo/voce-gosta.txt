Pique Novo - Você gosta

Intro:  (44-45-47-47-24/25-37  44-45-47-47/49-55)

D7+                   F#m7
Veja como é lindo a lua se deitando
G7M               A4(7/9)
Sobre o mar azul, sobre o mar azul
D7+                    F#m7
Veja como é lindo, as estrelas brilhando
G7+                 A4(7/9)
Para o nosso amor, para o nosso amor...
       F#m7     Bm7                 F#m7     Bm7
Que vai além dos sonhos, um amor que faz tremer
 F#m7             Bm7 Bm7/A  G7M  A4(7/9)
O céu, o mar e a Terra no auge do prazer
    G7+           F#m5+
Você gosta, do meu jeito
      Em              A4(7/9) A7
Sem vergonha, sem ter hora e lugar
       G7+        F#m5+
Faz proposta, eu aceito
      Em      A4(7/9)  A7(9)
O que vale é a gente se amar

----------------- Acordes -----------------
A4(7/9) = 5 X 5 4 3 X
A7 = X 0 2 0 2 0
A7(9) = 5 X 5 4 2 X
Bm7 = X 2 4 2 3 2
Bm7/A = 5 2 4 2 3 2
D7+ = X X 0 2 2 2
Em = 0 2 2 0 0 0
F#m5+ = 2 5 4 2 3 2
F#m7 = 2 X 2 2 2 X
G7+ = 3 X 4 4 3 X
G7M = 3 X 4 4 3 X
