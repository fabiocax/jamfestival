Grupo Raça - Tá Doendo Demais Essa Saudade

 G          D            Em7
Tá doendo demais essa saudade
C              D7            G    D7
Tô com medo do mel dessa paixão
G                   D         Em7
Mas quem não vai buscar felicidade
C              D7
Não ensina o caminho ao coração

G
Foi num samba de roda
               D7                G
Que eu vi teu olhar descendo a ladeira
                             D7                G
Fazendo zoeira pra me conquistar de qualquer maneira
                            D7               G
O amor, capoeira não pode jogar, que leva rasteira
                                D7              G
Feitiço é paixão, não é fácil curar nem com rezadeira

G7           C
Eu cai feito caça na sua armadilha nas garras do amor

                    G
Foi com quem eu brincava, gostava da fama de ser caçador
               D7
Fui beber do veneno gostoso em teus olhos pra me apaixonar
                 G                                      G7
Hoje eu morro de medo que um dia o brinquedo me faça chorar
               C
A saudade é malvada, mas eu não dou mole pra ela ficar
             G
Coração ta sofrendo, o amor tá doendo, mas vou te buscar
               D7
Não me tira da vida
                                               G
Paixão tão bonita não pode acabar nem de brincadeira

                             D7
Essa tal saudade vem me machucar
              G
Não vou dar bobeira
                                 D7             G
Ta doida, ta doida pra me ver chorar feito cachoeira
                                 D7              G
Mas hoje é domingo eu preciso sambar, só segunda-feira
                            D7                G       D7
Paixão é pimenta de bom paladar quando é verdadeira

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
