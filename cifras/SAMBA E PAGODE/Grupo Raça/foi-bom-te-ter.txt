Grupo Raça - Foi Bom Te Ter

[Intro]  Cm9  Fm7  Dm7/5-  G7

Cm9
Foi tão bom te ter
Fm7/9
Foi só te querer
Bb             Bb7  Bb7/9-
Foi tão bom te ter
Eb    Ab7+     Dm7/5-            G7/5+  G7
Em mim,e o que é que eu faço sem você
Cm9
Quero te dizer
Fm7/9
Que sou louco por você
Bb              Bb7/9-   G7
Sei que não há ninguém
            Eb9         Ab7+
Tão forte em vida que me faça
      Dm7/5-   G7
Te esquecer
  C9
Amoroooooorrr

             Em7
O que é que eu faço(o que é que eu faço)
           Gm
Sem teu abraço(sem teu abraço)
          F7    F6  F7+  F6
Te dei o mundo (dei o mundo,dei o mundo)
        Fm7  Bb7  Fm7
Te dei o céu(o céu)
       Em7     Dm7/5-  A7
E o infinito (e o infinito)
      Dm7  Em7  Fm7  G7/4  G7     C9
Sem teu abraço eu  não   sei viver

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Ab7+ = 4 X 5 5 4 X
Bb = X 1 3 3 3 1
Bb7 = X 1 3 1 3 1
Bb7/9- = 6 X 6 4 3 X
C9 = X 3 5 5 3 3
Cm9 = X 3 1 0 3 X
Dm7 = X 5 7 5 6 5
Dm7/5- = X X 0 1 1 1
E = 0 2 2 1 0 0
Eb = X 6 5 3 4 3
Eb9 = X 6 8 8 6 6
Em7 = 0 2 2 0 3 0
F6 = 1 X 0 2 1 X
F7 = 1 3 1 2 1 1
F7+ = 1 X 2 2 1 X
Fm7 = 1 X 1 1 1 X
Fm7/9 = X X 3 1 4 3
G7 = 3 5 3 4 3 3
G7/4 = 3 5 3 5 3 X
G7/5+ = 3 X 3 4 4 3
Gm = 3 5 5 3 3 3
