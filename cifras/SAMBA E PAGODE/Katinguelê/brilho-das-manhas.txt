Katinguelê - Brilho Das Manhãs

Introdução: C  Cm  Fm  Em  Dm  Fm

C  Am                Dm
Eu não posso te esquecer
   G7           C              Dm   G7   C
Porque me acostumei com o seu amor (seu amor)
Am                Dm
Vendo o dia amanhecer
   G7                        C
Eu fico triste e não tenho você
               Gm7    C              F
Será que não valeu, será que me esqueceu
Bm5-/7          E7              Am
A saudade invade a porta do meu peito e faz doer
Em5-/7         A7           Dm
Quantos bons momentos descartei sem perceber
Bm5-/7      E7        Gm             C7
Que um dia iria te perder, te perder
             F         G7     Em            B7
Meu bem, não faz assim, vê se volta pra mim
         Dm                  G7
O brilho das manhãs só tem razão de ser

         C         Em  Gm    C
Quando estou com você, eh eh
             F         G7
Meu bem, não faz assim
        Em                 B7
Preciso desse amor, amor, amor, meu amor
      Dm          C
A solidão é tão ruim
   Fm                G                C   G7 Gm
Te peço, tenha dó de mim, de todo coração

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
Bm5-/7 = X 2 3 2 3 X
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
Em5-/7 = X X 2 3 3 3
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
