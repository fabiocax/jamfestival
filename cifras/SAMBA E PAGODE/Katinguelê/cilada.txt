Katinguelê - Cilada

Intro: F F6 F F6 F D7

Gm        C7
  Êh mulher
              Am        D7/9
Me comovi com sua história
                    Gm
A qual se perder na vida
        C7
Tão decidida
           Am
Querendo se encontrar
       D#7            D7
Sem ninguém pra acreditar
Gm       C7
  Êh mulher
                    Am      D7
No início foi teu lado humano
                        Gm
Fui aos poucos me entregando
          C7
Me apaixonando

          Am
Então foi pra valer
           D#7            D7
Que eu te fiz meu bem querer
Bb                    C7
Mas     de repente eu vi
          Am          D#7          D7
Que só palavras sentimentos não te faziam mais feliz
Gm
  Seu sorriso se escondeu
C7
  Teu olhar entristeceu
    Am
Eu me indaguei
D#7         D7
Foi onde errei
Bb                    C7
  Éh   desde que conheceu
              Am        D#7            D7
Alguém que teve alguma sorte a mais na vida do que eu
Bb
  Seu sorriso então voltou
Gm          C7
Seu olhar então brilhou
     Am    D#7
Sem hesitar
         D7
Me abandonou
                    Bb             C7
Quem te viu na madrugada       perdida
                     Am        D#7    D7
Dando origem a uma virada      em sua vida
                     Gm
Pra você o amor é nada
           C7
Foi tão fingida
       Am D#7 D7
Oportunista
                         Bb      C7
Se algum dia eu te encontrar por aí
                     Am D#7        D7
Não sei bem se vou chorar se vou sorrir
                      Gm
Mas ao ver que se mantém
             C7
Longe dos programas
            Am D#7 D7
Ficarei feliz

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C7 = X 3 2 3 1 X
D#7 = X 6 5 6 4 X
D7 = X X 0 2 1 2
D7/9 = X 5 4 5 5 X
F = 1 3 3 2 1 1
F6 = 1 X 0 2 1 X
Gm = 3 5 5 3 3 3
