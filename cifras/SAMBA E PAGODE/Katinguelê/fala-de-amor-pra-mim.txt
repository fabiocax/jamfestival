Katinguelê - Fala de Amor Pra Mim

(intro) G D F D7 G D G D F D7 G D

      G              Am7   Bm7
Seu beijo gostoso me deixa louco
    Am7             D7     G   D7
Teu corpo menina me faz delirar
      G              Am7   Bm7
Seu beijo gostoso me deixa louco
    Am7             D7     G   F#m5-/7
Teu corpo menina me faz delirar
B7    Em7         Bm7
  Vem cá, fala de amor pra mim
    Am7             D7     G
Seu beijo me deixa assim, assim
F#m5-/7  B7  Em7                 Bm7
         Sei lá, vem cá falar de amor pra mim
    Am7             D7       Em7
Seu beijo me deixa assim, assim
                   Bm7
Vamos ver o sol se pôr
C              Bm7 F#m5-/7 B7
Namorar fazer amor

Em7                         Bm7
  Olhar a lua por trás das rochas
Am7                D7
  E falar do nosso amor
 G              Am7         Bm7
Sei que você me ama e eu te amo
Am7             D7
Eu não vivo sem você
 G                           Em7
Naquela noite em que você me deixou
Am7                   D7
  Eu não soube o que fazer



----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
F#m5-/7 = 2 X 2 2 1 X
G = 3 2 0 0 0 3
