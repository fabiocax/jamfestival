Katinguelê - Minha Felicidade

[Intro] Am7  F#m7/5-  F7/9  E7/9  Eb7  D7/9

G7M                G6          A7/9
Só tristeza em meu peito choro tanto
G7M              G6           Am7/9
Sem você não me acerto em meu leito
F7M                              Fm     Fm7   A7/11
Como vou mentir que não sinto sauda ... des?
            A7              Am7         Cm     D7  G7M  Cm
Deito e não durmo, sinto o afã vem a vonta ... de

G7M             G6             A7/9
De sentir a tua pele é o meu desejo
G7M                G6        Am7/9
De poder te dar um beijo novamente
F7M                            Fm     Fm7  A7/11
Incender com o teu corpo o meu fri ... o
           A7          Am7             Cm      D7  Bm7/5-
Deixa eu fazer teu coração pulsar mais for ... te

Cm        F7/9  B7M     G7          Cm
Faz tu ... do valer pra me fazer sorrir

G7/5+       Cm         F7/9        Bb7M
Quero ter você e ver a tristeza partir
   F            Fm7      Bb7          Eb7M
É grande o meu amor, pra toda a eternidade
    Cm      F     Cm    F7       Bb    Bb7M   D7/9
Meu bem, você é minha felici ... da ... de

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7/11 = X 0 2 0 3 0
A7/9 = 5 X 5 4 2 X
Am7 = X 0 2 0 1 0
Am7/9 = X X 7 5 8 7
B7M = X 2 4 3 4 2
Bb = X 1 3 3 3 1
Bb7 = X 1 3 1 3 1
Bb7M = X 1 3 2 3 1
Bm7/5- = X 2 3 2 3 X
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
D7/9 = X 5 4 5 5 X
E7/9 = X X 2 1 3 2
Eb7 = X 6 5 6 4 X
Eb7M = X X 1 3 3 3
F = 1 3 3 2 1 1
F#m7/5- = 2 X 2 2 1 X
F7 = 1 3 1 2 1 1
F7/9 = X X 3 2 4 3
F7M = 1 X 2 2 1 X
Fm = 1 3 3 1 1 1
Fm7 = 1 X 1 1 1 X
G6 = 3 X 2 4 3 X
G7 = 3 5 3 4 3 3
G7/5+ = 3 X 3 4 4 3
G7M = 3 X 4 4 3 X
