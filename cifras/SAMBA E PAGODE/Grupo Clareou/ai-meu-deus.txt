Grupo Clareou - Ai Meu Deus

Bm     D7
Ai meu deus
                G7+
Só você pra me ajudar
                F#7/5+ F#7
Só você pra me ajudar

Bm     D7
Ai meu deus
                G7+
Só você pra me ajudar
                F#7/5+ F#7
Só você pra me ajudar

Bm7    Am6    D7     G6  G7+ C#m7/5- F#7/5+ Bm7
Lalalaia lalaia lalaiala oh   oh     oh    oh

      Am6    D7     C#m7   C#7 F#7/5+ Bm7
Lalalaia lalaia lalaiala oh oh  oh    oh

 Bm7                   Am6
Do jeito que tá não dá pé

              D7    G6
Do jeito que tá só a fé
               G7+    C#m7/5-
Será que essa gente esqueceu
           F#7/5+
parece que nunca aprendeu
Bm7                    Am6
Do jeito que tá não dá não
          D7          G6
É briga de pai, mãe, irmão
           G7+    C#m7/5-
Sem rumo e sem direção
         F#7/5+
vivendo assim na ilusão
Bm7     E7/9          Am6
Cadê o amor, que faz o bem
      D7
Cadê a paz, que não se tem
G6           G7+ Em7 C#m7/5-
Meu senhor, meu senhor
         F#7/4 F#7 Bm7
Por favor, faz   a       lei

Bm7 Am6 Am
Ai meu deus
    D7             G7+
Só você pra melhorar
   G6           C#m7/5-
Só você pra encontrar
               F#7/5+  F#7
Os caminhos perdidos
Bm7    Am6
Ai meu deus
    D7        G7+
Só você pode mudar
    G6       C#m7/5-       (2x)
Só você pra ajudar
           F#7/5+  F#7
É o povo pedindo

G7+ C#m F#7/5+ F#7 Bm

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am6 = 5 X 4 5 5 X
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
C#7 = X 4 3 4 2 X
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
C#m7/5- = X 4 5 4 5 X
D7 = X X 0 2 1 2
E7/9 = X X 2 1 3 2
Em7 = 0 2 2 0 3 0
F#7 = 2 4 2 3 2 2
F#7/4 = 2 4 2 4 2 X
F#7/5+ = 2 X 2 3 3 2
G6 = 3 X 2 4 3 X
G7+ = 3 X 4 4 3 X
