﻿Atitude 67 - 8 Segundos

Capo Casa 3

[Intro] C5(9)  C9  C5(9)  C9  C5(9)  C9  C5(9)  C9
        G5(9)  G  G5(9)  G  G5(9)  G  G5(9)  G

         C5(9)  C9         C5(9)  C9
Se o eterno fosse esperto
                        G5(9)  G     G5(9)  G
Ele não desgrudava você
         C5(9)  C9         C5(9)  C9
Fechava o universo
                        G5(9)  G     G5(9)  G
E o espaço-tempo ia se perder

             C/E
8 segundos você acha pouco
                  C9
Mas senta em cima de um touro louco
                G5(9)  G     G5(9)  G
Aí você vai ver
             C/E
8 segundos antes disso tudo
                  C9
Eu nem te imaginava no meu mundo

                D11/F#
Agora eu

        C9                     G
Só quero 8 segundos pra falar
        C9                        G
O que eu não consegui dizer num mês
        C9
Depois que eu aprendi a amar
D11/F#
8 segundos bastam pra te convencer
        C9                        G
Que a vida é mais legal eu e você

( C5(9)  C9  C5(9)  C9  C5(9)  C9  C5(9)  C9 )
( G5(9)  G  G5(9)  G  G5(9)  G  D11/F# )

         C5(9)  C9         C5(9)  C9
Se o eterno fosse esperto
                        G5(9)  G     G5(9)  G
Ele não desgrudava você
         C5(9)  C9         C5(9)  C9
Fechava o universo
                        G5(9)  G     G5(9)  G
E o espaço-tempo ia se perder

             C/E
8 segundos você acha pouco
                  C9
Mas senta em cima de um touro louco
                G5(9)  G     G5(9)  G
Aí você vai ver
             C/E
8 segundos antes disso tudo
                  C9
Eu nem te imaginava no meu mundo
                D11/F#
Agora eu

        C9                     G
Só quero 8 segundos pra falar
        C9                        G
O que eu não consegui dizer num mês
        C9
Depois que eu aprendi a amar
D11/F#
8 segundos bastam pra te convencer
        C9
Que a vida é mais legal eu e você


Vou contar um segredo
                  G
Eu quebrei meu relógio
Só conto os segundos
              C9
Se eles forem nossos
E o pouco de verde
                 G
Perdido em seus olhos
Me faz viajar

----------------- Acordes -----------------
Capotraste na 3ª casa
C/E*  = 0 3 2 0 1 0 - (*D#/G na forma de C/E)
C5(9)*  = X 3 5 5 3 3 - (*D#5(9) na forma de C5(9))
C9*  = X 3 5 5 3 3 - (*D#9 na forma de C9)
D11/F#*  = 2 X X 2 3 3 - (*F11/A na forma de D11/F#)
G*  = 3 2 0 0 0 3 - (*A# na forma de G)
G5(9)*  = 3 X 0 2 3 3 - (*A#5(9) na forma de G5(9))
