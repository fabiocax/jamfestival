Atitude 67 - Tá Gostando Mais Ou Menos

[Intro]  A7+   A#m5-/7   Bm7    E79   A7+   A#m5-/7   Bm7    E79   A7+   A#m5-/7

                                 Bm7                 E79
Quando chega na balada já fica de cara
                 A7+                         A#m5-/7
Se uma novinha chega e sorri
                                 Bm7                        E79
A baixinha é muito baixa, a alta é muito alta
                  A7+                             A#m5-/7
A toda tatuada ele nem quer ouvir
                              Bm7                      E79
Ele já foi muito danado, agora tá enjoado
                        A7+                             A#m5-/7
Escolhendo a dedo mas nem se ligou
                                   Bm7                               E79
Que já virou foi comentário com o pessoal do bairro
                       A7+                                    A#m5-/7
O que aconteceu que esse rapaz mudou
                                   Bm7                                       E79
Tá gostando mais ou menos, tá gostando mais ou menos
                           A7+     A#m5-/7
Tem que gostar mais

                                   Bm7                                       E79
Tá gostando mais ou menos, tá gostando mais ou menos
                           A7+
Tem que gostar mais

----------------- Acordes -----------------
A#m5-/7 = X 1 2 1 2 X
A7+ = X 0 2 1 2 0
Bm7 = X 2 4 2 3 2
E79 = X X 2 1 3 2
