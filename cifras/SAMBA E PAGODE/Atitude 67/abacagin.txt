Atitude 67 - Abacagin

C       F               C
Tá perfeito assim pra mim
             F          C
Quem sou eu pra reclamar?
        F       C               G               C
Sempre fico até o fim, mas se quer saber vai lá
C                                       F
Eu não aguento mais esses drinks da moda
G                                       C
Mistura de gin com soda de novo pra variar
C                               F                       G
É caipirinha, caipirosca e caipifruta com kiwi, morango e uva
                        C
Não tem mais o que inventar
C                                       F
Então por isso eu vou contar o meu segredo
                        G                               C
É um drink feito por mim mesmo que é sem classificação
        C                               F
Eu sou suspeito, sei que sou muito suspeito
                        G                       C
Mas sei que ele vai ser eleito o novo drink do verão

        C               F       C               F
Abacagin com guaraná e hortelã pra refrescar
        C               F
Põe um gelin pra terminar
G                       Gm5+
Cê vai babar ba ba ba ba no abacagin com guarana

        C               F       C               F
Abacagin com guaraná e hortelã pra refrescar
        C               F
Põe um gelin pra terminar
G                       Gm5+
Cê vai babar ba ba ba ba no abacagin com guarana

----------------- Acordes -----------------
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
Gm5+ = 3 6 5 3 4 3
