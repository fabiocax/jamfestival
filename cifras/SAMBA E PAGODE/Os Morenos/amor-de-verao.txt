Os Morenos - Amor De Verão

Introo: G Em C Am D7

G        D    Em
Aquela garota tá sempre no meu pensamento
C           G               Am             D7
Eu fecho os olhos, vejo seu rosto a todo momento
G            D     Em
Por onde ela anda, será que também pensa em mim
C             G                  Am                 D7
Ou jogou de lado as nossas promessas de um amor sem fim
G       D         Em
E agora eu lembro das férias que juntos vivemos
C         G                   Am          D7
Só eu e você, não dá pra esquecer tudo de bom que fizemos
G        D         Em
Saudade, lembrança que o tempo não vai apagar
C          G         Am           D7
Teu corpo, meu corpo com tanta vontade de amar
G           D                     Am   C     D         G
O tempo não passa e eu preciso te ver, lindo amor de verão
           D                    Am  C       D       G
Um ano é demais pra ficar sem você, dona do meu coração


Solo do meio: Em C Am D7

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
