Os Morenos - Teu Charme

Introdução: G D/F# Em Cm G D/F# Em Cm C/D

 G                          G7+
Muitas vezes a gente não pensa
              C/G             Cm
Tem horas que só emoção nos conduz
      G                      G7+
Foi assim que eu fiquei de repente
             C/G               B7
Você tem o charme que chega e seduz
     Em             Cm
Você veio meio sem  avisar
       Bm            Em
Tomou conta do meu coração
     A7/4         A7
Não dava pra imaginar
         C/D
O que é paixão
    G                 G7+
Eu preciso da tua presença
             C/G               Cm
Que nem um surfista das ondas do mar

          G                G7+
Qualquer coisa no mundo compensa
             C/G                B7
Pra ter muito mais que você num olhar
      Em                   Cm
Nas estrelas se duvidar eu vou
    Bm                     Em
Pra você delas eu faço um cordão
   Am         Bm        C
Eu faço o impossível que for
       C/D       G    C/D
Só por essa paixão
            G
Eu quero te amar
    D/F#                Em
Olha só o que fez o teu charme
   Bm                  Am
Conseguiu me mudar de verdade
 Cm                 Bm    Em Am
Coração não para de sonhar
        D7   G
Eu quero te amar
      D/F#             Em
Desse sonho eu tenho direito
     Bm                  C7+
Só eu sei o que sinto no peito
      C/D          G
Já não dá pra segurar
  Bm     C7+
Ou ou ou ou
  Cm                  G
Coração não pára de sonhar
 Bm     C7+
Ou ou ou ou
      C/D         G         C/D
Já não dá pra segurar

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7/4 = X 0 2 0 3 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C/D = X X 0 0 1 0
C/G = 3 3 2 X 1 X
C7+ = X 3 2 0 0 X
Cm = X 3 5 5 4 3
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G7+ = 3 X 4 4 3 X
