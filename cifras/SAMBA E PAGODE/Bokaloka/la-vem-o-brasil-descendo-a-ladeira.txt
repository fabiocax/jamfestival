Bokaloka - Lá vem o Brasil descendo a Ladeira

Introdução:  D Bm7 A7

D               Bm                    A7
Quem desce do morro não morre no asfalto
                              D
Lá vem o Brasil descendo a ladeira
D             Bm                A7
Na bola, no samba, na sola, no salto
                               D
Lá vem o Brasil descendo a ladeira
D                Bm           A7
Da sua escola é passista primeira
                            D
Lá vem o Brasil descendo a ladeira
D              Bm              A7
No equilíbrio da lata não é brincadeira
                           D
Lá vem o Brasil descendo a ladeira
         C#7          C7     B7
E toda cidade que andava quieta
                                   Em
Naquela madrugada acordou mais cedo

               F#              Bm
Arriscando um verso gritou o poeta
              E                     A7
Respondeu o povo num samba sem medo
       D        C#7         C7       B7
E enquanto a mulata em pleno movimento
                                Em
Com tanta cadência descia a ladeira
              Gm               D
A todos mostrava naquele momento
   B7        Em        A7       D
A força que tem a mulher brasileira

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
C#7 = X 4 3 4 2 X
C7 = X 3 2 3 1 X
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
Gm = 3 5 5 3 3 3
