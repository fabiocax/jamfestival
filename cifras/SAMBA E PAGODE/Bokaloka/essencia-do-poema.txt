Bokaloka - Essência do Poema

[Intro]  Em  F#7  Bm  Bm7+  Em  A7  D  A7

D                            Em          A7
Vem me ver, que a vida ainda vale a pena
D                  Em
Vem ser a essência desse meu poema
Dm            G         Em   A7
É você, se quiser, a mulher

              D                Em         A7
Eu quero para viver por toda a minha vida
D                        Em
Jamais vou te deixar, querida
Dm               G         A   Am  D7
Só porque vou me dar pra você

       G
Do meu peito terás o amor
       F#m             Bm
Do meu corpo terás o calor
      Em                   A7            D    A7
Do jardim da minha vida, você é a única flor


       D                   Bm                C#m7/5-   F#7
Deixa amor, eu te pegar no colo e te deitar no leito
       Bm                                        Am     D7
Deixa amor, você vai se amarrar, vou te pegar de jeito
       G                  F#m     Bm
Deixa amor, oh oh, deixa amar, ah ah
       Em      A7         D    A7
Deixa amor, oh oh, deixa amar

[Solo]  Em  F#7  Bm  Bm7+  Em  A7  D  A7

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
Bm7+ = X 2 4 3 3 2
C#m7/5- = X 4 5 4 5 X
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F#7 = 2 4 2 3 2 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
