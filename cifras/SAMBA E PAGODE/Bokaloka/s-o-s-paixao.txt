Bokaloka - S.O.S paixão

INTRO:20 23 12 - 14 12 ? 14 12 - 10 22 10 22 20 32 20 34

D7+                 C#m7        C#m                                        F#7               Bm
  Já me cansei de ficar  por aí   vagando sem destino a chorar           por  voçê
                                G7            C7+             Em7               G7+    A7/9      D7+
  Apostei tudo que tinha e perdi  mesmo assim não desisti  de ser feliz
D7+                       C#m7                       C#m                              F#7                         Bm
  eu sei que pode atè demorar pra esquecer todas as coisas que vivi com voçê  tanto amor
                                          G7               C7+                              Em7         G/A  A7
  Mas não perco a esperança de encontrar nem que seja no infinito eu vou buscar
 D7+                           C#m7            E7/9          A7+
  Alguem que venha me trazer a paz       do amor
                                                   Em7             A7/9
 Que tire  as magoas do meu peito e leve a dor
    D                                C#m7                     C#m                G7+                   A7
 S.o.s paixão diz o meu coração que não sabe nao pode         viver sem teu amor
           C#m                           Dm   E7/9          A7+                 Em7            A7
 Mas que pena ter que te dizer           que acabou     (Que pena que acabou)



----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
A7/9 = 5 X 5 4 2 X
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
C7+ = X 3 2 0 0 X
D = X X 0 2 3 2
D7+ = X X 0 2 2 2
Dm = X X 0 2 3 1
E7/9 = X X 2 1 3 2
Em7 = 0 2 2 0 3 0
F#7 = 2 4 2 3 2 2
G/A = 5 X 5 4 3 X
G7 = 3 5 3 4 3 3
G7+ = 3 X 4 4 3 X
