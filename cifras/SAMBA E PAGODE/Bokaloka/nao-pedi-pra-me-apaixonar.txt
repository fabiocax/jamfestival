Bokaloka - Não Pedi Pra Me Apaixonar

Intro: A7+
(25 25 27 25 37 37 25 25) (210 210 29 27 29 25)

(25 25 27 25 36 37 25 25) (210 210 29 27 29 25) - (36 37 25 27 29)

 A7+       A5+      A6
.....Ela mexe comigo
 F#m7
.....E o pior que não sabe
  Bm                  Bm7/5+
.....Comentei com os amigos
G#m5-/7             C#7
.....Minha outra metade
F#m              F#m5+              F#m7
Ela.....ah, eu morro de amores por ela             (bis)
      F#m6              Bm7
Tô a ponto de largar aquela
        C#m7
Que há tempos me acompanhou
        Cº              Bm7
Mas o meu sentimento mudou

        D/E     A   D/E
Eu não pude conter  à na 1ª vez
        D/E     A7/9/11
Eu não pude conter  à na 2ª vez
 D7+                             E/D
.....E agora o que é que eu faço
                        C#m5-/7
Meu caminho tá sem traço
                             F#7
Quantas vezes eu me perguntei
           F#7
Como é que eu vou fazer?
 Bm                     C#m7
.....Se esse poço é venenoso
 D7+                    E/D
.....Se é certo ou duvidoso
  A         B7      Em          F#7
Nem quero saber.....nem quero saber
 Bm7
Olha, eu tô meio sem jeito
           D/E                E/D
Mas eu tô aqui, preciso te falar
 C#m7
Eu não sei se é direito
              F#7/9           F#7
Mas eu não pedi pra me apaixonar
 Bm7                 C#m7
Olha, eu te peço perdão
                   Dm           D/E
Mas quem manda na gente é o coração..
      A   B7    Em/G  F#7
É o amor......é o amor à 1ª vez
      A  E/G#
É o amor à na 2ª vez

----------------- Acordes -----------------
A = X 0 2 2 2 0
A5+ = X 0 3 2 2 0
A6 = 5 X 4 6 5 X
A7+ = X 0 2 1 2 0
A7/9/11 = 5 X 5 4 3 X
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
Bm7/5+ = X 2 5 2 3 2
C#7 = X 4 3 4 2 X
C#m5-/7 = X 4 5 4 5 X
C#m7 = X 4 6 4 5 4
Cº = X 3 4 2 4 2
D/E = X X 2 2 3 2
D7+ = X X 0 2 2 2
Dm = X X 0 2 3 1
E/D = X 5 X 4 5 4
E/G# = 4 X 2 4 5 X
Em = 0 2 2 0 0 0
Em/G = 3 X 2 4 5 X
F#7 = 2 4 2 3 2 2
F#7/9 = X X 4 3 5 4
F#m = 2 4 4 2 2 2
F#m5+ = 2 5 4 2 3 2
F#m6 = 2 X 1 2 2 X
F#m7 = 2 X 2 2 2 X
G#m5-/7 = 4 X 4 4 3 X
