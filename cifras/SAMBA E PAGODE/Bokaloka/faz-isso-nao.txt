Bokaloka - Faz isso não

Intro: Bb Dm Eb F F7

Bb7+
   Vieram me contar
    Dm7
Que você vai mudar daqui
Cm7
   E nem quer se despedir
Eb7+             Ebm7          Bb7+
   Só pra não chorar, faz isso não

           Dm7
Senta aqui, pra gente conversar
Cm7
   Levante a cabeça
Eb7+             Ebm7           Bb7+
   Olhe nos meus olhos, quero falar

   Dm7  Cm7
Viu,       o que o amor constroi
        Eb7+        F7             Bb  Bb7
Nada destroi, pense só um pouco em mim


Eb7+                                    Dm7
Fica amor, vamos dar um jeito em nossa vida
    Gm                  Cm7
Por favor ve se não complica
                   F7                  Bb       Bb7
Tudo o que eu mais quero é estar com você ê ê ê ê
Eb7+                                    Dm7
Fica amor, vamos dar um jeito em nossa vida
  Gm                  Cm7
Você também de mim precisa
F7               Bb               F7
Eu tenho a certeza vendo o seu olhar

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bb7 = X 1 3 1 3 1
Bb7+ = X 1 3 2 3 1
Cm7 = X 3 5 3 4 3
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Eb = X 6 5 3 4 3
Eb7+ = X X 1 3 3 3
Ebm7 = X X 1 3 2 2
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
Gm = 3 5 5 3 3 3
