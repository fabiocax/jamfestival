Dorival Caymmi - Horas


F#m   F#m/E   Em   Em/D
Se    já      fo...ra
Dm7/9  Dm9/C    F#m7/5- F7/11+
Que    importa ago......ra
Em7/5- A7/4/9 A7/9-  Dm7/9  Dm/C
Re.....talhar       a dor,   ai
F#m7/5- B7/9-  Bm7/5-  E7/13-
Que     doeu outro.....ra
F#m   F#m/E   Em   Em/D
In....fin.....da...da
Dm7/9  Dm9/C     F#m7/5- F7/11+
A      vez não é na......da
Em7/5- A7/4/9 A7/9-     Dm7/9  Dm/C
Pas....saram........-se ago.....ra
F#m7/5- B7   Dm6/F   Am
Ho......ras, ho......ras

----------------- Acordes -----------------
A7/4/9 = 5 X 5 4 3 X
A7/9- = 5 X 5 3 2 X
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
B7/9- = X 2 1 2 1 X
Bm7/5- = X 2 3 2 3 X
Dm/C = X 3 X 2 3 1
Dm6/F = 1 X 0 2 0 X
Dm7/9 = X 5 3 5 5 X
Dm9/C = X 3 3 2 5 X
E7/13- = 0 X 0 1 1 0
Em = 0 2 2 0 0 0
Em/D = X X 0 4 5 3
Em7/5- = X X 2 3 3 3
F#m = 2 4 4 2 2 2
F#m/E = X X 2 2 2 2
F#m7/5- = 2 X 2 2 1 X
F7/11+ = 1 X 1 2 0 X
