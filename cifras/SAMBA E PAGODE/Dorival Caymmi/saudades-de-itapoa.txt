Dorival Caymmi - Saudades de Itapoã

A7+                  Bm
Coqueiro de Itapoã, coqueiro
            E7     A7+
Areia de Itapoã, areia
                    Dm
Morena de Itapoã, morena
             E7       A7+
Saudade de Itapoã me deixa

Oh vento que faz cantiga nas folhas
    Bm
No alto do coqueiral
                        E7
Oh vento que ondula as águas

Eu nunca tive saudade igual
   A7/9-                                   A7+
Me traga boas notícias daquela terra toda manhã
   A7/9                                    A7+
E joga uma flor no colo de uma morena de Itapoã
 A7+                  Bm
Coqueiro de Itapoã, coqueiro

            E7     A7+
Areia de Itapoã, areia
                    Dm
Morena de Itapoã, morena
             E7       A7+
Saudade de Itapoã me deixa
    Bm    E7   A7+
Me deixa, me deixa...

----------------- Acordes -----------------
A7+ = X 0 2 1 2 0
A7/9 = 5 X 5 4 2 X
A7/9- = 5 X 5 3 2 X
Bm = X 2 4 4 3 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
