Dorival Caymmi - Maricotinha

D9(7)/C
Se fizer bom tempo amanhã
Se fizer bom tempo amanhã
E9(7)/D
Eu vou!...
D9(7)/C
Mas se por exemplo chover
Mas se por exemplo chover
Am7
Não vou!...(2x)
D7(9)
Diga a Maricotinha
Que eu mandei dizer
       D9(7)/C
Que eu não tô
    D7(9)
Não tô!
    D9(7)/C
Não vou!
    D7(9)
Não tô!

    D9(7)/C
Não vou!
D9(7)/C
Se fizer bom tempo amanhã
Se fizer bom tempo amanhã
E9(7)/D
Eu vou!
D9(7)/C
Mas se por exemplo chover
Mas se por exemplo chover
Am7
Não vou!...
D7(9)
Uma chuvinha, redinha
Cotinha
    D9(7)/C
Aí, piorou!
    D7(9)
Nem tô!
    D9(7)/C
Nem vou!
    D7(9)
Nem tô!
    D9(7)/C
Nem vou!
D9(7)/C
Se fizer bom tempo amanhã
Se fizer bom tempo amanhã
E9(7)/D
Eu vou!
D9(7)/C
Mas se por exemplo chover
Mas se por exemplo chover
Am7
Não vou!...
D7(9)
Diga a Maricotinha
Que eu mandei dizer
           D9(7)/C
Que eu não tô, hum!
    D7(9)
Não tô!
    D9(7)/C
Não vou!
    D7(9)
Não tô!
    D9(7)/C
Não vou!
D9(7)/C
Se fizer bom tempo amanhã
Se fizer bom tempo amanhã
E9(7)/D
Eu vou!
D9(7)/C
Mas se por exemplo chover
Mas se por exemplo chover
Am7
Não vou!
D7(9)
Uma chuvinha, redinha
Cotinha
    D9(7)/C
Aí, piorou! hum!
    D7(9)
Nem tô!
    D9(7)/C
Nem vou!
    D7(9)
Nem tô! hum!
    D9(7)/C
Nem vou!
D9(7)/C
Se fizer bom tempo amanhã
Se fizer bom tempo amanhã
D9(7)/C
Mas se por exemplo chover
Mas se por exemplo chover...(4x)

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
D7(9) = X 5 4 5 5 X
