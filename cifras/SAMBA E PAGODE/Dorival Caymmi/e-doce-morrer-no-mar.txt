Dorival Caymmi - É doce morrer no mar

( Dorival Caymmi e Jorge Amado )


  Dm       Dm/C    Gm/Bb  Gm7  Gm/F
É doce morrer   no mar
    A7/E     A7  A/G    Dm/F
Nas ondas verdes     do mar
  Dm       Dm/C    Gm/Bb  Gm7  Gm/F
É doce morrer   no mar
    A7/E     A7  A/G    Dm/F
Nas ondas verdes     do mar

  Dm        Dm/C       Gm/Bb        Gm
A noite que ele  não ve-----io, foi
Em7(b5)       A7       Dm/F
Foi    de tristeza pra mim
   Gm/Bb       Gm         Dm/F
Sa-vei---ro vol---tou so-zi---nho
       Gm/Bb                A  Bb/A  A  A7(b13)
Triste noi---te foi pra mim


  Dm       Dm/C    Gm/Bb  Gm7  Gm/F
É doce morrer   no mar
    A7/E     A7  A/G    Dm/F
Nas ondas verdes     do mar
(bis)

  Dm       Dm/C     Gm/Bb      Gm
Saveiro partiu de noi---te foi
Em7(b5)      A7         Dm/F   Gm/Bb  Gm    Dm/F
Ma-----druga-da, não voltou  O marinheiro bonito
    Gm/Bb              A  Bb/A  A  A7(b13)
Sereia    do mar levou

  Dm       Dm/C    Gm/Bb  Gm7  Gm/F
É doce morrer   no mar
    A7/E     A7  A/G    Dm/F
Nas ondas verdes     do mar
(bis)

    Dm       Dm/C       Gm/Bb         Gm
Nas ondas verdes do mar,      meu bem
Em7(b5)      A7     Dm/F
E|---------| le se foi afogar
    Gm/Bb    Gm       Dm/F      Gm/Bb
Fez su---a ca---ma de noivo no colo
           A7(b13)
De Iemanjá

Dm         Dm/C      Gm/Bb  Gm7  Gm/F
É doce morrer    no mar
  A7/E     A7    A/G    Dm/F
Nas ondas verdes     do mar
Dm         Dm/C    Gm/Bb  Gm7  Gm/F
É doce morrer   no mar
    A7/E      A7  A/G     Dm  Gm/Bb  A7/4  Dm
Nas ondas verdes       do mar

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/G = 3 X 2 2 2 X
A7 = X 0 2 0 2 0
A7(b13) = X 0 X 0 2 1
A7/4 = X 0 2 0 3 0
A7/E = 0 X 2 2 2 3
Bb/A = X 0 3 3 3 X
Dm = X X 0 2 3 1
Dm/C = X 3 X 2 3 1
Dm/F = X X 3 2 3 1
Em7(b5) = X X 2 3 3 3
Gm = 3 5 5 3 3 3
Gm/Bb = 6 5 5 3 X X
Gm/F = X X 3 3 3 3
Gm7 = 3 X 3 3 3 X
