Fundo de Quintal - Merece Respeito

[Intro] G  D7/9  G  C
        G  D7/9  G  C
        G

        Gº                Am
Nosso samba é mais forte resiste
                D7                  G7+
Mas há o que insiste em dizer que acabou
       Gº                   Am
Diz também que é coisa do passado
          D7                 Dm
Artigo esgotado, coisa sem valor

        G7/5+                 C#m5-/7
Há quem goste e finge que não gosta
               F7/9
E samba pelas costas
               G7+
Pra não dar o braço a torcer
          Gº                  Am
Há o que sente que o samba é quente

             D7                G7+ C7
Samba com a gente até o amanhecer
  F#m5-/7  B7
Ê ê ê
             Em7+       Em
Nosso samba merece respeito
Dm  G7/5+           C7+         Cm6   D7
É mais do que obra prima ou filosofia
            Dm   G7/13 G7/5+
Emoção pra valer
C7+         F7/9   G7+        Em
O samba é alegria, amor e poesia
  Am         D7        Dm
Que embala o povo a sonhar
           G7/13 G7/5+
Sempre a sonhar
C#m5-/7      F7/9
O samba me acalma
 G7+                 E7
É luz que envolve a alma
  A7       D7       G7+
O show tem que continuar
G7/13     C7+  F7/9     G7+    E7/9
Lalaia lalaia, lalaia lalaia lalaia
Am7         D7/9    Dm6  G7/5+
O show tem que continuar
     C#m5-/7  F7/9      G7+    E7/9
Lalaia lalaia, lalaia lalaia lalaia
A7          D7        G7+ G#7+
O show tem que continuar

( G  D7/9  G  C )
( G  D7/9  G  C )
( G  G7+ )

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
C#m5-/7 = X 4 5 4 5 X
C7 = X 3 2 3 1 X
C7+ = X 3 2 0 0 X
Cm6 = X 3 X 2 4 3
D7 = X X 0 2 1 2
D7 = X X 0 2 1 2
D7/9 = X 5 4 5 5 X
Dm = X X 0 2 3 1
Dm6 = X 5 X 4 6 5
E7 = 0 2 2 1 3 0
E7 = 0 2 2 1 3 0
E7/9 = X X 2 1 3 2
Em = 0 2 2 0 0 0
Em7+ = X X 2 4 4 3
F#m5-/7 = 2 X 2 2 1 X
F7/9 = X X 3 2 4 3
G = 3 2 0 0 0 3
G#7+ = 4 X 5 5 4 X
G7+ = 3 X 4 4 3 X
G7/13 = 3 X 3 4 5 X
G7/5+ = 3 X 3 4 4 3
Gº = 3 X 2 3 2 X
