Fundo de Quintal - É Ai Que Quebra a Rocha

 F
                        C7
Canta que eu pego a viola
                        F
E a viola eu faço chorar
                        C7
Canta que eu pego a viola
                        F         C7        F
E a viola eu faço chorar  canta aí  canta aí
                        C7
Canta que eu pego a viola
                        F
E a viola eu faço chorar
             C7       F
Me mandou calar (eu falo)
             C7       F
Me mandou sentar (levanto)
             C7       F
Me mandou falar (eu calo)
             C7       F
Mandou sufocar (eu canto)

                   C7
Só não posso ver criança chorar, porque
  F   C7                  F
Iaia....é aí que quebra a rocha
          C7                         F
Iaia....ô Iaia ...é aí que quebra a rocha
Solo: C7  F  C7  F  C7  F  C7  F  C7  F  C7  F
             C7       F
Me mandou recuar (avanço)
             C7       F
Me mandou errar  (acerto)
             C7       F
Me mandou parar (eu danço)
             C7       F
Mandou afrouxar (aperto)
                   C7
Só não posso ver criança chorar, porque
  F   C7                  F
Iaia....é aí que quebra a rocha
          C7                         F
Iaia....ô Iaia ...é aí que quebra a rocha
Solo: C7  F  C7  F  C7  F  C7  F  C7  F  C7  F

----------------- Acordes -----------------
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
