Fundo de Quintal - Doce Felicidade

 C
Introdução:   F7+ Fm6 Em7 A7 Dm G7 C7+ G7

    C7+                             E7
... Meu Deus valeu... o que você me fez achar
                Am         F
O alguém que eu viesse amar
                  Dm  G7
E abordasse o meu coração
    C7+                    E7
A perfeição  deu aquele sinal que sim
              Am        F
Disparando dentro de mim
          Dm    G7
O alarme da paixão
  C7+                    E7
Terminou  meu viver na escuridão
        Am                     Gm7       C7/13
A claridade então realçou o destino para dois
F7+    Fm6                Em7        A7
Revelou  simplesmente a razão de ser

                 Dm           G7
Com a expressão doce do prazer
                     C7+  Dm  G7
Não tenho tempo a perder
    C6/9       E7           E/G#
Se ontem eu chorei hoje e sorrir
    Am           Gm7           C7
O tempo que eu perdi, vou resgatar
F7+         Fm6    Em7        A7
Lado a lado com você numa só vida
Dm     Dm/C   G/B      G7
Ouviu bem    minha querida
  C6/9            E7        E/G#
Agora eu quero é mais ser feliz
     Am               Gm7        C7
Os lenços com meus prantos já desfiz
F7+             Fm6     Em7      A7
Pra você com afeto dou todo meu amor
         Dm   G7              C7+
Um céu azul, toda beldade da flor
   G7
Valeu!

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C6/9 = X 3 2 2 3 3
C7 = X 3 2 3 1 X
C7+ = X 3 2 0 0 X
C7/13 = X 3 X 3 5 5
Dm = X X 0 2 3 1
Dm/C = X 3 X 2 3 1
E/G# = 4 X 2 4 5 X
E7 = 0 2 2 1 3 0
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
F7+ = 1 X 2 2 1 X
Fm6 = 1 X 0 1 1 X
G/B = X 2 0 0 3 3
G7 = 3 5 3 4 3 3
Gm7 = 3 X 3 3 3 X
