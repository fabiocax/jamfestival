Fundo de Quintal - Brasil Nagô

Intro: Am7/9

 Am7/9  E7    Am7/9  Am7/9 Abm7/9 Gm7/9 C7 F7+ E7 Am7/9
O......o.......o.....o.....o......o....o....o....o....o
A7 Dm       G7           C7+        Em Ebm Dm
      Se mandarem me chamar eu vou
       G7            C7+      Bm5-/7
Sou brasileiro sou nação nagô
        E7          Am7/9 Abm7/9 Gm7/9  C7       F7+
Sou do sul sou do nordeste chimarrão cabra da peste
       E7            Am7/9
Sou valente eu sou paz e amor
A7 Dm        G7          C7+        Em Ebm Dm
      Levo a vida do jeito que for
    G7          C7+        Bm5-/7
Alegria riso choro e dor
         E7          Am7/9 Abm7/9  Gm7/9  C7    F7+
Eu sou branco eu sou negro viro o mundo pelo avesso
         E7                   Am7/9   A7 Dm
Tenho os pés no chão sou sonhador
          A7               Dm
Vou à procissão do santo padre

          A7               Dm
Saio da igreja entro nos bares
         A7                Dm
sob a proteção dos sete mares
      E7            Am7/9   A7 Dm
Peço axé ao meu babalaô
          A7        Dm              A7            Dm
Piano pandeiro ou viola baião rock samba nem dou bola
           A7             Dm
Tanto faz cachaça ou coca-cola
     E7              Am7/9         A7    Dm
Se mandarem me chamar eu tô (eu tô que tô)
     G7             C7+
Se mandarem me chamar...

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Abm7/9 = X X 6 4 7 6
Am7/9 = X X 7 5 8 7
Bm5-/7 = X 2 3 2 3 X
C7 = X 3 2 3 1 X
C7+ = X 3 2 0 0 X
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
Ebm = X X 1 3 4 2
Em = 0 2 2 0 0 0
F7+ = 1 X 2 2 1 X
G7 = 3 5 3 4 3 3
Gm7/9 = X X 5 3 6 5
