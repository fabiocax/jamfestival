Fundo de Quintal - Canto pra Velha Guarda

D                              A7  D
Velha, porém muitos anos de glória
    B7     Em7                F#7     Bm
A raiz do samba tem muitas histórias pra contar
A7   D   A7        D  %                Fo       Em7   B7/13
 O samba fez seu nome, tem nos anais muitos carnavais
Em7  F#m5/7             Em7 F#m5/7                 Em7
Foi, o canto desses maiorais canto de nossos ancestrais
G      G#0    D6/9
Foi e ainda é
F#m5/7                      B7         F#m5/7
Deixem que essa raiz venha nos ensinar
              B7                             Em7
Um lindo que faz enbalar, principalmente a alma
  Gm6      D                 B7
E então caindo nas graças do povo
Em7             A7      Am   D7
Um samba velho é sempre novo
G    G#0   D                  B7
E então caindo nas graças do povo
 Em7              A7       D
Um samba velho é sempre novo

          A7
A velha guarda
          D                    B7       Em7
*A velha guarda é quem guarda nossa bandeira
          A7                          D
Quando presente nas rodas de samba, é lenha na foqueira
   F#m5/7       B7            Em7
E queima sem cessar um só instante
         E7            Em5/7  A7
E tão marcante é brasileira
      D                    B7      Em7
A mocidade é quem sabe o valor da mesma
            A7                                  D
E traz no orgulho a verdade de bambas, e ser velha ela almeja

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
B7/13 = X 2 X 2 4 4
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D6/9 = X 5 4 2 0 0
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
Em7 = 0 2 2 0 3 0
F#7 = 2 4 2 3 2 2
G = 3 2 0 0 0 3
Gm6 = 3 X 2 3 3 X
