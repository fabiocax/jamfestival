Fundo de Quintal - Lucidez

(Para CAVACO)

E     F#
...     Ueraia
 F#m      B7 E
Ueraraue... ueraia...
 E
Por favor
             F#
Não me olhe assim
      F#m
Se não for
     B7          E
Vou viver só para mim

Aliás
              F#
Se isto aconteceu
      F#m
Tanto faz
       B7         E
Já me fiz por merecer

                F#
Mas cuidado não vá se entregar
               F#m      B7
Nosso caso não pode vazar
                 E
É tão bom se querer
     G#        A           Am
Sem saber como vai ter..mi..nar
G#m     C#7 F#m
Onde a lucidez se aninhar
Am
Pode deixar
G#m      C#7  F#m
Quando a solidão apertar
Am          D7
Olhe pro lado
G#m       C7   B7
Olhe pro lado
                   E                      F#           F#m           B7       E
Que eu estarei por láláiááááááá láláiáláááiáááááá  láláiááááááá  Láláiáláláláiáááááááá

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
C#7 = X 4 3 4 2 X
C7 = X 3 2 3 1 X
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
G# = 4 3 1 1 1 4
G#m = 4 6 6 4 4 4
