Fundo de Quintal - Conselho Amigo

Dm
Dá tudo de si
                                 Gm
Pra que eu de novo lhe possa ver sorrir
Faz por merecer
A7                            D7
Este novo amor que morre por você
Gm          C7
Abre o coração
                   F7+    A7     Dm
Deixando entrar gostoso essa paixão
                   Gm7
Que é todo o meu prazer e por que não
A74  A7              D  B7  E7  A7
Tenho você como se fosse meu irmão
D     Em7     F#m
Tenta se redimir
Em7     F#m          B7               Em  B7
A ter a paz morando mais nesse teu coração
Em  G(#5)             G
Faça este alguém sorrir

G5+        G          A7                 D    A7
E nunca é tarde pra saber o que é uma gratidão
D          Em7     F#m
Quem prossegue assim terá
Am                  D7           G7+  G6
Felicidade, afinidade de um bem querer
Gm        C7       F#m        B7
O eterno amor vivendo lado a lado
Em           A7    Am  D7
Para amar e ser amado

Dm
Dá tudo de si
                                 Gm
Pra que eu de novo lhe possa ver sorrir
Faz por merecer
A7                            D7
Este novo amor que morre por você
Gm          C7
Abre o coração
                   F7+    A7     Dm
Deixando entrar gostoso essa paixão
                   Gm7
Que é todo o meu prazer e por que não
A74  A7              D  B7  E7  A7
Tenho você como se fosse meu irmão
D     Em7     F#m
Tenta se redimir
Em7     F#m          B7               Em  B7
A ter a paz morando mais nesse teu coração
Em  G(#5)             G
Faça este alguém sorrir
G5+        G          A7                 D    A7
E nunca é tarde pra saber o que é uma gratidão
D          Em7     F#m
Quem prossegue assim terá
Am                  D7           G7+  G6
Felicidade, afinidade de um bem querer
Gm        C7       F#m        B7
O eterno amor vivendo lado a lado
Em           A7    Am  D7
Para amar e ser amado

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A74 = X 0 2 0 3 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
C7 = X 3 2 3 1 X
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F#m = 2 4 4 2 2 2
F7+ = 1 X 2 2 1 X
G = 3 2 0 0 0 3
G(#5) = 3 X 1 0 0 X
G5+ = 3 X 1 0 0 X
G6 = 3 X 2 4 3 X
G7+ = 3 X 4 4 3 X
Gm = 3 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
