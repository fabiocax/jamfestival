Fundo de Quintal - Cadê Ioiô

  F           F
Dona Fia, Dona Fia
     F          C7
Dona Fia, cadê Iô-iô
        F   D7
Cadê Iô-iô
     Gm7          C7
Cadê Iô-iô, Dona Fia
       F
Cadê Iô-iô
  D7     G7   C7            F
Cadê, cadê..........cadê Iô-iô
  D7     G7   C7            F
Cadê, cadê..........cadê Iô-iô

D7                Gm7
Iô-iô é moleque maneiro
             C7                F
Vem lá do Salgueiro e tem seu valor
                Cm                  F7             Bb7+
Toca cavaco, pandeiro e no partido alto é bom versador

                        Bb7+
Ê! quem sabe o seu paradeiro
      Bbm6               Am7
É o pandeiro, cavaco e tantã
            D7            G7
Quando ele encontra a rapaziada
            C7        F
Só chega em casa de manhã

Refrão:
    D7               Gm7
Quero encontrar esse bamba
              C7                F
Que em termos de samba é sensacional
                Cm
Para alegrar o pagode
             F7                     Bb
Que estou preparando lá no meu quintal
                       Bbm
Ih! foi num samba pra frente
            Eb7                  Am7
Que eu vi um valente versar pra Iô-iô
  D7              Gm7
Mas ele estava indecente
             C7                 F
Deixando o malandro de pomba-rolô

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Bb = X 1 3 3 3 1
Bb7+ = X 1 3 2 3 1
Bbm = X 1 3 3 2 1
Bbm6 = 6 X 5 6 6 X
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
Eb7 = X 6 5 6 4 X
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
G7 = 3 5 3 4 3 3
Gm7 = 3 X 3 3 3 X
