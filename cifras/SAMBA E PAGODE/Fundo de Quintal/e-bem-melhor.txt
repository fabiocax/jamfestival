Fundo de Quintal - É Bem Melhor

D B7 Em A7 D

É bem melhor parar com esse amor de fingimento
Está fazendo do nosso viver um eterno tormento
Tudo lhe dei e você não soube me corresponder
É bem melhor parar porquê.
Eu não sou masoquista já lhe tirei da lista
Eu abri minha vista num momento fatal
Da minha liberdade você sempre abusou
Porque eu era cego por esse falso amor

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
