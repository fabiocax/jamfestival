Fundo de Quintal - O Nó da Gravata

 Em
Introdução:  Em Em Am D7 G7+ E7 Am B7 Em C7 B7
     Em               E7             Am
Pode ser que eu não tenha te amado direito
                               D7
Mas se a gente encarar do meu jeito
                            G7+         F#m7/5- B7
Você vai perceber que estou certo afinal
     Em       E7                Am
Meu amor por você é uma carta marcada
                        D7
Você sabe que ganha a parada
                    G7+        Bm7/5- E7
Abusando de mim e pagando geral
     Am              D7              G7+
Pode ser que eu esqueça presentes e datas
         Em                  Am
Que eu afrouxe esse nó da gravata
      B7               Em       E7
E que pare na roda de amigos no bar
     Am          D7                   G7+
Pode ser, mas o resto é uma rede de intrigas

      Em                    Am
Dessa gente que quer nossas brigas
      B7            Em
E só pensa em nos separar
   E7            Am                    D7
Se você tá me querendo me chama que eu vou
   G7+               E7
Esquece, o pior já passou
     Am                 B7
Me abraça e me fala de amor
   Em
Perdoa
         E7       Am                    D7
Eu não posso mais ficar desse jeito que estou
  G7+                 E7
Sozinho e carente de amor
     Am               B7     Em  B7
Eu quero é ficar com você na boa

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
Bm7/5- = X 2 3 2 3 X
C7 = X 3 2 3 1 X
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F#m7/5- = 2 X 2 2 1 X
G7+ = 3 X 4 4 3 X
