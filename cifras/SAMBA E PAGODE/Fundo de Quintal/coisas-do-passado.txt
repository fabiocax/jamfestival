Fundo de Quintal - Coisas do Passado

(intro) C G7 C G7 C C7 F Fm C Ebº Dm G7

C             F                 C
Faz tanto tempo não te vejo
        Gm         C7           F
Pensei até em matar meus desejos
        Fm                      Bb7          Cm7
Talvez num abraço ou quem sabe num beijo
                D#m7/5-
De novo encontrar a saudade
G7                    C
Recomeçar pelo fim
        Gm      C7              F
E rebuscar os caminhos que um dia
Fm              Bb7          Cm7        G7
Me fizeram passar por carinhos que eu pouco conhecia
           C
Amor já faz
C7                      F                       Bb7
Tanto tempo que eu procuro encontrar a minha paz
                    Cm7                    Ab7+
Pois a vida sem você doeu doeu e dói demais

    D#m7/5-                          G7
Mas você segue sorrindo nem sequer olha pra traz
                C
Porque já faz
C7                      F                       Bb7
Tanto tempo nesse escuro já nem sei se vou te olhar
                    Cm7                    Ab7+
Sem lembrar que por você chorei chorei até cansar
    D#m7/5-
Mas são coisas do passado
                           G7
Vou deixar de lado o pranto
                                C
E vou correndo te buscar

(repete tudo)

----------------- Acordes -----------------
Ab7+ = 4 X 5 5 4 X
Bb7 = X 1 3 1 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Cm7 = X 3 5 3 4 3
D#m7/5- = X X 1 2 2 2
Dm = X X 0 2 3 1
Ebº = X X 1 2 1 2
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
