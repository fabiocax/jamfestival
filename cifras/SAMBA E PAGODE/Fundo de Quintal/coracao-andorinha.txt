Fundo de Quintal - Coração Andorinha

 D
Introdução: D
       D           B7        Em
Teu coração é uma pena que voa
        A7         D        A7
Teu coração é uma pena que voa
      D                           B7           Em
Que guarda misterio profundo num grande oceano
                             A7
Não deixa raizes  é mesmo cigano
                             D   G7
Que deixa marcar por onde passar
    F#7                               Bm
Às vezes é um tema alegre de uma canção
    E7/4    E7                   A7   D7
Em outra faz muitos prantos rolar
      G            Gm                     D
Que pode guardar pesadelos em sueus desenganos
          B7               Em
Ou sonhos bonitos que tá sonhando
          A7                 D
Até pede a Deus para não despertar

      D                      B7          Em
Um dia é a chama ardente de um carnaval
                                         A7
Em outro fica com jeito de assim mais formal
                               D   G7
Com jeito de festa em nobre salão
   F#7                                Bm
Pena que pode escrever um poema de amor
   E7/4   E7              A7  D7
Ou cenas ... de uma disilusão
    G                      Gm            D
Mas toda andorinha um dia também tem saudade
      B7                   Em
E volta correndo para sua cidade
          A7             D            A7
E a tua cidade é o meu coração

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
E7/4 = 0 2 0 2 0 X
Em = 0 2 2 0 0 0
F#7 = 2 4 2 3 2 2
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
