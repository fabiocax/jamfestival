Fundo de Quintal - Cantei Só Pra Distrair / Cadê Ioiô

      C7
Eu cantei !

F       Gm7            Am7     C7
Eu, cantei só pra distrair

        F               Dm             Gm7
Aquelas lágrimas que eu já derramei por ti

  C    C#°              Dm
Amei.......e não fui feliz

  F            G7                    Gm7    C7
Mas eu vou deixar de amar, para não chorar

    Gm7             C7  F
Cantando assim.....eu vivo

  Cm               B7/9   Bb7+
Sorrindo, vou me dis....traindo


  Bb7+  Bbm6     Am7          D7
Morreu...........a minha alegria

 Gm7        C7           F        F7
Minha companheira de orgia, mas morreu !

   Bb7+  Bbm6     Am7           D7
Morreu...........a minha alegria

  Gm7        C7          F
Minha companheira de orgia


*

  D7                  Gm7
Pagode de Hélio dos Santos

             C7                     F
Que em todos os cantos bota pra quebrar

                       Cm
Mostrando que é lá da Serrinha

              F7               Bb7+
E todos os são bambas naquele lugar

                Bb7+
Este samba da antiga

            Bbm6              Am7
Que alegra a vida e não tem idade

  D7               Gm7
Desce pra cá do asfalto

         C7               F
E vem alegrar o povo da cidade


Volta ao início até no "*"


refrão

    F           F
Dona Fia, Dona Fia

     F          C7
Dona Fia, cadê Iô-iô

        F   D7
Cadê Iô-iô

     Gm7          C7
Cadê Iô-iô, Dona Fia

       F
Cadê Iô-iô

  D7     G7   C7            F
Cadê, cadê..........cadê Iô-iô

  D7     G7   C7            F
Cadê, cadê..........cadê Iô-iô


D7                Gm7
Iô-iô é moleque maneiro

             C7                F
Vem lá do Salgueiro e tem seu valor

                Cm                  F7             Bb7+
Toca cavaco, pandeiro e no partido alto é bom versador

                        Bb7+
Ê ! quem sabe o seu paradeiro

      Bbm6               Am7
É o pandeiro, cavaco e tantã

            D7            G7
Quando ele encontra a rapaziada

            C7        F
Só chega em casa de manhã


Refrão *


    D7               Gm7
Quero encontrar esse bamba

              C7                F
Que em termos de samba é sensacional

                Cm
Para alegrar o pagode

             F7                     Bb
Que estou preparando lá no meu quintal

                       Bbm
Ih ! foi num samba pra frente

            Eb7                  Am7
Que eu vi um valente versar pra Iô-iô

  D7              Gm7
Mas ele estava indecente

             C7                 F
Deixando o malandro de pomba-rolô


Refrão.. *

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
B7/9 = X 2 1 2 2 X
Bb = X 1 3 3 3 1
Bb7+ = X 1 3 2 3 1
Bbm = X 1 3 3 2 1
Bbm6 = 6 X 5 6 6 X
C = X 3 2 0 1 0
C#° = X 4 5 3 5 3
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Eb7 = X 6 5 6 4 X
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
G7 = 3 5 3 4 3 3
Gm7 = 3 X 3 3 3 X
