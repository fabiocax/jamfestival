Fundo de Quintal - Saber Viver

 Intro: Eb,D#m,Dm,G7,Cm,F7,Bb,F7

  Bb Bb6     Em5/7-    A7        Dm
 Fantástico, é saber curtir a vida,
  G7           Cm               F#7
 Se libertar com prazer, vivendo os sonhos...
         F7        Bb    Cm6 D7
 com seu bem querer, (sorrir)
 Gm        D7/A
 Sorrir, cantar, lalaia
 G7           Cm                 F#7
 saborear o doce mel, sentir no peito
         F7      Bb
 nosso papai do céu, (repete a partir do Sorrir)(depois D7)
 Gm               Gm5+    Bb6              Em5/7-
 Irmão na vida tudo passa, assim  que nem fumaça,
 F#7 F7    Bb D7     Gm          Gm5+
 perdida no ar, entre lamento e sofrimento,
 Bb6           Em5/7-             F#7 F7    Bb F7
 mais vale o divertimento que se lamentar, corra.
 Bb Bb6                      C7
 Corra, corra atrás do seu ar puro,

                       F#7 F7     Bb   F7 F#7 G7 G#7
 faça mole o caminho duro sem se lamentar,
                          Gm7/9                    Em5/7-
 aguente o ferimento dos espinhos sem deixar que os inflamem
                   F#7 F7
 deixando correr na veia,
 Bbm    F7 Bbm    F7 F#7 G7 G#7
 a fé que incendeia,
                          C#7+
 para chegar o fim  de uma longa caminhada,
        F#7 F7    Bbm    F7
 e ter feliz jornada, a fé...
 Bbm    F7 Bbm   F7 F#7 G7 G#7
 a fé que incendeia,
                           C#7+
 para chegar ao fim de uma longa caminhada,
        F#7 F7      Bb   F7/4 F7
 e ter feliz jornada...(fantástico)
 *final:
 Bbm  F7   Bbm      Bbm   F7   Bbm
 a fé que incendeia, a fé que incendeia ...

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bb = X 1 3 3 3 1
Bb6 = X 1 3 0 3 X
Bbm = X 1 3 3 2 1
C#7+ = X 4 6 5 6 4
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
Cm6 = X 3 X 2 4 3
D#m = X X 1 3 4 2
D7 = X X 0 2 1 2
D7/A = 5 X 4 5 3 X
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
F#7 = 2 4 2 3 2 2
F7 = 1 3 1 2 1 1
F7/4 = 1 3 1 3 1 X
G#7 = 4 6 4 5 4 4
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
Gm5+ = 3 6 5 3 4 3
Gm7/9 = X X 5 3 6 5
