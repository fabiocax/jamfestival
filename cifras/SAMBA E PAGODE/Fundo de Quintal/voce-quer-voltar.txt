Fundo de Quintal - Você Quer Voltar

D

 D6/9         C#7
Você quer voltar
  C7     B7           Em
Mas não lhe darei perdão
 Gm  C7           F#m
Eu, não posso perdoar
 B7        E7 A7       D6/9
A quem magoou meu coração
D6/9 C#7 F#m                           C#7 F#m
        Se você me ouvisse não aconteceria
                           C#7 C7 Em
Entre nós a ingrata separação
               B7                  Em
Mas você insistiu em fazer tudo errado
E7                             A#7 A7
E com isso acabou com nossa união

----------------- Acordes -----------------
A#7 = X 1 3 1 3 1
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
C#7 = X 4 3 4 2 X
C7 = X 3 2 3 1 X
D = X X 0 2 3 2
D6/9 = X 5 4 2 0 0
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
Gm = 3 5 5 3 3 3
