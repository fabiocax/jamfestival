Fundo de Quintal - O Neguinho e a Senhorita

A     E7
  O Neguinho,
    A                E7         A
O Neguinho gostou da filha da Madame
          F#7          Bm
Que nós tratamos de sinhá
Senhorita também gostou do
  E7
Neguinho
Mas o Neguinho não tem dinheiro pra
  A
Gastar
                           F#7
A Madame tem preconceito de cor
    Bm
Não pôde evitar esse amor
     D         E7           A     F#7
Senhorita foi morar lá na Colina
         Bm       E7        A
Com o Neguinho que é compósito
    E7
  O Neguinho,

    A                E7         A
O Neguinho gostou da filha da Madame
          F#7          Bm
Que nós tratamos de sinhá
Senhorita também gostou do
  E7
Neguinho
Mas o Neguinho não tem dinheiro pra
  A
Gastar
                           F#7
A Madame tem preconceito de cor
    Bm
Não pôde evitar esse amor
     D         E7           A     F#7
Senhorita foi morar lá na Colina
         Bm       E7    A     A7
Com o Neguinho que é compósito
    D           E7          A
Senhorita foi morar lá na Colina
         Bm       E7    A
Com o Neguinho que é compósito

   Bm               E7        A
Senhorita ficou com nome na história
       C#              F#m
E agora é a rainha da escola
            D           Dm
Gostou do samba e hoje vive muito
 A    F#7
Bem
     Bm          E7     A
Ela devia nascer pobre também
            D           Dm
Gostou do samba e hoje vive muito
 A    F#7
Bem
     Bm          E7     A
Ela devia nascer pobre também

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
C# = X 4 6 6 6 4
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
F#7 = 2 4 2 3 2 2
F#m = 2 4 4 2 2 2
