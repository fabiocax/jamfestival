Fundo de Quintal - Ciranda do Povo

Introdução: E6/9

    A                 F#7    Bm
Já não é conversa de um ou dois
    Bm5-/7            E7        A   A7+
Sem essa de “vamo deixar pra depois"
  A6              Cº     Bm
É um desejo que está cravado em nossa crença
 E7                            A7+               E7
Real feito enchente, morte....seca, escândalo, doença
   C#m5-/7            F#7      Bm
É como se os trens lotados clamassem a cada manhã
  E7                      A              E7
Igual ao golpe de gol no peito do Maracanã
        C#m5-/7     F#7           Bm
Se os gritos de incêndio louvam a água ao invés do fogo
   E7                      A7+             E7
Desobedecer as regras, às vezes melhora o jogo
     Am                   Dm
Que nem a greve geral, parando  para movimentar
   G7                    C                E7
Ressaca pulverizando as pedras no quebra-mar

      Em5-/7           A7        Dm
Tal qual a explosão bonita, nos dias de carnaval
    G7                        C            E7
Fervor de sobrevivência das feras do pantanal
    A                Cº        Bm
Clarão de milho invadindo o escuro dos celeiros
    E7                               A            E7
Milhões de grãos refulgindo entre as unhas dos mineiros
 C#m5-/7            F#7           Bm
Co9mo se os caminhoneiros transportassem nova carga
 E7                            A           A7+
Com a memória  e o futuro buzinando nas estradas
   G7           F#7         Bm
O bêbado muito louco, fica sóbrio de emoção
  E7                       A               A7+
A equilibrista solta sombrinha e vem pro chão
   G7                 F#7        Bm          Dm
O povo abre a roda e dança....aqui, ali, acolá
A                E7     A                E7
Uma só voz na ciranda, canta pra melhorar
   A              Bm    C#m  D7+
Do Oiapoque ao Chuí, ciranda
   Bm              E7   A  A7+
Ciranda povo sem fraquejar
    A6            F#7      C#m5-/7   F#7
De Marajó aos confins dos Pam.......pas
   B7             E7   A   E7
Ciranda povo pra melhorar..

----------------- Acordes -----------------
A = X 0 2 2 2 0
A6 = 5 X 4 6 5 X
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
Bm5-/7 = X 2 3 2 3 X
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
C#m5-/7 = X 4 5 4 5 X
Cº = X 3 4 2 4 2
D7+ = X X 0 2 2 2
Dm = X X 0 2 3 1
E6/9 = X 7 6 6 7 7
E7 = 0 2 2 1 3 0
Em5-/7 = X X 2 3 3 3
F#7 = 2 4 2 3 2 2
G7 = 3 5 3 4 3 3
