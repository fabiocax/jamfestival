Fundo de Quintal - Falso Herói

[Intro] E7+  F#m7  G#m7  A7+  B7/13
        E7+/9

E7+/9            Am6  B7/13  E7+/9
  Eu já não poderi----a   viver
             Am6  B7/13  E7+/9
Outra desilusão   e     ver
                 Am6  B7/13  E7+/9
Ver mais uma paixão, mor----rer
                   Am6  B7/13  E7+/9
Outra vez dar sem re---ce-----ber

E7+/9            Am6  B7/13  E7+/9
  Eu já não poderi----a   viver
             Am6  B7/13  E7+/9
Outra desilusão   e     ver
                 Am6  B7/13  E7+/9
Ver mais uma paixão, mor----rer
                   Am6  B7/13  E7+/9  G#m7  C#7/9
Outra vez dar sem re---ce-----ber


Ab7+             A#m7
  Mais um ponto final
                    Ab7+  Ab6  G#m7
Fim de mais uma história
                  C#7
De que vale me achar
                  F#7+ F#6
Pra depois me perder
Bm                  E7
  Meu amor falso herói
                A7+  A6  Am7
Leva fama sem glória
                     D7
O que os olhos enxergam
               G7+  G6
O coração não vê

     F#m5-/7       B7          Em7  G7/13
Não vê talvez por ter medo da dor
F#m5-/7        B7            E7+
  Coração teu mal é mal de amor
         D#m7/5-              G#7
Eu não quero outro beijo mal dado
             C#m7+  C#m7
Esse amargo sabor
         Bm                      E7/9
Não vou mais me perder em seus braços, abraços

     Bbm5-/7     Am6       G#m7       Gº
De quem não me quis assim como eu já fiz
       F#m7     B7       Bm7  E7
Porque isso não é ser feliz
     Bbm5-/7     Am6       G#m7       Gº
De quem não me quis assim como eu já fiz
       F#m7     B7       E7+/9
Porque isso não é ser feliz

E7+/9            Am6  B7/13  E7+/9
  Eu já não poderi----a   viver
             Am6  B7/13  E7+/9
Outra desilusão   e     ver
                 Am6  B7/13  E7+/9
Ver mais uma paixão, mor----rer
                   Am6  B7/13  E7+/9  G#m7  C#7/9
Outra vez dar sem re---ce-----ber

Ab7+             A#m7
  Mais um ponto final
                    Ab7+  Ab6  G#m7
Fim de mais uma história
                  C#7
De que vale me achar
                  F#7+ F#6
Pra depois me perder
Bm                  E7
  Meu amor falso herói
                A7+  A6  Am7
Leva fama sem glória
                     D7
O que os olhos enxergam
               G7+  G6
O coração não vê

     F#m5-/7       B7          Em7  G7/13
Não vê talvez por ter medo da dor
F#m5-/7        B7            E7+
  Coração teu mal é mal de amor
         D#m7/5-              G#7
Eu não quero outro beijo mal dado
             C#m7+  C#m7
Esse amargo sabor
         Bm                      E7/9
Não vou mais me perder em seus braços, abraços

     Bbm5-/7     Am6       G#m7       Gº
De quem não me quis assim como eu já fiz
       F#m7     B7       Bm7  E7
Porque isso não é ser feliz
     Bbm5-/7     Am6       G#m7       Gº
De quem não me quis assim como eu já fiz
       F#m7     B7       E7+/9
Porque isso não é ser feliz

E7+/9            Am6  B7/13  E7+/9
  Eu já não poderi----a   viver

----------------- Acordes -----------------
A#m7 = X 1 3 1 2 1
A6 = 5 X 4 6 5 X
A7+ = X 0 2 1 2 0
Ab6 = 4 X 3 5 4 X
Ab7+ = 4 X 5 5 4 X
Am6 = 5 X 4 5 5 X
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
B7/13 = X 2 X 2 4 4
Bbm5-/7 = X 1 2 1 2 X
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
C#7 = X 4 3 4 2 X
C#7/9 = X 4 3 4 4 X
C#m7 = X 4 6 4 5 4
C#m7+ = X 4 6 5 5 4
D#m7/5- = X X 1 2 2 2
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
E7+ = X X 2 4 4 4
E7+/9 = X 7 6 8 7 X
E7/9 = X X 2 1 3 2
Em7 = 0 2 2 0 3 0
F#6 = 2 X 1 3 2 X
F#7+ = 2 X 3 3 2 X
F#m5-/7 = 2 X 2 2 1 X
F#m7 = 2 X 2 2 2 X
G#7 = 4 6 4 5 4 4
G#m7 = 4 X 4 4 4 X
G6 = 3 X 2 4 3 X
G7+ = 3 X 4 4 3 X
G7/13 = 3 X 3 4 5 X
Gº = 3 X 2 3 2 X
