Fundo de Quintal - Momento Infeliz

Introdução: Gm Gm/F Eb7 D7 Gm Gm/F Eb7 D7 Gm D7

     Gm          G7       Cm            D7              Gm      D7
Por culpa de um momento infeliz a nossa paz quase foi abalada
    Gm      G7               Cm              D7                     G           D7
Senti profundamente o que eu fiz más não faz mal isso não há de ser nada  (por culpa)
    Gm           G7         Cm          D7              Gm     D7
Por culpa de um momento infeliz a nossa paz quase foi abalada
  Gm         G7              Cm              D7                      Gm
Senti profundamente o que eu fiz más não faz mal isso não há de ser nada
           G     G°    G             E79            Am  E7
Procurei compor uma canção tentar esquecer o que se deu
   Am                 D7                   Am D7          Gm
Preciso aliviar meu coração apagar de uma vez o que aconteceu
            G    G°     G              E79            Am E7
Procurei compor  uma canção tentar esquecer o que se deu
    Am                 D7                 Am D7           Gm    D7 Gm
Preciso aliviar meu coração apagar de uma vez o que aconteceu
       G7             Cm          D7                       Gm79     D7   Gm
Vamos supor que aquele dia não surgiu o sol se pôs e até a lua se cobriu
         Gm/F           Cm                       Eb7         D7      G            D7
Afogue as mágoas sem ter medo de errar e de mãos dadas hoje vamos passear

          Eb7      D7         Gm/F
E de mãos dadas hoje vamos passear    (4x)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
E79 = X X 2 1 3 2
Eb7 = X 6 5 6 4 X
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
Gm/F = X X 3 3 3 3
Gm79 = X X 5 3 6 5
G° = 3 X 2 3 2 X
