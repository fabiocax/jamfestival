Fundo de Quintal - Só Felicidade

Fundo de Quintal  Só Felicidade -

Intro: D/A , G/D,  D/A , G/D, D/A , G/D,  D/A , G/D G/A

     D7M                                      G7M
To feliz, em saber que você vai bem
           D7M                                        Am7   B7
Ter problema é normal, todo mundo tem.
        Em7                             A7    C#m5-/7                         F#7
Mas tem que saber Contornar,     Tem que aprender resolver
Bm7                             G/A
Nunca se desesperar, Esse exemplo é você.

     D7M                                     G7M
E agora que esse vendaval passouâouuuuu
      D7M                                    Am7   B7
E aquele semblante de paz voltou
           Em7               A7       C#m5-                    F#7
Não deixe de agradecer,     DEUS em primeiro Lugar
Bm7                            G/A                        D7M
O Sofrimento acabou, Hoje é só comemorar


D7                   G7M     G6            A7   A/G
Já que você tá tão    feliz, dá um grito
            F#m             Am7
Solta o riso, não disfarça
D7                   G7M   G6          A7   A/G
Já que você tá tão   feliz, faz a festa
           F#m              Am7 D7
Cai no samba, Extravasa.
D7                   G7M     G6            A7   A/G
Já que você tá tão    feliz, dá um grito
            F#m             Am7
Solta o riso, não disfarça
D7                   G7M   G6          A7   A/G
Já que você tá tão   feliz, faz a festa
           F#m              Am7 D7        G  Bb7  A7
Cai no samba, Extravasa.

----------------- Acordes -----------------
A/G = 3 X 2 2 2 X
A7 = X 0 2 0 2 0
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
Bb7 = X 1 3 1 3 1
Bm7 = X 2 4 2 3 2
C#m5-/7 = X 4 5 4 5 X
D/A = X 0 X 2 3 2
D7 = X X 0 2 1 2
D7M = X X 0 2 2 2
Em7 = 0 2 2 0 3 0
F#7 = 2 4 2 3 2 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G/A = 5 X 5 4 3 X
G/D = X 5 5 4 3 X
G6 = 3 X 2 4 3 X
G7M = 3 X 4 4 3 X
