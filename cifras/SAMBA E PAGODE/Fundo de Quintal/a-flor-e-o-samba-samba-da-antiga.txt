Fundo de Quintal - A Flor e o Samba/samba da Antiga

Intro: C A7 Dm  G7

             C  A7              Dm       G7        C  G7
Vem sambar Iaia... vem sambar  Ioio     Iaia ... Ioio   2x
     C                 A7                Dm
Não tenho dinheiro só tenho pandeiro e viola
            G7                             C   G7
Mas vem depressa pro meu samba que ele consola viu
     C              A7                Dm
Menina pra que desamor veja quanta beleza
    G7                                  C   G7
O samba assim como a flor também é natureza
     C                 A7                Dm
Só vive pior quem não vai sambar na avenida
   G7                                      C   G7
O samba é o tesouro maior que se deixa na vida
     C            A7                   Dm
O samba é a liberdade sem sangue e sem guerra
      G                                  C   G7
Quem samba de boa vontade tem paz nessa terra


         A7     Dm                    G7           C
Vem pra roda menina, mexer com as cadeiras vem sambar
                     A7           Dm
Vem mexer com as cadeiras, vem sambar
                     G7           C
Vem mexer com as cadeiras, vem sambar
       A7          Dm             G7           C
Esse samba é da antiga, de gente amiga, vem sambar
                     A7           Dm
Vem mexer com as cadeiras, vem sambar
                     G7           C
Vem mexer com as cadeiras, vem sambar
A7            Dm                   G7               C
A idade não importa, a cor da tua pele não me interessa
             A7                 Dm
Se tem perna torta se tem perna certa
       G7                    C
Basta saber se tem samba na veia
A7            Dm                   G7               C
O samba veio de longe, hoje está na cidade, hoje esta nas aldeias
              A7            Dm
Nasceu no passado, vive no presente
                G7              C
Quem samba uma vez samba eternamente
  A7   Dm G7   C       A7   Dm G7   C
Iaia Iaia...Ioio.....Iaia Iaia...Ioio.....
                     A7           Dm
Vem mexer com as cadeiras, vem sambar
                     G7           C
Vem mexer com as cadeiras, vem sambar
                     A7           Dm
Vem mexer com as cadeiras, vem sambar
                     G7           C
Vem mexer com as cadeiras, vem sambar

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
