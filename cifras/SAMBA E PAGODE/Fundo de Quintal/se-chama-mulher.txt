Fundo de Quintal - Se Chama Mulher

Dm
Introdução:  G#° B° Am5-/7 D7 G7 C7 F
Dm            G#°  B°             D#7  D7
Pode ser uma dama...pode ser da ralé
                    G6/7  G5+ Gm
Mas se o peito tem chama
     C7       F          Dm
Seu nome é mulher, mulher
             G#°    B°      D#7       D7
Pode ser uma dama...pode até ser da ralé
                    G6/7  G5+ Gm
Mas se o peito tem chama
    C7     F    D#7  D7
Se Chama mulher
Gm                C7         Am5-/7
Eva nasceu da costela de Adão
                 D7
E inventou a tal da tentação
 Gm               C7            F   D#7 D7
Morda a maçã e verá o que é paraíso
 Gm                  C7          Am5-/7
Hoje a mulher é quem dá a decisão

                 D7
Sabe o sim, o talvez e o não
 Gm        Am        Bb       C7      F
Sabe dar lucro e também sabe dar prejuizo
 Gm               C7          Am5-/7
E só lembrar da Dalila e Sansão
                 D7
E só lembrar Salomé e João
 Gm               C7                F   D#7 D7
O homem perde o cabelo e também o juízo
 Gm                   C7          Am5-/7
Hoje quem manda é a sedução
                D7
Hoje a fêmea domina o machão
 Gm        Am        Bb       C7      F
Com seu olhar, seu andar o seu lindo sorriso
 Gm                  C7          Am5-/7
Quando a Maria é bonita então
                     D7
Acende a luz de qualquer lampião
 Gm            C7                F   D#7 D7
Vira poeta esquece seu lado bandido
 Gm                    C7          Am5-/7
Atrás de um homem que tem projeção
                 D7
Na retaguarda de um campeão
 Gm        Am        Bb       C7      F
Só a mulher faz o homem ser bem sucedido

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am5-/7 = 5 X 5 5 4 X
Bb = X 1 3 3 3 1
B° = X 2 3 1 3 1
C7 = X 3 2 3 1 X
D#7 = X 6 5 6 4 X
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G#° = 4 X 3 4 3 X
G5+ = 3 X 1 0 0 X
G6/7 = 3 X 3 4 5 X
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
