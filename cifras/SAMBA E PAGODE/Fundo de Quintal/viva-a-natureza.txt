Fundo de Quintal - Viva a Natureza

 G
Introdução: G7+ G6 G7+ E7 Am7 Cm G7+ D7

G7+          Am7
Lua cheia vento forte
G7+        E7         Am7
Foi felicidade e sorte
         D7
Para nós que alegria
G7+            Am7
Nos livrarmos de uma fria
G7+            E7          Am7
Pra entrar num quente amor ah
             D7      G7+  D7
Dia e noite, noite e dia
D7  G7+  F#m7/5-  B7
  Viva a natureza
Em7
Sua beleza
Ebm7 Dm7 G7/9
Abençoou

C7+            Cm6
Deu pra nossas vidas
Bm7        E7
Amor de verdade
A7/9         Am7     D7
Cheio de luz só claridade
G7+ F#m7/5-   B7
Num amor tão belo
   Em7
Dá tudo certo
  Ebm7 Dm7 G7/9
Um grande amor
C7+         Cm6        Bm7
Lado a lado cheio de desejos
      E7              A7/9
que a troca de beijos
      D7     Dm  G7/9
Tenha mais sabor

----------------- Acordes -----------------
A7/9 = 5 X 5 4 2 X
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
Bm7 = X 2 4 2 3 2
C7+ = X 3 2 0 0 X
Cm = X 3 5 5 4 3
Cm6 = X 3 X 2 4 3
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
Ebm7 = X X 1 3 2 2
Em7 = 0 2 2 0 3 0
F#m7/5- = 2 X 2 2 1 X
G = 3 2 0 0 0 3
G6 = 3 X 2 4 3 X
G7+ = 3 X 4 4 3 X
G7/9 = 3 X 3 2 0 X
