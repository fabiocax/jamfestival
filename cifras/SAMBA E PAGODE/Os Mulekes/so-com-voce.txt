Os Mulekes - Só com você

      E9             C#m7
Como pode alguém se entregar
     A7+                    B7
E depois dizer que estava errada..
      E9                 G#m7
Como pode alguém se apaixonar
     A7+                    B7    C°
E dizer que não sente mais nada..
      C#m7          G#m7
Nosso beijo foi tão quente
      C#m7        G#m7
Teu carinho envolvente
      A7+
Mas agora você diz
         E/G#
Que não vai mais me encontrar
         F#m7
Que não pode se prender
         B7          G#7
Que não quer me namorar
      C#m7          G#m7
Por que ainda é adolescente

      C#m7        G#m7
E precisa aproveitar a vida
      A7+
Mas o amor bateu em mim
         E/G#
Tá difícil esquecer
       A7+
Pra você foi diversão
         B7              E9
Mas pra mim é pra valer..Amor..
              B/D#                C#m7
Me deixa te pegar e te colocar no colo
             E/B                   A7+
Me deixa te amar e te mostrar quem sou
              E/G#                F#m7
Tenho tantos planos lindos com você
B7          E9
Só com você
              B/D#            C#m7
Eu quero voar no céu da tua boca
               E/B               A7+
Viver a minha vida pra te dar prazer
              G#m7              F#m7
Tenho tantos planos lindos com você
B7        E9
Só com você..

----------------- Acordes -----------------
A7+ = X 0 2 1 2 0
B/D# = X 6 X 4 7 7
B7 = X 2 1 2 0 2
C#m7 = X 4 6 4 5 4
C° = X 3 4 2 4 2
E = 0 2 2 1 0 0
E/B = X 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E9 = 0 2 4 1 0 0
F#m7 = 2 X 2 2 2 X
G#7 = 4 6 4 5 4 4
G#m7 = 4 X 4 4 4 X
