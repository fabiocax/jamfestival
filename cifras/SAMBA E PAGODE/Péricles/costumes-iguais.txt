Péricles - Costumes Iguais

C#m7               B9
Hoje alguém perguntou outra vez por você
A9
Eu sorri e menti, não consigo dizer
C#m7              B9                     A9
Que a gente perdeu o nosso laço de amor
C#m7                      B9
Nossa separação não me deixa dormir
A9                               B9
Os meus dias se vão, e eu perdido aqui
C#m7                   B9                     A9
Enlouqueço ao pensar que não vai voltar
A7M                         C#m7                                     A7M
Meus costumes são iguais, ponho a mesa pra nós dois
                               E/G#                C#m7
Depois lembro não te tenho mais, no seu lugar há dor
A7M                     C#m7                 A6/F#           B6/G#          AM7     B4(7/9)
É difícil assimilar, que eu te perdi, não vejo mais sentido sem você aqui
A7M    B9          C#m7        B9            A9      B9                C#m7
Ar pra respirar, chão pra caminhar, minha base o seu amor, ooh, ooh, oh
A9      B9            C#m7    B9           A9     B9      C#m7
Olho pro portão, fere o coração, não te ver chegar amor

----------------- Acordes -----------------
A6/F# = 2 4 2 2 2 2
A7M = X 0 2 1 2 0
A9 = X 0 2 2 0 0
B4(7/9) = 7 X 7 6 5 X
B6/G# = 4 6 4 4 4 4
B9 = X 2 4 4 2 2
C#m7 = X 4 6 4 5 4
E/G# = 4 X 2 4 5 X
