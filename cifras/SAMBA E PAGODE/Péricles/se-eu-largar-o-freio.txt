Péricles - Se Eu Largar o Freio

A
Vou de casa pro trabalho
                               G
Do trabalho vou pra casa na moral

Sem zoeira, sem balada, sem marola
                      A
Sem mancada, eu tô legal !!!

Faça sol ou faça chuva

O que eu faço pra você
          G
Nunca tá bom

Pago as contas, faço as compras

Tudo bem, eu sei
               A
É minha obrigação


Mas eu tenho
                 G
Reclamações a fazer

Mas eu tenho
                    A
Que conversar com você
A7+                  A6
A pia tá cheia de louça
            A7+                 A6
O banheiro parece que é de botequim
G
A roupa toda amarrotada

E você nem parece que gosta de mim
A7+               A6
A casa tá desarrumada
             A7+                 A6
E nenhuma vassoura tu passa no chão
      G
Meus dedos estão se colando

De tanta gordura que tem no fogão
A7+              A6
Se eu largar o freio...
 A7+                A6
Você não vai me ver mais
G
Se eu largar o freio...
                     A7+
Vai ver do que sou capaz
       A6
Se eu largar o freio...
A7+                 A6
Vai dizer que sou ruim
                 G
Se eu largar o freio...
                       A7+
Vai dar mais valor pra mim
A                                 G
Paracundê, paracundê, paracundê ê ô
                                  A
Paracundê, paracundê, paracundê ê a
                                  G
Paracundê, paracundê, paracundê ê ô
                                  A E7
Paracundê, paracundê, paracundê ê a

----------------- Acordes -----------------
A = X 0 2 2 2 0
A6 = 5 X 4 6 5 X
A7+ = X 0 2 1 2 0
E7 = 0 2 2 1 3 0
G = 3 2 0 0 0 3
