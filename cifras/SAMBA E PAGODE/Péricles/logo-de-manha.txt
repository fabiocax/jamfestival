Péricles - Logo de Manhã

C                                                  C7
Logo de manhã, De manhã, de manhã
                                                  F
Logo de manhã, De manhã, de manhã
        Fm                                 G
Teu corpo já procura o meu prazer
C                                                  C7
Logo de manhã, De manhã, de manhã
                                                  F
Logo de manhã, De manhã, de manhã
    Fm                               G
Renovo o meu desejo por você

C
O amor que nos invade noite a dentro
C7
É um rio que desagua no lençol
F
É um risco que extravasa o sentimento
Dm                 G
Um brilho que reflete a luz do sol


C
E a gente num cenário bagunçado
C7
E as roupas espalhadas pelo chão
F
E o fogo adormece embrasado
Dm                         G
Querendo mais um pouco de emoção

C
E o sono recarrega o gás
C7
E uma noite só não satisfaz
F
E o teu sorriso é luz do dia
Dm                        G                  C
Me acorda com um beijo e pede mais

----------------- Acordes -----------------
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
