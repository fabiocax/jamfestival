Sensação - Vem Me Abraçar

[Intro] Cm7  F7/9  Bb7M  Gm7  Em5-/7  Ebm  Bb6

Bb6
De nada vai adiantar
Dm7
Me resumir me desprezar
Cm7M                    Ebm     Cm F7
É bem melhor você parar com isso
Bb6
Que tal a gente se acertar?
Dm7
Botar as coisas no lugar?
Cm7M                         Ebm   Cm F7
Meu Deus do céu, vamos parar com isso

Fm7        Bb7/13
Eu não prossigo sem você
Fm7       Bb7/13
Não deixe tudo se perder
Eb9                         Eº
Não vá embora, por favor

              Cm7
Eu vou morrer de dor

F7/9    Dm7          Gm7      Cm
Meu ... grande amor, quero te dar
F7/9  Dm7    Gm7          Cm
Felicidade ... vem me abraçar

----------------- Acordes -----------------
Bb6 = X 1 3 0 3 X
Bb7/13 = 6 X 6 7 8 X
Bb7M = X 1 3 2 3 1
Cm = X 3 5 5 4 3
Cm7 = X 3 5 3 4 3
Cm7M = X 3 5 4 4 3
Dm7 = X 5 7 5 6 5
Eb9 = X 6 8 8 6 6
Ebm = X X 1 3 4 2
Em5-/7 = X X 2 3 3 3
Eº = X X 2 3 2 3
F7 = 1 3 1 2 1 1
F7/9 = X X 3 2 4 3
Fm7 = 1 X 1 1 1 X
Gm7 = 3 X 3 3 3 X
