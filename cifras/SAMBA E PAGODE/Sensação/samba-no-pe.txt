Sensação - Samba no pé

Introdução: D D D C7 F
Solo (cavaco): 15 14 25 23 14  15 14 25 23 14  30 32 33 21 23 13 12 23 21

                  C7
Samba na ponta do pé

Menina, menina
                  F
Samba na ponta do pé

Menina, menina

                C7
No maxixe no xaxado
                  F
No forró no contrapé
                  C7
Se dançar bem agarrado
                F
Aí todo mundo quer


                 Cm
O partido tem bailado
F7             Bb
É um ritual de fé
Bbm                Am
Tem que ser malandreado
   D7       Gm        C7       F
Na roda de samba com samba no pé

                  C7
Samba na ponta do pé

Menina, menina
                  F
Samba na ponta do pé

Menina, menina

                   C7
Esse passo ultrapassado
                      F
Hoje em dia ninguém quer
                     C7
Dois passos pra cada lado
                F
É coisa de Zé Mané

                Cm
Quebra no sapateado
F7               Bb
Põe gingado no balé
Bbm               Am
Tem que ser malandreado
   D7       Gm        C7      F
Na roda de samba com samba no pé

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bbm = X 1 3 3 2 1
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D7 = X X 0 2 1 2
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
Gm = 3 5 5 3 3 3
