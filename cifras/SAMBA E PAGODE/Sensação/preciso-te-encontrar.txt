Sensação - Preciso Te Encontrar

Intro: Bm  F#  C#m F# Bm F# Bm

F#                Bm   F#       Bm
Vou perguntar pro amor o que eu fiz
F#               Bm      A      G
Pra ser um filho da dor, um infeliz
F#              C#m        F#              B
Que se entregou pra valer, quis conhecer a paixão
F#               C#m   F#             B
Só encontrou o sofrer, machucou o coração
   F#            B    F#          G#m
Preciso te encontrar, não dá mais pra fingir
G#                   C#m     F#              B
O teu cheiro está no ar, vem pra me fazer sorrir
F#            Bm  F#    Bm
Quanta alucinação eu já vivi
F#              Bm     A         G
Chega de tanta ilusão, vou ser feliz
 F#          C#m       F#           B
Uma luz vai acender em meio a escuridão
F#               C#m   F#                B
Só o que quero é poder dar um fim na solidão

   F#           B     F#     G#m
Preciso te encontrar, você é tudo pra mim
G#                   C#m   F#               B
O teu cheiro está no ar perfumando o meu jardim

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
G#m = 4 6 6 4 4 4
