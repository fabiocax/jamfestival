Sensação - Quando o Sol Nascer

Intro: Am7/9  D7/9  F7M  G7/13  C6/9  Am7  Bm7/4  E7
          Am7  F7+  Dm7/9  G7/13  C6/9  F6  E7/11  E7

F#m7(b5)                     Fm6
Deixa eu entrar no seu mundo
          Em7
Mudar sua vida
                  Em7/4    A7(b9)
Te fazer bem mais feliz
 Dm7/9         G7/4  G7           C7M      G/B
Sinto a cada segundo meu amor crescer
 Gm7/4               C7/9
(Pena que você não vêeee)
F7+                                    Fm6
O que ele tem que eu não tenho
               Em7
Me diz que eu mudo
                Em7(b5)   A7(b9)
Eu quero você pra mim
Dm7/9             G7/13   G7(b9)               C6/9     Bm7(b5)  E7(b9)
Olha eu te amo tanto      não me deixe assim

Am7                Em7
Eu já tentei de tudo
                     F7M    Dm7 ^ Dm7/C  Bm7/4          E7(b9)
Pra fugir do absurdo     de não ter    você ao meu lado
Am7               Em7                              F7M      Dm7 ^ Dm7/C
Ando perdendo o sono pra viver esse sonho
           Gm7   Csus
De poder amado
F7+              Bb7/9
Quando o sol nascer
               Am7   Am7/G              D/F#   G7/4     Gm7        Csus
Eu vou te procurar     abrir meu coração só pra você entrar
F7+              Bb7/9
Quando o sol nascer
               Am7      Gb7(b5)          F7M    G7/4      C6/9        Am7
Eu vou te procurar    abrir meu coração só pra você entrar

----------------- Acordes -----------------
A7(b9) = 5 X 5 3 2 X
Am7 = X 0 2 0 1 0
Am7/9 = X X 7 5 8 7
Am7/G = 3 0 2 0 1 0
Bb7/9 = X 1 0 1 1 X
Bm7(b5) = X 2 3 2 3 X
Bm7/4 = 7 X 7 7 5 X
C6/9 = X 3 2 2 3 3
C7/9 = X 3 2 3 3 X
C7M = X 3 2 0 0 X
Csus = X 3 2 0 1 0
D/F# = 2 X 0 2 3 2
D7/9 = X 5 4 5 5 X
Dm7 = X 5 7 5 6 5
Dm7/9 = X 5 3 5 5 X
Dm7/C = X 3 0 2 1 1
E7 = 0 2 2 1 3 0
E7(b9) = X X 2 1 3 1
E7/11 = 0 2 0 2 0 X
Em7 = 0 2 2 0 3 0
Em7(b5) = X X 2 3 3 3
Em7/4 = X 7 X 7 8 5
F#m7(b5) = 2 X 2 2 1 X
F6 = 1 X 0 2 1 X
F7+ = 1 X 2 2 1 X
F7M = 1 X 2 2 1 X
Fm6 = 1 X 0 1 1 X
G/B = X 2 0 0 3 3
G7 = 3 5 3 4 3 3
G7(b9) = 3 X 3 1 0 X
G7/13 = 3 X 3 4 5 X
G7/4 = 3 5 3 5 3 X
Gb7(b5) = 2 X 2 3 1 X
Gm7 = 3 X 3 3 3 X
Gm7/4 = 3 X 3 3 1 X
