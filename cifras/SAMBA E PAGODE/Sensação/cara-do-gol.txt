Sensação - Cara do Gol

Introdução:  G G#º Am G C7/9 G D7/6


Bm7/5-    E7  Am                  D7/9           G7+
  É    brincadeira o gol que esse cara acabou de perder
              G7             C6/9
Com a bola no pé, na cara do gol
           D7/9           G7+
Na cara da bola o cara chutou
Bm7/5-   E7   Am                  D7/9           G7+
  Até    o  padre da igreja do morro rezou pra fazer (pra valer)
              G7             C6/9
Com a bola no pé, na cara do gol
           D7/9           G7+
Na cara da bola o cara chutou
Dm                                    G7
Um primeiro tempo jogado com muita emoção
            C6/9                            Cm
Mas vamos jogar, o gol tá maduro e o jogo tá bom
              G7             C6/9
Com a bola no pé, na cara do gol

           D7/9           G7+
Na cara da bola o cara chutou
Bm7/5-  E7  Am              D7/9                G7
    Segundo tempo veio o lançamento e o momento chegou
              G7             C6/9
Com a bola no pé, na cara do gol
           D7/9           G7+
Na cara da bola o cara chutou
Bm7/5-  E7    Am              D7/9                G7+
    Um contra-ataque, a bola, o craque, o gato e o gol
              G7             C6/9
Com a bola no pé, na cara do gol
           D7/9           G7+
Na cara da bola o cara chutou
Dm                         G7
O jogo acabou e meu time ganhou outra vez
           C6/9                                  Cm
É ruim de aturar, arranja outro time que esse freguês
              G7             C6/9
Com a bola no pé, na cara do gol
           D7/9           G7+
Na cara da bola o cara chutou
Bm7/5-  E7    Am              D7/9          G7+
    Pelé na área, botou no barbante e comemorou
              G7             C6/9
Com a bola no pé, na cara do gol
           D7/9           G7+
Na cara da bola o cara chutou
Bm7/5-  E7    Am              D7/9          G7+
    E   o Edinho foi lá no cantinho e quase pegou

              G7             C6/9
Com a bola no pé, na cara do gol
           D7/9           G7+
Na cara da bola o cara chutou
Bm7/5-  E7    Am              D7/9                G7+
    Lá vai o Dener com bola sorrindo no jardim do céu
              G7             C6/9
Com a bola no pé, na cara do gol
           D7/9           G7+
Na cara da bola o cara chutou
Bm7/5-  E7    Am          D7/9               G7+
    Bico na bola, olé, gaviola, rolinho e chapéu

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm7/5- = X 2 3 2 3 X
C6/9 = X 3 2 2 3 3
C7/9 = X 3 2 3 3 X
Cm = X 3 5 5 4 3
D7/6 = X 5 X 5 7 7
D7/9 = X 5 4 5 5 X
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
G = 3 2 0 0 0 3
G#º = 4 X 3 4 3 X
G7 = 3 5 3 4 3 3
G7+ = 3 X 4 4 3 X
