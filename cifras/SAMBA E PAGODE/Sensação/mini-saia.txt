Sensação - Mini-Saia

Introdução:  C  A7  Dm G7

 C                                      F7
Eu fico na janela esperando ela passar de minisaia
                                         C
É perna pra dedél do céu ao chão fica sobre a minisaia
                   Fm               G7
Ela usa pra provocar rebolando ela vai sambar
C                   F7                      C
A saia roda gira todos viram que ela vinha que alegria
  F7
Cheia de Bombom
   Fm              Bb7   Eb7+
A mulecada para até o futebol
        Dm5-/7       G7           Cm
O vento bate o sol derrama o seu amor
       Fm          Bb7      Eb7+
A cor do céu não é azul é Ro...sa
 G#7               G7
E a Rua inteira reparou
C                 F7
Na minisaia ( cheia de Bombom)  2X

Fm        Bb7      Eb7+
Ela sai assim de manhã
       Dm5-/7        G7       Cm
Começa o dia já botando pra quebrar
 Fm        Bb7           Eb7+
sabe que gostosa, fica toda prosa
  G#7                G7
Quando sacode o seu Balagandam
C                    F7                     C
A saia roda gira todos viram que ela vinha que alegria
    F7
Cheia de bombom
   Fm         Bb7        Eb7+
A mulecada para até o futebol
      Dm5-/7           G7        Cm
O vento bate o sol derrama o seu amor
     Fm        Bb7        Eb7+
A cor do céu não é azul é rosa
G#7               G7
E a rua inteira reparou
C             F7
Na minisaia ( cheia de Bombom) 2X



----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bb7 = X 1 3 1 3 1
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
Dm = X X 0 2 3 1
Dm5-/7 = X X 0 1 1 1
Eb7+ = X X 1 3 3 3
F7 = 1 3 1 2 1 1
Fm = 1 3 3 1 1 1
G#7 = 4 6 4 5 4 4
G7 = 3 5 3 4 3 3
