Sensação - Capoira

Intro: G D7 G D7

                                            G  D7
Zum zum zum, zum zum zum, capoeira mata um

        G                          D7
Pega a viola que eu quero te ver cantar
                                G
Firma o batuque pro nego cantarolar
                                  D7
Pega a viola que eu quero te ver cantar
                                   G
Firma o batuque pro nego cantarolar

              G                                          D7
Eu quero ouvir mais palmas, mais palmas, mais palmas sem parar
          Am7            D7     G  D7
ƒ roda de samba e tem capoeira ai‡
           G                                             D7
Eu quero ouvir mais palmas, mais palmas, mais palmas sem parar
          Am7            D7      G
ƒ roda de samba e tem capoeira ai‡


          Dm                  G7      C
No meio da roda eu n‹o marco bobeira ai‡
           Cm                 D7       G  F7
Eu entro na roda e n‹o levo rasteira ai‡
          E7                  A7
L‡ no terreiro levanto poeira ai‡
          D7                    G7
ƒ roda de samba e tem capoeira ai‡

          Dm               G7      C
Menina baiana que desce a ladeira ai‡
            Cm              G7      C
Sambando na roda n‹o Ž brincadeira ai‡
         E7                    A7
Pego a menina e caio na zueira ai‡
          D7                    G  D7
ƒ roda de samba e tem capoeira ai‡

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
F7 = 1 3 1 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
