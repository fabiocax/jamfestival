Sensação - Coral de Anjos

(intro) C7+ Bb7+ Am7 D7 G

Am7                   Bm7
Um coral de anjos cantou
    E7                      Am
Uma linda canção de amor
               E7             Am
Canção que falava pra mim
                     D7
Que iria ter fim minha dor
Bm Bb Am                   D7        G
( que iria ter fim minha dor )

                          Am
Depois vieram os querubins
Bm           E7       Am
 Arcanjos e os serafins
                  E7           Am
Dizendo que o meu coração
              D7               Bm Bb Am D7       G   Dm G
Dormia em nuvens de paixão         (de paixão)


C                C          C7+
E quando me dava por mim
                  C         Bb7+
Meu sonho reinava no céu
         Bb6          Bb7+
Ouvia o som de clarins
                            Am
Cantava o som de Gabriel

             E7            Am
E os angelicais tamborins
                 E7          Am
Trilhavam de estrelas pra mim
             D7  Dm   G G5+
Caminho de felicidade

C         C            C7+
E na proteção de Miguel
             C           Bb7+
O reino de Deus era flor
            Bb
Daí foi que Emanuel
Bb7+              Bb  Am
  Sorrindo nos abençoou
              E7      Am
Ganhei um novo coração
                D7         G
E o teu coração me ganhou

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Bb = X 1 3 3 3 1
Bb6 = X 1 3 0 3 X
Bb7+ = X 1 3 2 3 1
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
C7+ = X 3 2 0 0 X
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
G = 3 2 0 0 0 3
G5+ = 3 X 1 0 0 X
