Sensação - Combinar Você e Eu

Intro:D7/9/11 / C7+ / Bm7 / Bb7+ / Am7 / % / E7/9+ / E7/9- / Am7/9

                 D7/9
O meu mundo não é digno
Am7/9               D7/9
......Você não.........tem o meu signo
Gm7                     C7/9
......Não tem livro de história
Gm7                     C7/9
......Nem a minha faixa etária
Cm7              F7         Bb7+  ^  D7/9+
......Mas eu insisto em tentar
   Eb7+           Am5-/7     D7/9-
Me convencer e procurar
   G7         Gm7
Combinar você e eu
            F7          Bb7+
Porque  eu a vida nos ensina
      Eb7+         Am5-/7     D7
Que o amor nunca termina
    Gm7    B7/9  ^  C7/9    Gm7  ^  G#m7  ^
No adeus

^  Am7                      D7/9
*.....Nosso amor é feito um lírio
Gm7                    D7/9
......Que nos leva ao delírio
Gm7                         C7/9
......Abre as portas do mistério
Gm7                       C7/9
......Quando é, levado a sério
Cm7         F7                Bb7+  ^  D7/9+
......Então porque não se entregar
  Eb7+             Am5-/7    D7/9-
Se resolver, se apaixonar
      G7       Cm7
Combinar você e eu
         F7         Bb7+
Porque a vida nos ensina
      Eb7+          Am5-/7   D7/9-
Que o amor nunca termina
   G7  %   Cm7
No adeus
        F7          Bb7+
Porque a vida nos ensina
      Eb7+         Am5-/7   D7
Que o amor nunca termina
     D7/9/11
No adeus
INTRODUÇÃO E VOLTA NO *
                        Gm7   C7/9   Gm7   C7/9   Gm7
FINAL: No adeus

----------------- Acordes -----------------
Am5-/7 = 5 X 5 5 4 X
Am7 = X 0 2 0 1 0
Am7/9 = X X 7 5 8 7
B7/9 = X 2 1 2 2 X
Bb7+ = X 1 3 2 3 1
Bm7 = X 2 4 2 3 2
C7+ = X 3 2 0 0 X
C7/9 = X 3 2 3 3 X
Cm7 = X 3 5 3 4 3
D7 = X X 0 2 1 2
D7/9 = X 5 4 5 5 X
D7/9+ = X 5 4 5 6 X
D7/9- = X 5 4 5 4 X
D7/9/11 = X 5 5 5 5 5
E7/9+ = X 6 5 6 7 X
E7/9- = X X 2 1 3 1
Eb7+ = X X 1 3 3 3
F7 = 1 3 1 2 1 1
G#m7 = 4 X 4 4 4 X
G7 = 3 5 3 4 3 3
Gm7 = 3 X 3 3 3 X
