Molejo - Voltei

(intro) A7+ E D E7/9

(solo) 17 17 17 17 16 17 16 12 14 12 14 12 20 22 17 17 17 17 16 17 16 12 14 12 14 12 22 32

A7+        C#m7                   D          E7/9            A7+
Venho trabalhando venho me esforçando pra ter você ao lado meu
    C#m7                D                    E7      F°     F#m7
Mas ja estou cansando não ta adiantando ainda não reconheceu

(refrão 2x)
          C#m7                 D            E7/9           F° F#m7
Só por você eu parei de beber não faço festa lá no meu a.p
          C#m7                 D             E7/9           F° F#m7
Ja tô magrinho voltei a correr todos notaram só você não vê
         C#m7                  D              E7/9           F° F#m7
E palavrão é coisa do passado eu ja não ando todo amarrotado
         C#m7                   D              E7/9                 F° F#m7
Na minha boca não tem mais cigarro você não vê mas todos ja notaram

(refrão 2x)
         C#m7               D                  E7/9                F° F#m7
Essa rotina ja ta estressante não aguento mais beber refrigerante

         C#m7                      D           E7/9                     F° F#m7
O meu pulmão sabe que eu sou fumante vive pedindo trago a todo instante
         C#m7                     D             E7/9               F° F#m7
Vou confessar uma coisa pra você só por um dia eu parei de correr
         C#m7                  D           E7/9             A7+
A minha barba voltou a crescer arrumei logo ou você vai ver..
                   C#m7               D
Vou voltar pra sacanagem pra casa de massagem
E7/9                  A7+               C#m7               D
Ali sempre foi meu lugar ja tava com saudade das velhas amizades
E7/9                 A7+
Hoje eu vou me embriagar} (2x)
          C#m7                 D              E7/9             F° F#m7
Ou chega logo ou vai se arrepender vou fazer festa la no meu a.p
          C#m7                D             E7/9               F° F#m7
Pensando bem vou falar pra você se não vier vai ter cine privê
          C#m7                D             E7/9                  F° F#m7
Eu não renego mais o meu passado não vou pagar mais o bom namorado
         C#m7                 D                 E7/9            F° F#m7
Pensando bem ja tô mais sussegado ja posso andar todo amarrotado

----------------- Acordes -----------------
A7+ = X 0 2 1 2 0
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
E7/9 = X X 2 1 3 2
F#m7 = 2 X 2 2 2 X
F° = X X 3 4 3 4
