Molejo - Amor Estou Sofrendo

Introdução: Dm Dm7 Bb7 G#m7/5- A7 Dm A7
Dm                 Dm7
  Amor estou sofrendo
Em7/5-
Passando um pedaço
     Cm       F7/11 F7
Se estou merecendo
   Bb             C
Perdoa meu doce amor
F7                        Bb
Me ensina a voltar que eu vou
Gm             A7              B7 F#m7/5-
Chega de tanta dor, chega de sofrer
Gm              A7
Perdoa meu doce amor
F7                        Bb
Me ensina a voltar que eu vou
Gm             A7              Dm
Chega de tanta dor, chega de sofrer
Gm   A  A#
Quer saber

                        A
Perdi poesia, alegria e minha paz
Gm   A  A#
Quer saber
           A  D
Ainda amo demais
                                   Gm      C
   O vinho acabou mas a taça ainda não quebrou
                     F    Gm A Dm
Não diga que a fonte secou
Dm                        Dm7+
Se você me der mais uma chance no futuro
           Dm7                     Dm6
Eu lhe asseguro nunca mais amor eu juro
                    A
Você vai chorar por mim
                        Dm
Você não vai chorar por mim
             A
Não vai, não vai, não vai

----------------- Acordes -----------------
A = X 0 2 2 2 0
A# = X 1 3 3 3 1
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
Bb = X 1 3 3 3 1
Bb7 = X 1 3 1 3 1
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
Dm = X X 0 2 3 1
Dm6 = X 5 X 4 6 5
Dm7 = X 5 7 5 6 5
Dm7+ = X X 0 2 2 1
Em7/5- = X X 2 3 3 3
F = 1 3 3 2 1 1
F#m7/5- = 2 X 2 2 1 X
F7 = 1 3 1 2 1 1
F7/11 = 1 3 1 3 1 X
G#m7/5- = 4 X 4 4 3 X
Gm = 3 5 5 3 3 3
