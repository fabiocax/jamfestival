Molejo - Dança da Vassoura

Introdução:  F F Dm Gm C7 (2 vezes)

F       Dm         Gm          C7    F
  Diga aonde você vai, que eu vou varrendo
      Dm         Gm          C7    F
Diga aonde você vai, que eu vou varrendo
       Dm            Gm             C7            F
Vou varrendo, vou varrendo, vou varrendo, vou varrendo
       Dm            Gm             C7            F
Vou varrendo, vou varrendo, vou varrendo, vou varrendo
Dm         Gm      C7      F Dm
  Oh menininha eu sou seu fã
Dm         Gm      C7      F Dm
  Oh menininha eu sou seu fã
          Gm       C7    F Dm
Danço contigo até de manhã
          Gm       C7    F Dm
Danço contigo até de manhã
    Dm           Gm          C7          F
Na dança da bruxinha dança preta, dança loura
    Dm           Gm          C7          F
Na dança da bruxinha dança preta, dança loura

  Dm         Gm         C7          F
Agora todo mundo na dancinha da vassoura
  Dm         Gm         C7          F
Agora todo mundo na dancinha da vassoura
 Dm           Gm     C7          F
Varre pra esquerda, varre pra direita
 Dm        Gm            C7         F
Levanta poeira que essa dança é porreta
 Dm           Gm     C7          F
Varre pra esquerda, varre pra direita
 Dm        Gm            C7         F
Levanta poeira que essa dança é porreta
                   Gm
Pitipi pitipi pitipau
          C7       F
Pitipi pitipi pitipau
             D7                    Gm
Mas cuidado com o cabo da vassoura,
              C7                     F
é pior do que cenoura você pode se dar mal

----------------- Acordes -----------------
C7 = X 3 2 3 1 X
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
