Molejo - Preta Talento

Introdução: G6/9  C7/9
  G6/9                    C7/9               G6/9  C7/9
Preta, sei que você vem pro samba com seu talento
          G6/9
Com seu molejo e seu gingado
      C7/9             G6/9       C7/9
Você mexe com meu sentimento  (ô preta !)
        C7/9
Me enlouqueço só de te ver sambar
         G6/9
O meu desejo sempre foi te encontrar
        C7/9
Menina linda, quero te namorar
             G6/9
Pra sempre te amar
      C7/9
Eu e você formamos um lindo par
         G7
E agora juntos, vamos poder bailar
       C7/9
O seu jeitinho e o seu rebolar

           G                C#7/9
Vão me encantar.......vem pra cá !
 C7+            D/C
(Preta).....me faz esse carinho
    Bm         E7
(Preta).....eu sou seu pretinho
   Am           D7                  Dm   G7
(Preta).....contigo quero sempre estar
  C7+           D/C
(Preta).....me faz esse carinho
  Bm            E7
(Preta).....eu sou seu pretinho
   Am          D7                  G
(Preta).....contigo quero sempre estar
                         D7
Na introdução, na introdução

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C#7/9 = X 4 3 4 4 X
C7+ = X 3 2 0 0 X
C7/9 = X 3 2 3 3 X
D/C = X 3 X 2 3 2
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
G = 3 2 0 0 0 3
G6/9 = X X 5 4 5 5
G7 = 3 5 3 4 3 3
