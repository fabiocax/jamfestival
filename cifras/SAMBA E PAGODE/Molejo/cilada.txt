Molejo - Cilada

[Intro] Am  G7  C  F7  E7

[Primeira Parte]

  Am
Quase morri do coração
  G7(4)        G7
Quando ela me convidou
  C
Pra conhecer o seu ap
 Bm7(b5)     E7
Me amarrei, demorou
Am
Ela me usou o tempo inteiro
 G7
Com seu jeitinho sedutor
C
Eu fiz serviço de pedreiro
 Bm7(b5)        E7
De bombeiro, encanador


[Pré-Refrão]

A     A5+  A6      A7
  Inocente, apaixonado
D
  Eu tava crente crente
    Bm               E7
Que ia viver uma história de amor
A        A5+  A6     A7
  Que cilada,   desilusão
D
  Ela me machucou
Bm             E7
Ela abusou do meu coração

[Refrão]

Am          F7      E7
  Não era amor, ôh, ôh
    Am
Não era
          F7    E7
Não era amor, era
   Am
Cilada
          F7      E7
Não era amor, ôh, ôh
    Am
Não era
          F7    E7
Não era amor, era
   Am                         F7  E7
Cilada, cilada, cilada, cilada
   Am                         F7  E7
Cilada, cilada, cilada, cilada

[Segunda Parte]

  Am
Quase morrendo de cansaço
 G7(4)          G7
Pálido e me sentindo mal
 C
Me trouxe um whisky bem gelado
 Bm7(b5)          E7
Me fez um brinde sensual
Am
Aquele clima envolvente
G7
Acelerou meu coração
  C
Chegou um gigante de repente
    Bm7(b5)            E7
Gritando sujou, te peguei Ricardão

[Pré-Refrão]

A     A5+  A6      A7
  Inocente, apaixonado
D
  Eu tava crente crente
    Bm               E7
Que ia viver uma história de amor
A        A5+  A6     A7
  Que cilada,   desilusão
D
  Ela me machucou
Bm             E7
Ela abusou do meu coração

[Refrão]

Am          F7      E7
  Não era amor, ôh, ôh
    Am
Não era
          F7    E7
Não era amor, era
   Am
Cilada
          F7      E7
Não era amor, ôh, ôh
    Am
Não era
          F7    E7
Não era amor, era
   Am                         F7  E7
Cilada, cilada, cilada, cilada
   Am                         F7  E7
Cilada, cilada, cilada, cilada

[Primeira Parte]

  Am
Quase morri do coração
  G7(4)        G7
Quando ela me convidou
  C
Pra conhecer o seu ap
 Bm7(b5)     E7
Me amarrei, demorou
Am
Ela me usou o tempo inteiro
 G7
Com seu jeitinho sedutor
C
Eu fiz serviço de pedreiro
 Bm7(b5)        E7
De bombeiro, encanador

[Pré-Refrão]

A     A5+  A6      A7
  Inocente, apaixonado
D
  Eu tava crente crente
    Bm               E7
Que ia viver uma história de amor
A        A5+  A6     A7
  Que cilada,   desilusão
D
  Ela me machucou
Bm             E7
Ela abusou do meu coração

[Refrão]

Am          F7      E7
  Não era amor, ôh, ôh
    Am
Não era
          F7    E7
Não era amor, era
   Am
Cilada
          F7      E7
Não era amor, ôh, ôh
    Am
Não era
          F7    E7
Não era amor, era
   Am                         F7  E7
Cilada, cilada, cilada, cilada
   Am                         F7  E7
Cilada, cilada, cilada, cilada

   Am                         F7  E7
Cilada, cilada, cilada, cilada
   Am                         F7  E7
Cilada, cilada, cilada, cilada

----------------- Acordes -----------------
A = X 0 2 2 2 0
A5+ = X 0 3 2 2 0
A6 = 5 X 4 6 5 X
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
Bm7(b5) = X 2 3 2 3 X
C = X 3 2 0 1 0
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
F7 = 1 3 1 2 1 1
G7 = 3 5 3 4 3 3
G7(4) = 3 5 3 5 3 X
