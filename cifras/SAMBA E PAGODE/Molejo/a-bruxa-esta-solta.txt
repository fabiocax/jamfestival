Molejo - A Bruxa Está Solta

Introdução:  Bm E7 A F#7 Bm E7 A E7

           A                                         C#7
Agora é tarde, nosso amor tá acabado, tô falando a vera
            F#m                                    Em   A7
Você não soube respeitar o meu ciúme por isso já era
        D                               E/D
Pode xingar, me humilhar, não vou dar bola
         C#m                          F#7
Eu já falei que com você não quero guerra
            B7                  E7           A
Quando você vier na minha direção, vê se me erra
                                                  A
Eu disse que não era brincadeira você não acreditou
                                           E7
Fez gracinha, deu risada, pra todo mundo falou
                                         A     A7
Que eu tinha mais é que sofrer por teu amor ( amor )
          D                    E/D
Não adianta você vir se ajoelhar
         C#m                    F#7
Bater cabeça pra salvar seu orixá

          B7                    E7               A
Porque fizeram um feitiço de responsa pra nos separar
   Bm                  E7
A bruxa está solta, ô ô ô
    C#m            F#7
Soltaram um fubá, fu
  Bm                      E7         A
Agora não adianta você vir me procurar

A bruxa está solta

 A
Agora é tarde nosso amor tá acabado
         Db7
tô falando à vera
           F#m7
Você não soube respeitar o meu ciúme
              Em7    A7
Por isso já era
       D                                   G7/9
Pode xingar, me humilhar, não vou dar bola
       Dbm7                             F#7/9
Eu já falei que com você não quero guerra
            Bm7             E7
Quando você vier na minha direção
            A
Vê se me erra.
    Bm7                     E7
Eu disse que não era brincadeira
                 A
Voce não acreditou, fez gracinha, deu risada
                Bm7
Pra todo mundo falou
                      E7                     Em7   A7
Que eu tinha mais é que sofrer por teu amor, amor
        D                    G7/9
Não adianta você vir se ajoelhar
       Dbm7                      F#7/9
Bater cabeça pra salvar seu orixá
       B13/7 B11/7 B7
Porque fizeram um feitiço
  Bm7               E7    A
De responsa pra nos separar
    Bm7           E7
A bruxa está solta ô ô ô.
 Dbm7              F#7
Soltaram um fubá, fú
Bm7
Agora não adianta.
 E7           Em7    A7
Você vir me procurar. A

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
B11/7 = X 2 4 2 5 2
B13/7 = X 2 X 2 4 4
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
C#7 = X 4 3 4 2 X
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
Db7 = X 4 3 4 2 X
Dbm7 = X 4 6 4 5 4
E/D = X 5 X 4 5 4
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F#7 = 2 4 2 3 2 2
F#7/9 = X X 4 3 5 4
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
G7/9 = 3 X 3 2 0 X
