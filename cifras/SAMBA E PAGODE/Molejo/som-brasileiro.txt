Molejo - Som Brasileiro

Intro: Gm F#m Fm Cm Fm Cm

Fm
O clima é maneiro, não falta tempero
                                 Cm
Quem gosta de samba pode balançar

Se liga aê, manda ver, samba aê

Pode botar pra quebrar
Fm
Quem sabe o valor do som verdadeiro
                                         Cm
Conhece o que é bom, não se deixa enganar
                             Fm
Se liga aê, manda ver, samba aê
                   Cm
Pode botar pra quebrar
        Fm
Solta a voz, quero ouvir


Deixa a alma cantar
       Cm
Faz o samba ecoar, faz o samba ecoar
        Fm
Vem na palma da mão

Bate aí pra firmar
       Cm
Faz o samba ecoar, faz o samba ecoar
Fm              Cm
Lá, lá, laiá, laiá

Samba aê, manda ver, canta aê
  Fm                Cm
Laiá, lá, laiá, laiá

----------------- Acordes -----------------
Cm = X 3 5 5 4 3
Fm = 1 3 3 1 1 1
