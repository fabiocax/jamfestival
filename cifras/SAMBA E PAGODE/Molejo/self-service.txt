Molejo - Self-Service

Introdução.: Am D7 G E7 Am D7 G E7 Am D7 G E7 Am D7 G C G
                   G
Não quero mais saber
                     D
De comprar bujão de gás porque
          C
O meu fogão
            D                G
Onde já se viu eu vou vender
                 G G7
Não é que eu enjoei
              C  A7
Da comidinha sua
                     G
Mas de agora em diante
E7        Am             D7        G E7
Pode acreditar eu vou papar na rua
                     Am                  D7
Não é porque você esquece de por o tempero
               G                                          E7
O alho, a cebolinha, a pimenta de cheiro

                      Am
Confunde sal com açúcar
               D7
Que coisa maluca
                 G E7
Faz o maior salseiro
        Am                    D7
Jiló e pepino em salada de fruta
                G
Quero lhe ensinar
                    E7
Mas você não me escuta
                  Am
Bate no filé mignon
                D7
Soca o champignon
                   G E7
Usando a força bruta
            Am
Comer em casa
                   D7
Eu não quero mais não
          G                  E7
O Self-Service é a minha opção
             Am
Boto na balança
                  D7
Encho a minha pança
              G  E7
E como de montão

                Am
De agora em diante
                    D7
Só tô comendo a quilo
                 G
De agora em diante
                   E7
Só tô comendo a quilo
                 Am
De agora em diante
                   D7
Só tô comendo a quilo
                   G E7
Só tô comendo a quilo
Introdução

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
