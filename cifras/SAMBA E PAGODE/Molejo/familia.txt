Molejo - Família

Introdução: D G

 D               G                    D
Padas, Padas, Padas, Padas, Padas, Padas, Padas,
         D                    G
Padas, Padas, Padas, Padas, Padas,
D               G                    D
Padas, Padas, Padas, Padas, Padas, Padas, Padas,
         D                    G
Padas, Padas, Padas, Padas, Padas...
   A
Família, família
  D             G
Papai, mamãe, titia
   A
Família, família
  D           G
Almoça junto todo dia
 D            G
Nunca perde essa mania
     D                            G
Mas quando a filha quer fugir de casa

   D                      G
Precisa descolar um ganha-pão
D                        G
Filha de família não se casa
   D                          G
Papai, mamãe, não dão nenhum tostão
   A
Família ê
   G
Família á
  D    G
Família
  A
Família, família
   D              G
Vovô, vovó, sobrinha
      A
Família, família
D            G
Janta junto todo dia
D              G
Nunca perde essa mania
D                        G
Mas quando o nenê fica doente
    D                     G
Procura uma farmácia de plantão
    D              G
O choro do nenê estridente
   D                      G
Assim não dá pra ver televisão
  A
Família ê
  G
Família á
  D     G
Família
  A
Família, família
   D               G
Cachorro, gato, galinha
   A
Família, família
   D        G
Vive junto todo dia
D              G
Nunca perde essa mania
  D                     G
A mãe morre de medo de barata
  D                     G
O pai vive com medo de ladrão
 D                       G
Jogaram inseticida pela casa
  D                     G
Botaram um cadeado no portão
  A
Família ê
   G
Família á
   D    G
Família

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
