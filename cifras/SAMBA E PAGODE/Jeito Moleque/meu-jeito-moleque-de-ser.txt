Jeito Moleque - Meu jeito moleque de ser

Introdução: Am7

Am
Tá em cima da hora
         Dm
Falo to saindo
       E7                 Am
Boné na cabeça bermuda caindo
                               Dm
Vou dobrando a esquina vejo a rapaziada
           E7                     Am
Quem vai pro pagode uma gelada

Eu já to
Eu também
         Dm
Demorou então
             E7
Vou eu o Carlinhos o Fé e o Alemão
Am                                     Dm
  Então ta vamo ai que pra não se atrasar

          E7                 Am    Bm     C    D
Pagode moleque já vai começar
Am
Ta no jeito de andar
           Dm
No jeito de viver
             G7
Ninguém pode mudar
                   C          E7
E meu jeito moleque de ser
               Am
Não dá pra disfarçar
                 Dm
Não tem como esconder
             Bm7/5-
Eu sei que vai notar
       E7              Am
E meu jeito moleque de ser

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Bm = X 2 4 4 3 2
Bm7/5- = X 2 3 2 3 X
C = X 3 2 0 1 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
G7 = 3 5 3 4 3 3
