Jeito Moleque - Além do céu

Introdução: G7+ / C/G / G7+ / G7sus4 / G7+ / C/G / G7+ / Em7(add11) E7...

Am7(add11)       C9/D        G7+   Em7(add11)
Alem do céu e onde mora o nosso amor
  Am7(add11)       B7(#9)            Em7
Graças a deus meu coração te encontrou
Dm7  G7   C7+
Hoje eu.... vivo feliz
D7sus4            Bm7(add11)
A paz Reinando em mim
Em7      A7sus4     A7
Te quero tanto .... Tanto !
Am7(add11)    D7sus4    E7sus4      ( 2x  D7 )
Já não sei te esquecer
Gm7      Cm7
Eu...... Pirei
F7       Bb7+
Nesse Amor
Eb7+   Am7(add11)       D7(b9)             Gm7/9
Sonho Acordado ................. pensando em você
     Cm7   F7      Bb7+
Viajei.... me encantou

Eb7+      Am7(add11)       D7          G7+      E7sus4
O teu .........sorriso me ...........apaixonou

( Fine ... Eb7+ / % / G7+/D / % / Am7(add11) / C/D / E7+

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7sus4 = X 0 2 0 3 0
Am7(add11) = 5 X 5 5 3 X
B7(#9) = X 2 1 2 3 X
Bb7+ = X 1 3 2 3 1
Bm7(add11) = 7 X 7 7 5 X
C/D = X X 0 0 1 0
C/G = 3 3 2 X 1 X
C7+ = X 3 2 0 0 X
C9/D = X 5 5 5 3 3
Cm7 = X 3 5 3 4 3
D7 = X X 0 2 1 2
D7(b9) = X 5 4 5 4 X
D7sus4 = X X 0 2 1 3
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
E7+ = X X 2 4 4 4
E7sus4 = 0 2 0 2 0 X
Eb7+ = X X 1 3 3 3
Em7 = 0 2 2 0 3 0
Em7(add11) = X 7 X 7 8 5
F7 = 1 3 1 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
G7+ = 3 X 4 4 3 X
G7+/D = X 5 5 4 7 X
G7sus4 = 3 5 3 5 3 X
Gm7 = 3 X 3 3 3 X
Gm7/9 = X X 5 3 6 5
