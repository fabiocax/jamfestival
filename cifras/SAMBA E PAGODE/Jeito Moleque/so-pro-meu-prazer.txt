Jeito Moleque - Só pro meu Prazer

(intro) Am7 F#7 Bm E7/9- A7 D7 G6

D9/4           G7+
Não fala nada, deixa tudo assim por mim
D9/4               G7+
Eu não me importo se nós não somos bem assim
D9/4         G7+
É tudo real, as minhas mentiras
D9/4                    G7+
E assim não faz mal, e assim não me faz mal não

Bm
Noite e dia se completam
      A
Nosso amor e ódio eterno
Bm
Eu te imagino, eu te conserto
          A
Eu faço a cena que eu quiser
Em                      F#m
Tiro a roupa pra você, minha maior ficção de amor

Em             F#m  Em     A4
Eu te recriei só pro meu prazer
            D9/4
Só pro meu prazer

D9/4           G7+
Não vem agora com essas insinuações
D9/4              G7+
Os seus defeitos ou de algum medo normal
D9/4           G7+
Será que você não é nada que eu penso
D9/4               G7+
Também se não for, não me faz mal

Não me faz mal não.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A4 = X 0 2 2 3 0
A7 = X 0 2 0 2 0
Am7 = X 0 2 0 1 0
Bm = X 2 4 4 3 2
D7 = X X 0 2 1 2
D9/4 = X 5 7 9 8 5
E7/9- = X X 2 1 3 1
Em = 0 2 2 0 0 0
F#7 = 2 4 2 3 2 2
F#m = 2 4 4 2 2 2
G6 = 3 X 2 4 3 X
G7+ = 3 X 4 4 3 X
