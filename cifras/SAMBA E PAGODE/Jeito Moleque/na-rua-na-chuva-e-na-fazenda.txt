Jeito Moleque - Na Rua, Na Chuva e Na Fazenda

Am7
Não estou disposto
               E
A esquecer seu rosto de vez

E acho que é tão normal
Am7
Dizem que sou louco
              E
Por eu ter um gosto assim

Gostar de quem não gosta de mim

Am7         D         Am7
Jogue suas mãos para o céu
             D         G7M
E agradeça se acaso tiver
               C        G7M
Alguém que você gostaria que
            C         F#m7
Estivesse sempre com você

         B          F#m7
Na rua, na chuva, na fazenda
      B             E
Ou numa casinha de sapê

Am7
Não estou disposto
               E
A esquecer seu rosto de vez

E acho que é tão normal
Am7
Dizem que sou louco
              E
Por eu ter um gosto assim

Gostar de quem não gosta de mim

Am7         D         Am7
Jogue suas mãos para o céu
             D         G7M
E agradeça se acaso tiver
               C        G7M
Alguém que você gostaria que
            C         F#m7
Estivesse sempre com você
         B          F#m7
Na rua, na chuva, na fazenda
      B             E
Ou numa casinha de sapê

Am7         D         Am7
Jogue suas mãos para o céu
             D         G7M
E agradeça se acaso tiver
               C        G7M
Alguém que você gostaria que
            C         F#m7
Estivesse sempre com você
         B          F#m7
Na rua, na chuva, na fazenda
      B             E
Ou numa casinha de sapê.

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
B = X 2 4 4 4 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m7 = 2 X 2 2 2 X
G7M = 3 X 4 4 3 X
