Jeito Moleque - De Cara Pro Gol

Intro:  F7M  G#º  Am7  C7(9)  F7M  G#º  Am7  C7(9) E7(b9)

Am7                                Bm7(b5)           E7
Paro pra ver essa tua beleza / com toda a certeza, você é o show
Am7                                      Bm7(b5)                    E7
Eu vou driblando o zagueiro /  ignoro o goleiro e fico de cara pro gol

      Dm7                G7                  C7M               Am7
Tô na boa / passando na rua / um olhar me ensinua / vai ter fuzuê
       Dm7                            G7          Bm7(b5)             E7
Seu vestido indescente / seu jeito atrevida / não dá pra olhar sem querer


Am7                                 Bm7(b5)         E7
Bendita saída de praia / tomara que caia no papo do meu coração
      Am7
Tô na fila com bom argumento
             Bm7(b5)                       E7
Esperando o momento se liga na minha intenção

      Dm7                    G7                    C7M              Am7
Eu prefiro escolher / do que ser escolhido /  mina certa, parei em você

        Dm7              G7                   Bm7(b5)           E7
Perco a linha e perco o sentido / com toda a beleza que vem de você


Refrão:
  Am7             C7M                Bm7(b5)
Pago pra ver / a gente não se apaixonar
                E7                 Am7
Pago pra ver / o fogo da paixão queimar
               C7M                  Bm7(b5)
Pago pra ver / você zombar da minha dor
                 E7                    Am7
Eu quero ver / a gente se embolar de amor

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Bm7(b5) = X 2 3 2 3 X
C7(9) = X 3 2 3 3 X
C7M = X 3 2 0 0 X
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
F7M = 1 X 2 2 1 X
G#º = 4 X 3 4 3 X
G7 = 3 5 3 4 3 3
