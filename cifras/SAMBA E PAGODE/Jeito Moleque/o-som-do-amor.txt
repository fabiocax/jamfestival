Jeito Moleque - O Som do Amor

(intro) E C#m7 Am7 E C#m7 Am7 B7(9)

E                   G#m7         A
  Eu te via sempre aqui, mais não sei porque
        Am7
  Nunca tive a chance de me aproximar.
E                    G#m7          A
  Pra falar do que eu senti, ao te conhecer
    Am7                              C#m7           G#m7
Percebi que a vida armou pra gente se encontrar, em outro lugar
             F#m7                          G#m7                            F#m7
E hoje agente aqui, dividindo tantas coisas em comum, descobrindo tantos sonhos e segredos
                   G#m7     C#m7              F#m7          B7
Eu nunca poderia imaginar encontrar tanta beleza num olhar,   num olhar

  E                G#m7     C#m7          G#m7
Me faz sentir que o tempo parou por um momento
   A            Bm7          C#m7   B7
E posso ouvir o som do amor no ar
   E            G#m7      C#m7            G#m7
Me abraça com vontade, me mostra que é verdade

     A9          B7(9)            E
Que Deus mandou você pra ser meu par.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
B7(9) = X 2 1 2 2 X
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
E = 0 2 2 1 0 0
F#m7 = 2 X 2 2 2 X
G#m7 = 4 X 4 4 4 X
