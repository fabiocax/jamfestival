Jeito Moleque - Eu, Você e Mais Ninguém

(intro) E G7+ A7+ D7+ E G7+ A7+

E                 G7+
 Onde você vai estar
                      A7+
Depois que a festa terminar
                  E
Quando estiver relax
                    G7+
Me diz aonde vai estar
                   A7+  Am7
Se caso eu te telefonar
                      C#m7
Se caso eu quiser te ver
                        D7+
Assim eu chego como o vento
                  C#m7
Arrepiando seu ouvido
                        D7+
Te levo pra um lugar distante
                      F#m
Pra você não correr perigo


Ninguém vai notar  ninguém vai
A                          E
saber E tudo vai ficar bem

(refrão)
E G7+                 A7+
  Quanto tempo pra rolar
D7+                     E
   Eu  você e mais ninguém
G7+                 A7+  D7+
   Baby pode relaxar e tudo bem (2X)

(repete intro)

E                G7+
 Onde é que você vai
                     A7+
Depois que a festa terminar
                     E
Depois que estiver relax
E                     G7+
  Me diz aonde vai estar
                   A7+   Am
Se caso eu te telefonar
                      C#m7
Se caso eu quiser te ver
                       D7+
Assim eu chego como o vento
                  C#m7
Arrepiando seu ouvido
                        D7+
Te levo pra um lugar distante
                       F#m
Pra você não correr perigo

Ninguém vai notar  ninguém vai
A
saber
                      E
E tudo vai ficar bem...

(refrão 2x)

A7+         A/B
 Ahhh  me sinto preso a você
eu sei

G#m G#7      C#m7
 Ahhh  se um dia eu vou ter de
vez  não sei (2x)

A7+    D7+
Ahhhhhhh  E tudo vai ficar bem

(refrão 2x)

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/B = X 2 2 2 2 X
A7+ = X 0 2 1 2 0
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
C#m7 = X 4 6 4 5 4
D7+ = X X 0 2 2 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#7 = 4 6 4 5 4 4
G#m = 4 6 6 4 4 4
G7+ = 3 X 4 4 3 X
