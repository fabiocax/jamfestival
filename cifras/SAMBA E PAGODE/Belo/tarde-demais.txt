Belo - Tarde Demais

    F                    C
Nem sei quem é você, nem lembro dos teus olhos
    Bb                      C
Não choro de saudade, já perdi o teu retrato
   F                 C
Eu vivo bem assim: sozinho no meu canto
 Bb                     C
Adoro a solidão, não preciso de ninguém

Dm   C       Bb          C
Aprendi a mentir pra mim mesmo
      Dm     C        Bb           C
Eu me convenci que eu nunca fui feliz
          Dm   C           Bb          C        F         F/A
Nem nunca fiz amor com a mulher que eu sempre sonhei
          Bb              C
Foi só um sonho que eu criei

F     Dm          F     Dm
Por favor não telefona mais
           C      Am
Deixa eu viver em paz

C          D             F    Dm
Sei que um dia eu vou te esquecer
        F     Dm          C      Am
E vou poder viver um verdadeiro amor
C          D       Gm
Não se arrependa jamais
F  Dm  Bb     C     F
Já vai ser tarde demais

      F                   C
Se eu já me acostumei foi obra do acaso
    Bb                            C
Não sinto o teu perfume em nenhum canto do meu quarto
   F                C
Eu passo por você e juro que não vejo
    Bb                   C
Nem sombra do desejo que rasgou meu coração

Dm   C       Bb          C
Aprendi a mentir pra mim mesmo
      Dm     C        Bb           C
Eu me convenci que eu nunca fui feliz
          Dm   C           Bb          C        F         F/A
Nem nunca fiz amor com a mulher que eu sempre sonhei
          Bb              C
Foi só um sonho que eu criei

F     Dm          F     Dm
Por favor não telefona mais
           C      Am
Deixa eu viver em paz
C          D             F    Dm
Sei que um dia eu vou te esquecer
        F     Dm          C      Am
E vou poder viver um verdadeiro amor
C          D          F     Dm          F    Dm
Não se arrependa, mas por favor não telefona mais
           C      Am
Deixa eu viver em paz
C          D             F    Dm
Sei que um dia eu vou te esquecer
        F     Dm          C      Am
E vou poder viver um verdadeiro amor
C          D       Gm
Não se arrependa jamais
F  Dm  Bb     C     F
Já vai ser tarde demais

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
Gm = 3 5 5 3 3 3
