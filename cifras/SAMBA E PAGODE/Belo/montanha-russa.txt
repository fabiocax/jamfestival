Belo - Montanha Russa

Dm               Dm7/C
O céu é o limite feito pra nós dois
Gm7              A7
Contigo eu vou num vôo de primeira classe
Dm               Dm7/C
À bordo dessa nave eu vivo as emoções
Gm7              A7
Num porto de felicidade

Dm                  Dm7/C
Lembro quando te avistei
Gm7              A7
Li no teu sorriso
Dm               Dm7/C
Nosso amor surgindo
Gm7              A7
Encontrei o meu destino

Dm               Dm7/C
Eu fico admirando, eu tô me perguntando
Gm7              A7
Meu deus de onde vem tanta beleza

Dm               Dm7/C
A gente se entregando eu tô me apaixonando
Gm7              A7
Vou parabenizar a natureza

Dm               Dm7/C
Teu corpo é uma montanha russa de prazer
Gm7              A7
E nessas curvas eu me perco com você
Dm               Dm7/C
Me sinto no comando, eu tô nessa ferrari
Gm7              A7
São tantos os detalhes que me fazem amar você

(Dm  Dm7/C  Gm7  A7)
É o que me faz amar você
É o que me faz


----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Dm = X X 0 2 3 1
Dm7/C = X 3 0 2 1 1
Gm7 = 3 X 3 3 3 X
