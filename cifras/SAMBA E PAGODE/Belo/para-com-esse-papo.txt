Belo - Para com esse papo

 Am   F#7/5-        F7+               E7/9-         Am
     Já te avisei que eu sou um cara bem comprometido
F#7/5-      F7+          E7/9-          Em5-/7    A7/5+
    Eu te falei só vai rolar se for muito escondido
         Dm                 G7                 Am7+
Longe de tudo que o amor da minha vida possa ver
Am       Bm5-/7            E7/9-       Em5-/7    A7/5+
Mas admito que eu me sinto envolvido com você
          Dm                       G7         Am7+     Am
E mesmo assim não vou deixar de preservar minha paixão
        Bm5-/7           E7/9-          Am
A pioneira, a verdadeira dona do meu coração
                         Refrão
        F#7/5-            F7+    E7/9-      Am
Pára de ficar falando por aí da gente
        F#7/5-            F7+    E7/9-  Em5-/7
Pára de ficar zombando por aí contente
           A7/5+       Dm     E7/9     Am^G#m^Gm^C7
Se a minha dona descobrir vai ficar quente
                 F7+          E7/9-
Pode ser o nosso fim pra sempre

----------------- Acordes -----------------
A7/5+ = X 0 X 0 2 1
Am = X 0 2 2 1 0
Am7+ = X 0 2 1 1 0
Bm5-/7 = X 2 3 2 3 X
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
E7/9 = X X 2 1 3 2
E7/9- = X X 2 1 3 1
Em5-/7 = X X 2 3 3 3
F#7/5- = 2 X 2 3 1 X
F7+ = 1 X 2 2 1 X
G#m = 4 6 6 4 4 4
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
