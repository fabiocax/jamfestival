Belo - Vício

Intr.: A7+ A6 F#m7 Dm Dm7+ Dm7^Em7^F7+^Em7 Dm Dm A7M

           Em7    A7   D7M(9)
Você já tentou me amarrar
             Dm6 E7(9)    A7M
Mas o nosso amor era fogueira
A6        Em7     A7    D7M(9)
Eu queria só brincar de amar
            Dm6 G7(9)  C7+(9)
Você não queria brincadeira
            Gm7(9)
Que vontade louca
     C7(9)  F7+
Já virou um vício
F6         Fm6
Beijar sua boca
F/G          C7+(9)
Mas sem compromisso
Am7   Abm7    Gm7
Eu tô te querendo
C7(9)       F7M  F6
Só um pouco mais

    Bm7/5-      E7(9)                A7M
Pra ver se esse amor é o que me satisfaz

                 F#m7        Bm7
Por que você não para pra pensar
                    E7(9)        A7M
Não vê que eu tô querendo o seu amor
                   A6           Em7
Mas não vou entrar nessa de sonhar
               A7            D7M(9)
Pra não permanecer ao seu dispor
                           Dm6
Melhor deixarmos tudo como está
           G7(9)   C7M(9)      F7M
O nosso foi só uma tara mais nada
   Bm7/5-   E7(9)   A7M
Melhor é deixar pra lá

----------------- Acordes -----------------
A6 = 5 X 4 6 5 X
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
A7M = X 0 2 1 2 0
Abm7 = 4 X 4 4 4 X
Am7 = X 0 2 0 1 0
Bm7 = X 2 4 2 3 2
Bm7/5- = X 2 3 2 3 X
C7(9) = X 3 2 3 3 X
C7+(9) = X 3 2 4 3 X
C7M(9) = X 3 2 4 3 X
D7M(9) = X 5 4 6 5 X
Dm = X X 0 2 3 1
Dm6 = X 5 X 4 6 5
Dm7 = X 5 7 5 6 5
Dm7+ = X X 0 2 2 1
E7(9) = X X 2 1 3 2
Em7 = 0 2 2 0 3 0
F#m7 = 2 X 2 2 2 X
F/G = 3 X 3 2 1 X
F6 = 1 X 0 2 1 X
F7+ = 1 X 2 2 1 X
F7M = 1 X 2 2 1 X
Fm6 = 1 X 0 1 1 X
G7(9) = 3 X 3 2 0 X
Gm7 = 3 X 3 3 3 X
Gm7(9) = X X 5 3 6 5
