Belo - Depois do Amor

Perlla:
         G9
Eu quero mais
       Bm7
Um pouco mais
           C7+
Depois do amor
           Am7        Bm7      C7+       D7
Quero um carinho, um abraço me dá, por favor,

C7+                Bm7      Em      D7
No meu romantismo, não vejo problema
C7+
Te faço um pedido
   Bm7        Em7
Me leva pro cinema
  C7+    Bm7        Am7        D7
Você não vê, não consegue entender
   C7+               D7
Eu quero atenção, ao invés de prazer
C7+
Me pegue no colo

Bm7      Em7    D7
Diga que sou sua
C7+
Andar de mãos dadas
Bm7         Em7
Caminhar na rua

C7+               Bm7 Em
Bem, não faz isso comigo
D7  C7+            Bm7   Em
E...eu, preciso do teu abrigo
D7  C7+                 Bm7   Em
Meu bem, por favor, faz isso não
      Am7     Bm7 C7+   D7  G9
Eu preciso de carinho e atenção...

Belo:
        G9
Se pede mais
      Bm7
Um pouco mais
           C7+
Depois do amor
           Am7       Bm7       C7       D7
Pede um carinho, um abraço, te dou meu amor

C7+                Bm7      Em7     D7
No seu romantismo, não vejo problema
C7+
Me fez um pedido
   Bm7       Em7
Eu levo pro cinema
    C7+     Bm7         Am7       D7
Vem logo me ver, não consigo entender
    C7+                   D7
Teu dou mais atenção, não quero só prazer
C7+
Te pego no colo
Bm7         Em7    D7
Não tá mais sozinha
C7+
Como eu te adoro
Bm7            Em7
Sei que és toda minha

C7+               Bm7 Em
Vem, não faz isso comigo
D7  C7+            Bm7   Em
E...eu, preciso do teu abrigo
D7  C7+                 Bm7  Em
Meu bem, por favor, faz isso não
      Am7     Bm7 C7+    D7  G9
Eu preciso de carinho e atenção...

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Bm7 = X 2 4 2 3 2
C7 = X 3 2 3 1 X
C7+ = X 3 2 0 0 X
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
G9 = 3 X 0 2 0 X
