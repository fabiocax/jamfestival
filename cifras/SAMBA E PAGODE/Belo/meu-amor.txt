Belo - Meu Amor

introdução:F9  G4  G  F  D#9  G4

C                 G/C      F9
minha pele sente frio sem você
    Am7            G/B
seu cheiro ainda está em mim
G#  G4 G
no ar
Em7            Am7
o que fazer então
   Dm7               G4  G7
pra suportar essa vontade
            Em7   Am7
sofre o meu coração
        Dm7
volta pra mim
          G4    G7     C9
meu corpo clama por você

REFRÃO:


F9 G  C9             G/B     G/A#  A7
meu amor não consegui dormir direito
      Dm7             Dm/C       G4  G7
por favor diz que vai voltar pra mim
    Em7         E7         A4/7       A7
telefona nem se for pra dizer que acabou
      Dm7         Dm/C
já não posso mas viver
      Dm/B      G4   G7
não aguento mas sofrer
   C9
de amor

C9             G/C         F9
será que ainda lembro de nós dois
  Am7           G/B
você se foi sem me dizer
 G#   G4 G
adeus
       Em7      Am7
eu sofro de paixão
   Dm7            G4    G7
sem esse amor na minha vida
         Em7       Am7
não da pra te esquecer
         Dm7            G4     G     C9  G/C
volta pra mim meu corpo clama por você

REFRÃO:

----------------- Acordes -----------------
A4/7 = X 0 2 0 3 0
A7 = X 0 2 0 2 0
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
D#9 = X 6 8 8 6 6
Dm/B = X 2 X 2 3 1
Dm/C = X 3 X 2 3 1
Dm7 = X 5 7 5 6 5
E7 = 0 2 2 1 3 0
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
F9 = 1 3 5 2 1 1
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
G/A# = X 1 0 0 3 3
G/B = X 2 0 0 3 3
G/C = X 3 5 4 3 3
G4 = 3 5 5 5 3 3
G7 = 3 5 3 4 3 3
