Belo - Meia Luz

Int.: D  G  D  F

F           Bb
Preparei a sala pra você chegar
 Dm
Liguei o som pra emocionar
     Db            Eb      F
meia luz pra te ver entrar
           Bb
Antecipei abri um vinho pra nós dois
Dm
deixei surpresas pra depois
    Db         Eb      Bb
Uma data pra se guardar

Bb                Am
Tanto faz, se não veio preparada
          Dm                     F
Com lingerie que mais gosta de usar
            Dm                     (Bb Dm F Bb)1ª vez   (Bb F Bb)2ª vez
Pouco que importa já que eu vou tirar

Bb          Am
Aliás, a cortina esta fechada
        Db         Eb    F
pro seu corpo só eu olhar
F  Eb   Db         Eb
  Chega de imaginar..

D                 Bm
Me mostra além desse vestido
            G           Bm   Em        A7
Sussurra em meu ouvido, me suja de batom
     D                     Bm
Me arranha, eu quero que me marque
             G        Bm     Em         A7
Registre a intimidade com manchas no sofá
          D
De tanto amar...

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Db = X 4 6 6 6 4
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
