Belo - Desse Jeito É Ruim Pra Mim

(intro) A#m9  D#

Bbm7      Bbm7/G#      F#7+
Eu aqui olhando o telefone
    F#6/9        D#m7/9
Escrevendo o seu nome
    C#9          Cm7/4           F7
Esperando uma resposta que eu já sei

       Bbm7       Bbm7/G#       F#7+
E se eu sei por que eu fico feito bobo?
       F#6/9        D#m7/9
Sempre caio no teu jogo
     C#9     Cm7/4           F7
Olha só a armadilha que eu entrei

      F#7+
Você mudou sua atitude
     Bbm7/G#        G7/5-
E esqueceu de me avisar
      F#7+
Agora sei por quê

            G#sus4
Há outro em meu lugar

   C#9
Eu tento te entregar meu coração
Bbm7/4                           Bbm7/G#
Você não quer saber e diz que não
  F#9                     Bbm7
Parece que amar quem não te quer
   A9                        B9
Te faz sentir melhor ou mais mulher
    C#9
Pra que continuar agindo assim ?
   Bbm7/4                           Bbm7/G#
Não vê que desse jeito é ruim pra mim
   F#9                Bbm7
Um dia você pára pra entender
    A9                   B9   Bbm7  Fm7  F#9
Que tudo isso volta pra você!

----------------- Acordes -----------------
A#m9 = X 1 3 5 2 1
A9 = X 0 2 2 0 0
B9 = X 2 4 4 2 2
Bbm7 = X 1 3 1 2 1
Bbm7/4 = 6 X 6 6 4 X
Bbm7/G# = 4 1 3 1 2 1
C#9 = X 4 6 6 4 4
Cm7/4 = X 3 3 3 4 3
D# = X 6 5 3 4 3
D#m7/9 = X 6 4 6 6 X
F#6/9 = 2 1 1 1 X X
F#7+ = 2 X 3 3 2 X
F#9 = 2 4 6 3 2 2
F7 = 1 3 1 2 1 1
Fm7 = 1 X 1 1 1 X
G#sus4 = 4 6 6 6 4 4
G7/5- = 3 X 3 4 2 X
