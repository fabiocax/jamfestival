﻿Belo - O Amor Que Eu Sempre Quis

Intro: D7+/9  G7+  F#m7/11  Em7/9  D7+/9

E|----------------------------|
A|----------------------------|
D|-7--------------7----4---7--|
G|------6/7/6-----------------|
B|-10-------------10---7---10-|
E|------9/10/9----------------|

D7+/9                    G7+  F#m7/11  Em7/9  D7+/9
      Você não sabe me da
                       G7+  F#m7/11  Em7/9  D7+/9
O amor que eu sempre quis
                   G7+  F#m7/11  Em7/9  D7+/9
Eu quero te ver sonhar
                  G7+  Am  D7/9
E ser bem mais feliz

G7+                  F#m7
  Meu coração se fechou
          Am7       D7     G7+
você não sabe o que eu sofri

                     F#m7         Am7      D7  G7+
Na estrada da vida a pé    sem alguem pra dividir
        F#m          Em7  G/A  Gm/A  D7+/9  G7+   F#m7/11  Em7/9  D7+/9
Tanta dor   Eu só me iludi

D7+/9                    G7+  F#m7/11  Em7/9  D7+/9
      Você é tudo de bom
                 G7+   F#m7/11  Em7/9  D7+/9
Mas não sabe sonhar
                G7+  F#m7/11  Em7/9  D7+/9
Liberte essa emoção
                G7+ Am D7/9
Eu sou essa paixão

G7+                 F#m7
    Vou confiar em você
 Am7       D7     G7+
não vá me enganar
                F#m7     Am7     D7  G7+
Quem ama nunca trai  quero viver em paz
       F#m           Em7  G/A  Gm/A  D7+/9  G7+  F#m7/11  Em7/9  D7+/9
Então vem  Senão me faz sofrer

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
D7 = X X 0 2 1 2
D7+/9 = X 5 4 6 5 X
D7/9 = X 5 4 5 5 X
Em7 = 0 2 2 0 3 0
Em7/9 = X 7 5 7 7 X
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
F#m7/11 = 2 X 2 2 0 X
G/A = 5 X 5 4 3 X
G7+ = 3 X 4 4 3 X
Gm/A = 5 X 5 3 3 X
