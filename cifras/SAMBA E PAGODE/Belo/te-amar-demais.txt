Belo - Te Amar Demais

Intro: D7+   /  %  /  C#m7   /  %   /  C7+  /  F7+  /
D/E   / %  /

A7+        Bm7
Te amar demais só me faz mal
C#m7              D/E
Só me faz sofrer, te quis tanto que nem quis viver
    A7+              Bm7
Pra mim, era como um sonho te querer
C#m7              D/E                   Fº
F#m7
Pra te ver feliz, desenhei o mundo que você mais quis

           C#m7
Eu sofri demais mas aprendi
D7+                                D/E   Fº
Que o meu erro foi mer dar demais assim
F#m7        C#m7
De nos dois você foi quem perdeu
D7+                              D/E    G/A A7
Vejo minha falta no seu mundo eu sei

D7+        C#m7              Bm7
Você vai voltar, vai me procurar
D/E           A7+   G/A A7
Pra viver um sonho
D7+         C#m7
Vai querer ficar
                 Bm7  D/E           A7+

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
C7+ = X 3 2 0 0 X
D/E = X X 2 2 3 2
D7+ = X X 0 2 2 2
F#m7 = 2 X 2 2 2 X
F7+ = 1 X 2 2 1 X
Fº = X X 3 4 3 4
G/A = 5 X 5 4 3 X
