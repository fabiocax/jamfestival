Belo - O Dia Amanhaceu

C7+  C5+     C6               C5+
...Veja o dia...o sol está sorrindo pra nós
Fm7                          Bb7
foi tão boa essa noite que agente ficou
Eb7+                Gb7+
meu coração está apaixonado
Dm7              Gsus               C6/9
voce não sabe o bem...que voce me causou
Gb7+               Gsus                 C6/9 F7+ Gm7 Gsus
acho que eu to sentindo os sintomas do amor
C7+   C5+       C6                   C5+
...nossos corpos...tão gemeos unidos em um
Fm7                      Bb7
confesso dormir em teus braços
Eb7+                     Gb7+
foi como se eu pudesse voar
Dm7                  Gsus            C6/9
meu sangue ferve a cabeça começa a rodar
Gb7+               Gsus           C6/9   Gsus ***
voce deixou minha vida de pernas pro ar
              C6/9       Am7
hoje o dia amanheceu

         Gb7+                Gsus           C6/9   Am7
tao diferente que agente abraçado é como sonhar
           Gb7+            Bb7              Eb7+
beijo tua boca e posso sentir...o relogio parar
       Gb7+                 Gm7
fazer amor com voce me realizar
           Dm7               Gsus   C6/9  Gsus
sentir na pele o arrepio de te amar

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Bb7 = X 1 3 1 3 1
C5+ = X 3 2 1 1 X
C6 = 8 X 7 9 8 X
C6/9 = X 3 2 2 3 3
C7+ = X 3 2 0 0 X
Dm7 = X 5 7 5 6 5
Eb7+ = X X 1 3 3 3
F7+ = 1 X 2 2 1 X
Fm7 = 1 X 1 1 1 X
Gb7+ = 2 X 3 3 2 X
Gm7 = 3 X 3 3 3 X
Gsus = 3 2 0 0 0 3
