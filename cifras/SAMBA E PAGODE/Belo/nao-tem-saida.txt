Belo - Não Tem Saída

E7+
Eu errei
     C#m7             G#m C#7       F#m7
Mas jogue a primeira pedra quem jamais errou...
  D#m5-7            G#5+7 G#7      C#m7   0/0
 Quem alguma vez na vida   não se enganou
  F#7               A#dim         B7    B/A
Pra que condenar se é mais fácil perdoar......
    G7+  B7911
Perdoar.....
E7+      o/o
Já paguei .....
C#m7                 G#m7 C#7      F#m7   o/o
Pelo que eu fiz de errado com seu coração......
D#m5-7          G#5+7   G#7  C#m7   o/o
Que castigo é pior do que a solidão.....
F#m7                 B7911      E7+     C#m7
Me o que é preciso pra fazer você voltar.....
            Bm7  E79
Pra quê negar?
A7+      B/A     G#m7  C#m7

Aaaaprendi, é você ou nada
         F#m7
Tive um mundo em minhas mãos
  B711    Bm7       E79
E joguei fora, por nada
A7+      B/A           G#m7  C#m7
Hoje eu sei...que não tem saída
    F#m7
Tô pedindo pra você voltar
      B7911         o/o             E7+  D7911
Tô pedindo pra você ficar em minha vida.....
G7+  o/o
Perdoei...
  Em7                o/o           Am7   o/o
Que orgulho nada,eu quero amor no coração...
F#m5-7                  B5+7 B7     Em7    o/o
Que se dane o mundo eu tô  afim desta paixão...
     Am7                D7911         G7+  Em7
Só quem nunca errou é que não tem perdão ......
           Dm7  G79
Pra quê nagar?.... .....
C7+        D/C    Bm7        Em7
Aaaaprendìiii é você ou nada
         Am7            o/o
Tive o mundo em minhas mão
   D7911       Dm7       G79
E joguei tudo fora, por nada!
C7+      D/C            Bm7  Em7
Hoje eu sei... que não tem saída
      Am7             o/o
Tô pedindo pra você voltar
      D7911         o/o             Dm7  G79
Tô pedindo pra você ficar em minha vida......

( NA SEGUNDA VEZ  = Eb7+  Dm7  Cm7  Ab5-7  G7+ )

----------------- Acordes -----------------
