Belo - Quem Será

Intro 2x: C7+  Bm7  Am7/9  D7/11

  G7+                Em7
Escuta meu amor.. entenda de uma vez
      Am7/9                     D7/9/11
Você tem  que decidir se esse caso é pra valer
D/C      Bm7   Em7       Bm7   Em7
... Se alguma vez... te fiz sofrer
Am7/9                     D7/9/11
Foi loucura da paixão foi amor ..na contra mão
     G7+           Em7
*A sua estupidez já não te deixa ver
   Am7/9                              D7/9/11
Que você nasceu pra mim que eu preciso de você
D/C   Bm7    Em7      Bm7   Em7
..Porque ninguém ..vai conseguir
Am7/9                     D7/9/11
Separar você de mim porque eu não vou deixar

G7+                      Em7
Quem será... me diga quem será

Am7                           D7/11
Esse cara que te liga antes de você deitar
G7+                      Em7
Quem será... me diga quem será
Am7
Diz pra ele do perigo
    D7/11                 G7+
que ele corre ao te procurar...

( Eb7+  D7/11 )


----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Am7/9 = X X 7 5 8 7
Bm7 = X 2 4 2 3 2
C7+ = X 3 2 0 0 X
D/C = X 3 X 2 3 2
D7/11 = X X 0 2 1 3
D7/9/11 = X 5 5 5 5 5
Eb7+ = X X 1 3 3 3
Em7 = 0 2 2 0 3 0
G7+ = 3 X 4 4 3 X
