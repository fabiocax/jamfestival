Belo - Trilha Sem Fim

C#m7
Se o assunto é amor
        A          C#m7
Vale a pena tentar
B7              G#m
Tudo pra viver feliz
C#m7
Quem pensou que encontrou
      A                 C#m7
Se entregou sem pensar
B7                   G#m
Foi só mais um aprendiz
     C
Todo mundo sonhando
                       E
A procura do seu bem querer
      C
Tem anúncio nos classificados
         E E7
E até na tv
C#m                            B7
E quem disse que o encontro do amor é assim

  Bm                     C#7
O amor é destino é uma trilha sem fim
      F#m
Quer saber
       G#m              A       Bm7         C#m7
Pede a Deus que ele explica tim tim por tim tim

        E7       A
Quem já tem um amor
            B7                 G#m  C#7
Levanta as mãos e mostra que é feliz
                 A
Quem não tem um amor
         B7              G#m      C#7
Pra que negar no fundo sempre quis
                C#m
Quem perdeu um amor
       B7                  G#m          C#7
É há razão pra voltar e do amor não esquece
              F#m        G#m
O verdadeiro amor é um presente
     A   B7     C#m7
Só ganha quem merece

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
C#7 = X 4 3 4 2 X
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
