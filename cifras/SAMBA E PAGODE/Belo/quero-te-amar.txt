Belo - Quero Te Amar

Intro: F7M  Em  D#7M  Dm7

F7M           Em
Hoje eu acordei
   Dm7       Em
Percebi que você não estava aqui
F7M            Em
Será que não me ama mais
     Dm7       Em              Gm C79
Ou será que eu não te fiz feliz

F7M           Em
Se eu errei me diz em que?
   Dm7
Se eu errei foi sem querer
          Em
O meu medo foi te perder

F7M            Em
Você marcou meu coração
     Dm7
E despertou uma ilusão

         Em                  Gm C79
Porque que tem que ser assim?

Refrão 2x:
F7M
Quero te amar
 Em               D7    Dm
Te entregar o meu coração
 Em
Te dizer que ainda sou teu

F7M           Em
Hoje levantei
    Dm7       Em
percebi, que meu sonho estava diante de mim
F7M          Em
Resolvi comemorar
        Dm7         Em                 Gm C79
Com desejo de mais uma vez me apaixonar

  F7M                     Em
Talvez não se lembre daquele dia
             Dm7
Você é o motivo da minha alegria
                Em                      Gm C79
Quando nós nos olhamos pela primeira vez

 F7M             Em
Você me falou o que eu queria ouvir
       Dm7
A lua retrata o que eu senti
              Em                  Gm C79
A noite mais linda que eu já vivi

(Refrão 2x)

----------------- Acordes -----------------
C79 = X 3 2 3 3 X
D#7M = X X 1 3 3 3
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Em = 0 2 2 0 0 0
F7M = 1 X 2 2 1 X
Gm = 3 5 5 3 3 3
