Comunidade Católica Shalom - Amados Por Deus

[Intro] D  A  D  E  D  A  F#m  E


 D   D5(9) A9        F#m7(b13)  F#m7    E            D9
Gló---o---ria, ôôô   Gloria      a      Deus   nas alturas

Paz na Terra aos homens amados
 D   D5(9) A9        F#m7(b13)  F#m7    E            D9
Gló---o---ria, ôôô   Gloria      a      Deus   nas alturas

Paz na Terra aos homens amados por Deus

( E4  E  D/F#  E/G# )

    A9        D/A
Senhor Deus, Rei dos Céus
     E/G#    Asus4  A
Deus Pai Onipoten- -te
 A         D/A    E/G#   Asus4   A/C#
Nós vos louvamos, vos bendize- -mos
    D9           A/C#             Bm
Adoramos, glorificamos, vos damos graças

   Esus4          E  G5(9)
Por Vossa Imensa Glória

( E4  E  D/F#  E/G# )

    A9        D/A    E/G#    Asus4  A/C#
Senhor Jesus Cristo, Filho Unige-  -nito
        D9               A/C#
Senhor Deus, Cordeiro de Deus
              Bm   Esus4  E
Filho de Deus Pai

F#m                  D
Vós que tirais o pecado do mundo
 A9              Esus4  E
Tende piedade de nós
F#m                D
Vós que tirais o pecado do mundo
A              Esus4     E
Acolhei a nossa súplica
 D                A/C#
Vós que estais à direita do Pai
G5(9)             Esus4  E
Tende piedade de nós

    A         D/A
Só vós sois o Santo
   E/G#   Asus4  A/C#
Só vós o Senho-  -or
     D            A/C#
O Altíssimo Jesus Cristo
        Bm
Com o Espírito Santo
  Esus4            E    G5(9)   Esus4  E
Na Glória de Deus Pai, Amém

 D  A9   E/G#  F#m7     E         D9
Glória, ôôô   Gloria a Deus nas alturas
       A/C#      Bm       Esus E
Paz na Terra aos homens amados
 D   D5(9) A9        F#m7(b13)  F#m7    E            D9
Gló---o---ria, ôôô   Gloria      a      Deus   nas alturas

Paz na Terra aos homens amados por Deus
    Bm       E        G5(9)    D/F#
Aos homens amados por Deus
    Bm       E        A
aos homens amados por Deus

[Final]  A  D/A  A  D/A

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A9 = X 0 2 2 0 0
Asus4 = X 0 2 2 3 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D/A = X 0 X 2 3 2
D/F# = 2 X 0 2 3 2
D5(9) = X 5 7 9 X X
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E4 = 0 2 2 2 0 0
Esus = 0 2 2 1 0 0
Esus4 = 0 2 2 2 0 0
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
F#m7(b13) = X X 4 7 5 5
G5(9) = 3 X 0 2 3 3
