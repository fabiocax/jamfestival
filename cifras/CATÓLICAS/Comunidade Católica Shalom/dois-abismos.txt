Comunidade Católica Shalom - Dois Abismos

Intro:  Dm   Em F   C Dm   Em F
       (C C/E   F   C C/E   F)

C       C9       F
Eu escuto a melodia que Tu cantas
C         C9     F       G
Tu, Palavra de amor que me encanta
       G/F       Em       Am       Em       Am
Seduzindo-me pra sem...pre, inteiramen...te.
  Dm               F       G    C    C/E   F C C/E A4 A
Eterna canção de beleza e harmonia sem fim.

 D           A/C#       G
Tu me encerras em Teu peito, Amor ferido
 D          A/C#       G      G  D/F# Em D A/C#
Em Teus golpes de amor tens me vencido.
      A/G            F#m    Bm       F#m Bm Bm/A D/F#
De amor me tens cati....vo, rendi....do.
    Em           D/F#   G     A  D  D/F#   G Bm A/C# B/D# B
Fizeste-me presa de    Tua Bondade sem fim.


E            E/G#     A        A E/G# F#m B
Tu, mais íntimo de mim do que eu mesmo
 E    E/G#          A            B/D#
Infinito no finito em quem me perco.
   C#m        B     B/A G#m     C#m     G#m    A E/G#
Dois abismos que se abra...çam e se enla....çam
  F#m       A/E       D        A/C#   B     E B/D# D A/C# C G/B Am Am/G F#m7/5- C/E D B
Num laço de Amor eterno e ale...gria sem fim.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A/E = 0 X 2 2 2 0
A/G = 3 X 2 2 2 X
A4 = X 0 2 2 3 0
Am = X 0 2 2 1 0
Am/G = 3 X 2 2 1 X
B = X 2 4 4 4 2
B/A = X 0 4 4 4 X
B/D# = X 6 X 4 7 7
Bm = X 2 4 4 3 2
Bm/A = X 0 4 4 3 X
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
C/E = 0 3 2 0 1 0
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G#m = 4 6 6 4 4 4
G/B = X 2 0 0 3 3
G/F = 1 X X 0 0 3
