Comunidade Católica Shalom - Ressuscita-me Agora

C        Dm     F       Am    C
Espírito Santo, estou aqui
           Dm
Preciso da Tua mão
      F          C
Pra levantar, ficar de pé
         Dm     F         Am   C
Espírito Santo, quero recomeçar
              Dm
Pois tentando ser feliz
   F            C
Eu Te feri, Te desprezei
F                 Dm           Am     C
Ô... recebe o meu coração arrependido
F               Dm            Am   C
Ô... Pois só Tu és minha Esperança
F                 Dm           Am    C
Ô... recebe o meu coração arrependido
F               Dm            G
Ô... Pois só Tu és minha Esperança
          F                  Am    C
Espírito Santo, vem com Tua glória!

                Dm        F      (Am) C
Ressuscita-me agora! Preciso de ti! :|

Iter 1 - F G | Am | C4 C | C | F G | Am | C4 C | G |

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C4 = X 3 3 0 1 X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
