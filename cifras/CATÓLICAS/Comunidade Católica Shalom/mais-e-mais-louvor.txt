Comunidade Católica Shalom - Mais e Mais Louvor

Intro: F
   2x: Bb9 F/A Dm C9

Bb9                     F/D
    Sei que a cada manhã
             Dm
Tuas misericórdias se
            C9
Renovam em mim (bis)

 Gm7
Aonde eu irei, senhor
   Bb9
Se só em ti eu encontrei
    Dm
O caminho, a verdade
     F
E a vida

    Gm7
Sustenta-me com teu Amor.

     Bb9
E assim pra sempre eu viverei
 Bb     Dm     C
Para o teu louvor

    Bb   F/A
Te louvarei, de todo
   Gm           Dm
O coração, senhor
     Bb   F/A
Te exaltarei, és digno
    Dm        C9
De mais e ma...is (bis)

    Bb    F/A      Dm    C9
Louvo.......r, louvo.......r
    Bb    F/A
Louvo.......r
             Dm        C
És digno de mais e ma...is

(Volta à primeira estrofe)

   C     G/B
Te louvarei, de todo
   Am           Em
O coração, senhor
    C     G/B
Te exaltarei, és digno
   Em        D
De mais e ma...is (bis)

    C     G/B      Em     D
Louvo.......r, louvo.......r
    C     G/B
Louvo.......r
 Em              D
Digno de mais e mais
      G
Louvor

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bb9 = X 1 3 3 1 1
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
F/D = X X 0 5 6 5
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
Gm = 3 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
