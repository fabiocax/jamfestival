Comunidade Católica Shalom - Nada É Impossível a Ele

E                 E4 E       C#m        B
Nome mais lindo é o nome de Jesus
E                 E4 E       C#m        B
Canta, minha alma, ao teu libertador
E                 E4 E       C#m        B
Nome mais lindo é o nome de Jesus
E                 E4 E       C#m        B     E
Canta, minha alma, ao teu libertador

                             A         E/G#
Pois nada é impossível a Ele
                           A   B C#m
Ele pode tudo transformar
                      A
As barreiras cairão
                  E/G#
As feridas sararão
                         F#             B
Pelo poder do nome de Jesus :|

Inter1: E | E4 E |

Inter2: A C#m | B | 2x

A   C#m | B | A B | C#m |
Jesus,         Jesus,
A            E/G#
Jesus, Jesus
                    F#       B
O teu Nome tem poder :|

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E4 = 0 2 2 2 0 0
F# = 2 4 4 3 2 2
