Comunidade Católica Shalom - O Céu É Meu

 Bb           F/A
Senhor meu, amado meu
  Gm          Dm
Como a Tí se elevará
   Eb          Dm
O homem que criastes
   Eb            F
Se Tú não o levantares

     Bb          F/A
Quem poderá se libertar
      Gm            Dm
Da imperfeição, do mal agir
    Eb
Se Tú ó Deus amado meu
                     F
Não o ergues com Tua mão

Refrão:
   Bb           F/A       Gm
O céu é meu, a terra é minha

     Eb                 Dm
São meus os anjos e os santos
      Eb            F
E o próprio Deus é meu
   Bb          F/A  Fm/Ab Gm
O céu é meu, a terra é minha
        Eb               F         Bb
Pois o Cristo é meu e é tudo para mim

     Bb            F/A
Se o que Te peço é de Jesus
   Gm           Dm
Misericórdia e graça enfim
   Eb          Dm
Aceita esta pequena oferta
   Eb          F
E dá-me deste bem

    Bb           F/A
Se já me deste o teu Jesus
     Gm          Dm
Me alegrarei pensando assim
    Eb
Que Tú não tardas a chegar
              F
Se eu souber esperar

(Refrão)

(solo)

(Refrão)

       Eb                F         Bb
Pois o Cristo é meu e é tudo para mim

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
Fm/Ab = 4 3 3 1 X X
Gm = 3 5 5 3 3 3
