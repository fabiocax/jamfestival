Comunidade Católica Shalom - Aleluia Deus É Fiel

Intro 2x:  E A E A E A Bsus

E  A E      A E   C#m          C#sus      C#m     Bsus
Aleluia! Aleluia!           Aleluia!           Aleluia!
A  B/A         C#m
Jesus, Príncipe da Paz,
A         B       C#m
Vem nos falar da Salvação
A     B     E   G#    C#m
E anunciar o Tempo da Graça,
A            B   E       Bsus
E juntos cantar: Aleluia!
E         A  E                    E          A E C#m C#sus C#m Bsus
Deus é Fiel!!!  Deus é Fiel!!! / Deus é Fiel!!!
C#m          C#sus     C#m                 Bsus
Ele é sempre Fiel!!!       Deus é sempre Fiel!!!

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
B/A = X 0 4 4 4 X
Bsus = X 2 4 4 4 2
C#m = X 4 6 6 5 4
C#sus = X 4 6 6 6 4
E = 0 2 2 1 0 0
G# = 4 3 1 1 1 4
