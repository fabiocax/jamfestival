Comunidade Católica Shalom - Eu Quero É Deus e Mais Nada

Solo: ( D  E ) (4x)
      ( F#m  D  A  E  F#m  E  D )

F#m        E/G#          D         E    F#m
Procurava um sentido pra viver iê
           D                A
Vida vazia,  vida sem vida
                 E                   F#m
Vida morta se assim se pode descrever
              D      A                    E
Alegrias artificiais, noites em claro em vão
                  F#m             E/G#           D           E
Flores de plástico,  solidão em meio da multidãããããããããããããão

Parte 2:
F#m      E         D
Mas uma voz me chamou
     E    Fº F#m            E        D
Uma luz me invadiu,    um amor verdadeiro
F#m       E/G#           D
Que me mostrou   quem eu sou

        E        A     E        F#m   E  F#m
Neste amor cada dia querer desejaaaaaaaaaaaaaar

Refrão:
                        D      E
Eu quero é Deus e mais nada (uooh)
                 D     E
Eu quero é vida nova (iee)
                     D        E
Eu quero estar mais perto de Deus
            F#m
e tudo mais     virá
                        D     E
Eu quero é Deus e mais nada (uooh)
                 D     E
Eu quero é santidade (iee)
                     D        E
Eu quero estar mais perto de Deus
             F#m
este é o meu     lugar

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
F#m = 2 4 4 2 2 2
Fº = X X 3 4 3 4
