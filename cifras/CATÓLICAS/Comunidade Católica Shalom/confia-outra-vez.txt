Comunidade Católica Shalom - Confia Outra Vez

A                 E/G#
Às vezes em tua história
         F#m
Existem coisas
        D         Bm        E
Que não consegues compreender
 A                 E/G#
E mesmo sem perceberes
          F#m          D
Tu te escondes em tua dor
Bm             E
Por medo de sofrer

      D      C#m
Eu estou contigo
   D        Bm          E
E quero que venhas comigo
          D           C#m        D       E
Em cada lugar onde tua confiança se perdeu

 A9           E/G#
Vamos juntos agora

F#m                 D
Rever a tua história
  Bm             E4
Fechando as feridas
A9                E/G#
Curando todas as marcas
 F#m             D
Que não podem sarar
Bm                 E
Abrindo-te à vida
D                                 E
Devolvendo vida ao coração
D            Bm         E
Confia outra vez

A
Deixa eu tocar
               Bm
Deixa eu curar
D                      E
Não estás sozinho, não
 A                            Bm
Eu estou contigo até o fim
D                        E
Sempre te amarei
D                       A9
Sempre te amarei

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E4 = 0 2 2 2 0 0
F#m = 2 4 4 2 2 2
