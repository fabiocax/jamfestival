Comunidade Católica Shalom - Veni Creator Spiritus

G            D/F#
Veni creator spiritus
C              G
Imple superna gratia,
C                G
Quae tu creasti, pectora
Am               D
Quae tu creasti, pectora

Em             D/F#
Vem espirito criador
C          D
Derrama plenitude
Am            G
Dai graça aos corações
C            D
Que para ti criaste (2x)

Am
Recria-me
G
Reconstrói-me

C
Muda-me
D
Capacita-me a viver
Em              D
O que eu não consigo
C           D
Mas que anseio
Am            G
E anseio por que sou chamado
C     D
A vivê-lo

G
Veni creator spiritus
D
Veni creator spiritus
Em     C      D
Veni creator spiritus

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
