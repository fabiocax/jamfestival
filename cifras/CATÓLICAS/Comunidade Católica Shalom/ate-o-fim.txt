Comunidade Católica Shalom - Até o Fim

B    F#      G#m     C#        B
Sustenta minha decisão de não mais voltar atrás
     F#        G#m        C#           B
Sustenta meu desejo, então, de te amar cada vez mais
   B     G#m      C#         F#
Sem cessar te buscar e pequeno me fazer
    B       G#m       C#
Minha cruz desposar, pois eu sei que tua mão
B       F#    G#m         C#      B
Sustenta o meu coração inconstante, incapaz
     F#       G#m            C#        B
Sustenta meu olhar em ti pra que eu não te traia mais
     B         G#m     C#          F#
Pois eu sei que eu não sei te amar sem te ferir
      B    G#m      C#             B
Mas tua graça é maior, santifica meu agir, Jesus
F#    G#m           C#   C#7+
      A ti nada é impossível

Refrão:
   F#     B       G#m     C#
Eu creio no poder do teu amor, Senhor

      F#        B       G#m    C#
Tua misericórdia e salvação me fazem vencedor
B      F#       G#m      C#
Hoje tomo posse de tua graça em mim
B      F#    G#m   C#
Decididamente quero te seguir
      B    G#m     B  C#     B
Pois, eu sei, me bastarás até o fim
 G#m   B   C#   F#
Me bastarás até o fim

----------------- Acordes -----------------
B = X 2 4 4 4 2
C# = X 4 6 6 6 4
C#7+ = X 4 6 5 6 4
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
