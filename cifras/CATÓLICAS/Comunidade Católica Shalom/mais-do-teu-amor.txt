Comunidade Católica Shalom - Mais do Teu Amor

INTRO: E  A  E  A  F#m  A

E             A
   A minh'alma tem sede de Ti,
 E            A
   A minha carne Te deseja
 E          A
   como terra sedenta e sem água
   F#m             B
 meu Deus, como eu preciso de Ti

 F#m                  B
    Pois nada vale mais que o Teu amor
 F#m                B
    e nenhum passo eu sou capaz sem Ti.
F#m              C#m
   Tu és meu tudo, minha vida, meu dono.
  D  A
 Vem me saciar com Teu amor.

E      D
  Eu quero mais, mais do Teu amor.

 E          D
   Tudo que eu desejo é Te ver, Senhor,
A          F#m
  contemplar Tua face, Te adorar
A            F#        A
   E eternamente cantar

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
