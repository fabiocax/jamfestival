Comunidade Católica Shalom - Contigo Vou Cantar

      G D         A             Bm
A minh'alma se alegra no meu Deus
A         G D         A
A minh'alma  se alegra (bis)

G D Bm A ( 2X )

G                 A          Bm A
A minh'alma engrandece o Senhor
G                 A           Bm  A    C     A
Meu espírito exulta no meu Deus, meu Salvador
G                  A         Bm A
Ele viu, Maria, a tua pequenez
G                    A             Bm  A
Te escolheu para ser mãe do meu Senhor
C                 A
Contigo vou cantar

     G  D      A                Bm
A minh'alma se alegra no meu Deus
A       G   D       A
A minh'alma se alegra


G                  A        Bm A
Bem aventurada! Todos te dirão
G                    A        Bm  A    C     A
De sua graça o poderoso preencheu, meu coração
G                A           Bm A
Maravilhas fez conosco o Senhor
G                 A          Bm A
Alcançou-me o poder do Seu amor
C               A
contigo vou cantar

Intro (G A Bm) X3

     A     E      B           C#m
A minh'alma se alegra no meu Deus
B   A   E          B
A minh'alma se alegra

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
