Comunidade Católica Shalom - Acertei Deus

E         A         B            C#m
Mas olha só o que foi que aconteceu
    A        B           E
Eu mirei em você e "acertei Deus"

E            A                    B
Como quem descobre um tesouro eu vi
      Cº           C#m
transparência, generosidade
    A      B        E
verdade, unção e alegria
  A      B          Cº         C#m
Mas bem mais do que você possa fazer
A           B           E
Você é me conduz a santidade

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
Cº = X 3 4 2 4 2
E = 0 2 2 1 0 0
