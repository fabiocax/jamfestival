Comunidade Católica Shalom - O Elevador

D          G
Uma pequena via me levará
Bm              A
Para os Teus altares, oh meu rei
Bm      A               G
Pequeno serei então encontrarei
     Gm6
Um lugar em Tuas mãos
Bm      A           G
O Elevador que me fará subir até os céus
D   A                   G
São os Teus braços, oh Jesus
   Em           D/F#
Crescer me é impossível
     D
Devo suportar-me como sou
    Em                     C
Não quero ser grande, mas sim
            G A
Teu pequenino


      D
Como pássaro livre a voar
       Bm
Como a rosa no campo a nascer
       G            Gm6     D     A
Assim são os Teus pequeno-ooos
      D
Como a criança a correr
    Em              A          D   Bm
Para os Teus braços eu irei também
    Em              A           D
Para os Teus braços eu irei também

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
Gm6 = 3 X 2 3 3 X
