Comunidade Católica Shalom - Sacrário da Trindade

[Intro] C#m7  F#m7  C#m7  F#m7

  E          F#m7
Maria, Mãe de todos os povos
G#m7        Bm7     E7
Nós te amamos e bendizemos
   A7+         G#m7         C#m7
Por nos trazer, sempre em teu manto
F#m7       B7            E      B7
E por nos amar como teus filhos
F#m7       G#m7  F#m7            G#m7
Mãe da ternura, Sacrário da Trindade
D7+          A/C#
Junto a ti louvamos a Deus
     C7+             B7
Que te fez bem aventurada
F#m7          B7   E     B7
E por ti nos deu Jesus
E         F#m7
Maria, Mãe consoladora
G#m7          C#m7    Bm7        E7
Nos entregamos ao teu auxílio

  A7+            G#m7       C#m7
Pois somos teus, roga por nós
F#m7        B7               E     B7
E nos leva a Deus por teu Jesus

----------------- Acordes -----------------
A/C# = X 4 X 2 5 5
A7+ = X 0 2 1 2 0
B7 = X 2 1 2 0 2
Bm7 = X 2 4 2 3 2
C#m7 = X 4 6 4 5 4
C7+ = X 3 2 0 0 X
D7+ = X X 0 2 2 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m7 = 2 X 2 2 2 X
G#m7 = 4 X 4 4 4 X
