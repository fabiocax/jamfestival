Comunidade Católica Shalom - Glória a Deus Nas Alturas

[Intro] F  F  Bb/F
        Eb/F  Bb/F  F

 F      Eb9           Gm7  Bb9       Dm          C
Glória, glória, glória a Deus     nas altu......ras!
 F      Eb9           Gm7  Bb         Dm7     C       Bb
Glória, glória, glória a Deus      e aos homens toda paz... sua paz!

C            Bb      F        C
Senhor Deus, rei dos céus, Deus pai onipotente
 C          Bb     F    C
Nós Vos louvamos, bendizemos, adoramos
       Gm7          F/A       Bb Gm    Dm    C
Vos glorificamos e damos graças por Vossa imen-sa  gló-ria

 F      Eb9           Gm7  Bb9       Dm          C
Glória, glória, glória a Deus     nas altu......ras!
 F      Eb9           Gm7  Bb         Dm7     C       Bb
Glória, glória, glória a Deus      e aos homens toda paz... sua paz!

 C         Bb F       C
Senhor Jesus Cristo, filho unigênito

 C        Bb         F       C
Senhor Deus, cordeiro de Deus, filho de Deus pai
      Gm            F/A  Bb         C
Vós que tirais o pecado do mundo, tende piedade de nós
      Gm            F/A     Bb      Dm   C
Vós que tirais o pecado do mundo, acolhei a nossa súplica
 Bb         F/A     Gm      C    Bb  F/A   F Eb  Dm   Bb  C
Vós que estais sentado à direita do Pai, tem-de    piedade de nós

 F      Eb9           Gm7  Bb9       Dm          C
Glória, glória, glória a Deus     nas altu......ras!
 F      Eb9           Gm7  Bb         Dm7     C       Bb
Glória, glória, glória a Deus      e aos homens toda paz... sua paz!

  Eb           Bb    C/E F     C     Eb           Bb     F        C
Só Vós sois o Santo, só Vós o Senhor, só Vós o altíssimo Cristo Jesus
    Gm                    Bb            C           D
Com o Espírito Santo na glória de Deus pai, de Deus pai

G      F    Am C
Amém amém, amém
G  F   C
Amém amém, amém
Am  Em    D    C
Ame....ém!   Amém

[Final] C  G/B  F
        C/E  D4  D  G

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bb/F = X 8 8 7 6 X
Bb9 = X 1 3 3 1 1
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
D = X X 0 2 3 2
D4 = X X 0 2 3 3
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Eb = X 6 5 3 4 3
Eb/F = X 8 8 8 8 X
Eb9 = X 6 8 8 6 6
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
Gm = 3 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
