Comunidade Católica Shalom - Teu Coração

Intro: C   G/B   Am   Am/G   F     G4   G   C   G/B   Am   Am/G   F   G   C

 C      G/B      Am      Am/G     F        G4   G
Teu coração, é o meu coração,     Jesus, Jesus.
 C      G/B      Am      Am/G     F       G     C    G  Am  G/B
Teu coração, é o meu coração,     Jesus,     Jesus.
 C      G/B      Am      Am/G     F      Dm      Bb   G
Meu coração, é o Teu coração,     Jesus,       Jesus.
 C      G/B      Am      Am/G     F       G      C    A4  A
Meu coração, é o Teu coração,     Jesus,      Jesus.

 D      A/C#     Bm      Bm/A     G       Em     A4  A
Teu coração, é o meu coração,     Jesus,       Jesus
 D      A/C#     Bm      Bm/A     G       A     Bm  A  G  A  D
Teu coração, é o meu coração,     Jesus,     Jesus.       Jesus.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A4 = X 0 2 2 3 0
Am = X 0 2 2 1 0
Am/G = 3 X 2 2 1 X
Bb = X 1 3 3 3 1
Bm = X 2 4 4 3 2
Bm/A = X 0 4 4 3 X
C = X 3 2 0 1 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G4 = 3 5 5 5 3 3
