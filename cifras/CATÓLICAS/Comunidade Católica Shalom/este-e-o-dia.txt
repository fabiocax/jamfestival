Comunidade Católica Shalom - Este É o Dia

F         Gm           Am           Bb
Este é o dia que o Senhor fez para nós!
     F      Gm          Bb  C
Alegremo-nos e nele exultemos!
F         Gm           Am           Bb
Este é o dia que o Senhor fez para nós!
     F      Gm           C  F   Eb
Alegremo-nos e nele exultemos!

     F            Gm  Am           Bb
Daí graças ao Senhor porque ele é bom,
  F         Gm    Csus  F
Eterna é a sua misericórdia
   Bb         C/Bb        Am   Dm   Gm         Gm/F      Eb    Csus
A casa de Israel agora o di----ga, eterna é a sua misericórdia

   F           Gm         Am     Bb
A mão direita do Senhor fez maravilhas,
    F          Gm           Csus  F
A mão direita do Senhor me levantou
     Bb           C/Bb          Am  Dm
Não morrerei, mas ao contrário viverei

         Gm                Gm/F    Eb   Csus
Para contar as grandes obras do Senhor

   F              Gm      Am   Bb
A pedra que os pedreiros rejeitaram
    F              Gm       Csus F
Tornou-se agora a pedra angular
         Bb           C/Bb       Am  Dm
Pelo Senhor é que foi feito tudo isso,
        Gm           Gm/F       Eb     Csus
Que maravilhas ele fez a nossos olhos

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C/Bb = X 1 2 0 1 X
Csus = X 3 2 0 1 0
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
Gm/F = X X 3 3 3 3
