Comunidade Católica Shalom - Ensina-me a Te Amar

[Intro] Bb7M(9)  Eb7M  D7M(#11)  C#sus  E9  Bsus

E9                      A/E                F#/E     A/E
Se por Ti eu fui criado          e do barro fui tirado
E9 B/E                   C7M(#11) Bsus
Do Teu sopro veio o meu viver
E9                                 A/E              F#/E     A/E
     Eu que estava nos Teus planos       desde toda eter--nidade
E9          B/E    C7M(#11) Bsus
Quero em Ti permanecer
  E9            B/E    A      G#m7        F#m7       Bsus   E9 B/D#
Ensina-me a Te amar, ensina-me                      a Te buscar
 C#m7     C#m/B                 A              G#m
Ensina-me             a Te conhecer e a assim saber
      F#m7          G#m7              A           G#m
Que Tu és o meu Deus gran------de, Tu és o meu Deus for-----te
  F#m7              Bsus              G9(6) Esus
Tu és o meu Deus, Tu és o meu tudo, Senhor
     A        E/A     D/A     B/A  Bm7   G7M(9)   Esus
Nas Tuas criaturas, na luz que das alturas ilumina as trevas e tudo que há
     A      E/A       D/A
Na Tua obra prima, no homem que é de Ti

   B/A Bm7  Esus F#sus  Gsus  Absus
Imagem e semelhança, ó Deus Amor, Amor

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/E = 0 X 2 2 2 0
Absus = 4 3 1 1 1 4
B/A = X 0 4 4 4 X
B/D# = X 6 X 4 7 7
B/E = X 7 9 8 7 7
Bb7M(9) = X 1 0 2 1 X
Bm7 = X 2 4 2 3 2
Bsus = X 2 4 4 4 2
C#m/B = 7 X 6 6 5 X
C#m7 = X 4 6 4 5 4
C#sus = X 4 6 6 6 4
C7M(#11) = X 3 4 4 5 X
D/A = X 0 X 2 3 2
D7M(#11) = X X 0 1 2 2
E/A = X 0 2 1 0 0
E9 = 0 2 4 1 0 0
Eb7M = X X 1 3 3 3
Esus = 0 2 2 1 0 0
F#/E = X X 2 3 2 2
F#m7 = 2 X 2 2 2 X
F#sus = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
G#m7 = 4 X 4 4 4 X
G7M(9) = 3 2 4 2 X X
Gsus = 3 2 0 0 0 3
