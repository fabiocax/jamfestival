Comunidade Católica Shalom - À Casa do Pai

[Intro] G9  Bm7  A4  A
        G9  D/F#  Bm7  A4  A

Bm        D        E4          E
Vinde exultemos cantando ao Senhor
 G9                 Bm7             A4   A
Alegres voltemos em festa à casa do Pai
Bm          D        E4          E
Vinde ao banquete do seu grande Amor
      G9               Bm7              A4   A
Pois aquele que estava morto voltou a viver

      G9  D/F#      Bm7   A  D/F#      G9    D/F#       A4   A
Toda adoração, toda honra sempre ao Cordeiro Santo e Vencedor
      G9    D/F#     Bm7   A  D/F#       G9  D/F#       A4   A
Todas as nações cantarão a Glória e as Misericórdias do Senhor

Bm     D        E4          E
Vede o povo que Deus tanto amou
     G9            Bm7               A4   A
Revestido de Misericórdia e de compaixão

Bm     D    E4       E
Povo remido por sua Cruz
       G9                Bm7            A4   A
Nas estradas do mundo entoando um novo louvor

      G9  D/F#      Bm7   A  D/F#      G9    D/F#       A4   A
Toda adoração, toda honra sempre ao Cordeiro Santo e Vencedor
      G9   D/F#      Bm7   A  D/F#       G9  D/F#       A4   A
Todas as nações cantarão a Glória e as Misericórdias do Senhor

   A/C#  D    A     Bm        G9  D/F#   A4   A
É eter- -na, eter- -na, sua Misericór-  -dia

      G9  D/F#       A     Bm  A/C#  D  D/F#         G9    D/F#       A4   A
Toda adoração, toda honra sempre               ao Cordeiro Santo e Vencedor
      G9    D/F#     Bm7   A  D/F#       G9  D/F#       A4   A
Todas as nações cantarão a Glória e as Misericórdias do Senhor

[Final] G9  D/F#  Bm7  A
        G9  D/F#  A  D

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
Asus4 = X 0 2 2 3 0
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
E = 0 2 2 1 0 0
Esus4 = 0 2 2 2 0 0
G9 = 3 X 0 2 0 X
