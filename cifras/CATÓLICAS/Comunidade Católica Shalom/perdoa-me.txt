Comunidade Católica Shalom - Perdoa-me

 E9              B/D#     E9
Senhor, que Te deixaste ferir
       E/G#   A9  A/C#  E9
Do Teu sangue vem  a    Paz!
      F#m7    C#m7   B4  B
Aqui estou, perdoa - me!

C#m7  B/D#  A/E   E
Ky.rie     eleison
B/D#  C#m   B  A
Kyrie      eleison
B/D#  E F#m7 E/G#  A9
Ky...ri...e...e    eleison
C#m7  B  E9
Ooooo

    E9        B/D#     E9
Oh Cristo, elevado na Cruz
  E/G#   A9  A/C#   E9
És Amigo do  pe...cador!
    F#m7    C#m7     B4  B
Aqui estou, perdoa - me!


C#m7  B/D#   A/E  E
Christe  eleison
B/D#  C#m   B   A
Chri.iste...e   eleison
B/D# E F#m7 E/G#  A9
Chri.is.te...e   eleison
C#m7  B  E9
Ooo....o.o

   E9      B/D#        E9
Senhor, da morte, Vencedor
    E/G#    A9   A/C#  E9
Verdadeiro filho de    Deus!
     F#m7    C#m7    B4  B
Aqui estou, perdoa - me!

C#m7  B/D#  A/E   E
Ky.rie     eleison
B/D#  C#m   B   A
Kyrie      eleison
B/D#  E  F#m7  E/G#  A9
Ky...ri...e    eleison
C#m7  B  E9
Ooooo

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A/E = 0 X 2 2 2 0
A9 = X 0 2 2 0 0
B = X 2 4 4 4 2
B/D# = X 6 X 4 7 7
B4 = X 2 4 4 5 2
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E9 = 0 2 4 1 0 0
F#m7 = 2 X 2 2 2 X
