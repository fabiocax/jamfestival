Comunidade Católica Shalom - Ele Venceu

Intro: B  G#m  F#
     ( B  F#  E )

G#m          F#                    E
Celebramos a Cristo, aquele que em nós venceu.
G#m       F#               E
E com sua luz dissipou a escuridão em nós.
G#m             F#       E
Tempestades não podem abalar a fé
G#m          F#              E
De quem no Senhor encontra a proteção!

B/D#       E
Nada, nada nos calará!
B/D#           F#
Nada mais nos impedirá
C#m             B/D#
De erguer verdadeiro louvor.
      G            A             E/G#
Ao Senhor rendemos todo o nosso Amor!


 B            F#
Sim! Ressuscitou!
             G#m       E
Do abismo da morte Ele nos salvou!
 B           F#
Sim! Ele venceu!
        G#m          E
Basta olhar de onde nos tirou (bis)
     B       F#  E/G#  E
O Senhor!    Ô...Ô.....Ô

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
B/D# = X 6 X 4 7 7
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
G#m = 4 6 6 4 4 4
