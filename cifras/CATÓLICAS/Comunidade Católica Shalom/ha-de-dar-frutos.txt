﻿Comunidade Católica Shalom - Há de Dar Frutos

Capo Casa 2

[Intro] G  G  D  A

          D     D9                A9             G               A9
Há de dar frutos, o grão da minha vida, que hoje morre e vive em Ti
         D    D9               A9         G    Em            A9
Em minha carne, uma aliança se faz, pelo Teu Espirito fecundará
      G        D/F#   Em     A     D    D9
Para sempre o sim que não volta atrás

        Bm                  F#m       G  A               Bm
Há de subir como incenso ao céu o louvor..  que se erguerá
               F#m            G           A9     Em        A4  A7
pela oferta do meu coração de tudo o que sou, de tudo o que sou..

      Bm              F#m             G          A   D
Minha vida será terra pronta para acolher Tua vontade
    C              G                 A9    A4   A7
apoiado em tuas promessas quero ser fiel.

      Bm              F#m             G          A   D
Minha vida será terra pronta para acolher Tua vontade

    C              G            A4   A7        D   A9
apoiado em tuas promessas quero ser...   fiel ti.

        Bm                  F#m       G  A               Bm
Há de subir como incenso ao céu o louvor..  que se erguerá
               F#m            G           A9     Em        A4  A7
pela oferta do meu coração de tudo o que sou, de tudo o que sou..

      Bm              F#m             G          A   D
Minha vida será terra pronta para acolher Tua vontade
    C              G                 A9    A4   A7
apoiado em tuas promessas quero ser fiel.

      Bm              F#m             G          A   D
Minha vida será terra pronta para acolher Tua vontade
    C              G            A4   A7        D
apoiado em tuas promessas quero ser...   fiel ti

----------------- Acordes -----------------
Capotraste na 2ª casa
A*  = X 0 2 2 2 0 - (*B na forma de A)
A4*  = X 0 2 2 3 0 - (*B4 na forma de A4)
A7*  = X 0 2 0 2 0 - (*B7 na forma de A7)
A9*  = X 0 2 2 0 0 - (*B9 na forma de A9)
Bm*  = X 2 4 4 3 2 - (*C#m na forma de Bm)
C*  = X 3 2 0 1 0 - (*D na forma de C)
D*  = X X 0 2 3 2 - (*E na forma de D)
D/F#*  = 2 X 0 2 3 2 - (*E/G# na forma de D/F#)
D9*  = X X 0 2 3 0 - (*E9 na forma de D9)
Em*  = 0 2 2 0 0 0 - (*F#m na forma de Em)
F#m*  = 2 4 4 2 2 2 - (*G#m na forma de F#m)
G*  = 3 2 0 0 0 3 - (*A na forma de G)
