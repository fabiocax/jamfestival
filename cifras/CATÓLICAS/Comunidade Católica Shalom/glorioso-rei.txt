Comunidade Católica Shalom - Glorioso Rei

[Intro] C9  D4  G/B  Am  C9  E  D  A  C9  D4  G/B  Am7  C9  G/B  Am7  G/B  Am7  G  D4
        C9  D4  G/B  Am  C9  E  D  A  C9  D4  G/B  Am7  C9  G/B  Am7  G/B  Am7  G  D4

     C9      G/B        C9             D4
Como povo redimido, resgatado por Tua Cruz
     C9            G/B       Am7   G9    D4
Povo eleito, povo santo, celebramos Teu amor!
      C9              G/B          C9       D4
Entraremos por Tuas portas entre hinos de louvor
         C9          G/B           Am7      G9      D4
Novas vestes, novo canto. Nossa herança és Tu, Senhor!
   C9  G9    D9            C9   D9   Em
Glorio...so Rei! Ven...ce...dor na Cruz!
          Am       D9           Em
Por Teu sangue, enfim, temos a Paz!
  D      C9 G9 D9            C9 D9   Em
To...das as nações vi...rão Te ado..rar
       F   D9           G
Neste dia que não terminará

      C9           G/B         Am7         G/B
Seja uma a nossa voz! Seja eterno o nosso "Sim”!

        C9        D9          E4         E
Ao redor do Teu altar, para sem..pre!
      C9            G/B           Am7      G/B
Ao Rei dos reis que vem: Honra, glória e louvor
        C9         Am7           D4
Nossa vida, nosso amor, para sempre!
  C9  G9          D9              C9   D       Em
Glo...ri...o...so Rei! Ven...ce...dor na Cruz!
            C9      D4         Em
Por Teu sangue, enfim, temos a Paz!
   D      C9 G  D             C9 D  Em
To...das as nações vi...rão Te ado..rar
          F    D9        G
Neste dia que não terminará

( C9  D4  G/B  Am  C9  E  D  A  C9  D4  G/B  Am7  C9  G/B  Am7  G/B  Am7  G  D4 )
( C9  D4  G/B  Am  C9  E  D  A  C9  D4  G/B  Am7  C9  G/B  Am7  G/B  Am7  G  D4 )

Como povo redimido
Entraremos por Tuas portas
Glorioso Rei!

      C9         G/B          Am7                 G/B
Seja uma a nossa voz! Seja eterno o nosso "Sim”!
         C9                D9        E4          E
Ao redor do Teu altar, para sem..pre!
        C9                 G/B         Am7                G/B
Ao Rei dos reis que vem: Honra, glória e louvor
         C9                Am7             D9                     E
Nossa vida, nosso amor, para sempre! para sempre!
 D9         A E     F#m E   D  E      F#m
Glorio...so Rei! Ven...ce...dor na Cruz!
               Bm7    E4                F#m
Por Teu sangue, enfim, temos a Paz!
F#m  E  D A   E F#m   E   D9  E    F#m
To...das as nações vi...rão Te ado..rar
          G    E
Neste dia que não terminará

[Final] D  E4  A/C#  A  D9  D/F#  E  G  D/F#  G  D/F#  A

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Bm7 = X 2 4 2 3 2
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D4 = X X 0 2 3 3
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
E4 = 0 2 2 2 0 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G9 = 3 X 0 2 0 X
