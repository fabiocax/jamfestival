Comunidade Católica Shalom - Eterna União

[Intro] Dm  Dm/C#  Dm/C  Bb7+
        Dm  Dm/C#  Dm/C  Bb7+

F        C/F         Bb7M
Quem sou eu para que lembres tanto assim de mim
      Dm7               Dm7/C         G/B  Bb9
E me cerques com tanto carinho e proteção?
F       C/F         Bb7M
Quem és Tu amor que nunca desistiu de mim
      Dm7          Dm7/C       G/B        Bbm6   F     C#°
E com paciência me trouxe até aqui para me desposar?

      Dm                        Dm/C#               Dm/C
Então deixo que as chamas do teu infinito amor me consumam por inteiro
        Cm7 F7    Bb7+              Bbm6
Até que não reste nada meu, mas tudo seja Teu
    Dm                 Dm/C#
E tomado pela força da Tua ressurreição
      Dm/C                      Cm7 F7  Bb7+
Sinto Tua vida em minha carne e já  não vivo eu
           Bbm9                      F
Mas vive o amado meu eternamente em mim


F/A Bb    C        F  F/A   Bb   C            Dm7
Oh  doce eterna união que supera toda imperfeição
Dm7/C   G/B                    Bbm6
Que com amor juntais coisas tão desiguais
       Gm   C7/4     C7          F
Para manisfestar sua glória e o seu poder

F/A Bb    C        F  F/A   Bb   C            Dm7
Oh  doce eterna união que supera toda imperfeição
Dm7/C   G/B                    Bbm6
Que com amor juntais coisas tão desiguais
       Gm   C7/4     C7         Dm
Para manisfestar sua glória e o seu poder

       Dm                        Dm/C#              Dm/C
Então deixo que as chamas do teu infinito amor me consumam por inteiro
        Cm7 F7    Bb7+              Bbm6
Até que não reste nada meu, mas tudo seja Teu
    Dm                 Dm/C#
E tomado pela força da Tua ressurreição
      Dm/C                      Cm7 F7  Bb7+
Sinto Tua vida em minha carne e já  não vivo eu
           Bbm6                     F
Mas vive o amado meu eternamente em mim

F/A Bb    C        F  F/A   Bb   C            Dm7
Oh  doce eterna união que supera toda imperfeição
Dm7/C   G/B                    Bbm6
Que com amor juntais coisas tão desiguais
       Gm   C7/4     C7          F
Para manisfestar sua glória e o seu poder

F/A Bb    C        F  F/A   Bb   C            Dm7
Oh  doce eterna união que supera toda imperfeição
Dm7/C   G/B                    Bbm6
Que com amor juntais coisas tão desiguais
       Gm   C7/4     C7         Dm
Para manisfestar sua glória e o seu poder

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bb7+ = X 1 3 2 3 1
Bb7M = X 1 3 2 3 1
Bb9 = X 1 3 3 1 1
Bbm6 = 6 X 5 6 6 X
Bbm9 = X 1 3 5 2 1
C = X 3 2 0 1 0
C#° = X 4 5 3 5 3
C/F = X 8 10 9 8 8
C7 = X 3 2 3 1 X
C7/4 = X 3 3 3 1 X
Cm7 = X 3 5 3 4 3
Dm = X X 0 2 3 1
Dm/C = X 3 X 2 3 1
Dm/C# = X 4 3 2 3 X
Dm7 = X 5 7 5 6 5
Dm7/C = X 3 0 2 1 1
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
F7 = 1 3 1 2 1 1
G/B = X 2 0 0 3 3
Gm = 3 5 5 3 3 3
