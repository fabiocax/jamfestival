Comunidade Católica Shalom - A Tua Ternura

[Intro] Ab  Eb/G  Fm  Eb/G  Db  Eb  Fm  Bbm  Eb/G

Db      Ab/C        Bbm       Ab/C
Incomparável Deus amigo meu amado meu
 Db       Ab/C       Bbm         Ab
Quem poderá nos separar do Teu amor
 Bbm            Ab/C     Db  Ab/C
Morte ou dor, guerra ou solidão
 Bbm                Ab/C    Fm         Eb/G
Nada é maior que a bondade do Teu coração

        Ab            Eb/G        Fm            Eb
O Teu amor me conquistou, Tua ternura me  alcançou
            Db         Ab/C          Bbm          Eb/G
Como não te adorar, me entregar, corresponder ao Teu amor
       Ab          Eb/G        Fm             Eb
Que feriu meu coração e me ergueu com seu perdão
      Db             Ab/C     Bbm       Eb
Todo louvor a Ti, aquele que tudo em mim  realizou

( Db/F  Fm7  Eb6  Db  Fm  Bbm  Ab )


  Db           Ab/C                 Fm     Ab/C   Eb/Bb  Fm   Eb
Quero amar-Te mais, bem mais que a mim naqueles que tu   amas
  Db           Ab/C              Bbm    Db        Ebsus4  Eb
Quero amar-Te mais, bem mais que a mim naqueles que tu    amas

        Ab            Eb/G        Fm            Eb
O Teu amor me conquistou, Tua ternura me  alcançou
            Db         Ab/C          Bbm          Eb/G
Como não te adorar, me entregar, corresponder ao Teu amor
       Ab          Eb/G        Fm             Eb
Que feriu meu coração e me ergueu com seu perdão
      Db             Ab/C     Bbm       Eb
Todo louvor a Ti, aquele que tudo em mim  realizou

[Final] Db/F  Fm7  Eb6  Db  Ab/C  Bbm  Ab

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Ab/C = X 3 X 1 4 4
Bbm = X 1 3 3 2 1
Db = X 4 6 6 6 4
Db/F = X 8 X 6 9 9
Eb = X 6 5 3 4 3
Eb/Bb = X X 8 8 8 6
Eb/G = 3 X 1 3 4 X
Eb6 = X X 1 3 1 3
Ebsus4 = X 6 8 8 9 6
Fm = 1 3 3 1 1 1
Fm7 = 1 X 1 1 1 X
