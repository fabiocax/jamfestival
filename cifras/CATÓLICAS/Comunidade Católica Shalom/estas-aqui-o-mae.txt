Comunidade Católica Shalom - Estás Aqui, Ó Mãe

Intro:  C/Bb Bb  F/A  Dm7  C/E  F4  F  F/A  Bb  Bb

F                   F7M(9)
Por teus olhos eu aprendo a espero a esperar
Bb              F/A           Gm       Gm7(11)
Por teus lábios eu aprendo a louvar
F                  F7M(9)
Em teus passos eu aprendo a caminhar            2ªv
Bb                    F/A           Gm        C74(9)
Ao ver-te aos pés da cruz aprendo a confiar
          F          C/E
Quando a força me faltar
         Gm         C
Quando a vida me provar
          Dm            F/A
Eu quero estar em teus braços, ó Mãe
     Gm             Bb                 C74(9)  C  (intro)
E de ti aprender a crer, a esperar, a amar.

        F     C/E
Estás aqui, ó Mãe

        Dm     C
Estás aqui, ó Mãe
   F/A         Bb
Consolo no caminho
             F/A
Ternura e carinho
           Gm        Gm/F         C
Sob o teu manto encontrei meu lugar.

   Dm  C/E      F  C/E     Bb     F/A       Gm  Gm/F  C
Mari....ia!  Mari...ia!  Maria   Maria  Mari..ra!

  Bb           F/A
Mari....ia!   Mari....ia!
           Gm          Gm/F       C
Sob o teu manto encontrei meu lugar.

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C/Bb = X 1 2 0 1 X
C/E = 0 3 2 0 1 0
C74(9) = X 3 3 3 3 3
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
F4 = 1 3 3 3 1 1
F7M(9) = X 8 7 9 8 X
Gm = 3 5 5 3 3 3
Gm/F = X X 3 3 3 3
Gm7(11) = 3 X 3 3 1 X
