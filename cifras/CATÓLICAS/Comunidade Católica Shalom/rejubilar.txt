﻿Comunidade Católica Shalom - Rejubilar

Capo Casa 1

    E7M    D7M                    E7M          D7M         Bsus
Vou cantar,   pular   de alegria, rejubilar, raiou    novo dia
     E7M               D7M               Bsus  E7M  F#m7  Cº
Eu vou cantar, pular    de alegria

C#m7             G#m7                        A         A7M      G#m7
O sol    nascente brilhou mais forte, lançando seus raios de luz   e calor
      F#m7                            C#m7
Tal qual   Jesus vencendo a morte
                     D7M               B
Tornando em dia    uma noite de horror

C#m7          G#m7                    A          A7M      G#m7
Tristeza   se tornou alegria   e o cinza da terra em cores mudou
   F#m7                         C#m7
A todos fez brilhar nova vida
                       D7M        B
Brotou a esperança e um novo amor

----------------- Acordes -----------------
Capotraste na 1ª casa
A*  = X 0 2 2 2 0 - (*A# na forma de A)
A7M*  = X 0 2 1 2 0 - (*A#7M na forma de A7M)
B*  = X 2 4 4 4 2 - (*C na forma de B)
Bsus*  = X 2 4 4 4 2 - (*Csus na forma de Bsus)
C#m7*  = X 4 6 4 5 4 - (*Dm7 na forma de C#m7)
Cº*  = X 3 4 2 4 2 - (*C#º na forma de Cº)
D7M*  = X X 0 2 2 2 - (*D#7M na forma de D7M)
E7M*  = X X 2 4 4 4 - (*F7M na forma de E7M)
F#m7*  = 2 X 2 2 2 X - (*Gm7 na forma de F#m7)
G#m7*  = 4 X 4 4 4 X - (*Am7 na forma de G#m7)
