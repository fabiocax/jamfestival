Comunidade Católica Shalom - Rainha do Meu Coração

  G
Aqui estou
Am                  G/B
venho consagrar-me a ti.
   C
Me ofereço por inteiro como prova
         D/F#
da minha devoção.
  G
Aqui estou,
Am             G/B
ó incomparável mãe.
C             Am
Guarda-me e defende-me.
    Em      D/F#
Sou todo teu.
G           Am
Rainha do céu
               G/B
E do meu coração
   C
Segura firme a minha mão,

    Am           D
me ajuda a caminhar.
G           Am
Rainha do céu
               G/B
E do meu coração
   C                    Am
Segura firme a minha mão
 D                G    Am G/B C Am G/B C
Guarda-me em teu Jesus.
   G   Am   G/B C
Maria, Maria…
      Am        G/B      C D
Quão doce é teu nome entoar
  G    Am   G/B C
Maria, Maria…
   Am            D        G
Eu quero em teus braços ficar

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
