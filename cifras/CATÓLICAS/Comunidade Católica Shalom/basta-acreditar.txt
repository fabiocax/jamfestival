Comunidade Católica Shalom - Basta Acreditar

G                C9
Vamos cantar e celebrar
    Am                    D
A presença de Deus entre nós
 G         C9
O Seu amor, o Seu poder
   Am                D
E tudo que Ele pode fazer
      Em           Bm
Basta abrir o coração
   Em           Bm
E deixar Deus tocar
     Am  D            G
Emfim   , basta acreditar
          Em         Bm
Basta render-se ao Senhor
     Em        Bm    Am
Pois não há maior amor
        D                     G
Do que dar a vida como fez Jesus

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
