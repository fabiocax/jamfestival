Rosa de Saron - Memories

           F                 C9/E
My day has gone, the night is bringing
               Eb               Bb9
memories of past, memories of you
       Gm                 C
don't think that I've forgotten you
            F  Gm  Bb9  Dm  F  Gm  Bb9  Dm  C
I'm right here

F             C9/E         Eb
Sleeples and cold, it's four ten,
          Bb9                  F
only four ten at dawn, it's alright,
                C9/E        Eb            Bb9
I'm drinking a coffee, I get up, open the window
        Gm               C
and I scream out loud, loudly

        F             C9/E
Wherever you are, I will be
                 Dm                 Am7
Hoping that you will, be waiting for me

                 Bb9                     F/A
And you must be sure, I'm really your friend
               Ab9    Eb        F    C9  Eb  Bb9  F
Forever your friend, I'm very glad

             C9/E              Eb
It's only mid-day, of rainy weekend
          Bb9            F
I was in bed, but I woke up
              C9/E            Eb
I put on some pants, I pack my bags
              Bb9         Gm               C
grab my old guitar, Look, I'm right behind you

         F          C9/E
Where you are, I will be
                 Dm                 Am7
Hoping that you will, be waiting for me
              Bb9                        F/A
And you must be sure, I'm really your friend
               Ab9    Eb        F
Forever your friend, I'm so glad


Base Solo:

( F  Eb  Bb9  Eb  C9 ) (2x)


F          Eb                            Bb9
Then I realized, that the peace I didn't have
             Eb            C9                       F
Was here inside and I was lost in a castle of illusion
                    Eb                    Bb9
And with my mind clear, I'm coming back home
             Eb
because today I live,

                 F           C9/E
'cause where you are, I will be
               Dm                  Am7
Hoping that too wil, be waiting for me
                  Bb9                   F/A
And you must be sure, I'm really you friend
           Ab9    Eb        F
Forever friend, I'm very glad

Yeah, there was a castle of illusion

In a castle of illusion

----------------- Acordes -----------------
Ab9 = 4 6 8 5 4 4
Am7 = X 0 2 0 1 0
Bb9 = X 1 3 3 1 1
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
C9/E = X X 2 5 3 0
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
Gm = 3 5 5 3 3 3
