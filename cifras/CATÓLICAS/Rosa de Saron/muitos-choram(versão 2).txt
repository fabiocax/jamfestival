Rosa de Saron - Muitos Choram

Intro: Am  F  Am  F  Am  F

Am
O vento bate na janela, eu busco asas pra voar
F                    Em
Pra bem mais perto de você
F                    G         C G/B
    Bem mais perto de você

Am                                        C
Estamos livres, mas sozinhos, abandonados
F                             Em
    Por quem tinha que nos entender
F                             G
    Por quem tinha que nos defender

C         G           F
    Hoje muitos choram
                          C
Mas não desistem de viver
     G         Dm     G
Hoje muitos choram sorrindo

C         G       F
Hoje muitos choram
                       C
Mas não desistem de viver
        G     Dm     G
Hoje muitos choram sorrindo

( Am  F  Am  F  Am  F )

Am
    A vida passa como um rio, e não é mais tudo tão lindo
F                      Em           F
    Nos resta apenas confiar em Deus
                       G     C  G/B
Nos resta apenas confiar
Am                                                     C
    São só palavras e promessas dissimuladas, sem noção
F                                Em
    Que muitas vidas correm em suas mãos
F                                G
    Que muitas vidas correm em suas mãos

C         G           F
    Hoje muitos choram
                          C
Mas não desistem de viver
     G         Dm     G
Hoje muitos choram sorrindo
C         G       F
Hoje muitos choram
                       C
Mas não desistem de viver
        G     Dm     G
Hoje muitos choram sorrindo

C         G           F
    Hoje muitos choram
                          C
Mas não desistem de viver
     G         Dm     G
Hoje muitos choram sorrindo
C         G       F
Hoje muitos choram
                       C
Mas não desistem de viver
        G     Dm     G      F   G   Dm
Hoje muitos choram sorrindo

(solo) Dm  F  C  G (3x) Dm  F  Am  G

(solo)
E|----------------10----8-7------------------8------------------------|
B|--5h6p5--5h6~/8-----------8-5-------3h5-6----6--8-----8-6--5h6p5-|
G|-------7-----------------------4-5----------------7--------------7--|
D|--------------------------------------------------------------------|
A|--------------------------------------------------------------------|
E|--------------------------------------------------------------------|

E|----------------------------8/10-10-10-8-8p7-------12--10-8-10-12-10-8---|
B|--------------5---5-6-8-6-8-------------------8-8----------------------10-|
G|-5-4-5--4-5-7---7---------------------------------------------------------|
D|--------------------------------------------------------------------------|
A|--------------------------------------------------------------------------|
E|--------------------------------------------------------------------------|

E|----13--12--10--8--8-8/10-----------------------------------|
B|--10--10--10-----10--------12-------------------------------|
G|------------------------------------------------------------|
D|------------------------------------------------------------|
A|------------------------------------------------------------|
E|------------------------------------------------------------|

C         G           F
    Hoje muitos choram
                          C
Mas não desistem de viver
     G         Dm     G
Hoje muitos choram sorrindo
C         G       F
Hoje muitos choram
                       C
Mas não desistem de viver
        G     Dm     G      F   G   Dm
Hoje muitos choram sorrindo
(Am  F  C  G)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
