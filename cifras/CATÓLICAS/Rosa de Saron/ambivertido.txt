Rosa de Saron - Ambivertido

[Intro]  Cm  Eb  F  Gm  Ab
         Eb  Cm  Eb  F  Gm  Ab

Eb
Oi, Meu prezado amigo
                Cm
Desneceselfies não
Gm            Eb
dizem algo aqui
Eb
À não ser que eu esteja bem
                    Cm
Neste caso, há um porém
Bb               Ab
Tire um retrato meu
           Cm      Ab
E então, divulgue-me

Cm
Não sei
            Ab
Se é assim, se é pra mim

Eb
Se é o fim, o começo
Gm                Ab
Ou o erro que não se renuncia
Eb            Cm              Eb
Ou enfim a viragem, o prenúncio

O motim, o indício
Fm     Ab      Eb
De alguém a vir

Eb
Oi
É tão comum falar de mim mesmo
     Cm  Bb              Eb
Que não vejo mais nada além
Eb
Que meus mesmos améns
                 Cm
Tão sedentos de Deus
Bb             Ab
Solitários em si
E em mim
Um desmirim

Cm
Não sei
            Ab
Se é assim, se é pra mim
            Eb
Ou se é o fim, o começo
                   Ab
Ou o erro que não se renuncia
Bb             Cm           Eb
E enfim a viragem, o prenúncio

O motim, o indício
Fm  Gm       Bb        Cm
De alguém a vir e a sorrir
  Eb    Ab
E permitir-me
  Bb            Cm
Dizer, ipsis verbis:
            Ab
"Paixões febris
                   Eb
Não aquecem corações frios"

Tão frios

Cm
E eu não sei
Ab         Eb          Gm
Eu não sei se é assim, se é pra mim
Eb         Ab  Eb
Ou se é o fim, o começo

Ou se é aqui
        Cm         Eb       Fm
Que não se recomeça enfim o "eu sei"
Ab
Eu sei?
Eb
Eu sei...

( Eb )

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
Gm = 3 5 5 3 3 3
