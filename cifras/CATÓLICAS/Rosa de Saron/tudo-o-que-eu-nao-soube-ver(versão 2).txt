Rosa de Saron - Tudo o Que Eu Não Soube Ver

D                F#m                      G
Eu vejo nos seus rostos as marcas de uma vida
                A
Tristezas e alegrias
    D            F#m                     G
Eu vejo nos seus olhos, toda a minha história
                     A       D
E tudo o que eu não soube ver

D             G            D
Acho que não sei me expressar
D             G            D
Mil Razões terei pra tentar
D             G        D
Meu coração irei entregar
D                      G
Pois tudo o que há em mim
             D
Veio de Vocês

D            F#m                        G
Carrego suas marcas, seus traços dia a dia

                      A
Eternamente em meu viver
    D               F#m                        G
De tudo o que há na vida, seu amor me traz certeza
                    A     D
Existe algo para acreditar

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
