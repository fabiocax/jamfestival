Rosa de Saron - Olhos Vermelhos

(intro) Em D C D

Em                             D
Eu vejo seus olhos e o que eles vêem?
        C                       D
Por mais que eu tente não posso entender
   Em                 D
A sua beleza dissipa no ar
       C                         D
Enquanto seus sonhos caem por terra
Em                     D
Ao seu redor há tantas pessoas
         C              D
Mas todas estão distantes de você
Em                      D
Perdi um pouco de minha alegria
       C                         D
Por não conseguir te estender minha mão
Em                              C
Você se senta num banco e não mais se levanta
Em                              C             D
Você se fecha por dentro em quatro paredes se tranca

Em             D                C
Olhos vermelhos, sinto que vou chorar
      Em         D       C
Por ver te levarem sem nem mesmo lutar

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
