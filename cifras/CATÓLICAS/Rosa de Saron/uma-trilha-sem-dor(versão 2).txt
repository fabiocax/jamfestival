Rosa de Saron - Uma Trilha Sem Dor

E     B    C#m           A          E
 Me vi sair, trancado estava o porão
     B        C#m     A           E
 Já posso ouvir uma trilha sem dor
        B        C#m         A          E
 As paixões enfim, nos seus átrios deixei
       B           C#m    A          C#m
 E sorri, pois senti um amor sem igual
             B                         E
 Como uma canção, composta em meu coração
                 B          C#m
 Pode te transmitir toda verdade
                   A
 Do que sinto por você
      C#m              B            E
 Alma veja, perceba, sinta-se imortal...

( E B C#m A )

              E B C#m A
 Imortal em ti

E            B                      C#m
 Um acorde ouvi enquanto abria o portão
            A                   E
 Campos corri em busca de uma flor
                     B         C#m
 Como um anjo que habita o jardim
                    A                C#m
 Eu sorri, pois senti um amor sem igual
                B                       E
 E com essa canção composta em meu coração
                  B           C#m
 Quero te transmitir toda verdade
                    A
 Do que sinto por você
       C#m            B            E
 Alma veja, perceba sinta-se imortal
              E B C#m A
 Imortal em ti

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
