Rosa de Saron - Latitude, Longitude

Dm           F               C       G/B
  Pra quem fugiu, até que eu ando bem
Dm        F                C                 G
  Eu só esqueço, às vezes, onde guardei o olhar
    Dm         F              C            G/B
E amarro as memórias em um abraço dado num cobertor
Dm              G/B
  Não há nada errado aqui
      C     F
Só o frio e eu

         Am            F
Se for assim, eu fico aqui
        C             G/B                   Am
Serei então os passos dos caminhos que caminhei
 (G/B C)   F            C
Por onde andei, o que vivi
        G/B
E lembrarei dos pés que andavam
       Am           F
E que agora pairam pelo ar

C             G                           Am
 Sem ao menos saber quando vão se encontrar
(G/B C)   F          C
  Então caí. Descaminhei
         G/B                      F
Porque parei de andar por onde andei

Dm         F             C       G/B
  Eu procurei pegadas no alto mar
Dm      F           C                   G
  Me afoguei e cá retorno triste ao meu lar
   Dm            F
Talvez seja hora de mudar
        C             G/B
Sei que Deus tem algo para mim
Dm            G/B                    C    F
  Mas sei que tenho que dar o meu melhor aqui

          Am           F
Porque senão, eu fico aqui
        C             G/B                   Am
Serei então os passos dos caminhos que caminhei
 (G/B C)   F            C
Por onde andei, o que vivi
        G/B
E lembrarei dos pés que andavam
       Am           F
E que agora pairam pelo ar
C             G                           Am
 Sem ao menos saber quando vão se encontrar
(G/B C)   F          C
  Então caí. Descaminhei
         G/B
Porque parei de andar

           F     G
Eu verei o Teu sinal
           Am     C
Eu verei o Teu sinal
           F      G       Am
Eu verei o Teu sinal em breve,

Breve, breve (Repete)

( F  G  Am )

         Am            F
Se for assim, eu fico aqui
        C             G/B                   Am
Serei então os passos dos caminhos que caminhei
 (G/B C)   F            C
Por onde andei, o que vivi
        G/B                          Am F C
E lembrarei dos pés que andavam no chão...
        G/B                          Am F C G Am
E lembrarei dos pés que andavam no chão...

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
