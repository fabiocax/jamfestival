Rosa de Saron - Mais Que Um Mero Poema

           Dm
Parece estranho
         Bb9                  F
Sinto o mundo girando ao contrário
                             Dm
Foi o amor que fugiu da sua casa
        Bb9
E tudo se perdeu no tempo

    F
É triste e real
Eu vejo gente se enfrentando
         Dm
Por um prato de comida
Bb9
Água é saliva
F
Êxtase é alívio, traz o fim dos dias
      Dm                  Bb9
E enquanto muitos dormem, outros se contorcem
      F                                       G9  Bb9
É o frio que segue o rumo e com ele a sua sorte


          Dm
Você não viu?
         Bb9             F
Quantas vezes já te alertaram
                            Dm
Que a Terra vai sair de cartaz
           Bb9
E com ela todos que atuaram?
   F
E nada muda, é sempre tão igual
A vida segue a sina

 Dm                    Bb9
Mães enterram filhos, filhos perdem amigos
  F
Amigos matam primos
                                 Dm
Jogam os corpos nas margens dos rios contaminados
 Bb9
Por gigantes barcos
   F                                   G9
Aquilo no retrato é sangue ou óleo negro?

   Bb9           C9        Dm              F
Aqui jaz um coração que bateu na sua porta às 7 da manhã
  Bb9             C9     Dm                   F
Querendo sua atenção, pedindo a esmola de um simples amanhã
 Bb9                C9
Faça uma criança, plante uma semente
    Dm                               F
Escreva um livro e que ele ensine algo de bom
   Bb9                      C9
A vida é mais que um mero poema
       Dm
Ela é real

         Dm
É pão e circo, veja
   Bb9            F                                   Dm
A cada dose destilada, um acidente que alcooliza o ambiente
          Bb9
Estraga qualquer face limpa
      F
De balada em balada vale tudo
E as meninas
     Dm                     Bb9
Das barrigas tiram filhos, calam seus meninos
 F
Selam seus destinos
                         Dm
São apenas mais duas histórias destruídas
    Bb9                  F
Há tantas cores vivas caçando outras peles
Movimentando a grife

   G9                  Bb9                   Dm
A moda agora é o humilhado engraxando seu sapato
                     F
Em qualquer caso é apenas mais um chato

Bb9              C9        Dm                    F
Aqui jaz um coração que bateu na sua porta às 7 da manhã
  Bb9             C9     Dm                   F
Querendo sua atenção, pedindo a esmola de um simples amanhã
 Bb9                C9
Faça uma criança, plante uma semente
Dm                                F
Escreva um livro e que ele ensine algo de bom
   Bb9                        C9
A vida é mais que um mero poema
       Dm
Ela é real

Bb9                        C9
E ainda que a velha mania de sair pela tangente
 Dm
Saia pela culatra
          F                  Bb9
O que se faz aqui, ainda se paga aqui
          C
Deus deu mais que ar, coração e lar
 F
Deu livre arbítrio
Bb9
E o que você faz?
C9            G9
E o que você faz?

Bb9              C9   Dm  F
Aqui jaz um coração

Bb  C  Dm

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bb9 = X 1 3 3 1 1
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G9 = 3 X 0 2 0 X
