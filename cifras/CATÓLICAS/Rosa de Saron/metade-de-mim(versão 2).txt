﻿Rosa de Saron - Metade de Mim

Afinação Meio Tom Abaixo

Intro 2x: C  F

Riff 2x: C  F

Eb|--------0------------0------|
Bb|-----1-----1------1-----1---|
Gb|--0------------0------------|
Db|----------------------------|
Ab|----------------------------|
Eb|----------------------------|

Eb|----------------------------|
Bb|-------3-----------3--------|
Gb|----5----5------5-----5-----|
Db|--3----------3--------------|
Ab|----------------------------|
Eb|----------------------------|

Primeira Parte:
Am            G          C
   Se um dia você me chamar

            F               Am
Eu posso estar, eu posso estar
            G           C
Qualquer lugar é meu lugar
           F            C
Desde que seja o seu lugar
            C7           F
Longe, e assim perto de mim?
          Fm         C
Posso sentir você aqui
        G
Você aqui é tudo

Segunda Parte:
Dm                               F
   É como se o mar inventasse a sua água
Dm                             F
   É como se o ar resolvesse aparecer

Refrão:
           C  Am
Eu posso amar
     G
Me acolhe, me acolhe
          F                    C  Am
E me leve aonde quer que você vá
   G
Você é a metade de mim
    F                      Dm        Fm
Que eu nunca soube estar procurando por aí

Intro: C  F  (2x)

Primeira Parte com variação na letra:
Am          G         C
   Seu coração é o lugar
                F               Am
Que eu quero estar, eu quero estar
         G           C
Ouvir o som que ele faz
            F            C
Eu quero estar junto de Ti
            C7            F
Perto de você eu não sou nada
            Fm
Eu sou ninguém
            C
Eu fui ninguém
           G
Serei ninguém
Basta

Segunda Parte:
Dm                             F
   Só isso basta pra eu ser feliz
        Dm                                   F
Sempre.     Eu pararia todo o tempo neste momento aqui

Refrão:
           C  Am
Eu posso amar
     G
Me acolhe, me acolhe
          F                    C  Am
E me leve aonde quer que você vá
   G
Você é a metade de mim
    F                      Dm        Fm
Que eu nunca soube estar procurando por aí

(C  Am  G  F)

Refrão:
           C  Am
Eu posso amar
   G
Você é a metade de mim
  F               Dm
E eu pararia aqui
      Fm
Pararia aqui

Intro: C  F  (2x)  C

----------------- Acordes -----------------
Am*  = X 0 2 2 1 0 - (*G#m na forma de Am)
C*  = X 3 2 0 1 0 - (*B na forma de C)
C7*  = X 3 2 3 1 X - (*B7 na forma de C7)
Dm*  = X X 0 2 3 1 - (*C#m na forma de Dm)
F*  = 1 3 3 2 1 1 - (*E na forma de F)
Fm*  = 1 3 3 1 1 1 - (*Em na forma de Fm)
G*  = 3 2 0 0 0 3 - (*F# na forma de G)
