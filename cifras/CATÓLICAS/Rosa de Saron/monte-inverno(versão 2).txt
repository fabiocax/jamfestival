Rosa de Saron - Monte Inverno

Intro: D   Bm
          D   Bm   A

D                       D/C#   D                         Bm
Ouço a voz do vento a chamar pelo meu nome
                                                G
E creio estar sentido a sua presença
D/ F#     Em7   G
A minha volta
    D                  D/C#             D                              Bm
Olhei pra traz e vi meus antigos sonhos, e até chorei
                                             G
E hoje sinto saudades do que falei
    D/F#                          Em7    A
Lamento de mais a sua falta.

     D
Eu quero ver o sol atrás do monte, eu quero ver o brilho que ele traz
                                                Bm
Eu quero ouvir de novo a sua voz!
     G                                                     D/F#
Eu quero ver o sol atrás do monte, eu quero ver o brilho que ele traz

      Em7                                        G     F#m    Bm     A
Eu quero ouvir de novo a sua voz

D     D  A/C#  Bm  A

D                      D/C#                D                             Bm
Eu mudei, nem sinto, nem vejo as coisas como via antes
                                                                        G
Meus amigos cresceram, mudaram, ficaram distantes
     D/F#                         Em7      G
Perdoe meu choro é sincero
         D                   D/C#              D                     Bm
Mais digo sim, que mesmo confuso, perdido esperas por mim
                                                                       G
Os meus olhos fechados te enxergam bem perto de mim
     D/F#                      Em7    A
Espero te ver nesse inverno.

Refrão

Solo: G  D/F#  Bm  A  G


     D
Eu quero ver o sol atrás do monte, eu quero ver o brilho que ele traz
                                                Bm
Eu quero ouvir de novo a sua voz!
     G                                                     D/F#
Eu quero ver o sol atrás do monte, eu quero ver o brilho que ele traz
      Em7                                        G     F#m    Bm
Eu quero ouvir de novo a sua voz
A              G
Sua voz, sua voz
Em7            D
    Sua voz

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D/C# = X 4 0 2 3 2
D/F# = 2 X 0 2 3 2
Em7 = 0 2 2 0 3 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
