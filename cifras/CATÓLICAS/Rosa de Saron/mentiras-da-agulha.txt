Rosa de Saron - Mentiras da Agulha

Intro:  (Em C D Em)

Em                 C
Sempre andando destruído
        D                       Em
Se arrastando por becos para dar um baque
Em                 C
Se já não tem mais amigos
        D                         Em
Está só abatido porque não tem Deus
Em      C                   D
Todos sabem que você está fraco
                Em
Deprimido, atormentado
Em        D              Em
Palavras gritam do teu braço

Em                C    D
Mas não confie na agulha
Em                      C   D
Ela mente e você não escapará

Em      C            D
Não confie nela, você não sairá
 Em                             C D Em
Quando ela lhe chamar pelo seu nome

(Em     C       D       Em)


Em                      C
Depois de tudo a volta é dura
            D           Em
Verdadeiro e fiel é seu ideal
Em                C                     D
Mas apesar de estar cansado, acuado, com medo
                 Em
Não pare de lutar
Em        C             D
Assombrado lhe vi gritar
                Em
Que ela nunca lhe deixará
Em        D                     Em
Ainda é tempo, Deus vai lhe ajudar

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
