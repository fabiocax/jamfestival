Rosa de Saron - Nada Entre o Valor e a Vergonha

Intro: G5 (4x)  G5  B5  Em  C5  G5  B5  Em  C5

G5       B5          Em C
Oh, oh, oh, oh, oh, oh,

    G       D/F#    Em7       C9
Oh, oh, oh, oh, oh, oh (Haverá, haverá)  (2x)


Em7       C9 G
Vejo almas

                  Em7
não só pedaços de carne

     C9             G
Nem coisas pra se usar

Em7          C9        G
Sem calças, nem causas


                       Em7
Nada Entre o Valor e a vergonha

       C9             G
Nenhum prazo pra recuar

    Em7          D/F#          C9
Acredite, há um prazo pra recuar


     G            D/F#      Em7
Não deixe que destruam sua vida

        C9                 G
Não permita que roubem sua Fé

    D/F#    Em7   C9
Oh, oh, oh, oh

G               D/F#
Que o riso seja sua fala

Em7           C9         G
Toda vez que sua voz acabar


    D/F#     Em7    C9
Oh, oh, oh, oh

G     D/F#    Em7       C9
  Oh, oh, oh, oh, (Haverá, haverá)  (2x)

Em7       C9     G
Torpezas, vilezas

                       Em7
Atitudes de quem não se acanha

         C9              G
Em se despir a troco de nada

Em7       C9     G
Mas sente repulsa quando sua máscara cai

Em7       C9                G
E quem está na sua frente é Deus

Em7     D/F#                 C9
Acredite, há um prazo pra recuar


     G            D/F#      Em7
Não deixe que destruam sua vida

        C9                 G
Não permita que roubem sua Fé

    D/F#    Em7   C9
Oh, oh, oh, oh

G               D/F#
Que o riso seja sua fala

Em7           C9         G
Toda vez que sua voz acabar


    D/F#     Em7    C9  C9  C9
Oh, oh, oh, oh


Intro: G  D/F# Em7  C9  C9  C9  G  D/F#  Em7


C9                           G
O mundo vai te comparar, o mundo vai  julgar

    D/F#     Em7      C9
Mas no final só quem foi fiel

                  G                D/F#
Vai poder rir de todos por serem iguais

              Em7      D/F#       C9
Os mesmos que hoje zombam do diferente

       C9          C9
E sim, haverá, haverá de ser


     G            D/F#      Em7
Não deixe que destruam sua vida

        C9                 G
Não permita que roubem sua Fé

    D/F#    Em7   C9
Oh, oh, oh, oh

G               D/F#
Que o riso seja sua fala

Em7           C9         G
Toda vez que sua voz acabar

    D/F#     Em7    C9
Oh, oh, oh, oh

Final: G  D/F#  Em7  C9
       G  D/F#  Em7  C9  C9

----------------- Acordes -----------------
B5 = X 2 4 4 X X
C = X 3 2 0 1 0
C5 = X 3 5 5 X X
C9 = X 3 5 5 3 3
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
G5 = 3 5 5 X X X
