Rosa de Saron - E Sempre

[Intro] Am  G/B  C

F       Em             Am                  F
 Queria eu ser seu plural, longe das rimas vis
               Em            Am
Onde o tempo é que nos vê passar, posto, diria-lhe
Em  Am    F             C   G/B
 Você nunca perde por amar
       Am                    F     Am
Perde por guardar o amor, o amor é muito sério
                          F
Para ser retido em vão então, ame

F       Em            Am                      F
 Ame primeiro diga depois e quando dizer "te amo"
         Em         Am
Ame como no princípio, agora e sempre, amém
Em  Am    F             C   G/B
 Você nunca perde por amar
       Am                    F     Am
Perde por guardar o amor, o amor é muito sério

                           F    Am  Em      F   Dm  C
 Para ser retido em vão então, ame,  então ame

C             Am   Em     F
 Você não sabe mas eu te cuido
            Am  Em        C
Onde a vida não cede seu lugar, você não sabe
         F             C   G/B
Você nunca perde por amar
       Am                    F     Am
Perde por guardar o amor, o amor é muito sério
                          F          C
Para ser retido em vão então irmão, ame
   G/B          Dm                         F
O amor é muito raro para ser sentido em vão

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G/B = X 2 0 0 3 3
