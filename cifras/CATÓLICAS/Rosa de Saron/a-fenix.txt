﻿Rosa de Saron - A Fênix

Capo Casa 1

[Intro] Em  C

[Primeira Parte]

Em
  Esgote o meu tempo
C             D
  E veja o relógio voltar
Em
  Me dê por vencido
C            D
  E eu voltarei a voar
Em             D           C
  Não temo a chama a me incendiar
Em            D           Am
  A morte é apenas um passar

[Refrão]

   C             D           Em
O fogo que me envolve me consome


E renova o meu ser
 C         D       Em
Sempre há vida em mim
    C              D            Em
Em meio às minhas cinzas eu desperto

E do chão do deserto
 C         D          Em
Nasce uma rosa outra vez

( C   Am  )

[Segunda Parte]

Em
  Provoque a mudança
C               D
  E eu continuarei a viver
Em
  Destrua a esperança
C           D
  E eu a farei renascer
Em           D             C
  No árido chão vou florescer

[Terceira Parte]

Em
  Apague o meu nome
C            D
  E eu voltarei a escrever
Em
  Esqueça que existo
C            D
  E eu te farei perceber
Em             D              C
  E quando disser que eu não posso ser
Em               D             C
  Olhe ao seu redor, e vai me ver

[Refrão]

   C             D           Em
O fogo que me envolve me consome

E renova o meu ser
 C         D       Em
Sempre há vida em mim
    C              D            Em
Em meio às minhas cinzas eu desperto

E do chão do deserto
 C         D          Em
Nasce uma rosa outra vez

[Ponte]

                      C
You're gonna see me fly
              D       Em
You're gonna see me fly again
                       C
You better watch the sky
              D       Em
You're gonna see me fly again

                      C
You're gonna see me fly
              D       Em
You're gonna see me fly again
                       C
You better watch the sky
              D       Em
You're gonna see me fly again

[Refrão]

   C             D           Em
O fogo que me envolve me consome

E renova o meu ser
 C         D       Em
Sempre há vida em mim
    C              D            Em
Em meio às minhas cinzas eu desperto

E do chão do deserto
 C         D          Em
Nasce uma rosa outra vez
       C   D      Em
Outra vez, outra vez
       C   D   Em
Outra vez

    C              Am           Em
Em meio às minhas cinzas eu desperto

E do chão do deserto
 C         Am         Em
Nasce uma rosa outra vez

----------------- Acordes -----------------
Capotraste na 1ª casa
Am*  = X 0 2 2 1 0 - (*A#m na forma de Am)
C*  = X 3 2 0 1 0 - (*C# na forma de C)
D*  = X X 0 2 3 2 - (*D# na forma de D)
Em*  = 0 2 2 0 0 0 - (*Fm na forma de Em)
