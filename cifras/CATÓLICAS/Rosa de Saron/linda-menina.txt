Rosa de Saron - Linda Menina

(afinação 1/2 tom abaixo)

(intro) Bm  A  G  Em

Bm            A
   Menina, quando a noite vier
           G                          Em
Perturbar sua vida, destruir seus sonhos
Bm                 A
    Menina, há quanto tempo está
       G                          Em
Perdida e sozinha marcando seu destino

( A  Bm  A  Bm )
( A  Bm  A  G  F#m )

Bm   A/C#  D  G        F#m    A
    Linda       menina
Bm               A/C#  D      A                 G
   Seus olhos podem   brilhar sem mentiras
                                         A
Sem ter que doar sua pureza

                              Bm   A   E   G   A
Seus sonhos de amor

Bm                              A                   G
    Há uma outra mulher que menina
                            Em
Soube um dia viver sua sina
             G                                  A        Bm   A
Criar o Filho de Deus para todos libertar
         Bm     A
Libertar
         G       A
Libertar
         Em     G      A
Libertar

Bm                  A                         G                     Em
    Não chore linda menina mas clame sem cessar               (4x)

A  Bm     A  Bm    A  Bm  A     G   F#m
A_ve,      A_ve,     A_ve    Ma__ri_ a
A  Bm     A  Bm    A  Bm  Bm   D   A/C#
A_ve,      A_ve,     A_ve    Ma__ri__ a
A  Bm     A  Bm    A  Bm  A     G   F#m
A_ve,      A_ve,     A_ve    Ma__ri_ a
A  Bm     A  Bm    A  Bm  Bm  D   A/C#
A_ve,      A_ve,     A_ve    Ma__ri_ a

Bm               A
Ave, Ave,     Ave Maria
G                  Em
Ave, Ave,     Ave Maria
Bm               A
Ave, Ave,     Ave Maria
G                  Em            Bm
Ave, Ave,     Ave Maria

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
