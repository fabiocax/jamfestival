Rosa de Saron - Parusia

(intro) ( E  F  A  F  A)

E                                F
O seu amor, o nosso amor me faz pensar
         A                             F               A
É tão fácil lembrar, com a memória é muito simples
                                E
Difícil é esquecer quando se tem amor
        A                  F          A
E você tornou-se impossível de se esquecer

E              A          C          F               E
Há tempo que ficou pra trás todo silêncio e solidão
              A             C           B
Se o tempo foi, você ficou junto de mim a me aquecer
                 A           C                 A          E
Há tempo que o frio passou, ficou o calor da tua luz
                   A           C                  B
Se o tempo foi, você marcou presença onde não há valor

E                               F
Tal qual estou a esperar a sua volta

          A                             F                A
São joelhos calejados que tocam o precioso chão
E
Fazer o quê? Eu não sei o que fazer
  F
Ainda assim só há alegria porque sei
       A
Sua volta é iminente
       F           A
Ansioso espero por você

E               A                  C           A
Há tempo que ficou pra trás todo silêncio, e solidão
                  F             C               B
Se o tempo foi você ficou junto de mim a me aquecer
                  A                  C               A
Há tempo que o frio passou, ficou o calor da sua luz
                   F              B
Se o tempo foi você marcou presença onde não há valor

        B
A esperança viva está
                                            A
Minha mente só me traz você

(solo 3x) ( E  B  A  )

        B
A esperança viva está
                                            A
Minha mente só me traz você

     E                           F
Eu sei porque não há solidão
     C                     D
Eu sei porque não há solidão
      A                         B
Eu sei porque não há solidão
      C                    A
Eu sei porque não há solidão
     E                           F
Eu sei porque não há solidão
     C                     D
Eu sei porque não há solidão
      A
Eu sei porque não há solidão
      C                   A
Eu sei porque não há solidão

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
