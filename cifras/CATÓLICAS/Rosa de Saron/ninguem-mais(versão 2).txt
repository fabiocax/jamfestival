Rosa de Saron - Ninguém Mais

Intro 3x: ( A#m F# C#9 )
               (  A#m F# )

C#          C#9       C#
Minha vida, meu amor
C#             C#9         C#
Meu chão, meu céu, minha luz
        F#
Minha razão de existir
C#        C#9    C#           C#9     C#
Eu hoje vim aqui só pra te ver passar
       F#
Precisaria nem olhar
          C#9
Para este pobre coração
A#m
Basta sua sombra, e poderei me abraçar
Fm                          F#
Já seria o suficiente para mim
Nada aqui merece atenção
A#m
  Mas se um dia você não regressar

Fm
Deixe umas pegadas
                 D#m7
E alguém irá correr... não há nada
C#9
Não há nada igual
G#9                   F#
Nada poderia me afastar de Ti
C#9
Não, não há alguém
G#9                        F#
Que me faça tão bem como você faz
C#9
E nunca haverá
G#9                           F#
Me transforme no melhor que posso ser
C#9
Não há fim, não há volta
       G#9                        F#           A#m
Porque só quem pode preencher o meu vazio é você
G#9  F#          A#m F# C#9    ( A#m F# C#9 )
 Você, ninguém mais
C#      C#9    C#          Fm
Exalam por aqui, aromas de jasmins
        F#
Em uma carta que escreveu
        C#9
E agora guardo bem aqui
A#m                                 Fm
Cada simples pensamento meu é uma medida
                          F#
Que há tempos decidiu te amar sem reservas
            A#m
Tudo o que tenho de valor, são as minhas memórias
Fm                                D#m7
Se elas partissem, eu partiria em dois... não há nada
C#9
Não há nada igual
G#9                   F#
Nada poderia me afastar de Ti
C#9
Não, não há alguém
G#9                        F#
Que me faça tão bem como você faz
C#9
E nunca haverá
G#9                           F#
Me transforme no melhor que posso ser
C#9
Não há fim, não há volta
       G#9                        F#           A#m
Porque só quem pode preencher o meu vazio é você
G#9  F#
 Você, ninguém mais
Fm     F#      C#9
 A saudade aqui, é um verso carregado de ventania
Fm     F#   G#9    A#m    F#
Que um dia resolveu partir
                  C#9
Nunca mais faltou ar
 A#m                               Fm
Cada simples pensamento meu é uma medida
                          F#
Que há tempos decidiu te amar sem reservas...
C#9
Não há nada igual
G#9                   F#
Nada poderia me afastar de Ti
C#9
Não, não há alguém
G#9                        F#
Que me faça tão bem como você faz
C#9
E nunca haverá
G#9                           F#
Me transforme no melhor que posso ser
C#9
Não há fim, não há volta
       G#9                        F#           A#m
Porque só quem pode preencher o meu vazio é você
G#9  F#        C#
 Você, ninguém mais
A#m      G#9 F#          C#9     ( A#m F# C#9 A#m F# C#9 )
Você... você, ninguém mais
Intro 3x: A#m F# C#9
          A#m F#
C#          C#9       C#
Minha vida, meu amor
C#             C#9         C#
Meu chão, meu céu, minha luz
        F#
Minha razão de existir
C#        C#9    C#           C#9     C#
Eu hoje vim aqui só pra te ver passar
       F#
Precisaria nem olhar
          C#9
Para este pobre coração
A#m
Basta sua sombra, e poderei me abraçar
Fm                          F#
Já seria o suficiente para mim
Nada aqui merece atenção
A#m
  Mas se um dia você não regressar
Fm
Deixe umas pegadas
                 D#m7
E alguém irá correr... não há nada
C#9
Não há nada igual
G#9                   F#
Nada poderia me afastar de Ti
C#9
Não, não há alguém
G#9                        F#
Que me faça tão bem como você faz
C#9
E nunca haverá
G#9                           F#
Me transforme no melhor que posso ser
C#9
Não há fim, não há volta
       G#9                        F#           A#m
Porque só quem pode preencher o meu vazio é você
G#9  F#          A#m F# C#9    ( A#m F# C#9 )
 Você, ninguém mais
C#      C#9    C#          Fm
Exalam por aqui, aromas de jasmins
        F#
Em uma carta que escreveu
        C#9
E agora guardo bem aqui
A#m                                 Fm
Cada simples pensamento meu é uma medida
                          F#
Que há tempos decidiu te amar sem reservas
            A#m
Tudo o que tenho de valor, são as minhas memórias
Fm                                D#m7
Se elas partissem, eu partiria em dois... não há nada
C#9
Não há nada igual
G#9                   F#
Nada poderia me afastar de Ti
C#9
Não, não há alguém
G#9                        F#
Que me faça tão bem como você faz
C#9
E nunca haverá
G#9                           F#
Me transforme no melhor que posso ser
C#9
Não há fim, não há volta
       G#9                        F#           A#m
Porque só quem pode preencher o meu vazio é você
G#9  F#
 Você, ninguém mais
Fm     F#      C#9
 A saudade aqui, é um verso carregado de ventania
Fm     F#   G#9    A#m    F#
Que um dia resolveu partir
                  C#9
Nunca mais faltou ar
 A#m                               Fm
Cada simples pensamento meu é uma medida
                          F#
Que há tempos decidiu te amar sem reservas...
C#9
Não há nada igual
G#9                   F#
Nada poderia me afastar de Ti
C#9
Não, não há alguém
G#9                        F#
Que me faça tão bem como você faz
C#9
E nunca haverá
G#9                           F#
Me transforme no melhor que posso ser
C#9
Não há fim, não há volta
       G#9                        F#           A#m
Porque só quem pode preencher o meu vazio é você
G#9  F#        C#
 Você, ninguém mais
A#m      G#9 F#          C#9     ( A#m F# C#9 A#m F# C#9 )
Você... você, ninguém mais

----------------- Acordes -----------------
A#m = X 1 3 3 2 1
C# = X 4 6 6 6 4
C#9 = X 4 6 6 4 4
D#m7 = X X 1 3 2 2
F# = 2 4 4 3 2 2
Fm = 1 3 3 1 1 1
G#9 = 4 6 8 5 4 4
