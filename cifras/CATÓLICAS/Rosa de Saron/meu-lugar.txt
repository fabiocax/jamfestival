Rosa de Saron - Meu Lugar

Intro:  G#m  F#  E  C#m

B9    F#/A#  G#m
Cai o dia, a tarde escura
E
Acho que vai chover
B9    F#/A#  G#m
Eu que sempre me escondi do mundo
E
Hoje talvez queira sair
B9 F#/A# G#m E
Descobrir outros ares
B9   F#/A#  G#m  F#      E B9 E
Acho que preciso me encontrar

Refrão:

E
Se é sonho ou real não importa
C#m7                         B9
Eu preciso sentir e assim talvez

                    G#m
Eu encontre o meu lugar, lugar
E
E mesmo que eu perca o horizonte
C#m7
Estarei na esperança de que
    B9                G#m
Um dia alguém, quem sabe alguém?
     E                 Em
Me ajude a ver onde errei

B9    F#/A#   G#m        E
Vejo cata-ventos, conto pedras no chão
B9       F#/A#          G#m       E
Risco a parede da minha sala Sujo a minha mão.
B9  F#/A# G#m   E
Eu mudei, eu senti
B9     F#/A#       G#m  F#     E         B   E
Já não quero mais, olhar o que ficou pra trás

Refrão:

E
Se é sonho ou real não importa
C#m7                         B9
Eu preciso sentir e assim talvez
                   G#m
Eu encontre o meu lugar, lugar
E
E mesmo que eu perca o horizonte
C#m7
Estarei na esperança de que
   B9                G#m
Um dia alguém, quem sabe alguém?
     E                 Em
Me ajude a ver onde errei
                  B9
E pegue a minha mão
       F#       G#m      E
Na chuva me aqueça, me olha
B     G#m     F#        E
E torna intensa minha vida

( B  G#m7(9)  F#  E ) (2x)

(Refrão)

----------------- Acordes -----------------
B = X 2 4 4 4 2
B9 = X 2 4 4 2 2
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
F#/A# = 6 X 4 6 7 X
G#m = 4 6 6 4 4 4
G#m7(9) = X X 6 4 7 6
