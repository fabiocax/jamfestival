Católicas - Criança Feliz

E            B7
Criança feliz
             E
feliz a cantar
               B7
Alegre a embalar
                 E
seu sonho infantil
       E7         A
Oh! meu bom Jesus,
  Am               E
que a todos conduz
                 B7
Olhai as crianças
               E
Do nosso Brasil
B7                 E
Crianças com alegria
B7                        E
Qual um bando de andorinhas
B7                   E
Viram Jesus que dizia

A                B        E
Vinde a mim as criancinhas
B7                 E
Hoje no céu um aceno
B7                 E
Os anjos dizem amém
B7                  E
Porque Jesus Nazareno
B7                   E
Foi criancinha também

E            B7
Criança feliz
             E
feliz a cantar
               B7
Alegre a embalar
                 E
seu sonho infantil
       E7         A
Oh! meu bom Jesus,
  Am               E
que a todos conduz
  C#m        F#m
Olhai as crianças
     B7         E
Do nosso Brasil

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
