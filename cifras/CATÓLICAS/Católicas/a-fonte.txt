Católicas - A Fonte

C                                    Em      F      Dm              G7
Dentro de mim há uma fonte, uma fonte de amor
C                                    Em       F              Dm         G
Dentro de mim há uma ponte, para o irmão, para o Senhor
F                  G                            F              G
Posso perdoar quem me feriu, até o levantar se ele caiu
                C          Em       Dm    G7          C             Em    Dm  G7
Eu tenho paz (eu tenho paz)... Pra perdoar (pra perdoar)
                   C         Em          Dm   G7           C          Em    Dm   G7
Eu tenho amor (eu tenho amor)... Para te dar (para te dar)
                    C               Em        Dm  G7                C              Em      Dm G7
Pois eu sou bom (pois eu sou bom)...Meu pai é bom (meu pai é bom)
                     C                     Em    Dm   G7                    C        Em  Dm
Meu irmão é bom (meu irmão é bom)... Minha mãe é boa...
C                           Em       F       Dm              G7
Para chegar nesta fonte, encontrei muitas pedras
  C                               Em         F          Dm               G7
Doeram também os espinhos, que pisei pelo caminho
F              G
Mas não vou desanimar
         F      G                                   C            Em     Dm
Com fé eu posso continuar a caminhar, ( a caminhar)

----------------- Acordes -----------------
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
