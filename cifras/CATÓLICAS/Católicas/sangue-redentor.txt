Católicas - Sangue Redentor

[Intro] G  D/F#  Em  Bm  D/C
        C  B7(11)  Am7  F#°  B7

[Primeira Parte]

 Em           D/F#        Em  D/F#
Ponho-me aos pés da tua cruz
   Em         D/F#         Em9
Desejo ser lavado por teu sangue
   G            D/F#
E ver sair das tuas chagas
Em              Bm
   Sangue redentor
   C             D         Em7  D/F#
Tocar as minhas dores e curar-me

 Em           D/F#        Em  D/F#
Ponho-me aos pés da tua cruz
   Em         D/F#         Em9
Desejo ser lavado por teu sangue
   G            D/F#
E ver sair das tuas chagas

Em              Bm
   Sangue redentor
   C             D         Em7  D/F#
Tocar as minhas dores e curar-me

[Refrão]

            G       D/F#
Teu sangue cura Senhor
            Em
Teu sangue cura com amor
 G/B        C         D      Em
Teu sangue tem poder para curar-me

            G       D/F#
Teu sangue cura Senhor
            Em
Teu sangue cura com amor
 G/B        C         D      Em
Teu sangue tem poder para curar-me


[Final] G  D/F#  Em  Bm  D/C  C
        B7(11)  Am7  F#°  B7  Em

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
B7(11) = X 2 4 2 5 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/C = X 3 X 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
Em9 = 0 2 4 0 0 0
F#° = 2 X 1 2 1 X
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
