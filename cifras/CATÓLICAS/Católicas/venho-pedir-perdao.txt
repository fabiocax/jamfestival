Católicas - Venho Pedir Perdão

C                      F            C
Senhor Bom Jesus, vim aqui pedir perdão
F       C      Dm           G
Tenha Piedade, tenha compaixão

C                       F          C
Senhor Bom Jesus, vim aqui pedir perdão
F         C     G          C
Tenha Piedade, tenha compaixão

       F                    G     Em                      Am
Pelas vezes que não fui cristão / Quando explorei o meu irmão
     Dm                   G           F       G             C
Por tudo o que fiz contra vós /   Senhor vim pedir o teu perdão

          F               G               Em            Am
Cristo piedade desta multidão/  Peço do fundo do meu coração
           Dm                 G    F        G              C
Os vossos filhos pedem teu perdão/ Cristo pedimos o teu perdão

           F               G             Em               Am
Senhor Jesus, pedimos a benção/ Seu povo humilde pede proteção

         Dm               G        F      G          C
Tirai o fardo do meu coração/ Senhor pedimos teu perdão

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
