Católicas - Glória, Glória, Glória a Deus Nos Céus

E            F#m    F#7                          B7
Glória, glória, glória a Deus nos céus!
E        F#m     E             B7         E
E na   terra   paz aos filhos teus!
E                                  G#7
1-Deus e pai nós vos
C#m                             G#m
louvamos, adoramos
A                            E
nós vos bendizemos por
F#7               B7
vosso amor
E                          G#7
Damos glória eterna ao
C#m                    G#m
vosso Santo nome
A
vossos dons vos
 E                  F#7                  B7
a...agradece..mos o Pai!
E                                  G#7
2-Senhor nosso Jesus

C#m                G#m
Cristo ,Salvador
A                       E
Filho Unigênito
F#7               B7
de Deus Pai
E                                  G#7
Vós de Deus cordeiro
C#m                   G#m
vós cordeiro Santo
A                              E
nossas muitas culpas
F#7               B7
Senhor... perdoai!
E            F#m    F#7                          B7
Glória, glória, glória a Deus nos céus!
E        F#m                    B7         E
E na   terra   paz aos filhos teus!

 E                                  G#7
3-Vós que estai sentado
C#m                       G#m
Junto de Deus   Pai
A                            E
Como nosso irmão
             F#7               B7
Nosso intercessor
E                         G#7
Acolhei, benigno
C#m                    G#m
Os nossos pedidos
A                     E
Atendei Senhor
                F#7            B7
Esse nosso Clamor
E                               G#7
4-Vós, Senhor Jesus
C#m                      G#m
Somente sois o Santo
A                         E
De Deus o altíssimo
F#7        B7
O Senhor
E                          G#7
Com o Santo amor
C#m             G#m
Espirito Divino
A                              E
De Deus Pai na glória e no
F#7               B7
Puro esplendor
Refrão

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#7 = 2 4 2 3 2 2
F#m = 2 4 4 2 2 2
G#7 = 4 6 4 5 4 4
G#m = 4 6 6 4 4 4
