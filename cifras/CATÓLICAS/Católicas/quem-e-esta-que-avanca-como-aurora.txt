Católicas - Quem É Esta Que Avança Como Aurora?

 C         G            Am  (G Am)
Quem é esta que avança como aurora,
  C               G                    Am   (G Am)
Temível como um exército em ordem de batalha,
     C            G            Am  (G Am)
Brilhante como o sol e como a lua,
    C         G                 Am  (G Am)
Mostrando o caminho aos filhos seus?

   G/B  C         G                     Am (G Am G/B C)
Ah, ah, ah, minha alma glorifica ao Senhor,
      G       Am   (G Am G/B C)    G              Am
meu espírito exulta             em Deus, meu Salvador.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
