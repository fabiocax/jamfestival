Católicas - Te Amarei

[Intro] E  A

[Primeira Parte]

      E               A              E      A
Me chamaste para caminhar na vida contigo
     E                 A                    D   B
Decidi para sempre seguir-te, não voltar atrás
      E                  A                   E      C#m
Me puseste uma brasa no peito, uma flecha na alma
     F#m            B                   E
É difícil agora viver sem lembrar-me de ti

[Refrão]

       B                E     C#m           F#m
Te amarei Senhor, Te amarei Senhor, eu só encontro a paz e a
     B                E  A
Alegria bem perto de ti

[Segunda Parte]


       E                  A                   E      A  B
Eu pensei muitas vezes calar e não dar nem resposta
       E                A                   D  B
Eu pensei na fuga esconder-me, ir longe de ti
         E                   A                  E  C#m
Mas tua força venceu e ao final eu fiquei seduzido
       F#m            B                    E
E é difícil agora viver sem lembrar-me de ti

[Refrão]

      B                 E     C#m           F#m
Te amarei Senhor, Te amarei Senhor, eu só encontro a paz e a
     B                E
Alegria bem perto de ti

[Terceira Parte]

     E                  A                 E     A  B
Ó Jesus não me deixe jamais caminhar solitário
         E                 A                D  B
Pois conheces a minha fraqueza e o meu coração
       E                 A             E     C#m
Vem ensina-me a viver a vida na tua presença
    F#m                    B                  E
No amor dos irmãos, na alegria, na paz, na união

[Refrão]

      B                 E     C#m           F#m
Te amarei Senhor, Te amarei Senhor, eu só encontro a paz e a
     B                E
Alegria bem perto de ti

[Quarta Parte]

      E                  A                     E      A  B
Eu pensei muitas vezes calar e não dar nem resposta
       E                A                   D  B
Eu pensei na fuga esconder-me, ir longe de ti
         E                   A                  E  C#m
Mas tua força venceu e ao final eu fiquei seduzido
       F#m          B                   E
E é difícil agora viver sem saudades de ti

[Refrão]

       B                E     C#m           F#m
Te amarei Senhor, Te amarei Senhor, eu só encontro a paz e a
     B                E
Alegria bem perto de ti

( A  B  E )

----------------- Acordes -----------------
A = X 0 2 2 2 0
B9 = X 2 4 4 2 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
