Católicas - Make Tue Papa

Intro: C

   Am           F
 Make  tue, tue papa
   G          C
 Make tue tue pá (bis)
     Am         F
 Tu tue, tu tue papa
     G           C
 Tu tue, tu tue pá (bis)
    Am      F
 Moringo, moringo
    Am       C
 Moringo, morá (bis)
   Am      F
 Adjene, adjene
   G       C
 Adjene, adjá (bis)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
