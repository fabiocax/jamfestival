Católicas - Santo - Hosana Cantemos, Hosana Lovemos

G                      Am             D    C         D       G  D
San - to,santo é o Senhor, Terra e céu, cantam em seu louvor
G         G7             C          Cm      G       D          G  G7
Santo proclamam suas criaturas Hosana, Hosana, Hosana nas alturas
  C               G                   D7       G
Hosana cantemos Hosana louvemos  com filial ternura

   C                  D/C         Bm
Bendito é o que vem em nome do criador
   Am           D             D7             G
Bendito é o que traz a paz em plenitude do amor

  C    G
Hosa - na

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D/C = X 3 X 2 3 2
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
