Católicas - Sou a Verdade e a Vida

Dm   Gm     C7      F           Bb                 Dm        G#º     A
 “Sou a vida e a verdade! Quem crê em mim, ressuscitará...
Dm  Gm        C7         F   Bb    Dm       A7  Dm     D7
E feliz, na eternidade, para sempre viverá".
        Gm         C7       Fmaj7      Bb               Gm         A7    Dm    D7
Aleluia, (aleluia) Aleluia, (aleluia) Louvor e glória a Ti Senhor!
        Gm         C7       Fmaj7      Bb               Gm         A7    Dm
Aleluia, (aleluia) Aleluia, (aleluia) Louvor e glória a Ti Senhor!


----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bb = X 1 3 3 3 1
C7 = X 3 2 3 1 X
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Fmaj7 = 1 X 2 2 1 X
G#º = 4 X 3 4 3 X
Gm = 3 5 5 3 3 3
