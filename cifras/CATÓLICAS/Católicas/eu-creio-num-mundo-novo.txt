Católicas - Eu Creio Num Mundo Novo

E                                 B          E
Eu creio num mundo novo / pois Cristo ressuscitou!
E                                      B              E
Eu vejo  sua  luz  no  povo  /  por  isso  alegre  estou!

    B7             E           B7       E            A                E
Em toda pequena oferta, / na força da união, / no  pobre  que  se  liberta
      F#           B7
eu  vejo ressurreição!

     B7              E         B7            E       A               E         F#         B7
Na mão que foi estendida,/ no dom da libertação,/ nascendo uma nova vida,/ eu vejo ressurreição!
      B7         E         B7            E           A           E           F#    B7
Nas flores oferecidas/ e quando se dá perdão,/ nas dores compadecidas,/ eu vejo ressurreição!
      B7               E          B7                 E          A         E
Nos homens que estão unidos/ com outros, partindo o pão,/ nos fracos fortalecidos,
     F#         B7
eu vejo ressurreição!

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
