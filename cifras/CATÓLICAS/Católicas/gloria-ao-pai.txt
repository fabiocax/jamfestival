Católicas - Gloria Ao Pai

C        E-    A-     E-      F         C       D7          G
Glória a Deus nas alturas grande é teu poder imenso é seu amor.
   A-     E-   F     C      F           C     G           C
Glória a Deus nas alturas grande é teu poder imenso é seu amor.
 C        E-      A-     E-       F                C       D7    G
Glória ao pai, Glória ao filho, Glória ao santo Espírito consolador.
   A-      E-     F        C       F                C    G     C
Glória ao pai, Glória ao filho, Glória ao santo Espírito consolador.


----------------- Acordes -----------------
A- = X 0 2 2 1 0
C = X 3 2 0 1 0
D7 = X X 0 2 1 2
E- = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
