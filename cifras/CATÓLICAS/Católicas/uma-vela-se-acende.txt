Católicas - Uma Vela Se Acende

A
Uma vela se acende
                  Bm
no caminho a iluminar.
                  E
Preparemos nossa casa:
                    A
é Jesus quem vai chegar.

       D                                  A
No Advento a tua vinda nós queremos preparar.
                          E                        A
Vem, Senhor, que é teu Natal, vem nascer em nosso lar.

A
A segunda vela acesa
              Bm
vem a vida clarear.
                      E
Rejeitemos, pois, as trevas:
                  A
é Jesus quem vai chegar.



     D                                   A
No Advento a tua vinda nós queremos preparar.
                         E                         A
Vem, Senhor, que é teu Natal, vem nascer em nosso lar.

A
Na terceira vela temos
                   Bm
a esperança a crepitar.
                E
Nossa fé se reanima:
                    A
é Jesus quem vai chegar.

     D                                   A
No Advento a tua vinda nós queremos preparar.
                       E                          A
Vem, Senhor, que é teu Natal, vem nascer em nosso lar.

A
Eis a luz da quarta vela:
                    Bm
um clarão se faz brilhar.
                 E
Bate forte o coração:
                    A
é Jesus quem vai chegar.

     D                                   A
No Advento a tua vinda nós queremos preparar.
                       E                          A
Vem, Senhor, que é teu Natal, vem nascer em nosso lar.

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
