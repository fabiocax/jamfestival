Católicas - Salmo 33/34 - O Pobre Clama a Deus e Ele Escuta

   C                    G/B              F/C  C
O pobre clama a Deus e ele escuta:
         Dm7     C/E  F                    F/G
o Senhor liberta a vida dos seus servos.
    C                    G/B              F/C  C
O pobre clama a Deus e ele escuta:
       Dm7                F/G               C
o Senhor liberta a vida dos seus servos.

       Am7               G                         F
Bendirei o Senhor Deus em todo o tempo,
     Dm7                    F                   F/G
seu louvor estará sempre em minha boca.
          C             G/B           Am7   Am/G
Minha alma se gloria no Senhor;
      Dm7               F                F/G
que ouçam os humildes e se alegrem!

             Am7               G                         F
Mas ele volta a sua face contra os maus,
          Dm7        F                   F/G
para da terra apagar sua lembrança.

                       C             G/B                      Am7   Am/G
Clamam os justos, e o Senhor bondoso escuta
       Dm7               F            F/G
e de todas as angústias os liberta.

              Am7       G                     F     C
Do coração atribulado ele está perto
         Dm7               F                 F/G
e conforta os de espírito abatido.
                 C                 G/B                 Am7   Am/G
Mas o Senhor liberta a vida dos seus servos,
           Dm7           F                     F/G
e castigado não será quem nele espera.

----------------- Acordes -----------------
Am/G = 3 X 2 2 1 X
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
Dm7 = X 5 7 5 6 5
F = 1 3 3 2 1 1
F/C = X 3 3 2 1 1
F/G = 3 X 3 2 1 X
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
