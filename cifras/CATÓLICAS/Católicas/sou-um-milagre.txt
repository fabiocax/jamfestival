Católicas - Sou Um Milagre

      B9                                      F#
Nunca houve noite que pudesse impedir
           E/G#                           B9
o nascer do sol e a esperança
                                                      F#
E não há problema que possa impedir
                  E/G#                    B9
as mãos de Jesus pra me ajudar (2x)

      G#m7                         F/A
Haverá um milagre dentro de mim
                    D#7                         G#m7
Vem descendo o rio pra me dar a vida
            E                      B9                               F#
Este rio que emana lá da cruz, do lado de Jesus.

( E/G#   F#/Bb ) (2x)

    B9                    F#              G#m7  / F#
Aquilo que parecia impossível
       E                    F#                 B9
Aquilo que parecia não ter saída

   D#7                                              G#m7
Aquilo que parecia ser minha morte
               E                                 B9
Mas Jesus mudou minha sorte
                   F#                 B9
Sou um milagre estou aqui.  (2x)

C#m     E                     B9
Usa-me sou o teu milagre
C#m     E                    B9
Usa-me eu quero te servir
C#m     E                    B9
Usa-me sou a tua imagem
C#m     E                    B9
Usa-me oh filho de Davi

----------------- Acordes -----------------
B9 = X 2 4 4 2 2
C#m = X 4 6 6 5 4
D#7 = X 6 5 6 4 X
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
F# = 2 4 4 3 2 2
F#/Bb = 6 X 4 6 7 X
F/A = 5 X 3 5 6 X
G#m7 = 4 X 4 4 4 X
