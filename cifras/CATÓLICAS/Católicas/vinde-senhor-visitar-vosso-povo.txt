Católicas - Vinde, Senhor, Visitar Vosso Povo

  F          C7        F
Senhor,  atendei  os  pedidos;
  Bb         F           C7
Senhor  escutai  nossa  voz,
     F        C7      F
pois sempre serão atendidos
 Bb            C7       F
aqueles que esperam em Vós.

F7        Bb  C7
Vinde, Senhor,
              F
visitar vosso povo;
Dm       Bb      C7
Em Jesus Cristo,
             F
Recriá-lo de novo!
F7        Bb  C7
Vinde, Senhor,
              F
Visitar vosso povo;

Dm       Bb    C7
Em Jesus Cristo,
            F
Recriá-lo de novo!

    F          C7         F
Vós sois nossa grande esperança,
   Bb       F      C7
certeza de libertação.
   F         C7       F
Mandai vosso filho criança
   Bb        C7       F
ao mundo trazer salvação.

F7        Bb  C7
Vinde, Senhor,
              F
Visitar vosso povo;
Dm       Bb     C7
Em Jesus Cristo,
             F
Recriá-lo de novo!
F7        Bb  C7
Vinde, Senhor,
             F
Visitar vosso povo;
Dm       Bb     C7
Em Jesus Cristo,
            F
Recriá-lo de novo!

   F         C7        F
Do céu vai jorrar água viva
    Bb       F        C7
que faz o deserto florir.
  F         C7       F
E quem esta graça cultiva,
  Bb       C7      F
feliz haverá de sorrir.

F7        Bb  C7
Vinde, Senhor,
              F
Visitar vosso povo;
Dm       Bb    C7
Em Jesus Cristo,
             F
Recriá-lo de novo!
F7        Bb  C7
Vinde, Senhor,
              F
Visitar vosso povo;
Dm       Bb     C7
Em Jesus Cristo,
            F
Recriá-lo de novo!

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
