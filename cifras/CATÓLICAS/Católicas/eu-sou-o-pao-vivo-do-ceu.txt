Católicas - Eu Sou o Pao Vivo do Céu

[Intro] G  F  Em  Dm
        F  Em  Dm  C
        Em  Dm  C  Bº
        Am  G4  G

         C     C#°   Dm
Já esta tudo arrumadinho
   F/A      G/B        C   Em/B
A festa tem luz tem carinho
      Am       Am7/G    D/F#  D
Nossa mesa tem farta comida
       G
Para todos
    G  F/A  G/B   C
Também tem  be---bida

   Am        B7
Eu sou o pão vivo
    Em   A G/B A/C#
Do céu
  Dm           G7
Falou jesus cristo

    C   C/Bb
Na cei---a
   F/A  Fm/Ab
Por ele nossa
     C/G  Am7
Refeição
      Dm7
Faz a vida
      G7       C
Ser transformação

   Am/F#     B7/D#
Eu sou o pão vivo do
 Em    Em/C#
Céu
  Dm7        G/B
Falou jesus cristo
   Gm/Bb  C   Bb/D  C/E
Na cei____a
     F    G/F
Por ele nossa
     Em7   Am7
Refeição
      D7
Faz a vida ser
  G7       C
Transformação

( G  F  Em  Dm )
( C  Bº  Am  G )

   C         C#°   Dm
A festa da eucaristia
     F/A     G/B        C  Em/B
Tem jesus também tem maria
       Am     Am7/G    D/F#   D
Faz a gente cantar o louvor
      G
Tem amigos
   G   F/A    G/B    C
E tem  mui---to a---mor

   Am        B7
Eu sou o pão vivo
    Em   A G/B A/C#
Do céu
  Dm           G7
Falou jesus cristo
    C   C/Bb
Na cei---a
   F/A  Fm/Ab
Por ele nossa
     C/G  Am7
Refeição
      Dm7
Faz a vida
      G7       C
Ser transformação

   Am/F#     B7/D#
Eu sou o pão vivo do
 Em    Em/C#
Céu
  Dm7        G/B
Falou jesus cristo
   Gm/Bb  C   Bb/D  C/E
Na Cei----a
     F    G/F
Por ele nossa
     Em7   Am7
Refeição
      D7
Faz a vida ser
  G7       C
Transformação

( G  F  Em  Dm )
( C  Bº  Am  G )

    C      C#°    Dm
Sagrada é toda partilha
     F/A
Santa é
G/B       C  Em/B
Toda família
        Am
Que faz da
Am/G    D/F#   D
Mesa do pão
     G
O espaço
G  F/A   G/B    C
Da  co---mun---hão

   Am        B7
Eu sou o pão vivo
    Em   A G/B A/C#
Do céu
  Dm           G7
Falou jesus cristo
    C   C/Bb
Na cei---a
   F/A  Fm/Ab
Por ele nossa
     C/G  Am7
Refeição
      Dm7
Faz a vida
      G7       C
Ser transformação

   Am/F#     B7/D#
Eu sou o pão vivo do
 Em    Em/C#
Céu
  Dm7        G/B
Falou Jesus Cristo
   Gm/Bb  C   Bb/D  C/E
Na cei----a
     F    G/F
Por ele nossa
     Em7   Am7
Refeição
      D7
Faz a vida ser
  G7       C
Transformação

( G  F  Em  Dm )
( C  Bº  Am  G )

      C     C#°      Dm
Só comunga quem tem amor
          F/A
Estende a mão
G/B      C  Em/B
Ao sofredor
      Am    Am/G     D/F#   D
Olha sempre ao seu redor
      G
Cuida de
  G  F/A  G/B   C
Quem  é   me---nor

   Am        B7
Eu sou o pão vivo
    Em   A G/B A/C#
Do céu
  Dm           G7
Falou jesus cristo
    C   C/Bb
Na cei---a
   F/A  Fm/Ab
Por ele nossa
     C/G  Am7
Refeição
      Dm7
Faz a vida
      G7       C
Ser transformação

   Am/F#     B7/D#
Eu sou o pão vivo do
 Em    Em/C#
Céu
  Dm7        G/B
Falou Jesus Cristo
   Gm/Bb  C   Bb/D  C/E
Na cei----a
     F    G/F
Por ele nossa
     Em7   Am7
Refeição
      D7
Faz a vida ser
  G7       C
Transformação

[Final] G  F  Em  Dm
        C  G  C

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
Am = X 0 2 2 1 0
Am/F# = 2 X 2 2 1 0
Am/G = 3 X 2 2 1 X
Am7 = X 0 2 0 1 0
Am7/G = 3 0 2 0 1 0
B7 = X 2 1 2 0 2
B7/D# = X X 1 2 0 2
Bb/D = X 5 X 3 6 6
Bº = X 2 3 1 3 1
C = X 3 2 0 1 0
C#° = X 4 5 3 5 3
C/Bb = X 1 2 0 1 X
C/E = 0 3 2 0 1 0
C/G = 3 3 2 X 1 X
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Em = 0 2 2 0 0 0
Em/B = X 2 2 0 0 0
Em/C# = X 4 2 0 0 X
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
Fm/Ab = 4 3 3 1 X X
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G/F = 1 X X 0 0 3
G4 = 3 5 5 5 3 3
G7 = 3 5 3 4 3 3
Gm/Bb = 6 5 5 3 X X
