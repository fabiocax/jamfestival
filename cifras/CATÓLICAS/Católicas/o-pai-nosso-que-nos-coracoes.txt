Católicas - Ó Pai Nosso Que Nos Corações

E                                    B
Óh Pai Nosso que nos corações
C#m                            G#m
Dos que tem amor fraterno está
A                   E
A quem destes a certeza que um dia
      F#m               B
Terão a eterna vida.
E                   B
Nós queremos todo dia ter
C#m                    G#m
Seu amor em nossos corações
A                  E
Para unir todos os povos da Terra
    F#m     B     E
Vivendo em Tua lei

         G#m C#m
Óh   Senhor,
C#m                              G#m
Vem mostrar Tua bondade (Óh Senhor)

A                  E                  G#m  C#m
Vem ouvir esta oração (Óh Senhor)
C#m                           G#m
Que nos ensinou Jesus o Teu filho
A           B      E            G#m   C#m
Nosso Salvador (Óh   Senhor)
C#m                G#m
Que nos ensinou Jesus o Teu filho
A           B    E
Nosso Salvador

Lalaialaialaialaia: E B C#m G#m A E F#m B E    E B C#m G#m A E F#m B E

         G#m C#m
Óh   Senhor,
C#m                              G#m
Vem mostrar Tua bondade (Óh Senhor)
A                  E                  G#m  C#m
Vem ouvir esta oração (Óh Senhor)
C#m                           G#m
Que nos ensinou Jesus o Teu filho
A           B      E            G#m   C#m
Nosso Salvador (Óh   Senhor)
C#m                G#m
Que nos ensinou Jesus o Teu filho
A           B    E
Nosso Salvador

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
