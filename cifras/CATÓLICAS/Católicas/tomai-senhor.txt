Católicas - Tomai Senhor

 E      A       E        C#m            F#                        B
Tomai Senhor e recebei , toda minha liberdade , a minha memória também
A         B         G#m               C#m
O meu entendimento e toda a minha vontade .
F#m               B7                      E
Tudo o que tenho e possuo , Vós me deste com amor .
E7                     A                        G#m
Todos os dons que me destes , com gratidão vos devolvo.
    C#m          F#m      B7                 E
Disponde deles Senhor , segundo a vossa vontade .
  A     E      B         E          A         E       B                      E
Dai-me somente o vosso amor , vossa graça . Isso me basta , nada mais quero pedir.
E7                       A                       G#m
Todos os dons que me destes , com gratidão vos devolvo.
    C#m         F#m      B7                 E
Disponde deles Senhor , segundo a vossa vontade .
  A   E       B    E     A           E            B                   E
Dai-me somente o vosso amor , vossa graça . Isso me basta , nada mais quero pedir.

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
