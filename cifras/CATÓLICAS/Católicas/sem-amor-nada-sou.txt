Católicas - Sem Amor, Nada Sou

E       B7        C#m                  G#m
Ainda que eu fale todas as línguas que existem.
A         E          F#m         B7
Se eu nao tiver amor, nada sou, nada sou.
  E       B7        C#m                  G#m
Mesmo que eu seja profeta e fale ás multidões.
A         E          F#m         B7
Se eu nao tiver amor, nada sou, nada sou...

          E          B7
O amor é maior que a fé.
           C#m        G#m
O amor é maior que a esperança.
           A       E           B7
O amor é eterno, porque Deus é amor!

          E          B7
O amor é maior que a fé.
           C#m        G#m
O amor é maior que a esperança.
           A             B7    E
O amor é eterno, porque Deus é amor!


E       B7        C#m                  G#m
Mesmo que toda ciência seja por mim conheçida
A         E          F#m         B7
Se eu nao tiver amor, nada sou, nada sou.
E       B7        C#m                  G#m
Mesmo que minha fé transporte as montanhas
A         E          F#m         B7
Se eu nao tiver amor, nada sou, nada sou...

E       B7        C#m                  G#m
Mesmo que eu doe meus bens para os pobres
A         E          F#m         B7
Se eu nao tiver amor, nada sou, nada sou.
E       B7        C#m                  G#m
Mesmo que eu entregue o meu corpo às chamas
A         E          F#m         B7
Se eu nao tiver amor, nada sou, nada sou...

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
