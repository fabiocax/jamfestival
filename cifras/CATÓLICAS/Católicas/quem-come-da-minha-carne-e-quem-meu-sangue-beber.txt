Católicas - Quem Come da Minha Carne e Quem Meu Sangue Beber

C7           Gm             Dm
   Quem come da minha carne
            C
E quem meu sangue beber
        Gm
Eterna vida terá
Dm         C           C7
E eu o ressuscitarei
            F
Quem nos garante, é Jesus
Bb      C            C7
Eterna vida a de ter
            F
Quem nos garante, é Jesus
Bb      C
Eterna vida a de ter

        C        Am             Em         C          Am               Em
1- Escuta, ó meu povo, a minha lei/ Ouve atento as palavras que eu te digo/
     C7                    Gm                          Bb          C
Abrirei a minha boca em parábolas/ Os mistérios do passado lembrarei


          C            Am             Em            C      Am             Em
2- Não havemos de ocultar aos nossos filhos/ Mas à nova geração nós contaremos/
        C7                      Gm                           Bb        C
As grandezas do Senhor e seu poder/ Os seus feitos, que por nós realizou

       C         Am           Em         C          Am          Em
3- Rochedo no deserto ele partiu/ e lhe deu para beber água corrente/
       C7                      Gm                    Bb             C
Mas pecaram contra ele sempre mais/ Provocaram no deserto o Deus Altíssimo

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
