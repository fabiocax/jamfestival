Católicas - Quem É Esta Sombra Tão Bela

[Intro]  A  F#m

         Bm   E                   A   F#m
Quando o amor    quis na terra reinar
         Bm   E                     A    F#m
A sua palavra    quis ao mundo anunciar
  C#m                      D         E        A   A7
A sua celeste harmonia ansiava entre nós ressoar

     D                         C#m
Quem é esta sombra tão bela morrendo no sol, Resplandece mais
  F#m                         C#m
E este silêncio altíssimo de amor
  E       A    F#m
Maria, és tu!

         Bm  E                A     F#m
Pra realizar   este plano O Senhor
          Bm   E                  A    F#m
Quis encontrar    um silêncio de amor
  C#m                                D        E         A   A7
A luz nesta sombra brilhou, e a harmonia no silêncio ecoou


     D                         C#m
Quem é esta sombra tão bela morrendo no sol, Resplandece mais
  F#m                         C#m
E este silêncio altíssimo de amor
  E       A    F#m
Maria, és tu!

         Bm    E               A     F#m
De ti queremos    um eterno cantar
       Bm  E                A    F#m
Imenso céu    que contém o amor
   C#m                          D       E         A     A7
Tu és a mãe e por ti veio entre nós O Senhor, O Senhor!

     D                         C#m
Quem é esta sombra tão bela morrendo no sol, Resplandece mais
  F#m                         C#m
E este silêncio altíssimo de amor
  E       A    F#m
Maria, és tu!

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
