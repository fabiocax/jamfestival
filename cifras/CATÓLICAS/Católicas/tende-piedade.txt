Católicas - Tende Piedade

Bb  F             D#     Bb
Senhor, tende piedade de nós
D#     Bb       F
Somos todos pecadores.

Gm  Dm            D#     Bb
Senhor, tende piedade de nós
D#     Bb       F
Somos todos pecadores.

Gm  Dm            D#     Bb
Senhor, tende piedade de nós
F      D#         Bb   G
Somos todos pecadores.

C   G           F         C
Jesus, tende piedade de nós
 F      C       G
Somos todos pecadores.

Am Em    F     C
Jesus, tende piedade de nós

 F      C       G
Somos todos pecadores.

Am Em    F     C
Jesus, tende piedade de nós
 G      F       C        A
Somos todos pecadores.

 D  A           G         D
Senhor, tende piedade de nós
  G      D        A
Somos todos pecadores.

Bm  F#m        G         D
Senhor, tende piedade de nós
  G         D          A
Somos todos pecadores.

Bm  F#m         G        D
Senhor, tende piedade de nós
  A     G          D
Somos todos pecadores.

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D# = X 6 5 3 4 3
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
