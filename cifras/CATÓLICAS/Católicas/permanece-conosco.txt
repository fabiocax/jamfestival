Católicas - Permanece Conosco

G                                                     C                                G                                       Am
As sombras vão se abrindo / Quando a noite cai, e vão fugindo / Tantas luzes
                                C                                                     D
de um dia que jamais há de se acabar / de um dia que há de começar sempre /
G                                C                                                 G                          Am                 D  D7
Porque sabemos, que uma nova vida, aqui nascida, ninguém mais cancelará.

G                         Bm         C                  G      Am                          C                       D
SE TU VAlS AGORA, ANOITECERÁ / SE TU VAIS EMBORA, SENHOR O QUE
      G                                  Bm         C                   G      Am                             C
SERÁ / SE TU VAIS AGORA, ANOITECERÁ / MAS  SE  PERMANECES A
  D                        G
NOITE  NÃO VIRÁ /

  G                                          C                       G                                 Am
Como o mar se espalha, infinitamente, o vento sopra e abrirá os caminhos
           C                                                       D                                            G
escondidos tantos corações / Hão de ver, uma nova luz clara. / Como uma chama
          C                                                G                   Am                D    D7
que onde passa queima / O teu amor, todo mundo invadirá...



A humanidade, luta, sofre, espera, / é terra seca, e no céu não há nuvens mas a

vida não lhe faltará / e a esperança, brilhará para sempre. Contigo unidos, oh

fonte de água viva,  / tua presença, o deserto acabará. /

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
