Católicas - Livro da Vida

Intro: A7+  GSUS4  G7/9-  A7+  C#º  D/F#

A7+                     D7+
Um dia eu vou para um lugar
 A7+               D7+ C/G D6
Onde não mais haverá
A7+             D7+
Morte choro e dor
A7+                 D7+ C/G D6
Onde o amor vai reinar
A7+                 D7+
Face a face vou estar
A7+           D7+ C/G D6
Com o meu Criador
A7+            D7+     A7+                  D7+ D/F#
Na eternidade proclamar que Jesus Cristo é o Senhor

Bm7                     A9/C#
O teu reino de paz  de paz
D9                      E4 E
O teu reino de luz

Bm7                     A9/C#
Hoje eu quero buscar
D9                      E4 E
Em primeiro lugar

( A7+  Gsus4  G7(9-)  A7+  C#º  D/F# )

A7+                   D7+
Um anjo mostrou-me um rio
A7+             D7+ C/G D6
De agua viva
A7+                 D7+
Brilhava como um cristal
A7+             D7+ C/G D6
E o rio brotava
A7+         D7+     A7+          D7+ D/F#
Do trono de deus e do cordeiro
Bm7                     A9/C#
O teu reino de paz  (de paz )
D9             E4 E
O teu reino de luz
Bm7            A9/C#
Hoje eu quero buscar
D9            E4 E
Em primeiro lugar

----------------- Acordes -----------------
A7+ = X 0 2 1 2 0
A9/C# = X 4 2 2 0 X
Bm7 = X 2 4 2 3 2
C#º = X 4 5 3 5 3
C/G = 3 3 2 X 1 X
D/F# = 2 X 0 2 3 2
D6 = X 5 4 2 0 X
D7+ = X X 0 2 2 2
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
E4 = 0 2 2 2 0 0
G7(9-) = 3 X 3 1 0 X
G7/9- = 3 X 3 1 0 X
Gsus4 = 3 5 5 5 3 3
