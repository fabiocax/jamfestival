Católicas - Estou Aqui Senhor

Am      Dm       E7                    Am
Estou aqui Senhor, vim buscar o teu perdão
                 Dm                 E7               Am
Eu te dou livre acesso, eu abro as portas do meu coração
Am      Dm       E7                      Am
Estou aqui Senhor, eu me entrego em tuas mãos
                 Dm                 E7               Am    A7
Eu te dou livre acesso, eu abro as portas do meu coração

             Dm  G                   C   G  Am
Renova-me Senhor,  Refaz todo o meu ser,
              Dm              E7                 Am   A7
levanta-me Senhor Jesus e das cinzas me faz renascer
             Dm  G                   C   G  Am
Renova-me Senhor,  Refaz todo o meu ser,
              Dm              E7                 Am
levanta-me Senhor Jesus e das cinzas me faz renascer

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
G = 3 2 0 0 0 3
