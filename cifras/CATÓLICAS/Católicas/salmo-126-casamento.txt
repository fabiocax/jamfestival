Católicas - Salmo 126 - Casamento

Intro:(G  C  Am  D  G  C  Am  D)

G    D/F#            Em    Bm            C          Am           D
Se o senhor não edificar a casa   em vão trabalham os que a constroem;
G    D/F#           Em       Bm       C     Am     D
Se o senhor não guardar a cidade   em vão vigia a sentinela.

C            D                G       D/F#  Em
Inútil levantar-vos antes da auro -- o-- ra
C                     D            G      D/F#   Em
E a atrasar até alta noite vosso descan ---an-- -so
          C             D        G   D/F#   Em
Para comer o pão de um duro traba --- a --- lho
             C            D           C         G
Pois deus o dá aos seus amados  até durante o sono.

G         D/F#         Em      Bm  C          Am            D
Vede os filhos são um dom de deus  é uma recompensa o fruto das entranhas.
G            D/F#        Em                Bm        C                  Am    D
Dais como as flechas nas mãos    do   guerreiro   assim são os filhos gerados na
juventude.


refrão
Solo final:
G          D/F#            Em         Bm      C              Am             D
Feliz o homem que assim encheu sua aljava;   não será confundido  quando defender a sua causa;
G   D/F#      Em    Bm     C   Am            D
 contra seus  inimigos    à    porta da cidade.

refrão
Solo final: (Am  B7  Em  C  Am  D  G7+)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G7+ = 3 X 4 4 3 X
