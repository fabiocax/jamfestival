Católicas - É Como a Chuva Que Lava

C                  G7    Dm       G7          C
É como a chuva que lava, é como o fogo que arrasa
                 G7
Tua palavra é assim
     F         Em        Dm        C
Não passa por mim sem deixar um sinal

       C
Tenho medo de não responder
                       G7
De fingir que não escutei
       F                   Em
Tenho medo de ouvir Teu chamado
  Am           Dm         G7          C    A7
Virar do outro lado e fingir que não sei

D                   A7   Em       A7          D
É como a chuva que lava, é como o fogo que arrasa
                 A7
Tua palavra é assim
     G        F#m         Em       D
Não passa por mim sem deixar um sinal


       D
Tenho medo de não perceber
                          A7
De não ver o teu amor passar
       G                  F#m      Bm      Em
Tenho medo de estar distraído, magoado, ferido
     A7        D     B7
E então me fechar.

E                  B7   F#m     B7          E
É como a chuva que lava, é como fogo que arrasa
                B7
Tua palavra é assim
     A        G#m        F#m       E
Não passa por mim sem deixar um sinal

       E
Tenho medo de estar a gritar
                   B7
E negar o meu coração
       A                 G#m       C#m      F#m
Tenho medo de Cristo que passa, oferece uma graça
      B7       E  C7
E eu digo que não

F                  C7   Gm      C7          F
É Como a chuva que lava, é como fogo que arrasa
                 C7    Bb F C7 F Bb F
Tua palavra é assim...

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
Bb = X 1 3 3 3 1
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
C7 = X 3 2 3 1 X
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G#m = 4 6 6 4 4 4
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
