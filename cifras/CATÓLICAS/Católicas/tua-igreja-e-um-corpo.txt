Católicas - Tua Igreja É Um Corpo

G               Bm    C                  G      Em
Tua igreja é um corpo, cada membro é diferente
                    Am    D                   G  C  G
E há no corpo, certamente, coração, ó meu senhor
                 Bm   C                     G      Em
Dele nasce a caridade, dom maior, mais importante
                      Am    D                  G
Nele, enfim, achei radiante, minha vocação: o amor

           Bm         Em              Bm           Em
1 - Que loucura não fizeste, vindo ao mundo nos salvar
         A             D             A           D
    E depois que tu morreste, ficas vivo neste altar

            Bm             Em         Bm           Em
2 - Os teus santos compreenderam teu amor sem dimensão
          A         D             A          D
    E loucuras cometeram, em sua própria vocação

          Bm              Em          Bm       Em
3 - Sou pequeno, igual criança, cheio de limitações

          A                D             A          D
    Mas é grande minha esperança: sinto muitas vocações

          Bm           Em         Bm              Em
4 - Quero ser um missionário até quando o sol der luz
          A        D           A         D
    Dá-me por itinerário toda terra, ó jesus

          Bm            Em              Bm             Em
5 - O martírio, eis meu sonho. dar-te o sangue, de uma vez
            A           D            A              D
     A mil mortes me disponho; sofrerei com intrepidez

           Bm          Em            Bm           Em
6 - Tantas vocações sentindo, que martírio, meu senhor
          A            D            A           D
     Alegrei-me, descobrindo, minha vocação: o amor

          Bm           Em             Bm        Em
7 - Sentimento é coisa vaga. por meus atos provarei
            A               D          A          D
     Que o amor com amor se paga: toda cruz abraçarei

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
