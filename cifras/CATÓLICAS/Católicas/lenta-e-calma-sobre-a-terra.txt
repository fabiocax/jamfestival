Católicas - Lenta e Calma Sobre a Terra

C                     Dm
Lenta e calma sobre a terra,
         G7             C
Desce a noite e foge a luz.
  Am    Em   F    C           G            C
Quero agora despedir-me: Boa noite, meu Jesus!
  Am    Em   F    C           G7           C
Quero agora despedir-me: Boa noite, meu Jesus!

C                   Dm           G7             C
Ó senhor, dai-nos a benção, e do mal que nos seduz.
          Am       Em      F        G            C
Aos meus pais e a mim guardai: Boa noite, meu Jesus!
          Am       Em      F        G7           C
Aos meus pais e a mim guardai: Boa noite, meu Jesus!

C                    Dm            G7          C
A teus pés, ó virgem pura, peço a benção maternal!
     Am           Em         G            C
Boa noite, Mãe querida; Boa noite, meu Jesus!
     Am           Em         G7           C
Boa noite, Mãe querida; Boa noite, meu Jesus!

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
