Católicas - Mais Uma Vez

                   D                                                  Bm
Mais uma vez, estamos reunidos com muita alegria
                             Bm/A                           G
Hoje é um novo encontro é uma novo dia
                     Em                A    A7
Existe muita coisa para acontecer.
      D                                                       Bm
E é tão bom, agente reza, canta, louva e agradece
                                    Bm/A                           G
Deus vem ao nosso encontro e tudo acontece
                        Em                          A
E todo mundo fica muito mais feliz.

              G           Gm        D         A/C#   Bm
Meu irmão vamos lá, toma logo a minha mão
            Em             A7                      D  D7
Que a paz, a paz esteja no seu coração. (2x)


----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
Bm/A = X 0 4 4 3 X
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
