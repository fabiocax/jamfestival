Católicas - Teu Sol Não Se Apagará

 D                      Bm                   G
Teu sol não se apagará, tua lua não terá minguante,
    Em            A       D     G        A       D
Porque o Senhor será tua luz, ó povo que Deus conduz!

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
