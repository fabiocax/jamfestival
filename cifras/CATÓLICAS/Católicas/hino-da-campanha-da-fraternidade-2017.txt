Católicas - Hino da Campanha da Fraternidade 2017

Intro: Bb7 F/A Dm G Eb Dm C4 C


C7        F         C            F    C7
 Louvado sejas, ó Senhor, pela mãe terra,
          F          C      C/Bb   A  A/G  F#°
Que nos acolhe, nos alegra e dá o pão (cf. LS,1).
         Gm7           C          F   F/A
Queremos ser os teus parceiros na tarefa
        Bb            G7       C    C7
De "cultivar e bem guardar a criação".

        F    A7    Dm    Dm/C
Da Amazônia até os Pampas,
     Bb        G/B   C
Do Cerrado aos Manguezais,
         Gm   C7     F    Dm
Chegue a ti o nosso canto
     G9/B    C7   F  C   (F)
Pela vida e pela paz. (2x)


    C7        F            C            F    C7
02 - Vendo a riqueza dos biomas que criaste,
          F             C      C/Bb   A  A/G  F#°
Feliz disseste: tudo é belo, tudo é bom!
         Gm7         C          F   F/A
E pra cuidar da tua obra nos chamaste
        Bb           G7            C    C7
A preservar e cultivar tão grande dom (cf. Gn 1-2).

    C7            F         C            F    C7
03 - Por toda a costa do país espalhas vida;
           F           C      C/Bb     A  A/G  F#°
São muitos rostos - da Caatinga ao Pantanal:
         Gm7           C          F   F/A
Negros e índios, camponeses: gente linda,
        Bb            G7           C    C7
Lutando juntos por um mundo mais igual.

    C7        F          C            F    C7
04 - Senhor, agora nos conduzes ao deserto
               F          C      C/Bb   A  A/G  F#°
E, então nos falas, com carinho, ao coração (cf. Os 2,16),
         Gm7                C          F   F/A
Pra nos mostrar que somos povos tão diversos,
        Bb             G7          C    C7
Mas um só Deus nos faz pulsar o coração.

    C7        F              C            F    C7
05 - Se contemplarmos essa "mãe" com reverência,
          F          C      C/Bb   A  A/G  F#°
Não com olhares de ganância ou ambição,
         Gm7           C          F   F/A
O consumismo, o desperdício, a indiferença
          Bb            G7       C    C7
Se tornam luta, compromisso e proteção (cf. LS, 207).

    C7          F              C        F    C7
06 - Que entre nós cresça uma nova ecologia (cf. LS, Cap. IV),
          F          C      C/Bb     A  A/G  F#°
Onde a pessoa, a natureza, a vida, enfim,
         Gm7           C          F   F/A
Possam cantar na mais perfeita sinfonia
        Bb            G7            C    C7
Ao Criador que faz da terra o seu jardim.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/G = 3 X 2 2 2 X
A7 = X 0 2 0 2 0
Bb = X 1 3 3 3 1
Bb7 = X 1 3 1 3 1
C = X 3 2 0 1 0
C/Bb = X 1 2 0 1 X
C4 = X 3 3 0 1 X
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
Dm/C = X 3 X 2 3 1
E = 0 2 2 1 0 0
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
F#° = 2 X 1 2 1 X
F/A = 5 X 3 5 6 X
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G7 = 3 5 3 4 3 3
G9/B = 7 X X 7 8 5
Gm = 3 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
