Católicas - Vem Oh! Água Viva

D                    A9   Bb°           Bm
Batiza-me Senhor no Teu Espírito
                 G  Em      A9             A
Pois minh alma sedenta está.   (2X)

D                     F#m                   G
Vem oh! Água viva , oh! Água pura
      Em                     A9   A
Fecundar meu coração.
D                     F#m                   G
Vem oh! Água viva , oh! Água pura
              A                       D
Transformar meu coração.

D                A       Bb°           Bm
Cura-me Senhor no Teu Espírito
                G      Em       A9      A
Pois meu coração ferido está.   (2X)

D                     F#m                   G
vem oh! Água viva , oh! Água pura...

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
Bb° = X 1 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
