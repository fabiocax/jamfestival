Católicas - Os Cristãos Tinham Tudo Em Comum

Dm                  A7        Dm
Os cristãos tinham tudo em comum/
   Gm                       A7
Dividiam seus bens com alegria/
        Dm                      Gm
Deus espera que os dons de cada um,/
      Dm           A            Dm
se repartam com o amor no dia a dia (bis)

        C                       F
1  Deus criou este mundo para todos,
          A                    Dm
quem tem mais é chamado a repartir,
       Gm                    Dm
com os outros o pão, a instrução
        Bb                       A
e o progresso, fazer o irmão sorrir

         C                        F
2. Mas acima de alguém que tem riqueza,
        A                         Dm
está o homem que cresce ao seu valor,

     Gm                Dm
e liberto caminha pra Deus
     Bb                  A
repartindo com todos, o amor.

         C                    F
3. No desejo de sempre repartirmos
        A                    Dm
nossos bens, elevemos nossa voz,
       Gm                       Dm
Ao trazer pão e vinho para o altar
         Bb                     A
em quem Deus vai se dar a todos nós.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
