Católicas - Fazei Ressoar

  Cm       G7      (G7 F#7 Fm)   Fm         G7               Cm         G7
Fazei ressoar, ressoa.......ar a Palavra de Deus em todo o lugar! Vamos lá!
  Cm       G7      (G7 F#7 Fm)   Fm         G7               Cm
Fazei ressoar, ressoa.......ar a Palavra de Deus em todo o lugar!

      Cm         Fm     G7          Cm
Na cultura, na história, vamos expressar
                      Fm  G7         Cm         G7
Levando a Palavra de Deus em todo o lugar, vamos lá!

      Cm      Fm     G7         Cm
Na cultura popular, vamos catequizar
                Fm    G7      Cm         G7
celebrando fé e vida em todo lugar. Vamos lá!

      Cm            Fm      G7        Cm
Com o negro e com o índio, nós vamos louvar
             Fm      G7     Cm         G7
e com a comunidade vamos festejar. Vamos lá!

         Cm            Fm      G7      Cm
Com o pandeiro e com a viola, vamos cantar

                 Fm   G7        Cm         G7
Animando a nossa luta em todo lugar. Vamos lá!

      Cm         Fm        G7         Cm
O Evangelho é a Palavra que Deus programou
              Fm        G7             Cm         G7
Só Ele é o caminho, a verdade, a vida e amor. Vamos lá!

     Cm             Fm      G7        Cm
Juventude caminho aberto, vamos construir
                   Fm      G7      Cm         G7
Fraternidade e renovação, vamos transmitir. Vamos lá!

----------------- Acordes -----------------
Cm = X 3 5 5 4 3
F#7 = 2 4 2 3 2 2
Fm = 1 3 3 1 1 1
G7 = 3 5 3 4 3 3
