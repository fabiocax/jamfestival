Católicas - Santo, Senhor, Deus do Universo

[Intro] C  Am  Dm  G

[Primeira Parte]

 C             Am
Santo, Santo, Santo
 F                  G
Senhor, Deus do universo
   C                 Am
O céu e a terra proclamam
F            G
A Vossa glória

[Refrão]

  C            F C      F G
Hosa-na nas alturas, Hosana
  C            F C      F G
Hosa-na nas alturas, Hosana

    C               Am
Bendito Aquele que vem

    Dm         G
Em nome do Senhor
    C               Am
Bendito Aquele que vem
    Dm         G
Em nome do Senhor

[Refrão]

  C            F C      F G
Hosa-na nas alturas, Hosana
  C            F C      F G
Hosa-na nas alturas, Hosana

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
