Católicas - São João Batista

       D           A7                       D
Um dia, lá na Judéia/ Um homem chamado João/
   G         A7      D                       A7        D
falava com ternura/ De amor aos seus irmãos. (Bis).
             A7                  D
Viva João Batista/ Viva o precursor/
    G         A7         D          A7          D
Porque João Batista anunciou o Salvador. (Bis).
    D                  A7                              D
As vezes João se zangava/ com os duros de coração /
     G         A7               D             A7       D
Dizendo que já estava/ Muito perto a salvação. (Bis).
      D               A7                       D
Viva João Batista/ Viva o precursor/
   G     A7    D            A7               D
Porque João Batista / Anunciava o Salvador. (Bis).
       D                A7                   D
Às margens do rio Jordão/ João batizava o povo/
  G         A7     D                      A7          D
Dizendo que Deus iria/ Instaurar um Reino novo! (Bis)

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
