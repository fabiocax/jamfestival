Católicas - Vimos Aqui

 D      Bm        D E7        Am   D      Bm     D         C         D
Vimos aqui, ó Senhor, pra cantar/ Tua bondade, amor que se dá, sem cessar!
Bm      F#        Bm     F#   Bm     A7        D      A7        D       A7       D   A7
És o caminho, verdade e vida/ És o amigo, que perde a vida/ Buscando a todos salvar!
Bm      F#       Bm    F#  Bm        A7       D          A7        D          A7       D   A7
És o rochedo, o guia fiel/ És a esperança de todos, que buscam/ Viver em tua casa, Senhor!

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
F# = 2 4 4 3 2 2
