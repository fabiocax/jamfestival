Católicas - Hino do MCJ

Hino do MCJ

D       A9                    Bm  G
Venha viver uma nova história de amor com Deus!
D       A9                    Bm  G       D  A9 Bm
Venha viver uma nova história De amor com Deus!
G              D A9 Bm G                   D
De amor com Deus!   De amor com Deus!
     D          A9             Bm                            G
1. MCJ nasceu para unir os casais, Transformar a família num templo de amor.
    D                         A9               Bm                                          G
2. Monsenhor Atílio fundou, este movimento é demais, Evangelizando as famílias, sem os filhos jamais.
D       A9                    Bm  G
Venha viver uma nova história de amor com Deus!
D       A9                    Bm  G       D  A9 Bm
Venha viver uma nova história De amor com Deus!
G               D A9 Bm G           D
De amor com Deus!     União e paz!

----------------- Acordes -----------------
A9 = X 0 2 2 0 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
G = 3 2 0 0 0 3
