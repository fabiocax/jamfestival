Católicas - Companheira Maria

  C           Em                 F                  C
Companheira Maria, perfeita harmonia entre nós e o Pai
 F               C                      D7                    G
modelo dos consagrados, nosso sim ao chamado do Senhor confirmai

C      G    Am      Em
Ave Maria, cheia de graça,
 F                C                    D7                 G7
Plena de raça e beleza, queres, com certeza, que a vida renasça
 C       G   Am       Em
Santa Maria, mãe do Senhor
 F                   C                 G7            C
Que se fez pão para todos, criou mundo novo só por amor

 C             Em                F                   C
Intercessora Maria perfeita harmonia, entre nós e o Pai,
 F                C                 D7                       G
justiça dos explorados, combate o pecado, torna os homens iguais.

C      G    Am      Em
Ave Maria, cheia de graça,

 F                C                    D7                 G7
Plena de raça e beleza, queres, com certeza, que a vida renasça
 C       G   Am       Em
Santa Maria, mãe do Senhor
 F                   C                 G7            C
Que se fez pão para todos, criou mundo novo só por amor

 C               Em                 F                   C
Transformadora Maria, perfeita harmonia, entre nós e o Pai,
 F               C                   D7                     G
espelho de competência, afasta a violência enche o mundo de paz.

C      G    Am      Em
Ave Maria, cheia de graça,
 F                C                    D7                 G7
Plena de raça e beleza, queres, com certeza, que a vida renasça
 C       G   Am       Em
Santa Maria, mãe do Senhor
 F                   C                 G7            C
Que se fez pão para todos, criou mundo novo só por amor

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
