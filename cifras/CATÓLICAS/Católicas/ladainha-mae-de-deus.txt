Católicas - Ladainha Mãe de Deus

C                         G     Am       F        C
Os coros dos anjos vos louvam.  Maria, clamamos a vós
C                         G     Am       F        C
Saúdam-vos todos os santos.  Maria, clamamos a vós
C                         G     Am       F        C
O mundo dos astros vos louva.  Maria, clamamos a vós
 C                         G     Am       F        C
A santa Igreja vos louva.  Maria, clamamos a vós
C                         G     Am       F        C
Os homens na terra vos louvam.  Maria, clamamos a vós
    Am       F         G      C
T: Mãe de Deus, clamamos a vós.S:
C                         G     Am       F        C
Vós sois medianeira das graças. Maria, clamamos a vós.
C                         G     Am       F        C
Sois sede da sabedoria.Sois.  Maria, clamamos a vós
C                         G     Am       F        C
Mãe da eterna beleza.Sois.  Maria, clamamos a vós
C                         G     Am       F        C
Mãe do perpétuo socorro.  Maria, clamamos a vós
C                         G     Am       F        C
Sois Mãe do amor verdadeiro.  Maria, clamamos a vós

.   Am       F          G     C
T: Mãe de Deus, clamamos a vós.S:
C                         G     Am       F        C
Vós sois a alegria dos santos.  Maria, clamamos a vós
C                         G     Am       F        C
Dos mártires sois a Rainha.  Maria, clamamos a vós
C                         G     Am       F        C
Vós sois a Rainha dos justos.  Maria, clamamos a vós
C                         G     Am       F        C
Vós sois o socorro na luta.  Maria, clamamos a vós
C                       G      Am       F        C
Da paz sois fiel mensageira.  Maria, clamamos a vós
.   Am       F          G     C
T: Mãe de Deus, clamamos a vós.S:
C                         G     Am       F        C
Sois fonte de toda a virtude.  Maria, clamamos a vós
C                         G     Am       F        C
Sois templo do Espírito Santo.  Maria, clamamos a vós
C                         G     Am       F        C
Sois arca da nova aliança.  Maria, clamamos a vós
C                         G     Am       F        C
Do reino do céu sois a porta.  Maria, clamamos a vós
C                         G     Am       F        C
Sois glória da santa Igreja.  Maria, clamamos a vós
.   Am       F          G     C
T: Mãe de Deus, clamamos a vós.S:
C                         G     Am       F        C
Vós sois o refúgio nas dores.  Maria, clamamos a vós
C                         G     Am       F        C
Vós sois o auxílio do povo.  Maria, clamamos a vós
C                         G     Am       F        C
Vós sois dos enfermos saúde.  Maria, clamamos a vós
C                         G     Am       F        C
Consolo dos desamparados.  Maria, clamamos a vós
C                         G     Am       F        C
Na morte sois nossa esperança.  Maria, clamamos a vós

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
