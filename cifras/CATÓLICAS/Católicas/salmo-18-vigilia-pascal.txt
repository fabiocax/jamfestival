Católicas - Salmo 18 - Vigília Pascal

   C           Dm     G               F  C
Senhor, só tu tens palavras de vida eterna.

   C            Am         Dm        G           F  C
A lei do Senhor Deus é perfeita, conforto para a alma.

        C          C    C7  F  Fm     C        G7   C
O testemunho do Senhor é fiel, sabedoria dos humildes.


( Mais três estrofes.)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
