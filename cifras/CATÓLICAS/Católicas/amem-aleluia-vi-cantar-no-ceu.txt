Católicas - Amém, Aleluia! Vi Cantar No Céu

F  Dm     Bb  F
A mém, Alelu ia! (Bis)

             Dm
Vi cantar no céu
  Bb         F
A feliz multidão
Dm
Dos féis eleitos
 Bb       C
De toda nação

Ao que está sentado
No trono, louvor,
E poder ao Cristo,
Seu filho e Senhor.

Com amor eterno
Jesus nos amou,
E as nossas vestes
Com sangue lavou.

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
