Católicas - Hino Das Filhas de Maria

    C
Óh mãe querida, aos vossos pés um dia
                              G
Viemos nosso amor vos consagrar.
       C        G          C G/B Am7
Somos filhas devotas de maria
         F                       G
Assim juramos, junto ao vosso altar
      C
Cada uma de nós a vós pertence
               Am7               Dm
Guardai-nos sempre em vosso coração
         G                   C G/B Am7
E neste puro amor, que tudo vence
         F                    G
Encontraremos: força e proteção

refrão:
      C
O brasil, nas estrelas do cruzeiro
    Am7
O nome de maria, vê brilhar

       Dm
No coração do povo brasileiro
          G                   C G/B Am7
O vosso amor, ó mãe há de reinar
          F      G            C
O vosso amor, ó mãe há de reinar!

Segunda parte-
         C
A fita azul, será nossa bandeira
                              G
Penhor do vosso puro e santo amor
   C             G            C G/B Am7
A medalha que é nossa compannheira
        F                       G
Aumentará, em nós, sempre o fervor
       C
Se na luta faltar-nos a coragem
         Am7                Dm
O vosso amor vira nos socorrer
    G                           C G/B Am7
Beijando, na medalha, a doce imagem
           F                    G
Da mãe querida, havemos de vencer.

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
