Católicas - Dê Um Sorriso Só

D         A7                D
Dê um sorriso só,sorriso aberto
        A7            D
Sorriso certo,cheio de amor
D         A7                D
Dê um sorriso só,sorriso aberto
        A7            D
Sorriso certo,cheio de amor

     A7
Quem tem jesus
        D
Gosta de cantar
            A
Está sempre sorrindo
                D
Mesmo quando não dá
        A7           D
Tropeça aqui,oi,cai acolá
       G
Mas depressa levanta

    A7      D
E começa a cantar
       G
Mas depressa levanta
    A7      D
E começa a cantar

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
