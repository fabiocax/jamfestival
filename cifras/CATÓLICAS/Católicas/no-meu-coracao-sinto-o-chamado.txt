Católicas - No Meu Coração Sinto o Chamado

Intro: A E

 A
No meu coração sinto o chamado
                        E
Sei que é a voz do meu Senhor
     D            Bm        A  (A7)
Eu irei, eu seguirei aonde for (2x)

D                  A
Aleluia gloria a Deus
D                  A
Aleluia gloria a Deus
E                D              A
É Jesus, meu Senhor quem vai falar
E                D              A
É Jesus, meu Senhor quem vai falar

 A
No meu coração sinto o chamado

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
