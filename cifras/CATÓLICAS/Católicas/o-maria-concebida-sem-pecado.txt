Católicas - Ó Maria Concebida Sem Pecado

A     D      G    D             Bm         A
1.Ó Maria concebida sem pecado original
           D             G        D             A           E   A
Quero amar-vos toda a vida Com ternura filial

REFRÃO:
              A                  D                    A                 D
Vosso olhar a nós volvei/Vossos filhos protegei,
D7                  G                             D      A      D
Ó Maria, Ó Maria /Vossos filhos protegei (2x)


A     D      G    D             Bm         A
2.Mais que a aurora sois formosa,/Mais que o sol resplandeceis!
           D             G        D             A           E   A
Do universo, Mãe bondosa,/O louvor vós mereceis.


A     D      G    D             Bm         A
3.Nesta terra peregrina,/Nós buscamos vida e luz;
           D             G        D             A           E   A
Virgem santa, conduzi-nos/Para o Reino de Jesus!



A     D      G    D             Bm         A
4.Exaltamos a beleza/Com que Deus vos quis ornar.
           D             G        D             A           E   A
Vossa graça de pureza/Venha em nós também brilhar.

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
