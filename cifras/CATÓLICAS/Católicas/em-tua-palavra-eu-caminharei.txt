Católicas - Em Tua Palavra Eu Caminharei

Dm           Bb     C             F
Tu és minha vida, outro Deus não há!
Dm             Bb        C         F
Tu és minha estrada, a minha verdade
Gm       C      F         Dm
Em tua palavra eu caminharei
Gm            C       F           A7
Enquanto eu viver e até quando tu quiseres
Gm                C     F           Dm
Já não sentirei temor, pois, estás aqui:
Bb           C       Dm
Tu estás no meio de nós
Dm              Bb    C           F
Creio em Ti, Senhor, vindo de Maria
Dm             Bb      C           F
Filho eterno e Santo, homem como nós
Gm          C          F            Dm
Tu morreste por amor; vivo estás em nós
Gm        C    F                 A7
Unidade trina com o Espírito e o Pai
Gm     C F Dm
E um dia eu bem sei: tu retornarás

Bb C Dm
E abrirás o Reino dos Céus
Dm Bb C F
Tu és minha força, outro Deus não há!
Dm Bb C F
Tu és minha paz, minha liberdade...
Gm C F Dm
Nada nesta vida nos separará
Gm C F A7
Em tuas mãos seguras minha vida guardarás
Gm C F Dm
Eu não temerei o mal, tu me livrarás.
Bb C Dm
E no teu perdão viverei!
Dm Bb C F
Ó, Senhor da vida, creio sempre em ti!
Dm Bb C F
Filho Salvador, eu espero em ti!
Gm C F Dm
Santo Espírito de amor: desce sobre nós
Gm C F A7
Tu, de mil caminhos, nos conduzes a uma fé...
Gm C F Dm
E por mil estradas onde andarmos nós
Bb C Dm
Qual semente nos levarás! (2x)

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
