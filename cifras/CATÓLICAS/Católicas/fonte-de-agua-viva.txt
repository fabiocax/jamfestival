Católicas - Fonte de Água Viva

E                             F#m
Água cristalina que jorra do peito
  B7         E          B7
Aberto de Jesus lá na cruz.
  E                                F#m
É uma água viva, que cura e que liberta,
        B7         F#m    B7    E     B7
cuja a fonte é o pró....prio Jesus

   E           B7
Chuê, Chuê, Chuá, Chuá,
      F#m      B7        E   B7
Nesta água eu vou me banhar
   E           B7
Chuê, Chuê, Chuá, Chuá,
      F#m      B7        E       B7   E
Nesta água eu vou me banhar

----------------- Acordes -----------------
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
