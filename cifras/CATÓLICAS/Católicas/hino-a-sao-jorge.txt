Católicas - Hino a São Jorge

       D
Lá do céu
      A          D
O Senhor nos mandou
           B7    Em
Um amigo e protetor;
B7                  Em
        Valente guerreiro
         A7
Amigo sincero,
Nas horas difíceis
             D            D7
E de duro labor.

       G
Ó São Jorge,
              D
Protetor glorioso,
  Bm                Em
Que do céu tão bondoso,
      A7        D      D7
O Senhor quis mandar.

      G
Aqui vive
                 D
A teus pés, venturoso
Bm            Em
O teu povo humilde
Bm                 D
Que te quer muito amar.

      D
Nossa fé
     A         D
Em Cristo repousa,
          B7        Em
Nosso amor nele também,
B7          Em
É fonte de vida,
                A7
Esperança e alegria
De sempre vivermos
          D            D7
Em sua família.

       G
Ó São Jorge,
              D
Protetor glorioso,
        Bm        Em
Que do céu tão bondoso,
     A7           D      D7
O Senhor quis mandar.
      G
Aqui vive
                 D
A teus pés, venturoso
Bm              Em
O teu povo humilde
Bm                  D
Que te quer muito amar.

      D
Nossa luta
  A           D
É toda de Cristo,
      B7       Em
Na estrada do amor.
B7         Em
Queremos viver
                A7
Na caridade unidos,
Como lá no céu
             D            D7
Havemos de estar.

       G
Ó São Jorge,
              D
Protetor glorioso,
  Bm                Em
Que do céu tão bondoso,
      A7        D      D7
O Senhor quis mandar.
      G
Aqui vive
                 D
A teus pés, venturoso
Bm            Em
O teu povo humilde
Bm                 D
Que te quer muito amar.

        D
Nossa glória
     A          D
É estarmos bem juntos,
    B7              Em
No amor que nos congrega,

B7             Em
Lutando na Igreja,
              A7
Como povo de Deus,
Pelo santo ideal
                D            D7
Que Cristo ensinou.

       G
Ó São Jorge,
              D
Protetor glorioso,
  Bm                Em
Que do céu tão bondoso,
      A7        D      D7
O Senhor quis mandar.
      G
Aqui vive
                 D
A teus pés, venturoso
Bm            Em
O teu povo humilde
Bm                 D
Que te quer muito amar.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
