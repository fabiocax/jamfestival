Católicas - Bem Aventurados Os Misericodiosos

  C#m    A                 E
1.Levantarei meu olhar aos montes
D            A      E
De onde o auxílio virá
         B                 F#
Deus é a força de quem tem fé
A    B          C#
Misericórdia Ele é

  C#m                      A                          E
2.Quando erramos Ele é por nós
D            A       E
Mostra-nos o colo do Pai
        B           F#
Com seu sangue libertador
A        B       C#
Livra do mal e da dor

          E  B               C#m  A
Bem aventurados os misericordiosos
E                  B         F#
Porque eles alcançarão misericórdia


  C#m     A           E
3.Sem seu perdão quando eu cair
D           A        E
Quem poderá me levantar?
        B           F#
Se Deus perdoa quem somos nós
A        B       C#
Para não perdoar?

  C#m     A          E
4.O sangue de Cristo nos resgatou
D           A       E
Ele ressuscitou
      B           F#
Grite pro mundo inteiro ouvir
A        B       C#
Jesus Cristo é o Senhor

  A           B          C#m
5.Deixa o teu medo e tem fé
A       B       A
Um novo tempo virá
            B                C#m
Cristo está vivo: vivo entre nós!
A            B            C#m
E um dia Ele voltará

          A  E               F#m  D
Bem aventurados os misericordiosos
A                  E         B     (F#m)
Porque eles alcançarão misericórdia

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C# = X 4 6 6 6 4
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
