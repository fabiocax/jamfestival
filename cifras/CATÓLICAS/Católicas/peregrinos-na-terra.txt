Católicas - Peregrinos Na Terra 

E
1. Peregrinos na Terra nós somos
       B                    E  E7
nossa fé nos sustenta e conduz;
         A          A°      E C#m
Muitos deram por Cristo sua vida,
      B         B7          E
testemunhas do amor e da cruz.

       E               F#m
O martírio sinal de vitória,
     B                 E   E7
revigora a fé dos cristãos.
        A                   E   C#m
Nosso chão foi banhado com sangue,
     F#m        B        E
sacrifício de tantos irmãos.

E
2. Celebremos a augusta memória
       B                 E  E7
dos irmãos mortos em Cunhaú,

        A           A°      E C#m
e o martírio dos servos de Deus
         B      B7    E
que morreram em Uruaçu.

      E                F#m
O martírio sinal de vitória,
      B               E   E7
revigora a fé dos cristãos.
        A                   E   C#m
Nosso chão foi banhado com sangue,
     F#m         B       E
sacrifício de tantos irmãos.

E
3. Numa clara manhã de Domingo
      B                    E  E7
os fieis se reuniram na Igreja.
      A          A°       E  C#m
Com o padre na missa ofertaram.
      B       B7              E
Sua vida ao Senhor, glória seja!

      E                F#m
O martírio sinal de vitória,
      B              E   E7
revigora a fé dos cristãos.
      A                     E   C#m
Nosso chão foi banhado com sangue,
      F#m        B       E
sacrifício de tantos irmãos.

E
4. Junto ao rio, em Uruaçu
        B           E       E7
foram muitos os martirizados.
        A           A°        E C#m
Jovens, velhos, crianças, mulheres,
        B         B7       E
por Jesus todos foram imolados.

       E               F#m
O martírio sinal de vitória,
      B              E   E7
revigora a fé dos cristãos.
        A                  E   C#m
Nosso chão foi banhado com sangue,
       F#m       B       E
sacrifício de tantos irmãos.

E
5. Quando seu coração foi tirado
       B                  E   E7
com coragem Mateus proclamou
     A       A°         E C#m
sua fé em Jesus, Deus presente
     B         B7       E
no santíssimo Dom do amor.

     E                  F#m
O martírio sinal de vitória,
     B              E   E7
revigora a fé dos cristãos.
        A                   E   C#m
Nosso chão foi banhado com sangue,
     F#m         B        E
sacrifício de tantos irmãos.

E
6. O Evangelho chegou ao Rio Grande.
         B              E  E7
Com o martírio total doação.
       A        A°      E C#m
Hoje todos nós somos chamados
       B       B7      E
a uma Nova evangelização.

       E                F#m
O martírio sinal de vitória,
       B               E   E7
revigora a fé dos cristãos.
       A                    E   C#m
Nosso chão foi banhado com sangue,
       F#m       B        E
sacrifício de tantos irmãos.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A° = 5 X 4 5 4 X
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
