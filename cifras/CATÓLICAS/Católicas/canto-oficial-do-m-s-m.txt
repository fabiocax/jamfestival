Católicas - Canto Oficial do M.S.M

     A        D        A
Imaculado Coração de Maria
     A        D        A
Imaculado Coração de Maria
A7      D    E        A     E
És luz e guia, és luz e guia
F#m      Bm             E      A
És luz e guia dos teus filhos aqui

       A        D       A
Os sacerdotes a ti consagrados
       A        D       A
Os sacerdotes a ti consagrados
A7       D      E            A   E
Olha com amor, aperta ao coração
F#m         Bm
Faze-os semelhantes
       E       A
Ao teu filho Jesus
       E       A
Ao teu filho Jesus


     A     D       A
Os fiéis a ti consagrados
     A     D       A
Os fiéis a ti consagrados
A7           D
Reúne em teu exército
     E          A      E
Preparados pra batalha
    F#m         Bm
Pra que triunfe logo
       E        A
O Imaculado Coração

      A         D      A
Ao chegar nossa última hora
      A         D      A
Ao chegar nossa última hora
A7        D
Vem sem demora
E         A    E
Vem sem demora
F#m       Bm
Vem sem demora
    E              A
A levar-nos para o céu

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
