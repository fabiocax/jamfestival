Católicas - Com o Pé Na Estrada

F                C7              Dm                 C7
Tu chegaste um dia falaste de um Reino de amor e justiça e disseste:
F                 C7                Dm                   C7  Bb  Am Gm
Vai, inflama esta terra com minha palavra, E eu te seguirei...
F                      C7   Dm                                   Eb
Te seguirei neste caminhar, Deixei pra trás a saudade de onde eu venho.
C7                            F       F7          Bb  Gm                C7
Só trago um par de sandálias, e minha voz pra gritar que és a força que tenho
F                        C7   Dm                             Bb
E todo aquele que me escutar, Entenderá a verdade do teu Evangelho
C7                                     F  F7     Bb   Gm                     C7                    F  C7
E verei como são belos os pés de quem vai Te pregar, Não noutra cruz, mas no peito de quem Te aceitar.

Refrão:
     F               C7                     Dm                               C7
Eu vou sem descanso, pela estrada canto Teu nome, que é pra toda a terra escutar
           F     C7                   Dm                            Eb
E nessa estrada não existe nada que impeça que eu leve Tua luz pelo mundo,
                  C7                               F        C7
Porque continuo ouvindo tua voz no meu próprio cantar (can-tar)


 F                      C7  Dm                                       Eb     C7
Mas se nesse longo caminhar a solidão for mais forte que a força que tenho, esquecerei que sou fraco e
            F    F7   Bb   Gm                     C7                    F      C7
prometo que vou Te pregar, não noutra cruz mas no peito de quem Te aceitar.

     F               C7                     Dm                               C7
Eu vou sem descanso, pela estrada canto Teu nome, que é pra toda a terra escutar
           F     C7                   Dm                            Eb
E nessa estrada não existe nada que impeça que eu leve Tua luz pelo mundo
                  C7                               F     C7
Porque continuo ouvindo Tua voz no meu próprio cantar

Tua voz no meu próprio cantar, Tua voz no meu próprio cantar

La-iá, laiá-laiá, lá-lá-iá, laiá, laiá, laiá, laiá (2x)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
Gm = 3 5 5 3 3 3
