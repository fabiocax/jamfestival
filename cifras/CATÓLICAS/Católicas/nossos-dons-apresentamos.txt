Católicas - Nossos Dons Apresentamos!

[Intro] Dm  G  Dm  Am  Dm

        Dm    G    Dm
Nossos dons apresentamos
          Em     Am
Em memória do cordeiro
    G    Dm       G
Revivemos os seus passos
     Dm   Am   Dm
Somos povo caminheiro

[Refrão]

           F    C    Dm
Eis que o novo nascimento
     G       Dm
Da humana criatura
    F      C     Dm
É sinal da Páscoa nova
      G        Dm/A  Am  Dm
Nesta mesa já fulgura!


Dm       G        Dm
É feliz quem persevera
             Em   Am
Na justiça e na verdade
    G        Dm      G
Espalhando o bom perfume
 Dm         Am     Dm
E o frescor da caridade!

[Refrão]

           F    C    Dm
Eis que o novo nascimento
     G       Dm
Da humana criatura
    F      C     Dm
É sinal da Páscoa nova
      G        Dm/A  Am  Dm
Nesta mesa já fulgura!

Dm           G       Dm
Nossa terra, grande ventre!
             Em    Am
É o lugar da esperança
      G     Dm     G
Somos todos cultivados
 Dm       Am     Dm
No jardim da aliança!

[Refrão]

           F    C    Dm
Eis que o novo nascimento
     G       Dm
Da humana criatura
    F      C     Dm
É sinal da Páscoa nova
      G        Dm/A  Am  Dm
Nesta mesa já fulgura!

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Dm/A = X 0 X 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
