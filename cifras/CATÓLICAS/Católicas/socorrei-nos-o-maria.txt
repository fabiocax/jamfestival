Católicas - Socorrei-nos Ó Maria

     C        F   C            Am  F      G
Socorrei-nos, ó maria, noite e dia sem cessar;
     C     F     C              G     D    G
Os doentes e os aflitos, vinde, vinde consolar!

        G      G/F    C
Vosso olhar a nós volvei,
       G      G/F  C
Vossos filhos protegei!
    C        Am
Ó maria, ó maria,
       C      G   C
Vossos filhos socorrei!

     C     F     C         Am     F      G
Visitai os que padecem, aliviando-lhes a dor;
      C     F       C         G        D     G
A nós todos volvei, hoje, um olhar cheio de amor!

      C      F       C            Am    F      G
Dai saúde ao corpo enfermo, dai coragem na aflição.

       C     F      C          G         D   G
Sede a nossa doce estrela a brilhar na escuridão.

     C        F    C          Am     F     G
Que tenhamos, cada dia, pão e paz em nosso lar;
     C      F     C           G     D       G
E de deus a santa graça vos pedimos neste altar.

      C      F   C          Am     F     G
Convertei os pecadores para que voltem a deus;
         C      F    C         G     D       G
Dos transviados sede guia no caminho para os céus.

      C       F   C             Am     F     G
Nas angústias e receios sede, ó mãe, a nossa luz,
        C      F       C          G      D     G
Dai-nos sempre fé e confiança no amor do bom jesus.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/F = 1 X X 0 0 3
