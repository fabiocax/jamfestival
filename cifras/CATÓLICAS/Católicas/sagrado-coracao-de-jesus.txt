Católicas - Sagrado Coração de Jesus

C                     G
Sagrado Coração de Jesus
      F        G       C
Eu confio e espero em vós
C                     G
Sagrado Coração de Jesus
      F        G       C
Eu confio e espero em vós

C            Am7      Dm
Eu confio e espero em vós
C            G         C
Eu confio e espero em vós

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
