Católicas - Oração de São Francisco

  E                             G#m7  C#m
Senhor, fazei-me instrumento de vossa paz
            F#m                  C#m
Onde houver ódio, que eu leve o amor
             F#m                  C#m
Onde houver ofensa, que eu leve o perdão
                E             G#7       C#m
Onde houver discórdia, que eu leve a união
            F#m            G#7    C#m
Onde houver dúvida, que eu leve a fé

            F#m                    C#m
Onde houver erro, que eu leve a verdade
                 F#m                    C#m
Onde houver desespero que eu leve a esperança
                E            G#7     C#m
Onde houver tristeza, que eu leve alegria
             C             G#7    C#m
Onde houver trevas, que eu leve a luz

  E                            F#m7
Ó mestre, fazei que eu procure mais

     G#7               C#m
Consolar, que ser consolado
        A                    B
Compreender, que ser compreendido
 E   G#7       C#m
Amar, que ser amado
        G#7            C#m
Pois, é dando que se recebe
       A                  B
É perdoando que se é perdoado
      E      G#7    C#m          G#7      C#m
E é morrendo que se vive, para a vida eterna

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
F#m7 = 2 X 2 2 2 X
G#7 = 4 6 4 5 4 4
G#m7 = 4 X 4 4 4 X
