Católicas - Estás Entre Nós

Dm          Bb    C               F
Tu és minha vida, outro Deus não há
Dm             Bb    C           F
Tu és minha estrada, a minha verdade
Gm        C    F          Dm
Em tua palavra eu caminharei
Bb             C          F           A
Enquanto eu viver e até quando tu quiseres
Gm           C          F           Dm
Já não sentirei temor, pois estás aqui
Bb           C       Dm
Tu estás no meio de nós

  Dm             Bb   C          F
Creio em ti, Senhor, vindo de Maria
Dm              Bb     C          F
Filho eterno e Santo, homem como nós
Gm           C         F             Dm
Tu morreste por amor; vivo estás em nós
Bb        C            F          A
Unidade Trina com o Espírito e o Pai

Gm         C            F         Dm
E, um dia, eu bem sei: tu retornarás
Bb           C         Dm
E abrirás o Reino dos Céus

Dm           Bb    C               F
Tu és minha força, outro Deus não há
Dm           Bb   C          F
Tu és minha paz, minha liberdade
Gm          C    F         Dm
Nada nesta vida nos separará
Bb              C           F          A
Em tuas mãos seguras minha vida guardarás
Gm          C          F          Dm
Eu não temerei o mal, tu me livrarás.
Bb           C       Dm
E no teu perdão viverei

Dm           Bb      C              F
Ó, Senhor da Vida, creio sempre em ti
Dm          Bb  C             F
Filho Salvador, eu espero em ti
 Gm          C           F           Dm
Santo Espírito de amor, desce sobre nós
Bb            C              F          A
Tu, de mil caminhos, nos conduzes a uma fé
Gm            C    F              Dm
E por mil estradas onde andarmos nós
 Bb           C   A   Dm
Qual semente nos levarás

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
