Católicas - Ó Senhor, Tende Piedade de Nós

Am                       Dm      E
Oh   se nh or   tende piedade de nós
Am                       Dm      E
Oh   se nh or   tende piedade de nós

  C G   Am          Dm      E
Cri----sto tende piedade de nó--s
  C G   Am          Dm      E
Cri----sto tende piedade de nó--s

Am                    Dm     E
Oh se nh or tende piedade de nós
C    G   Am   C      E         Am
Nova criatura sou, o senhor me perdoou
Am                    Dm     E
Oh se nh or tende piedade de nós
C    G   Am   C      E         Am
Nova criatura sou, o senhor me perdoou

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
