Católicas - Dourados Trigais

       C                Em      F                D7 G      E                 Am       D7                 G
1. Dourados trigais o homem cultivou; os grãos que colheu em pão transformou.

  Am          Em            F                  C                 Am                                    Dm D7  G7          C
Humildemente, ofertamos, Senhor, tudo aquilo que o Teu grande Amor em nós realizou!

       C                Em      F                D7 G      E                 Am       D7           G
2. Verdes parreirais o homem cultivou; os grãos que colheu em vinho tornou.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
