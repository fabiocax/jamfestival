Católicas - Em Tua Mesa Quero Por

Intro: D G A D A

      D             F#m           G             D
As sementes que me deste, que não eram pra guardar,
                      F#m         G      A    D   D7
pus no chão da minha vida, quis fazer frutificar.

Refrão 2x:
          G                        D
Dos meus dons que recebi   pelo Espírito do amor,
          Em                          A7         D   D7  A
trago os frutos que colhi   e em tua mesa quero pôr.

       D            F#m          G         D
Pelos campos deste mundo, quero sempre semear
                    F#m            G  A       D   D7
os talentos que me deste, para eu mesmo cultivar.

(Refrão)

        D              F#m            G           D
Quanto mais eu for plantando, mais terei para colher;

                      F#m          G      A     D   D7
quanto mais eu for colhendo, mais terei a oferecer.

(Refrão)

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
