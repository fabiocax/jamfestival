Católicas - Vem Espírito

[Intro]  C  F  C  F  C  F

       C   F           C  F
Vem Espírito , Vem Espírito

Dm                G          Dm               G
Sozinho eu não posso mais , Sozinho eu não posso mais

Dm                G         C    ( F G )
Sozinho eu não posso mais viver

C                    G/B
Eu quero amar , Eu quero ser

Am               Em
Aquilo que Deus quer

Dm                G
Sozinho eu não posso mais

Dm                G
Sozinho eu não posso mais


Dm                G         C   ( F G )
Sozinho eu não posso mais viver

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
