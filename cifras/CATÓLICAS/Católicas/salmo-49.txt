Católicas - Salmo 49

   D                Bm
A todos que procedem retamente,
       C                    A
eu mostrarei a salvação que vem de Deus.


        F#m                 Bm
1-'Reuni à minha frente os meus eleitos,
      F#m                     Bm
que selaram a Aliança em sacrifícios!'
     C                  A
Testemunha o próprio céu seu julgamento,
         C                    A
porque Deus mesmo é juiz e vai julgar.R.


2-'Escuta, ó meu povo, eu vou falar; +

ouve, Israel, eu testemunho contra ti: *

Eu, o Senhor, somente eu, sou o teu Deus!


Eu não venho censurar teus sacrifícios, *

pois sempre estão perante mim teus holocaustos; R.

3-Imola a Deus um sacrifício de louvor *

e cumpre os votos que fizeste ao Altíssimo.

Quem me oferece um sacrifício de louvor, *

este sim é que me honra de verdade.

A todo homem que procede retamente, *

eu mostrarei a salvação que vem de Deus'.

----------------- Acordes -----------------
A*  = X 0 2 2 2 0 - (*G na forma de A)
Bm*  = X 2 4 4 3 2 - (*Am na forma de Bm)
C*  = X 3 2 0 1 0 - (*A# na forma de C)
D*  = X X 0 2 3 2 - (*C na forma de D)
F#m*  = 2 4 4 2 2 2 - (*Em na forma de F#m)
