Católicas - Salve Ó Virgem da Penha

Intro: E A E B7 E A E

E                   A        E   E7      A                 E
Salve, ó Virgem de céu pura rosa, Casto lírio de níveo candor;
        B7                 A                              B7
Dos mortais és a Mãe carinhosa, Nosso encanto, doçura e amor!


        A
Mãe da Penha, esses teus olhos
       B7     A      E
A nós volve, teus devotos; Somos filhos,
           B7          E      B7   E B7
os nossos votos Ouve, Mãe do Redentor
       E     E7         A          E      A B7 E A E
somos filhos os nossos votos ouve mãe do Redentor


      E         A         E E7      A                   E
Com a alma confiante, Senhora ao calor maternal de Seu manto;

      B7                 A                                 B7
Nós viemos pedir, nesta hora, Teu carinho, e sentir teu encanto! (Refrão)


    E         A       E E7
Ó Rainha, Senhora da Penha,
      A                    E
Ao aflito, oh! volve um olhar;
    B7                    A
O doente o conforto aqui tenha
                           E7
Tua bênção acompanhe-o ao lar! (Refrão)

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
