Católicas - Eu Acredito

[Intro] Bm  E7/B  Bm  E7

       Bm          E7           Bm
Eu acredito que o mundo será melhor
 E7                  Bm         E7      Bm
Quando o menor que padece acreditar no menor

      Bm          E7           Bm
Eu acredito que o mundo será melhor
 E7                  Bm         E7      Bm
Quando o menor que padece acreditar no menor

Bm                       Em            A          D7+
Quando os pequenos acreditarem no seu bem estar comum
   F#              Bm          E7        Bm
Sentindo as necessidades que padece cada um
 Em              Bm           F#          Bm
Unidos em Jesus Cristo, nós todos seremos um

Bm                  Em          A7/5-        D7+/9
Jesus Cristo veio à Terra para ver seu povo unido

       F#           Bm       E7             Bm
Disse até que cada grupo que luta em si dividido
    Em          Bm        F#        Bm   E7
Com muita facilidade, ele será destruído

Bm                 Em         A       D7+
Certo dia um jovem rico a  Jesus apareceu
      F#            Bm         E7           Bm
Perguntando o que fazer pra entrar no reino seu
      Em         Bm        F#          Bm
Jesus pede a caridade: o rapaz entristeceu

Bm                    Em        A                 D7+
Quem possui noventa e nove, só pensa em completar cem
      F#           Bm         E7           Bm
Nesta cegueira não sabe que depois a morte vem
    Em               Bm         F#                Bm
Seu corpo se vira em terra e na Terra deixa o que tem

Bm                 Em            A         D7+
Certo homem colheu tanto que seu armazém encheu
   F#               Bm       E7             Bm
Pensou que estava seguro: na mesma noite morreu
  Em             Bm          F#             Bm
Levaram só ele à cova: ficou tudo o que era seu

Bm               Em          A         D7+
Só confiar em dinheiro é loucura e vaidade
     F#         Bm        E7           Bm
Porque Cristo é vida, o caminho e a verdade
     Em                Bm           F#        Bm
Quem pensa o contrário disso, nunca terá liberdade

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7/5- = X 0 1 0 2 X
Bm = X 2 4 4 3 2
D7+ = X X 0 2 2 2
D7+/9 = X 5 4 6 5 X
E7 = 0 2 2 1 3 0
E7/B = X 2 2 1 3 X
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
