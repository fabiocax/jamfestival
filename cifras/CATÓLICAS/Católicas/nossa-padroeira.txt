Católicas - Nossa Padroeira

 E                                         B
Nossa Padroeira, é mãe da virgem e mãe de Deus
F#m           B              B7            E
Ela é nosso guia, ao Céu conduz os filhos teus
  E                          E7        A
Protegei Sant'Ana os nossos lares e lavoura
        Am          E              B         E     B
E defendei as criancinhas e a mocidade sofredora

        E                                                B
Nós queremos cantar os louvores de Sant'Ana, a mãe de Maria
        F#m             B           B7                  E
Ela é o nosso amparo e guia, nos protege ao céu nos conduz
        E                              E7                  A
Oh Sant'Ana guiai nossos passos, e enchei nossas almas de luz
         Am                E                       B             E
Para um dia entoarmos na glória com os anjos e santos a nossa vitória

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
