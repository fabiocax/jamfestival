Católicas - Cantai a Deus Com Alegria

   D        Bm        G    A7              D            G       A7
Cantai.........  ....... a Deus com alegri-...-a-...-a-....-a

Exultai.......  ......... em seu santuário-o-...-o.

Bm               F#m      G                A
....Povos todo, aplaudi ao Deus que nos criou
Bm                    F#m              G               A          G       A   A7
....Rejubilai em sua prese-ença cantando louvores ao Senhor-...-.O.-O!

     D        Bm        G    A7              D            G       A7
Dançai........  ...... a Deus com alegri-...-a-...-a-....-a

Exultai.........  ...... em seu santuário-o-...-o.

    D        Bm        G    A7              D            G       A7
Pulai.........  ...... a Deus com alegri-...-a-...-a-....-a

Exultai.......  ......... em seu santuário-o-...-o.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
