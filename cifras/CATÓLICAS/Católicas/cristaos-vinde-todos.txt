Católicas - Cristãos, Vinde Todos

F               C7
Cristãos, vinde todos
 F             C7
Com alegres cantos
 Dm         F
Oh!Vinde, oh!vinde
 Bb    C   F      Bb  F
Até Belém.Vede nascido
  Dm  F       C
Vosso Rei eterno

         F
Oh!Vinde e adoremos
        F
Oh!Vinde e adoremos
  Gm7       Bb       C7     F
Oh!Vinde e adoremos o Salvador

F           C7    F               C7
Humildes pastores deixam seu rebanho
     Dm    F        Bb   F   C
E alegres acorrem ao Rei do Céu

  F       Bb     Dm          C
Nós igualmente cheios de alegria

F
Oh!Vinde e adoremos
    F
Oh!Vinde e adoremos
 Gm7          Bb      F  C7 F
Oh!Vinde e adoremos o Salvador

  F        C7     F              C7
O Deus invisível, de eterna grandeza
    Dm           F     Bb   F      C
Sob véus de humildade, podemos ver
 F       Bb  F   Dm   F         C
Deus pequenino, Deus envolto em faixas

F
Oh!Vinde e adoremos
F
Oh!Vinde e adoremos
   Gm7          Bb     F C7 F
Oh!Vinde e adoremos o Salvador

F            C7         F           C7
Nasceu em pobreza, repousando em palhas
   Dm    F      Bb F    C
O nosso afeto lhe vamos dar
 F      Bb   F    Dm                 C
Tanto amou-nos! Quem não há de ama-lo?

F
Oh!Vinde e adoremos
F
Oh!Vinde e adoremos
   Gm7          Bb    F C7 F
Oh!Vinde e adoremos o Salvador

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm7 = 3 X 3 3 3 X
