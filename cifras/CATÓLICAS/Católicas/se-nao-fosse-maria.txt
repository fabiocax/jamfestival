Católicas - Se Não Fosse Maria

Introdução: F F/A Bb C7 F F/A Bb C7
       F                                C/E
Senhor, eu sei que não fui eu que te escolhi
 Dm                        Am
mas foste tu que assim escolheste
 Bb               F/A
ao aliviar meu fardo
                       G       C7/9           F    Bb/C
me dando Maria pra me conduzir.
       F                          C/E
E sei que tenho muito que agradecer
      Dm                      Am                       Bb
Se hoje a mãe que é sua é minha também
                         F/A              G
Aquela que um dia te educou
             C7        F      C7/9
hoje educa a mim

          F              Am             Bb              F/A
O que seria de mim se não fosse essa mãe que
           Gm         C7
me destes na cruz

         F                 Am              Bb         F/A
O que seria de mim, se não fosse Maria
        Gm                     C7
que me ensinou te amar,
        Gm                     C7                    F
que me ensinou te servir, Senhor Jesus.(2x)
         Gm                      C7                    Bb   F/A   Gm   F
que me ensinou te servir, Senhor Jesus.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bb/C = X 3 X 3 3 1
C/E = 0 3 2 0 1 0
C7 = X 3 2 3 1 X
C7/9 = X 3 2 3 3 X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
