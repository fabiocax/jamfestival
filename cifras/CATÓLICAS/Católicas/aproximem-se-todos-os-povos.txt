Católicas - Aproximem-se Todos Os Povos

              Am      E            Am                    C                    Am  E7
Aproximem-se todos os povos glória a Deus glória a Deus
         Am           E7             Am                   C                      G         G7
E de braços erguidos celebrem, glória a Deus, glória a Deus

                 C                                 G         Dm                                Am
Glória ao Pai que nos une no amor. A Jesus por nos dar salvação
             F             G              Am     (G-Am-G-Am)
e ao Espírito que é consolação

              Am      E            Am                    C                    Am  E7
Nas alturas louvores são dados, glória a Deus, glória a Deus
         Am           E7             Am                   C                      G         G7
Paz na terra aos homens amados, glória a Deus, glória a Deus

              Am      E            Am                    C                    Am  E7
Com a voz e instrumentos sonoros, glória a Deus, glória a Deus
         Am           E7             Am                   C                      G         G7
Digam todos unindo-se em coros, glória a Deus, glória a Deus

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
