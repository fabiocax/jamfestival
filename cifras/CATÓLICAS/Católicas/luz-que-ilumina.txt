Católicas - Luz Que Ilumina

    D         Bm        G          A7
1. Luz que ilumina os caminhos do amor
 D             Bm        G         A7
Luz que nos revela a mensagem do Senhor

 G     A7     D A/C# Bm   G     A7     G
Luz és Tu, Jesus.        Luz és Tu, Senhor

    D            Bm       G            A7
2. Luz que nos ensina a amar o nosso irmão
 D              Bm          G           A7
Luz que nos dá força nos momentos de aflição

----------------- Acordes -----------------
A/C# = X 4 X 2 5 5
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
G = 3 2 0 0 0 3
