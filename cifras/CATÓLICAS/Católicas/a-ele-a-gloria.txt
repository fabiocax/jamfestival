Católicas - A Ele a Glória

  E    G#m     C#m     C#m/B
Alfa, ômega, princípio e fim
            A  E/G#            F#m    B7
Sim ele é...         Sim ele é...
E    G#m       C#m     C#m/B
Alfa, ômega, princípio e fim
            A  E/G#             F#m    B7
Sim ele é...          Sim ele é...

E    B/D#    C#m C#m/B           A
      Lírio do vale,           estrela da manhã
        F#m             B7               E
Para sempre cantarei o seu louvor...
E    B/D#    C#m C#m/B            A                 B7
      Lírio do vale,            estrela da manhã...aaaa

              E  G#m      C#m     C#m/B
A ele a glória, a ele o louvor
                A       F#m
A ele o domínio
                 D      B4  B
Ele é o senhor

               E G#m       C#m    C#m/B
A ele a glória, a ele o louvor
                A        F#m
A ele o domínio
      B7      E
Ele é o senhor

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
B/D# = X 6 X 4 7 7
B4 = X 2 4 4 5 2
B7 = X 2 1 2 0 2
C#m = X 4 6 6 5 4
C#m/B = 7 X 6 6 5 X
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
