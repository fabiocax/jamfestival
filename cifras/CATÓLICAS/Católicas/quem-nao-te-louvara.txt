Católicas - Quem Não Te Louvará?

  C  G/B         Am (Am/G)       F        G    C (F/A, G/B)
Quem não Te louvará?     Oh, Senhor dos Exércitos?
  C  G/B         Am (Am/G)       F        G    C (F/A, G/B)
Quem não Te louvará?     Oh, Senhor dos Exércitos?
    C   (G/B)Am    Am/G  F      G      C (F/A, G/B)
1. Santo,    santo,      é o Senhor Jesus. (2x)

    C   (G/B)Am    Am/G  F      G      C (F/A, G/B)
2. Justo,    justo,      é o Senhor Jesus. (2x)

    C   (G/B)Am    Am/G  F      G      C (F/A, G/B)
3. Forte,    forte,      é o Senhor Jesus. (2x)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am/G = 3 X 2 2 1 X
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
