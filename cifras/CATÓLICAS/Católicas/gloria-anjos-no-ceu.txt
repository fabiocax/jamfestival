Católicas - Glória, Anjos no Céu

   D      Bm     F#m
Glória, glória, anjos no céu
 G       D           A7
Cantam, todos, seu amor
D      Bm    F#m
E, na terra, homens de paz
  G      A7        D
"Deus merece o louvor"

 F#                   Bm
Deus e Pai nós vos louvamos
E               A7
Adoramos, bendizemos
 G     Gm              D   Bm
Damos glória ao vosso nome
        E          A7
Vossos dons agradecemos

  D      Bm     F#m
Glória, glória, anjos no céu
 G       D           A7
Cantam, todos, seu amor

D      Bm    F#m
E, na terra, homens de paz
  G      A7        D
"Deus merece o louvor"

 F#                  Bm
Senhor nosso, Jesus Cristo
E             A7
Unigênito do Pai
 G       Gm             D    Bm
Vós, de Deus, Cordeiro Santo
        E           A7
Nossas culpas perdoai

  D      Bm     F#m
Glória, glória, anjos no céu
 G       D           A7
Cantam, todos, seu amor
D      Bm    F#m
E, na terra, homens de paz
  G      A7        D
"Deus merece o louvor"

 F#                       Bm
Vós, que estais junto do Pai
 E                  A7
Como nosso intercessor
G    Gm           D    Bm
Acolhei nossos pedidos
     E            A7
Atendei nosso clamor

  D      Bm     F#m
Glória, glória, anjos no céu
 G       D           A7
Cantam, todos, seu amor
D      Bm    F#m
E, na terra, homens de paz
 G      A7        D
"Deus merece o louvor"

 F#                   Bm
Vós, somente, sois o Santo
E                  A7
O Altíssimo, o Senhor
 G      Gm        D   Bm
Com o Espírito Divino
         E            A7
De Deus Pai no esplendor

  D      Bm     F#m
Glória, glória, anjos no céu
 G       D           A7
Cantam, todos, seu amor
D      Bm    F#m
E, na terra, homens de paz
  G      A7        D
"Deus merece o louvor"

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
