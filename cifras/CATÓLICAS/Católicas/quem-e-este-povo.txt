Católicas - Quem É Este Povo

                C               Dm
1. Quem é esse povo? Que povo é esse?
          G7                    C
Esse é o povo que vai morar no céu.
             C               Dm
Quem é esse povo? Que povo é esse?
          G7                    C     C7
Esse é o povo que vai morar no céu.

       F          G          C          Am         Dm         G7        C  C7
Vai morar, vai morar, vai morar, vai morar, vai morar, vai morar lá no céu.
       F          G          C          Am         Dm         G7        C
Vai morar, vai morar, vai morar, vai morar, vai morar, vai morar lá no céu.

              C             Dm
2. Como é gostoso sentir o gozo,
          G7                    C
Com esse povo que vai morar no céu.
           C             Dm
Como é gostoso sentir o gozo,
          G7                    C    C7
Com esse povo que vai morar no céu.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
