Católicas - Jesus Tu És o Pão Vindo do Céu

G         C   D            G
Jesus, tu és pão vindo do céu
C             D        Em
Que me dá forças pra viver
C               D            Em
Quem come da minha carne viverá eternamente, tu disseste
C           Am            D
E eu digo agora, já não posso viver sem ti.
G        C             D            G
Nada se compara a tua grandeza, ó senhor
 C               D              Em
Que mesmo sendo deus, te dás a mim ,
C           D               Em
És minha rocha, minha fortaleza,
         G      D                 G   G7
Em tuas mãos deposito todo o meu ser

        C                  D           Em
O teu corpo e sangue são sinais do teu amor
    C           G            Am           D   D#º    G
Venho me alimentar de ti, contigo quero estar pra sempre

 C              D    B7      Em
E unido a ti senhor, quero permanecer,
       G           D      C      G     D
Eu te amo, ó meu deus, te amo ó deus.

 G           C            D               G
E agora que sinto tua presença dentro em mim ,
C             D             Em
Fico sem palavras diante de tanto poder
      C           D       Em
Sinto tua voz me falar, como uma brisa leve a passar,
C      Am                D
Nada digo agora, apenas deixo me embalar
G     C            D            G
Obrigado por me amares tanto assim ,
 C                 D           Em
És um deus maravilhoso, não há outro igual
      C      D      Em
Bendito seja, para sempre glorificado,
C   G      D           G      G7
Em ti eu encontro paz real

        C                  D           Em
O teu corpo e sangue são sinais do teu amor
    C           G            Am           D   D#º    G
Venho me alimentar de ti, contigo quero estar pra sempre
 C              D    B7      Em
E unido a ti senhor, quero permanecer,
       G           D      C      G     D  G
Eu te amo, ó meu deus, te amo ó deus.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D#º = X X 1 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
