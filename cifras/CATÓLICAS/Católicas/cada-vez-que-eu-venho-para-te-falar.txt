Católicas - Cada Vez Que Eu Venho Para Te Falar

D                     G          A7
Cada vez que eu venho para te falar
D                Bm G    A      D
Na verdade eu venho para te escutar

  D        F#m       G         D
Fala-me da vida, preciso te escutar
  D         A7        G     A     D
Fala-me a verdade, que vai me libertar

D                     G          A7
Cada vez que eu venho para oferecer
D                Bm G    A      D
Na verdade eu venho para receber

  D            F#m       G           D
Dá-me o pão da vida que vai me alimentar
  D          A7        G     A     D
Dá-me a água viva que vai me saciar

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
