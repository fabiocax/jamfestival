Católicas - Santo Antônio Terno Amigo

C                   G           C
Santo Antônio terno amigo de Jesus
        Em              Dm
Pai querido santo protetor
     F                   C
Amparai-nos na vida e na morte
Am      F       G       C    C7
Pelo amor de nosso redentor

           F                       C
Aos vossos pés vos pedimos e suplicamos
        G     G7      C     C7
Uma graça de vosso poder
       F                C     Am
Pelo amor de Jesus socorrei-nos
        F       G             C   G
E aliviai nossa dor nosso sofrer

        C           G           C
Das misérias deste mundo afastai
        Em                      Dm
Os devotos que vos querem tanto bem

       F                     C   Am
Nesta vida guiai-nos por piedade
          F         G        C    C7
Ó grande santo para o céu também

           F                       C
Aos vossos pés vos pedimos e suplicamos
        G     G7      C     C7
Uma graça de vosso poder
       F                C     Am
Pelo amor de Jesus socorrei-nos
        F       G             C
E aliviai nossa dor nosso sofrer

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
