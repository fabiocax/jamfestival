Católicas - Credo

Am                            F
Creio em Deus pai, todo poderoso
C                 G
Criador do céu e da terra
Am
Creio em Jesus cristo
F                  C      G
Seu único filho, nosso senhor

Dm                 Am
Que foi concebido pelo poder
F    E7       A7
Do espírito santo
Dm               Am
E nasceu da santa virgem maria
F                       E       E7
Padeceu sob o poder  de poncio pilatos

Am               F
Foi crucificado, morto e sepultado
C        G
E desceu à mansão dos mortos

Am             F
E ao terceiro dia ressuscitou
C               G
Ressuscitou dos mortos

Dm             Am
E subiu ao céu,e esta sentado
F     E7         A7
À direita de deu pai
Dm          Am
De onde há de vir
F             E        E7
Julgar os vivos e os mortos

Am                  F
Creio no espírito, espírito santo
C                      G
E na santa igreja católica
      Am
Na comunhão dos santos
      F
No perdão dos pecados
C                     G
E na ressurreição dos mortos
Dm        E       Am
E na ida eterna, amém
Dm        E       Am
E na vida eterna, amém
F            E
Agora e mais que nunca
F            E         Am  F
Agora e mais que nunca amém

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
