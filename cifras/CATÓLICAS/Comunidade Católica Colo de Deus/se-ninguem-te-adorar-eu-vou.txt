Comunidade Católica Colo de Deus - Se Ninguém Te Adorar, Eu Vou

[Intro] D9  G9  D9  G9  A9

 D9                     G
Teu fogo é força que consome
 D9              G      A9
Que incômoda o meu coração
 D9                       G
Teu fogo é tudo que eu preciso
          D9                  G
Não vou perder, não posso perder

 D9                 G
Tua verdade me desvenda
  D9                   G      A
E arranca tudo que é mal em mim
  D9                       G
Tua glória é tudo que eu preciso
          D9                  G
Não vou perder, Não posso perder

     G                    D
Uoo ooo se ninguém te adorar eu vou

       D
Uoo ooo o meu coração você roubou
      G                          A
Uoo ooo não vou mais viver das minhas mentiras
    D                         G
Não vou perder, Não posso perder

     G        D
Uooou uooou uooou
     G        D
Uooou uooou uooou

         D
Vou erguer um altar de adoração a Ti
         D
Vou erguer um altar de adoração aqui
      A                          G
Até que a minha mão se una a Tua mão
                              D
E o meu coração seja um com o Teu

       A
Não há nada que eu queira mais
        G                           D
Não há nada que eu seja mais, eu sou Teu
     A
Até que a minha mão se una a Tua mão
      G                       D
E o meu coração seja um com o Teu

       A
Não há nada que eu queira mais
        G                            D
Não há nada que eu seja mais, eu sou Teu

     G                    D
Uoo ooo se ninguém te adorar eu vou
       D
Uoo ooo o meu coração você roubou
      G                          A
Uoo ooo não vou mais viver das minhas mentiras
            D9                G
Não vou perder, Não posso perder

    G          D
Uoooou uooou uooou
    G          D
Uoooou uooou uooou

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
D = X X 0 2 3 2
D9 = X X 0 2 3 0
G = 3 2 0 0 0 3
G9 = 3 X 0 2 0 X
