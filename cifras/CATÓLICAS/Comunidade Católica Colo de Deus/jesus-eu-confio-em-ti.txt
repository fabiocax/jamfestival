Comunidade Católica Colo de Deus - Jesus Eu Confio Em Ti

E                    A9
Teu amor enche o meu coração
                   E      B/C#
Maravilhoso e poderoso és
                         A9        F#m11
Não há ninguém que se compare a Ti
         E
Digno Tu és
              A9
Toda criação exalta a Ti
               E        B/C#
és o Único Senhor e Rei
                 A9     F#m11
Venha ser o meu Emanuel
         E
Santo Tu és

  A9  F#m11             C#m  B9
Jesus       eu confio em Ti
  A9  F#m11                        C#m  B9
Jesus       minha esperança está em Ti

             A9
Mais do que Ter eu quero Te conhecer
              E9
Mais do que o mundo pode me oferecer
          A9
Só a Tua graça me basta
             E9
Só a Tua graça

 A9                    F#m11        C#m                B9
Tudo que eu desejo é Te contemplar, quero em teu colo descansar
 A9                    F#m11        C#m                B9
Tudo que eu desejo é Te contemplar, Vou me entregar

----------------- Acordes -----------------
A9 = X 0 2 2 0 0
B/C# = X 6 X 4 7 7
B9 = X 2 4 4 2 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
E9 = 0 2 4 1 0 0
F#m11 = 2 2 4 2 2 2
