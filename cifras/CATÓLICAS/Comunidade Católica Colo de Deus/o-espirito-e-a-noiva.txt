Comunidade Católica Colo de Deus - O Espírito e a Noiva

[Intro] C#m  A9  E

C#m         A9                      E
 Abre os meus olhos, eu quero te ver
           E                     C#m
Os meus ouvidos, eu quero te ouvir
           A9            E
Abre o meu coração senhor
           C#m
Vem me mostrar

      A9            E      B4
O que eu não posso enxergar
      C#m            A9
Em verdade, eu quero ver
     E                 B4
Em verdade, eu quero ouvir
      C#m             A9            E
Em verdade, eu quero ver, eu quero ouvir
        B4
Vem me mostrar!


     C#m        A9           E   B4
O espírito e a noiva dizem, vem!
     C#m        A9           E   B4
O espirito e a noiva dizem, vem

----------------- Acordes -----------------
A9 = X 0 2 2 0 0
B4 = X 2 4 4 5 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
