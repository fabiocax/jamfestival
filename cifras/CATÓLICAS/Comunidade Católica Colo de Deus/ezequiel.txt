Comunidade Católica Colo de Deus - Ezequiel

[Intro] G  Em  Bm  A/C#

G            Em
Ossos se juntem
Bm             A/C#
Músculos se formem
G        Em
Pele a cubra
Bm      A/C#
Noiva levanta
                  G     Em
Que haja vida em nós/
                 Bm   A/C#
Que haja vida em nós

G            Em
Ossos se juntem
Bm              A/C#
Músculos se formem
G        Em
Pele a cubra

Bm      A/C#
Noiva levanta
                 G                      Em
Que haja vida em nós/  Que haja vida em nós
                 Bm                     A/C#
Que haja vida em nós / Que haja vida em nós

[Solo] G  Em  Bm  A/C#
       G

D
Como um renovo vem
A/C#               Em   D/F#   G
Sobre o povo e restaura A Tua igreja
D
Como um fogo vem
A/C#            Em  D/F#   G
E queima outra vez A Tua noiva

                D
Que haja fogo em nós
               A/C#
Que haja fogo em nós
                Em
Que haja fogo em nós
                G
Que haja fogo em nós

                  D    A/C#
Que haja fogo em nóooooooos
                  Em      G
Que haja fogo em nóooooooos

[Solo] D  A/C#  Em  G
       D  A/C#  Em  G

Que haja fogo em nós

----------------- Acordes -----------------
A/C# = X 4 X 2 5 5
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
