﻿Comunidade Católica Colo de Deus - Eterno El Shaday

Capo Casa 1

Intro: G C9 Em7 D

G            C9              Em7                D
Nanananananana...nanananananana...nananananananana...

Solo: G C9 Em7 D

G             C9            Em7
Quero adorar-te, e exaltar-te
               D
E te dizer que és meus Deus
G             C9
Em tua presença, na alegria
               D
No coração do meu Senhor

  Am                     G9
Eterno rei dos reis, bendito, el shaday
     C9           D
Teu nome eu adorarei
  Am
Jesus tu és meu rei

   G9
Dependo só de Ti
          C9               D              G
Em teus braços me abandonarei, pra te adorar

G            C9              Em7                D
Nanananananana...nanananananana...nananananananana... pra te adorar

(repete tudo)

      Am                 Em7
És bendito, Pai, meu lindo Jesus
   C9                 D
Espírito de amor sem fim
        Am                    Em7
Tens a liberdade, toma o meu coração
    C9                D
Cantamos para te exaltar (2x) e te adorar

G            C9              Em7                D
Nanananananana...nanananananana...nananananananana...

----------------- Acordes -----------------
Capotraste na 1ª casa
Am*  = X 0 2 2 1 0 - (*A#m na forma de Am)
C9*  = X 3 5 5 3 3 - (*C#9 na forma de C9)
D*  = X X 0 2 3 2 - (*D# na forma de D)
Em7*  = 0 2 2 0 3 0 - (*Fm7 na forma de Em7)
G*  = 3 2 0 0 0 3 - (*G# na forma de G)
G9*  = 3 X 0 2 0 X - (*G#9 na forma de G9)
