Comunidade Católica Colo de Deus - Santo Dos Santos

G        C   (C Dm Em)   G                 Am
Quero entrar no santo lugar, no santo dos santos
                 F             C
No meio do teu fogo eu quero estar
        G/B             Am           F
Eu vou deixar a minha oferta vou sacrificar
       G  Am    Am  G         F
Vou deixar morre meu eu, sou eu
     Am   G
Quem você quer
 F                                    Am
Constrangiu meu coração ao ver tua beleza
       G
Me rasguei
F                                    Am   G
E ao ver o teu olhar percebi que me ama

       F               Am
Vou ficar aqui , protegido aqui
      F                Am      G
Escondido aqui ,vou fica aqui


           F           Dm7
Meu desejo é  , meu desejo
          Am               G
Adentrar no lugar onde eu sei  que o senhor está

         Am         G
Cheguei aqui eu vi você dizer
         F       Dm
Cheguei aqui e vi você
         Am                G
Cheguei aqui e eu vi você me chamar de filho
     F
Abba pai

 F                 Dm
Tire as sandálias dos pés
          Am          G
Pois o lugar que estás
F     Dm       Am   G
É Santo, é Santo
 F             Dm       Am       G
Santo, Santo, Santo, é Santo

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
