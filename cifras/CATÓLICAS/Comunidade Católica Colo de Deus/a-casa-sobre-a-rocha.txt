Comunidade Católica Colo de Deus - A Casa Sobre a Rocha

Intro:  C   G   Am    C   D

          C              G          Am              C     D
Vou construir no monte de Sião, sobre Tua Rocha, a minha casa

Solo:  C  G  Am    C   D

Em                    D              C                         Am   D
Tudo o que eu vivi até agora não se comparará à Glória da segunda casa
Em                           D           C              Am  D
eis o tempo que o Senhor me prometeu: restituição, restauração
    C                           G      D/F#
E tudo o que perdido foi, de novo virá.
   C                            G     D/F#
Dupla honra Ele prometeu, meu olho verá.
Eu creio!

Solo:  C   G  Am    C   D

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
