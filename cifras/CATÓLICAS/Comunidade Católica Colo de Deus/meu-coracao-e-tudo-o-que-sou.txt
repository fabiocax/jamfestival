Comunidade Católica Colo de Deus - Meu Coração e Tudo o Que Sou

E             B
Recebe agora o meu coração e tudo o que sou
C#m           A9
É teu agora, aquilo que por tanto tempo eu reti
E                             B                           C#m   A9
Meu medo não é de me doar, meu medo é de te ofender e me afastar

E
Minha juventude
B
Só encontra o sentido em ti
C#m                A                 B
A minha vida eu entendi quando te percebi

C#m                      B
Tua voz me pedindo algo mais
A               E/G            F#m
Me chamando, me atraindo, conquistando
B
E dizendo


E
É tudo o que tens
B
Une o teu coração ao meu
A                     F#m                B
Pão e vinho, corpo e sangue, alma e divindade
E                       B
É nesse altar que eu me dou a ti
C#m        A          B           E
Nada nesse mundo me atrai (sou todo teu)

E             B
Recebe agora o meu coração e tudo o que sou
C#m           A9
É teu agora, aquilo que por tanto tempo eu reti
E                             B                           C#m   A9
Meu medo não é de me doar, meu medo é de te ofender e me afastar

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
E/G = 3 2 2 1 0 0
F#m = 2 4 4 2 2 2
