Comunidade Católica Colo de Deus - Desesperado Pelo Teu Amor

D              D7+               Bm
  Quem é você amor, aonde você está
                  G
Me deixa te conhecer, amor perfeito, Deus

D                 D7+
  Caminhos me mostraram
                    Bm
Pessoas quiseram preencher
                 G
O vazio que tá aqui, não conseguiram
Estou desesperado

D                        D7+
  Estou desesperado pelo teu amor
         Bm
O teu amor
                        G
Estou desesperado pelo teu amor
              D
Pelo teu amor

----------------- Acordes -----------------
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D7+ = X X 0 2 2 2
G = 3 2 0 0 0 3
