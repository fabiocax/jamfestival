Comunidade Católica Colo de Deus - Me Deixa Queimar

[Intro]  G  Em7  Bm7  A

            G               Em7
Me deixa morar, na sala do trono
          Bm7                 A
Porque eu sei, que lá moram anjos
         G                Em7
São serafins, são anjos de fogo
            Bm7               A
Eu gosto do fogo, me deixa queimar
G               Em7                    Bm7
Não me deixa viver tão longe do teu fogo
                A                    G
Eu preciso de mais um pouco mais de fogo
                Em7                   Bm7
Eu só quero queimar no meio do teu fogo
    A
Uuuuuuuhhh
G       Em7
Senhor
    Bm7   A
Senhor

----------------- Acordes -----------------
A9 = X 0 2 2 0 0
Bm7 = X 2 4 2 3 2
Em7 = 0 2 2 0 3 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
