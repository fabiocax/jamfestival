﻿Comunidade Católica Colo de Deus - Cheiro de Rosas

Capo Casa 2

[Intro] Em  D  C9
        Em  D  C9

Em      D/F#        C9
Estás aqui, agraciada
 Em       D/F#      C9
Posso sentir teu perfume
Em       D/F#          C9
Estás aqui, mãe do meu Senhor
 Em        D/F#    C9
Vem ficar perto de mim

           G        D/F#
Cheiro de rosas nesse lugar
   Em      D/F#
Maria, aqui está
       Am        C       D/F#
E o Espirito de Deus descerá
           G          D
Cheiro de rosas nesse lugar
    Em      D/F#
A rainha presente está

      Am         C  D/F#  Em  D/F#  C9  Em  D/F#  C9
Pentecostes acontecerá

----------------- Acordes -----------------
Capotraste na 2ª casa
Am*  = X 0 2 2 1 0 - (*Bm na forma de Am)
C*  = X 3 2 0 1 0 - (*D na forma de C)
C9*  = X 3 5 5 3 3 - (*D9 na forma de C9)
D*  = X X 0 2 3 2 - (*E na forma de D)
D/F#*  = 2 X 0 2 3 2 - (*E/G# na forma de D/F#)
Em*  = 0 2 2 0 0 0 - (*F#m na forma de Em)
G*  = 3 2 0 0 0 3 - (*A na forma de G)
