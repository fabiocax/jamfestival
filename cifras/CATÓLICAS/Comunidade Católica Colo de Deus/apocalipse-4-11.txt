﻿Comunidade Católica Colo de Deus - Apocalipse 4-11

Capo Casa 3

[Intro] Bm  A  Em  G

Bm
Digno tu és
A
Digno tu és
   Em              G
Só tu és digno senhor

   Bm        A         Em           G
De receber a honra, a glória e majestade
   Bm        A         Em           G
De receber a honra, a glória e majestade

( Bm  A  Em  G )

Bm
Digno tu és
A
Digno tu és
   Em              G
Só tu és digno senhor


   Bm        A         Em           G
De receber a honra, a glória e majestade
   Bm        A         Em           G
De receber a honra, a glória e majestade
   Bm        A         Em           G
De receber a honra, a glória e majestade
   Bm        A         Em           G
De receber a honra, a glória e majestade

      Bm                         A                  Em
E não outro como tu, não há tão belo como tu, tua beleza me
       G
Conquistou

      Bm                         A                  Em
E não outro como tu, não há tão belo como tu, tua beleza me
       G
Conquistou

----------------- Acordes -----------------
Capotraste na 3ª casa
A*  = X 0 2 2 2 0 - (*C na forma de A)
Bm*  = X 2 4 4 3 2 - (*Dm na forma de Bm)
Em*  = 0 2 2 0 0 0 - (*Gm na forma de Em)
G*  = 3 2 0 0 0 3 - (*A# na forma de G)
