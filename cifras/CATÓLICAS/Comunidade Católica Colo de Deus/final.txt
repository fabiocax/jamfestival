Comunidade Católica Colo de Deus - Final

E
Sobe as escadas do meu coração
C#m
Eu estou aqui, onde Você me deixou
E
Não consigo mais ir até Você
C#m
Então só me resta te dizer

          A9
Onde eu estou?
       C#m  B
Onde eu estou?
          A9
Onde eu estou?
       C#m  B
Onde eu estou?

    E/G#   A
Eu estou aqui, tão longe de Ti
            C#m                      B
Desesperado pelo Teu olhar que eu perdi

        E/G#    A
Não sei onde Te procurar, por isso eu vou gritar
         C#m            B
Olha pra mim! Olha pra mim!

A                                         E
Deixa eu reconstruir a minha história em Ti

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
