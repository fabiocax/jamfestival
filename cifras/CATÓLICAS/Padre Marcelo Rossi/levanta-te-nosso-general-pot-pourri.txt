Padre Marcelo Rossi - Levanta-te / Nosso General (pot-pourri)

Am              G          Am    F    G
    Levanta-te, levanta-te Senhor!
Am             F    G     Am
Levanta-te, levanta-te Senhor!
Am             G          Am    F    G
Levanta-te, levanta-te Senhor!
  Am           F    G     Am
Levanta-te, levanta-te Senhor!

Dm                  G      Am
Saiam diante de Ti Teus inimigos
      Dm
Se dispersem diante de Ti
E7                                 Am    F/A  Am  F/A
Todos aqueles que aborrecem Tua presença

Am             G          Am    F    G
Levanta-te, levanta-te Senhor!
  Am           C    G     Am
Levanta-te, levanta-te Senhor!
Am             G          Am    F    G
Levanta-te, levanta-te Senhor!

  Am           C    G     Am
Levanta-te, levanta-te Senhor!

Dm                  G      Am           Dm
Saiam diante de Ti Teus inimigos se dispersem diante de Ti
E7                  E/G#           Am  F/A  G/A  Am  F/A  G/A
Todos aqueles que aborrecem Tua presença

       Dm7           G            Am           Dm7
Tua presença reinará sobre todo império Tua presença reinará
  E7           E7/G#           Am   F/A  G/A  Am  F/A  G/A  Am  F/A  G/A
Governará sobre todos os principados

  Am         G    Am    F    G      Am         C    Am   F    G
Espírito de te....mor,  saia!     Espírito de maldição,  saia!
  Am         G    Am    F    G      Am        C    E7
Espírito de ran...cor,  saia!     Espírito de divisão, saia!
  Am           G   Am    F    G     Am        C    Am    F    G
Espírito de enfermidade, saia!    Espírito de rebelião,  saia!
  Am          G    Am    F    G     Am         C   E7
Espírito de imoralidade, saia!    Espírito de confusão, saia!

       Dm7           G            Am           Dm7
Tua presença reinará sobre todo império Tua presença reinará
  E7           E7/G#           Am    F/A  G/A  Am  F/A  G/A
Governará sobre todos os principados

  Am            G  Am    F    G      Am        C   Am    F    G
Espírito de perversão,   saia!     Espírito de ambição,  saia!
  Am             G  Am   F    G      Am        C  E7
Espírito de profanador,  saia!     Espírito de vaidade, saia!
  Am           G   Am    F    G      Am         C      Am   F    G
Espírito de murmuração,  saia!     Espírito de conversação, saia!
  Am          G    Am    F    G     Am         C   E7
Espírito de imoralidade, saia!    Espírito de confusão, saia!

       Dm7           G            Am           Dm7
Tua presença reinará sobre todo império Tua presença reinará
  E7           E7/G#           Am    F/A  G/A  Am  F/A  G/A
Governará sobre todos os principados

        Am                       F7M/A
Pelo Senhor marchamos, sim! O seu exército poderoso é.
    Dm7          E      E/G#    Am
Sua glória será vista em toda   Terra
         Am                               F7M
Vamos cantar o canto da vitória glória a Deus! Vencemos a batalha
    Dm7          E    E/G#   Am
Toda arma contra nós pere..cerá

     F               C         F             C
E o nosso general é Cristo, seguimos os seus passos
   F           Dm9   Em7   F   G   Am
Nenhum inimigo nos   re...sis..ti..rá
     F               C         F             C
E o nosso general é Cristo, seguimos os seus passos.
   F           Dm9   Em7   F   G   Am
Nenhum inimigo nos   re...sis..ti..rá

         Am                        F7M/A
Com o Messias marchamos, sim, em sua mão, a chave da vitória
        Dm7               E    E/G#    Am
Que nos leva a possuir a terra pro...metida
         Am                               F7M
Vamos cantar o canto da vitória glória a Deus! Vencemos a batalha
    Dm7          E    E/G#   Am
Toda arma contra nós pere..cerá

     F               C         F             C
E o nosso general é Cristo, seguimos os seus passos
   F           Dm9   Em7   F   G   Am
Nenhum inimigo nos   re...sis..ti..rá
     F               C         F             C
E o nosso general é Cristo, seguimos os seus passos
   F           Dm9   Em7   F   G   Am
Nenhum inimigo nos   re...sis..ti..rá

    F           G         Am
Nenhum inimigo nos resistirá!

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Dm9 = X 5 7 9 6 5
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E7 = 0 2 2 1 3 0
E7/G# = 4 X 2 4 3 X
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
F7M = 1 X 2 2 1 X
F7M/A = 5 X 3 5 5 X
G = 3 2 0 0 0 3
G/A = 5 X 5 4 3 X
