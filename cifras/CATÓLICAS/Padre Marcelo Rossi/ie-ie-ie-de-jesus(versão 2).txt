Padre Marcelo Rossi - Iê, Iê, Iê de Jesus

[Intro] D  G  A  G

D            G           D           G
Iê, Iê, Iê, Iê, Iê, Iê, Iê, Iê, Iê, Iê, Iê, Iê
 D           G              D           G
Uou, Ou, Ou, Uou, Ou, Uou, Uou, Ou, Ou, Uou, Ou, Uou

 D      G   D    G           D  G     D   G   D
Reza, Reza, Reza. Nós Rezaremos
 D      G         D  G           D   G     D   G   D
Louva, Louva, Louva, Nós Louvaremos

D              G          D            G
Iê, Iê, Iê, Iê, Iê, Iê, Iê, Iê, Iê, Iê, Iê, Iê
 D           G              D           G
Uou, Ou, Ou, Uou, Ou, Uou, Uou, Ou, Ou, Uou, Ou, Uou

D         G   D     G           D   G     D   G   D
Corre, Corre, Corre, Nós Correremos
D        G   D   G           D  G     D   G   D
Pula, Pula, Pula, Nós Pularemos


D              G           D           G
Iê, Iê, Iê, Lê, Iê, Iê, Iê, Iê, Iê, Lê, Iê, Iê
 D           G              D           G
Uou, Ou, Ou, Uou, Ou, Uou, Uou, Ou, Ou, Uou, Ou, Uou

D        G   D   G           D  G     D   G   D
Pula, Pula, Pula, Nós Pularemos
D        G   D      G           D   G    D   G   D
Grita, Grita, Grita, Nós Gritaremos

D              G           D           G
Iê, Iê, Iê, Lê, Iê, Iê, Iê, Iê, Iê, Lê, Iê, Iê
 D           G              D           G            D
Uou, Ou, Ou, Uou, Ou, Uou, Uou, Ou, Ou, Uou, Ou, Uou

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
