Padre Marcelo Rossi - Rio de Águas Vivas

Intro: Bb  F  Gm  D#  F

   Bb                F           Gm               D#
Eu quero em mim um rio de águas vivas
         D#m                                     Bb
Que possa jorrar e abençoar as vidas
F                                    D#             F
Por onde eu passar, as águas jorrarão
       Bb                       Bb7                D#
Curarão e libertarão, lavarão as feridas
            Bb
E o Espírito se moverá
               D#                  F                      Bb  (F)
E o vento de Deus soprará trazendo vitória (2x)

              Bb                                Gm
Jorra em mim, águas do rio de Deus
                     D#             Gm        F
Vem transbordar todo o meu coração
   Bb                             Gm
Quero nascer da Tua fonte, Senhor

        D#            Gm          D#            Gm
E nadar no Teu rio, e nadar no Teu rio
        D#             F           Bb
E nadar no Teu rio de amor

Solo:   Bb     F      Gm      F     E   F#

   B                F#         G#m              E
Eu quero em mim um rio de águas vivas
         Em                                     B
Que possa jorrar e abençoar as vidas
F#                                    E              F#
Por onde eu passar, as águas jorrarão
        B                         B7                E
Curarão e libertarão, lavarão as feridas
            B
E o Espírito se moverá
               E                  F#                     B   (F#)
E o vento de Deus soprará trazendo vitória

              B                                  G#m
Jorra em mim, águas do rio de Deus
                     E             G#m        F#
Vem transbordar todo o meu coração
   B                             G#m
Quero nascer da Tua fonte, Senhor
        E             B              F#
E nadar no Teu rio de amor

              B                                  G#m
Jorra em mim, águas do rio de Deus
                     E             G#m        F#
Vem transbordar todo o meu coração
   B                             G#m
Quero nascer da Tua fonte, Senhor
        E            G#m          E            G#m
E nadar no Teu rio, e nadar no Teu rio
        E             F#           B  (F#   G#m  F#  E)
E nadar no Teu rio de amor
        E             F#                  B
E nadar no Teu rio...  de amor

----------------- Acordes -----------------
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
Bb = X 1 3 3 3 1
Bb7 = X 1 3 1 3 1
D# = X 6 5 3 4 3
D#m = X X 1 3 4 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
Gm = 3 5 5 3 3 3
