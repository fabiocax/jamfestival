Padre Marcelo Rossi - Podes Reinar

[Intro] G  D  C  G  D  G

    G                           D
Senhor eu sei que é teu este lugar
                    C                 G  D7
Todos querem te adorar toma tu a direção
          G                          D
Sim, oh, vem, ó santo espirito os espaços preencher
     C           Am7        D    D7
Reverência a tua voz vamos fazer

          G      G7    C     Cm
Podes reinar senhor jesus ó sim
   G            Em        Am  D7
O teu poder teu povo sentirá
            G     G7        C             Cm
Que bom senhor saber que estás presente aqui
 G        D           C G/B Am G  D7
Reina senhor neste lugar

   G                        D
Visita cada irmão, ó meu senhor!

                  C                      G  D7
Dá-lhes paz interior e razões pra te louvar
    G                      D
Desfaz toda tristeza incertezas e desamor
      C                        D  D7
Glorifica o teu nome ó meu senhor

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G7 = 3 5 3 4 3 3
