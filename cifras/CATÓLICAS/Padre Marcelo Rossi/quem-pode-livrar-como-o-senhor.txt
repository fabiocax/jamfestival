Padre Marcelo Rossi - Quem Pode Livrar Como o Senhor

G            D/F#           Em
Quem pode livrar como o Senhor?

( C/D )

G          Bm              C
Ele é poderoso pra me guardar

( C/D )

G            D/F#           Em
Quem pode livrar como o Senhor?

( C/D )

G          Bm              C
Ele é poderoso pra me guardar

( D  B/D# )

Em         D       A/C#
Quando os meus inimigos


( A  A7  A6 )

Em       D/F#         A
se levantaram contra mim

( A  Bm )

C     D        G  Bm/F#
O Senhor estendeu sua
  Em   D    C        D       G
Mão para mim e me deu a vitória

( Dm/A  Bm )

C    C/E  B/F#  Em  B/F#
O Senhor  estendeu  sua
 G/D G/B   C        D       G
Mão para mim e me deu a vitória

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A6 = 5 X 4 6 5 X
A7 = X 0 2 0 2 0
B/D# = X 6 X 4 7 7
B/F# = X X 4 4 4 2
Bm = X 2 4 4 3 2
Bm/F# = X X 4 4 3 2
C = X 3 2 0 1 0
C/D = X X 0 0 1 0
C/E = 0 3 2 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Dm/A = X 0 X 2 3 1
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G/D = X 5 5 4 3 X
