Padre Marcelo Rossi - Misericórdia Senhor

D      A/C#   Bm     Bm/A   G      Em    A
Misericórdia, misericórdia, misericórdia Senhor (2x)
G    A     F#m Bm  Em        A       Am7  D D7
Senhor, pieda..de, tende piedade de nós
G    A     F#m Bm  Em        A       D  A
Senhor, pieda..de, tende piedade de nós

D      A/C#   Bm     Bm/A   G      Em    A
Misericórdia, misericórdia, misericórdia Senhor (2x)
G    A     F#m Bm  Em        A       Am7  D D7
Cristo, pieda..de, tende piedade de nós
G    A     F#m Bm  Em        A       D  A
Cristo, pieda..de, tende piedade de nós

D      A/C#   Bm     Bm/A   G      Em    A
Misericórdia, misericórdia, misericórdia senhor (2x)

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
Am7 = X 0 2 0 1 0
Bm = X 2 4 4 3 2
Bm/A = X 0 4 4 3 X
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
