Padre Marcelo Rossi - Sacramento da Cura

Intro: Bm A G Em F# F#7

Bm
ME ENSINOU A AMAR
A
ME CHAMOU DE AMIGO
G
MINHA FÉ NASCEU
Em              F#7
ENTÃO FIQUEI AQUI

Bm
ANDOU SOBRE O MAR
A
ME LIVROU DO PERIGO
G
CUROU BARTIMEU
Em                    F#7   Bm A
MUITOS MILAGRES EU VI

               G
É MISTÉRIO DE AMOR

                Em
O PAI ETERNO ENVIOU
   F#7                  E   F#
O SACRAMENTO QUE CURA O CORAÇÃO
Bm                 A  F#
NO ALTAR ESTÁ O MEU SENHOR
G                   A
VOU COMUNGAR O SEU AMOR
D
SEU MISTÉRIO EM MIM
G             F#
NO VINHO E NO PÃO
Em                        F#
É O QUE FAZ MOVER A SUA MÃO

Bm
ME ENSINOU A AMAR
A
ME CHAMOU DE AMIGO
G
MINHA FÉ NASCEU
Em              F#7
ENTÃO FIQUEI AQUI

Bm
ANDOU SOBRE O MAR
A
ME LIVROU DO PERIGO
G
CUROU BARTIMEU
Em                    F#7   Bm A
MUITOS MILAGRES EU VI

               G
É MISTÉRIO DE AMOR
                Em
O PAI ETERNO ENVIOU
   F#7                  E   F#
O SACRAMENTO QUE CURA O CORAÇÃO
Bm                 A  F#
NO ALTAR ESTÁ O MEU SENHOR
G                   A
VOU COMUNGAR O SEU AMOR
D
SEU MISTÉRIO EM MIM
G             F#
NO VINHO E NO PÃO
Em                        F#
É O QUE FAZ MOVER A SUA MÃO

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
F#7 = 2 4 2 3 2 2
G = 3 2 0 0 0 3
