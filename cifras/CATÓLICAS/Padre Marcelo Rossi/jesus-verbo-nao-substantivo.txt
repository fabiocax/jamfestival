﻿Padre Marcelo Rossi - Jesus Verbo, Não Substantivo

Capo Casa 4

Intro Falada:
Ontem,Jesus afinou minha guitarra e ressuscitou meus sentidos, me inspirou.
Papel e lápis na mão, comecei a pensar na canção que iria Escrever,
Porque falar e escrever sobre Jesus é redundante, seria melhor fazê-lo, é claro!
Logo, algo me diz que a única forma de não me exceder, é dizer a verdade. Dizer que pra Jesus o que importa é o que fazemos, não o que falamos. Dizer que Jesus é mais do que cinco letras, formando um nome; dizer que Jesus é Verbo e não substantivo.

Introdução: E  B  Am  A  B  E  A  B  E  G#m  C#m

   E                   B                        C#m
Jesus é mais que uma simples, que uma humana teoria
       A                     B           E
O que faz meu irmão lendo a bíblia todo dia?
      A                       G       E
Se o que está em cristo se resume em amor
     G#m            C#m
É melhor traduzir assim
    A                   B                E
Jesus, meus irmãos, é Verbo, não substantivo

                               B                    C#m
Jesus e mais que um templo de luxo com tendência barroca

     A                     B                    E
Ele sabe que a riqueza da alma é o maior dos tesouros
    A                    B                 E       G#m     C#m
Se você quer saber a verdade procure a resposta no seu dia-dia
   A                   B                 E
Jesus, meus irmãos, é Verbo, não substantivo

   E                             B                    C#m
Jesus e mais que um grupo de senhoras de pesada consciência
      A               B               E
Que querem ir para o céu pagando penitências
   A                B                    E        G#m     C#m
Falando da vida alheia, ditando o que é certo, fazendo intrigas
   A                   B                 E
Jesus, meus irmãos, e Verbo, não substantivo


  E               B                   C#m
Jesus merece bem mais que palavras bonitas
     A                    B                    E
Ele sabe de tudo o que existe por trás das cortinas
   A                   B          E         G#m        C#m
Jesus e mais que uma imagem no altar, pra salvar os pecados
   A                   B                 E    G7
Jesus, meus irmãos, é verbo, não substantivo

   C                   G               Am
Jesus transformará em atos todos os sermões
     F                       G                 C
Ele sabe que o inferno está cheio de boas intenções
     F                   G                 C         Em      Am
Ele sabe das armas que matam que cravam espinhos em nome da Rosa
   F                   G                 C
Jesus, meus irmãos, é Verbo, não substantivo

                    G                        Am
Jesus não entende porque os poderosos lhe aplaudem
     F                  G                 C
Não sabe por que a esperança é amiga da fraude
  F                   G                  C        Em       Am
Será que Jesus bate palma pra quem fica rico explorando a fé?
    F                   G                C
Jesus, meus irmãos, é Verbo, não substantivo

Solo: C  G  Am  F  G  C  F  G  C  Em  Am  C7

     F                   C                             Dm
Senhores, não dividam a fé; as fronteiras são para os países
       Bb                 C                  F
Neste mundo há mais religiões que meninos felizes
   Bb                  C
Jesus se torne mais visível
                F         Am        Dm
E deixe que os cegos lhe toquem as mãos
   Bb                  C                F
Jesus, meus irmãos, é Verbo, não substantivo

  F                                C             Dm
Jesus, não me sinto vestido com o amor que te professo
    Bb                      C                          F
Eu tenho a consciência tranqüila, por isso, não me confesso
    Bb                C             F         Am         Dm
Rezando dois Padres Nossos, o assassino não revive seus mortos
   Bb                  C                 F
Jesus, meus irmãos, é Verbo, não substantivo

                          C                 Dm
Jesus não desça ainda na terra, fique no Paraíso
  Bb                            C                F
Porque, todos que pensaram como tu não acharam saída,
     Bb               C                 F       Am    Dm
Tentaram mudar esse mundo, mas foram vencidos pelos leões
   Bb                      C                                      F   Bb
Morreram com sorrisos nos lábios, porque foram Verbo, não Substantivo
    F
Weeeeh

----------------- Acordes -----------------
Capotraste na 4ª casa
A*  = X 0 2 2 2 0 - (*C# na forma de A)
Am*  = X 0 2 2 1 0 - (*C#m na forma de Am)
B*  = X 2 4 4 4 2 - (*D# na forma de B)
Bb*  = X 1 3 3 3 1 - (*D na forma de Bb)
C*  = X 3 2 0 1 0 - (*E na forma de C)
C#m*  = X 4 6 6 5 4 - (*Fm na forma de C#m)
C7*  = X 3 2 3 1 X - (*E7 na forma de C7)
Dm*  = X X 0 2 3 1 - (*F#m na forma de Dm)
E*  = 0 2 2 1 0 0 - (*G# na forma de E)
Em*  = 0 2 2 0 0 0 - (*G#m na forma de Em)
F*  = 1 3 3 2 1 1 - (*A na forma de F)
G*  = 3 2 0 0 0 3 - (*B na forma de G)
G#m*  = 4 6 6 4 4 4 - (*Cm na forma de G#m)
G7*  = 3 5 3 4 3 3 - (*B7 na forma de G7)
