Padre Marcelo Rossi - Noites Traiçoeiras

D                       F#m            G      A7          D
Deus esta aqui neste momento sua presença e real em meu viver
                            F#m                G            A7     D
Entregue sua vida e seus problemas  fale com Deus ele vai ajudar você
D A Bm                 G         A7                D
Ohohoh Deus te trouxe aqui para aliviar os seus sofrimentos
D A Bm                   G                  A7                         D
Ohohoh E ele o autor da fé do principio ao fim em todos os seus tormentos

D7          G              A7                     F#m
E ainda se vier noite traiçoeira se a cruz pesada for
                Bm                  Em               A7
Cristo estará contigo o mundo pode ate fazer você chorar mas Deus te
        D
Quer sorrindo

D                      F#m              G            A7     D
Seja qual for o seu problema fale com Deus ele vai ajudar você. Após a
           F#m                G                A7      D
Dor vem alegria  pois Deus e amor e não te deixará sofrer
D  A/C# Bm                 G            A7              D
Oh  oh  oh Deus te trouxe aqui para aliviar o seus sofrimentos

D  A/C# Bm                   G                  A7                          D
Oh  oh  oh E ele o autor da fé do principio ao fim em todos os seus tormentos

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
