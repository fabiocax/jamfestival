Padre Marcelo Rossi - Desde o Nascer Ao Pôr-do-sol

  A
Desde o nascer ao
C#m
Pôr-do-sol
  D         E       E7
Seja louvado o nome do

Senhor
 A
Desde o nascer ao
C#m
Pôr-do-sol
  D               F#m
Seja louvado o nome do
 E       A        E
Senhor  Jesus
D           C#m
Proclamai a todos os povos
D           C#m
A salvação que ele nos
Trouxe

D          C#m
Rendei-lhe hinos de glória
E louvor
D        F#m E A    E
A Jesus salvador
  A
Desde o nascer ao
C#m
Pôr-do-sol
  D         E       E7
Seja louvado o nome do

Senhor
 A
Desde o nascer ao
C#m
Pôr-do-sol
  D               F#m
Seja louvado o nome do
 E         A
Senhor  Jesus

----------------- Acordes -----------------
A = X 0 2 2 2 0
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
