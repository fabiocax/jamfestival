Padre Marcelo Rossi - Deus é Maior

Bm                     A/C#      D
Não, não há!          Não, não há!
               A                 Em           Bm
Não há mal, que me possa vencer
Bm                     A/C#      D
Não, não há!          Não, não há!
               A                 Em           Bm
Não há mal, que me possa vencer

                 G               A                   D     A/C#   Bm
Pois tudo posso Naquele que me   for...   ta...   lece
           G              A       Bm
Tudo posso em Jesus Cristo
                 G               A                   D     A/C#   Bm
Pois tudo posso Naquele que me   for...   ta...   lece
           G              A       Bm
Tudo posso em Jesus Cristo

       Bm                 A/C#            D
Nenhum problema,         nem meus pecados
         A            Em                 Bm
Poderão superar o Amor de Deus

       Bm                 A/C#            D
Nenhum problema,         nem meus pecados
         A            Em                 Bm
Poderão superar o Amor de Deus

                  G          A                  D      A/C#    Bm
Deus é maior que tudo que me  a......con.......tece
              G              A       Bm
Deus é grande Supremo Rei!
                  G          A                  D      A/C#    Bm
Deus é maior que tudo que me  a......con.......tece
              G              A       Bm
Deus é grande Supremo Rei!

        Bm           A/C#            D
Nem a tristeza           nem mesmo a dor
        A           Em                  Bm
Poderá superar o Amor de Deus
        Bm           A/C#            D
Nem a tristeza           nem mesmo a dor
        A           Em                  Bm
Poderá superar o Amor de Deus
                G
Deus é maior

        Bm             A/C#           D
Nem a vingança         nem mesmo o ódio
         A          Em                  Bm
Poderá superar o Amor de Deus
        Bm             A/C#           D
Nem a vingança         nem mesmo o ódio
         A          Em                  Bm
Poderá superar o Amor de Deus
                 G
Deus é maior

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
