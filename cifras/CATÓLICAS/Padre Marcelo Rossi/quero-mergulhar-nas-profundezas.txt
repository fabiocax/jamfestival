Padre Marcelo Rossi - Quero Mergulhar Nas Profundezas

  E                     G#m  A
Quero mergulhar nas profundezas
                E
Do espírito de Deus
                  G#m  A
E descobrir suas riquezas
          E
Em meu coração

E      G#m A
É tão lindo
         E
Tão simples
       C#m                  A
Brisa leve, tão suave, doce espírito
          E
Santo de Deus
     C#m                    A
Tão suave, brisa leve, doce espírito
          E
Santo de Deus

----------------- Acordes -----------------
A = X 0 2 2 2 0
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
G#m = 4 6 6 4 4 4
