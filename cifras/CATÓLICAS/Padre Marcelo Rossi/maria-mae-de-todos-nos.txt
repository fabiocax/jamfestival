Padre Marcelo Rossi - Maria, Mãe de Todos Nós

INTRO: D  A  C  G  Gm/Bb  D  E7  A  D  D G A

G                     G  C  G                  G  C  G
No céu uma estrela guia      anunciou um novo dia      e uma
                                          D4   D  Am
luz então brilhou e um anjo anunciou que nasceria  do santo
                C            D                 G    Am
ventre de Maria  aquele que seria o verdadeiro amor  tanta
               C                                    G  Am
luz se fez verdade pureza e bondade da mais linda flor  santa
                 C                           G
mãe da esperança do anjo criança nosso salvador
D                                                  G
Maria dos desesperados, dos injustiçados dos sofredores
D                                                        G
Maria de todos os prantos, sonhos desencantos de tantas dores
C                  G             D             G
Maria de João e José, rainha da fé, rainha de luz
C                   G                D          G
Maria Mãe de todos nós bendita sois vós mãe de Jesus
              D          G                   D          G
bendita sois vós mãe de Jesus  bendita sois vós mãe de Jesus

A                     A  D  A                  A  D  A
No céu uma estrela guia      anunciou um novo dia      e uma
                                          E4   E  Bm
luz então brilhou e um anjo anunciou que nasceria  do santo
                D            E                 A    Bm
ventre de Maria  aquele que seria o verdadeiro amor  tanta
               D                                    A  Bm
luz se fez verdade pureza e bondade da mais linda flor  santa
                 D                           A
mãe da esperança do anjo criança nosso salvador
E                                                  A
Maria dos desesperados, dos injustiçados dos sofredores
E                                                        A
Maria de todos os prantos, sonhos desencantos de tantas dores
D                  A             E             A
Maria de João e José, rainha da fé, rainha de luz
D                   A                E          A
Maria Mãe de todos nós bendita sois vós mãe de Jesus
              E          A                   E          A
bendita sois vós mãe de Jesus  bendita sois vós mãe de Jesus
E        A
 Mãe de Jesus

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D4 = X X 0 2 3 3
E = 0 2 2 1 0 0
E4 = 0 2 2 2 0 0
E7 = 0 2 2 1 3 0
G = 3 2 0 0 0 3
Gm/Bb = 6 5 5 3 X X
