Padre Marcelo Rossi - Em Troca de Paz

Participação: Bruno e Marrone

      G
De repente uma lágrima cai
                  Em
são dos olhos do pai
            Am
chorando de dor
                             D
deve estar com vergonha da gente
                 Am
e com a força da mente
C          G
implora o amor

                            G
ve seu filho jogado na esquina
                Em            Am
e no beco a chassina doido demais
         C               G
ja não basta seu filho jesus

                 Am
que morreu numa cruz
D            G   C D7 D
em troca da paz

REFRÃO
        G        B7
Onde estão as lições
                     Em
que nasceram com a gente
    C               G
o amor acabou de repente
       Am                D  D7
é uma vida, é uma alma sem luz
       G        B7                 Em
onde está o calor de uma mão que afaga
      C                    G
é uma chama que nunca se apaga
          Am        D       G
quando encontra o amor de jesus(bis)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
