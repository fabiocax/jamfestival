Anjos de Resgate - Santo Espírito

[Intro]  Dm  Bb  F  C
         Dm  Bb  F  C

F                          C/E   Dm7                                  Bb
Quem sou eu para por impedimentos. Vem e faz tudo aquilo que queres fazer
F                                   C/E   Dm7                                    Bb
Ultrapassa os limites do meu entendimento, livre acesso te dou e assim vou me render

        F                      C/E
Santo Espírito de Deus vem com teu poder agir em mim
      Bb       F/A           Gm7  C
Quero experimentar da tua bondade
      F                      C/E
Faz arder meu coração, vem fazer uma renovação
        Bb          F/A            Gm7  C            F      F4  F  F4  F
Sobre a minha vida, minha história vem  com a tua unção

F                          C/E   Dm7                                  Bb
Quem sou eu para por impedimentos. Vem e faz tudo aquilo que queres fazer
F                                   C/E   Dm7                                    Bb
Ultrapassa os limites do meu entendimento, livre acesso te dou e assim vou me render


        F                      C/E
Santo Espírito de Deus vem com teu poder agir em mim
      Bb       F/A           Gm7  C
Quero experimentar da tua bondade
      F                      C/E
Faz arder meu coração, vem fazer uma renovação
        Bb          F/A            Gm7  C            F      Gm7
Sobre a minha vida, minha história vem  com a tua unção

      F/A       Bb    C  Gm7
Vem espírito restaurador
      F/A    Bb       C    Gm7
Vem espírito santificador
      F/A          Bb              C        Gm7          F/A    Bb     C
Vem espírito da verdade, que eu te dou liberdade, faça a tua vontade

      Gm7        F/A
Vem espírito habitar em mim
      Bb             C
Vem espírito, vem me conduzir
      Gm7           F/A       Bb     C
Vem espírito, vem espírito de Deus

        F                      C/E
Santo Espírito de Deus vem com teu poder agir em mim
      Bb       F/A           Gm7  C
Quero experimentar da tua bondade
      F                      C/E
Faz arder meu coração, vem fazer uma renovação
        Bb          F/A            Gm7  C            Bb   F/A   Gm7  C             Bb   F/A   Gm7   C
Sobre a minha vida, minha história vem  com a tua unção               com a tua unção

[Final]  Dm  Bb  F  C  Dm  Bb  F  C  Bb

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
F4 = 1 3 3 3 1 1
Gm7 = 3 X 3 3 3 X
