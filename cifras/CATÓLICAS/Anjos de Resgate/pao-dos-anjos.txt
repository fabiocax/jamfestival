Anjos de Resgate - Pão Dos Anjos

[Intro]  G  D/F#  Em7  Bb  C/B  G  D/F#  Em7  C/G  D/G  C/G

E|-------------------------------------------------------------------------------------|
B|-------------------------------------------------------------------------------------|
G|-------12--14b-(15)14b(15)~~-14brp12-14-14brp12-14--12-14-12-------12-14-14brp12-15b-|
D|-12-14--------------------------------------------14----------12h14------------------|
A|-------------------------------------------------------------------------------------|
E|-------------------------------------------------------------------------------------|

E|------------------------------------15-17-17b-17b-17brp15-17-|
B|--------15-17-17b-17rp15-15--15-17---------------------------|
G|-15-17-------------------------------------------------------|
D|-------------------------------------------------------------|
A|-------------------------------------------------------------|
E|-------------------------------------------------------------|

E|---15-17-15--14h15p14----------------------------------------|
B|-17------------------17-15-15/17-19-19h20p19-17~~\-----------|
G|-------------------------------------------------------------|
D|-------------------------------------------------------------|
A|-------------------------------------------------------------|
E|-------------------------------------------------------------|


 G                  D
Tão simples assim, tão fácil assim
C          Am          D
Aceite e prove dessa graça
G                       D
O pão sustenta o homem.Jesus sustenta a alma
 Am7       C               D4  D
Milagre assim não há quem faça
 Am7                              D
Corpo que era pão Sangue que era vinho
  Am  C               D
Pra eternidade é o caminho

G
Eis o Pão que os anjos comem
      D/F#
Transformado em pão do homem
       Em                      C           D
Só os filhos o consomem, Pão pra alma que tem fome
        G                      D/F#
Aos mortais dando comida dais também o Pão da vida
         Em7                     C       D          G  C/G  D/G  C/G
Que a família assim nutrida seja um dia reunida lá no céu

  G              D/F#
Trigo esmagado Cristo imolado
Am     C             D
Ambos vão tornar-se pão
G              D/F#
Um que perece, outro que permanece
 Am7    C        D
Para a nossa salvação
Am7           D       Am7/     C    D
Ó Senhor da unidade, tirai de nós a orfandade

G
Eis o Pão que os anjos comem
      D/F#
Transformado em pão do homem
       Em                      C           D
Só os filhos o consomem, Pão pra alma que tem fome
        G                               D/F#
Aos mortais dando comida dais também o Pão da vida
         Em                          C      D/F#       G  G  C
Que a família assim nutrida seja um dia reunida lá no céu

G
Eis o Pão que os anjos comem
      D/F#
Transformado em pão do homem
       Em                      C           D
Só os filhos o consomem, Pão pra alma que tem fome
        G                               D/F#
Aos mortais dando comida dais também o Pão da vida
         Em                          C      D          G
Que a família assim nutrida seja um dia reunida lá no céu

[Solo]  G  D/F#  Em7  C  D

E|--------------------------------------------------------------12-14-15b-15-17b-17~-|
B|------------------------12--13--15-------------------12-13-15----------------------|
G|----------------------12--12--12---------------12-14-------------------------------|
D|-5~-4-5-7-5-7/9-7------------------14-14-12-14-------------------------------------|
A|------------------9/---------------------------------------------------------------|
E|-----------------------------------------------------------------------------------|

        G                      D/F#
Aos mortais dando comida dais também o Pão da vida
           Em7                       C      D          G  C/G  D/G  C/G
Que a família assim nutrida seja um dia reunida lá no céu

 G                      D/F#    Em7/9
Tão simples assim, tão fácil assim

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C/B = X 2 2 0 1 0
C/G = 3 3 2 X 1 X
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D/G = 3 X X 2 3 2
D4 = X X 0 2 3 3
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
Em7/9 = X 7 5 7 7 X
G = 3 2 0 0 0 3
