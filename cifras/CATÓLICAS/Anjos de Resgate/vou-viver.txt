Anjos de Resgate - Vou Viver

Intro 2x: Bb   Bb/Ab   Eb/G   Ebm/Gb   F4   F

Bb                                     Bb/Ab                       Eb/G
Sinto a paz me invadindo, sua voz me pedindo pra ficar
               Ebm/Gb    F4   F
junto do seu olhar.
Bb                                          Bb/Ab                        Eb/G
Seu querer me envolvendo, e o meu ser se abrindo sem cessar
                     Ebm/Gb    F4   F
Quero a Deus adorar.

Dm      Gm        Cm
Vem viver, para sempre cantar.
F           F/Eb   Dm     Gm
Vem louvar ao nosso Deus.
          Cm               F           Eb
Seu amor me guia, me irradia.
Dm      Gm        Cm
Vou viver, para sempre cantar.
F          Eb       Dm     Gm
Vou louvar ao nosso Deus.

           Cm              F4   F   Bb
Seu amor me guia. Vou voar.

Bb                                     Bb/Ab
Venho a ti sem ter medo de cair.

                      Eb/G                            Ebm/Gb      F4  F
Ao meu lado tu estás, sempre a me acompanhar.
Bb                                   Bb/Ab                          Eb/G
Vou abrir os meus olhos e seguir Teus caminhos, ó Senhor.
                   Ebm/Gb     F4    F
Quero mais desse amor.

Dm      Gm         Cm
Vem viver, para sempre cantar.
F           F/Eb   Dm     Gm
Vem louvar ao nosso Deus.
          Cm               F         Eb
Seu amor me guia, me irradia.
Dm     Gm         Cm
Vou viver, para sempre cantar.
F          Eb       Dm     Gm
Vou louvar ao nosso Deus.
           Cm              F4   F   Bb
Seu amor me guia. Vou voar.

Eb                                      F
Vem, Senhor, me ensina a escutar tua voz e
Eb/G                               F/A
Me entregar por inteiro. Confessar: sou pequeno.
Gb/Bb                                           Ab/C
Toca em mim, com tuas mãos me faz viver para sempre.
Gb                            Ab                                      Bb
Quero mais te amar. Vou soltar minha voz e cantar.

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Ab/C = X 3 X 1 4 4
Bb = X 1 3 3 3 1
Bb/Ab = 4 X 3 3 3 X
Cm = X 3 5 5 4 3
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
Eb/G = 3 X 1 3 4 X
Ebm/Gb = 2 X 1 3 4 X
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
F/Eb = X X 1 2 1 1
F4 = 1 3 3 3 1 1
Gb = 2 4 4 3 2 2
Gb/Bb = 6 X 4 6 7 X
Gm = 3 5 5 3 3 3
