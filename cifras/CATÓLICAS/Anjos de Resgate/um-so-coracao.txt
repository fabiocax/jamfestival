Anjos de Resgate - Um Só Coração

G                                  Em
Ao abrir meus olhos no altar
        G                             Em                D
Vi o cálice Sagrado com o Sangue de Jesus
Bm7            C9                 G   G/B  C   Am7         Am7/G  D
E de dentro desse mesmo Cálice subia Majestosa Eucaristia
G                                   Em               G                   Em              D
Em Tua presença eu me sinto um mendigo sentado à mesa de um Rei
Bm7            C9              G        G/B  C
E sem ter como pagar tamanha refeição
      Am7             Am7/G     D/F#
Só posso dar-te eterna gratidão
    Am7                         D      E4 E
O Cálice e a Hóstia era um.
           Am7                          D4       D      D4  D
Meu desejo também era com Ele ser um

            G        C               D
Ser um só coração Meu Senhor
                G                C          D
O mesmo Sangue nas veias Senhor

G                        G/B
E estando unido a ti
          C             Am7
Esquecer-me de mim
         D                                             Em7  D C   Em7 D C
Concedei que eu fique eternamente assim

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Am7/G = 3 0 2 0 1 0
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D4 = X X 0 2 3 3
E = 0 2 2 1 0 0
E4 = 0 2 2 2 0 0
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
