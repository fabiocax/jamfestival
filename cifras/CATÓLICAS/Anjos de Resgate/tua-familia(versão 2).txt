Anjos de Resgate - Tua Família

    E        Am                       E
Percebe e entende que os melhores amigos
      Am                 C#m       B         A
São aqueles que estão em casa esperando por ti
     E         Am                       E
Acredita nos momentos mais difíceis da vida
      Am                C#m            B         A
Eles sempre estarão por perto pois só sabem te amar

  F#m             E                 Am        C#m   B
E se por acaso a dor chegar ao teu lado vão estar
    F#m             E                    A            B
Pra te acolher e te amparar pois não há nada como um lar

         A               B
Tua   família  volta pra ela
       G#m                 C#m
Tua família te ama e te espera
            F#m            B
Para ao teu lado sempre estar


         A               B
Tua   família  volta pra ela
       G#m                 C#m
Tua família te ama e te espera
            F#m            B           E
Para ao teu lado sempre estar tua família!

    E            Am                  E
Às vezes muitas pedras surgem pelo caminho
       Am                     C#m   B       A
Mas em casa alguém feliz te espera pra te amar
      E             Am                 E
Não deixa que a fraqueza tire a tua visão
          Am                   C#m
Que um desejo engane o teu coração
   B          A
Só Deus não é ilusão

  F#m             E                 Am        C#m   B
E se por acaso a dor chegar ao teu lado vão estar
    F#m             E                    A            B
Pra te acolher e te amparar pois não há nada como um lar

         A               B
Tua   família  volta pra ela
       G#m                 C#m
Tua família te ama e te espera
            F#m            B
Para ao teu lado sempre estar

         A               B
Tua   família  volta pra ela
       G#m                 C#m
Tua família te ama e te espera
            F#m            B
Para ao teu lado sempre estar
       E       A
Tua família!
       E       A
Tua família!
         E
Nossa família

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F9 = 1 3 5 2 1 1
Fm = 1 3 3 1 1 1
Fm6 = 1 X 0 1 1 X
G = 3 2 0 0 0 3
G#º = 4 X 3 4 3 X
G/A = 5 X 5 4 3 X
G/F = 1 X X 0 0 3
G4 = 3 5 5 5 3 3
Gm = 3 5 5 3 3 3
