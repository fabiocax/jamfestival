Anjos de Resgate - Exaltado Entre As Nações

C/G     G     C/G      G               Am/E    Em
G l ó r i a , todas as nações cantem v i t ó r i a,
C7+
Deus está por vir em sua
C/G   G      C/G           G
G l ó r i a ,  a terra tremerá,
             F9      C/E
O mundo inteiro se prostrará a ti cantando
C/G   G         C/G                G
G l ó r i a ,  ao Deus que é comunhão!
               C7+/G
Ao Corpo e Sangue de Jesus, cantemos:
C/E   D/F#    G       D/F#     C/G              D
A   l   e   l   u   i   a,    adorado em canções!
       G   D/F#     C                   D
A l e l u i a , exaltado entre as nações!
       Bm    C       G/B              C
A l e l u i a   honra e glória a ti convém,
         Am7      D        G
Pelos séculos dos séculos amém!

----------------- Acordes -----------------
Am/E = 0 X 2 2 1 0
Am7 = X 0 2 0 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
C/G = 3 3 2 X 1 X
C7+ = X 3 2 0 0 X
C7+/G = 3 X 2 4 1 X
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
F9 = 1 3 5 2 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
