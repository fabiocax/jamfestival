﻿Maiara e Maraisa - Quem Foi Que Soprou

Capo Casa 3

[Intro]  Bm  G  A9

E|-------------------------------------|
B|--3-2/3/2----------------------------|
G|----------4-2------------------------|
D|--------------4-2/4/2-----4-2-----4--|
A|----------------------5-4-----5-4----|
E|-------------------------------------|

 G
Tô com o lençol de ontem nas mãos
   D
Olhando sem piscar os travesseiros no chão
 G
Eu nem dormi depois que cê foi
  D
Imaginava tudo, tudo menos nós dois

    Em          D/F#      Bm         D
E, de repente, tudo muda numa noite só
   Em           D/F#   D
O nosso abraço deu um nó


   G          A9            Bm
Você mudou o meu sentido em 180°
    G          A9        Bm
Deixou meu coração virado

De cabeça pra baixo

                 G            A9
Quem foi que soprou no seu ouvido
                                Bm
O jeito que eu gosto de fazer amor?
              D
Você acertou tudo
                          G
Não teve nadinha que você errou
                  A9
Se não for pedir muito
                               Bm
Me dá um pouquinho de você de novo
                   D
De novo, de novo, novo

                 G            A9
Quem foi que soprou no seu ouvido
                                Bm
O jeito que eu gosto de fazer amor?
              D
Você acertou tudo
                          G
Não teve nadinha que você errou
                  A9
Se não for pedir muito
                               Bm
Me dá um pouquinho de você de novo
                   D
De novo, de novo, novo

( G  A9  Bm  D )

E|-----------------------------|
B|--3-2/3/2--------------------|
G|----------4-2-2/4~-----------|
D|-----------------------------|
A|-----------------------------|
E|-----------------------------|

 G
Tô com o lençol de ontem nas mãos
   D
Olhando sem piscar os travesseiros no chão
 G
Eu nem dormi depois que cê foi
  D
Imaginava tudo, tudo menos nós dois

    Em          D/F#      Bm         D
E, de repente, tudo muda numa noite só
   Em           D/F#    D
O nosso abraço deu um nó

   G          A9            Bm
Você mudou o meu sentido em 180°
    G           A9       Bm
Deixou meu coração virado

De cabeça pra baixo

                 G            A9
Quem foi que soprou no seu ouvido
                                Bm
O jeito que eu gosto de fazer amor?
              D
Você acertou tudo
                          G
Não teve nadinha que você errou
                  A9
Se não for pedir muito
                               Bm
Me dá um pouquinho de você de novo
                   D
De novo, de novo, novo

                 G            A9
Quem foi que soprou no seu ouvido
                                Bm
O jeito que eu gosto de fazer amor?
              D
Você acertou tudo
                          G
Não teve nadinha que você errou
                  A9
Se não for pedir muito
                               Bm
Me dá um pouquinho de você de novo
                   D
De novo, de novo, novo, de novo

                  G   A9
(Quem foi que soprou?)
    Bm                      D
De novo, de novo, de novo, novo, de novo
                  G   A9  Bm7
(Quem foi que soprou?)

----------------- Acordes -----------------
Capotraste na 3ª casa
Bb*  = X 1 3 3 3 1 - (*Db na forma de Bb)
C*  = X 3 2 0 1 0 - (*D# na forma de C)
Dm*  = X X 0 2 3 1 - (*Fm na forma de Dm)
Dm7*  = X 5 7 5 6 5 - (*Fm7 na forma de Dm7)
F*  = 1 3 3 2 1 1 - (*G# na forma de F)
F/A*  = 5 X 3 5 6 X - (*G#/C na forma de F/A)
Gm*  = 3 5 5 3 3 3 - (*A#m na forma de Gm)
