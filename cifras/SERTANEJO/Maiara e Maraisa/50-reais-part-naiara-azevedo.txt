Maiara e Maraisa - 50 Reais (part. Naiara Azevedo)

Intro: C#m B F#m G#7

C#m
Bonito!
A
Que bonito, hein?
E
Que cena mais linda
                          B
Será que eu estou atrapalhando o casalzinho aí

C#m
Que lixo!
                A
Cê tá de brincadeira
E                                       B
Então é aqui o seu futebol toda quarta-feira
   A
E por acaso esse motel
                   B
É o mesmo que me trouxe na lua de mel

                 C#m
É o mesmo que você me prometeu o céu
            A
E agora me tirou o chão
   A
E não precisa se vestir
                  B
Eu já vi tudo que eu tinha que ver aqui
       A
Que decepção
F#m                    B     B7
Um a zero pra minha intuição

Refrão:

E
Não sei se dou na cara dela ou bato em você
B
Mas eu não vim atrapalhar sua noite de prazer
F#m                             G#m    A
E pra ajudar pagar a dama que lhe satisfaz
     B7
Toma aqui uns 50 reais
E
Não sei se dou na cara dela ou bato em você
B
Mas eu não vim atrapalhar sua noite de prazer
F#m                             G#m    A
E pra ajudar pagar a dama que lhe satisfaz
     B7
Toma aqui uns 50 reais

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#7 = 4 6 4 5 4 4
G#m = 4 6 6 4 4 4
