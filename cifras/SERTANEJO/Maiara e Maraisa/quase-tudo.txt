﻿Maiara e Maraisa - Quase Tudo

Capo Casa 4

[Intro]  D  G

   D
A dona Ana lá dos móveis usados
Já deve ter vendido o nosso sofá
   G
O terno e o vestido de noiva
Devem estar num bazar, pra doar
   D
O carro que ficou comigo
   D/F#
Essa semana eu vou trocar
      G
E o Apê que nós dois dividimos
Já coloquei pra alugar
    Bm             A        G
Eu dei um fim em quase tudo
É, quase tudo

                                       D  G  D
Mas o amor que eu sinto por você tá aqui

                D/F#   G
Eu não doei eu não vendi
                                 Bm
Vai ser difícil alguém tirar de mim
        A                G
E se tirar eu deixo de existir
                                   D  G  D
O amor que eu sinto por você tá aqui
                D/F#   G
Eu não doei eu não vendi
                                  Bm
Vai ser difícil alguém tirar de mim
        A                G
E se tirar eu deixo de existir
                                   D  G  D
O amor que eu sinto por você tá aqui

( D/F#  G )

   D
A dona Ana lá dos móveis usados
Já deve ter vendido o nosso sofá
   G
O terno e o vestido de noiva
Devem estar num bazar, pra doar
   D
O carro que ficou comigo
   D/F#
Essa semana eu vou trocar
      G
E o Apê que nós dois dividimos
Já coloquei pra alugar
    Bm             A        G
Eu dei um fim em quase tudo
É, quase tudo

                                       D  G  D
Mas o amor que eu sinto por você tá aqui
                D/F#   G
Eu não doei eu não vendi
                                 Bm
Vai ser difícil alguém tirar de mim
        A                G
E se tirar eu deixo de existir
                                   D  G  D
O amor que eu sinto por você tá aqui
                D/F#   G
Eu não doei eu não vendi
                                  Bm
Vai ser difícil alguém tirar de mim
        A                G
E se tirar eu deixo de existir

                                   D  G  D
O amor que eu sinto por você tá aqui

----------------- Acordes -----------------
Capotraste na 4ª casa
A*  = X 0 2 2 2 0 - (*C# na forma de A)
Bm*  = X 2 4 4 3 2 - (*D#m na forma de Bm)
D*  = X X 0 2 3 2 - (*F# na forma de D)
D/F#*  = 2 X 0 2 3 2 - (*F#/A# na forma de D/F#)
G*  = 3 2 0 0 0 3 - (*B na forma de G)
