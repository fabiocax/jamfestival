Maiara e Maraisa - 10%

[Primeira Parte]

 D
Tô escorada na mesa
             E
Confesso eu quase caí da cadeira
  C#m
E esse garçom não me ajuda
               F#m
Já trouxe a vigésima saideira

    D
Já viu o meu desespero
                E
E aumentou o volume da televisão
 C#m
Sabe que sou viciada
           F#m
E bebo dobrado ouvindo um modão

[Pré-Refrão]


D
A terceira música nem acabou
E
Eu já tô lembrando

Da gente fazendo amor
 C#m
Celular na mão, mas ele não tá tocando
    F#m
Se fosse ligação
                      D
Nosso amor seria engano
           E
Seria engano

[Refrão]

    D
Garçom troca o DVD
                         E
Que essa moda me faz sofrer

E o coração não guenta
       C#m
Desse jeito você me desmonta
                  F#m
Cada dose cai na conta

E os dez por cento aumenta

    D
Garçom troca o DVD
                         E
Que essa moda me faz sofrer

E o coração não guenta
       C#m
Desse jeito você me desmonta
                  F#m
Cada dose cai na conta

E os dez por cento aumenta

              D
Ai cê me arrebenta
             E
E o coração não guenta
                      C#m  F#m
E os dez por cento aumenta

[Pré-Refrão]

D
A terceira música nem acabou
E
Eu já tô lembrando

Da gente fazendo amor
 C#m
Celular na mão, mas ele não tá tocando
    F#m
Se fosse ligação
                      D
Nosso amor seria engano
           E
Seria engano

[Refrão]

    D
Garçom troca o DVD
                         E
Que essa moda me faz sofrer

E o coração não guenta
       C#m
Desse jeito você me desmonta
                  F#m
Cada dose cai na conta

E os dez por cento aumenta

    D
Garçom troca o DVD
                         E
Que essa moda me faz sofrer

E o coração não guenta
       C#m
Desse jeito você me desmonta
                  F#m
Cada dose cai na conta

E os dez por cento aumenta

    D
Garçom troca o DVD
                         E
Que essa moda me faz sofrer

E o coração não guenta
       C#m
Desse jeito você me desmonta
                  F#m
Cada dose cai na conta

E os dez por cento aumenta

    D
Garçom troca o DVD
                         E
Que essa moda me faz sofrer

E o coração não guenta
       C#m
Desse jeito você me desmonta
                  F#m
Cada dose cai na conta

E os dez por cento aumenta

              D
Ai cê me arrebenta
             E
E o coração não guenta
                      C#m  F#m  D  F#m
E os dez por cento aumenta

----------------- Acordes -----------------
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
