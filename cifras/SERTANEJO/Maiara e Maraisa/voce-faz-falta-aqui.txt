Maiara e Maraisa - Você Faz Falta Aqui

Intro: A9

     A9
E|-0----------------------------------------|
B|-0---0---0---0---0---0---0---0------------|
G|-2-2---2---2---2---2---2---2---2----------|
D|-2----------------------------------------|
A|-0----------------------------------------|
E|------------------------------------------|

Primeira Parte:

   A9
Você, você, faz falta, falta

Aqui, aqui, aqui
       F#m7(11)
Que o som, o som, bate, bate

Volta, volta, volta
     D9
Não tem mais nada entre o chão


E o teto, teto, teto
     A9
Só o eco, eco, eco, eco

Primeira Parte:

Parte 1 de 4
     A9
E|-0----------------------------------------|
B|-0---0---0---0---0---0---0---0------------|
G|-2-2---2---2---2---2---2---2---2----------|
D|-2----------------------------------------|
A|-0----------------------------------------|
E|------------------------------------------|

Parte 2 de 4
     F#m7(11)
E|-0----------------------------------------|
B|-0---0---0---0---0---0---0---0------------|
G|-2-2---2---2---2---2---2---2---2----------|
D|-2----------------------------------------|
A|------------------------------------------|
E|-2----------------------------------------|

Parte 3 de 4
     D9
E|-0----------------------------------------|
B|-3----------------------------------------|
G|-2---2---2---2---2---2---2---2------------|
D|-0-0---0---0---0---0---0---0---0----------|
A|------------------------------------------|
E|------------------------------------------|

Parte 4 de 4
     A9
E|-0---------------0------------------------|
B|-0---0---0---0---0------------------------|
G|-2-2---2---2---2-2------------------------|
D|-2---------------2------------------------|
A|-0----------------------------------------|
E|------------------------------------------|

Pré-refrão:

        D9
Tem certeza que você levou tudo?
        F#m7(11)
Tem certeza que não esqueceu nada?
       D9
Eu só sei que até o criado mudo
        Dm
Do seu livro preferido sente falta


Refrão:

                     A9
É que eu te vejo em tudo

Em cada canto dessa casa
                    F#m7(11)
Nos cabides, nas gavetas

E na cama bagunçada
                D9
E no box do banheiro

Sempre que o vidro embaça
                      Dm
Tem seu nome, um coração

E uma flecha atravessada

                     A9
É que eu te vejo em tudo

Em cada canto dessa casa
                    F#m7(11)
Nos cabides, nas gavetas

E na cama bagunçada
                        D9
E o seu cheiro está grudado

Em cada roupa que eu usava
                 Dm
Você foi e esqueceu

                       A9
De levar a saudade na mala, mala, mala


Primeira Parte:

   B9
Você, você, faz falta, falta

Aqui, aqui, aqui
       G#m7(11)
Que o som, o som, bate, bate

Volta, volta, volta
     E9
Não tem mais nada entre o chão

E o teto, teto, teto
     B9
Só o eco, eco, eco, eco


Pré-refrão:

        E9
Tem certeza que você levou tudo?
        G#m7(11)
Tem certeza que não esqueceu nada?
       E9
Eu só sei que até o criado mudo
        Em
Do seu livro preferido sente falta


Refrão:

                     B9
É que eu te vejo em tudo

Em cada canto dessa casa
                    G#m7(11)
Nos cabides, nas gavetas

E na cama bagunçada
                E9
E no box do banheiro

Sempre que o vidro embaça
                      Em
Tem seu nome, um coração

E uma flecha atravessada

                     B9
É que eu te vejo em tudo

Em cada canto dessa casa
                    G#m7(11)
Nos cabides, nas gavetas

E na cama bagunçada
                        E9
E o seu cheiro está grudado

Eem cada roupa que eu usava
                 Em
Você foi e esqueceu
                       B9
De levar a saudade na mala, mala, mala

Você, você, faz falta, falta

Aqui, aqui, aqui

----------------- Acordes -----------------
A9 = X 0 2 2 0 0
B9 = X 2 4 4 2 2
D9 = X X 0 2 3 0
Dm = X X 0 2 3 1
E9 = 0 2 4 1 0 0
Em = 0 2 2 0 0 0
F#m7(11) = 2 X 2 2 0 X
G#m7(11) = 4 X 4 4 2 X
