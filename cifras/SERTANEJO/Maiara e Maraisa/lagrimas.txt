Maiara e Maraisa - Lágrimas

[Intro]  G

G
Bateu a porta na minha cara
                 Em
E disse vou embora
                                 C
E sem pensar em nada você jogou fora
Cm                                 G
Nossa história e o amor que eu te dei

G                                  Em
Menos de 24 horas, vem notícias suas
                                    C
Seus beijos espalhados no meio da rua
Cm                        G
Coisa feia essa sua loucura

                Em
Foi infantil demais
             D9
Você passou da conta

  C
Me explica esse seu desespero
                            D     Em
Se entregando a outra tão cedo demais

              D9
Se "tava" confiante
                   C
Voltou aqui porquê?
                  G
De ser pra ouvir eu dizer

É que lá fora não achou ninguém melhor que eu
Em
Se agora descobriu que ama o problema é seu
C
Tá chorando, acho melhor parar!
Cm
Se depender de mim, as lágrimas vão te afogar (2x) aaah
G
Elas vão te afogar
               Em
Elas vão te afogar

( C  Cm )

                 Em
Foi infantil demais
             D9
Você passou da conta
  C
Me explica esse seu desespero
                           Am     Em
Se entregando a outra tão cedo demais

              D9
Se "tava" confiante
                   C
Voltou aqui porquê?
                  G
De ser pra ouvir eu dizer

G
É que lá fora não achou ninguém melhor que eu
Em
Se agora descobriu que ama o problema é seu
C
Tá chorando, acho melhor parar!
Cm
Se depender de mim, as lágrimas vão te afogar (2x) aaah

G
Elas vão te afogar
               Em  G
Elas vão te afogar

C
Tá chorando, acho melhor parar!
Cm                                        G
Se depender de mim, as lágrimas vão te afogar

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D9 = X X 0 2 3 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
