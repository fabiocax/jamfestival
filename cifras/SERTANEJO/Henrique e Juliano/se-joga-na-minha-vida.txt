Henrique e Juliano - Se Joga Na Minha Vida

[Intro] Dm  F  Gm  A#

Dm
   Por que seu coração tá pisando no freio?
F
  Por que seu beijo tá com gostinho de medo?
     Gm
Por quê?
                        A#
Isso nem combina com você

     Dm
Engraçado que naquele amor sem futuro
   F
Você se entregou, pulou sem
      Gm
Paraquedas
     Dm
E comigo você tá em cima do muro
F                  Gm
Logo eu, a pessoa certa


             A#
Me diz por que tá com medo
          C
De ser feliz

     F
Ai, ai, ai, ai
     C
Por que cê tá com medo de
  Gm
Amar?
   A#
Faço o que for preciso pra
    F
Provar
                        C
Que meu beijo é o certo
                      Gm
Nosso amor é completo
    A#             C
Se joga na minha vida
    F
Ai, ai, ai, ai
      C
Por que cê tá com medo de
  Gm
Amar?
   A#
Faço o que for preciso pra
    F
Provar
                        C
Que meu beijo é o certo
                      Gm
Nosso amor é completo
    A#             C
Se joga na minha vida
           F      C  Gm  A#  C
Que eu te pego

Dm
Engraçado que naquele amor sem futuro
   F
Você se entregou, pulou sem
      Gm
Paraquedas
     Dm
E comigo você tá em cima do muro
F                  Gm
Logo eu, a pessoa certa

             A#
Me diz por que tá com medo
          C
De ser feliz

     F
Ai, ai, ai, ai
     C
Por que cê tá com medo de
  Gm
amar?
   A#
Faço o que for preciso pra
    F
provar
                        C
Que meu beijo é o certo
                      Gm
Nosso amor é completo
    A#             C
Se joga na minha vida
    F
Ai, ai, ai, ai
      C
Por que cê tá com medo de
  Gm
Amar?
   A#
Faço o que for preciso pra
    F
Provar
                        C
Que meu beijo é o certo
                      Gm
Nosso amor é completo
    A#             C
Se joga na minha vida
           F      C  Gm
Que eu te pego
 A#             C
Se joga na minha vida
           F
Que eu te pego

----------------- Acordes -----------------
A# = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
