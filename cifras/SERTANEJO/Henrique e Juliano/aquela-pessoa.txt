Henrique e Juliano - Aquela Pessoa

[Intro]  F9/C  G/D  Am

F9                     G/D
Você também tem que eu sei
          Am               Em
Aquela pessoa, não adianta negar
F9
Que tem passe livre
      G/D              Am            Em
Carta branca na sua vida para ir e voltar
        F9                G/D               Am  G/D
Quando quiser, nunca vai deixar de ser o que é éé

F9                    G/D             Am
Todo mundo tem uma pessoa, aquela pessoa
             G/D
Que te faz esquecer todas as outras
F9                    G/D             Am
Todo mundo tem uma pessoa, aquela pessoa
            G/D                  F9
Não precisa dia e nem hora pra chegar

                                   G/D
Na portaria do meu coração já tem seu nome

Pode entrar

( F9/C  G/D  Am )

F9                    G/D
Você também tem que eu sei
          Am               Em
Aquela pessoa, não adianta negar
F9
Que tem passe livre
      G/D              Am            Em
Carta branca na sua vida para ir e voltar
        F9                G/D               Am  G/D
Quando quiser, nunca vai deixar de ser o que é éé

F9                    G/D             Am
Todo mundo tem uma pessoa, aquela pessoa
             G/D
Que te faz esquecer todas as outras
F9                    G/D             Am
Todo mundo tem uma pessoa, aquela pessoa
            G/D                  F9
Não precisa dia e nem hora pra chegar
                                   G/D
Na portaria do meu coração já tem seu nome

Pode entrar

[Final]  F9/C  G/D  Am

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Em = 0 2 2 0 0 0
F9 = 1 3 5 2 1 1
F9/C = X 3 X 5 6 3
G/D = X 5 5 4 3 X
