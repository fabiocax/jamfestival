Henrique e Juliano - Granada

[Intro] C  G  Dm7  F

E|------------------------------------------|
B|------------------------------------------|
G|-----5--4/5-0----5h7-2/4-0----------------|
D|-3/5----------2---------------------------|
A|------------------------------------------|
E|------------------------------------------|

E|------------------------------------------|
B|-----6p5-6--------8/10\8------------------|
G|-----------7p5-7--------------------------|
D|-5h7--------------------------------------|
A|------------------------------------------|
E|------------------------------------------|

[Primeira Parte]

    C
Brigou de novo
    G
E agora é sério

   Dm7
Cabou de vez?
  F
Aham! Sei

    C              G
Não é de se estranhar
         Dm7
Você gritar ma minha porta
          F
Quatro e pouco da manhã

[Pré-Refrão]

             Am7      G
Eu espero você sem saber
           C
O dia que vai terminar outra vez
             F
E quando vai aparecer
            Am7          G
Eu queria sair, me relacionar
                 C
Mas e o medo de você brigar
                          F
Vir aqui, bater na minha porta

E eu não tá

[Refrão]

                 C               G
Eu sou o quebra galho dessa relação
                       Dm7
Cê me enche de expectativa
                                 F
E diz que precisa de mim na sua vida
                   C                       G
Mas quado a raiva passa, eu já não sirvo, não
                      Dm7
Você volta ilesa pra casa
E adivinha quem sobra
 F
Com a granada na mão?

C  G   Dm7
   Eu, eu
      F
O iludido da situação
C  G   Dm7
   Eu, eu
      F
O iludido da situação

[Solo] C  G  Dm7  F

E|------------------------------------------|
B|------------------------------------------|
G|-----4/5-0----5h7-2/4-0-------------------|
D|-----------2------------------------------|
A|------------------------------------------|
E|------------------------------------------|

E|------------------------------------------|
B|-----6p5-6--------------------------------|
G|-----------7p5-5/7\5----------------------|
D|-5h7--------------------------------------|
A|------------------------------------------|
E|------------------------------------------|

[Pré-Refrão]

             Am7      G
Eu espero você sem saber
           C
O dia que vai terminar outra vez
             F
E quando vai aparecer
            Am7          G
Eu queria sair, me relacionar
                 C
Mas e o medo de você brigar
                          F
Vir aqui, bater na minha porta

E eu não tá

[Refrão]

                 C               G
Eu sou o quebra galho dessa relação
                       Dm7
Cê me enche de expectativa
                                 F
E diz que precisa de mim na sua vida
                   C                       G
Mas quado a raiva passa, eu já não sirvo, não
                      Dm7
Você volta ilesa pra casa
E adivinha quem sobra
 F
Com a granada na mão?

C  G   Dm7
   Eu, eu
      F
O iludido da situação
C  G   Dm7
   Eu, eu
      F            C
O iludido da situação

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
Dm7 = X 5 7 5 6 5
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
