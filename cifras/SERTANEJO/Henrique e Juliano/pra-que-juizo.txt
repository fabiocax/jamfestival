Henrique e Juliano - Pra Que Juízo

Base intro 2x: Bm  D  A

Dedilhado da intro 2x:
  Bm
E|------------0-----------|
B|-----3----------3-------|
G|-------4----------4-----|
D|---4-----4----4-----4---|
A|-2----------------------|
E|------------------------|

  D
E|-----------0------------|
B|-----3---------3--------|
G|---2---2-----2---2------|
D|-0-------0---------0----|
A|------------------------|
E|------------------------|

  A
E|-----------0------------|
B|-----0--------0---0-----| 2x 
G|-------2--------2-------|
D|---2-----2--2-----2-----|
A|-0----------------------|
E|------------------------|


Bm
 Tomar juízo pra que
D
 Se quem eu quero é você
A                A
 Pra que juízo?

Bm
 E não se pode negar
D
 E nem tem como esconder
A
 Que eu tô louco, muito louco
A
 Por um beijo seu

           Bm
 Pra que juízo
D                 A
 Se eu já perdi o meu
           Bm
 E o meu desejo
D                   A
 É que você perca o seu

Bm
 Foi tentando evitar seu olhar
D
 Foi tentando fugir de você
A
 Foi querendo a vontade ocultar
A
 Foi tentando querer me conter
Bm
 Foi pensando no medo de errar
D
 Foi lutando pra não me perder
   A
 Enlouqueci

           Bm
 Pra que juízo
D                 A
 Se eu já perdi o meu
           Bm                      D
 E eu to querendo to sempre esperando

 É que você também perca o seu

Intro:( Bm D A )

           Bm
 Pra que juízo
D                 A
 Se eu já perdi o meu
            Bm
 E eu to querendo
D                   A         A
 É que também perca o seu

Bm
 Foi tentando evitar seu olhar
D
 Foi tentando fugir de você
A
 Foi querendo a vontade ocultar
A
 Foi tentando querer me conter
Bm
 Foi pensando no medo de errar
D
 Foi lutando pra não me perder
   A
 Enlouqueci

           Bm
 Pra que juízo
D                 A
 Se eu já perdi o meu
           Bm
 E eu to querendo
D                   A
 É que você perca o seu

           Bm
 Pra que juízo (pra que juízo)
D                 A
 Se eu já perdi o meu
           Bm                      D
 E eu to querendo to sempre esperando

 É que você também perca o seu

Intro: (Bm D A )

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
