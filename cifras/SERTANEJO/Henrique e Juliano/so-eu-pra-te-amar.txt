Henrique e Juliano - Só Eu Pra Te Amar

Intro: ( Am Bm C )
       ( Am Bm F )
       ( Am Bm C Am Bm F )

E|------------------------------------|
B|-1-1-1/3-3-5~--1-1-1/3-3/1-1~-------|
G|-2-2-2/4-4-5~--2-2-2/4-4/2-2~-------|
D|------------------------------------|
A|------------------------------------|
E|------------------------------------|

Am             D              C
Outra vez a mesma história
Am             D               F
Coração que nunca aprende
Am                 D       C
Sei que não caiu a ficha
         Am                D           F
Sinto muito, mas ela não sente

               Am
Chega de buscar em novos amores

                      D
Aquele velho amor que me deixou doente
                   Am
Doente ainda tô, amor ainda tem
                    D
O que me ameaça é o que mais me faz bem

Am                                  F
Em muitas camas você pode dormir
G                                         Am
Mas em qual delas você vai acordar?
                                   F
De quantas vidas você vai precisar
                   G                         D
Pra enxergar que só eu pra te amar?

Am                                  F
Em muitas camas você pode dormir
G                                         Am
Mas em qual delas você vai acordar?
                                    F
De quantas vidas você vai precisar
                     G                       D
Pra enxergar que só eu pra te amar?
                                    F
Só o amor pra suportar

Passagem: G   Am  F

               Am
Chega de buscar em novos amores
                     D
Aquele velho amor que me deixou doente
                   Am
Doente ainda tô, amor ainda tem
                     D
O que me ameaça é o que mais me faz bem

Am                                  F
Em muitas camas você pode dormir
G                                         Am
Mas em qual delas você vai acordar?
                                    F
De quantas vidas você vai precisar
                    G                        D
Pra enxergar que só eu pra te amar?

Am                                  F
Em muitas camas você pode dormir
G                                         Am
Mas em qual delas você vai acordar?
                                    F
De quantas vidas você vai precisar
                   G                         D
Pra enxergar que só eu pra te amar?
                                  F
Só o amor pra suportar

Am                                         F
Em muitas camas você pode dormir
G                                    Am
Mas em qual delas você vai acordar?
                                    F
De quantas vidas você vai precisar
                   G                         D
Pra enxergar que só eu pra te amar?
                                  F
Só o amor pra suportar

Final: ( G  Am )

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
