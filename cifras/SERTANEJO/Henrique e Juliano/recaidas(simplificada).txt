Henrique e Juliano - Recaídas

[Intro] G

[Primeira Parte]

Você pode, ficar com quem você quiser
                Em
Não tem nada a ver
             C
Eu não mando em você
             Am
Mais ainda choro
                                     D
E quando alguém comenta não quero saber
        G
Me preocupo
                                         Em
E apesar dos pesares eu sempre quero te ver bem
         C
E ainda vou além
          Am
Em uma relação sei que não vai ser fácil

               D
Amar outro alguém

[Segunda Parte]

   G
E hoje, mesmo separados sinto
                       D
Que seu corpo ainda é meu

As vezes me escondo
                              Am
E faço de tudo pra ninguém notar que eu
        C
Vivo e morro por ti
     Am                               D
Tem semanas que as vezes sofro vem as   recaídas

    G
As vezes eu queria ter o poder
                         D
De poder te apagar da memória

E nessa fraqueza ter força pra fazer
              Am                  C
Com que essa nossa história, não passe
              D                  G    D
De passado e fique da porta pra fora

[Refrão]

         G                        D
Se eu pudesse te apagar da minha mente
           Am   C
Apagaria agora
          G                           D
Mas toda vez que eu me lembro de nós dois
                 Am      C
Meu coração sempre chora
                       G  D
E é sempre a mesma história
  Am     C
Aaa  aaa


[Solo] G  Em  C  Am  D


[Repete a Segunda Parte]

   G
E hoje, mesmo separados sinto
                       D
Que seu corpo ainda é meu

As vezes me escondo
                              Am
E faço de tudo pra ninguém notar que eu
        C
Vivo e morro por ti
     Am                               D
Tem semanas que as vezes sofro vem as   recaídas

    G
As vezes eu queria ter o poder
                         D
De poder te apagar da memória

E nessa fraqueza ter força pra fazer
              Am                  C
Com que essa nossa história, não passe
              D                  G    D
De passado e fique da porta pra fora

[Refrão]

         G                        D
Se eu pudesse te apagar da minha mente
           Am   C
Apagaria agora
          G                           D
Mas toda vez que eu me lembro de nós dois
                 Am      C
Meu coração sempre chora
                       G  D
E é sempre a mesma história
  Am     C  G
Aaa  aaa

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
