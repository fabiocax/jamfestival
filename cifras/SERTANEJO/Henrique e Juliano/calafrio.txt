Henrique e Juliano - Calafrio

Intro:
( Em7(9) C4+  G  D )
( Em7(9) C4+  G  D5 )

Intro:
Parte 1:
  Em7(9)     C4+       G          D
E|-2-----3----2-----3-----------------------|
B|-----3---3------3----3-----1-0------------|
G|---0----------0----------0------2-----0---|
D|-----------------------0------------4---4-|
A|------------3---------------------5-------|
E|-0-------------------3--------------------|

Parte 2:
  Em7(9)     C4+       G          D5
E|-2-----3----2-----3-----------------------|
B|-----3---3------3----3-----1-0------------|
G|---0----------0----------0------2---------|
D|-----------------------0--------0---------|
A|------------3-----------------------------|
E|-0-------------------3--------------------|


Primeira Parte:
G
  Os nossos corpos se completam
               C
Mesmo estando longe
         Cm                 G
Em pensamento eu vou te buscar

Dá calafrio, um arrepio
                 C
Ao lembrar seu cheiro
      Cm                 G
Pela porta do quarto entrar

                          G/B
Sinto um silêncio e um vazio
             C
Bate o desespero
            Cm              G
Não vejo a hora de você voltar
                    G/B
E no espelho ainda tem
               C
Escrito em vermelho
                  Cm
Dentro de um coração
              G  D
"Tô indo embora"

Dedilhado Primeira Parte:

Parte 1 (2x):
  G                 C       Cm
E|------------------------------------------|
B|-0---0---0---0----1---1----4---4----------|
G|-0---0---0---0----0---0----5---5----------|
D|---0---0---0---0----2---2----5---5--------|
A|------------------3--------3--------------|
E|-3----------------------------------------|

Parte 2 (2x):
  G       G/B       C       Cm
E|------------------------------------------|
B|-0---0---0---0----1---1----4---4----------|
G|-0---0---0---0----0---0----5---5----------|
D|---0---0---0---0----2---2----5---5--------|
A|---------2--------3--------3--------------|
E|-3----------------------------------------|

Parte 3:
  G         D
E|----------2---2---------------------------|
B|-0---0----3---3---------------------------|
G|-0---0------2---2-------------------------|
D|---0---0--0-------------------------------|
A|------------------------------------------|
E|-3----------------------------------------|

Refrão:
                   C9
Pra nunca mais voltar

Foi tão difícil suportar
                     G            D
A dor de ouvir o seu adeus, seu adeus

                  C9
Eu não vou mais chorar

Eu sei, você quer me amar
                              G
Eu vou deixar o tempo te mostrar
       D
Que o seu amor sou eu

                    C9
Eu não vou mais chorar

Eu sei, você quer me amar
                              G
Eu vou deixar o tempo te mostrar
       D                   C9
Que o seu amor sou eu, sou eu

Repete Intro:
( Em7(9) C4+  G  D )
( Em7(9) C4+  G  D5 )

Primeira Parte:
G                           G/B
  Sinto um silêncio e um vazio
             C9
Bate o desespero
            Cm              G
Não vejo a hora de você voltar
                    G/B
E no espelho ainda tem
               C9
Escrito em vermelho
                  Cm
Dentro de um coração
              G  D
Tô indo embora

Refrão:
                   C9
Pra nunca mais voltar

Foi tão difícil suportar
                     G            D
A dor de ouvir o seu adeus, seu adeus

                  C9
Eu não vou mais chorar

Eu sei, você quer me amar
                              G
Eu vou deixar o tempo te mostrar
       D
Que o seu amor sou eu

                    C9
Eu não vou mais chorar

Eu sei, você quer me amar
                              G
Eu vou deixar o tempo te mostrar
       D                   C9
Que o seu amor sou eu, sou eu

Final:
  Em7(9)     C4+       G         D/F#   G
E|-2-----3----2-----3-----------------------|
B|-----3---3------3----3-----1-0--------3---|
G|---0----------0----------0------2-----0---|
D|-----------------------0--------0-----0---|
A|------------3-----------------------------|
E|-0-------------------3----------2-----3---|

----------------- Acordes -----------------
C = X 3 2 0 1 0
C4+ = X 3 X 0 3 2
C9 = X 3 2 0 3 3
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D/F# = 2 X 0 2 X X
D5 = X X 0 2 X X
Em7(9) = 0 X X 0 3 2
G = 3 2 0 0 3 3
G/B = X 2 0 0 3 3
