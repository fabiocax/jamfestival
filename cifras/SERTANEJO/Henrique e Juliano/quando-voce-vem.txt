Henrique e Juliano - Quando Você Vem

[Intro] C#  G#  F#
        C#  G#  F#

C#                                     F#
Beijo tecnicamente sem língua também é beijo, é beijo
       C#
Mas o seu eu gosto de qualquer jeito
                          F#
No pescoço, na boca ou no queixo
F#
O efeito é o mesmo

D#m           F#
O que vier de você
                C#          G#
Faço questão de aceitar, sem medo
D#m                    F#
Se eu soubesse que era bom assim
      C#                G#    F#
Tinha chegado aqui mais cedo


D#m                     F#
É que eu não posso ficar sem
       C#
Só seu gosto que mata minha fome
C#
Coração tem na testa seu nome

D#m               F#
Eu amo quando você vem
                C#
E deixa sobrando sorriso
                  G#
Na saudade você dá sumiço

D#m                     F#
É que eu não posso ficar sem
       C#
Só seu gosto que mata minha fome
C#
Coração tem na testa seu nome

D#m               F#
Eu amo quando você vem
                C#
E deixa sobrando sorriso
                  G#
Na saudade você dá sumiço

D#m                     F#
É que eu não posso ficar sem
                C#
Só beijando você fico bem

( C#  G#  F# )
( C#  G#  F# )

D#m           F#
O que vier de você
                C#          G#
Faço questão de aceitar, sem medo
D#m                    F#
Se eu soubesse que era bom assim
      C#                G#
Tinha chegado aqui mais cedo

D#m                     F#
É que eu não posso ficar sem
       C#
Só seu gosto que mata minha fome
C#
Coração tem na testa seu nome

D#m               F#
Eu amo quando você vem
                C#
E deixa sobrando sorriso
                  G#
Na saudade você dá sumiço

D#m                     F#
É que eu não posso ficar sem
       C#
Só seu gosto que mata minha fome
C#
Coração tem na testa seu nome

D#m               F#
Eu amo quando você vem
                C#
E deixa sobrando sorriso
                  G#
Na saudade você dá sumiço

D#m                     F#
É que eu não posso ficar sem
                C#
Só beijando você fico bem

( C#  G#  F#  C# )

----------------- Acordes -----------------
C# = X 4 6 6 6 4
D#m = X X 1 3 4 2
F# = 2 4 4 3 2 2
G# = 4 3 1 1 1 4
