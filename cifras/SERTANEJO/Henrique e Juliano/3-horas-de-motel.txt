Henrique e Juliano - 3 Horas de Motel

B                         F#
A lua dormiu, o sol já chegou
                         C#m
E a gente aqui fazendo amor
      E                                      F#
A chuva passou, mas o nosso fogo não apagou
               B
Tinha fogo demais
B                          F#
E por mais que a gente tentasse dormir
              E
O meu corpo colava no seu feito imã
        Em
Tanto tempo depois quem diria
F#                    B
E pra matar a saudade
                                  F#
Três dias, três noites de amor gostoso
                         C#m
Meu corpo pra sempre colado em seu corpo
E            F#
3 horas de motel foi pouco

B                                 F#
Três dias, três noites de amor gostoso
                         C#m
Meu corpo pra sempre colado em seu corpo
E            F#            B
3 horas de motel foi pouco

----------------- Acordes -----------------
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
