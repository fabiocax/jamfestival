Henrique e Juliano - Calafrio

Intro 2x: Em  C  G  D

Primeira Parte:

G
  Os nossos corpos se completam
               C
Mesmo estando longe
         Cm                 G
Em pensamento eu vou te buscar

Dá calafrio, um arrepio
                 C
Ao lembrar seu cheiro
      Cm                 G
Pela porta do quarto entrar


Sinto um silêncio e um vazio
             C
Bate o desespero

            Cm              G
Não vejo a hora de você voltar

E no espelho ainda tem
               C
Escrito em vermelho
                  Cm
Dentro de um coração
              G  D
"Tô indo embora"

Refrão:

                   C
Pra nunca mais voltar

Foi tão difícil suportar
                     G            D
A dor de ouvir o seu adeus, seu adeus

                  C
Eu não vou mais chorar

Eu sei, você quer me amar
                              G
Eu vou deixar o tempo te mostrar
       D
Que o seu amor sou eu

                    C
Eu não vou mais chorar

Eu sei, você quer me amar
                              G
Eu vou deixar o tempo te mostrar
       D                   C
Que o seu amor sou eu, sou eu

Repete Intro:

Em  C  G  D

Primeira Parte:

G
  Sinto um silêncio e um vazio
             C
Bate o desespero
            Cm              G
Não vejo a hora de você voltar

E no espelho ainda tem
               C
Escrito em vermelho
                  Cm
Dentro de um coração
              G  D
"Tô indo embora"

Refrão:

                   C
Pra nunca mais voltar

Foi tão difícil suportar
                     G            D
A dor de ouvir o seu adeus, seu adeus

                  C
Eu não vou mais chorar

Eu sei, você quer me amar
                              G
Eu vou deixar o tempo te mostrar
       D
Que o seu amor sou eu

                    C
Eu não vou mais chorar

Eu sei, você quer me amar
                              G
Eu vou deixar o tempo te mostrar
       D                   C
Que o seu amor sou eu, sou eu

Em  C  G  D  G

----------------- Acordes -----------------
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 3 3
