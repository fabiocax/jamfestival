Henrique e Juliano - Tudo Virou Saudade

F#m
Sinceramente, eu não sei o que fazer
                    D
Com essa falta que você está mim fazendo agora
    A
Era bonito o que a gente vivia
                E
O que você me dizia não me sai da memória

F#m
De vez em quando me pego lembrando
              D
Da gente se amando, era tão intenso
        A
Mas e agora que acabou, e o tempo passou
   E
E mesmo sem querer pensar eu penso

F#m  D    A       E
O    que será que você tá fazendo agora
E com quem você está

F#m   D
Quero saber
A        E
Ainda me lembro de você
                          F#m          D   A
Mesmo sem você se lembrar que eu existo
E
Ainda me importo com você
                           F#m    D  A
Mesmo sem você se importar comigo
E
Meu coração ama você
                           F#m
E nunca vai deixar de amar

E   F#m                  D                    A
Por quê que a amizade às vezes termina em amor
                 E                D
Mas o amor nunca termina em amizade
               E
E nunca terminou
E   F#m                  D                   A
Por quê que você foi falar tudo o que me falou
               E                     D      E F#m
E agora que acabou, tudo virou saudade

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
