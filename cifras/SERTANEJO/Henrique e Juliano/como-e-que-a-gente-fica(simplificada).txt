Henrique e Juliano - Como É Que a Gente Fica

Intro: F  Am

Primeira Parte:

F
  Eu tava certo
                          Am
De que o amor era para os outros

Não era pra mim
F
  Um cara esperto fechado no meu canto
         Am
Eu só vivia assim

Pré-refrão:

G
  Entre uma boca e outra
           D
Uma dose e outra

                           F
Toda madrugada nessa vida louca
                                 G
Nem passou pela cabeça me apaixonar

Refrão:

                      C
Eu esbarrei no seu olhar

Tropecei no seu beijo

Colei no seu rosto
              G
Eu queimei a língua

Foi na ponta do queixo

Me acertou de jeito
                Dm
Olha eu na sua vida

Pensa, explica
                    F   G
Como é que a gente fica

                      C
Eu esbarrei no seu olhar

Tropecei no seu beijo

Colei no seu rosto
              G
Eu queimei a língua

Foi na ponta do queixo

Me acertou de jeito
                Dm
Olha eu na sua vida

Pensa, explica
                    F   G
Como é que a gente fica
                    F  Am
Como é que a gente fica

Pré-refrão:

G
  Entre uma boca e outra
           D
Uma dose e outra
                           F
Toda madrugada nessa vida louca
                                 G
Nem passou pela cabeça me apaixonar

Refrão:

                      C
Eu esbarrei no seu olhar

Tropecei no seu beijo

Colei no seu rosto
              G
Eu queimei a língua

Foi na ponta do queixo

Me acertou de jeito
                Dm
Olha eu na sua vida

Pensa, explica
                    F   G
Como é que a gente fica

                      C
Eu esbarrei no seu olhar

Tropecei no seu beijo

Colei no seu rosto
              G
Eu queimei a língua

Foi na ponta do queixo

Me acertou de jeito
                Dm
Olha eu na sua vida

Pensa, explica
                    F   G
Como é que a gente fica

                      C
Eu esbarrei no seu olhar

Tropecei no seu beijo

Colei no seu rosto
              G
Eu queimei a língua

Foi na ponta do queixo

Me acertou de jeito
                Dm
Olha eu na sua vida

Pensa, explica
                    F   G
Como é que a gente fica
                    F  Am  F
Como é que a gente fica

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 3 3
