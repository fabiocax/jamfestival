Henrique e Juliano - Medo de Te Amar

[Intro] Em  D  Am  C

Em
Vai ser inevitável se eu te encontrar
D
Um beijo vai rolar e eu sei que vai doer
Am
Por isso é que eu tô tentando me afastar
C
Mas te confesso, tá difícil de esquecer

Em
Eu tento disfarçar, fingir que não te quero
G
Pra enganar meu coração
D
Desvio o meu olhar, pois se eu te ver
                 Am                C
Não vou conseguir agir com a razão

Em
Tô te querendo tanto

D                       Am
Mesmo assim vivo brigando com meu coração
C
Forçando ele a não se entregar
Em                      D
Numa hora eu tô sorrindo
                       Am
Noutra me pego chorando
                   C
Querendo entender esse meu medo de te amar
Em                  D
Tô te querendo tanto
                       Am
Mesmo assim vivo brigando com meu coração
C
Forçando ele a não se entregar
Em                      D
Numa hora eu tô sorrindo
                       Am
Noutra me pego chorando
                   C
Querendo entender esse meu medo de te amar

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
