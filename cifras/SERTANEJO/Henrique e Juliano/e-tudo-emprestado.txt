Henrique e Juliano - É Tudo Emprestado

Intro 2x: F# C# G#m B

D#m                 B
Hoje o vento virou, a maré tá pro meu lado
F#                   C#
Hoje eu tô por cima, problema é coisa do passado
D#m                 B
Tô montado na grana, tanque cheio de gasolina
F#                     C#
Meu carro tá lavado e elas vem que vem pra cima

D#m
Camarote pra nós separado
B                          F#
Muito bom pra quem tava quebrado
                      C#
Isso aqui tá lotado, agora só entra as mais gatas
D#m               B                            F#
Hoje eu tô mudado, todo mundo chamando pra foto
F#                    B           G#m     C#
Eu tô bem na parada, agora eu virei magnata


Refrão 2x:
F#                 C#
Mas é tudo emprestado
                 G#m
Ser pobre é bom de vez em quando
          B
Todo dia não tá dando
F#             C#
É tudo emprestado
            G#m
Aproveita que hoje eu tô pagando
           B
Amanhã o povo tá cobrando

( F# C# G#m B )

D#m
Camarote pra nós separado
B                          F#
Muito bom pra quem tava quebrado
                      C#
Isso aqui tá lotado, agora só entra as mais gatas
D#m               B                             F#
Hoje eu tô mudado, todo mundo chamando pra foto
F#                    B           G#m     C#
Eu tô bem na parada, agora eu virei magnata

Refrão 3x:
F#                 C#
Mas é tudo emprestado
                 G#m
Ser pobre é bom de vez em quando
          B
Todo dia não tá dando
F#             C#
É tudo emprestado
            G#m
Aproveita que hoje eu tô pagando
           B
Amanhã o povo tá cobrando

Final: ( F# C# G#m B )
       ( F# )

----------------- Acordes -----------------
B = X 2 4 4 4 2
C# = X 4 6 6 6 4
D#m = X X 1 3 4 2
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
