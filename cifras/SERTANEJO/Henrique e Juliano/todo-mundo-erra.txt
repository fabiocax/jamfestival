Henrique e Juliano - Todo Mundo Erra

  G
Os Dias Passam Devagar
                 D/F#
E eu vou andando sem cansar
                Em
Não posso desistir de amar alguém
            C
Que eu tanto Esperei
G
Sei que tenho os meus defeitos
                     D/F#
mas entenda, ninguém é perfeito
                Em
Eu tenho tanto medo de ficar sozinho
 C
Outra vez

Pré-refrão:
      Am
Se um dia te fiz chorar
       D
Me perdoa paixão, não foi minha intenção

       Am
Não consigo mais viver assim
       D              C D
Sem você perto de mim

Refrão:
G               D/F#                Em
Eu Te Amo de Verdade pode acreditar
                C
Coração quer te ver voltar
G            D/F#        Em          C
Todo mundo erra e o meu erro foi não confiar
G                D/F#                  Em
Aprendi com a distância e com a solidão
                    C
Que a saudade pode machucar um coração
G            D/F#           Em      C      D      G
Dá mais uma chance pra gente viver essa louca paixão

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
