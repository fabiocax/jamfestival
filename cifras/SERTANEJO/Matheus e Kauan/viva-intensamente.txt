Matheus e Kauan - Viva Intensamente

Solo: A  E  F#m  D  A  E

  F#m                       D
Aprendi, que a vida bate sempre sem pudor,
         A
Que nem sempre as coisas são como agente pensa,
      E
Que nem sempre o mundo gira como agente quer.
  F#m                    D
Aprendi ,que se eu cair devo me levantar,
    A
Que todo mundo vive altos e baixos,
    E
Que não vale a pena esperar
  F#m                 D
Viva intensamente, desconfie do futuro,
                      A                    E
Siga em frente, basta acreditar, vale a pena sonhar.

    A                          E
Nunca desista vá em frente até acertar,

                                            F#m
E nunca deixe que o medo impeça de tentar,
                                         D
Leve na raça faça o que o coração mandar.
                                                       Solo: A  E  F#m  D
Não deixe nada para depois , não da para esperar.  (2x)

  F#m              D
De valor a todo instante que você viver,
   A
A todo mundo que te quer o bem,
   E
Como se não houvesse o amanhã.
  F#m               D
Não duvide, não tenha medo de se arriscar.
   A                                E
Enfrente tudo e venha o que vier, conte com a sorte para te ajudar
     F#m             D
Viva intensamente, desconfie do futuro,
                 A                        E
Siga em frente, basta acreditar, vale a pena sonhar.

   A                           E
Nunca desista vá em frente ate acertar,
                                          F#m
E nunca deixe que o medo impeça de tentar,
                                         D
Leve na raça faça o que o coração mandar

                                                       Solo: A  E  F#m  D
Não deixe nada para depois , não da para esperar.  (3x)
 D                 E                  A
Não deixe nada para depois, não da para esperar.  (2x)

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
