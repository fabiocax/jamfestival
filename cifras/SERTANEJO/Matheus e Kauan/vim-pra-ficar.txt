Matheus e Kauan - Vim Pra Ficar

Introdução: F     C     Am

F                                     C
Quando é amor a gente sabe,
Am
Pena que a paixão confunde
                     C
os nossos olhares

F                                    C
Quando é amor a gente sente,
Am
pena que a paixão confunde
                      C
o coração da gente

                    F
O amor é só um
                      C
E o resto é conversa
                      F
O amor não machuca, traz paz te acalma

                      C
E nunca tem pressa

                   F
O amor é só um
                    C
E o resto é conversa
                     F
O amor não machuca, traz paz e te acalma
                      C
E nunca tem pressa

                       F
Mas quando nossa vez chegar
                C
Você vai saber só de olhar
               Am
Então vai sorrir
                                                  G
E torcer pro tempo passar bem devagar


                                 F
O beijo vai calar nós dois,
                 C
Deixar pra pensar só depois
                Am
Suas aventuras não te levarão
                         G
Pra nenhum lugar
                                                      F     G     Am
Sou seu amor e dessa vez vim pra ficar ah ah ah ah

F    G   Am   G

                            F
O amor é só um
                      C
E o resto é conversa
                      F
O amor não machuca, traz paz te acalma
                      C
E nunca tem pressa

                   F
O amor é só um
                    C
E o resto é conversa
                     F
O amor não machuca, traz paz e te acalma
                      C
E nunca tem pressa

                       F
Mas quando nossa vez chegar
                C
Você vai saber só de olhar
               Am
Então vai sorrir
                                                  G
E torcer pro tempo passar bem devagar


                                 F
O beijo vai calar nós dois,
                 C
Deixar pra pensar só depois
                Am
Suas aventuras não te levarão
                         G
Pra nenhum lugar

                       F
Mas quando nossa vez chegar
                C
Você vai saber só de olhar
               Am
Então vai sorrir
                                                  G
E torcer pro tempo passar bem devagar


                                 F
O beijo vai calar nós dois,
                 C
Deixar pra pensar só depois
                Am
Suas aventuras não te levarão
                         G
Pra nenhum lugar
                                                       F     G     Am
Sou seu amor e dessa vez vim pra ficar ah ah ah ah


F    G   Am   G

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
