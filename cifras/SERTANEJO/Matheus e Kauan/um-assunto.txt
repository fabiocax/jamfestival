Matheus e Kauan - Um Assunto

C
Tá rolando um assunto
G                      Am
Que você e seu novo namorado
Dm                         C
Já tem data marcada pra viagem
G
Numa praia vai ser seu noivado


C
Deve ter se esquecido
G                       Am
De mencionar que ainda me ama
Dm                                C
E que tem sentimentos, que a uma semana
G
Se deitou na minha cama

F                   G
Pra mim foi uma despedida

Am                   C
Pra você mais uma recaída
        F                   Dm
E quer saber, isso vai acontecer
              G
O resto da sua vida

           C                 G
Ele não tem culpa de você me amar
       Am                           F
Ele mal sabe que esse amor não vai durar
             C                      G
Ele é um cara bom que ama a pessoa errada
     Am                        G
E se amasse ele, não estaria deitada
       C
Em meus braços

C G Am F C G Am F

C
Deve ter se esquecido
G                       Am
De mencionar que ainda me ama
Dm                                C
E que tem sentimentos, que a uma semana
G
Se deitou na minha cama

F                   G
Pra mim foi uma despedida
Am                   C
Pra você mais uma recaída
        F                   Dm
E quer saber, isso vai acontecer
              G
O resto da sua vida

           C                 G
Ele não tem culpa de você me amar
       Am                           F
Ele mal sabe que esse amor não vai durar
             C                      G
Ele é um cara bom que ama a pessoa errada
     Am                        G
E se amasse ele, não estaria deitada
       C
Em meus braços

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
