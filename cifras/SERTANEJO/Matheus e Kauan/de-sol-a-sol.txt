﻿Matheus e Kauan - De Sol a Sol

Capo Casa 1

[Intro]  Bm

(Solo sem capotraste)

E|--6--3----------6--3----------|
B|-----------4------------4-----|
G|---5---5-----5---5---5----5---|
D|------------------------------|
A|------------------------------|
E|------------------------------|

 Bm
Você pensa muito
 G
Tem medo de tudo
     D
Mas tudo no nosso futuro
Eu vi quando te vi
        A
E nem precisei de Mãe de Ná


     Bm
Pra ver que você vai beijar minha boca
    G
Daqui um mês levar pra minha casa suas coisas
D
  Em um ano tem casório
    A
Já pode começar a pensar em quem vai convidar

                         Bm
Eu vou te amar de sol a sol
E não há nenhuma chance
                  A
De você fugir de mim
Eu sou o "a" do seu amor
            Em
E o "m" do seu fim
                  G       A
Você não viu mas eu já vi
                         Bm
Eu vou te amar de sol a sol
E não há nenhuma chance
                  A
De você fugir de mim
Eu sou o "a" do seu amor
            Em
E o "m" do seu fim
                  G      A
Você não viu mas eu já vi
Acredita em mim

[Solo]  Bm  A  Em  G  A

     Bm
Pra ver que você vai beijar minha boca
    G
Daqui um mês levar pra minha casa suas coisas
D
  Em um ano tem casório
    A
Já pode começar a pensar em quem vai convidar

                         Bm
Eu vou te amar de sol a sol
E não há nenhuma chance
                  A
De você fugir de mim
Eu sou o "a" do seu amor
            Em
E o "m" do seu fim
                  G       A
Você não viu mas eu já vi
                         Bm
Eu vou te amar de sol a sol
E não há nenhuma chance
                  A
De você fugir de mim
Eu sou o "a" do seu amor
            Em
E o "m" do seu fim
                  G      A
Você não viu mas eu já vi
Acredita em mim

[Solo]  Bm  A  Em  G  A

Acredita em mim

----------------- Acordes -----------------
Capotraste na 1ª casa
A*  = X 0 2 2 2 0 - (*A# na forma de A)
Bm*  = X 2 4 4 3 2 - (*Cm na forma de Bm)
D*  = X X 0 2 3 2 - (*D# na forma de D)
Em*  = 0 2 2 0 0 0 - (*Fm na forma de Em)
G*  = 3 2 0 0 0 3 - (*G# na forma de G)
