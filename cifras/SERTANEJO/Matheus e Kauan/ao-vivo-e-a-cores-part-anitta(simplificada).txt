Matheus e Kauan - Ao Vivo e a Cores (part. Anitta)

[Intro]  C  D  Em  G

[Primeira parte]
                   C              D
Por que você não sai daí e vem aqui?
          Em                       G
Pode invadir, pode chegar, pode ficar
         C               D         Em
No meu quarto, no meu abraço, apertado
         G                     C
Duvido que cê vai querer ir embora
        D
Não apavora
            Em                      G
Depois do amor a gente vai fazendo hora
         C          D
E eu não   aguento mais
       Em          G
Eu não    aguento mais

[Refrão]


         C             D
A tela fria desse celular
            Em                    G
Só ver sua foto não vai me esquentar
        C                  D
Amar você de longe é tão ruim
             Em            G
Te quero ao vivo e a cores aqui
   C  D  Em  G
Aqui

[Primeira parte]
                   C              D
Por que você não sai daí e vem aqui?
          Em                       G
Pode invadir, pode chegar, pode ficar
         C               D         Em
No meu quarto, no meu abraço, apertado
         G                     C
Duvido que cê vai querer ir embora
        D
Não apavora
            Em                      G
Depois do amor a gente vai fazendo hora
         C          D
E eu não   aguento mais
       Em          G
Eu não    aguento mais

[Refrão]

         C             D
A tela fria desse celular
            Em                    G
Só ver sua foto não vai me esquentar
        C                  D
Amar você de longe é tão ruim
             Em            G
Te quero ao vivo e a cores aqui
   C   D    Em  G
Aqui Iêêê, uôôôôô

   C   D    Em  G
Aqui Iêêê, uôôôôô

[Segunda parte]
       C             D
Então sai daí, sai daí, vem aqui, vem
Em            G
Invadir, invadir o meu quarto
 C              D
Meu abraço apertado, vem, vem,
   Em                     G
Duvido que cê vai querer sair

Por que você não sai daí,
 C             D
Sai daí, sai daí, vem aqui, vem
Em            G
Invadir, invadir o meu quarto
 C                 D
Meu abraço, meu abraço apertado
Em              G
Eu não aguento mais

[Refrão]

                 C             D
Olhar pra tela fria desse celular
            Em                    G
Só ver sua foto não vai me esquentar
        C                  D
Amar você de longe é tão ruim
             Em            G
Te quero ao vivo e a cores aqui
   C   D    Em  G
Aqui Iêêê, uôôôôô
   C   D    Em  G
Aqui Iêêê, uôôôôô

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 3 3
