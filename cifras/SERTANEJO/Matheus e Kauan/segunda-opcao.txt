Matheus e Kauan - Segunda Opção

 Cm          G#             D#   Bb        Cm
 Quer prioridade, exclusividade, tá de sacanagem?


Intro: Cm G# D# Bb (2x)


Refrão:
Cm      G#           D#          Bb          Cm
  Se você quiser voltar pra ser minha diversão
       G#          D#           Bb         Cm
 Minha fugida, meu lance, minha segunda opção. (2x)


Intro: Cm G# D# Bb


Cm          G#   D#            Bb        Cm
 Eu te dei amor,  foi só teu o meu coração
          G# D#            Bb          Cm
 E você voou, deixou minha vida sem chão.  (2x)



   Cm    G# D#                 Bb                 Cm
 Voou, voou, abandonou o nosso ninho foi se aventurar
         G# D#                    Bb                  Cm
 Voou, voou, pousando de galho em galho agora quer voltar.

 Cm          G#             D#   Bb        Cm
 Quer prioridade, exclusividade, tá de sacanagem?

Refrão:

      G#           D#          Bb          Cm
 Se você quiser voltar pra ser minha diversão
       G#          D#           Bb         Cm
 Minha fugida, meu lance, minha segunda opção. (2x)


Intro: Cm G# D# Bb (2x)


   Cm    G# D#                 Bb                 Cm
 Voou, voou, abandonou o nosso ninho foi se aventurar
         G# D#                    Bb                  Cm
 Voou, voou, pousando de galho em galho agora quer voltar.

 Cm          G#             D#   Bb        Cm
 Quer prioridade, exclusividade, tá de sacanagem?

Refrão:
      G#           D#          Bb          Cm
 Se você quiser voltar pra ser minha diversão
       G#          D#           Bb         Cm
 Minha fugida, meu lance, minha segunda opção. (3x)

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
D# = X 6 5 3 4 3
E = 0 2 2 1 0 0
G# = 4 3 1 1 1 4
