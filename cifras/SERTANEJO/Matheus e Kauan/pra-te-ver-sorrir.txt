Matheus e Kauan - Pra Te Ver Sorrir

Intro: G  D  Em  C

C                        D
Se quer jogar tudo pro alto
                 G
O problema é seu
                           Em
Não vou correr atrás de alguém
                  C
Que nunca mereceu
                        D
Se lembra dos momentos bons
              G
Que agente passou

E das lembranças
       Em
Que restaram desse nosso amor
C
Pense antes
      D                      G
De jogar o nosso amor pro espaço

                      Em
Você vai sentir muita falta
                  C
De um forte abraço
                       D
Mais se um dia sentir frio
              G
E se arrepender
                          Em
Pra sempre vou estar por perto
            D
Pra te aquecer

G                         D
Quem é que vai contar histórias
            Em
Pra você dormir
                      C
Quem é que vai te abraçar
                   G
Só pra te ver sorrir
                        D
E quem vai te levar pro mar
                   Em
Pra ver o por do sol
                     C
E quando decidir voltar
                G
Te espero no farol

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
