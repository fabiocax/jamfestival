Matheus e Kauan - Bebeu, Lembrou

[Intro] B  F#  G#m  E

B
Sua cara não engana
F#
Seus olhos não mentem
G#m
O cheiro de etanol exalando
E
E a vergonha a essa hora já era
B                                        F#
Coração tá no escuro e não tem pra onde ir
                        G#m
Arrumou outro pra se iludir
              E
Mas não adiantou
             B
Bebeu, lembrou
            F#
Chorou, ligou
        G#m                     E
Sou a saudade que você não evitou

             B
Bebeu, lembrou, xi!
           F#
Chorou, ligou
        G#m                       E
Essa chamada o coração não completou
             B
Bebeu, lembrou
            F#
Chorou, ligou
        G#m                    E
Sou a saudade que você não evitou
             B
Bebeu, lembrou, ê!
            F#
Chorou, ligou
        G#m                       E
Essa chamada o coração não completou
                           B  F#  G#m
Você nem vai ouvir o meu alô
          E                     B  F#  G#m  E
Sou a saudade que você não evitou
 B
Sua cara não engana
F#
Seus olhos não mentem
G#m
O cheiro de etanol exalando
E
E a vergonha a essa hora já era
B                                        F#
Coração tá no escuro e não tem pra onde ir
                        G#m
Arrumou outro pra se iludir
               E
Mas não adiantou
             B
Bebeu, lembrou
           F#
Chorou, ligou
        G#m                     E
Sou a saudade que você não evitou
             B
Bebeu, lembrou, xi!
           F#
Chorou, ligou
        G#m                       E
Essa chamada o coração não completou
             B
Bebeu, lembrou
           F#
Chorou, ligou
        G#m                     E
Sou a saudade que você não evitou
             B
Bebeu, lembrou
           F#
Chorou, ligou
        G#m                       E
Essa chamada o coração não completou
                           B  F#  G#m
Você nem vai ouvir o meu alô
          E                     B  F#  G#m
Sou a saudade que você não evitou
E                          B
Você nem vai ouvir o meu alô

----------------- Acordes -----------------
B = X 2 4 4 4 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
