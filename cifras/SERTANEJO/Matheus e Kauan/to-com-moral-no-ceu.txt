Matheus e Kauan - Tô Com Moral No Céu

[Introdção] B9  F#  E

[Solo Introdução]
E|----------------------------------------------------|
B|-----4---4--7\2-2------4---4--7/9-7-7/12------------|
G|-4-6---6-----------4-6---6--------------------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

[Primeira parte]

   B
Todo mundo falou:
     F#               E9
Não mexe com ela, não

Não vai, não
       B
Eu doido com ela
         F#                 E9
E a galera me dando pressão


Não vai, não
B                F#
  Eu quase desisti
        E9
Mas o amor gritou, gritou

Até ela me ouvir


[Pré-refrão]

       G#m
Quando o primeiro beijo dela
 F#              E
Resetou meu coração
          G#m
Eu já tinha beijado ela mil vezes
 F#        E
Na imaginação


[Refrão]

                 B9      F#
Tô com moral no céu, eu tô
             C#m                E
Tem um anjo me chamando de amor
                 B9      F#
Tô com moral no céu, eu tô
             C#m                E
Tem um anjo me chamando de amor
                 B9      F#
Tô com moral no céu, eu tô
             C#m                E
Tem um anjo me chamando de amor
                 B9      F#
Tô com moral no céu, eu tô
             A                  E
Tem um anjo me chamando de amor
Demorou, mas compensou

[Base do solo] B9  F#  A  E  B9  F#  A  E

[Solo]

Parte 1 de 2
E|-9/11-7-9~-7~-----11/12-11-9-9h11-------------------|
B|--------------9\------------------12~-14b16---------|
G|----------------------------------------------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

Parte 2 de 2
E|------------------11/12-11-9h11p9-------------------|
B|-14b16-12-14~-12~-----------------12~---------------|
G|----------------------------------------------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

[Primeira parte]

   B
Todo mundo falou:
     F#               E9
Não mexe com ela, não

Não vai, não
       B
Eu doido com ela
         F#                 E9
E a galera me dando pressão

Não vai, não
B                F#
  Eu quase desisti
        E9
Mas o amor gritou, gritou

Até ela me ouvir


[Pré-refrão]

       G#m
Quando o primeiro beijo dela
 F#              E
Resetou meu coração
          G#m
Eu já tinha beijado e9la mil vezes
 F#        E
Na imaginação


[Refrão]

                 B9      F#
Tô com moral no céu, eu tô
             C#m                E
Tem um anjo me chamando de amor
                 B9      F#
Tô com moral no céu, eu tô
             C#m                E
Tem um anjo me chamando de amor
                 B9      F#
Tô com moral no céu, eu tô
             C#m                E
Tem um anjo me chamando de amor
                 B9      F#  A  E
Tô com moral no céu, eu tô
                 B9      F#
Tô com moral no céu, eu tô
             C#m                E
Tem um anjo me chamando de amor
                 B9      F#
Tô com moral no céu, eu tô
             A                  E
Tem um anjo me chamando de amor
                 B9      F#
Tô com moral no céu, eu tô
             C#m                E
Tem um anjo me chamando de amor
                 B9      F#
Tô com moral no céu, eu tô
             A                  E
Tem um anjo me chamando de amor
                    B9
Demorou, mas compensou

----------------- Acordes -----------------
A = 5 7 7 6 5 5
B = 7 9 9 8 7 7
B9 = X 2 4 4 2 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
E9 = X 7 9 9 7 7
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
