Matheus e Kauan - Te Assumi Pro Brasil

Intro 2x: D9  E  F#m7(11)  E


Primeira Parte:

A9                    D9
   Ser feliz pra mim não custa caro
F#m7(11)           E
    Se você tá do lado

Eu me sinto tão bem

A9                 D9
   Você sempre me ganha na manha
F#m7(11)             E
    Que mistério cê tem

Pré-refrão:

A9                 D9
Arrumei a mala há mais de uma semana

 F#m7(11)
Só falta você me chamar
E
  Pra eu fugir com você
 A9                D9
Mudei meu status, já tô namorando
F#m7(11)
Antes de você aceitar
E                      D9
  Já te assumi pro Brasil
    E
Iê iêêê..

Refrão:

                D9
Por que te amo, eu não sei
 E             F#m7(11)     E
Mas quero te amar cada vez mais
               D9
O que na vida ninguém fez
 E                       F#m7(11)  E
Você fez em menos de um mês

                D9
Por que te amo, eu não sei
 E             F#m7(11)     E
Mas quero te amar cada vez mais
               D9
O que na vida ninguém fez
 E                       F#m7(11)
Você fez em menos de um mês

Intro 2x: D9  E  F#m7(11)  E

Pré-refrão:

A9                 D9
Arrumei a mala há mais de uma semana
 F#m7(11)
Só falta você me chamar
E
  Pra eu fugir com você
 A9                D9
Mudei meu status, já tô namorando
F#m7(11)
Antes de você aceitar
E                      D9
  Já te assumi pro Brasil
    E
Iê iêêê..

Refrão:

                D9
Por que te amo, eu não sei
 E             F#m7(11)     E
Mas quero te amar cada vez mais
               D9
O que na vida ninguém fez
 E                       F#m7(11)  E
Você fez em menos de um mês

                D9
Por que te amo, eu não sei
 E             F#m7(11)     E
Mas quero te amar cada vez mais
               D9
O que na vida ninguém fez
 E                       F#m7(11)
Você fez em menos de um mês

----------------- Acordes -----------------
A9 = X 0 2 2 0 0
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
F#m7(11) = 2 X 2 2 0 X
