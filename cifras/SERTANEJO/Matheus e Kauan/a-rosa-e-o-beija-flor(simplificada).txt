Matheus e Kauan - A Rosa e o Beija-flor

Intro: C  G  D

Primeira Parte:

  Em                      C
Quando você fica ao lado de uma pessoa
                             Em
E ela mesmo em silêncio lhe faz bem

Quando você fecha os olhos
C
E no pensamento está fotografado
              Em
O rosto desse alguém

                          C
E quando estiver no dia triste

Basta o sorriso dela
                  Em
Pra você ficar feliz

                         C
E quando se sentir realizado

E dizer que encontrou
                      Em   D
O bem que você sempre    quis

Pré-refrão:

  Em       C         G    D
Quando chorar de saudades
  Em       C       D
Quando morrer de ciúmes
    Em        C        G   D
E quando sua sensibilidade
Em     C         D
Identifica o perfume

Refrão:

             C                 G
Isso é amor,   tá rolando amor
                   D
É o encontro de metades
                   Em
A rosa e o beija flor
             C                 G
Isso é amor,   tá rolando amor
                   D
É o encontro de metades
                   Em
A rosa e o beija flor

             C                 G
Isso é amor,   tá rolando amor
                   D
É o encontro de metades
                   Em
A rosa e o beija flor
             C                 G
Isso é amor,   tá rolando amor
                   D
É o encontro de metades
                   Em     C
A rosa e o beija flor, oh oh
    G                      D
É amor, é o encontro de metades

Primeira Parte:

  Em                      C
Quando você fica ao lado de uma pessoa
                             Em
E ela mesmo em silêncio lhe faz bem

Quando você fecha os olhos
C
E no pensamento está fotografado
              Em
O rosto desse alguém

                          C
E quando estiver no dia triste

Basta o sorriso dela
                  Em
Pra você ficar feliz
                         C
E quando se sentir realizado

E dizer que encontrou
                      Em   D
O bem que você sempre    quis

Pré-refrão:

  Em       C         G    D
Quando chorar de saudades
  Em       C       D
Quando morrer de ciúmes
    Em        C        G   D
E quando sua sensibilidade
Em     C         D
Identifica o perfume

Refrão:

             C                 G
Isso é amor,   tá rolando amor
                   D
É o encontro de metades
                   Em
A rosa e o beija flor
             C                 G
Isso é amor,   tá rolando amor
                   D
É o encontro de metades
                   Em
A rosa e o beija flor

             C                 G
Isso é amor,   tá rolando amor
                   D
É o encontro de metades
                   Em
A rosa e o beija flor
             C                 G
Isso é amor,   tá rolando amor
                   D
É o encontro de metades
                   Em     C
A rosa e o beija flor, oh oh
    G                      D
É amor, é o encontro de metades

Em    C      G
Oh oh oh é amor
                   D
É o encontro de metades
               Em
A rosa e o beija flor

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
