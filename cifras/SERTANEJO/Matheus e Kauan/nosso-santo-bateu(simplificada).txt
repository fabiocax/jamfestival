Matheus e Kauan - Nosso Santo Bateu

Primeira Parte:

C
  As vezes acho que devia

Te dizer mais vezes
Em
   Que te amo e que te quero

Mais que qualquer coisa
F                            C
  Por essa noite vem morar aqui


Tem gente que chega e muda

Os planos da gente
Em
   E que faz a nossa vida


Caminhar pra frente
F                           C
  Agora sim eu sei pra onde ir


Pré-refrão:

F
  Dessa vida nada se leva
G
  E no fundo todo mundo espera
C
  Um amor que venha pra somar
Dm      F     G
   Pra completar

Refrã:

            F
O nosso santo bateu
                 G
O amor da sua vida sou eu
                      C
E tudo que é meu hoje é seu
                  F
E o fim nem precisa rimar


O nosso santo bateu
                 G
O amor da sua vida sou eu
                      C
E tudo que é meu hoje é seu
                  F       G  C  F
E o fim nem precisa rimar

Pré-refrão:


Dessa vida nada se leva
G
E no fundo todo mundo espera
C
Um amor que venha pra somar
  Dm  F     G
Pra  completar

Refrão:

            F
O nosso santo bateu
                 G
O amor da sua vida sou eu
                      C
E tudo que é meu hoje é seu
                  F
E o fim nem precisa rimar


O nosso santo bateu
                 G
O amor da sua vida sou eu
                      C
E tudo que é meu hoje é seu
                  F       G  F  G  C
E o fim nem precisa rimar

----------------- Acordes -----------------
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
