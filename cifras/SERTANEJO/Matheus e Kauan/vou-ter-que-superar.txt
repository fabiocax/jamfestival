﻿Matheus e Kauan - Vou Ter Que Superar

Capo Casa 2

[Intro] A9  B9  C#m

     C#m
Fui bobo
Imaturo demais
                              A9
Deixei escapar entre meus dedos
                                      E
Seu amor, nosso apartamento futuro perfeito
                                         B9
Se eu pudesse tentava de novo ter você aqui
         C#m
Quase morro

Tentando encontrar o contato novo
         A9
Dela agora
Eu sei que ela já tá em outra
                E
E que vai embora
                                                B9
Mas se existe consideração eu sei que vai me ouvir


                     A9
Só vou pedir desculpas
O nosso pra sempre acabou
                            C#m
E um novo amor veio com tudo
                                                        A9
Meu erro pesou na balança e não vamos mais ficar juntos
                         B9
Por que eu estraguei tudo
A9
Vou ter que superar
                           B9
Suas fotos com ele viajando
               G#m                          C#m
E as amigas comentando que são um belo casal
A9
Vou ter que superar
                  B9
O dia do casamento
                   G#m                         C#m
Esperando que o tempo aos poucos possa me curar
A9
Vou ter que superar
B9                           C#m
Que a gente não vai mais voltar

( B9  C#  D#m )

                    B9
Só vou pedir desculpas
O nosso pra sempre acabou
                           D#m
E um novo amor veio com tudo
                                             B9
Meu erro pesou na balança e não vamos mais ficar juntos
                         C#
Por que eu estraguei tudo
B9
Vou ter que superar
                         C#
Suas fotos com ele viajando
                A#m                       D#m
E as amigas comentando que são um belo casal
B9
Vou ter que superar
                C#
O dia do casamento
                   G#m
Esperando que o tempo aos poucos possa me curar
B9
Vou ter que superar
C#                           D#m
Que a gente não vai mais voltar

----------------- Acordes -----------------
Capotraste na 2ª casa
A#m*  = X 1 3 3 2 1 - (*Cm na forma de A#m)
A9*  = X 0 2 2 0 0 - (*B9 na forma de A9)
B9*  = X 2 4 4 2 2 - (*C#9 na forma de B9)
C#*  = X 4 6 6 6 4 - (*D# na forma de C#)
C#m*  = X 4 6 6 5 4 - (*D#m na forma de C#m)
D#m*  = X X 1 3 4 2 - (*Fm na forma de D#m)
E*  = 0 2 2 1 0 0 - (*F# na forma de E)
G#m*  = 4 6 6 4 4 4 - (*A#m na forma de G#m)
