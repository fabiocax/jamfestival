Felipe Araújo - Aerocorpo (part. Léo Santana)

[Intro] F  G  Am  C9

   F          G             Am          C9
E|-------1----3-------------------0-----3-------------|
B|-------1----3--3/5-3~-----------1-----3-------------|
G|-------2----4-----------------2-------0-------------|
D|-----3------5---------------2---------2-------------|
A|---3----------------------0-----------3-------------|
E|-1--------------------------------------------------|

[Primeira Parte]

Am              F
O seu abraço é tão gostoso
                   C9
Já que cê não sente
            G
Da ele pra mim

Deixa ele pra mim


Am              F
Esse seu beijo zica
                     C9
Se encostar na minha   boca
        G
Bate e fica

Sempre bate e fica

Am                     F
Esse sorriso é arma letal

Tava na reta

Me atingiu primeiro
C9                     G
 Me derrubou quando abriu

Já quero o ano inteiro

[Pré-Refrão]

 Dm
Você sem roupa
F
É brisa boa
 C                   G
Viagem pro resto da vida
              F   G
Pro resto da vida

[Refrão]

   F
Aqui ou lá
         G
Até sem decolar
       Am
Dou a volta no mundo
      C
Se a nave for seu corpo

    F
Aqui ou lá
             G
Se acaso cê voar
    Am
Cê volta pra pousar
         C
Aqui no meu aerocorpo

   F
Aqui ou lá
         G
Até sem decolar
       Am
Dou a volta no mundo
      C
Se a nave for seu corpo

    F
Aqui ou lá
             G
Se acaso cê voar
    Am
Cê volta pra pousar
         C
Aqui no meu aerocorpo

[Primeira Parte]

Am                     F
Esse sorriso é arma letal

Tava na reta

Me atingiu primeiro
C9                     G
 Me derrubou quando abriu

Já quero o ano inteiro

[Pré-Refrão]

 Dm
Você sem roupa
F
É brisa boa
 C                   G
Viagem pro resto da vida
              F   G
Pro resto da vida

[Refrão]

   F
Aqui ou lá
         G
Até sem decolar
       Am
Dou a volta no mundo
      C
Se a nave for seu corpo

    F
Aqui ou lá
             G
Se acaso cê voar
    Am
Cê volta pra pousar
         C
Aqui no meu aerocorpo

[Final] F  G  Am  C9

   F          G             Am          C
E|-------1----3-------------------0-----3-------------|
B|-------1----3--3/5-3~-----------1-----3-------------|
G|-------2----4-----------------2-------0-------------|
D|-----3------5---------------2---------2-------------|
A|---3----------------------0-----------3-------------|
E|-1--------------------------------------------------|

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
