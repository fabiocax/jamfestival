﻿Felipe Araújo - Alcontece

Capo Casa 1

[Intro] Am  F  Am  F  E
        Am  F  Am  F  E

Am                F
1, 2, 3 cheguei pra fazer a festa
 Am                F            E
Já liguei pra ex, hoje vai dar merda

 F                G
Desce um combo, que a sede tem pressa
 F                     E
Hoje tem cachaça, amanhã tem amnésia

 Am                  F
Se eu chegar às seis é coisa da vida
 C                          G
Se eu subir na mesa isso é culpa da bebida
 Am                          F
Se eu beijar de três, por favor
Ninguém me filma
E
E se der B.O. bota a culpa na bebida


 Am              F                       C
Na balada é amorzinho, amanhã cê nem conhece
        G          Am
Alcontece, alcontece
                    F                       C
Dormir com a princesinha e acordar com o shrek
        G          Dm
Alcontece, alcontece
              F                       E  F  E  F  E  F
Para na cama da ex e põe culpa no gps
                   Am
Alcontece, alcontece

( Am  F )
( Am  F  E )
( Am  F )
( Am  F  E )

Am                F
1, 2, 3 cheguei pra fazer a festa
 Am                F            E
Já liguei pra ex, hoje vai dar merda

 F                G
Desce um combo, que a sede tem pressa
 F                     E
Hoje tem cachaça, amanhã tem amnésia

 Am                  F
Se eu chegar às seis é coisa da vida
 C                          G
Se eu subir na mesa isso é culpa da bebida
 Am                          F
Se eu beijar de três, por favor
Ninguém me filma
E
E se der B.O. bota a culpa na bebida

 Am              F                       C
Na balada é amorzinho, amanhã cê nem conhece
        G          Am
Alcontece, alcontece
                    F                       C
Dormir com a princesinha e acordar com o shrek
        G          Dm
Alcontece, alcontece
              F                       E  F  E  F  E  F
Para na cama da ex e põe culpa no gps
                   Am
Alcontece, alcontece

 Am              F                       C
Na balada é amorzinho, amanhã cê nem conhece
        G          Am
Alcontece, alcontece
                    F                       C
Dormir com a princesinha e acordar com o shrek
        G          Dm
Alcontece, alcontece
              F                       E  F  E  F  E  F
Para na cama da ex e põe culpa no gps
                   Am
Alcontece, alcontece

( Am  F )
( Am  F  E )
( Am  F )
( Am  F  E )
( Am )

----------------- Acordes -----------------
Capotraste na 1ª casa
Am*  = X 0 2 2 1 0 - (*A#m na forma de Am)
C*  = X 3 2 0 1 0 - (*C# na forma de C)
Dm*  = X X 0 2 3 1 - (*D#m na forma de Dm)
E*  = 0 2 2 1 0 0 - (*F na forma de E)
F*  = 1 3 3 2 1 1 - (*F# na forma de F)
G*  = 3 2 0 0 0 3 - (*G# na forma de G)
