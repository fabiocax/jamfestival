Felipe Araújo - Ainda Sou Tão Seu

[Intro] D  A  Bm  G

D
 Era mais fácil ter beijado aquela boca
Mais cedo na balada
G
 Era mais fácil ter ficado com a minha turma
Só enchendo a cara
D
 Porque que eu não quis passar meu telefone
Pra ninguém, outra vez
G
 Porque que eu corri pro banheiro
Quando alguém falou seu nome
Bm
E chorei, chorei
   A                                     G
Parece que eu ainda não to pronto pra beber
Pra sair, pra viver

D
Ainda sou tão seu

                     A
Adivinha quem ainda não te esqueceu
             Bm
Nem meu coração nem eu
             G
Nem meu coração nem eu

 D
Ainda sou tão seu
                     A
Adivinha quem ainda não te esqueceu
             Bm
Nem meu coração nem eu
             G
Nem meu coração nem eu
Nem meu coração nem eu

( D  A  Bm  G )

      Bm
E chorei, chorei
   A                                     G
Parece que eu ainda não to pronto pra beber
Pra sair, pra viver

 D
Ainda sou tão seu
                     A
Adivinha quem ainda não te esqueceu
             Bm
Nem meu coração nem eu
             G
Nem meu coração nem eu

 D
Ainda sou tão seu
                     A
Adivinha quem ainda não te esqueceu
             Bm
Nem meu coração nem eu
             G
Nem meu coração nem eu
                    D
Nem meu coração nem eu

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
G = 3 2 0 0 0 3
