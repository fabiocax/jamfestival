Felipe Araújo - Noite Fracassada

[Intro] Em  G  D  A

E|---------------------------------|
B|-5/7/5~--5/7/5~--7/10~--10/5~----|
G|---------------------------------|
D|---------------------------------|
A|---------------------------------|
E|---------------------------------|

E|-------------------------|
B|-------------------------|
G|-2h4p2-------------------|
D|--------4-2-2-0--2-4-0~--|
A|-------------------------|
E|-------------------------|

D
Outra noite fracassada sem você do lado
 G                             Bm
Tô saindo da boate todo incomodado
       A
Tá na cara que não te esqueci


 D                                 G
Na saída, um casal se beijando na rua
                                     Bm
Até pareceu nós dois, bateu saudade sua
       A
Quase não reconheci

  Em
Até assustei
     G                         D
E o copo deixei cair da minha mão
                      A
Era você beijando outro sem parar

    Em
Por isso, eu liguei
    G                          D
Melhor esquecer a última mensagem que acabei de enviar
    A
Melhor desconsiderar

    Em                                  G
Esquece toda essa besteira que você já leu
                                    D
Não tá mais aqui o trouxa que escreveu
                                  A
E a culpa é da bebida, foi só recaída
Pelo amor de Deus

    Em
Esquece
                                 G
Percebi que tá bem melhor do que eu
                                       D
Esse beijo que deu nele "cê" nunca me deu
                          A
Continua o que "cê" tá fazendo
"Cê" vai beijando, eu vou bebendo

( Em  G  D  A )

  Em
Até assustei
     G                         D
E o copo deixei cair da minha mão
                      A
Era você beijando outro sem parar

    Em
Por isso, eu liguei
    G                          D
Melhor esquecer a última mensagem que acabei de enviar
    A
Melhor desconsiderar

    Em                                  G
Esquece toda essa besteira que você já leu
                                    D
Não tá mais aqui o trouxa que escreveu
                                  A
E a culpa é da bebida, foi só recaída
Pelo amor de Deus

    Em
Esquece
                                 G
Percebi que tá bem melhor do que eu
                                       D
Esse beijo que deu nele "cê" nunca me deu
                          A
Continua o que "cê" tá fazendo
"Cê" vai beijando, eu vou bebendo

    Em                                  G
Esquece toda essa besteira que você já leu
                                    D
Não tá mais aqui o trouxa que escreveu
                                  A
E a culpa é da bebida, foi só recaída
Pelo amor de Deus

    Em
Esquece
                                 G
Percebi que tá bem melhor do que eu
                                       D
Esse beijo que deu nele "cê" nunca me deu
                          A
Continua o que "cê" tá fazendo
"Cê" vai beijando, eu vou bebendo

( Em  G  D  A  Em )

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
