Felipe Araújo - Como Eu Te Amo / Tudo de Novo (Pot-Pourri)

[Intro] E  B  A  B
        E  B  C#m  B  A  B

                            E   A
Tão perto e tão distante de mim
E                        B
 É certo que eu me apaixonei
C#m             G#m     C#m
 E tudo fiz por essa paixão
                G#m          A
Eu me arrisquei demais na ilusão
  F#m         B
E você me queria também

E             A             E
 Você é tão fulgaz nos sentimentos
E                           B
 Com essa indiferença você teima em machucar
C#m             G#m      C#m
 Estamos quase a beira do fim
                G#m       A
Por que você não olha pra mim

      F#m         B
Põe de vez essa cabeça no lugar

             E          B      C#m
Só quero te dizer que o meu coração
       G#m        A
Está pedindo atenção
        F#m         B
Como eu sinto a sua falta
             E             B      C#m
Só quero te dizer que isso pode mudar
        G#m          A       G#m      F#m
Basta a gente se acertar e viver esse amor
           B
Como eu te amo
              E
Não vivo sem você

( B  E  A  E )

A
Eu quero tudo de novo
                 A7                   D    D4  D
Sentir seu corpo suado e molhado de beijos
         A
Eu quero sempre mais
           E
Ah como eu quero mais

A
 Eu quero tudo de novo
A7                                   D   D4  D
Olhos nos olhos queimando de tanto desejo
         A
Eu quero sempre mais
           E
Ah como eu quero mais
   A   A7
Você

 D                                         A   A7
A roupa que eu quero vestir essa noite é você
D                                     A    A7
Pra descobrir as estrelas do nosso prazer
D                   E
Como se o tempo e a vida
C#m                  F#m
Ficassem parados pra nós
Bm             E                   A  A7  (A  E)
Numa viajem de baixo dos nossos lençóis

A
Eu quero tudo de novo

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
D4 = X X 0 2 3 3
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
