Felipe Araújo - Não Te Esqueci Trem

[Intro] Em  C  G  D

E|---------------------------------------------|
B|------------------------5-5-7/8-7-5---5------|
G|-2h4-4-4-4-2-0---0--4-4-------------7---5~---|
D|---------------4-----------------------------|
A|---------------------------------------------|
E|---------------------------------------------|

                     Em
Voltei pra casa sem vontade
                      C
Bati de frente com a saudade
                        G
São tantas marcas de você aqui
                        D
Eu nem cheguei e já quero sair

                        Em
Achei que tava te esquecendo
Beijando, bebendo

                 C
Eu nem tava lembrando naquele momento
          G
Mas a noite passou e olha quem voltou
         B7
De mãos dadas com a ressaca
Olha eu de novo amor

 Em
Já tentei, tomei tudo que é pinga
              C
Os tombos que eu levei serviu pra aprender
             G
Que igual você não tem
                   D
Ainda não te esqueci, trem
Ainda não te esqueci

 Em
Já tentei, tomei tudo que é pinga
              C
Os tombos que eu levei serviu pra aprender
             G
Que igual você não tem
                   D
Ainda não te esqueci, trem
Ainda não te esqueci

( Em  C  G  D )

                        Em
Achei que tava te esquecendo
Beijando, bebendo
                 C
Eu nem tava lembrando naquele momento
          G
Mas a noite passou e olha quem voltou
         B7
De mãos dadas com a ressaca
Olha eu de novo amor

 Em
Já tentei, tomei tudo que é pinga
              C
Os tombos que eu levei serviu pra aprender
             G
Que igual você não tem
                   D
Ainda não te esqueci, trem
Ainda não te esqueci

 Em
Já tentei, tomei tudo que é pinga
              C
Os tombos que eu levei serviu pra aprender
             G
Que igual você não tem
                   D
Ainda não te esqueci, trem
Ainda não te esqueci

 Em
Já tentei, tomei tudo que é pinga
              C
Os tombos que eu levei serviu pra aprender
             G
Que igual você não tem
                   D
Ainda não te esqueci, trem
Ainda não te esqueci

( Em  C  G )

                   D
Ainda não te esqueci trem
                   Em
Ainda não te esqueci trem

----------------- Acordes -----------------
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
