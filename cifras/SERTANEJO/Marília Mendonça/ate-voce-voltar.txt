Marília Mendonça - Ate Você Voltar

[Intro] A

   A
Aqui sentado nessa mesa
                 E
Só o copo de cerveja é minha companhia
                       F#m
E essa casa está tão cheia
                           D
E parece vazia sem você comigo

   A
E hoje está fazendo um ano
                  E
Aqui no meu calendário
Ainda está marcando
               F#m
O dia e o mês,     foi a primeira vez
E o que me prometeu
D
  Será que se esqueceu


    F#m
De todos nossos planos
Nossos filhos, nosso apartamento
C#m
    Da nossa lua de mel
Do nosso casamento
Bm
   Como pude acreditar
               D
Nesse seu juramento
                 Dm
E agora estou sozinho outra vez

A
  De copo sempre cheio, coração vazio
E
  Tô me tornando um cara
                 F#m
Solitário e frio
                                    D
Vai ser difícil eu me apaixonar de novo
     E
E a culpa é sua

A                               E
  Antes embriagado do que iludido
Acreditar no amor
                    F#m
Já não faz mais sentido
                                D
Eu vou continuar nessa vida bandida
  E          A  E  Bm  D
Até você voltar

----------------- Acordes -----------------
Capotraste na 1ª casa
A*  = X 0 2 2 2 0 - (*A# na forma de A)
Bm*  = X 2 4 4 3 2 - (*Cm na forma de Bm)
C#m*  = X 4 6 6 5 4 - (*Dm na forma de C#m)
D*  = X X 0 2 3 2 - (*D# na forma de D)
Dm*  = X X 0 2 3 1 - (*D#m na forma de Dm)
E*  = 0 2 2 1 0 0 - (*F na forma de E)
F#m*  = 2 4 4 2 2 2 - (*Gm na forma de F#m)
