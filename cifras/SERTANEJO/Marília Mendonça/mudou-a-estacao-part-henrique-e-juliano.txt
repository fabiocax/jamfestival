Marília Mendonça - Mudou a Estação (part. Henrique e Juliano)

A                    C#m
Eu me apaixonei por um alguém
                       F#m
Que nunca quis amar ninguém
                      D
Eu mesmo me fiz seu refém, me prendi

A                      C#m
Eu, que era livre pra voar
                       F#m
Cortei as minhas próprias asas
                     D
Fiz de você a minha casa, fiquei aqui

(C#m)  Bm                      (A/C#)  D
   Com você eu fui sentindo o frio chegando
              (A/C#)  Bm
E o seu perfume acabando
                                          E
Eu me refiz pra não ter que ver você me deixando


A                      C#m
Não sei se mudou a estação
                     F#m
Sei que não é mais verão
                        D
Estou deixando o nosso ninho
                 A
Decidi voar sozinho

                     C#m
Vou em busca de outro amor
                            F#m
Não tem mais doce a nossa flor
                 D
Eu preciso de carinho
                     A
Nem que seja um pouquinho


Em                            (D/F#)   G
Com você eu fui sentindo o frio chegando
               (D/F#)  Em
E o seu perfume acabando
                                            A
Eu me refiz pra não ter que ver você me deixando

D                      F#m
Não sei se mudou a estação
                     Bm
Sei que não é mais verão
                        G
Estou deixando o nosso ninho
                  D
Decidi voar sozinho

                     F#m
Vou em busca de outro amor
                           Bm
Não tem mais doce a nossa flor
                 G
Eu preciso de carinho
                     D
Nem que seja um pouquinho


D                      F#m
Não sei se mudou a estação
                     Bm
Sei que não é mais verão
                        G
Estou deixando o nosso ninho
                 D
Decidi voar sozinho

                     F#m
Vou em busca de outro amor
                            Bm
Não tem mais doce a nossa flor
                 G
Eu preciso de carinho
                     D
Nem que seja um pouquinho

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
