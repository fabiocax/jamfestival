Marília Mendonça - Perto de Você

[Intro] G  Em  Bm  A

E|-----------------------------------------------------|
B|-----------------------------------------------------|
G|-----------------------------------------------------|
D|---7---7---7--7h9---------7---7---7-----7---7---7----|
A|-----9---9---------5-7~-----9---9---9-----9---9---7~-|
E|-----------------------------------------------------|

E eu que já me acostumei, tão bem
A      Bm                  D
É normal acordar todos os dias
                A
Pra cuidar de você
                 G   Em        D
Mas você não percebe e vive falando mal
         A
E mesmo sem saber

                   Em
Mas não consigo ir embora mais

                 G
A cada passo pra frente é o mesmo que dar dois pra trás
               D                      A
Pra perto de você, pra mais perto de você

               G              Em
É melhor aguentar seus gritos
              Bm                  A
Do que me afastar e nunca mais voltar
              G          Em
O que eu acho impossível
                    Bm                 A
Mas e se a porta fechar e não se abrir mais

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
