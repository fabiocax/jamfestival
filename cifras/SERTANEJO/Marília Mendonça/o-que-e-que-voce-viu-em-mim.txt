Marília Mendonça - O Que É Que Você Viu Em Mim

D
Estamos lado a lado nessa cama
                               G     G D
Só que hoje eu não vou querer dormir
Em                         G
Dessa vez não deu dor de cabeça,
                 A
Desliguei o celular
                         D   G D
Que surpresa você por aqui.

D
Mas surpresa de verdade foi o que
eu senti
                               G    G D
Se o mais dedicado sempre foi você
Em                            G
De repente eu vi tudo se inverter
                            A
Lembrei do quanto eu te magoei
                D
Deu um nó na garganta

             A
E a lágrima caiu

Em            G                  D
Vejo você perdoando as minhas mentiras
                              A
logo você que teve o pior de mim
Em                    G
Eu sei que errar é humano
          D                      A
Mas me aceitar depois de tudo é amor
Em
Uôuô

                      G
O que é que você viu em mim
                     D
Porque ainda me olha assim
                       A
Tem tanta gente interessante
                       Em
Porque que sou tão importante
                      G
O que é que você viu em mim
                     D
Porque ainda me ama assim
                       A
E ainda me beija como antes
                 Em
Você nunca quis ficar


                      G
O que é que você viu em mim
                     D
Porque ainda me olha assim
                       A
Tem tanta gente interessante
                       Em
Porque que sou tão importante
                      G
O que é que você viu em mim
                     D
Porque ainda me ama assim
                       A
E ainda me beija como antes
                 Em
Você nunca quis ficar distante

                      G      D
O que é que você viu em mim

----------------- Acordes -----------------
Capotraste na 3ª casa
A*  = X 0 2 2 2 0 - (*C na forma de A)
D*  = X X 0 2 3 2 - (*F na forma de D)
Em*  = 0 2 2 0 0 0 - (*Gm na forma de Em)
G*  = 3 2 0 0 0 3 - (*A# na forma de G)
