Marília Mendonça - De Sexta a Sexta

[Primeira Parte]

        C9               D4
Coloca aí, a música que gosta
           G
No volume máximo
                   Em7
E faz silêncio, encosta
               C9
E do resto eu cuido
              D4
Deita do meu lado
                G
Sem ar-condicionado
Era um vez
    Em7
Um coração gelado

[Pré-Refrão]

 C9              D4
Toma um gole de vida

                   G
E recarrega a bateria
             Em7
Menos o celular
                    C9
Que aqui cê não precisa
          Cm
Sentindo só a brisa, respira

[Refrão]

             C9
Eu já me cansei
               D4
De amores de quinta
               G       D4/F#  Em7
Que acabam na segunda-feira
                   Am7
Se é pra amar que seja
                C9
A semana inteira
De sexta á sexta
   G
Menos frescura
              Em7
E mais besteira

             C9
Eu já me cansei
               D4
De amores de quinta
               G          Em7
Que acabam na segunda-feira
                   Am7
Beijo no rosto, selinho, preliminar
    C9
Da pausa e continua
          G
Quando a música acabar
Em7                   Am7
   Beijo no rosto, selinho, preliminar
    C9
Da pausa e continua

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
C9 = X 3 5 5 3 3
Cm = X 3 5 5 4 3
D4 = X X 0 2 3 3
D4/F# = 2 X X 2 3 3
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
