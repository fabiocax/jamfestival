Marília Mendonça - Aceita Que Dói Menos

Introdução: A7  D  E  A  E  D  A  D  A

Primeira Parte:
                A
Tô aqui jogado fora
                E
E ela não quer mais me ver
                  Bm
Vacilei pisei na bola
              E
E ela me deu fora
                  A  D A
Tô perdido sem você

                     A
Quem me amava de verdade
             E
Se cansou e bateu asas
                       Bm
Já tem alguém que te adora
                     E
Enquanto busquei lá fora

                      A   A7
O que eu já tinha em casa

Refrão:
                  D
De tanta saudade dela
Chorando eu liguei pra ela
                        A
Pra dizer que estou sofrendo
                   E
Insisti e ela atendeu
Sorrindo me respondeu
  D         A         A7
Aceita que dói menos

                  D
De tanta saudade dela
Chorando eu liguei pra ela
                        A
Pra dizer que estou sofrendo
                   E
Insisti e ela atendeu
Sorrindo me respondeu
  D         A        A7
Aceita que dói menos

( D E A E D A D A )

Segunda Parte:
                  A
Tenho tanta liberdade
                    E
Mas outro amor pra mim não serve
                Bm
É como diz o ditado
                    E
Quem está do nosso lado
                         A    A7
Só tem valor depois que perde

Refrão:
                  D
De tanta saudade dele
Chorando eu liguei pra ele
                        A
Pra dizer que estou sofrendo
                   E
Insisti e ele atendeu
Sorrindo me respondeu
  D         A        A7
Aceita que dói menos

                  D
De tanta saudade dele
Chorando eu liguei pra ele
                        A
Pra dizer que estou sofrendo
                   E
Insisti e ele atendeu
Sorrindo me respondeu
  D         A
Aceita que dói menos
E             A
  Aceita que dói menos

E             A
  Aceita que dói menos

Final: D  A  D  A

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
