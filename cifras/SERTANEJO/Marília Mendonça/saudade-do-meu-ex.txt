Marília Mendonça - Saudade do Meu Ex

[Introdução] A9  B9  C#m

[Solo]
E|------------------------------------------4----------|
B|-----4-5p4----------4-4-4--5-4--5-4--5-4--5-7-5------|
G|-6-6-------6-4-2----4-4-4--6-4--6-4--6-4----6-6------|
D|-----------------------------------------------------|
A|-----------------------------------------------------|
E|-----------------------------------------------------|

[Primeira Parte]

C#m
Ai, o que é que eu fiz? Me enrolei

Que saudade do meu ex

Onde eu tava com a cabeça
                    F#m
Quando eu fui me meter


Com essa pessoa tão sem graça
        C#m
Esse menino nada a ver

É faculdade, academia

Hora certa pra dormir

Eu bebia todo dia
                   F#m
Hoje eu mal posso sair
                            C#m
Ele regra tudo o que vou fazer


[Segunda Parte]

F#m
    Terminei com o parceiro
              C#m
Que virava o copo
                        B/D   E
Bebia comigo do jeito que eu gosto
                         B9
Ele que era homem de verdade
G#7                    C#m
Ai, que saudade do meu ex


[Refrão]

Desconfia
                         A9
Agora eu faço o que eu quero
        B9
Tô nem aí
                       C#m
Se tá com vergonha de mim

Se quer saber
                      A9
Hoje eu vou beber de novo
                     B9
Vou voltar pra casa louca
             C#m
Pra você largar de mim

Desconfia
                         A9
Agora eu faço o que eu quero
        B9
Tô nem aí
                       C#m
Se tá com vergonha de mim

Se quer saber
                      A9
Hoje eu vou beber de novo
                     B9
Vou voltar pra casa louca
             C#m
Pra você largar de mim

                       A9
Ai, que saudade do meu ex
B9                        C#m
   Ai, que saudade do meu ex
                       A9
Ai, que saudade do meu ex
             B9          C#m
Ele que era homem de verdade


[Repete a Segunda Parte]

F#m
    Terminei com o parceiro
              C#m
Que virava o copo
                        B/D   E
Bebia comigo do jeito que eu gosto
                         B9
Ele que era homem de verdade
G#7                    C#m
Ai, que saudade do meu ex


[Refrão]

Desconfia
                         A9
Agora eu faço o que eu quero
        B9
Tô nem aí
                       C#m
Se tá com vergonha de mim

Se quer saber
                      A9
Hoje eu vou beber de novo
                     B9
Vou voltar pra casa louca
             C#m
Pra você largar de mim

Desconfia
                         A9
Agora eu faço o que eu quero
        B9
Tô nem aí
                       C#m
Se tá com vergonha de mim

Se quer saber
                      A9
Hoje eu vou beber de novo
                     B9
Vou voltar pra casa louca
             C#m
Pra você largar de mim


A9         B9
   Tô nem aí
                       C#m
Se tá com vergonha de mim

Se quer saber
                      A9
Hoje eu vou beber de novo
                     B9
Vou voltar pra casa louca
             C#m
Pra você largar de mim

                         A9
Agora eu faço o que eu quero
        B9
Tô nem aí
                       C#m
Se tá com vergonha de mim

Se quer saber
                      A9
Hoje eu vou beber de novo
                     B9
Vou voltar pra casa louca
         C#m
Pra você

                       A9
Ai, que saudade do meu ex
B9                        C#m
   Ai, que saudade do meu ex
                       A9
Ai, que saudade do meu ex
             B9          C#m
Ele que era homem de verdade

Ai, o que é que eu fiz? Me enrolei
Que saudade do meu ex

----------------- Acordes -----------------
A9 = X 0 2 2 0 0
B/D = X 5 4 4 4 X
B9 = X 2 4 4 2 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#7 = 4 6 4 5 4 4
