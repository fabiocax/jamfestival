Milionário e José Rico - Meu Martírio

         G        C         D
É um martírio gostar de alguém
     G                 D
Que vive nos braços de outro
         C                        G
Este alguém já foi meu amo........or
         G         C     D
Ao recordar que me acariciavas
  G                    D
Quando estavas ao meu lado
        C       D        G
Os meus olhos choram de dor

            D
Tu és para mim
   C                  G
A minha mulher ama...da
           D
E eu para ti
       C                        G
Sou o homem que tu despre......za


Peço desculpa
    C       D        G
Se tu me ouvires cantando
           D
Que eu te amo
          C        D       G
Pois meu vício és tu meu amor


          G        C
Se as estrelas do céu
      D           G
Me fizessem um milagre
               D
E os anjos também
       C                    G
Eu queria que ela volta...sse

E como outrora
   C        D        G
Seriamos felizes na vida
         D
Muito felizes
        C     D       G
E só a morte nos separasse

Refrão

Batida: òòò(valsa)

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
