Milionário e José Rico - Águas Passadas

INTRO: A7 D Em A7 D (2X)

          D      D7+
Eu já nem sei se sou
   D D7+             Em A7  Em A7
O homem ideal de sua vida
                  Em      A7
Se marcou, se marquei, nem sei
      A7                     D
Me perdoe se não sou fiel contigo

             D
Muitas vezes eu menti a mim mesmo
      D7                 G
Que enfim consegui te esquecer
                     D  Bm
Foi tão forte a solidão
                 Em           A7           D     ( D )
Esse meu pobre coração quase ficou de mal comigo

REFRÃO:

         A7                D
Águas passadas não rodam moinhos
          A7     Em     D
É o que sempre escuto dizer
               A7                    D
Se você não passou, eu também não passei
                   G           Em     A7    D
Ainda existe uma chance, se a gente se entender

INTRO:A7 D Em A7 D (2X)

          D      D7+
Eu já nem sei se sou
   D D7+             Em A7  Em A7
O homem ideal de sua vida
                  Em      A7
Se marcou, se marquei, nem sei
      A7                     D
Me perdoe se não sou fiel contigo

             D
Muitas vezes eu menti a mim mesmo
      D7                 G
Que enfim consegui te esquecer
                    D   Bm
Foi tão forte a solidão
                 Em            A7           D    ( D )
Esse meu pobre coração quase ficou de mal comigo

REFRÃO:
           A7                D
Águas passadas não rodam moinhos
          A7     Em     D
É o que sempre escuto dizer
               A7                    D
Se você não passou, eu também não passei
                   G           Em     A7    D     ( G D A7 D )
Ainda existe uma chance, se a gente se entender

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
D7+ = X X 0 2 2 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
