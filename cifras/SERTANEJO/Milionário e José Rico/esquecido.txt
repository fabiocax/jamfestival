Milionário e José Rico - Esquecido

Introdução: F, ( C7, F, C7, F) (2x)

F                                            C7
Meu amor faz hoje um mês que pro você fui desprezado
                                   F
Gostaria de saber como é que tem passado
                                      C7
Enquanto aqui sozinho vivo como você sabe
                                        F
Soluçando dia e noite por não ter felicidade!

F7            Bb                      F
Rudemente me pediu que esquecesse de você
            C7         Bb          C7            F
Mas eu sou tão esquecido que esqueci de lhe esquecer
F7                        Bb                   F
Essas frases não são minhas, o autor eu não conheço
         C7           Bb         C7      F
Copiei para dizer que esqueci de lhe esquecer
            C7             F
Mais de te amar eu não esqueço!


F                                            C7
Meu desejo é que esta carta ao chegar em sua mão
                                      F
Lhe encontre com saúde cheia de satisfação;
                                            C7
Será grande o meu prazer ao saber que está feliz
                                             F
Mesmo sendo desprezado, o seu bem eu sempre quis.

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
