Milionário e José Rico - Apaixonado

G
Moreninha linda, se tu soubesses
                            D
O quanto padesse quem tem amor
C                           G
Talvez não faria o que faz agora
          D             G
Quem te adora não tem valor

D
Não vivo sem ti
                C
Sem ti não sei viver
                    D
Se eu perder teus beijos
                         G
Juro querida que vou morrer.
D
Vou sofrer por ti
            C
Por ti vou sofrer

               D
Se for meu destino
                     G
Apaixonado quero morrer.

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
