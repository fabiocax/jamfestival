Milionário e José Rico - Paixão Anônima

[Intro] F  C  G7  C  C7  F  C  G7  C  G7  C

            G7       C
Você é a paixão escondida
                            G7
Que aqui dentro deste peito tem
                          C
Você é minha noite mal dormida
        G7               C
Mas não sabe que a quero bem

         G7            C
Você é razão que canto triste
     C7                    F
Cada música que faço é um apelo
                         C
Sem saber que este amor existe
     G7                   C
Você é a minha dor de cotovelo

     F
Você fala que o meu disco novo

                        C
Sempre vai para sua coleção
                         G7
Compra dois porque um oferece
                        C
Para o homem do seu coração

      G7               C
É por isso que canto e gravo
     G7               C
Interpreto tristeza e dor
           F                C
Por que é triste ter por amizade
               G7              C
A pessoa que a gente quer por amor

( F  C  G7  C  C7  F  C  G7  C  G7  C )

               G7           C
Você é a minha noite mal dormida
     C7                    F
Cada música que faço é um apelo
                         C
Sem saber que este amor existe
     G7                   C
Você é a minha dor de cotovelo

     F
Você fala que o meu disco novo
                        C
Sempre vai para sua coleção
                         G7
Compra dois porque um oferece
                        C
Para o homem do seu coração

      G7               C
É por isso que canto e gravo
     G7               C
Interpreto tristeza e dor
           F                C
Por que é triste ter por amizade
               G7              C
A pessoa que a gente quer por amor

( F  G7  C )

----------------- Acordes -----------------
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
G7 = 3 5 3 4 3 3
