Milionário e José Rico - Entrevista

Intro: F  Bb  F  C  F4

                F           Bb              F
E|-----------------------------------------------|
A|-----------------------------------------------|
D|-3----------------3-5--------5-----------------|
G|-----5---3-5/2~---------5-3~--------5~-3~-2~---|
B|---5---3--------------3----------3~------------|
E|-----------------------------------------------|

                 C           F4
E|-----------------------------------------------|
A|-----------------------------------------------|
D|-3-5----------5~----3-5-3----------------------|
G|--------5-2-3-------------3~-------------------|
B|-----3~----------------------------------------|
E|-----------------------------------------------|

             Bb                    F
Já me perguntaram em várias entrevistas
              C                      F
Se aqui tenho tudo e sou tão triste assim

                Bb                    F
As vezes não respondo, faço papel de mudo
                Bb         C7    F     F7
De que vale ter tudo e não ser feliz

             Bb               F
Sou um pobre ser com a alma ferida
               C                    F
Se dei valor a vida por que tive um bem
             Bb            F
Foi minha loucura esta criatura
                  C7                 F     F7
E sem querer a perdi para um outro alguém

           Bb         C7     F
Por esta razão me afastei de tudo
             C                    F
Longe das maldades que esta gente faz
              Bb                   F
Não façam perguntas, por favor eu peço
                C7                F    C7
Me concedam o direito de viver em paz

( F Bb F C F4 )

             Bb                    F
Já me perguntaram em várias entrevistas
              C                      F
Se aqui tenho tudo e sou tão triste assim
                Bb                    F
As vezes não respondo, faço papel de mudo
                Bb         C7    F     F7
De que vale ter tudo e não ser feliz

             Bb               F
Sou um pobre ser com a alma ferida
               C                    F
Se dei valor a vida por que tive um bem
             Bb             F
Foi minha loucura esta criatura
                  C7                 F     F7
E sem querer a perdi para um outro alguém

           Bb          C7      F
Por esta razão me afastei de tudo
             C                    F
Longe das maldades que esta gente faz
              Bb                   F
Não façam perguntas, por favor eu peço
                C7                 F
Me concedam o direito de morrer em paz

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
F4 = 1 3 3 3 1 1
F7 = 1 3 1 2 1 1
