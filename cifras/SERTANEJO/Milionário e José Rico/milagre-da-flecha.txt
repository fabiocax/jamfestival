Milionário e José Rico - Milagre da Flecha

(intro) ( C F D G C F G C )

                                                      G
Era alta madrugada, já cansado da jornada, eu voltava pro meu lar
                                                                      C
Quando apareceu no escuro, me encostando contra o muro, um ladrão pra me assaltar
                                                              G
Com o revólver no pescoço, eu expliquei pro moço, tenho filho pra criar
                                                             C
Sou arrimo de família, leva tudo, me humilha, mas não queira me matar

(refrão)
C     G                  C
Ave Maria aleluia, ave Maria
                                                           G
Mas o homem sem piedade, um escravo da maldade, começou me maltratar
                                                        C
Pra ver se eu tinha medo, antes de puxar o dedo, ele me mandou rezar
                                                           G
Eu nunca tinha rezado, eu que era só pecado, imploreri por salvação
                                                             C
Elevei meu pensamento, descobri neste momento, o que é ter religião


(refrão)
                                                       G
Um clarão apareceu, minha vista escureceu, e o bandido desmaiou
                                                                  C
E morreu não teve jeito, com uma flechada no peito, sem saber quem atirou
                                                                 G
Nesta hora agente grita, berra, chora e acredita, que o milagre aconteceu
                                                                 C
De joelho na calçada, perguntei com voz cansada, quem será que me atendeu

(refrão)
                                                                A
Já estava amanhecendo, a alegria me aquecendo, quando entrei na catedral
                                                      D
Cada santo que eu via, eu de novo agradecia, e jurava ser leal
                                                                A
Veja o santo de passagem, não me toque nas imagens, me avisou o sacristão
                                                                      D
Pois lá ninguém explicava, uma flecha que faltava... na imagem de São Sebastião

(refrão)

----------------- Acordes -----------------
A = X 0 2 2 2 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
