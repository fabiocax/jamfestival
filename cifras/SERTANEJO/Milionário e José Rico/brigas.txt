Milionário e José Rico - Brigas

        G                 G7               C          C4   C
Veja só, que tolice nós dois, brigarmos tanto assim
     Am                    F             D          D7
Se depois, vamos nós a sorrir, ficar de bem no fim
       G                  G7                      C        C4   C
Para quê maltratarmos o amor?! O amor não se maltrata não!
                            Cm                 D
Para quê?! Se essa gente o quer é ver nossa separação!

      G                  G7              C         C4   C
Brigo eu, você briga também, por coisas tão banais
      Am                 F                 D          D7
E o amor, em momentos assim, morre um pouquinho mais
         G            G7                    C                  Cm
E ao morrer, então é que se vê que quem morreu: fui eu e foi você!
          Bm    Em           Am      D
Pois sem amor,        estamos sós
          Eb    F    G
Morremos nós

      G                  G7              C         C4   C
Brigo eu, você briga também, por coisas tão banais

      Am                 F                 D          D7
E o amor, em momentos assim, morre um pouquinho mais
         G            G7                    C                  Cm
E ao morrer, então é que se vê que quem morreu: fui eu e foi você!
          Bm    Em           Am      D
Pois sem amor,        estamos sós
          Eb    F    G
Morremos nós

         G            G7                    C                  Cm
E ao morrer, então é que se vê que quem morreu: fui eu e foi você!
          Bm    Em           Am      D
Pois sem amor,        estamos sós
          Eb    F    G
Morremos nós

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C4 = X 3 3 0 1 X
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Eb = X 6 5 3 4 3
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
