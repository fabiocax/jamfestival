Milionário e José Rico - Decida

[Intro]  Dm

 Dm
Sente aqui comigo no sofá
E vamos conversar
        Bb         A
É hora de abrir o jogo
Nosso amor esta indo água abaixo
                  A7
Se deixar vira relaxo
                Dm  D7
Temporal apaga fogo

     Gm
Porque você não olha nos meus olhos
     Dm
Seu beijo não tem o mesmo sabor
   A
O seu carinho não me faz dormir
     Dm                      D7
Nem sua quando a gente faz amor

   Gm
Você só vai tomar banho sozinha
    Dm
Na hora do jantar me diz que já comeu
     A
Não vê novelas e nem liga o som
                                              Dm  D7
Diz que não tem nada bom que satisfaça o ego seu

              Gm
Você se esqueceu
                           Dm
Que dentro dessa casa eu existo
                 A
Que em 82 casou comigo
                           Dm  D7
Por isso exijo uma explicação
                      Gm
Se sou eu que te incomoda
                              Dm
Pra te fazer feliz fiz o que pude
                           Bb
Mas o incomodado é que se mude
   A7                    D   A  A/C#
Você quem vai tomar a decisão

   D
Decida
                          F#
Se vai embora ou ficar comigo
    G                       D
Se vai me respeitar como marido
      A                          D   A  A/C#
Pois desse jeito não estou agüentando
   D
Decida
                               F#
Ou pare de uma vez com esse delírio
    G                       D
Talvez você precisa usar colírio
    Em           A7            D
Pra enxergar o quanto ainda te amo

( Bb  A7  Dm  D7 )

              Gm
Você se esqueceu
                           Dm
Que dentro dessa casa eu existo
                 A
Que em 82 casou comigo
                           Dm  D7
Por isso exijo uma explicação
                      Gm
Se sou eu que te incomoda
                              Dm
Pra te fazer feliz fiz o que pude
                           Bb
Mas o incomodado é que se mude
   A7                    D   A  A7/C#
Você quem vai tomar a decisão

   D
Decida
                          F#
Se vai embora ou ficar comigo
    G                       D
Se vai me respeitar como marido
      A                          D   A  A/C#
Pois desse jeito não estou agüentando
   D
Decida
                               F#
Ou pare de uma vez com esse delírio
    G                       D
Talvez você precisa usar colírio
    Em           A7            D
Pra enxergar o quanto ainda te amo

[Final]  Bb  A7  D

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A7 = X 0 2 0 2 0
A7/C# = X 4 5 2 5 X
Bb = X 1 3 3 3 1
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
