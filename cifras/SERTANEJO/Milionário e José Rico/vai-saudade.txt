Milionário e José Rico - Vai Saudade

 A
eu vou embora para sempre de sua vida
                                   E
E nunca mais quero falar do nosso amor

Um homem triste apaixonado sofre muito
                                       A
Mas não divide com ninguém seu grande amor

( A G#m F#m )

 E
Vaaai saudade
 D             A
Leva a minha paixão
                           E
Que deixou dentro do meu peito,
       D               A
uma cicatriz em meu coração

( A G#m F#m )


E
Vaaai saudade
  D                 A
Vagar pelo mundo afora
                  E
Deixe esse pobre homem
                           A
morrer em paz Ao chegar à hora

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
