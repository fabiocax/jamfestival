Milionário e José Rico - Livro da Vida

D
Eu não quero que penses querida
                         A
Que eu sofro por te querer
                        A
A ilusão que foi meu castigo
G           A          D
É a razão do meu padecer
  D
Eu sinto saudade de alguém
D          D7       G
Que padece sem merecer
                            D
É o sangue do meu próprio sangue
     A                     D
E é a razão de todo o meu ser
D
Eu não peço que siga os meus passos
                         A
Pois o nosso amor terminou
                      A
Ficará gravado na mente

G          A      D
O erro que praticou
D
O mundo é um livro aberto
D7                         G
Pra ensinar quem não sabe viver
                           D
E as lições são bem declaradas
         A        D      G    A   D
E tão fácil de compreender

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
