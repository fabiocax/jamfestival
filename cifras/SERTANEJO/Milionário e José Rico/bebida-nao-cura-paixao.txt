Milionário e José Rico - Bebida Não Cura Paixão

D
Alguém veio me dizer que você vive a beber
                        A
Por alguém que não lhe quer
A
Procure entender
                                   D
Muito mais irá sofrer pelo amor dessa mulher

                                 D
Seu tempo já passou e você na conservou
     D7                   G
Esse alguém que tanto lhe quis
                D
Mas tudo tem um fim
                    A
Ela hoje pertence a mim
             D
E comigo é feliz

          A                 D
Amor sincero é o que sempre desejei

               G                   A
Você não deu valor a esse sublime amor
                D
E por isso a perdeu

( A  D  A )


----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
