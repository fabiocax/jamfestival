Milionário e José Rico - Paixão da Minha Vida

(intro) C B C G7 C

C      G7          C
Paixão de minha vida
             G7      C         G7
Mulher querida, meu anjo adorado
    Dm   C       G7
Razão do meu viver
F         G7           F            C   G7
Sinto prazer quando estou ao teu lado
C     G7     C
Você me realiza
                C7                F
E não precisa de um truque qualquer
                            C
Meu desejo é que seja somente
          G7           C
Eternamente minha mulher

F         Dm  G7             C
Obrigado amor por me dar calor

   G7                F                 C
Jamais amei outra pessoa como te amo tanto
F           Dm  G7            C
Obrigado amor por gostar de mim
        F           Dm           G7             C
Minha vida começa em teus braços e neles tem fim

C         G7             C
Eu andava no mundo sozinho
        G7        C                   G7
Sem ter um carinho nem o amor de ninguém
    Dm   C                   G7
O destino cruzou nossos passos
F               G7          F       C   G7
Caí nos teus braços enfim sou alguém
C     G7       C
Você me realiza
                C7                F
E não precisa de um truque qualquer
                             C
Meu desejo é que seja somente
          G7           C
Eternamente minha mulher

----------------- Acordes -----------------
B = X 2 4 4 4 2
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G7 = 3 5 3 4 3 3
