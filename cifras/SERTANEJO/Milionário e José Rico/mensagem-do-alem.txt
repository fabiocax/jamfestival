Milionário e José Rico - Mensagem do Além

(intro) Em Am B7 Em

                  B7                      Em
Onde estou sou mais feliz, do que fui até então
            B7        Am             Em
É verdade, aí se diz: aqui se tem consolação!
                B7                         Em
O que eu tenho é pra dar, quem quiser pode pedir
            B7       Am            Em  E
Meu desejo é ensinar, uma lagrima a sorrir
             Am         D7                 G
Quero ser sua esperança,    luz, consolo e verdade
Em              B7                          Em
Quem espera sempre alcança, paz e amor na eternidade

                  D                         G
Esqueça toda essa dor, não pense mais nessa tristeza
E                Am            D7                  G
O meu mundo é de esplendor,    vem contemplar esta beleza
C        Am       B7                             Em
Não há razão para temor, tenha mais fé nos dias seus

               Am            B7         Em
Ponha na vida mais amor, menos mundo e mais Deus

E             Am         D7                G
Quero ser sua esperança,    luz, consolo e verdade
Em              B7                          Em
Quem espera sempre alcança, paz e amor na eternidade

                  D                         G
Esqueça toda essa dor, não pense mais nessa tristeza
E                Am            D7                  G
O meu mundo é de esplendor,    vem contemplar esta beleza
C        Am       B7                             Em
Não há razão para temor, tenha mais fé nos dias seus
               Am            B7         Em
Ponha na vida mais amor, menos mundo e mais Deus

 Am         Em
Amooooooor é Deeeeuuuussssss

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
