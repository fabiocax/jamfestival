Milionário e José Rico - A Carta

[Intro]  E  E/G#  A  Bbm5-  E  A/C#  D  B7(4)

   E                                   B9
Estou escrevendo esta carta meio aos prantos
                 A9
Ando meio pelos cantos
                      F#m7
Pois não encontrei coragem
        B/A         E
De encarar o teu olhar
                    B9
Está fazendo algum tempo
                        A9
Que uma coisa aqui por dentro
                   F#m7
Despertou e é tão forte
         B7(4)      E
Que não pude te contar

  A9                              E/G#
Quando você ler eu vou estar bem longe

                     F#m7
Não me julgue tão covarde
         B/A         E   G#m7
Só não quis te ver chorar

 A9                               E/G#
Perdão amiga, são coisas que acontecem
       C#m7  E/G#  F#m7
Dê um beijo nos meninos
         B7(4)           E  A9  B9
Pois eu não vou mais voltar

 E
Como eu poderia dar
            B9
A ela esta carta, como eu vou deixar
                   A9                  F#m7
Pra sempre aquela casa se eu já sou feliz
                 B7(4)                 B7
Se eu já tenho amor, se eu já vivo em paz?
E                                      B9
E por isso decidi que eu vou ficar com ela
A minha passagem
               A9  F#m7
Por favor, cancela
      B9     E
Vá sozinha, não vou mais

  E                                B9
Quando cheguei no portão da minha casa
                   A9
Como se eu tivesse asas
                  F#m7
Me senti igual criança
        B/A      E
Deu vontade de voar
                     B9
Quase entrei pela janela

                      A9
Minha esposa ali tão bela
                        F#m7
Dei um forte e longo abraço
       B7(4)    E
E comecei a chorar

A9                             E/G#
E com as lágrimas as palavras vinham
                F#m7
E rolavam como pedras
       B/A         E  G#m7
E ela só a me escutar
A9                              E/G#
Ao enxugar minhas lágrimas com beijos
     C#m7    E/G#  F#m7
Revelou que já  sabia
      B7(4)  E  A9  B9
Mas iria perdoar

 E
Como eu poderia dar
            B9
A ela esta carta, como eu vou deixar
                   A9                  F#m7
Pra sempre aquela casa se eu já sou feliz
                 B7(4)                 B7
Se eu já tenho amor, se eu já vivo em paz?
E                                      B9
E por isso decidi que eu vou ficar com ela
A minha passagem
               A9  F#m7
Por favor, cancela
      B9     E           C7
Vá sozinha, não vou mais

 F                        Bb9   C
Como eu poderia dar a ela esta carta, como eu vou deixar
                   Bb9                 Gm7
Pra sempre aquela casa se eu já sou feliz
                 C7(4)                C7
Se eu já tenho amor, se eu já vivo em paz?
F                                      C
E por isso decidi que eu vou ficar com ela
A minha passagem
            Bb9    Gm7
Por favor, cancela
      C7         Dm      F  Bb9
Vá sozinha, não vou mais
                     F   Bb9  C#9  F
Vá sozinha, não vou mais

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A9 = X 0 2 2 0 0
B/A = X 0 4 4 4 X
B7 = X 2 1 2 0 2
B7(4) = X 2 4 2 5 2
B9 = X 2 4 4 2 2
Bb9 = X 1 3 3 1 1
Bbm5- = X 1 2 3 2 1
C = X 3 2 0 1 0
C#9 = X 4 6 6 4 4
C#m7 = X 4 6 4 5 4
C7 = X 3 2 3 1 X
C7(4) = X 3 3 3 1 X
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F#m7 = 2 X 2 2 2 X
F/A = 5 X 3 5 6 X
G#m7 = 4 X 4 4 4 X
Gm7 = 3 X 3 3 3 X
