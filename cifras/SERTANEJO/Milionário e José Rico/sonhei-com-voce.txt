Milionário e José Rico - Sonhei Com Você

[Intro]  Bm7  E7  A7+  A6  Bm7  E7  A7+  A6
         Bm7  E7  A7+  A9  E  E7  A

   A             E7        A
Depois de muito tempo acordado
       A       E7       A
Já cansado de tanto sofrer
               D         A
Esta noite dormi um pouquinho
            E
Sonhei com você

   Bm                  E
Você apareceu em meu quarto
      Bm                  E
E sorrindo me estendeu a mão
                               E7
Se atirou em meus braços e beijou-me
        A
Com emoção


     A          E7       A
E matando a paixão recolhida
       A        E7    A
Num delírio de felicidade
             D       A7
Em soluço você me dizia
           D
Amor, que saudade

                E7         A
De repente, em menos de minuto
                         E
Você se transformou num vulto
   D    E7      A  A7
E logo desapareceu
      D         E7         A
De repente, em menos de minuto
                         E
Você se transformou num vulto
   D    E7      A
E logo desapareceu

  E                    D            A
Quando acordei não te vi, que desespero
        E                    E7                 A
Minhas lágrimas molharam a fronha do meu travesseiro
     E                  D               A
Meu bem, como é maravilhoso sonhar com você!
  E                     E7    A
Amor como é triste acordar e não te ver

( Bm7  E7  A7+  A6  Bm7  E7  A7+  A6 )
( Bm7  E7  A7+  A9  E  E7  A )

     A          E7       A
E matando a paixão recolhida
       A        E7    A
Num delírio de felicidade
             D       A7
Em soluço você me dizia
           D
Amor, que saudade

                E7         A
De repente, em menos de minuto
                         E
Você se transformou num vulto
   D    E7      A  A7
E logo desapareceu
      D         E7         A
De repente, em menos de minuto
                         E
Você se transformou num vulto
   D    E7      A
E logo desapareceu

  E                    D            A
Quando acordei não te vi, que desespero
        E                    E7                 A
Minhas lágrimas molharam a fronha do meu travesseiro
     E                  D               A
Meu bem, como é maravilhoso sonhar com você!
  E                     E7    A
Amor como é triste acordar e não te ver

[Final]  D  C#m  Bm  A

----------------- Acordes -----------------
A = X 0 2 2 2 0
A6 = 5 X 4 6 5 X
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
A9 = X 0 2 2 0 0
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
