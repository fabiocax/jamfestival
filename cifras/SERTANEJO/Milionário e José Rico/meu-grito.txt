Milionário e José Rico - Meu Grito

[Intro]

      A       E7      A      D            E7  A      E7
E|--------------------------------------------------------|
B|--------------------------------------------------------|
G|--------------------------------------------------------|
D|--------------------------------------------------------|
A|-0-0----------------------------0-0-0---2---0----0------|
E|-----4-2----0-0-2---0--0---2-----------------------2-0--|

 A
Se eu demoro
                    E7
Mas aqui eu vou morrer
        D
Isso é bom
                       A
Mas eu não vivo sem você
                      A7
Eu não penso mais em nada
       D             Dm
A não ser, só em voltar

        A                           E7
Vou depressa e levo o meu amor nas mãos
Para lhe dar
        A
Já não durmo
                    E7
Morro até só em pensar
      D
E se canto
                        A
Só o seu nome quero gritar
                      A7
Mas se eu grito todo mundo
      D           Dm
De repente vai saber
        A           E7
Que eu morro de saudade
       A   D     A
E de amor por você

    A       E7
E|--------------|
B|--------------|
G|--------------|
D|--------------|
A|-0-0----------|
E|-----4-2-0----|

E7
Aí, que vontade de gritar
     A
Seu nome bem alto e no infinito
   E7
Dizer que o meu amor é grande
       A
Bem maior do que meu próprio grito
                    A7
Mas só falo bem baixinho
       D             Dm
E não conto pra ninguém
         A             E7
Pra ninguém saber seu nome
         A   D   A
Eu grito só meu bem

    A       E7
E|--------------|
B|--------------|
G|--------------|
D|--------------|
A|-0-0----------|
E|-----4-2-0----|

( E7  A  E7  A )

                   A7
Mas só falo bem baixinho
       D             Dm
E não conto pra ninguém
         A             E7
Pra ninguém saber seu nome
         A   D   A
Eu grito só meu bem

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
