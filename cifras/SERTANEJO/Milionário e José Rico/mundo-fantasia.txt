Milionário e José Rico - Mundo Fantasia

G                              D7              G
Acredite em Deus que o mundo ainda não está perdido
                                       D7
Se existe amor e compreensão por que sofrer?
           C                                   G
Por que sofrer é o que sempre pergunto a mim mesmo
            D7            C      D7       G
Se é tão sublime viver a vida na paz de deus
         D7               C
Mundo fantasia cheio de ilusão
            D7                 G
Meu pobre coração de tudo se cansou
         D7                C
Mundo fantasia longe do infinito
                    D7               G        C   D7   G   (repete tudo)
Onde não se ouve a voz do Mestre o Criador

( C  D7  G )

----------------- Acordes -----------------
C = X 3 2 0 1 0
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
