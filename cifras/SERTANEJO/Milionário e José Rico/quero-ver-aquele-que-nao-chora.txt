Milionário e José Rico - Quero Ver Aquele Que Não Chora

[Intro] Gm Dm A7 Dm Gm Dm A7 Dm

            Bb            A7
Quero ver aquele que não chora
Gm                           Dm
Quando perde a coisa mais querida
                       A7
Solidão é bicho que apavora
Gm                      Dm
Destruindo às vezes uma vida

            Bb            A7
Quero ver aquele que não chora
Gm                          Dm  D7
Ao ver o seu amor em outros braços
Gm             Dm             A7            Dm
Vivo a chorar, destino cruel, não tenho solução
Gm             Dm             A7                  D A7
Vivo a buscar, sem encontrar, paz para o meu coraçã_ão

D                       Bm               Em
Sempre que estás ao meu lado, é tudo tão lindo

A7                     G      A7      D
Breves momentos que a vida contigo me dá
Em                   B7                Em
Quem me ver assim sorrindo, feliz e cantando
E7                               A
Não imagina que eu amanhã vou chorar
D                Bm                   Em
Mata-me se tu quiseres, mas não me maltrate
A7                                         Dm
Tenho razões para odiar-te, mas só sei te amar

[Gm  Dm  A7  Dm  Gm  Dm  A7]

D                       Bm               Em
Sempre que estás ao meu lado, é tudo tão lindo
A7                     G      A7      D
Breves momentos que a vida contigo me dá
Em                   B7                Em
Quem me ver assim sorrindo, feliz e cantando
E7                               A
Não imagina que eu amanhã vou chorar
D                Bm                   Em
Mata-me se tu quiseres, mas não me maltrate
A7                                         Dm
Tenho razões para odiar-te, mas só sei te amar

[Gm  A7  D]

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
Bb = X 1 3 3 3 1
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
