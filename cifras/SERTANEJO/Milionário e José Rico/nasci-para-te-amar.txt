Milionário e José Rico - Nasci Para Te Amar

Intro: E  E7  A  A  E  E7  A  A
       E  E7  A  A7 D  E7  A  E  A

      A            E7
Eu nasci para te amar
                   A
Também quero ser amado
                      E7
Alguns beijos que me destes
                     A
Estão todos bem guardados
                   E7
Dentro do lado esquerdo
                     A
Tem um cofre bem fechado
                   E7
Para não correr perigo
                   A
Para nunca ser roubado

       E7           A
Que bonito amar na vida

       E7            A
Que bonito é querer bem
      E7           A
Amar sempre em segredo,
       E7               A
Sem contar para ninguém.

      A          E7
Meu amor é todo teu,
                    A
Quero ser correspondido
                      E7
Mais vale um amor sincero,
                    A
Do que mil amor fingido
                      E7
Quantas almas vivem tristes,
                   A
Neste mundo de ilusão
                      E7
Mas eu sei que todos têm
                A
Um amor no coração.

       E7           A
Que bonito amar na vida
       E7            A
Que bonito é querer bem
      E7           A
Amar sempre em segredo,
       E7               A
Sem contar para ninguém.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
