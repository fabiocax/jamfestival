Milionário e José Rico - Mané No Forró

Intro: E E D E

 E          A     B7          E
Onde tu tá Mané? Mané onde tu tá?
 A               E
Chegue puxar o fole
            F# B7   E
Que o forró vai começá (2X)

 E                E B7 E
Mané era um vaqueiro
 A             A E7 A
Afamado no sertão
 B             B F# B
Aprendeu a tocá fole
                     E
O forró que ele fazia
             F#  B7  E
Levantava poeira do chão

 E          A     B7          E
Onde tu tá Mané? Mané onde tu tá?

 A               E
Chegue puxar o fole
            F# B7   E
Que o forró vai começá (2X)

E                E B7 E
Mané era um vaqueiro
A             A E7 A
Afamado no sertão
 B             B F# B
Aprendeu a tocá fole
                     E
O forró que ele fazia
             F#  B7  E
Levantava poeira do chão

 E          A     B7          E
Onde tu tá Mané? Mané onde tu tá?
 A               E
Chegue puxar o fole
            F# B7   E
Que o forró vai começá (2X)

 E           B7    E
Mané era pernambucano
 A        E7  A
Morava no Ceará
 B           F#     B
Um dia sumiu na caatinga
 B7                 E
Não conseguimo encontrá

 E          A     B7          E
Onde tu tá Mané? Mané onde tu tá?
 A               E
Chegue puxar o fole
            F# B7   E
Que o forró vai começá (2X)

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F# = 2 4 4 3 2 2
