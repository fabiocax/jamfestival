Milionário e José Rico - Amor Eterno

Intro: A D A D A E7 A A7
       D E D E D E D E7 A

           A          D                 A          D                 A
E|--------------------------------------------------------------------------|
A|--------------------------------------------------------------------------|
D|--------------------------------------------------------------------------|
G|-13~-11-11-9---7-9-11-11-11-9-13~-11-11-9---7-9-11-11-11-9-13~-11-11-9----|
B|--------------------------------------------------------------------------|
E|-12~-10-10-9---7-9-10-10-10-9-12~-10-10-9---7-9-10-10-10-9-12~-10-10-9----|

        E7                A A7       D            E                  D
E|--------------------------------------------------------------------------|
A|--------------------------------------------------------------------------|
D|--------------------------------------------------------------------------|
G|-7-9-11-11-11-9-13~-11-9---14~-13-13-11---9-11-13-13-13-11-14~-13-13-11---|
B|--------------------------------------------------------------------------|
E|-7-9-10-10-10-9-12~-10-9---14~-12-12-10---9-10-12-12-12-10-14~-12-12-10---|

         E                  D            E                  D       E7    A
E|--------------------------------------------------------------------------|
A|--------------------------------------------------------------------------|
D|--------------------------------------------------------------------------|
G|-9-11-13-13-13-11-14~-13-13-11---9-11-13-13-13-11-14~-13-11---11-13-13-14-|
B|--------------------------------------------------------------10-12-12-14-|
E|-9-10-12-12-12-10-14~-12-12-10---9-10-12-12-12-10-14~-12-10---------------|


      A
O meu mundo é de felicidade
                           A7
Quando estou com a mulher amada
     D            A
Quem tem amor e saúde
   E    D         A
Na vida não falta nada

Preciso gritar bem alto
                    A7
Tudo que minha alma sente
      D                      A
Este amor que nasceu de um olhar
      E           D   A  A7
Há de durar eternamente

   D
O amor puro e sincero
            E7         A     A7
Com ciúme e sem preconceito
       D                A
É uma chama que não se apaga
     E       D        A      A7
Vive sempre acesa no peito

       D
Nosso amor será sempre eterno
               E7         A     A7
Eu te juro por Deus vida minha
       D              A
No meu reino de felicidade
     E    D       A
Tu serás sempre rainha

( A D A D A E7 A A7 )
( D E D E D E D E7 A A7 )

   D
O amor puro e sincero
            E7         A     A7
Com ciúme e sem preconceito
       D                A
É uma chama que não se apaga
     E       D        A      A7
Vive sempre acesa no peito

       D
Nosso amor será sempre eterno
               E7         A     A7
Eu te juro por Deus vida minha
       D              A
No meu reino de felicidade
     E    D       A     E7 A
Tu serás sempre rainha

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
