Milionário e José Rico - Prova do Tempo

(intro)  E7 A A7 D A E7 A

A                                            E7
Quando eu te vi pela vez primeira
  D                                           A
Senti algo estranho dentro de mim
                                      A7              D
Nem ao menos pensei que pudesse surgir
                          E7                      A
Um amor bem forte de repente assim

B7                                                E
Só mesmo o tempo poderá te provar
                         B7                           E
O quanto te adorei e o quanto eu te quis
                      D                     A
De que vale ter milhões em ouro
                                 E7                         A
Se com a pessoa amada não posso ser feliz

"É sempre assim amigo"


A                                        E7
Sei que nesta vida tudo se acaba
   D                                                          A
Estas foram as palavras que o Mestre pregou
                                       A7              D
E por que desfazer dos que são pequenos
                                    E7                                   A
Se nascemos e morremos sempre pelo mesmo amor

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
