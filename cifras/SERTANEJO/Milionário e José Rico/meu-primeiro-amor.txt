Milionário e José Rico - Meu Primeiro Amor

[Intro] F#  C#  Ebm  G#7  C#  C#m

 C#m
Saudade palavra triste
                            G#7
Quando se perde um grande amor

Na estrada longa da vida
                         C#m
Eu vou chorando a minha dor

Igual a uma borboleta
                             G#7
Vagando triste por sobre a flor
     F#m
Seu nome sempre em meus lábios
         A              G#7
Irei chamando por onde for
 C#7
Você nem sequer se lembra
                           F#m
De ouvir a voz desse sofredor

                       C#m
Que implora por seu carinho
          G#7             C#
Só um pouquinho do seu amor

Meu primeiro amor tão cedo acabou
                             G#7
Só a dor deixou neste peito meu

Meu primeiro amor foi como uma flor
                           C#
Que desabrochou e logo morreu

Nesta solidão, sem ter alegria
             C#7                   F#
O que me alivia são meus tristes ais
                                   C#
São prantos de dor, que dos olhos caem
              Ebm                 G#7
É porque bem sei, quem eu tanto amei
             C#
Não verei jamais

Meu primeiro amor tão cedo acabou
                             G#7
Só a dor deixou neste peito meu

Meu primeiro amor foi como uma flor
                           C#
Que desabrochou e logo morreu

Nesta solidão, sem ter alegria
             C#7                   F#
O que me alivia são meus tristes ais
                                   C#
São prantos de dor, que dos olhos caem
              Ebm                 G#7
É porque bem sei, quem eu tanto amei
             C#
Não verei jamais

----------------- Acordes -----------------
A = X 0 2 2 2 0
C# = X 4 6 6 6 4
C#7 = X 4 3 4 2 X
C#m = X 4 6 6 5 4
Ebm = X X 1 3 4 2
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
G#7 = 4 6 4 5 4 4
