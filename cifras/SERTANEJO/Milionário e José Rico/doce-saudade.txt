Milionário e José Rico - Doce Saudade

Polca: F# B F E B F# B G#m F# B

B
Enternecida saudade esta que sinto por ti
                                                  F#7
Saudade de outra saudade De um mundo em que não vivi
                        E                       B
Se soubesse o quanto sofre Meu peito longe do teu
                      F#          E      F#       B
Que tristeza ter saudade De alguém que não esqueceu

                    C#                      F#
Saudade é a luz da lua Luz que a tristeza gelou
E                     B         F#             B
Ao iluminar os caminhos Por onde o sol já passou

                    C#                        F#
Saudade, dor e carinho Tortura ao passo que apaga
E                     B      F#            B
A saudade é como o vinho É doce mas embriaga

----------------- Acordes -----------------
B = X 2 4 4 4 2
C# = X 4 6 6 6 4
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
F#7 = 2 4 2 3 2 2
G#m = 4 6 6 4 4 4
