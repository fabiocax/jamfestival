Milionário e José Rico - Meu Tesouro

   D
Dinheiro, que aproxima os inimigos
                   Bm
vai afastando os amigos
            A7
você me infentiçou
Dinheiro, com a maldade de um bruxo
com egoísmo, vaidade e luxo
             D
você me crucificou.

   D
E agora que me amava na pobreza
                 D7
foi embora de tristeza
              G
magoada de verdade
                    D
com meu ouro me deixou
                 A7
de bagagem só levou

               D
a minah felicidade.

             G                        Bm
De que me adianta a riqueza em minha vida
                    A7
se a pessoa mais querida
       G      A7       D
meu orgulho cobriu de dor
             G                       Bm
De que me adianta todo brilho deste ouro
                    A7
se perdi o maior tesouro
       G         A7   G
meus filhos e o meu amor.

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
