Milionário e José Rico - Tormento de Saudade

Intro 3x: D7  C  Am  G

   D7  C  Am G   D7                        C
E|----------3------------------------------------------------------|
A|----3--0--2------------------------------------------------------|
D|-0--2--2--0------------------------------------------------------|
G|-2--0--2--0---14/--14/--14-12-11-9-7~---11/--11/--11-9-7-5~------|
B|-1--1--1--3------------------------------------------------------|
E|-2--0--0--3---14/--14/--14-12-10-8-7~---10/--10/--10-8-7-5~------|

    Am                G
E|-----------------------------------------------------------------|
A|-----------------------------------------------------------------|
D|---------------------------------------------------16------------|
G|-5/--5/--5-4-2~------------------16----14-16-12-14----12~--------|
B|---------------------15----13-15----13---------------------------|
E|-5/--5/--5-3-2~---14----12---------------------------------------|

    D7                        C                     Am
E|-----------------------------------------------------------------|
A|-----------------------------------------------------------------|
D|-----------------------------------------------------------------|
G|-14/--14/--14-12-11-9-7~---11/--11/--11-9-7-5~---5/--5/--5-4-2~--|
B|-----------------------------------------------------------------|
E|-14/--14/--14-12-10-8-7~---10/--10/--10-8-7-5~---5/--5/--5-3-2~--|


    G
E|----------------------|
A|----------------------|
D|----------------------|
G|----------------------|
B|-10/--10/8-8~---------|
E|--8/---8/7-7~---------|

      G       D7
Companheiro
                    C
Fique aqui em minha mesa
                  Am
Participe da tristeza
                  G      C
Deste pobre amigo seu

                   D7
Eu queria estar sorrindo
                       Am
Mas a dor que estou sentindo
                    G     C
É mais forte do que eu

      G       D7
Companheiro
                    C
Se alguém diz o que sente
               Am
Ao amigo confidente
                G       C
É fugir da solidão

                    D7
Fique aqui não vá embora
                          C
Vou contar-lhe a minha história
                    G      C
Pra acalmar meu coração

                    D7
Fique aqui não vá embora
                          Am
Vou contar-lhe a minha história
                    G
Pra acalmar meu coração

( D7 C Am G ) 2x

      G       D7
Companheiro
                     C
Hoje está fazendo um ano
                       Am
Que a mulher que tanto amo
                 G     C
Já não vive para mim

                 D7
E inocente da verdade
                 C
O tormento da saudade
                    G      C
Que me faz sofrer assim

      G       D7
Companheiro
                       C
Ontem eu encontrei com ela
                     Am
Pois já não é mais aquela
                     G        C
A quem dei meu sobrenome

                     D7
Eis a razão da minha dor
                      C
Encontrei meu grande amor
                     G       C
Nos braços de outro homem

                     D7
Eis a razão da minha dor
                      Am
Encontrei meu grande amor
                     G
Nos braços de outro homem

( D7 C Am G ) 2x

      G       D7
Companheiro
                       C
Ontem eu encontrei com ela
                     Am
Pois já não é mais aquela
                     G        C
A quem dei meu sobrenome

                     D7
Eis a razão da minha dor
                      C
Encontrei meu grande amor
                     G       C
Nos braços de outro homem

                     D7
Eis a razão da minha dor
                      Am
Encontrei meu grande amor
                     G      ( C D7 G )
Nos braços de outro homem

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
