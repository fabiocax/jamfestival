Milionário e José Rico - Fofocas de Amor

[Intro] E  A  E7  A
           E  A  E7  A

A                                                                    E7
Por mais que eu queira esquecê-la não consigo
                                                               A
Por toda parte sempre está junto de mim
                                                             E7
A imagem linda de alguém que tanto quero
                                                        A
Se a perder sei que vou chegar ao fim

A                E7                                         A
Por que será que esta gente é tão maldosa
                     E7              D              A
Fazem fofocas pra nos ver na solidão
                           E7                                             A
Mas o que importa é que nos amamos loucamente
                     E7              D    E7      A
Nada nesta vida vai causar separação

----------------- Acordes -----------------
