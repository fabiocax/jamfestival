Milionário e José Rico - Tudo Passa

Intro: (B7 E A B7 E A B7 E)

E                                B7
Você precisa esquecer nosso passado
                              E
Por piedade não queria reviver
                                   B7
Como é tão triste a gente amar demais
                             E
E no futuro algo vem acontecer

E                                  B7
Não me censure por agir desta maneira
                              E
Você bem sabe que a amo loucamente
E                                B7
Vou fazer tudo pra esquecê-la de uma vez
                              E
Nem que eu tenha que sofrer eternamente

E                              B7
O tempo passa tudo acaba é a vida

                           E
O sofrimento domina o coração
                                 B7
Porém é tarde prá reviver o passado
                                     E
De uma lembrança que ficou na solidão

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
