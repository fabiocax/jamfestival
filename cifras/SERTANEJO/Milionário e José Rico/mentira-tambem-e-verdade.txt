Milionário e José Rico - Mentira Também É Verdade

Muitas vezes eu
            Gm
Minto a mim mesmo
      C
Que enfim
                 F   A7
Consegui te esquecer
     Dm
E assim
                 A7
Deixo a vida passando
                            Dm
A mentira me ajuda a viver

Refrão:
                               Gm
  Para o mundo eu escondo a verdade
          C                F
  E a saudade, consigo ocultar
         Dm           A7
  Tenho medo da realidade

                             Dm
  Porque nunca deixei de te amar

Solo: D7  Gm  C  F  Dm  A7  Dm

A mentira
            Gm
Não tem sofrimento
     C
A verdade
                F   A7
Quanta dor me traz
      Dm               A7
Na mentira esqueço o momento
                             Dm
Na verdade te amo demais

(Refrão 2x)

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
C = X 3 2 0 1 0
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
