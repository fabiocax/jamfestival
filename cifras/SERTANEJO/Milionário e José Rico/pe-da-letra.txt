Milionário e José Rico - Pé da Letra

Tom original em E (Mi) maior.

E               A        B7           E
Viajei de madrugada, na minha besta bainha
                    B7                  E
Fui numa festa do peão, na Fazenda Lagoinha
  F#                                            B7
Pertinho de Porto Alegre, eu cheguei lá de "tardinha"
                                          E
Fazendeiro Zé Valente, "Famia" da gente minha

E                 A                   B7      E
Soltei a mula no pasto, depois de dar um repasso
                 B7                        E
Dei um volta na sala, soltei meu peito de aço
  F#                                          B7
Vi uma gaúcha trigueira, fiz um verso no embaraço
                                           E
Quando repiquei a viola, ela caiu no meu braço

E                A              B7          E
Eu falei em casamento, me respondeu com frieza

                    B7                       E
Não me caso com violeiro, eu tenho muita riqueza
      F#                              B7
Sou rainha do gado, sou rica por natureza
                                             E
Só gostei da sua viola, desculpe minha franqueza

E                  A         B7              E
Respondi no pé da letra, sou lá de Minas Gerais
                    B7                         E
Tenho garimpo e diamante, sou um grande industrial
    F#                                     B7
Sou dono de muita terra, crio boiada em Goiás
                                            E
Eu compro sua fazenda, e todo o seu credencial

E             A         B7            E
O povo bateu palma, é isso mesmo "rapaiz"
                   B7                      E
Ela perguntou meu nome, eu só dei as iniciais
  F#                                         B7
Ela me abraçou chorando, apresentando seus pais
                                            E
O prazo do casamento, violeiro é você quem faz

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
