Milionário e José Rico - Pombinha Mensageira

Intro:  A7  D  A7  D

D                                 A7
Oh pombinha mensageira leve para minha amada
A7                              D
Esta carta apaixonada Que chorando eu escrevi
D                                  A7
Diga pra minha querida que eu só vivo penando
A7                                  D
E pergunto soluçando Que mal foi que cometi
D                                 A7
Voa depressa pombinha antes que a noite apareça
A7                                D
Antes que eu me enlouqueça Por favor esteja aqui
D                                    A7
Se trouxer boas notícias esta solidão eu venço
A7                                 D
Mas se não for o que penso Dei que não vou resistir
D                                A7
Quando ela abrir a carta vai notar borrões e erros
A7                               D
Diga que neste desterro Foi chorando que escrevi

D                                       A7
Os borrões foram meus prantos que caíram sobre as letras
A7                                    D
E os erros são memórias Que por ela eu perdi
D                                      A7
Com certeza ela entende mesmo estando mal escrita
A7                                  D
Não contém frases bonitas Mas falei o que eu senti
D                               A7
Diga que estou sofrendo por viver assim ausente
A7                                          D
E a amo loucamente Desde quando eu a vi
D                A7                D
Dia vem clareando ainda estou acordado
D                   A7                D
Tristonho desesperado E a pombinha não vem
D                       A7                D
Será que ela não sabe como é triste um abandono
D                    A7                   D  G A D
Já perdi noites de sono Sofrendo por querer bem

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
