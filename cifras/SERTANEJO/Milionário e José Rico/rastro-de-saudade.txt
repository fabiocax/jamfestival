Milionário e José Rico - Rastro de Saudade

 G
Um carro de boi rodando
   C               G
Rodando dentro de mim
    C                  G
Deixando rastros de saudade
   F       D7        G     G7
Saudade que não tem fim

 C           G        C                G
Ah, se eu pudesse voltar meu tempo que foi
   C             G       D7               G
Papai sempre contente tocando o carro de boi
 C           G       C                 G
Ah, se eu pudesse voltar meu tempo que foi
  C             G        D7              G
Papai sempre contente tocando o carro de boi

  G
Assim o tempo vai passando
   C                    G
Do meu sertão eu não esqueci

        C                  G
Se eu pudesse voltava a infância
       F       D7            G
Regressava ao berço em que nasci

----------------- Acordes -----------------
C = X 3 2 0 1 0
D7 = X X 0 2 1 2
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
