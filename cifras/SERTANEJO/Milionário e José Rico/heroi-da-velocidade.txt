Milionário e José Rico - Herói da Velocidade

Intro: A  A7  D  E7  A  A7  D  E7  A

               A
 Eu presto cantando, minha homenágem
                           E7
 Ao grande herói da velocidade
               D                  A
 Que parou na curva, do grande desejo
                Bm Bm/A E/G# E7           A  A7
 De repetir de novo,       (Senna) do BRASIL

 Refrão:
              D                  A
 Porém na manhã, de um domingo triste.
            E            D            A     A7
 O mundo parou, O herói tombou para sempre
              D                   A
 Deixou a certeza, de uma missão cumprida
                      E
 Se transferiu dessa vida,
                D           E     A           D  E
 Nos cobriu de saudades, e foi morar com DEUS


 *Repete desde o início...

 Final:
                      A   C  G7
 ...e foi morar com DEUS
        C        G7      C
 Nessa longa estrada da vida
                             G7
 Vou correndo e não posso parar
         F                C
 Na esperança de ser campeão
       G7                 C  C7
 Alcançando o primeiro lugar
         F                C
 Na esperança de ser campeão
       G7                 C  F  G  C
 Alcançando o primeiro lugar...

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
Bm/A = X 0 4 4 3 X
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
