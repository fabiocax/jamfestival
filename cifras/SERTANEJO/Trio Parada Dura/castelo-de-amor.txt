Trio Parada Dura - Castelo de Amor

                              A7           D
Num lugar longe bem longe lá no alto da colina
D7            G            A7         D
Onde vejo a imensidão e a beleza que fascina
D7            G        A7               D
Ali eu quero morar juntinho com minha flor
               E7     A7                D
Ali quero construir o nosso castelo de amor

 D                        A7             D
Quando longe muito longe surge o sol no horizonte
 D7             G     A7                D
Fazendo rajas no céu fazendo clarão nos montes
   D7           G       A7                D
Aquecendo toda terra bebendo orvalho das flores
                      E7      A7              D
Quero brindar com carinho o nosso castelo de amor

D                        A7               D
Quando longe muito longe formar nuvens no céu
      D7        G     A7            D
E a chuva lentamente cobre a terra com o véu

     D7            G        A7               D
E o vento calmo a soprar o nosso jardim em flor
               E7            A7              D
Quão felizes sentiremos em nosso castelo de amor

D                          A7          D
Quando um dia nossos sonhos tornarem realidade
  D7          G           A7     D
Unidos então seremos em plena felicidade
D7           G       A7           D
Aí então cantaremos louvores ao criador
               E7         A7               D
Será mesmo um paraíso o nosso castelo de amor

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
G = 3 2 0 0 0 3
