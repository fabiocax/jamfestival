Trio Parada Dura - Camisola Preta

Intro: G D G C G C D G D G

G                  D           G            D
As badaladas do relógio da matriz estão marcando nossa
             G
Triste despedida
        D                       G            A
Muito breve você vai casar com outro e para sempre vai
                D
Sair da minha vida.
        G           D           G           D
E no momento que você disser o sim estarei perto para
             G
Ouvir a sua voz
         D                     C                G
Em pensamentos eu estarei revivendo tudo o que houve
       D          G
No passado entre nós.


G          D                     C
Quando vestir aquela camisola preta

        G                      D
Companheira da nossa primeira vez
         G        G7          C
Vai ter outro ocupando meu lugar
           G          D          G
Mas minha sombra estará entre vocês.


Introdução: G D G C G C D G D G


 G                 D            G               D
Cada abraço que você ganhar do outro seu lindo corpo
                    G
Vai sentir o meu calor
         D                      G           A
E as canções apaixonadas que ouvir algumas delas vão
                  D
Falar do nosso amor.
         G          D            G             D
Serei a pedra no caminho que passar porque jamais vai
                G
Se esquecer de mim
            D                       C             G
Mas não me culpe se um dia for infeliz pois na verdade
       D              G
Foi você quem quis assim.


G          D                     C
Quando vestir aquela camisola preta
        G                      D
Companheira da nossa primeira vez
         G        G7          C
Vai ter outro ocupando meu lugar
           G          D          G
Mas minha sombra estará entre vocês.

----------------- Acordes -----------------
A = X 0 2 2 2 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
