Trio Parada Dura - Não Quero Piedade

[Intro]  C  G7  G7  C  C7  F  G7  C


       C                    G7                 C
Por favor não venha com mentiras pelo amor de Deus
                                           G7
As mentiras e falsas promessas nos fazem chorar
                                      C
Se você não me quer eu aceito a realidade
           F              G7                                    C
Prefiro ouvir uma triste verdade do que mil mentiras para me agradar

 C                      G7                C
Você nunca fez um sacrificio pelo nosso amor
                                              G7
Não reclamo mas me deixa triste seu modo de agir
                                             C
Só espero que os seus carinhos não sejam forçados
             F                  G7                                 C
Se for necessário eu morro apaixonado mas o nosso caso para por aqui


 G7                  C
Não... não quero piedade
           G7
Amor pela metade
                 C
É pouco pra nós dois
 G7                    C
Não... não repare meu jeito
             F                G7
Desculpe querida mas eu não aceito
                            C
Ser feliz agora e sofrer depois

 C                      G7                C
Você nunca fez um sacrificio pelo nosso amor

----------------- Acordes -----------------
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
G7 = 3 5 3 4 3 3
