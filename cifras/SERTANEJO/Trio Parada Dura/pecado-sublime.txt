Trio Parada Dura - Pecado Sublime

Intro: D A D A

D
Por que tempo vem antes do tempo
                            A
Sem ter tempo mudou meu caminho

E uniu-me com quem eu não amo
                           D
Mesmo junto eu me sinto sozinho

E agora só resta um lamento
            D7              G
De não ter te encontrado antes
                          D
Nós estamos em lares errados
      A                    D
Padecendo por sermos amantes

      A                  D
Não adianta forçar o destino

        A                  D
Com alguém que a gente não ama
           A              D
Quando em casa a cama é fria
      A                    D
Procuramos calor em outra cama


D
Nós sabemos que estamos errados
                       A
Mas o que poderemos fazer

Eu viver sem você não é vida
                          D
E sem eu você não quer viver

Não importa o que falam de nós
      D7                  G
Nos amamos e amar não é crime
                     D
Se o nosso amor é pecado
        A                  D
Nosso amor é um pecado sublime


      A                  D
Não adianta forçar o destino
        A                  D
Com alguém que a gente não ama
           A              D
Quando em casa a cama é fria
      A                    D
Procuramos calor em outra cama

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
