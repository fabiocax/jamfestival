Trio Parada Dura - O Carro e a Faculdade

Intro: A  E  A  A7  D  E  A

A                                             E
Eu tenho em meu escritório, em cima da minha mesa
                               E7                A
A miniatura de um carro, que a todos causam surpresa
                    F#m                    E
Muitos já me perguntaram, o motivo porque foi
                                                  A
Eu sendo um doutor formado, gosto de um carro de boi
                   E                        A
Respondi foi com o carro, nas estradas a rodar
                       E                      A
Que meu pai ganhou dinheiro, pra eu poder estudar
                  E       E7            A
Enquanto ele carreava, passando dificuldade
                   E                          A
As lições eu decorava, lá nos bancos da faculdade

(falado)
"Aohhh, meus amigos, essa é a história de um filho

que reconheceu o trabalho de seu pai"

A                                       E
Entre nossas duas vidas, existe comparação
                   E7                        A
Hoje eu seguro a caneta, como se fosse um ferrão
                       F#m                       E
Nos riscos de minha escrita, sobre a folha rabiscada
                           E       E7              A
Eu vejo os rastros que os bois, deixavam pelas estradas
                     E                          A
Fechando os olhos parece, que vejo estrada sem fim
                    E                       A
E um velho carro de boi, cantando dentro de mim
                  E                         A
Em meus ouvidos ficaram, os gemidos de um cocão
                   E       E7           A
E o grito de um carreiro, ecoando no grotão    (intro)
                                             E
Se tenho as mãos macias, eu devo tudo a meu pai
                                                A
Que teve as mãos calejadas, no tempo que longe vai
                         F#m                 E
Cada viagem que fazia, naquelas manhãs de inverno
                                E7              A
Era um pingo do meu pranto, nas folhas do meu caderno
                      E           E7             A
Meu pai deixou essa terra, mais cumpriu sua missão
                   E         E7              A
Carreando ele colocou, um diploma em minhas mãos
                       E           E7              A
Por isso guardo esse carro, com carinho e muito amor
                  E             E7             A    (E)    (A)
É lembrança do carreiro, que de mim fez um doutor

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
