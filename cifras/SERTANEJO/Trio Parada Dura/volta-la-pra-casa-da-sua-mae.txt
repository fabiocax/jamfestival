Trio Parada Dura - Volta Lá Pra Casa da Sua Mãe

G                         C
Vorta lá pra casa da sua mãe
D                           G
Vorta lá pra casa do seu pai
G                             C
Vorta pro lugar de onde cê veio
                  D                      G
Eu não te quero mais, eu não te quero mais
G                       D
Tudo que ela pedia eu fazia
                           G
Não tinha hora, não tinha dia
                             D
Tudo que ela falava eu concordava
                             G
E era assim a vida que eu levava
G                       D
No futebol, não vai
                            G
Na casa de um amigo, nem pensa
                          D
Tomar uma no boteco? ish han han!

                        G
Não tinha tempo nem pra cantar
G                        C
Até que um dia eu endoidei
          D                      G
Foi uma moda que no rádio eu escutei
                                D
Na minha cabeça uma grande confusão
                                   G
Que papo é esse da mulher não deixar não?
G                    C
Naquele dia eu acordei
        D                G
Caí na farra, bebi, dancei
G                        C
Perdi a hora, passei da conta
             D                       G
Cheguei em casa, a mala dela tava pronta
G                            C
E foi assim que tudo aconteceu
           D
Ela foi embora e eu falava
G
vorta! Vorta! Vorta! Ah, vorta, vorta

G                         C
Vorta lá pra casa da sua mãe
D                           G
Vorta lá pra casa do seu pai
G                             C
Vorta pro lugar de onde cê veio
                  D                      G
Eu não te quero mais, eu não te quero mais

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
