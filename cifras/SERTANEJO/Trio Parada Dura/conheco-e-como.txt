Trio Parada Dura - Conheço e Como

Intro: E A

              D                  A
Um dia desses lá no bairro onde eu moro
                        E
saiu um quebra pau, uma pancadaria
         D                        E
fechou o tempo os homens bateram na hora
                       A
foi todo mundo parar na delegacia
                     D       A
Quem foi reconhecido foi pra casa
                        D
ficou detida só a minha vizinha
       D     E             A
pra ter certeza e ela ser liberada
             E             A
me perguntaram se eu a conhecia


A
Mostraram ela pra pra mim falei

          E               A
conheço e como, e como, e como
 D
na verdade é que eu conheço e como
  E          D        A
desde quando ela era mocinha
A
Mostraram ela pra pra mim falei
          E             A
conheço e como, e como, e como
   D                         A
na verdade é que eu conheço e como
                   E                  A
há mais de dezoito anos mora na rua minha


A                D          A
Ontem a noite ela veio me procurar
                                  E
querendo agradecer o que eu fiz pra ela
E
por ter tirado daquela confusão
                         A
que eu sabia que não era com ela
A
Pra mim não custou nada o que eu fiz
                            D
além de tudo a moça é muito bela
                                  A
depois da briga ela faz tudo pra mim
E                            A
por eu viver tão sozinho como ela

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
