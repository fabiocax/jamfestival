Trio Parada Dura - Atravessando a Fronteira

Intro: G7  C  G7  C  G7  C

C                     G7                     C
Pra conhecer Mato Grosso eu deixei o meu estado
                    G7                    C
Atravessei a fronteira do nosso Brasil amado
F              G7                    C
Fui em Ponta Porá, Bela Vista e Corumbá
           C7          F           G7        C
Três Lagoas, Campo Grande, Aquidauana e Cuiabá.

C                   G7                     C
Conheci Porto Epitácio, Porto Quinze, Amambaí
                    G7                 C
Lá eu vi as paraguaias falando em guarani -
F                   G7                          C
Conheci Porto Murtinho, Dourados e Rio Brilhantes
       C7      F            G7          C
Conheci Maracajú, Cidade linda importante.

C                G7                       C
De lá fui a Poxoreu, terra do ouro e Diamante

C                G7                       C
Merulio e Toriquejo, Falei com as índias Xavantes
F                    G7                       C
Fui em General Carneiro, em També, em Xavantina,
       C7            F          G7        C
E lá em Barra do Garça conheci belas meninas.

C              G7                         C
Conheci Torixoreu, Ponte Branca e Araguaína
                      G7                        C
Dom Aquino, Rondonópolis, Alto Garça e Quiratinga.
F                  G7                     C
Conheci Alto Araguaia, e uma cidade sem fim
       C7         F             G7        C
Do cantar da siriema lá dos campos de Coxim.

C                     G7                       C
Fui a São Liz da Cárcere, Jaciara e Santa Elvira
                      G7                     C
Banhei nas águas serenas do lindo Rio Itiquira -
F                    G7                     C
Depois fui a Cacilândia, Aparecido do Tabuado
       C7          F           G7        C
Santana do Paranaíba, a Princesa do Estado.

----------------- Acordes -----------------
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
G7 = 3 5 3 4 3 3
