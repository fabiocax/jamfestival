Trio Parada Dura - Dama Entre Aspas (Dama do Vestido Longo) (part. Eduardo Costa)

G
Com o rosto triste por de traz da maquiagem
          C        G          C
Onde ela tenta esconder imensa dor
        D            C              G
Vestido longo, transparente e sensual
           D                          G
Sobre a luz negra de um salão multi cor
G                                 G7
E fim de noite e a boate esta vazia
      C
E a cidade já começa despertar
            D                    G
Todos os boêmios já se foram embora

          D                       G
E aquela dama se prepara pra deitar
 C                  G               C
Senta na cama em frente à penteadeira
                               G
Vê no espelho o seu rosto abatido

                                   D
Neste momento com os olhos rasos d'água
                                G
Vê seu futuro totalmente destruído
         C           G            C
O sono chega e a envolve de mansinho
                                   G
Quando ela sonha ser rainha de um lar
            E                         Am
Por que num mundo onde ninguém é perfeito
      D                        G
Ela também tem o direito de sonhar

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
