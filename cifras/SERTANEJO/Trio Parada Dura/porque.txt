Trio Parada Dura - Porque

[Intro] C  F  C  Dm  G  C  Dm  G

C
Não sei por que...Ultimamente você está me evitando
Por várias vezes eu te encontrei chorando
G                     Dm                   G
Por que? Qual o motivo que não fala Mais comigo
      Dm                  G
Até parece que somos inimigos
C     Dm  G
Por que?
C
Não sei por que...Estou sentido que está tão dividida Está agindo como fosse uma bandida
F
Por que?

 F                                        C
Não sei qual a razão que você Tanto me ignora
                                   Dm
Até parece que você quer ir embora
          G                      C   C7
Se queres ir não posso lhe prender


F                                                C
Amor se o seu amor chegou ao fim      Eu te respeito
                                       Dm
Porém não venha me tratar De qualquer jeito
           G                     C   C7
Talvez precise vir bater na mesma porta

F                               C
Amor eu tenho pena dessa sua estupidez
                                   Dm
Por que você não abre o jogo de uma vez
              G                     C
Mas não me venha me fazer de idiota

[Solo] F  C  Dm  G  C  C7

 F                                        C
Não sei qual a razão que você Tanto me ignora
                                   Dm
Até parece que você quer ir embora
          G                      C   C7
Se queres ir não posso lhe prender

F                                                C
Amor se o seu amor chegou ao fim      Eu te respeito
                                       Dm
Porém não venha me tratar De qualquer jeito
           G                     C   C7
Talvez precise vir bater na mesma porta

F                               C
Amor eu tenho pena dessa sua estupidez
                                   Dm
Por que você não abre o jogo de uma vez
              G                     C
Mas não me venha me fazer de idiota


----------------- Acordes -----------------
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
