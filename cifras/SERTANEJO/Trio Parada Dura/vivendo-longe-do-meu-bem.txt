Trio Parada Dura - Vivendo Longe do Meu Bem

G                 C                 G
Quem eu amo vive tão distante,
D7                                                             G
Talvez já nem mais pensa em mim.
               C                          G
Por isso eu estou sofrendo,
D7                                                   G
Bebendo e chorando assim
F                                                  G
Não sei se está sozinha,
D7                                                   G
Ou se tem outro alguem.
D7                                                       G
Só sei que eu estou morrendo,
D7                                                   G
Vivendo longe do meu bem.
D7                                                    G
Só sei que eu estou morrendo,
D7                                                       G
Vivendo longe do meu bem.
G               C                              G
As lágrimas banham meu rosto,

D7                                                   G
Num pranto de saudade e dor
                 C                          G
Meu Deus se eu tivesse ao menos,
D7                                                         G
Noticias do meu grande amor.
F                                          G
Se ainda está sozinha,
D7                                         G
Ou se tem outro alguem.
D7                                                     G
Só sei que eu estou morrendo,
D7                                                G
Vivendo longe do meu bem.
D7                                                        G
Só sei que eu estou morrendo,
D7                                                    G
Vivendo longe do meu bem.
              C                            G
As lágrimas banham meu rosto
       D7                                      G
Num pranto de saudade e dor.
                C                                        G
Meu Deus se eu tivesse ao menos,
D7                                                       G
Noticias do meu grande amor.
F                                            G
Será que esta sozinha,
D7                                         G
Ou se tem outro alguem.
D7                                                       G
Só sei que eu estou morrendo,
D7                                                 G
Vivendo longe do meu bem.
D7                                                         G
Só sei que eu estou morrendo,
D7                                                 G
Vivendo longe do meu bem.
D7                                                                                 G
Longe do meu bem, Vivendo longe do meu bem.(

----------------- Acordes -----------------
C = X 3 2 0 1 0
D7 = X X 0 2 1 2
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
