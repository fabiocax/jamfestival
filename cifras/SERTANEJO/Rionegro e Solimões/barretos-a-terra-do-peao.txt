Rionegro e Solimões - Barretos a Terra do Peão

(intro) G C D G C D

G                                     D
Eu cheguei em barretos na festa do peão
                                      G
A festa tava boa, tinha gente de montão
                                       D
A galera no estilo chapéu, bota e fivelão
                C                   G
E o som sertanejo embalava a multidão
       D           G
Rererereeii barretos
            D           G
Que festa boa, que emoção
       D           G
Rererereeii barretos
C         D     G
é a terra do peão
                                               D
Tinha boi, tinha cavalo e os peões mais afamados
                                               G
Tinha um locutor narrando de um jeito apaixonado
                                          D
Foi subindo a adrenalina começou a gritaria
           C                D           G
A platéia aplaudia quando o peão não caia
       D           G
Rererereeii barretos
            D           G
Que festa boa, que emoção
       D           G
Rererereeii barretos
C         D     G
é a terra do peão
                                    D
A festa tinha gente do brasil inteiro
                                         G
O povo tava juntado parecia um formigueiro
                                       D
Quando começou o show foi aquele piseiro
                  C        D               G
Chegou a tremer o chão na terra do boiadeiro
       D           G
Rererereeii barretos
            D          G
Que festa boa que emoção
       D           G
Rererereeii barretos
C         D     G
é a terra do peão
C         D     G
é a terra do peão
C         D     G
é a terra do peão

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
