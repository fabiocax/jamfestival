Rionegro e Solimões - Depois que perde

[Intro] D  C  D  G  D

E|--3-2--------------------------|
B|------5---2-3-1----------------|
G|--------4-------4-2------------|
D|--------------------2-4/5------|
A|-------------------------------|
E|-------------------------------|

G          Bm             C
Depois que perde é que se vê
   G
A falta que faz
        Bm         C
Outra pessoa pode haver
    G
Mas não satisfaz
    D                C
O coração só ama uma vez
            G
O resto é paixão
    
D                          Em
Vem, vem ser o que a gente era
            D
Vem com a primavera
                G
Florescer meu caminho
D                       Em
Vem aqui tá tudo sem graça
            D
Solidão me abraça
                G
Longe do seu carinho

G          Bm           C
Agora eu sei o que você é
       G
Importante demais
         Bm         C
Eu faço tudo que quiser
           G
Mas volta atrás

     D
Meu coração
           C
Não é de cristal
            G
Mas está em pedaços 

D                          Em
Vem, vem ser o que a gente era
            D
Vem com a primavera
                G
Florescer meu caminho
D                       Em
Vem aqui tá tudo sem graça
            D
Solidão me abraça
                G
Longe do seu carinho

D                          Em
Vem, vem ser o que a gente era
            D
Vem com a primavera
                G
Florescer meu caminho
D                       Em
Vem aqui tá tudo sem graça
            D
Solidão me abraça
                G
Longe do seu carinho

G          Bm             C
Depois que perde é que se vê

----------------- Acordes -----------------
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
