Rionegro e Solimões - Rei da Festa

Intro: A  E  B7  E

   E                    B7            E
Numa festa de peão, certo dia eu cheguei
                                   B7
Da mesa de inscrição, eu me aproximei
  A                                         E
E disse pra comissão: "quero mostrar meu valor"
                   B7
Disse uma moça no momento:
      A                 B7              E
"Num rodeio de talento não se aceita amador"

( A  E  B7  E )

     E                 B7             E
Com muita educação eu dei a minha resposta:
                                           B7
"Se aqui só tem campeão vou fazer minha proposta
     A                                 E
Você escolherá o touro que me fará derrotado
                    B7
Mas se acaso eu não cair
                     A             B7         A
Dos prêmios que conseguir quero um beijo apaixonado"

( A  E  B7  E )

     E                   B7            E
Com ajuda da comissão o touro foi escolhido
                                        B7
Foi momento de atenção não se ouvia um ruído
     A                                           E
A moça disse disse sorrindo: "vai ser dada a partida
                  B7
Pode despedir do mundo
                A                      E
Será o último segundo que você terá na vida"

       E              B7               E
Deixei o touro no chão, na poeira da arena
                                          B7
Com aplausos da multidão dei um beijo na morena
        A                                    E
Ninguém traz a competência estampada em sua testa
                 B7
Ganhei aquele troféu
                   A
Saí do banco dos réus
    B7              E
para ser o rei da festa
                  B7
Ganhei aquele troféu
                 A
Saí do banco dos réus
     B7           E
para ser o rei da festa

( A  E  B7  A  G#m  F#m  E )

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
