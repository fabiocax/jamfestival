Rionegro e Solimões - Ponto de Chegada

(intr) G F C G ( F Em Dm C ) G

E|--------------------------------------------------------------|
B|----7/8---5--6------5-6-5--------7/8-----5-6-5----------------|
G|--------7-------5-7--------7---------7---------7-5------------|
D|--5---------------------------5-------------------------------|
A|----------------------------------------------------2-0---0-2-|
E|--------------------------------------------------------3-----|

     C
Não precisa me dizer que não me quer
      F
Eu percebo pelo gesto e pelo olhar

Que seus gostos já não são os mesmos gostos
           C      G
Do nosso tempo.
        C
Eu me lembro que era fácil lhe agradar
      F
Que bastava meu amor pra ser feliz

De repente o que sou já não importa
         C      G
Nesse momento.

(refrão)
       C
O que foi que aconteceu com a gente
      C7               F
Por onde anda o nosso amor
                       G
Porque seu jeito indiferente
             C       G
Quando me aproximo.

      C
Quem foi que apagou a chama
     C7                F
Que havia em nossos corações
       G
Quem transformou as nossas vidas
         C          G
Separou nós dois.

(intro)

      C
Não precisa me dizer que não me quer
     F
Seu olhar pela janela já me diz

E você está buscando novos rumos
       C         G
Outros caminhos.

     C
Eu lembro quando a gente começou
       F
Sua estrada terminava sempre em mim

Já não sou mais o seu ponto de chegada
       C       G
Vivo sozinho.

(refrão  2x)

 C    ( F C F C )
Nos dois

----------------- Acordes -----------------
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
