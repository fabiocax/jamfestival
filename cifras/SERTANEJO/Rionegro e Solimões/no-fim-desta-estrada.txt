Rionegro e Solimões - No Fim Desta Estrada

Introdução -  C9  D9  G/B  C9  Am7  D  C  G


Solo Introdução   G|--4-5---7------------7----7-----9-----------------------------------4--5-----5--7
                  B|-----------8------8----5----------8--10----------------8---10---8---------8-8------10-10--
                  E|------------------------------------------7----10----7

                  G|---8-7-8


G/B C G         D/F#
Te procurei pela cidade
    C9        D
Em cada rua em cada esquina
     G/B
Eu te busquei
               D/F#
Te procurei entre o céu e o mar
      C9     D      G  G7
Em nenhum lugar eu pude te encontrar.

Refrão

C9            G/B  D  Em
Na minha saudade e na lembrança
D/F#  C9         G
Posso te ouvir, posso te ver

C9                G/B D  Em
Se ainda me resta alguma esperança
                 D          C   D   G
No fim desta estrada eu vou estar com você.


   G/B       C      D/F#
Parece distante a felicidade
    C9     D/F#       G   E9   G/B
É como a água que escorre pelas mãos
      G/B   C  G
Pode estar perto
      D/F#
Onde, eu não sei
  C9       D/F#       G/B   C9  G/B
É mistério que aflinge o coração.

Refrão

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D9 = X X 0 2 3 0
E9 = 0 2 4 1 0 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G7 = 3 5 3 4 3 3
