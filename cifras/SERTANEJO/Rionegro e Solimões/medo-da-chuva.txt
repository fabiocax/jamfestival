Rionegro e Solimões - Medo da chuva

(intro) E A E A E

E |------------77----------------------------------------------------|
B |----2/4--------7-7-7-7-7--9-10--10-10-10-10-10-9-7----10--9-7-----|
G |---------------------------------------------------9----------9---|
D |------------------------------------------------------------------|
A |------------------------------------------------------------------|
E |------------------------------------------------------------------|

 E                                 B
É pena que você pensa que eu sou seu
         B7
escravo,dizendo que eu sou seu marido,e não posso
 E            A                                  E
partir,como as pedras imóveis na praia eu fico ao
              F#m7     B7
seu lado,sem saber dos amores que a vida me trouxe e
             E E7    A
eu não pude viver,eu perdi o meu medo, o meu medo, o meu
       E           A
medo da chuva,pois a chuva voltando pra terra traz
         E       A
coisas do ar, aprendi o segredo,o segredo,o segredo
     E      B7
da vida,vendo as pedras que choram sozinhas no
       E       B
mesmo lugar, vendo as pedras que choram sozinhas no
        E
memso lugar.

( E A E A E )

  E
Eu não posso entender tanta gente aceitando a
  B        B7
mentira, de que os sonhos desfazem aquilo que o padre
 E E7        A
falou, porque quando eu jurei meu amor eu traí a mim
 E             F#m7    B7
mesmo, hoje eu sei que ninguém desse mundo e feliz
                 E     E7
tendo amado uma vez, uma vez
A
eu perdi o meu medo
                  E         A
meu medo,meu medo da chuva,pois a chuva voltando pra
                    E     A
terra traz coisas do ar,aprendi o segredo,
                          E       B7
o segredo,o segredo da vida,vendo as pedras que
                          E      B
choram sozinhas no mesmo lugar,vendo as pedras que
                          E      B7
choram sozinhas no mesmo lugar,vendo as pedras que
                         E A E A E
sonham sozinhas no mesmo lugar.

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m7 = 2 X 2 2 2 X
