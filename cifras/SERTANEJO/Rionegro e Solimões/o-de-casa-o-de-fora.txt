Rionegro e Solimões - Ô de casa Ô de Fora

[Intro] Bb  F  C  F

F       F7       Bb
Ô de casa, ô de fora
C                          F
Se for a saudade manda embora
                 C
Diga que a felicidade
Bb               F
Pôs a solidão pra fora
Bb     F         C
Ô de casa, ô de fora
Bb         C              F
Se for a saudade manda embora

( Bb  F  C  F )

    F
Eu agora estou vivendo
                     C
O amor que pedi pra Deus

Ela entrou em minha vida
     Bb             F
Completou os sonhos meus

Depois que ela chegou
                      C
Nessa casa ninguém chora
     Bb               C
Tenho todo o amor do mundo
                 F
Ô de casa, ô de fora

( Bb  F  C  F )

F       F7       Bb
Ô de casa, ô de fora
C                          F
Se for a saudade manda embora
                 C
Diga que a felicidade
Bb               F
Pôs a solidão pra fora
Bb     F         C
Ô de casa, ô de fora
Bb         C              F
Se for a saudade manda embora

( Bb  F  C  F )

F       F7       Bb
Ô de casa, ô de fora
C                          F
Se for a saudade manda embora
                 C
Diga que a felicidade
Bb               F
Pôs a solidão pra fora
Bb     F         C
Ô de casa, ô de fora
Bb         C              F
Se for a saudade manda embora

( Bb  F  C  F )

F
Eu agora estou vivendo
                     C
O amor que pedi pra Deus

Ela entrou em minha vida
     Bb             F
Completou os sonhos meus

Depois que ela chegou
                      C
Nessa casa ninguém chora
     Bb               C
Tenho todo o amor do mundo
                 F
Ô de casa, ô de fora

( Bb  F  C  F )

F       F7       Bb
Ô de casa, ô de fora
C                          F
Se for a saudade manda embora
                 C
Diga que a felicidade
Bb               F
Pôs a solidão pra fora
Bb     F         C
Ô de casa, ô de fora
Bb         C              F
Se for a saudade manda embora

[Final] Bb  F  C  F

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
