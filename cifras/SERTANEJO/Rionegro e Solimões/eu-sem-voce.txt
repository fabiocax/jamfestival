Rionegro e Solimões - Eu Sem Você

Intr.: F  Bb  F  Bb  F

F                         C
Eu sem você, arroz sem feijão, 
                         F
Eu sem você, leite sem café, 
              F7          Bb
Eu sem você, goiabada sem queijo,
         F           C       F
Eu sem você, pimenta sem acarajé.

                       C
Eu sem você, é dia sem sol, 
                         F
Eu sem você, é noite sem lua, 
                F7          Bb
Eu sem você, Brasil sem futebol,
          F    C           F
Rio sem escola de samba na rua...

           C                     F
Mais com você, sou paixão sou alegria,
                C             Bb         F
Festa de peão Barretos e o carnaval da Bahia
           C                 F
Mais com você eu sou pura emoção,
                Bb           C         F
Pula, pula bate forte no meu peito coração.

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
