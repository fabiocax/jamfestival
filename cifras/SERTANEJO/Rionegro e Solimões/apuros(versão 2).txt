Rionegro e Solimões - Apuros

D                            A
Estou sempre vivendo em apuros 
             D 
E muito inseguo 
               A
Por estar tão só 
                 D 
Cada dia que passa 
                             A
É uma troca de amor que eu faço 
          D 
Assim é pior 
 
                    A 
O motivo de tudo porque 
             D 
Foi perder você 
                   A 
Hoje eu tento enganar 
                       D 
Essa minha saudade imensa 
                                A 
Que virou doença tentando te achar 
 
 
             A 
Minhas queixas 
          D 
Meus apuros 
            A 
Minha insônia 
                D 
Meus dias escuros 
                          A 
A saudade quando fica assim 
                      D 
É sinal que não tem fim 
 
 
A                             D 
Cada amor diferente que encontro 
                               A 
Renova a esperança de te esquecer 
                          D 
Mas estando ao lado de outro 
                                       A 
Tem coisas iguais as que eu fiz com você 
                            D 
Meus apuros retornam eu penso 
                    A 
Que amor tão imenso 
                  D 
Não pode haver dois 
                              A 
É impossível que alguém faça tudo 
                                 D 
Construa um mundo e esqueça depois

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
