Rionegro e Solimões - Vida de Cão

e|-----------------------------------------|   
B|-----------------------------------------|   
G|---7/9--9--9--7--7--5--5--7/10--10--10---|   
D|-----------------------------------------|   
A|-----------------------------------------|   
E|-----------------------------------------|   

e|-----------------------------------------------------------------|   
B|-----------------------------------------------------------------|   
G|-----7---10--10--9---------5---8--8--7---------3--5^6--5--4--3---|   
D|----9-------------------7-------------------5--------------------|   
A|---9------------------7--------------------5---------------------|   
E|-----------------------------------------------------------------|   

             C                 G
Já faz tanto tempo que tudo acabou, 
                         F                    C
mas meu coração com  a solidão não se acustumou, 
                C7            F
Eu só penso nela não tem solução 
                       C
Saudade sufoca o dia inteiro, 
                               G
Quando chega a noite é um disespero 
                             C
Não agüento mais o vida de cão 

Refrão
          C7                 F
De alguma coisa ai pra eu bebe 
                              C
Pois já não consigo me controlar 
                             G
Esse amor passou todos os limites 
           F           C
Eu to precisando desabafar 

          C7                 F
De alguma coisa ai pra eu bebe 
                          C
Não se preocupe se eu chorar 
                    G
To apaixonado desesperado 
            F                 C
Vou ficar maluco se ela não voltar

e|-----------------------------------------|  
B|-----------------------------------------|   
G|---7/9--9--9--7--7--5--5--7/10--10--10---|   
D|-----------------------------------------|   
A|-----------------------------------------|   
E|-----------------------------------------|   

e|-----------------------------------------------------------------|   
B|-----------------------------------------------------------------|   
G|-----7---10--10--9---------5---8--8--7---------3--5^6--5--4--3---|   
D|----9-------------------7-------------------5--------------------|   
A|---9------------------7--------------------5---------------------|   
E|-----------------------------------------------------------------|   

                C7            F
Eu só penso nela não tem solução 
                       C
Saudade sufoca o dia inteiro, 
                               G
Quando chega a noite é um disespero 
                             C
Não agüento mais o vida de cão 

Refrão
          C7                F
De alguma coisa ai pra eu bebe 
                              C
Pois já não consigo me controlar 
                             G
Esse amor passou todos os limites 
           F           C
Eu to precisando desabafar 

          C7                 F
De alguma coisa ai pra eu bebe 
                          C
Não se preocupe se eu chorar 
                    G
To apaixonado desesperado 
            F                 C
Vou ficar maluco se ela não voltar

"AGORA TODOS OS APAIXONADOS:"

          C7                 F
De alguma coisa ai pra eu bebe 
                          C
Não se preocupe se eu chorar 
                    G
To apaixonado desesperado 
            F                 C
Vou ficar maluco se ela não voltar   !

...Mais uma pra nois garçon !!

----------------- Acordes -----------------
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
