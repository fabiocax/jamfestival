Rionegro e Solimões - Eu de Cá, Você de Lá

(intro) A  E  A  E  A  E  A  D  E  A

A
No final dessa história
                              E
Quem vai perder,  quem vai ganhar

Se o amor é tão forte
                               A
Não temos motivos, pra nos machucar

A
Não adianta fugir
              A7                 D
Porque essa paixão, nasceu pra ficar
                  A
Vamos parar de bobeira
                E                       A A7
Porque essa ciumeira, só faz a gente brigar

D                     A
Porque o tempo tá passando
                  E
E a gente se machucando
                 A
Eu de Cá, Você de Lá

         E
E quando bate saudade
      A
No coração dá medo
       E                 A
Da solidão e vontade de voltar
        E                  A
Eu tô maluco, ando numa bebedeira
                   D
Deus do Céu é brincadeira
      E           A
Eu de Cá, Você de Lá

                  E
Eu de Cá, Você de Lá
                  A
Eu de Cá, Você de Lá
                  E
Eu de Cá, Você de Lá
                  A
Eu de Cá, Você de Lá

(intro)

(repete tudo)

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
