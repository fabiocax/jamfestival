Rionegro e Solimões - Só Eu Sei

C                   F
Se é pra valer, ou não
G      F         C
Eu só quero entender
                    F
Me tira o sono, o juízo
G           F          C
Depois pede pra esquecer

         Dm        G
Você me liga, carente
    Dm              G
Uma voz, tão envolvente
   F
Iêêêê...
              C
Me faz seu abrigo
       Dm            G
Como sempre, eu te aceito
       Dm                G
Eu te quero de qualquer jeito
    F
Uhuuuhu...
                 G
Porque eu te preciso

      C     F      G  F
A verdade é que você
         C         F         G
Deita e rola com meus sentimentos
F    C F G F   
Eu sei..

          C       F          G
O quanto sofro, o quanto choro..
F      C       F          G   F   
Quando você se vai, só eu sei,
       C    F   G
Só eu sei

         Dm        G
Você me liga, carente
    Dm              G
Uma voz, tão envolvente
   F
Iêêêê...
              C
Me faz seu abrigo
       Dm            G
Como sempre, eu te aceito
       Dm                G
Eu te quero de qualquer jeito
    F
Uhuuuhu...
                 G
Porque eu te preciso

      C     F      G  F
A verdade é que você
         C         F         G
Deita e rola com meus sentimentos
F    C F G F   
Eu sei..

          C       F          G
O quanto sofro, o quanto choro..
F      C       F          G   F   
Quando você se vai, só eu sei,
     C    F   G   F
Só eu seiiiiiiiii
     C    F   G   F
Só eu seiiiiiiiii
     C    F   G   F
Só eu seiiiiiiiii

----------------- Acordes -----------------
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
