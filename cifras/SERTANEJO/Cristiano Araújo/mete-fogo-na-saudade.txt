Cristiano Araújo - Mete Fogo Na Saudade

D                A
Fiz de tudo pra salvar nosso amor
Bm                  G              D
Quantas vezes eu sorri pra não chorar
              A
Avisei mas você não me escutou
   Bm         A        D
Parecia que queria terminar

Bm                      G
Era isso que queria, conseguiu
               A
Toda nossa história você destruiu
D               A                   G  A
Só não entendo porque quer voltar atrás

Refrão 2x:
D                             A                 Em
Diz que quer me ver, quer me beijar, quer o meu corpo
Em                   G
Mas pega essa saudade e mete fogo

D                           A
Diz que tá sofrendo, tá chorando sem parar
            Em           G
Mas mete fogo na saudade, deixa queimar

Intro:  (D A Bm G)

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
