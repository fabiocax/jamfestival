Cristiano Araújo - Efeitos

[Intro]  Dm  C  A#  C

E|----------------------------------------------------------------|
B|----------------------------------------------------------------|
G|--5/7--7/9--9/10--10/12---12-10-12-10---------------------------|
D|---------------------------------------10h12-12-10----10--------|
A|---------------------------------------------------12----12-10--|
E|----------------------------------------------------------------|

 Dm                     C      F
Seus efeitos ainda dominam em mim
                           A#      C
As lembranças só me distanciam do fim
               A#         C           Dm     A#  C
Da batalha travada em tentar te esquecer

 Dm                              C       F
Te vejo nos meus sonhos me devolvendo o céu
                          A#    C
Então sinto na boca o gosto do mel
               A#            C    Dm   C
Que eu sentia sempre que beijava você


 A#                            C       Dm
Só sei que esse romance ainda vive em mim
                                       A#       C
O efeito do seu beijo que me deixou assim, assiiim

  F              (Pausa)   F      A#     C
Preciso de um remédio que cure essa saudade
                                      Dm
Que diminua a dor que no meu peito invade
                              A#      C
Que me cure ou me ajude a esquecer, iêee, iêee

  F              (Pausa)    F    A#     C
Preciso de um antídoto que salve esse amor
                                    Dm
Que tire os sintomas que me causam dor
                             A#
Eu não sei mais o que vou fazer
          C                     F    C
Se pra curar o meu remédio é você

[Solo] Dm  F  C  Dm

E|----------------------------------------------------------------|
B|----------------------------------------------------------------|
G|--5/7--7/9--9/10--10/12---12-10-12-10---------------------------|
D|--------------------------------------10h12-12-10----10---------|
A|--------------------------------------------------12----12-10---|
E|----------------------------------------------------------------|

 Dm                     C      F
Seus efeitos ainda dominam em mim
                           A#      C
As lembranças só me distanciam do fim
               A#         C           Dm     Bb  C
da batalha travada em tentar te esquecer

 Dm                              C       F
Te vejo nos meus sonhos me devolvendo o céu
                       A#       C
Então sinto na boca o gosto do mel
               A#         C       Dm   C
que eu sentia sempre que beijava você

 A#                            C       Dm
Só sei que esse romance ainda vive em mim
                                       A#       C
O efeito do seu beijo que me deixou assim, assiiim
  F              (Pausa)   F      A#     C
Preciso de um remédio que cure essa saudade
                                      Dm
Que diminua a dor que no meu peito invade
                              A#      C
Que me cure ou me ajude a esquecer, iêee, iêee

  F              (Pausa)    F    A#     C
Preciso de um antídoto que salve esse amor
                                    Dm
Que tire os sintomas que me causam dor
                             A#
Eu não sei mais o que vou fazer
          C                     F    C
Se pra curar o meu remédio é você
       Dm
Seus efeitos

----------------- Acordes -----------------
A# = X 1 3 3 3 1
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
