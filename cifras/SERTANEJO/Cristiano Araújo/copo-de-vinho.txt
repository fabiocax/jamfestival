Cristiano Araújo - Copo de Vinho

Intro: ( G#m  E   F# ) (2x)

G#m                    E            F#          G#m
    Eu e minha namorada a gente tava agarradinho,
               E          F#            G#m
até que o meu amor bebeu um copo de vinho

                E          F#         G#m
Ela perde o controle, ela fica animadinha,
                  E        F#          G#m
ta todo mundo olhando, a minha patricinha

                  E            F#            G#m
Tá chapada, tá doidona, tá descendo até o chão,
                E        F#          G#m
eu tô pagando mico, olha que situação

                     E             F#            G#m
E vai descendo descendo, perdendo a linha devagar,
                 E              F#           G#m
e vai subindo subindo, ela não para de dançar


  E    F#       G#m
Amor assim não dá...(2x)
               E          F#         G#m         G#m           E          F#          G#m
Eu gosto de você, você, você, você, você, você, você, você, você, você, você, você quer...?

----------------- Acordes -----------------
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
