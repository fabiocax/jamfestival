Cristiano Araújo - Perdeu o Cara Errado

Am                         F
Nesses dias chuvosos de frio
              C                           G
É aí que o vazio da tua ausência aumenta mais
Am                           F
E o silêncio desse telefone tá me deixando doente
C
Será que você também sente
G
Não há ser humano que aguente

Dm                                     F
Tanta solidão vai acabar com a minha raça
                     C
Não há nada que eu faça pra evitar o pranto
G
Já vi que só tá começando
  Dm                                   F
Se hoje eu já bebi, já sofri mais uma vez
                  C             G
Imagina daqui um mês, daqui um mês


  Dm                                         F
Jogado, bebendo pra entender o que cê fez comigo
                                  C
Um copo de cerveja, um coração ferido
                    G
Não foi falta de aviso
          Dm
E se eu sumir
                                   F
Não é coincidência ou coisa do destino
                                      C
Quando me ver com outra e estiver sorrindo
                     G
Vai me ver no seu passado

Você perdeu o cara errado

( Dm  F  C  G )

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
