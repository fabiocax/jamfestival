﻿Cristiano Araújo - Maus Bocados

Capo Casa 3

Intro: A9  D9
       A9  D9  D9/C#  Bm  E  D9/F#  E/G#  E
       A9  D9  A9


Primeira Parte:

                              A9
Sei que seu coração falou de mim
                                     D9   D9/C#
Sei que ele falou que eu tô fazendo falta
Bm                                   E
  Ele falou também que sem mim tá difícil
                                    A9   D9  A9
As noitadas e os amigos não tão ajudando


Segunda Parte:

                                A9  D9
Sei que seu coração gritou por mim

A9                                     D9  D9/C#
  Na última moda sertaneja que o DJ tocou
Bm                                      E
  Pra piorar era aquela que a gente dançava

A saudade bateu e você chorou


Terceira Parte:

D9                 E
  É, eu sei, tá difícil
               D9/F#
E se me perguntar
              E/G#   E
Como sei tudo isso


Refrão:

                    A9
É que eu também passei
                  D9
Por esses maus bocados
           D9/C#   Bm
Sofri, chorei, largado
               E        D9/F#     E/G#
E não te esqueci, não, não, não, não
    E      A9
Também passei
                            D9
E te liguei bêbado fora de hora
            D9/C#       Bm
Que nem cê tá fazendo agora
              E
Ligando a cobrar
    D9/F#   E/G#      A9
Chorando, querendo me amar

( D9  D9/C#  Bm  E  D9/F#  E/G#  E )
( A9  D9  A9 )


Variação Segunda e Terceira Parte:

                                A9  D9
Sei que seu coração gritou por mim
A9                                     D9  D9/C#
  Na última moda sertaneja que o DJ tocou
Bm                                      E
  Pra piorar era aquela que a gente dançava
                           D9
A saudade bateu e você chorou
                 E
É, eu sei, tá difícil
               D9/F#
E se me perguntar
              E/G#   E
Como sei tudo isso


Refrão Final:

                    A9
É que eu também passei
                  D9
Por esses maus bocados
           D9/C#   Bm
Sofri, chorei, largado
               E        D9/F#     E/G#
E não te esqueci, não, não, não, não
    E      A9
Também passei
                            D9
E te liguei bêbado fora de hora
            D9/C#       Bm
Que nem cê tá fazendo agora
              E
Ligando a cobrar
    D9/F#   E/G#      A9
Chorando, querendo me amar

                    A9
É que eu também passei
                  D9
Por esses maus bocados
           D9/C#   Bm
Sofri, chorei, largado
               E        D9/F#     E/G#
E não te esqueci, não, não, não, não
    E      A9
Também passei
                            D9
E te liguei bêbado fora de hora
            D9/C#       Bm
Que nem cê tá fazendo agora
              E
Ligando a cobrar
    D9/F#   E/G#      A9   D9  D9/C#
Chorando, querendo me amar
Bm            E         A9   D9  A9
  Chorando, querendo me amar

----------------- Acordes -----------------
Capotraste na 3ª casa
A9*  = X 0 2 2 0 0 - (*C9 na forma de A9)
Bm*  = X 2 4 4 3 2 - (*Dm na forma de Bm)
D9*  = X X 0 2 3 0 - (*F9 na forma de D9)
D9/C#*  = X 4 0 2 3 0 - (*F9/E na forma de D9/C#)
D9/F#*  = 2 X 0 2 3 0 - (*F9/A na forma de D9/F#)
E*  = 0 2 2 1 0 0 - (*G na forma de E)
E/G#*  = 4 X 2 4 5 X - (*G/B na forma de E/G#)
