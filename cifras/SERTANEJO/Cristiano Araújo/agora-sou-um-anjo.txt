Cristiano Araújo - Agora Sou Um Anjo

INTRO: C G Am F

       C G            Am                       F
Não chore pois estou aqui mesmo sem poder lhe ver
  C           G                Am             F
agora sou um anjo que vai te guiar pode escrever
       C G             Am                    F
não chore quero te ver bem quero te ver sorrindo
 C                G                   Am             F
a noite olhe pro céu vou ta entre as estrelas te pedindo

 C          G                  Am
calma não pense em fazer besteiras
             F          G            C
se estiver triste olhe para as estrelas
          G                Am
daqui de cima eu vou te ajudar
                F          G            C
a encontrar a melhor forma pra te consolar

           G                Am
dizem que levaram o cara errado

      F          G            C
e sei que queriam ter me ajudado
                G                Am
mas quando a brisa tocar o seu rosto
         F          G            C
pode certeza que sou eu do seu lado

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
