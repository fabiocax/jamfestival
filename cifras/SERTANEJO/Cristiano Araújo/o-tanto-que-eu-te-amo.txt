Cristiano Araújo - O Tanto Que Eu Te Amo

Intro:  A E Bm D A E

A
Meu coração detonou,
C#m
A minha mente pirou,
           D
Quando eu percebi que havia
       Bm                   E
Uma distância entre eu e você.

A
Distância só trás a dor,
C#m
Quando se tem um amor,
      D                        Bm
E te ver me olhando assim diferente
              E
Faz o peito doer.

D
Eu não preciso de palavras

       F#m
Pra dizer o amor por si só,
              D
Vai te convencer que o tempo
       Bm                 E
É o melhor caminho pra mudar
       D       E    A      E
E pra gente se acertar.

(Refrão)
A
Não cabe um sentimento sem razão,
E
Escute, siga a voz que vem do coração,
         D                        Bm
Me dá um beijo, flor, só sinta o meu calor,
D                                       E
Perceba a força que ainda existe nesse amor.

A
Não cabe um sentimento sem razão,
E
Escute, siga a voz que vem do coração,
          D                       Bm
Me dá um beijo, flor, só sinta o meu calor,
D                                       E
Perceba a força que ainda existe nesse amor.
                    A
E o tanto que eu te amo!!!

(Intro)
D
Eu não preciso de palavras...

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
