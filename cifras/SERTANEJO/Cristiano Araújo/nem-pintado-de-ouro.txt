Cristiano Araújo - Nem Pintado de Ouro

Intro 2x: A  E  F#m  D

A                          E
Você não sabe decifrar o amor
                               F#m
Mas tem diploma em me fazer sofrer
                            D
É imprudente sempre causa dor
                         A
Mas acordei agora pode crer
                                 E
Não leve mal mais gosto mais de mim
                               F#m
Já foi, já deu meu lance com você
                             D
Meu coração resolveu te excluir
                               A  E  D
E nem pintada de ouro quer te ver

A                  E
Se quer falar de amor

               F#m                      D                    A
Conjugue no passado pois no seu futuro já não estarei do lado
A                  E
Se quer falar de amor
             F#m                          D                   A
Não use o presente pois o amor que eu sentia hoje está ausente

A                   E
Você não sabe, não sabe decifrar o amor
F#m                 D
Você não sabe, não sabe decifrar o amor
A                   E                  D
Você não sabe, não sabe decifrar o amor

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
