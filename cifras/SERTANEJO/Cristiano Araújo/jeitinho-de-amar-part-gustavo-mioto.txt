Cristiano Araújo - Jeitinho de Amar (part. Gustavo Mioto)

Em
Se ligou sem razão
Am        C
Tá com saudade mas finge que não
Em                                  D
Cê não me engana, lembrou da nossa cama
Em
E eu não vou mentir
Am         C
Eu quero você todo dia
Em                          D
Do mesmo jeito que a gente fazia
C          G              D           Em
E se arrepia toda vez que a gente se fala
C         G                       D
E a mensagem que eu mandei agora pouco
            C
Foi pra provocar

G
Tô te esperando

               D                  Am
Mas se arruma toda, se perfuma toda
                   C
Vem fazer amor gostoso e vai embora
G           D             Am
Se é teu jeitinho de me amar
     C                    G
Sei lá, posso me acostumar

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
