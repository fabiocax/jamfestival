Cristiano Araújo - Já Te Desenhei

Intro: F  C  Bb  Dm  C

Solo:
E|--------------------------------------------------------------|
B|--------------------------------------------------------------|
G|-------------------------------------7-9-9h10-9-7-5-7---------|
D|-3--7-7h5-5h3-3h5--7-7h5-5h3-3---3h5------------------7-5h3-3-|
A|--------------------------------------------------------------|
E|--------------------------------------------------------------|

F
Pra que fazer assim

Por que que não disfarça?
    C
Se já chegou ao fim
                        Dm
Não ligo se me maltrata não
             Bb
Porque amo você


    G
Por todos os lugares
    D#
Me perco entre olhares
   Bb
Querendo te encontrar
    F
Preciso outra vez te ver
    G
Às vezes falta o ar
    D#
Não sei nem respirar
   Bb
Eu vou perdendo o chão
    F                    A
Seu olhar é minha direção

Refrão:
   Dm
Eu morro por um beijo
     Bb
Vivo pelo seu amor
    F
Eu já te desenhei
      C
Te invento se preciso for
    Dm
Sua boca é meu céu
    Bb
Seu corpo é o paraíso
   F
Te amo no presente
     C
No futuro eu quero estar contigo

( Dm  Bb  F  C ) (2x)

    G
Por todos os lugares
    D#
Me perco entre olhares
   Bb
Querendo te encontrar
    F
Preciso outra vez te ver
    G
Às vezes falta o ar
    D#
Não sei nem respirar
   Bb
Eu vou perdendo o chão
    F                    A
Seu olhar é minha direção

Refrão 4x:
   Dm
Eu morro por um beijo
     Bb
Vivo pelo seu amor
    F
Eu já te desenhei
      C
Te invento se preciso for
    Dm
Sua boca é meu céu
    Bb
Seu corpo é o paraíso
   F
Te amo no presente
     C
No futuro eu quero estar contigo

( Dm  Bb )

   F
Te amo no presente
     C                         Dm
No futuro eu quero estar contigo

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D# = X 6 5 3 4 3
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
