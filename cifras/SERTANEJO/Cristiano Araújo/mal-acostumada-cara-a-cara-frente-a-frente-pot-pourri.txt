Cristiano Araújo - Mal Acostumada / Cara a Cara, Frente a Frente (Pot-Pourri)

Intro: A Bm A/C# D A E
      A Bm A/C# D A E D A/C# Bm A E

A
Vou deixar que você vai embora
não precisa nem fechar a porta
                                     A  E/G#  F#m  E
porque eu já sei que não demora logo você vai voltar
Bm                             E
até parece que eu não te conheço
    Bm                            E
você briga diz que eu não te mereço
Bm                              E                      A            E
xinga, chora, jura e diz que nunca mais na vida vai me procurar
A
Agente já esta na boca do povo
todo mundo sabe desse jogo
       A7                          D
de te não aguento e não vou te deixar
                               A
é sempre assim parece brincadeira

                       E/G#   F#m     E
mais longe essa saudade é matadeira
                            A     (A  E/G# F#m E F#m Gm G#m ) ARRANJO
e não demora muito pra você voltar
A                                   Bm
Então volta com a cara de que nada aconteceu
                                 E
dizendo que o seu grande amor sou eu
                                A
e que a vida sem mim não vale nada,
   (A  E/G# F#m E F#m Gm G#m )
Que estava errada
        A
Então volta
                                      Bm
mais sei que não vai passar mais de um mês
                             E
e agente começar tudo outra vez
Bm          D            A    (G  F  E Am)
você já está mal acostumada

intr: ( Am  E  Am  E  Am  G  F  E)

final ( E  Dm  C  Bm  Am)

Am
Vai...
Já to sabendo que você
não me quer mais
que há muito tempo você quer
           Dm
que eu te deixe em paz
que meus beijos não
                      E
já não tem o mais sabor
         Dm
e que a paixão e o encanto
            Am   A
por mim passou
Dm
Vai..

Só não esqueça de deixar
          Am
meu coração

pois ele é seu e eu deixei
           F
em suas mãos
          E
preciso dele pra buscar
          A     E
um novo amor
A
Vai

que eu vou tentar te esquecer
               E
de qualquer jeito

vou sufocar

o meu amor

dentro do peito
           D
E outro alguém irá cruzar
           A   E
o meu caminho
D
Vai...

E pode ser que algum dia
        A
de repente

Eu te encontre cara a cara
E
frente a frente


você vai ver que já não
                         ( A F E Am )
sou mais tão sozinho

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G#m = 4 6 6 4 4 4
Gm = 3 5 5 3 3 3
