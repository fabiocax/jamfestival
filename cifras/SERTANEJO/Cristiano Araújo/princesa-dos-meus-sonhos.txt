Cristiano Araújo - Princesa Dos Meus Sonhos

Intro: Cm  Bb  G#  Fm  D#  Bb

E|--------4--3------------ ------- |
B|- 3-4-6------6--4--- ------- --- |
G|5------------------3-3-3h5--3h5--|
D|----------------------------- ---|
A|----------------------------- ---|
E|---------------------------- --- |

E|-----------------------------|
B|-----------------------------|
G|-----------------------------|
D|---0---3---5-----------------|
A|-----------------------------|
E|-----------------------------|

E|--------------------------------|
B|---------4-3------------------- |
G|-----3-5---- 5-4-3--------------| 
D|--5-----------------5-5-5--3~---|
A|--------------------------------|
E|--------------------------------|


Cm        Bb
Amor as nossas coisas
G#
Continuam no lugar
Fm         D#
Não quis mexer em nada
Bb
Te esperando pra voltar

Cm             Bb             G#
Você que sempre foi minha companheira
Fm               D#
A distancia era nada
Bb
E agora está de mais

Fm            Bb               Cm
Se eu não soube te amar não foi por ti
Fm            D#              Bb
Não creio no amor e não é por mim

Cm            Bb               G#
Mais não quero entender que te perdi
              Bb             Cm
E cada que se vai não vou sorrir
    Bb
Não não

      D#                         Bb             G
Quero te ver outra vez, com os olhinhos embaçados
       Cm
Ao me ver
G#                                   D#
Com uma doçura de um amor, que não te ver
          Bb          G              Cm
Esqueçe a briga daquele ultimo café
G#                       Bb
Você é princesa dos meus sonhos
G#                 Bb     G#              Bb
Quero te ver outra vez, quero te ver outra vez
Fm                        Bb
Você é princesa dos meus sonhos

Cm                 Bb              G#
Esquece o orgulho atras por um instante
Fm         D#                  Bb
Ou me prepara pra ser uma vez mais

Fm            Bb               Cm
Se eu não soube te amar não foi por ti
Fm            D#              Bb
Não creio no amor e não é por mim

Cm            Bb               G#
Mais não quero entender que te perdi
              Bb             Cm
E cada que se vai não vou sorrir
    Bb
Não não

      D#                         Bb             G
Quero te ver outra vez, com os olhinhos embaçados
       Cm
Ao me ver
G#                                   D#
Com uma doçura de um amor, que não te ver
          Bb          G              Cm
Esqueçe a briga  daquele ultimo café
G#                       Bb
Você é princesa dos meus sonhos
G#                 Bb     G#              Bb
Quero te ver outra vez, quero te ver outra vez
Fm                        Bb
Você é princesa dos meus sonhos .

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
D# = X 6 5 3 4 3
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
