Cristiano Araújo - Tchá Tchá Tchá

Bm               G                    D
Ai que vontade, ai que vontade que me dá
                  A                      Bm
De beijar a sua boca, te abraçar e te pegar
Bm               G                    D
Ai que vontade, ai que vontade que me dá
                   A                       Bm
De te colocar no colo e fazer o tchá tchá tchá
 (Bm G D)                     A
Tchá tchá tchá tchá tchá tchá

Bm              G
É sexta-feira, é noite de lua cheia
D                  A
Com esse brilho minha paixão incendeia
Bm             G
To na caçada, to saindo pra balada
D                   A
E tô chegando pra dizer pra essa moçada

Refrão:

Bm               G                    D
Ai que vontade, ai que vontade que me dá
                  A                      Bm
De beijar a sua boca, te abraçar e te pegar
Bm               G                    D
Ai que vontade, ai que vontade que me dá
                   A                       Bm
De te colocar no colo e fazer o tchá tchá tchá
 (Bm G D)                     A
Tchá tchá tchá tchá tchá tchá

Bm              G
É sexta-feira, é noite de lua cheia
D                  A
Com esse brilho minha paixão incendeia
Bm             G
To na caçada, to saindo pra balada
D                  A
E tô chegando pra dizer pra mulherada

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
G = 3 2 0 0 0 3
