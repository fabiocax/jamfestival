Lairton - Seresteiro Das Noites

Intro: G D G D G D

  G
 Existem momentos na vida
     D
 Que lembramos até morrer
 Passados tão tristes no amor
     G                        D
 Que ninguém consegue esquecer
     G
 Carrego uma triste lembrança
       D
 De um bem que jurou me amar
 Está presa em meu pensamento
                      G   G7
 E o tempo não vai apagar
 C
 Fui seresteiro das noites
     G
 Cantei vendo o alvorecer
     D
 Molhado com os pingos da chuva
       G                      G7
 Com flores pra lhe oferecer
  C
 Fui seresteiro das noites
     G
 Cantei vendo o alvorecer
    D
 Molhado com os pingos da chuva
       G                    D G D G D
 Com flores pra lhe oferecer
     G
 Enquanto eu cantava o amor
     D
 Em mim uma paixão nascia
 Entre a penumbra um rosto
     G                     D
 Na janela pra mim sorria
    G
 Um beijo uniu nossas vidas
  D
 Mas sepultou sonhos meus
 meses depois uma carta
                   G    G7
 E nela a palavra adeus
C
 Fui seresteiro das noites
    G
 Cantei vendo o alvorecer
    D
 Molhado com os pingos da chuva
       G
 Com flores pra lhe oferecer
  C
 Fui seresteiro das noites
     G
 Cantei vendo o alvorecer
     D
 Molhado com os pingos da chuva
       G                    D G D G D
 Com flores pra lhe oferecer
      G
 Meus cabelos estão grisalhos
     D
 Do sereno das madrugadas
 Meu violão velho num canto
     G                      D
 Já não faço mais serenatas
    G
 Abraço o calor do sol
   D
 Choro quando vejo a lua
 Parceira das canções lindas
                    G    G7
 Que cantei na sua rua
  C
 Fui seresteiro das noites...

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
