Lairton - Maria Helena

A                    C#m   Cm     Bm   F#7   Bm
Maria Helena és tu a minha inspiração
Bm           E7                A   G#  A
Maria Helena vem ouvir meu coração
A                        C#m     Cm    Bm   F#7   Bm
Na minha melodia eu ouço tu......a     voz
Bm          E7                       A
A mesma lua cheia, há de esperar por nós
A                     C#m   Cm     Bm   F#7   Bm
Maria Helena lembra o tempo que passou
Bm             C#7                C#7
Maria Helena o meu amor não se acabou
    Bm             Dm        A   A   G#   G   F#7
Das flores que guardei uma secou
F#7     Bm           E7          A   F#7
Maria Helena és a verbena que murchou
          Bm          Dm            C#m   F#7
: Maria Helena o meu amor não se acabou
        Bm           E7          A
Maria Helena és a verbena que murchou :

G#   G#   G#   A

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C#7 = X 4 3 4 2 X
C#m = X 4 6 6 5 4
Cm = X 3 5 5 4 3
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
F#7 = 2 4 2 3 2 2
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
