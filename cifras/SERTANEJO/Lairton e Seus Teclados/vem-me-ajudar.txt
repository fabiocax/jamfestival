Lairton - Vem Me Ajudar

E
Perdi você
   B
Porque não julguei
 F#m
Que o nosso amor
   C#m
Não fosse durar
        E
Já não sei o que fazer
    B
Nem por onde vou
D
Tento esquecer
    C#m
Pra não mais chorar
    E
Já pedi seu perdão
    B
Por tudo que fiz
 F#m
Só seu amor
   C#m
Me faz tão feliz
    E
Volte logo, meu bem
    B
Não posso esperar
D              C#m
Pois eu preciso do seu olhar
   A             B
Eu grito seu nome chorando
      B
Mas você não ouve
E
Vem, vem em ajudar
Sem seu carinho
       B
Eu não posso viver
F#m
Vem, vem me ajudar
          B
Porque só tem espinho
No meu caminho
E
Vem, vem me ajudar
                   B
Eu necessito de alguém para mim
F#m
Vem, vem me ajudar
        B
A minha noite é fria, sem alegria
E               B
O meu viver, não tem mais razão
F#m             C#m
Até o sol, não tem mais calor
      E              B
Eu vivia feliz, pois tinha você
D                  C#m
Que me deixou, sem nada dizer
       E
Levo a vida
        B
Pensando onde estará
 F#m               C#m
Quem eu tanto amo, de quem será
      E            B
De que vale viver sozinho assim
D               C#m
Minha saudade não tem mais fim
   A              B
Eu grito seu nome chorando
    B
Mas você não ouve

REFRÃO....

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
