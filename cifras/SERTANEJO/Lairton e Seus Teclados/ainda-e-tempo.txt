Lairton - Ainda É Tempo

   G                       D
Jogo duro não combina com você
  C9                        G
Até quando vai parar de dizer não
  G                        D
Toma jeito você pode me perder
  C                        G
Tô falando com a voz do coração
G7                           C
Coração em desespero é como copo de cristal
  D                           G
A qualquer momento pode se quebrar
  G7                              C
 e o amor em desespero é como barco em tempestade
  Am                          D
A qualquer momento pode naufragar

Refrão:
D7                    G
Ainda é tempo pra nós dois
                     D
Correr atrás do prejuízo
                  C9
Reacender nossa chama
  Am                 D
A vida espera por isso
  D                    G
Ainda é tempo pra nós dois
                   D
Incendiar a nossa cama
                  C9
Fazer amor todo dia
 Am                  D
Porque a gente se ama
                 C9
Por que a gente se ama
D            G
A gente se ama

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
