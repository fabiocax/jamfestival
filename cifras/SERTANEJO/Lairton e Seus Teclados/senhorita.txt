Lairton - Senhorita

D                    G                     D
Minha meiga senhorita eu nunca pude lhe dizer
G                     D
Você jamais me perguntou
C          G                 D
de onde eu venho e pra onde vou
           A            G            D
De onde eu venho não importa, já passou
         A         G            D
O que importa é saber pra onde vou

D|---------------0-----
A|--0-2-3-2-0----------
E|-------------3-------
 D                   G                       D
Minha meiga senhorita o que eu tenho é quase nada
G                     D
Mas tenho o sol como amigo
C            G                  D
Traz o que é seu e vem morar comigo
       A                G                   D
Uma palhoça no canto da serra será nosso abrigo
              A           G                   D
Traz o que é seu e vem correndo, vem morar comigo

D|---0---------
A|-----4-2-0---
E|--------------
A                              G
Aqui é pequeno mas dá pra nós dois
                                    D
E se for preciso a gente aumenta depois

D|---0---------
A|-----4-2-0---
E|-------------
A                                  G
Tem um violão que é pra noites de lua
                                    D
Tem uma varanda que é minha e que é sua
C      G     D               C     G
Vem morar comigo meiga senhorita
             D
Vem morar comigo
Solo: D G A Bm C Bm A D D G A Bm C Bm A D
A

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
