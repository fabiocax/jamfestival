Gustavo Mioto - Amor da Reconciliação

G
Eu morro de medo de viver na solidão,
Am
Mas daria tudo pra ter um por cento do seu coração
C                                                                     G   D
Apesar dos pesares e das brigas ainda sou teu
G
Se for pra morrer, que seja de saudade,
Am
Se for pra durar, que dure a eternidade.
C                                                                                  G
Se mandar mensagem dizendo te amo, que seja verdade
          Am                       C
E se quer saber porque tanto amor

G
Porque a gente se gosta,

A gente se ama, a gente se entende.
                           Em
Quando você tá brava


É exatamente quando eu tô carente.
                             C                                      Am
Se eu te peço um beijo, você diz que não,

Machuca o meu coração,
            C                                                     D
Mas depois de um segundo vem a melhor parte,
                  G
O amor da reconciliação.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
