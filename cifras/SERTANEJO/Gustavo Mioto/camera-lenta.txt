﻿Gustavo Mioto - Câmera Lenta

Capo Casa 3

[Intro] Am  C  G  D

   Am
O amor quando chega
Ele não vem rápido
  C                       G  D
Quando ele vem vem de foguete
     Am
Ele fura o sinal não tá nem aí
      C                    G  D
Me atropelou tava sem capacete

Am                              C
Arranhado no corpo foi coxa na mão
G                               D
 Puxão de cabelo quando ela beijou
      Am                    C                      D
Foi quente acelerado faltou ar até que a porta fechou

        Am
Câmera lenta

              C
A gente fez amor
              G                        D
Senti cada pedaço do seu corpo no meu corpo
                 Am               C
Liguei o ventilador mas não adiantou
              G                       D
O fogo que pegou aqui queimou o cobertor

        Am
Câmera lenta
              C
A gente fez amor
              G                        D
Senti cada pedaço do seu corpo no meu corpo
                 Am               C
Liguei o ventilador mas não adiantou
              G                       D
O fogo que pegou aqui queimou o cobertor

( Am  C  G  D )
( Am  C  G  D )

   Am
O amor quando chega
Ele não vem rápido
  C                       G  D
Quando ele vem vem de foguete
     Am
Ele fura o sinal não tá nem aí
      C                    G  D
Me atropelou tava sem capacete

Am                              C
Arranhado no corpo foi coxa na mão
G                               D
 Puxão de cabelo quando ela beijou
      Am                    C                      D
Foi quente acelerado faltou ar até que a porta fechou

        Am
Câmera lenta
              C
A gente fez amor
              G                        D
Senti cada pedaço do seu corpo no meu corpo
                 Am               C
Liguei o ventilador mas não adiantou
              G                       D
O fogo que pegou aqui queimou o cobertor

        Am
Câmera lenta
              C
A gente fez amor
              G                        D
Senti cada pedaço do seu corpo no meu corpo
                 Am               C
Liguei o ventilador mas não adiantou
              G                       D
O fogo que pegou aqui queimou o cobertor

        Am
Câmera lenta
              C
A gente fez amor
              G                        D
Senti cada pedaço do seu corpo no meu corpo
                 Am               C
Liguei o ventilador mas não adiantou
              G                       D
O fogo que pegou aqui queimou o cobertor

Câmera lenta

( Am  C  G  D )
( Am  C  G  D )

----------------- Acordes -----------------
Capotraste na 3ª casa
Am*  = X 0 2 2 1 0 - (*Cm na forma de Am)
C*  = X 3 2 0 1 0 - (*D# na forma de C)
D*  = X X 0 2 3 2 - (*F na forma de D)
G*  = 3 2 0 0 0 3 - (*A# na forma de G)
