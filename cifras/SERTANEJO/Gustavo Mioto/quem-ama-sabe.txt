Gustavo Mioto - Quem Ama Sabe

G            D/F#
Não diga não
Em           C                 D
Que eu te conheço , sei o que pensa
          G   D
Não diga não
G           D/F#
Porque será
Em              C               D
Que eu nÃo te escuto falar de amor
        G       D/F#    Em
Usar o tempo a nosso favor?
       C                D
Não adianta dizer que agora
          C
Você me quer

    D              D7
Porque , não vejo mais
         G         D/F#      Em
O teu sorriso brilhando pra mim

    C                Am
Porque , não sinto mais
         D    D7
Você em mim

[Refrão]

          Am                   D
Quem ama sabe quando tudo se acaba
             G           D/F#      Em
O brilho dos olhos de repente se apaga
          C                  D
Como num filme que termina triste
         G        G7
Você se Vai , Oh não
          Am                D
Quem ama Sabe onde nasce a dor
            G         D/F#       Em
E chega ao fim uma história de amor
         C             Am
Tenho você em minhas mãos
        C           Am
Mas eu sei que amanhã
         D
Você se vai

          G   D/F#
Não aprendi
Em        C                   D
Fechar os olhos fingir e não ver
           G        D/F#    Em
Que o teu amor é somente prazer
      C                D
Mas você não encontra coragem
       C
Pra dizer
    D             D7
Será , dentro de mim
           G         D/F#      Em
Como uma noite de inverno porque
   C             Am
Será , dentro de mim
          D  D7
Eu sem você

[Refrão]

          Am                   D
Quem ama sabe quando tudo se acaba
             G           D/F#      Em
O brilho dos olhos de repente se apaga
          C                  D
Como num filme que termina triste
         G        G7
Você se vai , oh não
          Am                D
Quem ama Sabe onde nasce a dor
            G         D/F#       Em
E chega ao fim uma história de amor
         C             Am
Tenho você em minhas mãos
        C           Am
Mas eu sei que amanhã
         C
Você se vai
        D          G
Não estará mais aqui

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
