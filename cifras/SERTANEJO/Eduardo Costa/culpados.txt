﻿Eduardo Costa - Culpados

Capo Casa 2

C
Tá pior assim
        Am
Não foi bom pra mim
         F                       G
Com certeza tá difícil pra você também
C                 Am
Você quis eu quis Passo infeliz
     F                          G
Foi besteira separarmos não pensamos bem
F                             G
Eu sei que faço falta em sua vida
  F                        C
Eu sei o seu desejo de voltar
F                         C
Por outro lado fico sem saída
F                           G
E sem você amor também não sei ficar
C                          Am
Nos braços da dor eu me prendo
     F                   G
Lembrando o que a gente viveu

 C                     Am
Você também sofre por dentro
F                    G
Pensando no sorriso meu
C                      Am
Por medo de errar nos erramos
F                     G
Culpados por não perceber
C                             Am
Que a gente foi feito um pro outro
F                     G    C.
Eu sempre vou ser de você

----------------- Acordes -----------------
Capotraste na 2ª casa
Am*  = X 0 2 2 1 0 - (*Bm na forma de Am)
C*  = X 3 2 0 1 0 - (*D na forma de C)
F*  = 1 3 3 2 1 1 - (*G na forma de F)
G*  = 3 2 0 0 0 3 - (*A na forma de G)
