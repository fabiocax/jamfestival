Eduardo Costa - Presente de Deus

Intro : E A E A E

E                                                             B
Hoje quando parei pra pensar comecei a me lembrar como eu te conheci
                                               A                     E
Voce parada ai na minha frente com um jeito envolvente de joelhos eu cai
                           E7                                   A
O coração batendo disparado um olhar apaixonado foi assim que aconteceu
                             E                     B
Senti que era amor pra vida inteira a paixão mais verdadeira
              E
A mulher dos sonhos meus
E        B
Maravilhosa
        A        E
Voce é linda demais
                    B
Voce foi Feita Pra mim
                A
Nunca amei ninguém assim
         E
Voce é a minha Paz

E        B
Maravilhosa
        A        E
Voce é linda demais
                  B
É meu presente de Deus
                    A
Sem voce eu não sou eu
                   E
Sem voce não vivo mais

Repete Intro

E        B
Maravilhosa
        A        E
Voce é linda demais
                    B
Voce foi Feita Pra mim
                A
Nunca amei ninguém assim
         E
Voce é a minha Paz
E        B
Maravilhosa
        A        E
Voce é linda demais
                  B
É meu presente de Deus
                    A
Sem voce eu não sou eu
                   E
Sem voce não vivo mais

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
