Eduardo Costa - Lua e Flor

  E                   B
Eu amava como amava algum cantor
      Amaj7           D7/9
De qualquer clichê de cabaré
  E             G#7
De lua e flor

    C#m            C#m7     F#m7
Eu sonhava com a feia na vitrine
          B7        E
Como carta que se assina em vão

  E                    B
Eu amava como amava um sonhador
     Amaj7
Sem saber porque
 D7/9             E       G#7
E amava ter no coração

   C#m         C#m7         F#m7
A certeza ventilada de poesia

         B7     E          B7
De que um dia amanhece não

     E                     Bm     E7
Eu amava como amava um pescador
        Amaj7                     Am
Que se encanta mais com a rede que com o mar

     E       C#m         F#m7
Eu amava como jamais poderia,
     B7         Amaj7         D7/9
Se soubesse como te encontrar

     E                     Bm     E7
Eu amava como amava um pescador
           Amaj7                        Am
Que se encanta mais com a rede que com o mar

     E       C#m       F#m7
Eu amava como jamais poderia
     B7         Amaj7     D7/9   E
Se soubesse como te encontrar

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Amaj7 = X 0 2 1 2 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
D7/9 = X 5 4 5 5 X
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m7 = 2 X 2 2 2 X
G#7 = 4 6 4 5 4 4
