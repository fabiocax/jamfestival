Eduardo Costa - Ponto Final

C
Não pense você que eu morri só posso dizer que eu sofri mas
Dm                                G
Acabou com o tempo as coisas se acalmam e os que eram fortes
                  C  C7
Desabam e olha eu aqui

Pré-refrão:

C7                       F                          C
ainda resta um pouco de você que insiste em me incomodar
                         Dm                        G
mas eu peço à Deus que passe que esse sentimento acabe e que as
                    C  C  F  G
coisas voltem pro lugar

Refrão:

C                           G       Dm     G               C
Vai gritar meu nome vai me procurar vai sentir meu cheiro vai
     G
querer me amar

C                           G       Dm     G               C  C7
E no desespero a dor e a solidão sai a minha caça sem explicação
F  Fm                     C             Dm             G
Eu não sou mais o mesmo é ponto final amor que me faz mal é
C
ilusão

----------------- Acordes -----------------
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
