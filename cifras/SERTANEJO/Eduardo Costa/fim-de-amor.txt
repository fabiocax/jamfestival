Eduardo Costa - Fim de Amor

(intro) Am F G C C7 E

         Am
  Tá na cara
                 F
  Vê se pára de mentir assim
     G          G7
  Acordei, me cansei
       C            E
  Vou cuidar de mim
      Am
  Me usou, me enganou
       F
  Me jogou no chão
         G      G7         C       E
  Fui a presa indefesa em suas mãos
     Am
  E agora volta

  Bate em minha porta
                F
  Me tirando a paz

              G
  E mesmo te amando
             G7
  Te falo chorando
  F          C        E
  Eu não te quero mais

(refrão)
            A        E            F#m
  Melhor sozinho do que mal acompanhado
      C#m            D
  Pra mim, você é passado
    Bm                 E
  Graças a Deus que acabou
            A        E            F#m
  Melhor sozinho do que mal acompanhado
      C#m        D
  Eu respiro aliviado
      Bm            E
  Pra você a fila andou
  D      E     A
  Fim de um amor

(intro) Am F G C C7 E

    Am
  E agora volta

  Bate em minha porta
                F
  Me tirando a paz
              G
  E mesmo te amando
             G7
  Te falo chorando
  F          C        E
  Eu não te quero mais

             A        E            F#m
  Melhor sozinho do que mal acompanhado
      C#m            D
  Pra mim, você é passado
    Bm                 E
  Graças a Deus que acabou
            A        E            F#m
  Melhor sozinho do que mal acompanhado
      C#m        D
  Eu respiro aliviado
      Bm            E    F
  Pra você a fila andou

            Bb        F            Gm
  Melhor sozinho do que mal acompanhado
      Dm            D#
  Pra mim, você é passado
    Cm                 F
  Graças a Deus que acabou
            Bb        F            Gm
  Melhor sozinho do que mal acompanhado
      Dm        D#
  Eu respiro aliviado
      Cm            F
  Pra você a fila andou
  D#      F    D#  Gm
  Fim de um amor
       Cm  D#    F   Bb
  É o fim, do nosso amor

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D# = X 6 5 3 4 3
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
