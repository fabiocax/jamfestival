Eduardo Costa - Apelou Perdeu

Intro:C C7 F FM C

    C
A noite é longa
  F                                              C
E é de madrugada que eu grito choro e chamo por você
    C
A noite é fria
     F
La fora a chuva cai e a saudade
                     C    G/B
só aumenta sem eu entender

AM                  EM
Não consigo explicação  Ah
F                   C   G/B
Pra você ter ido assim
AM                   EM                  F
Foi uma discussão normal uma briga de casal
     G            C      G
que tirou você de mim



  C
Apelou perdeu
       C7
Foi momento de fraqueza eu usei minha franqueza
    G
E você não entendeu
    DM
Apelou perdeu
      G
Se errei eu fui culpado já paguei os meus
           C                 G
pecados e você não percebeu
  C
Apelou perdeu
                           C7
Não entendo ate agora o motivo de ir
             F
embora e me deixar
         FM
sozinho aqui
      C
tenha dó de mim
        G
não me deixe assim   ( Pausa )
            C     Intr.: C C7 F FM C G/B
apelou perdeu

   continua ...
 AM
Não consigo explicação....

----------------- Acordes -----------------
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
