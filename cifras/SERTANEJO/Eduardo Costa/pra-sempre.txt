Eduardo Costa - Pra Sempre

[Intro] D  Em  Bm  Em  G  A

   D                                             A                   Bm
Eu quero te entregar apenas uma flor
G                                            D         Em
Sabendo que você merece um jardim
Em                                         Bm                          A
Se juntos temos que viver um grande amor
  G                                          A           D
Por isso foi que Deus criou você pra mim
D                                          A                     G             D
Sou muito mais feliz vivendo em teu olhar
G                                            D                       Em
Sabendo que você é luz no meu caminho
Em                      G                         A
Felicidade é o nosso despertar
A                     G                   A             D
E o nosso sol não saberá brilhar sozinho
A             D                            F#m
Nossa porta é a mesma
              Em
Nossas chaves são iguais

    G                              D      A       D                            A
Na alegria, na tristeza, quero ser a sua paz
D
Mas você em minha vida
           D
É meu sonho e meu desejo
            G                          Em              A                  D
É pra sempre essa flor e esse beijo
      D                            A              Em             Bm
Eu quero te entregar de vez meu coração
   G                                   D      F#m         Em
E te dizer que Deus me fez pra ser só teu
                                                  G
Eu vou deixar o mundo inteiro descobrir
   G                       Bm       A                    D
A história que a gente juntos escreveu
A                  D                       A              Em
Eu sei que tudo pode parecer um sonho
  G                                             D                          G
Um sonho que virou bem mais do que paixão
                                           G                                       A
Seu nome em minha aliança me acompanha
    G                   D                       A                                    D
E a prova do meu grande amor está em suas mãos

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
