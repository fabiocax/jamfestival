Eduardo Costa - Cena de Um Filme


Introd: D, A, Bm, F#m, G, D, Em, A, D, G, D

D
Eu amei um alguém que me amou pra valer
                          A
Um amor diferente, que a gente não vê
                                    Em
Como em cena de um filme foi quase real
G              A                 D     G   D
Um amor desse jeito eu nunca vi igual
                                       G   D
Ela foi meu começo, meu meio e meu fim
                      A
Entregou sua vida inteira pra mim,
Em
transformou meus desejos em realidade
G              A                   D
De repente se foi me deixando a saudade

REFRÃO:


        A          G             D
Eu só amei essa mulher na minha vida
              A           G          D
e agora me encontro em um beco sem saída
             A           G                   D
Meu Deus do céu me diz agora o que é que eu faço
                  A           G             D
Sem essa mulher comigo, minha vida é um fracasso

 A     G     D    A     G    D      G
Iêee ieiee ieiee aaaa ieee ieieee

D                                        G   D
Ela foi meu começo, meu meio e meu fim
                       A
Entregou sua vida inteira pra mim,
Em
transformou meus desejos em realidade
G              A                   D
De repente se foi me deixando a saudade

       A          G             D
Eu só amei essa mulher na minha vida
              A           G          D
e agora me encontro em um beco sem saída
             A           G                   D
Meu Deus do céu me diz agora o que é que eu faço
                  A           G             D
Sem essa mulher comigo, minha vida é um fracasso
                   A          G        A      G   D      G         D
Sem essa mulher comigo, só eu sei o que é que eu passo , que eu passo ouooo .

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
