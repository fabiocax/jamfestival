Eduardo Costa - Nasci Pra Te Amar

Intro:  F  Bb  C

F                       Dm
Sem sombra de desejo e emoção,
                            Bb
Entregando pra ninguém meu coração,
                     C
Foi então que eu ouvi sua voz.
F                          Dm
Senti o doce cheiro da sua paixão,
                                  Bb
No seu olhar eu encontrei a minha paz.
                   C
Eu sei que eu nasci pra te amar.
Bb                               C
Agora que encontrei você, me amarrei,
                Dm            C
Sou louco de amor, um sonhador...
F          Bb  C         Dm
Você me achou, me libertou,
  Bb
Quando eu achava que eu não tinha jeito,

                 C
Que eu era um escravo dessa solidão,
        Dm                           C         F  Bb  C
O seu amor veio como doce chuva no meio do sertão.

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
