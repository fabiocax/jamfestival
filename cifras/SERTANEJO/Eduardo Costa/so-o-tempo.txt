Eduardo Costa - Só o Tempo

G
Só o tempo é que pode
      D/F#
Entender um grande amor
           Em
Só o tempo é que pode
           D
Acalmar a minha dor

G
Só o tempo é que pode
        D/F#
Me trazer você de volta
       Em
Sei que um dia vai voltar
      G                        D
E bater na minha porta

Ref:
          G                        D
E se voltar eu estarei aqui

                      Em
Com o peito aberto pra te receber
                  G
O meu coração não esqueceu
               D                   G     D7
Que um dia eu amei você (2X)

----------------- Acordes -----------------
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
