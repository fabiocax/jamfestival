Eduardo Costa - Nos Bares da Cidade

E|----------------13-------|
B|-------12-13-15----15-12-|
G|-11h12-------------------|
D|-------------------------|
A|-------------------------|
E|-------------------------|

C                         F          C
Garçom me traga outra garrafa de cerveja
             F           C                                     G
Vou ficar sozinho nessa mesa, eu quero beber e chorar por ela
F                                        C
Garçom a minha vida agora tá de ponta-cabeça
                                      G
Já tentei mas nada faz com que eu esqueça
                F              C
Dos olhos, dos lábios daquela mulher
C                   F            C            F          C
Garçom ela saiu de vez da minha vida e agora busco uma saída
                    C7             F
Minha história de amor acaba em solidão

                                              C
Garçom se eu ficar muito chato e der algum vexame
                               G                    F         G        C
pegue toda minha cerveja e derrame, faça o que ela fez com a minha paixão

C                                        G
Derrama cerveja, derrama, derrama a tristeza do meu coração
                                              C              G
Que essa angústia é uma bebida misturada a batida com a solidão
C                                         C7                 F
Derrama cerveja, derrama, enquanto eu derramo toda essa saudade
                       C                   G                  C
Eu sou apenas um qualquer, bebendo por mulher nos Bares da Cidade

----------------- Acordes -----------------
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
