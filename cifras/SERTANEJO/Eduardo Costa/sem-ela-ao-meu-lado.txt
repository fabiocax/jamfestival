Eduardo Costa - Sem Ela Ao Meu Lado

D
Quem vê ela assim
                     G      D
Diz que está tudo bem
                           A
Ninguém imagina que tudo acabou

Já quem me olha percebe em meu rosto
           G                        D
Que estou sofrendo e morrendo de amor

Enquanto ela tá por aí numa boa
                                A
As coisas pra mim vão de mal a pior
                G                      D
Eu sei que ela não quer saber mais de mim
                                 A
Que tudo entre nós acabou, teve fim
                                   D
Mas é que a saudade judia sem ter dó


              A
Tanto sofrimento eu nem sei se aguento
          D
É como se me retalhasse por dentro
              A
O quanto eu lutei e briguei por esse amor
   G           D
E acabei perdendo
                A
Quando tudo se acaba alguém sai machucado
              D
Perdido, sem chão, coração quebrado

  D7       G
Não foi diferente
                 A
É assim que eu me sinto
                D
Sem ela ao meu lado.
   D7       G
Não foi diferente
                 A
É assim que eu me sinto
                 D
Sem ela ao meu lado

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
