Eduardo Costa - Uma Decisão

[Intro]  C G F G  C G C

  C                        G
Hoje eu acordei com uma ligação
  G                              C
você me dizia muitas coisas sobre nos
  F                                 C
Dizendo que eu tinha que ter uma decisão
 Am       G    G7  C
Sobre nossas vidas
  F                 C
Eu sem ter o que dizer
  G                C  C7
Ela ficou em silencio
refrão:
  F                                 C
Mas de repente veio uma vontade de falar
  G                        C     C7
Que o nosso amor  não teria fim
  F                    C
Eu te quero em minha vida

  G      G7      C    G
Não ficarei sem você porque eu te amo
  C       G     G7    C
Te amo eu te amooooooo

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
