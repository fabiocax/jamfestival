Teodoro e Sampaio - Não Conte Pra Ninguém

F#
Mulher vê se toma juízo
                                                      C#7
Você sabe que eu preciso desse seu amor;

Vê se me lava a serio
                                                          F#
Deixe desse lero-lero eu peço por favor.

Você precisa entender
                                               C#7
Que eu não consigo ficar sem você
B                                 F#
Você sabe que eu te amo
                                    C#7
Vivo pelos cantos chorando

Você adora me ver sofrer.

            C#7                                F#
Ai, amor não faz isso comigo, não

                                         C#7
Não maltrate o meu coração
B                        C#7        F#
Não me mate de tanta paixão...
C#7                                                   F#
Ai, amor se você gosta de outro alguém
                                              C#7
Fique com ele e comigo também
B                                                F#
Por favor não vai embora desta casa
                             C#7                          F#
Fique com ele só não conta pra ninguém.

----------------- Acordes -----------------
B = X 2 4 4 4 2
C#7 = X 4 3 4 2 X
F# = 2 4 4 3 2 2
