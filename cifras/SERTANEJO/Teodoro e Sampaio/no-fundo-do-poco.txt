Teodoro e Sampaio - No Fundo do Poço

                        B7                     E
Sempre que um filho chora o pai corre pra ajudar
                   B7                      E
O nosso pai é o Brasil que também ouço chorar
                B7                        E
Governo manda fazer mas o congresso não faz
                    E7     A               E
É um puxando pra frente e 10 puxando pra trás

                    B7                    E
Prefeito não tem dinheiro governo também não
                B7                    E
O país está quebrado de tanta corrupção
                      B7                         E
Do jeito que a coisa vai aumentando a inadimplência
                         E7         A            E
É o mundo inteiro quebrando caminhando pra falência

                 B7                   E
Investidores da bolça vivendo em desespero
                     B7                       E
Hong Kong, Nova Iorque, São Paulo e Rio de Janeiro

                    B7                     E
Vai caindo uma por uma e o dinheiro vai sumindo
                E7         A               E
É o efeito dominó, é o mundo que está falindo

                      B7                  E
Chegou no fundo do poço escuto o povo gritar
                      B7                         E
Não tenho pra onde fugir não tem mais o que apertar
                     B7                       E
Por favor não me condene só por dizer a verdade
           E7              A                       E
Nos estamos no fundo do poço, o fundo é mais embaixo

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
