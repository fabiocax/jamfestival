Teodoro e Sampaio - Ah Moleque

Intro: E

     E         G#m     A                     B
Madrugada na cidade em busca de amor e felicidade
        E          G#m        A                     B
Parei o carro no farol do meu lado parou uma celebridade
       A                               E
Com certeza carente de amor como eu, desceu do carro e um beijo me deu
    F#                                                          B
E a minha solidão chegará ao fim quando, ela me beijou dizendo assim
     E           G#m
Ah moleque!,ah moleque!
      A                               B
Vem comigo que eu te quero lá no meu apê
     E           G#m
Ah moleque!,ah moleque!
       A                               B
Hoje a noite é todinha nossa pra gente fazer amor
 E       G#m     A      B
Amor... amor... amor


Solo: E  C#m  A  B | 2x

       E         G#m         A                        B
Fui na cola da menina que cantava aos pneus em cada esquina
         E          G#m       A                                     B
Chegando lá no seu apê, me serviu um drink e pediu pra fechar as cortinas
      A                                E
Eu na sala esperando ela se banhar não vendo a hora de tudo começar
               F# 2x       F# 2x      F# 2x   F# 2x F#                          B
Uma estrela que caiu do céu só pra mim.... a vontade era imensa de gritar assim


     E           G#m
Ah moleque!,ah moleque!
      A                               B
Vem comigo que eu te quero lá no meu apê
     E           G#m
Ah moleque!,ah moleque!
       A                               B
Hoje a noite é todinha nossa pra gente fazer amor

     E           G#m
Ah moleque!,ah moleque!
      A                               B
Vem comigo que eu te quero lá no meu apê
     E           G#m
Ah moleque!,ah moleque!
       A                               B
Hoje a noite é todinha nossa pra gente fazer amor
 E       G#m     A      B
Amor... amor... amor

             E
Ah, moleque!

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
