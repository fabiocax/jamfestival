Teodoro e Sampaio - Não Diga

         G                                          C
fique calada, não diga nada, que é melhor pra nós dois
                     D                                        G
critica tudo que eu faço e não é nem sombra da mulher que já foi
                                           C
tudo que fala e em tudo que faz só me desaponta
                  D                     C          D        G
pra você não sou nada e só estou nessa casa pra pagar suas contas

              Em                        Bm                C
simplesmente adeus,vou pegar o que é meu e você fica  aqui
           D                         C            D         G
o mundo é grande, não sei quando e nem onde, mas vou ser feliz
               Em              Bm               C
o presente nos mostra que no futuro só vamos sofrer
             D                                      G     D
estou indo embora, já chegou a hora de você  me perder

                G                    Am
fica  com suas crises e com seu mau humor
               C                   D                      G
deve haver no mundo alguém que me queira do jeito que eu sou

               G7                 C
e se arrepender sentindo falta de mim
                 D                                           G
pode chorar à vontade, morrer de saudade, hoje marcou nosso fim.

              Em
simplesmente adeus...

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
