Teodoro e Sampaio - Guarda Roupa Maldito

Intro: E B7 E E7 A E B7 E

Meu bem estou aborrecido
         B7             E
Com tudo isso que aconteceu
   A
Acabei de chegar de viagem

E vim correndo para os braços teus
        B7
Quando abri a porta do quarto
     A                    E
Encontrei você chorando assim
                          B7
Um sorriso surgiu em meus lábios
        A          B7        E
Pois pensei que choravas por mim

tabrefrão

      B7
Fui correndo pegar o lencinho

        A                      E
Pra enxugar o teu pranto que caía
                           B7
Quando abri o nosso guarda-roupa
      A      B7         E
Meu vizinho ali se escondia
     B7
Você pode sumir desta casa
     A               E
Teu amante vai com você
      E7            A
Vou fazer um fogo bonito
                      B7
Neste guarda-roupa maldito
                            E
Outro homem não vai se esconder
     B7
Você pode seguir teu caminho

Vai viver jogada na lama
                         E
Só agora que eu fui conhecer
    A           B7         E
O machão que quebrou nossa cama

refrão


Repete Intro

Repete Refrão

FINAL:  A B7 E

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
