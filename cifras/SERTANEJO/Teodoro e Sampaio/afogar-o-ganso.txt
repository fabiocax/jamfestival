Teodoro e Sampaio - Afogar o Ganso

Intro: (G# D#)

        G#                                            D#
Se encher o copo eu bebo se tocar sanfona eu danço
                                      G#

É nesta noite que eu afogo o ganso

G#                                      D#
Tô chegando no pedaço cheio de amor pra dar
                                          G#
Trago no bolso um maço de dinheiro pra gastar
                   G#7                      C#
Tô afim de tomar todas pra esquentar o meu gogó
                   D#                 G#
Põe mais uma e se for da purinha é melhor


G#                                         D#
Vou rasgando a madrugada hoje o bicho vai pegar

                                          G#
Parece que a mulherada vieram todas pra cá
                   G#7                     C#
Umas vao de caipirinha outras vao tomando rum
                   D#                 G#
As que tomam na latinha jogo meu sete um


G#                                      D#
Já são quase quatro horas hoje o tempo voou
                                       G#
Levei duas lá pra fora só uma delas topou
                   G#7                     C#
Tá no papo tá na cara que ela vai pro meu bocó
                     D#                G#
Mas ela tá querendo varar a noite no forró

----------------- Acordes -----------------
C# = X 4 6 6 6 4
D# = X 6 5 3 4 3
G# = 4 3 1 1 1 4
G#7 = 4 6 4 5 4 4
