Teodoro e Sampaio - Momentos Felizes

[Intro] G  D  G  D

G                       D                G
Por que não fazer de conta que está tudo bem
G                                      C     F  C
Nem precisa dizer que me ama e só representar
    D                          Bm               E7
Às vezes é melhor mentir, às vezes é melhor fingir
  Am                 D                   C
Tentar fazer alguma coisa pra não ver nosso sonho

Sonhado, sofrido
         D                    C           G
E quase realizado assim tão de repente desmoronar

   C
É impossível dizer que acabou
                    G     C  G
Se existe momentos felizes
    D
Não existe um casal perfeito

              C
Que bata no peito e possa julgar
           D                     G
Que possa jurar que não tiveram crises

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
