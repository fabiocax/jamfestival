Teodoro e Sampaio - Loira Não É Burra

(intro)  C#  F# B

F#                    B                  C#7     F#
Loira não é burra, loira não é burra não
                                       G#m                C#7              F#
Quem disse que loira é burra não conhece o que é bom
                    Bb                   C#7        F#    F#7
Loira não é burra, loira não é burra não
                                      B
Quem inventou este boato
                                     C#7                                F#
Não passa de um cara chato, loira não é burra, não.

F#                                       B           C#7                  F#
Quem disse que loira é burra vai ter que se ver comigo
                           C#7                                   F#
Pode levar uma surra enfrentar o meu castigo
                                 F#7                              B
Eu defendo as mulheres seja do jeito que for
                               D#m7
Isso pra mim vale a pena

                                 C#7                                F#
Pode ser loira ou morena a burrice não tem cor.

F#                                       B           C#7              F#
Quem pensa que loira é burra pode crer não é feliz
                                  C#7                                        F#
Só pode ser por despeito de quem não sabe o que diz
                                 F#7                            B
A paixão e a inveja tem matado muita gente
                               D#m7
Quem quiser pode falar
                               C#7                                      F#
Se quiser me agradar me de uma loira de presente.

----------------- Acordes -----------------
B = X 2 4 4 4 2
Bb = X 1 3 3 3 1
C# = X 4 6 6 6 4
C#7 = X 4 3 4 2 X
D#m7 = X X 1 3 2 2
F# = 2 4 4 3 2 2
F#7 = 2 4 2 3 2 2
G#m = 4 6 6 4 4 4
