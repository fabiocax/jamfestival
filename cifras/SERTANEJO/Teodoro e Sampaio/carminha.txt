Teodoro e Sampaio - Carminha

Intro: C F G F C (2x)

            C
Tarde da noite e eu já estava deitado quando ouvi na casa ao lado um
                  G
verdadeiro quebra pau.
Era o cumpadi e mulher dele brigando e eu fique só assuntando aquela
          C
briga de casal.

                                                          C7
Comadre Carmem gritava feito uma louca, era um tal de cala boca e começou
    F
cair vazia
                           C                G7                    C
caia copo, voava mesa e cadeira e naquela quebradeira começou a baixaria.

(da uma pausa)
(no refrão muda a batida)

             C                        G
Carminha meu bem, não me enforque com a rede

                                      C
Carminha meu bem, não me esfole na parede
                     C7               F                  G
mais ela esfregava a cara dele no chão e por mais de uma hora só havia
    C
pescoção
                                   G
Carminha meu bem, não me bate com a tolha
                                      C
Carminha meu bem, nunca mais vô pra gandaia
                 C7                  F                      G
mais a comadre Carmem parece que não ouvia, quanto mais pedia calma mais
            C
a Carminha batia.

(da uma pausa e muda a batida)
(C F G F C) 2x

No outro dia apareceu de olho roxo, aranhado no pescoço e com a cara
   G
amassada
eu perguntei: "Compadre o que aconteceu?". Ele então me respondeu, que foi
           C
um tombo da escada
                                                              C7
me fiz de bobo pra não perder o freguês, ele pensa que eu não sei que
                  F
ele apanha da Carminha
                                 C                   G
e toda vez que ele chega de cara cheia leva tapa na orelha e começa a
   C
baixaria...        (REFRÃO 2x)

----------------- Acordes -----------------
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
