Teodoro e Sampaio - Coração a Diesel

(intro)  C F  C F C F

 F                                                                    C7
Dezoito pneus rodando e a saudade estacionada
                                Gm                            F
O nome dela na frente com letras iluminadas
                                                                           Bb
No radio Moda de Viola vou cortado a madrugada.
                               F
Vento chora na janela
                              C7          Bb            C7         F
Cabeça pesando nela e os olhos firme na estrada.

C7                                     Gm                   F
Vai coração... bater mais forte é impossível
                                         C7                              F
Queimando igual óleo diesel na injetora da paixão
      C7                Bb         C7      F
Vai coração... batendo igual biela
F7                              Bb                     C7                 F
Teu dono é gamado nela e apaixonado por caminhão.


F                                                                    C7
O ar que corre nos freios é igual sangue nas veias
                        Gm             C7                         F
O amor que sinto por ela é igual polia e correia.
                                 F7                              Bb
Saudade passa da conta e vira saudade e meia
                                        F
Se estou voltando pra casa
                              C7                Bb            C7         F
O meu bruto cria asas e sai da frente que a coisa feia.

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
Gm = 3 5 5 3 3 3
