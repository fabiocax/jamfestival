Teodoro e Sampaio - Carisma

Intro: A  D  A  D  A  D  G  D  A

     D
Aparentemente, você é igual
                          Em
As mulheres que conheci por aí
                               A
Mas é só  na aparência e nada mais
                          D
Você é especial, eu já percebi.

Você tem o dom de amar sob medida
                              G
Você faz o que eu gosto sem pedir
                            D
Me conhece por fora e por dentro
                 Em
Você lê meus pensamentos
  A          D
Tenho que admitir.
    G                      D
O calor dos abraços me encendeia

                 A                        D
O teu beijo me tonteia , teu sorriso me fascina
       G                      D
Tuas mãos me acariciam, me acalentam
                Em            A         D
Teu amor me afugenta, o teu charme me domina.
      D
É fantástico poder amar alguém
                         Em
Que tem classe até pra vestir
 -                           A
E me envolve com carisma e magia
                             D
Fazendo a gente deixar-se seduzir.
                   A           D
Sei que você tem alguma coisa mais
         A         D           G
Mas me falta as palavras pra dizer
                                  D
Sei que você me transporta com jeitinho
                   Em
Me conduz meus carinhos
     A           D
Com amor e com prazer.

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
