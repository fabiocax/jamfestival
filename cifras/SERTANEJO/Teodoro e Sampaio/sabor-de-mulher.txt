Teodoro e Sampaio - Sabor de mulher

C
 Quase todo homem tem na sua vida
                  G
 um amor de verdade

 mas é tão difícil

 ser fiel pra sempre
                C
 uma vida inteira
 C
 o próprio instinto faz
           C7              F
 o homem ser sempre um traidor
                            C
 coração volúvel leviano até
                                 D
 que não satisfeito com uma mulher

            G                     C
 vive procurando sempre um novo amor


          G
 que doce veneno a mulher possuí
          F                   C
 parece magia que ao homem seduz
           D               G
 domina e faz dele seu escravo
          C
 se ela quiser
              G
 no jeito de olhar no modo de andar
             F                 C
 e principalmente na hora de amar
               D                  G
 nem um homem pode até hoje explicar
                               C
 o sabor que tem um corpo de mulher

----------------- Acordes -----------------
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
D = X X 0 2 3 2
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
