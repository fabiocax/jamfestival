Bruno e Marrone - Eu Vou Te Esquecer

G                                                    D/F#
Está em suas mãos uma caixa de laço vermelho com meu coração
                                   C9                                                                 G               D/F#
e aquela aliança que selou a nossa união que durou dois anos, vinte e cinco dias, cinco horas, três minutos nem um segundo a mais.

    C9    D    Em7        C9       D       G     G7 (Am G/B)
Amanhã você verá que não vai ter mais nós dois!

           C9                                                 Bm7
Eu vou te esquecer! O nosso primeiro beijo no cinema: eu vou esquecer!
        E        D/F#         E/G#         Am             D             G      G7
Nossos corpos pegando fogo na cama: eu vou esquecer! Juro que vou te esquecer!

  Am G/B    C9                                                  Bm7
Eu vou te esquecer! A nossa Lua de Mel em Nova Iorque eu juro que vou esquecer!
       E           D/F#        E/G#         Am              D             G
As nossas duzentas brigas de ciúmes: eu vou esquecer! Eu juro: vou te esquecer!

SOLO:
    C9    D    Em7        C9       D       G     G7 (Am G/B)
Amanhã você verá que não vai ter mais nós dois!


           C9                                                 Bm7
Eu vou te esquecer! O nosso primeiro beijo no cinema: eu vou esquecer!
        E        D/F#         E/G#         Am             D             G      G7
Nossos corpos pegando fogo na cama: eu vou esquecer! Juro que vou te esquecer!

  Am G/B    C9                                                  Bm7
Eu vou te esquecer! A nossa Lua de Mel em Nova Iorque eu juro que vou esquecer!
       E           D/F#        E/G#         Am                 D             G   G7
As nossas duzentas brigas de ciúmes: eu vou esquecer! Eu juro: vou te esquecer!

Am G/B    C9                                                  Bm7
Eu vou te esquecer! A nossa Lua de Mel em Nova Iorque eu juro que vou esquecer!
       E           D/F#        E/G#         Am                 D             G
As nossas duzentas brigas de ciúmes: eu vou esquecer! Eu juro: vou te esquecer

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm7 = X 2 4 2 3 2
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G7 = 3 5 3 4 3 3
