Bruno e Marrone - Andar Solidão (Tá Doendo)

[Intro]  A  E  Bm  A  D

A
Oi, não sei se me conhece

                     E
Mas eu moro aqui do lado
                             Bm
No 1.008 onde tem um homem abandonado
                              D
Eu também já passei nessa situação
                A
Ouvindo esse modão
                                     E
Que lembra desde o dia que ela foi embora
                                 Bm
E desde aquele dia o meu peito chora
                          A        D
Não vim aqui pedir pra desligar o som

Apenas pra trocar esse modão


         A
Que tá doendo
                                   E
Pra suportar essa saudade é só bebendo
                              Bm
No 1.009 também tem alguém sofrendo
                  A         D
Esse andar agora chama solidão
                                   A
E pra calmar meu coração que tá doendo
                                   E
Pra suportar essa saudade é só bebendo
                               Bm
No 1.009 também tem alguém sofrendo
                   A       D
Esse andar agora chama solidão
                                 A
Mais uma dose pra acalmar meu coração

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
