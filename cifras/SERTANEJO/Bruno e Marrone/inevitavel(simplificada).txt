Bruno e Marrone - Inevitável

G                            C
É animal é tão voraz essa paixão
      G                          C
É vendaval me tira a paz faz confusão
           D             C             G
Chega dá medo sabe o segredo do meu coração
                                  C
É sempre assim sem avisar me surpreende
            G                             C
Demais prá mim quando sei lá chega e me rende
      D            C           G
Me devora depois some a alma chora

                       D
É inevitável te amar assim
                                  C
Quantas vezes digo não mas lá no fim
  D          G
Sempre me entrego
                           D
É inevitável o poder da paixão

                                 C
Se tento esquecer lá dentro o coração
        G          D          G    D
Fica surdo, fica mudo, fica cego

    G                        C
É animal é tão voraz essa paixão
      G                          C
É vendaval me tira a paz faz confusão
           D             C             G
Chega dá medo sabe o segredo do meu coração
                                  C
É sempre assim sem avisar me surpreende
            G                             C
Demais prá mim quando sei lá chega e me rende
      D            C           G
Me devora depois some a alma chora

                       D
É inevitável te amar assim
                                  C
Quantas vezes digo não mas lá no fim
  D          G
Sempre me entrego
                           D
É inevitável o poder da paixão
                                 C
Se tento esquecer lá dentro o coração
        G          D          C  G
Fica surdo, fica mudo, fica cego

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
