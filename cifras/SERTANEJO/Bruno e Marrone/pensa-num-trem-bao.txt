Bruno e Marrone - Pensa Num Trem Bão

Intro: F

F
Fim de semana e eu já tô na balada

Tô numa boa no meio da mulherada
       Bb
Tá um calor mas a cerveja tá gelada
             F
E o som que rola é só moda apaixonada

           C                  Bb
O trem tá bão, bão, bão, bão, bão
          F
Mas pensa num trem bão

        F
Se envolver comigo só se for agora

Hoje eu me arranjo, minha mãe vai ter uma nora
         Bb
Eu vou tomar um pileque, tá na hora

          F
Vou sair dessa, mandar a tristeza embora

           C                   Bb
O trem tá bão, bão, bão, bão, bão
                   F
Mas pensa num trem bão

          Bb
O trem tá bão vem pra cá

Deixa a tristeza pra lá
     F
Vem ser feliz, se divertir
           Bb
O trem tá bão vem pra cá

O amor tá solto no ar
  F
A alegria mora aqui

           C                  Bb
O trem tá bão, bão, bão, bão, bão
          F              F7
Mas pensa num trem bão

Refrão -------------

Bb
Eu vou, eu tô
F
Vem pra cá, vem dançar
           C                  Bb
O trem tá bão, bão, bão, bão, bão
              F          F7
Faz bem pro coração
Bb
Eu vou, eu tô
F
Vem pra cá, vem cantar
          C                   Bb
O trem tá bão, bão, bão, bão, bão
          F
Mas pensa num trem bão

--------------------  


----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
