Bruno e Marrone - Garçom

Tom: A#
(intro) Cm D7 Gm D7

Gm                          Cm
Garçom, aqui, nesta mesa de bar
                       D7                         Gm  D7
Você já cansou de escutar, centenas de casos de amor
Gm                             Cm
Garçom, no bar, todo mundo é igual
                        D7                             Gm   G7
Meu caso é mais um, é banal, mas preste atenção por favor

Cm                      F                  A#
Saiba que o meu grande amor hoje vai se casar
             D#                G7
E mandou uma carta pra me avisar,
             D7              Gm   G7
Deixou em pedaços o meu coração
Cm                 F                A#
E pra matar a tristeza, só mesa de bar,
             D#                   G7
Quero tomar todas, vou me embriagar,

              D7                  Gm     D7
Se eu pegar no sono, me deite no chão
Gm                                  Cm
Garçom, eu sei, eu estou enchendo o saco
                    D7
Mas todo bebum fica chato
                       Gm   D7
Valente e tem toda a razão
Gm                          Cm
Garçom, mas eu só quero chorar
                      D7
Eu vou minha conta pagar
                       Gm      G7
Por isso eu lhe peço atenção

Cm                      F                  A#
Saiba que o meu grande amor hoje vai se casar
              D#              G7
E mandou uma carta pra me avisar,
               D7             Gm   G7
Deixou em pedaços o meu coração
Cm                F                A#
E pra matar a tristeza, só mesa de bar,
             D#                 G7
Quero tomar todas, vou me embriagar,
               D7                  Gm  G7
Se eu pegar no sono, me deite no chão
Cm                      F                  A#
Saiba que o meu grande amor hoje vai se casar
              D#              G7
E mandou uma carta pra me avisar,
               D7             Gm   G7
Deixou em pedaços o meu coração
Cm                F                A#
E pra matar a tristeza, só mesa de bar,
             D#                 G7
Quero tomar todas, vou me embriagar,
                D7                      Gm  Cm
Se eu pegar no sono, me deite.... no chão
            Gm
Me deite no chão

----------------- Acordes -----------------
A# = X 1 3 3 3 1
Cm = X 3 5 5 4 3
D# = X 6 5 3 4 3
D7 = X X 0 2 1 2
F = 1 3 3 2 1 1
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
