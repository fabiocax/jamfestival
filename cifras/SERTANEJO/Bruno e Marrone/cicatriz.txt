Bruno e Marrone - Cicatriz

Intro 2x: Dm  F  C

Dm                F                 C
Insegurança, medo dentro do meu coração
                    Bb              Dm
Feridas, cicatrizes de uma grande paixão
Dm                F               C
Nada é por acaso, tinha que acontecer
                   Bb
Foi traçado meu destino, eu perdi você
                                    F
Foi fechada a porta, melhor pra nós dois
                 C                   Bb
Não existe mais agora, o antes e o depois
                              F
Hoje não tô bem, ontem fui feliz
                    C                    Bb      A
A vida é feita de escolha, a minha eu já fiz

        Dm                    F                    C
Cicatriz,   uma marca que não sai mais do meu coração

        Dm                  F                   C
Cicatriz, mergulhei e me afoguei numa grande ilusão
        Bb                                          F
Cicatriz, cara a cara, frente a frente, olhando pra mim
                        C                  Bb     A
Joguei as cartas sobre a mesa, o jogo teve fim
         Dm
Cicatriz


----------------- Acordes -----------------
A = X 0 2 2 2 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
