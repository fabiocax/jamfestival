Bruno e Marrone - Aí Que o Amor Acontece

  G                                      Em
Todo dia em algum momento eu penso em você,
                                    C          D
Já percebi que o sentimento pode aparecer,
                  G    ( G  D/F#  Em )
Aí que o amor acontece !
                                   C
Então posso dizer que sinto a sua falta,
                                    G
E lembrar do seu beijo não me traz a calma,
   D                C    D
Que eu sinto com você.
 Em                                    C
E mesmo quando eu estou com meus amigos,
                                      G
A sensação que eu tenho é que estou sozinho,
  D                C      D
Não consigo esconder.

Refrão:
  G                  C                 D
É que a paixão aconteceu e tomou conta de mim,

                    B
Não mando mais no coração,
               C
Eu não consigo durmir,
               D                       G
Eu não vou evitar, eu não vou desistir,
              C                   D
Agora eu já não aceito outra condição,
                  B                         C
Se não for assumir de vez essa nossa paixão,
                    D                  G    D
Não da mais pra ficar vivendo a solidão.

----------------- Acordes -----------------
B = X 2 4 4 4 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
