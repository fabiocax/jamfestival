Bruno e Marrone - Viagem

D             Bm7           E
Oh tristeza, me desculpe / Estou de malas prontas
   Em   A7
Hoje a poesia veio ao meu encontro
   Em     A7                    D     D7
Já raiou o dia, . . . . . . . .vamos viajar
G Abº                    D
Vamos indo de carona / Na garupa leve
                 Bm7                          E
Do vento macio  /  Que vem caminhando
      A7  D      A7
Desde muito longe / Lá do fim do mar
D   Bm7      E
Vamos visitar a estrela / Da manhã raiada
     Em                        A7
Que pensei perdida / Pela madrugada
                    Em   A7                    D    D7
Mas vai escondida, . . . querendo brincar
G     Abº                D
Senta nessa nuvem clara / Minha poesia
     Bm7                        E
Anda se prepara / Traz uma cantiga

    A7                        D    A7
Vamos espalhando música no ar
D                Bm7     E
Olha, quantas aves brancas / Minha poesia
        Em          A7
Dançam nossa valsa / Pelo céu que um dia
   Em          A7        D     D7
Fez todo bordado de raios de sol
G                   Abº   D
Oh poesia me ajude / Vou colher avencas
        Bm7      E
Lírios, rosas, dálias / Pelos campos verdes
     A7                       D     A7
Que você batiza de jardins do céu
D       Bm7                   E
Mas, pode ficar tranquila / Minha poesia
     Em                              A7
Pois nós voltaremos /  Numa estrela guia
         Em   A7                  D       D7
Num clarão de lua . . . .quando serenar
G                       Abº                       D
Ou talvez até, quem sabe / Nós só voltaremos
   Bm7                  E
No cavalo baio, no alazão da noite
       A7                 D
Cujo nome é Raio, Raio de Luar

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Abº = 4 X 3 4 3 X
Bm7 = X 2 4 2 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
