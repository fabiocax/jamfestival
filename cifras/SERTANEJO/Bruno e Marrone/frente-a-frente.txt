Bruno e Marrone - Frente a Frente

A                             E
Nos precisamos conversar, decidir o que vai ser
           F#m                  C#m
Se é uma briga de momento ou separação
        Bm                  F#m
Você tem que me dizer, quero ouvir a sua voz
      B7                              E       E5X
Me falando frente a frente o que vai ser de nos

                A                 E
O que eu não posso é ficar, esperando por você
      F#m                            C#m
Sem saber se vai voltar, ou vai me esquecer
     Bm                                   F#m
Você tem que resolver, se me quer ou vai abrir
     B7                   E
A indecisão me mata, você tem que decidir

    A          C#m             F#m
Vai, se o seu amor acabou diz pra mim
             C#m
Mas não me deixe assim,

D              E                       A
sei que vou passar um mal pedaço vou sofrer
        F#m                        Bm
Mas com o tempo vou parar de enlouquecer,
   C#       F#m    F#
é pior duvidar e perder
 Bm              E7                  A
Diz que esta na hora desse jogo se acabar,
         F#m                    Bm
se é pra ficar assim prefiro terminar
           C#                   F#  E
Mesmo te amando posso te dizer adeus

Solo: A  E  F#m  C#m  Bm  F#m  B7  E  E7

                A                 E
O que eu não posso é ficar, esperando por você...

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C# = X 4 6 6 6 4
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
