Bruno e Marrone - Detalhes / Cavalgada / Café da Manhã (Pot Pourri)

[Enquanto o Bruno fala se repete a sequência: Bm7  E7  A  A7+]

Bm7                     E7    A
    Não adianta nem tentar
    A7+    A  A7+  A#º
me esquecer
Bm7                                E7        A
    Durante muito, muito tempo em sua vida,
      A7+     A  A7+   A#º
eu vou viver.
Bm7                       E7
    Detalhes tão pequenos de nós dois
A               A7+                A     A#º
    são coisas muito grandes prá esquecer
Bm7                       G
    e a toda hora vão estar presentes,
   E7
você vai ver.

[Enquanto o Bruno fala se faz a sequência: Bm Bm/A G Em A7 D F#]


                                  Bm
             Estrelas mudam de lugar
                               Em
             chegam mais perto só pra ver
                              A7
             e ainda brilham de manhã
                                  D     F#
             depois do nosso adormecer
                                     Bm
             e na grandeza desse instante
                             Em
             o amor cavalga sem saber
                                 A7
             que na beleza dessa hora
                                  Bm
             o Sol espera pra nascer.

[Enquanto o Bruno fala se faz a sequência dedilhando: A7+ Bm7 A7+]

A7+
    Amanhã de manhã
               C#7
Vou pedir um café pra nós dois
               F#m
Te fazer um carinho e depois
                     A7
Te envolver em meus braços

A   A  G   A / D7+

                    E7
 E em seus abraços
                A7+
Na desordem do quarto esperar
F#m               Bm7
    Lentamente você se deitar
                  E7
Vou te amar outra vez

    A7+
Amanhã de manhã
                   C#7
Nossa chama outra vez tão acesa
               F#m
E o café esfriando na mesa
              A7
Esquecemos de tudo

A   A  G   A / D7+

                E7
Sem me importar
               A7+
Com o tempo correndo lá fora
F#m               Bm7
    Amanhã nosso amor não tem hora
E7                A7+
   Vou ficar por aqui

----------------- Acordes -----------------
A = X 0 2 2 2 0
A#º = X 1 2 0 2 0
A7 = X 0 2 0 2 0
A7+ = X 0 2 1 2 0
Bm = X 2 4 4 3 2
Bm/A = X 0 4 4 3 X
Bm7 = X 2 4 2 3 2
C#7 = X 4 3 4 2 X
D = X X 0 2 3 2
D7+ = X X 0 2 2 2
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
