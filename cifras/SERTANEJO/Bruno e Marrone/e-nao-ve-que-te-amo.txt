Bruno e Marrone - E Não Vê Que Te Amo

 C
Meu,
     F
Coração,
     G          C     G
Não vive sem você
C
E ,
       F
A solidão
     G        C                 C7
Não pára de doer em mim .... (em mim ...)2x  coral
         F
O tempo passa,
         G
Você não volta...

F     G         C
E não vê que te amo,
F      G          C
E não vê que te chamo ,

F     G            C
E não vê que sou frágil, ferido,
 Am                F     G     C
Menino perdido no mundo sem você.....

(solo)  G C F G C G

 C
Eu,
       F
Já tentei,
   G            C    G
Viver sem ter você
 C
Mas,
           F
Não consegui,
     G            C          C7
Meu mundo é todo seu...( enfim... ) (2x coral)
         F
O tempo passa
          G
Você não volta......

(refrão)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
