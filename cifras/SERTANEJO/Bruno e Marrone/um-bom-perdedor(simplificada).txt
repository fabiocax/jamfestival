Bruno e Marrone - Um Bom Perdedor

[Intro]  C  G  C  G

Am            Dm            G
Sei que você pensa em me deixar
C                 F   Dm              G
E eu não vou impedir ,siga a sua estrela
         Am                   Dm  G                C
Em todo caso eu digo que ficarei aqui neste mesmo lugar
      F                 Dm             E
Se que vai pode um dia voltar ,então esperarei
         Am                   Dm
E quando alguém conquistar o seu amor
G                    E
Não serei mais quem hoje eu sou
Am              Dm    G                C  G C G
Acho que sei perder ,acho que sei perder

Am           Dm         G           C                 F
Já não é preciso disfarçar essas lágrimas estão de mais
    Dm              G
Se é hora de ir então vai

       Am                  Dm             G                    C
Sim é claro eu esperava te convencer mas é bom deixar a água correr
        F                  Dm             E
O que importa agora ,as palavras que eu não pude dizer
        Am                 Dm
E se o vento hoje sopra a seu favor
G             E          ( F )
Eu não guardarei rancor
G                 C        Am        Dm
Acho que sei perder ,e não será a primeira vez
Bb       Am    Gm      C      F
Hoje é você amanhã será quem for, for
G                C    G
Serei um bom perdedor
Am      Dm
E meu mundo não vai mudar
Bb       Am    Gm     C      F   G  C  G  Am  Am  Dm  Dm  Bb  Am  Gm  C F C
Até que alguém oculpe seu lugar
       Am                   Dm              G                    C
Sim é claro eu esperava te convencer mas é bom deixar a água correr
        F                  Dm                E E E E
O que importa agora ,as palavras que eu não pude dizer
        Am                 Dm
E se o vento hoje sopra a seu favor
G             E          ( F )
Eu não guardarei rancor
G                 C    G   Am       Dm
Acho que sei perder ,e não será a primeira vez
Bb       Am    Gm      C      F
Hoje é você amanhã será quem for, for
G                C      G
Serei um bom perdedor
      Am    Am   Dm   Dm
E meu mundo não vai mudar
Bb       Am    Gm      C      F  G  C  G  Am  F
Até que alguém oculpe seu lugar

G                C      G
Serei um bom perdedor
     Am    Am   Dm   Dm
E meu mundo não vai mudar
Bb       Am   Gm    C      F     G    C  B Am
Até que alguém oculpe seu lugar

Am            Dm             G
Sei que você pensa em me deixar
C                 F    D             G
E eu não vou impedir, siga a sua estrela

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
