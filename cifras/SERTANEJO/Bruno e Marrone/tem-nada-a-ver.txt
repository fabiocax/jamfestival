Bruno e Marrone - Tem Nada a Ver

Intro: G B7 C G A7 A7 D7 D7

e|-------------------|---------------------|-------------------15-15-15~~-|
B|----------------12-|-15~~-12~~-16~~-12~~-|-17--15--13-------------------|
G|-------11-12-14----|---------------------|-17--16--14--14/16~~----------|
D|-12-14-------------|---------------------|------------------------------|
A|-------------------|---------------------|------------------------------|
E|-------------------|---------------------|------------------------------|

e|------------------------------------------------------------15---14---12---10~~-|
B|----------------------------------------------------15~~------------------------|
G|-------12-14-14/16-14~~--------12-14-14/16-14-12----14~~--15---14---12---11-----|
D|-12p14-------------------12p14-------------------14-----------------------------|
A|--------------------------------------------------------------------------------|
E|--------------------------------------------------------------------------------|


                 G
Alguém me disse que você falou
               D
Que acha graça e tem pena de mim

            C                             D
Que não me ama que o nosso caso já chegou ao fim
          G
Que eu telefono e não me atende mais
       D
Que pra você agora tanto faz
           C
Mas se é verdade que você não quer
         D
Responda se puder
      G                      B7
Quem é que liga no meio da noite
                Em
Diz que está sozinha
               G7                        C
Quem é que nos meus braços fala que é só minha
          D7                     G  D7
E chora de emoção na hora do prazer
     G                    B7
Porque será que você não assume
                 Em
Que eu sou seu homem
        G7                    C
Porque o tempo todo fala no meu nome
         D7                        G  G7
Confessa que você não sabe me esquecer
        C
Tem nada a ver
           D7                   G
Você é quem está fugindo da verdade
          G7               C
Parece que tem medo da felicidade
             D7                          G  G7
Você diz que não,      teu corpo diz que sim
          C                    D7
Tem nada a ver, a boca que me beija
   B7            Em
A mão que me apedreja
             G7                    C
Se deita em minha cama depois me difama
                  D7                    G
Diz que não me quer     mas vive atrás de mim

..::: D7 G B7 C G A7 A7 D7 D7 :::...

          G                 B7
Quem é que liga no meio da noite
              Em
Diz que está sozinha
                     G7                    C
Quem é que nos meus braços fala que é só minha
           D7                   G  D7
E chora de emoção na hora do prazer
        G                  B7
Porque será que você não assume
               Em
Que eu sou seu homem
          G7                    C
Porque o tempo todo fala no meu nome
           D7                     G  G7
Confessa que você não sabe me esquecer
           C
Tem nada a ver
                  D7          G
Você é quem está fugindo da verdade
              G7            C
Parece que tem medo da felicidade
           D7                           G  G7
Você diz que não,      teu corpo diz que sim
           C                         D7
Tem nada a ver, a boca que me beija
   B7            Em
A mão que me apedreja
             G7                     C
Se deita em minha cama depois me difama
                   D7                    G
Diz que não me quer     mas vive atrás de mim

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
