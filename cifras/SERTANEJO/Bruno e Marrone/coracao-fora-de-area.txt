Bruno e Marrone - Coração Fora de Área

Intro: A  Ab  F#m  E

Verso 1:
  A
 Você vai ligar

 Só não sei se é
                     F#m
 Pra xingar ou pra pedir perdão
                                Bm
 É de lua, é de veneta esse seu coração
         B                            E
 Ele as vezes me odeia e as vezes me quer

Verso 2:
   A
 Você vai ligar

 A saudade vai bater
          F#m
 E a madrugada vem


 E o frio da solidão
         Bm
 Não perdoa ninguém
       B
 Mesmo sem hora marcada
       E
 Você liga

Verso 3:
            D
 Mas dessa vez,
                               A
 Pode estar fora de área o meu coração
        F#m                          Bm
 Nessa distância cobertura é pura ilusão
       B                       E
 Pode deixar o seu recado na caixa postal

Verso 4:
           D
 Você vai ver
                                   A
 Que a consciência pesa e o quanto é ruim
            F#m                      B
 E que seus créditos comigo já estão no fim
             E                   A
 E o nosso amor está perdendo o sinal
  A Ab F#m E
Refrão:
           E
 Você vai ligar,
    D                        A
 Conheço suas manhas, suas crises
  F#m                      Bm
 Somando nossos momentos felizes
    E                        A
 O resultado não pode ser o final

           E
 Você vai ligar
     D
 Se não ligar
                 A
 Aí sou eu quem liga
    F#m                          Bm
 Brigar qualquer casal que ama briga
      E
 Um grande amor
              D
 Não pode nos fazer tão mal

( Dm  A )

----------------- Acordes -----------------
A = X 0 2 2 2 0
Ab = 4 3 1 1 1 4
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
