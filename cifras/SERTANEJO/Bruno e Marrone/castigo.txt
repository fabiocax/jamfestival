Bruno e Marrone - Castigo


D         Em              A7                 F#m
A gente briga, diz tanta coisa que não quer dizer
         Bm                 Em
Briga pensando que não vai sofrer
             A7            D
Que não faz mal se tudo terminar
         Em           A7                F#m
Um belo dia a gente entende que ficou sozinho
        Bm                  Em
Vem a vontade de chorar baixinho
        A7               D
Vem o desejo triste de voltar
         Em               A7               F#m
Você se lembra, foi isso mesmo que se deu comigo
          Bm                  Em
Eu tive orgulho e tenho por castigo
          A7             D
A vida inteira pra me arrepender
         Em
Se eu soubesse

         A7            F#-
Naquele dia o que sei agora
        Bm                 Em
Eu não seria esse ser que chora
         A7          D7
Eu não teria perdido você
         G
Se eu soubesse
         A7            F#-
Naquele dia o que sei agora
        Bm                   Em
Eu não seria esse ser que chora
         A7           D
Eu não teria perdido você.

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
F#- = 2 4 4 2 2 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
