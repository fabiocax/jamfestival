Bruno e Marrone - Eu e a Madrugada

[Intro]  E  B  G#m  C#m  F#  B  F#

     B        B9             D#m
Da janela eu olho a rua tão vazia
                  G#m                 C#m   G#
Madrugada, noite fria e eu aqui na solidão
     C#m                       F#
Como eu a noite se faz muito triste
    C#m       F#                          B  F#
Coração ainda insiste procurando esta paixão

    B             B9            B
Sabe Deus onde ela anda nessa hora
                      B7
Sou mais um que também chora
                   E
Desse mal que é tão comum
                   B             G#m
Uma mulher bonita, um coração sozinho
  C#m          F#        B   B7
E um desejo louco de carinho

   E                    B         G#m
Um grito de socorro nos muros da cidade
   C#m          F#            B
Sou eu aqui morrendo de saudade

 F#                      E
Chora a madrugada fria, traz a luz de um novo dia
B                     F#                  B
Só não traz a luz dos olhos da mulher que amo
F#                      E
Da janela vejo tudo e tudo sempre acaba em nada
B              F#         B
Toda noite é assim, eu a madrugada

( F#  E  B  F#  B  G )

     C             C9            C
Sabe Deus onde ela anda nessa hora
                       C7
Sou mais um que também chora
                     F
Desse mal que é tão comum
                       C        Am
Uma mulher bonita, um coração sozinho
  Dm          G        C   C7
E um desejo louco de carinho
   F                    C          Am
Um grito de socorro nos muros da cidade
   Dm          G            C
Sou eu aqui morrendo de saudade

 G                      F
Chora a madrugada fria, traz a luz de um novo dia
C                     G                    C
Só não traz a luz dos olhos da mulher que amo
G                      F
Da janela vejo tudo e tudo sempre acaba em nada
C               G           C
Toda noite é assim, eu a madrugada

G                      F
Chora a madrugada fria, traz a luz de um novo dia
C                     G                    C
Só não traz a luz dos olhos da mulher que amo
G                      F
Da janela vejo tudo e tudo sempre acaba em nada
C               G           C
Toda noite é assim, eu a madrugada

C               G          F    C  F C
Toda noite é assim, eu a madrugada

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
B9 = X 2 4 4 2 2
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
C7 = X 3 2 3 1 X
C9 = X 3 5 5 3 3
D#m = X X 1 3 4 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
G#m = 4 6 6 4 4 4
