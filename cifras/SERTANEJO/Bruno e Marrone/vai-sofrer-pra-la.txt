Bruno e Marrone - Vai Sofrer Pra Lá

Intr.:  A   D   A    G A D  A

        D
Ai meu Deus já não sei mais viver tão sozinho
    A
Chorando assim

Já faz mais de um mês que acabou e ela nem quer
 D          A
Saber de mim
        D                                      A
Já cheguei a zombar de um amigo que um dia também viveu
        G                 A
Uma história de amor infeliz e hoje quem sofre
   D
Assim sou eu
                A
Vai sofrer pra lá, vai
               D
Feito cão sem dono
           A              G              D
Vivo abandonado, pensando nela, perdendo o sono

               A
Vai beber pra lá, vai
                D
E eu que nem bebia
             A                 G  A         D
Tô ficando a toa, vê se me perdoa, mata essa agonia

Intr.: A   D   A    G A D  A

(Repete Tudo)

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
