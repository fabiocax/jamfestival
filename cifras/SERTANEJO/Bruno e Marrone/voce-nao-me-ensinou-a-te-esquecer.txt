Bruno e Marrone - Você Não Me Ensinou a Te Esquecer

Intro: F7  C#/F G  C7  F7

    Fm
Não vejo mais você faz tanto tempo
                    G7
Que vontade que eu sinto
    Bbm7                 C7
De olhar em seus olhos ganhar seus abraços
                     Fm7    C7
E verdade eu não minto

  Fm7
E nesse desespero em que me vejo
                     G/F
Já cheguei a tal ponto
Bbm7                         C7
De me trocar diversas vezes por você
                 Fm7   F7
Só pra vê se te encontro

Bbm7                    Eb7
Você bem que podia perdoar

G#                           Fm7
E só mais uma vez me aceitar
Gm                     C7
Prometo agora vou fazer por onde
              Fm7  F7 (A|--Bb-C-Db-Eb-F-G-G#) -> Notas Simples
Nunca mais perde-la

Refrão

Bbm7     Eb                     G#
Agora que faço eu na vida sem você
Fm                   Bbm7
Você não me ensinou a te esquecer
 C7                  Fm
Você só me ensinou a te querer
               F7
E te querendo eu vou tentando te encontrar

          Bbm7
Vou me perdendo
         Eb                         G#
Buscando em outros braços seus abraços
Fm                        Bbm7
Perdido no vazio de outros passos
 C7                    Fm
Do abismo em que  você se retirou
                        F7
E me atirou e me deixou aqui sozinho

Bbm7     Eb                  G#
Agora que faço eu na vida sem você
Fm                   Bbm7
Você não me ensinou a te esquecer
C7                   Fm
Você só me ensinou a te querer
                     C7                      Fm7
E te querendo eu vou tentando me encontrar

2ª parte

 Fm7
E nesse desespero em que me vejo
                     G7
Já cheguei a tal ponto
Bbm7                         C7
De me trocar diversas vezes por você
                 Fm7   F7
Só pra vê se te encontro

Solo : Bbm7 Eb G# Fm Bm7 C7 Fm F7

Bbm7     Eb                     G#
Agora que faço eu na vida sem você
Fm                   Bbm7
Você não me ensinou a te esquecer
 C7                  Fm
Você só me ensinou a te querer
               F7
E te querendo eu vou tentando te encontrar

          Bbm7
Vou me perdendo
         Eb                         G#
Buscando em outros braços seus abraços
Fm                        Bbm7
Perdido no vazio de outros passos
 C7                    Fm
Do abismo em que  você se retirou
                        F7
E me atirou e me deixou aqui sozinho

Bbm7     Eb                  G#
Agora que faço eu na vida sem você
Fm                   Bbm7
Você não me ensinou a te esquecer
C7                   Fm
Você só me ensinou a te querer
                     C7                      Fm7
E te querendo eu vou tentando me encontrar

----------------- Acordes -----------------
Bbm7 = X 1 3 1 2 1
Bm7 = X 2 4 2 3 2
C#/F = X 8 X 6 9 9
C7 = X 3 2 3 1 X
Eb = X 6 5 3 4 3
Eb7 = X 6 5 6 4 X
F7 = 1 3 1 2 1 1
Fm = 1 3 3 1 1 1
Fm7 = 1 X 1 1 1 X
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
G/F = 1 X X 0 0 3
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
