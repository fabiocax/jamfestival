Bruno e Marrone - Vida Vazia

[Intro]

G  G7M  G6  G7M
G  G7M  G6  G7M
Am7  D  G  G7M  G6  G7M

[TAB SOLO Intro]

Parte 1 de 4
E|-----x-x-------------x-x-----x-x-------------x-x-----|
B|-8-8-x-x-7--7-7-7-7--x-x-5-5-x-x-7--7-7-7-7--x-x-----|
G|-----x-x-------------x-x-----x-x-------------x-x-----|
D|-9-9-x-x-7--7-7-7-7--x-x-5-5-x-x-7--7-7-7-7--x-x-----|
A|-----------------------------------------------------|
E|-----------------------------------------------------|
   ↓ ↑ ↓ ↑ ↓  ↓ ↑ ↓ ↓  ↓ ↑ ↓ ↑ ↓ ↑ ↓  ↓ ↑ ↓ ↓  ↓ ↑


Parte 2 de 4
E|-----x-x-------------x-x------x-x--------------------|
B|-8-8-x-x-7--7-7-7-7--x-x--5-5-x-x--7-----------------|
G|-----x-x-------------x-x------x-x--------------------|
D|-9-9-x-x-7--7-7-7-7--x-x--5-5-x-x--7-----------------|
A|-----------------------------------------------------|
E|-----------------------------------------------------|
   ↓ ↑ ↓ ↑ ↓  ↓ ↑ ↓ ↓  ↓ ↑  ↓ ↑ ↓ ↑  ↓



Parte 3 de 4
E|-3-5/7--5--5-x-x-5--5-x-x-5-5-x-x-5--5/7-3-----------|
B|-------------------------------------------5---------|
G|-4-5/7--5--5-x-x-5--5-x-x-5-5-x-x-5--5/7-4-----------|
D|-------------------------------------------5---------|
A|-----------------------------------------------------|
E|-----------------------------------------------------|
   ↓ ↓    ↓  ↑ ↓ ↑ ↓  ↑ ↓ ↑ ↓ ↑ ↓ ↑ ↓  ↓   ↓ ↓


Parte 4 de 4
E|-----x-x-------------x-x-----x-x-------------x-x-----|
B|-8-8-x-x-7--7-7-7-7--x-x-5-5-x-x-7--7-7-7-7--x-x-----|
G|-----x-x-------------x-x-----x-x-------------x-x-----|
D|-9-9-x-x-7--7-7-7-7--x-x-5-5-x-x-7--7-7-7-7--x-x-----|
A|-----------------------------------------------------|
E|-----------------------------------------------------|
   ↓ ↑ ↓ ↑ ↓  ↓ ↑ ↓ ↓  ↓ ↑ ↓ ↑ ↓ ↑ ↓  ↓ ↑ ↓ ↓  ↓ ↑        


[Primeira parte]

   G             G7M
Depois que você foi embora
              G6             G7M
A solidão entrou, trancou a porta
          Am7
E não me deixa mais
D
  Eu já tentei sair
          Am7            D
Tentei fugir, não consegui
                 G  G7M  G6  G7M
Eu já não tenho paz

[Pré-refrão]

  Em
Até o meu sorriso é tão sem graça

Não há nada que desfaça
                        Bm
Essa tristeza em meu olhar

C
  O que é que eu vou fazer
             Am7
Pra te esquecer?
D
  O que é que eu vou fazer

Pra não sofrer?
Am7                               D/F#
    O que é que eu faço pra você voltar
G            G7M  G
  Pra minha vida?

[Refrão]

        Am7  D/F#       G  Em
Vida vazia,    saudade sua
        D             C
Dia nublado, vento gelado
           G  G7M  G6  G7M
Noite sem lua

        Am7  D/F#       G    Em
Vida vazia     de sentimento
           D            C
Noite sem sono, no abandono
          G  G7M  G6  G7M
Eu não aguento

[Solo]

G  G7M  G6  G7M
G  G7M  G6  G7M
Am7  D  G  G7M  G6  G7M

[TAB Solo]

Parte 1 de 4
E|-----x-x-------------x-x-----x-x-------------x-x-----|
B|-8-8-x-x-7--7-7-7-7--x-x-5-5-x-x-7--7-7-7-7--x-x-----|
G|-----x-x-------------x-x-----x-x-------------x-x-----|
D|-9-9-x-x-7--7-7-7-7--x-x-5-5-x-x-7--7-7-7-7--x-x-----|
A|-----------------------------------------------------|
E|-----------------------------------------------------|
   ↓ ↑ ↓ ↑ ↓  ↓ ↑ ↓ ↓  ↓ ↑ ↓ ↑ ↓ ↑ ↓  ↓ ↑ ↓ ↓  ↓ ↑


Parte 2 de 4
E|-----x-x-------------x-x------x-x--------------------|
B|-8-8-x-x-7--7-7-7-7--x-x--5-5-x-x--7-----------------|
G|-----x-x-------------x-x------x-x--------------------|
D|-9-9-x-x-7--7-7-7-7--x-x--5-5-x-x--7-----------------|
A|-----------------------------------------------------|
E|-----------------------------------------------------|
   ↓ ↑ ↓ ↑ ↓  ↓ ↑ ↓ ↓  ↓ ↑  ↓ ↑ ↓ ↑  ↓


Parte 3 de 4
E|-3-5/7--5--5-x-x-5--5-x-x-5-5-x-x-5--5/7-3-----------|
B|-------------------------------------------5---------|
G|-4-5/7--5--5-x-x-5--5-x-x-5-5-x-x-5--5/7-4-----------|
D|-------------------------------------------5---------|
A|-----------------------------------------------------|
E|-----------------------------------------------------|
   ↓ ↓    ↓  ↑ ↓ ↑ ↓  ↑ ↓ ↑ ↓ ↑ ↓ ↑ ↓  ↓   ↓ ↓


Parte 4 de 4
E|-----x-x-------------x-x-3---3-----------------------|
B|-8-8-x-x-7--7-7-7-7--x-x-3h5-3-----------------------|
G|-----x-x-------------x-x-------4-2-------------------|
D|-9-9-x-x-7--7-7-7-7--x-x-----------5-4-2---2---------|
A|-----------------------------------------5-----------|
E|-----------------------------------------------------|
   ↓ ↑ ↓ ↑ ↓  ↓ ↑ ↓ ↓  ↓ ↑


[Pré-refrão]

  Em
Até o meu sorriso é tão sem graça

Não há nada que desfaça
                        Bm
Essa tristeza em meu olhar

C
  O que é que eu vou fazer
             Am7
Pra te esquecer?
D
  O que é que eu vou fazer

Pra não sofrer?
Am7                               D/F#
    O que é que eu faço pra você voltar
G            G7M  G
  Pra minha vida?

[Refrão final]

        Am7  D/F#       G  Em
Vida vazia,    saudade sua
        D             C
Dia nublado, vento gelado
           G  G7M  G6  G7M
Noite sem lua

        Am7  D/F#       G    Em
Vida vazia     de sentimento
           D            C
Noite sem sono, no abandono
          G  G7M  G6  G7M
Eu não aguento

        Am7  D/F#     G  Em
Vida vazi____a, oh oh oh
        D             C
Dia nublado, vento gelado
           G  G7M  G6  G7M
Noite sem lua

        Am7  D/F#       G    Em
Vida vazia     de sentimento
           D            C
Noite sem sono, no abandono
          G  G7M  G6  G7M  G
Eu não aguento

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F# = 2 0 0 2 3 X
Em = 0 2 2 0 0 0
G = 3 2 0 0 3 3
G6 = 3 X 0 0 3 0
G7M = 3 X 0 0 3 2
