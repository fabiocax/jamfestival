Bruno e Marrone - Por Esse Amor


        F
sem direção,meu coração

         Bb  Bb4 Bb
anda perdido

           F
seja onde for,por esse amor
          Bb Bb4 Bb
vive esquecido

          C
dias sem sol
            Bb
noite sem lua
F
ferido


F
não posso mais, machuca demais

          Bb Bb4 Bb
esse desacerto

F
pra solidão, já disse não
                Bb Bb4 Bb
mas não tem jeito
          C
vivendo assim
            Bb
sou alvo do fim
            F     C
é tiro no peito

2 x refrão

F         C/E         Dm
por esse amor eu me engano
  Dm           Bb
o que fazer se eu amo
       Gm           C
quem sabe amanha vai voltar

F         C/E          Dm
por esse amor eu me entrego
Dm       Bb
juro não nego
Gm               C        F
preciso desse amor pra caminhar

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bb4 = X 1 3 3 4 1
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
