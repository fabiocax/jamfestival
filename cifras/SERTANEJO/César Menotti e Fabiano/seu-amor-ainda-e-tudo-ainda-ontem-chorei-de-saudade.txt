César Menotti e Fabiano - Seu Amor Ainda É tudo-Ainda Ontem Chorei De Saudade

Intro: Dm Gm C F A7

  Dm                                 Gm
Muito prazer em revê-la você está bonita
A7                                    Dm
Muito elegante mais jovem tão cheia de vida
Dm                                 Gm
Eu ainda falo de flores e declamo seu nome
A7                                    D     A7
Mesmo meus dedos me traem e disco seu telefone

Refrão:
 D     D7M       D6         D7M D
É minha cara, mudei, minha cara
                      G
Mas por dentro eu não mudo
 Em   Em7M   Em7     Em6         A7
O sentimento não pára a doença não sara
                 D        A7
Seu amor ainda é tudo     tudo


D                  Bm             Em
Daquele momento até hoje esperei você
   A7                                D  G  A7
Daquele maldito momento até hoje só você
   D                  Bm             Em
Eu sei que o culpado de não ter você, sou eu
 A7                                D      A7   D
E esse medo terrível de amar outra vez . . .é meu

( E  C#m F#m  B  E  B )
( E  E7  A  E  B  E )

Primeira Parte:
 E               C#m
Você me pede na carta
              F#m
Que eu desapareça
    B
Que eu nunca mais te procure
                  E   B
Pra sempre te esqueça
 E                  C#m
Posso fazer sua vontade
               F#m
Atender seu pedido
 B
Mas esquecer é bobagem
            E   B
É tempo perdido

Refrão:
E                         B
Ainda ontem chorei de saudade
F#m        B                     E   B
Relendo a carta, sentindo o perfume
 E
Mas que fazer com essa dor
          B
Que me invade
                              E   B     E
Mato esse amor ou me mata o ciúme

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
D6 = X 5 4 2 0 X
D7M = X X 0 2 2 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
Em6 = X 7 X 6 8 7
Em7 = 0 2 2 0 3 0
Em7M = X X 2 4 4 3
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
