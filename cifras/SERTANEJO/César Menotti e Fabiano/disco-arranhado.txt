César Menotti e Fabiano - Disco Arranhado

[Intro]  C  Am  F  G

C                  Am
Se ficar dançando assim desse jeito

     Dm
Vão achar que a música é boa

     F
Com você tudo fica perfeito

C                   Am
Esse sorrisão mais lindo do mundo

           Dm
De quem a vida é tão maravilhosa

       F
Mesmo ela não sendo isso tudo

Dm                  F
E que tal outro lugar


                          C
Pra sua casa ou pra minha vida

   G
Você escolhe, você escolhe

Dm               F                         C
Tristeza vai se matar com uma dose de alegria

       G
Com você completando o meu dia

Dm                  Dm
Vem ser a minha semana

                              F
Meu fim de semana e o meu feriado

                   C
Meu remédio controlado
               G
O meu disco arranhado

                           Dm
Naquela parte que diz eu te amo, te amo

    F                     C               G
Te amo, te amo, te amo, te amo, te amo, te amo



----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
