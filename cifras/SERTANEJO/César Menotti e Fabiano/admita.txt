César Menotti e Fabiano - Admita

G    D/F#            Em
Um dia você vai acordar
                         C
E quando abrir os olhos vou estar ao lado seu
G     D/F#           Em
Um dia você vai acordar
                       C
E perceber que o seu grande amor sempre fui eu

(intro 2x) G  D  Em  C

G                        D
Eu sei que é muito chato te ligar agora
        C                      Cm
Já são quase quatro horas da manhã
Não consigo dormir

G                    D
Eu sei que você tá numa deprê danada
         C
E que também perdeu o sono

        Cm
E com certeza tá pensando em mim

G        D                     Em
Sua amiga me disse tudo não adianta esconder
           C
Eu tô por dentro da situação
G     D                     C
Admita que esse cara só te faz sofrer
                Cm
Enquanto eu te quero você quer solidão

(refrão 2x)

G    D/F#            Em
Um dia você vai acordar
                         C
E quando abrir os olhos vou estar ao lado seu
G     D/F#           Em
Um dia você vai acordar
                       C
E perceber que o seu grande amor sempre fui eu

(solo 2x) G  D  Em  C

G                        D
Eu sei que é muito chato te ligar agora
        C                     Cm
Já são quase quatro horas da manhã
Não consigo dormir

G                    D
Eu sei que você tá numa deprê danada
         C
E que também perdeu o sono
        Cm
E com certeza tá pensando em mim

G        D                     Em
Sua amiga me disse tudo não adianta esconder
           C
Eu tô por dentro da situação
G     D                     C
Admita que esse cara só te faz sofrer
                Cm
Enquanto eu te quero você quer solidão

(refrão 2x)

G    D/F#            Em
Um dia você vai acordar
                         C
E quando abrir os olhos vou estar ao lado seu
G     D/F#           Em
Um dia você vai acordar
                       C                        G  D/F#
E perceber que o seu grande amor sempre fui eu
Em                    C                         G  D/F#  Em  C
E perceber que o seu grande amor sempre fui eu

----------------- Acordes -----------------
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
