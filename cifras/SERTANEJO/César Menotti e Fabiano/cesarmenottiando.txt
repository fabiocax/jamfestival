César Menotti e Fabiano - Cesarmenottiando

[Intro] G  D  C

G
Me largou do nada
                             D
Por um ciúme besta acabou comigo jogou na minha cara
                                Am        C
Que eu tava na farra e alguém me conheceu
                        G
Só pra constar não era eu
                        D
Sempre tem uma briga na manga
                        Am
Quando não tem motivo inventa
C                 G
Para de ser ciumenta
                           D
Eu tentei te esquecer na marra
                         Am
Mas não tô preparado pra isso
C                 D
Só fiquei no prejuízo

G
Meu coração, eu tô leiloando
D
Não era eu, mas a conta eu vou pagando
Am        C            G   D
Hoje eu tô Cesarmenottiando

( G  D  C )

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
