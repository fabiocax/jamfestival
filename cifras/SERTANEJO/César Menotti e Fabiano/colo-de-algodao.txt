﻿César Menotti e Fabiano - Colo de Algodão

Capo Casa 1

D9
  É que sem você eu sou
                      A9
  Só metade ou quase nada
D9
  E contigo a felicidade
                    A9
  Nunca me cobrou entrada
F#m                    C#/F
  Só quero colo de algodão
            B7
  E um lugarzinho pra dormir
           D9          E
  Me dê motivos pra nunca fugir

  A                        C#7
  Se eu tentar fugir me agarra
                F#m
  Me prende e amarra
         E         D9
  Que eu devo tá louco

            E
  E logo vai passar

  A                     C#7
  Mas minha loucura te chama
                     F#m
  Te quer na minha cama
      E          D9
  Sem medo sem drama
                 A/C#  Bm         E
  Pronta pra amaaaaaar até o amanhecer

----------------- Acordes -----------------
Capotraste na 1ª casa
A*  = X 0 2 2 2 0 - (*A# na forma de A)
A/C#*  = X 4 X 2 5 5 - (*A#/D na forma de A/C#)
A9*  = X 0 2 2 0 0 - (*A#9 na forma de A9)
B7*  = X 2 1 2 0 2 - (*C7 na forma de B7)
Bm*  = X 2 4 4 3 2 - (*Cm na forma de Bm)
C#/F*  = X 8 X 6 9 9 - (*D/F# na forma de C#/F)
C#7*  = X 4 3 4 2 X - (*D7 na forma de C#7)
D9*  = X X 0 2 3 0 - (*D#9 na forma de D9)
E*  = 0 2 2 1 0 0 - (*F na forma de E)
F#m*  = 2 4 4 2 2 2 - (*Gm na forma de F#m)
