César Menotti e Fabiano - Gordinha

F#m              D               A
Ela tá fora do padrão que você quer
           E           F#m
Mas eu gosto dela assim
                D            A
Ela não faz seu tipo de mulher
       E                  F#m
Mas é fora do normal pra mim

F#m              D               A
Ela tá fora do padrão que você quer
           E           F#m
Mas eu gosto dela assim
                D            A
Ela não faz seu tipo de mulher
       E                  F#m
Mas é fora do normal pra mim


F#m              D               A
Ela não anda, não malha, não corre

              E              F#m
Mas ela se cuida como ela pode
                    D                   A
Não quer saber de malhação, ela tá linda
             E                             D E
Vai comigo pro boteco mas não vai pra academia


F#m                      D
Ela é gordinha mas é minha
                    A
Eu não troco por nada
                E
Quando sai comigo
                  F#m
Ela arrasa na balada

F#m                      D
Ela é gordinha mas é minha
                 A
Tem gente querendo
                E
Olha que sucesso
                       F#m
Minha gordinha tá fazendo

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
