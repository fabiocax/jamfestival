César Menotti e Fabiano - Lance Certeiro

[Intro] E  A  E  A9

      E
O seu beijo é bom
Você é meu dom
 A        E
Amor, e eu sou alguém que te sente
Lê a sua mente
 A        E       C#m
Amor, e eu vou te levar pra viver muito mais
       G#m
Mais e mais o que ninguém te faz
C#m
Arrancar um sorriso no escuro
G#m
Quem sabe até pensar no futuro
    A                   B
Te amar no carro na esquina
             E
Encostado no muro
               B
O trem ficou bom

                 A
Acertei bem em cheio
                   B
Não foi no lado esquerdo
        A
Nem do direito
       E
Foi no meio
              B
Azar, quem perdeu
                  A
Eu cheguei foi primeiro
               B
No leilão do amor
       A          E
O lance foi certeiro

( B  A  B  A  E )
( B  A  B  A  E )

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
G#m = 4 6 6 4 4 4
