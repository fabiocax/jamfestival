César Menotti e Fabiano - Sem Medo de Ser Feliz

[Intro] E

 E            A   A#   B
Tira essa paixão da cabeça
 A                     E
Tira essa tristeza no olhar
                        B
Já não faz sentido essa busca
 A                    E
Já não vale a pena chorar
          B             A              E
Tempo perdido, amor bandido que ele te fez
              B             A            E
Final da história, você sem rumo mais uma vez

                                       B
E como o rio busca o mar, você vem me procurar
       A                                           E           Bsus
E encontra em meu peito esse amor, que ele não quis te dar
E                                      G#
Então viaja no meu corpo, sem medo de ser feliz

         A                   B         E
E eu te dou meu amor, faço amor como nunca fiz

          B
Perco a cabeça, me queimo em seu fogo
         E
Eu sem juízo, faço o seu jogo
             B
Sou seu brinquedo
                            E
O seu presente que caiu do céu
         B
Faço de tudo pra te agradar
              E
Dorme em meus braços te faço sonhar
        A      B
Mas nem amanheceu
        G#m      C#m
Você já me esqueceu
 A                 B
Mais uma vez você vai
G#m             C#m7
Leva um pedaço de mim
F#m7                B                  E
Mais uma vez vou ficar te esperando aqui
 A                 B
Mais uma vez você vai
G#m             C#m7
Leva um pedaço de mim
F#m7                B                  E
Mais uma vez vou ficar te esperando aqui

[Solo] E  A  F#m7  E

            B
Perco a cabeça, me queimo em seu fogo
         E
Eu sem juízo, faço o seu jogo
             B
Sou seu brinquedo
                            E
O seu presente que caiu do céu
         B
Faço de tudo pra te agradar
              E
Dorme em meus braços te faço sonhar
       A      B
Mas nem amanheceu
       G#m      C#m
Você já me esqueceu
 A                 B
Mais uma vez você vai
G#m             C#m7
Leva um pedaço de mim
 F#m7                B                  E
Mais uma vez vou ficar te esperando aqui
 A                 B
Mais uma vez você vai
 G#m             C#m7
Leva um pedaço de mim
 F#m7                B                  E
Mais uma vez vou ficar te esperando aqui

----------------- Acordes -----------------
A = X 0 2 2 2 0
A# = X 1 3 3 3 1
B = X 2 4 4 4 2
Bsus = X 2 4 4 4 2
C#m = X 4 6 6 5 4
C#m7 = X 4 6 4 5 4
E = 0 2 2 1 0 0
F#m7 = 2 X 2 2 2 X
G# = 4 3 1 1 1 4
G#m = 4 6 6 4 4 4
