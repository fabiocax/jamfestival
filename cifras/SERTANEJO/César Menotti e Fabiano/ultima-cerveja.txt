César Menotti e Fabiano - Última Cerveja

G                       D
Amigo, estou vivendo no sufoco
                   C
Já estou ficando louco
                       G
Outra vez briguei com ela
 G                      D
Amigo, vou abrir meu coração
               C
Quero a sua opinião
                   G
Estou sofrendo por ela

 G                        D
Amigo, eu também to indo mal
             C                      G
O que passa afinal eu também estou passando
 C                            G
Amigo, o que adianta eu dar conselho
                        D
Se quando eu olho no espelho

                   G       G7
Meus olhos estão chorando

D
Meu amigo, sente aqui na minha mesa
                    C                   G
Vamos brindar com cerveja e afogar a solidão
D
Eu sei que a bebida não é remédio
                C                    G
Mas ameniza o tédio que causou esta paixao
C
No outro dia eu amanheço de ressaca
                D7
Chuto o pau da barraca
                     G
Não quero mais saber dela
    A7                    D
Meu amigo voce pode ter certeza
            C    D
É a última cerveja
                G
Que eu bebo por causa dela

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
