César Menotti e Fabiano - Seu Neném Te Ama

[Intro] A  E  F#m  D

A                    E
Uai, amor, você falou que queria terminar
F#m                      D
Eu fui correndo pro bar, eu fui correndo pro bar
A                               E
Uai, meu bem, por que cê ficou brava
                       F#m               D
Você falou que não me amava, que não me amava
A                    E
Eu sofri e fui beber e bebi pra te esquecer
   D                                  E
E fui chorar minhas pitangas e fiquei bebo no colin de uma estranha
A                        E
Uai, meu bem, tudo bem, briga não
             F#m               D
Seu neném te ama, seu neném te ama
                                  A             E
Vamos fazer amor e para com esse drama ai ai ai ai
                                  F#m           D
Vamos fazer amor e para com esse drama ai ai ai ai

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
