César Menotti e Fabiano - Máquina do Tempo

 D                     G
Saudade que não vai embora
 D                     G  G/F#
Insiste em ficar comigo
Em
É o meu martírio
Bm
Foi o meu castigo
G
Por não te entender
D                       G
Hoje estaria em seus braços
D                 G    G/F#
Se eu tivesse aceitado
Em                               Bm
Tudo do seu jeito te guardado dentro
           G
Do meu coração
Em
Porquê o homem já não inventou
Bm
A máquina do tempo me levou

F#m                   G
De volta ao passado, quando estava ao seu lado
A               G       A       G
Pra te amar e sorrir outra vez
A D        A         Bm
Você levou a minha liberdade
F#m    G
Minha felicidade
Em7           G
Levou o meu sorriso
Em7            G
Levou os meus sentidos
A
E só me deixou aqui
D      A     Bm          F#m        G
Todos estes sentimentos, e os meus pensamentos
Em7           G         E7      G        A           D
E choro quando lembro de todos os momentos felizes com você

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G/F# = X X 4 4 3 3
