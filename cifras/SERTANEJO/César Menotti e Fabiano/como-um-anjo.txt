César Menotti e Fabiano - Como Um Anjo

Intro: G  C9  G  C9  G  C9  G

Primeira Parte:

        G    C9  G
Como um anjo
                        D
Você apareceu na minha vida
        Am
Como um anjo
               C            D
Repleto de ternura e de paixão

wow wow wow wow

        G    C9  G
Como um anjo
                            D
Encanto e sedução doce aventura
            Am
Hum que loucura

              C                D  D#º
Você desabrochando no meu coração


Segunda Parte:
         Em
Linda menina com olhar inocente
                        D
Malícia, desejo e tentação

Que me cobre de amor e carícias
                C
Vencendo a solidão
 C  G/B  Am  G       D
Só vo___cê pra me fazer feliz

Ai ai ai ai ai ai


Refrão:

G                             Em
Anjo, a luz do sol tá me acordando
                         C
Não vá embora estou te amando
       Am         D
Por favor não me deixe só

Ai ai ai ai ai ai

G                          Em
Anjo, não quero abrir meus olhos
                         C
Quero seguir vivendo um sonho
           D        G   D
de sermos só você e eu


Base do Solo:

G Em C D G


Solo:

1
E|----------------------------------------------------|
B|-10/12~--12~--12~-12~-------------------------------|
G|----------------------------------------------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

2
E|----------------------------------------------------|
B|-10/12-10-8-7-8-------------------------------------|
G|----------------9----9p7---7~--9-7-9~---------------|
D|-------------------------9--------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

3
E|----------------------------------------------------|
B|----------------------------------------------------|
G|-4-5-7-5-4-5~---4-4-4-------------------------------|
D|----------------------7-5-7~------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

4
E|----------------------------------------------------|
B|----------------------------------------------------|
G|-----------5--4----4-4-4----------------------------|
D|-5--7------7--5----5-5-5----------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|


Repete a Segunda Parte:

         Em
Linda menina com olhar inocente
                        D
Malícia, desejo e tentação

Que me cobre de amor e carícias
                C
Vencendo a solidão
 C  G/B  Am  G       D
Só vo___cê pra me fazer feliz

Ai ai ai ai ai ai


Refrão Final:

G                             Em
Anjo, a luz do sol tá me acordando
                         C
Não vá embora estou te amando
       Am         D
Por favor não me deixe só

Ai ai ai ai ai ai

G                          Em
Anjo, não quero abrir meus olhos
                         C9
Quero seguir vivendo um sonho
           D9       C9
de sermos só você e eu

D9  D#9  F9  D9  G

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
D#9 = X 6 8 8 6 6
D#º = X X 1 2 1 2
D9 = X 5 7 7 5 5
Em = 0 2 2 0 0 0
F9 = X 8 10 10 8 8
G = 3 2 0 0 3 3
G/B = X 2 0 0 3 3
