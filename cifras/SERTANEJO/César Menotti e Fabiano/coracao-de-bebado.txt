César Menotti e Fabiano - Coração de Bêbado

D                 F#
  Um brinde a nós dois
          Bm       A                G
Que conseguimos acabar com o nosso amor
D                    F#
A gente bem que se merece
     Bm                    A
É um sujo falando do mal lavado
          G
A gente apronta e o coração paga o pato
     Em
Não aponta esse dedo pra mim
         G
Pois tem três virados pra você
   D
O cheiro de álcool pisando algo
    A               Em
Tentando disfarçar sabe como é
           A
Coração de bêbo só fala a verdade
                 D
Minha boca me entrega

              F#                           Bm
Eu ligo pra você e falo que te amo, te amo, te amo
                G
E esse amor vagabundo termina lá em casa
        D
E a gente se amando
             A           G
E no auge da cama ouço você dizer
                 D
Minha boca me entrega
              F#                           Bm
Eu ligo pra você e falo que te amo, te amo, te amo
                G
E esse amor vagabundo termina lá em casa
        D
E a gente se amando
             A           G
E no auge da cama ouço você dizer
              D
Eu também te amo

( F#  Bm  G )
( D  A  G )

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
