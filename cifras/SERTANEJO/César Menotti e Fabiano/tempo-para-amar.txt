César Menotti e Fabiano - Tempo Para Amar

G                                    C  D                          G
Faz tanto tempo que o amor tinha tempo pra nos dois
G                                    C                  D                         G
Mas com o passar do tempo nosso tempo foi ficando pra depois
G                   C  D                                G
Faz tanto tempo que a gente era só felicidade
G                                    C                  D                                   G
Mas com o passar do tempo nosso tempo com o tempo virou saudade
G                       D                                     C                        G
Cadê aquele tempo que a gente tinha tempo - tempo pra amar
G                                  D                               C                  G
Se a gente voltar no tempo Vai ver que ainda é tempo de reconciliar
G                                     D                              C               G
Todo amor tem um momento que precisa de um tempo pra reciclar
G                               D                   C                        G
Sem essa de sofrimento felicidade é tempo - tempo pra amar

G                               D                   C                        G
Sem essa de sofrimento felicidade é tempo - tempo pra amar

Repete inicio


----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
