César Menotti e Fabiano - Fica Comigo

Introdução: D D4 D D4 D D4 D D4

D             G/D             D D4
Você acendeu a luz da minha vida
                Bm            A
E me ensinou um jeito de amar você
G                            D
Tirou todas as pedras do meu caminho
    D4    Bm                A
E sozinho não sei mais viver
 D               G/D             D
Você fez arder a chama do sentimento
                         Bm               A
E agora vem me dizer que tudo não passou de ilusão
F#
Olha pra mim, não faz assim
Bm                    G       Gm
Diz que é mentira que não é o fim
  D                   A               G D/F# Em D Asus
Tente entender dê uma chance pro meu coração
  D                A            G               D
Tira esse medo de mim, não faz assim, não vá embora

                       A             G                   D
Eu não aprendi te esquecer ficar sem você, não me deixe agora
                 A           F#           Bm F#m G
E tira do meu coração, a solidão, esse castigo
                  D                      A
Amor não me enlouqueça, por favor não me deixa
       D G A Em/D D
Fica comigo

D Asus

Volta para Você fez arder a chama do sentimento...

----------------- Acordes -----------------
A = X 0 2 2 2 0
Asus = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D4 = X X 0 2 3 3
Em = 0 2 2 0 0 0
Em/D = X X 0 4 5 3
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G/D = X 5 5 4 3 X
Gm = 3 5 5 3 3 3
