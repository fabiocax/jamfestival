César Menotti e Fabiano - Dá Medo

[Intro] C  Em  C  D

C                     D
Perdoa se eu fico brigando
               Bm               Em
É meu jeito estranho de falar Te amo
C                    D
Perdoa se eu sou ciumento
             Bm                    Em
Se eu sou grudento, juro, eu tô mudando
              C
É que você é linda
               D                      Em
A mulher mais linda que pisou no solo do meu coração
        C    D            Em
Te perder tá fora de cogitação
                       C
A lua vai dormir mais cedo
            D                       Em
Porque seu brilho é o maior desse lugar
                                 C
Porque ninguém substitui o seu olhar

        D
Dá medo
                      Em
Só de pensar em te perder

Dá medo

( C  D  Em  C  D  Em )

----------------- Acordes -----------------
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
