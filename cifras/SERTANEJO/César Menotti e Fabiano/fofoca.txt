﻿César Menotti e Fabiano - Fofoca

Capo Casa 1

E                B
Tudo tava numa boa,
         C#m              A
O nosso amor estava indo bem,
     E                     B
Pra mim não tinha outra pessoa,
        C#m                   A
E pra você não tinha mais ninguém

    E                 B
Um dia olhei no seu olhar
       C#m               A
E percebi que algo aconteceu
    E                B
Foi só uma fofoca à toa
             C#m              A
Que alguém falou e tudo se perdeu

  C#m       A          E                       B
Mentira, engano, que você ouviu de um outro alguém
   C#m     A        E                         B
Conversa fiada, de quem não quer ver a gente indo tão bem


E           B             C#m
Tira essa idéia da sua cabeça,
              A                     B
Só penso em você não quero mais ninguém,
            B                      C#m
Vê se não demora antes que eu enlouqueça
               A                   E
Esquece essa fofoca e vamos ficar bem

(2x)

----------------- Acordes -----------------
Capotraste na 1ª casa
A*  = X 0 2 2 2 0 - (*A# na forma de A)
B*  = X 2 4 4 4 2 - (*C na forma de B)
C#m*  = X 4 6 6 5 4 - (*Dm na forma de C#m)
E*  = 0 2 2 1 0 0 - (*F na forma de E)
