César Menotti e Fabiano - Chandon

Intro: D  Bm  A  Bm  G

ESTROFE

Em                                 G
Seu problema é achar que tudo é normal
                         Bm
E que todo mundo pensa igual
     A
Mas eu não to nada bem
Em                               G
Essa mania de negar que quer me ver
                                Bm
Passa a noite esperando eu aparecer
   A        Em   G   A
E se acontecer

REFRÃO

  D                    A
Prepare o Chandon meu bem

Em                G
Hoje a noite é nossa
          D                 A
Põe os Menotti no som meu bem
Em                G
Hoje a festa é nossa
   D                   A
Prepare o Chandon meu bem
Em              G
Hoje a noite é nossa
          D                A
Põe os Menotti no som meu bem
Em              G
Hoje a festa é nossa

SOLO: (D  A  Bm G)

ESTROFE

REFRAO

SOLO: (D  A  Bm G)

          D                A
Põe os Menotti no som meu bem
Em              G
Hoje a festa é nossa

D

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
