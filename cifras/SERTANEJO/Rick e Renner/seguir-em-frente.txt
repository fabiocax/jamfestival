Rick e Renner - Seguir Em Frente

[Intro] G/A  Dm7  Dm6  F  G7  C

C                     G
Vou seguir em frente, vou tentar mais uma vez
F                   C
Nada foi em vão,  perder você foi solução
Bb                   F
Já dei liberdade pro meu coração voar
Dm                  G
Ir de novo a luta e outra vez se apaixonar
C               G
Fui abandonado muito mais que uma vez
Dm                Am
Estou calejado, acho que me acostumei
                  F
Todo desprezado sempre aprende uma lição
D                G
Sou especialista em viver na solidão
   C           G                     Am   A7
Amanhã, semana que vem ou mês que vem quem sabe
       Dm           C                  G  G7
Me encontrar, eu vou encontrar um amor de verdade

    Em       Em7                         Am    A7
Um alguém, que queira alguém desse meu jeito assim
               Dm  Dm7             G
Que vai me dar valor, que vai gostar de mim
   C            G                    Am  A7
Amanhã, semana que vem ou mês que vem quem sabe
      Dm        C               G
Vou ganhar, o meu pedacinho de felicidade
     Em      Em7                          Am   A7
Um alguém, que queira alguém desse meu jeito assim
            Dm   Dm7               G
Que vai me adorar, que vai me dar amor
               C
Que vai cuidar de mim

(  C  Dm  C7  D  Fm7  C6 )

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C6 = 8 X 7 9 8 X
C7 = X 3 2 3 1 X
D = X X 0 2 3 2
Dm = X X 0 2 3 1
Dm6 = X 5 X 4 6 5
Dm7 = X 5 7 5 6 5
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
Fm7 = 1 X 1 1 1 X
G = 3 2 0 0 0 3
G/A = 5 X 5 4 3 X
G7 = 3 5 3 4 3 3
