﻿Rick e Renner - Aqueles Olhos

Capo Casa 2

A
Onde estão aqueles olhos, que até hoje eu procuro,
                                          E
dois faróis da madrugada no meu quarto escuro,

Bm                 E7    A                  F#m
Onde estão aqueles olhos,que falavam só de amor,
   B7                                    E
estrelas que eu ví brilhando e não sei a cor,

2°Parte:

A
Onde estão aqueles olhos, que eu busco pelo mundo a fora,
                                      E
quando amanheceu o dia o sol levou embora,

Bm                 E              A              F#m
Onde estão aqueles olhos,que eu trago na imaginação
     Bm                 E7              A      E
seu brilho invadiu meu quarto e meu coração,


Refrão:

     A         C#m            A             F#m
São azuis,são azuis, verdes negros eu não sei,
       Bm              E            A
são castanhos aqueles olhos que eu amei,

     A         C#m            A             F#m
São azuis,são azuis, verdes negros eu não sei,
       Bm              E            A
são castanhos aqueles olhos que eu amei


Repete a 2° Parte e o Refrão...

----------------- Acordes -----------------
Capotraste na 2ª casa
A*  = X 0 2 2 2 0 - (*B na forma de A)
B7*  = X 2 1 2 0 2 - (*C#7 na forma de B7)
Bm*  = X 2 4 4 3 2 - (*C#m na forma de Bm)
C#m*  = X 4 6 6 5 4 - (*D#m na forma de C#m)
E*  = 0 2 2 1 0 0 - (*F# na forma de E)
E7*  = 0 2 2 1 3 0 - (*F#7 na forma de E7)
F#m*  = 2 4 4 2 2 2 - (*G#m na forma de F#m)
