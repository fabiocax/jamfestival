Rick e Renner - Relax

[Intro] C  G/B  Am  G  F
        C  G/B  Am  G  F

       C           C7M/9      C
Pelos papos que você tinha comigo
 C7M/9              C       G/B        Am   A4  A
Toda vez que me sentia um maior abandonado
       Dm          Dm7M        Dm7
Pelas vezes que você ficou distante
                   G                    C   F/G
Pra que eu comigo mesmo caísse na realidade

      C         C7M/9           C
Seus carinhos, cachoeiras de carinhos
        C7M/9          C
Que me limpam das tristezas
      G/B        Am   A4  A
E me deixam renovado
        Dm          Dm7M
Pelas brigas, nossa
               Dm7
Foram tantas brigas

                F          G        C       Gm  C
Pro amor ficar seguro e alcançar maturidade

    F                         Am
Preparado, eu fui ficando preparado
                  Dm
Espantei meus fantasmas
       G          C  Gm  C
Dei a cara pra bater
   F
É por isto e muito mais
    G     Am
Que eu te amo
                     Dm
Nosso amor tá dando certo
        G             C
Nosso amor tinha que ser
   G/B     Am
Assim, gostoso
 F                    C
Nosso amor tinha que ser
G/B          Am  F
    Bem natural
           C     G/B    Am
Tinha que ser assim, relax
 F                    C
Nosso amor tinha que ser
  F/G    C
Amor  total

[Solo]

      C         C7M/9           C
Seus carinhos, cachoeiras de carinhos
        C7M/9          C
Que me limpam das tristezas
      G/B        Am   A4  A
E me deixam renovado
        Dm          Dm7M
Pelas brigas, nossa
               Dm7
Foram tantas brigas
                F          G        C       Gm  C
Pro amor ficar seguro e alcançar maturidade

    F                         Am
Preparado, eu fui ficando preparado
                  Dm
Espantei meus fantasmas
       G          C  Gm  C
Dei a cara pra bater
   F
É por isto e muito mais
    G     Am
Que eu te amo
                     Dm
Nosso amor tá dando certo
        G             C
Nosso amor tinha que ser
   G/B     Am
Assim, gostoso
 F               C
Nosso tinha que ser
G/B          Am  F
    Bem natural
           C     G/B    Am
Tinha que ser assim, relax
 F                    C
Nosso amor tinha que ser
  F/G    C
Amor  total

 G#        C#
Tinha que ser
   G#/C    A#m
Assim, gostoso
 F#                   C#
Nosso amor tinha que ser
G#/C          A#m  F#
     Bem natural
           C#    G#/C    A#m
Tinha que ser assim,  relax
 F#                   C#
Nosso amor tinha que ser
  F#/G#    C#
Amor    total

----------------- Acordes -----------------
A = X 0 2 2 2 0
A#m = X 1 3 3 2 1
A4 = X 0 2 2 3 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C# = X 4 6 6 6 4
C7M/9 = X 3 2 4 3 X
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
Dm7M = X X 0 2 2 1
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
F#/G# = 4 X 4 3 2 X
F/G = 3 X 3 2 1 X
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
G#/C = X 3 X 1 4 4
G/B = X 2 0 0 3 3
Gm = 3 5 5 3 3 3
