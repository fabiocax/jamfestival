Rick e Renner - Só Coisá

Ela nao me ama
               C
Ela só qué coisá
                       F
Só qué coisá só qué coisá
F                           C
O negocio dela é coisá comigo
             Bb                  F
Mas eu tenho medo de coisá com ela
                                 C
Ela é muito nova e me cheira perigo
              Bb                 F
Mas eu nao consigo ficar longe dela
                                         Bb
Ela quer que eu entre e eu passo só na porta
                C                      F
E ninguem se importa com esse rela-rela

(refrão)
F                            C
Ela é moderna e até meio louca

           Bb                  F
Cara de menina mas tem um corpão
                                      C                                    F
Não quer mais saber só de beija na boca ela gosta carinho e vai metendo a mão
                                       Bb                       C                  F
Ela quer que eu entre ao menos um pouquinho eu dou um tchauzinho e volto pro portão

(refrão)
F                                C
É uma tortura o que ela faz comigo
                Bb                    F
Quem brinca com fogo acaba se queimando
                                   C
Eu me controlava hoje eu não consigo
              Bb                       F
Gosto do perigo acho que eu tou entrando
                                         Bb
Ela quer que eu entre e vou entrar na dela
                  C                  F
Deixar de rela-rela e acabar coisando

(refrão)  F C F

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
