Rick e Renner - E Dez E Cem E Mil

INTRO: Bm - G - Em - D/F# - D - C - Am - Bb - D

        G              D
Você me parte de desejo
         Am            Em
e ate me deixa sem assunto
       C                    Em
E toda vez que vai embora
         Am                  D
na mesma hora eu quero ir junto
        G                      D
A gente briga mais se entende
       Am           Em
e no final da tudo certo
          C           Em                   Am    D7
Distansia coisa que machu....ca e so nao doi
             G       C/B
se estas por perto
                D            Am                    G                 Em
*Pedaço que faz falta do meu lado, parte que se completa no meu ser
           Am          D                G           Em
Meu doce veneno, meu pecado sem você eu deixo de viver

        D                   Am
E muito mais que um simples caso
                 G
porque nao tem limites nem medida
        C                D
E a validade do teu prazo
            Am                 Cm7      G
vai durar enquanto eu tiver vida
              G
amor nao se encontra como eu
             D/F#               D
nem por ai jogado igual ao teu
                  C            Am
amor de mais pra atras a gente tem
              D                Bm7
se meu amor e dez o teu e cem
               G
amor assim faz falta a qualquer um
                D/F#              Bm7
o nosso amor nao e um lugar comum
                C             Am
amar assim tao forte ninguem viu
               D             G     2ºpasada, fazer terminaçao D7 e pula pra *
#se meu amor e cem o teu e mil     3º fazer terminaçao Em7 e pula pra #


Arranjo central e final
C6 - G/B - Cm7 - C/G - G/B - D

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
C/B = X 2 2 0 1 0
C/G = 3 3 2 X 1 X
C6 = 8 X 7 9 8 X
Cm7 = X 3 5 3 4 3
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
