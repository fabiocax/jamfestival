Rick e Renner - Eu Mereço

INTRO: F9  Bb9  F9  Bb9

F9                  F4   F9
Foi só uma noite e nada mais
                    F4      F9
Era pra ser só uma transa a mais
                              Bb Bb9
Mas não foi assim como eu pensei
Bb                           Gm
De repente um vinho um clima bom
                           Bb
Um toque de carinho aquele som
                 C              F9
Sem perceber aos poucos me entreguei

                   F4     F9
Foi um lance de desejo antigo
                    F9     F4
Não sei o que aconteceu comigo
                           Bb Bb9
Eu nunca transei ninguém assim

Bb                         Gm
Só que eu dormi depois do amor
                         Bb
E quando a manhã me acordou
           C          F    (F Em Dm C)
Estava sem ela e sem mim

C
O que é que a gente faz
                          Bb
Quando a paixão vem de repente
                                                 F
Feito um raio rouba a paz e invade o coração da gente
                    C
Assim da noite pro dia
                      F    (F Em Dm C)
Um sentimento por alguém

C
O que é que a gente faz
                       Bb
Quando a noite foi gostosa
                                            F
Você fez e aconteceu com uma mulher maravilhosa
                 C
Mas acorda no silêncio
              F
De um quarto sem ninguém


Refrão

C                           Dm
É, vira inferno o que era céu
                    C
Tô num quarto de motel
      Bb   (C)      F    (F Em Dm C)
Sem nome, sem endereço

C                        Dm
É, quem mandou me dar assim
                     C
Isso é bem feito pra mim
      Bb      (C)      F
Manda outra que eu mereço.

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bb9 = X 1 3 3 1 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F4 = 1 3 3 3 1 1
F9 = 1 3 5 2 1 1
Gm = 3 5 5 3 3 3
