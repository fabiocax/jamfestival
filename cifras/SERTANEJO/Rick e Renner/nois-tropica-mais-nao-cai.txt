Rick e Renner - Nois Tropica Mais Não Cai

Intro: G  D7  C  G  D7  C  D7  G

Refrão 2x:
         G            D7
Nóis tropica mais não cai
            C                G
Pode botar fé que desse jeito vai
                       D7
Nóis tropica mais não cai
            C           D7     G        D7
Pode botar fé que desse jeito vai

        G                                                       D7
Tem gente que não pode beber da boa no primeiro gole tá caindo à toa

Tem gente que não pode beber nada
             C      D7            G
No segundo gole ta beijando a escada

Tem gente que não pode beber vinho
              G7               C
na primeira taça já está tontinho

                                    G
Tem gente que não pode com rabo de galo
            D7             G
Que acaba mamado mijando ralo

(Refrão 2x)

           G
Tem gente que não pode beber tequila
                                  D7
Logo bate o sono e o cabra cochila
                                  C
Não pode beber nenhuma água ardente
             C       D7             G
Se vai conversar tá cuspindo na gente

Tem gente que não bebe uísque com gelo
         G7                   C
Bebe cowboy que até incha o joelho
                                    G
Tem gente que não pode beber uma cerveja
              D7               G
Começa chorar debruçado na mesa

(Refrão 2x)

           G
Tem gente que não bebe cachaça pura
                                D7
Açúcar e limão bota gelo e mistura

Aí chama isso de caipirinha
           C      D7         G
E arrota azedo a noite inteirinha

Tem gente que quando não acha cachaça
               G7                  C
Bebe qualquer coisa e vontade não passa
                             G
A bebida tá te deixando com sono
             D7                 G
Cuidado que U de bebum não tem dono

(Refrão 2x)

(Intro 4x)

           G
Tem gente que não bebe cachaça pura
                                D7
Açúcar e limão bota gelo e mistura

Aí chama isso de caipirinha
           C      D7         G
E arrota azedo a noite inteirinha

Tem gente que quando não acha cachaça
               G7                  C
Bebe qualquer coisa e vontade não passa
                             G
A bebida tá te deixando com sono
             D7                 G
Cuidado que U de bebum não tem dono

(Refrão 4x)

----------------- Acordes -----------------
C = X 3 2 0 1 0
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
