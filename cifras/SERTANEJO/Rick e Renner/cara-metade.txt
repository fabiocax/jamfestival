Rick e Renner - Cara Metade

G
Feito uma tatuagem você ficou no meu coração
                                                      D
e por mais que eu disfarce não dá pra fugir dessa paixão

 C                           G
Têm noites que eu não durmo, que eu não durmo direito
 A                                                 D
fico rolando na cama buscando seu corpo, sentindo seu cheiro

C                     G               Em
É mesmo uma tortura, choro e me desespero
Am                                 D                   G    G7
quando amanhece o dia estou quase louco porque eu te quero

C                               G
vida da minha vida, minha cara metade
                       D                   D7              G    G7
depois que você foi embora não posso esconder morro de saudade
C                                  G
paixão da minha vida, és o ar que respiro

                  D       C         D        G
minha alma gêmea bandida volta que eu te preciso

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
