Zé Neto e Cristiano - Seu Polícia

[Intro] F  C  Dm  C  Bb9

[Primeira Parte]

F
  Seu polícia
                         C
É que eu separei recentemente
       Dm
De paixão eu tô doente
      Bb9
Será que o senhor me entende

F
  Os vizinhos estão reclamando
              C
Do volume do meu som
       Dm
Mas enquanto ela não voltar
    Bb9
Eu vou continuar


[Refrão]

               F
Me afogando no álcool
                       C
Com o som do carro no talo
         Dm
Manda a multa que eu vou pagar
       Bb9
Mas enquanto ela não voltar

              F
Sofrimento é mato
              C
Coração em pedaço
     Dm
Compreenda por favor
      Bb9
O meu amor me deixou

[Base Solo]

( F  C  Dm  Bb9 )

[Primeira Parte]

F
  Seu polícia
                         C
É que eu separei recentemente
       Dm
De paixão eu tô doente
      Bb9
Será que o senhor me entende

F
  Os vizinhos estão reclamando
              C
Do volume do meu som
       Dm
Mas enquanto ela não voltar
    Bb9
Eu vou continuar

[Refrão]

               F
Me afogando no álcool
                       C
Com o som do carro no talo
         Dm
Manda a multa que eu vou pagar
       Bb9
Mas enquanto ela não voltar

              F
Sofrimento é mato
              C
Coração em pedaço
     Dm
Compreenda por favor
      Bb9
O meu amor me deixou

               F
Me afogando no álcool
                       C
Com o som do carro no talo
         Dm
Manda a multa que eu vou pagar
       Bb9
Mas enquanto ela não voltar

              F
Sofrimento é mato
              C
Coração em pedaço
     Dm
Compreenda por favor
      Bb9           F
O meu amor me deixou

----------------- Acordes -----------------
Bb9 = X 1 3 3 1 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
