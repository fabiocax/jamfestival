﻿Zé Neto e Cristiano - Ai Fechou

Afinação Meio Tom Abaixo

B                       F#
Já dei entrada no sofá da sala
G#m                                E
Eu tenho algumas coisas, sei que não é muito
B                 F#                        E7+
A decoração e o enxoval a gente escolhe junto

B                            F#
Nosso cantinho tá quase quitado
G#m
Se é junto que a gente se entende
   E            B          F#/A#      G#m           E
Então pra que viver separados, cada um pra um lado?

 C#m                     G#m
E não existe paraíso pronto pra ninguém
F#                                E
A gente é quem constrói o nosso com o que tem!

( E  B/D#  C#m  B )


 B                 F#
E o que falta em dinheiro
                    G#m           E
A gente completa com amor uôuô, uôuô
B               F#                G#m            E
Uma casinha, eu e você, já temos tudo, aí fechou uôuô

 B                 F#
E o que falta em dinheiro
                    G#m           E
A gente completa com amor uôuô, uôuô
B               F#                G#m            E
Uma casinha, eu e você, já temos tudo, aí fechou uôuô

----------------- Acordes -----------------
A*  = X 0 2 2 2 0 - (*G# na forma de A)
B9*  = X 2 4 4 2 2 - (*A#9 na forma de B9)
E*  = 0 2 2 1 0 0 - (*D# na forma de E)
F#m*  = 2 4 4 2 2 2 - (*Fm na forma de F#m)
