Zé Neto e Cristiano - Love Dog (part. Fred & Gustavo)

INTRO: Em7 C9 D

     Em7
Todo comportado
          C9
Nem pareço o cara de ontem à noite
         G
Até te assustei
                     D           B7
Me encontrando agora na segunda vez

 Am7
Romântico
                   Em7
E nem estranhe essas flores que eu te dei
         C9                        D   B7
Porque a noite vai começar tudo outra vez


               Em7
Eu tenho o lado love love love

               C9
Mais quando eu ativo o lado dog
                   G
Eu faço o que não pode
                                D      B7
Um lado faz carinho enquanto o outro morde

               Em7
Eu tenho o lado love love love
               C9
Mais quando eu ativo o lado dog
                   G
Eu faço o que não pode
                                D      B7
Um lado faz carinho enquanto o outro morde
                Em7
Prazer sou love dog

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
