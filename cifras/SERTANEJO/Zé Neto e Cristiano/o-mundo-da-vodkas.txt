Zé Neto e Cristiano - O Mundo Dá Vodkas

1ª Parte:

Bm                               G
Tá querendo saber o que eu ando fazendo
                             D
Só uma dica, não tô mais sofrendo
                               A9
Se quer voltar cê tá perdendo tempo
Bm                                       G
É, então, me passa aí o número da sua opinião
                             D
Se precisar eu vou ligar pra ela
                             A9
Mas por enquanto deixa ela quieta

2ª Parte:

G               A9                        Bm
Se eu tô bebendo muito, o que cê tem com isso?
                         G
Cada um que cuide do seu vício

                             F#
Não dá palpite onde eu não te chamar

Pré-Refrão:

Em (Modulação)
Se chorei, ou se bebi
                  A                        Bm
O importante é que atrás de você eu não corri

Refrão:

Bm                              G
O que o amor rejeita, a balada aceita
           A                   Bm
O mundo da vodkas e eu vou tomar
                               G
Tô de copo em copo, só postando foto
       A                         Bm
Abre o meu snap que cê vai chorar
(2x)

2ª Parte

Pré-Refrão

Refrão

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
