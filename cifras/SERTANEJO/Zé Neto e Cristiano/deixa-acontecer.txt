Zé Neto e Cristiano - Deixa Acontecer

 G#m        E                 F#
Já pensou, se você desse uma chance pra virar amor
 G#m        E                    F#
Já tentou, esquecer todo o seu passado e quem te machucou
 C#m
Eu tô colhendo os frutos que eu não plantei
 G#m
Esquece essa dor deixa de lado
  E
Eu sei
                F#    B
Só que você não vê
B
Que a gente dorme junto
               F#
E acorda separado

É cada um pro lado
              C#m
Tá tudo bagunçado eu e você
        E                     F#
Mas sempre que te da saudade dele

               B
Você quer me ver

----------------- Acordes -----------------
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
