Zé Neto e Cristiano - Pra Não Te Perder

Intro: Bm G D A (2X)

       Em                  G
Seu cheiro no meu travesseiro
               D                  A
É tudo que eu tenho pra lembrar você
     Em                  G
O coração que fez no espelho
              D                 A
Com batom vermelho só me faz sofrer.
        Em                     G
Suas marcas ainda estão presentes
             D                 A
No meu consciente que lembra você
    Em                G
Do dia em que você partiu
               D                  A
Deixando um vazio difícil de esquecer
        Em              G    .
Mais eu pago qualquer preço
             D                 A
Me viro do avesso pra não te perder,

      Em               G
Eu vou até o fim do mundo,
           D                 A
Faço um absurdo só pra ter você..

      Bm
E dererê
                   G
Se for pra ficar junto
              A
Tem que ser você
    Bm.
Dererê
                 G
Te quero nessa vida
                 A
E todas que eu viver
    Bm
Dererê
                    G
Ta pra surgir no mundo
                A
Um sentimento assim,
      G
Dererêeee
               A
Eu nasci pra você, Você pra mim!

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
