﻿Zé Neto e Cristiano - Derreter a Aliança

Capo Casa 2

[Intro] F#m  E  D9  E

[Primeira Parte]

             D9
Eu tava tão certo
               E
Quando eu decidi
             F#m
Pedir a sua mão
Foi a maior besteira que eu fiz
    D9
Inocente eu fui
       E                    F#m
Mal sabia o que estava por vir

[Pré-Refrão]

               Bm7
Mais de cinco mil reais
                 E
Foi o que eu gastei

             F#m
Nesse par de alianças eu me endividei
            Bm7                  D9
Parcelei a palhaçada toda no cartão
                 E
E agora eu to pagando é com a solidão

[Refrão]

                     A
Vou derreter essa aliança
                      E
Vou tomar tudo de cachaça
                       F#m
Pra ver se passa e ameniza essa dor
                     D
Se aceita ouro no boteco
                 E
É pra lá que eu vou

                     A
Vou derreter essa aliança
                      E
Vou tomar tudo de cachaça
                       F#m
Pra ver se passa e ameniza essa dor
Eu vou fazer um rolo
 D            E
Com o dono boteco
                           A
Já que eu não tenho mais amor

( E  F#m  D  E )

[Primeira Parte]

             D9
Eu tava tão certo
               E
Quando eu decidi
             F#m
Pedir a sua mão
Foi a maior besteira que eu fiz
    D9
Inocente eu fui
       E                   F#m
Mal sabia o que estava por vir

[Pré-Refrão]

               Bm7
Mais de cinco mil reais
                 E
Foi o que eu gastei
             F#m
Nesse par de alianças eu me endividei
            Bm7                  D
Parcelei a palhaçada toda no cartão
                 E
E agora eu to pagando é com a solidão

[Refrão]

                     A
Vou derreter essa aliança
                      E
Vou tomar tudo de cachaça
                       F#m
Pra ver se passa e ameniza essa dor
                     D
Se aceita ouro no boteco
                 E
É pra lá que eu vou

                     A
Vou derreter essa aliança
                      E
Vou tomar tudo de cachaça
                       F#m
Pra ver se passa e ameniza essa dor
Eu vou fazer um rolo
 D            E
Com o dono boteco
                           A
Já que eu não tenho mais amor

 E                  A
Vou derreter essa aliança
                      E
Vou tomar tudo de cachaça
                       F#m
Pra ver se passa e ameniza essa dor
                     D
Se aceita ouro no boteco
                 E
É pra lá que eu vou

                     A
Vou derreter essa aliança
                      E
Vou tomar tudo de cachaça
                       F#m
Pra ver se passa e ameniza essa dor
Eu vou fazer um rolo
 D            E
Com o dono boteco
                           A  E
Já que eu não tenho mais amor

               A
Já que eu não tenho mais amor

----------------- Acordes -----------------
Capotraste na 2ª casa
A*  = X 0 2 2 2 0 - (*B na forma de A)
Bm7*  = X 2 4 2 3 2 - (*C#m7 na forma de Bm7)
D*  = X X 0 2 3 2 - (*E na forma de D)
D9*  = X X 0 2 3 0 - (*E9 na forma de D9)
E*  = 0 2 2 1 0 0 - (*F# na forma de E)
F#m*  = 2 4 4 2 2 2 - (*G#m na forma de F#m)
