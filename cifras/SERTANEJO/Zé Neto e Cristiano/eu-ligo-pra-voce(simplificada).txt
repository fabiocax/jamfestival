﻿Zé Neto e Cristiano - Eu Ligo Pra Você

Capo Casa 2

[Intro] D  A  Em  G

[Primeira Parte]

D
  Desse jeito que eu ando
                                  A
Eu não sei se aguento mais uma semana
                                                 Em
A saudade é tanta e no meio da noite o corpo reclama
            G   A
A falta de você,  a falta de você

[Segunda Parte]

D
  Tô jogado no canto
                                     A
Tô bebendo de um jeito que eu não bebia
                                             Em
A saudade judia quando olho a última fotografia

             G   A
Eu ligo pra você,  eu ligo pra você

[Refrão]

D
  Tô gritando socorro

Meu único pedido
A
  Aquele amor gostoso, faz de novo comigo
Em                                           G
    Quero sentir seu cheiro, ver seu corpo suar
   A
Então atende o celular

D
  Tô gritando socorro

Meu único pedido
A
  Aquele amor gostoso, faz de novo comigo
Em                                           G
    Quero sentir seu cheiro, ver seu corpo suar
   A
Então atende o celular

( D  A  Em  G  A )

[Segunda Parte]

D
  Tô jogado no canto
                                     A
Tô bebendo de um jeito que eu não bebia
                                             Em
A saudade judia quando olho a última fotografia
             G   A
Eu ligo pra você,  eu ligo pra você

[Refrão]

D
  Tô gritando socorro

Meu único pedido
A
  Aquele amor gostoso, faz de novo comigo
Em                                           G
    Quero sentir seu cheiro, ver seu corpo suar
   A
Então atende o celular

D
  Tô gritando socorro

Meu único pedido
A
  Aquele amor gostoso, faz de novo comigo
Em                                           G
    Quero sentir seu cheiro, ver seu corpo suar
   A
Então atende o celular

[Final] D

----------------- Acordes -----------------
Capotraste na 2ª casa
A*  = X 0 2 2 2 0 - (*B na forma de A)
D*  = X X 0 2 3 2 - (*E na forma de D)
Em*  = 0 2 2 0 0 0 - (*F#m na forma de Em)
G*  = 3 2 0 0 0 3 - (*A na forma de G)
