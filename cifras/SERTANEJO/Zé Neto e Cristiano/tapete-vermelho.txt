Zé Neto e Cristiano - Tapete Vermelho

intro: Bm G A

E|-------5-5---2-----------------------|
B|-2/3-2------3--5---------------------|
G|------4----4-------------------------|
D|-------------------------------------|
A|-------------------------------------|
E|-------------------------------------|



 D                        G
Essa é a data que a gente preparou
          D                              G
6 de Fevereiro nosso casamento e nesse momento
        (D/F#)      Em                               G
Tô te esperando no altar, na capelinha que a gente escolheu
                        Bm                 G
E mesmo antes de você entrar, a lágrima desceu

     G                    A
E na cabeça passa tanta coisa

     F#m
Eu imaginando o padre falando
      G                A
Já pode beijar a noiva-aaa

Bm  A   G                   A
Mas esperai, já passou da hora de você chegar
              F#m
Se bem que é normal a noiva demorar
        Bm
Tô te esperando pra gente casar
      G                      A
Mas espera aí, já passou da hora de você chegar
              F#m
Se bem que é normal a noiva demorar
         Bm
Mais um convidado veio me falar
            G                           A
Que pode esquecer o beijo, e nesse tapete vermelho
                F#m    Bm
A noiva não vai mais passar

E|---------2-2--------2-2--------------------------------------------------------|
B|--2/3-2------2/3-2---------3-5-5--3-3-2-2------------2h3p2h3p2-------2h3p2h3p2-|
G|--------4----------4----2/4--------------2-2------------2----------------------|
D|--------------------------------------------4------------4---------------------|
A|-------------------------------------------------------------------------------|
E|-------------------------------------------------------------------------------|

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
