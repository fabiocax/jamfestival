﻿Zé Neto e Cristiano - Bebida Na Ferida

Capo Casa 1

[Intro]  F9

[TAB - solo Intro]

E|-----8-7-3/5-----------------------------------------|
B|-6/8----------6p5-6--5-3--3/5--1---------------------|
G|-----------------------------------------------------|
D|--------------7p5-7--5-3--3/5--3---------------------|
A|-----------------------------------------------------|
E|-----------------------------------------------------|

[Primeira Parte]

                  C
Parecia um bom negócio
G                   Am7
  Você se desfazer de mim
F9                        C
   Só não contava com os juros
                     G
Que a saudade, no futuro

                  F9
Ia te cobrar por mim

[TAB - solo Primeira parte]

E|-----------------------------------------------------|
B|-3/5-3--3/5-1--3/5-6-5-3--3/5-1--3/5-3--3/5-6--------|
G|-----------------------------------------------------|
D|-3/5-3--3/5—3--3/5-7-5-3--3/5-3--3/5-3--3/5-7--------|
A|-----------------------------------------------------|
E|-----------------------------------------------------|

[Pré-Refrão]

              C
Agora tá de standby
                      G
Vai fazer isso com os outros, vai
               Am7
Tudo que vai, volta, ai, ai, ai
                  F
Quem abre também fecha a porta

[Refrão]

       C
Te perder
                 G
Foi a dor mais doída

Que eu senti na vida
       Am7
Sem você
               F
Joguei bebida na ferida

       C
Te perder
                 G
Foi a dor mais doída

Que eu senti na vida
       Am7
Sem você
               F
Joguei bebida na ferida
     G                     C
Que bom que o álcool cicatriza

( G  Am7  F )

(Solo Refrão)

[TAB - Solo refrão]

E|-3-5-7-7/8--7-3---4/5-3-----1-0----------------------|
B|----------------5---------------3-1------------------|
G|-4-5-7-7/9--7-4---4/5-4-1/2---0----------------------|
D|----------------5---------------3-2------------------|
A|-----------------------------------------------------|
E|-----------------------------------------------------|

[Pré-Refrão]

              C
Agora tá de standby
                      G
Vai fazer isso com os outros, vai
               Am7
Tudo que vai, volta, ai, ai, ai
                  F
Quem abre também fecha a porta

[Refrão final]

       C
Te perder
                 G
Foi a dor mais doída

Que eu senti na vida
       Am7
Sem você
               F
Joguei bebida na ferida

       C
Te perder
                 G
Foi a dor mais doída

Que eu senti na vida
       Am7
Sem você
               F
Joguei bebida na ferida
     G                     C   G   Am7
Que bom que o álcool cicatriza   (solo)
                    F
Joguei bebida na ferida
                        C
Que bom que o álcool cicatriza

[TAB - Solo refrão]

E|-3-5-7-7/8--7-3---4/5-3-----1-0----------------------|
B|----------------5---------------3-1------------------|
G|-4-5-7-7/9--7-4---4/5-4-1/2---0----------------------|
D|----------------5---------------3-2------------------|
A|-----------------------------------------------------|
E|-----------------------------------------------------|

----------------- Acordes -----------------
Capotraste na 1ª casa
Am7*  = X 0 2 0 1 0 - (*A#m7 na forma de Am7)
C*  = X 3 2 0 1 0 - (*C# na forma de C)
F*  = 1 3 3 2 1 1 - (*F# na forma de F)
F9*  = X X 3 2 1 3 - (*F#9 na forma de F9)
G*  = 3 2 0 0 3 3 - (*G# na forma de G)
