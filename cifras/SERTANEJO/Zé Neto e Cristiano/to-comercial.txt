Zé Neto e Cristiano - Tô Comercial

Intro: Dm Bb F C

   Dm
Tô na vitrine de novo
    Bb
Melhor que antes, um pouco
     C
Saí da coleira voltei pra zueira
     Gm
Com os amigos tô na bebedeira...

    Dm
Tô pra negócio na pista,
     Bb
Recuperando o tempo que eu perdi
    C
Tô sentindo que eu sou novidade,
 Gm
Fiquei longe da realidade

Bb
E preso na rotina

Gm
Isso não era vida
F
E agora eu voltei
              C
Tô livre outra vez!

        Dm
Eu tô comercial,
              Bb
Tô livre pra negócio
                  F
Falar de amor não quero
                        C
Mas beijar na boca eu posso

         Dm
Eu tô comercial,
              Bb
Tô na liquidação
                F
Nada de exclusividade
                   C
Eu tô fazendo é promoção!

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
