﻿Zé Neto e Cristiano - Long Neck

Capo Casa 2

[Intro] A  E  Bm  D

                     A       E      Bm     D
E|----------------------------------------------------|
B|---------------------2-------2-------2---3/5-3------|
G|-1/2-2-2-2-2-----2/4-----2/4-----2/4----------------|
D|-------------4-2-------2-------2--------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

[Primeira Parte]

Bm    D      A9  E
Isso não é amor
 Bm   D    A9   E
Deve ser doença
  Bm      D              A              E
Amor não faz a gente chorar, sofrer, beber
 Bm              D               E
Tô aqui nesse boteco tentando te esquecer


[Refrão]

                      A
De long neck em long neck
                     E
Uma hora a gente esquece
             Bm
Se não esquecer vou apelar
        A/C#  D              E
Pras bebida quente desse bar

                      A
De long neck em long neck
                 E
Uma hora a gente esquece
    D  A/C#  A   Bm
Se não es__que__cer vou apelar
        A/C#  D              E
Pras bebida quente desse bar
                                   A   E  Bm  D
Vai ser rabo de galo, conhaque, cynar

( A  E  Bm  D )

[Primeira Parte]

  Bm      D              A              E
Amor não faz a gente chorar, sofrer, beber
 Bm              D               E
Tô aqui nesse boteco tentando te esquecer

[Refrão]

                      A
De long neck em long neck
                     E
Uma hora a gente esquece
             Bm
Se não esquecer vou apelar
        A/C#  D             E
Pras bebida quente desse bar

                      A
De long neck em long neck
                 E
Uma hora a gente esquece
    D  A/C#  A   Bm
Se não es__que__cer vou apelar
        A/C#  D             E
Pras bebida quente desse bar

                      A
De long neck em long neck
                     E
Uma hora a gente esquece
             Bm
Se não esquecer vou apelar
        A/C#  D             E
Pras bebida quente desse bar
                                   A  E
Vai ser rabo de galo, conhaque, cynar

    D  A/C#  A   Bm
Se não es__que__cer vou apelar
        A/C#  D              E
Pras bebida quente desse bar
                                   A
Vai ser rabo de galo, conhaque, cynar

----------------- Acordes -----------------
Capotraste na 2ª casa
A*  = X 0 2 2 2 0 - (*B na forma de A)
A/C#*  = X 4 X 2 5 5 - (*B/D# na forma de A/C#)
A9*  = X 0 2 2 0 0 - (*B9 na forma de A9)
Bm*  = X 2 4 4 3 2 - (*C#m na forma de Bm)
D*  = X X 0 2 3 2 - (*E na forma de D)
E*  = 0 2 2 1 0 0 - (*F# na forma de E)
