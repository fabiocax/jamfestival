﻿Zé Neto e Cristiano - Conversando Com o Abajur

Capo Casa 3

[Intro] Am  Em  F
          G  Am  G7

C
Meu dia começa
Quando a noite chega
Am
Solidão companheira
                         G                                D
E o sentimento cai, e o sentimento cai

[Pré-Refrão]

Am
Vem a madrugada e eu aqui
                                C
Outra noite sem dormir
                                               D
Deu farol vermelho pra felicidade
Am                                                                      C
Até a luz do meu abajur é mais forte do que eu

                                                              D
Tá até fazendo sombra na minha saudade

[Refrão]

                                         G
E quanto mais eu penso nela, mais penso de novo
                                D
Nem a bebida me livra desse sentimento louco
                                 Am                                                     D
Enquanto o sol não volta, vou conversando com o abajur
              Am                C            G
Quem sabe ele me dá uma luz

( Am  C  G )

[Pré-Refrão]

Am
Vem a madrugada e eu aqui
                                C
Outra noite sem dormir
               D
Deu farol vermelho pra felicidade
Am                                                                      C
Até a luz do meu abajur é mais forte do que eu
                                                              D
Tá até fazendo sombra na minha saudade

[Refrão]

                                         G
E quanto mais eu penso nela, mais penso de novo
                                D
Nem a bebida me livra desse sentimento louco
                                 Am                                                     D
Enquanto o sol não volta, vou conversando com o abajur
              Am                C            G
Quem sabe ele me dá uma luz

[Final]

G                                             Am
vou conversando com o abajur
            C                                 G
Quem sabe ele me dá uma luz

----------------- Acordes -----------------
Capotraste na 3ª casa
Am*  = X 0 2 2 1 0 - (*Cm na forma de Am)
C*  = X 3 2 0 1 0 - (*D# na forma de C)
D*  = X X 0 2 3 2 - (*F na forma de D)
Em*  = 0 2 2 0 0 0 - (*Gm na forma de Em)
F*  = 1 3 3 2 1 1 - (*G# na forma de F)
G*  = 3 2 0 0 0 3 - (*A# na forma de G)
G7*  = 3 5 3 4 3 3 - (*A#7 na forma de G7)
