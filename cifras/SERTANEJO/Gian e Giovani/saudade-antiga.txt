﻿Gian e Giovani - Saudade Antiga

Afinação Um Tom Abaixo

Intro: D G D G

D                       G
Abri meu guarda-roupa e peguei meu paletó

D                              A
Que há muito tempo eu não vestia

D                       D7
Quando pus a mão no bolso

 G                Gm
Pro passado retornei...

D               G    A      D D7
Eu encontrei a sua fotografia

G
Assim toda amassada

D
Pelo tempo desbotada


G                                 D D7
Você, que um dia foi meu grande amor

G
A solidão bateu na porta

D
Trazendo tudo de volta

G              A                D G A
E uma saudade antiga então chegou

D                                    G
Quando a gente disse adeus naquele dia

A                                D
Você pediu pra devolver sua fotografia

F#m                                  Cm
E ao tirá-la da carteira pra te entregar

G                 A                     D D7
Você notou minha tristeza e não quis levar

G                     A           D D7
A sua foto em minhas mãos eu apertei

G             A                     D D7
Ao ver você partindo e me deixando só

G                  A               D D7
E sem coragem de rasgá-la eu amassei

G                   A          D
E guardei no bolso do meu paletó

             G                       D
Hoje a sua saudade rouba minha alegria

                 G                  A
Você estava tão longe, agora te encontro

          D D7
Na fotografia

              G
Eu que nem me lembrava

               D                G
Que vivia tão só porque tinha guardado

               A               G Gm D
o passado no bolso do meu paletó

----------------- Acordes -----------------
A*  = X 0 2 2 2 0 - (*G na forma de A)
Cm*  = X 3 5 5 4 3 - (*A#m na forma de Cm)
D*  = X X 0 2 3 2 - (*C na forma de D)
D7*  = X X 0 2 1 2 - (*C7 na forma de D7)
F#m*  = 2 4 4 2 2 2 - (*Em na forma de F#m)
G*  = 3 2 0 0 0 3 - (*F na forma de G)
Gm*  = 3 5 5 3 3 3 - (*Fm na forma de Gm)
