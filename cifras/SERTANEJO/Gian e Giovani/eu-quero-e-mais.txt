Gian e Giovani - Eu Quero É Mais

G
Amanheceu, um dia frio
                              Am
no carro eu vi você sair sem mim

Ainda tenho a chave e hoje eu vou entrar
                                       G      D
vim buscar meus sonhos que eu deixei neste lugar

G
Não é o fim, eu quero é mais
                                Am
o seu amor ficou grudado em em mim

tudo que passou foi pra me ensinar
                                      G     G7
que o tempo passa e tudo fica no mesmo lugar

C                         D
No quarto ainda a chama acesa
     G                        Em
do amor que nessa cama aqui rolou

      Am                 D
no espelho a marca de batom é a mesma,
             G
nada aqui mudou
C                              D
Foi só e quem fui da porta pra fora
       G                         Em
e lá fora a vida foi quem me ensinou
      Am                 D
que o tempo passa, mas não passa o tempo
     G       D
se existe amor

    G      C             G
Me ama, debruça no meu peito
  C          G        D            G   D
esquece do passado, preciso de um beijo
    G        C           G
Te amo e o tempo vai provar
    C               D         C            G
mas custe o que custar, antes de sair, me ama

       Eb                      G
Só eu sei a falta que você me faz
       Eb                            G
Quando eu digo que acabou, eu quero é mais

(G  D  C   G   D   G  D  C   G   D   G7)

C                         D
No quarto ainda a chama acesa
     G                        Em
do amor que nessa cama aqui rolou
      Am                 D
no espelho a marca de batom é a mesma,
             G
nada aqui mudou
C                              D
Foi só e quem fui da porta pra fora
       G                         Em
e lá fora a vida foi quem me ensinou
      Am                 D
que o tempo passa, mas não passa o tempo
     G       D
se existe amor

    G      C             G
Me ama, debruça no meu peito
    C          G        D            G   D
esquece do passado, preciso de um beijo
    G        C           G
Te amo e o tempo vai provar
     C               D         C            G
mas custe o que custar, antes de sair, me ama

        Eb                      G
Só eu sei a falta que você me faz
       Eb                            G
Quando eu digo que acabou, eu quero é mais

( G  D  C )

     G                                   D     C     G
Eu quero, eu quero, eu quero, eu quero mais    vo...cê

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Eb = X 6 5 3 4 3
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
