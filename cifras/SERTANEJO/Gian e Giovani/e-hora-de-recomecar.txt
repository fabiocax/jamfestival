Gian e Giovani - É Hora de Recomeçar

 Intro  A, A4, A, A4, A, D/E, E, D/E, E7, A4, A, A4


     A
UM AMOR QUE NAO DEU CERTO
       E
NAO MERECE A MINHA VIDA
      D
EU NAO  QUERO
       D/E     E    A     G/B  A/C#
A JUVENTUDE EM MIM PERDIDA
       D
EU VOU CONQUISTAR
     E/D          A/C#   F#m
PRA MIM UM SOMHO NOVO
      D/E E      A  D/E  E
E SONHAR   E SONHAR

       A
VOU TIRAR VOCE DE DENTRO
        E/G#
VOU LIMPAR A MINHA CASA

      D/F#        D/E       A   G/B  A/C#
ARRANCAR DE VEZ O MAL PELA RAIZ
      D
EU VOU CONSEGUIR
     E/D          A/C#  F#m
PRA MIM OUTRA PESSOA
      D/E E      A   G/B  A/C#
E SONHAR   E SONHAR

      D          E           A
O PASSADO NAO IMPORTA JA PASSOU
            F#m
NAO VOLTA MAIS
        Bm           E
VOU DEIXAR ENTRAR O NOVO
       Em             A7
E NAO VOU OLHAR PRA TRAS
    D              E/D
EU VOU COLHER NO FUTURO
      A/C#         F#m
O QUE EU PLANTAR AGORA
    Bm          E        A
E AGORA E HORA DE RECOMECAR
      Bm   E     A
E SONHAR   E SONHAR

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A4 = X 0 2 2 3 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D/E = X X 2 2 3 2
D/F# = 2 X 0 2 3 2
E = 0 2 2 1 0 0
E/D = X 5 X 4 5 4
E/G# = 4 X 2 4 5 X
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G/B = X 2 0 0 3 3
