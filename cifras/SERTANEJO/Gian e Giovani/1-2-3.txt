Gian e Giovani - 1, 2, 3

[Intro] D  A7  G
        D  A7  G  D

D                            A7
De uma cachaça boa não abro mão
               G              D
Comigo briga tem que ser no braço
                           A7
Só obedeço as ordens do coração
               G             D
 tudo que vem na cabeça eu faço
      D7             G
Já montei cavalo bravo
    A7              F#m
Conquistei muitas mulheres
       Bm          A7       G          D
Dei a volta pelo mundo acreditem se quiserem
      D7           G
Pra chegar onde cheguei
      A7      F#m       Bm          A7
Abri mão da vaidade e passar o que passei

              G        D
Tem que ter força de vontade
          D7             G
Não me arrependo do que fiz
  A7            D       Bm         Em
Faria tudo de novo pra chegar até aqui
         A7                D
Sorrir,cantar para o meu povo

 D                            A7
Vamos dar as mãos um,dois,três
                G            D
Quem errar o passo perde a vez
 D                            A7
Vamos dar as mãos um,dois,três
                G            D
Quem errar o passo perde a vez

        D7           G
Quero ouvir todos cantando
          A7             D
E quem desafinar cante outra vez

 D                            A7
Vamos dar as mãos um,dois,três
                G            D
Quem errar o passo perde a vez
 D                            A7
Vamos dar as mãos um,dois,três
                G            D
Quem errar o passo perde a vez

        D7           G
Quero ouvir todos cantando
          A7             D
E quem desafinar cante outra vez

( D  A7  G )
( D  A7  G  D )

D                            A7
De uma cachaça boa não abro mão
               G              D
Comigo briga tem que ser no braço
                           A7
Só obedeço as ordens do coração
               G             D
Tudo que vem na cabeça eu faço
      D7             G
Já montei cavalo bravo
    A7              F#m
Conquistei muitas mulheres
       Bm          A7       G          D
Dei a volta pelo mundo acreditem se quiserem
      D7           G
Pra chegar onde cheguei
      A7      F#m       Bm          A7
Abri mão da vaidade e passar o que passei
              G        D
Tem que ter força de vontade
          D7             G
Não me arrependo do que fiz
  A7            D       Bm         Em
Faria tudo de novo pra chegar até aqui
         A7                D
Sorrir,cantar para o meu povo

 D                            A7
Vamos dar as mãos um,dois,três
                G            D
Quem errar o passo perde a vez
 D                            A7
Vamos dar as mãos um,dois,três
                G            D
Quem errar o passo perde a vez

        D7           G
Quero ouvir todos cantando
          A7             D
E quem desafinar cante outra vez

 D                            A7
Vamos dar as mãos um,dois,três
                G            D
Quem errar o passo perde a vez
 D                            A7
Vamos dar as mãos um,dois,três
                G            D
Quem errar o passo perde a vez

        D7           G
Quero ouvir todos cantando
          A7             D
E quem desafinar cante outra vez

( D  A7  G )
( D  A7  G  D )

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
