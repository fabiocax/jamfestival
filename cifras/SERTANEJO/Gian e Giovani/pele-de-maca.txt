Gian e Giovani - Pele de Maçã

Intro: Am  D  G  C  G

            C  D             Bm   Em
Olhos de paixão,lábios de carmim
               Am           C   D7          G    C G
Beijos de hortelã,pele de maçã,cheiro de jasmim
            C   D        Bm   Em
Sorriso inocente,a me iludir
           Am              D7                     G
Voz de sedução,você é a canção mais linda que eu ouvi

D             Em
Minha loucura,minha ternura
C            D          G  D Em
Ah! se eu pudesse escrever
   D                  Em
O quanto eu te amo,o quanto eu te adoro
         C         D           G
Está canção seria pouco pra dizer

        D          Em
Doce paixão no coração

             C     D        G
Já não sei mais viver sem você
        D          Em
Doce paixão no coração
             Am    D        G
Já não sei mais viver sem você

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
