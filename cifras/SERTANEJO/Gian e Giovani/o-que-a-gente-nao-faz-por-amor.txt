Gian e Giovani - O Que a Gente Não Faz Por Amor

(intro) Bb  F  C7  Bb  F

F                                   Bb F
Ela já chega bem cedo me tira do sono faz um chamego e me diz
            Bb                     Gm                 C7
que sou seu dono faz um jeitinho manhoso de quem nada quer no
 Gm              C7                  F    C7
fim eu acabo seu homem e ela minha mulher

(parte 2)
    F                                      Bb F
E assim começa outra noite paixão e ternura dois corpos
           F7              Bb
rolando na cama gostosa loucura e eu nunca pensei que fosse tão bom
         F
assim um piscar de olhos tomou conta de mim

(estribilho)
    Gm                    C7        F        Bb F
E assim ela entrou de uma vez nesse peito meu e o que é que a
          Bb                                 F
gente não faz por amor mas o que a gente não faz por paixão

            C7            Bb           F  F7
briga com o mundo pisa no fundo do coração   e o que é que a
          Bb                                 F
gente não faz por amor mas o que a gente não faz por paixão
        C7          Bb             F
fica perdido, aborrecido na solidão

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
Gm = 3 5 5 3 3 3
