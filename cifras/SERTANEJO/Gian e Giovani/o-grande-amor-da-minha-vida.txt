Gian e Giovani - O Grande Amor da Minha Vida

C                     Em
A gente morou e cresceu
          Am
Na mesma rua
           C7              F
Como se fosse o sol e a lua
F/E         Dm      G7
Dividindo o mesmo céu
C                   Em
Eu a vi desabrochar
            Am
Ser desejada
   C7          F
Uma joia cobiçada
           G7
O mais lindo dos troféus

                 F
Eu fui o seu guardião
                 C
Eu fui seu anjo amigo

            Am
Mas não sabia
       D7
Que comigo
Por ela carregava
     G7
Uma paixão

C                  Em
Eu a vi se aconchegar
             Am
Em outros braços
       C7             F
E sai contando os passos
F/E             Dm
Me sentindo tão
        G7
Sozinho

     C              Em         Am
No corpo o sabor amargo do ciume
         C7                    F
A gente quando não se assume
                    G7
Fica chorando sem carinho

[Refrão]

          C                 C
O tempo passou e eu sofri calado
              F
Não deu pra tirar ela do pensamento
        Dm                  G7
Eu ia dizer que estava apaixonado
             C              G7
Recebi o convite do seu casamento
           C                    C
Com letras douradas num papel bonito
           F                      Fm6
Chorei de emoção quando acabei de ler
                            C
Num cantinho rabiscado no verso
                        G7
Ela disse meu amor eu confesso
        F                    G7
Estou casando mais o grande amor
          C       G7
Da minha vida é você

          C                 C
O tempo passou e eu sofri calado
              F
Não deu pra tirar ela do pensamento
        Dm                  G7
Eu ia dizer que estava apaixonado
             C              G7
Recebi o convite do seu casamento
           C                    C
Com letras douradas num papel bonito
           F                      Fm6
Chorei de emoção quando acabei de ler
                            C
Num cantinho rabiscado no verso
                        G7
Ela disse meu amor eu confesso
        F                    G7
Estou casando mais o grande amor
          C       G7
Da minha vida é você

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F/E = 0 X X 2 1 1
Fm6 = 1 X 0 1 1 X
G7 = 3 5 3 4 3 3
