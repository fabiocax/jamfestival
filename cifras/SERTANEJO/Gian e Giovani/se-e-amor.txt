Gian e Giovani - Se É Amor

(intro)  C   G   C   G

E|----------------------------------------------|
B|---5-5--------5-5--6--8-----------------------|
G|-------5--7--------------7--------------------|
D|----------------------------------------------|
A|----------------------------------------------|
E|----------------------------------------------|

G                  Dm
Vez em quando telefona
G                     C
pra saber se estou sozinho
Am                  Dm
Sente falta do meu beijo
                   G
pra matar o meu desejo
                     C
quando quer o meu carinho

               Dm
E eu te quero tanto

             G/B
me diz até quando
                C
vai ser do seu jeito

               Dm
vem, me ama e vai
                 G/B
veste a roupa e sai
               C      G
mesmo assim te aceito

(refrão 2x)
         C                         Dm
Se é amor, se é paixão, não entendi
                          G
me diz olhando nos meus olhos
                  C           G
que me ama e me adora e me devora
       C                           Dm
Se é amor, se é paixão, não sei dizer
                               G
só sei que o amor que a gente faz
                         C     G
é bom demais, não vai embora.

(solo)  C   G   Dm   G    C

               Dm
E eu te quero tanto
             G/B
me diz até quando
                C
vai ser do seu jeito

               Dm
vem, me ama e vai
                 G/B
veste a roupa e sai
               C      G
mesmo assim te aceito

(refrão 2x)
         C                         Dm
|Se é amor, se é paixão, não entendi

me diz olhando nos meus olhos
                  C           G
que me ama e me adora e me devora
        C                           Dm
Se é amor, se é paixão, não sei dizer
                               G
só sei que o amor que a gente faz
                         C
é bom demais, não vai embora.

(final)
 Dm  G                    C    G   C
              não vai embora.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
