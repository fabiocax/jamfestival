Gian e Giovani - Eu Quero Te Amar

Am
Quando seu coração
      Dm
sente falta de mim
G
Quando a solidão
       C
no seu peito explodir
        F
Não se acanhe, telefone
       Dm
que eu vou te atender
       G
Com certeza vou falar
          E
que ainda gosto de você
Am
Quando você lembrar
   Dm
do amor que agente fez
     G
E se isso te fizer voltar

        E             A E
conta comigo outra vez
   A                E                 F#m
Eu nunca consegui tirar você da minha vida
    Bm               E                     A  E
Fiz tudo pra te esquecer, mais não achei saída
   A                 E                   F#m
Eu ando tão carente, eu preciso do teu calor
    Bm                     E                 A
Vem logo pros meus braços, vem me encher de amor
            F#m            Bm
Eu quero te amar, eu quero ser
  E         A
Feliz com você
            F#m            Bm
Eu quero te amar, eu quero ser
  E         A
Feliz com você...

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
