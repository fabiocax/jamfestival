Gian e Giovani - Eu Dou a Cara Pra Bater


Intro.:
 F        Bb       C            Am       Dm          Gm       C       F  Bb//
e|--5-6-6/8-8--6--5-6----3-5-5/6-6--5--3--1----1-3-3/5-5---3---1---0---1--
B|------------------------------------------------------------------------
G|------------------------------------------------------------------------

F                                    Bb     C
Uma carta de amor pra falar dos teus olhos
                                   F   Bb C   F
Um coração desenhado o meu nome e o dela
                                    Bb    C
Uma vontade sem fim de ficar do seu lado.
                                       F    Dm
Não sou mais dono de mim,  só vivo pra ela.

        Bb        C
É algo fora do normal.
           Am           Dm
Eu nunca amei ninguém igual.
            Gm           Bb            C
Deus não me livre nunca, nunca desse amor. Uouou!


(REFRÃO)
F                                        Dm
   Eu dou a cara pra bater por esse amor.
                                       Gm
Eu ponho a mão no fogo enfrento a razão.
                                        Bb
Ninguem no mundo vai saber me amar assim.
    C          F
É amor , é paixão.

Sax     Bb         C         Am         Dm         Gm        C           F  Dm 
e|--5-6-8-8--6-6--5-6----3-5-6-6--5-5--3-5----5-6-8-8--6-6--5-5--3--1--0--1--
B|---------------------------------------------------------------------------
G|---------------------------------------------------------------------------

       Bb          C
É algo fora do normal.
          Am            Dm
Eu nunca amei ninguém igual.
           Gm            Bb            C
Deus não me livre nunca, nunca desse amor. Uouou!

Refrão 2x

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
