Gian e Giovani - Meus direitos

Em, A7, D7+, Em, F#

       Bm
PRA VIVER ASSIM
                                     Em
E BEM MELHOR VOCE VOLTAR AOS BRACOS DELE
         A7
VOCE ME FERE ASSIM
                                 D7+
AO ME DIZER QUE NAO SOU IGUAL A ELE
       G
VOCE QUER DE MIM
                       F#
UMA PESSOA QUE EU NAO POSSO SER
          F#7
POIS CADA UM TEM SEU JEITO DE VIVER
                  Bm  F#7
SUA MANEIRA DE QUERER
       Bm
PRA VIVER ASSIM
                                  Em
E BEM MELHOR EU PROCURAR OUTRA PESSOA

       A7
VOCE AGINDO ASSIM
                                D7+
ME MALTRATA, ME HUMILHA E ME MAGOA
          G
QUER COMPREENSAO
                                     F#
MAS NAO RESPEITA OS DIREITOS DO MEU CORACAO
       F#7
VOCE ACUSA QUE EU NAO LHE FIZ FELIZ
                     Bm  B7
NAO RECONHECE O QUE FIZ
Em       A7        D7+      G7+
TENTE MUDAR O SEU JEITO DE SER
Em          F#            F#7  Bm  F#
TENTE ENCONTRAR DENTRO DE SI VOCE
Em       A7        D7+       G7+
TENTE MUDAR O SEU JEITO DE SER
Em          F#            F#7   Bm  F#
TENTE ENCONTRAR DENTRO DE SI VOCE

       Bm
PRA SOFRER ASSIM E BEM MELHOR
    Em         F#            F#7   G  Em  Bm
   TENTE ENCONTRAR DENTRO DE SI VOCE

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
D7+ = X X 0 2 2 2
Em = 0 2 2 0 0 0
F# = 2 4 4 3 2 2
F#7 = 2 4 2 3 2 2
G = 3 2 0 0 0 3
G7+ = 3 X 4 4 3 X
