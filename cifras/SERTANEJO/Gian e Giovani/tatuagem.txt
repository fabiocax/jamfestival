Gian e Giovani - Tatuagem

Intro: (G, C/G, G, C/G)

         G
Quanto mais eu penso o tempo passa
                      C9
Mais aumenta essa vontade de estar com você
     G                                                    C9
Eu disfarço finjo estar contente, mais tá mais no que na cara que eu quero te ver
     G                               C9
De repente o que era brincadeira, mudou o brilho do meu olhar
      G                           C9
Nosso caso esta ficando sério, mistério bom pra desvendar
   Am                             Em
Morrendo de saudade, eu pego o telefone pra te ligar
     Am                                           D
E te dizer o que eu sinto agora, não dá pra controlar
     G
Ta dificil sem o teu carinho,
                               C9
Da a impressão que eu estou sozinho em plena multidão
       G                                            C9
Gosto tanto quando escondidinho, você vem me dar um beijo e eu pego a sua mão

      G                               C9
Justo eu que nunca imaginava, que um dia fosse me envolver
        G                              C9
Nessa história de amor proibido, quem sofre sou eu e você
    Am                             Em
Mas vê se dá um jeito, e joga logo fora essa solidão
     Am                                            D
E mostrar o amor que está guardado, dentro do coração
C9         G       C9      D
Há coração, é saudade demais
C9       G              C9         D
Me enfeitiçou,  me deu tanto prazer
C9     G          C9        D
Há coração, to sofrendo demais
  C9      G             C9         D
Só fico bem, quando estou com você
           Am          D            Em
Gravei seu nome com as letras da paixão
     Am           D         G    (C)
Fiz tatuagem de amor no coração
(Solo.: D, C, G, G4, G,   C,   D, C, G)
      G
Quanto mais eu penso o tempo passa
                     C9
Mais aumenta essa vontade de estar com você
       G                                                  C9
Eu disfarço finjo estar contente, mais tá mais no que na cara que eu quero te ver
     G                               C9
De repente o que era brincadeira, mudou o brilho do meu olhar
       G                          C9
Nosso caso esta ficando sério, mistério bom pra desvendar
  Am                                Em
Morrendo de saudade, eu pego o telefone pra te ligar
       Am                                         D
E te dizer o que eu sinto agora, não dá pra controlar
C9         G       C9      D
Há coração, é saudade demais
C9       G              C9         D
Me enfeitiçou,  me deu tanto prazer
C9     G          C9        D
Há coração, to sofrendo demais
  C9      G             C9        D
Só fico bem, quando estou com você
          Am           D            Em
Gravei seu nome com as letras da paixão
 Am     C9       D           G   G4, G
Fiz tatuagem de amor no coração

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C/G = 3 3 2 X 1 X
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G4 = 3 5 5 5 3 3
