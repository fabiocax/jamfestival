Gian e Giovani - Hey Hey Trem Bão

[Intro]  B7  E  B7  E  A  E  B7  E

            E
Olha a mão boba, escorrega o sabonete
                                             B7
Vou pegar ela no brete pra mostrar que sou peão
Pele macia, cheirosa, ensaboada
                                             E
Deixo essa mulher domada com meu laço de paixão

Ajeita a sela, é boa de montaria
                      E7
Dia e noite, noite e dia
                    A
É só isso que ela quer
                                     E
Abra a porteira, passa boi, passa boiada
                B7                     E   E7
Haja cerveja gelada pra domar essa mulher
           A                         E
Abra a porteira, passa boi, passa boiada

                B7                     E
Haja cerveja gelada pra domar essa mulher

                B7                   E
Hey! Hey! Trem bão, pula pula no chuveiro
                   B7            A            E
Conversa no travesseiro e funga funga no colchão
                B7                         E
Hey! Hey! Trem bão, vai de frente, vem de costas
                      B7                        E
Se tem gente que não gosta, não serve pra ser peão

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
