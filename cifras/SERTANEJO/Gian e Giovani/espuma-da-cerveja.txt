Gian e Giovani - Espuma da Cerveja

Introdução: G#  D#  G#  G#7  C#  D#  G#  C#  D#

G#              D#        G#
Na boate era quase meia noite
                                A#m
Eu fui beber para esquecer quem me esqueceu
                                    D#
Quando vi sentada em uma mesa uma mulher
                          G#    D#
Com um problema igual ao meu
          G#        D#                  G#
Ao meu convite ela sentou-se em minha mesa
           G#7                    C#
Uma cerveja foi o brinde em nossa dor
                                 G#
E a espuma que corria em nossos copos
           D#                       G#
Um velho amor cedeu lugar á um novo amor

G#              D#                      C#  G#
Espuma da cerveja, vai acabando aos poucos

             D#                   C#  G#
Espuma da cerveja, quase me deixa louco

(intro)

G#              D#        G#
Porém o dia amanheceu ela se foi
                                A#m
Para um lado para outro não se viu
                              D#
Nunca mais ela quis saber de mim
                                G#   D#
Notícias dela nunca mais eu consegui
        G#          D#        G#
Queria tanto encontrá-la novamente
              G#7                    C#
Mas não consigo descobrir onde ela esteja
                                    G#
O nosso amor que nasceu entre dois copos
           D#                        G#
Morreu depressa como a espuma da cerveja

 G#             D#                      C#  G#
Espuma da cerveja, vai acabando aos poucos
             D#                   C#  G#
Espuma da cerveja, quase me deixa louco

----------------- Acordes -----------------
A#m = X 1 3 3 2 1
C# = X 4 6 6 6 4
D# = X 6 5 3 4 3
G# = 4 3 1 1 1 4
G#7 = 4 6 4 5 4 4
