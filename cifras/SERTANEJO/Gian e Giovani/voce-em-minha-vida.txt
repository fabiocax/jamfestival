Gian e Giovani - Você Em Minha Vida

          E                  B7
Quando você chegou em minha vida
               A E
Tudo era diferente.
 E                  B7
Não falava em despedida
                  A E E7
E me amava loucamente.
         A
Hoje porém tristemente vejo
                        E
O nosso amor se desfazendo
                        B7
Em cada abraço em cada beijo
      A         B7          E
Eu sinto que estou lhe perdendo.

(refrão)
                    B7
O que foi que aconteceu?
                    E
Porque me tratar assim?

                       B7
Não me diga que é o adeus
         A           E
Eu não quero nosso fim.
                   B7
Não se entrega a ilusão
                     A E E7
Que domina os seus passos
                A
Ouça a voz do coração
                 B7
E sufoque esta paixão
                   A E
No aconchego dos meus braços.

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
