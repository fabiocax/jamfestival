Gilberto e Gilmar - Vai Meu Carro Velho, Vai

Intro: C  G7  C  G7  C

C                    G7     C
Lá vai o carro, com seu carreiro
                                  G7
Deixando rastros neste solo brasileiro meu

Cante uma música, velho cocão
                               C
Para que todos lembrem sua tradição
                G7         C
Minha boiada, deixe seu rastro
                                 F
Puxando forte o velho carro com seu casco sim
                                    C
Não é preciso usar ferrão porque ferir seu coração
           D            G7         C
Ouço seus passos no compasso do cocão
 G7            C
Vai meu carro velho, vai
       F          G7            C
Você e eu somos mesmo quase iguais

 G7             C
Porque, tudo mudou eu sei
            F          G7               C
Choro a saudade do que foi e não volta mais.

Intro: C  F  G7  C  F  G7  C

          C      G7         C
Serras e vales, meu carro vai
                               G7
Atravessando entre campos e revoadas de pardais

Meu carro cante pra mim uma canção
                                     C
Que seu carreiro tem no peito um coração que dói
                C
A noite vai, o dia vem
                              F
De longe escuta o rangido de cocão no além
                             C
Cantarolando este homem se agita
                       D        G7          C
Suspendendo o braço grita, viva o nosso sertão

 G7            C
Vai meu carro velho, vai
       F         G7            C
Você e eu somos mesmo quase iguais
  G7           C
Porque, tudo mudou eu sei
            F          G7                C
Choro a saudade do que foi e não volta mais
G7            C
Vai meu carro velho, vai
       F         G7            C
Você e eu somos mesmo quase iguais
  G7           C
Porque, tudo mudou eu sei
            F          G7                C
Choro a saudade do que foi e não volta mais
            F           G7               C
Choro a saudade do que foi e não volta mais

            F                  Fm     c
Choro a saudade do que foi não volta mais.

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G7 = 3 5 3 4 3 3
