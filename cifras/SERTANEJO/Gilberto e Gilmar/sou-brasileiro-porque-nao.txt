Gilberto e Gilmar - Sou Brasileiro, Porque não?

Introdução: D A7 D D7 G A7 D

D                                 G
Sou brasileiro, estou mais sossegado
                 A7                       D
Tudo vai ser mudado e o brasil vai melhorar
                                     A7
Vou pro trabalho todo dia mais contente
                                    D
Hoje felizmente pra frente vamos andar.
           Am
Até o dinheiro das minha economias
                D7                      G
Que há tempo eu não mexia tirei ele de lá
                                     D
Comprei um carro que há naos eu queria
             E7                     A7
Mas tinha medo que um dia fosse faltar

D                           G
Sou brasileiro, sou sim senhor

           A7              D
Se me Chamarem diga que estou
                             G
Sou brasileiro, sou sim senhor
           A7              D    Introdução
Se me chamarem diga que estou.

D                             G
Eu vou vivendo agora mais feliz
                 A7                    D
Com orgulho do país onde moro e vou ficar
                           A7
O meu amor que semrpe me dizia
                                  D
Cinco anos de namoro e nada de casar.
            Am
Eu explicava que a situação não dava
                 D7                   G
Mas ela me intimava falando em me deixar
                               D
Mas agora os bons tempos voltaram
                  E7                 A7
As coisas melhoraram e com ela vou morar.

Refrão......

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
G = 3 2 0 0 0 3
