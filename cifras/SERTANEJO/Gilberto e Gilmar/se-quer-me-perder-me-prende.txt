Gilberto e Gilmar - Se Quer Me Perder Me Prende

[Intro]  C  G  D  G  C  G  D  G

   G                    C
As águas não olham pra trás
    D                 G
E o vento segue a direção
    G              C
Saudade quando é demais
  D                G
Estraga qualquer coração
   G            C
A lua é dos namorados
     D             Em
E o sol adora quem bronzeia
    C                     G
Me ajude a procurar minha lágrima
    D                  G
Que eu deixei cair na areia
G                    C
Abraço que me amarrava
D                 G
Soltou o nó faz muito tempo

G                        C
Te guardo inteiro no meu peito
     D                 G
Mas se te perco não aguento
G                       C
Os pés tem os dedos pra frente
      D                  Em
Que é pro futuro não ter volta
C                     G
Se quer me perder me prende
D                     G
Se quer me segurar me solta
C                     G
Se quer me perder me prende
D                     G
Se quer me segurar me solta
G                 C
A chuva enche a represa
D                G
O amor enche o coração
G                 C
A vida cabe em um ovo
D              G
O Ovo cabe em um pão
G              C
O cálice não toma vinho
     D              Em
Mais dignifica seu sabor
    C                     G
Pra quê que eu quero dicionário
              D                 G
Se com quatro letras eu escrevo amor
    C                     G
Pra quê que eu quero dicionário
              D                 C      G
Se com quatro letras eu escrevo amor

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
