Diego e Victor Hugo - Santo Antonio

          C
Olha pra mim
                            F
Eu tô de novo juntando os cacos
                 Am
Colecionando fracassos
                  G
Tentando ficar de pé

         C
E dessa vez eu não vou dizer nada
         F
Minhas paixões já viraram piadas
  Am
É que no amor eu sou uma tragédia
  G
Se a minha vida fosse um filme
Seria uma comédia
F
Melhor nem falar, deixa quieto
      G
Pode parar no primeiro boteco


                         Dm
E quando eu penso agora vai
A pessoa só me tapeia
      F
Será que Santo Antônio me odeia?
        Am
Só eu gostar que a pessoa me esnoba
       G
Bora beber que amar tá foda

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
