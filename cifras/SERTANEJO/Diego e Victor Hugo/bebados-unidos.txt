Diego e Victor Hugo - Bêbados Unidos

F#
Não julgue aquele cara ali
                   C#
De chinelo e sem camisa

Jogado na calçada
             D#m
Com a cara batida

Ele não tem culpa
    B
Ele é mais uma vítima de uma
C#           F#
Ali naquele lugar

Onde se vende de tudo
                  C#
E a solidão é de graça

Ai, ai
                       D#m
E o povo bebe mais que água

                        B
É muito coração quebrado
                C#   A#
Por metro quadrado

                  D#m
Já to parando o carro

Vou lá beber com ele
                  B
Eu nem conheço o cara

Mas já passei por isso
          F#
Bêbados unidos

Jamais serão vencidos
                  C#
E o que ela fez com ele
                A#m
A minha fez comigo
                  D#m
Eu to parando o carro

Vou lá beber com ele
                  B
Eu nem conheço o cara

Mas já passei por isso
          F#
Bêbados unidos

Jamais serão vencidos
                   C#
E o que ela fez com ele
A#m           D#m
A minha fez comigo

----------------- Acordes -----------------
A# = X 1 3 3 3 1
A#m = X 1 3 3 2 1
B = X 2 4 4 4 2
C# = X 4 6 6 6 4
D#m = X X 1 3 4 2
F# = 2 4 4 3 2 2
