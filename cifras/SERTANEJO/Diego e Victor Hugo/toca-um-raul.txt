Diego e Victor Hugo - Toca Um Raul

G                         D
Beber sem saudade é uma coisa
                                  A
Mas beber com alguém na cabeça é outra
Ai, ai, ai

G                          D
Sofrer no silêncio é uma coisa
                                A
Mas com som da viola batendo é outra
Ai, ai, ai

G                      A
Já pedi pra tocar um Raul
                           D
Mas foi a mesma coisa que nada
                       Bm
O cantor até virou a cara
Ai, ai, ai

G                     A
Tá tocando a Boate Azul

                     Bm
Só pra me matar de raiva
Ai, ai, ai

D                    A
Essa sofrência ainda vai matar alguém
       Bm
Quando esse violão faz
G
Tchen, guen, tchen, guen, tchen, guen

D                 A
O coração na hora lembra de alguém
       Bm
Quando esse violão faz
G
Tchen, guen, tchen, guen, tchen, guen

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
G = 3 2 0 0 0 3
