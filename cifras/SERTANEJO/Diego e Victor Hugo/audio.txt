Diego e Victor Hugo - Áudio

B                    F#
Eu tô gravando esse áudio
                     G#m
Pra dizer que não dá mais
                    D#m    E
E a gente vai terminar de vez
                           F#
Espera, vou começar outra vez
   B                F#
Eu tô gravando esse áudio
                      G#m
Pra dizer que não dá mais
                  D#m
Você anda muito ausente
                  E
Eu já tô ficando louco
                        F#7
Pera aí, vou começar de novo
   B                 F#
Eu tô gravando esse aúdio
                      G#m
Pra dizer que não dá mais

                      D#m     E
Pra aguentar essa saudade de você
                               F#  F#7
Pra entender o quanto ainda te quero
   B                   F#
Eu tô jogando orgulho fora
                 G#m
Chego aí em meia hora
                     D#m
Pra acabar com esse clima
     E
Você vai ver
           F#
Eu tô falando sério
         F#7         B
Áudio enviado com sucesso

----------------- Acordes -----------------
B = X 2 4 4 4 2
D#m = X X 1 3 4 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
F#7 = 2 4 2 3 2 2
G#m = 4 6 6 4 4 4
