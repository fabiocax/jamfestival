Gusttavo Lima - Esse Amor Que Me Mata

Intro: D  E  C#m  F#m  Bm  E  A  D  A

       A                 A9
Estou sozinho e você tão longe
      A                 A7
Sabe deus onde você se esconde
D                A
Onde é que você está?
      A                   A9
O coração pede que eu te esqueça
         A              A9
Mas você vive na minha cabeça
D                    A
Com quem é que você está?

    F#m                            C#m
Telefona mande um aviso me dê um sinal
                             D
Alguma notícia um cartão postal
                                 A   E7
Pra ver se eu engano a minha paixão

       F#m                               C#m
Dê um grito quem sabe o vento me traga você
                                 D
Em meus pensamentos eu possa te ver
             E                 A  (A G#m F#m E)
E ouvir as batidas do seu coração.

E                                      A
Quando o amor vai embora a saudade devora
E                                   A
A solidão é demais quando é fora de hora
C#                  C#7
Quebra o silêcio e volta pra mim
         F#m               D9
?stou sentindo demais sua falta
    A             E                   A
Ou tira o meu coração esse amor que me mata.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
A9 = X 0 2 2 0 0
Bm = X 2 4 4 3 2
C# = X 4 6 6 6 4
C#7 = X 4 3 4 2 X
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
D9 = X X 0 2 3 0
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
