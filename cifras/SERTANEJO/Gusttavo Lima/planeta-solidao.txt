Gusttavo Lima - Planeta Solidão

   F              C9      Dm         Bb
E|--------5---4---2-------3------------------|
B|--7--------------------4--7-7-5-5---5-3--1-|
G|--------------------5-5-----------3--------|
D|-------------------------------------------|
A|-------------------------------------------|
E|-------------------------------------------|

F                       C9
É, se Deus me ouvisse agora
       Dm                   Bb
Eu pediria você, mais uma vez
  F                               C9
Se soubesse o quanto o meu peito chora
      Dm                  Bb
Entenderia e voltava talvez
      Dm                 C
Pro meu coração, não diga que não
F                 C9
É, se parasse o tempo
       Dm                      Bb
Eu morreria com você no pensamento

 Dm
Não suportaria a dor
 C
Que você me deixou
F                                         C
É, estou pagando os meus pecados por te amar demais
                                 Dm
Estou cumprindo penitencia me devolva a paz
          Bb         C
Pro meu coração, não diga que não
F                                         C
É, nem me conheço, às vezes nem sei quem sou
                                      Dm
Se me perguntam de onde vim, sei a onde estou
             Bb                   C
Planeta solidão marcou o meu coração, de dor
.
solo base ( F C Dm Bb F C Bb )

E|-1--4--------0------0--1---4--6-|
B|--2---2-----1-----1--1---2------|
G|----------0-----0---------------|
D|---------------2----------------|
A|--------3-----------------------|
E|--------------------------------|

F                 C9
É, se parasse o tempo
       Dm                      Bb
Eu morreria com você no pensamento
 Dm
Não suportaria a dor
 C
Que você me deixou
F                                         C
É, estou pagando os meus pecados por te amar demais
                                 Dm
Estou cumprindo penitencia me devolva a paz
          Bb         C
Pro meu coração, não diga que não
F                                         C
É, nem me conheço, às vezes nem sei quem sou
                                      Dm
Se me perguntam de onde vim, sei a onde estou
             Bb                   C         F
Planeta solidão marcou o meu coração, de dor, dedor


solo base ( F C Dm Bb F C Bb )

E|-1--4--------0------0--1---4--6-|
B|--2---2-----1-----1--1---2------|
G|----------0-----0---------------|
D|---------------2----------------|
A|--------3-----------------------|
E|--------------------------------|

De dor

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
