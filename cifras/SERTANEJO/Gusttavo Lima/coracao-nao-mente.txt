Gusttavo Lima - Coração Não Mente

F#m           D            A          E
No silêncio da noite eu escuto a sua voz
F#m                 D              A              E
Eu to sozinho nesse quarto, mas eu to pensando em nós
Bm                                            F#m
Aonde é que eu tava com a cabeça, quando eu te machuquei?
                      A                          E
Quando eu te fiz chorar, eu não sabia que eu te amava.
Bm                                             F#m
Quando eu te vi com outro o meu coração estremeceu.
                                A
Você passou por mim, não reconheceu
                              E
Mas vou lutar pra ter você de volta.

REFRÃO
F#m
Dá num fora nele, vem ficar comigo
     D
E eu posso até jurar não vou mais ser bandido
A
E dessa vez vai ser diferente

E
Eu posso até mentir...
C#7
Mas coração não mente.

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C#7 = X 4 3 4 2 X
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
