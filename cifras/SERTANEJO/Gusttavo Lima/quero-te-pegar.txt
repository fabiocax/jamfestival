Gusttavo Lima - Quero Te Pegar

B
Quero te pegar
                F#
E vai ser ham ham
G#m
Aquele amor gostoso
              E
Que eu sei que você gosta
 B
Deixa eu te pegar
               F#
E vai ser hum hum
  G#m                   E
Eu e você a vida inteira
                     B  F# G#m E
Pode fazer sua aposta

( B  F# G#m E )

B                             F#
Eu estremeço todo quando te vejo

     G#m          E
Seu beijo é só prazer
 B                         F#
Garota eu tô querendo tanto te dizer
       G#m                  E
Que eu tô apaixonado por você

C#m           G#m        E               F#
Eu e você bem juntinho vai ser bom demais
C#m         G#m        E             F#
Se você colar comigo não esquece mais
C#m                      E      F#
Dá só uma chance, vai rolar o ai, ai, ai

(Refrão)

----------------- Acordes -----------------
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
