Gusttavo Lima - Página de Amigos

Solo: D  A  G  D  A

E|----------------------------------------------------------------------|
B|-8/10--8/10-8-7-7-8---------------------------------7-7p8-7-----------|
G|--------------------9--9-9/11-9/11-11/9-7-----6-7-9---------9---------|
D|------------------------------------------7-9-------------------------|
A|----------------------------------------------------------------------|
E|----------------------------------------------------------------------|

D
Ela ligou terminando
A
Tudo entre eu é ela
G                              D  A
E disse que encontrou outra pessoa
D
Ela jogou os meus sonhos
A
Todos pela janela
G                                        D A
E me pediu pra entender encarar numa boa


 D
Como se meu coração
               A
Fosse feito de aço

Pediu pra esquecer os beijos
       G
E abraços

E pra machucar
                 D A
Ainda brincou comigo
D
Disse em poucas palavras
              A
Por favor, entenda

O seu nome vai ficar na
        G
Minha agenda
    A      D    A
Na página de amigos
     D
Como é que eu posso ser
             A
Amigo de alguém que eu tanto amei
    G
Se ainda existe aqui comigo
      D            A
Tudo dela e eu não sei
     D
Não sei o que vou fazer
             D7          G
pra continuar minha vida assim

        D
Se o amor que morreu dentro  (2x)
 A                  D
Dela ainda vive em mim

Solo: D  A  G  D  A

E|------------------------------------------------------------------|
B|-8/10--8/10-8-7-7-8---------------------------------7-7p8-7-------|
G|--------------------9--9-9/11-9/11-11/9-7-----6-7-9---------9-----|
D|------------------------------------------7-9---------------------|
A|------------------------------------------------------------------|
E|------------------------------------------------------------------|

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
