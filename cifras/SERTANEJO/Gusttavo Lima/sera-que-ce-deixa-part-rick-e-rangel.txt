Gusttavo Lima - Será Que Cê Deixa (part. Rick e Rangel)

  D                G
Você sorri com os olhos
                Em
E eu abro um sorriso
             A
Pra te ver sorrir
   D                   G
Quando você fecha os olhos
                Em
Eu te roubo um beijo
            A
E devolvo tudim
 Em                 G
Você me prende e o pior
                         D
É que eu me sinto mais livre
              A
Amarrado em você
   Em               G
Risquei a palavra ciúmes
             D
Do meu dicionário

               A
Eu mudei deu pra ver
     G    A            D  A
Me esforço pra te merecer
               D
Será que cê deixa
      C9                  G
Eu cuidar da sua vida e morar

Na sua casa
              D
Será que cê deixa
     C9
Eu deitar no seu colo
     G
No chão no tapete da sala
               D
Será que cê deixa
       C9
Eu ser sua opção
      G
Numa noite de sexta
               D     C9   G
Será que cê deixa
                D
Eu sei que cê deixa

----------------- Acordes -----------------
A = X 0 2 2 2 0
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
