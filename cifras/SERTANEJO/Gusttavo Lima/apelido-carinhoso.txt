Gusttavo Lima - Apelido Carinhoso

[Intro]  Dm  F  C  G

[Primeira parte]

Am                              F9
   Amor, não é segredo entre a gente
                          G
Que o meu término é recente
                                   Am
E você tá arrumando o que ela revirou

Am                          F9
   E esse sentimento pendente
                                 G
Que insiste em bagunçar a minha mente
                                     Am
Vai passar um dia, mas ainda não passou

[Primeira Parte – Dedilhado Primeira Estrofe]

Parte 1 de 4

   Am
E|------------------------------------------|
B|-------1-----1-----1-----1p0--------------|
G|-----2---2-----2-----2-------2-0----------|
D|---2-------2-----2-----2------------------|
A|-0----------------------------------------|
E|------------------------------------------|

Parte 2 de 4
   F9
E|-------3-----3-----3-----3----------------|
B|-----1---1-----1-----1-----1--------------|
G|---2-------2-----2-----2-----2------------|
D|-3----------------------------------------|
A|------------------------------------------|
E|------------------------------------------|

Parte 3 de 4
   G
E|------------------------------------------|
B|-----------------3-8~---------------------|
G|-----------0-2/4--------------------------|
D|---------0--------------------------------|
A|-0---0h2----------------------------------|
E|---3--------------------------------------|

Parte 4 de 4
   Am
E|-8-----7-----5----------------------------|
B|-5-5-----5-----5---8------5h6p5--3--------|
G|-5---5-----5-----5---5--------------------|
D|-7---------------------7--5h7p5--3--------|
A|------------------------------------------|
E|------------------------------------------|

[Primeira Parte – Dedilhado Segunda Estrofe]

Parte 1 de 4
   Am
E|------------------------------------------|
B|-1-----1------1------1----1p0-------------|
G|-2--2-----2------2-----2------2-0---------|
D|-2----2-----2------2-----2----------------|
A|-0----------------------------------------|
E|------------------------------------------|

Parte 2 de 4
   F9
E|-------3-----3-----3----------------------|
B|-----1---1-----1-----1--------------------|
G|---2-------2-----2-----2-2p0--------------|
D|-3---------------------------3-2----------|
A|------------------------------------------|
E|------------------------------------------|

Parte 3 de 4
   G
E|------------------------------------------|
B|-------3-----3-----3-----3/5\3--1p0-------|
G|-----0---0-----0-----0--------------------|
D|---0-------0-----0-----0------------------|
A|------------------------------------------|
E|-3----------------------------------------|

Parte 4
   Am
E|--------------------0---------------------|
B|-------1-----1------1---------------------|
G|-----2---2-----2----2---------------------|
D|---2-------2--------2---------------------|
A|-0------------------0---------------------|
E|------------------------------------------|

[Pré-refrão]

Dm
   Eu sei que você poderia ter
      F
Escolhido alguém menos complicado
C
  Que não tivesse no presente
        G
Uma pessoa do passado

Dm
   Aceitar essa situação
       F
É uma forma de amor
C
  Mas eu preciso que você
         G
Me faça só mais um favor

[Refrão]

Dm
   Ainda não me chame de meu nego
F                          C
  Ainda não me chame de bebê

Porque era assim que ela me chamava
G
  E um apelido carinhoso
                          Dm
É o mais difícil de esquecer

                           C/E  F
Ainda não me chame de meu nego
                         C
Ainda não me chame de bebê

Porque era assim que ela me chamava
G
  E um apelido carinhoso
                        Dm
É o mais difícil de esquecer

(Dm  F  C  G)

[Pré-refrão]

Dm
   Eu sei que você poderia ter
      F
Escolhido alguém menos complicado
C
  Que não tivesse no presente
        G
Uma pessoa do passado

Dm
   Aceitar essa situação
       F
É uma forma de amor
C
  Mas eu preciso que você
         G
Me faça só mais um favor

[Refrão Final]

Dm
   Ainda não me chame de meu nego
F                          C
  Ainda não me chame de bebê

Porque era assim que ela me chamava
G
  E um apelido carinhoso
                          Dm
É o mais difícil de esquecer

                           C/E  F
Ainda não me chame de meu nego
                         C
Ainda não me chame de bebê

Porque era assim que ela me chamava
G
  E um apelido carinhoso
                        Dm
É o mais difícil de esquecer

   Ainda não me chame de meu nego
F                          C
  Ainda não me chame de bebê

Porque era assim que ela me chamava
G
  E um apelido carinhoso
                          Dm
É o mais difícil de esquecer

                           C/E  F
Ainda não me chame de meu nego
                         C
Ainda não me chame de bebê

Porque era assim que ela me chamava
G
  E um apelido carinhoso
                        Dm7
É o mais difícil de esquecer

[Frase Final]
   Dm7                      F
E|-5------------------------1---------------|
B|-6-5-6-5-6-5-6-5-6-5-6-5--1---------------|
G|-5-5-5-5-5-5-5-5-5-5-5-5--2---------------|
D|-7-5-7-5-7-5-7-5-7-5-7-5--3---------------|
A|-5------------------------3---------------|
E|--------------------------1---------------|

----------------- Acordes -----------------
Am*  = X 0 2 2 1 0 - (*Gm na forma de Am)
C*  = X 3 2 0 1 0 - (*A# na forma de C)
C/E*  = X X 2 0 1 0 - (*A#/D na forma de C/E)
Dm*  = X X 0 2 3 1 - (*Cm na forma de Dm)
Dm7*  = X 5 7 5 6 5 - (*Cm7 na forma de Dm7)
F*  = 1 3 3 2 1 1 - (*D# na forma de F)
F9*  = X X 3 2 1 3 - (*D#9 na forma de F9)
G*  = 3 2 0 0 3 3 - (*F na forma de G)
