Gusttavo Lima - Aceita Ou Não Aceita

Intro: G

G
Eu sou como vidro, Se você deixar cair
    D
Eu posso te cortar Você pode decidir
   C9                                           G     D
Então não brinque sério Com um sentimento verdadeiro.
G
Flores que sempre nascem Num canteiro de um jardim
    D
Folhas que sempre secam Isso é fato e ponho um fim
     C9                                  G
Nas mãos de quem semeia O amor que incendeia.
  Am                   C            G      D
Feito tempestade Esse amor chegou até você
  Am                         C         D
É tu que me invade E só vai judiar nosso querer.

Refrão:
             G
Se você me quer Então preste atenção

                   Em
No que vou lhe dizer Meu amor é tão grande
              C    Am            D
Deu pra perceber Basta você aceitar
            G                                 Em
Minha condição Você ter paciência pra me aceitar
                                      C
Do jeito que eu sou Olhando em teu olhar
  Am            C      D                   G
Aceita ou não aceita agora Ou então vai embora.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
