Gusttavo Lima - Pra Ser Sincero

  E
Pra ser sincero
         A9               E
Eu não espero nada de você
          E
Também espero
A9                           B
Que você não queira nada de mim

         C#m                       G#m
Foi só um lance uma paixão sem romance
         A9                        E
Coisa de pele ou qualquer coisa assim
         C#m                      G#m
Vamos se amar antes que o dia amanheça
           A9                  B
Porque com o amanhecer esse amor vai ter fim

   E
Amanhã
          B                     C#m
Quando acordar e me ver do seu lado

               G#m                  A9
Não vá pensar você que estou apaixonado
                                 E     B
Só leve o meu cheiro grudado em você

         E
Não vou dar
              B                  C#m
Mais que o perfume preso em sua roupa
                 G#m
Então não perca tempo
            A9
Beija minha boca
             B                E
E me abraça em paz amor até o sol nascer

----------------- Acordes -----------------
A9 = X 0 2 2 0 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
G#m = 4 6 6 4 4 4
