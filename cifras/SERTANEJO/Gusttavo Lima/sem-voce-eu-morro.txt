Gusttavo Lima - Sem Você Eu Morro

(intro 2x) D  A  Bm  G

D                                             A
Não quero mais viver sozinho sem o seu carinho sem um beijo seu..
Bm                                       G
sou capaz de fazer loucuras e ir as alturas e pedir pra Deus
D                                               A
hoje eu sei que fiz tudo errado mais não precisava judiar assim

Bm                                      G
Meu peito anda machucado quero ser curado tenha dó de mim
Em                                     A
ve se entende que eu ja to doente  e meu remédio é seu amor
Bm                   G              D   A
estou desesperado  louco  apaixonado volta pra mim por favor

(refrão 2x)
D          A                Bm                G
Imploro eu te quero  te espero ter você comigo aqui de novo
D           A             Bm              G               D9  D
Choro sem você perto  pra mim tudo é deserto sem você eu morro

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D9 = X X 0 2 3 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
