Gusttavo Lima - O Velhinho (part. Paula Fernandes)

  D
Deixei meu sapatinho

     A          D
Na janela do quintal

   D
Papai noel deixou

       A          D  D7
Meu presente de natal

  G
Como é que papai noel

          D
Não se esquece de ninguém

  D            G     D
Seja rico ou seja pobre


G      A           D
O velhinho sempre vem!!

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
