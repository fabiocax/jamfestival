Gusttavo Lima - Dois Corações e Uma Historia

Intro: A D E A D E A D A

E|-5-5-4-----4-5-5-4-----------0-2-0-|
B|-------7-5---------7-5-3-2-0-2-3-2-|
G|-----------------------------2-2-2-|
D|-----------------------------2-0-2-|
A|-----------------------------0-0-0-|
E|-----------------------------0-0-0-|

                 Bm                    E
No meio da conversa, de um caso terminando
                   A A7+                   A6
Um fala o outro escuta e os olhos vão chorando
F#m           Bm                   E
A lógica de tudo é o desamor que chega
                 A  A7+                     A6
Depois que um descobre que o outro não se entrega
F#m              Bm                   E
Quem vai sair arruma as coisas, põe na mala
                 A A7+               A6
Enquanto o outro fuma um cigarro na sala

F#m          Bm                    E
O coração, palhaço, começa a bater forte
                D           D/E         A    A4/7 A
Quem fica não deseja que o outro tenha sorte
             D                      E
Longe um do outro, a vida é toda errada
                  A                  E/F#  F#
O homem não se importa com a roupa amarrotada
                Bm                  E
E a mulher em crise quantas vezes chora
                 D              E              A    F Bb
A dor de ter perdido um grande amor, que foi embora!
                    Cm                 F
Mas quando vem a volta, o homem se arruma
                  Bb  Bb7+                Bb6
Faz barba, lava o carro, se banha, se perfuma
Gm           Cm                       F
E liga pro amigo, que tanto lhe deu força
            Bb   Bb7+             Bb6
Jura, nunca mais vai perder essa moça
Gm              Cm                   F
E a mulher se abraça a mãe, diz obrigado
            Bb  Bb7+                Bb6
Põe aquela roupa que agrada o seu amado
Gm       Cm                       F
E passa a tarde toda cuidando da beleza
                 Eb        F          Bb  Bb4/7 Bb
Jantar a luz de velas e amor de sobremesa

Refrão 2x:
               Eb                F
E perto um do outro, a vida é diferente
                Bb                 F/A      G
A solidão dá espaço pro amor que estava ausente
                   Cm                F
Quem olha não tem jeito de duvidar agora
                Eb         F                       Bb   Eb F Bb
Da força da paixão que tem dois corações e uma história

                Eb         F                       Bb   Eb F Bb
Da força da paixão que tem dois corações e uma história

----------------- Acordes -----------------
A = X 0 2 2 2 0
A4/7 = X 0 2 0 3 0
A6 = 5 X 4 6 5 X
A7+ = X 0 2 1 2 0
Bb = X 1 3 3 3 1
Bb4/7 = X 1 3 1 4 1
Bb6 = X 1 3 0 3 X
Bb7+ = X 1 3 2 3 1
Bm = X 2 4 4 3 2
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D/E = X X 2 2 3 2
E = 0 2 2 1 0 0
E/F# = X X 4 4 5 4
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
F/A = 5 X 3 5 6 X
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
