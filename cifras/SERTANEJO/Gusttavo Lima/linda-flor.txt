Gusttavo Lima - Linda Flor

Tom: E

Intro 2x: E B A A

B|---5-7-9-7-----4-5-7-5-----|
G|-4-----------4---------4-6-|

B|-10-10-10-9-7---7-9-10-9-7---7-9/10-9-|
G|--------------9------------9----------|

B|---5-7-9-7-----4-5-7-5-----|
G|-4-----------4---------4-6-|

B|-10-10-10-9-7---7-9-10-9-7---7-9-5~~--|
G|--------------9------------9----------|

E                           B
 Seus olhos brilham feito estrelas
                     A
 Como o céu sobre o mar
E                   B                   A
 No vai e vem dessas águas começo a me lembrar

C#m                      B
 Dos bons momentos que passamos
                  F#m
 Nossas noites de amor
C#m                        B
 Em busca de um jardim perfeito
                         A     B
 Encontrei minha linda flor

E             B                 C#m
 Quero te prender, te ter só pra mim
               A                    E
 E buscar nos sonhos a força de um sim
             B             C#m
 Você é meu sol, a luz do luar
               A         B         E
 História de amor que só eu sei contar

Repete intro:

C#m                      B
 Dos bons momentos que passamos...

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
