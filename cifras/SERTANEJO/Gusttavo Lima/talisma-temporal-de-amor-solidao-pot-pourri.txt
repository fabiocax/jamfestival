Gusttavo Lima - Talismã / Temporal de Amor / Solidão (Pot-Pourri)

Intro: G C9 G C9

G   B7                       Em
Sabe, quanto tempo eu não te vejo.
                  C   Am                   F    D    D9
Cada vez você distante, mais eu gosto de você porque
 G  B7                     Em
Sabe, eu pensei que fosse fácil
                   C      Am
Esquecer seu jeito frágil
              F       C  G   D
De se dar sem receber só você

C               G     G7                C
Só você que me ilumina, meu pequeno talismã
                  G                      C   Am
Como é doce essa rotina de te amar toda manhã
                   Em                    C   Am
Nos momentos mais difíceis você é o meu divã
                   Em                         C
Nosso amor não tem segredo, sabe tudo de nós dois

 Am                  D
E joga fora os nossos medos
C       D          G  G7 C      D       Em
Vai saudade diz pra ela, diz pra ela aparecer
C       D          G   G  D/F# Em
Vai saudade ve se troca
                     C         D         G    D
A minha solidão por ela pra valer o meu viver


Intro: A  E7  F#m  C#m  D  A  A  B7  E7


  A                E
Chuva no telhado, vento no portão

   D       Bm             A         E7
e eu aqui      nesta solidã-----ã-ã-ão

 A                  E
Fecho a janela, tá frio o nosso quarto
  D       Bm              A    A7
e eu aqui    sem o teu abra----ço

Pré-Refrão:

 D                    E
Doido pra sentir seu cheiro
Fº                 F#m
Doido pra sentir seu gosto
 D                    E             D
Louco pra beijar seu beijo matar a saudade
        E       A
E esse meu desejo
 D               E
Vê se não demora muito
Fº          F#m
Co   ração tá re  clamando
  D                E                 D
Traga logo o seu carinho, tô aqui sozinho
      E         A  E7
Tô te esperando

             A
   Quando você chegar,
E                     F#m  D
  Tire essa roupa molhada
              A      E7
Quero ser a toalha
              A       E7
e o seu coberto---o-o-or
          A
Quando você chegar,
E                    F#m  D
   Mando a saudade sair
          A            E7
Vai trovejar  vai cair
                 D  E7
um temporal de amor
                 A
Um temporal de amor

De amor

 Intro: C G F C G
     C                                 C7                     F
Na cabeça sua imagem, no meu peito a vontade, de ser o homem dela...
       F           C                           Dm             G             C       G
Eu de volta do trabalho, vou de novo pro meu quarto, vou te amar da minha janela...

(refrão)
C                        G
Solidão, quando a luz se apaga..
              F                     C      G
Eu de novo em casa, morrendo de amor por ela...
C                      G
Solidão, que minha alma extravasa...
              F                    C      G
Não suporto a vontade de fazer amor com ela...

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C#m = X 4 6 6 5 4
C7 = X 3 2 3 1 X
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D9 = X X 0 2 3 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
Fº = X X 3 4 3 4
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
