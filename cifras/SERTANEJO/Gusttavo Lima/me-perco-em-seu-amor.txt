Gusttavo Lima - Me Perco Em Seu Amor

D                           A                   Am
No primeiro instante no primeiro olhar
                     G          Gm
Tive certeza nem vou disfarçar
                     D      A
Que o amor dos meus sonhos agora se realizou

D                           A                   Am
Nada nesse mundo vai tirar você de mim
          G             Gm
É verdade digo que sim
               D               A
Não brinco não é assim que eu quero amar

    G                   Bm                   A
Eu quero o amor mais bonito que alguém já viveu
        G              Bm         A
Que vale a pena e que seja eternamente

        D                   A
Ao seu lado é que eu sou feliz

      Bm              G
O teu beijo me faz viajar
      D             A
Me esqueço de quem sou
             G
Me perco em seu amor

D                         A                     Am
No encontro demos beijos sem cessar
                     G          Gm
Sentimos o amor veio prá ficar
               D            A
E tudo aquilo foi demais momentos de paz

D                           A                   Am
E tudo que eu tenho vivido com você
                G           Gm
Faz parte do que eu esperei
         D               A
Espero e é assim que eu quero amar

    G                   Bm                   A
Eu quero o amor mais bonito que alguém já viveu
        G              Bm         A
Que vale a pena e que seja eternamente... Prá sempre

        D                   A
Ao seu lado é que eu sou feliz
      Bm              G
O teu beijo me faz viajar
      D             A
Me esqueço de quem sou
             G
Me perco em seu amor

(refrão 2x)

(solo) C G Am F C G F

(refrão 3x)

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
