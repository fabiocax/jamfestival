Gusttavo Lima - Aliança

                    G#                 C#
    1 parte .       o sino ta tocando todo mundo esperando
                    F#                 C#
                    to aqui no altar esperando você chegar ..
                    G#                     C#
                    os padrinhos estão ali já olharam
                    C#
                    até pra min..
                    F#
                    eu confio em você você não vai me
                            C#       G#
                    deixar aqui no altar
                         C#
                    no altar ...
                    G#                 D#
                    o sino ta tocando você vai entrar de branco
                    F#
                    a aliança cor de ouro
                                                 C#
                    ta aqui dentro do meu bolso esperando você
                    G#
                    chegar chegar ..


    refrão: 2x      G#                      D#                        A#
                   vai celebrar nosso casamento a igreja está linda por
                              C#           G#     D#    G#
                   dentro esperando você chegar chegar vai celebar e vai durar para
                   D#                  A#            C#                G#
                   sempre meu amor eternamente na alegria na dor e na tristeza haha  ..

                   Intro: ( G# D# A# C# F# G# )

----------------- Acordes -----------------
A# = X 1 3 3 3 1
C# = X 4 6 6 6 4
D# = X 6 5 3 4 3
F# = 2 4 4 3 2 2
G# = 4 3 1 1 1 4
