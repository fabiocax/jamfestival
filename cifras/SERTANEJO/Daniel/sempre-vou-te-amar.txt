Daniel - Sempre Vou Te Amar

(intro 2x) G D Em C

E |-8/10\8-7-5---8/10\8---7-5/7--------------------------|
B |------------------------------------------------------|
G |-5/7-\5-4-2---5/7-\5---4-2/4--------------------------|
D |-------------------------------0-5-5-0-5-7-0----------|
A |------------------------------------------------------|
E |------------------------------------------------------|

E |-8/10\8-7-5---8/10\8---7-5/7-------------5/7-3--------|
B |------------------------------------------------------|
G |-5/7-\5-4-2---5/7-\5---4-2/4-------------2/4-0--------|
D |-------------------------------0-5-5-0-5--------------|
A |------------------------------------------------------|
E |------------------------------------------------------|

G                  D
Não somos duas crianças
               Em
Nosso mundo mudou
      C               G
O sentido e a razão

             D
O amor cresceu
               Em
E a gente perdeu
      C           G
O caminho da paixão

                  D
Você sumiu lá de casa
                Em
Não te vejo na rua
      C            G
E mudou o celular
              D
O que vou fazer
            Em
Pra você saber
           C
Que eu preciso te falar

(refrão)
   G                 D
 Ainda guardo bilhetes
              Em
 Que me escreveu
         C                 G
 Nossas fotos por todo lugar
                  D
 Ainda morro de desejo
               Em
 Por um beijo seu
        C
 Eu te amo e sempre
            G
 Vou te amar

    D     Em
Haha, haha
      C
Eu te amo e sempre
          G
Vou te amar

(repete)

    D     Em
Haha, haha
      C
Eu te amo e sempre
          G
Vou te amar

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
