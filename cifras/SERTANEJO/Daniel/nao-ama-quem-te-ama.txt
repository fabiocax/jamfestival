Daniel - Não Ama Quem Te Ama

Intro: F4   D#   F

     F4
Por amor
                                 D#                          F
Eu faço todas as vontades do seu coração
    Cm7
Por amor
                                 F7
Esqueço que você judia e te peço perdão
     D7
Por amor
                                 Gm                        Cm7
Eu me divido escondendo as minhas verdades

Nem sabe
          A#                   D#                     A#
Que por amor ainda faço as suas vontades...

    Gm
Por amor

                            Dm7
Eu tenho que disfarçar
                                      Cm7
Escondo as lágrimas no olhar
                                    F
Deixo a saudade no sufoco
    Gm
Por amor
                            Dm7
Jogo palavras ao vento
                                       Cm7
Eu nao te esqueço um momento
                           F           F4         F
Acham até que sou louco, bobo

                        F4
Meu corpo te pertence
                        F4
Minha boca meu olhar
                             Gm7
E sem você por perto não consigo respirar
                       D#                                    Cm7
Tua alma é vinho embriagando o meu desejo
                             F
E sempre amanheço procurando por seu beijo
                            F4
Mas porque não ama assim como eu te amo
                                    Gm7
E não vem correndo toda vez que chamo
                      D#                             Cm7
Sei que não existe amor igual ao meu
                          F
Você não percebeu
  D#         Cm7          F4
Não ama quem te ama

    Gm
Por amor
                            Dm7
Eu tenho que disfarçar
                                      Cm7
Escondo as lágrimas no olhar
                                    F
Deixo a saudade no sufoco
    Gm
Por amor
                            Dm7
Jogo palavras ao vento
                                       Cm7
Eu nao te esqueço um momento
                               F           F4         F
Acham até que sou louco, bobo

                        F4
Meu corpo te pertence
                        F4
Minha boca meu olhar
                             Gm7
E sem você por perto não consigo respirar
                       D#                                    Cm7
Tua alma é vinho embriagando o meu desejo
                             F
E sempre amanheço procurando por seu beijo
                            F4
Mas porque não ama assim como eu te amo
                                    Gm7
E não vem correndo toda vez que chamo
                      D#                             Cm7
Sei que não existe amor igual ao meu
                          F
Você não percebeu
  D#         Cm7          F4
Não ama quem te ama

G                       C
Meu corpo te pertence
                       C
Minha boca meu olhar
                                    Am7
E sem você por perto não consigo respirar
                       F                                    Dm7
Tua alma é vinho embriagando o meu desejo
                             G
E sempre amanheço procurando por seu beijo
                            C
Mas porque não ama assim como eu te amo
                                    Am7
E não vem correndo toda vez que chamo
                      F                             Dm7
Sei que não existe amor igual ao meu
                         G
Você não percebeu
  F         Dm7          C
Não ama quem te ama

----------------- Acordes -----------------
A# = X 1 3 3 3 1
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
Cm7 = X 3 5 3 4 3
D# = X 6 5 3 4 3
D7 = X X 0 2 1 2
Dm7 = X 5 7 5 6 5
F = 1 3 3 2 1 1
F4 = 1 3 3 3 1 1
F7 = 1 3 1 2 1 1
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
