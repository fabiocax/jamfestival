Daniel - Embriagado de Amor

Intro: F# D#m B G#m C# B/C#
F#                   B/F#                    F#   B/F# F#
Eu pensei que a minha base fosse bem mais forte
F#            B/F#                  C#/F B/C# C#
Que não cairia fácil, mas eu me enganei
B                                    F#/A#
De repente uma enxurrada de paixão surgiu
                          G#m
Minha fortaleza se abalou, caiu
             C#7                    F#      C#7
Quis brincar de amar, gostei, gostei
F#                   B/F#             F#   B/F# F#
Encontrei felicidade, encontrei a sorte
F#                     B/F#              C#/F B/D# C# B/C# C#
Fui nocauteado pelo encanto de uma flor
B                                 F#/A#
Eu pensei que fosse uma aventura a mais
                               G#m
Mas virou loucura que não se desfaz
         C#7        B/C#
Mas vejo louco de saudade

C#7        B/C#
Doido de vontade
F#          B B/C#
Ah! Eu estou
                F#
Embriagado de amor
              C#
Alucinado de paixão
                 B
Você me pegou de jeito
      C#                     F#   B B/C#
Já nem sinto os meus pés no chão
              F#
Embriagado de amor
              C#
Alucinado de paixão
                      B
Entrou de vez no meu peito
      C#               F#  B C# F#
Tomou conta do meu coração

----------------- Acordes -----------------
B = X 2 4 4 4 2
B/C# = X 6 X 4 7 7
B/D# = X 6 X 4 7 7
B/F# = X X 4 4 4 2
C# = X 4 6 6 6 4
C#/F = X 8 X 6 9 9
C#7 = X 4 3 4 2 X
D#m = X X 1 3 4 2
F# = 2 4 4 3 2 2
F#/A# = 6 X 4 6 7 X
G#m = 4 6 6 4 4 4
