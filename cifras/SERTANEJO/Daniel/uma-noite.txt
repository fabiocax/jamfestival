Daniel - Uma Noite

Intro:  D Bm7 G A7 A7/4 A7 D

D                Bm7
Noite de luar estrelas no céu
         G          A7    A7/4  A7
Parece mágica eu e você aqui
D                Bm7
Vejo o seu olhar refletindo em mim
         G                 A7      A7/4  A7
O brilho que o amor pode fazer sentir

D
Demais
          Bm7
Posso ir até o céu
        G
Em cada beijo seu
      A7         A7/4  A7
Posso tocar você,amar você
  D
É demais

         Bm7
Sentir o teu calor
           G        A7/4  A7
Não dá pra esquecer eu e você
  D
É demais

( D Bm7 G A7 A7/4 A7 D )

D                   Bm7
Quero ter você para sempre aqui
          G                       A7        A7/4  A7
Em tudo o que eu sonhar em tudo o que eu sentir
D                     Bm7
Quero o teu amor pode acreditar
         G                 A7     A7/4  A7
Vou te fazer feliz é só você deixar

D
Demais
          Bm7
Posso ir até o céu
        G
Em cada beijo seu
      A7         A7/4  A7
Posso tocar você,amar você

  D
É demais
         Bm7
Sentir o teu calor
           G        A7/4  A7
Não dá pra esquecer eu e você
  D  Bm7  G  G/A  Em7  A7/4  A7
É .................... eu e você

  D
É demais...
           Bm7      G
Ter você é demais...
   A7/4  A7  D      Bm7  G  A7/4  A7  D
Eu e você é  demais...

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
A7/4 = X 0 2 0 3 0
Bm7 = X 2 4 2 3 2
D = X X 0 2 3 2
Em7 = 0 2 2 0 3 0
G = 3 2 0 0 0 3
G/A = 5 X 5 4 3 X
