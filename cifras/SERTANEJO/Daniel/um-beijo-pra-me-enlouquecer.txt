Daniel - Um Beijo Pra Me Enlouquecer

(intro) G D C

G            D       C   G              D    C
Às vezes perto de você, eu juro não consigo ver
G            D         C               G  D C
Que existem outras pessoas além de você
G              D      C   G          D       C
E esse seu sorriso então, é fogo no meu coração
G            D         C              G
A coisa mais bonita desta festa de peão

(intro) G D C

G              C        D      G
Eu escrevi seu nome no meu chapéu
                C        D          G
A estrela mais linda brilha no meu céu
    C    D         C           G
Me dê um beijo pra me enlouquecer
                C       D      G
Eu escrevi seu nome no meu chapéu

                C        D          G
A estrela mais linda brilha no meu céu
   C     D         C           G
Você: um beijo pra me enlouquecer

(intro) G D C

G         D           C       G             D     C
Um laço forte a me laçar, fiquei preso no seu olhar
    G              D    C                G   D C
Um coice foi seu beijo doce pra me derrubar
G         D        C       G      D        C
Festa no meu coração, rodeio de amor e paixão
   G          D          C                     G
Você chegou, montou e domou pra sempre este peão

(refrão 3x)

(intro)

----------------- Acordes -----------------
A = X 0 2 2 2 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
