Daniel - Cobra Venenosa

(intro)   C G7 C G7 C

(declamando)

Você talvez não conhece, o veneno que as cobras têm.
Pois elas quando dá o bote balança o guizo também.
A cascavel é traiçoeira, quando ela quer se vingar,
Balança o guizo contente na hora dela picar.
A urutu é perigosa, de ruim não se manifesta,
É cobra tão venenosa que traz uma cruz na testa.
Jaracuçu Deus nos livre quando ela chega a picar,
Porque deixa o sinar dos seus dentes e a cicatriz no lugar.
Mas eu lhes digo a verdade.
Por cobra já fui picado,
Por cascavel, caninana, urutu esse malvado e, de todas eu já me livrei.
Esse veneno "Amargura" existe um contra veneno por isso tudo se cura.
Mas tem uma cobra do mato, uma cabocla lá do sertão,
Que trás o veneno nos zóio e ataca no coração.
Dessa uma vez fui picado um dia só por mardade,
Que ainda trago no peito a cicatriz da saudade.


(cantando)

C                     G7                          C
Já vai fazer quase um ano que eu deixei o meu sertão,
 F                G7     F           G7      C      C7
Por um veneno dos olhos que atingiu meu coração.
 F              G7                          C
Uma cabocla do mato, que tanto mal tem me feito.
 C             G7   F           G7   C
Uma olhada me deu, foi um veneno perfeito.

C                G7                       C
Essa cobra venenosa, cobra em forma de gente.
 F                  G7     F  G7            C      C7
Talvez a mais perigosa, pode matar de repente.
 F                     G7                    C
Procurei tantos remédios, andei por toda cidade.
 C               G7       F          G7        C
Mas tal qual não existe, nada que cure a saudade.

 F                 G7                   C
Agora vou repetir, a história mais dolorosa
 C              G7    F          G7           C
Essa cabocla do mato, é a cobra mais venenosa.

----------------- Acordes -----------------
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
G7 = 3 5 3 4 3 3
