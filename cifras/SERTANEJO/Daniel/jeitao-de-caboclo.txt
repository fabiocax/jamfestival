Daniel - Jeitão de Caboclo

[Intro]  G#  D#7  G#  G#7  C#  D#7  G#  D#7  G#

 G#    D#7     G#  C#            G#
Se eu pudesse voltar aos meus tempos de criança
 G#   D#7     G#           D#7
Reviver a juventude com muita perseverança
Morar de novo no sítio na casa de alvernaria
                C#      D#7     G#
Ver passarinhos cantando quando vem rompendo o dia
G#7          C#     D#7      G#
Eu voltaria a rever o pé de manjericão
         D#7  C#          G#
A curruira morando lá no oco do mourão
           C#        D#7      G#
Os bezerros no piquete e nossas vacas leiteiras
           D#7   C#    D#7      G#
E papai tirando leite bem cedinho na mangueira

( G#  D#7  G#  G#7  C#  D#7  G#  D#7  G# )

G#     D#7      G# C#           G#
Eu voltaria a rever o ribeirão Taquari

 G#   D#7         G#            D#7
Com suas águas bem claras onde  pesquei lambari
O velho carro de boi , o monjolo e a moenda
            C#    D#7       G#
As vacas Maria-Preta, a Tirolesa e a Prenda
 G#7         C#    D#7          G#
Na varanda tábua grande cheia de queijo curado
         D#7 C#            G#
E mamãe assando pão no forno de lenha ao lado
           C#   D#7           G#
Nossa reserva de mata, linda floresta fechada
              D#7  C#   D#7       G#
As trilhas fundas do gado retalhando a invernada

( G#  D#7  G#  G#7  C#  D#7  G#  D#7  G# )

 G#   D#7     G#   C#            G#
Queria rever o sol com seus raios florescentes
G#     D#7      G#              D#7
Escondendo atrás da serra roubando o dia da gente
O pé de dama-da-noite junto ao mastro de São João
             C#     D#7   G#
Que até hoje perfuma a minha imaginação
 G#7           C#   D#7          G#
O caso é que eu não posso fazer o tempo voltar
           D#7  C#        G#
Sou um cocão sem chumaço que já não pode cantar
          C#   D#7            G#
Vou vivendo na cidade perdendo as forças aos poucos
           D#7  C#     D#7     G#
Mas não consigo perder o meu jeitão de caboclo

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
