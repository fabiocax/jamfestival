Daniel - Declaração de Amor

D                  A    Bm  Bm/A             G
Minha vida e o meu mundo   são vazios sem você
 D                      A    Bm
 Nosso amor foi tão profundo
 Bm/A               G
Impossível esquecer
G                       D          Bm
  No meu corpo tem as suas digitais
G                       D          Bm  Bm/A
 Na jaqueta jeans, um beijo de batom
 G     Em7                 D
 Estou pensando em escrever
 Bm                     G    Em7   D   G
Volta, que eu te amo em luzes de neon
 D                        A   Bm
  Hoje eu encontrei no carro
Bm/A              G
Um bilhete de motel
   D                  A     Bm
  Minhas lágrimas molharam
 Bm/A            G
O pedaço de papel

G                      D        Bm
  São retratos, vídeo tapes e postais
G                    D          Bm
É uma confissão na capa de um CD
G      Em7                  D
  E essa música que eu canto
  Bm                  G     Em7 D G
Fiz aos prantos com saudades de você
D           A              Bm        F#m
Você vai ouvir minha declaração de amor
G              A         D D7
Onde você, estiver eu vou
G           A   D             Bm          Em7
Você vai saber nas ondas do rádio ou da Tevê
 G                             D
Que eu amo você.............. ê
D          A               Bm          F#m
Você vai ouvir minha declaração de amor
G             A        D D7
Onde você, estiver eu vou
G            A   D            Bm          Em7
Você vai saber nas ondas do rádio ou da Tevê
 G            D
Que eu amo você
    F#m        G   A     D
Que eu não existo sem você
    F#m      G      A         D     A    Bm
Que eu não existo sem vo.......o........cê......ê
     G Em7               G    D
     Oh, oh, oh, não, sem você

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
Bm/A = X 0 4 4 3 X
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em7 = 0 2 2 0 3 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
