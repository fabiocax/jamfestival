Daniel - Te Amo Cada Vez Mais

D
 Pegue-me
                 A
Aqueça-me em seu corpo amor
Bm
Quero te amar
                G
Como a primeira vez
A                    D
No tempo em que eu era seu
Dm    C
Só você e eu
Cm     Gm          D           A7/4  A7
E nos olhos o espelho da paixão
 D                 A
Não vá como vou viver assim
    Bm
Distante de você
                  G    A/G
O que vai ser de mim
        D/F#        Dm/F
Não posso te perder

             C/E     Cm
Jamais vou te esquecer
            Gm
Diz que é sonho
       D           A4     A7
Me acorde por favor
       D            A
Eu só quero o seu amor
Bm         F#m
No meu coração
      G               A     D   A7
Meu amor é mais que uma paixão
          D         A
O que eu fiz prá você
Bm            F#m
Não me deixe assim
        G         A     D
Eu não posso aceitar o fim
D                    A
Olha você precisa acreditar
   Bm
Na força do amor
            G  A/G    D/F#   Dm/F
E o seu coração necessita ver
  C/E        Cm        Gm
O porquê de nós, eu e você
        D            A4    A7
Você e eu e ninguém mais
      D             A
Eu só quero o seu amor
Bm        F#m
No meu coração
      G               A     D   A7
Meu amor é mais que uma paixão
          D         A
O que eu fiz prá você
Bm             F#m
Não me deixe assim
        G         A     D
Eu não posso aceitar o fim
Bb     Gm                    D
Não vá, você tem que me escutar, por Deus
Bb         Gm                A4
Me dê uma chance a mais prá amar você

[Solo]  D  E/D  A/C#  Am/C  G/B  F/A   C/G  F/A    A

   D     Dm         C/E      Cm
Amor...ooo...eu não quero ver
     Gm           D           A4       A7
Você partir e aquela porta se fechar
       D            A
Eu só quero o seu amor
Bm         F#m
No meu coração
      G               A      D   A7
Meu amor é mais que uma paixão
          D         A
O que eu fiz pra você
Bm            F#m
Não me deixe assim
    G          A    D
Eu não posso aceitar o fim

D  A  Bm  F#m  G  A  D  A7
Ôôôôôôôôôôôôôôôôôôôôô

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A/G = 3 X 2 2 2 X
A4 = X 0 2 2 3 0
A7 = X 0 2 0 2 0
A7/4 = X 0 2 0 3 0
Am/C = X 3 2 2 5 X
Bb = X 1 3 3 3 1
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
C/G = 3 3 2 X 1 X
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Dm = X X 0 2 3 1
Dm/F = X X 3 2 3 1
E/D = X 5 X 4 5 4
F#m = 2 4 4 2 2 2
F/A = 5 X 3 5 6 X
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
Gm = 3 5 5 3 3 3
