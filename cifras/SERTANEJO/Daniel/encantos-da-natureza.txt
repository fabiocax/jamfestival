Daniel - Encantos da Natureza

A                             E7
Tu que não tiveste a felicidade
                          A
Deixa a cidade, vem conhecer
                                     E7
Meu sertão querido meu reino encantado
                               A
Meu berço adorado que me viu nascer
                   D                   E7
Venha o mais depressa, não fique pensando
                                A
Estou te esperando para-te mostrar
                                        E7
Vou mostrar os lindos rios de águas claras
                              A    E7    A  E7  A  E7
E as belezas raras do nosso luar
A                                    E7
Quando a lua nasce, por de traz da mata
                             A
Fica cor de prata, a imensidão
                              E7
Então fico horas e horas olhando

                             A
A lua  banhando la no ribeirão
                 D               E7
Muitos não se importam, com este luar
                                   A
Nem lembra de olhar, o luar na serra
                                    E7
Mais estes não vive, são seres humanos
                               A       E7   A  E7  A  E7
Estão vegetando, em cima da terra
 A                                   E7
Quando a lua esconde, logo rompe aurora
                           A
Vou dizer agora, do amanhecer
                                   E7
Raios vermelhados, riscam o horizonte
                                A
O sol lá no monte, começa a nascer
             D                E7
Lá na mata canta, toda passarada
                            A
E lá  na paiada, pia o chororó
                                 E7
O rei do terreiro, abre a garganta
                                A   E7   A  E7  A  E7
Bate a asa e canta em cima do paiol
 A                                  E7
Quando o sol esquenta, cantam cigarras
                                      A
Em grande algazarra na beira da estrada
                                  E7
Lindas borboletas, de variadas cores
                                   A
Vem beijar as flores já desabrochadas
        D                      E7
Este pedacinho de chão encantado
                             A
Foi abençoado, por nosso senhor
                                 E7
Que nuca nos deixa faltar no sertão
                         A    A  E7  A  E7
Saúde união a paz e o amor

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
