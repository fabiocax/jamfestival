Daniel - Meu Reino Encantado / Saudade da Minha Terra (pot-pourri)

 B
Eu nasci num recanto feliz
                   F#7
Bem distante da povoação
Foi ali que eu vivi muitos anos
      E     F#7         B
Com papai mamãe e os irmãos
Nossa casa era uma casa grande
                    F#7 E7 F#7
Na encosta de um espigão
Um cercado pra apartar bezerro
                         B
E ao lado um grande mangueirão

 B
No quintal tinha um forno de lenha
                          F#7
E um pomar onde as aves cantava
Um coberto pra guardar o pilão
    E        F#7        B
E as traias que papai usava

De manhã eu ia no paiol
                         F#7 E7 F#7
Um espiga de milho eu pegava
Debulhava e jogava no chão
                            B
Num instante as galinhas juntava

 B
Nosso carro de boi conservado
                            F#7
Quatro juntas de bois de primeira
Quatro cangas, dezesseis canseis
     E        F#7        B
Encostados no pé da figueira
Todo sábado eu ia na vila
                             F#7 E7 F#7
Fazer compras para semana inteira
O papai ia gritando com os bois
                               B
Eu na frente ia abrindo as porteiras

 B
Nosso sítio que era pequeno
                          F#7
Pelas grandes fazendas cercado
Precisamos vender a propriedade
      E           F#7       B
Para um grande criador de gado
E partimos pra a cidade grande
                         F#7 E7 F#7
A saudade partiu ao meu lado
A lavoura virou colonião
                           B
E acabou-se meu reino encantado

 B
Hoje ali só existem três coisas
                          F#7
Que o tempo ainda não deu fim
A tapera velha desabada
        E       F#7         B
E a figueira acenando pra mim
E por ultimo marcou saudade
                           F#7 E7 F#7
De um tempo bom que já se foi
Esquecido em baixo da figueira
                      B
Nosso velho carro de boi

     B
De que me adianta viver na cidade
                               F#
Se a felicidade não me acompanhar
Adeus, paulistinha do meu coração
                            B
Lá pro meu sertão eu quero voltar
            E
Ver a madrugada, quando a passarada
                                     F#
Fazendo alvorada começa a cantar
Com satisfação arreio o burrão
                 E             B
Cortando o estradão saio a galopar
           E               F#
E vou escutando o gado berrando
                          B
Sabiá cantando no jequitibá

( B  F#  B  F#  E  B )

              B
Por nossa senhora, meu sertão querido
                                F#
Vivo arrependido por ter te deixado
Esta nova vida aqui na cidade
                               B
De tanta saudade, eu tenho chorado
             E
Aqui tem alguém, diz que me quer bem
                                         F#
Mas não me convém, eu tenho pensado
Eu digo com pena, mas essa morena
               E                 B
Não sabe o sistema que eu fui criado
            E                  F#
Tô aqui cantando de longe escutando
                                  B
Alguém está chorando com o rádio ligado

( B  F#  B  F#  E  B )

              B
Que saudade imensa do campo e do mato
                                   F#
Do manso regato que corta as campinas
Aos domingos ia passear de canoa
                                  B
Nas lindas lagoas de águas cristalinas
              E
Que doce lembrança daquelas festanças
                                        F#
Onde tinham danças e lindas meninas
Eu vivo hoje em dia sem ter alegria
           E                B
O mundo judia, mas também ensina
             E                   F#
Estou contrariado, mas não derrotado
                                B
Eu sou bem guiado pelas mãos divinas

( B  F#  B  F#  E  B )

              B
Pra minha mãezinha já telegrafei
                            F#
E já me cansei de tanto sofrer

Nesta madrugada estarei de partida
                                  B
Pra terra querida, que me viu nascer
            E
Já ouço sonhando o galo cantando
                                  F#
O inhambu piando no escurecer

A lua prateada clareando a estrada
            E                   B
A relva molhada desde o anoitecer
           E                 F#
Eu preciso ir pra ver tudo alí
                               B    F#7 B
Foi lá que nasci, lá quero morrer

[Final] B  F#  B  F#
        E  B  F#7  B

----------------- Acordes -----------------
B = X 2 4 4 4 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F# = 2 4 4 3 2 2
F#7 = 2 4 2 3 2 2
