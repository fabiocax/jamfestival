Daniel - Poeira da Estrada

E|-0-5-5-5-5--3----3--2-2-2--0---0-2-0-0---------------|
B|-2/7-7-7-7--4----4--3-3-3--2---2-3-2-2---0-0-1-2-----|
G|-----------------------------------------1-1-1-2-----|
D|-----------------------------------------------------|
A|-----------------------------------------------------|
E|-----------------------------------------------------|

E|-0-5-5-5-5--3----3--2-2-2--0---0-0-2/4-4----2-2-0-----|
B|-2/7-7-7-7--4----4--3-3-3--2---2-2-3/5-5----3-3-2-2---|
G|--------------------------------------------------2---|
D|------------------------------------------------------|
A|------------------------------------------------------|
E|------------------------------------------------------|

            A
Levantei a tampa voltei ao passado
                                  E
Meu mundo guardado dentro de um baú

Encontrei no fundo todo empoeirado
                                A
O meu velho laço bom de couro cru


Me vi no arreio do meu alazão
             A7               D
Berrante na mão no meio da boiada
                            A
Abracei o laço velho companheiro
                              B
Bateu a saudade, veio o desespero
             D         E           A
Sentindo o cheiro da poeria da estrada

            E       D        A
Estrada que era vermelha de terra
                  E                    A
Que o progresso trouxe o asfalto e cobriu
             E       D      A
Estrada que hoje chama rodovia
                 E                A
Estrada onde um dia meu sonho seguiu
            D              A
Estrada que antes era boiadeira
             E                       A
Estrada de poeira, de sol, chuva e frio
               D                  A
Estrada ainda resta um pequeno pedaço
             E                    A
A poeira do laço que ainda não saiu

A
Poeira da estrada, só resta a saudade
                         E
Poeira na cidade é a poluição

Não se vê vaqueiros tocando boiada
                            A
Trocaram o cavalo pelo caminhão

E quando me bate saudade do campo
                 A7               D
Pego a viola e canto a minha solidão
                               A
Não me resta muito aqui na cidade
                                B
E quando a tristeza pena de verdade
            D            E        A
Eu mato a saudade nas festas de peão

            E       D        A
Estrada que era vermelha de terra
                  E                    A
Que o progresso trouxe o asfalto e cobriu
             E       D      A
Estrada que hoje chama rodovia
                 E                A
Estrada onde um dia meu sonho seguiu
            D              A
Estrada que antes era boiadeira
             E                       A
Estrada de poeira, de sol, chuva e frio
               D                  A
Estrada ainda resta um pequeno pedaço
             E                    A
A poeira do laço que ainda não saiu

----------------- Acordes -----------------
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
C# = X 4 6 6 6 4
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
