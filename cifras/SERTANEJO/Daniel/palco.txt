Daniel - Palco

Int: G Bm C D

G
Muitas vezes sao os palco
    Bm
um destaque na vida de alguem
       C
muitas vezes nois olhamos
       D
e nao vemos mais ninguem
G
nesta hora que lembramos
    Bm
que cristo está a nos olhar..
    C
meditando em suas promessas
          D
e as vezes nos pegamos a chorar
G
vem a noite olha a lua
     Bm
está tao frio la fora

     C
e eu me perguntando a Deus
          D
o que será da minha historia
G
meus amigos que estavam do lado
     Bm
hoje ja nao vejo mais
     C
hoje ja dormem contigo
   D
no eterno lar de paz


[CORO]
   G                     Bm
Que gloria, gloria sem par
          C                           D
a gloria que um dia Deus prometeu me dar
         G              Bm
e muito mais do que almeijei
   C                      D
valeu a pena chorar-ha-ha-ha.. tudo que eu ja chorei


G Bm C D

  G
e quando la chegar
       Bm
quero a Deus o abraçar
         C
e em um grande coral com os anjos
       D
tambem cantar ao lar
    G
ouvir os anjos dizer amem
      Bm
entao os abraçar tambem
   C
e acerteza lá será
  D
a eternal jerusalém...

----------------- Acordes -----------------
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
