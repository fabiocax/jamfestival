Daniel - Mundo Velho


                        A                   A              A
e|------5-4h5h4-------5-5-5-------44------5-5-5--5-7-9-----5--|
B|--5h7----------7-5~-5-5-5--5h7--55-5-3~-5-5-5------------5--|
G|--------------------6-6-6----------6-4~-6-6-6--6-7-9-9~--6--|
D|--------------------7-7-7---------------7-7-7---------------|
A|--------------------7-7-7---------------7-7-7---------------|
E|--------------------5-5-5---------------5-5-5---------------|

A
Deus fez o mundo tão lindo
                 E7
Só belezas que rodeia
                                        A       E7 A
Colocando lá no espaço lua nova e lua cheia
              A
e|-5-7-9------5--|
B|---------------|
G|-6-7-9-9~---6--|

Fez o sol e a luz divina

                      E7
Que o mundo inteiro clareia
              A
e|-5-7-9------5--|
B|------------5--|
G|-6-7-9-9~---6--|

No céu estrelas paradas
               D  A
A lua e o sol passeiam
              A
e|-5-7-9------5--|
B|------------5--|
G|-6-7-9-9~---6--|

Deus fez o mar azulado
E o castelo da sereia
Fez peixe grande e pequeno
E também fez a baleia
Fez a terra onde formei
Meu cafezal de ameia
Baixadão cheio de água
Onde meu arroz cacheia

Deus fez cachoeiras lindas
Lá na serra serpenteia
Fez papagaio que fala
Passarada que gorjeia
Cangara canta de bando
A natureza ponteia
Pros catireiros de pena
Que no galho sapateia

Mundo velho mudou tanto
Que já está entrando areia
Grande pisa nos pequenos
Coitadinhos desnorteiam
Quem trabalha não tem nada
Enriquece quem tapeia
Pobre não ganha demanda
Rico não vai pra cadeia

Na moral do velho mundo
Quem não presta pisoteia
Os mandamentos de Deus
Tem gente que até odeia
Igrejas estão vazias
Antigamente eram cheias
O que é ruim está aumentando
O que é bom ninguém semeia

Ó meu Deus venha na Terra
Porque a coisa aqui tá feia
Mas que venha prevenido
Traga chicote e correia
Tem até mulher pelada no lugar da santa ceia
Só Deus pode dar um fim
No que o diabo desnorteia

Tom Original em Ab

----------------- Acordes -----------------
A = X 0 2 2 2 0
Ab = 4 3 1 1 1 4
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
