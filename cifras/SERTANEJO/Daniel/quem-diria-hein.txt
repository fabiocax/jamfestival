Daniel - Quem Diria, Hein?

Início: C7+ C7 C7+ C7 C7+

      Dm
Quem diria hein?!
    F                              G
A gente não se desgrudava o tempo todo
                 C           Em
Agora nem o endereço tenho
   Am
Eu conhecia cada palmo do seu corpo
            Dm
E agora nem seu telefone eu sei
        F                      G
Eu que vivia rindo a toa hoje choro e morro
        C
A cada dia sem ninguém


      F
Sem dizer também dos nossos planos
                                            Em
De se amar pra vida inteira que era nosso juramento

       Am
Hoje acordei e li uma nota no jornal
             Dm
Que me arrebentou por dentro
    F                             G
É a foto do meu grande amor com outro cara
                  C         G
Brindando o seu casamento


      C
E eu que imaginava que você me esperava
 Dm
Louca de saudade
      G
Não sabia dos teus planos que roubavam nossos sonhos
C              G
Escondendo a verdade
         C                  Am
E em tão pouco tempo foi entregar o nosso amor
Dm
Pra outra pessoa
       G
Foi loucura, ilusão, se envolver nessa paixão
       C
E eu que imaginava que você me esperava
 F
Louca de saudade...


solo: G G G G C


      F
Sem dizer também dos nossos planos
                                            Em
De se amar pra vida inteira que era nosso juramento
       Am
Hoje acordei e li uma nota no jornal
             Dm
Que me arrebentou por dentro
    F                             G
É a foto do meu grande amor com outro cara
                  C         G
Brindando o seu casamento


      C
E eu que imaginava que você me esperava
 Dm
Louca de saudade
      G
Não sabia dos teus planos que roubavam nossos sonhos
C              G
Escondendo a verdade
         C                  Am
E em tão pouco tempo foi entregar o nosso amor
Dm
Pra outra pessoa
       G
Foi loucura, ilusão, se envolver nessa paixão
       C
E eu que imaginava que você me esperava
 Dm
Louca de saudade


      G
Não sabia dos teus planos que roubavam nossos sonhos
C              G
Escondendo a verdade
         C                  Am
E em tão pouco tempo foi entregar o nosso amor
Dm
Pra outra pessoa
       G
Foi loucura, ilusão, se envolver nessa paixão
C                C7+
E nem me diz porque
  F             C                 Dm
Não posso acreditar, tem outro em meu lugar
G           F   Dm   C
Vivendo com você


**Mari, te amo**

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
C7+ = X 3 2 0 0 X
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
