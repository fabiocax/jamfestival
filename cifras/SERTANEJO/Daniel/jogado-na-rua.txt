Daniel - Jogado Na Rua

 F                             C
Eu ando largado na rua faz uma semana
                     Bb
minha vida jogada na lama
                        F  Bb
e o pivô disso tudo é você!
    F
Procuro a saida, uma luz que ilumine os meus
 C
passos
                           Bb
com o peito faltando um pedaço
                      F          F C
fica impossível tentar te esquecer.
    Dm                                  Am
Meus olhos derramam momentos de águas passadas
                    Bb
aí meu amor encho a cara
          F       F C
embriagado de amor.
    Dm                                   Am
E quando amanhece outra vez tô jogado na rua

                                 Bb
sem um postal, sem moral, nem conduta...
                 F
Um vagabundo carente de amor.
       C
Sem você...
                                Bb
O meu céu não tem estrelas nem lua,
                       F
sua falta me leva à loucura,
                C
tenho medo do anoitecer.
       C
Sem você...
                             Bb
Eu me sinto como um cão sem dono,
                         F
em perfeito e total abandono,
                 C
sem saída e pra onde correr.
      Gm   Bb
Tenho medo,
      F
tenho medo...
        Gm          Bb
Não me deixe nesta solidão.
      F
Tenho medo.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
