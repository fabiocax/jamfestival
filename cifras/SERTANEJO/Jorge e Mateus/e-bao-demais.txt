Jorge e Mateus - É Bão Demais

Refrão:
A          E             A
È Bão Demais , È Bão Demais
     A7           D
A Vida De Violeiro
                     E
Cantar No Brasil Inteiro
                  A          A            (E)
E Voltar Pro Meu Goiás   (Goiás é Mais)  { 2x }

 A
Quando Saio Pra Cantar Por Esse Brasil Afora
 E                                       A
Levo Deus No Coração , E Na Bagagem a Viola
                        A7                D
Nas Cidades Do Interior   Distritos e Capítais
                     A                           E
Fazendo O Que Agente Ama , Mas Quando A Saudade Chama
                  A
Eu Volto Pro Meu Goiás


Refrão:
A                                            E
Um Dia Canto No Sul , No Outro Ja Vou Pro Norte
                                          A
Do Nordeste Ao Sudeste Coração Bate Mais Forte
                            A7                D
Solto A Voz No Centro Oeste , Num Dueto De Paixão
                    A                          E
Junto Com o Meu Parceiro , Por Esse Brasil Inteiro
              A
Cantando Só Modão

(refrão)
(repete 2ª parte)
(repete refrão 2x)

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
