Jorge e Mateus - É Amor

G
Quando fico pensando em você
                              C
Eu esqueço do mundo pra te ter

Seu amor assumiu meu coração
                                G
Eu não fico um segundo sem te ver

O cupido chamou minha atenção
                                  C
Pra cuidar e investir nessa paixão

   Am      C
É amor, ôuo ôuo...
   Am        C
Só nós dois

                G
Vale a pena sonhar
       C         G
Ter você pra amar

          Am              C
Eu quero colo a vida inteira
         G
Vou te amar

( G  C )

G
Quando fico pensando em você
                              C
Eu esqueço do mundo pra te ter

Seu amor assumiu meu coração
                                G
Eu não fico um segundo sem te ver


O cupido chamou minha atenção
                                  C
Pra cuidar e investir nessa paixão

   Am      C
É amor, ôuo ôuo...
   Am        C
Só nós dois

                G
Vale a pena sonhar
       C         G
Ter você pra amar
          Am              C
Eu quero colo a vida inteira
         G
Vou te amar

G C

G          C      G
ôôôuôh ... É amor
G          C      G
ôôôuôh ... É amor


 Am              C
Eu quero colo a vida inteira
         G
Vou te amar

G          C      G
ôôôuôh ... É amor
G          C      G
ôôôuôh ... É amor


 Am              C
Eu quero colo a vida inteira
         G
Vou te amar
 Am              C
Eu quero colo a vida inteira
         G
Vou te amar
 Am              C
Eu quero colo a vida inteira

(C)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
G = 3 2 0 0 0 3
