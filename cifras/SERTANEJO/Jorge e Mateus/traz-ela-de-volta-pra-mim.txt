Jorge e Mateus - Traz Ela de Volta Pra Mim

(intro: A E D E A E)


(solo)
E|-----------------------------------------------------------------------7----|
B|-----5-5--5h7-5-----5-5--5-5h7-5-------5-5h7-------------------------5------|
G|--4/6----------7-6-7----4--------7-6-7------4-4-4--4-7-6-4--2-1----4--------|
D|---------------------------------------------------------------4-2----------|
A|----------------------------------------------------------------------------|
E|----------------------------------------------------------------------------|

     A
Meu bem
            Bm
O que aconteceu
                D
O nosso amor morreu
                  A       E
Quem sabe um dia vai voltar
  A
Você

                    Bm
Fez parte de uma história
                D
Tá dentro da memória
              A        E
Talvez isso possa voltar

 A
Quando dois seres como nós
 Bm
Passam por momentos assim
         D
Vai por mim
                     A   E
Vai passar é só um temporal
      A
Pois nada nessa vida é maior
          Bm
Do que o amor e uma grande paixão
     D
Coração
 E                     A  E
Traz ela de volta pra mim

 A                      E
Deixa que tudo vai se acertar
                       D
Pois ninguém pode enganar
                            A
Um coração que um dia jurou te amar
     E
Só amar
 A
Esteja
               E
Aonde você estiver
                   D
Você será minha mulher
     E
Meu bem
         A   E
Te amo tanto

(solo)

 A
Quando dois seres como nós
 Bm
Passam por momentos assim
         D
Vai por mim
                     A   E
Vai passar é só um temporal
      A
Pois nada nessa vida é maior
          Bm
Do que o amor e uma grande paixão
     D
Coração
 E                     A  E
Traz ela de volta pra mim

 A                      E
Deixa que tudo vai se acertar
                       D
Pois ninguém pode enganar
                            A
Um coração que um dia jurou te amar
     E
Só amar
 A
Esteja
               E
Aonde você estiver
                   D
Você será minha mulher
     E
Meu bem
         A
Te amo tanto

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
