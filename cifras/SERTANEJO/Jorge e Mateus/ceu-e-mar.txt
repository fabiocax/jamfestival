﻿Jorge e Mateus - Céu e Mar

Capo Casa 3

Intro 4x: Am  C  G  D

Primeira Parte:
Am               C
   Aumente o som   pra ficar bom
         G                     D
A nossa festa não tem hora pra acabar
Am               C                       G
   O teu sorriso   abre as portas do paraíso
                   D
Vem comigo pra gente dançar

Am               C
   A melhor hora   sempre é agora
        G                         D
E o melhor lugar é sempre onde você está
Am
   E a luz nunca se apaga
C
  Juízo sempre acaba
Am            C          D
   E a nossa música vai começar


Refrão:
 G                     C
Paz e amor é o que eu quero pra nós
       Em         D                  C
E que nada nesse mundo cale a nossa voz

 G              C
Céu e mar e alguém para amar
         Em       D                        C
E o arrepio toda vez que a gente se encontrar

         Am
E nunca vai passar
                C      D
Mesmo quando o sol chegar

Intro 2x: ( Am C G D )

Am               C
   Aumente o som   pra ficar bom
         G                     D
A nossa festa não tem hora pra acabar
Am               C                       G
   O teu sorriso   abre as portas do paraíso
                   D
Vem comigo pra gente dançar

Am               C
   A melhor hora   sempre é agora
        G                         D
E o melhor lugar é sempre onde você está
Am
   E a luz nunca se apaga
C
  Juízo sempre acaba
Am            C          D
   E a nossa música vai começar

Refrão:
 G                     C
Paz e amor é o que eu quero pra nós
       Em         D                  C
E que nada nesse mundo cale a nossa voz

 G              C
Céu e mar e alguém para amar
         Em       D                        C
E o arrepio toda vez que a gente se encontrar

 G                     C
Paz e amor é o que eu quero pra nós
       Em         D                  C
E que nada nesse mundo cale a nossa voz

 G              C
Céu e mar e alguém para amar
         Em       D                        C
E o arrepio toda vez que a gente se encontrar

         Am
E nunca vai passar
                C      D
Mesmo quando o sol chegar

----------------- Acordes -----------------
Capotraste na 3ª casa
Am*  = X 0 2 2 1 0 - (*Cm na forma de Am)
C*  = X 3 2 0 1 0 - (*D# na forma de C)
D*  = X X 0 2 3 2 - (*F na forma de D)
Em*  = 0 2 2 0 0 0 - (*Gm na forma de Em)
G*  = 3 2 0 0 0 3 - (*A# na forma de G)
