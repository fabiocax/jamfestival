Jorge e Mateus - Vai Entender

Intro: C  G  Am   F

                         C                G/B
E|--3---1---0-------0----3---1---0----------|
B|--------------3-------------------3---3---|
G|------------------------------------------|
D|------------------------------------------|
A|------------------------------------------|
E|------------------------------------------|

                      Am                  G
E|--3---1---0-------0---0---0---0---0-------|
B|--------------3-----------------------3---|
G|------------------------------------------|
D|------------------------------------------|
A|------------------------------------------|
E|------------------------------------------|

            C
E|---------------------|
B|--3-----1------------|
G|---------------------|
D|---------------------|
A|---------------------|
E|---------------------|


preparação para entrar:
F7M, Am7 e Dm (um toque em cada, no Dm você já pode começar a cantar)


  Você sabe meus defeitos
             G
Sempre me entrego pra você
Dm
  Sempre depois da meia noite
                 G
Você some e me deixa enlouquecer
        C       (F, C)
Pra que?

      (faça slides com a corda abafada)               G
Você diz que tá carente mas não deixa a gente se encontrar
      Bb                                       F
Na primeira hora do meu dia faz questão de me magoar
            Dm                                 G
Joga um balde de água fria só que não apaga o fogo
              C        F
E eu quero de novo

Refrão:

                C
Quero a minha paz
                   G
Que eu não tenho mais
                Am                G   F
Deixa eu pelo menos tentar te esquecer

               C
Quero a minha paz
               G
Pra mim tanto faz
                 Am              G   F
Estar sozinho, perto ou longe de você
C
   Vai entender

(Repete tudo)

C                              F
Você sabe meus defeitos
C
Sempre depois da meia noite
                            F
Você some e me deixa enlouquecer
               C
Vai entender

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
F7M = 1 X 2 2 1 X
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
