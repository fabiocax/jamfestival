Jorge e Mateus - Me Leva Pra Casa

(intro) G Em C D G Em C D

G          Em        G            Em         C
Cansei de ficar sozinho, na rua não tem carinho
  D               G    D  G
ouou, me leva pra casa
               Em          G
No amor sempre fui bandido,
              Em          C
e agora que estou perdido
  D               G     D/F#
ouou, me leva pra casa

Em         Bm           C                G       D/F#
Tive que perder você, pra ver que estava errado
   Em           Bm                C          D
Cansei de ver o sol nascer , sem ter você do lado
        G         Bm         C          G      D/F#
Hoje eu sei, e me dei, sempre a pessoa errada
    Em          C         D                G    D
Eo amor que ganhei só eu sei... não valeu nada

         G       Bm           C          G     D/F#
Hoje eu sei, e achei, quem eu tanto procurava
      Em     C    D D4 D               G  Em C D G Em C D
Meu amor é você ououuu... me leva pra casa

(repete tudo)

   D         G Bm C D G Bm C D
Me leva pra casa...

----------------- Acordes -----------------
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D4 = X X 0 2 3 3
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
