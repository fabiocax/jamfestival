Jorge e Mateus - Houver Razões

Intro: D/F#  E/G#  A5(9)  E  D5(9)  A5(9)

Passagem Intro:

   D5(9)   A5(9)
E|2-------2--------------------------------|
B|5-------2--------------------------------|
G|4-------4--------------------------------|
D|2-------4--------------------------------|
A|--6-4---2--------------------------------|
E|----------6-2----------------------------|

Primeira Parte:

F#m
  Eu já fiz de tudo pra te convencer
D5(9)                                  A5(9)
  Mandei rosas vermelhas lindas pra você
           E               F#m
Falei de amor, fiz uma canção
                                  D5(9)
A lua se foi, nem vi o sol chegar

                                 A5(9)
Acreditei que o tempo não ia passar
        E
Foi ilusão

Segunda Parte:
Bm                                            D5(9)
   Enquanto houver razões eu não vou desistir
                                    A5(9)
Se for pra eu chorar, quero chorar por ti
                  E
Porque não te esqueço
Bm                                      D5(9)
   Vou te esperar passe o tempo que for
                                A5(9)
Deixe bem guardado esse nosso amor
                 E
Sei que eu te mereço

Refrão:
A5(9)                  E
  Eu vou deixar você voar
              F#m             D5(9)  E5
Bater as suas asas pra longe de mim
A5(9)                     E
  Mas só pra ver você voltar
               F#m
E toda arrependida me dizer
  D5(9)        E5
Amor te quero sim
A5(9)                  E
  Eu vou deixar você voar
              F#m             D5(9)  E5
Bater as suas asas pra longe de mim
A5(9)                     E
  Mas só pra ver você voltar
               F#m
E toda arrependida me dizer
  D5(9)        E5
Amor te quero sim

( A5(9)  E  F#m  D5(9)  A5(9) )

   D5(9)   A5(9)
E|2-------2--------------------------------|
B|5-------2--------------------------------|
G|4-------4--------------------------------|
D|0-------4--------------------------------|
A|--6-4---2--------------------------------|
E|----------6-2----------------------------|


Repete a Primeira Parte:
F#m
  Eu já fiz de tudo pra te convencer
D5(9)                                  A5(9)
  Mandei rosas vermelhas lindas pra você
           E               F#m
Falei de amor, fiz uma canção
                                  D5(9)
A lua se foi, nem vi o sol chegar
                                 A5(9)
Acreditei que o tempo não ia passar
        E
Foi ilusão

Repetição da Segunda Parte:
Bm                                           D5(9)
  Enquanto houver razões eu não vou desistir
                                    A5(9)
Se for pra eu chorar, quero chorar por ti
                  E
Porque não te esqueço
Bm                           C#m7      D5(9)
  Vou te esperar passe o tempo que for
                                A5(9)
Deixe bem guardado esse nosso amor
                 E
Sei que eu te mereço

Refrão Final:
A5(9)                  E
  Eu vou deixar você voar
              F#m             D5(9)  E5
Bater as suas asas pra longe de mim
A5(9)                     E
  Mas só pra ver você voltar
               F#m
E toda arrependida me dizer
  D5(9)        E5
Amor te quero sim
A5(9)                  E
  Eu vou deixar você voar
              F#m             D5(9)  E5
Bater as suas asas pra longe de mim
A5(9)                     E
  Mas só pra ver você voltar
               F#m
E toda arrependida me dizer
  D5(9)        E5
Amor te quero sim

( A5(9)  E  F#m  D5(9)  A5(9) )

   D5(9)   A5(9)
E|2-------2-------2------------------------|
B|5-------2-------2------------------------|
G|4-------4-------4------------------------|
D|2-------4-------4------------------------|
A|--6-4---2-------2------------------------|
E|----------6-2----------------------------|

----------------- Acordes -----------------
A5(9) = X 0 2 2 0 0
Bm = X 2 4 4 3 2
C#m7 = X 4 6 4 5 4
D/F# = 2 X 0 2 3 2
D5(9) = X 5 7 9 X X
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E5 = 0 2 2 X X X
F#m = 2 4 4 2 2 2
