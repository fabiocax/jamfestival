Jorge e Mateus - Página Virada

Intro:
E|-------------------------------------------------------------4s5-------8s10--8--------------------------|
B|---------5p6---------------5b----4s5-------------------5-5s6-----8-7-8---------8s6-6b--5s3-3b--1p0------|
G|-----5h7-----7--5--7--4--7-----7-----7--5--4--5---5--7--------------------------------------------------|
D|-4s5--------------------------------------------7-------------------------------------------------------|
A|--------------------------------------------------------------------------------------------------------|
E|--------------------------------------------------------------------------------------------------------|


C
Vem... que esse orgulho não
Vai dar em nada
O que passou é página virada
                             Dm
Melhor pensar que nada aconteceu
Riff 1:
E|-----1-0-1-3---|
B|---3-----------|
G|-2-------------|
D|---------------|
A|---------------|
E|---------------|



Dm
Não, para que eu vou viver
          G7
na solidão
                         Dm
Não posso enganar meu coração
G7
Eu penso em você o tempo
C     Riff 2
inteiro
Riff 2:
E|----8-8--7-8-8s10--|
B|-------------------|
G|-------------------|
D|-10----------------|
A|-------------------|
E|-------------------|


  C
Vem... quem ama não consegue
Estar sozinho
Eu vivo te buscando em meu
caminho
     C7                    F
Porque já desisti de te esquecer
               G7
Não, não vou ficar vivendo
        C
do passado
       Am
Saber quem estava certo
           Dm
ou errado
      G7                     C
Só quero ver você voltar pra mim
Quem mais precisa desse
 E7
amor sou eu
Porque de nós quem mais
   Am
sofreu fui eu
                            G7
Por isso mesmo é que eu te digo
                     C
Não tem sentido eu sem você
Quem sabe o que é melhor pra
E7
mim sou eu
                       Am
Quem sente falta de você sou eu
F                  C
Eu te quero ao meu lado
               G7     C
É o que pede meu coração

Solo:
E|-------------------------------------------------------------4s5-------8s10--8--------------------------|
B|---------5p6---------------5b----4s5-------------------5-5s6-----8-7-8---------8s6-6b--5s3-3b--1p0------|
G|-----5h7-----7--5--7--4--7-----7-----7--5--4--5---5--7--------------------------------------------------|
D|-4s5--------------------------------------------7-------------------------------------------------------|
A|--------------------------------------------------------------------------------------------------------|
E|--------------------------------------------------------------------------------------------------------|


C
Vem... que de você só lembro
com carinho
Pra que buscar na rosa o espinho
Se há coisas tão bonitas pra
     Dm
lembrar
Riff 1:
E|-----1-0-1-3---|
B|---3-----------|
G|-2-------------|
D|---------------|
A|---------------|
E|---------------|


Dm                          G7
Não, viver mentindo não é solução
                      Dm
Inútil procurar outra emoção
    G7
Você é uma paixão mal
     C    Riff 2
resolvida
Riff 2:
E|----8-8--7-8-8s10--|
B|-------------------|
G|-------------------|
D|-10----------------|
A|-------------------|
E|-------------------|


C
Vem... prefiro confessar sem
vaidade
No fundo estou morrendo de
saudade
C7                   F
Você não pode nem imaginar
              G7
Não, não deixe quem te ama
       C
esperando
    Am                       Dm
Ficaram tantas coisas nos ligando
       G7                   C
Só quero ver você voltar pra mim
Quem mais precisa desse
 E7
amor sou eu
Porque de nós quem mais
   Am
sofreu fui eu
                          G7
Por isso mesmo é que eu te digo
                       C
Não tem sentido eu sem você
Quem sabe o que é melhor pra
E7
mim sou eu
                        Am
Quem sente falta de você sou eu
F                C
Eu te quero ao meu lado
                   G7      C
É o que pede meu coração...

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
E7 = 0 2 2 1 3 0
F = 1 3 3 2 1 1
G7 = 3 5 3 4 3 3
