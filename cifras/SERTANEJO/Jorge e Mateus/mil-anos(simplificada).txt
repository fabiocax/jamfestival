Jorge e Mateus - Mil Anos

(intro) Am F C G Am F

(primeira parte)
Am                               F
Você fala demais, diz muitas besteiras da boca pra fora
      C                         G                      Am   F
Mas sei que se arrepende por jogar o que não era lixo fora

(segunda parte)
       C                G             Am              F
Você briga, me ignora, pisa na bola, sente saudades chora
 Dm                        F                G
Diz que é de raiva pra não assumir que me adora
     C                                 G
Eu sei que um grande amor não é facil de se encontrar
 Am                         F
Mais difícil ainda é deixar de gostar
       Dm                  Bb                      G
Se o clima ta pesado não existe pecado que não se possa perdoar
     C                      G
Vai ver a gente nao conhece o amor direito

    Am                       F          Dm            Bb
Prazer eu sou um cara cheio de defeitos igualzinho aquele
               G
Que você aprendeu a amar

(refrão)
             F                   G                           Am
Mesmo que o Sol se apague vem a Lua te trazer de volta aos sonhos meus
         F                 G                            Am
Pode passar mil anos você vai me amar e eu vou ser pra sempre seu
             F                   G                          Am
Mesmo que o Sol se apague vem a Lua te trazer de volta aos sonhos meus
         F                 G                            Am
Pode passar mil anos você vai me amar e eu vou ser pra sempre seu

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
