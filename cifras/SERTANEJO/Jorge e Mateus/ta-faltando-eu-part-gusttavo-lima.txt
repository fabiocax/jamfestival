Jorge e Mateus - Tá Faltando Eu (part. Gusttavo Lima)

[Intro]  E  B9  C#m  A9  E  B9  C#m  A9  G#  F#  E

E|--------------------------------------------|
G|-----9-7-------12--9---------10--9-7-5--9---|
B|--------------------------------------------|
D|-7/9---7--7/12-12--9---7/11--11--9-7-6--9---|
A|--------------------------------------------|
E|--------------------------------------------|

E|--------------------------------------------|
G|-----9-7-------12--9---------10--9-7-5------|
B|--------------------------------------------|
D|-7/9---7--7/12-12--9---7/11--11--9-7-6------|
A|--------------------------------------------|
E|--------------------------------------------|

 E          B9        C#m
Tá faltando eu em mim
            A            E
Pergunto, mais não sei quem sou
         B9               C#m
Não sei se é bom ou se é ruim

              A
Quero ficar, não sei se vou

 E           B9             C#m
Sou doce e amargo ao mesmo tempo
            A
Me policio sem razão
 E               B9      C#m
Razão é o tipo que invento
                 A  G#  F#m     E
Pra não cair na palma da mão

 E        B9        C#m
Tá faltando mais ação
         A
Pra encarar e não fugir
E        B9          C#m
A lava que já foi vulcão
         A              E
É um iceberg dentro de mim

 E        B9           C#m
Pegadas que se tornam areia
          A               E
Castelos de areia sempre caem
           B9              C#m
Se olham pra mim de cara feia
            A  G#  F#m     E
Meu coração desaba num ai

  E      B9            C#m
Preciso me curtir bem mais
        A                E
É pena que só olho pros lados
               B9            C#m
Se a alma quer um banho de sais
              A            E
O corpo quer me ver apaixonado

         B9       C#m
O medo aguça atração
           A      E
A solidão na pele arde
             B9            C#m
Espero que quando eu me ver
       A
E acordar não seja tarde

( E  B9  C#m  A )

E          B9        C#m
Tá faltando eu em mim
            A            E
Pergunto, mais não sei quem sou
         B9               C#m
Não sei se é bom ou se é ruim
              A
Quero ficar, não sei se vou

 E           B9             C#m
Sou doce e amargo ao mesmo tempo
            A
Me policio sem razão
 E               B9      C#m
Razão é o tipo que invento
                 A  G#  F#m     E
Pra não cair na palma da mão

 E        B9        C#m
Tá faltando mais ação
         A
Pra encarar e não fugir
E        B9          C#m
A lava que já foi vulcão
         A              E
É um iceberg dentro de mim

 E        B9           C#m
Pegadas que se tornam areia
          A               E
Castelos de areia sempre caem
           B9              C#m
Se olham pra mim de cara feia
            A  G#  F#m     E
Meu coração desaba num ai

  E      B9            C#m
Preciso me curtir bem mais
        A                E
É pena que só olho pros lados
               B9            C#m
Se a alma quer um banho de sais
              A            E
O corpo quer me ver apaixonado

         B9       C#m
O medo aguça atração
           A      E
A solidão na pele arde
             B9            C#m
Espero que quando eu me ver
       A
E acordar não seja tarde

----------------- Acordes -----------------
A = X 0 2 2 2 0
A9 = X 0 2 2 0 0
B9 = X 2 4 4 2 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
G# = 4 3 1 1 1 4
