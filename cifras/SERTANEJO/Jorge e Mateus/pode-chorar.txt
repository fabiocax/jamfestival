Jorge e Mateus - Pode Chorar

[Intro] C  G  Am  F  Dm  Am  G
        C  G  Am  F  Dm  Am  G

[Primeira Parte]

  C                    G
Quase que acabo com a minha vida
 Am                   F
Você me pôs num beco sem saída
      C             G
Por que fez isso comigo?

 C                  G
Me dediquei somente a você
 Am                       F
Tudo que eu podia, eu tentei fazer
     C            G
Mas não, não adiantou

[Segunda Parte]


                        F
Você não sabe o que é amar
                        Am
você não sabe o que é amor
                    F
Acha que é somente ficar, ficar, ficar
Dm             G
E se rolar, rolou

                       F
Não se maltrata o coração
                       Am
De quem não merece sofrer
                      F
Não vou ficar na solidão, de mão em mão
Dm              G7
  Assim como você

[Refrão]

         C
Pode chorar
                        G
Mas eu não volto pra você
         Am
Pode chorar
                       F
Você não vai me convencer
         Dm
Pode chorar
                             Am          G
Você se lembra o quanto eu chorei por você

         C
Pode chorar
                        G
Mas eu não volto pra você
         Am
Pode chorar
                       F
Você não vai me convencer
         Dm
Pode chorar
                             Am          G
Você se lembra o quanto eu chorei por você

( C  G  Am  F  Dm  Am  G )

[Primeira Parte]

  C                    G
Quase que acabo com a minha vida
 Am                   F
Você me pôs num beco sem saída
      C             G
Por que fez isso comigo?

 C                  G
Me dediquei somente a você
 Am                       F
Tudo que eu podia, eu tentei fazer
     C            G
Mas não, não adiantou

[Segunda Parte]

                        F
Você não sabe o que é amar
                        Am
você não sabe o que é amor
                    F
Acha que é somente ficar, ficar, ficar
Dm             G
E se rolar, rolou

                       F
Não se maltrata o coração
                       Am
De quem não merece sofrer
                      F
Não vou ficar na solidão, de mão em mão
Dm              G7
  Assim como você

[Refrão]

         C
Pode chorar
                        G
Mas eu não volto pra você
         Am
Pode chorar
                       F
Você não vai me convencer
         Dm
Pode chorar
                             Am          G
Você se lembra o quanto eu chorei por você

         C
Pode chorar
                        G
Mas eu não volto pra você
         Am
Pode chorar
                       F
Você não vai me convencer
         Dm
Pode chorar
                             Am          G
Você se lembra o quanto eu chorei por você

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
