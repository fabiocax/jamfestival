Jorge e Mateus - Cachaça Com Limão

F                             C                                  F
Bão,Bão,Bão,Bão Cachaça com Limão, Cachaça com Limão Cachaça com Limão
                             C                    Bb                  F
Bão,Bão,Bão,Bão Cachaça com Limão enquanto agente bebe agente passa a mão

F                   Bb         C
A festa começou uma dose outra vez
                                               F
Enquanto eu estou bebendo mais de dez mulheres peguei
        F7           Bb                       C                          F
Aqui em Bh o clima é assim enquanto eu bebo e canto você traz mulher pra mim

F                             C                                  F
Bão,Bão,Bão,Bão Cachaça com Limão, Cachaça com Limão Cachaça com Limão
                             C                    Bb                  F
Bão,Bão,Bão,Bão Cachaça com Limão enquanto agente bebe agente passa a mão

F                                            C
Eu já estou meio esquisito, estou vendo meio embaçado
                                       F
Nem consigo enxergar quem estar do meu lado

                       F7                    Bb
Mais agora eu estou que estou e o trem ficou bom
               C                     F
Desse mais uma cachaça misturado com limão

F                             C                                  F
Bão,Bão,Bão,Bão Cachaça com Limão, Cachaça com Limão Cachaça com Limão
                             C                    Bb                  F
Bão,Bão,Bão,Bão Cachaça com Limão enquanto agente bebe agente passa a mão

F                                          C
De Segunda a Sexta-Feira trabalho igual um animal
                                      F
E no Sábado e Domingo bebo até passar mal
   F7                                     Bb
As minas já estão falando está na hora de parar
               C                                 F
Mais enquanto eu tiver em pé eu não vou parar de tomar

F                             C                                  F
Bão,Bão,Bão,Bão Cachaça com Limão, Cachaça com Limão Cachaça com Limão
                             C                    Bb                  F
Bão,Bão,Bão,Bão Cachaça com Limão enquanto agente bebe agente passa a mão

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
