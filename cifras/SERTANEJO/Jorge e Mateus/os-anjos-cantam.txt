Jorge e Mateus - Os Anjos Cantam

[Intro]

[Primeira Parte]

 C#m
No primeiro instante
       E
Vi que era amor
 B
No momento em que
         F#
A gente se encontrou
 C#m
No segundo instante
       E
Vi que era você
    B
Eu já te amo tanto
        F#
Sem te conhecer


[Segunda Parte]

C#m
    É que nos meu sonhos você era linda
E
  Pessoalmente é mais linda ainda
B
  Nosso amor veio de outras vidas
    F#
Eu vou te amar

Nas outras vidas que virão

C#m
    É que você nasceu pra ser minha
E
  Vamos dividir uma casinha
B
  Uma cama, dormir de conchinha
 F#
Deus abençoou a nossa união

[Refrão]

                          C#m
E os anjos cantam nosso amor
       E        B     F#
Oh uoh   oh uoh   oh uoh
                          C#m
E os anjos cantam nosso amor
       E        B     F#
Oh uoh   oh uoh   oh uoh

( C#m  E  B  F# )

[Primeira Parte]

 C#m
No primeiro instante
       E
Vi que era amor
 B
No momento em que
         F#
A gente se encontrou
 C#m
No segundo instante
       E
Vi que era você
    B
Eu já te amo tanto
        F#
Sem te conhecer

[Segunda Parte]

C#m
    É que nos meu sonhos você era linda
E
  Pessoalmente é mais linda ainda
B
  Nosso amor veio de outras vidas
    F#
Eu vou te amar

Nas outras vidas que virão

C#m
    É que você nasceu pra ser minha
E
  Vamos dividir uma casinha
B
  Uma cama, dormir de conchinha
 F#
Deus abençoou a nossa união

[Refrão]

                          C#m
E os anjos cantam nosso amor
       E        B     F#
Oh uoh   oh uoh   oh uoh
                          C#m
E os anjos cantam nosso amor
       E        B     F#
Oh uoh   oh uoh   oh uoh

                          C#m
E os anjos cantam nosso amor
       E        B     F#
Oh uoh   oh uoh   oh uoh
                          C#m
E os anjos cantam nosso amor
       E        B     F#
Oh uoh   oh uoh   oh uoh

                          G#m
E os anjos cantam nosso amor

----------------- Acordes -----------------
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
