Jorge e Mateus - Paixão Goiana

Intro: G D G D G
G
Não tem base e nem quantia,
                    D
Não tem dia nem semana,
                                C       G
Para eu cair nos braços da minha paixão goiana.
                                        D
Em trindade conheci, um encanto de menina,
                        G
Nascida em Goiatuba, criada em Pontalina,
                              G7              C
Ao me apaixonar por ela, foi grande meu desengano,
                G                             D
Vencido pela saudade, procurei em todas as cidades,
                        G
Do centro-oeste goiano.

(Intro)

G
Não tem base e nem quantia

                           D
Não tem dia nem semana
                                        C       G
Para eu cair nos braços da minha paixão goiana.
                                      D
Procurei minha paixão, na querida itajá,
                           G
Em mineiros e caçu, bom jesus e iporá,
                        G7              C
Rio verde e jataí, doverlândia e caiapônia,
                 G                            D
Do saudoso paulo lopes, santa helena e quirinópolis,
                   G
Aragarças e britânia.

(Intro)

G
Não tem base e nem quantia,
                     D
Não tem dia nem semana,
                                  C       G
Para eu cair nos braços da minha paixão goiana.
                                   D
Procurei em aporé, em itarumã passei,
                               G
Em jussara ouvi notícias, dos olhos que eu adorei,
                     G7                  C
Em bom jardim terminou, o meu rodeio tirano,
                    G                           D
E voltei para itumbiara, trazendo a jóia mais cara,
                      G
Do centro-oeste goiano.

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
