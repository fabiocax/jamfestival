Jorge e Mateus - Te Amo Tanto Que Nem Sei

(intro 2x) C C4

    C
No dia em que nos conhecemos
   C                   G
Alguma coisa em mim mudou
   Dm                 G
Senti estar amanhecendo
   F              C
A noite da desilusão

 C
O dia se encheu de flores
      C7               F
E o céu escuro se abriu
    Fm                       Em
E o sol que me embalou nos braços
   Am                   Dm
Achei calor em seu abraço
      G                C      ( F C C#° )
Meu pobre coração sorriu


(parte 2)
   Dm                          G
A luz que reluziu nos olhos meus
    Em                        Am
Brilhou e refletiu nos olhos seus
      Dm                       G
Foi muito mais do que eu imaginei
Dm        G                 C    ( F C )
Amor te amo tanto que nem sei

                      C
Te amo tanto que nem sei
                      G
Fica dificil de explicar
                       Dm
Faltam palavras pra dizer
                    G
Só não dá pra esconder
                   C   ( F C )
A paixão em meu olhar

(refrão)
                    C
Te amo tanto que perdi
                   G
O jeito de me declarar
                      Dm
Estava cego pro  amor
          G                           C  ( F C C#º )
Quando te vi eu não sabia o que era amar

(parte 2)

(refrão)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C#° = X 4 5 3 5 3
C#º = X 4 5 3 5 3
C4 = X 3 3 0 1 X
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
