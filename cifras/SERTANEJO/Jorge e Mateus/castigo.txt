Jorge e Mateus - Castigo

Intro: C G Am F G C G7 Am F Em G C
E|----------------------------------------------------------------------------------
B|---------------------------5-8-5------5-8-5---------------------------------------
G|----------5-5--------7-7--5------7-5-7-----5--------------------------------------
D|---3/5-5-5----3/5-5-5-------------------------------------------------------------
A|----------------------------------------------------------------------------------
E|----------------------------------------------------------------------------------

E|----------------------------------------------------------------------------------
B|--------------------------------8h10-8--------------------------------------------
G|-----------9-9-7--------10-10-9--------10-999-------------------------------------
D|---10-10-10-----12-12-12----------------------------------------------------------
A|----------------------------------------------------------------------------------
E|----------------------------------------------------------------------------------

             C
To sentindo raiva nesse seu olhar
                                G
Que jeito mais bobo de me castigar
F                                 C
Eu sei q fui grosso mais saiu assim

                Am                 G
É sempre que você não acredita em mim
              F       G               C
E me manda embora dizendo q não rola mais

C
Sei que não sou santo, nunca fui um anjo, mais nao sou ruim
                                          G
E quando eu amo todo meu amor, quero ela só pra mim
F                             C
Quando você quer você me abandona
                  Am                G
Me deixa o pó do caco me manda pra lona
               F           G            C
Ai eu canto e bebo pra tentar te esquecer

G                               C
Eu já perdi o jogo, já perdi o norte
                                G
Sei q no amor também não to com sorte
             F        G              C
To roendo o toco to rindo pra não chorar
 G                            C
Nessa indiferença paixão dividida
            Am             G
Agora cada um segue a sua vida
              F             G           C
De hoje em diante e eu pra cá você pra lá

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
