Jorge e Mateus - Me Virando

Intro 2x:  C  G  Am  F

 C                   G
A gente passa
              Am                                  F
Por várias situações em um só dia
  C                   G                   Am   F
Primeiro beijo, aí depois briga
C                  G                      Am   F
Se ama e também faz intriga

 C                     G             Am
A gente forma um casalzinho
                     F
Assim meio sem juízo
C               G                Am  F
Você mimada e atrevida
C                 G               Am  F
E eu tô nem aí pra vida

Dm                                F
Se me agride eu te vejo

                                                  C
Me maltrata, eu sempre esqueço
                                  G
E me vejo novamente
                                Dm
Preso em seus abraços

Dm                                        F
Tem gente que vira lembrança
                                          C
Outros que viram passado
                                       G
Eu tô sempre me virando
                                Dm     G
Pra "tá" do seu lado

Refrão:
 C
Seria pior não ter nunca mais
G                                                      Dm
O seu mal humor me tirando a paz
             F
Mas cinco minutos depois...
  C
Te provoco risos, te beijo a força
    G                                           Dm
Mais uma vez você me perdoa

 F                        G                 C
Diz eu te amo com voz rouca

(   C    G    Dm    F   )

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
