Jorge e Mateus - Se Disser Bye Bye

Intro: F#m D A E

F#m                                                    E
É eu acho que a saudade me encontrou
                                             F#m
E fez eu relembrar o nosso amor
                                                    E
Não sei mais controlar meu coração
                                              Bm
Que quando escuta uma ligação
                           E
Pensa que é você
              Bm
Não tem jeito é paixão
                               E
Não dar pra esconder

                       D                         A
Não te encontrei em nenhum lugar
                     E                  F#m
Me diz onde é que você está

                            D                         A
Um segundo encontro para conversar
                               E          F#m
Eu tenho sonhos pra realizar

                                      D
Eu tô tentando te encontrar
                                            E
Mas, não consigo achar você
                                        D
E não me canso de esperar
                                    E
Mas se quiser pode dizer

Refrão:
         D                           A
Bye bye, e se disser bye bye
              E                F#m
Eu paro de correr atrás
                D                          A
E se disser tchau, que não vai rolar
              E                        F#m
Vou aceitar que não dá mais

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
