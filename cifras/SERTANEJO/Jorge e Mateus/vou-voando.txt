Jorge e Mateus - Vou Voando

Intro: F Am G C

F                Am
Por que você não vem?
                     G      C
E deita aqui do meu lado

F                Am
Acende a luz a noite
              G         C
Vamos ficar acordados

F               Am
Voar no mar de estrelas
                  G      C
E desvendar o infinito

F               Am
Se embriagar de amor
                     G      C
Dizer o que nunca foi dito

F
Dias que não voltam mais

Tempo que ficou pra trás
Am
Fotos e recordações

Primaveras e verões
F
Juras de amor sem fim
                   G
Sem você tá tão ruim
C
Amanheceu
         G
E no meu sonho teu sorriso me chamando
        Dm
Eu vou voando
           F
E eu vou voando

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
