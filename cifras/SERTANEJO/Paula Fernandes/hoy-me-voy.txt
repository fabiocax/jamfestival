﻿Paula Fernandes - Hoy Me Voy

Capo Casa 1

Participação: Juanes

Intro: F Dm C

F          Dm                        C
Lembrarás, lembrarás quando se for o sol
    Dm              E7             F
Que eu te amei, mas deixo sua vida agora
F          Dm                        C
Te dirán, te dirán las flores del jardin
    Dm              E7             F
Cuanto te amé y cuanto soñé tu luz
F         G         Am        F
Devolva-me esse coração que você não soube amar
              Dm
E o que eu te dava, amor
        C
Sem pensar

          G                  Am
Devuélvelo, devuélvelo no es tuyo no

F                 E7                   F
Ya se acabó el tiempo en que tú brillabas
          G         Am        F
Devolva-me esse coração que você não soube amar
               Dm         C
E o que eu te dava se acabou


C9                              G
Hoy me voy pero no, no se va la herida grande que me queda
 F                         Dm            F             G
Por amarte, por mi culpa, por los besos que en el mar te di
C9                 G
Coração que sangrou e coragem me faltou para deixar-te
 F                         Dm            F      G
Tive medo, fui covarde, é por isso que de ti me despeço
 F
Hoy me voy
 Fm        C
Hoy me voy

          G         Am        F
Devolva-me esse coração que você não soube amar
               Dm         C
E o que eu te dava se acabou

C                              G
Hoy me voy pero no, no se va la herida grande que me queda
 F                         Dm            F      G
Por amarte, por mi culpa, por los besos que en el mar te di
C                              G
Corazón me sobró y coraje me faltó para dejarte
 F                         Dm            F      G
Tive medo, fui covarde, é por isso que de ti me despeço
F
Hoy me voy
Fm
Hoy me voy
C
Hoy me voy
        G
E me despeço
F
Hoy me voy
Fm      C
Hoy me voy

----------------- Acordes -----------------
Capotraste na 1ª casa
Am*  = X 0 2 2 1 0 - (*A#m na forma de Am)
C*  = X 3 2 0 1 0 - (*C# na forma de C)
C9*  = X 3 5 5 3 3 - (*C#9 na forma de C9)
Dm*  = X X 0 2 3 1 - (*D#m na forma de Dm)
E7*  = 0 2 2 1 3 0 - (*F7 na forma de E7)
F*  = 1 3 3 2 1 1 - (*F# na forma de F)
Fm*  = 1 3 3 1 1 1 - (*F#m na forma de Fm)
G*  = 3 2 0 0 0 3 - (*G# na forma de G)
