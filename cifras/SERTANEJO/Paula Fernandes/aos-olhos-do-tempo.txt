Paula Fernandes - Aos Olhos do Tempo

Intro: C G Am F

    C
Pensei em palavras que trouxessem você
         Am                 F
Palavras bonitas, de certo aflitas
         G
Perdendo a razão pra te ver
           C
Num tom de romance
           G
Uma voz que entoasse
          Am
No embalo do sonho profundo
          F
Pressa de te ter
          C
Palavras vividas
          G
Quem sabe em outras vidas
            Am
Num passe de mágica

          F
Então poderia ser
              C
Alguém que te inspira
          G
O ar que respira
            Am
Juro, eu lhe faria viver
          F
Outro grande amor
          C
Seria de novo
             G
Bem mais que vontade
            Am
A necessidade que o tempo
    F
Não apagou
    Am              F
O amor aos olhos do tempo
      G
Não se vai
    Am          F
Meu amor a cada momento
       G
Aumenta mais

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
