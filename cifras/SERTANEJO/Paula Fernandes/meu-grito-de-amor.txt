Paula Fernandes - Meu Grito de Amor

Intro: C  G/B  A7  D7  G  C  G  C/D

       G      C         G
Me dei demais, não fui ca...paz
C           G  C G          D
De tomar conta de mim , e amei assim
C        G      C            G
Only the passion, out of control
C                G  C G                D  C/D
Over before if began , and it was the end
C                   D
 Mergulhei nas incertezas do amor
       G     B7   Em
Fui um louco sonhador
            C      D                   G C    G
I was just dreaming, it was only me in lo...ve

      Em       Bm        C                 G    D9/F#
Dói demais o silêncio, e o vazio frio dos lençóis
      Em            Bm       C
Dói demais quando a noite, invade o nosso quarto

           D           C        G  C  G
E traz no vento,o meu grito de amor
      Em       Bm        C                 G    D9/F#
Dói demais o silêncio, e o vazio frio dos lençóis
      Em            Bm       C
Dói demais quando a noite, invade o nosso quarto
           D            C/D      G   D  E   D E   A
E traz no vento, o meu grito de amor

D        A   D          A
Me dei demais, não fui capaz
D            A D A       E
De conta de mim, e amei assim
D         A     D            A
Only the passion, out of control
D                A D A                E
Over before if began, and it was the end
D                   E
 Mergulhei nas incertezas do amor
        A    C#7  F#m
Fui um louco sonhador
            D      E                   A  D  A
I was just dreaming, it was only me in lo...ve
      F#m      C#m       D                 A    E
Dói demais o silêncio, e o vazio frio dos lençóis
      F#m           C#m     D
Dói demais quando a noite invade o nosso quarto
           E            Bm        A
E traz no vento, o meu grito de amor
D                   A
     O meu grito de amor
D           E       A
     O meu grito de amor

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C#7 = X 4 3 4 2 X
C#m = X 4 6 6 5 4
C/D = X X 0 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
D9/F# = 2 X 0 2 3 0
E = 0 2 2 1 0 0
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
