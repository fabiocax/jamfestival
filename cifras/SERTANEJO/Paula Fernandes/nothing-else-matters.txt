Paula Fernandes - Nothing Else Matters

Em                          D      C
So close no matter how far
Em                                D           C
Couldn't be much more from the heart
Em                        D      C
Forever trusting who we are
G       B7                Em
And nothing else matters

Em                       D         C
Never opened myself this way
Em                             D    C
Life is ours, we live it our way
Em                              D    C
All these words I don't just say
G       B7                Em
And nothing else matters

Em                        D       C
Trust I seek and I find in you
Em                       D            C
Every day for us something new

Em                      D          C
Open mind for a different view
G   B7           Em         C   Am
And nothing else matters

D                                     C
Never cared for what they do
D                                       C
Never cared for what they know
D         Em
But I know

(repete tudo)
D                                      C
Never cared for what they say
D                                         C
Never cared for games they play
D                                       C
I never cared for what they do
D                                         C
I never cared for what they know

D          Em
And I know

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
