﻿Paula Fernandes - Apaixonada Por Você

Capo Casa 1

Intro: E  B7  E  B7  E  B7  E  B7  E

 E
Apaixonada por você
                     B7  A
E dessa vez o trem pegou
A                       E
Eu fui seguindo a tua trilha
                 B7
Fui cair numa armadilha
                   E   B7 E
Estou presa pelo teu amor
E
Apaixonada por você
                          B7       A
E dessa vez não vai ter jeito não
                      E
Quando me beija e me abraça
                   B7
Feito um gole de cachaça
                    E E7 A
Me embriaga o coração

                        E
Quando me beija e me abraça
                    B7
Feito um gole de cachaça
                    E B7
Me embriaga o caração

Refrão:
E                    B7
Ai,ai,ai o amor apareceu

Tava procurando ele
       A          E
E ele procurando eu
                        B7
Ai,ai,ai balançou virou mexeu

O amor desse menino
      A            E  B7 E
Bagunçou o peito meu

E
Apaixonada por você
                     B7 A
E você doidinho por mim
                     E
Feito um fogo que pegou
                      B7
Quando agente se encontrou
               E   B7 E
Acendeu um estopim

Apaixonada por você
                        B7  A
Eu vou cantando essa cançao
                    E
Sou feliz é desse jeito
                 B7
Com a viola no peito
                E E7 A
E você no coraçao
                    E
Sou feliz é desse jeito
                B7
Com a viola no peito
               E B7
E você no coraçao

----------------- Acordes -----------------
Capotraste na 1ª casa
A*  = X 0 2 2 2 0 - (*A# na forma de A)
B7*  = X 2 1 2 0 2 - (*C7 na forma de B7)
E*  = 0 2 2 1 0 0 - (*F na forma de E)
E7*  = 0 2 2 1 3 0 - (*F7 na forma de E7)
