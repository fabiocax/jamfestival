Paula Fernandes - Diga

B9
O que será de mim se não te tenho?
G#m
Me faz tão mal querer-te ao meu lado então
F#m
Necessito escutar a voz do vento
A9
Soar teu nome sobre o meu corpo

B9
Sem razão de amar assim na vida
G#m
Junto a ti deixei morar meu coração
F#m
Quando chegar a noite e eu pedir a Deus
A9
Um desejo incontrolável

B9
Diga, diga, diga que me ama
G#m
Que esse amor não é só uma promessa

F#m
Sinta apossar o amor que eu sinto por ti
A9
Diga pro meu coração

B9
Diga, diga que sempre estarás aqui
G#m
E que não perderás o que sou para ti
F#m
Valeu a pena dar meu todo nesse jogo
A9
Muito mais que uma vontade

E                      B9
Chega quando menos se espera
F#m                      A9
Esse amor será sempre assim

B9
Me senti sou como um passageiro
G#m
Que deixou sua vida em outra estação
F#m
A espera de um trem que vai lento
A9
Num suspiro incontrolável

E                        B9
Chega quando menos se espera
F#m                 A9
Esse amor será sempre assim

B9
Diga, diga, diga que me ama
G#m
Que esse amor não é só uma promessa
F#m
Sinta apossar o amor que eu sinto por ti
A9
Diga pro meu coração

B9
Diga, diga que sempre estarás aqui
G#m
E que não perderás o que sou para ti
F#m
Valeu a pena dar meu todo nesse jogo
A9
Eu preciso desse amor

B9
Dime dime que me amas
G#m
Dame alguna esperanza
F#m
sinto que darlo poco
A9
Que es preciso por tu amor

B9
Diga, diga que sempre estarás aqui
G#m
E que não perderás o que sou para ti
F#m
Valeu a pena dar meu todo nesse jogo
E
Eu preciso desse amor

B9
Diga, diga, diga que me ama
G#m
Que esse amor não é só uma promessa
F#m
Sinta apossar o amor que eu sinto por ti
A9
Diga pro meu coração

B9
Diga, diga que sempre estarás aqui
G#m
E que não perderás o que sou para ti
F#m
Valeu a pena dar meu todo nesse jogo
A9
Eu preciso desse amor

----------------- Acordes -----------------
A9 = X 0 2 2 0 0
B9 = X 2 4 4 2 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
