Paula Fernandes - Música e Rodeio

(intro)  A D  A D

          A
Já vai começar
                    F#
Todo mundo vai saber
            D
Nessa arena
                            E
Vejo a festa acontecer
                       A
Então vem dançar
                      F#
Ta pintando um novo som
                    D
Um Country music
                           E
Conta a vida do peão

E                                        D
Basta segurar, sonhar, persistir

                     A                            E
Deixa a harmonia do universo invadir
E                               D
E pra te guiar uma oração
                  A                  E                    A
Pra Nossa Senhora te seguir meu campeão

 D                           Bm
Som que contagia os sonhos do mundo
                       F#                                   D
Traz prazer pra quem tem um amor profundo
                        E                       Bm
Essa é a minha vida, música e rodeio
              D            E                   A
Em toda festa de peão eu to no meio

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
