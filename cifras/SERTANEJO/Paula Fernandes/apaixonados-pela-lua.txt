Paula Fernandes - Apaixonados Pela Lua

              D
Ontem tive um sonho
          A
Caminhamos entre as nuvens do céu
             Em
Desenhamos lembranças de nós dois
                G        A
Envolvidos no azul do véu

             D
Não ficaremos juntos
                    A
Se não soubermos a arte de amar
                  Em
Esse segredo tão bonito do caminho
                  G                 A
Hieroglifos de um livro para decifrar

               D
Na viagem desse sonho
                    A
Nossas almas eu vi flutuar

                Em
Nas delicadas linhas do infinito
                   G                    A
Fomos filhos de um romance de um amor lunar

                 D
Apaixonados pela lua, lua , lua
              A
Cheia de mistérios
                    Em
Nos finos grão de areia, mente branca
           G                        A
Segredo e solidão em seus hemisférios

(Repete Tudo)

(Refrão 2x)

                 D     D   D
Apaixonados pela luaaaa,

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
