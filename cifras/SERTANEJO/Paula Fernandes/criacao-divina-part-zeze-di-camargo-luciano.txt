Paula Fernandes - Criação Divina (part. Zezé Di Camargo & Luciano)

A             E               F#m
Deus criou o universo em harmonia
Bm                A               E
Fez do Sol que dá calor aos nossos dias
     A          G             F#
Fez a Lua pra servir de inspiração
          Bm           A           E
E a chuva pra matar a sede do sertão

       A           E              F#m
Fez o rio só pra se encontrar com o mar
   Bm            A               E
Deu o dom a minha voz e o meu cantar
      A         G          F#
Fez a árvore, o fruto e a flor
   Bm         E               A
Fez o homem, a mulher e fez o amor

A        E       Bm
E fez você pra mim
     D                E            A
Obra prima de um artista sonhador

A           E       Bm
Deus fez você pra mim
D                 E                 A  E  F#m  Bm  A  E
Criação divina que eu ganhei do criador

       A            E              F#m
Deus criou pra cada um seu próprio dom
        Bm          A             E
E junto com cada talento, uma missão
         A                   G          F#
Pra uns a força, astúcia, a fama e o poder
           Bm                    E
Pra outros deu a inquietude do saber

    A             E                 F#m
Da pedra fez o diamante, a prata, o ouro
      Bm          A           E
E deu pra cada criatura um tesouro
         A         G          F#
Ele guardou a joia de maior valor
          Bm             E         A
Deu de presente pra este humilde cantador

A        E       Bm
E fez você pra mim
      D          E            A
Obra prima de um artista sonhador
A           E       Bm
Deus fez você pra mim
     D                    E          A
Criação divina que eu ganhei do criador

A        E       Bm
E fez você pra mim
      D           E            A
Obra prima de um artista sonhador
A           E       Bm
Deus fez você pra mim
     D                    E          A  F#m Bm E A
Criação divina que eu ganhei do Criador

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
