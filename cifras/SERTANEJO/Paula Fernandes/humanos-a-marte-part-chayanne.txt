Paula Fernandes - Humanos a Marte (part. Chayanne)

 F#m                    D
Te quero como nunca quis antes
    A                     E
Te quero porque é tão natural
 F#m                    D
Porque falamos como amantes
    A                     E
Não penso, em tudo sou anormal

           A
Y en mi soledad
        E                     F#m
Cuando quiera yo salir a buscarte
            D                  A
Cuando miras a la luna y no está
          E                   F#m
Cuando lleguen los humanos a Marte
        D               A
Mira, dejaré la vida pasar

        E                    F#m
Cuando tengas la intención de casarte

        D                   A
Cuando sepas que ya no puedo más
       E                F#m
Besarás con esa obra de arte
         D                   A
A éste loco que ya no puede más
E          F#m     D       A     E  F#m  D   A  E
   Uooah ahh!

F#m                       D
Te espero com amor de verdade
     A              E
E tudo que ele possa te dar
    F#m                     D
As flores, o carinho, a vontade
    A                   E
Contigo a vida inteira estar

          A
E vou confessar
       E                   F#m
Quando vejo meu amor tão distante
        D                  A
Olho o céu e nem a lua está
       E              F#m
Os humanos chegariam à marte
         D                  A
Eu na janela vendo a vida passar
          E                    F#m
Cuando tengas la intención de casarte
         D                  A
Cuando sepas que ya no puedo más
     E                   F#m
Besarás con esa obra de arte
         D                   A
A éste loco que ya no puede más
E          F#m     D       A     E  F#m  D   A
   Uooah ahh!

F#m                 D
Luz que guia o meu sentir
           A
E que me faz pulsar
                       E
Você sabe que esse amor me mata
F#m                 D
Diga que eu vou esperar
               A
Que é pra ela provar
              E
Esse amor de verdade

          A
E vou confessar
       E                   F#m
Quando vejo meu amor tão distante
        D                  A
Olho o céu e nem a lua está
       E              F#m
Os humanos chegariam à marte
         D                  A
Eu na janela vendo a vida passar
          E                    F#m
Cuando tengas la intención de casarte
         D                  A
Cuando sepas que ya no puedo más
     E                   F#m
Besarás con esa obra de arte
         D                   A
A éste loco que ya no puede más
E          F#m     D       A     E  F#m  D   A   E
   Uooah ahh!

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
