João Paulo e Daniel - Paloma

(intro 2x) A E B7 E

E
Assim como um dia triste não tem calor
B7                                  E
Estou me sentindo triste sem teu amor
E
Se foi para muito longe sem um adeus
B7                                   E
Partiu destruindo todos os sonhos meus

E                                B7
Voa paloma e vem devolver me a calma
                                       E
Traga o seu calor pra aquecer minha alma
                              B7
Velhas melancolias estão de volta
                                    E
Pois sem o teu amor nada mais importa
Onde quer que eu vá
                   B7
Estou sempre esperando


Pra dizer que apesar da tua ausência
              E
Sempre te amarei

E
Se foi melodia triste de uma canção
B7                             E
Deixou toda uma poesia de solidão
Voa Paloma....

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
