João Paulo e Daniel - Desatino

[Intro]  C

C                       F  C
  Em um quarto de hotel
                C     F  C
Loucamente apaixonado
Eu estou desesperado
              G          G4  G
Feito uma estrela sem o céu

                   Dm
Sei que já é madrugada
                    C  F  C
E meu sono que não vem
                G  G7
Será que me convém
                        C
Sair assim cortando estrada

             C            F  C
Conversando com a solidão

                  C           F  C
Pra encontrar um certo alguém
                C  C7
Será que me convém
                        F  F4
Ou tudo acabe dando em nada

   G             C
Se ontem eu por lá passei
                        G
Saudade me apertou mas não parei
Minhas mãos grudaram firmes no volante
            C
E o carro acelerei

                     C   F  C
Eu quis fugir do destino
               G   G4
Fugir da realidade
                  F
E sufocando a saudade
          G                      C
Aquela cidade fui deixando pra trás

                    C   F C
É que esse meu desatino
                   G    G4
E uma mulher envolvente
          F                  G
Amor diferente, olhar de serpente
            G7               C  F  C
E o doce veneno que me satisfaz

----------------- Acordes -----------------
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
F4 = 1 3 3 3 1 1
G = 3 2 0 0 0 3
G4 = 3 5 5 5 3 3
G7 = 3 5 3 4 3 3
