João Paulo e Daniel - Se Você Quiser Voltar

G
Me bateu uma vontade  de chorar.
                          D
Aparentemente sem explicação.
       Am              D       Am            D
Me deu um nó na garganta, um aperto aqui no peito.
       Am           D             G     G7
Eu tentei me controlar, não teve jeito.

        C
Foi talvez uma lembrança da infância,
     G
Uma dor dessas que ficam escondida.
         Am
Ou quem sabe veio à tona,
          D                       G   G7
Nosso história de amor mal resolvida.

        C
Algum trauma, algum medo muito grande,
     G
Um desejo, uma saudade reprimida.

      Am
E por mais que eu não queira,
      D                            G     G7
Pode ser a sua ausência em minha vida.

         C
Meu desespero quando você foi embora,
        G
Era preciso não deixar transparecer.
           D
É que ninguém tem nada a ver com meus problemas,
          G                        G7
E até de mim, eu consegui me esconder.

            C
Mas este homem forte, tão determinado,
          G
Que pode tudo e não aceita se entregar.
            D
Entrega os pontos abre os braços e a janela,
                   G
Se você quiser voltar.

D                  G
Se você quiser voltar.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
