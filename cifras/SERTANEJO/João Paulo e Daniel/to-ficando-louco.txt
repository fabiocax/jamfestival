João Paulo e Daniel - Tô Ficando Louco

(intro) G  D  C  G  C  G  D  G

G                         D
Ela diz que é cedo pra se amarrar
                G
E pede mais um tempo
                            D
Diz que por enquanto só quer namorar
                  G
Não pensa em casamento

G7                        C
Eu já não agüento mais ficar
D                          G
Nesse agarra-agarra no portão
 G7                          C
Quando vê que o bicho vai pegar
D                           G    D    G
Ela tira o corpo e diz que não, não, não...

       D                G
Ela só quer me ver no sufoco

            D                  G
Tô ficando louco, tô ficando louco (2x)

G                        D
Fico despeitado e saio por aí
              G
E entro na cachaça
                             D
Bebo a noite inteira e fico sem dormir
                 G
Mas a paixão não passa

G7                        C
Eu já não agüento mais ficar
D                          G
Nesse agarra-agarra no portão
  G7                          C
Quando vê que o bicho vai pegar
D                           G    D    G
Ela tira o corpo e diz que não, não, não...

        D                G
Ela só quer me ver no sufoco
            D                  G
Tô ficando louco, tô ficando louco (2x)

(repete tudo)
(último refrão 6x)

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
