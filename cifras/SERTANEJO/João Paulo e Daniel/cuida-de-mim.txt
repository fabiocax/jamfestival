João Paulo e Daniel - Cuida de Mim

[Intro]  G  Em  Am  D  Em  D/F#

       G  D/F#       Em
Outra vez   cheguei tarde
       C         Am         D  Em  D/F#
Você quer saber por onde andei
       G   D/F#    Em
Quase morto de saudade
      C         Am        D
Meu amor pra você eu guardei
 Em  D/F#  D  G   C  G
Meu co----ra-ção

Carregado de amor
        Em
Tendo que se ausentar
    D       C           G  C  G
Abatido só pensa em voltar
          D
Se estou longe daqui
       Em
Não importa o prazer

     D        C         G
Seduzido só penso em você

          D
Então se solta
               Em
Faça o que eu faço
            D
Me dá um abraço
 C        G   C  G
Cuida de mim
          D
Pra que ciúme?
          Em
Sou todo seu
            C
Vem pra beijar
            D       G
Quem nunca te esqueceu

( G  Em  Am  D  Em  D/F# )

       G  D/F#       Em
Outra vez   cheguei tarde
       C         Am         D  Em  D/F#
Você quer saber por onde andei
       G   D/F#    Em
Quase morto de saudade
      C         Am        D
Meu amor pra você eu guardei
 Em  D/F#  D  G   C  G
Meu co----ra-ção

Carregado de amor
        Em
Tendo que se ausentar
    D       C           G  C  G
Abatido só pensa em voltar
          D
Se estou longe daqui
       Em
Não importa o prazer
     D        C         G
Seduzido só penso em você

          D
Então se solta
               Em
Faça o que eu faço
            D
Me dá um abraço
 C        G   C  G
Cuida de mim
          D
Pra que ciúme?
          Em
Sou todo seu
            C
Vem pra beijar
            D       G
Quem nunca te esqueceu

          D
Então se solta
               Em
Faça o que eu faço
            D
Me dá um abraço
 C        G   C  G
Cuida de mim
          D
Pra que ciúme?
          Em
Sou todo seu
            C
Vem pra beijar
            D       C
Quem nunca te esqueceu

[Final]  G  C  G  D  G

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
