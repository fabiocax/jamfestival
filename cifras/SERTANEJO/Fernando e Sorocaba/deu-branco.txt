Fernando e Sorocaba - Deu Branco

     Cm
Me diz o que é que tá acontecendo
Será que colocaram alguma coisa na bebida
      G#
Que tá mudando o meu comportamento
E se eu cheguei sozinho ou com alguém
              D#               Bb
Eu nem lembro, já tô lento

Fm
Quem é essa pessoa aqui do lado
                                    Cm
Perguntando se eu tô louco
                                               Fm
Eu nem sabia que eu tinha enrosco
                                                G
Agora eu tô com a corda no pescoço

       G#                    Bb            Cm
Deu branco que eu tava namorando
                            G#
Peraí, amor, tô sarando

            Bb                Cm
Nunca mais eu vou beber
        G#                   Bb            Cm
Deu branco que eu tava namorando
                               G#
Peraí, amor, tô lembrando
                 Bb             Cm
Que eu tô junto com você

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
D# = X 6 5 3 4 3
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
