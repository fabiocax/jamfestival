Fernando e Sorocaba - Você É Mais

Intro 2x:
                D       G
E|---------------------3---------------|
A|-------------------------------------|
D|---0-------------------0-------------|
G|---2---------2-----------0-----------|
B|-----3---------3-----------3--2--0---|
E|-------5--0--2-----------------------|

(pode dedilhar a intro durante a estrofe)

D           G            D
Vento leva, mas não traz
             G                    D
Sentimentos que não retornarão jamais
           G                   D
Sou teu sul, você é meu norte
      A                  G                              D        (Intro 1x)
Só o teu coração que não vê que juntos eu e você somos bem mais fortes

Refrão:

F                       G                      D
Mas estou certo que um dia nosso sol vai brilhar
F                  G                D      A
Se o universo conspira nada pode mudar aaaah
A                 G
Como eu te quero nunca quis ninguém
                     G      A
Vem pra mim vem me amar aaaah
A                G                             Bm                         E
Te imaginei, nas madrugadas e hoje eu sei que você não é exatamente o que eu esperava
      D            A            D
Hoje eu sei, ah eu sei, você é mais

(Intro 2x)

D       G               D             G                       D
Tua flor tem espinhos, muitas pedras, teus caminhos tortos demais
           G         D        A          G             D       (intro 1x)
Tempestade de memórias é tão louca e imortal nossa historia

Refrão:
F                       G                      D
Mas estou certo que um dia nosso sol vai brilhar
F                  G                D      A
Se o universo conspira nada pode mudar aaaah
A                 G
Como eu te quero nunca quis ninguém
                     G      A
Vem pra mim vem me amar aaaah
    A            G                             Bm                         E
Te imaginei, as madrugadas e hoje eu sei que você não é exatamente o que eu esperava
     D             A            D
Hoje eu sei, ah eu sei, você é mais   (2x)

(Intro 2x)

Huum mais

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
