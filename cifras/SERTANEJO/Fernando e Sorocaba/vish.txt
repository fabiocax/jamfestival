Fernando e Sorocaba - Vish

Introdução 2x:

(D A E F#m )

D
Vish
               A
O que ta acontecendo?
                    E
Bagunçou tudo aqui dentro
                  F#m
Não consigo nem falar

D
Vish
                 A
Me pegou desprevenido
                     E
Nunca fiquei tão perdido
                F#m
Onde isso vai parar?


            D
To meio sem sentido
              E
Semana sem domingo
                 F#m
Palhaço sem o circo pra ver
               D
Programa sem ibope
               E
Cassino sem a sorte
                  F#m               D
Um sul sem ter o norte pra que?


Refrão:

    A            E
Me diz quem é você
              Bm
De onde você veio

Quem te autorizou a
         D
Brincar sem me conhecer

    A            E
Me diz quem é você
               Bm
Onde eu vou parar

Onde quer me levar
             D
Não pertenço a você


Solo:

(A E Bm D E)

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
