Fernando e Sorocaba - Volta Lá Pro Seu Lugar

(intro) D

         G                F#/D          A            G           F#/D         D     D4
E|------------------------------------------------------------------------2--2/3-------|
B|-----0------------3------------0-------------0-----------3-------------3-------3-----|
G|----2--2---------2---2--------2--2----------2--2-------2----2--------2-----------2---|
D|---0----0-------0-----0------2----2--------0----0-----0-------0----0------0--------0-|
A|----------------------------0-------0------------------------------------------------|


                   G
Volta lá pro seu lugar
                   F#/D
Eu não quero mais você
                    A
Minha vida foi te amar
                    G
Mas, não vou te enganar
         A           D    D4
Também posso te esquecer
                   G
Volta lá pro seu lugar

                     F#/D
Eu não quero mais sofrer
               A
E no jogo da paixão
               G
Mando no meu coração
        A        D  D4
Não preciso de você

(solo)

E|-------------------------7----------5----3---------------------------------|
B|------------------------------------------------------------------7--5---3-|
G|------------------4-4--5/7----------5----4---------------------------------|
D|----------------------------------------------------------5-5---5/7--5---4-|
A|----4/5--4/5--4/5----------------------------4/5-4/5-4/5-------------------|
E|---------------------------------------------------------------------------|

E|-----------------------------------------------------------------------------------------|
B|--3---5---7---8---7---5---7--8---12---10---8---7--5--5/7---5---3------------8--7---5---3-|
G|-----------------------------------------------------------------------------------------|
D|--4---5---7---9---7---5---7--9---12---10---9---7--5--5/7---5---4----------7/9--7---5---4-|
A|--------------------------------------------------------------------7---9----------------|
E|-----------------------------------------------------------------------------------------|

                    G   A
Quase que eu acreditei
                 D
Em tudo que me falou
                   A
Se faltou com a verdade
              G
Diga com sinceridade
                  D      D4
Que foi você quem errou

                  G     A
Quase que eu acreditei
                  D
Nas promessas de amor
               A
Vou sair da sua vida
                    G
Por favor, não me persiga
              A    D  D4
Eu não quero essa dor

(refrão)
 G
Volta lá pro seu lugar
                   D
Eu não quero mais você
                    A
Minha vida foi te amar
                    G
Mas, não vou te enganar
         A           D    D4
Também posso te esquecer
                   G
Volta lá pro seu lugar
                      D
Eu não quero mais sofrer
               A
E no jogo da paixão
               G
Mando no meu coração
        A        D  D4
Não preciso de  você

(solo) G  D  A  G  A  D  G  D

   G     A
Quase que eu acreditei
                  D
Nas promessas de amor
               A
Vou sair da sua vida
                    G
Por favor, não me persiga
              A    D  D4
Eu não quero essa dor.

(refrão)

(repete 2x essa parte)

                G     A
E no jogo da paixão
                D
Mando no meu coração
              A
Não preciso de você

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
D4 = X X 0 2 3 3
F#/D = X 5 4 3 2 2
G = 3 2 0 0 0 3
