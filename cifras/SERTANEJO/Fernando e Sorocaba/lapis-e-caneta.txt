Fernando e Sorocaba - Lápis e Caneta

Intro: A  E

       A                       E
Já consigo imaginar nós dois juntos
                Bm
E um cachorro danado no sofá
         D
Que não deixa a gente namorar

       A                      E
Já consigo imaginar você reclamando
                  Bm
Que na quarta meu time vai jogar
           D
E você torcendo pra luz acabar

       F#m
O engraçado é que
          E         D
Eu te conheço a tão pouco tempo


             Bm
Eu juro, não fiz de caso pensado
        D
Aquela lua e o céu estrelado
           A
Sua rua deserta, o silêncio no carro
              E                      Bm
E aquele suspense antes do beijo roubado
   E
Roubado

Refrão 2x:
A                                      E
O meu coração tinha amores escritos a lápis
                           Bm
Você apagou e escreveu com caneta
                    D
Te amo com todas as letras

A                                  E
O meu coração calejado, tão desconfiado
                               Bm
Olhou pra você e te quis de primeira
                    D
Te amo com todas as letras

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
