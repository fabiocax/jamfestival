﻿Fernando e Sorocaba - Nove Entre Dez

Capo Casa 6

[Intro] G  Em7  Bm7

[Primeira Parte]

        G
Um matemático
        Em                   Bm7
Disse que suas medidas são exatas
     G
Um crítico de artes
 Em7                    Bm7
Disse que você é obra prima

[Pré-Refrão]

   C9                            G/B
O jornaleiro disse que você só traz
        D9
Boas notícias
    C9                    G/B
Eu vou ficar seguro com você

           D9
Disse a polícia

[Refrão]

            C9            G
Nove entre dez especialistas
                 D9
Me recomendam você
            C9            G
Nove entre dez especialistas
                 D9
Me recomendam você

Eu demorei pra entender

[Segunda Parte]

        G
O jardineiro
                    Em7           D9
Disse que o seu perfume vem das flores
    G
O chef de cozinha
               Em7                     D9
Disse que seu beijo tem os melhores sabores

[Pré-Refrão]

  C9                            G/B
O astronauta disse que o seu abraço
               D9
Tem gravidade zero
    C9                              G/B              D
Os bilionários desse mundo, joia igual você nunca tiveram

[Refrão]

            C9            G
Nove entre dez especialistas
                 D9
Me recomendam você
            C9            G
Nove entre dez especialistas
                 D9
Me recomendam você

Eu demorei pra entender

[Ponte]

    C9                       G/B
E agora quem sou eu pra duvidar
                            D
Que o amor da minha vida é você (eu demorei pra entender)
    C9                       G/B
E agora quem sou eu pra duvidar
                            D
Que o amor da minha vida é você

[Refrão]

            C9            G
Nove entre dez especialistas
                 D9
Me recomendam você
            C9  G
Nove entre dez
                 D9
Me recomendam você

            C9  G
Nove entre dez
                 D9
Me recomendam você

Eu demorei pra entender

[Final] C9  G  D9
        C9  G  D9

----------------- Acordes -----------------
Capotraste na 6ª casa
Bm7*  = X 2 4 2 3 2 - (*Fm7 na forma de Bm7)
C9*  = X 3 5 5 3 3 - (*F#9 na forma de C9)
D*  = X X 0 2 3 2 - (*G# na forma de D)
D9*  = X X 0 2 3 0 - (*G#9 na forma de D9)
Em*  = 0 2 2 0 0 0 - (*A#m na forma de Em)
Em7*  = 0 2 2 0 3 0 - (*A#m7 na forma de Em7)
G*  = 3 2 0 0 0 3 - (*C# na forma de G)
G/B*  = X 2 0 0 3 3 - (*C#/F na forma de G/B)
