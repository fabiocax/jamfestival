Fernando e Sorocaba - Todo Canto

 D                    A                       G
Vendi o meu melhor cavalo porque ela montou nele
D                       A                     G
Vendi o meu chapéu e o laço porque ela usou eles

A                                      G
Eu vou me livrar do sofá e de todo lugar que a gente fez amor
D                                A               G  A
A cama, o rack, as cadeiras, a mesa, não adiantou

                         D
Como é que arranca as paredes?
           A
O chão da sala?
                 Bm                  A
Até na área, a gente não perdoou nada
                         D
Como é que arranca as paredes?
           A
O chão da sala?
           G                  A
A casa inteira eu tô doando


                         D
Como é que arranca as paredes?
           A
O chão da sala?
                 Bm                  A
Até na área, a gente não perdoou nada
                         D
Como é que arranca as paredes?
           A
O chão da sala?
           G
A casa inteira eu tô doando
           A                       D
Porque a gente se amou em todo canto

D                    A                       G
Vendi o meu melhor cavalo porque ela montou nele
D                       A                     G
Vendi o meu chapéu e o laço porque ela usou eles

A                                      G
Eu vou me livrar do sofá e de todo lugar que a gente fez amor
D                                A               G  A
A cama, o rack, as cadeiras, a mesa, não adiantou

                         D
Como é que arranca as paredes?
           A
O chão da sala?
                 Bm                  A
Até na área, a gente não perdoou nada
                         D
Como é que arranca as paredes?
           A
O chão da sala?
           G                  A
A casa inteira eu tô doando

                         D
Como é que arranca as paredes?
           A
O chão da sala?
                 Bm                  A
Até na área, a gente não perdoou nada
                         D
Como é que arranca as paredes?
           A
O chão da sala?
           G                A
A casa inteira eu tô doando

                         D#
Como é que arranca as paredes?
           A#
O chão da sala?
                 Cm                 A#
Até na área, a gente não perdoou nada
                         D#
Como é que arranca as paredes?
           A#
O chão da sala?
           G#
A casa inteira eu tô doando
           A#                       D#
Porque a gente se amou em todo canto

           G#
A casa inteira eu tô doando
           A#                       D#
Porque a gente se amou em todo canto

----------------- Acordes -----------------
A = X 0 2 2 2 0
A# = X 1 3 3 3 1
Bm = X 2 4 4 3 2
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D# = X 6 5 3 4 3
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
