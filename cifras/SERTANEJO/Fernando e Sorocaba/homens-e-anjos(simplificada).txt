Fernando e Sorocaba - Homens e Anjos

Intro : G D Em C (2x)

    G             D
Eu vivo no chão, ela tem asas
    Em               C
Sou a escuridão, ela é iluminada
G               D
Ela é tudo, eu sou quase nada
Em         C
Homens e anjos

G                   D
Sou tão comum, ela é diferente
Em                      C
Sou tão ciumento, mas anjos não mentem
G                    D
Morro por ela, ela vive pra gente
Em         C
Homens e anjos

(refrão)


G                    D                      Em
Um raio e um arcos iris, dividindo o mesmo céu
                   C                       G
Belezas tão diferentes, pintadas num só papel
                 D                       C
Vencemos o impossível com a força do coração
Am                       C
Parece estranho mas nós nos amamos
            G
Homens e anjos

(repete intro apenas 1 vez)

     G               D
Os nossos caminhos se encontravam
    Em                   C
Talvez foi destino, talvez o acaso
   G                  D
O doce o amargo, a pureza e o pecado
Em         C
Homens e anjos

(refrão)

G                    D                      Em
Um raio e um arcos iris, dividindo o mesmo céu
                   C                       G
Belezas tão diferentes, pintadas num só papel
                 D                       C
Vencemos o impossível com a força do coração
Am                       C
Parece estranho mas nós nos amamos
            G   D   C   C
Homens e anjos
           G
Homens e anjos
           D      Em    C
Homens e anjos   ooooooOOO
G                    D                      Em
Um raio e um arcos iris, dividindo o mesmo céu
                   C                       G
Belezas tão diferentes, pintadas num só papel
                 D                       C
Vencemos o impossível com a força do coração
Am                       C
Parece estranho mas nós nos amamos
            G   D
Homens e anjos


    G             D
Eu vivo no chão, ela tem asas
    Em               C
Sou a escuridão, ela é iluminada
G               D
Ela é tudo, eu sou quase nada
Em         C
Homens e anjos

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
