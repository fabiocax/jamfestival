Fernando e Sorocaba - Lápis de Cera

          C
E|-------------------------|
B|----------1-3-3h5--3-----|
G|------0---0-0-0h0--0-----|
D|----2---2----------------|
A|--3----------------------|
E|-------------------------|

        Em                      Am
E|-------X------X----|-----------------------|
B|-----0-X--0-0-X-0--|--1---1-3-3h5--3~------|
G|-----0-X--0-0-X-0--|--2---2-2-2h4--2~------|
D|-------X------X----|-----------------------|
A|---2---X--2---X-2--|--0---0----------------|
E|-------X------X----|-----------------------|

C   Em
Deixa, deixa
 Am       Am/G            F
Deixa eu entrar no seu pensamento
          Em                 G
E do seu pensamento não sair jamais.

 C    Em
Deixa, deixa
 Am           Am/G            F
Deixa o nosso amor a favor do vento
          Em         G
Que aí o vento sabe o que faz
 F       Em            Dm
Nem o calor de dez mil fogueiras
 G                      F
Serviria assim pra comparar
        Em              Dm
A essa paixão que me incendeia
 G
Mas você não quer acreditar

Refrão:

C          G
Ai quem me dera
Am          Em
Se a primavera
 F       Em           Dm
Fizesse nascer na sua janela
   G                    C
Uma flor com meu nome nela
         G
E se as estrelas
 Am          Em
Formassem letras
 F       Em           Dm
Escreveria com lápis de cera
 G
Te amo a noite inteira.

(Repete Tudo)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am/G = 3 X 2 2 1 X
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
