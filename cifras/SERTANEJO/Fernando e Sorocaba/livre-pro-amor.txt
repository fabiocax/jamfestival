Fernando e Sorocaba - Livre Pro Amor

Intro: G - D/F# - Em - Bm - C (D - G)

G                  D
A gente fica faz tempo
                    C
Mas você vive esquecendo
                  D
Tudo que me prometeu
G                     D
É um vem e vai nosso amor
            C
Você se acomodou
                       D
Não cuida mais do que é seu

   Em
É duro te ver nas noitadas
   Bm
Perdida pelas madrugadas
   C                              D
Tentando buscar em alguém apenas diversão


    Em
Meu medo é falar com você
   Bm
E você dizer que não me quer
     C                               D
Nem ligar se eu sair ficando com outra qualquer

    G     D/F#   Em
Te quero, oh meu bem
                     Bm
Choro, choro pelos cantos
                 C
Joga tudo fora e vem

 G     D/F#   Em
Pode me dar valor
                     Bm
Se você não me quer mais
           C    D    G
Me deixa livre pro amor

----------------- Acordes -----------------
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
