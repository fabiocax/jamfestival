Fernando e Sorocaba - Juliana / Força de Um Furacão / Estrela Solitária (Pot-Pourri)

(intro):
E|-----------------------------------------|
B|-----------------------------------------|
G|-----------------------------------------|
D|-----------------------------------------|
A|------3----2----3-2---3----2-------3-2---|
E|0-0-0---0-0-0-0------0-0--0-0--0-0-------|


E|--------------------|-----------------|-----------------|-------------------|
B|------10---10---10--|-------8---8---8-|--------7---7--7-|-------3-----3---3-|
G|----11--11----------|-----9---9---9---|------7--7---7---|-----0---0---------|
D|---0------0---0-----|---0-------------|----0------------|---5-------5---5---|
A|--------------------|-----------------|-----------------|-0-----------------|
E|-0------------------|--0--------------|-0---------------|-------------------|

     H.N...
E|--------5------12-|--------------------|--------------|
B|------5-----12----|--------3-----3---3-|--------------|
G|----5----12-------|------0---0---------|--------------|
D|------------------|----5-------5---5---|--2p0---------|
A|------------------|--0-----------------|-----2p0------|
E|------------------|--------------------|--------2~----|



E|--------------------|-----------------|-----------------|-------------------|
B|------10---10---10--|-------8---8---8-|--------7---7--7-|-------3-----3---3-|
G|----11--11----------|-----9---9---9---|------7--7---7---|-----0---0---------|
D|---0------0---0-----|---0-------------|----0------------|---5-------5---5---|
A|--------------------|-----------------|-----------------|-0-----------------|
E|-0------------------|--0--------------|-0---------------|-------------------|

     H.N...                                  H.N...
E|--------5------12-|--------------------|--------5--------12--|
B|------5-----12----|--------3-----3---3-|------5-------12-----|
G|----5----12-------|------0---0---------|----5------12--------|
D|------------------|----5-------5---5---|---------------------|
A|------------------|--0-----------------|---------------------|
E|------------------|--------------------|---------------------|



D       A
Ela é linda
Bm              G
Ela é tudo o que eu queria
D       A        Bm
Ela entrou no coração..
D        A
Olhos Claros
Bm              G
Que me envolvem de magia
D       A        Bm
E me deixa sem ação..

A
Não há lugar
Bm
Que quero estar
      G              A     G  A
Se eu não tiver Você...

(refrão)
D
Me abrace
A
Não vá embora
Bm              G        D A Bm
Eu te quero toda horaaa
G
ó Juliana
D
Me de a mão
A
Me segure
Bm             G
Que seja eterno enquanto dure
D     A   Bm   G
Amooooor
D      A  Bm    G
ó Julianaaa

ó Juliana

(solo 2x) D A Bm G

E|-----------5--0-------------------------------------|
B|--7--0--7----------0--5--7---3--0--3---7--8--7--5---|
G|--7--7--7--7--7----7--7--7-------------7--7--7--7---|
D|-----------------------------5--5--5----------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

E|----------------------------------------------3---3-|
B|--------3--0--3--5---2--0--2--3--0------------3---3-|
G|--2/4---4--4--4--4------------------1--2-1/2--2---0-|
D|---------------------2--2--2--2--2-----2----------0-|
A|--------------------------------------------------2-|
E|--------------------------------------------------3-|


( D ) (2x)

D    A  Bm     G
Não dá, já chega!
D                 A
Uma tola conversa de louco
         G
E tudo teve um fim.

D|-----|
A|2p0--|
E|----0|

D    A   Bm   G
Já não me aceita,
D                A
Você olha com desprezo
             Bm             G A
E só enxerga as coisas ruins.

      Bm
Todo mundo tem defeitos,
      A                 G
Me perdoe se não sou perfeito assim!

D             A                 Bm
Me desculpe se não sou de ferro!
              G
Me perdoe se não sou de aço!
D            A               Bm
Toda vez a tempestade passa
             G               D
E você volta para os meus braços!

D            A            Bm
A vida não é só fantasia,
              G
A vida não é feita de ilusão.
D            A         Bm
O amor é bem mais forte
           G         D
que a força de um furacão!

(solo 2x) D  A  Bm  G

E|----------------------------|------------------------|
B|----------------------------|------------------------|
G|---7--9--9/11--9--7--7/9----|------------------------|
D|-7------------------------9-|------------------------|
A|----------------------------|-4h5-5-5-4h5-5-5--2---5-|
E|----------------------------|--------------------5---|

E|----------------------------|
B|------------------------/12-|
G|-----7--9--9/11--9--7-------|
D|--7-------------------------|
A|----------------------------|
E|----------------------------|

D    A  Bm     G
Não dá, já chega!
D                 A
Uma tola conversa de louco
         G
E tudo teve um fim.

D    A   Bm   G
Já não me aceita,
D                A
Você olha com desprezo
             Bm             G A
E só enxerga as coisas ruins.

      Bm
Todo mundo tem defeitos,
      A                 G
Me perdoe se não sou perfeito assim!

D             A                 Bm
Me desculpe se não sou de ferro!
              G
Me perdoe se não sou de aço!
D            A               Bm
Toda vez a tempestade passa
             G               D
E você volta para os meus braços!

D            A            Bm
A vida não é só fantasia,
              G
A vida não é feita de ilusão.
D            A         Bm
O amor é bem mais forte
           G         D
que a força de um furacão!

(solo 2x) D  A  Bm  G

E|-----------------------------|
B|-----------------------------|
G|---7--9--9/11--9--7--7/9--7~-|
D|-7---------------------------|
A|-----------------------------|
E|-----------------------------|

D            A         Bm
O amor é bem mais forte
           G         D
que a força de um furacão!


E|-----------|
B|-----------|
G|-----------|
D|-----------|
A|-0-2-3-----|
E|-------0-1-|

F         C         G                    Am
Madrugada sem você noite que não quer passar
F         C         G
Tudo é vazio se eu não puder te amar
F          C           G          Am
Madrugada sem você óh dona da paixão
F                G
Estrela solitária no céu do meu coração

E|------5---7---8---------8--10--12--10/12---10---|
B|------------------6-----------------------------|
G|----4/5---7---9---------9--10--12--10/12---10---|
D|------------------7-----------------------------|
A|------------------------------------------------|
E|------------------------------------------------|

E|------5---7---8----------8----7-|
B|-------------------6------------|
G|----4/5---7---9--------7/9--5/7-|
D|-------------------7------------|
A|--------------------------------|
E|--------------------------------|

Am        F               C                   G
O relógio está mentindo, ainda há tempo pra recomeçar
Am                    F              C               G
Sei que as horas, não voltam jamais, mas te peço uma chance a mais
Am                                      F
Estrela solitária que habita o meu céu, força reluzente, linda flor lábios de mel
C                                            G
Que erradia traz pra dentro do meu coração, eu te quero tanto não preciso sim ou não
Am                                          F
Estrela solitária teu amor já mora em mim, volta pros meus braços não dá pra viver assim,
C                                    G
espere comigo até o dia amanhecer, fonte que me traz a alegria de viver.

F         C         G                    Am
Madrugada sem você noite que não quer passar
F         C         G
Tudo é vazio se eu não puder te amar
F          C           G          Am
Madrugada sem você óh dona da paixão
F               G
Estrela solitária.

F         C         G                    Am
Madrugada sem você noite que não quer passar
F         C         G
Tudo é vazio se eu não puder te amar
F          C           G          Am
Madrugada sem você óh dona da paixão
F                G
Estrela solitária no céu do meu coração

E|------5---7---8---------8--10--12--10/12---10---|
B|------------------6-----------------------------|
G|----4/5---7---9---------9--10--12--10/12---10---|
D|------------------7-----------------------------|
A|------------------------------------------------|
E|------------------------------------------------|

E|------5---7---8------|
B|-------------------6-|
G|----4/5---7---9------|
D|-------------------7-|
A|---------------------|
E|---------------------|

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
