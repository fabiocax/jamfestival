Fernando e Sorocaba - Minha Menina

[Intro]  C  F7M

C                                Am7
Ela escuta trinta vezes a mesma música
                          F7M
Liga pra mãe todo fim de tarde
                            C
Só pra dizer que tá com saudade

C                            Am7
Ela tem o prazer em fazer o bem
                        F7M
Ama os bichos como ninguém
                            C
Ajuda um velho a subir no trem
Am7             G           F7M
Outra dessa eu sei que não tem

C
Essa é minha menina
              F7M
De boné pra trás


Sem maquiagem é linda
             Am7
De calça rasgada
                 G
Dentro do meu carro
               F7M
Com vidro abaixado

Escutando um som alto
C
Essa é minha menina
                  F7M
Que quando eu tô mal

Vem me joga pra cima
             Am7
Que sente orgulho
                   G
Em dizer que é só minha
           F7M
É minha menina

É minha menina

( C )

[Solo]  F7M  C  F7M

C                                Am7
Ela escuta trinta vezes a mesma música
                          F7M
Liga pra mãe todo fim de tarde
                            C
Só pra dizer que tá com saudade

C                            Am7
Ela tem o prazer em fazer o bem
                        F7M
Ama os bichos como ninguém
                            C
Ajuda um velho a subir no trem
Am7             G           F7M
Outra dessa eu sei que não tem

C
Essa é minha menina
              F7M
De boné pra trás

Sem maquiagem é linda
             Am7
De calça rasgada
                 G
Dentro do meu carro
               F7M
Com vidro abaixado

Escutando um som alto
C
Essa é minha menina
                  F7M
Que quando eu tô mal

Vem me joga pra cima
             Am7
Que sente orgulho
                   G
Em dizer que é só minha
           F7M
É minha menina

É minha menina

C
Essa é minha menina
              F7M
De boné pra trás

Sem maquiagem é linda
             Am7
De calça rasgada
                 G
Dentro do meu carro
               F7M
Com vidro abaixado

Escutando um som alto
C
Essa é minha menina
                  F7M
Que quando eu tô mal

Vem me joga pra cima
             Am7
Que sente orgulho
                   G
Em dizer que é só minha
           F7M
É minha menina

É minha menina

[Final]  C  F7M  C

----------------- Acordes -----------------
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
F7M = 1 X 2 2 1 X
G = 3 2 0 0 0 3
