Fernando e Sorocaba - Máquina do Tempo

Intro : D  A  G  G  (2x)

D
A chuva cai
A
Você se vai
Em
Lágrimas ao vento
Em
Não posso mais

D
Conto de fadas
A
História sem final
Em
Pedaço do céu
Em
Atração fatal

D         A
Nem mil palavras

G                        D
Farão que você volte atrás
               A
Sofro, choro, sangro
G
Ah eu não tenho paz
D            A          G
Se eu te magoei, eu errei
                    D
Um passado sem perdão
          A  G
Deixa marcas no coração

refrão:

D    A   G   A
Aaahhh
D            A       G
Ah se eu pudesse voltar atrás
Na máquina do tempo

D    A   G   A
Aaahhh
D            A       G
Ah se eu pudesse voltar atrás
Na máquina do tempo, amor

(Intro) D  A  G  G  (2x)

D
Amor selvagem
A
Doce inspiração
Em
Ela foi meu céu
Em
Ela foi meu chão

D           A       G
Ninguém consegue tocar

Tão profundo sentimento
D                  A
Lembranças vem e ficam
G
Gravadas em meu peito

D        A         G
O meu futuro é vazio
G
Pois meus planos tem você
D        A        G
Nem mil anos farão eu te esquece

refrão:

D    A   G   A
Aaahhh
D            A       G               (3x)
Ah se eu pudesse voltar atrás
Na máquina do tempo

D    A   G   A
Aaahhh
D            A       G
Ah se eu pudesse voltar atrás
Na máquina do tempo, amor

(D  A  D)

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
