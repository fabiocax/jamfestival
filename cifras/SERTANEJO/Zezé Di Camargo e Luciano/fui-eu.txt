Zezé Di Camargo e Luciano - Fui Eu

Intro: Bb  C  Bb  C

F                Am    Dm
É mais um dia sem você
                         Am  Bb
Mais uma noite que eu  espero
            F/A          Gm   Gm7
Se alguém no mundo quis você
    C  Bb C
Fui eu

F                     Am
Te dei os sonhos que sonhei
Dm                   Am      Bb
Te imaginei pra vida inteira
              F/A       Gm   Gm7
Se alguém fez tudo por você
    C  Bb C
Fui eu

Ponte:

Bb7+               Bbº
Diz agora o que eu faço pra viver
A7         Bbº         Dm                Dm7/C
Se a cada dia é mais difícil te esquecer
     Gm
Tudo isso faz doer de demais
   C7sus4          C7       F
Eu queria só voltar atrás ficar contigo

Bb7+               Bbº
Diz agora o que eu faço pra aceitar
A7          Bbº     Dm            Dm7/C
Será q existe outra pessoa em meu lugar
      Gm                     (Gm/A)  Bb
Mas o tempo vai te convencer    e um dia vai reconhecer
        C
Que sem mim não pode  mais viver

Refrão:
F          Dm      Dm/C
Sentimento dói por dentro
Gm       Gm7            C
E a solidão não quer parar de machucar
F          Dm      Dm/C
Sentimento dói por dentro
Gm        Gm7           C            Bb  C  Bb C#
Meu coração não quer ninguém no seu lugar

Verso:
Eb                     Gm
Te dei os sonhos que sonhei
Cm                   Gm      G#
Te imaginei pra vida inteira
              Eb/G       Fm Fm7
Se alguém fez tudo por você
    Bb
Fui eu

Ponte:
Ab7+               Dº
Diz agora o que eu faço pra viver
G7         Dº        Cm                Cm7/Bb
Se a cada dia é mais dificil te esquecer
     Fm
Tudo isso faz doer de demais
   Bb7sus4    Bb7                 Eb
Eu queria só voltar atrás ficar contigo

Ab7+               Dº
Diz agora o que eu faço pra aceitar
G7          Dº       Cm            Cm7/Bb
Será q existe outra pessoa em meu lugar
      Fm                             G#
Mas o tempo vai te convencer    e um dia vai reconhecer
        Bb
Que sem mim não pode  mais viver

Refrão:
Eb          Cm        Cm7/Bb
Sentimento dói por dentro
Fm      Fm7            Bb
E a solidão não quer parar de machucar
Eb          Cm        Cm7/Bb
Sentimento dói por dentro
Fm       Fm7            Bb
Meu coração não quer ninguém no seu lugar

Eb          Cm        Cm7/Bb
Sentimento dói por dentro
Fm      Fm7            Bb
E a solidão não quer parar de machucar
Eb          Cm        Cm7/Bb
Sentimento dói por dentro
Fm       Fm7            Bb
Meu coração não quer ninguém no seu lugar

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Ab7+ = 4 X 5 5 4 X
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bb7 = X 1 3 1 3 1
Bb7+ = X 1 3 2 3 1
Bb7sus4 = X 1 3 1 4 1
Bbº = X 1 2 0 2 0
C = X 3 2 0 1 0
C# = X 4 6 6 6 4
C7 = X 3 2 3 1 X
C7sus4 = X 3 3 3 1 X
Cm = X 3 5 5 4 3
Cm7/Bb = 6 3 5 3 4 3
Dm = X X 0 2 3 1
Dm/C = X 3 X 2 3 1
Dm7/C = X 3 0 2 1 1
Dº = X X 0 1 0 1
Eb = X 6 5 3 4 3
Eb/G = 3 X 1 3 4 X
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
Fm = 1 3 3 1 1 1
Fm7 = 1 X 1 1 1 X
G# = 4 3 1 1 1 4
G7 = 3 5 3 4 3 3
Gm = 3 5 5 3 3 3
Gm/A = 5 X 5 3 3 X
Gm7 = 3 X 3 3 3 X
