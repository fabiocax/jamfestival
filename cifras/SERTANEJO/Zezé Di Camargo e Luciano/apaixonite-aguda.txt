Zezé Di Camargo e Luciano - Apaixonite Aguda

Intro: A  E  F#m  D

A
Tá na cara que a paixão me pegou
Coração está bombando de amor
                  E
Arrebentando meu peito
                       Bm
Um homem quando se apaixona
                     E
Deixa a emoção vir à tona
                     A
E não assume seus defeitos
A paixão está pulsando nas veias
                            A7
Eu caí sem forças na sua teia
                     D  G  D
Sem medo de me entregar
                   A
Mas eu não me arrependo
          F#m        E
Eu me entrego eu me rendo

       D             A   D  A
Dá pra ver no meu olhar

       E                                        D
Sou assim sentimental, sou pacional ninguém me muda
                              A
tô curtindo essa apaixonite aguda
                                 E    A  E
E me entrego aos prazeres desse vício
                                              D
Esse vírus da paixão não me faz mal mas me domina
                             A
E não tem imunidade e nem vacina
                    E                    (solo) G  D  E  A  G  D  E  A  F
Já que não sou imortal que eu morra disso

Bb
A paixão está pulsando nas veias
                            Bb7
Eu caí sem forças na sua teia
                     Eb
Sem medo de me entregar
                   Bb
Mas eu não me arrependo
        G#m          F
Eu me entrego eu me rendo
        Eb          Bb  Eb Bb
Dá pra ver no meu olhar

       F                                        Eb
Sou assim sentimental, sou pacional ninguém me muda
                              Bb
tô curtindo essa apaixonite aguda
                                 F    Bb  F
E me entrego aos prazeres desse vício
                                              Eb
Esse vírus da paixão não me faz mal mas me domina
                             Bb
E não tem imunidade e nem vacina
                    F                Bb    Eb  Bb
Já que não sou imortal que eu morra disso

       F                                        Eb
Sou assim sentimental, sou pacional ninguém me muda
                             Bb
tô curtindo essa apaixonite aguda
                                 F    Bb  F
E me entrego aos prazeres desse vício
                                              Eb
Esse vírus da paixão não me faz mal mas me domina
                            Bb
E não tem imunidade e nem vacina
                    F                Eb    Bb  F  Eb  Bb
Já que não sou imortal que eu morra disso

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bb = X 1 3 3 3 1
Bb7 = X 1 3 1 3 1
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
