Zezé Di Camargo e Luciano - Pelas Mãos de Deus

Declamado:
Existem segredos e sentimentos
Que o coração jamais explicará, jamais

 A                                        Bm
Pra que deixar o medo dominar desejos do coração
E                                                 A   E
Pra que fechar os olhos escondendo do amor e da paixão
 A                                                    Bm
Todo o sonho tem seu preço e muitas vezes é preciso acreditar
E                                                   A  E
Não importa os caminhos que na vida o amor vai te levar

             Bm                 D
Destinos marcados, traçados, cruzados
               A
Escritos por Deus, pelas mãos de Deus
           Bm                        D                E
Amores que nascem que morrem e se apagam sem dizer adeus
                                 A             E
Chorando o amor que a alma prometeu e que se perdeu


   A
Quem disse que o amor não tem espinhos
              D
E é só de felicidade que se vivi o sentimento
 E
Dia a dia mostra as suas faces espera um anjo
                  A     E
O amor tem seus defeitos

 A
Se tá perdido a boca fica muda os olhos segam
       D
Feito réu o coração paga os pecados
 E
Existem mil segredos e mistérios que assombram
         A     E
No amor desesperado


             Bm                 D
Destinos marcados, traçados, cruzados
               A
Escritos por Deus, pelas mãos de Deus
           Bm                        D                E
Amores que nascem que morrem e se apagam sem dizer adeus
                                A               E
Chorando o amor que a alma prometeu e que se perdeu

   A
Quem disse que o amor não tem espinhos
            D
E é só de felicidade que se vivi o sentimento
 E
Dia a dia mostra as suas faces espera um anjo
                  A     E
O amor tem seus defeitos


 A
Se tá perdido a boca fica muda os olhos segam
      D
Feito réu o coração paga os pecados
 E
Existem mil segredos e mistérios que assombram
         A     E
No amor desesperado
   Bm                  D                       A
Existem mil segredos e mistérios no amor desesperado

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
