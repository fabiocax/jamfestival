Zezé Di Camargo e Luciano - Mais Que Te Adoro

[Intro]  F  A#

 F
Sinto um amor chegando
 A#
E a vida vai mudando
C                           A#   F
E o meu mundo se aliando, a você
F
Somos cheios de defeitos
A#
Simplesmente homem e mulher
C                             A#   F
Nos não somos perfeitos,mas o amor é
C                           Dm
Eu sei que tem dia de sol,no outro dia temporal
 A#                                     C
E os ventos das brigas não vão desatar nossos laços
Nosso abraço

F               A#
Ai ai ai ai ai mas que te adoro,minha paz

 Dm                                         C
Será que já posso dizer que te amo ou que é cedo demais
 F             A#
Ai ai ai ai ai mas que te adoro,minha paz
 Dm                                         C
Será que já posso dizer que te amo ou que é cedo demais
 F                   Dm          A#                C
Oh oh oh oh oh oh oh oh oh oh oh oh oh mais que te adoro
 F                   Dm          A#                C
Oh oh oh oh oh oh oh oh oh oh oh oh oh mais que te adoro

----------------- Acordes -----------------
A# = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
