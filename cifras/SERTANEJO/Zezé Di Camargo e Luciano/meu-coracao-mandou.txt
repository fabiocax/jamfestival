Zezé Di Camargo e Luciano - Meu Coração Mandou

Intro:Gm Bb F

F
Toda estrada tem espinhos

Pedras no caminho
          Bb
Mas mesmo assim eu vou
         F
Meu coração mandou

F
Pode ser que assim sozinho

Cruze o meu destino
          Bb
Um anjo protetor
        F
Meu verdadeiro amor
C                              Dm         Bb
Quero alguem que seja do jeito que eu sou uouo

F               C         F
Olhos de menino um sonhador


     Bb
  R  O meu amor nao pode ser uma paixao
                          F
  E  Tem q nascer crescer brotar com emoção
                 C           Bb      F
  F  E ter nos olhos corpo alma e coração

  R  F                       Bb
     Tem que ser forte pra vencer a solidao
  Ã                         F
     Alguém que possa me mostrar a direção
  O         C        Bb            F
     O meu amor não pode ser uma ilusão


----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
