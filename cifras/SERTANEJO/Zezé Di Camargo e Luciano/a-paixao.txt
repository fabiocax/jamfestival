Zezé Di Camargo e Luciano - A Paixão

Intro:D

     D
     A paixão
     Morde o coração
      G4                                          G G4 G
     Entra pelas veias Explodindo sentimentos de ilusão
             D
     É um perigo
     A paixão
     É pura sedução
     É feita de momentos
            D7                        G G4 G G4
     Que se perdem quando chega a razão
             D
     É um perigo
        A                 G                 D
     Cuidado porque esse fogo pode te queimar
       A                       G
     A gente muitas vezes se entrega
                        D
     Pensa que chegou a hora

         A                          G
     Mas esse amor é um sonho passageiro
                        D
     Que no vento vai embora
             A
     Comigo foi assim
        G
     Um temporal
     D
     Um sonho
             A
     Você passou por mim
          G        D
     Com seu amor cigano

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
G4 = 3 5 5 5 3 3
