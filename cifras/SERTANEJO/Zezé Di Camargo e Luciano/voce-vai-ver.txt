Zezé Di Camargo e Luciano - Você Vai Ver

Intro: F  C  Dm  A  Bb  F  C  F  C7

E|-------------------8-------|-------------5----------|
B|-10-10-11-10-8-6-8---8-5-6-|-6-8-6-5---5---5--------|
G|---------------------------|---------7-------6-7----|
D|---------------------------|------------------------|
A|---------------------------|------------------------|
E|---------------------------|------------------------|

E|------------------------|----------------------------|
B|------------------------|----------------------------|
G|---------------------10-|-----------9-10-10/12-10----|
D|---8-12-10-8-10-9/10----|-8-8-8-7-8------------------|
A|-8----------------------|----------------------------|
E|------------------------|----------------------------|

E|-8-8-8-6-5-------------------------------------------|
B|-----------8-6-5-6-5-3-------------------------------|
G|-----------------------5-3-2-------------------------|
D|-----------------------------5-3-2-3-----------------|
A|-----------------------------------------------------|
E|-----------------------------------------------------|


 F               C             Dm   Dm/C  Bb
Você pode encontrar muitos amores
                    Am              Gm   Gm/F  C
Mas ninguém vai te dar o que eu te dei
              A7           Dm
Podem até te dar algum prazer
               Bb             F
Mas posso até jurar você vai ver
                     C               F   C7   F
Que ninguém vai te amar como eu te amei

            C                Dm    Dm/C  Bb
Você pode provar milhões de beijos
                  Am             Gm Gm/F   C
Mas sei que você vai lembrar de mim
                   A7          Dm
Pois sempre que um outro te tocar
              Bb           F
Na hora você pode se entregar
                   C                 F    C7
Mas não vai me esquecer nem mesmo assim

          F             C        Dm
Eu vou ficar, guardado no seu coração
           F7      Bb           F/A           C
Na noite fria solidão, saudade vai chamar meu nome
         F                C         Dm
Eu vou ficar, num verso triste de paixão
         F7         Bb            F/A       C
Em cada sonho de verão, no toque do seu telefone
          F
Você vai ver

(F  C  F7  Bb  F  C  F  C  F)

 C                Dm    Dm/C  Bb
Você pode provar milhões de beijos
                  Am             Gm Gm/F   C
Mas sei que você vai lembrar de mim
                   A7          Dm
Pois sempre que um outro te tocar
              Bb           F
Na hora você pode se entregar
                   C                 F    C7
Mas não vai me esquecer nem mesmo assim

          F             C        Dm
Eu vou ficar, guardado no seu coração
           F7      Bb           F/A           C
Na noite fria solidão, saudade vai chamar meu nome
         F                C         Dm
Eu vou ficar, num verso triste de paixão
         F7         Bb            F/A       C
Em cada sonho de verão, no toque do seu telefone
          F
Você vai ver

(F  C  F7  Bb  F  C  F  C  F)

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
Dm/C = X 3 X 2 3 1
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
F7 = 1 3 1 2 1 1
Gm = 3 5 5 3 3 3
Gm/F = X X 3 3 3 3
