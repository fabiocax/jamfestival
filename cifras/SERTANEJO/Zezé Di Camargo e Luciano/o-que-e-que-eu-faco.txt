Zezé Di Camargo e Luciano - O Que É Que Eu Faço

Intro:Eb Bb Cm7 Ab Eb Fm7 Bb

Eb           Bb  Ab      Bb           Eb
Por quanto tempo me dediquei a amar você
              Bb           Ab         Bb                 Ab
Não fiz mais nada porque nada mais me da tanto prazer
Ab            Fm7              Bb
Lembrando as coisas que eu te dei
Ab           Fm7          Bb
Nem me dei conta o tanto que chorei

                        Eb
Me diz o que é que eu faço
                Bb            Cm7
O que é que eu faço com os abraços
            Gm7            Ab
Se eu não tenho mais seu corpo
                Eb        Fm7
O que é que eu faço agora com os meus olhos
Bb              Eb
Sem você pra olhar?

                Bb              Cm7
O que é que eu faço com meus beijos
        Gm7        Ab
Que só querem sua boca?
                Eb            Fm7
O que é que eu faço com esse amor inútil
Bb            Eb
Sem você pra amar?

           Bb Ab          Bb          Eb
Na minha vida não deixei nada pra depois
          Bb            Ab        Bb
Qual a saída,se eu fiz tudo na medida pra nós dois
Ab           Fm7            Bb
Olhando as coisas que eu ganhei
Ab          Fm7           Bb
Nem me dei conta o tanto que chorei

                        Eb
Me diz o que é que eu faço... (solo) repetir 3*parte


Ab       Bb               Eb      Cm7
E sem falar dos nossos livros e cds
Ab        Bb          Eb          Cm7
As fotos das viagens que a gente fez
Ab     Bb              Eb           Cm7         Ab
Ainda sinto o seu perfume em cada roupa que eu usei
            Cm7        Eb
E pra eu não acabar comigo

                      E
Me diz o que é que eu faço
                B            C#m7
O que é que eu faço com os abraços
            G#m7                   A
Se eu não tenho mais seu corpo
                E         F#m7
O que é que eu faço agora com os meus olhos
B               E
Sem você pra olhar?
                B               C#m7
O que é que eu faço com meus beijos
        G#m7       A
Que só querem sua boca?
                E             F#m7
O que é que eu faço com esse amor inútil
B             E
Sem você pra amar?

----------------- Acordes -----------------
A = X 0 2 2 2 0
Ab = 4 3 1 1 1 4
B = X 2 4 4 4 2
Bb = X 1 3 3 3 1
C#m7 = X 4 6 4 5 4
Cm7 = X 3 5 3 4 3
E = 0 2 2 1 0 0
Eb = X 6 5 3 4 3
F#m7 = 2 X 2 2 2 X
Fm7 = 1 X 1 1 1 X
G#m7 = 4 X 4 4 4 X
Gm7 = 3 X 3 3 3 X
