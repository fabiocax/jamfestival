Zezé Di Camargo e Luciano - Bella Senz'anima

INTRO : D  Bm7  D  Bm7  D  G7+

           D     Bm            D        Bm
E adesso siediti     su quella seggiola     stavolta
  D       Bm7             A      A4  A          C         C7+
ascoltami     senza interrompere        è tanto tempo Che
C        A       A4  A           D         G7+
  volevo dirtelo       vivere insieme a te      è stato
  D     G7+            D        G7+            A      A4  A
inutile     tutto senz´allegria     senza una lacrima
             C       C7+  C          A     A4  A
niente da aggiungere        ne da dividere        nella tua
 G                 D                      Am7     D7
trappola ci son caduto anch´io avanti il prossimo     gli
          G
lascio il posto mio
         Bm        A4         A              D        G/D
Povero diavolo Che pena mi fa    e quando a letto lui      ti
  D             G/D             D      G/D D/F#        A4
chiederá di piú      glielo concederai          perche tu fai
     A           C      C7+  C           A4    A        D
cosi   come sai fingere        se ti fa comodo   adesso so

        G/D          D         G/D          D        G/D D/F#
chi sei     e non ci sofro piú     e se verrai di lá
      A4         A           C       C7+  C         A4
te lo dimostreró   e questa volta tu        te lo ricorderai
B            E       A/E           E      A/E
  e adesso spogliati     come sai fare tu      ma non
   E      A/E        B4           B             D        D7+
illuderti     lo non ci casco piú     tu mi rimpiangerai
D        B4        B  A  B/F#
  Bella Senz´anima
         E     A          E    A    E
Nanananararara   Nananararara  Uuuu
       B4           B             D        D7+ D
lo non ci casco piú     tu mi rimpiangerai       Bella
 B4         B             E     A         E     A     E
Senz´anima        Nanananararara   Nananararara  Uuuu
       B4           B             D        D7+  D
lo non ci casco piú     tu mi rimpiangerai        Bella
 B4         B              E
Senz´anima        Nanananararara

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/E = 0 X 2 2 2 0
A4 = X 0 2 2 3 0
Am7 = X 0 2 0 1 0
B = X 2 4 4 4 2
B/F# = X X 4 4 4 2
B4 = X 2 4 4 5 2
Bm = X 2 4 4 3 2
Bm7 = X 2 4 2 3 2
C = X 3 2 0 1 0
C7+ = X 3 2 0 0 X
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
D7+ = X X 0 2 2 2
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
G/D = X 5 5 4 3 X
G7+ = 3 X 4 4 3 X
