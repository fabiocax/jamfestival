Zezé Di Camargo e Luciano - Conflito

Intr:A A4 Bm Bm4 E A A4 E

E           A
Outra vez aqui,
                                  Bm
De frente pra você E querendo te falar

Outra vez aqui,
  D
De peito aberto Alma transparente?.
                               A         E
Uma verdade em meu olhar, Meu olhar...

**         A   A4 A
Outra vez aqui,

Querendo te falar Da minha vida e te
         D D4 D
Fazer entender,

Palavras não convencem Enquanto a gente está

               A
Em mundos diferentes,
                        Bm     E
Confundem a cabeça Da gente,
                    A         A7
E a vida não Tem solução,uo ou uo uo
     D
À incerteza bate forte
                            A
E deixa um homem Feito um menino
                         Bm
Que ainda não fez Seu destino
 E                A
Não aprendeu A lição
      E
É assim que eu me sinto Quando estou aqui
      D
Com medo, Temendo, Querendo fugir?.
     A                                      E
E todos os conflitos da Terra Desabaram em mim

O cristal quebrou, Já passou da medida?.
     D
A porta fechou E eu não vejo saída
     A                                      E
E todos os conflitos da Terra Desabaram em mim
  D                  A
Por Deus, Volta pra mim!

Intr:
repete do **

(final)
 E        A
Volta pra mim!
Volta pra mim!  E A

----------------- Acordes -----------------
A = X 0 2 2 2 0
A4 = X 0 2 2 3 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
Bm4 = X 2 2 4 3 2
D = X X 0 2 3 2
D4 = X X 0 2 3 3
E = 0 2 2 1 0 0
