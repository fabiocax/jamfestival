Zezé Di Camargo e Luciano - Quem Canta Não Para

      E
Tô aqui pra falar pra você
                                  A
Me arrepia dizer, quem canta não para
       E
Tá na veia na pele queimando no peito pulsando
             B
Tá na minha cara

       A
Eu ganhei de presente este dom

E o coração faz o som
                  B
Que sai da minha alma
     B                               E
E parar, de cantar pra você , nem pensar!

Refrão 2x:
            F#m                B
Cantar todo dia , cantar onde for

           G#m             C#m
Cantar poesias falando de amor
           F#m              B
Agente agradece e nunca esquece
              A                  E
Que sem a plateia não existe cantor

E
hohohohohohoho

Solo: F#m  B  G#m  C#m  F#m  B  A  E

(Repete Tudo)

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
