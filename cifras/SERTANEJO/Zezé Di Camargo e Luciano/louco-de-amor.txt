Zezé Di Camargo e Luciano - Louco de Amor

(intro)  A D A E A D A E

A                 D/F#              A          E/G#
   Eu fui querendo meio sem querer
A                 D/F#              E/G#
   Cada dia mais e mais você.
A                 D/F#                A       E/G#
   Não conseguia mais me controlar
A                    D/F#                     E/G#
   Passei a meditar pra não me enlouquecer.

 C#m7                                F#m
   Me tornei assim um metafísico
 Bm         Bm7/A                  D/E  E4       E7
   Me dediquei ao mundo espiritual
C#m7                                F#m
   Me harmonizei com a natureza
                 Bm                   Bm7/A
   Comprei velas, incenso, pedras.
               D/E  A/E   E/G#
   Fiz meu mapa astral


          A              E/G#   D/F#    E/G#
   Neurótico, paranóico, degenerado.
         A                      E/G#              D/F#       E
   e como costumo dizer, completamente tarado (2x)

                          A         E/G#
Mas louco de amor...
                    D/F#            E
Louco de amor, por você!
                      A            E/G#
Louco de amor, ooooooo
                    D/F#            E
Louco de amor, por você!

A                 D/F#             A       E/G#
  Mas nada disso conseguiu fazer
A                       D/F#              E/G#
  Eu gostar um pouco menos de você.

C#m7                                F#m
  Fui sair com outras, fiz análise
Bm         Bm7/A         D/E        E4    E7
  No desespero de tentar resolver
C#m7                                F#m
  Fiz sessões diárias de hipnose
              Bm                 Bm7/A     D/E A/E E/G#
  Regressão, coma induzido pra te esquecer.

          A              E/G#   D/F#    E/G#
   Neurótico, paranóico, degenerado.
         A                      E/G#              D/F#       E
   e como costumo dizer, completamente tarado (2x)

                          A       E/G#
Mas louco de amor...
                    D/F#            E
Louco de amor, por você!
                      A           E/G#
Louco de amor,  ooooooo
                    D/F#            E
Louco de amor, por você!

(solo) A E/G# D/F# E/G#   A E

     A              E/G#   D/F#    E/G#
   Neurótico, paranóico, degenerado.
         A                      E/G#              D/F#       E
   E como costumo dizer, completamente tarado (2x)

                          B       F#/A#
Mas louco de amor...
                    E/G#            F#
Louco de amor, por você!
                      B           F#/A#
Louco de amor,  ooooooo
                    E/G#            F#
Louco de amor, por você!

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/E = 0 X 2 2 2 0
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
Bm7/A = 5 2 4 2 3 2
C#m7 = X 4 6 4 5 4
D = X X 0 2 3 2
D/E = X X 2 2 3 2
D/F# = 2 X 0 2 3 2
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E4 = 0 2 2 2 0 0
E7 = 0 2 2 1 3 0
F# = 2 4 4 3 2 2
F#/A# = 6 X 4 6 7 X
F#m = 2 4 4 2 2 2
