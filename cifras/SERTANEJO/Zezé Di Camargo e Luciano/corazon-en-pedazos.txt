Zezé Di Camargo e Luciano - Corazón En Pedazos

INTRO: A7  G  D  A7  G  D

        D                        A7
Hoy te busco como el mar, busco siempre las arenas, ya no
 Em              A7        D
puedo hablar de amor solo puedo hablar de penas
    D                        Em                     A7
Solamente pienso en ti justamente cuando se, que me has
             D                          E7
abandonado y jamais piensas volver  te doy mi corazón hecho
   A        E7                       A       G        D
pedazos, a punto de morir por un flechazo porque, porque,
    A7                                     D
porque me prometiste tanto que después no fue


Refrão

A7                          G                           D
  Dime si yo te di poquito amor, si te he besado sin calor si
                   A7                               D
no fue hombre para ti  Dime si todo fue un bendito sueño un

               A7             D
delirio tan pequeño que debe así


INTRO: A7  G  D  A7  G  D
REPETE Refrão

A7                         G                           D
 Dime si yo te di poquito amor, si te he besado sin calor si
                   A7                               D
no fue hombre para ti  Dime si todo fue un bendito sueño un
               A7              G  D
delirio tan pequeño que debe así

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
