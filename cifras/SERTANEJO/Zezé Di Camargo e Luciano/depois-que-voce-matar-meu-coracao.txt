Zezé Di Camargo e Luciano - Depois Que Você Matar Meu Coração

Intro: D C#m Bm D/E

E|-------------------------------------------------
B|-10-------10-10/12-------12-12/14----------------
G|----11-------------13-------------14-14----------
D|-------12-------------14----------------16-14----
A|-------------------------------------------------
E|-------------------------------------------------

E|--------------------------------------------------
B|-10-------10-10/12-------12-12/14-------14-14/17--
G|----11-------------13-------------14-14-----------
D|-------12-------------14--------------------------
A|--------------------------------------------------
E|--------------------------------------------------

Depois de tanto amor
        E/G#              E
Você me pede pra esquecer
        D
Pra nunca mais te procurar

               A    E
Que foi tudo engano

 A
Depois de tanta luz
           E/G#                  E
Que eu vi brilhando em seu olhar
              D
Como é que eu posso aceitar
               A
Que já não te amo
              E
Que já não te amo

      D             E
Quem vai tirar de mim
        F#m
Essa saudade, essa paixão
               D
Só vou te esquecer
             E             A      (A7) E4 E
Depois que você matar meu coração

   A                           E/G#         E
Depois que descobri o quanto é bom amanhecer
          D
No mesmo quarto com você
                  A     E
Em meus braços dormindo

    A
Não dá pra apagar
        E/G#
Como se fosse num papel

         D
Que voa longe, vai pro céu
                 A
Pouco a pouco sumindo
                 E
Pouco a pouco sumindo

Refrão:
A
Depois de tanto amor...

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
D/E = X X 2 2 3 2
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E4 = 0 2 2 2 0 0
F#m = 2 4 4 2 2 2
