Zezé Di Camargo e Luciano - Dois Corações e Uma História

Intro 2x: A  D  E

Primeira Parte

A                Bm
  No meio da conversa
                 E
De um caso terminando
                     A
Um fala e o outro escuta

E os olhos vão chorando
       Bm
A lógica de tudo
         E
É o desamor que chega
           A
Depois que um descobre

Que o outro não se entrega


          Bm
Quem vai sair arruma
    E
As coisas põe na mala
    A
Enquanto o outro fuma

Um cigarro na sala
     Bm
E o coração palhaço
   E
Começa a bater forte
           D
Quem fica não deseja
      E                 A
Que o outro tenha sorte

Refrão:

              D
E longe um do outro
                 E
A vida é toda errada
                  A
O homem não se importa
                   F#
Com a roupa amarrotada
                Bm
E a mulher em crise
                E
Quantas vezes chora
                 D               E
A dor de ter perdido um grande amor
     A
Que foi embora

Segunda parte:

                  Bm
Mas quando vem a volta
              E
O homem se arruma
                   A
Faz barba, lava o carro

Se banha, se perfuma
             Bm
E liga pro amigo
                   E
Que tanto lhe deu força
              A
E jura nunca mais

Vai perder essa moça

            Bm
E a mulher se abraça
        E
À mãe, diz obrigado
         A
E põe aquela roupa

Que agrada o seu amado
           Bm
E passa a tarde toda
          E
Cuidando da beleza
          D
Jantar à luz de velas
    E      A
E amor de sobremesa

Refrão:

              D
E perto um do outro
              E
A vida é diferente
                A
A solidão dá espaço
                      F#
Ao amor que estava ausente
                   Bm
Quem olha não tem jeito
             E
De duvidar agora
                D
Da força da paixão que tem
E                   A
  Dois corações e uma história

              D
E perto um do outro
              E
A vida é diferente
             A
A solidão dá espaço
                      F#
Ao amor que estava ausente
                   Bm
Quem olha não tem jeito
             E
De duvidar agora
                D
Da força da paixão que tem
E                         A   F#m
  Dois corações e uma história
                Bm
Da força da paixão que tem
E                   A         D  E  A
  Dois corações e uma história

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
