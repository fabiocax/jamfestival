Zezé Di Camargo e Luciano - Vida, Viola e Violeiro

C
 A alma de um violeiro
            G
É quando entoa pela madrugada
             F
Um dedo de prosa com a sua amada
  C               G
E um ponteio de viola
   C
A vida de um violeiro
           G
Viola nas costas vai pegando estrada
              F
Sem saber ao certo o ponto de chegada
   C               G
Só um ponteio de viola
 C        G          F               C    G
Violeiro só, sem a viola, ponto sem nó
 C        G             F              C    G
Viola na mão, de um violeiro, sem solidão
   C
O sonho de um violeiro

            G
É beira de rio cigarro de palha
             F
Picotando o fumo um fio da navalha
     C                G
Num peixe fisgado na hora
   C
A mágoa de um violeiro
              G
É quando a cabocla vai pra outras bandas
      F
Sabe Deus aonde é que ela anda
     C                G
E assim um violeiro chora
C       G           F              C    G
Violeiro só, sem a viola, ponto sem nó
 C        G             F              C    G
Viola na mão, de um violeiro, sem solidão

----------------- Acordes -----------------
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
