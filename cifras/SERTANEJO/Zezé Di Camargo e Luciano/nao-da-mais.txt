Zezé Di Camargo e Luciano - Não Dá Mais

Intro:Gm F Eb Bb

Bb
Nao dá mais

Melhor fazer de conta que essa história
      F
Já ficou pra trás

F
Sorriso apagado
                       Cm
No meu rosto já não quero mais
Eb                                            Bb       F
Dessa vez eu pra valer eu vou buscar outro caminho  sozinho


Bb
Eu vou lembrar
                                      F
Seu beijo seu abraço seu carinho no amanhecer

                                         Cm
O vinho derramando no seu corpo a me  enloquecer
    Eb                         F
Eu vou lembrar mais nao vou chorar

Bb
Nao vou deixar
                                   F
Me enganar com uma lagrima em seu rosto
                                Cm
Por um toque de malicia no meu corpo
Eb           F
Nem por mentira

Bb
Vc vai ver
                               F
Que perdeu aquele cara que te ama
      Cm           Eb              F
Por momentos de aventura em outra cama
Eb            F               Bb
Vou tirar vc de vez da minha vida

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
