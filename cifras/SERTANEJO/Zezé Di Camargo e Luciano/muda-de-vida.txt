Zezé Di Camargo e Luciano - Muda de Vida

       D
  não sei aonde eu ando com a cabeça

  o que é que eu vou fazer pra' que eu te esqueça
        D
  por mais que você faça não tem jeito
                    A
  eu me sinto um idiota
      G
  esqueça pois não vai me iludir
      D
  eu sei da sua vida por aí
      A                       G
  e quando a saudade aperta o peito
                    D
  pede arrego pede volta

          A7
  muda de vida ou vai me perder
            D
  e seja comigo o que sou com você

               A7
  você está perdida e não sabe o que quer
  G          D
  mu...da de vida

          A7
  muda de vida ou vai me perder
            D
  e seja comigo o que sou com você
              A7
  não seja bandida e aprenda a viver
   A   G      A    E
  mu...da de vi...da

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
