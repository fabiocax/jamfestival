Zezé Di Camargo e Luciano - Linda, Linda

o sol chegou, tão atrevido
               G
Penetrando na cortina
         Dm            G7
Eu posso ver todo seu corpo
              C         G7
Descoberto, linda, linda
          C             C7
E uma vontade me queima todo
                F     Bb F
Me pedindo pra ficar
         C            G7
Mas o relógio logo desperta
                   C   G7
Tenho que ir trabalhar

       C
Me aproximo ponho meu corpo
               G
No seu corpo devagar
    Dm                 G7
Suavemente, beijo seu rosto

             C      G7
Para não te acordar
         C             C7
E num esforço, eu vou saindo
                  F       Bb F
E deixo aqui meu coração
          C           G7
E no meu carro me acompanha
            C       C G/B Am
Uma tal de solidão

G
E no trânsito agitado
    C    F  C
Eu sofro
Dm                 G7
Todos os meus pensamentos
            C   F C C G/B Am
Estão com você
G                       C
No retrovisor eu vejo seu rosto
F
Dá vontade de voltar
             G7
E te amar de novo
Dm                 G7
Mas o trabalho me chama
                C     (Dm G7)
Tenho que te esquecer

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G7 = 3 5 3 4 3 3
