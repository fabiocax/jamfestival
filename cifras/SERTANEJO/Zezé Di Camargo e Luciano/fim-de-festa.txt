Zezé Di Camargo e Luciano - Fim de Festa


G                      D      Em                C
É fim de festa, é madrugada, chegou a hora de dizer adeus
G                            D      Em                      C
No carro eu vou pegando a estrada, mas seu olhar ficou nos olhos meus
D                 C
Uma semana cheia de paixão
D                 C
E de repente a saudade
D
Invadiu meu coração
                   G
É fim de festa na cidade

Solo: G D Em C D

Vocalização:
( G D Em C D )
Ooo...ooo...ooo...

Am                                    D
Um olhar, um sorriso, um abraço e um beijo

Am                            D
Foi assim o começo da nossa paixão

G
Você me acertou em cheio
               D
Foi direto no meu coração
Em
Foi como um grande rodeio
               Bm
Onde o toro derruba o peão
C
Eu só entrei pra ganhar,
G
Venci toda solidão
      A
Você foi como um prêmio pra mim
                  D
Mas deixei que fugisse das mãos

Vocalização:
( G D Em C D )
Ooo...ooo...ooo...

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
