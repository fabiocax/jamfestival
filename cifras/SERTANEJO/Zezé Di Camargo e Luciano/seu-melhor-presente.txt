Zezé Di Camargo e Luciano - Seu Melhor Presente

Intro: Gm7  C7  Gm7  C7  F  Bb  F

Verso 1:

   F                                              Bb - F
Achar que uma simples paixao nao vai te machucar
                                           C   C4 - C
Pensar que já conhece todas as regras do amor
   Bb                                  F
É tudo tão misterioso, é fácil se enganar
           Dm                     Bb
E a gente nem precebe estar envolvido
       Gm                    Bb
É a vitória do amor e da paixão
       Gm                    Bb
E o desejo de te ter no coração
     F
Não passa...

Verso 2:


      F                                        Bb - F
Se a luz do meu caminho segue em tua direção
                                          C    C4 - C
Não brigo com os meus instintos, é a solução
    Bb                                     F
Um beijo, uma vontade louca me faz prosseguir
           Dm               Bb
Nem me pergunto se corro perigo
       Gm                    Bb
É a vitória do amor e da paixão
       Gm                    Bb
E o desejo de te ter no coração
     F        C
Não passa...

Refrão:

     F
Não dá pra esconder
                C
Quando um sentimento tem razão
           Bb
A melhor saída é assumir
           F     C
O amor da gente
     F
Não dá pra esconder
            C
É coisa de pele, é sedução
              Bb
Me dou por inteiro, eu quero ser
               F
Seu melhor presente

Pós-Refrão:

       Gm                    Bb
É a vitória do amor e da paixão
       Gm                    Bb
E o desejo de te ter no coração
     F       F4  F
Não passa...
       Gm                    Bb
É a vitória do amor e da paixão
       Gm                    Bb
E o desejo de te ter no coração
     F
Não passa...

Solo: F  Am  Bb  Bb7  C  C7

Verso 3:

      F                                        Bb - F
Se a luz do meu caminho segue em tua direção
                                          C    C4 - C
Não brigo com os meus instintos, é a solução
    Bb                                     F
Um beijo, uma vontade louca me faz prosseguir
           Dm               Bb
Nem me pergunto se corro perigo
       Gm                    Bb
É a vitória do amor e da paixão
       Gm                    Bb
E o desejo de te ter no coração
     F
Não passa...

Refrão:

    F
Não dá pra esconder
                C
Quando um sentimento tem razão
           Bb
A melhor saída é assumir
           F     C
O amor da gente
     F
Não dá pra esconder
            C
É coisa de pele, é sedução
              Bb
Me dou por inteiro, eu quero ser
               F
Seu melhor presente

Pós-Refrão (Finaliza):

       Gm                    Bb
É a vitória do amor e da paixão
       Gm                    Bb
E o desejo de te ter no coração
     F       F4  F
Não passa...
       Gm                    Bb
É a vitória do amor e da paixão
       Gm                    Bb
E o desejo de te ter no coração
     F       Bb  F
Não passa...

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bb7 = X 1 3 1 3 1
C = X 3 2 0 1 0
C4 = X 3 3 0 1 X
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
F4 = 1 3 3 3 1 1
Gm = 3 5 5 3 3 3
Gm7 = 3 X 3 3 3 X
