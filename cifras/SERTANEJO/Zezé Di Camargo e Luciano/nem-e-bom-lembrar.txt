Zezé Di Camargo e Luciano - Nem É Bom Lembrar

G
Não se encontra um grande amor nas avenidas
                              D
Nas esquinas,ou na solidão de um bar
     C                G             Em
Pode ser uma paixão enganando o coração
    C           D         G         C/D
Mas Depois,nem é bom lembrar
       G
Não se encontra um grande amor num passatempo
                                D
São momentos, uma transa por transar
       C                     G                  Em
São mentiras, fingimentos, quem quiser tem que pagar
   C       D          G       C/D
Mas depois, nem é bom lembrar
   G                                        G5+
Lembrei de você outro dia,enquanto a chuva caía (Refrão)
     C                           G        D/F#
Seu rosto ainda está vivo no meu olhar
    G                                              G5+
Te amei de um jeito tão puro,sonhei pra nós dois um futuro

       C              D                    G     C/D
Mais um rio é pouco demais pra quem busca o mar
   G                        B7
Passou por mim como um vendaval (Refrão)
    Em                       Dm        G7
Me deixou assim me fez tanto mal
    C            C#º          G/D   Em
Mas eu consegui deixar de te amar
Am            D         G
Agora eu nem quero te lembrar

----------------- Acordes -----------------
Am = X 0 2 2 1 0
B7 = X 2 1 2 0 2
C = X 3 2 0 1 0
C#º = X 4 5 3 5 3
C/D = X X 0 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G/D = X 5 5 4 3 X
G5+ = 3 X 1 0 0 X
G7 = 3 5 3 4 3 3
