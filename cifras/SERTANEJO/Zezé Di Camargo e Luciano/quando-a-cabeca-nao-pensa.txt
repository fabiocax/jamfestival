Zezé Di Camargo e Luciano - Quando a Cabeça Não Pensa

Intro: C#, F#, C#, B, F#, C#

     F#             B     F#
Acreditei demais naquele amor
                             C#
Dei minha vida pra ficar com ela
          G#m                  C#
Eu fui promessa, fui o mel da flor,
              G#m
Chuva de paixão
        C#    F#
Fui seu cobertor
                       B    F#
Eu fiz de tudo para te seduzir
            F#7               B
Dei a minha rede para você dormir
                          F#
Mas eu acordei e caí na real
                                  G#m
Vi que era um sonho, amor de carnaval
           B                   F#
Andorinha que voou do meu quintal

B                          F#
 Acordei a tempo de me proteger
                           C#
Utopia que me leva para você
           G#m          C#   F#
Dou a minha vida pra te esquecer
             C#
Mas eu  não sei as coisas vão ficar assim
        F#
Só sei que cada dia eu fico mais afim
        C#
Contradições do coração
 B                 F#           B F#
Amor virando solidão dentro de mim
 C#
Eu já nem sei se peço pra você ficar
          F#
Mas se partir , eu sei que vou te procurar
                 C#
Esqueço às vezes a razão
  B             C#   F#          B  F#
E escancaro o coração pra você entrar
              C#
Já dizia o ditado
                     G#m
Quando a cabeça não pensa
        C#       F#
O corpo paga o pecado

----------------- Acordes -----------------
B = X 2 4 4 4 2
C# = X 4 6 6 6 4
F# = 2 4 4 3 2 2
F#7 = 2 4 2 3 2 2
G#m = 4 6 6 4 4 4
