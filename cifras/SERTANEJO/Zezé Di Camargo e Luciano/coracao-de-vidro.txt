Zezé Di Camargo e Luciano - Coração de Vidro

Intro: A  E/G#  F#m  E  A   A7  D  E  A

 A                                         E
O coração de quem ama, é mais fraco que o vidro
                                               A
Vira e mexe tá quebrando, se não for correspondido
     A7           D
O coração de quem ama, tem sempre a parede fina
                              E               A
É feito casca de ovo, transparente, igual cortina
                E                         A
O meu coração é esse coração que rasga à toa
  A7            D         E          A
Vive todo remendado por amar uma pessoa

A    E/G#   F#m   E    D    E
Ai ,  ai ,  ai  , a....a....ai,
     D            A
Meu coração tá pisado,
                E
Metade bate no tempo,

                 D      A
Metade bate atrasa.....do
A     E/G#    F#m    E    D    E
Ai ,   ai   , ai   , a....a....ai,
     D            A
Meu coração tá naquela
                   E
Um terço bate por mim,
       E7             A
Dois terços batem por ela

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
