Zezé Di Camargo e Luciano - Te Extraño (Saudade)

Intro:
E|-9/7-5h7-5-------9/7-5h7-5-----5-9/7-5h7-5-2-2-4/5-4-0-0-------|
B|-----------7/9-9-----------5/7---------------------------3-2-2-|
G|---------------------------------------------------------------|
D|---------------------------------------------------------------|
A|---------------------------------------------------------------|
E|---------------------------------------------------------------|

A
Tu siempre viviste en mis sueños
                                        D
Fuiste mi luz, mi secreto ha pasado el tiempo
                       A
Y aún te quiero tanto como ayer


Mi corazón te busca y cuenta los dias

Mi miente te piensa, mi alma esta tan vacia
         D
Nuestra primera vez

           A
Se me grabó en la piel

 D            E             A             D
Talvez yo sea de tu pasado solo un recuerdo más
 A         D                   E
Y yo oigo tu voz que me llama

Refrão:
 D             E
Sueño, lloro, siento
     A                D
Que vive aún la esperanza
A           D                  E
Te extraño, debo arrancar esta página
         A
De mi vida

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
