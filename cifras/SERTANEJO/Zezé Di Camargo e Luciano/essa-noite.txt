Zezé Di Camargo e Luciano - Essa Noite

(intro) E D A

  E
Essa noite, vou cair na festa
Essa noite, vai ser bom à bessa
        B
Tem rodeio
                          E
Alegria pra quem tá de saco cheio
Essa noite, vai subir poeira
Essa noite, não fico de bobeira
       B
Entro na dança
                                  E
Mulher brava com carinho a gente amansa
      A
E grito Êô! Êô! Êô! Êô!
         E
Desço a madeira
                                   B
Vou namorar, me divertir a noite inteira

                           E
Bebendo todas paro só segunda feira
      A
E grito Êô! Êô! Êô! Êô!
          E
Noitada louca
                            B
Fim de semana, sábadão, cabeça Ôca
                       A         E
Essa noite é noite de beijar na boca

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
