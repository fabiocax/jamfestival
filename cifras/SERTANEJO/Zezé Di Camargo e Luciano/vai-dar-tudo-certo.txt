Zezé Di Camargo e Luciano - Vai Dar Tudo Certo

INTRO: ( G  G/C )

G                                D/F#        C
Se a gente colocar a nossa fé em ação e confiamos, e pedimos
   G                   D               G
a Deus, ele ouve e responde e dá tudo certo


G7             C   C4  C               G
 Vai dar tudo certo,     vai dar tudo certo se a gente
     D                C         D        G
colocar a nossa fé em ação vai dar tudo certo


SOLO: ( G  G/C )

G                                   D/F#                C
Sei que a vida não é só de momentos bons é há tempos difíceis
                 G                       D
a vida é mesmo assim mas se a gente colocar a nossa fé em

 C   D              G
ação  vai dar tudo certo

REPETE REFRÃO

G7            C     C4  C               G
 Vai dar tudo certo,      vai dar tudo certo se a gente
     D                C         D        G    G7
colocar a nossa fé em ação vai dar tudo certo  Vai dar tudo
 C    C4  C              G                    D
certo,     vai dar tudo certo se a gente colocar a nossa fé
   C         D        G
em ação vai dar tudo certo

----------------- Acordes -----------------
C = X 3 2 0 1 0
C4 = X 3 3 0 1 X
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
G = 3 2 0 0 0 3
G/C = X 3 5 4 3 3
G7 = 3 5 3 4 3 3
