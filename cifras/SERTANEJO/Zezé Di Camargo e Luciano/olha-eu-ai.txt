Zezé Di Camargo e Luciano - Olha Eu Aí

INTRO: ( G  G4  G ) (4x)

        Am         C                             G
 Olha eu ai... viajando no seu sentimento dentro do meu coração
        Am           C                                 G
 Olha eu ai... de repente no seu pensamento andando em sua direção
Bm               Am  C                 Bm           Am         C
 Um brilho no olhar   da paixão que bateu e já era tempo de deixar
                                  D
acontecer, valer o seu desejo e o meu
D/F# G           G/C              G             G/C              G
     A gente às vezes deixa de amar, por não ouvir o coração falar por
                        D
orgulho, vaidade ou sei lá o quê
D/F# G            G/C             G                 G/C
     Não vale a pena sentir solidão se posso dizer sim pra que dizer
 G                            D              Am           C
não? Meu amor foi feito na medida pra você é só me deixar provar e
       G                   Am  C  Am/E         G  G4  G  G4
amar você vai ver  Olha eu ai.....     Olha eu ai.....      Olha
   Am  C  Am/E         G   G4  G
eu ai.....  Olha eu ai.....


INTRO: ( G  G4  G )  (4x)
Bm               Am  C                 Bm           Am         C
 Um brilho no olhar   da paixão que bateu e já era tempo de deixar
                                  D
acontecer, valer o seu desejo e o meu
D/F# G           G/C              G             G/C              G
     A gente às vezes deixa de amar, por não ouvir o coração falar por
                        D
orgulho, vaidade ou sei lá o quê
D/F# G            G/C             G                 G/C
     Não vale a pena sentir solidão se posso dizer sim pra que dizer
 G                               D              Am           C
não? Meu amor foi feito na medida pra você é só me deixar provar e
       G                   Am  C  Am/E           G  G4  G  G4
amar você vai ver  Olha eu ai.....      Olha eu ai.....      Olha
   Am  C  Am/E          G   G4  G  G4        Am  C  Am/E         G  G4  G
eu ai.....     Olha eu ai.....      Olha eu ai.....      Olha eu ai.....
G4         Am  C  Am/E         G  G4  G  G4  G
   Olha eu ai.....    Olha eu ai.....

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am/E = 0 X 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
G = 3 2 0 0 0 3
G/C = X 3 5 4 3 3
G4 = 3 5 5 5 3 3
