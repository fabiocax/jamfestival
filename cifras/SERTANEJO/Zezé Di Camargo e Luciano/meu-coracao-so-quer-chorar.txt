Zezé Di Camargo e Luciano - Meu Coração Só Quer Chorar

Intro: D C G D C D G  G9 G G4 G G9 G

G                     C                G  G9 G G4 G G9 G
Sempre que me deixa assim pensando coisas
                                 F F9 F F4 F F9 F
Saio pela noite a fora a te procurar
D                         G  G9 G G4 G G9 G
  Meu coração só quer chorar

G                 C             G
Dentro da cabeça pensamentos voam
                                F F9 F F4 F F9 F
Perdidos sem saber por onde começar
D                          G  G9 G G4 G G9 G
  Meu coração só quer chorar

  D                                       C
Me diga por favor por Deus por que agiu assim
                               G
Onde está o erro que mudou em mim
                          D
Tira essa dúvida do meu olhar

                                            C
Me diga por favor por Deus que não me esqueceu
                                 G
O nosso grande amor ainda não morreu
                          D
Tira essa dúvida do meu olhar
                        G
Meu coração só quer chorar


Intro: D C G D C D G  G9 G G4 G G9 G


G                 C             G
Dentro da cabeça pensamentos voam
                                F F9 F F4 F F9 F
Perdidos sem saber por onde começar
D                          G  G9 G G4 G G9 G
  Meu coração só quer chorar

  D                                       C
Me diga por favor por Deus por que agiu assim
                               G
Onde está o erro que mudou em mim
                          D
Tira essa dúvida do meu olhar
                                            C
Me diga por favor por Deus que não me esqueceu
                                 G
O nosso grande amor ainda não morreu
                          D
Tira essa dúvida do meu olhar
C                        G  G9 G G4 G G9 G
Meu coração só quer chorar ...Chorar, Chorar, Chorar
D                        G  G9 G G4 G G9 G
Meu coração só quer chorar ...

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
F = 1 3 3 2 1 1
F4 = 1 3 3 3 1 1
F9 = 1 3 5 2 1 1
G = 3 2 0 0 0 3
G4 = 3 5 5 5 3 3
G9 = 3 X 0 2 0 X
