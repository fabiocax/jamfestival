Zezé Di Camargo e Luciano - Chamando Seu Nome

Intro:  Gb Abm7 Bbm7 B Dº Ebm7 Ab7 Abm7 B Ebm7 Ab7 B Gb Db7 Gb Abm7 Gb Db7

Gb            Abm7
 Um mar revolto
Gb
 Uma revolta
        Db7
Saber que te perdi
Gb             Abm7
  Passos sem rumo
Gb
 Ida sem volta
           Db7
Assim sou eu  sem você

Gb        Abm7       Gb
*É como a lua sem a noite
          Abm7       Gb
É como o vento sem açoite
              Db7
Assim sou eu sem você

   B
Te juro, não tô aguentando
Gb
A saudade, até quando
      Db7
Fico chamando seu nome
              Gb  Gb7
Chamando seu nome
   B
Me   sinto em meio a um deserto
  Gb
Jogado sem você por perto
      Db7
Fico chamando seu nome
               Gb  Db7
Chamando seu nome

Gb  Db/F           B/Eb
   Aqui dentro de mim
        B              Db7
Brigo comigo pra te procurar
Gb   Db/F            B/Eb
   Eu sei, não é o fim
               B
Está nos seus olhos
      Db7
E no meu olhar
       Abm7                 B
Quero tudo que a gente não fez
      Bbm7               Eb7
Sem pensar no que virá depois
        Abm7
Fico chamando seu nome
   Db7          Gb
Chamando seu nome

(Gb Bbm7 B Db7) 2X
Volta ao *

----------------- Acordes -----------------
Ab7 = 4 6 4 5 4 4
Abm7 = 4 X 4 4 4 X
B = X 2 4 4 4 2
B/Eb = X 6 X 4 7 7
Bbm7 = X 1 3 1 2 1
Db/F = X 8 X 6 9 9
Db7 = X 4 3 4 2 X
Dº = X X 0 1 0 1
Eb7 = X 6 5 6 4 X
Ebm7 = X X 1 3 2 2
Gb = 2 4 4 3 2 2
Gb7 = 2 4 2 3 2 2
