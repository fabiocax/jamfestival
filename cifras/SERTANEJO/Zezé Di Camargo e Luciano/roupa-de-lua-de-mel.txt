Zezé Di Camargo e Luciano - Roupa de Lua de Mel

G                              D
Voltei era de madrugada e me assustei
 Am                  D                     G
As luzes estavam acesas, não fui eu que deixei
G            G7                     C
Entrei e vi suas coisas jogadas no chão
 Am                  D              G
Se misturando aos pedaços do meu coração
 C              D                    Bm
Fiquei parado na sala escutando o chuveiro
         Em               C
Perdi a fala, veio o desespero
          D                    G G7
Era te aceitar ou te mandar embora
C                       D                  Bm
Andei, fui até nosso quarto e vi tudo arrumado
           Em              C
Fiquei revivendo nosso passado
            A7             D
Enquanto a chuva caía lá fora
G                                        D
E ali na penumbra do quarto chorei de emoção

 Am                   D             G
Ouvindo o barulho da água caindo no chão
 G               G7                       C
Ouvi o spray do perfume, o secar dos cabelos
 Am              D                     G  G7
E vi minha felicidade me olhando no espelho
  C                 D                Bm
Você veio usando uma roupa de lua de mel
               Em               C
Trazendo no rosto o olhar mais fiel
           D                 G  G7
E ali no tapete te amei sem pensar
 C                    D                    Bm
Quando amanheceu encontrei um bilhete de adeus
               Em                  Am
Dizendo fui embora, acredite por Deus
            D                   G
Só vim te rever, não prometo voltar

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
