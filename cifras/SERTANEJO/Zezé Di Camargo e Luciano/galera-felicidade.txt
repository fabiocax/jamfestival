Zezé Di Camargo e Luciano - Galera Felicidade

Intro: D  E  A

    A (pausa)               A                     E
Eu vou quebrando, eu vou rasgando, com a bota empueirada
            E7                  A
Atrás da mulherada, atrás da mulherada
                                            E
No bailão ou no rodeio, sou mais eu nessa parada
            D         E            A
Atrás da mulherada, atrás da mulherada (bis)
(pausa) A                        E
E quando chego no bailão o coro come
D   E                             A
A minha fome de paixão vai aumentando
          A7    A                 D
Eu danço reggae, danço vanerão e rock
         E                           A
Do Chuí ao Iapóque eu tiro a poeira do chão (bis)
(pausa)     A             E
E pra morena mais bonita do salão
                E           A
Eu digo: ô trem bão, ô trem bão

                                  E
Se essa loirinha incendiar meu coração
             E              A
Eu digo: ô trem bão, ô trem bão (bis)
Falado: "Ah! Eu tô feliz. Da sua boca eu quero um beijo, eu quero bis.”
A         D               A
Vem cá morena, sai na janela
         E                      A    A7  A
Venha ver a lua, como está tão bela (bis)
A                                                         E
A lua quando vem saindo por de trás da montanha é uma solidão

                                           E                A     A7 A
Até parece uma coroa de prata, coração da mulata lá do meu sertão (bis)
A        D              A
Vem cá morena, sai na janela
      E                         A   A7  A
Venha ver a lua, como está tão bela (bis)

Fim: E  E7  A  Ab  A

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Ab = 4 3 1 1 1 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
