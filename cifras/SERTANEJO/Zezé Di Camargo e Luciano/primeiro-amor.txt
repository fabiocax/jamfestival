Zezé Di Camargo e Luciano - Primeiro Amor

Intro: D D/G C/D G/D D G/D C/D G A

D
Eu quero te ver
F#
Eu quero saber
Bm           G
Como vai você
D                       F#
Será que ainda está sozinha
    Bm      D7
Como eu estou
    G              C7/9
Não dá pra esquecer todo aquele amor
     D                  B7
Que ainda marcou a minha vida
      E7/9
Já chorei de dor
Bb          C      D  A4 A
Não quero mais sofrer
 A       G
Coração nunca mais bateu

        D
Como batia
    A        G
Solidão acabou com minha
     D
Alegria
A            G              D
Solução é ficar bonito todo dia
               B7
Pra quando você chegar
        E7/9
E se apaixonar
Bb        C      D
Como o primeiro amor

----------------- Acordes -----------------
A = X 0 2 2 2 0
A4 = X 0 2 2 3 0
B7 = X 2 1 2 0 2
Bb = X 1 3 3 3 1
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C/D = X X 0 0 1 0
C7/9 = X 3 2 3 3 X
D = X X 0 2 3 2
D/G = 3 X X 2 3 2
D7 = X X 0 2 1 2
E7/9 = X X 2 1 3 2
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
G/D = X 5 5 4 3 X
