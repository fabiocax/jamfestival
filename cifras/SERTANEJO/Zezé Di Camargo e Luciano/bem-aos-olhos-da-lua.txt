Zezé Di Camargo e Luciano - Bem Aos Olhos da Lua

 G                D
 Bem aos olhos da lua
 Em          Bm
 Resolvi te amar
 C           G
 Uma estrela sorriu a toa
 Am           D
 Ao me ver te beijar
 G             D
 Percebi que o silencio
 Em           Bm
 Fala pela emoção
 C            G
 Uma estrada só é deserta
 Am           D
 Aonde não há paixão
 C          G
 Abrace meu corpo inteiro
 C             G
 Deixe a brisa passar
 C             G
 Nós não vamos pedir carona

 Am                 D
 Enquanto o sol não chegar
 C              G
 Nessa estrada nenhum caminho
 C              G
 Vai poder nos levar
 C             G
 Passarinho só constroi o ninho
 Am          D
 Aonde puder amar
 G          C             D
 Quem nunca parou numa estrada
 G          C            D
 Pra fazer amor de madrugada
 G             C            D
 Nunca ouviu o sol dizer pra lua
                C      D
 Que ela brilha tanto
             G
 Porque é apaixonada

 Percebi que o silencio...

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
