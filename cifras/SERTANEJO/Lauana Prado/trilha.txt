Lauana Prado - Trilha

[Intro] Ab  Cm  Ab  Cm  Ab  Cm
        Ab  Cm  Bb  Fm

[Primeira Parte]

   Ab
O dia é hoje

A hora é agora
    Cm                       Bb
To desligando do mundo lá fora
    Fm
Ah, ah!

     Ab
Comprei passagem só de ida
     Cm
Pra viajar no seu sorriso
               Bb
Entrar na sua vida
    Fm
Ah, ah!


[Pré-Refrão]

             Ab
Um banho de mar
                  Cm
Seu beijo pra ganhar
                     Bb
Não tenho nada a perder
           Ab
E pra relaxar
                 Cm
Deitar no seu abraço
               Bb
Com vista pra você

[Refrão]

   Ab                            Cm
Levar minha boca pra fazer uma trilha
                Bb
Com a ponta da língua
               Fm
Seu corpo tem tudo que eu preciso
    Ab                         Cm
Quero explorar a parte mais bonita
                 Bb
Que ninguém foi ainda
           Fm
Mergulhar fundo em você

( Ab  Cm  Bb  Fm  Ab )

[Primeira Parte - Variação]

Cm
  O dia é hoje
             Bb
A hora é agora
                             Fm
To desligando do mundo lá fora
    Bb
Ah, ah!

Ab
  Comprei passagem só de ida

Pra viajar no seu sorriso
               Cm
Entrar na sua vida
    Bb
Ah, ah!

[Pré-Refrão]

             Ab
Um banho de mar
                  Cm
Seu beijo pra ganhar
                     Bb
Não tenho nada a perder
           Ab
E pra relaxar
                 Cm
Deitar no seu abraço
               Bb
Com vista pra você

[Refrão]

   Ab                            Cm
Levar minha boca pra fazer uma trilha
                Bb
Com a ponta da língua
               Fm
Seu corpo tem tudo que eu preciso
    Ab                         Cm
Quero explorar a parte mais bonita
                 Bb
Que ninguém foi ainda
           Fm
Mergulhar fundo em você

   Ab                            Cm
Levar minha boca pra fazer uma trilha
                Bb
Com a ponta da língua
               Fm
Seu corpo tem tudo que eu preciso
    Ab                         Cm
Quero explorar a parte mais bonita
                 Bb
Que ninguém foi ainda
           Fm
Mergulhar fundo em você

   Ab                      Cm
Levar, minha boca, pra, fazer uma trilha
Bb                    Fm
  Com a ponta da língua

    Ab                         Cm
Quero explorar a parte mais bonita
                 Bb
Que ninguém foi ainda
           Fm
Mergulhar fundo em você

( Ab  Cm  Bb  Fm  Ab )

 Ab        Bb         Cm
Mergulhar fundo em você

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Bb = X 1 3 3 3 1
Cm = X 3 5 5 4 3
Fm = 1 3 3 1 1 1
