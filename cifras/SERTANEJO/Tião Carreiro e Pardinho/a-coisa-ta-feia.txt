Tião Carreiro e Pardinho - A Coisa Tá Feia

[Intro]  E  B7  E

[Solo da viola]

E|-------------------------------------------------------------------------------------------------|
B|-------0-----------------------------------------------------------------------------------------|
G#|-------------------------------3--------------------1/0-----------------------------7---------8--|
E|---------------0--2--3--4-------4--0-----0--1--2---------2------------5/7--7--7--9-----9--7--9---|
B|---0-----2--4-------------------------4----------------------0-----------------------------------|

E|--7--7--7---------------------------0--10----7----5--12h-|
B|--9--9--9----9-9--------------------0--12----9----7--12h-|
G#|-------------8-8-------8---7-------------------------12h-|
E|-----------------------9---7-------------------------12h-|
B|0----------0---------0---0-------0--------0------0---12h-|

 E                  B7                   E
Burro que fugiu do laço tá debaixo da roseta
                   B7                   E
Quem fugiu de canivete foi topar com baioneta
            A                               B7
Já está no cabo da enxada quem pegava na caneta

Quem tinha mãozinha fina foi pegar na picareta
                     E           B7         E
Já tem doutor na pedreira dando duro na marreta!

            F#                     B7
A coisa tá feia,......a coisa tá preta
                      E          B7         E
Que não for filho de Deus, tá na unha do capeta!

  E                B7                    E
Criança na mamadeira já esta fazendo carreta
                   B7                       E
Até o leite das crianças virou droga na chupeta
   A                                    B7
Já está pagando o pato até filho de proveta
Mundo velho é uma bomba girando neste planeta
                           E          B7           E
Qualquer dia a bomba estoura, é só relar na espoleta!

            F#                     B7
A coisa tá feia,......a coisa tá preta
                      E          B7         E
Que não for filho de Deus, tá na unha do capeta!

  E                   B7                        E
Quem dava caixinha alta, já está cortando a gorjeta
                     B7                      E
Já não ganha mais esmola nem quem anda de muleta
     A                                   B7
Faz mudança na carroça quem fazia na carreta
Colírio de dedo duro é pimenta malagueta
                 E            B7         E
Sopa de caco de vidro, é banquete de cagueta!

            F#                     B7
A coisa tá feia,......a coisa tá preta
                      E          B7         E
Que não for filho de Deus, tá na unha do capeta!

  E                  B7                      E
Quem foi o rei do baralho virou trouxa na roleta
                   B7                     E
Gavião que pegava cobra, já foge de borboleta
 A                                    B7
Se o Picaço fosse vivo ia pintar tabuleta
Bezerrada de gravata, que se cuide, não se meta
                  E            B7      E
Quem mamava no governo, agora secou a teta!

            F#                     B7
A coisa tá feia,......a coisa tá preta
                      E          B7         E
Que não for filho de Deus, tá na unha do capeta!

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
