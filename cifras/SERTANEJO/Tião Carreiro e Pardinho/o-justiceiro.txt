Tião Carreiro e Pardinho - O Justiceiro

(intro) A E7 A A G A A G A

  A
Eu vim de longe, de onde a chuva é coisa rara
                                               E7
Onde a gente sofre e cala, dia e noite sem parar
             Bm
Eu sou de um povo que não deixa pra depois
                     E7     D      E7     A
Sou de onde agarra o boi a unha no carrascal
                                      E7
Não tive escola não escrevo sou grosseiro
  A7                                         D
Mas porém sou brasileiro deste céu azul de anil
             E7                     D
Durmo em baixeiro estendido no pedregulho
                     E7                       A         A G A
Mesmo assim eu me orgulho de ser filho do Brasil



 A
Perdi meus pais, cresci no mundo sozinho
                                                  E7
Andei por muitos caminhos, sempre escolhendo o melhor
          Bm
Passando fome fui vivendo e aprendendo
                     E7           D       E7    A
Devagar fui compreendendo que a verdade é uma só
                                   E7
Topei com a onça certo dia na cancela
 A7                                         D
Perseguindo uma vitela, cuja mãe tinha morrido
                         E7              A
Só sei dizer que a nossa luta foi tão feia
                    E7                      A        A G A
Sangue que manchou areia foi do animal vencido


 A
Como a serpente que ninguém chegava perto
                                         E7
Na tocaia do deserto quatro homens fui topar
         Bm
Quatro sujeito, quatro cabras indecentes
                   E7             D        E7     A
Tombaram na areia quente sem ter tempo pra rezar
                                      E7
Segui um rastro de um sujeito macumbeiro
  A7                                             D
Que tinha dez cangaceiros mais veloz do que um puma
           E7                          A
Cruzei fronteiras sem temer nenhum fracasso
                   E7                           A
Na justiça do meus braços desordeiro não se apruma

 A
Você seu moço, que só vive na cidade
                                                          A G A
Não conhece a verdade que se passa no sertão
           D
Aonde o homem
                    A
Faz a lei na pura bala
                      E7                      A       A G A
Onde a gente nem não fala, pra não perder a razão

fui cara a cara , peito a peito, frente a frente
 A7                D
Vi tombar um inocente
        E             A         A G A
Nas garras de um valentão
          D                      A
Brigaram tanto por causa do ordenado
                     E7                     A         A G A
Um deles era o empregado e o outro era o patrão

 A
Quem fere a ferro, com ele vai ser ferido
 A7                                           D
Por Deus nada é esquecido, Liberdade, Paz e Amor
                   E7          A
Só a justiça vence no juízo final
                  E7
Quando tudo for parar
D       E7       A
Na balança do Senhor

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
G = 3 2 0 0 0 3
