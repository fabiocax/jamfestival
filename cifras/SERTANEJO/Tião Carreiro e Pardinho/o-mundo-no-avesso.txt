Tião Carreiro e Pardinho - O Mundo No Avesso

(intro)  D A7 D A7 D A7 D

                                               A7
O mundo já está no avesso , no avesso eu dou embalo
                                         D    D7
Carneiro comendo leão e o pinto matando galo
   G                                        A7
Cavaleiro vai por baixo , por cima vai o cavalo
                                            D  D7
É sapo engolindo cobra e o coco quebrando ralo
    G                     A7
É mulher virando homem, homem virando mulher
     D            A7    D     A7                        D
Do jeito que o diabo gosta Tá... do jeito que o diabo quer

D                                              A7
O mar não esta pra peixe , a vida ta um caso serio
                                              D   D7
Eu já estou vendo defunto indo a pé pro cemitério
    G                                          A7
O touro mata o toureiro , soldado prende o sargento

                                           D   D7
Banana come o macaco e a cobra morde São Bento

D                                            A7
Já tem criança nascendo cobre enfermeira no tapa
                                         A7
Onde e que nos estamos tentaram matar o papa
    G                                    A7
A cruz foge do diabo , cachorro foge do gato
                                                D   D7
Tem queijo treinado boxe pra quebrar a cara do rato

D                                                   A7
Qualquer dia a lua esquenta , qualquer dia o sol esfria
                                             D  D7
O sol vai andar de noite , caminha a lua de dia
    G                               A7
O inquilino não paga e na casa continua
                                              D   D7
Empregado já tem força pra jogar o patrão na rua

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
