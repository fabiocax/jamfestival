Tião Carreiro e Pardinho - Esqueça Tua Maria

[Intro] C  F  G7  C  F  G7  C

C                      A7          Dm
Eu sei de gente que anda aqui na redondeza
      G7           F             G7       C
Rindo da minha tristeza Do meu triste padecer
                A7               Dm
Gente que sabe a vida feliz que tive
      Fm            C          D    G    C
Sabe até onde ela vive E não querem me dizer

C                 A7          Dm
Se eu soubesse, eu não ia condenar
    G7         F                G7         C
Só queria perguntar Por que foi que ela deixou
             A7             Dm
Nossa morada onde nós vivia bem
      Fm            C        D    G    C
Talvez por causa de alguém Maria me abandonou

C                A7                Dm
Gente malvada por despeito e por maldade

    G7                 F           G7           C
Pra aumentar mais a saudade E pra mais me envenenar
              A7                Dm
Passa na rua quando me vê na janela
      Fm            C        D    G    C
Me pergunta sempre dela E se ela vai voltar

C                   A7             Dm
Pois essa gente que invejava o meu amor
     G7                    F         G7              C
Pra aumentar mais a minha dor Veja o que fizeram um dia
                 A7                   Dm
Pois escreveram com carvão na minha porta
      Fm            C        D    G    C
Seu amor nunca mais volta Esqueça tua Maria

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
