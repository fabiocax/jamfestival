Tião Carreiro e Pardinho - Rei Dos Canoeiros

A
Segunda Feira de tarde estava caindo garoa

Cheguei na beira do rio peguei a velha canoa

E a canoa foi rodando
                          G↓   D↓ A
Ai, ai eu fui sentado na proa


A
Lá no porto das Araras que o rio claro deságua

Vou entrando na vazante água pesada recoa

Eu jogo a tarrafa n'água
                            G↓  D↓ A
Ai, ai tirar peixe a gente soa



A
No lugar que não da nada a gente desacorçoa

Deixo o meu anzol de espera onde o peixe grande amoa

Eu volto alegre pro rancho
                          G↓  D↓ A
Ai, ai quando faço pesca boa


A
O vento forte do sul vem deitando as taboas

A garça da meia volta pra descer lá na lagoa

Ela vem de manhã cedo
                              G↓  D↓ A
Ai, ai quando é de tarde ela voa


A
Sou violeiro e pirangueiro e só canto moda boa

Todas as modas que eu invento quem escuta não enjoa

Estando com meu companheiro
                         G↓  D↓ A
Ai, ai garanto a minha coroa

----------------- Acordes -----------------
A = X 0 2 2 2 0
