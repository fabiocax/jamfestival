Tião Carreiro e Pardinho - Nosso Romance

 E      B7         E
Chora viola apaixonada
           B7         A              E
Que o seu dono tem paixão e também chora
       B7         A           E
Quanta gente por amor está sofrendo
      B7                     E
Igual a eu suspirando toda hora

            A
Pra onde foi a mulher que mais eu amo
        B7                          E
Pode estar perto também pode estar distante
Meu deus do céu não existe dor maior
            B7                         E
Do que a distância que separa dois amantes
      A         E7             A
Onde andara a paixão da minha vida
                   B7              E
Será que canta ou será que está chorando
Se nesta hora ela estiver me ouvindo

          B7                       E
Perdão querida se lhe maltrato cantando

         A
Tenho certeza que ela nunca esquece
                B7             E
Nunca esquece daquelas horas tão belas
O nosso mundo pequenino foi tão lindo
         B7                    E      E7
Quatro paredes uma porta e uma janela
        A             E7          A
Fomos felizes num pedacinho de mundo
              B7              E
Só o silêncio estava de setinela
Aquele beijo que durou quinze minutos
           B7                        E
Depois meu braço foi o  travisseiro dela

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
