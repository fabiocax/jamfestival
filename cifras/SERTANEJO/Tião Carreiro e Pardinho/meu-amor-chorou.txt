Tião Carreiro e Pardinho - Meu Amor Chorou

(intro) D A7 D A7 D

D           A7                D
Meu amor chorou Eu também chorei
                    A7
(Foi na hora da partida ai ai meu Deus
                       D
Como é triste a despedida) (bis)

D                  A7
Saí tristonho chorando
                    D
Pra fazer a embarcação
                 A7
Eu deixei o meu amor
                D
Na saída do portão
                 A7
Eu deixei o meu amor
                D   (intro)
Na saída do portão


(refrão)
 D                 A7
Bem na hora da partida
                   D
Coração triste chorou
                   A7
Ao despedir do meu bem
                 D
Coração quase parou
                   A7
Ao despedir do meu bem
                 D   (intro)
Coração quase parou

(refrão)

D                  A7
Saí tristonho chorando
                    D
Cortando pelas estrada
                   A7
Como é triste é penoso
                     D
Despedir da minha amada
                   A7
Como é triste é penoso
                     D   (intro)
Despedir da minha amada

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
