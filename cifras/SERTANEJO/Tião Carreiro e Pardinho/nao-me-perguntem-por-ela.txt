Tião Carreiro e Pardinho - Não Me Perguntem Por Ela

(intro) A7 D A7 D A7 D A7 D

     A7                 D         A7             D
Por favor não me perguntem, Se eu sei onde ela esta
  A7                    D     A7             D
Perguntando eu fico triste e sou capaz de chorar
  A7                    D     A7              D
Perguntando eu fico triste e sou capaz de chorar.

  A7                 D     A7               D
Vocês precisam ter pena, vocês precisam ter dó
                              A7                D
De alguém que esta sofrendo, chorando e vivendo só
 A7                  D     A7                     D
Todo mundo esta sabendo, que eu morro por causa dela
                             A7               D
Não aumente o meu sofrer, me perguntando por ela.

(refrão)
 A7                  D      A7             D
Já vai completar um ano que ela se foi de mim

                           A7         D
Um ano de amargura e de tormento sem fim
  A7                 D     A7               D
Não cabe mais sofrimento nessa pobre vida minha
                          A7                  D
O pior e que eu não sei se ela ainda esta sozinha.

(refrão)
 A7                  D      A7                D
Vocês podem ter certeza vou findar com minha dor
                        A7            D
Hoje mesmo vou sair em busca do meu amor
 A7                   D    A7           D
Mesmo que já tenha outro ocupando meu lugar
                             A7             D
Vou mostrar a minha raça comigo ela vai voltar.

(refrão)

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
