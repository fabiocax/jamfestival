Tião Carreiro e Pardinho - Bandeira Branca

|--12----------------------------------------------------------------|
|--12---------------0-----------0-------------0-------------0--------|
|--12-------------------------------------------------------------7--|
|--12---0--2--4--7--------7--7---------7--7----------7--9------------|
|--12------------------0-----------0-------------0------------0------|


|--------------------------------------------------------------------|
|----0-----------0-----------0-------------0-------------0-----------|
|8---------8--7------------------------------------------------------|
|----------------------9--7--------7--7----------0--4----------4--7--|
|-------0-----------0-----------0-------------0-------------0--------|


|------------------------------------------12-12-12-
|--0-------------0------------0------------12-12-12-
|------------------------------------------12-12-12-
|---------0--2----------2--5---------0--0--12-12-12-
|-----0-------------4------------0---------12-12-12-



Eb
Vou contar o que nunca vi pro sertão e prá cidade

Nunca vi guerra sem tiro, e nem cadeia sem grade.
            Bb7
Nunca vi um prisioneiro que não queira a liberdade
            Eb           Bb7               Eb    Bb7 Eb (2x)
Nunca vi mãe amorosa do filho não ter saudade

Eb
Nunca vi homem pequeno que ele não fosse papudo

Eu nunca vi um doutor fazer falar quem é mudo
      Bb7
Nunca vi um boiadeiro carregar dinheiro miúdo
       Eb                    Bb7          Eb       Bb7 Eb
Nunca vi homem direito vestir calça de veludo.

Eb
Eu nunca vi um carioca que não fosse bom sambista

Nunca vi um pernambucano que não fosse bom passista
      Bb7
Nunca vi um paraibano que não fosse repentista
      Eb                Bb7            Eb      Bb7 Eb (2x)
Nunca vi um deputado apanhar de jornalista

Eb
Eu nunca vi um paulista da vida se mar dizendo

Nunca vi um paranaense que não esteja enriquecendo
        Bb7
Eu nunca vi um baiano no facão sair perdendo
        Eb                   Bb7        Eb       Bb7 Eb
Eu nunca vi um mineiro da luta saí correndo

Eb
Nunca vi um catarinense depois de velho aprendendo

Nunca vi um matogrossense de medo andar tremendo
         Bb7
Eu nunca vi um gaúcho prá laçá precisar de treino
         Eb                Bb7            Eb       Bb7 Eb (2x)
Eu nunca vi um goiano por paixão beber veneno.

Eb
Nunca vi um fazendeiro andar em cavalo que manca

Prá fechar a boca de sogra não vi chave, não vi tranca.
      Bb7
Prá terminar meu pagode vou falar botando panca
  Eb                       Bb7              Eb        Bb7 Eb
Quero ver meus inimigos levantar bandeira branca.

----------------- Acordes -----------------
Bb7 = X 1 3 1 3 1
Eb = X 6 5 3 4 3
