Tião Carreiro e Pardinho - Punhal da Falsidade

Intro: F C7 F C7

 Fm
Deixe Esta mulher cheia de anéis
                                      Bbm
Junto com seus coronéis Em delírio a beber
                        C7
Deixe Que ela erga sua taça
                                         Fm
Em saúde da desgraça Que arruinou o meu viver

Deixe Que ela siga vida louca
                     F                   Bbm
beijando de boca em boca, tudo isso terá fim
                         Fm                      C7
Eu, que lhe dei um amor puro, hei de vê-la no futuro,
                     F
pagar o que fez pra mim

F                             C7
Eu quero vê-la chorar de saudades

                                   F
Lembrar os tempo em que foi tão feliz
                            C7
Quando o punhal de sua falsidade
                 Bb             F
Deixou em minha alma essa cicatriz
 F                              C7
Viro-lhe o rosto e pelas madrugadas
                                  F
Eu seguirei com a dor que me consome
             Bbm                    F
Tal qual um boêmio sem rumo e sem nada
         C7                      F   (intro)
Pela traição de uma mulher sem nome.

(repete tudo)

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bbm = X 1 3 3 2 1
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
