Tião Carreiro e Pardinho - Laçador de Cachorro

E|-10-10-10-10-10/13-13--08-08-08-08-08/11-11--11-11-11-11-11-08-08-5-5-08-08---------6-
B|-11-11-11-11-11/15-15--10-10-10-10-10/13-13--13-13-13-13-13-10-10-6-6-10-10-6-6-6/8-6-
G|---------------------------------------------------------------------------6-6-6/7--6-
E|------------------------------------------------------------------------------------6-
B|--------------------------------------------------------------------------------------


Intro: E  A  E  A  D  A

A                E                       A
Um laçador de cachorro do coração de serpente.
                E                      A
Homem só pela metade diabo em forma de gente.
                      E
Não vale um tostão furado quem persegue um inocente.
D                  E                    A
Vi uma cena cruel que não sai da minha mente.
                    E                         A
Pra salvar seu cachorrinho coitado de um garotinho,
                 D     A
Foi morto covardemente.        (Repique 2X E A)


A                E                        A
Um garoto de dez anos depois que fez a lição.
                 E                       A
Pertinho da sua casa brincava no quarteirão.
                   E
Com seu luluzinho branco cachorro de estimação.
D                         E                   A
De repente igual um raio vinha um laçador de cão.
               E                      A
O garoto assustado pegou seu cão estimado,
                  D   A
E apertou no coração.

A                     E                      A
Com seu cãozinho nos braços ele saiu na carreira.
                E                    A
Laçador bateu atrás igual onça pegadeira.
                  E
Vou laçar esse moleque e arrastar pela poeira.
D                     E               A
Foge, foge luluzinho da laçada traiçoeira.
                     E                        E
Embora eu seja arrastado mas meu cãozinho adorado,
                          D   A
É uma flor que ninguém cheira.

( E  A )

A                E                          A
Garoto perdeu a vida nas mãos de um homem ruim.
                  E                        A
Despedindo deste mundo coitadinho disse assim.
                     E
Trate bem meu cachorrinho que tem nome de Marfim.
D                       E                  A
Papaizinho, mamãezinha peço não chore por mim.
                  E                          A
Vocês precisam coragem que a morte é uma passagem,
                      D   A
Pra vida que não tem fim.

A                  E                       A
Nossa justiça não falha a lei não está dormindo.
                    E                         A
No fundo de uma prisão vai ter fim esse assassino.
                     E
Ele vai morrer nas grades sem vela e em desatino.
D                              E            A
E mais uma triste história escrita pelo destino.
                 E                     A
Depois do aconteceu o cachorrinho morreu,
                 D   A
De saudade do menino.

( E  A )

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
