Tião Carreiro e Pardinho - Confiança

E                                    (B)         E    B
Quem tem confiança no peito Canta em qualquer altura
E                                             B
Quem tem confiança no braço Não põe arma na cintura
F#7                                               B
Quem tem confiança na esposa Nunca trás ela segura
       (B)         (E)             (B)        (E)
Quem não tem confiança em deus É doente que não tem cura

E                                    (B)         E    B
Quem tem porco no chiqueiro Tem confiança na gordura
E                                             B
Quem tem gado na invernada Tem confiança na fartura
F#7                                        B
A mulher que tem beleza Não confia na pintura
     (B)         (E)             (B)        (E)
Quem confia no Mobral Não caminha nas escuras

E                                    (B)         E    B
Quem nunca comprou bacia Tem confiança na gamela
E                                             B
Quem confia no cachorro Não põe grade na janela

F#7                                               B
Quem tem confiança no santo Não precisa queimar vela
     (B)         (E)             (B)        (E)
Quem não põe tranca na porta Tem confiança na tramela

E                                    (B)         E    B
Quem tem confiança na terra Não põe adubo na planta
E                                             B
Quem tem confiança no galo Não perde a hora e levanta
F#7                                               B
O despertador da roça É o galo quando canta
     (B)         (E)             (B)        (E)
Riqueza do cantador Tá no peito e na garganta

----------------- Acordes -----------------
B = X 2 4 4 4 2
E = 0 2 2 1 0 0
F#7 = 2 4 2 3 2 2
