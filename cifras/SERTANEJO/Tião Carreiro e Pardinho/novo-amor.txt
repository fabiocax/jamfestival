Tião Carreiro e Pardinho - Novo Amor

Introdução:  D A7  ( 4 x)

D             A7                               D  D7
Você não é mais meu grande amor
             G          A7         D
Não adianta querer voltar
               A7                          D
Meu coração já sabe esquecer
                          A7                            D
Aprendeu escolher só quem sabe amar!

                     A7                                      D
Você não serve nem para beijar os pés
                         A7                                    D
De um novo amor que agora pra mim surgiu
                A7                                D   D7
Eu encontrei com minha felicidade
                 G                   A7                  D
Depois do dia em que você se despediu.
                   E7                               A
Meu novo amor veio de outra galáxia

                    E7                                              A
De corpo e alma nos meus braços se entregou
                A7
Não tive sorte no meu primeiro romance
                     G               A7           D
Mas no segundo a felicidade dobrou.

                            A7                         D
Eu escapei da fogueira de um vulcão
                    A7                                          D
Entrei num lago que só tem ondas de amor
                      A7                               D   D7
Sai de um mundo rodeado de espinhos
                        G                A7                D
Entrei num mundo rodeado de esplendor.
                   E7                                             A
Deixei a fera, estou nos braços de uma Santa
                        E7                                     A
Meu mundo é lindo sou feliz nos dias meus
                 A7
Sai do inferno, agora estou no céu
                         G                 A7                       D
Das mãos do diabo fui parar nas mãos de Deus

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
G = 3 2 0 0 0 3
