Tião Carreiro e Pardinho - Viola Cabocla

D
Viola cabocla não era lembrada
                               A7
Veio pra cidade sem ser convidada
Junto com os vaqueiros trazendo a boiada
                                 D
No cheiro do mato, no pó da estrada
                E             D
Fez grande sucesso com a disparada........

(intro)

D
Viola cabocla feita de pinheiro
                                A7
Que leva alegria ao sertão inteiro

Trazendo a saudade dos que já morreram
                                   D
Nas noites de lua, tu sai no terreiro
                E                D
Consolando a mágoa do triste violeiro!


(intro)

D
Viola cabocla é bem brasileira
                              A7
Sua melodia atravessou fronteira

Levando a beleza pra terra estrangeira
                             D
No nosso sertão é a mensageira
               E              D
É o verde amarelo da nossa bandeira.....

(intro)

D
Viola cabocla seu timbre não falha
                             A7
Criada no mato como a samambaia,

Veio pra cidade de chapéu de palha
                                 D
Mostrou seu valor vencendo a batalha
              E                D
Voltou pro sertão trazendo a medalha!

(intro) Termina em D

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
