Tião Carreiro e Pardinho - Arrependida

[Intro] G  Am  D7
        G  Em  D7  G

G                                 D
Eu não sou culpado se hoje você chora
                             G
Foi você mesma que me abandonou
                             D
Implorei tanto pra não ir embora
            C                G
Mas minhas súplicas não escutou
                               D
Hoje você chora triste arrependida
                               G
Para os meus braços você quer voltar
             G7                  C
Você foi maldosa arruinou minha vida
        D                  G
Me compreenda não vou perdoar

( G  Am  D7  G  Em  D7  G )


 G                            D
Na sua ausência eu chorei de dor
                         G
Não suportei fui á sua procura
                             D
Encontrei você com um novo amor
         C                G
Trocava beijos e fazia juras
                         D
Naquela noite fiquei embriagado
                     G    G7
Amanheci bebendo no bar
                      C
Estava triste e desesperado
            D                 G
Chamei seu nome comecei a chorar
O seu retrato que eu tinha guardado
pra não recorda-la eu já joguei fora
Existe outra que vive ao meu lado
Que me faz carinho depois que foi embora
Segue seu caminho vai viver na lama
Por piedade esqueça de mim
Por sua culpa todos me difamam
O meu nome rola num abismo sem fim

       D                           G
Segue mulher, vai viver de mão em mão
          D                          G7
Porque o remorso pouco a pouco lhe consome
          C                      G
Sinto um dor dentro do meu coração
          D    C
Tenho vergonha
                           G
Por você usar... o meu sobrenome

( G  Am  D7  G  Em  D7  G )

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
