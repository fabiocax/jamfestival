Tião Carreiro e Pardinho - Nossas Vidas

introdução (G D C  G D G)

G
A mulher que eu mais amei vive distante
                        D
E com ela foi também minha paixão
        C               G
Por isso hoje eu caminho muito triste
C               D       G
Neste mundo só vivemos de ilusão


G
Eu me lembro nossas madrugadas frias
        G7              C
Bem mais fria a minha alma padeceu
                        D       G
Que dor imensa, que tristeza, que fracasso
                A               D       G
Ver em outros braços quem já foi todo o meu ser



D                       G
Vai mulher vai mostrar tua beleza
                D       C       G
Esse dom da natureza não é qualquer um que tem
                C               G
Mas não se esqueça que nós temos muitas vidas
                A       D               G
Eu te esperarei querida, nesse mundo ou mais além

----------------- Acordes -----------------
A = X 0 2 2 2 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
