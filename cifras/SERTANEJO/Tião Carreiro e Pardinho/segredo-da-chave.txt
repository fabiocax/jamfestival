Tião Carreiro e Pardinho - Segredo da Chave

(intro) D G A7 D A7 D A7 D

D
Preto vei saiu da mata
      A7            D
vei aqui pra trabalhar (2x)
        A7              D
o que Deus faz desse mundo
        A7           D   (intro)
ninguém pode desmanchar. (2x)

D
A lunga está trabalhando
       A7             D
junto com o pai Carreiro (2x)
       A7          D
fazendo serviço limpo
       A7             D   (intro)
no centro deste terreiro. (2x)

D
Cachoeira de água limpa

        A7             D
corre em pé corre deitada (2x)
     A7          D
sua água cristalina
         A7         D   (intro)
por Deus foi abençoada (2x)

D
Essa chave é um segredo
        A7             D
que eu não posso explicar (2x)
        A7            D
distranca e abre caminho
       A7              D   (intro)
ninguém pode mais trancar (2x)

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
