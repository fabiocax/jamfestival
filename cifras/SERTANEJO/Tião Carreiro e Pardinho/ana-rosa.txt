Tião Carreiro e Pardinho - Ana Rosa

(intro) E7 A E7 A

E|----------------------------------
B|----------------------------------
G|--------------------A--E7--A|-----
E|----------4-----0-----------------
B|--2-/-5------0--------------------

A                      E7
Ana Rosa casou com Chicuta
                          A
uma caipira bastante atrasado
                      D
Levava a vida de carreiro
             E7                   A
fazendo transporte era o seu ordenado
                             E7
Tinha um ciúme doentio pela moça
                     A
que dava pena do coitado
                  D
Batia na pobre mulher com a vara

     E7                 A   D   E7 A
de ferrão de bater no gado, ai.

A                       E7
Resolveu abandonar o marido
                          A
porque a vida já não resistia
                      D
Quando chegou em Botucatu
          E7          A
aquela cidade toda dormia
                          E7
Só encontrou uma porta aberta
                        A
mas ali não entrava família
            D
Resolveu contar sua história
       E7                  A   D   E A7
e se abrigar até no outro di...a.

A                           E7
O Chicuta quando chegou em casa
                   A
Ana Rosa não encontrou
                  D
Ele arreou sua besta e
         E7              A
como uma fera a galope tocou
                   E7
Na chegada de Botucatu
                         A
pra um caboclo ele perguntou
                D
Seu moço essa mulher lá nas
     E7                     A    D   E7 A
fortunata vi quando ela entrou, ai.

A                     E7
Num barzinho ali da saída
                        A
sem destino resolveu chegar
                          D
Encontrou com um tal Menegildo
            E7               A
e com o Costinha pegou conversar
                             E7
Vocês querem pegar uma empreitada
                       A
só se for pra não trabalhar
                    D
Pra matar a minha mulher minha
    E7                A   D   E7 A
proposta vai lhe agradar, ai.

A                      E7
O Costinha montou a cavalo
                     A
e tocou lá pra fortunata
                     D
Conversando com Ana Rosa disse
              E7                A
que era um tropeiro da zona da mata
                               E7
Meu patrão lhe mandou uma proposta
                              A
diz que leva e nunca lhe maltrata
                        D
Seu marido anda a sua procura
               E7            A    D   E7 A
jurou que encontrando ele te ma...ta.

A                     E7
Ana rosa montou na garupa
                       A
e o cavalo saiu galopeando
                       D
Quando chegou no lava-pé aonde
        E7                  A
os bandidos já estavam esperando
                          E7
Quando ela avistou seu marido
                          A
para todo santo foi chamando
                          D
Vou perder minha vida inocente
              E7                  A   D   E7 A
partirei com Deus deste mundo tirano, ai.

A                    E7
Derrubaram ela da garupa
                      A
já fazendo cruel judiação
                        D
Foi cortando ela aos pedaços
              E7                  A
uma preta assistindo a cruel judiação
                             E7
Foi correr dar parte a autoridade
                       A
já fizeram imediata prisão
                           D
Hoje lá construíram uma igreja tem
          E7                   A   D   E7 A
feito milagre pra muitos cristãos, ai.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
