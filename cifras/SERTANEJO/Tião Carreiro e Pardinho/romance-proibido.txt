Tião Carreiro e Pardinho - Romance Proibido

(intro)  D A7 D A7 D

D                               A7
Eu gostava tanto, tanto, tanto dela
                                       D
Que no meu peito grande paixão se escondia
                              A7
E para mim ela nunca revelou nada
                            D
E para ela eu também nada dizia
        G
Mas um dia o amor falou mais alto
                            A7      D
De corpo e alma nos seus braços eu caia
                                 A7
Mais um beijo e um abraço disse tudo
            G         A7            D
Sem dizer nada falei tudo que eu queria

                    A7         D
A mulher da minha vida Foi embora

                 A7          D
Foi pra longe viver Noutro país.
       G                           A7     D
Aqui distante estou curtindo uma saudade
            A7                       E       (intro)
Perdi pra sempre a mulher que tanto quis.

D                               A7
Nosso romance era contra lei divina
                                 D
O melhor jeito foi abreviar seu fim
                                  A7
Deus me tirou de uma estrada proibida
                                   D
Ela foi embora e o destino quis assim
        G
Deus escreve direito por linhas tortas
                        A7         D
Todo mal transforma em semente do bem
                                 A7
Quando um amor se despede vai embora
           G             A7             D
Deixa um espaço para um outro amor que vem

(refrão)

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
