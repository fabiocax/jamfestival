Tião Carreiro e Pardinho - Ato de Bravura

Intro: A7 D A7 D

D                     A7                     D
Estou numa guerra quente, subindo a temperatura
                     A7                      D
Uma mulher e dois homens, a parada vai ser dura
                    A7                         D
Os dois doentes por ela, é um só que vai ter cura
                    A7                     D
Eu jogo a casca no fogo, ponho ovo na gordura
D7                  G                          D   A7
Pela mulher que eu amo, todo o meu sangue derramo
                    D    Intro.
Não entrego a rapadura.

D                    A7                          D
Eu jurei que ela é minha, e não quebro a minha jura
                     A7                        D
Se eu perder essa parada, troco até de assinatura
                    A7                         D
Por esse amor tão forte, eu faço qualquer loucura

                     A7                     D
Eu tenho um chimith west, tanto corta como fura
D7               G                          D    A7
Com ele eu abro ala, na hora que eu mando bala
                       D     (intro)
Eu clareio a noite escura.

D               A7                        D
A menina vive presa, e está muito bem segura
                  A7                      D
Debaixo de sete chaves, segredo na fechadura
                  A7                       D
Tem uma guarda severa, para lhe dar cobertura
                      A7                           D
Só tem guardas escolhidos, dois metros e dez de altura
D7                   G                     D    A7
Vou vencer esta batalha, a menina é a medalha
                    D     (intro)
Pro meu ato de bravura.

D                 A7                       D
Eu roubei essa menina, meu anjinho de candura
                 A7                    D
Da cabeça até os pés, ela só tem formosura
                  A7                     D
Hoje eu digo para ela, apertando na cintura
                     A7                     D
Meu bem escapei com vida, sem cair na sepultura
D7              G                         D    A7
Confesso de coração, minha vida era um limão
                D     (intro)
Agora virou doçura.

D                  A7                            D
Eu comprei uma casinha, só falta passar a escritura
                   A7                          D
Está no tijolo a vista, sem reboque e sem pintura
                    A7                        D
Mas dentro tem uma jóia, que muita gente procura
                 A7                    D
Para mim caiu do céu, essa linda criatura
D7                 G                          D    A7
Minha casinha modesta, não é um castelo em festa
                      D  A7 D
Mas tem amor com fartura.

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
