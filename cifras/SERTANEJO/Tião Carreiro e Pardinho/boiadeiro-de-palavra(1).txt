Tião Carreiro e Pardinho - Boiadeiro de Palavra

A               E7                       A
Boiadeiro de palavra que nasceu lá no sertão
                                              E7
Não pensanva em casamento por gostar da profissão
                        D             E7                   A
Mas ele caiu no laço de uma rosa em botão
                  E7                   A
Morena cor de canela cabelos cor de carvão
   A7                  D       E7             A
Desses cabelos compridos quase esbarrava no chão
                       E7                 A
E pra encurtar a história era filha do patrão

  A                   E7                       A
Boiadeiro deu um pulo de pobre foi a nobreza
                                          E7
Além da moça ser rica dona de grande beleza
                     D        E7             A
Ele disse assim pra ela com classe e delicadeza:
                       E7                        A
- Esses cabelos compridos são a minha maior riqueza

   A7             D            E7         A
Se um dia você cortar nos separa na certeza
                     E7                       A E7 A
Além de te abandonar vai haver muita surpresa

A                    E7                A
Um mês depois de casado o cabelo ela cortou
                                       E7
Boiadeiro de palavra nessa hora confirmou
                    D          E7         A
No salão que a esposa foi com ela ele voltou
                    E7                     A
Mandou sentar na cadeira e desse jeito falou:
   A7               D             E7         A
- Passe a navalha no resto do cabelo que sobrou
                   E7                          A  E7 A
O barbeiro não queria a lei do trinta mandou

 A                   E7                     A
Com o dedo no gatilho pronto pra fazer fumaça
                                      E7
Ele virou um leão querendo pular na caça
                     D            E7        A
Quem mexeu nesse cabelo vai cortar o resto de graça
                   E7                 A
A navalha fez limpeza na cabeça da ricaça
A7                D          E7               A
Boiadeiro caprichoso caprichou mais na pirraça
                 E7                     A   E7 A
Faz a morena careca dar uma volta na praça


A                   E7                  A
E lá na casa do sogro ela falou sem receio
                                                E7
- Vim devolver sua filha pois não achei outro meio
                   D           E7             A
A minha maior riqueza eu olho e vejo no espelho
                    E7                       A
É um rosto com vergonha que à toa fica vermelho
    A7                D            E7           A
Sou igual a um puro sangue que não deita no arreio
                  E7                      A
Prefiro morrer de pé do que viver de joelho

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
