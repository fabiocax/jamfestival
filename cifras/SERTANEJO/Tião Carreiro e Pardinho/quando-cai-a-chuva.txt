Tião Carreiro e Pardinho - Quando Cai a Chuva

Introdução: (A7 D A E7 A)

A                                              Bm
Quando cai a chuva lembro meu bem que me abandonou
                      E7             Bm      E7      A
Partiu para bem distante em meus braços não mais voltou
                        A7                       D
Quando cai a chuva olhando as flores do meu jardim ?
                    A            E7             A
Lembro do meu benzinho que hoje vive longe de mim.

  E7            A            E7            A
Quando cai a chuva aumenta mais meu dissabor
  E7              A                 E7          A
Por que a fria chuva me traz lembrança do meu amor ?

(Falado)

A noite era chuvosa e eu me lembro
Cruéis recordações me faz sofrer.
Recordo a hora, o mês, era uma noite de dezembro.

Aquela noite, não consigo esquecer.
Segui seus passos até a estação
E no momento que ela partia
As lágrimas que rolavam dos meus olhos
Misturavam-se com a chuva que caia.
Por isso quando cai a chuva
Molhando as flores do meu canteiro.
Em meu quarto solitário,
Minhas lágrimas molham a fronha do meu travesseiro?.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
