Luan Santana - Mais Que Amigos

Intro: D A G/B A D A G

D                  F#m            G             Bm          A/C#
Não é preciso mais adormecer pra sonhar com um Anjo descendo do céu
G           D/F#      Em                   A4  A
Basta Você perceber que sou mais que um amigo fiel

D                    F#m               G            Bm      A/C#
Sou aquele que traz a alegria de Deus e a entrega direto ao seu coração
G              F#m     Bm       C           G/A      A
E com Você vou sorrir e chorar. Lado a lado vamos caminhar

 Em             D/F#         G/B               A/C#
Quando de ajuda Você precisar dou minha vida pra te resgatar
Em               D/F#            G                        A4     A
Esse é o desejo de Deus (de Deus) de hoje em diante o seu Anjo sou eu!

D                    A          G         Bm        A
Sou muito mais que um amigo sou um Anjo que o Senhor enviou!
D                  A/C#
Pode gritar para o mundo ouvir

       Em       G       A       D
Sou um Anjo que o Senhor enviou pra Ti!

D              F#m                G                   Bm         A/C#
Não tenho asas nem sei voar mas o que o mundo não pode eu posso te dar!
G              D/F#                Em          A4  A
Vou lhe mostrar o caminho de Deus só Ele pode santificar
Em              D/F#     G/B              A/C#
Quando de ajuda Você precisar dou minha vida pra te resgatar
Em             D/F#                      G                   A4     A
Esse é o desejo de Deus (de Deus) de hoje em diante o seu Anjo sou eu...

D                    A            G        Bm      A
Sou muito mais que um amigo sou um Anjo que o Senhor enviou!
D                 A/C#
Pode gritar para o mundo ouvir
        Em      G     A          Dm
Sou um Anjo que o Senhor enviou pra Ti!

Dm             C                 Gm             Dm         C/E      Dm
Quando Você se ferir e do céu se afastar eu te trarei para perto de Deus
Dm            C/E                 Bb/F             Gm                 A   Bb   Bb/D
Quando sentir solidão vem comigo rezar e eu levarei suas preces  a  Deus
Eb                 Bb/D           Ab           Cm      Bb
Nós somos mais que amigos: somos Anjos que o Senhor enviou!
Eb                 Bb/D             Ab           Cm      Bb
Vamos gritar para o mundo ouvir somos Anjos que o Senhor enviou!
Eb                Bb/D              Ab           Cm      Bb
Nós somos mais que amigos: somos Anjos que o Senhor enviou!
Eb                Bb/D              Ab           Cm         Bb              Eb
Vamos gritar para o mundo ouvir somos Anjos que o Senhor enviou pra Ti!

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A4 = X 0 2 2 3 0
Ab = 4 3 1 1 1 4
Bb = X 1 3 3 3 1
Bb/D = X 5 X 3 6 6
Bb/F = X 8 8 7 6 X
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
G/A = 5 X 5 4 3 X
G/B = X 2 0 0 3 3
Gm = 3 5 5 3 3 3
