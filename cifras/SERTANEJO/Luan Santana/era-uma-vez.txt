﻿Luan Santana - Era Uma Vez

Capo Casa 6

[Intro] D  G  D

         D
Era uma vez
                            G
Um dia em que todo dia era bom
      D
Delicioso gosto e o bom gosto
                                G
Das nuvens serem feitas de algodão
          A
Dava pra ser herói
                                  Bm
No mesmo dia em que escolhia ser vilão
      A                             G
E acabava tudo em lanche,um banho quente
                  D
E talvez um arranhão

          D
Dava pra ver

                    G
A ingenuidade a inocência cantando no tom
            D
Milhões de mundos e universos tão reais
                     G
Quanto a nossa imaginação
            A
Bastava um colo, um carinho e o remédio
                  Bm
Era beijo e proteção
         A                        G
Tudo voltava a ser novo no outro dia
                 D
Sem muita preocupação

                     G
É que a gente quer crescer
A                          Bm
  E quando cresce quer voltar do início
                   G
Porque o joelho ralado
         A                       Bm
Dói bem menos que um coração partido
                     G
É que a gente quer crescer
A                          Bm
  E quando cresce quer voltar do início
                   G
Porque o joelho ralado
         A                       Bm
Dói bem menos que um coração partido

----------------- Acordes -----------------
Capotraste na 6ª casa
A*  = X 0 2 2 2 0 - (*D# na forma de A)
Bm*  = X 2 4 4 3 2 - (*Fm na forma de Bm)
D*  = X X 0 2 3 2 - (*G# na forma de D)
G*  = 3 2 0 0 0 3 - (*C# na forma de G)
