Luan Santana - Everest

Intro: G  C9  Em7  D9  D4  D

G
Eu vou contar meu segredo
C9
Falar pro mundo inteiro
 Em7
Tatuar o seu nome
D9
Roubar o seu beijo
C9
Porque eu te amo sem medo

          G
Quer saber
        C9
Faço de tudo pra te merecer
       Em7
Quer saber
         D                   D4 D D9
O impossível é pouco pra você


Refrão:
G                                   C9
Vou escalar o Everest com uma mão só
                                         Em7
Atravessar um oceano em um barco de papel
                                          D9
Se for preciso eu posso até roubar um avião
D4  D  D9
Pra te levar pro céu

G                                       C9
Eu corro a 120 com um carro na contramão
                                       Em7
Me lanço ao vento do decimo quinto andar

Aprendo a voar
             D D4 D D9
Para te provar

      Am
Que a fé move montanhas
      C/B       C
E eu movo o mundo

Pra te amar

Solo: G  C9  Em7  C9

F                                         Am
Eu atravesso o universo, mudo o meu destino por você
       C9
Por você

Refrão:
G                                   C9
Vou escalar o Everest com uma mão só
                                         Em7
Atravessar um oceano em um barco de papel
                                          D9
Se for preciso eu posso até roubar um avião
D4  D  D9
Pra te levar pro céu

G                                       C9
Eu corro a 120 com um carro na contramão
                                       Em7
Me lanço ao vento do decimo quinto andar

Aprendo a voar
             D  D4  D  D9
Para te provar

      Am
Que a fé move montanhas
      C/B       C
E eu movo o mundo

Pra te amar

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C/B = X 2 2 0 1 0
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
D4 = X X 0 2 3 3
D9 = X X 0 2 3 0
Em7 = 0 2 2 0 3 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
