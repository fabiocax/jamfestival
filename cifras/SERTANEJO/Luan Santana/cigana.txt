Luan Santana - Cigana

        G
Um dia uma cigana leu a minha mão
        C9
Falou que o destino do meu coração
         Am
Daria tantas voltas, mas ia encontrar você

        G
Eu confesso que na hora duvidei
            Am
Lembrei de quantas vezes eu acreditei,
                        C                G
Mas não dava certo, não era pra acontecer

      C9
Foi só você chegar
      Am
Pra me convencer
               F#
Estava escrito nas estrelas, eu ia te conhecer
            C9
Foi só você me olhar

        F#
Que eu me apaixonei
          G5/E                     D
Valeu a pena esperar, esse grande amor, que eu sempre sonhei
          G
Vou te amar, pra sempre vou te amar

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C9 = X 3 5 5 3 3
D = X X 0 2 3 2
F# = 2 4 4 3 2 2
G = 3 2 0 0 0 3
