Luan Santana - Bailando (feat. Enrique Iglesias)

Intro: Em  C  G  D

E|-0-2-2h3-2-0-3--2--3-2-0-----------------|
B|-----------------------------------------|
G|-----------------------------------------|
D|-----------------------------------------|
A|-----------------------------------------|
E|-----------------------------------------|

E|-0-2-2h3-2-0-3-------0-----------3-2-1/2-|
B|---------------3-3-3---3-3-0-3-3---------|
G|-----------------------------------------|
D|-----------------------------------------|
A|-----------------------------------------|
E|-----------------------------------------|

E|-3-5-7-5-3-/7--5-5/3-/7--5-5/3-3---------|
B|-5-7-8-7-5-/8--7-7/5-/8--7-7/5-5---------|
G|-----------------------------------------|
D|-----------------------------------------|
A|-----------------------------------------|
E|-----------------------------------------|


E|-8-8-8-10--8-8--7-7----------------------|
B|-8-8-8-10--8-8--8-8--3-3-----------------|
G|---------------------4-4-----------------|
D|-----------------------------------------|
A|-----------------------------------------|
E|-----------------------------------------|

E|-8-8-7-7-5-5--3-3/2--2-------------------|
B|-8-8-8-8-7-7--5-5/3--3-------------------|
G|-----------------------------------------|
D|-----------------------------------------|
A|-----------------------------------------|
E|-----------------------------------------|

Em                               C
Yo te miro, se me corta la respiración
G                                    D
Cuanto tú me miras se me sube el corazón
(Bate bem mais forte o coração)
Em                                      C
Y en un silencio tu mirada dice mil palabras
G                                            D
La noche en la que te suplico que no salga el sol

   Am
Bailando (bailando)
   C
Bailando (bailando)
                G
Dois corpos no cio, um imenso vazio
               D
Esperando o amor (esperando o amor)
   Am
Bailando (bailando)
   C
Bailando (bailando)
               G
Esse fogo por dentro, vai me enlouquecendo
             D
E a gente suando

                    Em
Con tu física y tu química también tu anatomía
                     C
La cerveza y el tequila y tu boca con la mía
            G
Ya no puedo más (não aguento mais)
             D
Ya no puedo más (não aguento mais)
            Em
A nossa melodia tem calor, tem fantasia
           C
Até filosofia, é desejo que vicia
             G
Não aguento mais (ya no puedo más)
             D
Não aguento mais (ya no puedo más)

Refrão:
                   Em
Yo quiero estar contigo, vivir contigo
           C
Bailar contigo, tener contigo
           G
Una noche loca (una noche loca)
            D
Ay besar tu boca (y besar tu boca)
                  Em
Eu quero estar contigo, sorrir contigo
           C                             G
Dançar contigo, viver contigo uma noite louca
               D
Tão tremenda e louca

 Em   C    G    D
(Ooh, ooh, ooh, ooh)

Em                                   C
Tu me miras y me llevas a otra dimensión
(Me leva a outra dimensão)
 G                            D
Tu latidos aceleran a mi corazón
(Seu brilho acelera o meu coração)
Em                                C
Que ironia do destino não poder tocar você
 G                                  D
Abrazarte y sentir la magia de tu olor

 Am
Bailando (bailando)
   C
Bailando (bailando)
                G
Dois corpos no cio, um imenso vazio
               D
Esperando o amor (esperando o amor)
   Am
Bailando (bailando)
   C
Bailando (bailando)
               G
Esse fogo por dentro, vai me enlouquecendo
             D
E a gente suando

     Em
Con tu física y tu química también tu anatomía
                     C
La cerveza y el tequila y tu boca con la mía
            G
Ya no puedo más (não aguento mais)
             D
Ya no puedo más (não aguento mais)
            Em
A nossa melodia tem calor, tem fantasia
           C
Até filosofia, é desejo que vicia
             G
Não aguento mais (ya no puedo más)
             D
Não aguento mais (ya no puedo más)

Refrão:
                   Em
Yo quiero estar contigo, vivir contigo
           C
Bailar contigo, tener contigo
           G
Una noche loca (una noche loca)
            D
Ay besar tu boca (y besar tu boca)
                  Em
Eu quero estar contigo, sorrir contigo
           C                             G
Dançar contigo, viver contigo uma noite louca
               D
Tão tremenda e louca

 Em   C    G    D
(Ooh, ooh, ooh, ooh)

Em
Bailando amor
C               G
Bailando amor
                     D
Es que se me va el dolor

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
