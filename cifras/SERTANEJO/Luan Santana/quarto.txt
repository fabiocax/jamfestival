Luan Santana - Quarto

[Intro] Am  F  G

[Primeira Parte]

Am
   Deixa eu ver, deixa eu ver
                  F      G
Tô querendo só olhar, olhar
Am
  Deixa eu ver se a vista desse quarto
            F        G
Vai me agradar, agradar

[Pré-Refrão]

 Am               G           F         G
Porque eu vou passar o resto desse ano lá
 Am          G
Vou mudar algumas coisas
     F                      G
Pra gente não sair mais de lá


[Refrão]

 Am            F
Deixa a cama, deixa as paredes
Am
  Vou pôr areia, pra fazer amor na praia
Dm
  Tirar o teto, pra fazer à luz da lua
Am
  Pôr uma faixa de pedestre e uma placa
Dm
  Pra quando der vontade de se amar na rua

 Am            F
Deixa a cama, deixa as paredes
Am
  Vou pôr areia, pra fazer amor na praia
Dm
  Tirar o teto, pra fazer à luz da lua
Am
  Pôr uma faixa de pedestre e uma placa
Dm
  Pra quando der vontade de se amar na rua

[Primeira Parte]

Am
   Deixa eu ver, deixa eu ver
                  F      G
Tô querendo só olhar, olhar
Am
  Deixa eu ver se a vista desse quarto
            F        G
Vai me agradar, agradar

[Pré-Refrão]

 Am               G           F         G
Porque eu vou passar o resto desse ano lá
 Am          G
Vou mudar algumas coisas
     F                      G
Pra gente não sair mais de lá

[Refrão]

 Am            F
Deixa a cama, deixa as paredes
Am
  Vou pôr areia, pra fazer amor na praia
Dm
  Tirar o teto, pra fazer à luz da lua
Am
  Pôr uma faixa de pedestre e uma placa
Dm
  Pra quando der vontade de se amar na rua

 Am            F
Deixa a cama, deixa as paredes
Am
  Vou pôr areia, pra fazer amor na praia
Dm
  Tirar o teto, pra fazer à luz da lua
Am
  Pôr uma faixa de pedestre e uma placa
Dm
  Pra quando der vontade de se amar na rua

Am
  Vou pôr areia, pra fazer amor na praia
Dm
  Tirar o teto, pra fazer à luz da lua
Am
  Pôr uma faixa de pedestre e uma placa
Dm
  Pra quando der vontade de se amar na rua

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
