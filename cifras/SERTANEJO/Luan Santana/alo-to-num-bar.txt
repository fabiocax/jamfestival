Luan Santana - Alô To Num Bar

Fm                     C#
Alo, to num bar, chego já
                   G#           Eb
Tô aqui batendo um papo, comendo água.
Fm                       C#
Alo, tou num bar, chego já
                   G#              Eb
Pode ir fazendo a cama pra quem te ama

Fm                     Eb
Hoje convidei alguns amigos pra beber
Fm                    Eb
Daqui a pouco só vai dar eu e você

     C#
Não fique preocupada nem grilada
                G#
porque não vou demorar
   C#
Eu Não vou te deixar abandonada
                    C7
Vale a pena me esperar

(C7)
Pra gente se amar

Fm                     C#
Alo, to num bar, chego já
                   G#                  Eb
Tô aqui batendo um papo, tomando uma gelada
Fm                      C#
Alo, tou num bar, chego já
                   G#              Eb
Pode ir fazendo a cama pra quem te ama

               C#              Eb
Daqui a pouco amor volto pra casa
                 C#
Pra gente dar um show
        Eb                C#
De madrugada vamos fazer amor
           Eb                     C#
Beijar na boca vou te dar meu calor
           C7
Te deixar louca

----------------- Acordes -----------------
C# = X 4 6 6 6 4
C7 = X 3 2 3 1 X
Eb = X 6 5 3 4 3
Fm = 1 3 3 1 1 1
G# = 4 3 1 1 1 4
