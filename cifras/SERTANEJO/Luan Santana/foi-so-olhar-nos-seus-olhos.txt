Luan Santana - Foi Só Olhar Nos Seus Olhos

Intro: Am F C G

   Am
Preciso encontrar um jeito de falar
   F
Coragem não tenho, mas não posso deixar
       C                                           G  (G)
Pra depois, posso me arrepender se eu não me declarar

         Am
Você não sabe mas faz mais de uma semana

Eu não durmo direito
F
Fico horas ensaiando em frente ao espelho
C
Como é que eu vou dizer pra você
          G         F
Que eu me apaixonei

F           G
Seja o que Deus quiser

        C       G      Am     G   F G
Vou gritar pra quem quiser ouvir

Refrão:
        Am
Que foi só olhar nos seus olhos como um toque de mágica
F                                       C
Beijo na sua boca pra me convencer que nada
                                 G
Que eu sentia antes de você era amor

        Am
Que foi só olhar nos seus olhos como um toque de mágica
F                                       C
Beijo na sua boca pra me convencer que nada
                                 G
Que eu sentia antes de você era amor
      Am
Era amor

Intro: ( Am F C G )

   Am
Preciso encontrar um jeito de falar
   F
Coragem não tenho, mas não posso deixar
       C                                           G  (G)
Pra depois, posso me arrepender se eu não me declarar

         Am
Você não sabe mas faz mais de uma semana

Eu não durmo direito
F
Fico horas ensaiando em frente ao espelho
C
Como é que eu vou dizer pra você
          G         F
Que eu me apaixonei

F           G
Seja o que Deus quiser
        C       G      Am     G   F G
Vou gritar pra quem quiser ouvir

Refrão:
        Am
Que foi só olhar nos seus olhos como um toque de mágica
F                                       C
Beijo na sua boca pra me convencer que nada
                                 G
Que eu sentia antes de você era amor

        Am
Que foi só olhar nos seus olhos como um toque de mágica
F                                       C
Beijo na sua boca pra me convencer que nada
                                 G
Que eu sentia antes de você era amor
      Am
Era amor

Intro: ( Am F C G )

      Am
Era amor

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
