Luan Santana - O Nosso Tempo É Hoje

F                Am
Apague a luz e vem, vem se deitar meu bem
G                  C9
Já pega o cobertor pra me cobrir de amor
F                  Am
Deixa a TV no mudo, pra não ficar escuro
G                     C9
Pra eu poder te olhar, enquanto eu te beijar
Am          G        C9
Não olha assim, eu não vou responder por mim
Am              G
Vou te chamar

C9                   F                        G
Quero que se prepare êê, o nosso tempo é hoje eee
C9                    F                      G C9
Quero que se prepare êê, o nosso tempo é hoje eee

F              Am
Viaja comigo Traz o seu perigo
G               C9
Nós nessa fogueira a noite inteira

F              Am
Viaja comigo traz o seu perigo
G               C9                   F     Am G C9
Nós nessa fogueira O nosso tempo é hoje êêê iê iê

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C9 = X 3 5 5 3 3
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
