Luan Santana - Faz de Conta (Curtição)

Intro: C Am F G

C                                        G
Chega fazendo de conta que não me conhece não
      F
Sorri pra pra todo mundo
Dm
Me olha por um segundo
G
Pra ver se tem minha atenção

C
Se falo contigo me ignora
            G
Não quer me dar razão
F                                 Dm
Diz pra todo mundo que vive me dando fora
   G
Que eu sirvo só pra curtição

           C
Me diz pra quê?


Fazer tanto charminho
      Am
Tá perdendo seu tempo, sem me dar carinho
F
Assuma de vez
    Dm
Que quer me amar
G
Quem desdenha quer comprar
         C
Quero pedir, menina não brinque comigo
    Am
Não deixe de mais meu coração de castigo
     F                            Dm
Você sabe que tem toda minha atenção
      G
E que eu não sirvo só pra curtição

         C
Quer me amar
     Am
Se entregar
           F       Dm
Não quer falar
               G
Que é meu o seu coração
             F      G         C
Que eu não sirvo só pra curtição

( C Am F G   A )

          D
Me diz pra quê?

Fazer tanto charminho
       Bm
Tá perdendo seu tempo, sem me dar carinho
G
Assuma de vez
    Em
Que quer me amar
A
Quem desdenha quer comprar
         D
Quero pedir, menina não brinque comigo
    Bm
Não deixe de mais meu coração de castigo
     G                            Em
Você sabe que tem toda minha atenção
     A
E que eu não sirvo só pra curtição

        D
Quer me amar
     Bm
Se entregar
           G      Em
Não quer falar
               A
Que meu o seu coração
             G      A          D
Que eu não sirvo só pra curtição

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
