Luan Santana - Sempre Com Você

C        G
E eu que achava que
Am           F
a vida fosse só uma viagem
C           G             Am        F
que o mundo tinha me esquecido!
C        G
E eu que achava que
Am            F
o amor não existia pra todo mundo
C       G           Am     F
que era coisa de cinema!
Am              G
Ai você apareceu
                      Am
com um olhar me convenceu
                   G
anjo de asas coloridas
   F     G      C
amor alem da vida!
             G          Am
Eu não resisti a tanto amor

           F     G      C
o meu coração se entregou
           G
E venha o que vier
    Am            F      G    C
eu vou estar pra sempre com você!

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
