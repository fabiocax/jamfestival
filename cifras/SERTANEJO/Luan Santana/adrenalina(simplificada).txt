Luan Santana - Adrenalina

Refrão:
A                       E
   Meu coração tá disparado
                 F#m
Meu corpo tá viciado
                   D                    A
Nessa louca adrenalina que me faz arrepiar
                      E
Meu sangue ferve nas veias
                     F#m
Quando você me incendeia
 D       A  E   D
Vem me amar

Primeira Parte:
A                E
  Eu sou vidrado   em você
  D
Química perfeita não consigo entender
A              E
  É impossível   te dizer não

   D
Você criou as regras

E estou na palma das suas mãos

Segunda Parte:
Bm                       D
  Tô me arriscando pelo seu amor
A                     E
  Me aventurei pra ficar perto do seu calor
Bm              D
  Inconsequente   nossa paixão
A                 E
  A cada dia uma nova emoção

Refrão:
A                       E
   Meu coração tá disparado
                 F#m
Meu corpo tá viciado
                   D                    A
Nessa louca adrenalina que me faz arrepiar
                      E
Meu sangue ferve nas veias
                     F#m
Quando você me incendeia
 D       A
Vem me amar

                     E
Meu coração tá disparado
                 F#m
Meu corpo tá viciado
                   D                    A
Nessa louca adrenalina que me faz arrepiar
                      E
Meu sangue ferve nas veias
                     F#m
Quando você me incendeia
 D       A  E    D
Vem me amar

Primeira Parte:
A                E
  Eu sou vidrado   em você
  D
Química perfeita não consigo entender
A              E
  É impossível   te dizer não
   D
Você criou as regras

E estou na palma das suas mãos

Segunda Parte:
Bm                       D
  Tô me arriscando pelo seu amor
A                     E
  Me aventurei pra ficar perto do seu calor
Bm              D
  Inconsequente   nossa paixão
A                 E
  A cada dia uma nova emoção

Refrão:
A                       E
   Meu coração tá disparado
                 F#m
Meu corpo tá viciado
                   D                    A
Nessa louca adrenalina que me faz arrepiar
                      E
Meu sangue ferve nas veias
                     F#m
Quando você me incendeia
 D       A
Vem me amar

Terceira Parte:
Bm                         D
  Quando você chegar mais perto vai ver
A                      E
  Que a pulsação acelera quando to com você
Bm           D
  Essa magia   chega e domina
A                             E
  Meu peito explode é pura adrenalina

Refrão:
A                       E
   Meu coração tá disparado
                 F#m
Meu corpo tá viciado
                   D                    A
Nessa louca adrenalina que me faz arrepiar
                      E
Meu sangue ferve nas veias
                     F#m
Quando você me incendeia
 D       A
Vem me amar

                     E
Meu coração tá disparado
                 F#m
Meu corpo tá viciado
                   D                    A
Nessa louca adrenalina que me faz arrepiar
                      E
Meu sangue ferve nas veias
                     F#m
Quando você me incendeia
 D       A  E  F#m
Vem me amar
 D
Vem me amar

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
