Luan Santana - Sempre Com Você

Intro:
E|--4--4--4--4\2--2--2--1--1-----|--2--2--2--2---2--2--2--2--|
B|--4--4--4--4\2--2--2--2--2-----|--2--2--2--2---2--2--2--2--|
G|-------------------------------|---------------------------|
D|-------------------------------|---------------------------|  (3x)
A|-------------------------------|---------------------------|
E|-------------------------------|---------------------------|

E|--4--4--4--4\2--2--2--1--1-----|--2--2--2--2---4--4--4--4--|
B|--4--4--4--4\2--2--2--2--2-----|--2--2--2--2---4--4--4--4--|
G|-------------------------------|---------------------------|
D|-------------------------------|---------------------------|  (3x)
A|-------------------------------|---------------------------|
E|-------------------------------|---------------------------|

C#        Ab
E eu que achava que
Bbm           F#
a vida fosse só uma viagem
C#           Ab             Bbm        F#
que o mundo tinha me esquecido!

C#        Ab
E eu que achava que
Bbm            F#
o amor não existia pra todo mundo
C#       Ab           Bbm     F#
que era coisa de cinema!

Bbm           Ab/C
Ai você apareceu
      Fm              Bbm
com um olhar me convenceu
                   Ab
anjo de asas coloridas
   F#     Ab      C#
amor alem da vida!

Refrão:
          Ab          Bbm
Eu não resistia  tanto amor
           F#     Ab      C#
o meu coração se entregou
          Ab
E venha o que vier
    Bbm            F#      Ab    C#
eu vou estar pra sempre com você!

Solo: ( Bbm C# D#/C Bbm F# C# D#/C Bbm F# )

Bbm              Ab
Ai você apareceu
       Fm             Bbm
com um olhar me convenceu
                   Ab
anjo de asas coloridas
   F#     Ab      C#
amor alem da vida!

Refrão 2x:
          Ab          Bbm
Eu não resistia  tanto amor
           F#     Ab      C#
o meu coração se entregou
          Ab
E venha o que vier
    Bbm            F#      Ab    C#
eu vou estar pra sempre com você!

Solo: ( C# Ab Bbm F# C# D#/C Bbm F# )

Sempre com você!
Pra sempre com você!

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
Ab/C = X 3 X 1 4 4
Bbm = X 1 3 3 2 1
C# = X 4 6 6 6 4
D#/C = X 3 5 X 4 3
F# = 2 4 4 3 2 2
Fm = 1 3 3 1 1 1
