Luan Santana - Eu, Você, o Mar e Ela

Intro: Dm7  F  Am  G4(6)

Intro e Primeira Parte:

Parte 01 de 02

   Dm7              F
E|---------------5--------5-----7-8---------|
B|-------6-----8--------6-----6-------------|
G|-----5-----5--------5-----5---------------|
D|---7-----7--------------------------------|
A|-5----------------8-----------------------|
E|------------------------------------------|

Parter 02 de 02

   Am                G4(6)
E|-------8-----10-7--------7-----8-5--------|
B|-----5-----5-----------5-----5------------|
G|---5-----5-----------5-----5--------------|
D|-7-----------------5----------------------|
A|------------------------------------------|
E|------------------------------------------|


Primeira Parte:

Dm7        F          Am     G4(6)
    Ser romântico às vezes ajuda
Dm7         F
    Mas se fecho os olhos
 Am         G4(6)
Te imagino nua

Dm7           F
    Talvez pareça
          Am      G4(6)
Uma cena de Hollywood
Dm7        F                  Am
    Se tá pensando isso, por favor
         G4(6)
Não se ilude

Dm7        F         Am        G4(6)
    Eu só quero uma noite de amor
Dm7         F
    Como as outras
        Am          G4(6)
Só mais uma que passou

Refrão:

                       Am
Mas foi só a porta fechar
     G               Dm
Pra mudar minha cabeça
   F                Am
A sua boca vale o preço
                 G              Dm
Pra perder o sossego que eu tinha


   F                Am
A lua até beijou o mar
     G                Dm
Pra não ficar de vela
    F                   Am
Os quatro perdidos de amor
       G          Dm  F
Eu, você, o mar e ela
Am        G          Dm  F  Am  G
   Eu, você, o mar e ela

Primeira Parte:

Dm        F          Am     G
   Ser romântico às vezes ajuda
Dm         F
   Mas se fecho os olhos
 Am         G
Te imagino nua

Dm           F
   Talvez pareça
          Am      G
Uma cena de Hollywood
Dm        F                  Am
   Se tá pensando isso, por favor
         G
Não se ilude

Dm        F         Am        G
   Eu só quero uma noite de amor
Dm         F
   Como as outras
        Am          G
Só mais uma que passou

Refrão:

                       Am
Mas foi só a porta fechar
     G               Dm
Pra mudar minha cabeça
   F                Am
A sua boca vale o preço
                 G              Dm
Pra perder o sossego que eu tinha

   F                Am
A lua até beijou o mar
     G                Dm
Pra não ficar de vela
    F                   Am
Os quatro perdidos de amor
       G          Dm  F
Eu, você, o mar e ela
Am        G          Dm  F  Am
   Eu, você, o mar e ela

     F                 Am
Mas foi só a porta fechar
     G               Dm
Pra mudar minha cabeça
   F                Am
A sua boca vale o preço
                 G              Dm
Pra perder o sossego que eu tinha

                    Am
A lua até beijou o mar
     G                Dm
Pra não ficar de vela
    F                   Am
Os quatro perdidos de amor
       G          Dm  F  Am  G
Eu, você, o mar e ela
        Dm  F  Am
O mar e ela
       G
Eu, você, o mar e ela

----------------- Acordes -----------------
Am = X X 7 P5 5 8
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
F = X 8 X P5 6 5
G = 3 2 0 0 3 3
G4(6) = X X P5 5 5 7
