Luan Santana - Dia, Lugar e Hora

Intro: E  Am9  E  Am9  E  A9
       E  A/E  E  E4

Primeira Parte - Primeira Estrofe:

      E
Se a moça do café não demorasse tanto

Pra me dar o troco
   E7  D/F#  E/G#  A7M
Se eu não   ti____vesse

Discutido na calçada
                 Am
Com aquele cara louco
                       E
E ó que eu nem sou de rolo

Parte 1 de 3

   E   E7    D/F#  E/G#
E|-0---4-----2-----4------------------------|
B|-0---3-----3-----5------------------------|
G|-1---4-----2-----4------------------------|
D|-2----------------------------------------|
A|-2----------------------------------------|
E|-0---0-----2-----4------------------------|


Parte 2 de 3

        A7M              Am
E|-2/4----------4---4-----------------------|
B|----------5---5---5----5------------------|
G|--------6---6---6---6--5------------------|
D|------------------------------------------|
A|------0----------------0------------------|
E|------------------------------------------|

Parte 3 de 3

   E
E|------------------------------------------|
B|-9--9--10--9--7--9------------------------|
G|-9--9---9--9--8--9------------------------|
D|-9--9--11--9-----9------------------------|
A|------------------------------------------|
E|-0----------------------------------------|

Primeira Parte - Segunda Estrofe:

       E
Se eu não tivesse atravessado
                         E7
Aquela hora no sinal vermelho
(lick 1)     A9
Se eu não parasse bem na hora do almoço
                Am
Pra cortar o cabelo
                      (lick 2)
E ó que eu nem sou vaidoso

Parte 1 de 4

E|-0-0--0-0-0-0--0-0-0---0-0-0-0------------|
B|-0-0--0-0-0-0--0-0-0---0-0-0-0------------|
G|-1-1--1-1-1-1--1-1-1---1-1-1-1------------|
D|-2-2--2-2-2-2--2-2-2---0-0-0-0------------|
A|-2-2--2-2-2-2--2-2-2---2-2-2-2------------|
E|-0-0--0-0-0-0--0-0-0---0-0-0-0------------|


Parte 2 de 4(Lick 1)

E|------------------------------------------|
B|-7/9-7-5----------------------------------|
G|------------------------------------------|
D|-7/9-7-6----------------------------------|
A|------------------------------------------|
E|---------7\3------------------------------|

Parte 3 de 4

E|-0-0--0-0-0-0--0-0-0---0-0-0-0-0----------|
B|-0-0--0-0-0-0--0-0-0---1-1-1-1-1----------|
G|-2-2--2-2-2-2--2-2-2---2-2-2-2-2----------|
D|-2-2--2-2-2-2--2-2-2---2-2-2-2-2----------|
A|-0-0--0-0-0-0--0-0-0---0-0-0-0-0----------|
E|------------------------------------------|


Lick 2:

E|------------------------------------------|
B|-9h10-9--7/9\5----------------------------|
G|-9----9-----------------------------------|
D|-9h11-9--7/9\6----------------------------|
A|------------------------------------------|
E|------------------------------------------|

Pré-refrão:

    E            B/D#
Eu não teria te encontrado
    Bm/D          D/E  E/G#
Eu não teria me apaixonado
    A
Mas   aconteceu
           Am              E  B
Foi mais forte que eu e você

Aí eu disse

Parte 1 de 2

   E        B/D#      Bm/D     D/E  E/G#
E|-0---0-----------------------2----4-------|
B|-0---0----7---7-----7---7----3----5-------|
G|---1---1--4---4-----4---4------2----4-----|
D|------------4---4-----4---4---------------|
A|----------6---------5---------------------|
E|-0---------------------------0----4-------|

Parte 2 de 2

   A                         E    B
E|-----------0--0---0-0-0----0-0----0-0h2---|
B|-0h2-----0-----------------0-0--4-------0-|
G|-2-----2------4/5-4-2-2h4--1-1------------|
D|-----2--------------------------4---------|
A|-0------------------------------2---------|
E|---------------------------0--------------|

Refrão:

 E                     F#m7(11)
Quer que eu faça um café?
               B4
Ou faça minha vida
                C#m7
Se encaixar na sua?
  B4           A9
Aqui mesmo na rua
  E/G#        F#m7(11)
Era pra ser agora
               E/G#  A9
Quando é pra acon_tecer

Tem dia, lugar e tem hora

Intro: E  Am9  E  Am9  E  A9
       E  A/E  E  E4

Repetição Primeira Parte - Segunda Estrofe:

       E
Se eu não tivesse atravessado
                         E7
Aquela hora no sinal vermelho
(lick 1)     A9
Se eu não parasse bem na hora do almoço
                Am
Pra cortar o cabelo
                       E
E ó que eu nem sou vaidoso

Repetição Pré-refrão:

    E            B/D#
Eu não teria te encontrado
    Bm/D          D/E  E/G#
Eu não teria me apaixonado
    A9
Mas    aconteceu
           Am              E
Foi mais forte que eu e você

Aí eu disse

Refrão:

 E                     F#m7(11)
Quer que eu faça um café?
               B4
Ou faça minha vida
                C#m7
Se encaixar na sua?
  B4           A9
Aqui mesmo na rua
  E/G#        F#m7(11)
Era pra ser agora
               E/G#  A9
Quando é pra acon_tecer
     B4               E
Tem dia, lugar e tem hora
B4
Eu disse

 E                     F#m7(11)
Quer que eu faça um café?
               B4
Ou faça minha vida
                C#m7
Se encaixar na sua?
  B4           A9
Aqui mesmo na rua
  E/G#        F#m7(11)
Era pra ser agora
               E/G#  A9
Quando é pra acon_tecer
     B4
Tem dia, lugar e tem hora

Intro: E  Am9  E  Am9  E  A9
       E  A/E  E  E4

      E
Se a moça do café não demorasse tanto

Base Solo: C#m7  B4  A9 (8x)

Solo:

Parte 1 de 6

E|----------------------------------------------------|
B|------------12~-------------------12p11p9\7---7-----|
G|-11b13r11p9-------11/13---------------------9---9/11|
D|------------------------11~9/11\9-------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

Parte 2 de 6

E|-----9--11b12~~r11p9--------14-16-------------------|
B|-/9-------------------17b19-------14~~b\------------|
G|----------------------------------------------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

Parte 3 de 6

E|-19-------------------------------------------------|
B|----17----------21\19p17----------------14-16-------|
G|-------14/16-18----------16-------11/13-------------|
D|----------------------------18\14-------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

Parte 4 de 6

E|----------------------------------------------------|
B|----------------------------------------------------|
G|-11/13-13~~-----------------------------------------|
D|----------------------------------------------------|
A|----------------------------------------------------|
E|----------------------------------------------------|

Parte 5 de 6

E|----------------------------------------------------|
B|----------------------------------------------------|
G|----------------------------------------------------|
D|-11~~\9-11------------------------------------------|
A|------------9/11~-----------------------------------|
E|----------------------------------------------------|

Parte 6 de 6

E|----------------------------------------------------|
B|----------------------------------------------------|
G|----------------------------------------------------|
D|------9---------------------------------------------|
A|-9/11---11-9-7-9------------------------------------|
E|-----------------9~---------------------------------|

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/E = 0 X 2 2 2 0
A7M = X 0 2 1 2 0
A9 = X 0 2 2 0 0
Am = X 0 2 2 1 0
Am9 = X 0 2 4 1 0
B = X 2 4 4 4 2
B/D# = X 6 X 4 7 7
B4 = X 2 4 4 5 2
Bm/D = X 5 X 4 7 7
C#m7 = X 4 6 4 5 4
D/E = X X 2 2 3 2
D/F# = 2 X 0 2 3 2
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E4 = 0 2 2 2 0 0
E7 = 0 2 2 1 3 0
F#m7(11) = 2 X 2 2 0 X
