Luan Santana - Palácios e Castelos

Intro: E  F#  B  E  F#  B  E  B  F#
E|---------------4~-4~-6-7-9--7-6-7-11-11~-|
B|-4-4-5-7-5-4-5-5~-5~-8-9-11-9-7-9-12-12~-|
G|-6-6-7-9-7-5-7---------------------------|
D|-----------------------------------------|
A|-----------------------------------------|
E|-----------------------------------------|

E|------------------------------------------|
B|-4-4-5-7-5-4-5~-5-5-7-9--7-5-7~-7-7-9--11-|
G|-5-5-7-9-7-5-7~-7-7-9-11-9-7-9~-7-7-11-13-|
D|------------------------------------------|
A|------------------------------------------|
E|------------------------------------------|

E|---------------------------------------|
B|-12-14-12-16-12-12~--------------------|
G|-13-15-13-16-12-12~--------------------|
D|---------------------------------------|
A|---------------------------------------|
E|---------------------------------------|



Primeira parte:
 B                 E
Te pedi, que me levasse a sério
 B                 E
Construí, palácios e castelos
B                   E           C#m   F#
Esperei, por muito tempo antes de te ver

 B                 E
Li, re-li, seu manual inteiro
 B                  E
Descobri, todos os seus segredos
 B                 E         C#m  F#
Desenhei, o mapa que me leva a você

Pré-refrão:
   C#m
A gente se conheceu na escola
      G#m            F#
Eu pegando onda e jogando bola
       C#m
Você era modelo e adorava dançar
    E                       B
Praia, cinema, festinhas no ar

Refrão:
Se tá com dor eu sou doutor
 E
Tá com frio eu sou calor
                         B
Mando flores, dou beijinho em sua mão
     G#m
Se quer brincar de se esconder
 C#m
No escurinho eu e você
   F#                 B
É só pedir com o coração

    B7
Se tá com dor eu sou doutor
 E
Tá com frio eu sou calor
                         B
Mando flores, dou beijinho em sua mão
       G#m
Se quer brincar de se esconder
 C#m
No escurinho eu e você
   F#                 B
É só pedir com o coração
E|--------------|
B|-6-4-2-2-4-2~-|
G|--------------|
D|--------------|
A|--------------|
E|--------------|


(repete tudo)

Refrão 2x:
Se tá com dor eu sou doutor
 E
Tá com frio eu sou calor
                         B
Mando flores, dou beijinho em sua mão
     G#m
Se quer brincar de se esconder
 C#m
No escurinho eu e você
   F#                 B
É só pedir com o coração

----------------- Acordes -----------------
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
