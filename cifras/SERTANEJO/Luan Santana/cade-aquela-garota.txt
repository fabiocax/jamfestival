﻿Luan Santana - Cadê Aquela Garota

Capo Casa 3

Intro:
E|------6------6------3----------------------------|
B|-----------4------4------------------------------|
G|----5----3------5--------------------------------|
D|--6----------------------------------------------|
A|-------------------------------------------------|
E|-------------------------------------------------|

F      G       Am          F         G             F9
Cade aquela garota que contagiava o mundo ao seu redor?
F      G          Am              F             G               F9
Aonde foi para a sua vontade de sonhar e dar pra vida o seu melhor
C             G                  Am
Se ele te deixou não vai valer apena
           F                              C
Dar suas lágrimas pra alguém sem nenhum valor
        G
O erro não é seu
     Am
Ele quem perdeu
 F                             G
Anjo quebrado se ergue pra um novo amor


Um novo amor

C
Eu vim aqui pra fazer
Am
Você voar outra vez
Dm
Darei meu ar pra você respirar
Dm
Meu sonho pra você sonhar
F                          G
Não chore nunca mais meu anjo porque hoje

C
Eu vim aqui pra fazer
Am
Você voar outra vez
Dm
Darei meu ar pra você respirar
Dm
Meu sonho pra você sonhar
F                         G
Não chore nunca mais meu anjo porque hoje

F         G         Am
Eu vim fazer você voar outra vez
F         G        Am
Eu vim fazer você voar outra vez
F         G        Am
Eu vim fazer você voar outra vez
F         G        Am
Eu vim fazer você voar

----------------- Acordes -----------------
Capotraste na 3ª casa
Am*  = X 0 2 2 1 0 - (*Cm na forma de Am)
C*  = X 3 2 0 1 0 - (*D# na forma de C)
Dm*  = X X 0 2 3 1 - (*Fm na forma de Dm)
F*  = 1 3 3 2 1 1 - (*G# na forma de F)
F9*  = 1 3 5 2 1 1 - (*G#9 na forma de F9)
G*  = 3 2 0 0 0 3 - (*A# na forma de G)
