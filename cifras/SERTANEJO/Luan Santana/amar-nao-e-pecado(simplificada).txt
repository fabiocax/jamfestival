Luan Santana - Amar Não É Pecado

Intro 2x: Am F C9 G

       Am           F         C9           G
Eu não sei de onde vem essa força que me leva pra você
       Am          F         C9              G
Eu só sei que faz bem, mas confesso que no fundo eu duvidei
       Am         F         C9                        G
Tive medo e em segredo,  guardei o sentimento e me sufoquei
       Am       F             C9                        G
Mas agora é a hora, eu vou gritar pra todo mundo de uma vez

Refrão:
            C9
Eu tô apaixonado
                G                  Am                   F
Eu tô contando tudo e não tô nem ligando pro que vão dizer
              C9
Amar não é pecado
                 G                  Am                  F
E se eu tiver errado, que se dane o mundo, eu só quero você


Solo:
E|--------------------------------|
B|--------------------------------|
G|-----4-3-4-6-6/8-8-9-8-6-4-3-1--|
D|---6----------------------------|
A|--6-----------------------------|
E|--------------------------------|

E|--------------------------------|
B|--------------------------------|
G|-4-3-4~-6-6/8-8-9-8-6-4-3-1-----|
D|--------------------------------|
A|--------------------------------|
E|--------------------------------|


Intro 2x: ( Am F C9 G )

       Am           F         C9           G
Eu não sei de onde vem essa força que me leva pra você
       Am          F         C9              G
Eu só sei que faz bem, mas confesso que no fundo eu duvidei
       Am         F         C9                        G
Tive medo e em segredo,  guardei o sentimento e me sufoquei
       Am       F             C9                        G
Mas agora é a hora, eu vou gritar pra todo mundo de uma vez

Refrão:
            C9
Eu tô apaixonado
                G                  Am                   F
Eu tô contando tudo e não tô nem ligando pro que vão dizer
              C9
Amar não é pecado
                 G                  Am                  F
E se eu tiver errado, que se dane o mundo, eu só quero você

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C9 = X 3 5 5 3 3
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
