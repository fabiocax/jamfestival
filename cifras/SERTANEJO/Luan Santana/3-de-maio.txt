Luan Santana - 3 de Maio

Intro: C#m  E  B  F#

 B               F#
Se todo lugar eu olho eu vejo seu rosto
 G#m             E
Se a brisa do oceano vem me traz seu gosto
  B                F#
Quando você chega perto eu quase desmaio
G#m                     E
    Vai ficar marcado, hoje, dia 3 de maio

B                   F#
Acordei bem cedo e decidi tentar a sorte
G#m                      E
    Na minha vida a timidez sempre falou mais forte
B                   F#
O seu não eu já ganhei me esnobar eu saio
G#m                 E
    Vai ficar marcado, hoje, dia 3 de maio

C#m                         E             G#m  F#
    Chegou a hora e do seu lado eu começo a   suar

B                            F#             E
  Foi mal eu já nem sei mais o que ia dizer
C#m                            E            G#m  F#  B
    Trouxe essa flor de plástico pra te presen__te___ar
            F#
Eu vou te amar até a flor morrer

Refrão:
  B                                  F#
Quando o amor bate na porta é só deixar ele entrar
    E                                          G#m   E   F#
Na vida tem escolhas e o mais certo é não ter medo de errar
   B                                          F#
O beijo tão gostoso que você me deu me fez esquecer da hora
    E                                 G#m   E   F#
Eu viajei direto pra saturno e não voltei até agora

 C#m        F#
Me acertou feito um raio
 C#m  B/D#  E  G#5  F#             B
Não  vou    esque__cer, dia 3 de maio

( B F# G#m E )(2x)

C#m                         E             G#m  F#  B
    Chegou a hora e do seu lado eu começo a   su___ar
B                            F#             E
  Foi mal eu já nem sei mais o que ia dizer
C#m                            E            G#m  F#  B
    Trouxe essa flor de plástico pra te presen__te___ar
                 F#
Eu vou te amar até a flor morrer

Refrão 2x:
  B                                  F#
Quando o amor bate na porta é só deixar ele entrar
    E                                          G#m   E   F#
Na vida tem escolhas e o mais certo é não ter medo de errar
   B                                          F#
O beijo tão gostoso que você me deu me fez esquecer da hora
    E                                 G#m   E   F#
Eu viajei direto pra saturno e não voltei até agora

 C#m        F#
Me acertou feito um raio
 C#m  B/D#  E  G#5  F#             B
Não  vou    esque__cer, dia 3 de maio

B  F#
      Dia 3 de maio

----------------- Acordes -----------------
B = X 2 4 4 4 2
B/D# = X 6 X 4 7 7
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
G#5 = 4 6 6 X X X
G#m = 4 6 6 4 4 4
