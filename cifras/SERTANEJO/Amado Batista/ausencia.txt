Amado Batista - Ausência

D
Quando a saudade vem tortura-me
Bm        G      A
Na tua ausência amor
Eu não suporto a solidão
G       A     D    A D
E essa grande dor
É triste a vida sem os teus beijos
D7                   G
Sem teus abraços meu bem
            D
Meus olhos choram
Bm      A      D      D G A D A D A
Meu coração também  
D
Sigo sem rumo na esperança
Bm       G         A
De encontrar minha flor
Para dar vida em minha vida
G     A          D      A D
E reviver nosso amor
Só Deus quem sabe
O quanto te amo
D7                   G
O quanto eu te quero bem
         D
És o meu mundo
A       G       D    D G A D A D A
Meu paraíso também

D
Hoje tão triste me lembro o dia
Bm        G         A
Que tu partiste meu bem
Deixando comigo 
Tanta amargura
G         A       D       A D
E teu desprezo também
Eu suportei
Porque sou forte
D7               G
Mas eu chorei de dor
          D
Juro querida
A         G         D       D G A D A D A
Jamais terei outro amor

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
