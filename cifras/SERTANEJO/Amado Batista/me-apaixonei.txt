Amado Batista - Me Apaixonei

Intro: D A B7 E D A B7 E

E
Vou tocar nossa canção
                                      B7  A
Vou fazer voe feliz e te amar de coração

E depois de tanto amor
                                 B7            E
De emoções que nós sentimos vou ficar na solidão.
E
Com certeza vou lembrar
                                        B7  A
Dos momentos felizes, certamente vou chorar

Eu nunca vou esquecer
                                B7               E7
Nem que eu não queira lembrar, eu senti muito prazer

A
Eu senti o seu carinho,
                                          E7
O seu jeito de me ver, me apaixonei e amei você

Hoje estou aqui sozinho, não sei quando vou lhe ver
        D             A           B7            E
Pra sentir este carinho e nunca mais lhe esquecer.

E
Não vejo e hora de te ver
                                      B7  A
Estou muito ancioso pra poder amar você

Nesse nosso novo encontro
                                        B7       E
O nosso amor vai ser tanto, não tem jeito de mentir.

O encontro foi marcado
                                         B7 A
Na folhinha do calendário e decretado feriado

Neste dia não tem sol
                               B7            E
Não tem chuva e vendaval, tem amor e é natural.

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
