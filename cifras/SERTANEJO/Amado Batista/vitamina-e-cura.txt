Amado Batista - Vitamina e Cura

INTRODUÇÃO: C7   Fm C7 Fm C7 Eb Ab C7 Fm
                 Fm C7 Fm C7 Eb Ab C7 Fm

         Fm                  C
Só de te ver acho que vale a pena
        C7                   Fm
Ai de você se eu te puser as mãos
                         C
Esse teu corpo de pele morena
       C7            Fm
Me envenena de excitação
                            C
Eu tô que tô e ninguém me segura
         C7                  Fm
Tô muito louco, tô de alto astral
                        C
Você é minha vitamina e cura
         C7               Fm
Minha mistura de bem e de mal

Fm                        C7
Meu coração bate mais excitado
          Fm                C7
Meu corpo vibra de tanto querer
        Eb               Ab
O teu pedaço de amor e pecado
      C7                  Fm
É o bocado que eu quero comer

(repete tudo desde o começo)

----------------- Acordes -----------------
Ab = 4 3 1 1 1 4
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Eb = X 6 5 3 4 3
Fm = 1 3 3 1 1 1
