Amado Batista - Tempo Perdido

(intro) F C F

F         C Dm
Quando o amor passar
      Am     Bb
Da hora de terminar
          F         C
Qualquer sentimento é por obrigação
F         C    Dm
Transar só por transar
      Am    Bb
Pensamento noutro lugar
         F           C  Bb
O beijo na boca não tem mais paixão

         F
O corpo não mente
        Gm        Dm       Bb
A alma não sente, não sabe como enganar
           F
No espelho dos olhos
          C  F
Tá escrito não dá

F       C           Dm
Só o silêncio ficou entre nós
       Am           Bb
A indiferença calou nossa voz
     F               C  F
O nosso amor foi ficando esquecido
           C                 Dm
Só a tristeza sorriu pra nós dois
       Am           Bb
A felicidade ficou pra depois
        F           C
Que pena que tudo foi tempo perdido

F      C   Dm
Quando o amor dançar
                 Am    Bb
Não vai dar mais pra consertar
         F        C
O que era um sonho virou solidão
          Bb          Bbm   F
Não foi bem assim que eu sonhei te amar

Foi tempo perdido, foi tempo perdido.

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bbm = X 1 3 3 2 1
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
Gm = 3 5 5 3 3 3
