Amado Batista - Começando a Chorar

Intro:(G C G C D)

D                  A
eu estou de novo aqui
Bm             F#m
a brigar com solidão
G                   D
mais ela não quer sair
G                    A
é mais forte essa paixão.

D                   A
minha vida é uma rotina
Bm               F#m
de sofrer e te esperar
G                  D
porque fez isso comigo
Bm       A           D
se eu quero é só te amar

G
não dá pra esperar
D
olhe em meu olhar
A                              D
não vê que estou começando a chorar
G
não judie assim
D
traz você pra mim
G           A
um amor profundo
G           A
nada nesse mundo
A           D
vai nos separar...

----------------- Acordes -----------------
A = X 0 2 2 2 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
