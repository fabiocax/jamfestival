Amado Batista - Meus Braços Te Esperam

(intro) D  A  G D

D            A             G            D  A
Eu  gostaria tanto de te amar mais uma vez
    D          A            G                  D
Senti a felicidade  e esquecer o que você  me fez
                 A    G             D
Meus braços te esperam  vou te esperar
C               G           D
 Sem voê minha vida é tão triste
C                 G          D
 Meu caminho é incerto não existe
                  A    G             D   (D A G D)
Meus braços te esperam   vou te esperar
D                   A                   G
Não quero ficar iludido se já estou perdido
              D   A
cheio de emoções
D                    A                 G                D
Sou um resto de esperança e só você está nas minhas canções
                 A   G                  D
Meus olhos te esperam  pra não mais chorar
C                G           D
 Sem você minha vida é tão triste
C                 G          D
 Meu caminho é incerto não existe
C                 G           D
 Sem você minha vida é tão triste
C                G            D
 Sem você minha vida é tão triste......

----------------- Acordes -----------------
A = X 0 2 2 2 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
