Amado Batista - Não Aceito Desculpas

D
Meu Deus, não me deixe assim
  G                        A              D  A7
Quero essa mulher pra mim, não aceito desculpa
 D
Sinto falta da costela
  G                     A                 D
Que você tirou pra ela, não aceito desculpas.

D
Sei que você tem poder
  G                        A             D  A7
De impedir o meu sofrer, não aceito desculpas
D
Você  que me fez do pó
   G                        A             D  A7
Vê se agora tenha dó - não é minha essa culpa.

       D                   A7
Meu Deus eu só queria ser feliz
        G               A7
Eu não sei se te peço demais
  D                          A7
Devolva essa mulher ao meu jardim
         G          A7        D
E não tira ela de mim nunca mais.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
