Amado Batista - Mascando Chiclete

(intro) G F#m Em D

G                 D7
Aquela menina imagina
     C          D7     C
O futuro que pretende ter
     C                  G
Mas ela não sabe que a vida
               D7
Não é tão colorida
             G
Como parece ser
G                 D7
Aquela menina imagina
     C         D7      G
O futuro que pretende ter
    C                   G
Mas ela não sabe que a vida
               D7
Não é tão colorida
             G
Como parece ser
    D7
Mascando seu chiclete
        G
Sem pensar no amanhã
    D7
Dançando discoteca
        G
A cada dia mais um fã
   C
É filha de papai
      G
Teve sempre o que bem quis
          D7         D     G
E tem certeza que será feliz
    D7
Andando na garupa
      G
Da motoca de alguém
     D7
Distribuindo beijos
       G
Mesmo sem saber a quem
  C
Aceita um cigarro
     G
Um uisque pra esquentar
       D7              G
E acredita que vai abafar
G                 D7
Aquela menina imagina
     C         D7      G
O futuro que pretende ter
    C                   G
Mas ela não sabe que a vida
               D7
Não é tão colorida
             G
Como parece ser
                  D7
Aquela menina imagina
     C        D7       G
O futuro que pretende ter
    C                   G
Mas ela não sabe que a vida
               D7
Não é tão colorida
             G
Como parece ser
     D7
Tem só 14 anos
     G
E já pensa que é maior
     D7
Os livros de orgia
        G
Ela já sabe de cor
     C
Não sabe que futuramente
   G
A consequencia vem
       D7
E ela pensa
          D     G
Que está tudo bem
    D7
Conhece um garotão
         G
Que só quer lhe agradar
   D7
Recebe de presente
       G
O que vai ter que pagar
     C
E assim que ela percebe
         G
Que está errando demais
         D7
É muito tarde
        D      G
Pra voltar atras
                  D7
Aquela menina imagina
     C         D7      G
O futuro que pretende ter
    C                   G
Mas ela não sabe que a vida
               D7
Não é tão colorida
             G
Como parece ser
                  D7
Aquela menina imagina
     C         D7      G
O futuro que pretende ter
    C                   G
Mas ela não sabe que a vida
              D7
Não é tão colorida
             G
Como parece ser

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
