Amado Batista - Papai Noel

(intro) G D7 A7 D

D
Os sinos anunciam os sons divinos
                               A7
O mundo se prepara para as festas

As cidades coloridas com papel
                               D
Enfeitadas pra esperar Papai Noel.


Os corações se abrem pro amor
                    A7
No céu aparece um sinal -

Um raio de luz vem numa estrela
                               D
Anunciando a noite linda de natal.

   D7                 G
Sobre a neve em seu trenó
          A7        D
vai chegar papai Noel
                      G
Com suas renas voadoras
        D             A7
traz os presentes do céu
G
As crianças vêm correndo
                      D
pra com ele se encontrar
                        G
Jesus vai sorrir pro mundo
         A7             D
e essa gente vai cantar:

D                                  A7
Quero que as pessoas vençam o rancor
                                D
A aprendam juntas a cantar o amor -
  G                         D
Se você está distante de Jesus
      A7                  D
Abra o coração pra esta luz.

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
