Amado Batista - Estou Sonhando

(intro) D

E
Fico te olhando, te observando
A                                             E
Você  é a musa que sonhei na vida pra estar comigo.

Eu fico imaginando, eu te namorando
 A                                                E
Minha vida seria a melhor do mundo eu estou sonhando.

F#                B
Será que você um dia
                     A                    E
Vai sentir por mim amor e realizar meu sonho
F#                 B
Nesse dia o mundo vai
                 A                     E
Olhar pra mim feliz, pois estou te amando

E
Fico me lembrando da gente conversando,
 A                                                           E
Mas  nunca imaginei que aquela gata linda, estivesse me olhando

Mas fiquei sonhando ao te ver me olhando
    A                                             E
E  depois do seu comentário lindo eu fiquei sonhando

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
D = X X 0 2 3 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
