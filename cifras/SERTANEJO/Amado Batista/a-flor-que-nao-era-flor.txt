Amado Batista - A Flor Que Não Era Flor

C                                           G
Corri pra apanhar uma flor Que não era uma flor
F                                     C    G
E dei tudo por um amor que não era o meu
   C                                G
Talvez eu nem tenha mais o que procurar
    F      G         C    G
Talvez, talvez, não sei
C                                       G
Tentei provar a mim mesmo que eu era feliz
F                                     C    G
Fechei os olhos tentando não ver a ilusão
  C                                G
E lá no fundo do poço quando despertei
   F    G      C     G
Já era noite então
C                                           G
Corri pra apanhar uma flor que não era uma flor
F                                     C    G
E dei tudo por um amor que não era o meu
   C                                G
Talvez eu nem tenha mais o que procurar
    F      G         C    G
Talvez, talvez, não sei
C                                       G
Tentei provar a mim mesmo que eu era feliz
F                                     C    G
Fechei os olhos tentando não ver a ilusão
  C                                G
E lá no fundo do poço quando despertei
   F    G      C     G
Já era noite então

----------------- Acordes -----------------
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
