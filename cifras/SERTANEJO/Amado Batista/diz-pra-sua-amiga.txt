Amado Batista - Diz Pra Sua Amiga

Intro: A  E  B7  A  E  B7  E

E                         A     E
Diz pra sua amiga que falou comigo
                              A       E
Que meu estado é grave, que corro perigo
    E7          A
Diz pra sua amiga

                                E
Diz pra sua amiga que me viu assim
                                 B7
Só pensando nela, sem pensar em mim
                                E   E7
Diz pra sua amiga que eu estou no fim
A                              E
Diz pra sua amiga não agir assim
                                 B7
E conta pra ela tim-tim por tim tim
                                E  E7
Pede a sua amiga pra voltar pra mim

A                                 E
Diz pra sua amiga vir me dar carinho
                                      B7
Que eu estou carente, estou aqui sozinho
                           E  E7
Pede a sua amiga pra vir me ver
A                                      E
Diz pra sua amiga que não está brincando
                                 B7
Que eu estou doente eu estou piorando
                                   E
Diz pra sua amiga que eu posso morrer

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
