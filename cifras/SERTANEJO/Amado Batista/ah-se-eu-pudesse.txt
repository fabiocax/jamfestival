Amado Batista - Ah! Se Eu Pudesse

Intro: G  C  G  G  C  F  D  F  C  G  D7

          G
Não durmo mais de tanto pensar nela
                            A7
Já não consigo mais viver feliz
        C                  G
Estou doente perdendo a coragem
                          D7
A minha vida está por um triz
         G
Posso morrer de um momento pro outro
                       A7
E a saudade acabando comigo
         C                      G
Vou caminhando entre a vida e morte
                            D7
E ela não sabe que corro perigo

G         C         G
Ah! se eu pudesse fazer
G         C          F
Voltar os tempos de outrora
D7                 F
Pra cantar e divertir
C                   G
E não sofrer como agora (2x)

D7        G
    Felicidade é coisa passageira
                                A7
Que todos sentem por alguns momentos
            C                  G
Fica com a gente depois vai embora
                            D7
Deixando só marcas de sofrimento

         G
E as lembranças das horas felizes
                          A7
Me acompanham por onde estiver
         C                 G
Ficou gravada no meu pensamento
                    D7
A imagem daquela mulher

G         C         G
Ah! se eu pudesse fazer
G         C          F
Voltar os tempos de outrora
D7                 F
Pra cantar e divertir
C                   G
E não sofrer como agora (4x)

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
