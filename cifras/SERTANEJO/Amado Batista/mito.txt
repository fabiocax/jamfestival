Amado Batista - Mito

(intro) D7 G C D7 G D7

G                    D7
Não, eu não quero saber
             C                   G  D7
Como anda você, como vai a sua vida
G                         D7
Eu sei tudo que vão me dizer
                  C
E que você está bem,
          D7               G
Muito feliz com outro alguém

D7
Quando alguém me fala seu nome
                     G
Eu mudo logo de assunto
                                   D7
Digo que você foi um simples momento

Que tudo acabou é um passado
                 G
Que não volta mais.

D7
E se dia um alguém
                         G
Te disser que eu te esqueci
                                D7
Não me procure, não se deixe levar
                                        G
Quando o amor caba não se tem outro jeito

D7
Me dediquei a você, te amei pra valer
                                 G
Não pode negar fomos muito felizes
       D7
Cheguei até mesmo a pensar
                             G
Que este amor nunca tivesse fim.
D7
Você é o mito da minha história
        G
É o sonho impossível, a esperança perdida
      D7
O que eu quero agora é esquecer de você
                   G
E pensar mais em mim.

----------------- Acordes -----------------
C = X 3 2 0 1 0
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
