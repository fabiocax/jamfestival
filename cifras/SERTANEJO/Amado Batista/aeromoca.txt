Amado Batista - Aeromoça

E                                A 
Na Segunda-feira o telefone toca
                            E       B
É meu amor me ligando de algum país 
                          C#m
Pode estar em Londres ou em Nova York
   F#7                 B7        E
Em Buenos Aires ou talvez Paris

                                A
Eu atendo e ela fala que me ama
                       E         B
E longe de mim ela não é feliz
                             C#m
Que seu avião está pousando em Roma
                 F#7                  B7
Que eu sou seu homem que ela sempre quis

A                                 E
Terça-feira eu fico louco de saudade
A                                  E
E na Quinta-feira eu já estou no fim
C#m                     G#m
De repente ela liga do Japão
A           B7                  E
Que seu avião está indo pra Pequim

E                C#m      A        B7
Com ela meu coração viaja mundo afora
    F#7       B7                  E
Por uma aeromoça fui me apaixonar
                  C#m   F#m           B7
E ela outra vez ligou, falou comigo agora
F#m             G#7       C#m
Que hoje me espera pro jantar

A                           E
Meu amor chegou está no Brasil
              B7                 E
Cheia de paixão louca pra me amar

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#7 = 2 4 2 3 2 2
F#m = 2 4 4 2 2 2
G#7 = 4 6 4 5 4 4
G#m = 4 6 6 4 4 4
