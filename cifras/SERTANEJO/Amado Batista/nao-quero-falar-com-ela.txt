Amado Batista - Não Quero Falar Com Ela

C
Intro:  F   C   G7   C   53   40   42   F   C   G7   C   G7

   C
   Se ela ligar outra vez
                      G7
   Diga que eu não estou
   Invente uma história qualquer
                            C
   Diz que arranjei outro amor
   Se ela ligar outra vez
    C7                 F
   Pode dizer que eu saí
                             C
   Mas se ela insistir e voltar a ligar
   G7                      C  53  40  42  F
     Pode dizer que eu morri
                             C
   Mas se ela insistir e voltar a ligar
   G7                      C
     Pode dizer que eu morri
                       G7
   Não quero falar com ela
                              C
   Entre eu e ela tá tudo acabado
                     G7
   O nosso sonho passou
   E aquela história de amor
                C
   Ficou no passado
                       G7
   Não quero falar com ela
                                 C
   Esse amor por ela tá me machucando
   F                 C
    Não deixe ela saber
                        G7
   Que minha vida é sofrer
                      C     53  40  42  F
   E que eu estou chorando
                    C
   Não deixe ela saber
                        G7
   Que minha vida é sofrer
                      C
   E que eu estou chorando.

----------------- Acordes -----------------
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
G7 = 3 5 3 4 3 3
