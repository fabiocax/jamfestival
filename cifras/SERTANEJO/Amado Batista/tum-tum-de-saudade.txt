Amado Batista - Tum Tum de Saudade

Intro: C C7 F G C F G C G

C
Às vezes a minha cabeça 
F
Parece faltar parafuso
G                      C
Estão me chamando de louco é você 
G                      C     G
Que ta me deixando confuso

C
Quem vê meu olhar tão distante 
F
Já sabe da minha paixão
G                           C
Conhece o meu pensamento e até 
G               C            G
Escuta que meu coração

C                   F
Bate tum tum tum de saudade 
G               C
Doido querendo você
                F
Vem trazer esse amor
               G                 C
Arrancar minha dor eu sou ave ferida
C                   F
Bate tum tum tum de saudade
G                  C
Volta correndo pra mim
                   F
Vem dizer que sou teu
                      G
Traz de volta o que é meu
              C            G
Você é minha vida
(INTRO) (REPETE A MUSICA TODA)

----------------- Acordes -----------------
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
