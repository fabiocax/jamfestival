Amado Batista - Amigo Forte

E
Como é linda a vida quando é vivida
                     A               E
Como é triste o passado quando lembrado

Lembrar dos amigos que tanto tempo faz
                          A             E  B7
Lembrar dos lugares antigos que o pensamento traz.

                              A
Lembrar o cachorro chamado Norte
                                 E   B7
É lembrar com saudade um amigo forte.
                 E
Lembrar dos amigos é lembrar do amigo Ló
                 A                   E
E de tanta saudade a garganta dá um nó

E
Que saudade do grupo escolar,

nos feriados recital de poesia
                            A
Do amor inocente poder lembrar
                   E    B7
eram tantas alegrias.

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
