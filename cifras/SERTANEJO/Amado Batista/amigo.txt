Amado Batista - Amigo

  G
Amigo fique um pouco aqui comigo

Estou muito magoado estou perdido
                           D
Preciso de você pra conversar
  Am                        D
Amigo quem eu mais amei na vida
 Am                        D
Me abriu no coração uma ferida
  C                       G     G7
Que nem o tempo vai cicatrizar
C
Hoje descobri suas mentiras
G
Que o tempo todo fingiu me enganou
D
Meu coração traído está chorando
 C  D     G       E
Lágrimas de dor

  A
Amigo eu também estou assim

Quem me jurou um dia amor sem fim
                      E
Também mentiu me fez muita maldade
 Bm                     E
Na sua vida também fui mais um
 Bm                         E
Nós temos muitas coisas em comum
                A               A7
Eu e você deixados na saudade

  D
Amigo vamos sair por ai
 A
Nos divertir e curtir coisas boas
 E
Devemos é mesmo comemorar
  A                            A7
Pra quê sofrer por mulheres à toa
  D
Amigo a tempestade vai passar
A
O mal tempo vai embora e o bom vem
                               E
Você vai encontrar um grande amor
   D                        A
E você merece ser feliz também

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
E = 0 2 2 1 0 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
