Amado Batista - Paixão Violenta

(intro) G C   F  D

G          C           G
Ando pela vida e pela sorte
         F          C
Estou aqui, meu bem

 C       F        C
Eu já andei todo canto
      F               C
À procura de outro encanto
   F       D     G
Porém não encontrei

  G
Trago
    C                     G
Os mesmos defeitos de outrora
   C               G
A mesma paixão violenta
        F              D
Que, enciumada, vai à forra

 A
Tenho
   D                 A
A mesma escolha de amigos
      D               A
Continuo vidrado nas pingas

     G                 E
Dos bares de ponta de rua

   B
Eu amo
    E                   B
Cada vez mais modas caipiras
        E                      B
E vou pregar-lhe as mesmas mentiras
       A                    Gb
Se eu dormir com uma amiga sua

      Gb                 B
É que isso faz parte da vida
                Gb
De um bicho normal
       Gb                  B
De um cara que tem seus defeitos
           Ab
E suas virtudes

  Ab              Cm
Aceite minhas atitudes
    Ab                Cm
Confesso, eu adoro você
       Db             Ab
Sem você estou numa pior
    Db                                Eb
E ainda por cima quem ama e curte desgosto
            Ab
Não anda legal

   Ab                     Cm
Então, meu bem, não me rejeite
     Ab                    Cm
Me aceite do jeito que eu sou
       Db                 Ab
Vamos juntos entrar numa boa

     Eb                  Ab     E7
Superando as crises do amor

   A        D           A
Andei pela vida e pela sorte
   G     D       A
Estou aqui, meu bem.

----------------- Acordes -----------------
A = X 0 2 2 2 0
Ab = 4 3 1 1 1 4
B = X 2 4 4 4 2
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D = X X 0 2 3 2
Db = X 4 6 6 6 4
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
Eb = X 6 5 3 4 3
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
Gb = 2 4 4 3 2 2
