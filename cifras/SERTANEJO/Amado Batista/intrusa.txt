Amado Batista - Intrusa

E
Você...                                                    B

Entrou na minha vida sem saber por onde

Mexeu nos meus segredos
                E
Eu nem sei a fonte

Fofocas e fofocas
                       B
Me envolvem em duas vidas

Tentando colocar em jogo um romance
                E
Que esteve falido

(REFRÃO)
E           E7
Você...                          
    A
Não sabe o que está falando
  E
Você...
                             B
Se soubesse que eu te adoro tanto
         A
Não estaria aí dizendo coisas que não deve  
                B
De seu melhor amigo
E      E7
Você...
                           A
Que eu tive a coragem de dizer baixinho
         E
Só pra você
                          B
Os segredos de uma vida a dois
     A   B
Justamente você foi servir de intrusa
                     E
Numa segunda história

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
