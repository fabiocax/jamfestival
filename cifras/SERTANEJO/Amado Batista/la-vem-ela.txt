Amado Batista - Lá Vem Ela

(intro) C F G7 C

C                           G7
Que beleza de mulher,lá vem ela
               F        G7           C
Sozinha a passar, quem será o dono dela
                                    G7
Eu queria ter a sorte que seu homem tem
               F                G7                C
De todo esse corpo um dia eu queria ser dono também

C                          G7
Todo ela passa na rua de casa
              F    G7                   C
De cabeça baixa finge que não quer me ver
C                 C7               F
Eu como sou atrevido, dou uma olhada
                   G7                C
Sei que isso é errado, mas o que fazer?

C                                  G7
Quem será o felizardo que dorme com ela
      F                             C C7
Será que ele dá amor como eu quero dar
  F                               C
Se ela soubesse como eu tão carinhoso
                                    G7
Deixava eu provar do seu corpo gostoso
                               C
E com certeza nunca ia me deixar.

----------------- Acordes -----------------
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
G7 = 3 5 3 4 3 3
