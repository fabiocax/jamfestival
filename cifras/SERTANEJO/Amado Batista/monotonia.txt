Amado Batista - Monotonia

Capo Casa 7

Intro: G  D  C

G                             D
Onde está aquele amor, que nasceu em nós
                    C
O que aconteceu com ele
            G
Morreu de repente
              G7                  C
Nossos beijos já não tem, o mesmo sabor
                     G
E o nosso amor acontece
        D                      G
Como um caso qualquer, tão de repente
G                                     D
Não sentimos mais prazer, e fingimos sentir
                    C
Quando o coração não quer
       G
Não adianta,
             G7                          C
Nossos pensamentos voam, Buscam outras emoções
                    G
O tempo passa e nós dois
         D                       G          G7
Mergulhados nas mentiras ilhados de solidão

Refrão 2x:
     C
É o fim,
                 G
Desta nossa fantasia
                  D
Quero ter um novo dia
 D7                  G              G7
Sentir no peito o verdadeiro amor
       C
Se em nós,
           G
Tudo é monotonia
                  D
Vamos tentar nos achar
D7     D           G
Em outros corações

----------------- Acordes -----------------
Capotraste na 7ª casa
C*  = X 3 2 0 1 0 - (*G na forma de C)
D*  = X X 0 2 3 2 - (*A na forma de D)
D7*  = X X 0 2 1 2 - (*A7 na forma de D7)
G*  = 3 2 0 0 0 3 - (*D na forma de G)
G7*  = 3 5 3 4 3 3 - (*D7 na forma de G7)
