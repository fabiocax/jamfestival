Amado Batista - Amor a Primeira Vista

(intro) B E F# B E F#

B
Eu não sabia de onde você vinha
              B7  E
E nem quem você era
                        F#7
Só sei que era muito bonita
                          B
Como as flores da primavera.

Tinha nos olhos um grande brilho
                               B7  E
E no falar um jeito meigo com pureza
                           F#7
E nos cabelos uma grande mecha
                       E          B
Que enfeitava ainda mais sua beleza.

          F#7              B
E o meu amor nasceu tão forte
            F#7          B
Como um romance de revista
                  F#7
Era mesmo uma paixão
        E       F#7      B
Esse amor a primeira vista.

B
E a esta altura eu já sabia seu nome
                               B7  E
Quem você era e de onde você vinha
                              F#7
Era a receita que o medico passou
                            B
Pra aliviar a minha dor doida.

Você é tudo isso e muito mais
                  B7  E
É a  razão da minha vida
                       F#7
E essa água não matou sede
                   E          B
Porque guardei esse amor comigo.

        F#7          B
Não fui tão forte ainda
        F#7         B
Pra revelar esse amor
                       F#7
Tenho medo de ouvir um não
        E     F#7        B
E ter que suportar essa dor.

----------------- Acordes -----------------
B = X 2 4 4 4 2
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
F#7 = 2 4 2 3 2 2
