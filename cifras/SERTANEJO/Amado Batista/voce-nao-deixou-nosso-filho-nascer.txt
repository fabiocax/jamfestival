Amado Batista - Você Não Deixou Nosso Filho Nascer

(intro) D

G            A           G      D
Você não deixou nosso filho nascer
  G              A       G      D
Bebeu tanta coisas pra ele morrer
D
Não posso entender a sua crueldade
  G               A       G      D
Destruiu nosso mundo de felicidade.

Para conservar esse corpo seu
      G        A        G      D
Não quis ser mãe de um filho meu

      D7              G
Um choro dentro da noite
                         D
As vezes atrapalha a gente
                 A7
Levantaria com sono
                   D
Mas ficaria contente
      D7            G
O que será de nós dois
                        D
Não vamos ter sossego mais
                 A7
Você tirou o direito
   G          A7      D
Que eu tinha de ser pai.

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
G = 3 2 0 0 0 3
