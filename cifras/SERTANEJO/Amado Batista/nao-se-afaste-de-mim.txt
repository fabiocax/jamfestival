Amado Batista - Não Se Afaste de Mim

(intro) C Bb F C7 Bb F C7

C
Não se afaste de mim
         F                       C7
Meu coração por você está apaixonado

Ele vai te seguir
           F                                  C7
Vai implorar pra voltar, fique aqui do meu lado.

Bb       F       C7
Não se afaste de mim
Bb       F       C7
Não se afaste de mim..!

C7
Você é diferente
              F               C7
É o tipo que agita o meu coração

Seu amor é tão quente
           F                         C7
Me faz vibrar, suspirar e morrer de paixão.

G7                 F               C
Não se afaste de mim já é tarde demais
G7              F                     C
Você já é meu amor minha luz, minha paz.
               G7          F                  C
Me deixe fazer dos teus braços meu eterno abrigo
             G7            F                     C
Me deixe beijar os teus lábios quero dormir contigo

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
G7 = 3 5 3 4 3 3
