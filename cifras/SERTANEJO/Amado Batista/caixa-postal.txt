Amado Batista - Caixa Postal

Intro: E  A  B7  A  B7  E  B7

E
Toda hora na volta do dia
Eu penso nela
B7
E na hora de dormir, então
                  E    E7
Aí que eu penso nela

     A
E rolando de um lado pro outro
      B7
Só pensando porque ela não liga
      A
Já deixei dois recados na sua
         B7
Caixa postal

     A
E eu sei que esse amor nem começou
      B7
Mas rolaram olhares românticos
                E   E7
E palavras de amor

  A
Talvez fosse amor de mais pra nós dois
   B7
Exageros em nossas paixões
      A
Ou só eu que te amei por nós dois
         B7
Estou sofrendo de mais sem razões

     A        B7
Ou será, ou será
                 E
Que ela não me amou

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
