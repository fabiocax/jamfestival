Amado Batista - Musa Inspiradora

F                                             C7
Foi pensando em nosso amor que eu fiz esta canção
                            F                    C7
Foi olhando em teu olhar que me veio a inspiração -
F                                 F7        Bb
Em você eu vejo a paz que eu preciso pra viver
                 F              C7                  F
E nem um mortal vai ter o poder de te afastar de mim.

F                                     C7
Ninguém pode definir o que é um coração
                             F               C7
Nem tampouco permitir pois ele sempre tem razão
F                          F7       Bb
Se o tempo interferir e fizer separação

Algo vai ficar,
 F               C7                   F
Pra nos machucar nos versos desta canção.

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
F7 = 1 3 1 2 1 1
