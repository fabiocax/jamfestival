Simone e Simaria - Pássaro Noturno

Intro 2x: C#m  G#m  D#m  F#

Primeira parte:

      C#m         G#m      D#m                 F#
Procurei e encontrei, meu amor, minha cara metade
          C#m          G#m         D#m               F#
 Me entreguei, te mostrei quem eu sou, a minha verdade
C#m                G#m                D#m
    Sou pássaro noturno, quer ser meu par?
               F#
 Procuro entender
C#m                      G#m            D#m
    Eu sem você, já não durmo, nem sei voar
            F#
 Sonhar, viver

Refrão 2x:

C#m
Todo dia eu quero, todo dia espero

G#m                              D#m
Todo dia eu chamo, todo dia eu amo
      F#
Só você
 Yeah yeah
C#m
E vai valer a pena, mudar essa cena
G#m                               D#m
Viver meu roteiro, amo o tempo inteiro
      F#
Só você

( C#m  G#m  D#m  F# )

Primeira parte:

      C#m         G#m      D#m                 F#
Procurei e encontrei, meu amor, minha cara metade
          C#m          G#m         D#m              F#
 Me entreguei, te mostrei quem eu sou, a minha verdade
C#m                G#m                D#m
    Sou pássaro noturno, quer ser meu par?
               F#
 Procuro entender
C#m                      G#m            D#m
    Eu sem você, já não durmo, nem sei voar
            F#
 Sonhar, viver

Refrão 2x:

C#m
Todo dia eu quero, todo dia espero
G#m                              D#m
Todo dia eu chamo, todo dia eu amo
      F#
Só você
 Yeah yeah
C#m
E vai valer a pena, mudar essa cena
G#m                                D#m
Viver meu roteiro, amo o tempo inteiro
      F#
Só você

( C#m  G#m  D#m  F# )

                  C#m
Sou pássaro noturno

----------------- Acordes -----------------
C#m = X 4 6 6 5 4
D#m = X X 1 3 4 2
F# = 2 4 4 3 2 2
G#m = 4 6 6 4 4 4
