Simone e Simaria - Loka (part. Anitta)

Intro 2x: Am  F  C  G

Primeira Parte:
        Am
Cadê você, que ninguém viu?
         F
Desapareceu, do nada sumiu
        C
Tá por aí tentando esquecer
          G
O cara safado que te fez sofrer

        Am
Cadê você? Onde se escondeu?
              F
Porque sofre se ele não te mereceu
              C
Insiste em ficar em cima desse muro
            G
Espera a mudança em quem não tem futuro


Segunda Parte:
                    Am
Deixa esse cara de lado
                               F
Você apenas escolheu o cara errado
                                       C
Sofre no presente por causa do seu passado
                            G
Do que adianta chorar pelo leite derramado?

Pré-Refrão:
 Dm                     G
Põe aquela roupa e o batom
    Dm            F               G
Entra no carro, amiga, aumenta o som

Refrão 2x:
                 Am
E bota uma moda boa
   F                        C
Vamos curtir a noite de patroa
G                           Am
Azarar uns boys, beijar na boca
   F                       C  G
Aproveitar a noite, ficar louca

Pós-Refrão 2x:
                    Am     F      C
Esquece ele e fica louca, louca, louca
  G                        Am   F      C  G
Agora chora no colo da patroa, louca, louca

( Am  F  C  G ) (2x)

Primeira Parte:
Am
Cadê você, que ninguém viu?
         F
Desapareceu, do nada sumiu
        C
Tá por aí tentando esquecer
          G
O cara safado que te fez sofrer

        Am
Cadê você? Onde se escondeu?
              F
Porque sofre se ele não te mereceu
              C
Insiste em ficar em cima desse muro
            G
Espera a mudança em quem não tem futuro

Segunda Parte:
                    Am
Deixa esse cara de lado
                               F
Você apenas escolheu o cara errado
                                       C
Sofre no presente por causa do seu passado
                            G
Do que adianta chorar pelo leite derramado?

Pré-Refrão:
 Dm                     G
Põe aquela roupa e o batom
    Dm            F               G
Entra no carro, amiga, aumenta o som

Refrão 2x:
                 Am
E bota uma moda boa
   F                        C
Vamos curtir a noite de patroa
G                           Am
Azarar uns boys, beijar na boca
   F                       C  G
Aproveitar a noite, ficar louca

Pós-Refrão 2x:
                    Am     F      C
Esquece ele e fica louca, louca, louca
  G                        Am   F      C  G
Agora chora no colo da patroa, louca, louca

Final 2x: Am  F  C  G

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
