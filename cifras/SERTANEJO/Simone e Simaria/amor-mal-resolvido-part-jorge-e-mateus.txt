﻿Simone e Simaria - Amor Mal Resolvido (part. Jorge e Mateus)

Capo Casa 1

Intro: F#m7  D9  A9  E/G#

E|-------------------------------------------------------|
B|-------------------------------------------------------|
G|-------------------------------------------------------|
D|---6-7-77-7-7---6-7-77-7-9-7--6-7-77-7-9-7-6-----------|
A|-9------------9-------------7----------------9-7-------|
E|-------------------------------------------------------|

Primeira Parte:

    A9
O que foi?
      D9                                    A9
Tá batendo na minha porta essas horas será por quê?
      D9                                                A9
Qual foi a parte que você não entendeu, não quero mais te ver
       E/G#
Queria eu que fosse mesmo assim
        D9
Mas o amor falou mais alto outra vez

     E/G#
E olha a besteira que a gente fez

Pré-Refrão:

Bm7                           F#m7
   O meu lençol tá todo bagunçado
                             Bm7
Você deixou marcas pra todo lado
Depois do amor adormeci
       D9                   E/G#
E só acordei com o barulho do seu carro

Refrão:

       F#m7       D9
Foi embora outra vez
Como da última vez
   A9                 E/G#
Matou sua vontade abusando da sua ex
     F#m7             D9
Que sempre te atende nessas horas de carência
   A9                  E/G#
E depois paga o preço sofrendo as consequências
       F#m7       D9
Foi embora outra vez
Como da última vez
   A9                 E/G#
Matou sua vontade abusando da sua ex
     F#m7             D9
Que sempre te atende nessas horas de carência
   A9                  E/G#
E depois paga o preço sofrendo as consequências
 D9                    E/G#
De um amor mal resolvido

Gostar de você é meu pior castigo

 F#m7 D9 A9 E/G#

E|-------------------------------------------------------|
B|-------------------------------------------------------|
G|-------------------------------------------------------|
D|---6-7-77-7-7---6-7-77-7-9-7--6-7-77-7-9-7-6-----------|
A|-9------------9-------------7----------------9-7-------|
E|-------------------------------------------------------|

 C#m7 A9 E9 B/D#

E|-------------------------------------------------------|
B|-------------------------------------------------------|
G|----8-9-99-9----8-9-99-9-11-9---8-9-99-9-11-9-8--------|
D|-11----------11---------------9-----------------11-9---|
A|-------------------------------------------------------|
E|-------------------------------------------------------|

Primeira Parte:

       E
O que foi?
      A9                                    E
Tá batendo na minha porta essas horas será por quê?
      A9                                                E
Qual foi a parte que você não entendeu, não quero mais te ver
       B/D#
Queria eu que fosse mesmo assim
        A9
Mas o amor falou mais alto outra vez
     B/D#
E olha a besteira que a gente fez

Pré-Refrão:

F#m7                            C#m7
   O meu lençol tá todo bagunçado
                             F#m7
Você deixou marcas pra todo lado
Depois do amor adormeci
       A9                      B/D#
E só acordei com o barulho do seu carro

Refrão:

       C#m7       A9
Foi embora outra vez
Como da última vez
   E                  B/D#
Matou sua vontade abusando da sua ex
     C#m7             A9
Que sempre te atende nessas horas de carência
   E                B/D#
E depois paga o preço sofrendo as consequências
       C#m7       A9
Foi embora outra vez
Como da última vez
   E                  B/D#
Matou sua vontade abusando da sua ex
     C#m7             A9
Que sempre te atende nessas horas de carência
   E                B/D#
E depois paga o preço sofrendo as consequências
       F#m7       D9
Foi embora outra vez
Como da última vez
   A9                 E/G#
Matou sua vontade abusando da sua ex
     F#m7             D9
Que sempre te atende nessas horas de carência
   A9                  E/G#
E depois paga o preço sofrendo as consequências
D9                     E
De um amor mal resolvido

Gostar de você é meu pior castigo

Final: F#m7 D9 A9 E

----------------- Acordes -----------------
Capotraste na 1ª casa
A9*  = X 0 2 2 0 0 - (*A#9 na forma de A9)
B/D#*  = X 6 X 4 7 7 - (*C/E na forma de B/D#)
Bm7*  = X 2 4 2 3 2 - (*Cm7 na forma de Bm7)
C#m7*  = X 4 6 4 5 4 - (*Dm7 na forma de C#m7)
D9*  = X X 0 2 3 0 - (*D#9 na forma de D9)
E*  = 0 2 2 1 0 0 - (*F na forma de E)
E/G#*  = 4 X 2 4 5 X - (*F/A na forma de E/G#)
E9*  = 0 2 4 1 0 0 - (*F9 na forma de E9)
F#m7*  = 2 X 2 2 2 X - (*Gm7 na forma de F#m7)
