Victor e Leo - Noite Estelar

(capo na 7ª casa ou abaixe 3 tons e meio)

(intro) (G) (Em  C D) (3x)  Em D G

G                    Em
Meia noite e meia saudade me desperta
C                          G      D
 Fico vendo estrelas da janela
G              Em
violão no peito Pensamento nela
C                         G
oh paixão sem freio na banguela
D                                     Em
Eu que andava meio sem motivos para sonhar
C                                G
Descobri que não existe hora nem lugar
      Am
pro amor chegar
          D                  G
o céu mais lindo está no seu olhar


C     D      Em
    Noite estelar
             C                     D                 Em
Sonhos que me levam ventos que te trazem para sempre ficar
C     D      Em
    Noite estelar
              C                 D              Em
Um beijo no espaço sorriso um abraço pra sempre ficar
C     D      G
     No seu olhar

 (solo) D D (Em C D) (2x) Em D G
E |-7-8-10-10/11--10-8-7-7--------------------------7-10-12-12/14--15-17-17/19--7----------
B |-8-9-11-11/12--11-9-8-7-7---------------------------------------15-------------10-12-12-
G |------------------------8---------------7-9-9/11----------------------------------------
D |-------------------------------------7-9------------------------------------------------
A |--------------------------------7-9-9---------------------------------------------------
E |-----------------------------10---------------------------------------------------------

G               Em
Meia noite e meia saudade me desperta
C                          G      D
 Fico vendo estrelas da janela
G              Em
violão no peito Pensamento nela
C                         G
oh paixão sem freio na banguela
D                                     Em
Eu que andava meio sem motivos para sonhar
C                                G
Descobri que não existe hora nem lugar
      Am
pro amor chegar
          D                  G
o céu mais lindo está no seu olhar

C     D      Em
    Noite estelar
             C                     D                 Em
Sonhos que me levam ventos que te trazem para sempre ficar
C     D      Em
    Noite estelar
              C                 D              Em
Um beijo no espaço sorriso um abraço pra sempre ficar

C     D      Em
    Noite estelar
             C                     D                 Em
Sonhos que me levam ventos que te trazem para sempre ficar
C     D      Em
    Noite estelar
              C                 D              Em
Um beijo no espaço sorriso um abraço pra sempre ficar

C     D      G
     No seu olhar

----------------- Acordes -----------------
Capotraste na 7ª casa
A*  = X 0 2 2 2 0 - (*E na forma de A)
Am*  = X 0 2 2 1 0 - (*Em na forma de Am)
B*  = X 2 4 4 4 2 - (*F# na forma de B)
C*  = X 3 2 0 1 0 - (*G na forma de C)
D*  = X X 0 2 3 2 - (*A na forma de D)
E*  = 0 2 2 1 0 0 - (*B na forma de E)
Em*  = 0 2 2 0 0 0 - (*Bm na forma de Em)
G*  = 3 2 0 0 0 3 - (*D na forma de G)
