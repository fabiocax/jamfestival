Victor e Leo - Sem Trânsito, Sem Avião

Intro: Am F C9 G Am F C9 G C9

Dedilhado da Intro:
E|------------------------------------------------------------------------|
B|---0-1---------1-----1-1-1-1-1-0-0--------0-1---------1-----1-1-0h1p0-1-|
G|-2----------2------0--------------------2----------2------0-------------|
D|--------3-3------3-----------------------------3-3------3---------------|
A|-0---0----3----------3---3---3---3------0---0----3----------3---------3-|
E|------------------------------------------------------------------------|

E|-----------------------|
B|--------5-5/3-3-1------|
G|-----------------------|
D|----3/5-5-5/3-3-2------|
A|-----------------------|
E|-----------------------|

 Am           F
Saia desse asfalto e vem
     C9               G
pra nossa estrada que é de chão

 Am           F
Tem poeira e barro tem
   C9              G
Cavalo e boi, preste atenção
Dm                 E
Você não vai se arrepender,
      Am           F
Pois, a paisagem, pode crer,
     C9     G    C9
É demais, é de babar...
     ( C )
E|----------------|
B|---1-1----------|
G|-------0--------|
D|---------2------|
A|---3-3----------|
E|----------------|

Refrão 2x:
    Am             F
Descalço, sem se preocupar
    C9                   G
Se solte e venha pro sertão
  Am            F
O céu no chão parece estar
  Fm               C9
Sem trânsito, sem avião

  Am             F
Descalço, sem se preocupar
    C9                   G
Se solte e venha pro sertão
  Am            F
O céu no chão parece estar
  Fm
Sem trânsito, sem avião
Solo: Am  F  C9  G (2x)

E|---------------------------------8----10^-8--------8-------8/12--12-12-13-15-15-10-10-----|
B|----------------------8---10--10---10-------10--10---10----9/13--13-13-15-17-17-12-12-----|
G|-----------5-7-7/9--9---9-----------------------------------------------------------------|
D|------5/7---------------------------------------------------------------------------------|
A|------------------------------------------------------------------------------------------|
E|------------------------------------------------------------------------------------------|

E|--12-12---------------------------15^-----------15^-----------|
B|--12----15-13-12--------13-15~-13-----13-13~-13-----13-15~~---|
G|--12-----------------14---------------------------------------|
D|------------------15------------------------------------------|
A|--------------------------------------------------------------|
E|--------------------------------------------------------------|

Sanfona: Am F C9 G Am F Fm
     ( C )
E|----------------|
B|---1-1----------|
G|-------0--------|
D|---------2------|
A|---3-3----------|
E|----------------|

 Am           F
Saia desse asfalto e vem
     C9               G
pra nossa estrada que é de chão
 Am           F
Tem poeira e barro tem
   C9              G
Cavalo e boi, preste atenção
Dm                 E
Você não vai se arrepender,
      Am           F
Pois, a paisagem, pode crer,
     C9     G    C9
É demais, é de babar...
     ( C )
E|----------------|
B|---1-1----------|
G|-------0--------|
D|---------2------|
A|---3-3----------|
E|----------------|

Refrão 3x:
  Am             F
Descalço, sem se preocupar
    C9                   G
Se solte e venha pro sertão
   Am            F
O céu no chão parece estar
  Fm               C9
sem trânsito, sem avião

   Am            F
O céu no chão parece estar
 Fm               C9
sem trânsito, sem avião

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
B = X 2 4 4 4 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
Dm = X X 0 2 3 1
E = 0 2 2 1 0 0
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
