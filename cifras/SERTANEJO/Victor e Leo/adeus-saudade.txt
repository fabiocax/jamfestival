﻿Victor e Leo - Adeus Saudade

Capo Casa 3

Intro:  F C F C G C

     F                 C
A maior besteira que fiz,
        F                  C
Foi querer ser bem mais feliz,
      G                C
Na cidade do que no campo
       G                 C
Ainda bem que fiz esse canto
         F        G        C
Que, em parte, na dor põe fim
    F          G      C
Ele é mais ou meno assim...

Refrão:
 F      C    F    C   F     C            G
Ê...saudade Ê saudade Ê saudade, sai de mim
F        C    F     C   F     C            G
Ô...saudade Ô saudade Ô saudade, sai de mim


       F                     C
Só na roça é que eu posso ouvir
     F               C
Tiquaçú, melro e juriti,
      G                   C
Só na roça é que eu posso ver
   G                 C
O que me sustenta nascer
        F       G     C
E a saudade, matando vai
       F      G            C
Canto que é pra ver se ela sai

Refrão:
 F      C    F     C   F     C            G
Ê...saudade Ê saudade Ê saudade, sai de mim
F        C    F     C   F     C            G
Ô...saudade Ô saudade Ô saudade, sai de mim

   F                 C
O gemer do carro de boi
        F              C
E a peonada tocando os boi
        G              C
São lembrança do que vivi
        G              C
Lá nas terra de onde saí
         F    G        C
Quando a lua no horizonte
        F        G       C
Dava o berço pro sol dormi

Refrão:
     F                  C
Não passou de grande ilusão
       F               C
Viver longe do meu sertão
       G            C
Viver longe do coração
        G              C
Das nascentes daquele chão
       F       G       C
Tô vortando pois resolvi
       F      G       C
Da saudade me despedir

 F      C    F     C   F     C            G
Ê...saudade Ê saudade Ê saudade, sai de mim
F        C    F     C   F     C      G         C
Ô...saudade Ô saudade Ô saudade, Adeeeeeeeeeeeeeeeeeuss

----------------- Acordes -----------------
Capotraste na 3ª casa
C*  = X 3 2 0 1 0 - (*D# na forma de C)
F*  = 1 3 3 2 1 1 - (*G# na forma de F)
G*  = 3 2 0 0 0 3 - (*A# na forma de G)
