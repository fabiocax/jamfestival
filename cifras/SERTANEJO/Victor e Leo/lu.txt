﻿Victor e Leo - Lu

Capo Casa 2

Intro: Am  F  C  G (Abafado)
          Am       F               G    Am
E|------7--5-7---7-5--7---10-----------------|
B|---------------------------8-8h10---5h7----|
G|-------------------------------------------|
D|-------------------------------------------|
A|-------------------------------------------|
E|-------------------------------------------|

          Am       F               G       F
E|------7--5-7---7-5--7---10-----------------|
B|---------------------------8-8h10----------|
G|-------------------------------------------|
D|-------------------------------------------|
A|-------------------------------------------|
E|-------------------------------------------|


Am               F
Esse som lembra coisas
C                   G
Das quais procurei fugir

Am                   F
Foi tão bom, que eu queria
C                   G
Que o tempo voltasse aqui
Gm                            Bb
Naquela agenda simples que eu te dei
F            C
Havia um poema
Dm                         Bb
Nem sempre as coisas que a gente vê
F                                E
São mais reais que o que nos faz crescer

Refrão:
Am        F
Lu, diz onde
C                 G
Você se escondeu de mim
Am
Oh Lu
F
Responde
C                            G
Não é bom que as coisas fiquem assim

Am        F
Lu, diz onde
C                 G
Você se escondeu de mim
Am
Oh Lu
F
Responde
G                       F          Am
Não é bom que as coisas fiquem assim

Solo: Am  G  F  G  Am  F  G  F

Am               F
Esse som lembra coisas
C                   G
Das quais procurei fugir
Am                   F
Foi tão bom, que eu queria
C                   G
Que o tempo voltasse aqui
Gm                            Bb
Naquela agenda simples que eu te dei
F            C
Havia um poema
Dm                         Bb
Nem sempre as coisas que a gente vê
F                                E
São mais reais que o que nos faz crescer

Refrão:
Am        F
Lu, diz onde
C                 G
Você se escondeu de mim
Am
Oh Lu
F
Responde
C                            G
Não é bom que as coisas fiquem assim

Am        F
Lu, diz onde
C                 G
Você se escondeu de mim
Am
Oh Lu
F
Responde
G                       F
Não é bom que as coisas fiquem...

Am        F
Lu, diz onde
C                 G
Você se escondeu de mim
Am
Oh Lu
F
Responde
C                            G
Não é bom que as coisas fiquem assim

Am        F
Lu, diz onde
C                 G
Você se escondeu de mim
Am
Oh Lu
F
Responde
G                       F             Am
Não é bom que as coisas fiquem fiquem assim

Final: Am

----------------- Acordes -----------------
Capotraste na 2ª casa
Am*  = X 0 2 2 1 0 - (*Bm na forma de Am)
Bb*  = X 1 3 3 3 1 - (*C na forma de Bb)
C*  = X 3 2 0 1 0 - (*D na forma de C)
Dm*  = X X 0 2 3 1 - (*Em na forma de Dm)
E*  = 0 2 2 1 0 0 - (*F# na forma de E)
F*  = 1 3 3 2 1 1 - (*G na forma de F)
G*  = 3 2 0 0 0 3 - (*A na forma de G)
Gm*  = 3 5 5 3 3 3 - (*Am na forma de Gm)
