Victor e Leo - Ritmo da Chuva

(solo 2x) G  C  G  D7

 G                           C
Olho para a chuva que não quer cessar
 G                D7
Nela vejo o meu amor
G                           C
Esta chuva ingrata que não vai parar
       G        D7    G   D7
Pra' aliviar a minha dor
    G                             C
Eu sei que o meu amor pra' muito longe foi
 G                D7
Com a chuva que caiu
    G                        C
Oh gente por favor pra' ela vá contar
      G          D7     G   G7
Que o meu coração se partiu

(refrão)
 C                  Bm
Chuva traga o meu benzinho

 Am                G
Pois preciso de carinho
 Em                 C      D7              G  D7
Diga a ela pra' não me deixar    triste assim

  G                    C
O ritmo dos pingos ao cair no chão
 G                D7
Só me deixa relembrar
   G                           C
Tomara que eu não fique a esperar em vão
    G           D7     G   G7
Por ela que me faz chorar

(intro)

G                  C
O ritmo dos pingos ao cair no chão
G                D7
Só me deixa relembrar
   G                           C
Tomara que eu não fique a esperar em vão
   G           D7     G   G7
Por ela que me faz chorar

(refrão)
C            Bm
Chuva traga o meu benzinho
Am                G
Pois preciso de carinho
 Em                 C      D7              G  D7
Diga a ela pra' não me deixar    triste assim

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
