﻿Victor e Leo - O Beijo Que Eu Mais Quis

Capo Casa 2

[Intro]  F  C  F  C

F
Às vezes penso que você
                              C
É só ilusão pra entreter meu mundo
F
Mas meio segundo em seu olhar
                               C
E nada me conseguiu tocar tão fundo

F
Às vezes penso que a razão
                                  C
É mera desculpa do coração pra se enganar
F
Meus olhos se fecham pra sentir
                                   C
O toque do beijo que eu mais quis beijar

         Am           Em
Fingimos tanto que não era sol

F                        C
Até que amanheceu e tudo se mostrou
             Am
Flor que se abriu
          Em
Pros meus dias bons
F
Me deixe ser seu beija-flor
                            G
Eu quero te chamar de meu amor

[Solo]  F  C  F  C

         Am           Em
Fingimos tanto que não era sol
F                        C
Até que amanheceu e tudo se mostrou
             Am
Flor que se abriu
          Em
Pros meus dias bons
F
Me deixe ser seu beija-flor
                            G
Eu quero te chamar de meu amor

F
Às vezes penso que você
                              C
É só ilusão pra entreter meu mundo
F
Mas meio segundo em seu olhar
                               C
E nada me conseguiu tocar tão fundo

F
Às vezes penso que a razão
                                  C
É mera desculpa do coração pra se enganar
F
Meus olhos se fecham pra sentir
                                   C
O toque do beijo que eu mais quis beijar

F
Meus olhos se fecham pra sentir
                                   C
O toque do beijo que eu mais quis beijar

----------------- Acordes -----------------
Capotraste na 2ª casa
Am*  = X 0 2 2 1 0 - (*Bm na forma de Am)
C*  = X 3 2 0 1 0 - (*D na forma de C)
Em*  = 0 2 2 0 0 0 - (*F#m na forma de Em)
F*  = 1 3 3 2 1 1 - (*G na forma de F)
G*  = 3 2 0 0 0 3 - (*A na forma de G)
