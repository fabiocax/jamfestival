﻿Victor e Leo - Mari Mariana

Capo Casa 7

Intro 2x: C  F

C
Vou te esperar naquela porteira
                       F
Na beira da beira da noite

Dm
Vou te chamar
               G
Vou pedir pra vê-la
                                    C
Na primeira estrela quando ela brilhar

Am               Dm
Fica combinado assim:
                   G
Te dou a lua do sertão
                    Am
Você me dá você pra mim


Dm
Mari Mari Mari Mariana
Am
O amor se faz
Dm
Minha viola não se engana
Am          C
Se faz em nós

                 Dm
Te espero na porteira
G                     C
Na beira da beira da noite

Solo 2x: C  F

Am               Dm
Fica combinado assim:
                   G
Te dou a lua do sertão
                    Am
Você me dá você pra mim

Dm
Mari Mari Mari Mariana
Am
O amor se faz
Dm
Minha viola não se engana
Am          C
Se faz em nós

                 Dm
Te espero na porteira
G                     C
Na beira da beira da noite

Dm
Mari Mari Mari Mariana
Am
O amor se faz
Dm
Minha viola não se engana
Am          C
Se faz em nós

                 Dm
Te espero na porteira
G                     C
Na beira da beira da noite

                 Dm
Te espero na porteira
G                     C
Na beira da beira da noite

----------------- Acordes -----------------
Capotraste na 7ª casa
Am*  = X 0 2 2 1 0 - (*Em na forma de Am)
C*  = X 3 2 0 1 0 - (*G na forma de C)
Dm*  = X X 0 2 3 1 - (*Am na forma de Dm)
F*  = 1 3 3 2 1 1 - (*C na forma de F)
G*  = 3 2 0 0 0 3 - (*D na forma de G)
