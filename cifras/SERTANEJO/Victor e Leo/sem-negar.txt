﻿Victor e Leo - Sem Negar

Capo Casa 7

Intro 2x: Dm  F  C

Dm                    F
Olho da janela e você vem
                  C
Trazendo o meu café

O sol vai nascendo,
                    Dm
Vou subir a serra a pé
                 F
Vou olhar lá de cima,
                 C
Você no meu quintal
                                   Dm   F         C
Colocando as roupas pra secar no varal,  no meu varal

Dm                           F          C
Hoje eu falei pra serra e o sol, e o sereno
             Dm          F         C
Você é minha vida, meu amor, meu alento


Dm     F                     C
Sem negar, eu digo que eu te amo
Dm      F                           C
Sem pensar, que eu digo que eu te quero

----------------- Acordes -----------------
Capotraste na 7ª casa
C*  = X 3 2 0 1 0 - (*G na forma de C)
Dm*  = X X 0 2 3 1 - (*Am na forma de Dm)
F*  = 1 3 3 2 1 1 - (*C na forma de F)
