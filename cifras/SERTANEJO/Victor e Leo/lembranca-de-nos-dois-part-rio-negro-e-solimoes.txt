Victor e Leo - Lembrança de Nós Dois (part. Rio Negro e Solimões)

[Intro] G  C  G  C  G  C

      G                                  C
Pela noite, mil estrelas a brilhar
         G                                            D
O meu pensamento se põe a vagar
              Em                                   C
Junto ao vento frio e forte da invernada
        Am                                          D
Que balança o pasto inteiro da baixada
       C                                     G
Esse vento, que me traz tanta saudade
          C                                    G
De um passado cheio de felicidade
     D                                             G        D
É lembrança de nós dois na madrugada

G       C                                        G
É lembrança de nós dois na madrugada
G       C                                        G
É lembrança de nós dois na madrugada

     C                             G
Estrela de paixão enluarada
       D       C                    G   C G C
Nós dois, a sós, na madrugada

            G                                      C
Meu amor, queria tanto te encontrar
                     G                            D
Bem que a gente poderia relembrar
              Em                                  C
Nossos beijos e encontros escondidos
                  Am                            D
Tudo o que até hoje não foi esquecido
           C                                      G
Todos dormem e nós dois ali sonhando
            C                               G
Madrugada, só a lua nos olhando
        D                                              G     D
E ouvindo a voz do nosso amor cantando

G       C                                        G
É lembrança de nós dois na madrugada
G       C                                        G
É lembrança de nós dois na madrugada
     C                             G
Estrela de paixão enluarada
       D       C                    G
Nós dois, a sós, na madrugada

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
