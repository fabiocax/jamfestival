Victor e Leo - Boiadeiro Errante?

[Intro] B  F#7  E  B   B  F#7  B
        B  F#7  E  B   B  F#7  B

B                     F#7       B
Eu venho vindo de uma querência distante
        F#7          B                        F#7
Sou um boiadeiro errante que nasceu naquela serra
                    E               F#7
O meu cavalo corre mais que o pensamento
     E            F#7                     B
Ele vem no passo lento porque ninguém me espera
           F#7                     E        F#7       B
Tocando a boiada auê-uê-uê-ê boi  eu vou cortando estrada uê boi

            F#7                    E         F#7     B
Tocando a boiada auê-uê-uê-ê boi  eu vou cortando estrada

 B                      F#7         B
Toque o berrante com capricho Zé Vicente
     F#7         B                    F#7
Mostre para essa gente o clarim das alterosas

                         E           F#7
Pegue no laço não se entregue companheiro
         E           F#7                        B
Chame o cachorro campeiro que essa rez é perigosa
           F#7                      E     F#7       B
Olhe na janela auê uê uê ê boi  que linda donzela uê boi
           F#7                     E    F#7     B
Olhe na janela auê uê uê ê boi  que linda donzela

                     F#7                B
Sou boiadeiro minha gente o que é que há
         F#7          B                           F#7
Deixe o meu gado passar vou cumprir com a minha sina
                       E        F#7
Lá na baixada quero ouvir a siriema
         E              F#7                        B
Pra lembrar de uma pequena que eu deixei lá em Minas

          F#7                      E       F#7       B
Ela é culpada auê uê uê ê boi  de eu viver nas estradas uê boi
         F#7                      E       F#7       B
Ela é culpada auê uê uê ê boi  de eu viver nas estradas

                    F#7         B
O rio tá calmo e a boiada vai nadando
        F#7          B                       F#7
Veja aquele boi berrando Chico Bento corre lá
                     E          F#7
Lace o mestiço salve ele das piranhas
        E          F#7                    B
Tire o gado da campana pra viagem continuar
               F#7                   E      F#7             B
Com destino a Goiás auê uê uê ê boi  deixei Minas Gerais uê boi
               F#7                   E      F#7             B
Com destino a Goiás auê uê uê ê boi  deixei Minas Gerais uê boi.

----------------- Acordes -----------------
B = X 2 4 4 4 2
E = 0 2 2 1 0 0
F#7 = 2 4 2 3 2 2
