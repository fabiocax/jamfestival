﻿Victor e Leo - Sabor Dos Ventos

Capo Casa 2

      Am  C    G                                    F
Você sabe, também não penso que poderia ser tão diferente,
                   G
O amor entre nós dois
      Am  G/B C   G                                       Dm
Você fala,       como não se importasse mas no fundo quisesse saber,
C/E  F                 G
       se escrevo de você
Am                 G                F                        G
Ontem eu sei que exagerei, mas não posso ignorar o que sinto
Am                 G               F
E você sabe tudo o que falei, mas esse é meu instinto

Refrão:

C                G                     Dm
  um dia eu me entrego e no outro quero dominar,
            F
Ah se eu pudesse entender o tempo
C            G                  Dm
  não teria medo algum de lhe dizer,

                  F
O toque dos seus lábios tem sabor dos ventos

----------------- Acordes -----------------
Capotraste na 2ª casa
Am*  = X 0 2 2 1 0 - (*Bm na forma de Am)
C*  = X 3 2 0 1 0 - (*D na forma de C)
C/E*  = 0 3 2 0 1 0 - (*D/F# na forma de C/E)
Dm*  = X X 0 2 3 1 - (*Em na forma de Dm)
F*  = 1 3 3 2 1 1 - (*G na forma de F)
G*  = 3 2 0 0 0 3 - (*A na forma de G)
G/B*  = X 2 0 0 3 3 - (*A/C# na forma de G/B)
