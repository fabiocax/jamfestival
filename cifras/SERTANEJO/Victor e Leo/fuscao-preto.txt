﻿Victor e Leo - Fuscão Preto

Capo Casa 2

Intro: G D G D G

G                                 D
Me disseram que ela foi vista com outro
             C         D          G
Num fuscão preto pela cidade a rodar
                             D
Bem vestida igual à dama da noite
            C           D           G
Cheirando a álcool e fumando sem parar
                                    D
Meu Deus do céu, diga que isso é mentira
           C         D          G
Se for verdade esclareça por favor
                             D
Daí a pouco eu mesmo vi o fuscão
          C             D             G
E os dois juntos se desmanchando em amor

Refrão:
        D                    C
Fuscão preto você é feito de aço

                      D
Fez o meu peito em pedaços
                     C  D
Também aprendeu a mataaar
        D                        C
Fuscão preto com o seu ronco maldito
                  D
Meu castelo tão bonito
                G
Você fez desmoronar

----------------- Acordes -----------------
Capotraste na 2ª casa
