﻿Victor e Leo - Quando Vi Você

Capo Casa 4

[Intro]  G  C  Am  D/F#
         G  C  Am  D/F#

G                C                D
   Quando vi você, não tive reação
       C                      G
Sem palavras, fiquei só te olhando
           C              D
Vi que te amar não era opção
      C                   G
Minha vida estava te chamando
            Am                C
Pouco pra dizer, muito pra sentir
              D
Fui embora sonhando

G                  C              D
   Quando te liguei, não acreditei
       C                 G
Era a voz do meu anjo falando
                 C        D
Bem mais que um dia imaginei

        C             G
Era um sonho se realizando
               Am            C
Muito mais que só pensar em mim
               D
Eu estava te amando

G          D
    Amo você
Am     D    G              C
    Entendo   seu jeito de ser
Am     D        G
   Me rendo ao vento
        C             D     G
Que me leva onde estiver você

[Solo]  G  C  Am  D
        G  C  Am  D

G                  C              D
   Quando te liguei, não acreditei
       C                 G
Era a voz do meu anjo falando
                 C        D
Bem mais que um dia imaginei
        C             G
Era um sonho se realizando
               Am            C
Muito mais que só pensar em mim
               D     D/F#
Eu estava te amando

G          D
    Amo você
Am     D    G              C
    Entendo   seu jeito de ser
Am     D        G
   Me rendo ao vento
        C             D     G
Que me leva onde estiver você

G          D
    Amo você
Am     D    G              C
    Entendo   seu jeito de ser
Am     D        G
   Me rendo ao vento
        C             D     G
Que me leva onde estiver você

----------------- Acordes -----------------
Capotraste na 4ª casa
Am*  = X 0 2 2 1 0 - (*C#m na forma de Am)
C*  = X 3 2 0 1 0 - (*E na forma de C)
D*  = X X 0 2 3 2 - (*F# na forma de D)
D/F#*  = 2 X 0 2 3 2 - (*F#/A# na forma de D/F#)
G*  = 3 2 0 0 0 3 - (*B na forma de G)
