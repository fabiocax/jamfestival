﻿Victor e Leo - Não Vá Pra Califórnia

Capo Casa 2

Caso queira tocar a versão do primeiro disco, remova o capo e mude o tom pra A

C
Espere um pouco
              Am
Pra que pressa de partir?
 C
Espere um pouco
             Am
Fique mais um tempo aqui

(refrão)
     F          C           G
Amor, não vá pra Califórnia
     F          C           G
Amor, não vá pra Califórnia

C                                   Am
Não pense, não espere que vou impedir
C                                  Am
Também não posso ir, só posso te pedir


           F           C                    G
Antes do seu avião subir, saudade vai chegar
          F             C          G
Amarrando o meu sorriso só pra ver
         F             C              G
Só pra ver alguma lágrima querendo se soltar
               F                  C         G       C
Dos meus olhos quando a hora de dizer adeus chegar

----------------- Acordes -----------------
Capotraste na 2ª casa
Am*  = X 0 2 2 1 0 - (*Bm na forma de Am)
C*  = X 3 2 0 1 0 - (*D na forma de C)
F*  = 1 3 3 2 1 1 - (*G na forma de F)
G*  = 3 2 0 0 0 3 - (*A na forma de G)
