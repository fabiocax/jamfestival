Victor e Leo - Pagode Em Brasília

E|--0-11---0-2---0-5---0-4-----0-7---0-2---0-5---0-4---|
B|-0--12--0--4--0--7--0--5----0--9--0--4--0--7--0--5---|
G|-----------------------------------------------------|
D|-----------------------------------------------------|
A|-----------------------------------------------------|
E|-----------------------------------------------------|

E|--0-7---0-9----0-11---0-12---------------------------|
B|-0--9--0--10--0--12--0--9----------------------------|
G|-----------------------------------------------------|
D|-----------------------------------------------------|
A|-----------------------------------------------------|
E|-----------------------------------------------------|

 E
Quem tem mulher que namora
                    B7
Quem tem burro empacador

Quem tem a roça no mato
                       E
Me chama que jeito eu dou

                   E7
Eu tiro a roça do mato
               A  E A
Sua lavoura melhora
B7
E o burro empacador
                 E
Eu corto ele na espora
B7
E a mulher namoradeira
                           E
Eu passo um couro e mando embora

(Intro)

E
Tem prisioneiro inocente
                 B7
No fundo de uma prisão

Tem muita sogra encrenqueira
                 E
E tem violeiro embrulhão
                  E7
Pros prisioneiro inocente
               A  E A
Eu arranjo advogado
B7
E a sogra encrequeira
                  E
Eu dou de laço dobrado
B7
E os violeiro embrulhão
                      E
Com meus verso tão quebrado

(Intro)

E
Bahia deu Rui Barbosa
                 B7
Rio Grande deu Getúlio

Em Minas deu Juscelino
                    E
De São Paulo eu me orgulho
                  E7
Baiano não nasce burro
                      A  E A
Gaúcho é o rei das cochilhas
B7
Paulista ninguém contesta
                     E
É o brasileiro que brilha
B7
Quero ver cabra de peito
                   E
Pra fazer outra Brasília

(Intro)

E
No estado de Goiás
                   B7
Meu pagode está mandando

O Bazar do Valdomiro
                 E
Em Brasília é soberano
               E7
O repique da viola balanceia
         A  E A
O chão goiano
B7
Vou fazer minha retirada
                     E
E despedir dos paulistanos
B7
Adeus que eu já vou embora
                   E
Que Goiás ta me chamando

(Intro)

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
