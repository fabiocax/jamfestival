﻿Victor e Leo - A Felicidade e Seu Espelho

Capo Casa 2

[Intro] C  Am  F  G

C                  G             Am
Fiz uma tapera com paina de amor
                E              Am
Na beira de uma estrada de chão
              C
Um riacho ali passava
F                      C
 Também tinha um pé de flamboyant vermelho
G                  C        C  G
A felicidade e seu espelho

C                        G           Am
Nesse céu, bem no meio do meu sertão
                   E              Am
Só faltava a minha estrela chegar
                     C
Com o vento em seus cabelos
F             C
 E a grama da campina no joelho

G                  C         C7
A felicidade e seu espelho

F                       C
Nas garoas refrescantes do verão
F
Seu vestido molhado
C            Am
Seu beijo de paixão
F                           G
E, em noites claras, feitas pra se amar
F          C              G
Eu cantava só pra ela e o luar

C
Todos os meus sonhos
     Am
Eram dela e dessa terra
F                      G
De tesouros escondidos na simplicidade
C
Eu e ela, fim de tarde
Am
Olhando da janela
F
O pôr do Sol nascendo
G             C
No espelho da felicidade

( C  Am  F  G )

C7  F                    C
Nas garoas refrescantes do verão
F
Seu vestido molhado
C            Am
Seu beijo de paixão
F                           G
E, em noites claras, feitas pra se amar
F          C              G
Eu cantava só pra ela e o luar

C
Todos os meus sonhos
     Am
Eram dela e dessa terra
F                      G
De tesouros escondidos na simplicidade
C
Eu e ela, fim de tarde
Am
Olhando da janela
F
O pôr do Sol nascendo
G             C
No espelho da felicidade

( G  C )

----------------- Acordes -----------------
Capotraste na 2ª casa
Am*  = X 0 2 2 1 0 - (*Bm na forma de Am)
C*  = X 3 2 0 1 0 - (*D na forma de C)
C7*  = X 3 2 3 1 X - (*D7 na forma de C7)
E*  = 0 2 2 1 0 0 - (*F# na forma de E)
F*  = 1 3 3 2 1 1 - (*G na forma de F)
G*  = 3 2 0 0 0 3 - (*A na forma de G)
