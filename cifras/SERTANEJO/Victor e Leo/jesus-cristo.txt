﻿Victor e Leo - Jesus Cristo

Capo Casa 3

Intro: Em G Bm Am

 Em                            G
Olho pro céu e vejo uma nuvem branca que vai passando
Bm                                Am
Olho para a terra e vejo uma multidão que vai caminhando
Em                                 G
Como essa nuvem branca essa gente não sabe aonde vai
Bm                          Am
Quem poderá dizer o caminho certo é você meu pai

(refrão 2x)
Em             G              Bm            Am
Jesus Cristo, Jesus Cristo, Jesus Cristo eu estou aqui...

Em                               G
Toda essa multidão tem no peito amor e procura a paz
Bm                  Am
E apesar de tudo a esperança não se desfaz
Em                                  G
Olhando a flor que nasce no chão daquele que tem amor

Bm                              Am
Olho pro céu e sinto crescer a fé no meu Salvador

Refrão 2x:
Em             G              Bm            Am
Jesus Cristo, Jesus Cristo, Jesus Cristo eu estou aqui...

Solo: Em G Bm Am
     Em                  G                         Bm             Am
E|--------------------------------|        e|-------------------------------------------|
B|--------------------------------|        B|-------------------------8-----------------|
G|---------------7-10/12-10-7-----|        G|---7-3-5-7-5-3-3h5---5/7---7\5-3-5p3-------|
D|--------7-8-10--------------8~--|        D|-------------------------------------5~----|
A|---8/10-------------------------|        A|-------------------------------------------|
E|--------------------------------|        E|-------------------------------------------|

   Em                      G                              Bm                       Am
E|-------------6---------6-6h8-6-------|         e|--------------------------------------------6---------|
B|---8-8-8-8-6---8---6h8---------6-----|         B|---3h4p3--------------------3h4p3---3-4-6/8---8~~\----|
G|-------------------------------------|         G|---------5p3--------3-3h5---------5-------------------|
D|-------------------------------------|         D|-------------3-3h5------------------------------------|
A|-------------------------------------|         A|------------------------------------------------------|
E|-------------------------------------|         E|------------------------------------------------------|


Em                                 G
Em cada esquina eu vejo o olhar perdido de um irmão
Bm                               Am
Em busca do mesmo bem nessa direção caminhando vem
Em                            G
É meu desejo ver aumentando sempre essa procissão
Bm                             Am
Para que todos cantem na mesma voz essa oração

(refrão 2x)
Em             G              Bm            Am
Jesus Cristo, Jesus Cristo, Jesus Cristo eu estou aqui...

O refrão é cantado mais duas vezes só vocal e palmas.
 Quando for voltar com o instrumental completo, a música sobe meio tom.

Arraste o capotraste para a 4ª e mantenha os mesmos acordes tocando o refrão três vezes.

No encerramento, bata 3 vezes no acorde de Em 1 vez com as cordas soltas e 2 vezes no Em denovo. Fica assim:

E|-----------------------|
B|-----------------------|
G|---0--0--0--0--0--0----|
D|---2--2--2--0--2--2----|
A|---2--2--2--0--2--2----|
E|---0--0--0--0--0--0----|

----------------- Acordes -----------------
Capotraste na 3ª casa
Am*  = X 0 2 2 1 0 - (*Cm na forma de Am)
Bm*  = X 2 4 4 3 2 - (*Dm na forma de Bm)
Em*  = 0 2 2 0 0 0 - (*Gm na forma de Em)
G*  = 3 2 0 0 0 3 - (*A# na forma de G)
