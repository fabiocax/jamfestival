Victor e Leo - Boa Sorte Pra Você

Intro 2x: Am F C G

C                   Am
  Pra você não foi sério
C                 Am
  E se ainda te quero
         Dm
Sei que não dá mais
F
  Já tentamos
G
  Desisti
C                  Am
  Você fez tudo errado
C                    Am
  Sem me ver ao seu lado
      Dm
Você desprezou
F             G
  Quem apenas   te deu valor


Refrão:
C                                F
  Eu já disse o que tinha pra dizer
                Am
Não quis te perder
                Dm
Fiz tudo por você
   G        C
E nada por mim
                    F
Se tinha que ser assim

Tudo bem
       Am
Já passou
                 Dm
Boa sorte pra você
     G
É o fim do nosso amor

(repete tudo)

Refrão 2x:
C                                F
  Eu já disse o que tinha pra dizer
                Am
Não quis te perder
                Dm
Fiz tudo por você
   G        C
E nada por mim
                    F
Se tinha que ser assim

Tudo bem
       Am
Já passou
                 Dm
Boa sorte pra você
     G             C
É o fim do nosso amor
                                 F
  Eu já disse o que tinha pra dizer
                Am
Não quis te perder
                Dm
Fiz tudo por você
   G        C
E nada por mim
                    F
Se tinha que ser assim

Tudo bem
       Am
Já passou
                 Dm
Boa sorte pra você
     G             Am
É o fim do nosso amor

F                 C  G
  Não quis te perder
Am                F  C  G
   Fim do nosso amor
                 Am
Boa sorte pra você

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
