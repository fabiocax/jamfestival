﻿Victor e Leo - Beijo de Luz

Capo Casa 4

Intro 2x: Am  F  G

Ritmo: ↓↑↓↑↓↑

Solo Intro

E|-------------------------------------------------------------3---
B|-----1-1-1------1-0-1-0-5p3---3s1---1-0-0-1-1-1------1-0-1-3-0---
G|-0-0-2-2-2-0----2-0-0-0-----5-----2---0-0-2-2-2-0----2-0-0---0---
D|-2-----------2--3---------------------------------2--3-------0---
A|-------------------------------------------------------------2---
E|-------------------------------------------------------------3---

          Am  C
Lembro de cor
                  G
Das coisas que você me dizia
    Am  C
Tão só,
                G
Não mais me sentia


              Am  C
Você disse assim:
               G
Ninguém está sozinho
          Am  C
Antes do fim,
               G
Há sempre um caminho

Am
Eu me lembro disso
C
Nosso compromisso
G
Era apenas nossa união
     Am
Seu beijo no meu beijo
C
Luzes no meu peito
G
Fogo dentro do coração

Refrão 2X
             Am  F
Um beijo de luz
         Dm                 G
Exclusivamente pra te fazer bem
    Am  F
De luz
                Dm                  G
Você não é exatamente o que eu imaginei
                  Am
É mais do que sonhei

Solo Meio(Uma variação do solo da introdução)

E|------------|-----------------|----------------------------------------------3---
B|-1-1------1-|------0-1-0------|-5-5p3---3-----5-5p3-------1-1-1-----1-0-1-3--0---
G|-2-2-0----2-|------0-0-0------|-------5---5-2--------0--0-2-2-2-0---2-0-0----0---
D|-------2--3-|------Frase|------|----------------------2------------2-3--------0---
A|------------|---opcional ou---|----------------------------------------------2---
E|------------|deixar o F soando|----------------------------------------------3---

(Repete tudo)

O Victor na versão ao vivo usa algumas variações nos acordes:

G|- Variação G4

Am - Variação Am7 e A9

Dm - Variação D9

F|- F9

----------------- Acordes -----------------
Capotraste na 4ª casa
A*  = X 0 2 2 2 0 - (*C# na forma de A)
Am*  = X 0 2 2 1 0 - (*C#m na forma de Am)
C*  = X 3 2 0 1 0 - (*E na forma de C)
Dm*  = X X 0 2 3 1 - (*F#m na forma de Dm)
F*  = 1 3 3 2 1 1 - (*A na forma de F)
F9*  = 1 3 5 2 1 1 - (*A9 na forma de F9)
G*  = 3 2 0 0 0 3 - (*B na forma de G)
