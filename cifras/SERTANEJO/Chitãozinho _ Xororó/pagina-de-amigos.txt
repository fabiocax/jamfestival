Chitãozinho & Xororó - Página de Amigos

[Refrão]

 C
Como é que eu posso ser
             G
Amigo de alguém que eu tanto amei
    F
Se ainda existe aqui comigo
      C             G
Tudo dela e eu não sei
     C
Não sei o que vou eu fazer
          C7                F  Fm
Pra continuar minha vida assim
       C
Se o amor que morreu dentro
 G                  C
Dela ainda vive em mim

[Solo] C  G  F  C  G


E|-------------------------------------------------------------|
B|-1/8-8-6-5-5-3-3--3/5-3/5--3--1-1------1-3-1-3/5-6p5-3-------|
G|----------------------------------0-2------------------------|
D|-------------------------------------------------------------|
A|-------------------------------------------------------------|
E|-------------------------------------------------------------|

[Primeira Parte]

C
Ela ligou terminando
 G
Tudo entre eu e ela
   F
E disse que encontrou
          C  G
Outra pessoa
C
Ela jogou os meus sonhos
 G
Todos pela janela
   F
E me pediu pra entender
              C  G
Encarar numa boa

[Pré-Refrão]

 C
Como se meu coração
               G
Fosse feito de aço

Pediu pra esquecer os beijos
       F
E abraços
E pra machucar
                 C   G
Ainda brincou comigo
 C
Disse em poucas palavras
              G
Por favor, entenda

O seu nome vai ficar na
        F
Minha agenda
    G          C    G
Na página de amigos

[Refrão]

 C
Como é que eu posso ser
             G
Amigo de alguém que eu tanto amei
    F
Se ainda existe aqui comigo
      C             G
Tudo dela e eu não sei
     C
Não sei o que vou eu fazer
          C7                F  Fm
Pra continuar minha vida assim
       C
Se o amor que morreu dentro
 G                  C
Dela ainda vive em mim

[Solo] C  G  F  C  G

E|-------------------------------------------------------------|
B|-1/8-8-6-5-5-3-3--3/5-3/5--3--1-1------1-3-1-3/5-6p5-3-------|
G|----------------------------------0-2------------------------|
D|-------------------------------------------------------------|
A|-------------------------------------------------------------|
E|-------------------------------------------------------------|

[Primeira Parte]

C
Ela jogou os meus sonhos
 G
Todos pela janela
   F
E me pediu pra entender
              C  G
Encarar numa boa

[Pré-Refrão]

 C
Como se meu coração
               G
Fosse feito de aço

Pediu pra esquecer os beijos
       F
E abraços
E pra machucar
                 C   G
Ainda brincou comigo
 C
Disse em poucas palavras
              G
Por favor, entenda

O seu nome vai ficar na
        F
Minha agenda
    G          C    G
Na página de amigos

[Refrão]

 C
Como é que eu posso ser
             G
Amigo de alguém que eu tanto amei
    F
Se ainda existe aqui comigo
      C             G
Tudo dela e eu não sei
     C
Não sei o que vou eu fazer
          C7                F  Fm
Pra continuar minha vida assim
       C
Se o amor que morreu dentro
 G                  C
Dela ainda vive em mim

       C
Se o amor que morreu dentro
 G                  C
Dela ainda vive em mim

----------------- Acordes -----------------
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
