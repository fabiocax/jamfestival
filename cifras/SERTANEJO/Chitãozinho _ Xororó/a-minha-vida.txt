Chitãozinho & Xororó - A Minha Vida

A minha vida (Comme D'Habitute) (My way)

De: Fraçois/Thibault/Revraux/Paul Anka
versão: Eduardo Lajes/Paulo Sérgio Valle
Introd.: G

    G            Bm/F#
Eu sei, eu aprendi
          F6           E4        E/G#
A me aceitar do jeito que eu sou
    Am          Am7/G         D/F#  D
Deixei meu coração falar por mim
           G4       G
Na vida e na canção
  G
Amei
G7+       F/G  G
   Até demais
      C  C5+ C6          Eb6
Libertei        paixões contidas
     G/D         D           C/G G
Mas fiz do teu amor a minha vida

    G
Sofri
          Bm/F#
Me maltratei
          F6        E4    E  D/F# E/G#
Mas insisti nos mesmos erros
    Am        Am7/G
E até na solidão
           D/F# D
Eu me agarrei
           G4    G
Num resto de ilusão
    G  G7+          F/G G
Talvez    fosse melhor
             C C5+ C6
Deixar pra trás
           Eb6
Mágoas sofridas
     G/D         D
Mas fiz do teu amor
         C/G G
A minha vida
G/D C/D  D   G G7+              F/G
 Você   não vê   que eu  sou assim
 G       C             C7+
Meu coração é dono de mim
 C          Am            D
Não quer saber se eu vou sofrer
D/C          B7           Em
E o que vai ser se eu te perder
           Am          D           C G
Só quer fazer do teu amor A minha vida
    G             Bm/F#
Tentei viver pra mim
        F6             E4 E D/F# E/G#
E te arrancar do pensamento
     Am           Am7/G
Busquei novas paixões
         D/F# D         G4   G
Pra enganar     meu sentimento
    G  G7+    F/G
Voltei   E hoje eu sei
         C C5+ C6         Eb6
Que pra mim     não tem saída
    G/D         D
Eu fiz do teu amor
         C/G G
A minha vida

Solo: G/D C/D D G Gb/G  F/G
      G79- C C5+ C6
 C          Am            D
Não quer saber se eu vou sofrer
D/C          B7           Em
E o que vai ser se eu te perder
           Am          D
Só quer fazer do teu amor
         C G
A minha vida
 Em        Am          D
Só quer fazer do teu amor
         C  C/E   C/G
A minha vi.......da

Finaliza: Cm Eb6 Cm/G G

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
Am7/G = 3 0 2 0 1 0
B7 = X 2 1 2 0 2
Bm/F# = X X 4 4 3 2
C = X 3 2 0 1 0
C/D = X X 0 0 1 0
C/E = 0 3 2 0 1 0
C/G = 3 3 2 X 1 X
C5+ = X 3 2 1 1 X
C6 = 8 X 7 9 8 X
C7+ = X 3 2 0 0 X
Cm = X 3 5 5 4 3
Cm/G = X X 5 5 4 3
D = X X 0 2 3 2
D/C = X 3 X 2 3 2
D/F# = 2 X 0 2 3 2
E = 0 2 2 1 0 0
E/G# = 4 X 2 4 5 X
E4 = 0 2 2 2 0 0
Eb6 = X X 1 3 1 3
Em = 0 2 2 0 0 0
F/G = 3 X 3 2 1 X
F6 = 1 X 0 2 1 X
G = 3 2 0 0 0 3
G/D = X 5 5 4 3 X
G4 = 3 5 5 5 3 3
G7+ = 3 X 4 4 3 X
Gb/G = X X 5 6 7 6
