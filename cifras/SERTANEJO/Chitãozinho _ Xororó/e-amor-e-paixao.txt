Chitãozinho & Xororó - É Amor, É Paixão

[Pré-solo]

E|----9-----|----9----10---9--7---5--4------|
B|--10------|--10---10---10--9---7--6-------|
G|----------|-------------------------------|
D|----------|-------------------------------|
A|----------|-------------------------------|
E|------8h9-|---------------------------8h9-|

E|---7--9--7---5--4--2--|
B|--9--9--9-------------|
G|------------6--4--2---|
D|----------------------|
A|----------------------|
E|----------------------|

[Intro]

     F#m                      C#                     D
E|-9--9--9--10-10-10-9--7-5-4--|-7-7-7-9-9-9-7-5-4-2--|
B|-10-10-10-10-10-10-10-9-7-6--|-9-9-9-9-9-9-9--------|

G|-----------------------------|---------------6-4-2--|
D|-----------------------------|----------------------|
A|-----------------------------|----------------------|
E|-----------------------------|----------------------|

     Bm                C#                        F#
E|-5-5-5-7-7-7-5-4-2-1--|-9-9-9-10-10-10-9-6-4-2--|
B|-7-7-7-7-7-7-7-6-3-2--|-9-9-9-9--9--9--9--------|
G|----------------------|------------------6-4-3--|
D|----------------------|-------------------------|
A|----------------------|-------------------------|
E|----------------------|-------------------------|

[Primeira parte]

                     C#   D#m
Quanto mais o tempo passa
                  A#m  B
Mais eu gosto de você
                        F#   Aº
Quanto mais tempo eu te olho
                      G#m  E
Mais e mais quero te ver
                     B    C#
Quanto mais você me beija
                     F#  D#m
Mais eu quero te beijar
                     G#
Quanto mais eu te namoro
                 C#
Mais quero te namorar

[Refrão]

F#        B          D#m       B           F#     B
   É amor,  é paixão,    bate forte o meu coração
           C#         B
E eu tô assim, mais afim de viver
F#        B          D#m       B           F#  B   F#  B  F#  F#m
   É amor,  é paixão,    bate forte o meu coraçã...ã......ão

( F#m  C#  D  Bm  C#  F# )

[Segunda parte]

                     C#   D#m
Quanto mais me da carinho
                    A#m  B
Mais ainda eu te preciso
                     F#        Aº
Quanto mais você sorri pra mim
                      G#m  E
Mais quero te ver sorrindo
                     B   C#
Quanto mais você me cura
                     F#  D#m
Mais eu quero te cuidar
                      G#
Quanto mais você me amar assim
                 C#
Mais vou me apaixonar

[Final]
                           F#   C#  F#
E|--9-9-9-10-10-10-9-5-4-2--|
B|--9-9-9-9--9--9--9--------|
G|-------------------6-4-3--|
D|--------------------------|
A|--------------------------|
E|--------------------------|

----------------- Acordes -----------------
A#m = X 1 3 3 2 1
Aº = 5 X 4 5 4 X
B = X 2 4 4 4 2
Bm = X 2 4 4 3 2
C# = X 4 6 6 6 4
D = X X 0 2 3 2
D#m = X X 1 3 4 2
E = 0 2 2 1 0 0
F# = 2 4 4 3 2 2
F#m = 2 4 4 2 2 2
G# = 4 3 1 1 1 4
G#m = 4 6 6 4 4 4
