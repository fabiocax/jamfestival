Chitãozinho & Xororó - Falando Às Paredes

Intro: G  G/B  C
E|-----------------------------------------------------|
B|--2---2----2-----------------------------------------|
G|-----------------------------------------------------|
D|--3---0h3--3-----------------------------------------|
A|--4---4----4-----------------------------------------|
E|-----------------------------------------------------|

E|-----------------------------------------------------|
B|--1-0--2---------------------------------------------|
G|--2-0------------------------------------------------|
D|-------3---------------------------------------------|
A|-------4---------------------------------------------|
E|-----------------------------------------------------|

( G  D/F#  Em  Am  Am7  D7 )

E|--2--3--3/5------------------------------------------|
B|--3--5--5/7------------------------------------------|
G|-----------------------------------------------------|
D|-----------------------------------------------------|
A|-----------------------------------------------------|
E|-----------------------------------------------------|



     G           D/F#        Em
Eu agora estou falando às paredes
       Am          Am7            D7  B7
Já não tenho mais você pra conversar
      Em         Em7+         Em7  E6
Na varanda está vazia aquela rede
         Am         Am7          D7
Onde às vezes eu deitava pra te amar
      G          D/F#          Em
Você foi o amanhecer mais colorido
        Am        Am7        D7   B7
Sem sentido se tornou entardecer
     Em       Em7+        Em7  E6
O vazio da saudade foi tirando
      Am         Am7        D7
A vontade que eu tinha de viver
         C
Só Deus sabe tudo o que eu estou sentindo
       Bm
A tristeza dói no peito sem parar
          Am          Am7         D7
Quantas noites mal dormidas já passei
        Am        Am7         D7
Na esperança de ouvir você chegar
       C
Foram tantas cartas que eu perdi a conta
       Bm
E nos muros, quantas frases escrevi
        Am        Am7         D7
Na esperança que você leia só uma
      Am      Am7            D7
E me faça esquecer que te perdi

      G          D/F#          Em
Você foi o amanhecer mais colorido
        Am        Am7        D7   B7
Sem sentido se tornou entardecer
     Em       Em7+        Em7  E6
O vazio da saudade foi tirando
      Am         Am7        D7
A vontade que eu tinha de viver
         C
Só Deus sabe tudo o que eu estou sentindo
       Bm
A tristeza dói no peito sem parar
          Am          Am7         D7
Quantas noites mal dormidas já passei
        Am        Am7         D7
Na esperança de ouvir você chegar
       C
Foram tantas cartas que eu perdi a conta
       Bm
E nos muros, quantas frases escrevi
        Am        Am7         D7
Na esperança que você leia só uma
      Am      Am7            C
E me faça esquecer que te perdi

(Intro - Primeira Parte)

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Am7 = X 0 2 0 1 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
E6 = X X 2 4 2 4
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
Em7+ = X X 2 4 4 3
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
