Chitãozinho & Xororó - Meu Irmão

 E                                       A
Quem cai e não encontra forças pra se levantar
F#m      A           E
É só chamar que ele vem
F#m                E
E não nega isso a ninguém

 E                                    A
Quem tem em seu coração tristeza e solidão
F#m        A         E
Em todo lugar ele está
F#m       A                E
É só lembrar que tudo fica bem

 E      C#m        A         E
Feliz natal pra você, meu irmão!
   F#m     A                E
Renasce Jesus em nosso coração
E      C#m        A         E
Feliz natal pra você, meu irmão!
          F#m        A                E
Renasce o amor de Jesus em nosso coração


 E                                    A
Quem tem em seu coração tristeza e solidão
F#m        A         E
Em todo lugar ele está
F#m       A                E
É só lembrar que tudo fica bem

 E                           A     F#m     E
Sempre tem alguém a quem preciso pedir perdão
 F#m                    A                  E
Só preciso fazer minha parte e estender a mão

E      C#m        A         E
Feliz natal pra você, meu irmão!
   F#m     A                E
Renasce Jesus em nosso coração
E      C#m        A         E
Feliz natal pra você, meu irmão!
          F#m        A                E
Renasce o amor de Jesus em nosso coração
          F#m        A                E
Renasce o amor de Jesus em nosso coração

----------------- Acordes -----------------
A = X 0 2 2 2 0
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
