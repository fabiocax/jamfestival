Chitãozinho & Xororó - Queixas

Dm
Diz que me odeia, que sou o culpado
                              A
Que você é certa e eu sou errado

Diz que sou um fraco, que não me deseja
                A7                Dm
Diz que nada sente nem quando me beija

Diz que não suporta nada do que faço
          D7                    Gm
Diga se puder sairia dos meus braços
                                   Dm
Quando estou perto quer que eu vá embora
                A                D   A7
Quando estou saindo diz que me adora

            D                D#7
Agente se queixa, chora e reclama
              Bm                      A
Lembra dos defeitos diz que não se ama

Em                               A
Mais no fim de tudo, somos tão carentes
                  G            D
Somos um, do outro muito dependentes

                 D#7
Agente se nega, diz tanta besteira
              D7                  G
Mais no fim sabemos, é coisa passageira
              A                D
Nós erramos tanto e nos esquecemos
              A                D
Que o amor é tudo tudo que queremos

             G                    D
Nós erramos tanto tanto que esquecemos
              A                  D G Gm D
Que o amor é tudo tudo que queremos.



----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D#7 = X 6 5 6 4 X
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
