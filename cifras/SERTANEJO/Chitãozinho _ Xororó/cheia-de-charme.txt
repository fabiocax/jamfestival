Chitãozinho & Xororó - Cheia de Charme

[Intro] C  Bb/C  C  Bb/C  Bb  C6  C
        Dm  Bb  C6  C  F  C4  C

F
Quando a vi
Am            Dm
Logo alí tão perto
Gm              C4  C
  Tão ao meu alcance
       F4  F
Tão distante
      Bb7+
Tão real
            Gm  A47  A7
Tão bom perfume
     Dm  C6  Bb  C
Sei lá
F        Am             Dm
Investi tudo naquele olhar
Gm          C4  C      F4  F
  Tantas palavras num breve

       Bb7+
Sussurrar
         Gm      C6     C       F   Am
Paixão assim não acontece todo dia
Bb  Gm        C  F/A
   Cheia de charme
Bb  Gm     C   F/A  Bb
Um desejo enorme
    C      Dm  Am  Bb
De se aventurar
Gm          C    F/A
   Cheia de charme
Bb  Gm     C   F/A
Um desejo enorme
 Bb     C     Dm   Am  Bb  C6    F   F4  F
De revolucionar,         oh, oh!

[Solo] Bb  C6  C  Dm  Bb  C6  C  F  C4  C

F        Am           Dm
Me perdi entre seus cabelos
Gm         C4  C
  Pela sua pele
         F4  F         Bb7+
Nos seus lábios tão macios
            Gm  A47  A7
Tão bom perfume
     Dm  C6  Bb  C
Sei lá
F       Am              Dm
Investi tudo naquele olhar
Gm          C4  C      F4  F        Bb7+
  Tantas palavras num breve sussurrar
          GM     C6     C
Paixão assim não acontece
     F   Am  Bb
todo dia,   hummmm!
Gm        C   F/A
Cheia de charme, hummm!
Bb    Gm    C  F/A  Bb
Um desejo enorme
      C      Dm  Am  Bb
De se aventurar
Gm           C   F/A
   Cheia de charme
Bb    Gm    C  F/A
Um desejo enorme
 Bb     C      Dm  Am  Bb  C6    F  F4  F
De revolucionar,         oh, oh!

Solo: Bb  C6  C  Dm  Bb  C6  C  F  Am  Bb

Gm           C   F/A
   Cheia de charme
Bb    Gm    C  F/A  Bb
Um desejo enorme
      C      Dm  Am  Bb
De se aventurar
Gm           C   F/A
   Cheia de charme
Bb    Gm    C  F/A
Um desejo enorme
Bb      C      Dm  Am  Bb  C6     F  F4  F
De revolucionar,          oh, oh!

( Bb  C6  C  Dm  Bb  C6  C  F  C4  C  F )

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bb/C = X 3 X 3 3 1
Bb7+ = X 1 3 2 3 1
C = X 3 2 0 1 0
C4 = X 3 3 0 1 X
C6 = 8 X 7 9 8 X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
F4 = 1 3 3 3 1 1
Gm = 3 5 5 3 3 3
