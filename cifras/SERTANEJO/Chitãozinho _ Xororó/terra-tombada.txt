Chitãozinho & Xororó - Terra Tombada

Intro: G  D/F#  Em  Am  D   Am  Am/G  D/F# D (C) G

            C      G               C      G
È calor de mês de agosto, é meados de estação
                              Eº  Dº      D
Vejo sobras de queimadas e fumaça no espigão
                     Am         Am/G           D/F#
Lavrador tombando terra, dá de longe a impressão
                    D                      G        D
De los ângulos cor de sangue desenhados pelo chão
      C   D         G                  C        G
Terra tombada é promessa de um futuro que se espelha
                    G7                        C
No quarto verde dos campos, a grande cama vermelha
                   Am           D            G
Onde o parto da semente, faz brotar de suas covas
   Em           A7        D               G
O fruto da natureza, cheirando a criança nova

          D                        G
Terra tombada solo sagrado chão quente

                   D       Am         D       G
Esperando que a semente venha lhe cobrir de flor
             D                         G
Também minha alma ansiosa espera confiante
                        D        Am    D7    G
Que em meu peito você plante a semente do amor

       C      D    G                 C     G
Terra tombada é criança deitada num berço verde
                                 Eº  Dº          D
Com a boca aberta pedindo para o céu matar-lhe a sede
                     Am               Am/G         D/F#
Lá na fonte ao pé da serra é o seio do sertão
                  D                       G       C  D
A água, leite da terra, alimenta a plantação
      C    D       G                      C  G
O vermelho se faz verde, vem o botão, vem a flor
                    G7                      C
Depois da flor a semente, o pão do trabalhador
                    Am         D             G
Debaixo das folhas mortas a terra dorme segura
    Em               A7        D         G
Pois nos dará para o ano novo parto de fartura

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Am = X 0 2 2 1 0
Am/G = 3 X 2 2 1 X
C = X 3 2 0 1 0
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
D7 = X X 0 2 1 2
Dº = X X 0 1 0 1
Em = 0 2 2 0 0 0
Eº = X X 2 3 2 3
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
