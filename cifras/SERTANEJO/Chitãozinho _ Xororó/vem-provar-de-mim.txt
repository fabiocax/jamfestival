Chitãozinho & Xororó - Vem Provar de Mim

(intro) Bbm  Ebm   G#   C#  F
            Bbm  Bb7  Ebm  Bbm  F  Bbm  F  Bbm

Bbm
Tem gosto de sol quando é chuva
                         Ebm
No frio parece calor.
                                  F
Tem gosto de vinho é uva
         Ebm        F     Bbm  F  Bbm
E às vezes parece licor.

Bbm
É doce e parece açúcar
         Bb7                      Ebm
E às vezes tem gosto de sal.
                  G#             C#     Bbm
É calmo demais, às vezes é paz
           F                     Bb   Bb7
E às vezes é um temporal
       Ebm       G#             C#     Bbm
É calmo demais, às vezes é paz

           F                     Bb   Bb7
E às vezes é um temporal

Bb                    Dm    Bb                    Dm
Vem provar de mim...vem provar de mim
  Bb                              F
Ve se descobre o sabor

         Cm         F         Cm          F
vem provar de mim pra sentir o gosto
                                           Bb  F
Me deixe mostrar quem eu sou

Bb                    Dm    Bb                    Dm
Vem provar de mim,vem provar de mim
Bb            Bb7         Eb
Ve se descobre o sabor...
                                Bb
Vem provar de mim pra depois
   G             Cm         F           Bb
Dizer se eu sirvo pra ser seu amor...

É doce e parece açúcar...

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
Bb7 = X 1 3 1 3 1
Bbm = X 1 3 3 2 1
C# = X 4 6 6 6 4
Cm = X 3 5 5 4 3
Dm = X X 0 2 3 1
Eb = X 6 5 3 4 3
Ebm = X X 1 3 4 2
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
