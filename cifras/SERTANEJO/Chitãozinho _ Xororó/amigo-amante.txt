Chitãozinho & Xororó - Amigo Amante

[Intro] F  C  G7  C
        F  C  G7  C

Cm
Não importa a distância e lugar

Eu só sei que irei te buscar
       Fm
Pelo coração
      G
Te amarrar no meu corpo talvez
            Fm               G
Nos seus braços morrer outra vez
     Cm
De paixão

( Fm  G )

Cm
Com meus olhos olhando nos seus
      A#m   A#m7        C
Descobrir em você o meu deus

                Fm
Na mais doce loucura
      Cm
E a saudade que existe entre nós
         D#             G#
Vai calar duma vez nossa voz
      G       G7       C
Com bastante amor e ternura

F  G7

C
Se voce sente o mesmo que eu

Saberá que jamais me perdeu
       C7              G7
E de amores estamos morrendo
          Dm               G7
Se eu sou tudo o que voce quer
         Dm             G7
Não sou nada sem voce mulher
                        C           Am7  Dm7  G7
Tanto amor e estamos sofrendo

C
Eu não sei porque estamos assim

Se você é tão louca por mim
      C7               F
Só depende da gente querer
       C
Se eu sou seu amigo amante
                       G
Se te quero a todo instante
     G7               C
Minha amiga amante é voce

( F  C  G7  C )

C
Se voce sente o mesmo que eu

Saberá que jamais me perdeu
       C7              G7
E de amores estamos morrendo
          Dm               G7
Se eu sou tudo o que voce quer
         Dm             G7
Não sou nada sem voce mulher
                        C           Am7  Dm7  G7
Tanto amor e estamos sofrendo

C
Eu não sei porque estamos assim

Se voce é tão louca por mim
      C7               F
Só depende da gente querer
       C
Se eu sou seu amigo amante
                       G
Se te quero a todo instante
     G7               C
Minha amiga amante é voce

----------------- Acordes -----------------
A#m = X 1 3 3 2 1
A#m7 = X 1 3 1 2 1
Am7 = X 0 2 0 1 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Cm = X 3 5 5 4 3
D# = X 6 5 3 4 3
Dm = X X 0 2 3 1
Dm7 = X 5 7 5 6 5
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
G# = 4 3 1 1 1 4
G7 = 3 5 3 4 3 3
