Chitãozinho & Xororó - Terra Querida

C                                C7                            F
Venho de longe pra ver meus queridos pais
              G7    F   G7                 C
Pois a saudade já não suportava mais
       Am                                Dm
Sou brasileiro descendente de tupi
           G7     F        G7           C      F G7 C
Eu vim rever minha terra onde nasci
C          C7                            F
É que a saudade apertou meu coração
G7                       F          G7     C
Saudade dos meu pais de meus irmãos
             Am                       Dm
Minha saudade do cantar da juriti

         G7               F     G7    C
Foi a razão que me fez voltar aqui
C
Terra querida
                  G7
Eu sempre te adorei


Quero estar junto de ti
                                            C F G7 C
E ao teu lado para sempre eu viverei

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G7 = 3 5 3 4 3 3
