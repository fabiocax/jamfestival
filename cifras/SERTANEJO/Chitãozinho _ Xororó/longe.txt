Chitãozinho & Xororó - Longe

Intro: (Dm  Fm  C  G)

     C               G/B
Tão longe estás de mim
             C
Que eu penso ser o fim
                   Em
Do nosso grande amor
                    Dm     A
Por não ouvir a sua voz
                   Dm
Mil vales, céus e rios
             G    Dm
Ficaram no vazio
                 G
Pavor do nunca mais
                      C  G
Somente existe entre nós...
   G            C
O Longe me faz mal
G/B              C
Porque lembra final

                C7
E o fim me desespera
                    F
Neste tempo de esperar
    Fm               Fm
Que volte aos braços meus
              C
Porque depois do adeus
           Dm
A dúvida ficou
     G            C      G
Se vais ou não voltar...
               C       G/B
Na espera do será
                C
A gente nunca está
              Em
Completamente só
      Am                    Dm    A
Se olhar num ponto muito além
              Dm
A lua na amplidão
G              Dm
A iluminar no chão
             G
De nossa solidão
                            C     G
Os passos meus e os seus também...
              C
Se a lua que te vê
                 C7+
Dissesse o que você
               C7
Está pensando agora
                   F
Nesta ausência tão cruel
              Fm
Se já me esqueceu
                C    Am
Ou lembras como eu
               Dm
E ao nosso juramento
G           C
Se mantém fiel...

----------------- Acordes -----------------
A = X 0 2 2 2 0
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
C7+ = X 3 2 0 0 X
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
Fm = 1 3 3 1 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
