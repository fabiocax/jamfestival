Chitãozinho & Xororó - Página Virada

Intro: D Em A7 D Em A7

D
Vem... que esse orgulho não
Vai dar em nada
O que passou é página virada
                             Em
Melhor pensar que nada aconteceu
Não, para que eu vou viver
          A7
na solidão
                         Em
Não posso enganar meu coração
A7
Eu penso em você o tempo
D Em A7
inteiro
  D
Vem... quem ama não consegue
Estar sozinho
Eu vivo te buscando em meu

caminho
     D7                    G
Porque já desisti de te esquecer
               A7
Não, não vou ficar vivendo
        D
do passado
       Bm
Saber quem estava certo
           Em
ou errado
      A7                     D
Só quero ver você voltar pra mim
Quem mais precisa desse
 F#7
amor sou eu
Porque de nós quem mais
   Bm
sofreu fui eu
                            A7
Por isso mesmo é que eu te digo
                     D
Não em sentido eu sem você
Quem sabe o que é melhor pra
F#7
mim sou eu
                       Bm
Quem sente falta de você sou eu
G                  D
Eu te quero ao meu lado
               A7     D
É o que pede meu coração
Solo
D
Vem... que de você só lembro
com carinho
Pra que buscar na rosa o espinho
Se há coisas tão bonitas pra
     Em
lembrar
                           A7
Não, viver mentindo não é solução
                      Em
Inútil procurar outra emoção
    A7
Você é uma paixão mal
     D    Em   A7
resolvida
D
Vem... prefiro confessar sem
vaidade
No fundo estou morrendo de
saudade
D7                   G
Você não pode nem imaginar
              A7
Não, não deixe quem te ama
       D
esperando
    Bm                       Em
Ficaram tantas coisas nos ligando
       A7                   D
Só quero ver você voltar pra mim
Quem mais precisa desse
 F#7
amor sou eu
Porque de nós quem mais
   Bm
sofreu fui eu
                          A7
Por isso mesmo é que eu te digo
                       D
Não tem sentido eu sem você
Quem sabe o que é melhor pra
F#7
mim sou eu
                        Bm
Quem sente falta de você sou eu
G                D
Eu te quero ao meu lado
                   A7      D
É o que pede meu coração...

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
F#7 = 2 4 2 3 2 2
G = 3 2 0 0 0 3
