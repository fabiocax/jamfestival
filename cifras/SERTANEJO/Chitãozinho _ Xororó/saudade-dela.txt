Chitãozinho & Xororó - Saudade Dela


Intro:F C F C F


          F
     Meu Deus, eu já percebi
                                 C
     Que não adianta encher a cara
     Eu só fico de zoeira
                Bb                    F
     A dor não passa e a paixão não sara
     Meu Deus, se tiver um jeito
                             C
     Muda o defeito que me magoa
     Me traga de ela volta
                                 F
     Ou então me arranje outra pessoa
     Meu Deus, o que tô passando
                                       C
     Eu posso dizer que nem cachorro passa
     Eu sou tão ligado nela

                   Bb                   F
     Que outra mulher pra mim não tem graça
     Meu  Deus, Tô pedindo alguém
                             C
     Mas posso cair em contradição
     Pois só ela é o remédio
                              F
     Que cura o tédio dessa paixão

                      C
     Meu Deus, eu mudei
                F
     Tô arrependido
                       C
     Sei que fui tão durão
                   Bb              F
     E por ser machão só tenho sofrido
                      C
     Meu Deus, eu mudei
                    F
     Sou outra pessoa
                   C
     De saudade dela
                                F
     Sou um menino que chora à toa

----------------- Acordes -----------------
Bb = X 1 3 3 3 1
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
