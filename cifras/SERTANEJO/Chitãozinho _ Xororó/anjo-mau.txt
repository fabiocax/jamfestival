Chitãozinho & Xororó - Anjo Mau

[Intro] F  C  G7  C  C7  F  C  G  C

C                           G7                  C
Fui eu quem fiz você mudar de vida
    G7                            C                             Dm
Lhe dando cama e comida e conforto de um lar
                                                G7
Fui eu quem lhe dei tanto carinho
                                                                        C
Ensinei-lhe o bom caminho para nunca tropeçar

   C                             G7              C
Fui eu quem um dia lhe tirei da lama
        G7              C               C7                      F
Fiz você uma dama lhe dando uma vida honesta
                                                     C
E agora com o seu comportamento
                                  G7                                      C
Vou dizer neste momento para mim você não presta

                  D7                                  G
Você não quis ser uma mulher honrada

                                   D7                         G
Preferiu descer a escada até o ultimo degrau
                F                                    C
Infelizmente a verdade é nua e crua
                       G7                              C
Sua casa é a rua meu querido anjo mau

----------------- Acordes -----------------
C = X 3 2 0 1 0
C7 = X 3 2 3 1 X
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
