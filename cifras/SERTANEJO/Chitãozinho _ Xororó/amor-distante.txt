Chitãozinho & Xororó - Amor Distante

(intro) G#  D#7  G#  C#  D#7  G#

G#                                                                     D#7
se eu fosse um passarinho, queria voar no espaço
                                                                         G#
e pousar de vagarinho, na voltinha dos seus braços
                                                                         D#7
Queria sentir seu carinho, e aliviar a dor que passo
                                                                          G#
queria te dar um beijinho, e depois um forte abraço

(intro)

 G#                                                      D#7
depois que você partiu, minha vida é sofrer
                                                                        G#
me escreva sem demora, que estou louco pra saber
                                                                      D#7
o lugar que você mora, também quero lhe escrever
                                                                          G#
marcando pra qualquer hora, um encontro com você


(intro)

G#                                                      D#7
voce partiu e me deixando, na mais negra ansiedade
                                                                        G#
sofrendo tanta amargura, e chorando de saudade
                                                                          D#7
meu coração não resiste, pra dizer mesmo a verdade
                                                       G#
para mim já não existe, a tal de felicidade

(intro)

G#                                                                  D#7
é um ditado muito certo, quem ama nunca esquece
                                                                             G#
quem tem seu amor distante, chora, suspira e padece
                                                                    D#7
coração sofre bastante, saudade no peito cresce
                                                                          G#
se você tem um outro amor, seja franca e me esclarece

(intro)

----------------- Acordes -----------------
C# = X 4 6 6 6 4
D#7 = X 6 5 6 4 X
G# = 4 3 1 1 1 4
