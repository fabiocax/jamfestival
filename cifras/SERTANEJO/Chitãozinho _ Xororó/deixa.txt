Chitãozinho & Xororó - Deixa

Intro: E A E A E Cº C#m B A B

E            A                 E  A E            A
Que seu sentimento pode ser engano que essa diferença pode,
       E  E  Ebm  C#m
Ser fatal             pois você tem tanto e eu tenho tão,
 G#m              A                                  B
Pouco que eu não sirvo pra você que esse amor não é normal,
E                A                     E    A E
Que eu não sou aquele que seus pais sonhavam que por mim,
   A              E  E  Ebm  C#m
Perderam toda ilusão             mas se eles pudessem me ver,
    G#m                 A      (A G# F#m)       B
Com seus olhos sei que tudo mudaria por isso amor.

Refrão:
E            Cº          C#m           C#m/B               A
Deixa que murmurem que comentem não importa o que eles pensem,
      B          E    B  E          Cº           C#m
Meu amor está contigo    deixa para que viver sofrendo nosso,
  B           A              B           E
Amor está crescendo ninguém vai nos separar.

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
C#m = X 4 6 6 5 4
C#m/B = 7 X 6 6 5 X
Cº = X 3 4 2 4 2
E = 0 2 2 1 0 0
Ebm = X X 1 3 4 2
F#m = 2 4 4 2 2 2
G# = 4 3 1 1 1 4
G#m = 4 6 6 4 4 4
