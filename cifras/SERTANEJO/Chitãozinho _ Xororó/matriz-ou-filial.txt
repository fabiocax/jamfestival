Chitãozinho & Xororó - Matriz ou Filial

G        Am
Quem sou eu
          D7
Pra ter direitos exclusivos
      Bm
Sobre ela
          Em
Se eu não posso
                    Am
Sustentar os sonhos dela
        D7
Se nada tenho
                     Dm  G
E cada um vale o que tem
          Am
Quem  sou eu
        D7
Pra sufocar a solidão
       Bm
Da sua boca


          Em
Se hoje  diz que e  matriz
         Am
E quase louca
          D7
Quando brigamos
             Dm  G
Diz que e a filial

   C             D7
Afinal se amar demais
                     Bm
Passou a ser o meu defeito
         Em
E bem possível
                        Am
Que eu não tenha mais direito

          D7
De ter matriz
                         Dm   G
Por ter somente amor pra dar
      C         D7
Afinal o que ela pensa
                   Bm
Conseguir me desprezando
         Em
Se a sua sina
                   Am
Sempre e voltar chorando
       D7
Arrependida me pedindo
      Dm  G
Pra ficar
  C    Cm   G
Afinal quem sou eu?

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
Dm = X X 0 2 3 1
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
