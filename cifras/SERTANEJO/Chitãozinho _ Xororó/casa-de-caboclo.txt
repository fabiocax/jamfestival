Chitãozinho & Xororó - Casa de Caboclo

C                                             G7
A minha casa que é casa de caboclo
                                                        C
Não tem conforto como outras casas tem
                                                      G7
O que eu tenho realmente é muito pouco
                                   F         G7       C
Mas felizmente dá pra mim e mais alguém.

C                                                G7
Graças à Deus é uma casa abençoada
                                                      C
Na minha mesa sempre tem o que comer
                                                   G7
E por ventura se alguém pedir pousada
                                    F      G7      C
Esteja certo que eu hospedo com prazer.

C                                                      G7
Eu não invejo quem tem casa mais bonita
                                                        C
Nem menosprezo um ranchinho beira-chão

                                               G7
O que importa é acharem casa rica
                                     F        G7 C
Ou num casebre um bondoso coração.

C                                               G7
E quem procura uma casa de caboclo
                                              C
Não é preciso ficar rouco de chamar
                                                     G7
É o bastante dar sinal que está chegando
                                       F             G7     C
Já vem alguém e vai mandando a gente entrar.

C                                                   G7
Quem não conhece uma casa de caboclo
                                                 C
Não faça pouco vá lá em casa passear
                                                   G7
Um cafézinho com bolinhos não demora
                                        F    G7    C
Conforme a hora também fica pra jantar.

C                                                      G7
Casinha simples encostada ao pé da serra
                                                 C
Se é amigo não repara onde eu moro
                                                      G7
Vá ver de perto o meu céu aqui na terra
                            F             G7      C
E conhecer as criancinhas que eu adoro.

----------------- Acordes -----------------
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G7 = 3 5 3 4 3 3
