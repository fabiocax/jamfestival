Chitãozinho & Xororó - Rastros Na Areia

Gm                                               Cm
O sonho quer tive esta noite foi um exemplo de amor
                       Gm            Cm               D7
Sonhei que na praia deserta eu caminhava com nosso Senhor
    Gm                                          Cm
Ao longo da praia deserta, quis o Senhor me mostrar
                    Gm                          D7
Cenas por mim esquecidas, de tudo que fiz nesta vida
                G
Ele me fez recordar
 G                                                        D7
Cenas das horas felizes, que a mesa era farta na hora da ceia
                                                            G
Por onde eu havia passado ficaram dois pares de rastros na areia
  C                 Cm              Bm
Então o Senhor me falou:  "Em seus belos momentos
Em
passados
  C                       D7                  Gm
Para guiar o seus passos, eu caminhava ao seu lado."


   Gm                                  Cm
Porem minha falta de fé tinha que aparecer
                   Gm                    Cm                  D7
Quando passavam as cenas das horas mais tristes de todo meu ser
 Gm                                              Cm
Então ao Senhor reclamei, somente dois rastros ficou
                     Gm                         D7
Quando eu mais precisava, quando eu sofri e chorava
                   G
O Senhor me abandonou
 G                                                     D7
Naquele instante sagrado que ele abraçou-me dizendo assim:
                                                        G
"Usei a coroa de espinhos, morri numa cruz e duvidas de mim
C                        Cm     Bm                  Em
Filho, estes rastros são meus, ouça o que vou lhe dizer
 C                          D7               Gm
Nas suas horas de angustias Eu carregava você

C                        Cm     Bm                  Em
Filho, estes rastros são meus, ouça o que vou lhe dizer
 C                          D7               Gm
Nas suas horas de angustias Eu carregava você"

----------------- Acordes -----------------
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
Cm = X 3 5 5 4 3
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
