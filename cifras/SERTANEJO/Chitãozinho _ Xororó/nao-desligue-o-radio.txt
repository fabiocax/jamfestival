Chitãozinho & Xororó - Não Desligue o Rádio

D           D7+       D    D7+
Se ligar o rádio por acaso
D          D7+       D    D7+
E de repente ouvir a minha voz
D
Não mude a estação
  F°                      Em
Escute o coração de quem te ama
   A7
Eu tenho tanta coisa pra dizer
Em                      Gm     A
E gostaria que chegasse até você
     G                   A7
Por isso eu lhe peço por favor
        D        D7+  Em   D7+
Não desligue o rádio

D           D7+       D    D7+
Eu vou lembrar a música que um dia
D           D7+       D    D7+
Você ouviu pela primeira vez

 D                  D7               G
Em minha companhia falando de amor e ilusão
                              Gm
Eu quero te dizer que te preciso
   D           D7+       Bm
E continuo sendo aquele amigo
   Em                           A7
E quer estar contigo mesmo sendo assim
   D
Em forma de canção

D       F°         Em  A7
Não desligue o rádio
 A7              D  D7+ D D7+          Em
Escute a minha voz,       eu ando tão sozinho
                     A7
E não consigo esquecer
                D  D7+ D
Eu e você já fomos nós
D       F°         Em  A7
Não desligue o rádio,
                 D  D7+ D
escute essa canção
      D7+    Em                    A7
É a única maneira que achei para chegar
            D   Em  A7
Até seu coração

(solo)

D           D7+       D    D7+
Eu quero te falar da minha vida
D           D7+       D    D7+
Com essa voz que sai do coração
D                          D7
Dizer que a sua ausência aumenta
                   G
E alimenta o meu sofrer
                   Gm
E só agora nessa melodia
     D                    Bm          Em
Encontrei uma maneira de dizer que em todos os meus dias
   A7                    D
perdi muito de mim sem ter você

(refrão)

É a única maneira que achei para chegar
Até seu coração...

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
D7+ = X X 0 2 2 2
Em = 0 2 2 0 0 0
F° = X X 3 4 3 4
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
