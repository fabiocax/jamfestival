Chitãozinho & Xororó - É Disso Que o Velho Gosta

[Intro] A  D  A  D
        A  D  A  D

D                   A
Eu sou um peão de estância
                 D
Nascido lá no galpão
                   A
E aprendi desde criança
                D7
A honrar a tradição
      G
Meu pai era um gaúcho

Que nunca conheceu luxo
       E7           A
Mas viveu folgado enfim
                      Em
E quando alguém perguntava
                  A
O que ele mais gostava

                 D
O velho dizia assim

                    A
Churrasco e bom chimarrão
                     D
Fandango, trago e mulher
                     A
É disso que o velho gosta
                    D
É isso que o velho quer

( A  D  A  D )

D                     A
E foi assim que aprendi
                   D
A gostar do que é bom
                   A
A tocar minha cordeona
                    D7
Cantar sem sair do tom
      G
Ser amigo dos amigos

Nunca fugir do perigo
     E7              A
Meu velho pai me ensinou
                 Em
Eu que vivo a cantar
                    A
Sempre aprendi a gostar
                    D
Do que meu velho gostou

                      A
Churrasco e bom chimarrão
                     D
Fandango, trago e mulher
                     A
É disso que o velho gosta
                    D
É isso que o velho quer

( A  D  A  D )

                 A
Sai da minha fazenda
                  D
E me soltei pelo pago
                 A
Hoje tenho uma gaúcha
                D7
Para me fazer afago
                  G
E quando vier o piazito

Para enfeitar nosso ninho
       E7         A
Mais alegria vou ter
                  Em
E se ele me perguntar
                  A
Do que se deve gostar
                   D
Como meu pai vou dizer

                      A
Churrasco e bom chimarrão
                     D
Fandango, trago e mulher
                     A
É disso que o velho gosta
                    D
É isso que o velho quer

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
