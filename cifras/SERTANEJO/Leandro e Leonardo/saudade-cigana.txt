Leandro & Leonardo - Saudade Cigana

     D
Essa coisa que me entra pelos poros
                      A       G
E me invade o coração
G                               A                             D
É a saudade fazendo alarde quando quer chegar
D                    A
Toma conta de tudo
A                                           D
Faz sua morada dentro do meu peito
                 A
Me faz tão carente, me deixa aflito
                         D
Me põe tão sem jeito

  D         G                        A                        D
Essa saudade cigana que vem, que vai e que volta
               G                       A                         D
Chega e entra no meu coração sem bater na porta
              G                     A                            D
Essa saudade fica revirando os meus pensamentos

           G                        A                           D
Me sufocando explode no peito, me sangra por dentro

          A                            D
Dói demais (dói demais), dói sim (dói sim)
    G                      A          G          A              D
Na falta que você me faz, o meu mundo é tão ruim
         A                               D
Dói demais (dói demais), dói sim (dói sim)
  G                      A           G             A             D
Mas quando você voltar, vou te amar além de mim

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
G = 3 2 0 0 0 3
