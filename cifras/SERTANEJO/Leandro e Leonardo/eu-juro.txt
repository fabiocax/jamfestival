Leandro & Leonardo - Eu Juro

[Intro] F  Dm  Am  Bb/C  C7

F           Bb/D   C/E   F
 Eu vejo a luz do teu olhar
          Bb    C   Dm
Como uma noite de luar
             Bb          Bb/C  C7
Luz que me guia onde eu for
   F         Bb/D  C/E   F
Você, meu motivo pra sorrir
         Bb/D   C/E   Dm
Caminho certo pra seguir
          F/C
Saiba que é só teu
 G/B             Bb/C  C7
Meu verdadeiro amor

    F             Dm
Eu juro, por mim mesmo
                    Am
Por Deus, por meus pais

         Bb/C  C7
Vou te amar
    F               Dm              Am
Eu juro, que esse amor não acaba jamais
         Bb  C
Vou te amar

   Gm              Bb/C     C7
É tanto querer, é tanta paixão
   Gm               Bb/C    C7
Te amo do fundo do meu coração
    F     Dm  Am  Bb/C  C7
Eu juro

F         Bb/D C/E   F
 Um homem e  uma mulher
             Bb    C   Dm
Juntos pro que der e vier
        Bb             Bb/C  C7
A meia luz, a dois, a sós
   F      Bb/D  C/E    F
Então, a gente vai sonhar
         Bb/D C/E Dm
E conseguir realizar
              F/C
Um mundo de amor sem fim
     G/B              Bb/C  C7
Porque só depende de nós

    F             Dm
Eu juro, por mim mesmo
                    Am
Por Deus, por meus pais
         Bb/C  C7
Vou te amar
    F               Dm              Am
Eu juro, que esse amor não acaba jamais
         Bb  C
Vou te amar

   Gm              Bb/C     C7
É tanto querer, é tanta paixão
   Gm               Bb/C    C7
Te amo do fundo do meu coração
    F     Dm  Am  Bb/C  C7
Eu juro

[Solo]

E|-----------------------------|-------------------------------------|
B|-------13~------------10h-11-|--13p--11p--10-----------------------|
G|--/10------------------------|----------------12br-----Lp--10--9~--|
D|-----------------------------|-------------------------------------|
A|-----------------------------|-------------------------------------|
E|-----------------------------|-------------------------------------|

             PM
E|-----------------------------|---------------------------------|
B|-----------------------------|---------------------------------|
G|--L--------------------------|----------------------------10~--|
D|-----10--12--8--10\----------|--8--10--12p--10~------/10-------|
A|-------------------------12--|---------------------------------|
E|-----------------------------|---------------------------------|

E|----------15p--13--15s--17--18--18br--|---L---L--17--15br---------------|
B|------13------------------------------|------------------------18--17~--|
G|---L----------------------------------|---------------------------------|
D|--------------------------------------|---------------------------------|
A|--------------------------------------|---------------------------------|
E|--------------------------------------|---------------------------------|

E|---------------------------------------|----------20br--------20b--20~--|
B|---L--18b--18p-17p-15------------------|------18------------------------|
G|-----------------------17\------17s-19-|--------------------------------|
D|---------------------------------------|--------------------------------|
A|---------------------------------------|--------------------------------|
E|---------------------------------------|--------------------------------|

    F             Dm
Eu juro, por mim mesmo
                    Am
Por Deus, por meus pais
         Bb/C  C7
Vou te amar
    F               Dm              Am
Eu juro, que esse amor não acaba jamais
         Bb  C
Vou te amar

   Gm              Bb/C     C7
É tanto querer, é tanta paixão
   Gm               Bb/C    C7
Te amo do fundo do meu coração
    F     Dm  Am  Bb/C  C7
Eu juro

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bb = X 1 3 3 3 1
Bb/C = X 3 X 3 3 1
Bb/D = X 5 X 3 6 6
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
C7 = X 3 2 3 1 X
Dm = X X 0 2 3 1
F = 1 3 3 2 1 1
F/C = X 3 3 2 1 1
G/B = X 2 0 0 3 3
Gm = 3 5 5 3 3 3
