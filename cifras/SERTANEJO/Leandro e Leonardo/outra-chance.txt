Leandro & Leonardo - Outra Chance

(Intro)

Falado: “Se havia uma verdade nas mentiras que contei
é que ela foi em minha vida a mulher que eu mais amei"

E                                            B7
Tenho tanto amor pra dar pra ela
A                                        E    B7
Mas ela não quer saber de mim
E                                       B7
Fico no meu quarto só pensando
    A                F#m                  B7
Será que esse amor chegou ao fim?

Parte 2:

       C#m                               G#m
Te procuro no telefone o dia inteiro
           A                                                              E    E   B7
E uma voz que me atende me diz que você não esta
           C#m                                                    G#m
Mas no fundo eu escuto uma voz sussurrando baixinho

           A                                                  B7
Se for ele peça, por favor, pra não mais ligar
                  A                                           E
Deixa eu falar com ela um minuto, por favor
                  A                              B7               E    E   B7
Eu quero pedir outra chance, provar o meu amor
C#m              G#m                 A   B7         E     E   B7
Não desligue agora, peça pra ela falar comigo
C#m               G#m                 A
Vá, insista um pouco, fala pra ela
                            B7                        E
Que estou ficando louco de saudade dela
                   A                        E
De saudade dela, de saudade dela

(Intro)
(Repete Parte 2)

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
F#m = 2 4 4 2 2 2
G#m = 4 6 6 4 4 4
