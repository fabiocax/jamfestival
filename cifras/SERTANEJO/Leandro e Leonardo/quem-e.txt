Leandro & Leonardo - Quem É

(intro)  C  C/B  F/A  C/G  G7  C  C/B  F/A  F/G

C                                       G
Quem é o dono dos seus olhos?
         F
Quem é?
                                     C
Quem roubou você de mim?
                             G/B
Sei que ele não te ama
                                 F
Só quer te levar pra cama
                                     C     G/B   Am
Só pra lhe ter um pouquinho

      G                                          Am
Eu sei que é de mim que você gosta
                              G
Mexo com seu sentimento
                           C
Toco no seu coração

       G                                Am
Me dê só uma noite de prazer
                              F
Quero provar pra você
             G               C
Que eu sou sua paixão

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
C/B = X 2 2 0 1 0
C/G = 3 3 2 X 1 X
F = 1 3 3 2 1 1
F/A = 5 X 3 5 6 X
F/G = 3 X 3 2 1 X
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
G7 = 3 5 3 4 3 3
