Leandro & Leonardo - O Quanto o Nosso Amor Valeu

[Intro] Em  Bm  C  G  D

E|---------------------------|---------------------------|
B|---------------------------|---------------------------|
G|---------------------------|---------------------------|
D|---5--7--7h9---------------|--5--7--7h9----------------|
A|--------------7--7--9--7---|-------------7--7--9--7----|
E|---------------------------|---------------------------|

E|---------------------------|---------------------------|
B|--5--7--7h8--5--5----------|---------------------------|
G|-------------------5--5----|--5--7--7h9--7--5--4-------|
D|---------------------------|----------------------7----|
A|---------------------------|---------------------------|
E|---------------------------|---------------------------|

   G                    D
Tentei, mas foi inútil pôr alguém em seu lugar
  C             Am     G
Você marcou demais, você me fez sonhar D7
   G                      D
Busquei em outra boca o sabor do beijo seu,

     C                     G
Em outros braços percebi o quanto o nosso amor
G   D/F#
Valeu

Em            Bm   C                     D    D#º
Saiu da minha vida levou a minha outra metade,
Em                     Bm      C
De tudo que passamos juntos, ficou para mim só a
   C    D#º
Saudade

Em                                     Bm
Cadê você que não me liga pra dizer ao menos se está tudo bem ?
              C
Ou se alguma vez ouviu seu coração pedindo pra
   G
Voltar ?

      Am                                Em
Porque eu muitas vezes me peguei aqui sonhando
                                 D            C
Com você chegando, dizendo: "te amo, amor, voltei
 D    G
Pra ficar”

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D#º = X X 1 2 1 2
Em = 0 2 2 0 0 0
G = 3 2 0 0 0 3
