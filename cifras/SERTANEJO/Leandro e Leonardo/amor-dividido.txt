Leandro & Leonardo - Amor Dividido

Intro: A E/G# D/F# Dm/F A/E B7 E4/7 E7 A E/G# D/F# Dm/F E4/7 E7 A D

                A
     Se for pra ter , você comigo e pensando nele
     Dormir comigo e sonhar com ele
                                      E4/7  E7
     Ter que sorrir pra não chorar de dor
               A               D/F#            A/E
     Pra que viver , sabendo o certo,cometendo erros
                 D                      A
     Se o nosso amor quebrou,não tem conserto
              Bm                  D          E7    A
     Já me cansei do jogo seu , decida entre ele e eu
              D                                       A
  *  Você com outro , fazendo amor e o pensamento em mim
                                 E7
     Do mesmo jeito faz comigo assim
                                  A
     Não quero mais esse amor dividido
                 D                                        A
     Ganhar seus beijos , saber que o outro também já provou

                                 E7
     Te amar sabendo que ele te amou
            D            E7     A
     Amor assim melhor jogar no lixo
          E7
     Você diz que sou seu homem
              A
     Que é só minha e de mais ninguém
                 E7               D            A
     Mas está na cara,com certeza fala pra ele também
           E7
     Passo horas em frente o espelho
             A
     Em pensamento acabo tudo
                    E7               D         E7  D         E4/7
     Mas quando me abraça toda raiva passa , então fico mudo

  SOLO: A F#m D E4/7 E7 A F#m D E4/7 A
  REPETE DESDE *
        E7 A      E/G# D/F# Dm/F A/E    E4/7 D     A/C#  Bm  A
     Então fico mudo                  então fico mudo

----------------- Acordes -----------------
A = X 0 2 2 2 0
A/C# = X 4 X 2 5 5
A/E = 0 X 2 2 2 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D/F# = 2 X 0 2 3 2
Dm/F = X X 3 2 3 1
E/G# = 4 X 2 4 5 X
E4/7 = 0 2 0 2 0 X
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
