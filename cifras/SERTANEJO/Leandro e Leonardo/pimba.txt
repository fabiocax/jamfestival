Leandro & Leonardo - Pimba

Intro: G7 C G7 C

                G7                     C
Pode crer rapaziada tudo em nome da paixão
                   G7
e se a mina tá querendo eu já fico no veneno
                  C
cheio de má intenção
F                                       C
Uma mina apaixonada tem no beijo a sedução
                G7
pode crer rapaziada pimba, amor, felicidade
                    C
só faz bem pro coração
(Estribilho)
C                                   G7
E se elas querem um abraço e um beijinho
                       C
a gente pimba, a gente pimba
                                     G
e se elas querem muito amor muito carinho

                       C
a gente pimba, a gente pimba
                                 G7
e se elas querem um amasso gostosinho
                       C
a gente pimba, a gente pimba
                                  G7
e se elas querem alegria e brincadeira
                       C
a gente pimba, a gente pimba
(Introdução)
                 G7                     C
Pode crer rapaziada tudo em nome da paixão
                  G7
e se amina tá querendo eu já fico no veneno
                  C
cheio de má intenção
F                                       C
Uma mina apaixonada tem no beijo a sedução
                G7
pode crer rapaziada pimba, amor, felicidade
                    C
só faz bem pro coração
(Estribilho)
C                                   G7
E se elas querem um abraço e um beijinho
                       C
a gente pimba, a gente pimba
                                      G7
e se elas querem muito amor muito carinho
                       C
a gente pimba, a gente pimba
                                 G7
e se elas querem um amasso gostosinho
                       C
a gente pimba, a gente pimba
                                  G7
e se elas querem alegria e brincadeira
                       C
a gente pimba, a gente pimba
(Solo
 Repete: Estribilho 3 vezes)

----------------- Acordes -----------------
C = X 3 2 0 1 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G7 = 3 5 3 4 3 3
