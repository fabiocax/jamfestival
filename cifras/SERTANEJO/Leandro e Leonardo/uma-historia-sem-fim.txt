Leandro & Leonardo - Uma História Sem Fim

B7                E
Nosso amor foi assim
B7                G#
Uma história sem fim
A               B7
Sem dizer um adeus
  E
Você fugiu de mim
B7              E
Eu queria saber
B7              G#
A razão e o porquê
A               B7
Preferi a verdade
 E
Se eu ia te prender
C#m
Meu coração sincero
G#m
Te amou demais
A                 B7
Eu só vivia pra você

C#m
Um sentimento lindo
G#m
Não se desfaz
A                  B7
É tão difícil esquecer

E
Será que estou errado?
C#m
Será que sou culpado?
A                         B7
Foi só um caso ou uma paixão?
E
Queria olhar seus olhos
C#m
E ouvir da sua boca
A                        B7
Que não passou de uma ilusão
A                               E
Assim a nossa história vai ter fim
A                 B7            E
Assim a nossa história vai ter fim

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
C#m = X 4 6 6 5 4
E = 0 2 2 1 0 0
G# = 4 3 1 1 1 4
G#m = 4 6 6 4 4 4
