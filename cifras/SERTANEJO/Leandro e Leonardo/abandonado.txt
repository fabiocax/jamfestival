Leandro & Leonardo - Abandonado

Intro: D A E B/D# C#m B F#/A# B E A E

   E                 B/D# C#m                    G#m
    Abandonado por você , tenho tentado te esquecer
   A                       E/G#                         F#m
    No fim da tarde uma paixão , no fim da noite uma ilusão
                         B4/7  B7
    No fim de tudo a solidão
   E                 B/D# C#m                  G#m
    Apaixonado por você , tenho tentado não sofrer
   A                 E/G#                       F#m
    Lendo antigas poesias , rindo em novas companhias
         B4/7        E   B/D#
    E chorando por você
   C#m           G#m                  A
    Mas você não vem , nem leva com você
                 E     B/D#
    Toda essa saudade
   C#m              G#m              F#
    Nem sei mais de mim , onde vou assim
      F#/A#       B4/7   B7
    Fugindo da verda.....de

   E                 F#m B7                E   B/D#
    Abandonado por você , Apaixonado por você
   C#m        E/B            A             F#m            B7
    Sem outro porto ou outro cais , sobrevivendo aos temporais
                         E     A  B7
    Essa paixão ainda me guia
   E                 F#m B7                E   F#m  E/G#  G#7
    Abandonado por você , Apaixonado por você
   C#m        E/B         A                F#m           B7
    Eu vejo o vento te levar , mas tenho estrelas pra sonhar
                           A    E
    E ainda te espero todo dia

 SOLO : C#m G#m A B7 E/G# G#7 C#m G#m F# F#/A# B7 G#7
  C#m           G#m                  A
   Mas você não vem , nem leva com você
                 E     B/D#
    Toda essa saudade
   C#m              G#m              F#
    Nem sei mais de mim , onde vou assim
      F#/A#       B4/7   B7
    Fugindo da verda.....de
                           E
    E ainda te espero todo dia
   C#m   F#m   B7       G#m  C#m
    Te espero......todo dia
   F#m         B7         A  E/G#  F#m  E
    Ainda te espero todo dia,uuu....

----------------- Acordes -----------------
A = X 0 2 2 2 0
B = X 2 4 4 4 2
B/D# = X 6 X 4 7 7
B4/7 = X 2 4 2 5 2
B7 = X 2 1 2 0 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E/B = X 2 2 1 0 0
E/G# = 4 X 2 4 5 X
F# = 2 4 4 3 2 2
F#/A# = 6 X 4 6 7 X
F#m = 2 4 4 2 2 2
G#7 = 4 6 4 5 4 4
G#m = 4 6 6 4 4 4
