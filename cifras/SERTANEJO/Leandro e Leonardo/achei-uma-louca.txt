Leandro & Leonardo - Achei Uma Louca

[Intro] D G C Am F C

                  D           G
Achei uma louca pra gostar de mim
C Am        D                 G
Como eu fui bobo em pensar assim
C   Am       D                 G
Quando eu achei que tava tudo bem
C          F              G            C
Ela foi embora, eu fiquei sem ninguém

           D                  G
Achei uma louca pra gostar de mim
C                              G
Mas nem tudo na vida é como a gente quer
C     Am     D                  G
Só depois de tudo é que eu percebi
C                F           G          C
Que machuquei o coração de uma mulher

                        D
Sempre pensei que uma paixão

G                       C
Durasse apenas um só verão
G                      C
Sempre pensei que o amor
                    F                 G        C
Fosse igual à uma flor, mudando de estação

                     D
Aconteceu com você e eu
G                      C
Não tive tempo de perceber
G                     C
E só depois que te perdi
G              C
É que eu percebi
F            G       C
Quanto amo você

(refrão)
                     D
Sempre pensei que a solidão
G                    C
Fosse mentira do coração
G                 D
Era tão fácil pra mim
G               C
Eu tinha você assim
F                 G     C
Na palma da minha mão

                         D
Bastava um toque do meu olhar
G                    C
E você vinha me procurar
G                    C
Pra nada disso eu liguei
     F                            G    C
Só agora eu sei que aprendi a te amar

(Refrão)
D

----------------- Acordes -----------------
Am = X 0 2 2 1 0
C = X 3 2 0 1 0
D = X X 0 2 3 2
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
