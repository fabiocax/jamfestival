Leandro & Leonardo - Copo de Vinho

Intro: Em F#m G D Em F#m G A4/7 A7

         D                                   Em7
    Descobri , que a felicidade não mora num só coração
   G          A7              Em7   A7
    Num único beijo,só numa paixão
                                          D    A4/7
    E amar é bem mais que uma transa qualquer
         D                                 A7
    Percebi , que o amor não vem sempre do mesmo olhar
                                G
    Do mesmo sorriso,do mesmo lugar
                                   D       Em
    Do corpo de um homem ou de uma mulher

  F#m      G C G                                 D    G D
    Foi assim , depois que eu senti sua mão me tocar
                                 A7
    Eu não tive medo nenhum de sonhar
         Em7                                 A7        D        G D
    Me livrei da saudade de alguém que eu queria tanto esquecer


               A7
    Um copo de vinho pra comemorar
                 D
    O amor e o carinho que você me dá
                    A7                                    D    G D
    Deitado em seus braços,nem lembro que ela já me fez sofrer
               A7
    Um copo de vinho na sede de amar
               D
    Na hora do beijo e do nosso prazer
    D7          G                   A7            D
    Ela foi embora,mas Deus,nessa hora,me trouxe você

Solo 2x: Em F#m G D

  F#m      G C G                                 D    G D
    Foi assim , depois que eu senti sua mão me tocar
                                 A7
    Eu não tive medo nenhum de sonhar
         Em7                                 A7        D        G D
    Me livrei da saudade de alguém que eu queria tanto esquecer

              A7
    Um copo de vinho pra comemorar
                 D
    O amor e o carinho que você me dá
                    A7                                    D    G D
    Deitado em seus braços,nem lembro que ela já me fez sofrer
               A7
    Um copo de vinho na sede de amar
               D
    Na hora do beijo e do nosso prazer
    D7          G                   A7            D
    Ela foi embora,mas Deus,nessa hora,me trouxe você

   Bm         G                   A7          D       Em F#m G D
    Ela foi embora,mas Deus,nessa hora,me trouxe você

----------------- Acordes -----------------
A4/7 = X 0 2 0 3 0
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
Em7 = 0 2 2 0 3 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
