Leandro & Leonardo - A Rotina

Intro:  D  Em  G  A7  D  A7

D                      A7                         Bm D7 G                         Em      A7
E a rotina começa de novo na segunda-feira Esperar tanto tempo sozinho não é brincadeira
       F#7                      Bm              G           E7                           Gm    A7
Terça-feira é muito pior, é difícil ficar sem você      Já é quarta e o sol outra vez    vai aparecer
D                      A7              Bm  D7  G      Em                         A7
E o desejo de novo em meu  peito acende a vontade De pegar o telefone e dizer: amor que saudade
                          F#7   Bm                 G
Quinta-feira eu começo a viver Vem chegando o fim-de-semana
 D                      A7                   D A7  D                         Em
Sexta-feira a gente se ama, se ama, se ama      E aí tá tudo bem, você é o que eu preciso
      A7                    D     A7
Tudo certo eu e você   no paraíso
D                        Em                   G                 A7     D       A7
O cinema pra curtir   e no sábado sonhar No domingo a gente volta   a chorar.

Intro:  D  Em  G  A7  D  A7

D                      A7              Bm  D7  G      Em                         A7
E o desejo de novo em meu  peito acende a vontade De pegar o telefone e dizer: amor que saudade

                          F#7   Bm                 G
Quinta-feira eu começo a viver Vem chegando o fim-de-semana
 D                      A7                   D A7  D                         Em
Sexta-feira a gente se ama, se ama, se ama      E aí tá tudo bem, você é o que eu preciso
      A7                    D     A7
Tudo certo eu e você   no paraíso
D                        Em                   G                 A7     D       A7
O cinema pra curtir   e no sábado sonhar No domingo a gente volta   a chorar.

Intro:  D  Em  G  A7  D  A7

----------------- Acordes -----------------
A7 = X 0 2 0 2 0
Bm = X 2 4 4 3 2
D = X X 0 2 3 2
D7 = X X 0 2 1 2
E7 = 0 2 2 1 3 0
Em = 0 2 2 0 0 0
F#7 = 2 4 2 3 2 2
G = 3 2 0 0 0 3
Gm = 3 5 5 3 3 3
