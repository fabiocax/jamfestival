Leandro & Leonardo - Touro de Rodeio


G                    C              G
Em toda festa de Rodeio tem arrasta pé
C                  G
Aí é que chove mulher
 C                 G   C
A fim de segurar peão
G                    C                 G
Aí eu pego logo um copo e peço uma cerveja
 C                     G
Depois que eu fico beleza
    C                  G    F C
Eu gasto as botas no salão
D              C                   G
Ë neste agarradinho que eu me dou bem
          Em                    D
Sou um campeão não tem pra ninguém
               C                G
Ninguém me derruba dentro da arena
D                C                     G
Eu sou bom de montaria eu sou bom de braço

             Em                  D
Vou prender você dentro do meu laço
            C               G
Dou o meu amor pra você morena
                         D
Eu sou seu Cowboy e Violeiro
     C        D         G
Eu quero em voce me agarrar
                                   D
Meu coração ta que nem touro de rodeio
    C         D           G        D7  G
Pulando pra querer te namorar

----------------- Acordes -----------------
C = X 3 2 0 1 0
D = X X 0 2 3 2
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
