Leandro & Leonardo - Não Olhe Assim

[Intro]  C  G/B  Am  Em  F  C/E  D4/7  D7

 G                   Bm
Tire seus olhos dos meus
         C           D
Eu não quero me apaixonar
 G               Bm
Ficou em mim o adeus
        C                 D
Que deixou esse medo de amar

Em              Bm
Eu já amei uma vez e senti
   C               G  Bm
A força de uma paixão
Em                      Bm
A gente às vezes se entrega demais
    C                 D
Esquece de ouvir a razão

            G       Em
Não olhe assim, não

        Bm           C
Você é linda demais
            G                    D
Tem tudo aquilo que um homem procura
          G   D
Em uma mulher

            G       Em
Não olhe assim, não
         Bm          C
Porque até sou capaz
        G                D
De atender esse meu coração
                    G   D
Que só diz que te quer

( G  Bm  C  D )
( G  Bm  C  D )

Em              Bm
Eu já amei uma vez e senti
   C               G  Bm
A força de uma paixão
Em                      Bm
A gente às vezes se entrega demais
    C                 D
Esquece de ouvir a razão

            G       Em
Não olhe assim, não
        Bm           C
Você é linda demais
            G                    D
Tem tudo aquilo que um homem procura
          G   D
Em uma mulher

            G       Em
Não olhe assim, não
         Bm          C
Porque até sou capaz
        G                D
De atender esse meu coração
                    G   D
Que só diz que te quer

            G       Em
Não olhe assim, não
        Bm           C
Você é linda demais
            G                    D
Tem tudo aquilo que um homem procura
          G   D
Em uma mulher

            G       Em
Não olhe assim, não
         Bm          C
Porque até sou capaz
        G                D
De atender esse meu coração
                    G   D
Que só diz que te quer

----------------- Acordes -----------------
Am = X 0 2 2 1 0
Bm = X 2 4 4 3 2
C = X 3 2 0 1 0
C/E = 0 3 2 0 1 0
D = X X 0 2 3 2
D4/7 = X X 0 2 1 3
D7 = X X 0 2 1 2
Em = 0 2 2 0 0 0
F = 1 3 3 2 1 1
G = 3 2 0 0 0 3
G/B = X 2 0 0 3 3
