Leandro & Leonardo - Talvez Você Se Lembre

(intro) A E F#m E D A B7 E


A               E
Brindamos a felicidade
           F#m
Nosso novo ninho
          C#m
Nossos sentimentos
      Bm                   G
Tanto amor sem nenhuma maldade
               E7               A
Carinhos e abraços e tantos momentos
A                     E
Me lembro,talvez você lembre
              F#m                C#m
Alguns sonhos loucos de amor,tão perfeitos
        Bm                   G
Mas não foi o que sempre sonhei e
                E7                 A     E7
Hoje brindo sozinho a dor do meu peito

A         E             F#m
Ah! coração por favor não vá parar
        C#m
Sem ter razão
      D          E7
Pede a ela pra voltar
A             F#m          B7            E7
Eu preciso me entregar,coração,coração,coração
A        E           F#m
Ah! solidão se o destino é ficar só
    C#m        D          E7
Solidão,dá um jeito na saudade
         A          F#m       B7
Faz um sonho de verdade e me deixe
                      E7
Um pouco mais nessa ilusão
A        E        F#m
Ah! coração por favor não vá parar
          C#m
Sem ter razão
       D         E7
Pede a ela pra voltar
A               F#m          B7             E7
Eu preciso me entregar,coração,coração,coração

----------------- Acordes -----------------
A = X 0 2 2 2 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
G = 3 2 0 0 0 3
