Leandro & Leonardo - Piensa En Mi

[Intro]  A  F#m  D  E7

A                              F#m
En vez de ponerte a pensar en él
A                                F#m
En vez de que vivas llorando por él

          D
Piensa en mi
          E7
Llora por mi
           A
Llama me a mi
                  F#m
No no le hables a él
          D
Piensa en mi
          E7
Llora por mi
           A
Llama me a mi

                  F#m   D  E7
No no le hables a él, a él
              A
No llores por él

A                                 F#m
Recuerda que hace mucho tiempo te amo

Te amo, te amo oh, oh
A                         F#m
Quiero hacerte muy muy feliz
           D            E7
Vamos a tomar primer avión
        A               F#m
Con destino a la felicidad
          D  E7           A
La felicidad para mi eres tú

          D
Piensa en mi
          E7
Llora por mi
           A
Llama me a mi
                  F#m
No no le hables a él
          D
Piensa en mi
          E7
Llora por mi
           A
Llama me a mi
                  F#m   D  E7
No no le hables a él, a él
              A
No llores por él

( A  F#m  D  E7 )
( A  F#m  D  E7 )

A                                 F#m
Recuerda que hace mucho tiempo te amo

Te amo, te amo oh, oh
A                         F#m
Quiero hacerte muy muy feliz
           D            E7
Vamos a tomar primer avión
        A               F#m
Con destino a la felicidad
          D  E7           A
La felicidad para mi eres tú

          D
Piensa en mi
          E7
Llora por mi
           A
Llama me a mi
                  F#m
No no le hables a él
          D
Piensa en mi
          E7
Llora por mi
           A
Llama me a mi
                  F#m   D  E7
No no le hables a él, a él
              A
No llores por él

----------------- Acordes -----------------
A = X 0 2 2 2 0
D = X X 0 2 3 2
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
