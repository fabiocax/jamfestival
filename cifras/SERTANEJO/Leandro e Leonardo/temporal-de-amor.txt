Leandro & Leonardo - Temporal de Amor

[Intro] A  E7  F#m  C#m  D  A  A  B7  E7

E|------------------------------------2----0-2-0-------------|
B|-2---2------0-0-2-3-2-0-2-----2--------3-------3---2---2---|
G|---2---2-------------------2----2--------------------2---2-|
D|------------------------4-----4-----0--0-0-0-0-0-----------|
A|-0-0-0-0------2-2-2-2-2----2----2------------------0-0-0-0-|
E|------------0----------------------------------------------|

E|------------------------------------2----0-2-0------------------|
B|-2---2------0-0-2-3-2-0-2-----2--------3-------3---2---2----0---|
G|---2---2-------------------2----2--------------------2---2------|
D|------------------------4-----4-----0--0-0-0-0-0----------------|
A|-0-0-0-0------2-2-2-2-2----2----2------------------0-0-0-0------|
E|------------0-----------------------------------------------0---|

  A                E
Chuva no telhado, vento no portão
  D      Bm           A  E7
E eu aqui  nesta solidão
 A                   E
Fecho a janela, tá frio o nosso quarto

  D      Bm              A   A7
E eu aqui   sem o teu abraço

 D                     E
Doido pra sentir seu cheiro
 Fº                   F#m
Doido pra sentir seu gosto
 D                    E                D
Louco pra beijar seu beijo matar a saudade
        E       A
E esse meu desejo
 D                E
Vê se não demora muito
 Fº         F#m
Coração tá reclamando
  D                 E                D
Traga logo o seu carinho, tô aqui sozinho
           E    A  E7
Tô te esperando

          A
Quando você chegar
E                    F#m  D
 Tire essa roupa molha---da
              A     E7
Quero ser a toalha
              A       E7
E o seu coberto---o-o-or
          A
Quando você chegar
E                   F#m  D
  Mando a saudade sair
          A            E7
Vai trovejar vai cair
                 D  E7
Um temporal de amor
                 A
Um temporal de amor

[Solo]

 D                     E
Doido pra sentir seu cheiro
 Fº                   F#m
Doido pra sentir seu gosto
 D                    E                D
Louco pra beijar seu beijo matar a saudade
        E       A
E esse meu desejo
 D                E
Vê se não demora muito
 Fº         F#m
Coração tá reclamando
  D                 E                D
Traga logo o seu carinho, tô aqui sozinho
           E    A  E7
Tô te esperando

          A
Quando você chegar
E                    F#m  D
 Tire essa roupa molhada
              A     E7
Quero ser a toalha
              A  E7
E o seu cobertor
          A
Quando você chegar
E                   F#m  D
  Mando a saudade sair
          A            E7
Vai trovejar vai cair
                 D  E7
Um temporal de amor

          A
Quando você chegar
E                    F#m  D
 Tire essa roupa molhada
              A     E7
Quero ser a toalha
              A  E7
E o seu cobertor
          A
Quando você chegar
E                   F#m  D
  Mando a saudade sair
          A            E7
Vai trovejar vai cair
                 D  E7
Um temporal de amor
                 A  E7
Um temporal de amor
                 A
Um temporal de amor

----------------- Acordes -----------------
A = X 0 2 2 2 0
A7 = X 0 2 0 2 0
B7 = X 2 1 2 0 2
Bm = X 2 4 4 3 2
C#m = X 4 6 6 5 4
D = X X 0 2 3 2
E = 0 2 2 1 0 0
E7 = 0 2 2 1 3 0
F#m = 2 4 4 2 2 2
Fº = X X 3 4 3 4
