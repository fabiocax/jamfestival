from fastapi import FastAPI, Request
from fastapi.responses import HTMLResponse, FileResponse
import uvicorn
from fastapi.staticfiles import StaticFiles
import glob
import sys
import os
from fastapi import FastAPI, File, HTTPException, UploadFile
import sqlite3
from fastapi.middleware.cors import CORSMiddleware
os.environ["SDL_VIDEODRIVER"] = "dummy"
from pi import Jam
from pathlib import Path



conn = sqlite3.connect(':memory:')
app = FastAPI()

MODE="defualt"

app.add_middleware(
    CORSMiddleware,
    allow_origins=['*']
)
try:

    DIRJANS=sys.argv[1]
except:
    DIRJANS="jams/"

embeddedStatus=False
if embeddedStatus == True:
    embedded="file:///"
else:
    embedded=""
conn.execute('''CREATE TABLE jam
                (id INTEGER PRIMARY KEY AUTOINCREMENT, nome TEXT, file TEXT)''')

conn.execute('''CREATE TABLE show
                (id INTEGER PRIMARY KEY AUTOINCREMENT, nome TEXT, file TEXT)''')
conn.execute('''CREATE TABLE json
                (id INTEGER PRIMARY KEY AUTOINCREMENT, nome TEXT, file TEXT)''')
ID_LIST=[]
Jam_select=""
music_select=0

def import_cifras():
    
    global ID_LIST
    ID_LIST=[]
    files=''
    conn.execute("DELETE FROM JAM;")
    for pdfs in glob.glob(DIRJANS+'*.pdf'):
        conn.execute("INSERT INTO jam (nome, file) VALUES ('"+pdfs.replace(DIRJANS,'').replace(".pdf",'')+"', '"+pdfs.replace(DIRJANS,'')+"')")
    for pdfs in glob.glob(DIRJANS+'*.txt'):
        conn.execute("INSERT INTO jam (nome, file) VALUES ('"+pdfs.replace(DIRJANS,'').replace(".txt",'')+"', '"+pdfs.replace(DIRJANS,'')+"')")
    conn.commit()
    cursor = conn.execute("SELECT * FROM jam order by nome")
    for row in cursor:
        ID_LIST.append(row[0])
        files=files+"<option value='"+row[2]+"'>"+row[1]+"</option>"
    return files
import_cifras()


def import_show():
    DIRJANS2="show/"
    global ID_LIST
    ID_LIST=[]
    files=''
    conn.execute("DELETE FROM SHOW;")
    for pdfs in glob.glob(DIRJANS2+'*.pdf'):
        conn.execute("INSERT INTO show (nome, file) VALUES ('"+pdfs.replace(DIRJANS2,'').replace(".pdf",'')+"', '"+pdfs.replace(DIRJANS2,'')+"')")
    for pdfs in glob.glob(DIRJANS2+'*.txt'):
        conn.execute("INSERT INTO show (nome, file) VALUES ('"+pdfs.replace(DIRJANS2,'').replace(".txt",'')+"', '"+pdfs.replace(DIRJANS2,'')+"')")
    conn.commit()
    cursor = conn.execute("SELECT * FROM show order by nome")
    for row in cursor:
        ID_LIST.append(row[0])
        files=files+"<option value='"+row[2]+"'>"+row[1]+"</option>"
    return files
import_show()

def import_yaml():
    files=''
    conn.execute("DELETE FROM json;")
    for pdfs in glob.glob(DIRJANS+'*.html'):
        conn.execute("INSERT INTO json (nome, file) VALUES ('"+pdfs.replace(DIRJANS,'').replace(".html",'')+"', '"+pdfs.replace(DIRJANS,'')+"')")
    conn.commit()
    cursor = conn.execute("SELECT * FROM json order by nome")
    for row in cursor:

        files=files+"<option value='"+row[2]+"'>"+row[1]+"</option>"
    return files


@app.get("/", response_class=HTMLResponse)
async def read_items():
    files=import_cifras()
    html=""
    for item in open("app.html"):
        html=html+item.replace('\n','').replace('{filesoptin}',files).replace("{embedded}",embedded)
    return html

@app.get("/show", response_class=HTMLResponse)
async def read_items():
    files=import_show()
    html=""
    for item in open("show.html"):
        html=html+item.replace('\n','').replace('{filesoptin}',files).replace("{embedded}",embedded)
    return html


@app.get("/old", response_class=HTMLResponse)
async def read_items():
    files=import_cifras()
    html=""
    for item in open("app_old.html"):
        html=html+item.replace('\n','').replace('{filesoptin}',files).replace("{embedded}",embedded)
    return html

@app.get("/pi", response_class=HTMLResponse)
async def read_items():
    global MODE
    MODE="pi"
    files=import_cifras()
    html=""
    for item in open("app_pi.html"):
        html=html+item.replace('\n','').replace('{filesoptin}',files).replace("{embedded}",embedded)
    return html

@app.get("/pi2", response_class=HTMLResponse)
async def pi2():
    global MODE
    files=import_cifras()
    MODE="pi2"
    html=""
    for item in open("app_pi2.html"):
        html=html+item.replace('\n','').replace('{filesoptin}',files).replace("{embedded}",embedded)
    return html

@app.get("/pi_select", response_class=HTMLResponse)
async def read_items(name : str ):
    global j
    html=""
    Jam_select=DIRJANS+name+'.mp3'
    j= Jam(Jam_select)
    return html

@app.get("/pi_play", response_class=HTMLResponse)
async def read_items():
    html=""
    j.play()
    return html
@app.get("/pi_stop", response_class=HTMLResponse)
async def read_items():
    html=""
    j.stop()
    return html

@app.get("/pi_curtime", response_class=HTMLResponse)
async def read_items():
    html=j.get_info()
    return html

@app.get("/pi_busy", response_class=HTMLResponse)
async def pi_busy():
    
    try:
            teste=j.is_busy()            
    except:
        raise HTTPException(status_code=404, detail="Item not found")
    
    if teste == False:
        raise HTTPException(status_code=302, detail="Found")
    

@app.get("/pi_cur_music", response_class=HTMLResponse)
async def pi_cur_music():
    global j    
    cursor=conn.execute("SELECT * FROM jam where id="+str(ID_LIST[music_select]))
    for row in cursor:
        
        musica=row[2]
        Jam_select=DIRJANS+row[1]+'.mp3'
        j= Jam(Jam_select)        
        return musica
    return ""
@app.get("/pi_next", response_class=HTMLResponse)
async def pi_next():
    global music_select
    print(MODE)
    if (MODE =="pi2"):
        music_select=music_select+1
    if (MODE == "pi"):
        j.stop()
    
    return ""
@app.get("/pi_previous", response_class=HTMLResponse)
async def pi_previous():
    global music_select
    if (MODE =="pi2"):
        music_select=music_select-1
    if (MODE == "pi"):
        j.play()
    return ""


@app.get("/update", response_class=HTMLResponse)
async def read_items():
    os.system("git pull");
    return ""


@app.post("/uploadfile/")
async def create_upload_file(file: UploadFile = File(...)):
    file_location = f"{DIRJANS}/{file.filename}"
    with open(file_location, "wb+") as file_object:
        file_object.write(file.file.read())
    return {"info": f"Arquivo {file.filename} enviado com sucesso!"}

app.mount("/cifras", StaticFiles(directory="cifras"), name="files")
app.mount("/jams", StaticFiles(directory=DIRJANS), name="jams")
app.mount("/show", StaticFiles(directory="show/"), name="show")
app.mount("/static", StaticFiles(directory='static'), name="statics")
uvicorn.run(app, host="0.0.0.0", port=8000, log_level="info")
